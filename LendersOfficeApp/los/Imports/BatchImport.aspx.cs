using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los;
using LendersOfficeApp.los.common;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.los.Imports
{

	public partial class BatchImport : LendersOffice.Common.BaseServicePage
	{
		protected LendersOfficeApp.los.common.EmployeeRoleChooserLink sEmployeeLoanRepChoice;
		protected LendersOfficeApp.los.common.EmployeeRoleChooserLink sEmployeeProcessorChoice;
		protected LendersOfficeApp.los.common.EmployeeRoleChooserLink sEmployeeManagerChoice;
		protected LendersOfficeApp.los.common.EmployeeRoleChooserLink sEmployeeLoanOpenerChoice;
		protected LendersOfficeApp.los.common.EmployeeRoleChooserLink sEmployeeCallCenterAgentChoice;
		protected LendersOfficeApp.los.common.EmployeeRoleChooserLink sEmployeeRealEstateAgentChoice;
		protected LendersOfficeApp.los.common.EmployeeRoleChooserLink sEmployeeLenderAccExecChoice;
		protected LendersOfficeApp.los.common.EmployeeRoleChooserLink sEmployeeLockDeskChoice;        
        protected string m_title;
        protected string m_importFunction = "";
        protected string m_fileFilter = "";
		protected string m_fileExtensions = "";
        protected bool m_isPointImport = false;
        protected bool m_isFnmaImport = false;
        protected string m_format = "";
		protected bool importingWithTemplate = false;
        protected bool isEditLeadsInFullLoanEditor = false;
		protected System.Web.UI.WebControls.Repeater m_templateRepeater;
    
		static BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private string GetEmployeeNameFromId(Guid employeeId)
		{
            SqlParameter[] parameters = {
                                            new SqlParameter("@EmployeeID", employeeId)
                                        };

			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "GetEmployeeDetailsByEmployeeId", parameters )) 
			{
				return (reader.Read())?(string) reader["FullName"]:null;
			}
		}

        // OPM 9831 db - I'm commenting this out because at some point, tbe button that calls this function in the UI
        // was permanently hidden, therefore I'm not updating this function for case 9831.  If we do
        // ever need this function again, it will need to be changed to match the current assignment mechanism
		/*protected void PopulateRelationships(object sender, System.EventArgs args) 
		{
			EmployeeRelationships eRl = new EmployeeRelationships();
			eRl.Retrieve( BrokerUser.EmployeeId );

			EmployeeRoleChooserLink[] rLinks = new EmployeeRoleChooserLink[]
				{
					sEmployeeManagerChoice
					, sEmployeeLenderAccExecChoice
					, sEmployeeLockDeskChoice
				};
			
			foreach( EmployeeRoleChooserLink eL in rLinks )
			{
				if( BrokerUser.IsInRole( eL.Role ) == false && eRl.Contains(eL.Role))
				{
					EmployeeRelationships.Spec rS = eRl[eL.Role];
					string employeeName = GetEmployeeNameFromId(rS.EmployeeId);
					if(employeeName != null)
					{
						eL.Data = rS.EmployeeId.ToString();
						eL.Text = employeeName;
					}
				}
			}
		}*/

        public static string GetDefEmployeeIdIdForSpecificRole( string role, bool isPointImport, out string desc )
        {
            // OPM 9831 - For the Manager, Processor, and Lender Acct Execs, we want to default to "Use LO Settings"
            // even if the importing user already has those roles.  Per the spec:
            // "Default the Lender Account Exec and Manager to Use LO Settings. Do this even if the 
            // current user has the Lender Acct Exec or the Manager role. It�s better to rely on the agent 
            // assignment of the LO than the current user�s role since the lender sometime gives a user roles 
            // that they don�t really belong to so that they can bypass certain restrictions."
            // Note that Processor was also added to this.

            if (BrokerUser.IsInRole(role) == true && role != "Manager" && role != "LenderAccountExec")
            {
                desc = BrokerUser.DisplayName;
                return BrokerUser.EmployeeId.ToString();
            }

            if( isPointImport == true )
            {
                if( role == "Agent")
                {
                    desc = "Auto-matching";
                    return "Current";
                }
                else if (role == "Manager" || role == "LenderAccountExec" || role == "Processor")
                {
                    desc = "Use LO Settings";
                    return "Current";
                }
                else
                {
                    desc = "None";
                    return "None";
                }
            }
            else
            {
                if (role == "Manager" || role == "LenderAccountExec" || role == "Processor")
                {
                    desc = "Use LO Settings";
                    return "Current";
                }
                else
                {
                    desc = "None";
                    return "None";
                }
            }
        }

        protected void PageInit( object sender, System.EventArgs a ) 
        {
			
			
			string formatType = RequestHelper.GetSafeQueryString("format");
			if( (formatType == null) || (formatType.TrimWhitespaceAndBOM().Equals("")) )
			{
				HttpCookie cookie = HttpContext.Current.Request.Cookies.Get("lastFT");
				if(null != cookie && cookie.Value.TrimWhitespaceAndBOM() != "")
					formatType = cookie.Value.TrimWhitespaceAndBOM();
			}

			RequestHelper.StoreToCookie("lastFT", formatType, SmallDateTime.MaxValue);
			
			switch (formatType) 
            {
                case "FannieMae":
                    SetupFannieMaeImport();
                    break;
                case "Mismo":
                    SetupMismo23Import();
                    break;
                case "Point":
                default:
                    SetupCalyxPointImport();
                    break;
            }

            DragDropZone.Attributes.Add("extensions", m_fileExtensions);

			if( IsPostBack == false )
			{
				// 8/19/2005 kb - We now use chooser links.  Each link is initialized
				// to point to the employee if he has the role or none.

				EmployeeRoleChooserLink[] rLinks = new EmployeeRoleChooserLink[]
				{ sEmployeeLoanRepChoice
				, sEmployeeProcessorChoice
				, sEmployeeManagerChoice
				, sEmployeeLoanOpenerChoice
				, sEmployeeCallCenterAgentChoice
				, sEmployeeRealEstateAgentChoice
				, sEmployeeLenderAccExecChoice
				, sEmployeeLockDeskChoice
                , sEmployeeCloserChoice
				};

				foreach( EmployeeRoleChooserLink eL in rLinks )
				{
                    string name;
                    eL.Data = GetDefEmployeeIdIdForSpecificRole( eL.Role, m_isPointImport, out name );
                    eL.Text = name;
				}
			}

            BindBranch(sBranchId);

            if (m_isPointImport)
                sStatusT.Enabled = false; // 7/19/2005 dd - Do not allow user to set status option while import POINT file.
            else
                Tools.Bind_sStatusT_ForBatchImport(sStatusT, BrokerUser.BrokerId);

			RegisterService("main", "/los/Imports/BatchImportService.aspx");

            BrokerDB brokerDB = BrokerDB.RetrieveById(BrokerUser.BrokerId);

            importingWithTemplate = brokerDB.ForceTemplateOnImport;
            isEditLeadsInFullLoanEditor = brokerDB.IsEditLeadsInFullLoanEditor;

            EnableJqueryMigrate = false;
            RegisterJsGlobalVariables("isEditLeadsInFullLoanEditor", isEditLeadsInFullLoanEditor);
            RegisterJsScript("angular-1.5.5.min.js");
            RegisterJsScript("DragDropUpload.js");
            RegisterJsGlobalVariables("MaxJsonLength", ConstApp.MaxJsonLength);
			RegisterJsScript("LQBPopup.js");
            RegisterCSS("DragDropUpload.css");
        }

		protected void ChoiceFilter( object sender , LendersOfficeApp.los.common.FixedChoice choice )
		{
			if( m_isPointImport == false && m_isFnmaImport == false)
			{
				if( choice.Data == "Current" )
				{
                    choice.Use = false;
				}
				else
				{
					choice.Use = true;
				}
			}
			else
			{
				choice.Use = true;
			}
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			SetupTemplateList();
		}

        private void SetupMismo23Import() 
        {
            m_title = "Import Loans";
            this.RegisterJsGlobalVariables("m_fileFilter", ".xml$");
            m_fileExtensions = ".xml";
            m_importFunction = "ImportMismo23";
            m_format = "Mismo";
        }
        private void SetupCalyxPointImport() 
        {
            m_title = "Import Loans";
            this.RegisterJsGlobalVariables("m_fileFilter", ".(brw|prs)$");
            m_fileExtensions = ".brw, .prs, .cb1, .cb2, .cb3, .cb4, .cb5";
            m_importFunction = "ImportCalyx";
            m_isPointImport = true;
            m_format = "Point";
        }
        private void SetupFannieMaeImport() 
        {
            m_title = "Import Loans";
            this.RegisterJsGlobalVariables("m_fileFilter", ".(fnm|dat|exp|imp|0du|1du|2du|3du|4du|5du|6du|7du|8du|9du)$");
			m_fileExtensions = ".fnm, .dat, .exp, .imp, .0du, .1du, .2du, .3du, .4du, .5du, .6du, .7du, .8du, .9du";
            m_importFunction = "ImportFannie";
            m_isFnmaImport = true;
            m_format = "FannieMae";
            if (BrokerUser.DoLastLoginNm != "") 
            {
                FannieMaeMORNETUserID.Text = BrokerUser.DoLastLoginNm;
                RememberDoLogin.Checked = true;
            } 
            else if (BrokerUser.DuLastLoginNm != "") 
            {
                FannieMaeMORNETUserID.Text = BrokerUser.DuLastLoginNm;
                RememberDoLogin.Checked = true;
            }
        }
		private void BindBranch(DropDownList ddl) 
        {
            if(BrokerUser.HasPermission(Permission.BrokerLevelAccess)) 
            {
                // Only broker access level able to search by branch.
                SqlParameter[] parameters = {
                                                 new SqlParameter("@BrokerID", BrokerUser.BrokerId )
                                            };

                using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( BrokerUser.BrokerId, "ListBranchByBrokerID" , parameters ) ) 
                {
                    ddl.DataSource = reader;
                    ddl.DataValueField = "BranchID";
                    ddl.DataTextField = "Name";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem("<-- Any branch -->", Guid.Empty.ToString()));
                }
            } 
            else 
            {
                ddl.Items.Insert(0, new ListItem("<-- N/A -->", Guid.Empty.ToString()));
                ddl.Enabled = false;
            }
        }

		private void SetupTemplateList()
		{
			if ( importingWithTemplate )
			{
				DataSet dS = new DataSet();

				Guid brokerId = BrokerUser.BrokerId;
				Guid branchId = BrokerUser.BranchId;
				Guid employeeId = BrokerUser.EmployeeId;
				bool filterByBranch = !BrokerUser.HasPermission( Permission.AllowAccessToTemplatesOfAnyBranch );

				SqlParameter[] parameters = {
												new SqlParameter( "@BrokerId", brokerId),
												new SqlParameter( "@BranchId", branchId),
												new SqlParameter( "@EmployeeId", employeeId),
												new SqlParameter( "@FilterByBranch", filterByBranch)
											};

				// 12/16/2008 PA
				#region Sorted Template List
				DataSetHelper.Fill(dS, brokerId, "RetrieveLoanTemplateByBrokerID", parameters);

				if (dS.Tables.Count > 0)
				{
					dS.Tables[0].DefaultView.Sort = "sLNm ASC";
					m_templateList.DataSource = dS.Tables[0].DefaultView;
					m_templateList.DataValueField = "sLId";
					m_templateList.DataTextField = "sLNm";
					m_templateList.DataBind();
					m_templateList.Items.Insert(0, new ListItem("<-- Select Template -->", Guid.Empty.ToString()));
				}
				#endregion

				#region Original template list
				/*
				using ( DbDataReader reader = StoredProcedureHelper.ExecuteReader( "RetrieveLoanTemplateByBrokerID" , parameters ) ) 
				{
					m_templateList.DataSource = reader;
					m_templateList.DataValueField = "sLId";
					m_templateList.DataTextField = "sLNm";
					m_templateList.DataBind();
					m_templateList.Items.Insert(0, new ListItem("<-- Select Template -->", Guid.Empty.ToString()));
				}
				*/
				#endregion
			}
		}

		#region Web Form Designer generated code
		protected override void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}

}
