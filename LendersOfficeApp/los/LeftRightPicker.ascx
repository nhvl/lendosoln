﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftRightPicker.ascx.cs"
    Inherits="LendersOfficeApp.los.LeftRightPicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<style type="text/css">
    .LRPHeader
    {
        font-weight: bold;
    }
</style>

<script type="text/javascript">

var idPrefix = '#' + <%=AspxTools.JsString(ClientID) %> + '_';

<% if (!this.EnableModalBehavior) { %>
    function <%= AspxTools.JsStringUnquoted(ClientID) %>_GetSelectedItems() {
        var $assigned = $(idPrefix + 'Assigned');
        var assignedValues = [];
        $assigned.find('option').each(function(index, element) {
            assignedValues.push(element.value);
        });

        return assignedValues;
    }
<% } %>

jQuery(function($){
    $('#assign').click(onAssign);
    $('#remove').click(onRemove);

    var $available = $(idPrefix + 'Available');
    var $assigned = $(idPrefix + 'Assigned');
    //For some reason manipulating these guys with javascript shrinks them
    //So remember their widths so we can reset them.
    var availableWidth = $('#Left').width();
    var assignedWidth = $('#Right').width();

    /*<%--
    Deselect everything in the other multi-select when we're changed,
    so only one can be selected at a time
    --%>*/
    $available.change(function() { $assigned.val('') });
    $assigned.change(function() { $available.val('') });

    $available.dblclick(function() { transferSelected($available, $assigned) });
    $assigned.dblclick(function() { transferSelected($assigned, $available) });

    <% if (this.EnableModalBehavior) { %>

    if (ML.EnableSaveOnUnload) {
        $(window).on("beforeunload", function(){
            onSave();
        });
    }

    $(idPrefix + 'm_btnSave').click(onSave);

    /*<%-- If we have some initial assignments, they override whatever the server gave us
    --%>*/

    var init = (getModalArgs() && getModalArgs().init) || [];

    if($.isArray(init))
    {
        transferAll($assigned, $available);
        transferByID($available, $assigned, init);
    }

    /*<%--And then make it a nice size for the user
     --%>*/
    if(window.resize) resize(800, 610);
    <% } %>

    function onAssign() {
        transferSelected($available, $assigned);
    }

    function onRemove() {
        transferSelected($assigned, $available);
    }

    function onSave() {
        var res = new Array();
        var valuesString = [];
        $assigned.find('option').each(function(index, element) {
            res.push({text: element.text, value: element.value});
            valuesString.push(element.value);
        });
        args = window.dialogArguments || {};

        args.OK = true;

        args.result = res;
        var resJSON = JSON.stringify(res);
        args.resultJSON = resJSON;

        args.valuesString = valuesString.join('|');
        var valuesJSON = JSON.stringify(valuesString);
        args.valuesJSON = valuesJSON;

        if(args.callback && typeof(args.callback) == "function")
        {
            args.callback(res, resJSON, valuesJSON);
        }
        onClosePopup(args);
    }

    function transferSelected(from, to) {
        transfer(from, to, 'option:selected');
    }

    function transferAll(from, to) {
        transfer(from, to, 'option');
    }

    function transfer(from, to, selector){
        //For every option
        $(from).find(selector).each(function(index, elem) {
            //append its outerHTML (no, we can't just use .html(), that's too easy :( )
            $(to).append(elem.outerHTML);
            //And then finally remove all of the selected options
        }).remove();

        if ($('#IsLoanStatusPicker:checked').length) {
            // Loan statuses use a different method that doesn't sort alphabetically.
            sortLoanStatuses(to);
        }
        else {
            sortList(to);
        }
    }

    // Instead of sorting alphabetically, when picking loan statuses we need
    // to "sort" chronologically, using the s_sStatusT_Order ordering.
    function sortLoanStatuses(target) {
        var orderedSelections = [];
        var statuses = <%= AspxTools.JsArray(CPageBase.s_sStatusT_Order.Select(s => CPageBase.sStatusT_map_rep(s))) %>;

        // For each status in the chronological ordering
        for (var i = 0; i < statuses.length; i++) {
            var matchingStatus = $(target).find("option[value='" + statuses[i] + "']")[0];

            // If it appears in the target list, add it to our selections
            if (matchingStatus) {
                orderedSelections.push(matchingStatus.outerHTML);
            }
        }

        $(target).empty().append(orderedSelections.join(''));
    }

    function sortList(target)
    {
        var options = $(target).children('option');
        options.sort(function(a, b) {
            if ( a.text.toLowerCase() < b.text.toLowerCase() ) {
                return -1;
            }

            if ( a.text.toLowerCase() > b.text.toLowerCase() ) {
                return 1;
            }

            return 0;
        });
        $(target).empty().append(options);
    }

    function transferByID(from, to, idList)
    {
        $(from).find('option').filter(function(index){
            return ($.inArray(this.value, idList) != -1);
        }).each(function(index, elem){
            $(to).append(elem.outerHTML);
        }).remove();
    }
});
</script>

<style type="text/css">
    span.Instructions
    {
        display: block;
    }
    div.Action
    {
        width: 100%;
    }

    div.Action input
    {
        width: 98%;
    }

    div.Close
    {
        margin-top: 70px;
    }

    div.Cancel
    {
        display: none;
    }

    #Middle
    {
        width: 9.8%;
        height: 100%;
        margin-top: 20%;
        position: relative;
    }
    #Left, #Right { width: 44%; }
    #Left { float: left; margin-left: 1%; }
    #Right { margin-right: 1%; }
    div.Column
    {
        float: right;
    }
    div.Column span { display: block; }
    div.Column span.LRPHeader { padding-bottom: 4px; }
    div.Column span.Instructions { padding-bottom: 4px; }
</style>
<div id="LRPickerMain">
    <div id="TopSpacing"> &nbsp; </div>
    <div id="Right" class="Column">
        <span class="LRPHeader"> <ml:EncodedLiteral ID="RightColumnHeader" runat="server"></ml:EncodedLiteral> </span>
        <span class="Instructions">
            &nbsp;
        </span>
        <div>
            <select runat="server" id="Assigned" multiple="true" size="30" style="width: 100%; min-height: 428px;" />
        </div>
    </div>
    <div id="Middle" class="Column">
        <div class="Assign Action">
            <input type="button" value="Add >>" id="assign"/>
        </div>
        <div class="Remove Action">
            <input type="button" value="Remove <<" id="remove"/>
        </div>
        <div class="Close Action">
            <asp:Button ID="m_btnSave" runat="server" Text="Close" />
        </div>
        <div class="Cancel Action">
            <input type="button" value="Cancel" onclick="onClosePopup();" />
        </div>
    </div>
    <div id="Left" class="Column">
        <span class="LRPHeader"> <ml:EncodedLiteral ID="LeftColumnHeader" runat="server"></ml:EncodedLiteral> </span>
        <span class="Instructions">
                <i>(To select multiple items, hold the "<b>CTRL</b>" key down while selecting)</i>
        </span>
        <div>
            <select runat="server" id="Available" multiple="true" size="30" style="width: 100%; min-height: 428px;" />
        </div>
    </div>
</div>
