<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ils" TagName="EmployeeRoleChooserLink" Src="common/EmployeeRoleChooserLink.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Footer" Src="common/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Header" Src="common/header.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="loanfind.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.loanfind" enableViewState="True" EnableSessionState="False"  ValidateRequest="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
		<title>Find Loan</title>
  </HEAD>

	<body bottomMargin="0" bgColor="gainsboro" leftMargin="0" topMargin="0" rightMargin="0" scroll="yes">
    <script type="text/javascript">
    var defaultLoanTempleName;
    var quickPricerTemplateName;
    var gSelectionCount = 0;
    var g_sNoLoanSelectedMessage = <%= AspxTools.JsString(JsMessages.NoLoanSelected) %>;


    function _init()
    {
        switchStatusDDL();
        if (typeof (refreshOpenWindowList) == "function") refreshOpenWindowList();
  		disableButtons(true);

    }

    function switchStatusDDL()
    {

        if(document.getElementById("m_searchForLeadsRB").checked)
        {
            document.getElementById("m_loanStatusDDL").style.display = "none";
            document.getElementById("m_leadStatusDDL").style.display = "";
        }
        else{
            document.getElementById("m_loanStatusDDL").style.display = "";
            document.getElementById("m_leadStatusDDL").style.display = "none";
        }
    }

    function resetDates() {
        var ddl = <%= AspxTools.JsGetElementById(m_createdDateDDL) %>;
        if( ddl ) {
            ddl.selectedIndex = 0;
        }
    }

    function BatchExport(id)
    {
      showModal('/LegacyLink/CalyxPoint/BatchExportOptions.aspx?id=' + id);
    }

    function BatchDataTracExport(id)
    {
      showModal('/Integration/DataTrac/BatchExportOptions_DataTrac.aspx?id=' + id);
    }
function onCheckAll()
{
  var cb = document.getElementsByName("cb");
  var maincb = document.getElementById("maincb");

  if (typeof(cb) == "undefined") return;

  var count = 0;

  if (cb.length == null) {
    cb.checked = maincb.checked;
    highlightRowByCheckbox(cb);
    count = 1;
  } else {
    count = cb.length;
    for (var i = 0; i < count; i++) {
      cb[i].checked = maincb.checked;
      highlightRowByCheckbox(cb[i]);

    }

  }
  if (!maincb.checked)
    gSelectionCount = 0;
  else
    gSelectionCount = count;

  disableButtons(gSelectionCount == 0);
}
function hasCheck() {
  var ret = false;

  var collection = document.getElementsByName("cb");
  if (typeof(collection) != "undefined") {
    if (collection.length != null) {
      for (var i = 0; i < collection.length; i++) {
        if (collection[i].checked) {
          ret = true;
          break;
        }
      }
    } else {
      // Only one record in search result.
      ret = collection.checked;
    }
  }
  if (!ret)
    alert(g_sNoLoanSelectedMessage);
  return ret;
}

function logOut() {
    var logout = retrieveFrameProperty(parent, "frmCode", "logOut");

    if (logout != null) {
        logout();
    }
    else {
        location.href = <%= AspxTools.JsString(VirtualRoot) %> + '/logout.aspx';
    }
}

function editLoan(id, name) {
    callFrameMethod(parent, "frmCode", "editLoan", [id, name]);
}
function editLead(id, name) {
    callFrameMethod(parent, "frmCode", "editLead", [id, name]);
}

function viewLoan(id) {
    callFrameMethod(parent, "frmCode", "viewLoan", [id]);
}

function viewLoanAccessPermission(userid, loanId) {
}


function createTask( loanId )
{
  var isTemplate = <%= AspxTools.JsGetElementById(m_searchForTempsRB) %>.checked ? 'True' : 'False';
  var queryString = '?loanId=' + loanId + '&IsTemplate=' + isTemplate;
  var options = 'width=750,height=400,center=yes,resizable=yes,scrollbars=yes,status=yes,help=no';
  window.open(VRoot + ML.TaskEditorUrl + queryString, '_blank', options);
}



function batchDelete() {


  var extraQueryString = <%= AspxTools.JsString(getExtraQueryString()) %>;

  var list = getCheckList();
  if (list.length == 0) {
    alert(g_sNoLoanSelectedMessage);
    return;
  }

  if (list.length == 1) {
   	if(list[0] == <%= AspxTools.JsString(PMLDefaultLoanTemplateID) %>)
   	{
		alert('Unable to delete ' + '"'+defaultLoanTempleName+'"' + ' because it is the Default Loan Template.\nContact an administrator for assistance with deleting the ' + '"'+defaultLoanTempleName+'"' + ' loan template.');
		return;
	}
	else if(list[0] == <%= AspxTools.JsString(QuickPricerTemplateId) %>)
   	{
		alert('Unable to delete ' + '"'+quickPricerTemplateName+'"' + ' because it is the QuickPricer Template.\nContact the administrator for assistance with deleting the ' + '"'+quickPricerTemplateName+'"' + ' loan template.');
		return;
	}
  }


  if (confirm(<%= AspxTools.JsString(JsMessages.ConfirmLoanDelete) %>)) {
      var args = new Object();
      args["Count"] = list.length + '';
	  var n = 0;
      for (var i = 0; i < list.length; i++)
      {
		if(list[i] == <%= AspxTools.JsString(PMLDefaultLoanTemplateID) %>)
        {
			alert('Unable to delete ' + '"'+defaultLoanTempleName+'"' + ' because it is the Default Loan Template.\nContact an administrator for assistance with deleting the ' + '"'+defaultLoanTempleName+'"' + ' loan template.');
			var xCount = (args["Count"] - 1);
			args["Count"] = xCount;
		}
		else if(list[i] == <%= AspxTools.JsString(QuickPricerTemplateId) %>)
   	    {
   	        alert('Unable to delete ' + '"'+quickPricerTemplateName+'"' + ' because it is the QuickPricer Template.\nContact the administrator for assistance with deleting the ' + '"'+quickPricerTemplateName+'"' + ' loan template.');
			var xCount = (args["Count"] - 1);
			args["Count"] = xCount;
	    }
	    else
	    {
            args["LoanID_" + n] = list[i];
            n++
        }
      }

      var result = gService.loanutils.call("BatchDelete", args);

	  if( !result.error )
	  {
        if( result.value.Error != null && result.value.Error == "True" )
		{
		  if( result.value.UserMsg != null )
		  {
			alert( result.value.UserMsg );
		  }
		  else
		  {
			alert( <%= AspxTools.JsString(JsMessages.FailedToCompleteOperation) %> );
		  }
		}
		else
		{
		  <%= AspxTools.JsGetElementById(m_findBtn) %>.click();
		}
	  }
	  else
	  {
		alert( <%= AspxTools.JsString(JsMessages.FailedToCompleteOperation) %> );
	  }
  }

}


function assignChangeStatus() {
  var list = getAssignList();

  if (list.length == 0) {
    alert(g_sNoLoanSelectedMessage);
    return;
  }
  else if( list.length > 100 ) {
    alert(<%= AspxTools.JsString(JsMessages.TooManyLoansSelected) %>);
    return;
  }
  var args = {
    Loans  : "['" + list.join("','")+"']"
  };

  var result = gService.utils.call("CacheLoans", args);

  if(result.error)
  {
    alert('Unable to store assigned loans');
    return;
  }

  showModal("/los/Portlets/AssignAndChangeStatus.aspx?key=" + result.value.Key , null, null, null, function(args){
		if (args.OK) {
			var removeFromCurrentLoanListExists = (typeof (removeFromCurrentLoanList) == "function");
			if (args.bConvertedLeadToLoan && removeFromCurrentLoanListExists) {
				var length = args.convertedIds.length;
				for (var i = 0; i < length; i++) {
					removeFromCurrentLoanList(args.convertedIds[i]);
					callFrameMethod(parent, "frmCode", "closeWindow", [args.convertedIds[i]]);
				}
			}
			<%= AspxTools.JsGetElementById(m_findBtn) %>.click();
		}
	}, {hideCloseButton:true});
}
function disableButtons(b) {
  var btnDelete = document.getElementById("btnDelete");
  var btnAssign = document.getElementById("btnAssign");

  if (null != btnDelete)
	btnDelete.disabled = b;
  if (null != btnAssign)
    btnAssign.disabled = b;

  if(<%= AspxTools.JsBool(CanExportLoans) %>)
  {
	<%= AspxTools.JsGetElementById(m_exportBtn)%>.disabled = b;
	if (<%= AspxTools.JsGetElementById(m_exportToDTBtn)%>)
		<%= AspxTools.JsGetElementById(m_exportToDTBtn)%>.disabled = b;
  }
}
function onSelectCheck(cb) {
  if (cb.checked)
    gSelectionCount++;
  else
    gSelectionCount--;
  highlightRowByCheckbox(cb);
  disableButtons(gSelectionCount == 0);
}
function getAssignList() {
  var list = new Array();

  var collection = document.getElementsByName("cb");
  if (typeof(collection) == "undefined") {
    return list;
  }

  var length = collection.length;

  for (var i = 0; i < length; i++) {
    if (collection[i].checked) {
      var loanid = collection[i].value.substring(0, 36);
      var loanName = collection[i].value.substring(36);
      if (collection[i].getAttribute('isAssignable') == 'True') {
        list.push(loanid);
      } else {
        alert('You do not have permission to assign this loan ' + loanName + '. Skipping this loan.');
      }
    }
  }

  // This code is for if there only one checkbox in datagrid.
  if (collection.length == null) {
    if (collection.checked) {
      var loanid = collection.value.substring(0, 36);
      var loanName = collection.value.substring(36);
      if (collection.getAttribute('isAssignable') == 'True')
        list.push(loanid);
      else
        alert('You do not have permission to assign this loan ' + loanName + '. Skipping this loan');

    }
  }

  return list;
}

function getCheckList() {
  var list = new Array();

  var collection = document.getElementsByName("cb");
  if (typeof(collection) == "undefined") {
    return list;
  }

  var length = collection.length;
  for (var i = 0; i < length; i++) {
    if (collection[i].checked) {
      var loanid = collection[i].value.substring(0, 36);
      var loanName = collection[i].value.substring(36);
      if(<%= AspxTools.JsString(PMLDefaultLoanTemplateID) %> == loanid)
      {
         defaultLoanTempleName = loanName;
      }
      if(<%= AspxTools.JsString(QuickPricerTemplateId) %> == loanid)
      {
         quickPricerTemplateName = loanName;
      }
      if (collection[i].getAttribute('isEditable') == 'True') {
        list.push(loanid);
      } else {
        alert('You do not have permission to edit this loan ' + loanName + '. Skipping this loan.');
      }
    }
  }

  // This code is for if there only one checkbox in datagrid.
  if (collection.length == null) {
    if (collection.checked) {
      var loanid = collection.value.substring(0, 36);
      var loanName = collection.value.substring(36);
      if(<%= AspxTools.JsString(PMLDefaultLoanTemplateID) %> == loanid)
      {
         defaultLoanTempleName = loanName;
      }
      if(<%= AspxTools.JsString(QuickPricerTemplateId) %> == loanid)
      {
         quickPricerTemplateName = loanName;
      }
      if (collection.getAttribute('isEditable') == 'True')
        list.push(loanid);
      else
        alert('You do not have permission to edit this loan ' + loanName + '. Skip this loan');

    }
  }

  return list;
}
function f_takeSnapshot() {
  var list = getCheckList();
  if (list.length == 0) {
    alert(g_sNoLoanSelectedMessage);
    return;
  }

  var args = new Object();
  args["Count"] = list.length + '';
  for (var i = 0; i < list.length; i++)
    args["LoanID_" + i] = list[i];

  var result = gService.main.call("TakeResultSnapshot", args);
  if (!result.error) {
    var key = result.value["Key"];

    window.open('DownloadSnapshot.aspx?key=' + key, "_parent", "toolbar=no,menubar=no,location=no,status=no");
  }
}

    function f_toggleLoanOptions(loanId) {

        var optionDiv = document.getElementById('LoanOptions_' + loanId);

        if (optionDiv.style.display === 'block') {
            optionDiv.style.display = 'none';
            return;
        }
        else {
            optionDiv.style.display = 'block';
        }

        var data = {'loanId': loanId};

        var str_data = JSON.stringify(data);

            callWebMethodAsync({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'PipelineService.aspx/GetLoanOptions',
            data: str_data,
            dataType: 'json',
            async: false,
            success: function(response) {optionDiv.innerHTML = response.d; },
            failure: function(response) { alert("error - " + response); },
            error: function(XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); }
        });
    }

        function rateLock( loanId, action )
        {
            var queryString = '?loanId=' + loanId + '&action=' + action;
            var options = 'width=770,height=400,center=yes,resizable=yes,scrollbars=yes,status=yes,help=no';
            window.open(VRoot + '/los/RateLock/LockRate.aspx' + queryString, '_blank', options);
        }
    </script>
		<form id="loanfind" method="post" runat="server">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
				<tr valign="top" style="HEIGHT: 100%">
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td>
									<uc1:header id="m_header" runat="server"></uc1:header>
								</td>
							</tr>
							<tr>
								<td style="PADDING-TOP: 0px" vAlign="top">
									<table cellSpacing="2" cellPadding="3" width="100%" border="0">
										<tr>
											<td class="FormTableHeader" colSpan="4">
												Search Criteria for Loan / Lead / Loan Template
											</td>
										</tr>
									</table>
									<div style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 6px; BORDER-TOP: 2px groove; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; MARGIN: 6px; BORDER-LEFT: 2px groove; WIDTH: 100%; PADDING-TOP: 6px; BORDER-BOTTOM: 2px groove; BACKGROUND-COLOR: whitesmoke">
										<table class="FormTable" cellSpacing="0" cellPadding="0" border="0" bgcolor="whitesmoke">
											<tr>
												<td valign="top">
													<table cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td valign="top">
																<table cellpadding="0" cellspacing="2" border="0">
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Loan Number
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:textbox id="m_loanNumberTF" runat="server" style="PADDING-LEFT: 4px" Width="140"/>
																			<span style="COLOR: dimgray">** </span>
																		</td>
																	</tr>
                                                                    <tr>
																		<td class="FieldLabel" nowrap>
																			Loan Reference Number
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:textbox id="m_loanReferenceNumberTF" runat="server" style="PADDING-LEFT: 4px" Width="140"/>
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Last Name
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:textbox id="m_lastNameTF" runat="server" style="PADDING-LEFT: 4px" Width="100" />
																			<span style="COLOR: dimgray">** </span>
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			First Name
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:textbox id="m_firstNameTF" runat="server" style="PADDING-LEFT: 4px" Width="100" />
																			<span style="COLOR: dimgray">** </span>
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			SSN (Last Four)
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
                                                                            <input type="text" id="ssnLastFour" runat="server" class="mask" style="PADDING-LEFT: 4px" preset="ssn-last-four" Width="100" />
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Property Address
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:TextBox id="m_propertyAddressTF" runat="server" style="PADDING-LEFT: 4px" Width="140" />
																			<span style="COLOR: dimgray">** </span>
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			MERS MIN Number
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:textbox id="m_mersMinTF" runat="server" style="PADDING-LEFT: 4px" Width="140"/>
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Agency Case Number
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:textbox id="m_agencyCaseNumTF" runat="server" style="PADDING-LEFT: 4px" Width="140"/>
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Lender Case Number
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:textbox id="m_lenderCaseNumTF" runat="server" style="PADDING-LEFT: 4px" Width="140"/>
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Investor Loan Number
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:textbox id="m_investorLoanNumTF" runat="server" style="PADDING-LEFT: 4px" Width="140"/>
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Subservicer Loan Number
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:textbox id="m_subservicerLoanNumTF" runat="server" style="PADDING-LEFT: 4px" Width="140"/>
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" colspan="3">
																			<span style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; WIDTH: 270px; PADDING-TOP: 4px">
																				<font color="dimgray">**</font> Return partial matches for: </span>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3">
																			<table>
																				<tr>
																					<td>
																						<asp:CheckBox id="m_loanNumberPartialMatchCB" runat="server" Text="Loan Number" style="PADDING-LEFT: 8px" />
																					</td>
																					<td>
																						<asp:CheckBox id="m_firstNamePartialMatchCB" runat="server" Text="First Name" />
																					</td>
																				</tr>
																				<tr>
																					<td>
																						<asp:CheckBox id="m_lastNamePartialMatchCB" runat="server" Text="Last Name" style="PADDING-LEFT: 8px" />
																					</td>
																					<td>
																						<asp:CheckBox id="m_propertyAddressPartialMatchCB" runat="server" Text="Property Address" />
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																</table>
															</td>
															<td valign="top">
																<div style="MARGIN: 8px; BORDER-LEFT: 0px groove; WIDTH: 0px">
																</div>
															</td>
															<td vAlign="top">
																<table cellpadding="1" cellspacing="2" border="0">
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Branch
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:DropDownList id="m_branchDDL" runat="server" style="PADDING-LEFT: 4px" Width="140" />
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Loan Status
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:dropdownlist id="m_loanStatusDDL" runat="server" Width="140" />
																			<asp:DropDownList ID="m_leadStatusDDL" runat="server" Width = "140"></asp:DropDownList>
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Status Date
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:DropDownList id="m_createdDateDDL" runat="server" style="PADDING-LEFT: 4px" Width="140" />
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Loan Type
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<asp:dropdownlist id="m_loanTypeDDL" runat="server" Width="140" />
																		</td>
																	</tr>
																	<tr>
																		<td class="FieldLabel" nowrap>
																			Search for
																		</td>
																		<td>
																			&nbsp;
																		</td>
																		<td nowrap>
																			<div style="PADDING-TOP: 8px">
																			    <asp:RadioButton ID="m_searchForLeadsRB" onclick="switchStatusDDL()" runat="server" Text="Leads" GroupName="Search" />
																				<br />
																				<asp:RadioButton id="m_searchForLoansRB" onclick="switchStatusDDL()" runat="server" Text="Loans" GroupName="Search" Checked="True" />
																				<br />
																				<asp:RadioButton id="m_searchForTempsRB" runat="server" Text="Templates" GroupName="Search" onclick="switchStatusDDL(); resetDates();" />
                                                                                <br />
																				<asp:RadioButton id="m_searchForTestLoansRB" onclick="switchStatusDDL()" runat="server" Text="Test Loans" GroupName="Search" />
																			</div>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
												<td valign="top">
													<div style="MARGIN: 8px; BORDER-LEFT: 0px groove; WIDTH: 0px">
													</div>
												</td>
												<td valign="top">
												    <div style="height: 290px; overflow-y: scroll;">
													<table cellpadding="0" cellspacing="2" border="0">
														<!-- Call Center Agent -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Call Center Agent</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_telemarketerChoice" runat="server" RoleType="CallCenterAgent" />
															</td>
														</tr>
														<!-- Closer -->
														<TR>
															<TD class="FieldLabel" noWrap width="160">Closer</TD>
															<TD noWrap>
																<ils:EmployeeRoleChooserLink id="m_closerChoice" runat="server" RoleType="Closer"/>
															</TD>
														</TR>
														<!-- Collateral Agent -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Collateral Agent</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_collateralAgentChoice" runat="server" RoleType="CollateralAgent"/>
															</td>
														</tr>
														<!-- Credit Auditor -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>Credit Auditor</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_creditAuditorChoice"  runat="server" RoleType="CreditAuditor"/>
															</td>
														</tr>
														<!-- Disclosure Desk -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>Disclosure Desk</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_disclosureDeskChoice"  runat="server" RoleType="DisclosureDesk"/>
															</td>
														</tr>
														<!-- Doc Drawer -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>Doc Drawer</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_docDrawerChoice"  runat="server" RoleType="DocDrawer"/>
															</td>
														</tr>
														<!-- Funder -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Funder</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_funderChoice" runat="server" RoleType="Funder"/>
															</td>
														</tr>
														<!-- Insuring -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Insuring</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_insuringChoice" runat="server" RoleType="Insuring" />
															</td>
														</tr>
														<!-- Junior Processor -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>Junior Processor</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_juniorProcessorChoice"  runat="server" RoleType="JuniorProcessor"/>
															</td>
														</tr>
														<!-- Junior Underwriter -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>Junior Underwriter</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_juniorUnderwriterChoice"  runat="server" RoleType="JuniorUnderwriter"/>
															</td>
														</tr>
														<!-- Legal Auditor -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>Legal Auditor</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_legalAuditorChoice"  runat="server" RoleType="LegalAuditor"/>
															</td>
														</tr>
														<!-- Lender Account Executive -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Lender Account Executive</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_lenderAccountExecChoice" runat="server" RoleType="LenderAccountExecutive" />
															</td>
														</tr>
														<!-- Loan Officer -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Loan Officer</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_agentChoice" runat="server" RoleType="LoanOfficer" />
															</td>
														</tr>
														<!-- Loan Officer Assistant -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>Loan Officer Assistant</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_loanOfficerAssistantChoice"  runat="server" RoleType="LoanOfficerAssistant"/>
															</td>
														</tr>
														<!-- Loan Opener -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Loan Opener</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_loanOpenerChoice" runat="server" RoleType="LoanOpener" />
															</td>
														</tr>
														<!-- Lock Desk -->
														<TR>
															<TD class="FieldLabel" noWrap width="160">Lock Desk</TD>
															<TD noWrap>
																<ils:EmployeeRoleChooserLink id="m_lockdeskChoice" runat="server" RoleType="LockDesk" />
															</TD>
														</TR>
														<!-- Manager -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Manager</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_managerChoice" runat="server" RoleType="Manager" />
															</td>
														</tr>
														<!-- Post-Closer -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Post-Closer</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_postCloserChoice" runat="server" RoleType="PostCloser" />
															</td>
														</tr>
														<!-- External Post-Closer -->
														<asp:PlaceHolder runat="server" ID="ExternalPostCloserSearch">
														<tr>
														    <td class="FieldLabel" width="160" nowrap>Post-Closer (External)</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_externalPostCloserChoice" runat="server" RoleType="Pml_PostCloser"/>
															</td>
														</tr>
														</asp:PlaceHolder>
														<!-- Processor -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Processor</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_processorChoice" runat="server" RoleType="Processor" />
															</td>
														</tr>
														<!-- External Processor -->
														<asp:PlaceHolder runat="server" ID="BPSearch">
														    <tr>
														        <td class="FieldLabel" width="160" nowrap>Processor (External)</td>
														        <td>
														            <ils:EmployeeRoleChooserLink id="m_loanBPChoice" runat="server" RoleType="Pml_BrokerProcessor"/>
														        </td>
														    </tr>
                                                        </asp:PlaceHolder>
														<!-- Purchaser -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>Purchaser</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_purchaserChoice"  runat="server" RoleType="Purchaser"/>
															</td>
														</tr>
														<!-- QC Compliance -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>QC Compliance</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_qcComplianceChoice"  runat="server" RoleType="QCCompliance"/>
															</td>
														</tr>
														<!-- Real Estate Agent -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Real Estate Agent</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_realEstateAgentChoice" runat="server" RoleType="RealEstateAgent" />
															</td>
														</tr>
														<!-- Secondary -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>Secondary</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_secondaryChoice"  runat="server" RoleType="Secondary"/>
															</td>
														</tr>
														<!-- External Secondary -->
														<asp:PlaceHolder runat="server" ID="ExternalSecondarySearch">
														<tr>
														    <td class="FieldLabel" width="160" nowrap>Secondary (External)</td>
															<td nowrap>
                                                                <ils:EmployeeRoleChooserLink runat="server" ID="m_externalSecondaryChoice" RoleType="Pml_Secondary"/>
                                                            </td>
														</tr>
														</asp:PlaceHolder>
														<!-- Servicing -->
                                                        <tr>
															<td class="FieldLabel" width="160" nowrap>Servicing</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_servicingChoice"  runat="server" RoleType="Servicing"/>
															</td>
														</tr>
														<!-- Shipper -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Shipper</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_shipperChoice" runat="server" RoleType="Shipper" />
															</td>
														</tr>
														<!-- Underwriter -->
														<tr>
															<td class="FieldLabel" width="160" nowrap>Underwriter</td>
															<td nowrap>
																<ils:EmployeeRoleChooserLink id="m_underwriterChoice" runat="server" RoleType="Underwriter" />
															</td>
														</tr>

													</table>
												    </div>
													<div style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; WIDTH: 360px; COLOR: dimgray; PADDING-TOP: 4px">
														Select employees to search for by clicking the link next to each role. To
														accept "any" assignment, choose "allow any". To only show loans with no
														assignment, choose "unassigned".
													</div>
												</td>
											</tr>
										</table>
										<table cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td>
													<asp:Button id="m_findBtn" runat="server" Text="Find using search parameters" onclick="onFindLoanClick"></asp:Button>
													<input type="button" value="Reset search parameters" onclick="self.location='loanfind.aspx';">


												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
						</table>
						<div style="BORDER-RIGHT: 2px groove; BORDER-TOP: 2px groove; MARGIN: 0px 6px 6px; BORDER-LEFT: 2px groove; WIDTH: 100%; BORDER-BOTTOM: 2px groove; BACKGROUND-COLOR: whitesmoke; TEXT-ALIGN: left">
							<div style="PADDING-RIGHT: 6px;PADDING-LEFT: 6px;PADDING-BOTTOM: 6px;PADDING-TOP: 6px">
								<asp:Button id="m_exportBtn" runat="server" Text="Export selected files to POINT" Enabled="False" onclick="m_exportBtn_Click"></asp:Button>
								<% if (IsDataTracIntegrationEnabled) { %>
								<asp:Button id="m_exportToDTBtn" runat="server" Text="Export selected files to DATATRAC" Enabled="False" onclick="m_exportToDTBtn_Click"></asp:Button>
								<% } %>
								<asp:Panel id="m_deletePanel" runat="server" style="DISPLAY: inline"><INPUT id=btnDelete onclick="if( !parentElement.disabled ) batchDelete();" type=button value="Delete selected files">
								</asp:Panel>
								<input id="btnAssign" type="button" value="Assign &amp; change status" onclick="assignChangeStatus();">
								<% if (IsBecomePlusUser) { %>
								<input id="btnTakeSnapShot" type="button" value="Take Snapshot of Pricing Result" onclick="f_takeSnapshot();">
								<% } %>
							</div>
						</div>
						<div style="BORDER-RIGHT: lightgrey 2px solid; BORDER-TOP: lightgrey 2px solid; MARGIN: 0px 6px 6px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 100%; BORDER-BOTTOM: lightgrey 2px solid; BACKGROUND-COLOR: gainsboro; TEXT-ALIGN: left">
							<div style="PADDING-RIGHT: 6px; PADDING-LEFT: 6px; FONT-WEIGHT: bold; PADDING-BOTTOM: 6px; PADDING-TOP: 6px; TEXT-ALIGN: center<% if (SearchWasPerformed) { %>; BACKGROUND-COLOR: #ffffcc<% } %>">
								<ml:PassthroughLabel id="m_resultCountLabel" runat="server"></ml:PassthroughLabel>
							</div>
						</div>
						<div style="BORDER-RIGHT: lightgrey 2px solid; BORDER-TOP: lightgrey 2px solid; MARGIN: 0px 6px 6px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 100%; BORDER-BOTTOM: lightgrey 2px solid">
							<ml:CommonDataGrid id="m_loanDataGrid" enableViewState="true" runat="server" allowpaging="true" allowcustompaging="true" allowsorting="true" pagesize="50">
							<PagerStyle VerticalAlign="Middle" Mode="NumericPages" horizontalalign="right" Position="TopAndBottom" ></PagerStyle>
								<columns>
									<asp:TemplateColumn>
										<headertemplate>
											<input type="checkbox" id='maincb' name="maincb" onclick="onCheckAll();">
										</headertemplate>
										<itemtemplate>
											<input type=checkbox name='cb' value='<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId").ToString())%><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "aBFirstNm").ToString())%>_<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "aBLastNm").ToString())%>;<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLNm").ToString())%>'
                                                isEditable=<%#AspxTools.HtmlAttribute(AllowToEdit(Container.DataItem).ToString())%> isAssignable=<%#AspxTools.HtmlAttribute(AllowToAssign(Container.DataItem).ToString())%> onclick="onSelectCheck(this);">
										</itemtemplate>
									</asp:TemplateColumn>
		                            <asp:TemplateColumn HeaderText="Loan Number" SortExpression="sLNm">
		                            <ItemTemplate>
                                        <a href="javascript:void(0);" onclick="f_toggleLoanOptions('<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "sLId").ToString().Replace("-","")) %>'); return false;" >
                                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLNm").ToString())%>
                                        </a>
                                        <div id='LoanOptions_<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "sLId").ToString().Replace("-","")) %>' style="padding:0.2em;font-size:0.8em">
                                        </div>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
									<asp:templatecolumn headertext="Last Name" SortExpression="aBLastNm">
										<itemtemplate>
											<b>
												<%# AspxTools.HtmlString(Eval("aBLastNm").ToString())%>
											</b>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn HeaderText="First Name" SortExpression="aBFirstNm">
										<itemtemplate>
											<%# AspxTools.HtmlString(Eval("aBFirstNm").ToString())%>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn HeaderText="Loan Officer" SortExpression="sEmployeeLoanRepName">
										<itemtemplate>
											<%# AspxTools.HtmlString(Eval("sEmployeeLoanRepName").ToString()) %>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templatecolumn HeaderText="Lender Account Exec" SortExpression="sEmployeeLenderAccExecName">
										<itemtemplate>
											<%# AspxTools.HtmlString(Eval("sEmployeeLenderAccExecName").ToString())%>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:BoundColumn DataField="LoanType" HeaderText="Loan Type" SortExpression="LoanType" />
									<asp:BoundColumn DataField="LoanStatus" HeaderText="Loan Status" SortExpression="LoanStatus" />
									<asp:BoundColumn DataField="sStatusD" SortExpression="sStatusD" HeaderText="Status Date" DataFormatString="{0:d}" />
									<asp:templatecolumn HeaderText="Property Address">
										<itemtemplate>
											<%# AspxTools.HtmlString(Eval("PropertyAddress").ToString())%>
										</itemtemplate>
									</asp:templatecolumn>
									<asp:templateColumn HeaderText="Branch" SortExpression="sBranchNm">
										<itemtemplate>
											<%# AspxTools.HtmlString(Eval("sBranchNm").ToString())%>
										</itemtemplate>
									</asp:templateColumn>
								</columns>
							</ml:CommonDataGrid>
						</div>
						<div style="BORDER-RIGHT: lightgrey 2px solid; BORDER-TOP: lightgrey 2px solid; MARGIN: 0px 6px 6px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 100%; BORDER-BOTTOM: lightgrey 2px solid; BACKGROUND-COLOR: gainsboro; TEXT-ALIGN: left">
							<div style="PADDING-RIGHT: 6px; PADDING-LEFT: 6px; FONT-WEIGHT: bold; PADDING-BOTTOM: 6px; PADDING-TOP: 6px; TEXT-ALIGN: center<% if (m_resultCountCap.Text != "") { %>; BACKGROUND-COLOR: #ffffcc<% } %>">
								<ml:PassthroughLabel id="m_resultCountCap" runat="server"></ml:PassthroughLabel>
							</div>
						</div>
					</td>
				</tr>
				<tr style="HEIGHT: 10%">
					<td>
						<uc1:Footer runat="server" id="Footer1" />
					</td>
				</tr>
			</table>

		</form>
	</body>
</HTML>

