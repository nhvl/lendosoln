using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Caching;
using LendersOffice.QueryProcessor;
using LendersOffice.Reports;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
namespace LendersOfficeApp.los.Reports
{
	public partial class TmpReportResult : BasePage
	{
        private string m_ExceptionDetails
        {
            set
            {
                if (value.EndsWith(".") == false)
                {
                    ClientScript.RegisterHiddenField("m_ErrorMessage", value + ".");
                }
                else
                {
                    ClientScript.RegisterHiddenField("m_ErrorMessage", value);
                }
            }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            Guid reportId = RequestHelper.GetGuid("ReportId");
            BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal; //User as BrokerUserPrincipal;

            try
            {   
                LoanReporting loanReporting = new LoanReporting();
                Report report = loanReporting.Publish(reportId, brokerUser.BrokerId, brokerUser);

                if (null != report)
                {
                    report.Id = Guid.NewGuid();

                    // 05/12/06 mf - OPM 2582 & 4668.  To reduce server memory usage
                    // do not use server cache for custom reports.

                    AutoExpiredTextCache.AddToCacheByUser(brokerUser, (string)report, TimeSpan.FromMinutes(30), report.Id);
                    //                Cache.Insert(report.Id.ToString(), report, null, DateTime.Now.AddMinutes(5), Cache.NoSlidingExpiration, CacheItemPriority.NotRemovable, null);
                    Response.Redirect("ReportResult.aspx?id=" + report.Id);
                    return;
                }

                throw new CBaseException(ErrorMessages.UnableToRetrievePublishedReport, ErrorMessages.UnableToRetrievePublishedReport + " ReportId=" + reportId);
            }
            catch (PermissionException exc)
            {
                m_ExceptionDetails = "Permission Denied - Unable to generate report.\nTo generate this report, you need to remove all restricted fields.";
                Tools.LogError(exc);
            }
            

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
