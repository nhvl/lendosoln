﻿// <copyright file="TableParser.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Michael Leinweaver
//    Date:   7/23/2015
// </summary>
namespace LendersOffice
{
    using System;
    using System.Collections;
    using System.Drawing;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using LendersOffice.AntiXss;
    using LendersOffice.QueryProcessor;
    using MeridianLink.CommonControls;

    /// <summary>
    /// The structure of a report is defined as a tree when a report has more than one table:<para></para>
    ///                      Dummy outer table <para></para>
    ///                      /       |       \ <para></para>
    ///                   Table 1  Table 2    ... <para></para>
    ///                  /   |   \ <para></para>
    ///             Row 1  Row 2  ... <para></para>
    /// What we need to do is grab the tables nested in the dummy table and then add each of those
    /// tables, row by row, to the report.
    /// </summary>
    public static class TableParser
    {
        private const int TableCellPadding = 4;
        private const int TableCellSpacing = 0;
        private const int ColumnCellPadding = 0;
        private const int ColumnCellSpacing = 4;

        private static readonly Unit Width = Unit.Percentage(100.0);
        private static readonly Unit BorderWidth = Unit.Parse("0");

        private const string HeaderFont = "bold 8pt arial";
        private const string ColumnTitleBorder = "2px solid #FFF";
        private const string PrintButtonHTML = "<input type=\"button\" style=\"font: 9pt arial; width: 80px; border-width: 1px;\" value=\"Print\" onclick=\"onPrintTable( getMyGroup( this , 'group' ) )\">";

        private static readonly Color TableHeaderColor = Color.Lavender;
        private static readonly Color TableBackgroundColor = Color.WhiteSmoke;
        private static readonly Color TableBorderColor = Color.MidnightBlue;
        private static readonly Color TableFooterColor = Color.LightSteelBlue;
        private static readonly Color SubtotalTableHeaderColor = Color.PaleGoldenrod;
        private static readonly Color SubtotalTableFooterColor = Color.Tan;

        public static void Parse(PlaceHolder m_RenderedTable, ArrayList tables, Columns columns)
        {
            // If the report is empty, there are no tables to parse. 
            if (tables.Count < 1)
            {
                return;
            }

            Rows completePage = tables[0] as Rows;

            // If the containing table has no entries, there's
            // nothing to parse.
            if (completePage.Count == 0)
            {
                return;
            }

            // If completePage's children are of type Row (not of type Rows), then completePage
            // is the actual report table and not a dummy table. This situation occurs if the report
            // is comprised of only a single table.
            if (completePage[0] != null && completePage[0].GetType() == typeof(QueryProcessor.Row))
            {
                AddControl(m_RenderedTable, completePage, columns);
            }
            else
            {
                AddTable(m_RenderedTable, completePage, columns);
            }

            m_RenderedTable.DataBind();
        }

        // There are cases when rendering report tables where the nesting of the tables is deeper
        // than the three level diagram drawn above, which we can test by determining if the items
        // in the current table are Rows themselves. In those cases, we need to continue drilling
        // down in order to correctly render all of the tables in the report.
        private static void AddTable(PlaceHolder m_RenderedTable, Rows currentTable, Columns columns)
        {
            foreach (var entry in currentTable)
            {
                if (entry is Row)
                {
                    AddControl(m_RenderedTable, currentTable, columns);

                    // If currentTable does not contain any nested table children, we 
                    // add currentTable to the report and then return to prevent infinite
                    // recursion.
                    return;
                }
                else
                {
                    AddTable(m_RenderedTable, entry as Rows, columns);
                }
            }

            // If specified in the report, we add another table containing calculated subtotal results.
            if (currentTable.GroupBy.Count > 0 && currentTable.Results.Count > 0)
            {
                AddSubtotalResultsToTableGroup(currentTable, m_RenderedTable, columns);
            }
        }

        private static void AddControl(PlaceHolder m_RenderedTable, Rows currentTable, Columns reportColumns)
        {
            // Each table must be within its own div so it can be printed individually.
            Panel groupDiv = MakeDiv();

            Table tableControl = MakeTable();

            MakeHeader(currentTable.GroupBy, tableControl, reportColumns, TableHeaderColor);

            foreach (var row in currentTable)
            {
                tableControl.Rows.Add(MakeRow(row as Row, reportColumns));
            }

            if (currentTable.Results.Count > 0)
            {
                AddSubtotalResultsToCurrentTable(currentTable.Results, tableControl, reportColumns);
            }

            MakeFooter(tableControl, reportColumns.Count, currentTable.Count);

            groupDiv.Controls.Add(tableControl);

            m_RenderedTable.Controls.Add(groupDiv);

            // Add a line break between each table.
            m_RenderedTable.Controls.Add(new PassthroughLiteral() { Text = "<br />" });
        }

        private static Panel MakeDiv()
        {
            Panel newPanel = new Panel();

            newPanel.ID = "group";

            newPanel.Width = Width;

            newPanel.Style.Add("page-break-inside", "avoid");

            return newPanel;
        }

        private static Table MakeTable()
        {
            Table returnTable = new Table();

            returnTable.CellPadding = TableCellPadding;

            returnTable.CellSpacing = TableCellSpacing;

            returnTable.BackColor = TableBackgroundColor;

            returnTable.BorderColor = TableBorderColor;

            returnTable.BorderWidth = BorderWidth;

            returnTable.BorderStyle = BorderStyle.Solid;

            returnTable.Width = Width;

            return returnTable;
        }

        private static void MakeHeader(LookupGroup groupingList, Table tableControl, Columns columns, Color backgroundColor)
        {
            MakeTableLegend(tableControl, columns.Count, groupingList.Count, backgroundColor);

            MakeGroupingRow(groupingList, tableControl, columns.Count, backgroundColor);

            MakeColumnTitlesRow(tableControl, columns, backgroundColor);
        }

        private static void MakeTableLegend(Table tableControl, int colCount, int groupingListCount, Color backgroundColor)
        {
            TableHeaderRow returnHeaderRow = new TableHeaderRow();

            TableHeaderCell resultsCell = new TableHeaderCell();

            resultsCell.ColumnSpan = colCount;

            string resultsText = "Report Results";

            resultsCell.Text = groupingListCount > 0 ? String.Format("{0}, {1}", resultsText, "grouped by the following fields:") : resultsText;

            returnHeaderRow.Cells.Add(resultsCell);

            returnHeaderRow.Style.Add("font", HeaderFont);

            returnHeaderRow.HorizontalAlign = HorizontalAlign.Left;

            returnHeaderRow.BackColor = backgroundColor;

            tableControl.Rows.Add(returnHeaderRow);
        }

        private static void MakeGroupingRow(LookupGroup groupingList, Table tableControl, int colCount, Color backgroundColor)
        {
            int groupingCounter = 1;

            foreach (LookupLabel group in groupingList)
            {
                TableHeaderRow groupByRow = new TableHeaderRow();

                TableHeaderCell groupByCell = new TableHeaderCell();

                groupByCell.Text = String.Format("({0}) {1}: {2}", groupingCounter, group.Label, (group.Value != null ? group.Value : ""));

                groupByCell.ColumnSpan = colCount;

                groupByRow.Cells.Add(groupByCell);

                groupByRow.Style.Add("font", HeaderFont);

                groupByRow.BackColor = backgroundColor;

                groupByRow.HorizontalAlign = HorizontalAlign.Left;

                tableControl.Rows.Add(groupByRow);

                ++groupingCounter;
            }
        }

        private static void MakeColumnTitlesRow(Table tableControl, Columns columns, Color backgroundColor)
        {
            TableHeaderRow returnRow = new TableHeaderRow();

            foreach (Column c in columns)
            {
                TableHeaderCell newHeader = new TableHeaderCell();

                newHeader.Text = c.Name;

                var colKind = c.Kind;

                HorizontalAlign alignment;

                if (colKind == Field.E_ClassType.Pct || colKind == Field.E_ClassType.Csh || colKind == Field.E_ClassType.Cnt)
                {
                    alignment = HorizontalAlign.Right;
                }
                else
                {
                    alignment = HorizontalAlign.Left;
                }

                newHeader.HorizontalAlign = alignment;

                newHeader.Style.Add("border-top", ColumnTitleBorder);

                newHeader.Style.Add("border-left", ColumnTitleBorder);

                newHeader.Style.Add("border-right", ColumnTitleBorder);

                returnRow.Cells.Add(newHeader);
            }

            returnRow.Style.Add("font", HeaderFont);

            returnRow.BackColor = backgroundColor;

            tableControl.Rows.Add(returnRow);
        }

        private static TableRow MakeRow(Row row, Columns columns)
        {
            TableRow returnRow = new TableRow();

            for (int i = 0; i < columns.Count; ++i)
            {
                TableCell newCell = new TableCell();

                newCell.Text = AspxTools.HtmlString(GetText(row[i].Data as string));

                var colKind = columns[i].Kind;

                if (colKind == Field.E_ClassType.Pct || colKind == Field.E_ClassType.Csh || colKind == Field.E_ClassType.Cnt)
                {
                    newCell.CssClass = "g";
                }

                returnRow.Cells.Add(newCell);
            }

            return returnRow;
        }

        private static string GetText(string field)
        {
            string returnText;

            if (field != null && field.Length > 0)
            {
                returnText = field;
            }
            else if (field != null)
            {
                returnText = ".";
            }
            else
            {
                returnText = "null";
            }

            return returnText;
        }

        private static void AddSubtotalResultsToCurrentTable(CalculationResults results, Table tableControl, Columns columns)
        {
            TableRow subtotalRow = new TableRow();

            subtotalRow.VerticalAlign = VerticalAlign.Top;

            int i = 0;

            foreach (Column c in columns)
            {
                subtotalRow.Cells.Add(MakeColumnCell(results, c, i));

                ++i;
            }

            tableControl.Rows.Add(subtotalRow);
        }

        private static TableCell MakeColumnCell(CalculationResults results, Column c, int i)
        {
            TableCell returnCell = new TableCell();

            returnCell.HorizontalAlign = HorizontalAlign.Right;

            // Each cell in the subtotal rows will be comprised of a nested table, which will be formatted
            // in such a way that the labels for the cells will be left-aligned and the values for the
            // cells will be right-aligned.
            Table columnCellTable = new Table();

            columnCellTable.Width = Width;

            columnCellTable.CellPadding = ColumnCellPadding;

            columnCellTable.CellSpacing = ColumnCellSpacing;

            foreach (CalculationResult result in results)
            {
                if (result.Column != i)
                {
                    continue;
                }

                AddSubtotalCell(result, c, columnCellTable, i);
            }

            returnCell.Controls.Add(columnCellTable);

            return returnCell;
        }

        private static void AddSubtotalCell(CalculationResult result, Column c, Table columnCellTable, int i)
        {
            string subtotalType = GetSubtotalType(result);

            string format = GetSubtotalFormat(c);

            if (result.What == E_FunctionType.Av && c.Kind == Field.E_ClassType.Cnt)
            {
                format = "N3";
            }

            TableRow calculationRow = new TableRow();

            TableCell labelCell = new TableCell();

            labelCell.HorizontalAlign = HorizontalAlign.Left;

            labelCell.Style.Add("font-weight", "bold");

            labelCell.Text = String.Format("{0}:", subtotalType);

            calculationRow.Cells.Add(labelCell);

            TableCell resultCell = new TableCell();

            resultCell.Style.Add("text-decoration", "underline");

            resultCell.HorizontalAlign = HorizontalAlign.Right;

            resultCell.Text = result.GetFormattedResult(format);

            calculationRow.Cells.Add(resultCell);

            columnCellTable.Rows.Add(calculationRow);
        }

        private static string GetSubtotalType(CalculationResult result)
        {
            switch (result.What)
            {
                case E_FunctionType.Av:
                    return "Avg";

                case E_FunctionType.Sm:
                    return "Tot";

                case E_FunctionType.Mn:
                    return "Min";

                case E_FunctionType.Mx:
                    return "Max";

                case E_FunctionType.Ct:
                    return "Num";

                default:
                    return "Unk";
            }
        }

        private static string GetSubtotalFormat(Column c)
        {
            switch (c.Kind)
            {
                case Field.E_ClassType.Pct:
                    return "N3";

                case Field.E_ClassType.Csh:
                    return "C2";

                case Field.E_ClassType.Cnt:
                    return "N0";

                default:
                    return "N";
            }
        }

        private static void MakeFooter(Table tableControl, int colCount)
        {
            TableRow footerRow = new TableRow();

            footerRow.BackColor = SubtotalTableFooterColor;

            footerRow.CssClass = "m_Ui";

            TableCell printButtonCell = new TableCell();

            printButtonCell.Text = PrintButtonHTML;

            printButtonCell.HorizontalAlign = HorizontalAlign.Left;

            printButtonCell.ColumnSpan = colCount;

            footerRow.Cells.Add(printButtonCell);

            tableControl.Rows.Add(footerRow);
        }

        private static void MakeFooter(Table tableControl, int colCount, int recordCount)
        {
            TableRow footerRow = new TableRow();

            footerRow.BackColor = TableFooterColor;

            footerRow.CssClass = "m_Ui";

            TableCell footerCell = new TableCell();

            footerCell.ColumnSpan = colCount;

            MakeFooterLabels(footerCell, recordCount);

            footerRow.Cells.Add(footerCell);

            tableControl.Rows.Add(footerRow);
        }

        private static void MakeFooterLabels(TableCell footerCell, int recordCount)
        {
            Table innerFooterTable = new Table();

            innerFooterTable.Width = Width;

            TableRow innerTableRow = new TableRow();

            TableCell printButtonCell = new TableCell();

            printButtonCell.Text = PrintButtonHTML;

            printButtonCell.HorizontalAlign = HorizontalAlign.Left;

            innerTableRow.Cells.Add(printButtonCell);

            TableCell footerRecordCountCell = new TableCell();

            footerRecordCountCell.Text = String.Format("Record count = {0}", recordCount);

            footerRecordCountCell.HorizontalAlign = HorizontalAlign.Right;

            footerRecordCountCell.Style.Add("font-weight", "bold");

            innerTableRow.Cells.Add(footerRecordCountCell);

            innerFooterTable.Rows.Add(innerTableRow);

            footerCell.Controls.Add(innerFooterTable);
        }

        private static void AddSubtotalResultsToTableGroup(Rows currentTable, PlaceHolder m_RenderedTable, Columns reportColumns)
        {
            Panel groupDiv = MakeDiv();

            Table subtotalGroupTable = MakeTable();

            MakeHeader(currentTable.GroupBy, subtotalGroupTable, reportColumns, SubtotalTableHeaderColor);

            AddSubtotalResultsToCurrentTable(currentTable.Results, subtotalGroupTable, reportColumns);

            MakeFooter(subtotalGroupTable, reportColumns.Count);

            groupDiv.Controls.Add(subtotalGroupTable);

            m_RenderedTable.Controls.Add(groupDiv);

            // Add a line break between each table.
            m_RenderedTable.Controls.Add(new PassthroughLiteral() { Text = "<br />" });
        }
    }
}