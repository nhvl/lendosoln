namespace LendersOffice
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Conversions;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.NmlsCallReport;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LendersOfficeApp.LOAdmin.TaskBackendUtilities;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Summary description for ReportExport.
    /// </summary>
    public partial class ReportExport : BasePage
	{
        /// <summary>
        /// Gets the user running the export.
        /// </summary>
        private AbstractUserPrincipal CurrentPrincipal => this.User as AbstractUserPrincipal;

        protected bool m_reload = false;
        
		/// <summary>
        /// Load cached report and display as text.
        /// </summary>
        /// 

        private Report GetReportFromIDUsingFileHandle(string id)
        {
            Report temp = null;

            LocalFilePath filePath = LocalFilePath.Invalid;
            string resultFromDB = null;
            if (false == AutoExpiredTextCache.TryGetStringOrFileHandlerFromCacheByUser(PrincipalFactory.CurrentPrincipal, id, out resultFromDB, out filePath))
            {
                return temp;
            }

            if (resultFromDB != null)
            {
                temp = (Report)resultFromDB;
            }
            else if (filePath != LocalFilePath.Invalid)
            {
                temp = Report.GetReport(filePath);
                FileOperationHelper.Delete(filePath.Value); // Cleanup temp file.
            }

            return temp;
        }
        private Report GetReportFromIDUsingStrings(string id)
        {
            Report temp = null;
            string result;
            if ((result = AutoExpiredTextCache.GetFromCacheByUser(PrincipalFactory.CurrentPrincipal, id)) != null)
            {
                temp = (Report)result;
            }
            return temp;
        }

		protected void PageLoad( object sender , System.EventArgs a )
		{
            Page.ClientScript.GetPostBackEventReference(this, ""); // This will add __doPostBack

            // We export on the first hit -- skip refreshes.  We want
            // to load the report and convert into a manageable text
            // format.  Once converted, we send it back as an attachment.

            try
            {
                // We fetch the report from the cache and render it
                // according to the given type.  If the report is
                // no longer cached, then we punt and say sorry.

                string id = RequestHelper.GetSafeQueryString( "id" ) , type =  RequestHelper.GetSafeQueryString( "type" );
                string cb = "";

                if( id != null && type != null )
                {
                    // Retrieve the report using the implicit string
                    // conversion of the contained, cached object.

                    Report rA = null;

					// 05/12/06 mf - OPM 2582 & 4668.  To reduce server memory usage
					// do not use server cache for custom reports.

                    try
                    {
                        rA = GetReportFromIDUsingFileHandle(id);
                        if (rA == null)
                        {
                            rA = GetReportFromIDUsingStrings(id);
                        }
                    }
                    catch (Exception e)
                    {
                        Tools.LogErrorWithCriticalTracking(e);
                        rA = GetReportFromIDUsingStrings(id);
                    }


//                    if( Cache.Get( id ) != null )
//                    {
//                        rA = Cache.Get( id ) as Report;
//                    }
         
                    if( rA == null )
                    {
                        // Mark the error in the message container and
                        // skip attachment loading.

                        m_ExportError.Value = ErrorMessages.UnableToLoadReport;

                        Tools.LogError( "Report Export Error: Couldn't load ( " + id + " / " + type + " ) from the cache." );

                        return;
                    }

                    // We presume to have a valid report.  Now translate it
                    // according to the type.  If type is invalid, punt.
                    switch( type )
                    {
                        case "txt":
                        {
                            // Set output as tsv file attachment.
                            string outputFile = ReportExporter.ConvertToTxtAsFile(this.CurrentPrincipal, rA);

                            RequestHelper.SendFileToClient(Context, outputFile, "text/plain", "Report.txt");
                            Response.End();
                            return;
						}

                        case "csv":
                        {
                            // Set output as csv file attachment.
                            string outputFile = ReportExporter.ConvertToCsvAsFile(this.CurrentPrincipal, rA);

                            RequestHelper.SendFileToClient(Context, outputFile, "text/comma-separated-values", "Report.csv");
                            Response.End();
                            return;
						}

                        case "mcr_xml":
                        cb = RetrieveCallReportXml(rA);
                        Response.ContentType = "text/xml";
                        Response.AddHeader("Content-Disposition", "attachment; filename=\"MCR.xml\"");
                        break;

                        case "mcr_html":
                        cb = RetrieveCallReportHtml(rA);
                        break;

                        case "uldd":
                            string ulddErrorMessage = string.Empty;
                            cb = RetrieveUlddReportXml(rA, out ulddErrorMessage);

                            if (string.IsNullOrEmpty(ulddErrorMessage) == false)
                            {
                                // TODO: Display Error Message
                                Response.ContentType = "text/html";
                                cb = ulddErrorMessage;
                            }
                            else
                            {
                                Response.ContentType = "text/xml";
                                Response.AddHeader("Content-Disposition", "attachment; filename=\"ULDD.xml\"");
                            }
                            break;

                        default:
                        {
                            // Mark the error in the message container and
                            // skip attachment loading.

                            m_ExportError.Value = "Unrecognized report export type.  Please try re-running your query.";

                            Tools.LogError( "Report Export Error: Didn't recognize export type for ( " + id + " / " + type + " )." );

                            return;
                        }
                    }
                }
                else
                {
                    // Identifier of cached report not specified, so tell
                    // the logger and user something is wrong.

                    m_ExportError.Value = "Unable to load the report for export.  Try re-running your query.";

                    Tools.LogError( "Report Export Error: Cached report id and type not specified." );

                    return;
                }

                // Write out the generated text file to the renderer in
                // our response container.

                Response.Output.Write( cb );
            }
            catch( Exception e )
            {
                // Log the error and display failure to user.  We return
                // early so that the page can be rendered normally, without
                // terminating the response prematurely, as we would do if
                // we had a complete document to attach.

                m_ExportError.Value = e.Message;

                Tools.LogError( e );

                return;
            }

            // Always end the response outside of a try-catch
            // block so that the .net layer can catch the end
            // of thread signal.

            Response.Flush();
            Response.End();
		}
        private static Dictionary<string, FieldEnumerator.LoanFieldInfo> FieldsByName = FieldEnumerator.ListLoanFieldsByName();
        private string RetrieveUlddReportXml(Report rA, out string ulddErrorMessage)
        {
            ulddErrorMessage = string.Empty;

            List<Row> rowList = rA.Flatten();

            List<Guid> loanIdList = new List<Guid>();

            foreach (Row row in rowList)
            {
                if (row.Key != Guid.Empty)
                {
                    loanIdList.Add(row.Key);
                }
            }
            if (loanIdList.Count == 0)
            {
                ulddErrorMessage = "<ol><li>There are no loans in this report.</ol></li>";
                return null;
            }
            UlddExporter exporter = new UlddExporter(loanIdList, null);

            List<UlddExportError> errorList = null;
            if (exporter.Verify(out errorList) == false)
            {
                // Contains error.
                StringBuilder htmlErrorMessage = new StringBuilder();
                // ideally the window opening code should be in a common js file
                // and only the parts that affect the UI should be page specific.
                // if they close the pipeline page, they may still want to be able to use this page and the loans. 
                
                string script = @"gOpenedWindows = {};
    function f_openLoanToField(loanid,frameUrl, winUrlOption, name, winParam)
    {
        var wo = window.opener; // the report
        if(null != wo && !wo.closed)
        {
            var woo = wo.opener;    // custom reports
            if(null != woo && !woo.closed)
            {
                var w = woo.opener; // pipeline
                if(w != null && !w.closed)
                {
                    var cw = w.parent.document.getElementsByName('frmCode')[0].contentWindow;
                    if(cw.f_isLoanWindowOpen(loanid))
                    {
                        cw.f_getWindow(loanid).changeBodyURLTo(frameUrl);
                        return;
                    } 
                    else{
                        w.editLoanOption(loanid, name, winUrlOption, winParam);
                        return;
                    }               
                }
            }            
        }
        if(gOpenedWindows[name] != null)
        {
            if(gOpenedWindows[name].closed)
            {
                gOpenedWindows[name] = window.open('" + Tools.VRoot + @"/newlos/LoanApp.aspx?loanid='+loanid+'&body_url='+winUrlOption + winParam, name);
            }else{
                gOpenedWindows[name].changeBodyURLTo(frameUrl);
            }
        }
        else{
            gOpenedWindows[name] = window.open('" + Tools.VRoot + @"/newlos/LoanApp.aspx?loanid='+loanid+'&body_url='+winUrlOption + winParam, name);
        }        
    }";
                htmlErrorMessage.AppendLine("<script>");
                htmlErrorMessage.AppendLine(script);
                htmlErrorMessage.AppendLine("</script>");
                //string csvExportURL = "ULDDErrorsCSV.aspx?reportid="+rA.Id; // add back when importer ready
                //string csvImportURL;
                htmlErrorMessage.AppendLine("<div>");
                //htmlErrorMessage.AppendLine("<input type=\"button\" onclick=\"window.open('" + csvExportURL + "')\" value=\"Export to CSV\">"); // add back when ready.
                //htmlErrorMessage.AppendLine("<input type=\"button\" onclick=\"window.open('"+csvImportURL+"')\" value=\"Import from CSV\">");
                htmlErrorMessage.AppendLine("</div>");
                htmlErrorMessage.AppendLine("<ol>");
                foreach (UlddExportError exportError in errorList)
                {
                    htmlErrorMessage.AppendLine("<li>Loan Number: " + exportError.sLNm);
                    htmlErrorMessage.AppendLine("<ul>");
                    foreach (var exportErrorItems in exportError.ErrorList.GroupBy((a)=>a.PageLocation))
                    {
                        htmlErrorMessage.AppendLine("<li>Page: " + exportErrorItems.First().PageLocation);
                        htmlErrorMessage.AppendLine("<ul>");
                        foreach (var exportErrorItem in exportErrorItems)
                        {
                            htmlErrorMessage.AppendLine("<li>" + "<input type='checkbox'/>"+
                                ((FieldsByName.ContainsKey(exportErrorItem.FieldId) && FieldsByName[exportErrorItem.FieldId].Pages[0].Url != "Everything_Else") ?
                                string.Format("<a href='#' onclick=\"f_openLoanToField('{0}', '{1}', '{2}', {3}, '{4}')\">Fix:  </a>",
                                    exportError.sLId, 
                                    UrlForFieldFrame(exportErrorItem, exportError),
                                    (int) Enum.Parse(typeof(E_UrlOption), "Page_" + FieldsByName[exportErrorItem.FieldId].Pages[0].Id),//UrlForFieldWindow(exportErrorItem, exportError),
                                    AntiXss.AspxTools.JsString(exportError.sLNm.Replace(" ", "_")),
                                    ParamForFieldWindow(exportErrorItem, exportError)
                                    )
                                    : "")
                                 + exportErrorItem.ErrorMessage +"."
                                 + "</li>");
                        }
                        htmlErrorMessage.AppendLine("</ul>");
                        htmlErrorMessage.AppendLine("</li>");
                        
                    }
                    htmlErrorMessage.AppendLine("</ul>");
                    htmlErrorMessage.AppendLine("</li>");
                }
                htmlErrorMessage.AppendLine("</ol>");
                ulddErrorMessage = htmlErrorMessage.ToString();
                return null; // 8/29/2012 dd - Don't allow to export if there is error.
            }
            return exporter.Export();
        }
        private string UrlForFieldFrame(UlddExportErrorItem exportErrorItem, UlddExportError exportError)
        {
            var url = FieldsByName[exportErrorItem.FieldId].Pages[0].Url.Replace("~", Tools.VRoot);
            url += (url.Contains("?") ? "&" : "?");
            url += "loanid=" + exportError.sLId
                    +"&highlightId=" + exportErrorItem.FieldId 
                    + "&appid=" + exportError.appid;
            return url;
                                 
        }
        private string UrlForFieldWindow(UlddExportErrorItem exportErrorItem, UlddExportError exportError)
        {
            return FieldsByName[exportErrorItem.FieldId].Pages[0].Url.Replace("~", Tools.VRoot).Replace("?", "&")
                                 + "&highlightId=" + exportErrorItem.FieldId
                                 + "&appid=" + exportError.appid
                                 + "&highURL=" + (int)Enum.Parse(typeof(E_UrlOption), "Page_" + FieldsByName[exportErrorItem.FieldId].Pages[0].Id);

        }
        private string ParamForFieldWindow(UlddExportErrorItem exportErrorItem, UlddExportError exportError)
        {
            return "&highlightId=" + exportErrorItem.FieldId
                                 + "&appid=" + exportError.appid
                                 + "&highURL=" + (int)Enum.Parse(typeof(E_UrlOption), "Page_" + FieldsByName[exportErrorItem.FieldId].Pages[0].Id);

        }
        private McrCallReportXml CreateCallReport(Report report)
        {
            McrCallReportXml callReport = new McrCallReportXml();
            callReport.McrFormatT = (E_McrFormatT) RequestHelper.GetInt("reportType");
            callReport.McrPeriodT = (E_McrPeriodT) RequestHelper.GetInt("period");
            callReport.Year = RequestHelper.GetInt("year");
            callReport.McrVersionT = (E_McrVersionT)RequestHelper.GetInt("reportVersion");

            
            var list = McrRmlaXml.Generate(report, callReport.McrPeriodT, callReport.Year, callReport.McrVersionT, callReport.McrFormatT, PrincipalFactory.CurrentPrincipal.BrokerId);

            foreach (var rmla in list)
            {
                callReport.RmlaList.Add(rmla);
            }


            return callReport;
        }
        private string RetrieveCallReportXml(Report report)
        {
            McrCallReportXml callReport = CreateCallReport(report);

            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.ASCII;

            XmlWriter writer = XmlWriter.Create(sb, settings);
            writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
            callReport.WriteXml(writer);
            writer.Flush();

            return sb.ToString();
        }
        private string RetrieveCallReportHtml(Report report)
        {
            McrCallReportXml callReport = CreateCallReport(report);
            return callReport.ToHtml();
        }
        private void AddError(HtmlTable table, string error)
        {
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableCell cell = new HtmlTableCell();
            cell.InnerText = "- " + error;
            row.Cells.Add(cell);
            table.Rows.Add(row);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}
}
