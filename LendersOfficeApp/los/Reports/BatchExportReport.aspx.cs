﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CommonLib;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.QueryProcessor;
using LendersOffice.Reports;
using LendersOffice.Security;
using LendersOffice.XsltExportReport;
using LendersOffice.Constants;

namespace LendersOffice
{
    public partial class BatchExportReport : BaseServicePage
    {
        private Guid m_reportId
        {
            get
            {
                Guid reportid = new Guid(m_reportIdField.Value);
                if (reportid == Guid.Empty)
                {
                    //Try getting Report Type from Request (if m_reportIdField is not loaded yet)
                    XsltMapItem item = XsltMap.Get(Request["Type"]);
                    reportid = DefaultReportId(item, Broker.BrokerID);
                }
                return reportid;
            }
            set
            {
                m_reportIdField.Value = value.ToString();
            }
        }

        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }
        private BrokerDB Broker
        {
            get { return BrokerDB.RetrieveById(BrokerUser.BrokerId); }
        }
        private String ErrorMessage
        {
            set { Page.ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + "."); }
        }
        public static Guid DefaultReportId(XsltMapItem item, Guid BrokerId)
        {

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@MapId", item.Name)
                                        };
            Guid reportId = Guid.Empty;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "GetQueryForBatchExport", parameters))
            {
                if (reader.Read())
                {
                    reportId = (Guid)reader["QueryId"];
                }
            }
            return reportId;
        }

        protected int m_itemCount = 0;

        protected bool m_isAllowTableSort = true;


        private void LoadData()
        {
            // Load the current report.  We want latest every time the
            // user posts back.
            //
            // 4/1/2005 kb - Implemented as part of query driven pipeline
            // task: case 932.
            if (m_reportId == Guid.Empty)
            {
                // NO-OP if no report id is select
                return;
            }
            try
            {
                XsltMapItem item = XsltMap.Get(Request["Type"]);
                RegisterJsGlobalVariables("type", Request["Type"]);
                m_customFieldName.Text = item.CustomFieldDescription;
                m_customFieldControls.Visible = !string.IsNullOrEmpty(item.CustomFieldDescription);
                

                // Get the currently selected report and render it in our
                // pipeline.

                E_ReportExtentScopeT scope = E_ReportExtentScopeT.Assign;
                if (BrokerUser.HasPermission(Permission.BrokerLevelAccess))
                {
                    scope = E_ReportExtentScopeT.Broker;
                }
                else if (BrokerUser.HasPermission(Permission.BranchLevelAccess))
                {
                    scope = E_ReportExtentScopeT.Branch;
                }
                else if (BrokerUser.HasPermission(Permission.TeamLevelAccess))
                {
                    scope = E_ReportExtentScopeT.Team;
                }                

                // 2/7/2006 dd - OPM 4036 - Cap the pipeline.
                //m_lR.MaxRowCount = LendersOffice.Constants.ConstAppDavid.MaxRecordsInPipeline;

                LoanReporting loanReporting = new LoanReporting();
                Report report = loanReporting.Pipeline(m_reportId, BrokerUser, m_sortOrder.Value, scope);

                if (report == null)
                {
                    throw new InvalidOperationException("Batch Export result is null.");
                }
                List<string> columnList = new List<string>();
                List<string> sortTypeList = new List<string>();
                foreach (Column column in report.Columns)
                {
                    columnList.Add(column.Name);
                    switch (column.Kind)
                    {
                        case Field.E_ClassType.Txt:
                            sortTypeList.Add("text");
                            break;
                        case Field.E_ClassType.Pct:
                            sortTypeList.Add("percent");
                            break;
                        case Field.E_ClassType.Cnt:
                            sortTypeList.Add("digit");

                            break;
                        case Field.E_ClassType.Csh:
                            sortTypeList.Add("currency");
                            break;
                        case Field.E_ClassType.Cal:
                            sortTypeList.Add("shortDate");
                            break;
                        case Field.E_ClassType.Invalid:
                        case Field.E_ClassType.LongText:
                            sortTypeList.Add("false");
                            break;
                        default:
                            throw new UnhandledEnumException(column.Kind);
                    }
                }

                m_isAllowTableSort = columnList.Count < 20; // 2/11/2014 dd - I noticed if there is high amount of column the TableSorter plug in is really slow.
                //    Therefore to avoid slow down, I am turning off column sort on client if there are too many columns.
                //    20 is just a random guess.
                m_columnRepeater.DataSource = columnList;
                m_columnRepeater.DataBind();

                List<Row> rows = report.Flatten();
                m_rowRepeater.DataSource = rows;
                m_rowRepeater.DataBind();

                m_itemCount = rows.Count;
                m_numLoans.Text = rows.Count.ToString();

                m_reportName.Text = report.Label.Title;
                m_reportPanel.Visible = true;
                RegisterJsObject("g_sortTypeList", sortTypeList);
            }
            catch (PermissionException e)
            {
                Tools.LogError("User tried to run Batch Export that contains restricted fields. Error Message: " + e.UserMessage, e);
                m_reportName.Text = "No Report Selected";
                m_rowsPanel.Visible = false;
            }
            catch (CBaseException e)
            {
                // Oops!
                if (e.UserMessage == "Report ID Undefined")
                {
                    //No error if lacking existing report ID
                }
                else if (e.UserMessage != "")
                {
                    Tools.LogError(ErrorMessage = "Failed to render web form: " + e.UserMessage, e);
                }
                else
                {
                    Tools.LogError(ErrorMessage = "Failed to render web form", e);
                }
                m_reportName.Text = "No Report Selected";
                m_rowsPanel.Visible = false;
            }

        }
        private void PagePreRender(object sender, System.EventArgs a)
        {

        }

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.EnableJqueryMigrate = false;
            this.m_refreshBtn.Click += new EventHandler(m_refreshBtn_Click);
            this.Load += new System.EventHandler(this.PageLoad);
            this.PreRender += new System.EventHandler(this.PagePreRender);
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);

            RegisterService("batchExport", "/los/Reports/BatchExportReportService.aspx");
        }

        void m_refreshBtn_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void PageInit(object sender, EventArgs e)
        {
            m_rowRepeater.ItemDataBound += new RepeaterItemEventHandler(m_rowRepeater_ItemDataBound);
            this.RegisterJsScript("LQBPopup.js");
            RegisterJsScript("jquery.tablesorter.dev.js");
        }

        private void m_rowRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Row row = e.Item.DataItem as Row;

            Repeater repeater = e.Item.FindControl("m_rowCellRepeater") as Repeater;

            repeater.DataSource = row.Items;
            repeater.DataBind();
        }
        private void PageLoad(object sender, EventArgs e)
        {
            XsltMapItem item = XsltMap.Get(Request["Type"]);

            if (!IsPostBack)
            {
                m_reportId = DefaultReportId(item, Broker.BrokerID);
                m_exportBtn.InnerText = "Export to " + item.Description;
                m_exportCompleteEmail.Text = EmployeeDB.RetrieveById(BrokerUser.BrokerId, BrokerUser.EmployeeId).Email;

                LoadData();
            }
            else if (m_reportId != Guid.Empty)
            {
                //Save report selection to database

                SqlParameter[] parameters = {
                                                new SqlParameter("@MapId", item.Name),
                                                new SqlParameter("@BrokerId", Broker.BrokerID),
                                                new SqlParameter("@QueryId", m_reportId)
                                            };
                StoredProcedureHelper.ExecuteNonQuery(Broker.BrokerID, "SaveQueryForBatchExport", 3, parameters);
            }
        }
    }
}