using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Drivers.FileSystem;
using LendersOffice.Drivers.MethodInvoke;
using LendersOffice.QueryProcessor;
using LendersOffice.Queries.MethodInvoke;
using LendersOffice.Reports;
using LendersOffice.Security;
using LqbGrammar;
using LqbGrammar.DataTypes;
using LqbGrammar.Drivers;
using MeridianLink.CommonControls;
using LendersOffice.AntiXss;

namespace LendersOffice
{
    public partial class LoanReports : BasePage
	{
        private LendersOffice.Reports.LoanReporting         m_Loans;

        private readonly string RESTRICTED_FIELDS_MSG = "Report Query contains restricted field(s).";
        private readonly string NO_ACCESS_MSG = "Permission is required to access.";
        private readonly string REMOVE_FIELDS_MSG = "To access, remove restricted field(s).";
        
        Hashtable _htSampleQueries;
        Hashtable _currentReports;        

        private BrokerUserPrincipal BrokerUser
		{
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private Guid m_BrokerId
        {
            get
            {
                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if( broker == null )
                {
                    return Guid.Empty;
                }

                return broker.BrokerId;
            }
        }

        private Guid m_PmlDojoId;

        private Guid GetPmlDojoId()
        {
            object id = Tools.GetPmlDojoBrokerId();
            if (id != null)
            {
                return new Guid(id.ToString());
            }

            return Guid.Empty;
        }

        private String m_EmployeeNm
        {
            get
            {
                // Lookup current principal.

                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if( broker == null )
                {
                    return "";
                }

                return broker.DisplayName;
            }
        }

        private Guid m_EmployeeId
        {
            get
            {
                // Lookup current principal.

                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if( broker == null )
                {
                    return Guid.Empty;
                }

                return broker.EmployeeId;
            }
        }

        private String m_ExceptionDetails
        {
            set
            {
                if( value.EndsWith( "." ) == false )
                {
                    Page.ClientScript.RegisterHiddenField( "m_ErrorMessage" , value + "." );
                }
                else
                {
                    Page.ClientScript.RegisterHiddenField( "m_ErrorMessage" , value );
                }
            }
        }

        private String m_FeedbackMessage
        {
            // Access member.

            set
            {
                if( value.EndsWith( "." ) == false )
                {
                    Page.ClientScript.RegisterHiddenField( "m_Feedback" , value + "." );
                }
                else
                {
                    Page.ClientScript.RegisterHiddenField( "m_Feedback" , value );
                }
            }
        }

        protected Boolean CanUserPublishReports
        {
            get { return BrokerUser.HasPermission(Permission.CanPublishCustomReports); }
        }

        /// <summary>
        /// Process request made through query grid's hyperlinks.
        /// </summary>

        protected void HandleGridOperation( object sender , System.Web.UI.WebControls.DataGridCommandEventArgs a )
        {
            // Process report specific request.

            Guid task = Guid.NewGuid(); // Tools.LogInfo( "Loan reporting: Processing grid request (" + task.ToString() + ")..." );
            string[] arguments = a.CommandArgument.ToString().Split(',');
            string sQueryId = arguments[0];
            
            try
            {
                // Lookup particular command and then execute it on
                // the given query.

                switch (a.CommandName)
                {
                    case "Edit":
                        {
                            // Simply redirect to the editor with this query.

                            Response.Redirect("LoanReportEditor.aspx?id=" + sQueryId + "&mode=edit" + "&rt=0", false);
                        }
                        break;

                    case "View":
                        {
                            // Simply redirect to the editor with this query.
                            
                            // If we're trying to view a sample report, we'll need to do a few things differently in the editor.
                            string extras = string.Empty;
                            if (arguments.Length > 1 && arguments[1] == "Demo")
                            {
                                extras = "&isSample=true";
                            }

                            Response.Redirect("LoanReportEditor.aspx?id=" + sQueryId + "&mode=view" + extras + "&rt=0", false);
                        }
                        break;

                    case "Demo":
                        {
                            // Simply redirect to the editor with this query.

                            Response.Redirect("LoanReportEditor.aspx?id=" + sQueryId + "&mode=demo" + "&rt=0", false);
                        }
                        break;

                    case "Unpublish":
                        {
                            SqlParameter[] parameters = {
                                                            new SqlParameter("@BrokerId", BrokerUser.BrokerId)
                                , new SqlParameter("@NamePublishedAs", "")
                                , new SqlParameter("@IsPublished", false)
                                , new SqlParameter("@QueryId", sQueryId)
                                                        };

                            StoredProcedureHelper.ExecuteNonQuery(BrokerUser.BrokerId, "UpdateReportQuery", 3, parameters);
                        }
                        break;

                    case "Run":
                        {
                            // We load the query and execute it right here.  Once run,
                            // we add the report to the cache and redirect to the view
                            // page.  We could open a new window.  for now, we'll just
                            // redirect.

                            Guid id = new Guid(sQueryId);
                            Guid brokerId = BrokerUser.BrokerId;
                            Guid datakey = Guid.NewGuid();

                            // If it's a sample report, then we need to pull from the DB that has the sample reports, not the DB that the user is on.
                            if (arguments.Length > 1 && arguments[1] == "Demo")
                            {
                                brokerId = m_PmlDojoId;
                            }

                            if (id == Guid.Empty)
                            {
                                break;
                            }

                            var queryIdenfifier = ReportQueryIdentifier.Create(id.ToString());
                            var brokerIdentifier = BrokerIdentifier.Create(brokerId.ToString());
                            var fileIdentifier = FileIdentifier.TryParse(datakey.ToString("N"));
                            if (queryIdenfifier == null || brokerIdentifier == null || fileIdentifier == null)
                            {
                                throw new CBaseException("Loan reporting: Failed to get report query.", "One of the identifiers is incorrectly formatted");
                            }

                            // NOTE: creating and registering a mock implementation of ICustomReportRunner allows testing the GUI independent of the report generation code.
                            var queryFactory = GenericLocator<ICustomReportRunner>.Factory;
                            var query = queryFactory.CreatePreparedRunner(queryIdenfifier.Value, brokerIdentifier.Value, fileIdentifier.Value, enforceReportSizeCap:true);

                            var filedbFactory = GenericLocator<IFileDbDriverFactory>.Factory;
                            var filedb = filedbFactory.Create();
                            var storage = FileStorageIdentifier.TryParse(ConstStage.FileDBSiteCodeForTempFile).Value;

                            MethodInvokeHelper.Run<CustomReportRunner, string>(query, null);

                            const int PollInterval = 1000; // 1 second
                            const int Timeout = 5 * 60 * 1000; // 5 minutes
                            var quitTime = DateTime.Now.AddMinutes(Timeout);
                            while (!filedb.FileExists(storage, fileIdentifier.Value))
                            {
                                if (DateTime.Now >= quitTime)
                                {
                                    throw new CBaseException("Loan reporting: Process timed out prior to completion.", "TIMEOUT: query_id=" + id.ToString());
                                }

                                System.Threading.Thread.Sleep(PollInterval);
                            }

                            string errorMessage = null;
                            Report ra = null;

                            try
                            {
                                const string ErrorPrefix = "ERROR: ";
                                Action<LocalFilePath> readHandler = delegate (LocalFilePath path)
                                {
                                    string contents = TextFileHelper.ReadFile(path);
                                    if (contents.StartsWith(ErrorPrefix))
                                    {
                                        errorMessage = contents.Substring(ErrorPrefix.Length);
                                    }
                                    else
                                    {
                                        ra = (Report)contents;
                                    }
                                };

                                filedb.RetrieveFile(storage, fileIdentifier.Value, readHandler);
                            }
                            finally
                            {
                                filedb.DeleteFile(storage, fileIdentifier.Value);
                            }

                            if (errorMessage != null)
                            {
                                throw new CBaseException(errorMessage, "Unable to load query_id=" + id.ToString());
                            }

                            AutoExpiredTextCache.AddToCacheByUser(BrokerUser, (string)ra, TimeSpan.FromMinutes(30), ra.Id);
                            Page.ClientScript.RegisterHiddenField("m_ReportCache", ra.Id.ToString());
                        }
                        break;

                    case "Copy":
                        {
                            // Load the query, give it a new name, and create a new
                            // one accordingly.
                            Guid id = new Guid(sQueryId);

                            if (id == Guid.Empty)
                            {
                                break;
                            }
                            CopyReport(id);
                            RefreshAllDataGrids();
                        }
                        break;

                    case "Remove":
                        {
                            // Search all queries for this broker and check the
                            // matching one against our current employee's id.
                            // Only the creator can remove it.

                            Guid id = new Guid(sQueryId);

                            if (id == Guid.Empty)
                            {
                                break;
                            }

                            // Locate all queries and compare employee identifiers.

                            DataSet dn = new DataSet();

                            SqlParameter[] parameters = {
                                                            new SqlParameter("@BrokerID", m_BrokerId),
                                                            new SqlParameter("@QueryID", id)
                                                        };

                            try
                            {
                                DataSetHelper.Fill(dn, m_BrokerId, "GetReportQueryDetails", parameters);
                            }
                            catch (CBaseException e)
                            {
                                Tools.LogError("Loan reporting: Failed to get report query details.");

                                throw e;
                            }

                            if (dn.Tables.Count > 0)
                            {
                                foreach (DataRow row in dn.Tables[0].Rows)
                                {
                                    if (id == new Guid(row["QueryId"].ToString()) && m_EmployeeId != new Guid(row["EmployeeId"].ToString()))
                                    {
                                        m_ExceptionDetails = "Permission denied.  Only owner may remove";

                                        return;
                                    }
                                }
                            }

                            // We have a lock.  Delete this report query
                            // and bail out.

                            try
                            {
                                parameters = new SqlParameter[] {
                                                                new SqlParameter("@QueryID", id)
                                                            };
                                StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, "RemoveReportQuery", 0, parameters);
                                RefreshAllDataGrids();
                            }
                            catch (CBaseException e)
                            {
                                // Note the error and report to user.

                                m_ExceptionDetails = "Unable to remove report.  Please try again";

                                Tools.LogError("Loan reporting: Failed to remove report query.");
                                Tools.LogError(e);

                                return;
                            }
                        }
                        break;
                }
            }
            catch (PermissionException e)
            {
                m_ExceptionDetails = "The selected report contains restricted fields that require permission to access. Please remove them in order to run the report.";
                Tools.LogError("Loan reporting: Exception caught during handling grid operation.", e);
            }
            catch (CBaseException e)
            {
                m_ExceptionDetails = e.UserMessage ?? "Failed to handle request.  Please try again.";

                Tools.LogError("Loan reporting: Exception caught during handling grid operation.", e);
            }

        }
       

        /// <summary>
        /// Handle request for new, blank report.
        /// </summary>

        protected void AddNewReportQueryClick( object sender , System.EventArgs a )
        {
            // Process new query request.

            Guid task = Guid.NewGuid(); Tools.LogInfo( "Loan reporting: Processing new query request (" + task.ToString() + ")..." );

            try
            {
                // Redirect to the editor without a query id.

                Response.Redirect( "LoanReportEditor.aspx?mode=edit" + "&rt=0", false );
            }
            catch (CBaseException e)
            {
                m_ExceptionDetails = "Error adding new query.";

                Tools.LogError( "Loan reporting: Exception caught during add new report.", e );
            }

        }

        protected void AddToFavorites(object sender, System.EventArgs a)
        {
            List<Guid> selectedIds = GetSelectedItems();
            if (selectedIds.Count() == 0)
            {
                m_FeedbackMessage = "Please select at least one report.";
                return;
            }

            string csvSelectedIds = string.Join(",", (from id in selectedIds select id.ToString()).ToArray());
            
            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@EmployeeId", m_EmployeeId),
                                                new SqlParameter("@CommaSeparatedReportIds", csvSelectedIds)
                                            };

                StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, "AddReportsToFavoriteForEmployee", 0, parameters);
            }
            catch (SqlException e)
            {                
                m_ExceptionDetails = "Unable to add to favorite reports.  Please try again";
                Tools.LogError("Loan reporting: Failed to add to favorite reports.", e);
                return;
            }

            RefreshAllDataGrids();
            AlertUser("The selected report(s) have been added to favorites.");
        }

        protected void RemoveFavorites(object sender, System.EventArgs a)
        {
            List<Guid> selectedIds = GetSelectedItems();
            if (selectedIds.Count() == 0)
            {
                m_FeedbackMessage = "Please select at least one report.";
                return;
            }

            string csvSelectedIds = string.Join(",", (from id in selectedIds select id.ToString()).ToArray());

            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@EmployeeId", m_EmployeeId),
                                                new SqlParameter("@CommaSeparatedReportIds", csvSelectedIds)
                                            };
                StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, "RemoveFavoriteReportsForEmployee", 0, parameters);
            }
            catch (SqlException e)
            {
                m_ExceptionDetails = "Unable to add to favorite reports.  Please try again";
                Tools.LogError("Loan reporting: Failed to add to favorite reports.", e);
                return;
            }

            RefreshAllDataGrids();
            AlertUser("The selected report(s) have been removed from favorites.");
        }
        /// <summary>
        /// Handle request for new, blank report.
        /// </summary>

        private List<Guid> GetSelectedItems()
        {
            string sCheckboxId = "Selection" + txtSelTabCode.Value;
            MeridianLink.CommonControls.CommonDataGrid grid;
            switch (txtSelTabCode.Value)
            {
                case "C":
                    grid = m_Grid;
                    break;
                case "O":
                    grid = m_Othr;
                    break;
                case "D":
                    grid = m_exReport;
                    break;
                case "S":
                    grid = m_Demo;
                    break;
                case "F":
                    grid = m_FavReports;
                    break;
                default:
                    grid = m_Grid;
                    break;
            }
            List<Guid> selects = new List<Guid>();
            foreach (DataGridItem item in grid.Items)
            {
                if (item == null)
                {
                    continue;
                }

                CheckBox selection = item.FindControl(sCheckboxId) as CheckBox;

                if (selection == null)
                {
                    continue;
                }

                if (selection.Checked == true)
                {
                    selects.Add(new Guid(selection.Attributes["Key"]));
                }

                selection.Checked = false;
            }
            return selects;
        }

        private Hashtable GetSampleQueries()
        {
            Hashtable samples = new Hashtable();
            DataSet ds = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_PmlDojoId)
                                        };
            DataSetHelper.Fill(ds, m_PmlDojoId, "GetReportQueryDetails", parameters);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                samples.Add(row["QueryId"], row["QueryName"]);
            }
            return samples;
        }

        private Hashtable GetCurrentQueries()
        {
            Hashtable currentReports = new Hashtable();
            DataSet ds = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_BrokerId)
                                        };
            DataSetHelper.Fill(ds, m_BrokerId, "GetReportQueryDetails", parameters);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                currentReports.Add(row["QueryId"].ToString(), row["QueryName"]);
            }
            return currentReports;
        }


        private List<Guid> GetSelectedSamplesCopy()
        {
             // Find the selected items and copy each.  For each,
                // we check all the names before copying any.

                Hashtable samples = GetSampleQueries();                
                List<Guid> selects = GetSelectedItems();

                if( selects.Count <= 0 )
                {
                    m_FeedbackMessage = "Please specify at least one sample to copy.";
                    return null;
                }

                // Given each sample id, we check for a report already
                // in place with the target file's name.  If any name
                // collides, we abort, and do not copy any of them.

               

                
                // Now, because each sample query has no collisions in
                // the current set, we copy the instance into a new
                // query owned by the invoking user.

                foreach (Guid id in selects)
                {
                    string name = samples[id] as string;

                    if (_currentReports.ContainsValue(name) == true)
                    {
                        m_ExceptionDetails = "Unable to add sample reports -- report with the name \'" + name + "\' already exists.\nOmit duplicate and try again.\nNo samples were copied at this time.";

                        return null;
                    }
                }
                return selects;
        }

        private void CopyReport(Guid id)
        {
            // Lookup the corresponding xml document for the query.

            List<SqlParameter> pa = new List<SqlParameter>();
            DataSet ds = new DataSet();
            Query rq = new Query();

            pa.Add(new SqlParameter("@BrokerID", m_BrokerId));
            pa.Add(new SqlParameter("@QueryID", id));

            try
            {
                DataSetHelper.Fill(ds, m_BrokerId, "GetReportQuery", pa);
            }
            catch (CBaseException e)
            {
                Tools.LogError("Loan reporting: Failed to get report query.");

                throw e;
            }

            if (ds.Tables.Count > 0)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (id == new Guid(row["QueryId"].ToString()))
                    {
                        rq = row["XmlContent"].ToString();

                        break;
                    }
                }
            }

            if (rq.Id != id)
            {
                m_ExceptionDetails = "Unable to copy report.  Failed to load query";

                return;
            }

            // Find an existing query that already has the name we
            // plan to use for the duplicate.

            List<SqlParameter> pn = new List<SqlParameter>();
            DataSet dn = new DataSet();

            pn.Add(new SqlParameter("@BrokerID", m_BrokerId));

            try
            {
                DataSetHelper.Fill(dn, m_BrokerId, "GetReportQueryDetails", pn);
            }
            catch (CBaseException e)
            {
                Tools.LogError("Loan reporting: Failed to get report query details.");

                throw e;
            }

            // When a copy is about to be made, the name shall be name of the original report
            // followed by "(" and the copyString, a space, copyNumber and finally ")"
            // Exception is when copyNumber = 1. Then the space and copyNumber don't appear
            // in the new name
            string newName = "";

            if (dn.Tables.Count > 0)
            {
                const int QueryNameMaxLength = 64;
                const int nMaxRetries = 100;
                int nRetries = 0;
                int copyIndex = 0;
                string originalName = rq.Label.Title;

                Match m = Regex.Match(originalName, @"\s\(copy\s?(?<number>\d{0,3})\)$", RegexOptions.IgnoreCase | RegexOptions.Singleline);
                if (m.Success)
                {
                    int currentIndex = -1;
                    Group gr = m.Groups["number"];

                    if (gr != null && gr.Value.Length != 0 && Int32.TryParse(gr.Value, out currentIndex))
                    {
                        copyIndex = (currentIndex > 0) ? currentIndex + 1 : 2; //index goes from 0 to 2,3,4...
                    }
                    originalName = originalName.Substring(0, m.Index);
                }

                List<string> dup = (from s in dn.Tables[0].AsEnumerable()
                                    select s.Field<string>("QueryName").ToLower()).ToList<string>();

                do
                {
                    string append = (copyIndex == 0) ? " (copy)" : string.Format(" (copy {0})", copyIndex);
                    newName = originalName + append;
                    newName = (newName.Length > QueryNameMaxLength) ? originalName.Substring(0, QueryNameMaxLength - append.Length) + append : newName;

                    copyIndex = (copyIndex > 0) ? copyIndex + 1 : 2; //index goes from 0 to 2,3,4...
                    nRetries++;

                } while (dup.Contains<string>(newName.ToLower()) && nRetries < nMaxRetries);

            }

            // Create a new query with an updated name.  We use
            // the current user's credentials.  Update the name
            // and identifier, as well as the last saved info.

            //ArrayList pc = new ArrayList();

            rq.Label.Title = newName;
            rq.Label.SavedBy = m_EmployeeNm;
            rq.Label.SavedId = m_EmployeeId;
            rq.Label.SavedOn = DateTime.Now;

            rq.Id = Guid.NewGuid();

            SqlParameter[] parameters = {
                new SqlParameter("@BrokerID", m_BrokerId),
                new SqlParameter("@QueryID", rq.Id),
                new SqlParameter("@EmployeeID", m_EmployeeId),
                new SqlParameter("@QueryName", rq.Label.Title),
                new SqlParameter("@XmlContent", rq.ToString())
            };

            try
            {
                StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, "CreateReportQuery", 0, parameters);
            }
            catch (CBaseException e)
            {
                // Note the error and report to user.

                m_ExceptionDetails = "Unable to create duplicate report.  Please try again";

                Tools.LogError("Loan reporting: Failed to create duplicate report query.", e);

                return;
            }
        }

        protected void CopyReportQueries(object sender , System.EventArgs a )
        {
            if (txtSelTabCode.Value == "S") //If sample query tabs is selected
            {
                CopySampleQueriesClick();
                RefreshAllDataGrids();
            }
            else
            {
                List<Guid> selects = GetSelectedItems();
                if (selects == null) return;
                else
                {
                    foreach (Guid selectedQueryId in selects)
                    {
                        if (_htSampleQueries.Contains(selectedQueryId))//if this is a sample query
                        {
                            CopySampleQuery(selectedQueryId);
                        }
                        else
                        {
                            CopyReport(selectedQueryId);
                        }
                    }

                    RefreshAllDataGrids();
                }
            }
        }

        protected void CopySampleQuery(Guid id)
        {
            DataSet dq = new DataSet();
            SqlParameter[] parameters = {
                                            new SqlParameter( "@BrokerId" , m_PmlDojoId ) //Justin
                                            , new SqlParameter( "@QueryId"  , id )
                                        };

            DataSetHelper.Fill(dq, m_PmlDojoId, "GetReportQuery", parameters);

                foreach( DataRow row in dq.Tables[ 0 ].Rows )
                {
                    string sName = row["QueryName"].ToString();
                    if (_currentReports.ContainsValue(sName))
                    {
                        m_ExceptionDetails = "Unable to copy sample report(s) -- report with the name \'" + sName + "\' already exists.";
                        return;
                    }

                    //ArrayList pa = new ArrayList();
                    Query  query = new Query();

                    query = row[ "XmlContent" ].ToString();

                    query.Label.SavedBy = m_EmployeeNm;
                    query.Label.SavedId = m_EmployeeId;
                    query.Label.SavedOn = DateTime.Now;

                    query.Id = Guid.NewGuid();
                    parameters = new SqlParameter[] {
                        new SqlParameter("@BrokerID", m_BrokerId),
                        new SqlParameter( "@EmployeeID" ,      m_EmployeeId ),
                        new SqlParameter( "@QueryID"    ,          query.Id ),
                        new SqlParameter( "@QueryName"  , query.Label.Title ),
                        new SqlParameter( "@XmlContent" ,  query.ToString() )
                    };

                    try
                    {
                        StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, "CreateReportQuery", 0, parameters);
                    }
                    catch (SqlException e)
                    {
                        if (e.Message.IndexOf("Duplicated query name", 0, StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            m_ExceptionDetails = "Unable to create sample report '" + query.Label.Title + "' because another report with the same name exist.'";
                        }
                        else
                        {
                            m_ExceptionDetails = "Unable to create sample report '" + query.Label.Title + "'.  Please try again";
                            Tools.LogError("Loan reporting: Failed to create sample report query.", e);
                        }
                        return;
                    }
                    catch (CBaseException e)
                    {
                        // Note the error and report to user.

                        m_ExceptionDetails = "Unable to create sample report '" + query.Label.Title + "'.  Please try again";
                        Tools.LogError("Loan reporting: Failed to create sample report query.", e);

                        return;
                    }
                    break;
                }
        }
        protected void CopySampleQueriesClick()
        {
            // Process copy samples request.

            Guid task = Guid.NewGuid(); Tools.LogInfo( "Loan reporting: Processing copy request (" + task.ToString() + ")..." );

            try
            {
                List<Guid> selects = GetSelectedSamplesCopy();
                if (selects == null) return;

                foreach( Guid id in selects )
                {
                   CopySampleQuery(id);
                }

                m_FeedbackMessage = "New report queries have been successfully created from selected samples.";
            }
            catch (CBaseException e)
            {
                m_ExceptionDetails = "Error copying samples.";

                Tools.LogError( "Loan reporting: Exception caught during copy sample queries." );
                Tools.LogError( e );
            }

        }

        /// <summary>
        /// Bind our datagrid to the current reports.
        /// </summary>

        protected void BindGrid()
        {
            // Bind ui to latest persistent state.

            try
            {
                // Get the loans for this broker.  At some point, we need to
                // use a view specification using the user's credentials and
                // a security filter to get per-report access privelages.

                SqlParameter[] parameters = {
                                                new SqlParameter("@IncludeEmployeeId", m_EmployeeId),
                                                new SqlParameter("@BrokerID", m_BrokerId),
                                                new SqlParameter("@RequestingEmployeeId", m_EmployeeId)
                                            };
				DataSet dS = new DataSet();

				DataSetHelper.Fill( dS, m_BrokerId , "ListReportQuery", parameters);


                // Extend the data table by adding columns for last saved by
                // info and query comments.  These values are stored within
                // the text of the query, so we must crack each open and
                // pull out the content.

                DataTable dt = dS.Tables[ 0 ];

                dt.Columns.Add( "QuerySavedBy" );
                dt.Columns.Add( "QuerySavedOn" );
                dt.Columns.Add( "QueryDetails" );
                dt.Columns.Add("QueryAccess");
                dt.Columns.Add("SecureFields");

                foreach( DataRow row in dt.Rows )
                {
					try
					{
						// Open the query to read it.

						Query query = row[ "XmlContent" ].ToString();

						if( query.Id != Guid.Empty )
						{
							row[ "QuerySavedBy" ] = query.Label.SavedBy;
							row[ "QuerySavedOn" ] = query.Label.SavedOn.ToLongDateString();
							row[ "QueryDetails" ] = query.Details;
                            row["QueryAccess"] = query.CanUserRun(BrokerUser).ToString();
                            row["SecureFields"] = query.hasSecureFields.ToString();
						}
					}
                    catch (CBaseException e)
					{
						m_ExceptionDetails = "Error binding list to web form.";

						Tools.LogError( "Loan reporting: Exception caught during grid binding of " + row[ "QueryName" ] + "." );
						Tools.LogError( "Loan reporting: " + row[ "XmlContent" ] + ".", e );
					}
                }

                // Apply the data view.

                m_Grid.DataSource = dt.DefaultView;
                m_Grid.DataBind();
            }
            catch (CBaseException e)
            {
				// Oops!

                m_ExceptionDetails = "Error binding list to web form.";

                Tools.LogError( "Loan reporting: Exception caught during grid binding.", e );
            }
        }

        private void ProcessFavorite(DataRow row)
        {
            try
            {
                Query query = row["XmlContent"].ToString();

                if (query.Id != Guid.Empty)
                {
                    row["QuerySavedBy"] = query.Label.SavedBy;

                    row["QuerySavedOn"] = query.Label.SavedOn.ToLongDateString();

                    row["QueryDetails"] = query.Details;

                    row["QueryAccess"] = query.CanUserRun(BrokerUser).ToString();
                    row["SecureFields"] = query.hasSecureFields.ToString();
                }
            }
            catch (CBaseException e)
            {
                m_ExceptionDetails = "Error binding list to web form.";

                Tools.LogError("Loan reporting: Exception caught during favorites binding of " + row["QueryName"] + ".");
                Tools.LogError("Loan reporting: " + row["XmlContent"] + ".", e);
            }
        }

        protected void BindFavorites()
		{
			// Bind ui to latest persistent state.

			try
			{
				// Get the loans for this broker.  At some point, we need to
				// use a view specification using the user's credentials and
				// a security filter to get per-report access privelages.                
				DataSet dS = new DataSet();

                SqlParameter[] parameters = {
                                                new SqlParameter("@EmployeeId", m_EmployeeId)
                                            };
                DataSetHelper.Fill(dS, m_BrokerId, "GetFavoriteReportsForEmployeePreserveEmployeeFaves", parameters);
				
				DataTable dt = dS.Tables[ 0 ];		

				// Apply the data view.
                dt.Columns.Add("QuerySavedBy");
                dt.Columns.Add("QuerySavedOn");
                dt.Columns.Add("QueryDetails");
                dt.Columns.Add("QueryAccess");
                dt.Columns.Add("SecureFields");
                dt.Columns.Add("IsSample");

                bool getFromPMLDojo = false;
                HashSet<string> sampleQueryIds = new HashSet<string>();

                for (int i = dt.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow row = dt.Rows[i];
                    
                    // If we see that a QueryId is empty, then these should be sample reports that are stored on a different DB on the PMLDojo broker. We'll need to pull from that db after this.
                    // At the same time, remove this from the data table and keep track of the QueryId stored in the Favorites table.
                    if (row["QueryId"] == DBNull.Value)
                    {
                        getFromPMLDojo = true;
                        sampleQueryIds.Add(row["FavQId"].ToString());
                        row.Delete();
                    }
                    else
                    {
                        row["IsSample"] = "false";
                        this.ProcessFavorite(row);
                    }
                }

                // Remove this column since we don't need it anymore.
                dt.Columns.Remove("FavQId");

                // We need to grab from PMLDojo.
                if (getFromPMLDojo)
                {
                    dt.AcceptChanges();

                    DataSet dojoDS = new DataSet();

                    SqlParameter[] parm = {
                                              new SqlParameter("@BrokerID", m_PmlDojoId)
                                          };


                    DataSetHelper.Fill(dojoDS, m_PmlDojoId, "ListReportQuery", parm);
                    DataTable dojoTable = dojoDS.Tables[0];
                    dojoTable.Columns.Add("QuerySavedBy");
                    dojoTable.Columns.Add("QuerySavedOn");
                    dojoTable.Columns.Add("QueryDetails");
                    dojoTable.Columns.Add("QueryAccess");
                    dojoTable.Columns.Add("SecureFields");
                    dojoTable.Columns.Add("IsSample");

                    var dojoRows = dojoTable.AsEnumerable().Where(row => sampleQueryIds.Contains(row["QueryId"].ToString()));
                    foreach (DataRow row in dojoRows)
                    {
                        row["IsSample"] = "Demo";
                        this.ProcessFavorite(row);
                        dt.Rows.Add(row.ItemArray);
                    }
                }

                m_FavReports.DataSource = dt.DefaultView;
                m_FavReports.DataBind();
			}
            catch (CBaseException e)
			{
				// Oops!

				m_ExceptionDetails = "Error binding list to web form.";

				Tools.LogError( "Loan reporting: Exception caught during favorites binding.", e );
			}
		}
		/// <summary>
		/// Bind our datagrid to the current reports.
		/// </summary>

		protected void BindOthr()
		{
			// Bind ui to latest persistent state.

			try
			{
				// Get the loans for this broker.  At some point, we need to
				// use a view specification using the user's credentials and
				// a security filter to get per-report access privelages.

                SqlParameter[] parameters = {
                                                new SqlParameter("@ExcludeEmployeeId", m_EmployeeId),
                                                new SqlParameter("@BrokerId", m_BrokerId),
                                                new SqlParameter("@RequestingEmployeeId", m_EmployeeId),
                                                new SqlParameter("@IncludeDisabledUsers", 1)
                                            };
				DataSet dS = new DataSet();

				DataSetHelper.Fill( dS , m_BrokerId, "ListReportQuery", parameters);

				// Extend the data table by adding columns for last saved by
				// info and query comments.  These values are stored within
				// the text of the query, so we must crack each open and
				// pull out the content.

				DataTable dt = dS.Tables[ 0 ];

				dt.Columns.Add( "QuerySavedBy" );
				dt.Columns.Add( "QuerySavedOn" );
				dt.Columns.Add( "QueryDetails" );
                dt.Columns.Add( "QueryAccess");
                dt.Columns.Add("SecureFields");

                DataTable activeDt = dt.Clone();    // reports from active users
                DataTable disableDt = dt.Clone();   // reports from disable users

                foreach ( DataRow row in dt.Rows )
				{
					// Open the query to read it.

					try
					{
						Query query = row[ "XmlContent" ].ToString();

						if( query.Id != Guid.Empty )
						{
							row[ "QuerySavedBy" ] = query.Label.SavedBy;
							row[ "QuerySavedOn" ] = query.Label.SavedOn.ToLongDateString();
							row[ "QueryDetails" ] = query.Details;

                            row["QueryAccess"] = query.CanUserRun(BrokerUser).ToString();
                            row["SecureFields"] = query.hasSecureFields.ToString();

						}

                        DataTable newDt = (bool)row["IsActiveUser"] ? activeDt : disableDt;
                        DataRow newRow = newDt.NewRow();
                        newRow.ItemArray = row.ItemArray;
                        newDt.Rows.Add(newRow);
                    }
                    catch (CBaseException e)
					{
						m_ExceptionDetails = "Error binding list to web form.";

						Tools.LogError( "Loan reporting: Exception caught during othr binding of " + row[ "QueryName" ] + "." );
						Tools.LogError( "Loan reporting: " + row[ "XmlContent" ] + ".", e );
					}
				}

				// Apply the data view.

				m_Othr.DataSource = activeDt.DefaultView;
				m_Othr.DataBind();

                m_exReport.DataSource = disableDt.DefaultView;
                m_exReport.DataBind();
            }
            catch (CBaseException e)
			{
				// Oops!

				m_ExceptionDetails = "Error binding list to web form.";

				Tools.LogError( "Loan reporting: Exception caught during othr binding.", e );
			}
		}
		
		protected void BindDemo()
        {
            // Bind ui to latest persistent state.

            try
            {
                // Get the loans for this broker.  At some point, we need to
                // use a view specification using the user's credentials and
                // a security filter to get per-report access privelages.

                DataSet   ds = new DataSet();

                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", m_PmlDojoId),
                                                new SqlParameter("@RequestingEmployeeId", m_EmployeeId)
                                            };


                DataSetHelper.Fill( ds , m_PmlDojoId, "ListReportQuery" , parameters );

                // Get the blank report id so we can detect the one to skip
                // when binding to this grid.

                Guid bi = GetBlankReportId();


                // Extend the data table by adding columns for last saved by
                // info and query comments.  These values are stored within
                // the text of the query, so we must crack each open and
                // pull out the content.

                DataTable dt = ds.Tables[ 0 ];

                dt.Columns.Add( "QuerySavedBy" );
                dt.Columns.Add( "QuerySavedOn" );
                dt.Columns.Add( "QueryDetails" );

                foreach( DataRow row in dt.Rows )
                {
                    // Find the blank report template and remove
                    // it from the list.

                    if( row[ "QueryId" ].ToString() == bi.ToString() )
                    {
                        dt.Rows.Remove( row );

                        break;
                    }
                }

                foreach( DataRow row in dt.Rows )
                {
                    // Open the query to read it.

					try
					{
						Query query = row[ "XmlContent" ].ToString();

						if( query.Id != Guid.Empty )
						{
							row[ "QuerySavedBy" ] = query.Label.SavedBy;
							row[ "QuerySavedOn" ] = query.Label.SavedOn.ToLongDateString();

							row[ "QueryDetails" ] = query.Details;
						}
					}
                    catch (CBaseException e)
					{
						m_ExceptionDetails = "Error binding list to web form.";

						Tools.LogError( "Loan reporting: Exception caught during demo binding of " + row[ "QueryName" ] + "." );
						Tools.LogError( "Loan reporting: " + row[ "XmlContent" ] + ".", e );
					}
				}

                m_Demo.DataSource = dt.DefaultView;
                m_Demo.DataBind();
            }
            catch (CBaseException e)
            {
                m_ExceptionDetails = "Error binding list to web form.";

                Tools.LogError( "Loan reporting: Exception caught during demo binding.", e );
            }
        }

        private Guid GetBlankReportId()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", LendersOffice.Admin.BrokerDB.AdminBr)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(LendersOffice.Admin.BrokerDB.AdminBr, "GetAdminBrokerReportQueryTemplateId", parameters))
            {
                if (reader.Read())
                {
                    return (Guid)reader["QueryId"];
                }
            }
            return Guid.Empty;
        }

        /// <summary>
        /// This is an expensive method. Only call after perform Copy or Delete.
        /// </summary>
        private void RefreshAllDataGrids()
        {
            if (!ConstStage.EnableOPM_456337_Fix_CustomReportList)
            {
                // dd 4/28/2017 - We are in the old behavior. In old behavior the data grids are always refresh in PreRender event.
                // Therefore this method is not need.
                return;
            }

            BindFavorites();
            BindGrid();
            BindOthr();
            BindDemo();
        }
        /// <summary>
        /// Render this page.
        /// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
            // Initialize page objects.

            try
            {
                // Bind to the grid.
                BindFavorites();
				BindGrid();
				BindOthr();
				BindDemo();
            }
            catch (CBaseException e)
            {
				// Oops!

                m_ExceptionDetails = "Error prerendering web form.";

                Tools.LogError( "Loan reporting: Exception caught during page prerender.", e );
            }
		}

        /// <summary>
        /// Initialize this page.
        /// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
            // Initialize page objects.

            try
            {
                // Bind to the grid.
                _htSampleQueries = GetSampleQueries();
                _currentReports = GetCurrentQueries();

                BindFavorites();
                BindGrid();
				BindOthr();
				BindDemo();

                // Set visibility of columns based on
                // the user's permissions.

                if (!CanUserPublishReports)
                {
                    var publishedAs = m_Grid.Columns[4];
                    var publishLink = m_Grid.Columns[5];
                    publishedAs.Visible = publishLink.Visible = false;

                    var reportName = m_Grid.Columns[2];
                    var showProps = m_Grid.Columns[3];

                    reportName.ItemStyle.Width = Unit.Pixel(300);
                    showProps.ItemStyle.Width = Unit.Pixel(320);
                }

                // for active users' reports
                var viewOthers = m_Othr.Columns[6];
                var editOthers = m_Othr.Columns[7];

                editOthers.Visible = BrokerUser.HasPermission(Permission.CanEditOthersCustomReports);
                viewOthers.Visible = !editOthers.Visible;

                // for disable users' reports
                viewOthers = m_exReport.Columns[6];
                editOthers = m_exReport.Columns[7];

                editOthers.Visible = BrokerUser.HasPermission(Permission.CanEditOthersCustomReports);
                viewOthers.Visible = !editOthers.Visible;
            }
            catch (CBaseException e)
            {
				// Oops!

                m_ExceptionDetails = "Error loading web form.";

                Tools.LogError( "Loan reporting: Exception caught during page load.", e );
            }
		}

        /// <summary>
        /// Initialize this page.
        /// </summary>

		protected void PageInit( object sender, System.EventArgs a )
		{
            this.RegisterJsScript("LQBPopup.js");	
            bool isAdmin = BrokerUser.IsInRole(ConstApp.ROLE_ADMINISTRATOR);
            bool canRunCustomReports = BrokerUser.HasPermission(Permission.CanRunCustomReports);
            if (!(isAdmin || canRunCustomReports))
                Response.Redirect("~/los/main.aspx?showAlert=t");

            // Initialize page objects.

            try
            {
				// Initialize loan reporting members.
				m_Loans = new LoanReporting();
                this.m_PmlDojoId = GetPmlDojoId();
			}
            catch (CBaseException e)
            {
				// Oops!

                m_ExceptionDetails = "Error initializing web form.";

                Tools.LogError( "Loan reporting: Exception caught during page init.", e );
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.EnableJqueryMigrate = false;
            InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            if (!ConstStage.EnableOPM_456337_Fix_CustomReportList)
            {
                // dd 4/28/2017 - This PreRender event is unneccessary and only cause performance issue by calling extra ListReportQuery.
                // Remove once the fix enable on production.
                this.PreRender += new System.EventHandler(this.PagePreRender);
            }
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
		#endregion

        /// <summary>
        /// Make the column at idx span all columns to its right, and contain the favorite legend.
        /// </summary>
        /// <param name="cells"></param>
        /// <param name="idx"></param>
        private void AddFavoriteHeader(TableCellCollection cells, int idx)
        {
            int span = 1;
            while (cells.Count > idx + 1)
            {
                span++;
                cells.RemoveAt(idx + 1);
            }

            cells[idx].ColumnSpan = span;
            cells[idx].Attributes["align"] = "right";
            cells[idx].Style.Add("padding-right", "10px");
            cells[idx].Text = "<img style='border:none;' src='../../images/favorite.png' align='middle' alt='Favorite' width='12' height='12'>&nbsp;-&nbsp;Favorite&nbsp;reports</img>";
        }

		/// <summary>
		/// Handle grid binding.
		/// </summary>
        /// 

        protected void OnSampleGridBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs a)
        {
            if (a.Item.ItemType == ListItemType.Header)
            {
                AddFavoriteHeader(a.Item.Cells, 3);
            }

            DataRowView dR = a.Item.DataItem as DataRowView;
            
            if(dR == null)
            {
                return;
            }

            PlaceHolder favoriteIconPlaceHolder = (PlaceHolder)a.Item.FindControl("FavoriteIconPlaceHolderSample");
            favoriteIconPlaceHolder.Visible = dR["IsFavorite"].ToString() != "0";
        }

        protected void CurrReportGridBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs a)
        {            
            // Lookup the state of the report and hide links if not
            // allowed to edit.

            DataRowView dR = a.Item.DataItem as DataRowView;

            if(a.Item.ItemType == ListItemType.Header)
            {
                AddFavoriteHeader(a.Item.Cells, 3);
            }


            if (dR == null)
            {
                return;
            }

            Label restricted = a.Item.FindControl("m_CurrRestrictedField") as Label;
            Label permission = a.Item.FindControl("m_CurrPermRequired") as Label;
            LinkButton lRun = a.Item.FindControl("Run") as LinkButton;
            PlaceHolder favoriteIconPlaceHolder = (PlaceHolder)a.Item.FindControl("FavoriteIconPlaceHolderCurrent");
            favoriteIconPlaceHolder.Visible = dR["IsFavorite"].ToString() != "0";
            

            bool hasPermission = false;
            bool hasSecureFields = false;

            hasPermission = SafeConvert.ToString(dR["QueryAccess"]).ToLower() == "true";
            hasSecureFields = SafeConvert.ToString(dR["SecureFields"]).ToLower() == "true";

            if (!hasSecureFields)
            {
                //RESTRICTED_FIELDS_MSG / NO_ACCESS_MSG / REMOVE_FIELDS_MSG
                restricted.Visible = false;
                permission.Visible = false;
            }
            else if (!hasPermission)    // && hasSecureFields
            {
                restricted.Visible = true;
                permission.Visible = true;
                restricted.Text = RESTRICTED_FIELDS_MSG;
                permission.Text = REMOVE_FIELDS_MSG;

                lRun.Enabled = false;
            }
            else    // hasSecureFields && hasPermission
            {
                restricted.Visible = true;
                restricted.Text = RESTRICTED_FIELDS_MSG;
                permission.Visible = false;
            }

            Boolean bIsPublished = (Boolean)DataBinder.Eval(a.Item.DataItem, "IsPublished");

            // Show published state.
            Label published = a.Item.FindControl("Published") as Label;                
            LinkButton unpublish = a.Item.FindControl("Unpublish") as LinkButton;
            LinkButton publish = a.Item.FindControl("Publish") as LinkButton;

            // Stores the record's "is published" value in the hidden column
            if (published != null)
            {
                published.Text = bIsPublished.ToString();
            }

            // Show the published checkmark for published reports.                
            if (publish != null) publish.Visible = !bIsPublished;
            if (unpublish != null) unpublish.Visible = bIsPublished;
        }

        protected void FavoriteReportGridBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs a)
		{
			DataRowView dR = a.Item.DataItem as DataRowView;

			if( dR != null )
			{
                if (_htSampleQueries.ContainsKey(dR["QueryId"]))
                     a.Item.Cells[2].Text = $"Sample - {dR["QueryName"]}";                            

                Panel summaryPanel = a.Item.FindControl("m_OthersSummaryPanel") as Panel;
                Label restricted = a.Item.FindControl("m_OthersRestrictedField") as Label;
                Label permission = a.Item.FindControl("m_OthersPermRequired") as Label;
                CheckBox selectionF = (CheckBox)a.Item.FindControl("SelectionF");
                selectionF.Attributes["Key"] = dR["QueryID"].ToString();

                EncodedLiteral savedByLiteral = (EncodedLiteral)a.Item.FindControl("FavoritesSavedByLiteral");
                savedByLiteral.Text = dR["QuerySavedBy"].ToString();
                EncodedLiteral savedOnLiteral = (EncodedLiteral)a.Item.FindControl("FavoritesSavedOnLiteral");
                savedOnLiteral.Text = dR["QuerySavedOn"].ToString();
                EncodedLiteral queryDetailsLiteral = (EncodedLiteral)a.Item.FindControl("FavoritesQueryDetailsLiteral");
                queryDetailsLiteral.Text = dR["QueryDetails"].ToString();

                string queryArgs = dR["QueryID"].ToString() + "," + dR["IsSample"].ToString();
                LinkButton lRun = a.Item.FindControl("Run") as LinkButton;
                lRun.CommandName = "Run";
                lRun.CommandArgument = queryArgs;

                LinkButton viewLink = (LinkButton)a.Item.FindControl("View");
                viewLink.CommandName = "View";
                viewLink.CommandArgument = queryArgs;

                bool hasPermission = false;
                bool hasSecureFields = false;

                hasPermission   = SafeConvert.ToString(dR["QueryAccess" ]).ToLower() == "true";
                hasSecureFields = SafeConvert.ToString(dR["SecureFields"]).ToLower() == "true";

                if (!hasSecureFields)
                {
                    //RESTRICTED_FIELDS_MSG / NO_ACCESS_MSG / REMOVE_FIELDS_MSG
                    summaryPanel.Visible = true;
                    restricted.Visible = false;
                    permission.Visible = false;
                }
                else if (!hasPermission)    // && hasSecureFields
                {
                    summaryPanel.Visible = false;
                    restricted.Visible = true;
                    permission.Visible = true;
                    restricted.Text = RESTRICTED_FIELDS_MSG;
                    permission.Text = NO_ACCESS_MSG;

                    lRun.Enabled = false;                    
                }
                else    // hasSecureFields && hasPermission
                {
                    summaryPanel.Visible = true;
                    restricted.Visible = true;
                    restricted.Text = RESTRICTED_FIELDS_MSG;
                    permission.Visible = false;
                }                
			}		
		}

        protected void ReportGridBound( object sender , System.Web.UI.WebControls.DataGridItemEventArgs a )
		{
			// Filter out ui on a per row basis.
			
			// Lookup the state of the report and hide links if not
			// allowed to edit.

			DataRowView dR = a.Item.DataItem as DataRowView;
            
            if (a.Item.ItemType == ListItemType.Header)
            {
                AddFavoriteHeader(a.Item.Cells, 4);
            }

            if (dR == null)
            {
                return;
            }

            Panel summaryPanel = a.Item.FindControl("m_OthersSummaryPanel") as Panel;
            Label restricted = a.Item.FindControl("m_OthersRestrictedField") as Label;
            Label permission = a.Item.FindControl("m_OthersPermRequired") as Label;
            PlaceHolder favoriteIconPlaceHolder = (PlaceHolder)a.Item.FindControl("FavoriteIconPlaceHolderAvailable");
            favoriteIconPlaceHolder.Visible = dR["IsFavorite"].ToString() != "0";

            CheckBox selectionO = (CheckBox)a.Item.FindControl("SelectionO");
            selectionO.Attributes["Key"] = dR["QueryID"].ToString();

            LinkButton lRun = a.Item.FindControl("Run") as LinkButton;
            LinkButton lEdit = a.Item.FindControl("Edit") as LinkButton;

			if( ( Boolean ) dR[ "IsPublished" ] == true && BrokerUser.EmployeeId != ( Guid ) dR[ "EmployeeId" ] )
			{
				// If published and not by you, then no editing.
				if( lEdit != null )
				{
					lEdit.Enabled = false;
				}
			}

            bool hasPermission = false;
            bool hasSecureFields = false;

            hasPermission = SafeConvert.ToString(dR["QueryAccess"]).ToLower() == "true";
            hasSecureFields = SafeConvert.ToString(dR["SecureFields"]).ToLower() == "true";

            if (!hasSecureFields)
            {
                //RESTRICTED_FIELDS_MSG / NO_ACCESS_MSG / REMOVE_FIELDS_MSG
                summaryPanel.Visible = true;
                restricted.Visible = false;
                permission.Visible = false;
            }
            else if (!hasPermission)    // && hasSecureFields
            {
                summaryPanel.Visible = false;
                restricted.Visible = true;
                permission.Visible = true;
                restricted.Text = RESTRICTED_FIELDS_MSG;
                permission.Text = NO_ACCESS_MSG;

                lRun.Enabled = false;
                lEdit.Enabled = false;
            }
            else    // hasSecureFields && hasPermission
            {
                summaryPanel.Visible = true;
                restricted.Visible = true;
                restricted.Text = RESTRICTED_FIELDS_MSG;
                permission.Visible = false;
            }
		}

        protected void ExReportGridBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs a)
        {
            // Filter out ui on a per row basis.

            // Lookup the state of the report and hide links if not
            // allowed to edit.

            DataRowView dR = a.Item.DataItem as DataRowView;

            if (a.Item.ItemType == ListItemType.Header)
            {
                AddFavoriteHeader(a.Item.Cells, 4);
            }

            if (dR == null)
            {
                return;
            }

            Panel summaryPanel = a.Item.FindControl("m_OthersSummaryPanel") as Panel;
            Label restricted = a.Item.FindControl("m_OthersRestrictedField") as Label;
            Label permission = a.Item.FindControl("m_OthersPermRequired") as Label;
            PlaceHolder favoriteIconPlaceHolder = (PlaceHolder)a.Item.FindControl("FavoriteIconPlaceHolder4ExReport");
            favoriteIconPlaceHolder.Visible = dR["IsFavorite"].ToString() != "0";

            CheckBox selectionD = (CheckBox)a.Item.FindControl("SelectionD");
            selectionD.Attributes["Key"] = dR["QueryID"].ToString();

            LinkButton lRun = a.Item.FindControl("Run") as LinkButton;
            LinkButton lEdit = a.Item.FindControl("Edit") as LinkButton;

            if ((Boolean)dR["IsPublished"] == true && BrokerUser.EmployeeId != (Guid)dR["EmployeeId"])
            {
                // If published and not by you, then no editing.
                if (lEdit != null)
                {
                    lEdit.Enabled = false;
                }
            }

            bool hasPermission = false;
            bool hasSecureFields = false;

            hasPermission = SafeConvert.ToString(dR["QueryAccess"]).ToLower() == "true";
            hasSecureFields = SafeConvert.ToString(dR["SecureFields"]).ToLower() == "true";

            if (!hasSecureFields)
            {
                //RESTRICTED_FIELDS_MSG / NO_ACCESS_MSG / REMOVE_FIELDS_MSG
                summaryPanel.Visible = true;
                restricted.Visible = false;
                permission.Visible = false;
            }
            else if (!hasPermission)    // && hasSecureFields
            {
                summaryPanel.Visible = false;
                restricted.Visible = true;
                permission.Visible = true;
                restricted.Text = RESTRICTED_FIELDS_MSG;
                permission.Text = NO_ACCESS_MSG;

                lRun.Enabled = false;
                lEdit.Enabled = false;
            }
            else    // hasSecureFields && hasPermission
            {
                summaryPanel.Visible = true;
                restricted.Visible = true;
                restricted.Text = RESTRICTED_FIELDS_MSG;
                permission.Visible = false;
            }
        }

        protected void AlertUser(string message)
        {
            string sScript = @"<script type='text/javascript'>
            /*[CDATA[*/
                alert('" + message + @"');
            /*]]*/
            </script>";

            ClientScript.RegisterStartupScript(this.GetType(), "Updated", sScript);
        }
    }
}
