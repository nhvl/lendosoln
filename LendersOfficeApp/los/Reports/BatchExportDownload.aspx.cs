﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.XsltExportReport;

namespace LendersOfficeApp.los.Reports
{
    public partial class BatchExportDownload : BaseServicePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            RegisterService("main", "/los/Reports/BatchExportDownloadService.aspx");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["cmd"] == "download")
            {
                DownloadReport(Request["reportid"]);

                return;
            }
            if (Page.IsPostBack == false)
            {
                LoadDataGrid();
            }
        }

        private void DownloadReport(string reportId)
        {
            AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;

            try
            {
                XsltExportResultStatusItem resultObject = XsltExport.GetResult(principal.BrokerId, principal.UserId, reportId);

                string tempFile = FileDBTools.CreateCopy(E_FileDB.Normal, resultObject.ReportId);

                Response.Clear();
                Response.ContentType = "application/download";
                Response.AddHeader("Content-Disposition", $"attachment; filename=\"{resultObject.OutputFileName}\"");
                Response.WriteFile(tempFile);
                Response.Flush();
                Response.End();
            }
            catch (NotFoundException)
            {
                m_errorPanel.Visible = true;
                m_errorMsg.Text = "Report not found.";
            }
        }

        private void LoadDataGrid()
        {
            AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;

            var list= XsltExport.ListByUserId(principal.BrokerId, principal.UserId);

            m_grid.DataSource = list;
            m_grid.DataBind();
        }

        protected HtmlControl GenerateActionLink(object item)
        {
            HtmlControl cnt = null;
            HtmlAnchor anchor = null;
            XsltExportResultStatusItem resultObject = item as XsltExportResultStatusItem;

            if (resultObject == null)
            {
                return cnt;
            }

            if (resultObject.Status == E_XsltExportResultStatusT.Complete)
            {
                anchor = new HtmlAnchor();
                anchor.InnerText = "download";
                anchor.HRef = "#";
                anchor.Attributes.Add("onclick", "return f_download('" + resultObject.ReportId + "');");
            }
            else if (resultObject.Status == E_XsltExportResultStatusT.Error)
            {
                cnt = getRetryOrRestrictedAnchor(resultObject);
            }
            else if (resultObject.Status == E_XsltExportResultStatusT.Pending)
            {
                if (resultObject.CreatedDate.AddHours(1) < DateTime.Now)
                {
                        cnt = getRetryOrRestrictedAnchor(resultObject);
                }
            }
            else
            {
                throw new UnhandledEnumException(resultObject.Status);
            }
            return cnt ?? anchor;
        }

        protected HtmlControl getRetryOrRestrictedAnchor(XsltExportResultStatusItem resultObject)
        {
            try
            {
                // OPM 118577 - Check for restricted fields            
                HtmlAnchor anchor = null;

                string xml = resultObject.RequestXml;
                XsltExportRequest request = null;

                IEnumerable<string> xsltList = null;
            
                    request = XsltExportRequest.Parse(xml);
                    var mapItem = XsltMap.Get(request.MapName);
                    xsltList = mapItem.XslFileList;
           

                if (xsltList != null)
                {
                    XsltExportWorker worker = new XsltExportWorker(request, PrincipalFactory.CurrentPrincipal);
                    HashSet<string> possibleFieldSet = worker.ExtractPossibleLoanFields(xsltList);

                    foreach (string fieldId in possibleFieldSet)
                    {
                        if (LoanFieldSecurityManager.IsSecureField(fieldId) && !LoanFieldSecurityManager.CanReadField(fieldId))
                        {
                            anchor = new HtmlAnchor();
                            anchor.InnerText = "restricted";
                            anchor.Style.Add(HtmlTextWriterStyle.TextDecoration, "none");
                            anchor.Disabled = true;
                            return anchor;
                        }
                    }
                }

                // If we got here then no restricted fields found
                anchor = new HtmlAnchor();
                anchor.InnerText = "retry";
                anchor.HRef = "#";
                anchor.Attributes.Add("onclick", "return f_retry('" + resultObject.ReportId + "');");
                return anchor;
            }
            catch (NotFoundException )
                    {
                        var control = new HtmlGenericControl("span") {  InnerText = "Error: Missing Map"};
                        control.Style.Add("color", "red");
                        return control;
                    }
        }
    }
}