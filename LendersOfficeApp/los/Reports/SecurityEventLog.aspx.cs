﻿#region auto-generated code
namespace LendersOfficeApp.los.Reports
#endregion
{
    using System;
    using System.Data.Common;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Common.TextImport;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Security event logs page.
    /// </summary>
    public partial class SecurityEventLog : BaseServicePage
    {
        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Compatible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">EventArgs e.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowAccessingSecurityEventLogsPage))
            {
                throw new CBaseException("You do not have permission to access security event logs.", "You do not have permission to access security event logs.");
            }

            if (this.Request["cmd"] == "download")
            {
                string filterToDateStr, fromDateStr, isNowStr, ipAddr, securityEventTypeStr, employeeIdStr;
                DateTime? filterToDate = null;
                DateTime? fromDate = null;
                bool calculateToNow;
                SecurityEventType eventType;
                Guid employeeUserId;    

                try
                {
                    filterToDateStr = this.Request["todate"];
                    fromDateStr = this.Request["fromdate"];
                    isNowStr = this.Request["isnow"];
                    ipAddr = this.Request["ipaddr"];
                    securityEventTypeStr = this.Request["securityeventtype"];
                    employeeIdStr = this.Request["employeeuserid"];

                    calculateToNow = bool.Parse(isNowStr);

                    if (calculateToNow)
                    {
                        filterToDate = DateTime.MaxValue;
                    }
                    else
                    {
                        if (filterToDateStr != string.Empty)
                        {
                            filterToDate = CDateTime.Create(filterToDateStr, FormatTarget.Webform).DateTimeForComputationWithTime;
                        }
                    }

                    if (fromDateStr != string.Empty)
                    {
                        fromDate = CDateTime.Create(fromDateStr, FormatTarget.Webform).DateTimeForComputationWithTime;
                    }

                    eventType = (SecurityEventType)int.Parse(securityEventTypeStr);
                    employeeUserId = Guid.Parse(employeeIdStr);
                }
                catch
                {
                    throw new CBaseException("Trying to export security event logs with invalid search parameters.", "Trying to export security event logs with invalid search parameters.");
                }

                SecurityEventLogsFilter filter = new SecurityEventLogsFilter();
                filter.BrokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
                filter.FromDate = fromDate;
                filter.FilterToDate = filterToDate;
                filter.EventType = eventType;
                filter.IpAddr = ipAddr;
                filter.EmployeeUserId = employeeUserId;

                SecurityEventLogHelper.DownloadSecurityEventLogs(filter);                
            }

            if (!Page.IsPostBack)
            {
                RegisterJsScript("jquery.tmpl.js");
                RegisterService("securityEventLog", "/los/Reports/SecurityEventLogService.aspx");

                Tools.Bind_SecurityEventType(securityEventTypeDdl);                

                RegisterJsGlobalVariables("ResultsPerPage", SecurityEventLogHelper.SearchResultsPerPage);
                RegisterJsGlobalVariables("DefaultFromDate", DateTime.Now.AddDays(-2).ToString("MM/dd/yyyy HH:mm:ss"));
                RegisterJsGlobalVariables("DefaultToDate", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
            }            
        }
    }
}