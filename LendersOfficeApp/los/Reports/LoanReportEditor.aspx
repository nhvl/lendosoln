<%@ Page Language="c#" CodeBehind="LoanReportEditor.aspx.cs" AutoEventWireup="false" EnableEventValidation="true" Inherits="LendersOffice.LoanReportEditor" %>
<%@ Register TagPrefix="UCR" TagName="ReportLabelUserControl" Src="../QueryProcessor/ReportLabelUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="CommentEditUserControl" Src="../QueryProcessor/CommentEditUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="FieldFnGridUserControl" Src="../QueryProcessor/FieldFnGridUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="FieldLayoutUserControl" Src="../QueryProcessor/FieldLayoutUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="FieldSorterUserControl" Src="../QueryProcessor/FieldSorterUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="FieldGroupsUserControl" Src="../QueryProcessor/FieldGroupsUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="FieldOrdersUserControl" Src="../QueryProcessor/FieldOrdersUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="RelatesTreeUserControl" Src="../QueryProcessor/RelatesTreeUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="FilterRangeUserControl" Src="../QueryProcessor/FilterRangeUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="FilterClassUserControl" Src="../QueryProcessor/FilterClassUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="OptionEntryUserControl" Src="../QueryProcessor/OptionEntryUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="SearchListUserControl" Src="../QueryProcessor/SearchListUserControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LoanReportEditor</title>

    <script type="text/javascript">
		var gReportEventHandlers = new Array();
		var gReportEventProgress = new Array();
		var gReportEventInNested = 0;

        function f_updateRunButton(){
            try{
                var runBtn = document.getElementById('m_RunReport');
                var runMsg = document.getElementById('m_RunReportMsg');
                var g_CondRestrictedFields = document.getElementById('g_CondRestrictedFields');
                var isDisabled = false;

                if (!runBtn)
                    return;

                if (typeof(g_SearchRestrictedFields) != 'undefined'){
                     isDisabled |= (g_SearchRestrictedFields > 0);
                }

                if (typeof(g_CondRestrictedFields) && g_CondRestrictedFields != null && typeof(g_CondRestrictedFields.value) != 'undefined'){
                    isDisabled |= (g_CondRestrictedFields.value == 'true');
                }

                runBtn.disabled = isDisabled;
                if (runMsg)
                    runMsg.style.display = isDisabled ? '' : 'none';
            }catch(e){ window.status = e.message; }
        }

        function stringSplitRemainder(stringToSplit, delimiter, max) {
            if (max === 0) {
                return [];
            }

            var splitString = stringToSplit.split(delimiter);
            var arrayToReturn = splitString.slice();
            if (max >= splitString.length) {
                return arrayToReturn;
            }
            else {
                arrayToReturn.splice(max, splitString.length - max);
            }

            for (i = max; i < splitString.length; i++) {
                arrayToReturn[max - 1] += (delimiter + splitString[i]);
            }

            return arrayToReturn;
        }

		function onInit() { try
		{
		    resize(840, 650);

<%--
			// Check if we have defined a report and cached it.  If so, then
			// launch the report in its own window.
--%>
			if( document.getElementById("m_ReportCache") != null )
			{
				window.open( "ReportResult.aspx?id=" + document.getElementById("m_ReportCache").value, "_blank" , "resizable=yes,status=no,menubar=yes,scrollbars=yes,width=780,height=500" );
			}
<%--
			// Notify listeners that we have loaded.  Any
			// control that wants to process gui updates
			// after the page has completely loaded should
			// register an onload callback.
--%>
			fireReportEvent( "onload" , "loader" , null );
			registerForReportEvent( null , null , f_updateRunButton , "remove" );
			// Initialize panel display upon return from postback, or if
			// for the first time.

			switch( document.getElementById("m_Panel").value )
			{
				case "m_BaseA": showPanel( m_BaseA );
				break;

				case "m_BaseB": showPanel( m_BaseB );
				break;

				case "m_BaseC": showPanel( m_BaseC );
				break;

				case "m_BaseD": showPanel( m_BaseD );
				break;

				default:
				{
					showPanel( m_BaseA );
				}
				break;
			}

			// Check if we're copying a query.  If so, it must
			// be dirty or the flag variable wouldn't say copying.
			// We first check if we're saving the query.

			if( document.getElementById("m_Phase").value == "Storing" )
			{
                PolyConfirmSave(function(confirmed){
                    document.getElementById("m_Phase").value = "Copying";

                    if( confirmed){
                        document.getElementById("m_SaveQuery").click();                        
                    }
                },true, function(){
                    document.getElementById("m_Phase").value = "";
                });
			}

			if( document.getElementById("m_Phase").value == "Copying" )
			{
				document.getElementById("m_CopyQuery").click();

				return;
			}

			// Handle error message display.

			if( document.getElementById("m_ErrorMessage") )
			{
				displayError( document.getElementById("m_ErrorMessage").value );

				return;
			}

			// Handle feedback message open.

			if( document.getElementById("m_Feedback") )
			{
				displayInfo( document.getElementById("m_Feedback").value );

				return;
			}

			//Show msg if run button is disabled
			try{
			    var runBtn = document.getElementById('m_RunReport');
                var runMsg = document.getElementById('m_RunReportMsg');

                if (!runBtn || !runMsg)
                    return;

                runMsg.style.display = runBtn.disabled ? '' : 'none';
			}catch(e){}
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function onBeforeUnload() { try
		{
			// Turn on the animated gif.

			window.setTimeout( "m_Wait.className = 'Visible';" , 500 );
		}
		catch( e )
		{
			window.status = e.message;
		}}

		var gLockEditorPage = false;
		var gLockEditorNote = "";

		function lockEditorPage( bLocked , lockMsg ) { try
		{
			gLockEditorPage = bLocked;
			gLockEditorNote = lockMsg;
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function registerForReportEvent( oContainer , oStore , fHandler , sEvent ) { try
		{
			// Add a new entry in the function table.  This
			// entry will get pinged when other controls
			// invoke the fire report event method with a
			// specific event code.

			var entry = new Object();

			entry.cont = oContainer;
			entry.stor = oStore;
			entry.func = fHandler;
			entry.code = sEvent;

			gReportEventHandlers[ gReportEventHandlers.length ] = entry;
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function fireReportEvent( sEvent , sArgument , oContainer ) { try
		{
<%--
			// Walk the set.  We avoid recursion by detecting
			// whether we are currently firing an event or not.
			// While we are checking for an existing event in
			// progress, we don't worry about other calls to
			// fire an event coming in.  Once we start firing
			// the ripple events, other requests to fire new
			// events may come in.
--%>
			var i , j , n , entry = new Object();

			entry.cont = oContainer;
			entry.code = sEvent;

			gReportEventProgress[ gReportEventProgress.length ] = entry;

			++gReportEventInNested;

			for( i = 0 ; i < gReportEventHandlers.length ; ++i )
			{
				var handler = gReportEventHandlers[ i ];

				if( handler == null || handler.func == null )
				{
					continue;
				}

				if( handler.code == sEvent && handler.cont != oContainer )
				{
					for( j = 0 , n = gReportEventProgress.length ; j < n ; ++j )
					{
						var current = gReportEventProgress[ j ];

						if( current != null && handler.cont == current.cont && handler.code == current.code )
						{
							break;
						}
					}

					if( j != n )
					{
						continue;
					}

					handler.func( handler.cont , handler.stor , sArgument );
				}
			}

			if( gReportEventInNested == 1 )
			{
				gReportEventProgress.splice( 0 , gReportEventProgress.length );
			}

			--gReportEventInNested;
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function sendReportEvent( sEvent , sArgument , oContainer ) { try
		{
<%--
			// Find matching container as registered for the
			// given event and fire directly to it.  If multiple
			// entries exist for the given container, we fire
			// them all.
--%>
			var i;

			for( i = 0 ; i < gReportEventHandlers.length ; ++i )
			{
				// Get the report handler and check it out.

				var handler = gReportEventHandlers[ i ];

				if( handler == null || handler.func == null )
				{
					continue;
				}

				if( handler.code == sEvent && handler.cont == oContainer )
				{
					handler.func( handler.cont , handler.stor , sArgument );
				}
			}
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function displayError( eMsg ) { try
		{
			// Display error notification.

			if( eMsg != null )
			{
				alert( eMsg );
			}
		}
		catch( e )
		{
		}}

		function displayQuery( qMsg ) { try
		{
			// Display error notification.

			if( qMsg != null )
			{
				if( confirm( qMsg ))
				{
					return true;
				}
			}

			return false;
		}
		catch( e )
		{
		}}

		function displayInfo( iMsg ) { try
		{
			// Display error notification.

			if( iMsg != null )
			{
				alert( iMsg );
			}
		}
		catch( e )
		{
		}}

		function f_showHelp(event){
		    Modal.ShowPopup('LegendHelpDiv', null, event);
		    return false;
		}
    </script>

    <style type="text/css">
        .divPopup
        {
            display: none;
            border: black 3px solid;
            padding: 5px;
            position: absolute;
            width: 420px;
            height: 120px;
            font-weight: normal;
            background-color: whitesmoke;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
        }
        .nav
        {
            width: 100%;
            text-align: center;
            margin-top: 10px;
        }
        #m_RunReportMsg
        {
            padding-left: 5px;
            color: Red;
            position: relative;
            top: -3px;
        }
        #PrintGroupsLabel
        {
            position:relative;
            left: 1px;
        }
        #PrintGroupsLabel span
        {
            position: relative;
            bottom: 1px;
        }
        .PageHolder { display: inline-block; min-width:780px; width: auto; height: 553px; }
        .Border { border: 2px solid gray; }
        .none-border { border: none; }
        .position-relative { position: relative; }
        .w-662 { width: 662px; }
        .w-454 { width: 454px; }
        .w-396 { width: 396px; }
        .w-296 { width: 296px; }
        .w-224 { width: 224px; }
        #m_BaseA { width: 770px; }
        #m_BaseB { width: 774px; }
        #m_BaseC { width: 778px; }
        #m_BaseD { width: 1140px; height: 450px; }
        #m_BaseD .Bordered .left { position: absolute; left: 4px; top: 4px; width: 450px; }
        #m_BaseD .Bordered .right { position: absolute; left: 470px; top: 4px; width: 650px; background-color:gainsboro; }
        .m-root {
            MARGIN: 4px; BORDER: 1px groove; BACKGROUND-COLOR: white; OVERFLOW-Y: scroll; OVERFLOW-X: hidden;
        }
        .m-root[disabled] a {
            color: gray !important;
            cursor: default !important;
        }
    </style>
</head>
<body ms_positioning="FlowLayout" class="BasePage" onload="onInit();" onbeforeunload="onBeforeUnload();">
    <h4 class="page-header" id="page-header">Report Editor</h4>
    <div id="LegendHelpDiv" style="z-index: 900; width: 350px; height: 90px;" class="divPopup">
        (C), (F), (L), or (I) after a field name indicates that the field is restricted.<br />
        Corresponding permissions are required to access these fields and any custom reports
        which contain these fields.<br />
        Permissions can be granted via the Employee Edit page.<br />
        <div class="nav">
            [<a href="javascript:void(0);" onclick="Modal.Hide(); return false;">close</a>]
        </div>
    </div>
    <form id="LoanReportEditor" method="post" runat="server" style="width:100%;">
    <%
        //
        // Current panel state.
        //
    %>
    <input id="m_Panel" type="hidden" runat="server" name="m_Panel" />
    <input id="m_Dirty" type="hidden" runat="server" name="m_Dirty" />
    <input id="m_Phase" type="hidden" runat="server" name="m_Phase" />
    <input id="m_State" type="hidden" runat="server" name="m_State" />
    <input id="m_Emode" type="hidden" runat="server" name="m_Emode" />

    <script>
        function showPanel(oPanel) {
            try {
                // Validate if we are locked in the editor or can switch
                // panels freely.  We use this lock as a loose modal-
                // mechanism.

                if (oPanel == null || oPanel.id == null) {
                    return;
                }

                if (gLockEditorPage == true) {
                    if (gLockEditorNote != null && gLockEditorNote != "") {
                        alert(gLockEditorNote);
                    }

                    return;
                }

                // Walk the panels and turn off all toggleable
                // display panels.

                m_BaseA.className = m_BaseA.className.replace(/Visible/i, "Hidden");
                m_BaseB.className = m_BaseB.className.replace(/Visible/i, "Hidden");
                m_BaseC.className = m_BaseC.className.replace(/Visible/i, "Hidden");
                m_BaseD.className = m_BaseD.className.replace(/Visible/i, "Hidden");

                // We finish by showing the panel.

                var cn = oPanel.className.replace(/Hidden/i, "Visible");

                var onshow = oPanel.getAttribute("onshow");
                if (!!onshow) eval(onshow);

                document.getElementById("m_Panel").value = oPanel.id;

                oPanel.className = cn;

                //Set min width for main UI
                var minWidth = (oPanel.id=="m_BaseD")? 1140 : 780;

                document.getElementById("m_Panels").style.minWidth = minWidth + "px";
                //m_Panels has margin left right: 8px. set min width to make header fill content width.
                document.getElementById("page-header").style.minWidth = minWidth + 16 + "px";
            }
            catch (e) {
                window.status = e.message;
            }
        }

        function closePanel() {
            try {
                // Validate if we are locked in the editor or can switch
                // panels freely.  We use this lock as a loose modal-
                // mechanism.

                if (gLockEditorPage == true) {
                    if (gLockEditorNote != null && gLockEditorNote != "") {
                        alert(gLockEditorNote);
                    }

                    return;
                }

                // Go back to the front page.  If at front page, then
                // close the window.  We should have an unload handler
                // to check if we should save first.

                if (document.getElementById("m_Panel").value != "" && document.getElementById("m_Panel").value != m_BaseA.id) {
                    showPanel(m_BaseA);
                }
                else {
                    // Check dirty bit and ask
                    // to save if dirty.
                    var isPageDirty = document.getElementById("m_Dirty").value == "Dirty" && document.getElementById("m_Emode").value == "Edit";

                    var noSaveHandler = function(){
                        window.location = VRoot + "/los/reports/" + <%= AspxTools.JsString(GetReturnUrl()) %>;
                    }

                    if (!isPageDirty){
                        noSaveHandler();
                        return;
                    }
                    
                    PolyConfirmSave(function(confirmed){
                        if (confirmed){
                            document.getElementById("m_Phase").value = "Closing";
                            document.getElementById("m_SaveQuery").click();
                            return;
                        } 

                        noSaveHandler();
                    }, true);
                }
            }
            catch (e) {
                window.status = e.message;
            }
        }

        function markReportAsDirty() {
            try {
                // Find the dirty bit container and set it.

                if (document.getElementById("m_Dirty") != null) {
                    document.getElementById("m_Dirty").value = "Dirty";
                }

                // Uncomment to track bit tagging.

                // dumpCallStack();
            }
            catch (e) {
                window.status = e.message;
            }
        }

        function dumpCallStack() {
            try {
                // Walk the list and dump names as we go.

                var stack = "", f = arguments.callee.caller;

                while (f != null) {
                    var fstr = f.toString();

                    if (stack != "") {
                        stack += "\n";
                    }

                    stack += fstr.substr(0, fstr.indexOf("("));

                    f = f.caller;
                }

                alert("[ Call stack ]\n" + stack);
            }
            catch (e) {
                window.status = e.message;
            }
        }
    </script>

    
    <div class="PageHolder Border" id="m_Panels">
        <%
            //
            // Provide toplevel links panel that overlays on top of
            // underlying panels.  These links provide navigation
            // through the three main panels.
            //
        %>
        <div class="BasePanel" id="m_Tasks" show="always">
            <div class="Bordered Separated" style="position: relative; border-top: none; border-left: none; border-right: none; height: 516px;">
                <span style="position: absolute; bottom: 2px; float: left;">Restricted Fields:&nbsp;&nbsp;
                    (C) Closer&nbsp;&nbsp;&nbsp; (F) Finance&nbsp;&nbsp;&nbsp; (L) Lock Desk&nbsp;&nbsp;&nbsp; (I) Investor Info
                    <a href="javascript:void(0);" style="font-weight: bold;" onclick="javascript:f_showHelp(event); return false;">
                        ?</a></span>
            </div>
            <div class="Separated" style="width: 100%; padding: 2px;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <span>
                                <asp:Button ID="m_SaveQuery" runat="server" CssClass="CmdButton" OnClick="SaveQuery"
                                    Text="Save"></asp:Button>
                            </span><span>
                                <input id="m_Close" type="button" class="CmdButton" value="Close" onclick="closePanel();">
                            </span><span id="m_Review" class="Inlined">
                                <input type="button" class="CmdButton" style="width: 120px;" value="Set range & run..."
                                    onclick="showPanel( m_BaseD );">
                            </span><span id="m_Run" class="Hidden">
                                <asp:Button ID="m_RunReport" runat="server" CssClass="CmdButton" Style="width: 120px;"
                                    OnClick="RunReport" Text="Generate Report"></asp:Button>
                                <span id="m_RunReportMsg">This report contains restricted fields.</span> </span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%
            //
            // Review and layout panel.  This panel provides an
            // interface for restructuring report layout and
            // reviewing the conditions.
            //
        %>
        <div class="BasePanel Hidden position-relative" id="m_BaseA" onshow="m_Review.className = 'Inlined'; m_Run.className = 'Hidden'; document.getElementById('m_Close').value = 'Close';">
            <div class="Bordered Separated" style="position: relative; border-top: none; border-left: none;  border-right: none; height: 441px;">
                <div style="position: absolute; left: 0px; top: 4px; width: 458px;">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="ControlTitle">
                        <tr>
                            <td>
                                Report details
                            </td>
                        </tr>
                    </table>
                    <UCR:ReportLabelUserControl ID="m_ReportLabel" runat="server" CssClass="PanelControl Bordered Separated w-454" Entry="406px" Height="30px"></UCR:ReportLabelUserControl>
                </div>
                <div style="position: absolute; left: 0px; top: 70px; width: 458px;">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="ControlTitle">
                        <tr>
                            <td>
                                Report fields
                            </td>
                            <td align="right">
                                <input type="button" class="ControlTitleLink" value="Add / Remove Fields" onclick="showPanel( m_BaseB );">
                            </td>
                        </tr>
                    </table>
                    <UCR:FieldFnGridUserControl ID="m_FieldFnGrid" runat="server" CssClass="PanelControl Bordered Separated w-454"  Height="172px"></UCR:FieldFnGridUserControl>
                </div>
                <table cellpadding="0" cellspacing="0" border="0" style="position: absolute; left: 0px; top: 306px; width: 458px;">
                    <tr valign="top">
                        <td style="width: 228px;">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="ControlTitle">
                                <tr>
                                    <td>
                                        Sorting order
                                    </td>
                                </tr>
                            </table>
                            <UCR:FieldSorterUserControl ID="m_FieldSorter" runat="server" CssClass="PanelControl Bordered Separated w-224" Height="88px"></UCR:FieldSorterUserControl>
                        </td>
                        <td style="width: 4px;">
                        </td>
                        <td style="width: 228px;">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="ControlTitle">
                                <tr>
                                    <td>
                                        Grouping order
                                    </td>
                                    <td align="right">
                                        <span style="position: relative; top: -2px;">(show unit totals) </span>
                                        <asp:CheckBox ID="m_UseUnitCols" runat="server" TextAlign="Left"></asp:CheckBox>
                                    </td>
                                </tr>
                            </table>
                            <UCR:FieldGroupsUserControl ID="m_FieldGroups" runat="server" CssClass="PanelControl Bordered Separated w-224" Height="70px"></UCR:FieldGroupsUserControl>
                            <label id="PrintGroupsLabel"><asp:CheckBox ID="PrintGroupsOnSeparatePages" runat="server"/><span>Print groups on separate pages</span></label>
                        </td>
                    </tr>
                </table>
                <div style="position: absolute; left: 470px; top: 4px; width: 300px;">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="ControlTitle">
                        <tr>
                            <td>
                                Conditions
                            </td>
                            <td align="right">
                                <input type="button" class="ControlTitleLink" value="Add / Edit / Remove Conditions" style="height:20px"
                                    onclick="showPanel( m_BaseC );">
                            </td>
                        </tr>
                    </table>
                    <UCR:RelatesTreeUserControl ID="m_CondViewing" runat="server" CssClass="PanelControl Bordered Separated w-296" ReadOnly="True" Height="298px"></UCR:RelatesTreeUserControl>
                </div>
                <div style="position: absolute; left: 470px; top: 345px; width: 300px;">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="ControlTitle">
                        <tr>
                            <td>
                                Report summary
                            </td>
                        </tr>
                    </table>
                    <UCR:CommentEditUserControl ID="m_CommentEdit" runat="server" CssClass="PanelControl Bordered Separated w-296" Entry="100%" Height="58px"></UCR:CommentEditUserControl>
                </div>
            </div>
        </div>
        <%
            //
            // Compose report view.  Add new fields to the report
            // and design their ordering.
            //
        %>
        <div class="BasePanel Hidden position-relative" id="m_BaseB" onshow="m_Review.className = 'Inlined'; m_Run.className = 'Hidden'; document.getElementById('m_Close').value = 'Back';">
            <div class="Bordered Separated" style="position: relative; border-top: none; border-left: none; border-right: none; height: 436px;">
                <div style="position: absolute; left: 0px; top: 4px; width: 358px;">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="ControlTitle">
                        <tr>
                            <td>
                                Fields to include in the report
                            </td>
                        </tr>
                    </table>
                    <UCR:SearchListUserControl ID="m_SearchIncld" runat="server" CssClass="PanelControl Bordered Separated"
                        HideReportFields="True" SelectionLabel="include" Height="396px" Width="358px">
                    </UCR:SearchListUserControl>
                </div>
                <div style="position: absolute; left: 374px; top: 4px; width: 400px;">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="ControlTitle">
                        <tr>
                            <td>
                                Report fields
                            </td>
                        </tr>
                    </table>
                    <UCR:FieldLayoutUserControl ID="m_FieldLayout" runat="server" CssClass="PanelControl Bordered Separated w-396" Height="396px"></UCR:FieldLayoutUserControl>
                </div>
            </div>
        </div>
        <%
            //
            // Provide interface for editing a report's conditions.
            //
        %>
        <div class="BasePanel Hidden position-relative" id="m_BaseC" onshow="m_Review.className = 'Inlined'; m_Run.className = 'Hidden'; document.getElementById('m_Close').value = 'Back';">
            <div class="Bordered Separated" style="position: relative; border-top: none; border-left: none; border-right: none; height: 443px;">
                <div style="position: absolute; left: 0px; top: 4px; width: 458px;">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="ControlTitle">
                        <tr>
                            <td>
                                Conditions
                            </td>
                        </tr>
                    </table>
                    <UCR:RelatesTreeUserControl ID="m_CondSetting" runat="server" CssClass="PanelControl Bordered Separated"
                        Height="396px" Width="100%"></UCR:RelatesTreeUserControl>
                </div>
                <div style="position: absolute; left: 474px; top: 4px; width: 300px;">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="ControlTitle">
                        <tr>
                            <td>
                                Select condition parameters
                            </td>
                        </tr>
                    </table>
                    <UCR:OptionEntryUserControl ID="m_OptionEntry" runat="server" CssClass="PanelControl Bordered Separated"
                        SelectionLabel="select" Height="396px"></UCR:OptionEntryUserControl>
                </div>
            </div>
        </div>
        <%
            //
            // Allow user to specify final filter and generate
            // a report.  They can save thier design here too.
            //
        %>
        <div class="BasePanel Hidden position-relative" id="m_BaseD" onshow="m_Review.className = 'Hidden'; m_Run.className = 'Inlined'; document.getElementById('m_Close').value = 'Back';">
            <div class="Bordered Separated" style="position: relative; border-top: none; border-left: none; border-right: none; height: 495px;">
                <div class="left">
                    <table cellpadding="0" cellspacing="0" border="0" class="ControlTitle w-454">
                        <tr>
                            <td>
                                Status event range
                            </td>
                        </tr>
                    </table>
                    <UCR:FilterRangeUserControl ID="m_FilterRange" runat="server" CssClass="PanelControl Bordered Separated" Height="434px" Entry="160px" Width="100%">
                        <fields>
                                    <Item Label="Lead New" Key="sLeadD"></Item>
									<Item Label="Loan Open" Key="sOpenedD"></Item>
									<Item Label="Pre-Qual" Key="sPreQualD"></Item>
									<Item Label="Registered" Key="sSubmitD"></Item>
									<Item Label="Pre-Processing" Key="sPreProcessingD"></Item>
									<Item Label="Processing" Key="sProcessingD"></Item>
									<Item Label="Document Check" Key="sDocumentCheckD"></Item>
									<Item Label="Document Check Failed" Key="sDocumentCheckFailedD"></Item>
									<Item Label="Submitted" Key="sLoanSubmittedD"></Item>
									<Item Label="Pre-Underwriting" Key="sPreUnderwritingD"></Item>
									<Item Label="In Underwriting" Key="sUnderwritingD"></Item>
									<Item Label="Pre-Approved" Key="sPreApprovD"></Item>
									<Item Label="Approved" Key="sApprovD"></Item>
									<Item Label="Condition Review" Key="sConditionReviewD"></Item>
									<Item Label="Final Underwriting" Key="sFinalUnderwritingD"></Item>
									<Item Label="Pre-Doc QC" Key="sPreDocQCD"></Item>
									<Item Label="Clear to Close" Key="sClearToCloseD"></Item>
									<Item Label="Docs Ordered" Key="sDocsOrderedD"></Item>
									<Item Label="Docs Drawn" Key="sDocsDrawnD"></Item>
									<Item Label="Docs Out" Key="sDocsD"></Item>
									<Item Label="Docs Back" Key="sDocsBackD"></Item>
									<Item Label="Funding Conditions" Key="sFundingConditionsD"></Item>
									<Item Label="Funded" Key="sFundD"></Item>
									<Item Label="Recorded" Key="sRecordedD"></Item>
									<Item Label="Final Docs" Key="sFinalDocsD"></Item>
									<Item Label="Loan Closed" Key="sClosedD"></Item>
									<Item Label="Submitted For Purchase Review" Key="sSubmittedForPurchaseReviewD"></Item>
                                    <Item Label="In Purchase Review" Key="sInPurchaseReviewD"></Item>
                                    <Item Label="Pre-Purchase Conditions" Key="sPrePurchaseConditionsD"></Item>
                                    <Item Label="Submitted For Final Purchase Review" Key="sSubmittedForFinalPurchaseD"></Item>
                                    <Item Label="In Final Purchase Review" Key="sInFinalPurchaseReviewD"></Item>
                                    <Item Label="Clear to Purchase" Key="sClearToPurchaseD"></Item>
                                    <Item Label="Loan Purchased" Key="sPurchasedD"></Item>
									<Item Label="Ready For Sale" Key="sReadyForSaleD"></Item>
									<Item Label="Loan Shipped" Key="sShippedToInvestorD"></Item>
									<Item Label="Investor Conditions" Key = "sSuspendedByInvestorD"></Item>
									<Item Label="Investor Conditions Sent" Key = "sCondSentToInvestorD"></Item>
									<Item Label="Loan Sold" Key="sLPurchaseD"></Item>
                                    <Item Label="Counter Offer Approved" Key="sCounterOfferD"></Item>
                                    <Item Label="Loan Suspended" Key="sSuspendedD"></Item>
									<Item Label="Loan On-Hold" Key="sOnHoldD"></Item>
									<Item Label="Loan Canceled" Key="sCanceledD"></Item>
									<Item Label="Loan Denied" Key="sRejectD"></Item>
									<Item Label="Loan Withdrawn" Key="sWithdrawnD"></Item>
									<Item Label="Loan Archived" Key="sArchivedD"></Item>
									<Item Label="Lead Canceled" Key="sLeadCanceledD"></Item>
									<Item Label="Lead Declined" Key="sLeadDeclinedD"></Item>
                                    <Item Label="Lead Other" Key="sLeadOtherD"></Item>
					    </fields>
                        <macros for="Start">
									<Item Label="Today" Key="cToday"></Item>
									<Item Label="Yesterday" Key="cYesterday"></Item>
									<Item Label="This Monday" Key="cThisMonday"></Item>
									<Item Label="Last Monday" Key="cLastMonday"></Item>
									<Item Label="Next Monday" Key="cNextMonday"></Item>
									<Item Label="Beginning of This Month" Key="cThisMonthBeginning"></Item>
									<Item Label="Beginning of Last Month" Key="cLastMonthBeginning"></Item>
									<Item Label="Beginning of Next Month" Key="cNextMonthBeginning"></Item>
									<Item Label="Start of Current Cal. Quarter" Key="cCurrentQuarterBeginning"></Item>
									<Item Label="Start of Last Cal. Quarter" Key="cLastQuarterBeginning"></Item>
									<Item Label="Beginning of This Year" Key="cThisYearBeginning"></Item>
									<Item Label="Beginning of Last Year" Key="cLastYearBeginning"></Item>
								</macros>
                        <macros for="End">
									<Item Label="Today" Key="cToday"></Item>
									<Item Label="Yesterday" Key="cYesterday"></Item>
									<Item Label="This Friday" Key="cThisFriday"></Item>
									<Item Label="Last Friday" Key="cLastFriday"></Item>
									<Item Label="Next Friday" Key="cNextFriday"></Item>
									<Item Label="End of This Month" Key="cThisMonthEnd"></Item>
									<Item Label="End of Last Month" Key="cLastMonthEnd"></Item>
									<Item Label="End of Next Month" Key="cNextMonthEnd"></Item>
									<Item Label="End of Current Cal. Quarter" Key="cCurrentQuarterEnd"></Item>
									<Item Label="End of Last Cal. Quarter" Key="cLastQuarterEnd"></Item>
									<Item Label="End of This Year" Key="cThisYearEnd"></Item>
									<Item Label="End of Last Year" Key="cLastYearEnd"></Item>
								</macros>
                    </UCR:FilterRangeUserControl>
                </div>
                <div class="right">
                    <table cellpadding="0" cellspacing="0" border="0" class="ControlTitle w-662">
                        <tr>
                            <td>
                                Include only files with selected statuses
                            </td>
                        </tr>
                    </table>
                    <div class="PanelControl Bordered Separated" style="padding: 4px; width: 100%;">
                        <div style="padding-bottom: 0;">
                            <UCR:FilterClassUserControl ID="m_FilterActiv" runat="server" GroupLabel="Loan Active Statuses"
                                FilterId="sStatusT">
                                <item label="Loan Open" argument="0"></item>
                                <item label="Pre-qual" argument="1"></item>
                                <item label="Registered" argument="3"></item>
                                <item label="Pre-Processing" argument="29"></item>
                                <item label="Processing" argument="22"></item>
                                <item label="Document Check" argument="30"></item>
                                <item label="Document Check Failed" argument="31"></item>
                                <item label="Submitted" argument="28"></item>
                                <item label="Pre-Underwriting" argument="32"></item>
                                <item label="In Underwriting" argument="13"></item>
                                <item label="Pre-Approved" argument="2"></item>
                                <item label="Approved" argument="4"></item>
                                <item label="Condition Review" argument="33"></item>
                                <item label="Final Underwriting" argument="23"></item>
                                <item label="Pre-Doc QC" argument="34"></item>
                                <item label="Clear To Close" argument="21"></item>
                                <item label="Docs Ordered" argument="35"></item>
                                <item label="Docs Drawn" argument="36"></item>
                                <item label="Docs Out" argument="5"></item>
                                <item label="Docs Back" argument="24"></item>
                                <item label="Funding Conditions" argument="25"></item>
                                <item label="Funded" argument="6"></item>
                                <item label="Recorded" argument="19"></item>
                                <item label="Final Docs" argument="26"></item>
                                <item label="Submitted For Purchase Review" argument="40"></item>
                                <item label="In Purchase Review" argument="41"></item>
                                <item label="Pre-Purchase Conditions" argument="42"></item>
                                <item label="Submitted For Final Purchase Review" argument="43"></item>
                                <item label="In Final Purchase Review" argument="44"></item>
                                <item label="Clear To Purchase" argument="45"></item>
                                <item label="Loan Purchased" argument="46"></item>
                                <item label="Ready For Sale" argument="39"></item>
                                <item label="Loan Shipped" argument="20"></item>
                                <item label="Investor Conditions" argument="37"></item>
                                <item label="Investor Conditions Sent" argument="38"></item>
                                <item label="Loan Sold" argument="27"></item>

                                <item label="Loan On-Hold" argument="7"></item>
                                <item label="Loan Other" argument="18"></item>
                            </UCR:FilterClassUserControl>
                        </div>
                        <div style="padding-bottom: 0;">
                            <UCR:FilterClassUserControl ID="m_FilterInact" runat="server" GroupLabel="Loan Inactive Statuses"
                                FilterId="sStatusT">
                                <item label="Counter Offer Approved" argument="47"></item>
                                <item label="Loan Denied" argument="10"></item>
                                <item label="Loan Canceled" argument="9"></item>
                                <item label="Loan Closed" argument="11"></item>
                                <item label="Loan Suspended" argument="8"></item>
                                <item label="Loan Withdrawn" argument="48"></item>
                                <item label="Loan Archived" argument ="49"></item>
                            </UCR:FilterClassUserControl>
                        </div>
                        <div style="padding-bottom: 0;">
                            <UCR:FilterClassUserControl ID="m_FilterOther" runat="server" GroupLabel="Lead Statuses"
                                FilterId="sStatusT">
                                <item label="Lead New" argument="12"></item>
                                <item label="Lead Canceled" argument="15"></item>
                                <item label="Lead Declined" argument="16"></item>
                                <%--<item label="Loan Other" argument="18"></item>--%>
                                <item label="Lead Other" argument="17"></item>
                            </UCR:FilterClassUserControl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%
            //
            // Provide toplevel links panel that overlays on top of
            // underlying panels.  These links provide navigation
            // through the three main panels.
            //
        %>
        <div class="BasePanel">
            <div id="m_Wait" class="Hidden" style="position: absolute; top: 220px; left: 80px;
                padding: 0.3in; background: whitesmoke; border: 2px solid gainsboro;" onclick="this.className='Hidden';">
                <div style="width: 3in; padding: 16px; background-color: white; border: 2px solid gainsboro;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="Waiting.gif">
                            </td>
                            <td>
                                <span style="font: bold 11px arial;">Processing request. Please wait... </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>

    <uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>
    <!--[if lte IE 6.5]><iframe id="selectmask" style="display:none; position:absolute;top:0px;left:0px;filter:mask()"  src="javascript:void(0)" ></iframe><![endif]-->
</body>
</html>
