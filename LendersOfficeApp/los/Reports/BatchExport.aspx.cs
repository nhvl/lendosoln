﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.XsltExportReport;
using LendersOffice.Admin;
using LendersOffice.Security;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice
{
    public partial class BatchExport : BasePage
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }
        private BrokerDB Broker
        {
            get { return BrokerDB.RetrieveById(BrokerUser.BrokerId); }
        }
        
        /// <summary>
        /// Gets the forced compatibility mode for the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode <see cref="E_XUAComaptibleValue.Edge"/>.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }
        
        protected void PageLoad(object sender, System.EventArgs e)
        {
            var reports = XsltMap.ListActive(Broker).OrderBy(o => o.Description);
            m_exportFormats.DataSource = reports;
            m_exportFormats.DataBind();
            m_noReportFormats.Visible = (reports.Count() == 0);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            EnableJqueryMigrate = false;
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion
    }
}
