﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchExportDownload.aspx.cs" Inherits="LendersOfficeApp.los.Reports.BatchExportDownload" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
 
</head>
<body bgcolor="gainsboro">
<script type="text/javascript">
  function f_download(reportId)
  {
    window.open("BatchExportDownload.aspx?cmd=download&reportid=" + reportId, "_parent");
    return false;
  }

  function f_retry(reportId)
  {
    var args = { ReportId: reportId };

    gService.main.call("retry", args)
    location.href = location.href;
    return false;
  }
</script>
    <form id="form1" runat="server">
    <div>
      <div class="FormTableHeader">
            Report Download
      </div>
      
      <div id="m_errorPanel" runat="server" style="font-color:red" visible="false">
      <ml:EncodedLiteral ID="m_errorMsg" runat="server" />
      </div>  
    <ml:CommonDataGrid ID="m_grid" runat="server" AutoGenerateColumns="false">
    <Columns>
    <asp:BoundColumn DataField="ReportId" HeaderText="Report Id" />
    <asp:BoundColumn DataField="ReportName" HeaderText="Report Name" />
    <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" />
    <asp:BoundColumn DataField="Status" HeaderText="Status" />
    <asp:TemplateColumn>
    <ItemTemplate>
    <%# AspxTools.HtmlControl(GenerateActionLink(Container.DataItem)) %>
    </ItemTemplate>
    </asp:TemplateColumn>
    </Columns>
    </ml:CommonDataGrid>
    </div>
    </form>
</body>
</html>
