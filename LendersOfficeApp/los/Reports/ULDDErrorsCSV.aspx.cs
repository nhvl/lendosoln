﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.QueryProcessor;
using DataAccess;
using LendersOffice.Conversions;
using System.Text;
using System.Reflection;

namespace LendersOfficeApp.los.Reports
{

    
    public partial class ULDDErrorsCSV : BasePage
    {
        private class PropertyInfoComparer : IEqualityComparer<PropertyInfo>
        {

            #region IEqualityComparer<PropertyInfo> Members

            public bool Equals(PropertyInfo x, PropertyInfo y)
            {
                return x.Name == y.Name;
            }

            public int GetHashCode(PropertyInfo obj)
            {
                return obj.Name.GetHashCode();
            }

            #endregion
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            var reportid = RequestHelper.GetSafeQueryString("reportid");
            if (reportid != null)
            {
                // Retrieve the report using the implicit string
                // conversion of the contained, cached object.

                Report rA = null;

                // 05/12/06 mf - OPM 2582 & 4668.  To reduce server memory usage
                // do not use server cache for custom reports.

                string result;
                if ((result = AutoExpiredTextCache.GetFromCache(reportid)) != null)
                {
                    rA = (Report)result;
                }
                if (rA == null)
                {
                    // Mark the error in the message container and
                    // skip attachment loading.

                    m_ExportError.Text = "Unable to load your report.  Reports expire after 30 minutes.  Please re-run your report.";

                    Tools.LogError("Report Export Error: Couldn't load ( " + reportid + " from the cache.");

                    return;
                }
                string ulddErrorMessage = string.Empty;

                List<Row> rowList = rA.Flatten();

                List<Guid> loanIdList = new List<Guid>();

                foreach (Row row in rowList)
                {
                    if (row.Key != Guid.Empty)
                    {
                        loanIdList.Add(row.Key);
                    }
                }
                if (loanIdList.Count == 0)
                {
                    ulddErrorMessage = "<ol><li>There are no loans in this report.</ol></li>";
                    return;
                }

                UlddExporter exporter = new UlddExporter(loanIdList, null);

                List<UlddExportError> errorList = null;
                var props = typeof(CPageData).GetProperties().Union(typeof(CAppData).GetProperties(), new PropertyInfoComparer()).ToDictionary((m) => m.Name);
                if (exporter.Verify(out errorList) == false)
                {
                    StringBuilder htmlErrorMessage = new StringBuilder();
                    var fieldIDSet = new HashSet<string>();
                    foreach (var loanErrors in errorList)
                    {
                        foreach (var error in loanErrors.ErrorList)
                        {
                            if (false == fieldIDSet.Contains(error.FieldId))
                            {
                                fieldIDSet.Add(error.FieldId);
                            }
                        }
                    }

                    var fieldIDs = fieldIDSet.ToArray();
                    htmlErrorMessage.Append("Loan ID,\t");
                    htmlErrorMessage.Append(string.Join(", ", fieldIDs));
                    htmlErrorMessage.Append(Environment.NewLine);
                    htmlErrorMessage.AppendLine("Type," + string.Join(",", (from f in fieldIDs
                                                                            select ((props.ContainsKey(f)) ? GetFriendlyTypeName(props[f].PropertyType) : "not found")).ToArray()));

                    foreach (var loanErrors in errorList)
                    {
                        htmlErrorMessage.Append(GetErrorsAsCSVRow(loanErrors, fieldIDs));
                    }

                    Response.Clear();
                    Response.ContentType = "text/comma-separated-values";
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"Report.csv\"");
                    Response.Write(htmlErrorMessage.ToString());
                    Response.Flush();
                    Response.End();
                }
            }
        }

        private string GetFriendlyTypeName(Type t)
        {
            if (t.IsEnum)
            {
                return t.Name + ": " + string.Join(";", Enum.GetNames(t));
            }
            else if (t == typeof(CDateTime))
            {
                return "Date/Time";
            }
            else
            {
                return t.Name;
            }
        }
        private const string requiredMsg = "<REQUIRED>";
        private string GetErrorsAsCSVRow(UlddExportError loanErrors, string[] fieldIDs)
        {
            var errors = loanErrors.sLId + ",\t";
            var erroneousFieldIDs = new HashSet<string>();
            foreach (var error in loanErrors.ErrorList)
            {
                erroneousFieldIDs.Add(error.FieldId);
            }
            foreach (var fieldID in fieldIDs)
            {
                if (erroneousFieldIDs.Contains(fieldID))
                {
                    errors += requiredMsg;
                }                
                errors += ",";
            }
            errors = errors.TrimEnd(',');
            return errors;
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
