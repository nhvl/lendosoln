﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.los.Reports
{
    public partial class PublishReport : LendersOffice.Common.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Guid queryid = RequestHelper.GetGuid("queryid");

                SqlParameter[] parameters = {
                                                new SqlParameter( "@QueryID" , queryid),
                                                new SqlParameter( "@BrokerID" , BrokerUser.BrokerId )
                                            };

                using( IDataReader sR = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "GetReportQuery" , parameters))
						{
							if( sR.Read())
							{                                
                                m_NamePublishedAs.Text =  sR["QueryName"].ToString();
                            }
                        }
            }
        }
        private BrokerUserPrincipal BrokerUser
        {
            // Access member.
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }
        protected void Publish(object sender, EventArgs e)
        {
            // Publish the currently selected report.

            try
            {
                if (m_NamePublishedAs.Text.TrimWhitespaceAndBOM() == "")
                {
                    throw new ArgumentException("Please specify a name to publish this report by.");
                }

                SqlParameter[] parameters = {
                                                 new SqlParameter("@BrokerId", BrokerUser.BrokerId)
                    , new SqlParameter("@NamePublishedAs", m_NamePublishedAs.Text.TrimWhitespaceAndBOM())
                    , new SqlParameter("@IsPublished", 1)
                    , new SqlParameter("@QueryId", RequestHelper.GetSafeQueryString("queryid"))
                                            };

                StoredProcedureHelper.ExecuteNonQuery(BrokerUser.BrokerId, "UpdateReportQuery", 3, parameters);

                string sScript = @"
                <script type='text/javascript'>//<!--
onClosePopup();//--></script>";

                Page.ClientScript.RegisterStartupScript(Page.GetType(), "close", sScript);

            }
            catch (ApplicationException ex)
            {
                Tools.LogError("Unable to publish report.", ex);
            }
        }       
    }
}
