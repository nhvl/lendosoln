﻿#region auto-generated code
namespace LendersOfficeApp.los.Reports
#endregion
{
    using System;
    using System.Data.Common;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Security event log service.
    /// </summary>
    public partial class SecurityEventLogService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Process method.
        /// </summary>
        /// <param name="methodName">Method name.</param>
        protected override void Process(string methodName)
        {
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowAccessingSecurityEventLogsPage))
            {
                throw new CBaseException("You do not have permission to access security event logs.", "You do not have permission to access security event logs.");
            }

            switch (methodName)
            {
                case "Search":
                    this.Search();
                    break;
                case "NowClick":
                    this.NowClick();
                    break;
                default:
                    throw CBaseException.GenericException($"Unhandled method [{methodName}]");
            }
        }                

        /// <summary>
        /// Update the "now" date.
        /// </summary>
        private void NowClick()
        {
            SetResult("nowDate", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
        }

        /// <summary>
        /// Search the logs.
        /// </summary>
        private void Search()
        {
            string filterDateStr = GetString("ToDate");
            string fromDateStr = GetString("FromDate");
            bool calculateToNow = GetBool("IsNow");
            string ipAddr = GetString("IPAddr");
            SecurityEventType eventType = GetEnum<SecurityEventType>("SecurityEventType");
            Guid employeeUserId = GetGuid("EmployeeUserId");
            int currentPage = GetInt("CurrentPage");

            DateTime? filterDate = null;
            DateTime? fromDate = null;

            if (calculateToNow)
            {
                filterDate = DateTime.MaxValue;
            }
            else
            {
                if (filterDateStr != string.Empty)
                {
                    filterDate = CDateTime.Create(filterDateStr, FormatTarget.Webform).DateTimeForComputationWithTime;
                }
            }

            if (fromDateStr != string.Empty)
            {
                fromDate = CDateTime.Create(fromDateStr, FormatTarget.Webform).DateTimeForComputationWithTime;
            }

            string[] splitIP = ipAddr.Split('.');

            if (ipAddr != string.Empty)
            {
                foreach (string s in splitIP)
                {
                    if (s != string.Empty)
                    {
                        int result;
                        if (!int.TryParse(s, out result))
                        {
                            throw new CBaseException("Invalid IP", "Invalid IP");
                        }

                        if (result < 0 || result > 255)
                        {
                            throw new CBaseException("Invalid IP", "Invalid IP");
                        }
                    }                    
                }
            }

            SecurityEventLogsFilter filter = new SecurityEventLogsFilter();
            filter.BrokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            filter.FromDate = fromDate;
            filter.FilterToDate = filterDate;
            filter.EventType = eventType;
            filter.IpAddr = ipAddr;
            filter.EmployeeUserId = employeeUserId;

            SecurityEventLogSearchResults searchResults = SecurityEventLogHelper.SearchSecurityEventLogs(filter, currentPage);

            SetResult("logs", SerializationHelper.JsonNetAnonymousSerialize(searchResults));
        }
    }
}