<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<%@ Page Language="c#" CodeBehind="LoanReports.aspx.cs" AutoEventWireup="false" Inherits="LendersOffice.LoanReports" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Custom Reports</title>
    <link href="../../css/stylesheetnew.css" type="text/css" rel="stylesheet">
    <style>
        .basepage
        {
            overflow-y: auto;
            background-color: #003366;
        }
        .pageholder
        {
            border-right: gray 2px solid;
            border-top: medium none;
            margin-bottom: 8px;
            margin-left: 8px;
            border-left: gray 2px solid;
            width: 774px;
            margin-right: 8px;
            border-bottom: gray 2px solid;
            position: relative;
            height: 460px;
            background-color: gainsboro;
        }
        .pagetabset
        {
            margin-top: 8px;
            margin-left: 8px;
            width: 100%;
            margin-right: 8px;
            border-bottom: medium none;
            position: relative;
            height: 21px;
            background-color: transparent;
        }
        .pagetab
        {
            border-right: medium none;
            padding-right: 3px;
            border-top: whitesmoke 1px solid;
            display: inline;
            padding-left: 3px;
            z-index: 1;
            padding-bottom: 4px;
            font: bold 11px arial;
            border-left: whitesmoke 2px solid;
            width: 160px;
            cursor: hand;
            color: whitesmoke;
            padding-top: 3px;
            border-bottom: medium none;
            position: absolute;
            background-color: gray;
            text-align: center;
        }
        .pagetabactive
        {
            border-right: gray 2px solid;
            padding-right: 3px;
            border-top: whitesmoke 1px solid;
            display: inline;
            padding-left: 3px;
            z-index: 2;
            padding-bottom: 4px;
            font: bold 11px arial;
            border-left: whitesmoke 2px solid;
            width: 160px;
            cursor: default;
            color: #003366;
            padding-top: 3px;
            border-bottom: medium none;
            position: absolute;
            background-color: gainsboro;
            text-align: center;
        }
        .basepanel
        {
            padding-right: 4px;
            padding-left: 4px;
            left: 0px;
            padding-bottom: 4px;
            font: 11px arial;
            padding-top: 4px;
            position: absolute;
            top: 0px;
            background-color: transparent;
            right:0px;
        }
        .cmdbutton
        {
            border-right: gainsboro 1px outset;
            padding-right: 1px;
            border-top: gainsboro 1px outset;
            padding-left: 1px;
            padding-bottom: 1px;
            font: 11px arial;
            border-left: gainsboro 1px outset;
            width: 80px;
            padding-top: 1px;
            border-bottom: gainsboro 1px outset;
            position: relative;
            text-align: center;
        }
        .paneltitle
        {
            border-right: medium none;
            padding-right: 2px;
            border-top: medium none;
            margin-top: 2px;
            padding-left: 8px;
            padding-bottom: 2px;
            font: bold 12px arial;
            border-left: gainsboro 2px solid;
            width: 160px;
            padding-top: 2px;
            border-bottom: medium none;
            position: relative;
        }
        .panelcontrol
        {
            width: 100%;
            position: relative;
        }
        .controltitle
        {
            padding-right: 4px;
            padding-left: 4px;
            margin-bottom: 4px;
            padding-bottom: 4px;
            font: bold 11px arial;
            width: 100%;
            color: white;
            padding-top: 4px;
            position: relative;
            height: 27px;
            background-color: #808080;
        }
        .controltitletext
        {
            font: bold 11px arial;
            color: white;
            text-align: left;
        }
        .lookuprowheader
        {
            font: bold 12px arial;
        }
        .lookuprowitem
        {
            padding-bottom: 5px;
            font: 12px arial;
            padding-top: 4px;
        }
        .lookuprowlink
        {
            padding-right: 4px;
            padding-bottom: 5px;
            font: 12px arial;
            padding-top: 4px;
            text-align: left;
        }
        .separated
        {
            margin-top: 4px;
        }
        .bordered
        {
            border-right: 2px groove;
            border-top: 2px groove;
            border-left: 2px groove;
            border-bottom: 2px groove;
        }
        .visible
        {
            display: block;
        }
        .hidden
        {
            display: none;
        }
        div.BasePanel img
        {
            vertical-align: bottom;
        }
        div.BasePanel .LookupRowHeader img
        {
            position: relative;
            bottom: 1px;
        }
        .page-header {
            min-width:780px;
        }
    </style>

    <script type="text/javascript">
        function onInit() {
            try {
                resize(815, 600);
                // Check if we have defined a report and cached it.  If so, then
                // launch the report in its own window.

                if (document.getElementById("m_ReportCache") != null) {
                    window.open("ReportResult.aspx" + "?" + "id" + "=" + document.getElementById("m_ReportCache").value, "_blank", "resizable=yes,status=no,menubar=yes,scrollbars=yes,width=780,height=500");
                }

                // Display the last tabbed page before the postback.

                if (document.getElementById("m_Last")) {
                    showPage(document.getElementById("m_Last").value);
                }

                // Handle error message display.

                if (document.getElementById("m_ErrorMessage")) {
                    displayError(document.getElementById("m_ErrorMessage").value);
                }

                // Handle feedback message open.

                if (document.getElementById("m_Feedback")) {
                    displayInfo(document.getElementById("m_Feedback").value);

                    return;
                }
            }
            catch (e) {
                window.status = e.message;
            }
        }

        function onBeforeUnload() {
            try {
                // Turn on the animated gif.

                window.setTimeout("m_Wait.className = 'Visible';", 500);
            }
            catch (e) {
                window.status = e.message;
            }
        }

        function displayError(eMsg) {
            try {
                // Display error notification.

                if (eMsg != null) {
                    alert(eMsg);
                }
            }
            catch (e) {
            }
        }

        function displayInfo(iMsg) {
            try {
                // Display error notification.

                if (iMsg != null) {
                    alert(iMsg);
                }
            }
            catch (e) {
            }
        }

        function PublishReport(item) {
            var queryid = item.getAttribute("queryid");
            var url = '/los/reports/PublishReport.aspx?queryid=' + queryid;
            var modalArg = '';
            showModal(url, modalArg, 'dialogWidth:410px;dialogHeight:250px;center:yes;resizable:yes;scroll:no;status=no;help=no;', null, function(){ window.location.reload(); });
        }
    </script>

</head>
<body ms_positioning="FlowLayout" class="BasePage" onload="onInit();" onbeforeunload="onBeforeUnload();">
    <%
        //
        // Page title and other decorations.
        //
    %>
    <h4 class="page-header">Custom Reports</h4>
    <form id="LoanReports" method="post" runat="server">
    
    <div class="PageTabSet" style="width: 774px; text-align: right">
        <div id="m_TabF" class="PageTab" style="left: 20px" onmouseover="if( className == 'PageTab' ) style.color = 'orange';"
            onmouseout="if( className == 'PageTab' ) style.color = 'whitesmoke'; else style.color = '#003366';"
            onclick="if( className != 'PageTabActive' ) showPage( 'm_Fav' );">
            Favorite reports
        </div>
        <div id="m_TabC" class="PageTab" style="left: 160px" onmouseover="if( className == 'PageTab' ) style.color = 'orange';"
            onmouseout="if( className == 'PageTab' ) style.color = 'whitesmoke'; else style.color = '#003366';"
            onclick="if( className != 'PageTabActive' ) showPage( 'm_Curr' );">
            Your reports
        </div>
        <div id="m_TabE" class="PageTab" style="left: 300px" onmouseover="if( className == 'PageTab' ) style.color = 'orange';"
            onmouseout="if( className == 'PageTab' ) style.color = 'whitesmoke'; else style.color = '#003366';"
            onclick="if( className != 'PageTabActive' ) showPage( 'm_ExtR' );">
            Others' reports
        </div>
        <div id="m_TabD" class="PageTab" style="left: 450px" onmouseover="if( className == 'PageTab' ) style.color = 'orange';"
            onmouseout="if( className == 'PageTab' ) style.color = 'whitesmoke'; else style.color = '#003366';"
            onclick="if( className != 'PageTabActive' ) showPage( 'm_DisableR' );">
            Disabled users' reports
        </div>

        <div id="m_TabS" class="PageTab" style="left: 600px" onmouseover="if( className == 'PageTab' ) style.color = 'orange';"
            onmouseout="if( className == 'PageTab' ) style.color = 'whitesmoke'; else style.color = '#003366';"
            onclick="if( className != 'PageTabActive' ) showPage( 'm_Samp' );">
            Sample reports
        </div>
        <input id="m_Last" type="hidden" runat="server" name="m_Last" value="m_Curr">
    </div>

    <script type="text/javascript">
        function GetActiveTabCode() {
            if (m_Curr.style.display == "block")
                return "C";
            else if (m_ExtR.style.display == "block")
                return "O";
            else if (m_DisableR.style.display == "block")
                return "D";
            else if (m_Samp.style.display == "block")
                return "S";
            else if (m_Fav.style.display == "block")
                return "F";
            else
                return "C";

        }
        function showPage(sPageId) {
            try {
                // Hide all pages and show the selected one.

                m_Curr.style.display = "none";
                m_ExtR.style.display = "none";
                m_DisableR.style.display = "none";
                m_Samp.style.display = "none";
                m_Fav.style.display = "none";

                m_TabC.style.color = "whitesmoke";
                m_TabE.style.color = "whitesmoke";
                m_TabD.style.color = "whitesmoke";
                m_TabS.style.color = "whitesmoke";
                m_TabF.style.color = "whitesmoke";

                m_TabC.className = "PageTab";
                m_TabE.className = "PageTab";
                m_TabD.className = "PageTab";
                m_TabS.className = "PageTab";
                m_TabF.className = "PageTab";

                switch (sPageId) {
                    case "m_Curr":
                        {
                            document.getElementById("m_btnAddToFavorites").style.display = "inline";
                            document.getElementById("m_btnRemoveFavorites").style.display = "none";

                            m_Curr.style.display = "block";
                            m_TabC.style.color = "#003366";
                            m_TabC.className += "Active";
                        }
                        break;

                    case "m_ExtR":
                        {
                            document.getElementById("m_btnAddToFavorites").style.display = "inline";
                            document.getElementById("m_btnRemoveFavorites").style.display = "none";

                            m_ExtR.style.display = "block";
                            m_TabE.style.color = "#003366";
                            m_TabE.className += "Active";
                        }
                        break;

                    case "m_DisableR":
                        {
                            document.getElementById("m_btnAddToFavorites").style.display = "inline";
                            document.getElementById("m_btnRemoveFavorites").style.display = "none";

                            m_DisableR.style.display = "block";
                            m_TabD.style.color = "#003366";
                            m_TabD.className += "Active";
                        }
                        break;

                    case "m_Samp":
                        {
                            document.getElementById("m_btnAddToFavorites").style.display = "inline";
                            document.getElementById("m_btnRemoveFavorites").style.display = "none";

                            m_Samp.style.display = "block";
                            m_TabS.style.color = "#003366";
                            m_TabS.className += "Active";
                        }
                        break;

                    case "m_Fav":
                        {
                            document.getElementById("m_btnAddToFavorites").style.display = "none";
                            document.getElementById("m_btnRemoveFavorites").style.display = "inline";

                            m_Fav.style.display = "block";
                            m_TabF.style.color = "#003366";
                            m_TabF.className += "Active";
                        }
                        break;
                }

                document.getElementById("m_Last").value = sPageId;
                document.getElementById('txtSelTabCode').value = GetActiveTabCode();
            }
            catch (e) {
            }
        }
    </script>

    </SCRIPT>
    <input type="hidden" id="txtSelTabCode" runat="server" />
    <div class="PageHolder" id="m_Panels">
        <%
            //
            // Execution and command button panel.
            //
        %>
        <div class="BasePanel">
            <div class="Bordered Separated" style="border-right: medium none; border-top: medium none;
                border-left: medium none; position: relative; height: 417px">
            </div>
            <div class="Separated" style="padding-right: 2px; padding-left: 2px; padding-bottom: 2px;
                width: 100%; padding-top: 2px">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left">
                            <span>
                                <asp:Button ID="m_AddNewQuery" runat="server" CssClass="CmdButton" Text="Add new"
                                    OnClick="AddNewReportQueryClick"></asp:Button>
                            </span>
                        </td>
                        <td align="right">
                            <span>
                                <asp:Button ID="m_btnRemoveFavorites" runat="server" CssClass="CmdButton" Width="160px"
                                    Text="Remove selected from favorites" OnClick="RemoveFavorites"></asp:Button>
                            </span><span>
                                <asp:Button ID="m_btnAddToFavorites" runat="server" CssClass="CmdButton" Width="160px"
                                    Text="Add selected to favorites" OnClick="AddToFavorites"></asp:Button>
                            </span><span>
                                <asp:Button ID="m_CopySamples" runat="server" CssClass="CmdButton" Width="160px"
                                    Text="Copy selected reports" OnClick="CopyReportQueries"></asp:Button>
                            </span><span>
                                <input type="button" class="CmdButton" value="Close" onclick="onClosePopup();">
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <%
            //
            // Available reports list.
            //
        %>
        <div id="m_Fav" class="BasePanel" style="display: block">
            <div class="Bordered Separated" style="border-right: medium none; border-top: medium none;
                margin-top: 8px; border-left: medium none; width: 100%; position: relative; height: 413px">
                <div class="PanelControl Bordered Separated" style="overflow-y: scroll; width: 100%;
                    height: 404px">
                    <ML:CommonDataGrid ID="m_FavReports" runat="server" CellPadding="2" GridLines="None"
                        AutoGenerateColumns="False" AllowSorting="True" Width="100%" OnItemCommand="HandleGridOperation"
                        OnItemDataBound="FavoriteReportGridBound">
                        <Columns>
                            <asp:TemplateColumn HeaderText="" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="10px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="SelectionF" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="QueryId" Visible="false" ItemStyle-CssClass="LookupRowItem"
                                HeaderStyle-CssClass="LookupRowHeader" SortExpression="QueryName" HeaderText="Report Name">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="QueryName" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                SortExpression="QueryName" HeaderText="Report Name"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EmployeeName" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                SortExpression="EmployeeName" HeaderText="Owner"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="290px">
                                <ItemTemplate>
                                    <div style="width: 230px;">
                                        <div style="width: 100%; display: block;">
                                            <a href="#" onclick="var a = parentElement , b = parentElement.nextSibling;  if(b.nodeType !== 1){b = b.nextSibling;} if( a.style.display == 'block' ) { a.style.display = 'none'; b.style.display = 'block'; } else { b.style.display = 'none'; a.style.display = 'block'; } return false;">
                                                show properties </a>
                                        </div>
                                        <div style="width: 100%; display: none; margin-bottom: 4px;">
                                            <span style="text-align: justify; width: 100%;">Saved by:
                                                <ml:EncodedLiteral ID="FavoritesSavedByLiteral" runat="server"></ml:EncodedLiteral>
                                                <br>
                                                On:
                                                <ml:EncodedLiteral ID="FavoritesSavedOnLiteral" runat="server"></ml:EncodedLiteral>
                                                <br>
                                                <asp:Panel ID="m_OthersSummaryPanel" runat="server">
                                                    Summary:
                                                    <ml:EncodedLiteral ID="FavoritesQueryDetailsLiteral" runat="server"></ml:EncodedLiteral>
                                                    <br>
                                                </asp:Panel>
                                                <ML:EncodedLabel ID="m_OthersRestrictedField" Style="display: block;" ForeColor="Red" Visible="false"
                                                    runat="server"></ML:EncodedLabel>
                                                <ML:EncodedLabel ID="m_OthersPermRequired" Style="display: block;" ForeColor="Red" Visible="false"
                                                    runat="server"></ML:EncodedLabel>
                                            </span><a href="#" onclick="var a = parentElement , b = parentElement.previousSibling;  if(b.nodeType !== 1){ b = b.previousSibling;} if( a.style.display == 'block' ) { a.style.display = 'none'; b.style.display = 'block'; } else { b.style.display = 'none'; a.style.display = 'block'; } return false;">
                                                hide properties </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                HeaderStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Run" runat="server">
                                        run
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                HeaderStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="View" runat="server">
                                        view
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </ML:CommonDataGrid>
                </div>
            </div>
        </div>
        <div id="m_Curr" class="BasePanel" style="display: block">
            <div class="Bordered Separated" style="border-right: medium none; border-top: medium none;
                margin-top: 8px; border-left: medium none; width: 100%; position: relative; height: 413px">
                <div class="PanelControl Bordered Separated" style="overflow-y: scroll; width: 100%;
                    height: 404px">
                    <ML:CommonDataGrid ID="m_Grid" runat="server" CellPadding="2" GridLines="None" AutoGenerateColumns="False"
                        AllowSorting="True" Width="100%" OnItemCommand="HandleGridOperation" OnItemDataBound="CurrReportGridBound">
                        <Columns>
                            <asp:TemplateColumn ItemStyle-Width="15px">
                                <ItemTemplate>
                                    <asp:PlaceHolder runat="server" ID="FavoriteIconPlaceHolderCurrent"><img style='border:none;' src='../../images/favorite.png' align='middle' alt='Favorite' width='12' height='12'></img></asp:PlaceHolder>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="10px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="SelectionC" runat="server" Key=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                    </asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="QueryName" HeaderStyle-Width="140px" ItemStyle-CssClass="LookupRowItem"
                                HeaderStyle-CssClass="LookupRowHeader" SortExpression="QueryName" HeaderText="Report Name"
                                ItemStyle-Width="250px"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px">
                                <ItemTemplate>
                                    <div>
                                        <div style="width: 100%; display: block;">
                                            <a href="#" onclick="var a = parentElement , b = parentElement.nextSibling;  if(b.nodeType !== 1) { b = b.nextSibling;} if( a.style.display == 'block' ) { a.style.display = 'none'; b.style.display = 'block'; } else { b.style.display = 'none'; a.style.display = 'block'; } return false;">
                                                show properties </a>
                                        </div>
                                        <div style="width: 100%; display: none; margin-bottom: 4px;">
                                            <span style="text-align: justify; width: 100%;">Saved by:
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QuerySavedBy" )) %>
                                                <br>
                                                On:
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QuerySavedOn" )) %>
                                                <br>
                                                Summary:
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryDetails" )) %>
                                                <br>
                                                <ML:EncodedLabel ID="m_CurrRestrictedField" Style="display: block;" ForeColor="Red" Visible="false"
                                                    runat="server"></ML:EncodedLabel>
                                                <ML:EncodedLabel ID="m_CurrPermRequired" Style="display: block;" ForeColor="Red" Visible="false"
                                                    runat="server"></ML:EncodedLabel>
                                            </span><a href="#" onclick="var a = parentElement , b = parentElement.previousSibling; if(b.nodeType !== 1) { b = b.previousSibling;} if( a.style.display == 'block' ) { a.style.display = 'none'; b.style.display = 'block'; } else { b.style.display = 'none'; a.style.display = 'block'; } return false;">
                                                hide properties </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="NamePublishedAs" HeaderStyle-Width="150px" SortExpression="NamePublishedAs"
                                HeaderText="Published&nbsp;As" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                HeaderStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Unpublish" runat="server" Visible="false" CommandName="Unpublish"
                                        CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>unpublish</asp:LinkButton>
                                    <asp:LinkButton ID="Publish" runat="server" Visible="false" href="javascript:void(0)" OnClientClick="javascript:PublishReport(this);" QueryID=<%# AspxTools.JsStringUnquoted(DataBinder.Eval( Container.DataItem , "QueryID").ToString()) %>>publish</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Run" runat="server" CommandName="Run" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                        run
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" CommandName="Edit" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                        edit
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Style="display: none;" CommandName="Remove" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString())%>>
                                        delete
                                    </asp:LinkButton><a href="#" onclick="if(confirm('Do you want to delete this item?')) previousSibling.click(); return false;">
                                        delete </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </ML:CommonDataGrid>
                </div>
            </div>
        </div>
        <%
            //
            // Available reports list.
            //
        %>
        <div id="m_ExtR" class="BasePanel" style="display: block">
            <div class="Bordered Separated" style="border-right: medium none; border-top: medium none;
                margin-top: 8px; border-left: medium none; width: 100%; position: relative; height: 413px">
                <div class="PanelControl Bordered Separated" style="overflow-y: scroll; width: 100%;
                    height: 404px">
                    <ML:CommonDataGrid ID="m_Othr" runat="server" CellPadding="2" GridLines="None" AutoGenerateColumns="False"
                        AllowSorting="True" Width="100%" OnItemCommand="HandleGridOperation" OnItemDataBound="ReportGridBound">
                        <Columns>
                            <asp:TemplateColumn ItemStyle-Width="15px">
                                <ItemTemplate>
                                    <asp:PlaceHolder runat="server" ID="FavoriteIconPlaceHolderAvailable"><img style='border:none;' src='../../images/favorite.png' align='middle' alt='Favorite' width='12' height='12'></img></asp:PlaceHolder>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="10px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="SelectionO" runat="server">
                                    </asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="QueryName" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                SortExpression="QueryName" HeaderText="Report Name"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EmployeeName" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                SortExpression="EmployeeName" HeaderText="Owner" ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="240px">
                                <ItemTemplate>
                                    <div style="width: 230px;">
                                        <div style="width: 100%; display: block;">
                                            <a href="#" onclick="var a = parentElement , b = parentElement.nextSibling; if(b.nodeType !== 1) { b = b.nextSibling;} if( a.style.display == 'block' ) { a.style.display = 'none'; b.style.display = 'block'; } else { b.style.display = 'none'; a.style.display = 'block'; } return false;">
                                                show properties </a>
                                        </div>
                                        <div style="width: 100%; display: none; margin-bottom: 4px;">
                                            <span style="text-align: justify; width: 100%;">Saved by:
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QuerySavedBy" )) %>
                                                <br>
                                                On:
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QuerySavedOn" )) %>
                                                <br>
                                                <asp:Panel ID="m_OthersSummaryPanel" runat="server">
                                                    Summary:
                                                    <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryDetails" )) %>
                                                    <br>
                                                </asp:Panel>
                                                <ML:EncodedLabel ID="m_OthersRestrictedField" Style="display: block;" ForeColor="Red" Visible="false"
                                                    runat="server"></ML:EncodedLabel>
                                                <ML:EncodedLabel ID="m_OthersPermRequired" Style="display: block;" ForeColor="Red" Visible="false"
                                                    runat="server"></ML:EncodedLabel>
                                            </span><a href="#" onclick="var a = parentElement , b = parentElement.previousSibling; if(b.nodeType !== 1) { b = b.previousSibling;} if( a.style.display == 'block' ) { a.style.display = 'none'; b.style.display = 'block'; } else { b.style.display = 'none'; a.style.display = 'block'; } return false;">
                                                hide properties </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Run" runat="server" CommandName="Run" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                        run
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="View" runat="server" CommandName="View" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                        view
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="40px" Visible="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Edit" runat="server" CommandName="Edit" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                        edit
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </ML:CommonDataGrid>
                </div>
            </div>
        </div>

        <%
            //
            // Disable users' report list.
            //
        %>
        <div id="m_DisableR" class="BasePanel" style="display: block">
            <div class="Bordered Separated" style="border-right: medium none; border-top: medium none;
                margin-top: 8px; border-left: medium none; width: 100%; position: relative; height: 413px">
                <div class="PanelControl Bordered Separated" style="overflow-y: scroll; width: 100%;
                    height: 404px">
                    <ML:CommonDataGrid ID="m_exReport" runat="server" CellPadding="2" GridLines="None" AutoGenerateColumns="False"
                        AllowSorting="True" Width="100%" OnItemCommand="HandleGridOperation" OnItemDataBound="ExReportGridBound">
                        <Columns>
                            <asp:TemplateColumn ItemStyle-Width="15px">
                                <ItemTemplate>
                                    <asp:PlaceHolder runat="server" ID="FavoriteIconPlaceHolder4ExReport"><img style='border:none;' src='../../images/favorite.png' align='middle' alt='Favorite' width='12' height='12'></img></asp:PlaceHolder>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="10px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="SelectionD" runat="server">
                                    </asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="QueryName" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                SortExpression="QueryName" HeaderText="Report Name"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EmployeeName" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                SortExpression="EmployeeName" HeaderText="Owner" ItemStyle-Width="150px"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="240px">
                                <ItemTemplate>
                                    <div style="width: 230px;">
                                        <div style="width: 100%; display: block;">
                                            <a href="#" onclick="var a = parentElement , b = parentElement.nextSibling; if(b.nodeType !== 1) { b = b.nextSibling;} if( a.style.display == 'block' ) { a.style.display = 'none'; b.style.display = 'block'; } else { b.style.display = 'none'; a.style.display = 'block'; } return false;">
                                                show properties </a>
                                        </div>
                                        <div style="width: 100%; display: none; margin-bottom: 4px;">
                                            <span style="text-align: justify; width: 100%;">Saved by:
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QuerySavedBy" )) %>
                                                <br>
                                                On:
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QuerySavedOn" )) %>
                                                <br>
                                                <asp:Panel ID="m_OthersSummaryPanel" runat="server">
                                                    Summary:
                                                    <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryDetails" )) %>
                                                    <br>
                                                </asp:Panel>
                                                <ML:EncodedLabel ID="m_OthersRestrictedField" Style="display: block;" ForeColor="Red" Visible="false"
                                                    runat="server"></ML:EncodedLabel>
                                                <ML:EncodedLabel ID="m_OthersPermRequired" Style="display: block;" ForeColor="Red" Visible="false"
                                                    runat="server"></ML:EncodedLabel>
                                            </span><a href="#" onclick="var a = parentElement , b = parentElement.previousSibling; if(b.nodeType !== 1) { b = b.previousSibling;} if( a.style.display == 'block' ) { a.style.display = 'none'; b.style.display = 'block'; } else { b.style.display = 'none'; a.style.display = 'block'; } return false;">
                                                hide properties </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Run" runat="server" CommandName="Run" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                        run
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="View" runat="server" CommandName="View" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                        view
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="40px" Visible="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Edit" runat="server" CommandName="Edit" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                        edit
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </ML:CommonDataGrid>
                </div>
            </div>
        </div>

        <%
            //
            // Sample reports list.
            //
        %>
        <div id="m_Samp" class="BasePanel" style="display: none">
            <div class="Bordered Separated" style="border-right: medium none; border-top: medium none;
                margin-top: 8px; border-left: medium none; width: 100%; position: relative; height: 413px">
                <div class="PanelControl Bordered Separated" style="overflow-y: scroll; width: 100%;
                    height: 404px">
                    <ML:CommonDataGrid ID="m_Demo" runat="server" CellPadding="2" GridLines="None" AutoGenerateColumns="False"
                        AllowSorting="True" Width="100%" OnItemCommand="HandleGridOperation" OnItemDataBound="OnSampleGridBound">
                        <Columns>
                            <asp:TemplateColumn ItemStyle-Width="15px">
                                <ItemTemplate>
                                    <asp:PlaceHolder runat="server" ID="FavoriteIconPlaceHolderSample"><img style='border:none;' src='../../images/favorite.png' align='middle' alt='Favorite' width='12' height='12'></img></asp:PlaceHolder>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="10px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="SelectionS" runat="server" Key=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                    </asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="QueryName" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                SortExpression="QueryName" HeaderText="Report Name" ItemStyle-Width="325px">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="" ItemStyle-CssClass="LookupRowItem" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="280px">
                                <ItemTemplate>
                                    <div style="width: 250px;">
                                        <div style="width: 100%; display: block;">
                                            <a href="#" onclick="var a = parentElement , b = parentElement.nextSibling; if(b.nodeType !== 1) { b = b.nextSibling;} if( a.style.display == 'block' ) { a.style.display = 'none'; b.style.display = 'block'; } else { b.style.display = 'none'; a.style.display = 'block'; } return false;">
                                                show properties </a>
                                        </div>
                                        <div style="width: 100%; display: none; margin-bottom: 4px;">
                                            <span style="text-align: justify; width: 100%;">Saved by:
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QuerySavedBy" )) %>
                                                <br>
                                                On:
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QuerySavedOn" )) %>
                                                <br>
                                                Summary:
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryDetails" )) %>
                                            </span><a href="#" onclick="var a = parentElement , b = parentElement.previousSibling; if(b.nodeType !== 1) { b = b.previousSibling;} if( a.style.display == 'block' ) { a.style.display = 'none'; b.style.display = 'block'; } else { b.style.display = 'none'; a.style.display = 'block'; } return false;">
                                                hide properties </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" CommandName="Run" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString() + ",Demo") %>>
                                        run
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-CssClass="LookupRowLink" HeaderStyle-CssClass="LookupRowHeader"
                                ItemStyle-Width="30px">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" CommandName="Demo" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryID" ).ToString()) %>>
                                        view
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </ML:CommonDataGrid>
                </div>
            </div>
        </div>
        <%
            //
            // Popup busy wait dialog.
            //
        %>
        <div class="BasePanel">
            <div id="m_Wait" class="Hidden" style="border-right: gainsboro 2px solid; padding-right: 0.3in;
                border-top: gainsboro 2px solid; padding-left: 0.3in; z-index: 9; background: whitesmoke;
                left: 80px; padding-bottom: 0.3in; border-left: gainsboro 2px solid; padding-top: 0.3in;
                border-bottom: gainsboro 2px solid; position: absolute; top: 40px" onclick="this.className='Hidden';">
                <div style="border-right: gainsboro 2px solid; padding-right: 16px; border-top: gainsboro 2px solid;
                    padding-left: 16px; padding-bottom: 16px; border-left: gainsboro 2px solid; width: 3in;
                    padding-top: 16px; border-bottom: gainsboro 2px solid; background-color: white">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="Waiting.gif">
                            </td>
                            <td>
                                <span style="font: bold 11px arial">Processing request. Please wait... </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <uc1:cModalDlg ID="CModalDlg1" runat="server" />
    </form>
</body>
</html>
