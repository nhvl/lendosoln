﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.ObjLib.Conversions.ProvidentFunding;
using LendersOfficeApp.common;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using System.Web;

namespace LendersOfficeApp.los.Reports
{
    public partial class SubservicingExport : LendersOffice.Common.BasePage
    {
        protected void PageLoad(object sender, EventArgs e)
        {
            string dateStr = m_fundedDate.Text.TrimWhitespaceAndBOM();
            DateTime date;
            if (DateTime.TryParse(dateStr, out date))
            {
                BrokerUserPrincipal user = BrokerUserPrincipal.CurrentPrincipal;
                
                //Prepare Query Parameters

                List<SqlParameter> parameters = new List<SqlParameter>(){
                    new SqlParameter("@brokerId", user.BrokerId),
                    new SqlParameter("@FundD", date)};

                //If User only has Branch or Individual access,
                // stored procedure receives non-null values:
                // {Branch level: @employeeId, @branchId}
                // {Individual level: @employeeId}
                if (!user.HasPermission(Permission.BrokerLevelAccess))
                {
                    parameters.Add(new SqlParameter("@employeeId", user.EmployeeId));
                    if (user.HasPermission(Permission.BranchLevelAccess))
                    {
                        parameters.Add(new SqlParameter("@branchId", user.BranchId));
                    }
                }
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(user.BrokerId, "ListLoansForSubservicingExport", parameters))
                {
                    m_grid.DataSource = reader;
                    m_grid.DataBind();

                }

                if(m_grid.Items.Count>0)
                {
                    //Set UI Elements to "found"
                    m_ExportBtn1.Visible = true;
                    m_ExportBtn2.Visible = true;
                    m_noLoansFound.Visible = false;
                }
                else
                {
                    //Set UI Elements to "not found"
                    m_ExportBtn1.Visible = false;
                    m_ExportBtn2.Visible = false;
                    m_noLoansFound.Visible = true;
                }

                m_emptyDateField.Visible = false;
            }
            else
            {
                //Set UI Elements to "no start funded date entered"
                m_emptyDateField.Visible = true;
                m_ExportBtn1.Visible = false;
                m_ExportBtn2.Visible = false;
                m_noLoansFound.Visible = false;
            }

        }

        protected string FormatNullableLoanStatus(object o)
        {
            try
            {
                return CPageBase.sStatusT_map_rep((E_sStatusT)o);
            }
            catch (InvalidCastException)
            {
                return "";
            }
        }
        protected string FormatNullableLoanType(object o)
        {
            try
            {
                return CPageBase.sLT_map_rep((E_sLT)o);
            }
            catch (InvalidCastException)
            {
                return "";
            }
        }

        protected void ExportClick(object sender, EventArgs e)
        {
            string param = m_downloadInfo.Value;

            string[] data = param.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (data.Length < 2)
            {
                return;
            }

            int downloadType = int.Parse(data[0]);

            List<Guid> loanIds = new List<Guid>(data.Length - 1);

            for (int i = 1; i < data.Length; i++)
            {
                loanIds.Add(new Guid(data[i]));
            }            
            
            ProvidentFundingExport exporter = new ProvidentFundingExport(loanIds);

            switch (downloadType)
            {
                // 0 -> export loan sale file as csv, 1 -> zip of all the other files.
                // Appends a cookie that is used to stop the download animation on the client
                case 0:
                    Response.ContentType = "text/plain";
                    Response.AppendCookie(new HttpCookie("downloadToken", m_downloadTokenId.Value));
                    Response.AppendHeader("content-disposition", "attachment; filename=\"loansalefile.txt\"");
                    string loanSaleFileCSV = exporter.GetLoanSaleFileCsv();
                    Response.Write(loanSaleFileCSV);
                    break;
                case 1:
                    Response.ContentType = "application/zip";
                    Response.AppendCookie(new HttpCookie("downloadToken", m_downloadTokenId.Value));
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"export.zip\"");
                    exporter.WriteToZip(Response.OutputStream);
                    break;
                default:
                    return;
            }

            Response.Flush();
            Response.End();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.EnableJqueryMigrate = false;
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
