<%@ Page language="c#" Codebehind="TmpReportResult.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Reports.TmpReportResult" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>TmpReportResult</title>
    <script type="text/javascript">
        function displayError( eMsg ) {
            try{
			    if( eMsg != null ){
				    alert( eMsg ); // Display error notification.
			    }
		    }
		    catch( e ){}
		}
		
		function onInit() {
		    try {
		        // Handle error message display.
		        if (document.getElementById("m_ErrorMessage")) {
		            displayError(document.getElementById("m_ErrorMessage").value);
		            return;
		        }
		    } 
		    catch (e) { }
		}
	</script>
  </head>
  <body MS_POSITIONING="FlowLayout" onload="onInit();">
    <form id="TmpReportResult" method="post" runat="server">
     </form>
	
  </body>
</html>
