﻿namespace LendersOffice
{
    using System;
    using System.Web.UI;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.QueryProcessor;
    using LendersOfficeApp.los.Portlets;
    using LendersOffice.Security;

    public partial class ChooseBatchExportReport : LendersOffice.Common.BasePage
    {

        private BrokerUserPrincipal BrokerUser
        {
            // Get the current user principal.
            get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
        }

        private BrokerDB m_broker = null;
        private BrokerDB Broker
        {
            get
            {
                if (m_broker == null)
                {
                    m_broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                }
                return m_broker;
            }
        }

        private String ErrorMessage
        {
            // Set a new error message.

            set
            {
                Page.ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + ".");
            }
        }

        private String CommandToDo
        {
            // Set a new command to do.

            set
            {
                Page.ClientScript.RegisterHiddenField("m_commandToDo", value);
            }
        }

        /// <summary>
        /// Initialize this form.
        /// </summary>

        protected void PageLoad(object sender, System.EventArgs a)
        {
            if (!this.IsPostBack)
            {
                this.ChooseReport.BindData(Guid.Empty);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion



        protected void ChooseReport_OnReportError(object sender, ErrorEventArgs args)
        {
            this.ErrorMessage = args.ErrorMessage;
        }

        protected void ChooseReport_OnReportChosen(object sender, ChooseReportEventArgs args)
        {
            RegisterJsGlobalVariables("SelectedReport", args.SelectedReport.QueryId.ToString());
        }
    }
}
