using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.QueryProcessor;
using LendersOffice.Security;
using LendersOffice.Reports;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Collections.Generic;
using LendersOffice.Constants;

namespace LendersOffice
{
	public partial class LoanReportEditor : LendersOffice.Common.BasePage
	{
        protected LendersOffice.QueryProcessor.ReportLabelUserControl m_ReportLabel;
        protected LendersOffice.QueryProcessor.CommentEditUserControl m_CommentEdit;
        protected LendersOffice.QueryProcessor.FieldFnGridUserControl m_FieldFnGrid;
		protected LendersOffice.QueryProcessor.FieldSorterUserControl m_FieldSorter;
		protected LendersOffice.QueryProcessor.FieldGroupsUserControl m_FieldGroups;
		protected LendersOffice.QueryProcessor.RelatesTreeUserControl m_CondViewing;
        protected LendersOffice.QueryProcessor.SearchListUserControl  m_SearchIncld;
        protected LendersOffice.QueryProcessor.FieldLayoutUserControl m_FieldLayout;
        protected LendersOffice.QueryProcessor.RelatesTreeUserControl m_CondSetting;
        protected LendersOffice.QueryProcessor.OptionEntryUserControl m_OptionEntry;
        protected LendersOffice.QueryProcessor.FilterRangeUserControl m_FilterRange;
        protected LendersOffice.QueryProcessor.FilterClassUserControl m_FilterActiv;
        protected LendersOffice.QueryProcessor.FilterClassUserControl m_FilterInact;
        protected LendersOffice.QueryProcessor.FilterClassUserControl m_FilterOther;
		private LendersOffice.QueryProcessor.FieldLookup                    m_Loans;
		private LendersOffice.QueryProcessor.Query                          m_Query;

        private string m_rt;

        private Boolean m_IsInDemoMode
        {
            get
            {
                if( RequestHelper.GetSafeQueryString( "mode" ) != null && RequestHelper.GetSafeQueryString( "mode" ) == "demo" )
                {
                    return true;
                }

                return false;
            }
        }

		private Boolean m_IsInViewMode
		{
			get
			{
				if( RequestHelper.GetSafeQueryString( "mode" ) != null && RequestHelper.GetSafeQueryString( "mode" ) == "view" )
				{
					return true;
				}

				return false;
			}
		}

        private Boolean m_isSampleReport
        {
            get
            {
                if (RequestHelper.GetSafeQueryString("isSample") != null && RequestHelper.GetSafeQueryString("isSample") == "true")
                {
                    return true;
                }

                return false;
            }
        }
		
		private Boolean m_IsInEditMode
        {
            get
            {
                if( RequestHelper.GetSafeQueryString( "mode" ) == null || RequestHelper.GetSafeQueryString( "mode" ) == "edit" )
                {
                    return true;
                }

                return false;
            }
        }

		private BrokerUserPrincipal BrokerUser
		{
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private Guid m_BrokerId
        {
            get
            {
                // Lookup current principal.

                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if( broker == null )
                {
                    return Guid.Empty;
                }

                return broker.BrokerId;
            }
        }

        private Guid m_BranchId
        {
            get
            {
                // Lookup admin broker's current id.

                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if (broker == null) 
                {
                    return Guid.Empty;
                }

                return broker.BranchId;
            }
        }

        private Guid m_BAdminId
        {
            get
            {
                // Lookup admin broker's current id.
                return BrokerDB.AdminBr;
            }
        }
		
		private Guid m_PmlDojoId
        {
            get
            {
                object id = Tools.GetPmlDojoBrokerId();
                if( id != null )
                {
                    return new Guid( id.ToString() );
                }

                return Guid.Empty;
            }
        }

        private string m_EmployeeNm
        {
            get
            {
                // Lookup current principal.

                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if( broker == null )
                {
                    return "";
                }

                return broker.DisplayName;
            }
        }

        private Guid m_EmployeeId
        {
            get
            {
                // Lookup current principal.

                BrokerUserPrincipal broker = BrokerUserPrincipal.CurrentPrincipal;

                if( broker == null )
                {
                    return Guid.Empty;
                }

                return broker.EmployeeId;
            }
        }

        private string m_ExceptionDetails
        {
            set
            {
                if( value.EndsWith( "." ) == false )
                {
                    ClientScript.RegisterHiddenField( "m_ErrorMessage" , value + "." );
                }
                else
                {
                    ClientScript.RegisterHiddenField( "m_ErrorMessage" , value );
                }
            }
        }

        private String m_FeedbackMessage
        {
            set
            {
                if( value.EndsWith( "." ) == false )
                {
                    ClientScript.RegisterHiddenField( "m_Feedback" , value + "." );
                }
                else
                {
                    ClientScript.RegisterHiddenField( "m_Feedback" , value );
                }
            }
        }

        /// <summary>
        /// Handle ui events.
        /// </summary>

        protected void QueryHasBeenUpdated( object sender , string a )
        {
            // The query is now considered out of sync with the
            // database.  We want to track this with a dirty bit.

            m_Dirty.Value = "Dirty";
        }

        protected void RemoveFieldClick( object sender , LendersOffice.QueryProcessor.FieldLayoutEventArgs a )
        {
            // Remove layout and organization references to this
            // field.  Conditions may still reference the field.

            String id = a.Target;

            m_Query.Columns.Nix( id );
            m_Query.Sorting.Nix( id );
            m_Query.GroupBy.Nix( id );
        }

        /// <summary>
        /// Save the current query to the database.  If the query
        /// already exists, then we overwrite the row with our
        /// local instance.  Else, we make a new report in the
        /// table.
        /// </summary>

        protected void SaveQuery( object sender , System.EventArgs a )
        {
            // Commit the current query to the database.  If not
            // currently persisted, we add as new.
			//
			// 3/7/2005 kb - We keep a stringified copy of our query
			// in case we need to roll back on error.  All errors
			// should throw out to the outer catch.

			String rollBack = String.Empty;

			try
			{
				rollBack = m_Query.ToString();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to stash query rollback instance." , e );

				m_ExceptionDetails = "Failed to save.";

				m_Phase.Value = "Error";

				return;
			}

			try
            {
                // Validate the input query's name and punt if it
                // is empty.

                if( m_Query.Label.Title.TrimWhitespaceAndBOM() == "" )
                {
                    m_ExceptionDetails = "Report not saved.  Please specify a report name";

                    if( m_Phase.Value == "Closing" )
                    {
                        m_Phase.Value = "";
                    }

                    return;
                }

				if( m_IsInEditMode == true )
				{
					// Initialize search for matching query, and populate
					// this query with the last saved by info.  We time
					// stamp the local instance of this query and get its
					// details from the db.  If not found, then we know
					// it is a new one, so fall out to the create code
					// that follows.

					m_Query.Label.SavedBy = m_EmployeeNm;
					m_Query.Label.SavedId = m_EmployeeId;
					m_Query.Label.SavedOn = DateTime.Now;

                    LoanReporting reportRunner = new LoanReporting();

                    if (ConstStage.MaxReportSizeBeforeFailCustomReport != 0)
                    {
                        var reportSize = reportRunner.GetReportSize(m_Query, BrokerUserPrincipal.CurrentPrincipal, true, E_ReportExtentScopeT.Default);
                        if (reportSize.RowsByColumns > ConstStage.MaxReportSizeBeforeFailCustomReport)
                        {

                            throw new CBaseException("Your report is too big too be run manually.  Consider adding conditions or removing columns to reduce the report size.",
                                $"Report's size was {reportSize.RowsByColumns} ({reportSize.NumRows} rows, {m_Query.Columns.Count} columns) which exceeds {nameof(ConstStage.MaxReportSizeBeforeFailCustomReport)} ({ConstStage.MaxReportSizeBeforeFailCustomReport})");
                        }
                    }

                    try
					{
                        SqlParameter[] parameters1 = {
                                                        new SqlParameter( "@BrokerId" , m_BrokerId )
                                                    };
                        string sInputTitle = m_Query.Label.Title.TrimWhitespaceAndBOM();
                        bool dbHasQueryWithMatchingID = false;
						using( IDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "GetReportQueryDetails" , parameters1 ) )
						{
                            string sQueryTitle = (sInputTitle.Length > 64) ? sInputTitle.ToLower().Substring(0, 64) : sInputTitle.ToLower();                                
                            
                            // check for duplicates or matching query id, whichever is first.
                            while( sR.Read() == true )
							{
								// Check for a duplicate.  If same name is found, we cannot
								// save another.
                                if (sR["QueryName"].ToString().ToLower().TrimWhitespaceAndBOM() == sQueryTitle.TrimWhitespaceAndBOM() && (Guid)sR["QueryId"] != m_Query.Id)
								{
									// 3/2/2005 kb - We found a report with the same name in our
									// broker, so I suspect it collides.  We need to serve up an
									// appropriate error message.

									m_ExceptionDetails = "Report not saved.  Report with same name already exists";

									m_Phase.Value = "";

									return;
								}

                                if ((Guid)sR["QueryId"] == m_Query.Id)
                                {
                                    dbHasQueryWithMatchingID = true;
                                    break;
                                }
                            }
                        }

						// Check for a match.  The filled data set should have
						// exactly one table with one row that contains the
						// query's details.  If we never find an id match, then
						// we eventually fall out to the create call that follows.

                        if (dbHasQueryWithMatchingID)
                        {
                            try
                            {
                                SqlParameter[] parameters = {
                                                                new SqlParameter( "@BrokerID"   , m_BrokerId        ),
                                                                new SqlParameter( "@QueryID"    , m_Query.Id        ),
                                                                new SqlParameter("@QueryName", sInputTitle),
                                                                new SqlParameter("@XmlContent" , m_Query.ToString())
                                                            };
                                var affected = StoredProcedureHelper.ExecuteNonQuery(m_BrokerId, "UpdateReportQuery", 0, parameters);
                                if (affected != 1)
                                {
                                    Tools.LogError("UpdateReportQuery run with queryId '" + m_Query.Id + "' affected " + affected + " rows." +
                                        " It should have been 1. XmlContent: " + m_Query.ToString());
                                }
                                else
                                {
                                    Tools.LogInfo("UpdateReportQuery run with queryId " + m_Query.Id + Environment.NewLine
                                        + "XmlContent: " + Environment.NewLine + m_Query.ToString());
                                }
                            }
                            catch (SqlException sqe)
                            {
                                if (sqe.Message.Contains("Duplicated query name is not allowed within a broker"))
                                {
                                    m_ExceptionDetails = "Report not saved.  Report with same name already exists";

                                    m_Phase.Value = "";

                                    return;
                                }

                                throw;
                            }
                            catch (Exception e)
                            {
                                // Note to self: something exploded.

                                Tools.LogError("Loan reporting: Exception caught during save existing query.", e);

                                throw;
                            }

                            // Mark the query as saved.

                            m_State.Value = "Saved";

                            if (m_Phase.Value == "Closing")
                            {
                                Page.Response.Redirect(GetReturnUrl(), false);
                            }

                            m_Dirty.Value = "";

                            return;
                        }
                        else // if not dbHasQueryWithMatchingID
                        {
                            // We are probably adding the report and need to allow the save to happen.
                            // Ideally we should have a new report mode so we know we want the save to go through.
                        }
					}
					catch( Exception e )
					{
						// Note to self: something exploded.

						Tools.LogError( "Loan reporting: Exception caught during check if existing query.", e );

						throw;
					}
				}
				else
				if( m_IsInDemoMode == true )
				{
					// Reset the query id because we are forcing a save
					// as new report operation.
					//
					// 3/2/2005 kb - Perform check against name so we can
					// better report collision errors.  More and more copying
					// is now going on because we split out the report set
					// into "My queries" and "Others' queries".

					try
					{
                        string sInputTitle = m_Query.Label.Title.TrimWhitespaceAndBOM();
                        string sQueryTitle = (sInputTitle.Length > 64) ? sInputTitle.ToLower().Substring(0, 64) : sInputTitle.ToLower();

                        SqlParameter[] parameters = {
                                                        new SqlParameter("@QueryName", sQueryTitle),
                                                        new SqlParameter("@BrokerId", m_BrokerId)
                                                    };

                        using (IDataReader sR = StoredProcedureHelper.ExecuteReader(m_BrokerId, "GetReportQueryDetails", parameters))
						{
							if( sR.Read() == true )
							{
								// 3/2/2005 kb - We found a report with the same name in our
								// broker, so I suspect it collides.  We need to serve up an
								// appropriate error message.

								m_ExceptionDetails = "Report not saved.  Report with same name already exists";

								m_Phase.Value = "";

								return;
							}
						}
					}
					catch( Exception e )
					{
						// Note to self: something exploded.

						Tools.LogError( "Loan reporting: Exception caught during check if existing query.", e );

						throw;
					}

					m_Query.Id = Guid.NewGuid();
				}
				else
				if( m_IsInViewMode == true )
				{
					// Reset the query id because we are forcing a save
					// as new report operation in my queries' set.
					//
					// 3/2/2005 kb - Perform check against name so we can
					// better report collision errors.  More and more copying
					// is now going on because we split out the report set
					// into "My queries" and "Others' queries".

					try
					{
                        string sQueryTitle = (m_Query.Label.Title + " (copy)");
                        sQueryTitle = (sQueryTitle.Length > 64) ? sQueryTitle.Substring(0, 64) : sQueryTitle;
                        SqlParameter[] parameters = {
                                                        new SqlParameter("@QueryName", sQueryTitle),
                                                        new SqlParameter("@BrokerId", m_BrokerId)
                                                    };
                        using (IDataReader sR = StoredProcedureHelper.ExecuteReader(m_BrokerId, "GetReportQueryDetails", parameters ))
						{
							if( sR.Read() == true )
							{
								// 3/2/2005 kb - We found a report with the same name in our
								// broker, so I suspect it collides.  We need to serve up an
								// appropriate error message.

								m_ExceptionDetails = "Report not saved.  Report with same name already exists";

								m_Phase.Value = "";

								return;
							}
						}
					}
					catch( Exception e )
					{
						// Note to self: something exploded.

						Tools.LogError( "Loan reporting: Exception caught during check if existing query.", e );

						throw;
					}

					m_Query.Label.Title = m_Query.Label.Title + " (copy)";

					m_Query.Id = Guid.NewGuid();
				}

                // Save a new query with the given name and a unique id.
                // If we fail (most likely because the name is not unique),
                // then we report through the ui.

                try
                {
                    SqlParameter[] parameters = {
                          new SqlParameter( "@BrokerID", m_BrokerId)
						, new SqlParameter( "@QueryID"    , m_Query.Id          )
						, new SqlParameter( "@EmployeeID" , m_EmployeeId        )
						, new SqlParameter( "@QueryName"  , m_Query.Label.Title )
						, new SqlParameter( "@XmlContent" , m_Query.ToString()  )

                                                };

                    StoredProcedureHelper.ExecuteNonQuery( m_BrokerId, "CreateReportQuery" , 0, parameters);
                }
                catch( Exception e )
                {
                    // Note to self: something exploded.

                    Tools.LogError( "Loan reporting: Exception caught during save new query.", e );

                    throw;
                }

                // Mark the query as saved.

                m_State.Value = "Saved";

                if( m_Phase.Value == "Closing" )
                {
                    Page.Response.Redirect(GetReturnUrl(), false);
                }

                m_Dirty.Value = "";

                // Redirect if we are saving a new query.

                if( m_IsInDemoMode == true || m_IsInViewMode == true )
                {
                    Response.Redirect( "LoanReportEditor.aspx?id=" + m_Query.Id + "&mode=edit" + "&rt=" + m_rt, false );
                }
            }
            catch (CBaseException e)
            {
                // Write out the exception to our log.  We also roll
                // back any changes that may have occurred.

                m_ExceptionDetails = e.UserMessage ?? "Error saving query.";

                m_Phase.Value = "Error";

                Tools.LogError(e);

                m_Query = rollBack;
            }
            catch( Exception e )
            {
                // Write out the exception to our log.  We also roll
				// back any changes that may have occurred.

                m_ExceptionDetails = "Error saving query.";

				m_Phase.Value = "Error";

				Tools.LogError( e );

				m_Query = rollBack;
            }
        }

        /// <summary>
        /// Copy the existing query to a new instance using the
        /// instance that resides within the database.
        /// </summary>

        protected void CopyQuery( object sender , System.EventArgs a )
        {
            // Copy the specified query to a new instance.  We use
            // the version that was last saved to the database.
			//
			// 3/7/2005 kb - We keep a stringified copy of our query
			// in case we need to roll back on error.  All errors
			// should throw out to the outer catch.

			String rollBack = String.Empty;

			try
			{
				rollBack = m_Query.ToString();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to stash query rollback instance." , e );

				m_ExceptionDetails = "Failed to save.";

				m_Phase.Value = "Error";

				return;
			}

            try
            {
                // Dirty queries need to be saved first -- at least,
                // attempt to save them.  We always duplicate from
                // the persistent form of the query.

                if( m_State.Value != "Saved" )
                {
                    // Notify the user that new reports need to be
                    // saved prior to duplication.

                    m_FeedbackMessage = "Report not duplicated.  New reports need to be saved before they can be duplicated";

                    return;
                }

                if( m_Dirty.Value == "Dirty" && m_Phase.Value != "Copying" )
                {
                    // Mark the intermediate intent as copying and
                    // return to the client for confirmation.

                    m_Phase.Value = "Storing";

                    return;
                }

                // We need to get the query from the database and resave
                // it as a new query with a new id, a new name, and new
                // saved/created-by attributes.

                //ArrayList pN = new ArrayList();
                Query   copy = new Query();

                //pN.Add( new SqlParameter( "@BrokerId" , m_BrokerId ) );
                //pN.Add( new SqlParameter( "@QueryId"  , m_Query.Id ) );

                copy.Id = Guid.Empty;

                try
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerId", m_BrokerId),
                                                    new SqlParameter("@QueryId", m_Query.Id)
                                                };
					using( IDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "GetReportQuery" , parameters ) )
					{
						if( sR.Read() == true )
						{
							// We found the source (only result accepted from
							// the query) and will now track the duplicate.

							copy = sR[ "XmlContent" ].ToString();
						}
					}
                }
                catch( Exception e )
                {
                    // Note to self: something exploded.

                    Tools.LogError( "Loan reporting: Exception caught during get of existing query." );

                    throw e;
                }

                if( copy.Id != m_Query.Id )
                {
                    // Query not found because it hasn't been saved.  Tell
                    // the user to save the query prior to duplicating it.
                    // This might occur if the embedded id is out of sync
                    // with the id contained by the row.

                    throw new ArgumentException( "Unable to find source report in database for copy." );
                }

                // Find the existing query as it is within the database.
                // We use this query and update the name and resave it.

                //ArrayList pA = new ArrayList();

                //pA.Add( new SqlParameter( "@QueryName" , copy.Label.Title + " (copy)" ) );
                //pA.Add( new SqlParameter( "@BrokerID"  , m_BrokerId                   ) );

                try
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@QueryName" , copy.Label.Title + " (copy)" ),
                                                    new SqlParameter( "@BrokerID"  , m_BrokerId                   )
                    };
					using( IDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "GetReportQueryDetails" , parameters) )
					{
						if( sR.Read() == true )
						{
							// Not allowed to duplicate names.  Tell user to
							// try again.

							m_ExceptionDetails = "Report not duplicated.  Report with duplication name already exists";

							m_Phase.Value = "";

							return;
						}
					}
                }
                catch( Exception e )
                {
                    // Note to self: something exploded.

                    Tools.LogError( "Loan reporting: Exception caught during check for duplicate details.", e );

                    throw;
                }

                // We pulled the query from the database.  Now, we just need
                // to time stamp it as a new query and save it out with a
                // new identifier.

                copy.Label.SavedBy = m_EmployeeNm;
                copy.Label.SavedId = m_EmployeeId;
                copy.Label.SavedOn = DateTime.Now;

                copy.Label.Title += " (copy)";

                copy.Id = Guid.NewGuid();

                try
                {
                    // Save a new query with the given name and a unique id.
                    // If we fail (most likely because the name is not unique),
                    // then we report through the ui.
                    SqlParameter[] parameters = {
                                                                            new SqlParameter( "@BrokerID", m_BrokerId)
						, new SqlParameter( "@EmployeeID" , m_EmployeeId     )
						, new SqlParameter( "@QueryID"    , copy.Id          )
						, new SqlParameter( "@QueryName"  , copy.Label.Title )
						, new SqlParameter( "@XmlContent" , copy.ToString()  )

                                                };
                    StoredProcedureHelper.ExecuteNonQuery( m_BrokerId, "CreateReportQuery" , 0, parameters);
                }
                catch( Exception e )
                {
                    // Note to self: something exploded.

                    Tools.LogError( "Loan reporting: Exception caught during create copy of query.", e );
                    throw;
                }

                // We redirect to the new, duplicated query.

                Response.Redirect( "LoanReportEditor.aspx?id=" + copy.Id.ToString() + "&mode=edit" + "&rt=" + m_rt, false );
            }
            catch( Exception e )
            {
				// Write out the exception to our log.  We also roll
				// back any changes that may have occurred.

				m_ExceptionDetails = "Error saving query.";

				m_Phase.Value = "Error";

				Tools.LogError( e );

				m_Query = rollBack;
            }
        }

        /// <summary>
        /// Run the local report and pop open a results window.
        /// </summary>

        protected void RunReport( object sender , System.EventArgs a )
        {
            // We use the current query to invoke our query processor.
            // The results are cached as a report and we redirect to
            // the report viewing page.

            Guid task = Guid.NewGuid(); DateTime dtag = DateTime.Now; Tools.LogInfo( "Loan reporting: Running report (" + task.ToString() + ")..." );

            try
            {
                // Generate the report and cache it.  When we postback,
                // we will pass the id to the popup window through the
                // window's query string.

                LoanReporting lR = new LoanReporting();

                if (m_Query.Columns.Count > 0)
                {
                    Report rA = lR.RunReport(m_Query, BrokerUser, !BrokerUser.HasPermission(Permission.AllowExportingFullSsnViaCustomReports), E_ReportExtentScopeT.Default, enforceReportSizeCap:true);

                    if (rA != null)
                    {
                        rA.Label.Title = m_Query.Label.Title;
                        rA.Id = Guid.NewGuid();

                        // 05/12/06 mf - OPM 2582 & 4668.  To reduce server memory usage
                        // do not use server cache for custom reports.
                        //						Cache.Insert( rA.Id.ToString() , rA , null , DateTime.Now.AddMinutes( 30 ) , Cache.NoSlidingExpiration , CacheItemPriority.NotRemovable , null );

                        AutoExpiredTextCache.AddToCacheByUser(BrokerUser, (string)rA, TimeSpan.FromMinutes(30), rA.Id);
                        ClientScript.RegisterHiddenField("m_ReportCache", rA.Id.ToString());
                    }


                    else
                    {
                        throw new CBaseException(ErrorMessages.ProblemWithReportRun, "Report run returned null.");
                    }
                }
                else
                {
                    m_ExceptionDetails = "Empty report -- please specify fields in your report layout.";
                }
            }
            catch (PermissionException e)
            {
                m_ExceptionDetails = "Permission Denied - Unable to generate report.\nTo generate this report, you need to remove all restricted fields.";

                Tools.LogError(e);
            }
            catch (CBaseException e)
            {
                m_ExceptionDetails = e.UserMessage;

                Tools.LogError(e);
            }
            catch (Exception e)
            {
                // Write out the exception to our log.

                m_ExceptionDetails = "Error running report.";

                Tools.LogError(e);
            }
            finally
            {
                // Notify log we have finished.  If we don't see this
                // message in the log, then we are still running.

                Tools.LogInfo( "Loan reporting: Done running in " + DateTime.Now.Subtract( dtag ).TotalMilliseconds + " ms (" + task.ToString() + ")." );
            }
        }

        /// <summary>
        /// Restore previously saved query state.
        /// </summary>

        protected override void LoadViewState( object s )
        {
            // Retrieve query and schema from the web page.  If not,
            // found, we use the default, current instances.

            base.LoadViewState( s );

            try
            {
                // Lookup previously cached postback state.

                if( ViewState[ "Query" ] != null )
                {
                    m_Query = ViewState[ "Query" ].ToString();
                }
            }
            catch( Exception e )
            {
                m_ExceptionDetails = "Error loading cached contents.";

                Tools.LogError( "Loan reporting: Exception caught during query load view state.", e );
            }
        }

        /// <summary>
        /// Write out current query state.  This will get embedded
        /// in the web page content, so no need to worry about cache
        /// expiration or hogging server memory in between postbacks.
        /// </summary>

        protected override object SaveViewState()
        {
            // Write out the current query before saving the view state.

            try
            {
                // Serialize the loan query as an xml document and place
                // it in the encoded view state.

                ViewState.Add( "Query" , m_Query.ToString() );
            }
            catch( Exception e )
            {
                m_ExceptionDetails = "Error saving contents to cache.";

                Tools.LogError( "Loan reporting: Exception caught during save view state." );
                Tools.LogError( e );
            }

            return base.SaveViewState();
        }

        /// <summary>
        /// Reconnect our ui components with the current
        /// data objects for query state and lookup.
        /// </summary>

        public void BindView()
        {
            // Bind query ui elements.
            BrokerDB brokerDB = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;

            m_ReportLabel.DataSource = m_Query;
            m_ReportLabel.BindView();

            m_CommentEdit.DataSource = m_Query;
            m_CommentEdit.BindView();

			m_SearchIncld.DataSource = m_Query;
			m_SearchIncld.DataLookup = m_Loans;

            m_SearchIncld.BindView(brokerDB);

            m_FieldFnGrid.DataSource = m_Query;
            m_FieldFnGrid.DataLookup = m_Loans;
            m_FieldFnGrid.BindView(brokerDB);

            m_FieldLayout.DataSource = m_Query;
            m_FieldLayout.DataLookup = m_Loans;
            m_FieldLayout.BindView(brokerDB);

            m_FieldSorter.DataSource = m_Query;
            m_FieldSorter.DataLookup = m_Loans;
            m_FieldSorter.BindView();

			m_FieldGroups.DataSource = m_Query;
			m_FieldGroups.DataLookup = m_Loans;
			m_FieldGroups.BindView();
			
			m_CondViewing.DataSource = m_Query;
            m_CondViewing.DataLookup = m_Loans;
            m_CondViewing.BindView();

            m_CondSetting.DataSource = m_Query;
            m_CondSetting.DataLookup = m_Loans;
            m_CondSetting.BindView();

            m_OptionEntry.DataSource = m_Query;
            m_OptionEntry.DataLookup = m_Loans;
            m_OptionEntry.BindView();

            m_FilterRange.DataSource = m_Query;
            m_FilterRange.BindView();

            m_FilterActiv.DataSource = m_Query;
            m_FilterActiv.BindView();

            m_FilterInact.DataSource = m_Query;
            m_FilterInact.BindView();

            m_FilterOther.DataSource = m_Query;
            m_FilterOther.BindView();

            m_Query.DisplayTablesOnSeparatePages = PrintGroupsOnSeparatePages.Checked;

			// 2/2/2005 kb - BUGBUG: We need to get a cleaner ui for
			// specifying the inclusion of unit columns in a crossed
			// report.  Apb wants units for sure, but this ui is
			// clunky.  Closer to ideal is an entirely different view
			// of the layout that shows the crossing effects and allows
			// the user to specify what columns to show...

			if( IsPostBack == false )
			{
				if( m_Query.Options.Has( "UseUnitCols" ) == true )
				{
					m_UseUnitCols.Checked = true;
				}
				else
				{
					m_UseUnitCols.Checked = false;
				}
			}
			else
			{
				if( m_UseUnitCols.Checked == false )
				{
					m_Query.Options.Nix( "UseUnitCols" );
				}
				else
				{
					m_Query.Options.Add( "UseUnitCols" );
				}
			}
            
        }

        /// <summary>
        /// Initialize this page.
        /// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
            // Bind our user controls to the respective
            // query aspects.

            try
            {
                // Bind query ui elements.

                BindView();
            }
            catch( Exception e )
            {
                m_ExceptionDetails = "Error loading web form.";

                Tools.LogError( "Loan reporting: Exception caught during page load." );
                Tools.LogErrorWithCriticalTracking( "Loan reporting load:" , e );
            }
        }

        protected void PagePreRender(object sender, System.EventArgs a)
        {
            if (!m_Query.Relates.hasFieldsAccess(BrokerUser))
            {
                ClientScript.RegisterHiddenField("g_CondRestrictedFields", "true");
            }

            if (m_Query.hasSecureFields && !m_Query.CanUserRun(BrokerUser))
            {
                m_RunReport.Attributes.Add("disabled", "true");
            }
            else
            {
                m_RunReport.Attributes.Remove("disabled");
            }
        }

        /// <summary>
        /// Initialize this page.
        /// </summary>

		protected void PageInit( object sender , System.EventArgs a )
		{
            // Get this page ready to go for the current request.
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("customreport.css");
            this.RegisterJsScript("utilities.js");
            try
            {
                // Initialize loan reporting members.

				LoanReporting lR = new LoanReporting();

                m_Query = new Query();

                m_Loans = lR.Fields;

                // Initialize event handlers here (they keep getting
                // deleted in the web designer generated section).

                m_CondViewing.Dirty += new QueryUpdateEventHandler( QueryHasBeenUpdated );
                m_CondSetting.Dirty += new QueryUpdateEventHandler( QueryHasBeenUpdated );
                m_FilterActiv.Dirty += new QueryUpdateEventHandler( QueryHasBeenUpdated );
                m_FilterInact.Dirty += new QueryUpdateEventHandler( QueryHasBeenUpdated );
                m_FilterOther.Dirty += new QueryUpdateEventHandler( QueryHasBeenUpdated );

                // Initialize button controls state based on mode.

                if( m_IsInDemoMode == true )
                {
                    m_SaveQuery.Text    = "Save new";

                    m_Emode.Value = "Demo";
                }

				if( m_IsInViewMode == true )
				{
					m_SaveQuery.Text    = "Save new";

					m_Emode.Value = "View";
				}
				
				if( m_IsInEditMode == true )
                {
                    m_Emode.Value = "Edit";
                }

                // 9/6/2005 kb - We had some confusion as to how to get
                // the referring page so we can redirect back there on
                // close of the editor.  Because we use multiple ways of
                // posting back to this editor, the only safe way to
                // capture the url referrer is to explicitly list it in
                // the query string for this editor.  We only pull it
                // out if it's currently undefined.

                // OPM 247106 - Querystring should not contained raw return URL. Use mapped value instead.
                string rt = RequestHelper.GetSafeQueryString("rt").TrimWhitespaceAndBOM();
                if (string.IsNullOrWhiteSpace(m_rt) && !string.IsNullOrWhiteSpace(rt))
                {
                    m_rt = rt;
                }

                // Fetch the query from the database, or make a new one,
                // depending on what we have in the query string for this
                // page.  If the user lacks access, then we don't let
                // them save.  Anyone can open.

                if ( IsPostBack == false )
                {
                    // Figure out whether we're showing an existing query,
                    // or initializing empty for a new one.

                    m_Query.Label.Title = "New report...";
                    m_Query.Id          = Guid.NewGuid();

					if( RequestHelper.GetSafeQueryString( "id" ) != null )
                    {
                        // Initialize with the query given by the id.  We
                        // make sure that the broker ids match before
                        // displaying.  We also allow for reading from the
                        // admin broker's report set when in demo mode.

                        List<SqlParameter> pA = new List<SqlParameter>();

						pA.Add( new SqlParameter( "@QueryID" , new Guid( RequestHelper.GetSafeQueryString( "id" ) ) ) );

                        Guid brokerId = Guid.Empty;
                        if( m_IsInEditMode == true )
                        {

                            pA.Add( new SqlParameter( "@BrokerID" , m_BrokerId ) );
                            brokerId = m_BrokerId;
                        }

						if( m_IsInViewMode == true )
						{
                            // If it's a sample report, we'll need to look at the pml dojo.
                            if (m_isSampleReport)
                            {
                                pA.Add(new SqlParameter("@BrokerID", m_PmlDojoId));
                                brokerId = m_PmlDojoId;
                            }
                            else
                            {
                                pA.Add(new SqlParameter("@BrokerID", m_BrokerId));
                                brokerId = m_BrokerId;
                            }
						}
						
						if( m_IsInDemoMode == true )
                        {
                            //pA.Add( new SqlParameter( "@BrokerID" , m_BAdminId ) );
							pA.Add( new SqlParameter( "@BrokerID" , m_PmlDojoId ) );
                            brokerId = m_PmlDojoId;
                        }

						using( IDataReader sR = StoredProcedureHelper.ExecuteReader( brokerId, "GetReportQuery" , pA ) )
						{
							if( sR.Read() == true )
							{
                                // Stream in the query as an xml document and convert
                                // it to our query.

                                m_Query = sR[ "XmlContent" ].ToString();
                            }
                        }

                        // Reset the dirty flag because we just loaded a query.

                        m_State.Value = "Saved";

                        m_Phase.Value = "";
                        m_Dirty.Value = "";
                    }
                    else
                    {
                        // No query specified, so assume new query.  We initialize
                        // the query to a default state here.  This includes setting
                        // default filter conditions to include active loans.

                        Guid blankId  = GetBlankReportId();


						if( blankId == Guid.Empty )
						{
							Tools.LogError( "Loan reporting: Unable to query blank report -- no id found." );
                            return;
						}

						// Get the template query and copy its content.

                        List<SqlParameter> pA = new List<SqlParameter>();

                        pA.Add(new SqlParameter("@BrokerID", BrokerDB.AdminBr));
                        pA.Add(new SqlParameter("@QueryID", blankId));

                        using (IDataReader sR = StoredProcedureHelper.ExecuteReader(BrokerDB.AdminBr, "GetReportQuery", pA))
						{
							if( sR.Read() == true )
							{
                                // Stream in the query as an xml document and convert
                                // it to our query.

                                Guid savedid = m_Query.Id;

                                m_Query = sR[ "XmlContent" ].ToString();

                                // Restore our default, new query id.

                                m_Query.Id = savedid;
                            }
                        }

                        // Set the dirty flag because we just created a query.

                        m_Dirty.Value = "Dirty";

                        m_Phase.Value = "New";
                        m_State.Value = "New";
                    }

                    PrintGroupsOnSeparatePages.Checked = m_Query.DisplayTablesOnSeparatePages;
    		    }                
            }
            catch( Exception e )
            {
                m_ExceptionDetails = "Error initializing web form.";

                Tools.LogError( "Loan reporting: Exception caught during page init." );
                Tools.LogError( e );
            }
		}

        private Guid GetBlankReportId()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerDB.AdminBr)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerDB.AdminBr, "GetAdminBrokerReportQueryTemplateId", parameters))
            {
                if (reader.Read())
                {
                    return (Guid)reader["QueryId"];
                }
            }
            return Guid.Empty;
        }

        /// <summary>
        /// Gets the return URL based on page properties.
        /// </summary>
        /// <returns></returns>
        protected string GetReturnUrl()
        {
            switch (m_rt)
            {
                // todo: at some point get rid of all the m_rt references, as we no longer use PublishedReports.aspx.
                case "0":
                    return "LoanReports.aspx";
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unexpected rt value: " + m_rt);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//

            EnableJqueryMigrate = false;
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterVbsScript("common.vbs");
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
            this.PreRender += new System.EventHandler(this.PagePreRender);

        }
		#endregion

	}

}
