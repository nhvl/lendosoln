﻿<%@ Page language="c#" Codebehind="ChooseBatchExportReport.aspx.cs" AutoEventWireup="false" Inherits="LendersOffice.ChooseBatchExportReport" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="ChooseReport" Src="..\Portlets\ChooseReport.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
	<head>
		<title>Choose Pipeline Report</title>
	<link href="../../css/stylesheet.css" rel="stylesheet">
	<style>
		.highlightreport { BACKGROUND-COLOR: white; }
		form { margin-bottom: 0; }
		</style>
	</head>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto;" onload="onInit();">
    <h4 class="page-header">Choose custom report format</h4>
		<form id="ChoosePipeline" method="post" runat="server">
	<table cellpadding="4" cellspacing="0" border="0" style="width:100%; height:100%; width: calc(100vw - 4px);">
			<tr height="100%" valign="top">
			<td>			
                <uc:ChooseReport runat="server" ID="ChooseReport" OnReportChosen="ChooseReport_OnReportChosen" OnReportError="ChooseReport_OnReportError"  />
			</td>
			</tr>
			<tr>
			<td>
				<div style="PADDING: 4px; TEXT-ALIGN: center; WIDTH: 100%;">
				    <input type="button" value="Cancel" style="WIDTH: 60px;" onclick="onClosePopup();">
				</div>
			</td>
			</tr>
			</table>
		</form>

<script type="text/javascript">
    function onInit() {
        resize(400, 500);
        var errorMessage = document.getElementById('m_errorMessage');
        if (errorMessage != null && errorMessage.value != "") {
            alert(errorMessage.value);
        }
    }

    if (ML.SelectedReport) {
        var args = window.dialogArguments || {};
        args.ReportID = ML.SelectedReport;
        onClosePopup(args);
    }
</script>
</body>
</html>
