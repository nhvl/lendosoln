﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOffice.Accounting;

namespace LendersOfficeApp.los.Reports
{
    public partial class AccountingReport : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (BrokerUserPrincipal.CurrentPrincipal.HasRole(E_RoleT.Accountant) == false)
            {
                throw new AccessDenied();
            }
            if (Request.QueryString["cmd"] == "download")
            {
                GenerateReport();
            }
        }

        private void GenerateReport()
        {
            DateTime startDt = DateTime.Now.AddMonths(-1);
            DateTime endDt = DateTime.Now;

            if (string.IsNullOrEmpty(Request.QueryString["start"]) == false)
            {
                DateTime.TryParse(Request.QueryString["start"], out startDt);
            }
            if (string.IsNullOrEmpty(Request.QueryString["end"]) == false)
            {
                DateTime.TryParse(Request.QueryString["end"], out endDt);
            }
            string message = null;
            string csv = AccountReport.Generate(BrokerUserPrincipal.CurrentPrincipal.BrokerId, startDt, endDt, out message);
            Response.ClearContent();
            Response.ContentType = "text/comma-separated-values";
            Response.AddHeader("Content-Disposition", "attachment; filename=\"Accounting.csv\"");
            Response.Output.Write(csv);
            Response.Flush();
            Response.End();

        }
    }
}
