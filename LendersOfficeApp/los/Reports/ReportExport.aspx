<%@ Page language="c#" Codebehind="ReportExport.aspx.cs" AutoEventWireup="false" EnableSessionState="True" Inherits="LendersOffice.ReportExport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<HTML>
<HEAD>
    <TITLE>
        ReportExport
    </TITLE>
    <META name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <META name="CODE_LANGUAGE" Content="C#">
    <META name=vs_defaultClientScript content="JavaScript">
    <META name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
</HEAD>
<BODY MS_POSITIONING="FlowLayout" onload="onInit();">
    <SCRIPT language=javascript>

        <!--

        //
        // function:
        //
        // onInit
        //
        // description:
        //
        // Initialize the export report view in case no attachment
        // arrives from the page.
        //

        function onInit()
        {
            // Update the message text on error.  We should only get
            // this far if the code-behind rendered normally, which
            // means the attachment never worked.
            
            <% if (m_reload) { %>
            setTimeout("PostBack();", 1000);
            <% } %>

            if( document.getElementById("m_ExportError").value != "Undefined" )
            {
                m_Message.innerHTML = "Export error: " + document.getElementById("m_ExportError").value;
            }
        }
        
        <% if (m_reload) { %>
        function PostBack()
        {
            if (typeof(__doPostBack) == 'function')
                __doPostBack('','');
        }
        <% } %>
        // -->

    </SCRIPT>
    <FORM id="ReportExport" method="post" runat="server">
        <INPUT id="m_ExportError" type="hidden" value="Undefined" name="m_ExportError" runat="server"> 
        <INPUT id="m_ExportLabel" type="hidden" value="Undefined" name="m_ExportLabel" runat="server"> 
        <DIV style="BORDER: midnightblue 2px solid; PADDING: 0.2in; FONT: bold 11pt arial; WIDTH: 100%; BACKGROUND-COLOR: whitesmoke; TEXT-ALIGN: center">
            <div id="m_Message"></div>
        </DIV>
    </FORM>
</BODY>
</HTML>
