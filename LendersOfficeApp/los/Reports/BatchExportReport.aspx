<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchExportReport.aspx.cs" Inherits="LendersOffice.BatchExportReport" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style>
        #m_submittedPanel {
            display: none;
        }
    </style>
</head>
<body style="background:gainsboro">
    <form id="form1" runat="server">
    <div>
        <asp:Panel id="m_headerPanel" runat="server" style="WIDTH: 100%; PADDING: 0px;">
            <table>
                <tr>
                    <td style="width:100%">
                        Now showing:&nbsp;<ml:EncodedLabel runat="server" ID="m_reportName" />
                        &nbsp;<a href="#" onclick="onChooseClick();">(change)</a>
                    </td>
                    <td style="width:50px">
                        <asp:Button ID="m_refreshBtn" runat="server" Text="Refresh" style="text-align:right;" />
                    </td>
                </tr>
            </table>
            
	    </asp:Panel>
	    
	    <input id="m_reportIdField" type="hidden" runat="server" />
	    <input id="m_sortOrder" type="hidden" runat="server" />
	    <input id="m_selectedLoanList" type="hidden" runat="server" />
	    <asp:Panel runat="server" ID="m_reportPanel" Visible="false">
        <asp:Panel id="m_rowsPanel" runat="server" style="BORDER: 0px solid lightgray; WIDTH: 100%; PADDING: 0px;">
            <div width="100%" style="text-align:right;"><span id="selectedLoansLabel"></span> of 
            <ml:EncodedLabel runat="server" ID="m_numLoans" /> loans selected.
            &nbsp;&nbsp;&nbsp;
        <input type="button" onclick="f_goToFirst();" value="<<" />
        <input type="button" onclick="f_goToPrev();" value="<" />
		<input type="text" class="pagedisplay" id="pageDisplayTb" readonly="readonly" style="text-align:center"/>
		<input type="button" onclick="f_goToNext();" value=">" />
        <input type="button" onclick="f_goToLast();" value=">>" />
            
	        </div>

            <table id="mainGrid" cellpadding="2" cellspacing="1" border="0" width="100%" class="tablesorter">
                <thead>
                    <tr class="GridHeader">
                        <asp:Repeater ID="m_columnRepeater" runat="server" EnableViewState="False">
                            <HeaderTemplate><th><input type="checkbox" id="checkAllCb" /></th></HeaderTemplate>
                            <ItemTemplate>
                                <th style="cursor:pointer; color:blue; text-decoration:underline"><%# AspxTools.HtmlString(Container.DataItem.ToString()) %></th>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </thead>
                <tbody>
                <asp:Repeater ID="m_rowRepeater" runat="server" EnableViewState ="false">
                <ItemTemplate>
                <tr>
                <td><input type="checkbox" class="row_checkbox" value=<%# AspxTools.HtmlAttribute(Eval("Key").ToString()) %>/></td>
                    <asp:Repeater ID="m_rowCellRepeater" runat="server" EnableViewState="false">

                    <ItemTemplate><td><%# AspxTools.HtmlString(Eval("Text").ToString()) %></td></ItemTemplate>
                    </asp:Repeater>
                </tr>
                </ItemTemplate>
                </asp:Repeater>
                </tbody>
            </table>

        </asp:Panel>
        <asp:Panel id="m_submitPanel" runat="server" style="WIDTH: 100%; PADDING: 0px;">
            <div style="float:left;margin:0px 5px;">Send export complete notification to 
                <asp:TextBox runat="server" ID="m_exportCompleteEmail" />
                <asp:CheckBox runat="server" ID="m_exportCompleteEmailChecked" />
            </div>
            <div style="float:right;margin:0px 5px;">
                <span>
                    <span id="m_customFieldControls" runat="server">
                    <asp:CheckBox runat="server" ID="m_setCustomField"/>
                    Set <ml:EncodedLabel runat="server" ID="m_customFieldName"/> to
                    <asp:TextBox runat="server" ID="m_customFieldValue"></asp:TextBox>
                    </span>
                    <button runat="server" ID="m_exportBtn" onclick="return requestBatchExport();" type="button"/>
                </span>
            </div>
        </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="m_submittedPanel" runat="server" style="position:absolute;top:0;left:0;width:100%; height:100%;">
            <div style="padding:15px;">
                <p>
                    Your export is being processed. We will send a notification to <ml:EncodedLabel runat="server" ID="m_submittedEmail" style="font-weight:bold" /> when it is complete, or if we encounter any problems in creating your export.
                </p>
                <p>
                    Thank you for using LendingQB.
                </p>
                <input type="submit" value="Close" onclick="onClosePopup();" />
            </div>
        </asp:Panel>
    </div>
    </form>
    <script type="text/javascript">

        function f_onChange_emailLocked() {
            var checked = $('#m_exportCompleteEmailChecked').prop('checked'); 
            if (!checked)
            {
                if (<%= AspxTools.JsGetElementById(m_exportCompleteEmail) %> != null)
                {
                    $('#m_exportCompleteEmail').prop('readOnly', true); 
                    formatReadonlyField(<%= AspxTools.JsGetElementById(m_exportCompleteEmail) %>);
                }
            }
            else
            {
                $('#m_exportCompleteEmail').prop('readOnly', false).css({'background-color':''});
                
            }
            
        }
        function f_onChange_customField() {
            var checked = $('#m_setCustomField').prop('checked'); 
            if (!checked)
            {
                if (<%= AspxTools.JsGetElementById(m_customFieldValue) %> != null)
                {
                    $('#m_customFieldValue').prop('readOnly', true); 
                    formatReadonlyField(<%= AspxTools.JsGetElementById(m_customFieldValue) %>);
                }
            }
            else
            {
                $('#m_customFieldValue').prop('readOnly', false).css({'background-color':''}); 
            }
        }
        function onChooseClick()
        {
            showModal( "/los/Reports/ChooseBatchExportReport.aspx", null, null, null, function(args){
                if( args != null && args.ReportID != null )
                {
			        <%= AspxTools.JsGetElementById(m_reportIdField) %>.value = args.ReportID;
                        document.getElementById("m_selectedLoanList").value = null;
			        <%= AspxTools.JsGetElementById(m_refreshBtn) %>.click();
		        }
            }, {hideCloseButton:true});
        }

		
		
        function _init() {

            $('#m_exportCompleteEmailChecked').click(f_onChange_emailLocked); 
            $('#m_setCustomField').click(f_onChange_customField); 
            f_onChange_emailLocked();
            f_onChange_customField();

            
            var isAllowTableSort = <%= AspxTools.JsBool(m_isAllowTableSort) %>;
            if (isAllowTableSort && typeof g_sortTypeList != "undefined")
            {
                var headersOption = { 0: { sorter:false}};
                for (var i = 0; i < g_sortTypeList.length; i++)
                {
                    headersOption[i + 1] = {sorter:g_sortTypeList[i] == 'false' ? false : g_sortTypeList[i]};
                }
                $('#mainGrid').tablesorter({headers:headersOption});
                $('#mainGrid').on('sortEnd', function() { f_goToPage(g_currentPage); });
            }
            $('#checkAllCb').click(function() { f_checkAll($(this).prop('checked')); });
            
            $(document).on('click', '.row_checkbox', function() { f_updateSelectLabel();});
            
            f_goToFirst();
            f_updateSelectLabel();

        }
        
        
        function f_checkAll(bChecked) 
        {
            bChecked = !!bChecked; // Convert to bool
            $('.row_checkbox').prop('checked', bChecked);
            f_updateSelectLabel();
        }
        function f_updateSelectLabel()
        {
            var count =$('.row_checkbox:checked').length;
            $('#m_exportBtn').prop('disabled', count == 0); 
            $('#selectedLoansLabel').text(count);
        }
        function requestBatchExport()
        {
        	var selectedLoanList = [];
	        $('.row_checkbox:checked').each(function(i, elem) {
	            selectedLoanList.push($(elem).val());
	        });

	        var customFieldChecked = $("#m_setCustomField").prop("checked");
	        var customFieldValue = $("#m_customFieldValue").val();

	        var args = {
	            selectedIdsJson: JSON.stringify(selectedLoanList),
	            customFieldChecked: customFieldChecked == undefined ? "" : customFieldChecked,
	            customFieldValue: customFieldValue == undefined ? "" : customFieldValue,
	            type: ML.type,
	            email: $("#m_exportCompleteEmail").val()
	        };

	        var result = gService.batchExport.call("BatchExportReports", args);

	        if(result.error) {
	            alert(result.UserMessage || 'System error.  Please contact us if this happens again');
	        }
	        else {
                $("#m_headerPanel").toggle(false);
                $("#m_reportPanel").toggle(false);
                $("#m_submittedPanel").toggle(true);
                $("#m_submittedEmail").text(result.value.email);
	        }
        }

        
        var g_pageSize = 100;
        var g_currentPage = 0;
        var g_totalItemsCount = <%= AspxTools.JsNumeric(m_itemCount) %>;
        var g_totalPages = Math.ceil(g_totalItemsCount / g_pageSize) - 1;
        
        function f_goToFirst()
        {
            f_goToPage(0);
        }
        function f_goToLast()
        {
            f_goToPage(g_totalPages);
        }
        function f_goToPrev()
        {
            f_goToPage(g_currentPage - 1);
        }
        function f_goToNext()
        {
            f_goToPage(g_currentPage + 1);
        }
        function f_goToPage(pg)
        {
            if (pg < 0)
            {
                g_currentPage = 0;
            }
            else if (pg > g_totalPages)
            {
                g_currentPage = g_totalPages;
            }
            else
            {
                g_currentPage = pg;
            }
            $('#pageDisplayTb').val('Page ' + (g_currentPage + 1) + ' of ' + (g_totalPages + 1));
            var startIndex = g_pageSize * g_currentPage;
            
            var css = ['GridItem', 'GridAlternatingItem']
            $('#mainGrid > tbody > tr').each(function(i, elem)
            {
                var endIndex = startIndex + g_pageSize;
                
                var bVisible = i >= startIndex && i <= endIndex;
                $(elem).toggle(bVisible);
                if (bVisible)
                {
                var odd = (i % 2 == 0)
                $(elem).removeClass(
                css[odd ? 0 : 1]).addClass(
                css[odd ? 1 : 0]);
                }
            });
            
        }
    </script>
</body>
</html>
