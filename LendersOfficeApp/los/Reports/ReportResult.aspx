<%@ Page Language="c#" CodeBehind="ReportResult.aspx.cs" AutoEventWireup="false"
    Inherits="LendersOffice.ReportResult" EnableViewState="false" %>

<%@ Register TagPrefix="UCR" TagName="TemplatedUserControl" Src="../QueryProcessor/TemplatedUserControl.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>Report Result </title>
    <style type="text/css">
        /* .layoutkey */.a
        {
            background-color: lavender;
        }
        /* .layoutsub */.b
        {
            background-color: palegoldenrod;
        }
        /* .layoutdata */.c
        {
            background-color: whitesmoke;
        }
        /* .layoutalternatingdata */.d
        {
            background-color: lightgrey;
        }
        /* .layoutresult */.e
        {
            background-color: lightsteelblue;
        }
        /* .layoutdefault */.f
        {
            background-color: whitesmoke;
        }
        /* .alignnumerical */.g
        {
            text-align: right;
        }
        /* .aligndefault */.h
        {
            text-align: left;
        }
        #ReportHeader
        {
            width: 100%;
        }
        #ReportHeader h1
        {
            font: bold 14pt arial;
            border: 0px;
        }
        #ReportHeader img
        {
            float: right;
        }
        .Header
        {
            border: lightgray 2px solid;
            padding: 0.1in;
            width: 100%;
            background-color: whitesmoke;
        }
        #m_Header
        {
            display: none;
        }

    </style>
</head>
<body onload="onInit();" ms_positioning="FlowLayout">

    <script type="text/javascript">

        <!--
        /*<%--
        //
        // function:
        //
        // onInit
        //
        // description:
        //
        // Initialize the report view.  We have two states: initialize
        // the report, or display the report.  We have the query string
        // as part of the dialog arguments.  If there, we postback to
        // the server with this query in a hidden variable.  If it's
        // not present, then we have the report already, so we pass
        // through to the client.
        //
        --%>*/
        function onInit()
        {
            /*<%--
            // Initialize display items by pulling content from
            // our hidden variable set.
            --%>*/
            try
            {
                /*<%--
                // Update the title of the document with that of our
                // current report.  This is helpful for printing.
                --%>*/
                document.title = document.getElementById("m_ReportTitle").value;
                /*<%--
                // This must be the return visit.  We expect that
                // the query was already processed.
                --%>*/
                m_Message.textContent = "Processing complete ( " + document.getElementById("m_ResultCount").value + " ).";
                m_Date.innerHTML    = document.getElementById("m_ProcessDate").value;

                if( document.getElementById("m_ReportError").value != "Undefined" )
                {
                    m_Message.innerHTML = "Failed: " + document.getElementById("m_ReportError").value + ".";
                }

                m_Divider.style.display = "block";
                m_Header.style.display  = "block";
                
                if(document.getElementById("m_PrintReport").value == "true")
                {
                    document.getElementById("m_Header").style.display = "none";
                    onPrePrint();
                }
            }
            catch( e )
            {
                m_Message.innerHTML = "Failed: " + e.description + ".";
            }
        }
        /*<%--
        //
        // function:
        //
        // onPrintAll
        //
        // description:
        //
        // Print the entire report.
        //
        --%>*/
        function onPrintAll()
        {
            /*<%--
            // Walk the document and set pagebreaks on all the
            // tables.
            --%>*/
            var i , c , pagebreak = "auto";

            if( document.getElementById("m_ProcessOpts") != null )
            {
                if( document.getElementById("m_ProcessOpts").value.indexOf( "break on grouping" ) != -1 )
                {
                    pagebreak = "always";
                }
            }

            for( i = 0 , c = 0 ; i < LoanReportPopup.children.length ; ++i )
            {
                var block = LoanReportPopup.children[ i ];

                if( block.id != "group" )
                {
                    continue;
                }

                block.style.pageBreakAfter = pagebreak;

                ++c;
            }
            /*<%--
            // Setup the postprint handler to clean up the table
            // display by restoring each table's display style.
            --%>*/
            window.onbeforeprint = onPrePrint;
            window.onafterprint = onPostPrint;
            /*<%--
            // Initiate a print of this page.  Only the specified
            // table should be present, so only that table should
            // get printed.
            --%>*/
            window.print();
        }
        /*<%--
        //
        // function:
        //
        // onPrintTable
        //
        // description:
        //
        // We print the currently specified table.  We need to
        // navigate the report and hide all the tables that
        // are present.  Once hidden, we show the specified
        // table and print. 
        //
        --%>*/
        function onPrintTable( objGroup )
        {
            // Walk the document and turn off all tables.

            var i;

            for( i = 0 ; i < LoanReportPopup.children.length ; ++i )
            {
                var block = LoanReportPopup.children[ i ];

                if( block.id != "group" )
                {
                    continue;
                }

                block.style.display = "none";
            }

            objGroup.style.pageBreakBefore = "auto";
            objGroup.style.pageBreakAfter  = "auto";

            objGroup.style.display = "block";
            /*<%--
            // Setup the postprint handler to clean up the table
            // display by restoring each table's display style.
            --%>*/
            window.onbeforeprint = onPrePrint;
            window.onafterprint = onPostPrint;
            /*<%--
            // Initiate a print of this page.  Only the specified
            // table should be present, so only that table should
            // get printed.
            --%>*/
            window.print();
        }

        function onPrePrint()
        {
            // Update the header bar's visibility.

			var i , hideMe = $('.m_Ui'); 

            m_Commands.style.visibility = "hidden";

            if( hideMe && hideMe.length )
            {
				for( i = 0 ; i < hideMe.length ; ++i )
				{
					hideMe[ i ].style.visibility = "hidden";
				}
			}
			else if(hideMe)
			{
				hideMe.style.visibility = "hidden";
			}				
        }

        function onPostPrint()
        {
            // Find all tables and restore them.

            var i , showMe = $('.m_Ui');

            for( i = 0 ; i < LoanReportPopup.children.length ; ++i )
            {
                var block = LoanReportPopup.children[ i ];

                if( block.id != "group" )
                {
                    continue;
                }

                block.style.pageBreakBefore = "auto";
                block.style.pageBreakAfter  = "auto";

                block.style.display = "block";
            }

            if( showMe && showMe.length != null )
            {
				for( i = 0 ; i < showMe.length ; ++i )
				{
					showMe[ i ].style.visibility = "visible";
				}
			}
			else if( showMe)
			{
				showMe.style.visibility = "visible";
			}				

            // Update the header bar's visibility.

            m_Commands.style.visibility = "visible";
            m_Date.style.visibility     = "visible";
        }
        /*<%--
        //
        // function:
        //
        // getMyGroup
        //
        // description:
        //
        // Find the object's containing report table.  Each table
        // must have an id of 'table'.
        //
        --%>*/
        function getMyGroup( objElement , label )
        {
            /*<%--
            // Walk up the object hierarchy until we find
            // a parent known as table.
            --%>*/
            while( objElement != null && objElement.id != label )
            {
                objElement = objElement.parentElement;
            }

            return objElement;
        }
        
        function f_saveFile(controlid, id, mode) {
            var ctr = $('#' + controlid);
            if (ctr.prop('disabled')) {
                return;
            }

            ctr.data('original-text', ctr.val());
            ctr.prop('disabled', 'disabled');
            ctr.val('Please wait...');
            var param = "&type=";
            switch(mode)
            {
                case "txt":
                    param += "txt";
                    break;
                case "csv":
                default:
                    param += "csv";
                    break;
            }

            window.setTimeout(function() { ctr.removeProp('disabled').val(ctr.data('original-text')); }, 10000);
            var url = "ReportExport.aspx?id=" + id + param;
            var win = window.open(url, '_blank', 'resizable=yes,status=no,menubar=yes,scrollbars=yes,width=780,height=500');
            if (win == null)
                alert('Unable to open new window.  This may be caused by a pop-up blocker program on your computer.  Please contact Technical Support for assistance.');
        }

        <% if (m_isMortgageCallReport) { %>
        function f_exportMortgageCallReport()
        {
          var url = 'MortgageCallReportExport.aspx?id=' + <%= AspxTools.JsString(m_reportId)%>;
          var win = window.open(url, '_blank', 'resizable=yes,status=no,menubar=yes,scrollbars=yes,width=780,height=500');
        }
        <% } %>
        
        function f_exportULDD()
        {
                          var url = 'ReportExport.aspx?id=' + <%= AspxTools.JsString(m_reportId)%> + '&type=uldd';
          var win = window.open(url, '_blank', 'resizable=yes,status=no,menubar=yes,scrollbars=yes,width=780,height=500');

        }
        // -->
jQuery(function($){
    $('#ToPdf').click(function() {
        if ($(this).prop('disabled')) {
            return; 
        }

        $(this).prop('disabled', 'disabled').val('Please wait...');
        
        var pdfWindow = window.open(window.location + "&ToPdf=t", "_blank", 'resizable=yes,status=no,menubar=yes,scrollbars=yes,width=780,height=500');
        window.setTimeout(function() {
            $('#ToPdf').removeProp('disabled').val('Export to Pdf...');
        },10000);
    });
});
    </script>

    <form id="LoanReportPopup" method="post" runat="server">
    <input id="m_ReportTitle" type="hidden" value="Undefined" name="m_ReportTitle" runat="server" />
    <input id="m_ResultCount" type="hidden" value="Undefined" name="m_ResultCount" runat="server" />
    <input id="m_ProcessTime" type="hidden" value="Undefined" name="m_ProcessTime" runat="server" />
    <input id="m_ProcessDate" type="hidden" value="Undefined" name="m_ProcessDate" runat="server" />
    <input id="m_ProcessOpts" type="hidden" value="Undefined" name="m_ProcessOpts" runat="server" />
    <input id="m_ReportError" type="hidden" value="Undefined" name="m_ReportError" runat="server" />
    <input id="m_PrintReport" type="hidden" value="Undefined" name="m_PrintReport" runat="server" />
    <div id="m_Header" class="Header">
        <table cellpadding="0" cellspacing="0" style="font: bold 9pt arial;" width="100%">
            <tr>
                <td nowrap>
                    <span id="m_Date" style="width: 100%; text-align: left"></span>
                </td>
                <td nowrap>
                    <span id="m_Commands" style="width: 100%; text-align: right">
                        <%if(ShowULDDButton){ %>
                      <input type="button" id="m_exportULDD" value="Export as ULDD ..." runat="server" onclick="f_exportULDD();" />
                      <%} %>
                      
                        <input type="button" id="m_exportMortgageCallReport" value="Export as Mortgage Call Report ..."
                            runat="server" onclick="f_exportMortgageCallReport();" />
                        <span style="margin-right: 4px;">
                            <asp:Button ID="m_ExportToCsv" Text="Export to Excel (csv)..." NoHighlight Width="130px" Font-Names="arial"
                                Font-Size="9pt" runat="server" />
                        </span><span style="margin-right: 4px;">
                            <asp:Button ID="m_ExportToTsv" Text="Export to Text (txt)..." NoHighlight Width="130px" Font-Names="arial"
                                Font-Size="9pt" runat="server" />
                        </span><span style="margin-right: 4px;">
                            <span>
                                <input type="button" style="font: 9pt arial;" value="Export to Pdf..." id="ToPdf" NoHighlight runat="server"/>
                            </span>
                        <span style="margin-right: 4px;">
                            <input type="button" style="font: 9pt arial;" value="Print..." onclick="onPrintAll();">
                        </span><span style="margin-right: 0px;">
                            <input type="button" style="font: 9pt arial;" value="Close" onclick="onClosePopup();">
                        </span>

                        </span>
                </td>
            </tr>
        </table>
    </div>
    <br id="m_Divider" style="display: none">
    <div id="ReportHeader">
        <img src="" alt="Company Logo" runat="server" id="Logo" />
        <h1 class="Header">
            <ml:EncodedLabel ID="ReportTitle" runat="server"></ml:EncodedLabel>
        </h1>
    </div>

    <asp:PlaceHolder ID="m_RenderedTable" runat="server">
    </asp:PlaceHolder>

    <asp:Repeater ID="m_ReportCalcs" runat="server" EnableViewState="False" OnItemDataBound="m_ReportCalcs_ItemDataBound">
        <ItemTemplate>
            <div id="calcs" style="width: 100%; page-break-inside : avoid">
                <table cellpadding="4px" bgcolor="Whitesmoke" border="0" width="100%" style="border: 0px midnightblue solid;
                    font: 8pt arial;">
                    <thead bgcolor="Wheat">
                        <tr>
                            <th align="left" colspan=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "FieldCount" ).ToString()) %>>
                                <span style="font: bold 9pt arial; width: 100%;"><u>
                                    <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "HeaderDescriptor" )) %>
                                </u></span>
                            </th>
                        </tr>
                        <tr>
                            <ml:PassthroughLiteral runat="server" Id="HeaderFieldNames"></ml:PassthroughLiteral>                        
                        </tr>
                    </thead>
                        <ml:PassthroughLiteral runat="server" ID="RenderedCalcs"></ml:PassthroughLiteral>
                    <tfoot bgcolor="Peru">
                        <tr>
                            <th align="right" colspan=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "FieldCount" ).ToString()) %>>
                                <table width="100%" style="font: 8pt arial; <%=AspxTools.HtmlString(UIDisplay)%>" id="m_ReportCalcsUi">
                                    <thead>
                                        <tr>
                                            <th align="left">
                                                <span class="m_Ui">
                                                    <input type="button" style="font: 9pt arial; width: 80px; border-width: 1px;" value="Print"
                                                        onclick="onPrintTable( getMyGroup( this , 'calcs' ) )">
                                                </span>
                                            </th>
                                            <th align="right" nowrap>
                                                <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "FooterDescriptor" )) %>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </th>
                        </tr>
                    </tfoot>
                </table>
                <br>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <div id="m_Message" style="border: midnightblue 0px solid; padding: 0.2in; font: bold 9pt arial;
        width: 100%; background-color: whitesmoke; text-align: center">
        <%=AspxTools.HtmlString(ProcessingText)%>
    </div>
    </form>
</body>
</html>
