﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchExport.aspx.cs" Inherits="LendersOffice.BatchExport" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <style type="text/css">
        #export_list
        {
            padding-left:14px;
            padding-top:14px;
            font-size:14px;
        }

        div.loading-message{
            display: none;
            text-align: center;
            padding: 10px;
        }

        div.ui-dialog-titlebar{
            display: none;
        }

        .loading-gif{
            width:35px;
            height: 35px;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript">
        $(document).on("click", "a", disableLink);

        $(document).keydown(preventEsc);

        // Pressing ESC will cause the browser to stop all loading, navigation, and animation.
        // We catch this event in case the user is attempting close out of the popup.
        function preventEsc(e) {
            if (e.keyCode == 27) {
                e.preventDefault();
            }
        }

        function disableLink(e) {
            var el = $(this);

            if (hasDisabledAttr(el)) {
                return;
            }

            $(".loading-message").dialog({
                height: 100,
                width: 300,
                modal: true,
                resizable: false,
                closeOnEscape: false
            });

            // The gif must be appended manually in to prevent navigation from stopping the animation.
            $(".loading-message").append($("<img class=\"loading-gif\" src=\"../../images/loading.gif\"/>"));

            setDisabledAttr(el, true);
        }
    </script>
    <form id="form1" runat="server">
                  <div class="FormTableHeader" style="padding:10px;">
            Report Formats
      </div>
        <div id="export_list">

            <ul>
                <asp:Repeater runat="server" ID="m_exportFormats">
                    <ItemTemplate>
                        <li>
                            <a href="BatchExportReport.aspx?Type=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>">
                                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString()) %>
                            </a>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:PlaceHolder runat="server" ID="m_noReportFormats" Visible="false">
                    <li>
                        No Available Formats
                    </li>
                </asp:PlaceHolder>
            </ul>
        </div>
        <div class="loading-message">
             <p>Please Wait, Loading...</p>
        </div>
    </form>
</body>
</html>
