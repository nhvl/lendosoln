﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountingReport.aspx.cs" Inherits="LendersOfficeApp.los.Reports.AccountingReport" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Accounting Report</title>
</head>
<body bgcolor="gainsboro" style="padding-left:5px;">
  <script type="text/javascript">
    function f_generate()
    {
      var dtStart = <%= AspxTools.JsGetElementById(m_fromDate) %>.value;
      var dtEnd = <%= AspxTools.JsGetElementById(m_toDate) %>.value;
      window.open('AccountingReport.aspx?start=' + dtStart + '&end=' + dtEnd + '&cmd=download', '_parent');
    }
    function f_close() {
      onClosePopup();
    }
  </script>
    <form id="form1" runat="server">
    <div>
    <div class="MainRightHeader">Accounting Report</div>
    <br />
    <div>Funding Period: From <ml:DateTextBox ID="m_fromDate" runat="server" /> - To <ml:DateTextBox ID="m_toDate" runat="server" /></div>
    <br />
    <input type="button" value="Generate Report" onclick="f_generate();" />&nbsp;&nbsp;
    <input type="button" value="Close" onclick="f_close();" />
    </div>
    </form>
</body>
</html>
