﻿<%@ Page Language="C#" EnableEventValidation="true" AutoEventWireup="true" CodeBehind="SubservicingExport.aspx.cs" Inherits="LendersOfficeApp.los.Reports.SubservicingExport" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
   
    <script type="text/javascript">
        var downloadStatusCheckId;
        var cookieId;
        
        function exportClick(dlType) {            
            var rows = $('#m_grid').prop("rows");
            var loanIds = "";
            if (rows != null) {
                for (var i = 1; i < rows.length; i++) {
                    if (rows[i].cells[0].children[0].checked) {
                        loanIds += rows[i].cells[0].children[0].value + ",";
                    }
                }
            }

            document.getElementById("m_downloadInfo").value = dlType + "," + loanIds;
            cookieId = new Date().getTime();
            var temp = cookieId;
            document.getElementById("m_downloadTokenId").value = cookieId;
            // Call setTimeout to avoid case where download completes before the 500 millisecond timer
            // for page unload (ie the animation appears after the the download is complete)
            window.setTimeout(startCheckingDownloadStatus, 250);
        }

        function startCheckingDownloadStatus() {
            downloadStatusCheckId = setInterval(checkIfDownloadComplete, 100);
        }

        function checkIfDownloadComplete() {
            if (compareCookie('downloadToken', cookieId)) {
                $("#m_Wait").css("visibility", "hidden");
                clearInterval(downloadStatusCheckId);                
            }
        }

        function checkForSelected() {
            var rows = $('#m_grid tr');
            var Eunchecked = false, Echecked = false;
            if (rows != null) {
                for (var i = 1; i < rows.length; i++) {
                    if (rows[i].cells[0].children[0].checked) {
                        Echecked = true;
                    }
                    if (!rows[i].cells[0].children[0].checked) {
                        Eunchecked = true;
                    }
                    if (Echecked && Eunchecked) {
                        break;
                    }
                }
                $('#m_ExportBtn1').prop("disabled", !Echecked);
                $('#m_ExportBtn2').prop("disabled", !Echecked);
                $('.HeaderItem [type="checkbox"]').prop("checked", !Eunchecked);
            }
        }
        function doSelectAll(obj) {
            var rows = $('#m_grid tr');
            for (var i = 1; i < rows.length; i++) {
                rows[i].cells[0].children[0].checked = obj.checked;
            }
            $('#m_ExportBtn1').prop("disabled", !obj.checked);
            $('#m_ExportBtn2').prop("disabled", !obj.checked);
        }

        function onBeforeUnload() {
            try {
                // Turn on the animated gif.
                window.setTimeout('$("#m_Wait").css("visibility", "visible");', 250);
            }
            catch (e) {
                window.status = e.message;
            }
        }
    </script>
    
</head>
<body onload='checkForSelected()' onbeforeunload='onBeforeUnload();' style="background-color:gainsboro">
    <form id="form1" runat="server">
        <table style='width:100%;'>
            <tr>
                <td class='FormTableHeader' style='width:100%;padding:5px;'>
                    Provident Funding Subservicing Export
	            </td>
	        </tr>
	        <tr>
	            <td>
	                <label>Show loans funded on or after </label>
	                <ml:DateTextBox runat='server' ID="m_fundedDate" AutoPostBack="true"></ml:DateTextBox>
	                <br />
	                <br />
	                <ml:EncodedLabel runat='server' ID='m_emptyDateField' Text="Please select a Funded date" style="font-weight:bold;"/>
	                <ml:EncodedLabel runat='server' ID='m_noLoansFound' Text="No Matching Loans Found" style="font-weight:bold;" Visible='false' />
	            </td>
	        </tr>
	        <tr>
	            <td>
	                <ml:CommonDataGrid runat='server' ID='m_grid' DataKeyField='sLId'>
	                    <Columns>
	                        <asp:TemplateColumn>
	                            <HeaderTemplate><asp:CheckBox runat='server' class="HeaderItem" id='selectAll' onclick="doSelectAll(this);"/></HeaderTemplate>
	                            <ItemTemplate>
	                                <input type="checkbox" id="exportCB_id" name="exportCB_name" onclick="checkForSelected(this)"
                                        value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId").ToString()) %>' NotForEdit />
	                            </ItemTemplate>
	                        </asp:TemplateColumn>
	                        <asp:BoundColumn DataField="sLNm" HeaderText="Loan Number" SortExpression="sLNm"></asp:BoundColumn>
	                        <asp:BoundColumn DataField="aBFirstNm" HeaderText="Borrower First Name" SortExpression="aBFirstNm"></asp:BoundColumn>
	                        <asp:BoundColumn DataField="aBLastNm" HeaderText="Borrower Last Name" SortExpression="aBLastNm"></asp:BoundColumn>
	                        <asp:BoundColumn DataField="sFinalLAmt" HeaderText="Total Loan Amount" SortExpression="sFinalLAmt" DataFormatString="{0:c}"></asp:BoundColumn>
	                        <asp:BoundColumn DataField="sFundD" HeaderText="Funded Date" SortExpression="sFundD" DataFormatString="{0:d}"></asp:BoundColumn>
	                        <asp:BoundColumn DataField="sLPurchaseD" HeaderText="Loan Sold Date" SortExpression="spPurchaseD" DataFormatString="{0:d}"></asp:BoundColumn>
	                        <asp:TemplateColumn HeaderText="Loan Status" SortExpression="sStatusT">
	                            <ItemTemplate>
	                                <%# AspxTools.HtmlString(FormatNullableLoanStatus(DataBinder.Eval(Container.DataItem, "sStatusT"))) %>
	                            </ItemTemplate>
	                        </asp:TemplateColumn>
	                        <asp:TemplateColumn HeaderText="Loan Type" SortExpression="sLT" >
	                            <ItemTemplate>
	                                <%# AspxTools.HtmlString(FormatNullableLoanType(DataBinder.Eval(Container.DataItem, "sLT"))) %>
	                            </ItemTemplate>
	                        </asp:TemplateColumn>
	                    </Columns>
	                </ml:CommonDataGrid>
	                <br />
	            </td>
	        </tr>
	        <tr>
	            <td>
	                <asp:Button runat='server' id='m_ExportBtn1' Text="Export Loan Sale File" OnClientClick="exportClick(0)" OnClick='ExportClick' Visible='false'/>
	                <asp:Button runat='server' id='m_ExportBtn2' Text="Export Zip File" OnClientClick='exportClick(1)' OnClick='ExportClick' Visible='false'/>
	                <asp:HiddenField runat='server' id='m_downloadInfo' Value='' />
	                <asp:HiddenField runat='server' id='m_downloadTokenId' Value='' />
	            </td>
	        </tr>
	    </table>	    
	    <div id="m_Wait" style="display:block;border-right: gainsboro 2px solid; padding-right: 0.3in;
                border-top: gainsboro 2px solid; padding-left: 0.3in; z-index: 9; background: whitesmoke;
                left: 200px; padding-bottom: 0.3in; border-left: gainsboro 2px solid; padding-top: 0.3in;
                border-bottom: gainsboro 2px solid; position: absolute; top:100px; visibility:hidden;">
                <div style="border-right: gainsboro 2px solid; padding-right: 16px; border-top: gainsboro 2px solid;
                    padding-left: 16px; padding-bottom: 16px; border-left: gainsboro 2px solid; width: 3in;
                    padding-top: 16px; border-bottom: gainsboro 2px solid; background-color: white">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <img src="Waiting.gif"/>
                            </td>
                            <td>
                                <span style="font: bold 11px arial">Processing request. Please wait... </span>
                            </td>
                        </tr>
                    </table>
                </div>
        </div>
    </form>
</body>
</html>
