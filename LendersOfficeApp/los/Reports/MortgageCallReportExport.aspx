﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MortgageCallReportExport.aspx.cs" Inherits="LendersOfficeApp.los.Reports.MortgageCallReportExport" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Mortgage Call Report Export</title>
    <style>
      .pagetitle
      {
        padding:4px;
        font-weight:bold;
        background-color:Maroon;
        color:White;
      }
    </style>
</head>
<body bgcolor="gainsboro">
<script type="text/javascript">
  function _init()
  {    
    document.getElementById("m_hfStateRevenueChanged").value = "0" ; 
    mReport_SelectedIndexChanged();    
  }
  function f_openWindowOfType(type)
  {
      if(type == 'xml')
      {
        var url = f_generateUrl() + '&type=mcr_xml';
        var win = window.open(url, '_blank', 'resizable=yes,status=no,menubar=yes,scrollbars=yes,width=780,height=500');  
      }
      else if(type == 'html')
      {
          var url = f_generateUrl() + '&type=mcr_html';
          var win = window.open(url, '_blank', 'resizable=yes,status=no,menubar=yes,scrollbars=yes,width=1024,height=768'); 
      }
  }
  function f_promptToSave()
  {
    if ( document.getElementById("m_hfStateRevenueChanged").value == 1)
    {
        var choice = confirm("Data changed. Do you want to save??");
        if (choice == true)
        {  
          f_doSave();
        }
    }
  }
  function f_doSave()
  {
        var revenueByStateCode = {};
        $('#m_dgRevenue tr').not('.GridHeader').each(function()
        {
            revenueByStateCode[$.trim($($(this).children('td')[0]).text())] = 
                 $($(this).find('.revenueTB')[0]).val();
        });
        var details = { 
            'year' : $('#m_oldYear').val(),
            'quarter' : $('#m_oldQuarter').val(),
            'revenueByStateCode': revenueByStateCode
        };
        
        var data = JSON.stringify(details);
      callWebMethodAsync({
            type: "POST",
            url: "MortgageCallReportExport.aspx/SaveData",
            data: data,
            contentType: 'application/json; charset=utf-8',
            dataType: "json"
        }).then(
            function(msg){
                document.getElementById("m_hfStateRevenueChanged").value = "0" ; 
            },
            function(a,b,c){
                alert('Could not update.');
            }
        );
  }
  
  function f_downloadXml()
  {
    if ( document.getElementById("m_hfStateRevenueChanged").value == 1)
    {
        f_doSave(); 
    } 
    f_openWindowOfType('xml');
  }
  function f_preview()
  {
    if ( document.getElementById("m_hfStateRevenueChanged").value == 1)
    {
        f_doSave(); 
    }   
    f_openWindowOfType('html');   
  }
  function f_generateUrl()
  {
    var year = <%= AspxTools.JsGetElementById(m_yearDDL) %>.value;
    var period= <%= AspxTools.JsGetElementById(m_periodDDL) %>.value;
    var reportType = <%= AspxTools.JsGetElementById(m_reportTypeDDL) %>.value;
    var reportVersion = <%= AspxTools.JsGetElementById(m_reportVersionDDL) %>.value;
    return 'ReportExport.aspx?id=' + <%= AspxTools.JsString(m_id) %> + '&period='+period + '&year=' + year + '&reportType=' + reportType + '&reportVersion=' + reportVersion;
  }
  
  function mReport_SelectedIndexChanged()
  {    
    if ( document.getElementById("m_reportVersionDDL").selectedIndex == 0)    
        document.getElementById("m_Revenue").style.display = "none";
    else
        document.getElementById("m_Revenue").style.display = "block";                        
  }
  
  function m_ValueChanged()
  {      
    document.getElementById("m_hfStateRevenueChanged").value = "1" ;                            
  }
  
  function mDate_SelectedIndexChanged()
  {
    f_promptToSave();
    __doPostBack('', '');  
  }
       
</script>
    <form id="form1" runat="server">        
    <asp:HiddenField ID="m_hfStateRevenueChanged" runat="server"></asp:HiddenField> 
    <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
    <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />  
    <script type="text/javascript">

        var theForm = document.forms['form1'];
        if (!theForm) {
            theForm = document.form1;
        }
        function __doPostBack(eventTarget, eventArgument) {
            if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                theForm.__EVENTTARGET.value = eventTarget;
                theForm.__EVENTARGUMENT.value = eventArgument;
                theForm.submit();
            }
        }

      </script>  
    <asp:HiddenField ID="m_oldYear" runat="server" />
    <asp:HiddenField ID="m_oldQuarter" runat="server" />
    
    <div class="pagetitle">
    Mortgage Call Report Export
    </div>
    
    <table>
        <tr>
        <td class="FieldLabel">Report Version</td>
        <td><asp:DropDownList ID="m_reportVersionDDL" runat="server" onchange="mReport_SelectedIndexChanged()"/></td>
      </tr>
      <tr>
        <td class="FieldLabel">Report Type</td>
        <td><asp:DropDownList ID="m_reportTypeDDL" runat="server" /></td>
      </tr>
      <tr>
        <td class="FieldLabel">Year</td>
        <td><asp:DropDownList ID="m_yearDDL" runat="server" onchange="mDate_SelectedIndexChanged()" /></td>
      </tr>
      <tr>
        <td class="FieldLabel">Period</td>
        <td><asp:DropDownList ID="m_periodDDL" runat="server" onchange="mDate_SelectedIndexChanged()" /></td>
      </tr>
    </table>
    
    <table id="m_Revenue" style="display:none;">
      <tr >
        <td class="FieldLabel">
            Revenue from origination-related activities in the reporting quarter
        </td>
      </tr>
      <tr>
        <td>
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="340" Height="200" BorderWidth="0px" BorderColor="Gray"> 
                  <asp:DataGrid runat="server" id="m_dgRevenue" OnItemDataBound="OnDataBound_m_dgRevenue" Width="95%"  AutoGenerateColumns="false">
                    <AlternatingItemStyle CssClass="GridAlternatingItem" />
                    <ItemStyle CssClass="GridItem" />
                    <HeaderStyle CssClass="GridHeader" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="State">
                            <ItemTemplate>
                                <ml:EncodedLiteral runat="server" id="state"></ml:EncodedLiteral>
                            </ItemTemplate>
                        </asp:TemplateColumn>            
                        <asp:TemplateColumn HeaderText="Origination-related revenue">
                            <ItemTemplate>                                
                                <ml:MoneyTextBox runat="server" id="revenue" onchange="m_ValueChanged()"  class="revenueTB"></ml:MoneyTextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>                        
                    </Columns>
                  </asp:DataGrid>
                </asp:Panel>
        </td>
      </tr>
    </table>
    
    <table>
      <tr>
        <td colspan="2" align="center">
          <input type="button" value="Download Xml" onclick="f_downloadXml();" />
          &nbsp;&nbsp;
          <input type="button" value="Preview" onclick="f_preview();" />
          &nbsp;&nbsp;
          <input type="button" value="Close" onclick="f_promptToSave();onClosePopup();" />
        </td>
      </tr>
    </table>
    </form>
</body>
</html>
