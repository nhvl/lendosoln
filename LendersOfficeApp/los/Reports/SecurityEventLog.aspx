﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SecurityEventLog.aspx.cs" Inherits="LendersOfficeApp.los.Reports.SecurityEventLog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        body {
            background-color: gainsboro;
            width: 100%;
        }
        form {
            margin: 5px;
        }
        .align-right {
            text-align: right;
        }
        .float-right {
            float: right;
        }
        table {
            width: 100%;
        }
    </style>
    <title>Security Event Log</title>
</head>
<body>
    <script type="text/javascript">
        var isToDateValid = true;
        var isFromDateValid = true;
        var isIPValid = true;
        var currentPage = 0;

        $(function () {
            $("#noResults").hide();
            clearSearch();
            $("#exportWithoutSearch").prop("disabled", true);
        });

        function pickUser() {
            LQBPopup.Show(VRoot + "/newlos/Tasks/RoleAndUserPicker.aspx?IsBatch=1&IsPipeline=1&hideConditionsWarning=1", {
                hideCloseButton: true,
                width: 500,
                height: 300,
                onReturn: function (returnArgs) {
                    if (typeof (returnArgs) != 'undefined' && returnArgs != null) {
                        $j("#userName").text(returnArgs.name);
                        $j("#userName").val(returnArgs.id);
                    }
                }
            });
        }

        function setDateValid(id, val) {
            if (id == "fromDate") {
                isFromDateValid = val;
            }
            else {
                isToDateValid = val;
            }
        }

        function validateDate(date, id) {
            var regex = /^\s*$|^(\d{2})\/(\d{2})\/(\d{4})$|^(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2})$/;
            var result = regex.exec(date);
            if (result == null) {
                alert("Please enter the date in MM/DD/YYYY or MM/DD/YYYY HH:MM:SS format.");
                setDateValid(id, false);
                return;
            }
            else {
                var month, day, year;
                if (date.length == 10) {
                    // MM/DD/YY format
                    month = parseInt(result[1]);
                    day = parseInt(result[2]);
                    year = parseInt(result[3]);
                }
                else if (date.length == 19) {
                    // MM/DD/YY HH:MM:SS format
                    month = parseInt(result[4]);
                    day = parseInt(result[5]);
                    year = parseInt(result[6]);
                    var hour = parseInt(result[7]);
                    var minute = parseInt(result[8]);
                    var second = parseInt(result[9]);

                    if (hour < 0 || hour > 23) {
                        alert("Hour must be between 0 and 23");
                        setDateValid(id, false);
                        return;
                    }
                    if (minute < 0 || minute > 59) {
                        alert("Minute must be between 0 and 59");
                        setDateValid(id, false);
                        return;
                    }
                    if (second < 0 || second > 59) {
                        alert("Second must be between 0 and 59");
                        setDateValid(id, false);
                        return;
                    }
                }

                if (day < 1 || day > 31) {
                    alert("Day must be between 1 and 31");
                    setDateValid(id, false);
                    return;
                }

                if (month < 1 || month > 12) {
                    alert("Month must be between 1 and 12");
                    setDateValid(id, false);
                    return;
                }
                
                if (year < 1900 || year > 3000) {
                    alert("Year is invalid.");
                    setDateValid(id, false);
                    return;
                }
            }

            setDateValid(id, true);
        }

        function validateIP(ipAddr) {
            if (ipAddr == "") {
                isIPValid = true;
                return;
            }

            var splitIp = ipAddr.split(".");

            var curIpValid = true;
            $.map(splitIp, function (val, i) {
                var regex = /^\s*$|^([0-9]|[1-9][0-9]|[1-9][0-9][0-9])$/;
                if (!regex.test(val)) {
                    curIpValid = false;
                }

                var num = parseInt(val);
                if (num < 0 || num > 255) {
                    curIpValid = false;
                }
            });

            if (!curIpValid) {
                alert("IP address is not valid.");
                isIpValid = false;
                return;
            }

            isIPValid = true;
        }

        function nowBoxClick(checked) {
            $("#toDate").prop("readonly", checked);
            if (checked) {
                isToDateValid = true;
                $("#toDate").val("");
            }
            else {
                gService.securityEventLog.callAsyncSimple("NowClick", null, function (result) {
                    if (!result.error) {
                        ML.DefaultToDate = result.value.nowDate;
                        $("#toDate").val(ML.DefaultToDate);
                    }
                    else {
                        alert(result.UserMessage);
                    }
                });
            }
        }

        function enableButtonsIfInputsValid() {            
            $("#searchBtn").prop("disabled", !(isToDateValid && isFromDateValid && isIPValid));
            $("#exportWithSearch").prop("disabled", !(isToDateValid && isFromDateValid && isIPValid));
        }

        function clearUserName() {
            $("#userName").val("").text("None");
        }

        function clearSearch() {
            isToDateValid = true;
            isFromDateValid = true;
            isIPValid = true;
            currentPage = 0;
            enableButtonsIfInputsValid();

            $("#fromDate").val(ML.DefaultFromDate);
            $("#toDate").val(ML.DefaultToDate);            
            $("#ipAddr").val("");
            $("#securityEventTypeDdl").val(0); // All Events
            clearUserName();

            $("#resultDiv").empty();
            $("#noResults").hide();

            $("#exportWithoutSearch").show();

            $("#nowCheckBox").prop("checked", true);
            nowBoxClick(true);
        }

        function nextClick() {
            currentPage++;
            search();
        }

        function previousClick() {
            currentPage--;
            search();
        }

        function download() {
            var employeeId = $("#userName").val();
            if (employeeId == "") {
                employeeId = "00000000-0000-0000-0000-000000000000";
            }

            fetchFileViaFrame(VRoot + "/los/reports/securityeventlog.aspx?cmd=download&todate=" + $("#toDate").val() + "&fromDate=" + $("#fromDate").val()
                + "&isnow=" + $("#nowCheckBox").prop("checked") + "&ipaddr=" + $("#ipAddr").val() + "&securityeventtype=" + $("#securityEventTypeDdl").val()
                + "&employeeuserid=" + employeeId
                , null);
        }

        function search() {
            var bothDatesNotFilled = (($("#fromDate").val() != "" && ($("#toDate").val() == "" && !$("#nowCheckBox").prop("checked"))) ||
                ($("#fromDate").val() == "" && ($("#toDate").val() != "" && !$("#nowCheckBox").prop("checked"))));

            if (bothDatesNotFilled) {
                alert("Both date fields must be filled out");
                return;
            }

            var data = {
                ToDate: $("#toDate").val(),
                FromDate: $("#fromDate").val(),
                IsNow: $("#nowCheckBox").prop("checked"),
                IPAddr: $("#ipAddr").val(),
                SecurityEventType: $("#securityEventTypeDdl").val(),
                EmployeeUserId: $("#userName").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#userName").val(),
                CurrentPage: currentPage
            }

            gService.securityEventLog.callAsyncSimple("Search", data, function (result) {
                if (!result.error) {
                    var logs = JSON.parse(result.value.logs);

                    if (logs.SecurityEventLogs.length === 0) {
                        $("#resultDiv").empty();
                        $("#noResults").show();
                    }
                    else {
                        $("#resultDiv").empty();
                        $("#logTemplate").tmpl(logs).appendTo("#resultDiv");
                        $("#noResults").hide();

                        $("#viewPrevious").toggle(currentPage != 0);
                        var atLastPage = logs.TotalSecurityEventLogsCount <= (currentPage + 1) * ML.ResultsPerPage;
                        $("#viewNext").toggle(!atLastPage);

                        if (atLastPage) {
                            $("#resultsRange").text(currentPage * ML.ResultsPerPage + 1 + " - " + logs.TotalSecurityEventLogsCount + " ");
                        }
                        else {
                            $("#resultsRange").text(currentPage * ML.ResultsPerPage + 1 + " - " + (currentPage + 1) * ML.ResultsPerPage + " ");
                        }
                    }

                    $("#exportWithoutSearch").hide();
                }
                else {
                    alert(result.UserMessage);
                    $("#exportWithSearch").prop("disabled", true);
                }
            });
        }
    </script>

    <script id="logTemplate" type="text/x-jquery-tmpl">
        <span class="FieldLabel">
            Showing events <span id="resultsRange"></span>of ${TotalSecurityEventLogsCount} <a id="viewPrevious" onclick="previousClick()">view previous</a> <a id="viewNext" onclick="nextClick()">view next</a> <input id="exportWithSearch"class="float-right" type="button" value="Export Event Report" onclick="download()" />
        </span>
        <br />
        <br />
        <table class="FormTable Table">
            <tr class="FormTableHeader">
                <th>Security Event Type</th>
                <th>Event Description</th>
                <th>User Name</th>
                <th>Login Name</th>
                <th>User Type</th>
                <th>Timestamp</th>
                <th>IP Address</th>
            </tr>
            {{each SecurityEventLogs}}
            <tr class="GridAutoItem">
                <td>
                    ${SecurityEventType}
                </td>
                <td>
                    ${DescriptionText}
                </td>
                <td>
                    ${UserFullNm}
                </td>
                <td>
                    ${LoginNm}
                </td>
                <td>
                    ${UserType}
                </td>
                <td>
                    ${TimeStamp}
                </td>
                <td>
                    ${ClientIP}
                </td>
            </tr>
            {{/each}}
        </table>
    </script>

    <form id="form1" runat="server">
    <h4 class="page-header">Security Event Log</h4>
    <div>
        <table>
            <tr>
                <td class="FieldLabel">
                    Event Type 
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="securityEventTypeDdl"></asp:DropDownList>
                </td>
                <td class="FieldLabel">
                    User Name
                </td>
                <td>
                    <span runat="server" id="userName">None</span>
                    [ 
                    <a id="ClearUserName" onclick="clearUserName();">None</a> 
                    | 
                    <a id="PickUser" onclick="pickUser();">Pick User</a> 
                    ]
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    From
                </td>
                <td>
                    <asp:TextBox id="fromDate" runat="server" onchange="validateDate(this.value, this.id); enableButtonsIfInputsValid();"></asp:TextBox>
                </td>
                <td class="FieldLabel">
                    IP Address
                </td>
                <td>
                    <asp:TextBox id="ipAddr" runat="server" onchange="validateIP(this.value); enableButtonsIfInputsValid();"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    To
                </td>
                <td>
                    <asp:TextBox id="toDate" runat="server" onchange="validateDate(this.value); enableButtonsIfInputsValid();"></asp:TextBox>
                    <asp:CheckBox id="nowCheckBox" runat="server" onclick="nowBoxClick(this.checked); enableButtonsIfInputsValid();"/> Now
                </td>
                <td colspan="2">
                </td>
                <td class="align-right">
                    <input type="button" value="Clear Search" onclick="clearSearch();"/>
                </td>
                <td class="align-right">
                    <input type="button" value="Search" id="searchBtn" onclick="currentPage = 0; search();"/>
                </td>
            </tr>            
        </table>    
        <hr />
        <div id="resultDiv">
        </div>
        <input id="exportWithoutSearch" class="float-right" type="button" value="Export Event Report" />
        <span class="FieldLabel" id="noResults">No logs match the specified criteria.</span>
    </div>
    </form>
</body>
</html>
