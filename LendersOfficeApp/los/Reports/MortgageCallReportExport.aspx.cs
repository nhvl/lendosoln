﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.NmlsCallReport;
using System.Data;
using LendersOffice.QueryProcessor;
using DataAccess;
using System.Collections;
using System.Web.Services;
using LendersOffice.Security;


namespace LendersOfficeApp.los.Reports
{
    public partial class MortgageCallReportExport : BasePage
    {
        protected string m_id;
        protected string m_reportType;
        protected Dictionary<int, Dictionary<E_McrQuarterT, HashSet<String>>> m_statesDictionary;
        protected bool m_stateRevenueChanged;

        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            base.OnInit(e);
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            m_id = RequestHelper.GetSafeQueryString("id");
            m_statesDictionary = McrRmlaXml.GetRevenueByStateByYearAndQuarter(m_id, PrincipalFactory.CurrentPrincipal);
            m_stateRevenueChanged = m_hfStateRevenueChanged.Value == "1";
            if (!IsPostBack)
            {
                m_periodDDL.Items.Add(new ListItem("Q1", E_McrPeriodT.Q1.ToString("D")));
                m_periodDDL.Items.Add(new ListItem("Q2", E_McrPeriodT.Q2.ToString("D")));
                m_periodDDL.Items.Add(new ListItem("Q3", E_McrPeriodT.Q3.ToString("D")));
                m_periodDDL.Items.Add(new ListItem("Q4", E_McrPeriodT.Q4.ToString("D")));

                m_reportTypeDDL.Items.Add(new ListItem("Standard", E_McrFormatT.Standard.ToString("D")));
                m_reportTypeDDL.Items.Add(new ListItem("Expanded", E_McrFormatT.Expanded.ToString("D")));

                m_reportVersionDDL.Items.Add(new ListItem("Version 1", E_McrVersionT.Version1.ToString("D")));
                m_reportVersionDDL.Items.Add(new ListItem("Version 2", E_McrVersionT.Version2.ToString("D")));
                m_reportVersionDDL.Items.Add(new ListItem("Version 3", E_McrVersionT.Version3.ToString("D")));
                m_reportVersionDDL.Items.Add(new ListItem("Version 4", E_McrVersionT.Version4.ToString("D")));
                m_reportVersionDDL.Items.Add(new ListItem("Version 5", E_McrVersionT.Version5.ToString("D")));
                m_reportVersionDDL.SelectedValue = E_McrVersionT.Version5.ToString("D");

                // 7/21/2011 dd - The year must be between the current year minus 2 and current year plus 1 (From NMLS 
                // Call Report validation rule. Year must be greater than equal 2011.
                int currentYear = DateTime.Now.Year;
                for (int year = currentYear - 2; year <= currentYear + 1; year++)
                {
                    if (year >= 2011)
                    {
                        m_yearDDL.Items.Add(year.ToString());
                    }
                }

                SelectPreviousQuarter();
            }           
        }
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                mDate_SelectedIndexChanged();
            }
        }
        

        private void SelectPreviousQuarter()
        {
            int quarter = ((DateTime.Now.Month - 1) / 3) - 1;
            int year = DateTime.Now.Year;

            if (quarter < 0)
            {
                quarter = 3;
                year--;
            }
            
            m_periodDDL.SelectedIndex = quarter;
            m_yearDDL.SelectedValue = year.ToString();

            BindRevenueByStateByYearAndQuarter((E_McrQuarterT)quarter, year);          
        }

        protected void mDate_SelectedIndexChanged()
        {            
            int currentYear = DateTime.Now.Year;
            var quarter = McrRmlaXml.ConvertPeriodToQuarter((E_McrPeriodT)Int32.Parse(m_periodDDL.SelectedValue));
            var year = Int32.Parse(m_yearDDL.SelectedValue);
            BindRevenueByStateByYearAndQuarter(quarter, year);
        }

        protected void BindRevenueByStateByYearAndQuarter(E_McrQuarterT quarter, int year)
        {
            m_oldQuarter.Value = ((int)quarter).ToString();
            m_oldYear.Value = year.ToString();

            m_dgRevenue.DataSource = null;
            m_dgRevenue.DataBind();

            DataTable dt = new DataTable();
            DataRow dr;
            dt.Columns.Add(new DataColumn("State", typeof(string)));
            dt.Columns.Add(new DataColumn("Revenue", typeof(string)));
            string[] st = new string[m_statesDictionary[year][quarter].Count];

            m_statesDictionary[year][quarter].CopyTo(st);
            Array.Sort(st);

            var brokerStatesDictionary = new Dictionary<string,string>();
            var brokerID = PrincipalFactory.CurrentPrincipal.BrokerId;
            var broker = LendersOffice.Admin.BrokerDB.RetrieveById(brokerID);
            var revenueByStateByQuarterByYear = ObsoleteSerializationHelper.JsonDeserialize<Dictionary<int, Dictionary<E_McrQuarterT, Dictionary<string, string>>>>(broker.NmlsCallReportOriginationRelatedRevenueJSONContent);
            if (revenueByStateByQuarterByYear != null)
            {
                if (revenueByStateByQuarterByYear.ContainsKey(year))
                {
                    if (revenueByStateByQuarterByYear[year].ContainsKey(quarter))
                    {
                        brokerStatesDictionary = revenueByStateByQuarterByYear[year][quarter];
                    }
                }
            }

            for (int j = 0; j < st.Length; j++)
            {
                dr = dt.NewRow();
                dr[0] = st[j];
                if (brokerStatesDictionary.ContainsKey(st[j]))
                {
                    dr[1] = brokerStatesDictionary[st[j]];
                }
                else
                {
                    dr[1] = "$0.00";
                }
                dt.Rows.Add(dr);
            }
            m_dgRevenue.DataSource = dt;
            m_dgRevenue.DataBind();
        }

        protected void OnDataBound_m_dgRevenue(object sender, DataGridItemEventArgs args)
        {            
            DataRowView row = (DataRowView)args.Item.DataItem;
            if (row == null)
                return;

            Literal state = (Literal)args.Item.FindControl("state");
            MeridianLink.CommonControls.MoneyTextBox revenue = (MeridianLink.CommonControls.MoneyTextBox)args.Item.FindControl("revenue");            
            state.Text = row[0].ToString();
            revenue.Text = row[1].ToString();                           
        }
        [WebMethod]
        public static void SaveData(int year, E_McrQuarterT quarter, Dictionary<string,string> revenueByStateCode)
        {
            var brokerID = PrincipalFactory.CurrentPrincipal.BrokerId;
            var broker = LendersOffice.Admin.BrokerDB.RetrieveById(brokerID);
            var revenueByStateByQuarterByYear = ObsoleteSerializationHelper.JsonDeserialize<Dictionary<int, Dictionary<E_McrQuarterT, Dictionary<string, string>>>>(broker.NmlsCallReportOriginationRelatedRevenueJSONContent);
            if (revenueByStateByQuarterByYear == null)
            {
                revenueByStateByQuarterByYear = new Dictionary<int, Dictionary<E_McrQuarterT, Dictionary<string, string>>>();
            }
            if (!revenueByStateByQuarterByYear.ContainsKey(year))
            {
                revenueByStateByQuarterByYear.Add(year, new Dictionary<E_McrQuarterT, Dictionary<string, string>>());
            }
            if (!revenueByStateByQuarterByYear[year].ContainsKey(quarter))
            {
                revenueByStateByQuarterByYear[year].Add(quarter, revenueByStateCode);
            }
            else
            {
                revenueByStateByQuarterByYear[year][quarter] = revenueByStateCode;
            }
            var json = ObsoleteSerializationHelper.JsonSerialize(revenueByStateByQuarterByYear);
            if (json != broker.NmlsCallReportOriginationRelatedRevenueJSONContent)
            {
                broker.NmlsCallReportOriginationRelatedRevenueJSONContent = json;
                broker.Save();
            }
            
        }
                
    }
}
