﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.XsltExportReport;
using DataAccess;

namespace LendersOfficeApp.los.Reports
{
    public partial class BatchExportDownloadService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch(methodName){
                case "retry":
                    Retry();
                    break;
            }
        }

        private void Retry()
        {
            AbstractUserPrincipal principal = this.User as AbstractUserPrincipal;
            string reportId = GetString("ReportId");
 
            XsltExport.ResubmitRequest(principal.BrokerId, principal.UserId, reportId);
        }
    }
}
