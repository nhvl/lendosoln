﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PublishReport.aspx.cs" Inherits="LendersOfficeApp.los.Reports.PublishReport" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Publish Report</title>   
 
 <link href="../../css/stylesheetnew.css" type="text/css" rel="stylesheet"/>
 
 
 <script type="text/javascript">
     //<!--
     function _init() {
         //window.resizeTo(800, 600);
         //resizeForIE6And7(400, 250);
     }
     function f_close(arg) {
         onClosePopup();
         return false;
     }
//-->
 </script>
</head>
<body bgColor=gainsboro margin="0" scroll="no" onload="_init();">
    <form id="form1" runat="server">
    <table id=Table1 style="MARGIN: 0px" cellpadding="5px" width="100%" border=0>
        <tr>
            <td class="MainRightHeader" nowrap colspan="2">Publish Report</td>
        </tr>
        
        <tr>
        <td colspan="2" style="text-align:center; font-weight:bold; color:red">
           "Warning"
        </td>
        </tr>
        
        <tr>        
        <td colspan="2" style="text-align:left;">
           •	This report will be visible to all users regardless of access level. <br/>
           •	The generated report is based on your level of access.
        </td>
        </tr>
        <tr>
            <td style="height:3em">&nbsp;</td>
        </tr>            
        <tr>
            <td class="FieldLabel" noWrap width="0%">Publish Report As </td>
            <td style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; PADDING-TOP: 8px" noWrap width="100%"><asp:textbox id="m_NamePublishedAs" style="PADDING-LEFT: 4px" runat="server" Width="23em" MaxLength="100"></asp:textbox><asp:RequiredFieldValidator ID="rfvAnswer1" runat="server" ControlToValidate="m_NamePublishedAs"><img runat="server" src="../../images/error_icon.gif" alt="ERR" /></asp:RequiredFieldValidator>
            </td>
        </tr>
     </table>
       
     <div nowrap style="text-align:right; padding:0.25em 2em 0.25em 0.25em; margin-right:2em">
          <asp:Button ID="btnPublish" runat="server" Text="Agree & Publish Report" OnClick="Publish" />          
          <input type="button" value="Cancel" style="width: 55px" onclick="f_close(false);">
      <div>
   
</form>
</body>
</html>
