﻿#region Generated Code
namespace LendersOfficeApp.los.Reports
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LendersOffice.XsltExportReport;

    /// <summary>
    /// This service is used to asynchronous create Batch Report Export requests.
    /// </summary>
    public partial class BatchExportReportService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// This method takes in a method's name and calls the corresponding method.
        /// </summary>
        /// <param name="methodName">The name of the method to be called.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "BatchExportReports":
                    this.BatchExportReports();
                    break;
                default:
                    throw new CBaseException("Failed to export the report.  Please try again.", "The method '" + methodName + "' is not handled in BatchExportReportService.aspx.cs");
            }
        }

        /// <summary>
        /// This method creates the request for the actual Batch Export Report.
        /// </summary>
        private void BatchExportReports()
        {
            bool customFieldChecked = GetBool("customFieldChecked", false);
            string customFieldValue = GetString("customFieldValue");
            string mapName = GetString("Type");
            DefaultXsltExportCallBack callback = new DefaultXsltExportCallBack();
            string selectedIdsJson = GetString("selectedIdsJson");
            List<Guid> selectedIds = SerializationHelper.JsonNetDeserialize<List<Guid>>(selectedIdsJson);

            string email = GetString("email");
            string emailPattern = ConstApp.EmailValidationExpression;

            // If not a valid email, revert to current user's email.
            if (!Regex.IsMatch(email.TrimWhitespaceAndBOM(), emailPattern))
            {
                email = EmployeeDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId).Email;
            }

            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            parameters.Add(new KeyValuePair<string, string>("Email", email));

            XsltMapItem item = XsltMap.Get(mapName);

            if (customFieldChecked)
            {
                if (string.IsNullOrEmpty(item.CustomFieldId) == false)
                {
                    parameters.Add(new KeyValuePair<string, string>("UpdatedField", item.CustomFieldId));
                    parameters.Add(new KeyValuePair<string, string>("UpdatedData", customFieldValue));
                }
            }

            XsltExportRequest request = new XsltExportRequest(PrincipalFactory.CurrentPrincipal.UserId, selectedIds, mapName, callback, parameters);
            XsltExport.SubmitRequest(PrincipalFactory.CurrentPrincipal.BrokerId, request); // The report generation will occur on LODEVSERVER.

            SetResult("email", email);
        }
    }
}