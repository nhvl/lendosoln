///
/// Author: Kirk Bulis
/// Author: David Dao
/// Author: Michael Leinweaver
/// 
namespace LendersOffice
{
    using System;
    using System.Collections;
    using System.Text;
    using System.Web.UI;
    using DataAccess;
    using ExpertPdf.HtmlToPdf;
    using ExpertPdf.HtmlToPdf.PdfDocument;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using PdfRasterizerLib;
    using System.Web.UI.WebControls;
    using MeridianLink.CommonControls;

    public partial class ReportResult : BasePage
    {
        protected bool m_isMortgageCallReport = false;

        protected bool m_isULDDReport = false;
        protected bool ShowULDDButton
        {
            get
            {
                return true;
            }
        }
        protected override bool GoingToPdf
        {
            get
            {
                return RequestHelper.GetBool("ToPdf");
            }
        }

        protected string UIDisplay
        {
            get
            {
                if (GoingToPdf)
                {
                    return "display:none";
                }
                else
                {
                    return "";
                }
            }
        }

        protected string ProcessingText
        {
            get
            {
                if (GoingToPdf)
                {
                    return "Processing complete ( " + m_ResultCount.Value + " ).";
                }
                else
                {
                    return "Processing request. This may take a minute...";
                }
            }
        }

        private Document GetPdfFromHtml(string html, string footer)
        {
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + '/';
            Tools.LogInfo("When generating a pdf report result, base url is: " + baseUrl);

            try
            {
                PdfConverter pdfc = new PdfConverter();
                if (!string.IsNullOrEmpty(ConstStage.PdfConverterLicenseKey)) pdfc.LicenseKey = ConstStage.PdfConverterLicenseKey;

                SetPdfMargins(pdfc, 10);
                pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter;
                pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Landscape;
                pdfc.PdfDocumentOptions.ShowFooter = true;
                pdfc.PdfFooterOptions.FooterText = footer;
                pdfc.PdfFooterOptions.ShowPageNumber = true;
                pdfc.PdfFooterOptions.PageNumberText = "Page";

                Document doc = null;
                Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream fs)
                {
                    doc = pdfc.GetPdfDocumentObjectFromHtmlStream(fs.Stream, Encoding.UTF8, baseUrl, null);
                };

                BinaryFileHelper.OpenRead(html, readHandler);
                return doc;
            }
            catch (Exception ex)
            {
                // Restart PDF Conversion Process
                Tools.RestartPdfConversionProcess();
                throw ex;
            }

            //This would theoretically cause the pdf to print itself if you can figure out how to stick it in there,
            //it's possible with iTextSharp but I dunno about ExpertPdf
            /*
            PdfActionJavaScript printAndClose = new PdfActionJavaScript(@"
            var pp = this.getPrintParams();
            pp.interactive = pp.constants.interactionLevel.automatic;
            this.print(pp);
            this.closeDoc(true);
            ");
             */

        }

        private static void SetPdfMargins(PdfConverter pdfc, int size)
        {
            pdfc.PdfDocumentOptions.LeftMargin = pdfc.PdfDocumentOptions.RightMargin =
                 pdfc.PdfDocumentOptions.TopMargin = pdfc.PdfDocumentOptions.BottomMargin = size;
        }

        static string footerFmt = "{0} Created by: {1} {2}";
        protected override void Render(HtmlTextWriter writer)
        {
            if (GoingToPdf)
            {
                DateTime RunDate = m_rA.Stats.FinalNow;
                string filename = RunDate.Year + "-" + RunDate.Month.ToString("00") + "-" + RunDate.Day.ToString("00") + "_" + m_rA.Label.Title + ".pdf";

                string tempFile = TempFileUtils.NewTempFilePath() + ".htm";

                Action<TextFileHelper.LqbTextWriter> writeHandler = delegate(TextFileHelper.LqbTextWriter sw)
                {
                    using (HtmlTextWriter hw = new HtmlTextWriter(sw.Writer))
                    {
                        base.Render(hw);
                    }
                };

                TextFileHelper.OpenNew(tempFile, writeHandler);

                //The client-side script will look for "true" in this element and if found will hide all ui elements.
                m_PrintReport.Value = "true";
                var user = BrokerUserPrincipal.CurrentPrincipal;
                string footer = string.Format(footerFmt, m_rA.Label.Title, user.FirstName + " " + user.LastName, RunDate);

                if (ConstStage.UsePDFRasterizerServiceForPDFConversion)
                {
                    string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + '/';
                    Tools.LogInfo("When generating a pdf report result, base url is: " + baseUrl);

                    PDFConversionOptions options = new PDFConversionOptions()
                    {
                        TopMargin = 10,
                        LeftMargin = 10,
                        RightMargin = 10,
                        BottomMargin = 10,
                        PageSize = (int)PdfPageSize.Letter,
                        PageOrientation = (int)PDFPageOrientation.Landscape,
                        ShowFooter = true,
                        FooterText = footer,
                        ShowPageNumber = true,
                        PageNumberText = "Page",
                    };
                    Response.Clear();
                    Response.ClearHeaders();
                    //Need to remove any compression filtering, otherwise it comes out garbled.
                    Response.Filter = null;
                    if (Context.Items.Contains("COMPRESSED"))
                    {
                        Context.Items.Remove("COMPRESSED");
                    }
                    string path = Tools.ConvertHtmlFileToPdfFile(baseUrl, tempFile, options);
                    RequestHelper.SendFileToClient(Context, path, "application/pdf", filename);
                    Response.End();
                }
                else
                {
                    var pdf = GetPdfFromHtml(tempFile, footer);

                    Response.Clear();
                    Response.ClearHeaders();
                    //Need to remove any compression filtering, otherwise it comes out garbled.
                    Response.Filter = null;
                    pdf.Save(Response, false, filename);
                    pdf.Close(); // Need to clean up the resource.
                    Response.Flush();
                    Response.End();
                }
            }
            else
            {
                base.Render(writer);
            }
        }
        private Report GetReportFromIDUsingFileHandle()
        {
            Report temp = null;
            LocalFilePath filePath = LocalFilePath.Invalid;
            string resultFromDB = null;
            if (false == AutoExpiredTextCache.TryGetStringOrFileHandlerFromCacheByUser(BrokerUserPrincipal.CurrentPrincipal, m_reportId, out resultFromDB, out filePath))
            {
                return temp;
            }

            if (resultFromDB != null)
            {
                temp = (Report)resultFromDB;
            }
            else if(filePath != LocalFilePath.Invalid)
            {
                temp = Report.GetReport(filePath);
                FileOperationHelper.Delete(filePath.Value); // Cleanup temp file.
            }

            return temp;
        }
        private Report GetReportFromIDUsingStrings()
        {
            Report temp = null;
            string result;
            if ((result = AutoExpiredTextCache.GetFromCacheByUser(BrokerUserPrincipal.CurrentPrincipal, m_reportId)) != null)
            {
                temp = (Report)result;
            }
            return temp;
        }

        private Report rA
        {
            get
            {
                if (m_rA != null) return m_rA;

                if (m_reportId == null)
                {
                    // Identifier of cached report not specified, so tell
                    // the logger and user something is wrong.

                    m_ReportError.Value = "Unable to load the report for display.  Try re-running your query.";

                    Tools.LogError("Report Result: Error!  Cached report id not specified.");

                    return null;
                }

                // Retrieve the report using the implicit string
                // conversion of the contained, cached object.

                Report temp = null;

                // 05/12/06 mf - OPM 2582 & 4668.  To reduce server memory usage
                // do not use server cache for custom reports.
                try
                {
                    temp = GetReportFromIDUsingFileHandle();
                    if (temp == null)
                    {
                        temp = GetReportFromIDUsingStrings();
                    }
                }
                catch (Exception e)
                {
                    Tools.LogErrorWithCriticalTracking(e);
                    temp = GetReportFromIDUsingStrings();
                }
                
                if (temp == null)
                {
                    // Mark the error in the message container and
                    // skip attachment loading.

                    m_ReportError.Value = "Unable to load the report for display.  Try re-running your query.";

                    Tools.LogError("Report Result Error: Couldn't load ( " + m_reportId + " ) from the cache.");

                    return null;
                }

                m_rA = temp;
                return m_rA;

            }
        }
        
        private Report m_rA = null;
        
        protected string m_reportId
        {
            get
            {
                return RequestHelper.GetSafeQueryString("id");
            }
        }

        /// <summary>
        /// Load cached report and display as html.
        /// </summary>

        protected void PageLoad(object sender, System.EventArgs a)
        {
            // Place calculation start in the log.
            try
            {
                // We fetch the report from the cache and render it
                // according to the given type.  If the report is
                // no longer cached, then we punt and say sorry.
                if (this.rA == null)
                {
                    return; // rA's getter will already set m_ReportError.Value
                }

                m_isMortgageCallReport = rA.IsNmlsCallReport;

                m_isULDDReport = rA.Label.Title.Contains(" ULDD ");
                m_exportULDD.Visible = m_isULDDReport;
                m_exportULDD.Disabled = rA.Stats.HitCount == 0;
                // Initialize our repeaters with the tables and reports
                // so that we'll see the html for each table.

                ArrayList rc = new ArrayList();
                ArrayList rp = new ArrayList();

                if (rA.Results.Count > 0)
                {
                    foreach (CalculationResult calc in rA.Results)
                    {
                        if (calc.What == E_FunctionType.Sm)
                        {
                            rp.Add(rA.Columns[calc.Column].Id);
                        }
                    }

                    rc.Add(new ReportLayoutWrapper(rA));
                }

                // Save the link to the cached report and set the
                // link buttons to export the right one.
                string safeId = AspxTools.JsString(RequestHelper.GetSafeQueryString("id"));
                m_ExportToTsv.OnClientClick = $"f_saveFile('{m_ExportToTsv.ClientID}',{safeId},'txt'); return false;";
                m_ExportToCsv.OnClientClick = $"f_saveFile('{m_ExportToCsv.ClientID}',{safeId},'csv'); return false;";

                // Update the display statistics for the client.  We
                // pull these details from the report.

                m_ResultCount.Value = rA.Stats.HitCount + " total loan files";
                m_ProcessTime.Value = rA.Stats.ProcTime.TotalSeconds.ToString() + " seconds";

                m_ProcessDate.Value = rA.Stats.FinalNow.ToLongDateString() + " :: " + rA.Stats.FinalNow.ToShortTimeString() + " :: ";
                
                foreach (string tz in TimeZone.CurrentTimeZone.DaylightName.Split(' '))
                {
                    m_ProcessDate.Value += tz[0];
                }

                m_ReportTitle.Value = rA.Label.Title;

                TableParser.Parse(m_RenderedTable, rA.Tables, rA.Columns);

                m_ReportCalcs.DataSource = rc;
                m_ReportCalcs.DataBind();
                m_exportMortgageCallReport.Visible = m_isMortgageCallReport;
                string title = rA.Label.Title;
                string startDate = rA.Start.ToShortDateString();
                string endDate = rA.End.ToShortDateString();

                if (rA.Start != DateTime.MaxValue) title += " from " + startDate;
                if (rA.End != DateTime.MinValue) title += " to " + endDate;

                ReportTitle.Text = title;

                var brokerid = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
                var pmlSiteId = BrokerDB.RetrieveById(brokerid).PmlSiteID;

                if (HasLogo(pmlSiteId))
                {
                    Logo.Src = ResolveUrl("~/LogoPL.aspx?id=" + pmlSiteId.ToString());
                }
                else
                {
                    Logo.Visible = false;
                }

                if (rA.DisplayTablesOnSeparatePages)
                {
                    m_ProcessOpts.Value = "break on grouping";
                }

                //Don't display the export to pdf button if we don't have a valid license
                ToPdf.Visible = !string.IsNullOrEmpty(ConstStage.PdfConverterLicenseKey);

                //On the other hand, if we're on dev, local or test/demo, always display it.
                if (!ToPdf.Visible)
                {
                    ToPdf.Visible = ConstAppDavid.CurrentServerLocation == ServerLocation.Development ||
                                    ConstAppDavid.CurrentServerLocation == ServerLocation.LocalHost ||
                                    ConstAppDavid.CurrentServerLocation == ServerLocation.Demo ||
                                    ConstAppDavid.CurrentServerLocation == ServerLocation.DevCopy;
                }

            }
            catch (Exception e)
            {
                // Log the error and display failure to user.  We return
                // early so that the page can be rendered normally, without
                // terminating the response prematurely, as we would do if
                // we had a complete document to attach.

                m_ReportError.Value = ErrorMessages.Generic;

                Tools.LogError(e);
            }

        }

        protected void m_ReportCalcs_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            ReportLayoutWrapper item = (ReportLayoutWrapper)args.Item.DataItem;

            PassthroughLiteral headerFieldNames = (PassthroughLiteral)args.Item.FindControl("HeaderFieldNames");            
            headerFieldNames.Text = item.HeaderFieldNames;

            PassthroughLiteral renderedCalcs = (PassthroughLiteral)args.Item.FindControl("RenderedCalcs");
            renderedCalcs.Text = item.RenderedCalcs; 
        }

        private bool HasLogo(Guid pmlSiteId)
        {
            return FileDBTools.DoesFileExist(E_FileDB.Normal, pmlSiteId.ToString().ToLower() + ".logo.gif");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary> 
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion

    }

    /// <summary>
    /// Report table layout construction is performed by
    /// this table container.  We export properties that
    /// yield table content for the ui rendering phase.
    /// Each entry is invoked by the containing repeater.
    /// </summary>

    internal class ReportLayoutWrapper
    {
        Report m_Collection; // table details to render

        public string HeaderDescriptor
        {
            get { return "ACCUMULATIVE REPORT CALCULATIONS"; }
        }

        public string HeaderFieldNames
        {
            // Access member.

            get
            {
                // Collect column information and display the content.

                StringBuilder header = new StringBuilder();

                foreach (Column column in m_Collection.Columns)
                {
                    // Display the cell with the correct alignment.
                    // Numeric fields align right.  All others
                    // align left.

                    string alignment;

                    switch (column.Kind)
                    {
                        case Field.E_ClassType.Pct:
                        case Field.E_ClassType.Csh:
                        case Field.E_ClassType.Cnt:
                            alignment = "right";
                            break;
                        default:
                            alignment = "left";
                            break;
                    }
                    header.AppendFormat("<th align=\"{0}\">{1}</th>", alignment, column.Name);
                }

                return header.ToString();
            }
        }

        public string FooterDescriptor
        {
            get { return ""; }
        }

        public string RenderedCalcs
        {
            // Access member.

            get
            {
                // Add the aggregation results in the final row, if
                // any results are currently present.

                StringBuilder calcs = new StringBuilder();

                if (m_Collection.Results.Count > 0)
                {
                    int i = 0;

                    calcs.Append("<tr style='vertical-align: top;'>");

                    foreach (Column column in m_Collection.Columns)
                    {
                        int n = 0;

                        calcs.Append
                        ("<td align=\"right\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"4\" style=\"font: 8pt arial;\">"
                        );

                        foreach (CalculationResult result in m_Collection.Results)
                        {
                            // Each result must have its label and value
                            // on one line, in a fixed-width font, and
                            // with the correct numerical formatting.

                            string what, format;

                            if (result.Column != i)
                            {
                                continue;
                            }

                            switch (result.What)
                            {
                                case E_FunctionType.Av: what = "Avg"; break;
                                case E_FunctionType.Sm: what = "Tot"; break;
                                case E_FunctionType.Mn: what = "Min"; break;
                                case E_FunctionType.Mx: what = "Max"; break;
                                case E_FunctionType.Ct: what = "Num"; break;
                                default: what = "Unk"; break;
                            }

                            switch (column.Kind)
                            {
                                case Field.E_ClassType.Pct: format = "N3"; break;
                                case Field.E_ClassType.Csh: format = "C2"; break;
                                case Field.E_ClassType.Cnt: format = "N0"; break;
                                default:
                                    {
                                        format = "N";
                                    }
                                    break;
                            }

                            if (result.What == E_FunctionType.Av && column.Kind == Field.E_ClassType.Cnt)
                            {
                                format = "N3";
                            }

                            calcs.AppendFormat("<tr><td align=\"left\"><b>{0}:</b></td><td align=\"right\"><u>{1}</u></td></tr>", what, result.GetFormattedResult(format));

                            ++n;
                        }

                        calcs.Append("</table></td>");

                        ++i;
                    }

                    calcs.Append("</tr>");
                }

                return calcs.ToString();
            }
        }

        public int FieldCount
        {
            get { return m_Collection.Columns.Count; }
        }

        internal ReportLayoutWrapper(Report rCollection)
        {
            // Initialize members.

            m_Collection = rCollection;
        }

    }
}