using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Reminders;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
using LendersOfficeApp.los.admin;
using LendersOffice.Reports;
using System.Diagnostics;
using LendersOffice.HttpModule;
using LendersOffice.Admin;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los
{

	public partial class pipeline : LendersOffice.Common.BaseServicePage 
	{
		protected LendersOfficeApp.los.Portlets.PipelinePortlet m_pipelinePortlet;
        protected LendersOfficeApp.los.Portlets.TestPortlet m_testPortlet;
        protected LendersOfficeApp.los.Portlets.DisclosurePortlet m_disclosurePortlet;
        protected LendersOfficeApp.los.Portlets.MyLeadPortlet m_myLeadPortlet;
        protected LendersOfficeApp.los.Portlets.UnassignLoanPortlet m_unassignLoanPortlet; 
        protected LendersOfficeApp.los.Portlets.LoanPoolPortlet m_loanPoolPortlet;
        protected LendersOfficeApp.los.Portlets.UnassignLeadPortlet m_unassignLeadPortlet;        
        protected LendersOfficeApp.los.Portlets.RemindersPortlet m_remindersPortlet;
        protected LendersOfficeApp.los.Portlets.MyLoanRemindersPortlet m_myLoanRemindersPortlet;
        protected LendersOfficeApp.los.Portlets.DiscTrackPortlet m_discTrackPortlet;
        protected LendersOfficeApp.los.Portlets.TradeMasterPortlet m_tradeMasterPortlet;

        protected LendersOfficeApp.los.common.header m_header;
        private ArrayList m_controlList;
        private BrokerDB m_broker;
        private CPipelineView m_tabView;

        protected String tabNavigatorList = "<ul id='tabList' class='tabnav'>";
        public int selectedTabIndex = 0;
        public int numPipelineTabs = 0;
        protected int numTabs = 0;

        private BrokerUserPrincipal BrokerUser =  BrokerUserPrincipal.CurrentPrincipal;
        public override string StyleSheet
        {
            get { return VirtualRoot + "/css/stylesheet.css"; }
        }
        
        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE10;
        }


        protected bool IsTaskAlertProcessorRunning()
        {
            // Lookup status of dashboard calculator.
            // 6/22/2006 dd - Temporary disable task alert while we move our server to web farm environment.
            return false;
        }
        private string TaskEditorUrl
        {
            get
            {
                BrokerDB brokerDB = this.BrokerUser.BrokerDB;
                if (brokerDB.IsUseNewTaskSystem)
                {
                    return "/newlos/Tasks/TaskEditorV2.aspx";
                }
                else
                {
                    return "/los/reminders/TaskEditor.aspx";
                }
            }
        }
        private void SetVisibleExclusive(System.Web.UI.UserControl ctl) 
        {

            foreach (UserControl uc in m_controlList) 
            {
                if (uc == ctl && uc != null) 
                {
                    uc.Visible = true;
                } 
                else 
                {
                    if (uc != null) 
                    {
                        uc.Visible = false;
                    }
                }
            }
        }

        private Int32 PageIndex(String sPageKey)
        {
            // Walk tab collection and return offset of match.
            // We use the access key to stash a page code for
            // absolute lookup of tabbed pages.

            for (int i = 0; i < m_controlList.Count; i++)
            {
                UserControl portlet = (UserControl)m_controlList[i];
                if (portlet.Attributes["pageId"] == sPageKey)
                    return i;
            }

            return 0;
        }

        private void changeSelectedPipeline(int selectedTabIndex)
        {
            if (m_tabView.Tabs[selectedTabIndex].TabType == E_TabTypeT.PipelinePortlet)
            {
                m_tabView.LastCustomReportId = m_tabView.Tabs[selectedTabIndex].ReportId;
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
		{

            
            HttpCookie supportSuccessCookie = Request.Cookies[LoginUserControl.SupportCookieName];

            if (supportSuccessCookie != null)
            {
                Request.Cookies.Remove(LoginUserControl.SupportCookieName);

                bool status;
                string message = null;

                if (supportSuccessCookie.Value == "EmailFailure")
                {
                    message = "You must enter and confirm a support email on 'My Profile' before using the support center."; 

                }
                else if (bool.TryParse(supportSuccessCookie.Value, out status))
                {
                    if (status)
                    {
                        message = "Thank you, your email has been confirmed. You may now use the support center."; 
                    }
                    else
                    {
                        message = "There was an error confirming your email please verify your email on 'My Profile'.";
                    }
                }

                if (!string.IsNullOrEmpty(message))
                {
                    ClientScript.RegisterHiddenField("EmailConfirmation", message);
                }
            }

            this.HelpFile = "FAQ";

            string eventTarget = Page.Request.Params["__EVENTTARGET"];

            if (!Page.IsPostBack || eventTarget == "addTab" ||
                eventTarget == "reorderTab")
            {
                selectedTabIndex = m_tabView.Index;
            }
            else if (eventTarget == "closeTab")
            {
                selectedTabIndex = m_tabView.Index;
                changeSelectedPipeline(selectedTabIndex);
            }
            else if (eventTarget == "changeTab")
            {
                selectedTabIndex = Int32.Parse(Page.Request.Params["__EVENTARGUMENT"]);
                changeSelectedPipeline(selectedTabIndex);
            }
            else if (ViewState["selectedTabIndex"] != null)
            {
                selectedTabIndex = (Int32)ViewState["selectedTabIndex"];
            }
            else if (RequestHelper.GetSafeQueryString("page") != null)
            {
                string page = RequestHelper.GetSafeQueryString("page");
                try
                {
                    selectedTabIndex = Int32.Parse(page);
                }
                catch (System.FormatException)
                {
                    selectedTabIndex = PageIndex(page);
                }
            }

            // If something went wrong and the index is out of bounds, reset to 0.
            if (selectedTabIndex < 0 || selectedTabIndex >= m_tabView.Count)
            {
                selectedTabIndex = 0;
            }

            m_tabView.Index = selectedTabIndex;
            Tools.UpdateMainTabsSettingXmlContent(BrokerUser.ConnectionInfo, BrokerUser.UserId, m_tabView);
            
            ViewState["selectedTabIndex"] = selectedTabIndex;
            ViewState["numPipelineTabs"] = numPipelineTabs;
            ViewState["numTotalTabs"] = numTabs;
            Context.Items["MainTabSelectedPgKey"] = selectedTabIndex;
            
            SetVisibleExclusive((UserControl)m_controlList[selectedTabIndex]);
            if (m_controlList[selectedTabIndex] is LendersOffice.Common.IAutoLoadUserControl)
            {
                ((LendersOffice.Common.IAutoLoadUserControl)m_controlList[selectedTabIndex]).LoadData();
            }

            IncludeStyleSheet("~/css/Tabs.css");

            RegisterJsGlobalVariables("IsBlankTemplateInvisible", PrincipalFactory.CurrentPrincipal.BrokerDB.IsBlankTemplateInvisibleForFileCreation);
            RegisterJsGlobalVariables("IsEditLeadsInFullLoanEditor", PrincipalFactory.CurrentPrincipal.BrokerDB.IsEditLeadsInFullLoanEditor);
		}

        protected void PageInit( object sender, System.EventArgs a ) 
        {
            //Case 144677
            //the tabs implementation doesn't use these pageId's, but apparently these still get used as the 
            //parameter for 'page' from somewhere...  So these will be used in the meantime to handle those 
            //cases where the old PageId's are still being used.

            m_pipelinePortlet.Attributes.Add("pageId", "Pipe");
            m_disclosurePortlet.Attributes.Add("pageId", "Disc");
            m_unassignLoanPortlet.Attributes.Add("pageId", "NeLo");
            m_myLeadPortlet.Attributes.Add("pageId", "YoLe");
            m_unassignLeadPortlet.Attributes.Add("pageId", "NeLe");
            m_loanPoolPortlet.Attributes.Add("pageId", "LoPo");
            m_tradeMasterPortlet.Attributes.Add("pageId", "Trad");
            m_MyTasksPortlet.Attributes.Add("pageId", "YourTasksTab");
            m_remindersPortlet.Attributes.Add("pageId", "YoTa");
            m_discTrackPortlet.Attributes.Add("pageId", "TaTr");
            m_myLoanRemindersPortlet.Attributes.Add("pageId", "TiYL");
            m_testPortlet.Attributes.Add("pageId", "Test");

            EnableJqueryMigrate = false;

            //OPM 4134 Ethan - required by gService call in f_chat (HeaderFrame.aspx)
            RegisterService("lookupRecord", "/common/RecordsLookupService.aspx");
            RegisterService("loanCreation", "/newlos/LoanCreateService.aspx");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("loanCreation.js");
            RegisterJsScript("LQBPopup.js");

            m_header.ShowRecentWindowList = true;

            // Begin generating the code for the jquery tabs
            // Add user control to tab. Order matter.

            m_controlList = new ArrayList();
            bool[] tabCheckList;

            m_broker = this.BrokerUser.BrokerDB;

            tabCheckList = new bool[12];
            m_tabView = Tools.GetMainTabsSettingXmlContent(BrokerUser.ConnectionInfo, BrokerUser.UserId);
            
            /*
             * Adjust tabs depending on postback event
             */
            if (Page.IsPostBack)
            {
                if (Page.Request.Params["__EVENTTARGET"] == "closeTab")
                {
                    int closeTabIndex = Int32.Parse(Page.Request.Params["__EVENTARGUMENT"]);
                    m_tabView.RemoveAt(closeTabIndex);
                }
                else if(Page.Request.Params["__EVENTTARGET"] == "reorderTab")
                {
                    string args = Page.Request.Params["__EVENTARGUMENT"];
                    int[] positions = Array.ConvertAll(args.Split(','), s => Int32.Parse(s));
                    m_tabView.MoveTo(positions[0], positions[1]);

                }
            }

            /* 
             * Go through and add tabs. If their permission for that tab type
             * has been revoked, then remove the tab from the view
             */
            int ind = 0;
            while (ind < m_tabView.Count)
            {
                CTab tab = m_tabView.Tabs[ind];

                if (CheckPermissionsForTabType(tab.TabType))
                {
                    tabCheckList[(int)tab.TabType] = true;
                    AddTab(tab);
                    ind++;
                }
                else 
                {
                    m_tabView.RemoveAt(ind);
                }
            }

            /* 
             * This is for constructing default tabs for
             * new users/users with new permissions
             */ 
            for(int i = 0; i < tabCheckList.Length; i++)
            {
                bool hasTabType = tabCheckList[i];
                if (hasTabType)
                {
                    continue;
                }
                else
                {
                    if (CheckPermissionsForTabType((E_TabTypeT)i))
                    {
                        CTab newTab = new CTab((E_TabTypeT)i, CTab.GetTabName((E_TabTypeT)i));
                        m_tabView.Add(newTab, false);
                        AddTab(newTab);
                    }
                }
            }

            finalizeTabs();



			// Place a list of tab ids in an array on the client side
			// so we can map positions of tabs into key codes before
			// posting back (which could change the tab layout from
			// permission changes).


            RegisterService("loanutils", "/newlos/LoanUtilitiesService.aspx");
            RegisterJsGlobalVariables("TaskEditorUrl", TaskEditorUrl);
   
        }

        // Should eventually be moved if we support all tab types. Default names can
        // be left to that tab's constructor.
        private bool CheckPermissionsForTabType(E_TabTypeT type)
        {
            switch(type)
            {
                case E_TabTypeT.PipelinePortlet:
                    return BrokerUser.HasAtLeastOneRole(new E_RoleT[] { 
                                    E_RoleT.Processor, E_RoleT.LoanOfficer, E_RoleT.RealEstateAgent, E_RoleT.Manager, E_RoleT.Accountant,
                                    E_RoleT.LenderAccountExecutive, E_RoleT.LockDesk, E_RoleT.Underwriter, E_RoleT.LoanOpener, E_RoleT.Closer,
                                    E_RoleT.Administrator, E_RoleT.Shipper, E_RoleT.Funder, E_RoleT.PostCloser, E_RoleT.CollateralAgent,
                                    E_RoleT.DocDrawer, E_RoleT.Insuring, E_RoleT.CreditAuditor, E_RoleT.DisclosureDesk, E_RoleT.JuniorProcessor, 
                                    E_RoleT.JuniorUnderwriter, E_RoleT.LegalAuditor, E_RoleT.LoanOfficerAssistant, E_RoleT.Purchaser, 
                                    E_RoleT.QCCompliance, E_RoleT.Secondary, E_RoleT.Servicing, E_RoleT.CallCenterAgent});
                case E_TabTypeT.DisclosurePortlet:

                    return ConstStage.DisclosureCountOnPipelineCachingTimeInMs >= 0 && (BrokerUser.HasPermission(Permission.ResponsibleForInitialDiscRetail) ||
                            BrokerUser.HasPermission(Permission.ResponsibleForInitialDiscWholesale) ||
                            BrokerUser.HasPermission(Permission.ResponsibleForRedisclosures));
                case E_TabTypeT.LoanPoolPortlet:
                case E_TabTypeT.TradeMasterPortlet:
                    return (BrokerUser.HasPermission(Permission.AllowCapitalMarketsAccess));
                case E_TabTypeT.MyTasksPortlet:
                    return !ConstStage.DisableTaskPipelineTab && (m_broker.IsUseNewTaskSystem);
                case E_TabTypeT.RemindersPortlet:
                case E_TabTypeT.DisctrackPortlet:
                case E_TabTypeT.MyLoanRemindersPortlet:
                    return (!m_broker.IsUseNewTaskSystem);
                case E_TabTypeT.UnassignedLoanPortlet:
                case E_TabTypeT.MyLeadPortlet:
                case E_TabTypeT.UnassignLeadPortlet:
                    return false;
                case E_TabTypeT.TestPortlet:
                    return BrokerUser.HasPermission(Permission.AllowCreatingTestLoans);
                default:
                    throw CBaseException.GenericException("Unsupported tab type: " + type);
            }
        }

        private string GetTabColoring(E_TabTypeT type)
        {
            switch (type)
            {
                case E_TabTypeT.PipelinePortlet:
                    return "#3b50ce";
                case E_TabTypeT.DisclosurePortlet:
                    return "#dd191d";
                case E_TabTypeT.LoanPoolPortlet:
                case E_TabTypeT.TradeMasterPortlet:
                    return "#43A047";
                case E_TabTypeT.MyTasksPortlet:
                case E_TabTypeT.RemindersPortlet:
                case E_TabTypeT.DisctrackPortlet:
                case E_TabTypeT.MyLoanRemindersPortlet:
                    return "#FB8C00";
                case E_TabTypeT.UnassignedLoanPortlet:
                case E_TabTypeT.MyLeadPortlet:
                case E_TabTypeT.UnassignLeadPortlet:
                    return "#424242";
                case E_TabTypeT.TestPortlet:
                    return "#FDD835";
                default:
                    throw CBaseException.GenericException("Unsupported tab type: " + type);
            }
        }

        private UserControl GetPortlet(E_TabTypeT tabType)
        {
            switch (tabType)
            {
                case E_TabTypeT.PipelinePortlet:
                    return m_pipelinePortlet;
                case E_TabTypeT.DisclosurePortlet:
                    return m_disclosurePortlet;
                case E_TabTypeT.LoanPoolPortlet:
                    return m_loanPoolPortlet;
                case E_TabTypeT.TradeMasterPortlet:
                    return m_tradeMasterPortlet;
                case E_TabTypeT.MyTasksPortlet:
                    return m_MyTasksPortlet;
                case E_TabTypeT.RemindersPortlet:
                    return m_remindersPortlet;
                case E_TabTypeT.DisctrackPortlet:
                    return m_discTrackPortlet;
                case E_TabTypeT.MyLoanRemindersPortlet:
                    return m_myLoanRemindersPortlet;
                case E_TabTypeT.UnassignedLoanPortlet:
                    return m_unassignLoanPortlet;
                case E_TabTypeT.MyLeadPortlet:
                    return m_myLeadPortlet;
                case E_TabTypeT.UnassignLeadPortlet:
                    return m_unassignLeadPortlet;
                case E_TabTypeT.TestPortlet:
                    return m_testPortlet;
                default:
                    throw CBaseException.GenericException("Unsupported tab type: " + tabType);
            }
        }

        private void AddTab(CTab tab)
        {
            var htmlString = Server.HtmlEncode(tab.TabName);
            string label = string.IsNullOrEmpty(htmlString.TrimWhitespaceAndBOM()) ? "&nbsp" : htmlString;
            var control = GetPortlet(tab.TabType);
            string borderTopColor = GetTabColoring(tab.TabType);

            string disclosureCss = "";
            
            // This will make it so that the disclosure tabs will overflow to the left instead of right
            // Also generates dynamic tab name
            if (tab.TabType == E_TabTypeT.DisclosurePortlet)
            {
                disclosureCss = "direction: rtl; ";

                label = String.Format("Disclosures ({0} Due)", GetDisclosurePipelineCount(this.BrokerUser));
            }

            tabNavigatorList += "<li class='mainTabs' id='tab" + numTabs + "'><a href=\"#\" style='"+disclosureCss+"border-top-color: " + borderTopColor + ";'  onclick=\"onTabClick( '" + numTabs + "')\">" + label + "</a></li>";

            m_controlList.Add(control);

            if (tab.TabType == E_TabTypeT.PipelinePortlet)
            {
                numPipelineTabs++;
            }
            numTabs++;

        }


        private int GetDisclosurePipelineCount(BrokerUserPrincipal principal)
        {
            int cacheDuration = ConstStage.DisclosureCountOnPipelineCachingTimeInMs;

            if (cacheDuration == 0)
            {
                return GetUncachedDisclosurePipelineCount(principal);
            }
            else if (cacheDuration < 0)
            {
                return -1;
            }

            // If any of the permissions or the const stage changes, it will force a refresh.
            string key = $"DiscCount_{principal.HasPermission(Permission.ResponsibleForInitialDiscWholesale)}_{principal.HasPermission(Permission.ResponsibleForInitialDiscRetail)}_{principal.HasPermission(Permission.ResponsibleForRedisclosures)}_{cacheDuration}";
            string data = AutoExpiredTextCache.GetFromCacheByUser(principal, key);
            int count; 

            if (string.IsNullOrEmpty(data))
            {
                count = this.GetUncachedDisclosurePipelineCount(principal);
                AutoExpiredTextCache.AddUserString(principal, count.ToString(), TimeSpan.FromMilliseconds(cacheDuration), key);
            }
            else
            {
                count = int.Parse(data);
            }

            return count;
        }

        private int GetUncachedDisclosurePipelineCount(BrokerUserPrincipal principal)
        {
            Stopwatch sw = Stopwatch.StartNew();
            var reportRunner = new LoanReporting();
            var report = reportRunner.DisclosurePipeline(BrokerUser, true);
            sw.Stop();

            var monitorItem = PerformanceMonitorItem.Current;
            if (monitorItem != null)
            {
                monitorItem.AddTimingDetail("LoanReporting.DisclosurePipeline", sw.ElapsedMilliseconds);
            }

            return report.Flatten().Count;
        }



        private void finalizeTabs()
        {
            PassthroughLiteral tabNavLiteral = new PassthroughLiteral();
            tabNavLiteral.Text = tabNavigatorList + "<li class='mainTabs' id='addTabBtn'><a href=\"#\" onclick=\"onAddBtnClick();\">+</a></li></ul>";
            tabs.Controls.Add(tabNavLiteral);

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}
}
