﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.FeeTypes
{
    public partial class FeeTypeTable : System.Web.UI.UserControl
    {
        public string GfeLineNumber
        {
            set { gfeLineNumber.Value = value; }
        }

        public string GfeBoxOption1_Text
        {
            set { gfeSection_A_Text.Value = value; }
        }

        public string GfeBoxOption2_Text
        {
            set { gfeSection_B_Text.Value = value; }
        }

        public string GfeBoxOption3_Text
        {
            set { gfeSection_C_Text.Value = value; }
        }

        public string GfeBoxOption1_Value
        {
            set { gfeSection_A_Value.Value = parseValue(value); }
        }

        public string GfeBoxOption2_Value
        {
            set { gfeSection_B_Value.Value = parseValue(value); }
        }

        public string GfeBoxOption3_Value
        {
            set { gfeSection_C_Value.Value = parseValue(value); }
        }

        private string parseValue(string value)
        {
            return ((E_GfeSectionT)Enum.Parse(typeof(E_GfeSectionT), value)).ToString("D");
        }

        // Used when there is only 1 option
        public string GfeBox_Value
        {
            set { gfeSection_A_Text.Value = value; }
        }

        // When blank, default is 1st visible button
        public string GfeBox_DefaultValue
        {
            set { gfeSection_DefaultValue.Value = parseValue(value); }
        }

        public int getIndexOfRadioButton(String label)
        {
            if (gfeSection_A_Text.Value == label)
                return 1;
            else if (gfeSection_B_Text.Value == label)
                return 2;
            else if (gfeSection_C_Text.Value == label)
                return 3;
            else
                return -1;

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            ControlId.Value = this.ClientID;
            Page.ClientScript.RegisterClientScriptInclude(typeof(FeeTypeTable), "FEE_TYPE_TABLE_SCRIPT", "FeeTypeTable.js");
        }
        
        public void BindGrid(List<FeeType> source)
        {
            if (source.Count >= 1)
            {
                gv.DataSource = source;
                gv.DataBind();
            }
            else
            {
                source.Add(FeeType.CreateEmpty());
                gv.DataSource = source;
                gv.DataBind();
                gv.Rows[0].Style.Add(HtmlTextWriterStyle.Display, "none");
            }
        }

        private void setupRadioButton(HtmlInputRadioButton button, string buttonValue, HtmlGenericControl label, string labelText)
        {
            if (string.IsNullOrEmpty(labelText))
            {
                button.Style.Add(HtmlTextWriterStyle.Display, "none");
            }
            else
            {
                button.Value = buttonValue;
                label.InnerText = labelText;
            }
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            HtmlInputHidden FeeTypeId = (HtmlInputHidden)e.Row.FindControl("FeeTypeId");
            HtmlInputText Description = (HtmlInputText)e.Row.FindControl("Description");
            HtmlInputCheckBox Apr = (HtmlInputCheckBox)e.Row.FindControl("Apr");
            HtmlInputCheckBox Fha = (HtmlInputCheckBox)e.Row.FindControl("Fha");

            FeeTypeId.Value = ((FeeType)e.Row.DataItem).FeeTypeId.ToString();
            Description.Value = ((FeeType)e.Row.DataItem).Description;
            Apr.Checked = ((FeeType)e.Row.DataItem).Apr;
            Fha.Checked = ((FeeType)e.Row.DataItem).FhaAllow;

            // Set gfe section radio fields
            HtmlInputRadioButton gfeSection_A = (HtmlInputRadioButton)e.Row.FindControl("gfeSection_A");
            HtmlInputRadioButton gfeSection_B = (HtmlInputRadioButton)e.Row.FindControl("gfeSection_B");
            HtmlInputRadioButton gfeSection_C = (HtmlInputRadioButton)e.Row.FindControl("gfeSection_C");
            HtmlGenericControl gfeSection_A_lbl = (HtmlGenericControl)e.Row.FindControl("gfeSection_A_lbl");
            HtmlGenericControl gfeSection_B_lbl = (HtmlGenericControl)e.Row.FindControl("gfeSection_B_lbl");
            HtmlGenericControl gfeSection_C_lbl = (HtmlGenericControl)e.Row.FindControl("gfeSection_C_lbl");

            setupRadioButton(gfeSection_A, gfeSection_A_Value.Value, gfeSection_A_lbl, gfeSection_A_Text.Value);
            setupRadioButton(gfeSection_B, gfeSection_B_Value.Value, gfeSection_B_lbl, gfeSection_B_Text.Value);
            setupRadioButton(gfeSection_C, gfeSection_C_Value.Value, gfeSection_C_lbl, gfeSection_C_Text.Value);

            // Select value
            if (((FeeType)e.Row.DataItem).GfeSection == E_GfeSectionT.LeaveBlank && gfeSection_DefaultValue.Value == E_GfeSectionT.LeaveBlank.ToString("D"))
            {
                // No value selected, select first visible radio button
                if (gfeSection_A.Style[HtmlTextWriterStyle.Display] != "none")
                    gfeSection_A.Checked = true;
                else if (gfeSection_B.Style[HtmlTextWriterStyle.Display] != "none")
                    gfeSection_B.Checked = true;
                else
                    gfeSection_C.Checked = true;
            }
            else
            {
                string selectedValue = ((FeeType)e.Row.DataItem).GfeSection == E_GfeSectionT.LeaveBlank ? gfeSection_DefaultValue.Value : ((FeeType)e.Row.DataItem).GfeSection.ToString("D");
                gfeSection_A.Checked = gfeSection_A.Value == selectedValue;
                gfeSection_B.Checked = gfeSection_B.Value == selectedValue;
                gfeSection_C.Checked = gfeSection_C.Value == selectedValue;
            }

            // If only 1 option is defined, disable single option (since no choice really available)
            if (!string.IsNullOrEmpty(gfeSection_A_Text.Value) && string.IsNullOrEmpty(gfeSection_B_Text.Value)
                && string.IsNullOrEmpty(gfeSection_C_Text.Value))
            {
                gfeSection_A.Disabled = true;
            }
        }
    }
}