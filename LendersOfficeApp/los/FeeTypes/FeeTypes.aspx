﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeeTypes.aspx.cs" Inherits="LendersOfficeApp.los.FeeTypes.FeeTypes" %>
<%@ Register TagPrefix="uc1" TagName="FeeTypeTable" Src="~/los/FeeTypes/FeeTypeTable.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>GFE Fee Types</title>
    <style type="text/css">
        body 
        {
            background-color: gainsboro;
        }
        
        div
        {
            padding: 5px;
        }
        
        .sectionLabel
        {
            font-weight: bold;
        }
        
        #divBtns
        {
            float: right;
        }
        
        #divTables
        {
            padding: 0px;
            margin: 0px;
            width: 525px;
        }
    </style>
</head>
<body>
    <script type="text/javascript" >
        var dirty = false;
        var deletedIds = [];

        function confirmExit() {
            if (confirm("Do you want to save changes before closing?")) {
                $('#saveBtn').click();
            }
            onClosePopup();
        }

        window.onbeforeunload = function() {
            if (dirty) {
                return UnloadMessage;
            }
        }

        $(function() {
            resize(550, 700);
            $(".feeTypeRequirementT").change(function() {
                dirty = true;
            });

            $('#cancelBtn').click(function() {
                if (!dirty || confirm("Are you sure you want to close without saving?")) {
                    dirty = false; // So the window.unload will not bother us
                    onClosePopup();
                }
            });

            $('#saveBtn').click(function() {
                if (!dirty) {
                    // Nothing to save, just close
                    onClosePopup();
                    return;
                }

                this.disabled = true;
                var feeTypesToSave = getFeeTypes();
                if (feeTypesToSave) {
                    save(JSON.stringify(feeTypesToSave));
                }
                else {
                    this.disabled = false;
                }
            });

            function getFeeTypes() {
                var feeTypes = [];
                $tables = $('.feeTypeTable');
                $tables.each(function() {
                    var $feeTypeTable = $(this);
                    var $tbody = $feeTypeTable.find("tbody");
                    var $rows = $tbody.find(".GridItem, .GridAlternatingItem");
                    var gfeLineNumber = $feeTypeTable.find(".gfeLineNumber").val();

                    var keepGoing = true;

                    $rows.each(function() {
                        $row = $(this);
                        if (!$rows.is(':visible')) {
                            return true; // ignore hidden row and continue
                        }

                        var description = $.trim($row.find(".description").val());
                        if (!description) {
                            feeTypes = null;
                            keepGoing = false;

                            alert("Cannot Save. One or more Fee Types is missing a description.");
                            return false;
                        }

                        var feeType = {
                            FeeTypeId: $row.find(".feeTypeId").val(),
                            Description: description,
                            GfeLineNumber: gfeLineNumber,
                            GfeSection: $row.find("input[type=radio]:checked").val(),
                            Apr: $row.find(".apr").prop('checked'),
                            Fha: $row.find(".fha").prop('checked')
                        };

                        if (contains(feeType, feeTypes)) {
                            feeTypes = null;
                            keepGoing = false;

                            alert("Cannot Save. Fee Types in the same table must have different descriptions.");
                            return false;
                        }

                        feeTypes.push(feeType);
                    });

                    if (!keepGoing)
                        return false;
                });

                return feeTypes;
            }

            function contains(feeType, ftArray) {
                for (var i = 0; i < ftArray.length;  i++) {
                    if (ftArray[i].GfeLineNumber == feeType.GfeLineNumber
                        && ftArray[i].Description == feeType.Description) {
                        return true;
                    }

                    return false;
                }
            }

            function save(feeTypesToSave) {
                var data = {
                    'feeTypesToSave': feeTypesToSave,
                    'idsToDelete': deletedIds,
                    'feeTypeRequirementT': $("select.feeTypeRequirementT option:selected").val()
                };

                data = JSON.stringify(data);

                callWebMethodAsync({
                    type: "POST",
                    url: "FeeTypes.aspx/Save",
                    data: data,
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    async: false,
                    success: function(msg) {
                        deletedIds = [];
                        dirty = false;
                        onClosePopup();
                    },
                    error: function(msg) {
                        var d = msg.responseText;
                        if (d) {
                            d = JSON.parse(d).Message;
                        }
                        if (console && console.log) {
                            console.log(msg.responseText);
                        }
                        alert('Could not update Error:' + d);

                    }
                });
            }
        });
    </script>
    <h4 class="page-header">Fee Type Setup</h4>
    <form id="form1" runat="server">        
        <div>
            Custom fees must be selected from the below options 
            <asp:DropDownList ID="FeeTypeRequirementT" runat="server" class="feeTypeRequirementT" />
        </div>
        
        <div id="divTables" >
            <div>
                <span class="sectionLabel">800 ITEMS PAYABLE IN CONNECTION WITH LOAN</span>
                <uc1:FeeTypeTable ID="FeeTypeTable_800" runat="server" GfeLineNumber="800" GfeBoxOption1_Text="A1" GfeBoxOption1_Value="B1"
                    GfeBoxOption2_Text="B3" GfeBoxOption2_Value="B3" GfeBoxOption3_Text="N/A" GfeBoxOption3_Value="NotApplicable" />
            </div>

            <div>
                <span class="sectionLabel">900 ITEMS REQUIRED BY LENDER TO BE PAID IN ADVANCE</span>
                <uc1:FeeTypeTable ID="FeeTypeTable_900" runat="server" GfeLineNumber="900" GfeBoxOption1_Text="B3" GfeBoxOption1_Value="B3"
                    GfeBoxOption2_Text="B11" GfeBoxOption2_Value="B11" GfeBoxOption3_Text="N/A" GfeBoxOption3_Value="NotApplicable" />
            </div>
            
            <div>
                <span class="sectionLabel">1000 RESERVES DEPOSITED WITH LENDER</span>
                <uc1:FeeTypeTable ID="FeeTypeTable_1000" runat="server" GfeLineNumber="1000" GfeBox_Value="B9" />
            </div>
            
            <div>
                <span class="sectionLabel">1100 TITLE CHARGES</span>
                <uc1:FeeTypeTable ID="FeeTypeTable_1100" runat="server" GfeLineNumber="1100" GfeBoxOption1_Text="B4" GfeBoxOption1_Value="B4"
                    GfeBoxOption2_Text="B6" GfeBoxOption2_Value="B6" GfeBoxOption3_Text="N/A" GfeBoxOption3_Value="NotApplicable" />
            </div>
            
            <div>
                <span class="sectionLabel">1200 GOVERNMENT RECORDING & TRANSFER CHARGES</span>
                <uc1:FeeTypeTable ID="FeeTypeTable_1200" runat="server" GfeLineNumber="1200" GfeBoxOption1_Text="B7" GfeBoxOption1_Value="B7"
                    GfeBoxOption2_Text="B8" GfeBoxOption2_Value="B8" GfeBox_DefaultValue="B8" />
            </div>
            
            <div>
                <span class="sectionLabel">1300 ADDITIONAL SETTLEMENT CHARGES</span>
                <uc1:FeeTypeTable ID="FeeTypeTable_1300" runat="server" GfeLineNumber="1300" GfeBoxOption1_Text="B4" GfeBoxOption1_Value="B4"
                    GfeBoxOption2_Text="B6" GfeBoxOption2_Value="B6" GfeBoxOption3_Text="N/A" GfeBoxOption3_Value="NotApplicable" />
            </div>
            
            <div id="divBtns" >
                <input type="button" value="Save" id="saveBtn" />
                <input type="button" value="Cancel" id="cancelBtn" />
            </div>
        </div>
    </form>
</body>
</html>
