﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeeTypeTable.ascx.cs" Inherits="LendersOfficeApp.los.FeeTypes.FeeTypeTable" %>
<%@ Import namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess" %>

<style type="text/css">
    .feeTypeTable
    {
        padding: 0px;
        margin: 0px;
    }
</style>

<div class="feeTypeTable">
    <input type="hidden" id="ControlId" runat="server" class="controlId" />
    <input type="hidden" id="gfeSection_A_Text" runat="server" class="optionA_text" />
    <input type="hidden" id="gfeSection_A_Value" runat="server" class="optionA_value" value="0" />
    <input type="hidden" id="gfeSection_B_Text" runat="server" class="optionB_text" />
    <input type="hidden" id="gfeSection_B_Value" runat="server" class="optionB_value" value="0" />
    <input type="hidden" id="gfeSection_C_Text" runat="server" class="optionC_text" />
    <input type="hidden" id="gfeSection_C_Value" runat="server" class="optionC_value" value="0" />
    <input type="hidden" id="gfeSection_DefaultValue" runat="server" class="option_default" value="0" />
    <input type="hidden" id="gfeLineNumber" runat="server" class="gfeLineNumber" />

    <asp:GridView ID="gv" runat="server" RowStyle-CssClass="GridItem" AlternatingRowStyle-CssClass="GridAlternatingItem" HeaderStyle-CssClass="GridHeader" AutoGenerateColumns="false" UseAccessibleHeader="true" Width="500px" OnRowDataBound="gv_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderStyle-Width="20px" ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                    <input type="checkbox" id="chkAll" runat="server" class="chkAll" />
                </HeaderTemplate>
                <ItemTemplate>
                    <input type="checkbox" id="chkDelete" runat="server" class="chkDelete" />
                    <input type="hidden" id="FeeTypeId" runat="server" class="feeTypeId" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fee Type" HeaderStyle-Width="275px" ItemStyle-Width="275px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <input type="text" maxlength="100" id="Description" class="description" runat="server" style="width:95%;" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="GFE Box" HeaderStyle-Width="125px" ItemStyle-Width="125px">
                <ItemTemplate>
                    <input type="radio" name="gfeSection" class="optionA" id="gfeSection_A" runat="server" checked /><span ID="gfeSection_A_lbl" runat="server" class="optionA_lbl" />
                    <input type="radio" name="gfeSection" class="optionB" id="gfeSection_B" runat="server" /><span ID="gfeSection_B_lbl" runat="server" class="optionB_lbl" />
                    <input type="radio" name="gfeSection" class="optionC" id="gfeSection_C" runat="server" /><span ID="gfeSection_C_lbl" runat="server" class="optionC_lbl" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="APR" HeaderStyle-Width="20px" ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <input type="checkbox" id="Apr" runat="server" class="apr" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FHA Allowable" HeaderStyle-Width="60px" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <input type="checkbox" id="Fha" runat="server" class="fha" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <div>
        <input type="button" value="Add new fee" id="add" class="add" />
        <input type="button" value="Delete selected fees" id="delete" class="delete" />
    </div>
</div>