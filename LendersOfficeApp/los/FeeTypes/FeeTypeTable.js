﻿var sNewRow = '<tr>\
	                <td align="center" style="width:20px;">\
                        <input type="checkbox" class="chkDelete" />\
                        <input type="hidden" class="feeTypeId" value="00000000-0000-0000-0000-000000000000" />\
                    </td><td align="center" style="width:275px;">\
                        <input type="text" maxlength="100" class="description" style="width:95%;" />\
                    </td><td style="width:125px;">\
                        <input type="radio" class="optionA" /><span class="optionA_lbl"></span>\
                        <input type="radio" class="optionB" /><span class="optionB_lbl"></span>\
                        <input type="radio" class="optionC" /><span class="optionC_lbl"></span>\
                    </td><td align="center" style="width:20px;">\
                        <input type="checkbox" class="apr" />\
                    </td><td align="center" style="width:60px;">\
                        <input type="checkbox" class="fha" />\
                    </td>\
                </tr>'

$(function() {
    $(".chkAll").click(function() {
        var $this = $(this);
        $this.parents("tbody").find(".chkDelete").prop("checked", $this.prop("checked"));
    });

    $(".chkDelete").on("click", function() {
        var $this = $(this);
        var $tbody = $this.parents("tbody");

        if (!$this.prop("checked")) {
            // unchecked, so uncheck check all box
            $tbody.find(".chkAll").prop("checked", false);
        } else {
            // checked, so check check all box if there are no unchecked checkboxes now
            var $unchecked = $tbody.find(".chkDelete:not(:checked)");
            if ($unchecked.length == 0)
                $tbody.find(".chkAll").prop("checked", true);
        }
    });

    $(".description, .optionA, .optionB, .optionC, .apr, .fha").on("change", function() {
        dirty = true;
    });

    function setupRadioButton($button, buttonValue, $label, labelText) {
        if (!labelText) {
            $button.hide();
        } else {
            $button.val(buttonValue);
            $label.text(labelText);
        }
    }

    $('.delete').click(function() {
        var $this = $(this);
        var $feeTypeTable = $this.parents(".feeTypeTable")
        var $deleteRows = $feeTypeTable.find('.chkDelete:checked').parents("tr");

        $deleteRows.each(function() {
            var $tr = $(this);
            var $feeTypeId = $tr.find('.feeTypeId');

            // Add to deleteIds only if feeTypeId is not empty and not equal to Guid.Empty (ie. not new)
            if ($feeTypeId.val() && $feeTypeId.val() != '00000000-0000-0000-0000-000000000000') {
                deletedIds.push($feeTypeId.val());
            }

            $tr.remove();
        });

        // Recolor remaining items
        // Set row class (for alternate item coloring)
        var $remainingRows = $feeTypeTable.find('.GridItem, .GridAlternatingItem');
        $remainingRows.removeClass();
        $remainingRows.each(function(index) {
            if (index % 2 == 0) {
                $(this).addClass("GridItem");
            } else {
                $(this).addClass("GridAlternatingItem");
            }
        });

        // Uncheck check all box (if it was checked)
        var $tbody = $feeTypeTable.find("tbody");
        $tbody.find(".chkAll").prop("checked", false);

        dirty = true;
    });

    $('.add').click(function() {
        var $this = $(this);
        var $feeTypeTable = $this.parents(".feeTypeTable")
        var $tbody = $feeTypeTable.find("tbody");
        var $rows = $tbody.find(".GridItem, .GridAlternatingItem")
        var rowCount = $rows.length;

        // If table is "empty" just show single hidden row
        if (rowCount == 1 && !$rows.is(':visible')) {
            $rows.show();
        } else { // Otherwise, add row
            var $newRow = $(sNewRow);

            // Set row class (for alternate item coloring)
            if (rowCount % 2 == 0) {
                $newRow.addClass("GridItem");
            } else {
                $newRow.addClass("GridAlternatingItem");
            }

            //Set up radio buttons
            var $optionA = $newRow.find(".optionA");
            var $optionB = $newRow.find(".optionB");
            var $optionC = $newRow.find(".optionC");

            // Group options together
            var controlId = $feeTypeTable.find(".controlId").val()
            $optionA.prop("name", controlId + "_gfeSection_" + rowCount);
            $optionB.prop("name", controlId + "_gfeSection_" + rowCount);
            $optionC.prop("name", controlId + "_gfeSection_" + rowCount);

            var optionA_text = $feeTypeTable.find(".optionA_text").val();
            var optionB_text = $feeTypeTable.find(".optionB_text").val();
            var optionC_text = $feeTypeTable.find(".optionC_text").val();

            setupRadioButton($optionA, $feeTypeTable.find(".optionA_value").val(), $newRow.find(".optionA_lbl"), optionA_text);
            setupRadioButton($optionB, $feeTypeTable.find(".optionB_value").val(), $newRow.find(".optionB_lbl"), optionB_text);
            setupRadioButton($optionC, $feeTypeTable.find(".optionC_value").val(), $newRow.find(".optionC_lbl"), optionC_text);

            // Set up initial selection (default value or first visible radio button)
            var option_default = $feeTypeTable.find(".option_default").val();
            if (optionA_text)
                $optionA.prop('checked', true);
            
            if (optionB_text && (!$optionA.prop('checked') || $optionB.val() == option_default))
                $optionB.prop('checked', true);

            if (optionC_text && ((!$optionA.prop('checked') && !$optionB.prop('checked')) || $optionC.val() == option_default))
                $optionC.prop('checked', true);

            // If only 1 option is defined, disable single option (since no choice really available)
            if (optionA_text && !optionB_text && !optionC_text) {
                $optionA.prop('disabled', true);
            }

            $tbody.append($newRow);

            // new row isn't checked, so uncheck check all box
            $tbody.find(".chkAll").prop("checked", false);
        }

        dirty = true;
    });
});