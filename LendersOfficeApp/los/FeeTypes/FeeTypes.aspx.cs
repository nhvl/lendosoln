﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.los.FeeTypes
{
    public partial class FeeTypes : LendersOffice.Common.BasePage
    {
        protected Guid BrokerId
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerId;
            }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingFeeTypeSetup
                };
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BrokerDB broker = BrokerDB.RetrieveById(BrokerId);

            this.EnableJqueryMigrate = false;
            Tools.Bind_FeeTypeRequirementT(FeeTypeRequirementT);
            Tools.SetDropDownListValue(FeeTypeRequirementT, broker.FeeTypeRequirementT);

            //this part is pretty hardcoded since most of the data needed to make it more dynamic is encapsulated
            if (broker.IsB4AndB6DisabledIn1100And1300OfGfe)
                hideB4andB6();

            FeeTypeTable_800.BindGrid(FeeType.RetrieveAllByBrokerIdAndLineNumber(BrokerId, "800"));
            FeeTypeTable_900.BindGrid(FeeType.RetrieveAllByBrokerIdAndLineNumber(BrokerId, "900"));
            FeeTypeTable_1000.BindGrid(FeeType.RetrieveAllByBrokerIdAndLineNumber(BrokerId, "1000"));
            FeeTypeTable_1100.BindGrid(FeeType.RetrieveAllByBrokerIdAndLineNumber(BrokerId, "1100"));
            FeeTypeTable_1200.BindGrid(FeeType.RetrieveAllByBrokerIdAndLineNumber(BrokerId, "1200"));
            FeeTypeTable_1300.BindGrid(FeeType.RetrieveAllByBrokerIdAndLineNumber(BrokerId, "1300"));
        }

        private void hideB4andB6()
        {
            switch (FeeTypeTable_1100.getIndexOfRadioButton("B6"))
            {
                case 1:
                    FeeTypeTable_1100.GfeBoxOption1_Text = "";
                    break;
                case 2:
                    FeeTypeTable_1100.GfeBoxOption2_Text = "";
                    break;
                case 3:
                    FeeTypeTable_1100.GfeBoxOption3_Text = "";
                    break;
            }

            switch (FeeTypeTable_1300.getIndexOfRadioButton("B4"))
            {
                case 1:
                    FeeTypeTable_1300.GfeBoxOption1_Text = "";
                    break;
                case 2:
                    FeeTypeTable_1300.GfeBoxOption2_Text = "";
                    break;
                case 3:
                    FeeTypeTable_1300.GfeBoxOption3_Text = "";
                    break;
            }
        }

        public class feeTypeRowData
        {
            public Guid FeeTypeId;
            public string Description;
            public string GfeLineNumber;
            public E_GfeSectionT GfeSection;
            public bool Apr;
            public bool Fha;
        }

        [WebMethod]
        public static string Save(string feeTypesToSave, Guid[] idsToDelete, E_FeeTypeRequirementT feeTypeRequirementT)
        {
            List<feeTypeRowData> rows = ObsoleteSerializationHelper.JsonDeserialize<List<feeTypeRowData>>(feeTypesToSave);
            
            List<FeeType> toSave = new List<FeeType>();
            foreach (feeTypeRowData rowData in rows)
            {
                FeeType feeType;
                if (rowData.FeeTypeId == Guid.Empty)
                {
                    feeType = FeeType.Create(PrincipalFactory.CurrentPrincipal.BrokerId);
                }
                else
                {
                    feeType = FeeType.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId, rowData.FeeTypeId);
                }

                feeType.Description = rowData.Description;
                feeType.GfeLineNumber = rowData.GfeLineNumber;
                feeType.GfeSection = rowData.GfeSection;
                feeType.Apr = rowData.Apr;
                feeType.FhaAllow = rowData.Fha;

                toSave.Add(feeType);
            }

            try
            {
                BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);

                FeeType.SaveAndDelete(db.BrokerID, toSave, idsToDelete);

                
                db.FeeTypeRequirementT = feeTypeRequirementT;
                db.Save();

                return "OK";

            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking(e);
                throw;
            }
        }
    }
}
