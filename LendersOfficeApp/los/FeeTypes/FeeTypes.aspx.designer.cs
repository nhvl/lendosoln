﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LendersOfficeApp.los.FeeTypes {
    
    
    public partial class FeeTypes {
        
        /// <summary>
        /// Head1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlHead Head1;
        
        /// <summary>
        /// form1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// FeeTypeRequirementT control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList FeeTypeRequirementT;
        
        /// <summary>
        /// FeeTypeTable_800 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.los.FeeTypes.FeeTypeTable FeeTypeTable_800;
        
        /// <summary>
        /// FeeTypeTable_900 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.los.FeeTypes.FeeTypeTable FeeTypeTable_900;
        
        /// <summary>
        /// FeeTypeTable_1000 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.los.FeeTypes.FeeTypeTable FeeTypeTable_1000;
        
        /// <summary>
        /// FeeTypeTable_1100 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.los.FeeTypes.FeeTypeTable FeeTypeTable_1100;
        
        /// <summary>
        /// FeeTypeTable_1200 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.los.FeeTypes.FeeTypeTable FeeTypeTable_1200;
        
        /// <summary>
        /// FeeTypeTable_1300 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.los.FeeTypes.FeeTypeTable FeeTypeTable_1300;
    }
}
