<%@ Import namespace="LendersOffice.Common"%>
<%@ Register TagPrefix="UC" TagName="Header" Src="common/header.ascx" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/common/HeaderNav.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="CacheTableRequestResults.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.CacheTableRequestResults" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cache Table Request Results</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY style="MARGIN: 0px" bgColor="gainsboro" MS_POSITIONING="FlowLayout">
		<FORM id="CacheTableRequestResults" method="post" runat="server">
			<UC:HEADER id="Header1" runat="server"></UC:HEADER><UC:HEADERNAV id="HeaderNav1" runat="server">
				<MenuItem URL="~/LOAdmin/main.aspx" Label="Main"></MenuItem>
				<MenuItem URL="~/LOAdmin/Broker/CacheTableRequestResults.aspx" Label="Cache Table Request Results"></MenuItem>
			</UC:HEADERNAV>
			<table style="WIDTH: 8in" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td class="FormTable" style="FONT-SIZE: 12pt"><br>
						<table style="WIDTH: 8in" cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td class="FieldLabel" style="FONT-SIZE: 12px">Cache Table Request Results<br>
									<br>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel" style="FONT-SIZE: 12px">
									<P>
										<ml:EncodedLiteral ID="successLit" Runat="server" Text="Successes: " /></P>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel" style="FONT-SIZE: 12px">
									<ml:EncodedLiteral ID="failureLit" Runat="server" Text="Failures: " /></td>
							</tr>
						</table>
						<br>
						<a href="CacheTableRequestErrors.aspx" style="font-size: 10pt; font-weight: bold;">Return to List</a>
					</td>
				</tr>
			</table>
		</FORM>
	</BODY>
</HTML>
