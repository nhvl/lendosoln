﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewConsumer.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.ConsumerPortal.ViewConsumer" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>View Consumer Info</title>
    <style>
        #content table.grid { border-collapse:collapse; border:solid 1px black; }
        #content table.grid td { padding:3px 10px; border:solid 1px #D4D0C8; }
        #content table.grid td:first-child { width:200px; }
        #content { padding: 20px; }
        #content table.grid tr td { background-color: #CCC; }
        #content table.grid tr.alt td { background-color: White; }
    </style>
</head>
<body onload="onInit();">
<h4 class="page-header">View Consumer</h4>
<form id="form1" runat="server">
    <div id="content" runat="server" style="text-align:center">
    <asp:PlaceHolder ID="contentPanel" runat="server">
        <table class="grid">
            <tr>
                <td class="FieldLabel">Id:</td>
                <td><ml:EncodedLabel ID="Id" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr class="alt">
                <td class="FieldLabel">Name:</td>
                <td><ml:EncodedLabel ID="Name" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr>
                <td class="FieldLabel">E-mail:</td>
                <td><ml:EncodedLabel ID="Email" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr class="alt">
                <td class="FieldLabel">Phone:</td>
                <td><ml:EncodedLabel ID="Phone" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr>
                <td class="FieldLabel">Broker Name: </td>
                <td><ml:EncodedLabel ID="BrokerNm" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr class="alt">
                <td class="FieldLabel">Consumer Portal Name:</td>
                <td><ml:EncodedLabel ID="CPName" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr>
                <td class="FieldLabel">Last Logged In:</td>
                <td><ml:EncodedLabel ID="LastLogin" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr class="alt">
                <td class="FieldLabel">Password Reset Code:</td>
                <td><ml:EncodedLabel ID="PwResetCode" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr>
                <td class="FieldLabel">Password Reset Date:</td>
                <td><ml:EncodedLabel ID="PwResetDate" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr class="alt">
                <td class="FieldLabel">Num of Failed Attempts:</td>
                <td><ml:EncodedLabel ID="NumFailed" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr>
                <td class="FieldLabel">Is Temp Password:</td>
                <td><ml:EncodedLabel ID="IsTempPw" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr class="alt">
                <td class="FieldLabel">Notify on Status Changes?</td>
                <td><ml:EncodedLabel ID="NotifyOnStatus" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr>
                <td class="FieldLabel">Notify on New Document Requests?</td>
                <td><ml:EncodedLabel ID="NotifyOnDocs" runat="server"></ml:EncodedLabel></td>
            </tr>
            <tr class="alt">
                    <td class="FieldLabel">Security Pin</td>
                <td><ml:EncodedLabel ID="SecurityPin" runat="server"></ml:EncodedLabel></td>
            </tr>
        </table>
        <table style="padding-top:15px;" cellpadding="3" border="0">
            <tr>
                <td class="FieldLabel">Actions:</td>
                <td>
                    <a href="#" brokerid=<%=AspxTools.HtmlAttribute(BrokerId.ToString()) %> consumerid=<%=AspxTools.HtmlAttribute(ConsumerId.ToString()) %> onclick="return showLoans(this);" >View Loans</a>&nbsp;|&nbsp;
                    <asp:LinkButton ID="SetPwdBtn" Text="Generate Temp Password" runat="server" OnClick="SetPwdBtn_OnClick"></asp:LinkButton> | 
                    <asp:LinkButton ID="ResetLoginAttempts" Text="Clear Login Attempts" runat="server" OnClick="ResetLoginAttempts_OnClick"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">Temp Password:</td>
                <td>
                    <asp:TextBox ID="tempPass" MaxLength="16" runat="server" ReadOnly></asp:TextBox>
                    &nbsp;<a href="#" onclick="copyTempPw(event)">copy</a>
                </td>
            </tr>
        </table>
        </asp:PlaceHolder>
    </div>
    <ml:EncodedLabel ID="ErrorMsg" ForeColor="Red" Visible="false" runat="server"></ml:EncodedLabel>
    <div style="width:100%; text-align:center;" >
        <button type="button" onclick="onClosePopup()">Close</button>
    </div>
</form>
    <script>
    function onInit() { try
    {
        if( window.dialogArguments != null )
        {
            <% if( IsPostBack == false ) { %>
            resize( 500 , 520 );        				
            <% } %>
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}
        
    function showLoans( o ) 
    { 
        try
        {
            var consumerId = o.getAttribute("consumerid");
            var brokerId = o.getAttribute("brokerid");
            showModal( "/LOAdmin/ConsumerPortal/FindLoansConsumer.aspx?consumerid=" + encodeURIComponent(consumerId) + "&brokerid=" +encodeURIComponent(brokerId) );
        }
        catch( e )
        {
            window.status = e.message;
        }
        return false;
    }
    function copyTempPw(event) {
        event.preventDefault();
        var a = event.currentTarget;
        
        var o = document.getElementById("tempPass");
        o.focus();
        o.select();
        if (document.execCommand('copy')) {
            var text = a.textContent;
            a.textContent = "copied";
            setTimeout(function(){ a.textContent = text; }, 1000);
        }
    }
</script>
</body>
</html>
