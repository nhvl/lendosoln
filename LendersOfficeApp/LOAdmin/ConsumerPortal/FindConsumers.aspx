﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FindConsumers.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.ConsumerPortal.FindConsumers"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Find Consumers - LendingQB" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">
<style>
    .note:empty { display:none }
</style>

<div id="container">
    <form runat="server">
        <div class="container">
            <div class="alert alert-danger" id="TooManyResultsMsg" Runat="server" Visible="False" EnableViewState="False">
                Search returns over 100 records. Please refine your search criteria.
            </div>
            
            <div class="panel panel-default"><div class="panel-body">
                <div class="row">
                    <div class="col-sm-3"><div class="form-group form-group-sm">
                        <label>Email:</label>
                        <asp:TextBox ID="m_EmailFilter" runat="server" 
                            class="form-control" autofocus autocomplete="email"/>
                    </div></div>
                    
                    <div class="col-sm-3"><div class="form-group form-group-sm">
                        <label>Name:</label>
                        <asp:TextBox ID="m_NameFilter" runat="server"
                            class="form-control" autocomplete="name"/>
                    </div></div>
                    
                    <div class="col-sm-3"><div class="form-group form-group-sm">
                        <label>Consumer Id:</label>
                        <asp:TextBox runat="server" ID="m_IdFilter"
                            class="form-control" autocomplete="on"/>
                    </div></div>
                    
                    <div class="col-sm-3"><div class="form-group form-group-sm">
                        <label>Broker:</label>
                        <asp:TextBox runat="server" ID="m_BrokerFilter"
                            class="form-control" autocomplete="on"/>
                    </div></div>
                </div>
                
                <div class="clearfix">
                    <div class="pull-left">
                        <span class="clipper"><a href="../main.aspx';" class="btn btn-default">Back</a></span>
                    </div>
                    <div class="pull-right">
                        <span class="clipper"><asp:Button ID="m_Search" OnClick="SearchClick" runat="server" Text="Search" class="btn btn-primary" /></span>
                        <span class="clipper"><asp:Button id="m_Reload" onclick="ReloadClick" runat="server" Text="Reload" class="btn btn-default" /></span>
                    </div>
                </div>
                
                <ml:EncodedLabel id="m_Notes" runat="server" CssClass="note" />

                <div class="table-responsive">
                    <ML:CommonDataGrid id="m_Grid" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyField="Id"
                        CssClass="table table-striped table-link-ws" OnItemCommand="OnGridSort">
                        <Columns>
                            <ASP:TemplateColumn ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <a data-id=<%# AspxTools.HtmlAttribute(Eval("Id").ToString()) %> data-brokerid=<%# AspxTools.HtmlAttribute(Eval("BrokerId").ToString()) %> onclick="return onViewConsumer( this );" href="#">
                                        view
                                    </a>
                                </ItemTemplate>
                            </ASP:TemplateColumn>
                            <asp:BoundColumn HeaderText="Id" DataField="Id" SortExpression="Id" />
                            <asp:TemplateColumn HeaderText="Name" SortExpression="FirstName">
                                <ItemTemplate>
                                    <%# AspxTools.HtmlString((Eval("FirstName").ToString() + " " + Eval("LastName").ToString()).TrimWhitespaceAndBOM()) %>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn HeaderText="Email" DataField="Email" SortExpression="Email" />
                            <ASP:TemplateColumn SortExpression="BrokerNm" HeaderText="Broker">
                                <ItemTemplate>
                                    <a brokerid=<%# AspxTools.HtmlAttribute(Eval("BrokerID").ToString()) %> onclick="return onEditBroker( this );" href="#">
                                        <%# AspxTools.HtmlString( Eval( "BrokerNm").ToString() ) %>
                                    </a>
                                </ItemTemplate>
                            </ASP:TemplateColumn>
                            <ASP:BoundColumn SortExpression="LastLoginD" DataField="LastLoginD" HeaderText="Last Login" />
                        </Columns>
                    </ML:CommonDataGrid>
                </div>
            </div></div>
        </div>
    </form>
</div>

<script>
    function onViewConsumer(o) {
        var Id = o.getAttribute("data-id");
        var BrokerId = o.getAttribute("data-brokerid");
        showModal("/LOAdmin/ConsumerPortal/ViewConsumer.aspx?consumerid=" + Id + "&brokerid=" + BrokerId);
        return false;
    }

    function onEditBroker(o) {
        try {
            var sBrokerId = o.getAttribute("brokerid");
            showModal("/LOAdmin/Broker/BrokerEdit.aspx?cmd=edit&brokerid=" + sBrokerId, null, null, null, function (result) {
                if (result.OK == true && result.commandName != null && result.commandArgs != null) {
                    document.getElementById("m_EventToProcess").value = result.commandName + "*" + result.commandArgs;

                    document.getElementById("m_Reload").click();
                }
            });

        }
        catch (e) {
            window.status = e.message;
        }
        return false;
    }
    "cellspacing rules border style".split(" ").forEach(function (a) {$("table").removeAttr(a)});
</script>
</asp:Content>
