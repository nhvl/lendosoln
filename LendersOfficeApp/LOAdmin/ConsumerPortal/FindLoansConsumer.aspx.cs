﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using System.Data.SqlClient;
using LendersOffice.ObjLib.ConsumerPortal;

namespace LendersOfficeApp.LOAdmin.ConsumerPortal
{
    public partial class FindLoansConsumer : LendersOffice.Admin.SecuredAdminPage
    {
        protected Int64 QueryConsumerId
        {
            get
            {
                return RequestHelper.GetInt("consumerid", -1);
            }
        }

        private Guid BrokerId
        {
            get { return RequestHelper.GetGuid("brokerid"); }
        }

        private void SetErrorMsg(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                ErrorMsg.Text = "";
                return;
            }

            ErrorMsg.Visible = true;
            ErrorMsg.Text = msg;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (QueryConsumerId != -1)
            {
                QueryPanel.Visible = false;
                LoansPanel.Visible = true;
                HeaderPanel.Visible = false;
                //Bind Directly
                DataBind(QueryConsumerId);
            }
            else
            {
                if (!IsPostBack)
                {
                    QueryPanel.Visible = true;
                    LoansPanel.Visible = false;
                }
            }


        }

        protected void SearchClick(object sender, System.EventArgs a)
        {
            //Show results
            DataBind(Int64.Parse(m_ConsumerId.Text));
            LoansPanel.Visible = true;
        }

        protected void DataBind(Int64 consumer)
        {
            try
            {
                var loans = ConsumerPortalUser.GetLinkedLoanApps(BrokerId, QueryConsumerId);
                if (loans.Count() > 0)
                {
                    LoansGrid.DataSource = loans;
                    LoansGrid.DataBind();
                }
                else
                {
                    SetErrorMsg("No loans found");
                }
            }
            catch (SqlException e)
            {
                Tools.LogError(e);
                SetErrorMsg("Exception: " + e.Message);
            }
        }

        protected void LoansGrid_ItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            Label m_LoanNumber = e.Item.FindControl("m_LoanNumber") as Label;
            Label m_BorrowerName = e.Item.FindControl("m_BorrowerName") as Label;
            Label m_PropertyAddress = e.Item.FindControl("m_PropertyAddress") as Label;
            Label m_LoanStatus = e.Item.FindControl("m_LoanStatus") as Label;

            m_LoanNumber.Text = DataBinder.Eval(e.Item.DataItem, "sLNm").ToString();
            m_BorrowerName.Text = DataBinder.Eval(e.Item.DataItem, "aBNm").ToString();
            m_PropertyAddress.Text = DataBinder.Eval(e.Item.DataItem, "sSpFullAddr").ToString();
            m_LoanStatus.Text = DataBinder.Eval(e.Item.DataItem, "LoanStatus").ToString();

        }
    }
}
