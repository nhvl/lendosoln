﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FindLoansConsumer.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.ConsumerPortal.FindLoansConsumer" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../common/header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Find Loans By Consumer</title>
    <link href="../../css/stylesheet.css" type=text/css rel=stylesheet >
    <style type="text/css">
        #content table.grid { border-collapse:collapse; border:solid 1px black; }
        #content table.grid td { padding:1px 6px; border:solid 0px #D4D0C8; }
        /*#content table.grid td:first-child { width:200px; }*/
        #content { padding-top: 20px; }
        #content table.grid tr td { background-color: #CCC; }
        #content table.grid tr.alt td { background-color: White; }
    </style>
</head>
<body onload="onInit();">
    <h4 class="page-header">View Loans</h4>
    <form id="form1" runat="server">
    <asp:panel id="HeaderPanel" Runat="server">
        <UC:HEADER id="Header1" runat="server"></UC:HEADER>
        <UC:HEADERNAV id="HeaderNav1" runat="server">
	        <MenuItem URL="~/LOAdmin/main.aspx" Label="Main" />
		    <MenuItem URL="~/LOAdmin/ConsumerPortal/FindLoansConsumer.aspx" Label="Find Loans for Consumer" />
	    </UC:HEADERNAV>
	</asp:panel>
    <div id="content">
                <asp:panel id="QueryPanel" Runat="server">
						    <table style="WIDTH: 8in" cellSpacing="0" cellPadding="0" border="0">
							    <TR>
								    <td><asp:button id="m_search" onclick="SearchClick" Runat="server" Text="Search"></asp:button><input style="DISPLAY: none" type="text"/></td>
								    <td class="FieldLabel">Consumer ID:
									    <asp:textbox id="m_ConsumerId" Runat="server" columns="37" MaxLength="36"></asp:textbox>
									    <asp:requiredfieldvalidator id="loanIdReq" Runat="server" ControlToValidate="m_ConsumerId" Display="Dynamic" ErrorMessage="* Must provide Consumer ID"></asp:requiredfieldvalidator></td>
							    </TR>
						    </table>
						    <hr/>
				</asp:panel>
    			<asp:panel id="LoansPanel" Runat="server">
    			    <asp:Repeater ID="LoansGrid" runat="server" OnItemDataBound="LoansGrid_ItemBound">
    			        <HeaderTemplate>
    			            <table class="grid" style="margin-left:20px;" cellSpacing="0" cellPadding="0" border="0">
    			        </HeaderTemplate>
    			        <ItemTemplate>
    			            <tr>
    			                <td class="FieldLabel">Loan Number:</td>
    			                <td><ml:EncodedLabel id="m_LoanNumber" runat="server"></ml:EncodedLabel></td>
    			                <td class="FieldLabel">Borrower Name:</td>
    			                <td><ml:EncodedLabel id="m_BorrowerName" runat="server"></ml:EncodedLabel></td>
    			            </tr>
    			            <tr>
    			                <td class="FieldLabel">Property Address:</td>
    			                <td><ml:EncodedLabel id="m_PropertyAddress" runat="server"></ml:EncodedLabel></td>
    			                <td class="FieldLabel">Loan Status:</td>
    			                <td><ml:EncodedLabel id="m_LoanStatus" runat="server"></ml:EncodedLabel></td>
    			            </tr>
    			        </ItemTemplate>
    			        <AlternatingItemTemplate>
    			            <tr class="alt">
    			                <td class="FieldLabel">Loan Number:</td>
    			                <td><ml:EncodedLabel id="m_LoanNumber" runat="server"></ml:EncodedLabel></td>
    			                <td class="FieldLabel">Borrower Name:</td>
    			                <td><ml:EncodedLabel id="m_BorrowerName" runat="server"></ml:EncodedLabel></td>
    			            </tr>
    			            <tr class="alt">
    			                <td class="FieldLabel">Property Address:</td>
    			                <td><ml:EncodedLabel id="m_PropertyAddress" runat="server"></ml:EncodedLabel></td>
    			                <td class="FieldLabel">Loan Status:</td>
    			                <td><ml:EncodedLabel id="m_LoanStatus" runat="server"></ml:EncodedLabel></td>
    			            </tr>
    			        </AlternatingItemTemplate>
    			        <FooterTemplate>
    			            </table>
    			        </FooterTemplate>
    			    </asp:Repeater>
				</asp:panel>
            <div style="COLOR: red; TEXT-ALIGN: center"><ml:EncodedLabel id="ErrorMsg" Runat="server"></ml:EncodedLabel></div>
    
    </div>
    </form>
    <script type="text/javascript">
        <!--

            function onInit() { try
            {
                if( window.dialogArguments != null )
			        {
				        <% if( IsPostBack == false ) { %>
				        resize( 650 , 300 );        				
				        <% } %>
			        }
            }
            catch( e )
            {
                window.status = e.message;
            }}
        
        //-->
    </script>
</body>
</html>
