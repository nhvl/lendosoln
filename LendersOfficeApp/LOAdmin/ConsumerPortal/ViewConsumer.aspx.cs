﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Security.Cryptography;
using LendersOffice.ObjLib.ConsumerPortal;

namespace LendersOfficeApp.LOAdmin.ConsumerPortal
{
    public partial class ViewConsumer : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }
        
        protected Int64 ConsumerId
        {
            get
            {
                return RequestHelper.GetInt("consumerid", -1);
            }
        }
        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("brokerid", Guid.Empty);
            }
        }

        private void BindData()
        {
            ConsumerPortalUser user = ConsumerPortalUser.Retrieve(BrokerId, ConsumerId);
            Id.Text = user.Id.ToString();
            Name.Text = (user.FirstName + " " + user.LastName).TrimWhitespaceAndBOM();
            Email.Text = user.Email;
            Phone.Text = user.Phone;
            BrokerNm.Text = BrokerDB.RetrieveById(user.BrokerId).Name;
            if (user.LastLoginD.HasValue)
            {
                LastLogin.Text = user.LastLoginD.Value.ToString();
            }
            PwResetCode.Text = user.PasswordResetCode;
            if (user.PasswordResetCodeRequestD.HasValue)
            {
                PwResetDate.Text = user.PasswordResetCodeRequestD.Value.ToString();
            }
            NumFailed.Text = user.LoginAttempts.ToString();
            IsTempPw.Text = user.IsTemporaryPassword ? "Yes" : "No";
            NotifyOnStatus.Text = user.IsNotifyOnStatusChanges ? "Yes" : "No";
            NotifyOnDocs.Text = user.IsNotifyOnNewDocRequests ? "Yes" : "No";
            SecurityPin.Text = user.SecurityPin;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ConsumerId == -1)
            {
                content.Visible = false;
                SetErrorMsg("Invalid consumer Id.");
                return;
            }
            if (!IsPostBack)
            {
                SetErrorMsg("");
                BindData();
            }
        }

        private void SetErrorMsg(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                ErrorMsg.Text = "";
                return;
            }

            ErrorMsg.Visible = true;
            ErrorMsg.Text = msg;
        }

        protected void SetPwdBtn_OnClick(object sender, System.EventArgs a)
        {
            ConsumerPortalUser user = ConsumerPortalUser.Retrieve(BrokerId, ConsumerId);
            string password = user.GenerateTempPassword();
            tempPass.Text = password;
            user.Save();
        }

        protected void ResetLoginAttempts_OnClick(object sender, System.EventArgs a)
        {
            ConsumerPortalUser user = ConsumerPortalUser.Retrieve(BrokerId, ConsumerId);
            user.LoginAttempts = 0;
            user.Save();
        }
    }
}
