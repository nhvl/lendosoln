﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;
using System.Web.UI.WebControls;

namespace LendersOfficeApp.LOAdmin.ConsumerPortal
{
    public partial class FindConsumers : LendersOffice.Admin.SecuredAdminPage
    {
        public class _Item
        {
            public long Id { get; set; }
            public Guid BrokerId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string BrokerNm { get; set; }
            public DateTime? LastLoginD { get; set; }
        }
        
        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void SearchClick(object sender, System.EventArgs a)
        {
            // Rebind the grid.
            BindDataGrid();
        }

        /// <summary>
        /// Handle reload request.
        /// </summary>

        protected void ReloadClick(object sender, System.EventArgs a)
        {
            // Rebind the grid.
            BindDataGrid();
        }

        /// <summary>
        /// Handle grid command.
        /// </summary>

        protected void OnGridSort(object sender, DataGridCommandEventArgs a)
        {
            // Need to rebind datagrid everytime user click on sort.
            if (a.CommandName == "sort")
            {
                BindDataGrid();
            }
        }
        private void BindDataGrid()
        {
            // Validate permissions.  If the user can't access
            // each broker to edit, then don't show the edit
            // column.  We also need to block the page so that
            // straight url typing can't bypass security.  As
            // well, we do the same for the other link columns.

            if (HasPermissionToDo(E_InternalUserPermissions.EditEmployee) == false)
            {
                m_Grid.Columns[0].Visible = false;
            }

            List<_Item> list = new List<_Item>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                List<SqlParameter> parameters = new List<SqlParameter>();

                if (!string.IsNullOrEmpty(m_BrokerFilter.Text))
                {
                    parameters.Add(new SqlParameter("@BrokerNm", m_BrokerFilter.Text.Replace("'", "''").TrimWhitespaceAndBOM()));
                }
                if (!string.IsNullOrEmpty(m_EmailFilter.Text))
                {
                    parameters.Add(new SqlParameter("@Email", m_EmailFilter.Text.Replace("'", "''").TrimWhitespaceAndBOM()));
                }
                if (!string.IsNullOrEmpty(m_IdFilter.Text))
                {
                    parameters.Add(new SqlParameter("@Id", m_IdFilter.Text.Replace("'", "''").TrimWhitespaceAndBOM()));
                }
                if (!string.IsNullOrEmpty(m_NameFilter.Text))
                {
                    parameters.Add(new SqlParameter("@Name", m_NameFilter.Text.Replace("'", "''").TrimWhitespaceAndBOM()));
                }

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "ConsumerPortal_FindUsers", parameters))
                {
                    while (reader.Read())
                    {
                        _Item item = new _Item();

                        item.Id = (long)reader["Id"];
                        item.BrokerId = (Guid)reader["BrokerId"];
                        item.FirstName = (string)reader["FirstName"];
                        item.LastName = (string)reader["LastName"];
                        item.Email = (string)reader["Email"];
                        item.BrokerNm = (string)reader["BrokerNm"];
                        item.LastLoginD = reader.SafeDateTimeNullable("LastLoginD");

                        list.Add(item);
                    }
                }
            }


            m_Grid.DataSource = list;
            m_Grid.DataBind();

            m_Notes.Text = String.Format("{0} rows returned", m_Grid.Items.Count);
            TooManyResultsMsg.Visible = m_Grid.Items.Count >= 100;
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
