﻿<%@ Page Language="C#" CodeBehind="EConsentExpiration.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Dislcosure.EConsentExpiration"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="E-Consent Expiration" %>

<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style type="text/css">
        .center-div {
            margin: 0 auto;
            /*min-width: 200px;
            max-width: 500px;*/
            width: 70%;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id$='LoanId']").focus();
        });
        
        function setTooltip(selector, title) {
            selector.prop("title", title);
            selector.focus();
            selector.tooltip({ placement: 'bottom' });
            selector.tooltip('show');
            selector.one("blur change", function () { selector.tooltip("destroy"); });
        }
        function confirmUserAction(e) {
            e.preventDefault();
            var data = { LoanId: $("[id$='LoanId']").val().trim() }
            if (!data.LoanId) {
                $("[id$='LoanId']").val('');
                setTooltip($("[id$='LoanId']"), "Please fill out the Loan Id");
                return false;
            }
            $('#myConfirm #btnOK').click(function () {
                $('#myConfirm').modal('hide');
                //return true;
                return submitEConsent();
            });
            $('#myConfirm').modal('show');
            return false;
        }
        function transformResponse(text) {
            try {
                return JSON.parse(text, function (key, value) {
                    if (typeof value == "string") {
                        var result, d;
                        // parse ASP.NET DateTime type
                        {
                            result = /\/Date\((-?[0-9]+)(?:[a-zA-Z]|(?:\+|-)[0-9]{4})?\)\//g.exec(value);
                            if (!!result) {
                                return new Date(parseFloat(result[1]));
                            }
                        }
                    }
                    return value;
                });
            } catch (e) {
                return text;
            }
        }
        function submitEConsent() {
            //e.preventDefault();
            var data = { LoanId: $("[id$='LoanId']").val().trim() }
            if (!data.LoanId) {
                $("[id$='LoanId']").val('');
                setTooltip($("[id$='LoanId']"), "Please fill out the Loan Id");
                return false;
            }
            $.ajax({
                type: "POST",
                url: window.location.pathname + "/RunEConsent",
                data: JSON.stringify(data),
                contentType: "application/json; charset:utf-8",
                dataType: "json",
                converters: { "text json": transformResponse }
            }).done(function (data, textStatus, jqXHR) {
                if (!data.d.Success) {
                    showError(data.Content);
                    return false;
                }
                else {
                    return true;
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                showError(JSON.parse(jqXHR.responseText).Message);
                return false;
            });
            return false;
        }
        function showError(message) {
            $("div[role='alert']").removeClass("hide");
            $("[id$='ErrMsg']").html(message);
        }
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="center-div">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">E-Consent Expiration</h3>
                    </div>
                    <div class="panel-body">
                        <form onsubmit="return confirmUserAction(event);">
                            <p>
                                This page will allow manually expiring e-consent for a single loan
            file that currently has sDisclosureNeededT set to 
            E_sDisclosureNeededT.AwaitingEConsent.
                            </p>
                            <p>
                                It is meant for use when the nightly process fails to update a file.
                            </p>
                            <p>
                                You will need to manually verify that the update worked. If the 
            request finishes without logging any errors it likely succeeded.
                            </p>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input id="LoanId" class="form-control pull-right" required style="width: 350px" autocomplete="off" />
                                </div>
                                <div class="col-xs-6">
                                    <span class="clipper">
                                        <button type="submit" id="RunEConsentExpirationForSingleLoan" class="btn btn-primary">Manually Expire E-Consent for Loan Id</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                        <p>
                            <div class="alert alert-error alert-dismissible hide" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Error Message!</strong>
                                <div id="ErrMsg">Numquam quos fuga quam suscipit sapiente perferendis magnam. </div>
                            </div>
                        </p>
                        <!--Confirm Dialog-->
                        <div id="myConfirm" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog modal-width">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Confirm E-Consent Expiration
                                        </h4>
                                    </div>
                                    <div class="modal-body text-center">
                                        Are you sure you want to manually expire e-consent for this file?
                                        This action cannot be undone.
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel</button></span> <span class="clipper">
                                                    <button type="button" class="btn btn-primary" id="btnOK">
                                                        OK</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
