﻿<%@ Page Language="C#" CodeBehind="DisclosureAutomation.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Dislcosure_Automation.DisclosureAutomation"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Disclosure Automation" %>

<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style>
        .center-div {
            margin: 0 auto;
            width: 60%;
        }
    </style>
    <script type="text/javascript">
        $(function() {
            $("[id$='LoanId']").focus();
        });
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="center-div">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="form1" runat="server" class="form-horizontal">
                            <p>
                                This page will provide a read-only view of the disclosure fields and history of a loan. Any modifications should 
            be done directly via the database.
                            </p>
                            <div class="form-group">
                                <label class="control-label col-xs-2">Loan ID:</label>
                                <div class="col-xs-6">
                                    <asp:TextBox ID="LoanId" CssClass="form-control" Text="425109f1-ca83-4375-9062-fb1cbe3062d5" runat="server" autocomplete="off"></asp:TextBox>
                                </div>
                                <div class=" col-xs-4">
                                <span class="clipper">
                                    <asp:Button ID="Lookup" Text="Lookup" runat="server" OnClick="Lookup_OnClick" CssClass="btn btn-primary" UseSubmitBehavior="false"/>
                                </span>
                                </div>
                            </div>
                            <div id="ErrPanel" runat="server">
                            <div class="alert alert-error alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Error Message!</strong> <span id="ErrMsg" runat="server">Numquam quos fuga quam suscipit sapiente perferendis magnam. </span>
                            </div>
                            </div>
                            <div id="Content" runat="server">
                                <label>sLNm: </label>
                                <span id="sLNm" runat="server"></span>
                                <div class="form-group">
                                    <div class="checkbox col-xs-2">
                                        <label>
                                            <asp:CheckBox ID="sNeedInitialDisc" Enabled="false" runat="server" />sNeedInitialDisc</label>
                                    </div>
                                    <div class="checkbox  col-xs-2">
                                        <label>
                                            <asp:CheckBox ID="sNeedRedisc" Enabled="false" runat="server" />sNeedRedisc</label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <asp:CheckBox ID="sEConsentCompleted" Enabled="false" runat="server" />sEConsentCompleted</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-2">sDisclosureNeededT: </label>
                                    <div class="col-xs-8">
                                        <asp:DropDownList CssClass="form-control" ID="sDisclosureNeededT" Enabled="false" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        <label>sDisclosuresDueD: </label>
                                        <span id="sDisclosuresDueD" runat="server"></span>
                                    </div>
                                    <div>
                                        <label>sLastDisclosedD: </label>
                                        <span id="sLastDisclosedD" runat="server"></span>
                                    </div>
                                </div>


                                <asp:GridView ID="m_borrowerInfo" Caption="Borrower Info" runat="server" CssClass="table table-striped table-condensed table-layout-fix"
                                    CellSpacing="0" CellPadding="0" BorderWidth="0" GridLines="None"
                                    AllowSorting="false" OnSorting="m_borrowerInfo_Sorting">
                                </asp:GridView>


                                <asp:GridView ID="m_disclosureAuditHistory" AutoGenerateColumns="false" Caption="Disclosure Audit History" runat="server" CssClass="table table-striped table-condensed table-layout-fix"
                                    CellSpacing="0" CellPadding="0" BorderWidth="0" GridLines="None" UseAccessibleHeader="true">
                                    <Columns>
                                        <asp:BoundField DataField="Timestamp" HeaderText="Timestamp" HeaderStyle-CssClass="col-xs-3" />
                                        <asp:BoundField DataField="Description" HeaderText="Description" HeaderStyle-CssClass="col-xs-7" />
                                        <asp:BoundField DataField="UserName" HeaderText="User" HeaderStyle-CssClass="col-xs-2" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
