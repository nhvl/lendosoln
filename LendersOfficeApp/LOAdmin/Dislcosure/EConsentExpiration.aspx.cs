﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Disclosure;
using System.Web.Services;
namespace LendersOfficeApp.LOAdmin.Dislcosure
{
    public partial class EConsentExpiration : LendersOffice.Admin.SecuredAdminPage
    {
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        [WebMethod]
        public static EConsentStatus RunEConsent(string LoanId)
        {
            //Validate input
            if (String.IsNullOrEmpty(LoanId)) return new EConsentStatus(false, "Loan Id cannot be empty.");

            var loanIds = new List<Guid>();
            loanIds.Add(new Guid(LoanId));

            bool generateCriticalOnFailure = false;
            NightlyDisclosureRunner.ProcessEConsentExpirationForLoans(
                loanIds,
                generateCriticalOnFailure);
            return new EConsentStatus(true, "");
        }
        public class EConsentStatus
        {
            public bool Success { get; set; }
            public string Content { get; set; }
            public EConsentStatus(bool status, string content)
            {
                Success = status;
                Content = content;
            }
        }
    }
}
