﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Audit;
using System.Data;
using System.ComponentModel;

namespace LendersOfficeApp.LOAdmin.Dislcosure_Automation
{
    public partial class DisclosureAutomation : LendersOffice.Admin.SecuredAdminPage
    {
        //Rebuild by Long Nguyen
        //Email: longn@meridianlink.net
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        private class BorrowerAuditInfo
        {
            public string Name { get; private set; }
            public string Email { get; private set; }
            public string EConsentReceivedD { get; private set; }
            public string EConsentDeclinedD { get; private set; }

            public BorrowerAuditInfo(string name, string email, string eConsentReceivedD, string eConsentDeclinedD)
            {
                Name = name;
                Email = email;
                EConsentReceivedD = eConsentReceivedD;
                EConsentDeclinedD = eConsentDeclinedD;
            }
        }

        private void BindDDL()
        {
            sDisclosureNeededT.Items.Add(new ListItem("None", E_sDisclosureNeededT.None.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Initial Disclosures - RESPA Application Received", E_sDisclosureNeededT.InitDisc_RESPAAppReceived.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Initial Disclosures - Loan Registered", E_sDisclosureNeededT.InitDisc_LoanRegistered.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Initial Disclosures - Consumer Portal Verbal Submission", E_sDisclosureNeededT.InitDisc_RESPAAppReceivedVerbally.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("E-Consent Declined - Paper Disclosure Required", E_sDisclosureNeededT.EConsentDec_PaperDiscReqd.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("E-Consent Not Received - Paper Disclosure Required", E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("APR Out of Tolerance, Redisclosure Required", E_sDisclosureNeededT.APROutOfTolerance_RediscReqd.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - Loan Amount Change", E_sDisclosureNeededT.CC_LoanAmtChanged.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - Loan Locked", E_sDisclosureNeededT.CC_LoanLocked.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - Lock Extended", E_sDisclosureNeededT.CC_LockExtended.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - Relocked", E_sDisclosureNeededT.CC_Relocked.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - Float Down Lock", E_sDisclosureNeededT.CC_FloatDownLock.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - GFE Charge/Discount Changed", E_sDisclosureNeededT.CC_GFEChargeDiscountChanged.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - Property Value Changed", E_sDisclosureNeededT.CC_PropertyValueChanged.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - U/W Credit Score Changed", E_sDisclosureNeededT.CC_UWCreditScoreChanged.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - Program Changed", E_sDisclosureNeededT.CC_ProgramChanged.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - Impound Required Field Changed", E_sDisclosureNeededT.CC_ImpoundChanged.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - Purchase Price Changed", E_sDisclosureNeededT.CC_PurchasePriceChanged.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Changed Circumstance - Custom Redisclosure Field Changed", E_sDisclosureNeededT.Custom.ToString("d")));
            sDisclosureNeededT.Items.Add(new ListItem("Awaiting E-Consent", E_sDisclosureNeededT.AwaitingEConsent.ToString("d")));
        }

        protected override void OnInit(EventArgs e)
        {
            BindDDL();
            ErrPanel.Visible = false;
            Content.Visible = false;

            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Lookup_OnClick(object sender, EventArgs e)
        {
            var EmptyChar = "-";
            var lid = LoanId.Text.Trim();
            if (String.IsNullOrEmpty(lid))
            {
                ShowError("Loan ID is required.");
                return;
            }
            Guid sLId = new Guid(lid);
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId,
                typeof(DisclosureAutomation));
            dataLoan.InitLoad();

            sLNm.InnerText = dataLoan.sLNm;
            sNeedInitialDisc.Checked = dataLoan.sNeedInitialDisc;
            sNeedRedisc.Checked = dataLoan.sNeedRedisc;
            sEConsentCompleted.Checked = dataLoan.sEConsentCompleted;
            Tools.SetDropDownListValue(sDisclosureNeededT, dataLoan.sDisclosureNeededT);
            sDisclosuresDueD.InnerText = dataLoan.sDisclosuresDueD_rep;
            sLastDisclosedD.InnerText = dataLoan.sLastDisclosedD.IsValid ?
                dataLoan.sLastDisclosedD.ToStringWithTime("M/dd/yyyy HH:mm:ss") :
                String.Empty;

            //Make up
            if (String.IsNullOrEmpty(sDisclosuresDueD.InnerText))
            {
                sDisclosuresDueD.InnerText = EmptyChar;
            }
            if (String.IsNullOrEmpty(sLastDisclosedD.InnerText))
            {
                sLastDisclosedD.InnerText = EmptyChar;
            }

            var borrowerInfo = new List<BorrowerAuditInfo>();
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);

                borrowerInfo.Add(new BorrowerAuditInfo(dataApp.aBNm, dataApp.aBEmail,
                    dataApp.aBEConsentReceivedD.ToStringWithTime("M/dd/yyyy HH:mm:ss"),
                    dataApp.aBEConsentDeclinedD.ToStringWithTime("M/dd/yyyy HH:mm:ss")));
                if (dataApp.aCIsValidNameSsn)
                {
                    borrowerInfo.Add(new BorrowerAuditInfo(dataApp.aCNm, dataApp.aCEmail,
                        dataApp.aCEConsentReceivedD.ToStringWithTime("M/dd/yyyy HH:mm:ss"),
                        dataApp.aCEConsentDeclinedD.ToStringWithTime("M/dd/yyyy HH:mm:ss")));
                }
            }

            m_borrowerInfo_dtb = Dislcosure_Automation_Method.ToDataTable(borrowerInfo);

            m_borrowerInfo.DataSource = m_borrowerInfo_dtb;
            m_borrowerInfo.DataBind();
            if (m_borrowerInfo.Rows.Count != 0)
            {
                m_borrowerInfo.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            var disclosureAuditItems = AuditManager.RetrieveAuditList(sLId)
                .Where(audit => audit.CategoryT == E_AuditItemCategoryT.DisclosureESign);
            m_disclosureAuditHistory.DataSource = disclosureAuditItems;
            m_disclosureAuditHistory.DataBind();
            if (m_disclosureAuditHistory.Rows.Count != 0)
            {
                m_disclosureAuditHistory.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            ShowContent();

        }


        private DataTable m_borrowerInfo_dtb;
        private string m_borrowerInfo_sortDirection = "ASC";
        //private DataTable m_disclosureAuditHistory_dtb;
        //private string m_disclosureAuditHistory_sortDirection = "ASC";
        protected void m_borrowerInfo_Sorting(object sender, GridViewSortEventArgs e)
        {
            SetSortDirection(ref m_borrowerInfo_sortDirection);
            if (m_borrowerInfo_dtb != null)
            {
                //Sort the data.
                m_borrowerInfo_dtb.DefaultView.Sort = e.SortExpression + " " + m_borrowerInfo_sortDirection;
                m_borrowerInfo.DataSource = m_borrowerInfo_dtb;
                m_borrowerInfo.DataBind();
                if (m_borrowerInfo.Rows.Count != 0)
                {
                    m_borrowerInfo.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
                int columnIndex = 0;
                foreach (DataControlFieldHeaderCell headerCell in m_borrowerInfo.HeaderRow.Cells)
                {
                    if (headerCell.ContainingField.SortExpression == e.SortExpression)
                    {
                        columnIndex = m_borrowerInfo.HeaderRow.Cells.GetCellIndex(headerCell);
                    }
                }
            }
        }
        protected void SetSortDirection(ref string sortDirection)
        {
            if (sortDirection == "ASC")
            {
                sortDirection = "DESC";
            }
            else
            {
                sortDirection = "ASC";
            }
        }
        private void ShowError(Exception ex)
        {
            Content.Visible = false;
            ErrMsg.InnerText = parseException(ex);
            ErrPanel.Visible = true;
        }
        private void ShowError(string message)
        {
            Content.Visible = false;
            ErrMsg.InnerText = message;
            ErrPanel.Visible = true;
        }
        string parseException(Exception e)
        {
            var message = "";
            if (String.IsNullOrEmpty(e.Message))
                message = e.ToString();
            else
                message = e.Message;
            if (!message.Contains("UserMessage:") && !message.Contains("DeveloperMessage:")) return message;
            var msg=message.Split(new string[] { "UserMessage:", "DeveloperMessage:" }, StringSplitOptions.RemoveEmptyEntries);
            return msg[1].Trim();
        }
        private void ShowContent()
        {
            Content.Visible = true;
            ErrPanel.Visible = false;
        }
    }
    public static class Dislcosure_Automation_Method
    {
        //http://stackoverflow.com/questions/564366/convert-generic-list-enumerable-to-datatable
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
