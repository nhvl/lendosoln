﻿<%@ Page language="C#" Codebehind="BrowserUsageStatistics.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.BrowserUsageStatistics"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Browser Usage Statistics - LendingQB" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">

<div class="container"><div class="panel panel-default"><div class="panel-body ng-cloak" ng-app="app">
    <browser-usage-statistics><p>loading&hellip;</p></browser-usage-statistics>
</div></div></div>

<form runat="server"></form>
</asp:Content>
