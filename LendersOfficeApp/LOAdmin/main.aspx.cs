using System;
using System.Collections;
using System.Linq;
using System.Threading;
using System.Web.Services;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin
{
    public partial class main : BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RegisterCSS("newdesign.main.css");
                RegisterCSS("newdesign.lendingqb.css");

                RegisterJsScript("jquery-1.11.2.min.js");
                RegisterJsScript("masonry-3.2.2.min.js");
                RegisterJsScript("bootstrap-3.3.4.min.js");

                RegisterJsScript("IEPolyfill.js");
                RegisterJsScript("LOAdmin.Main.js");
            }
            
            EnforceUserAuthenthicateAndDisplayName();

            //add copyright to footer
            copyright.Text = ConstAppDavid.CopyrightMessage;
        }

        void EnforceUserAuthenthicateAndDisplayName()
        {
            var principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            
            // if pricipal doesn't exist, log out user
            if (principal == null) 
            {
                Response.Redirect("~/logout.aspx");
                return;
            }

            adminName.Text = string.IsNullOrEmpty(principal.DisplayName) ? "Administrator" : principal.DisplayName;
        }

        [WebMethod]
        public static IEnumerable GetQueueStatus()
        {
            var principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (principal == null) { throw new UnauthorizedAccessException("User has not logged in."); }
            
            return DBMessageQueue.GetQueueCounts().Select(i => new {id = i.Id, name = i.Name, count = i.Count, ageInSeconds = i.OldestMessageAgeInSeconds});
        }

        protected void GenerateError_OnClick(object sender, EventArgs e)
        {
            ErrorUtilities.DisplayErrorPage("This is a test error. Generated in LoAdmin please ignore.", false, Guid.Empty, Guid.Empty);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion
    }
}
