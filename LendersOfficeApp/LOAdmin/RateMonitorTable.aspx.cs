﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.RateMonitor;
using DataAccess;
using System.Web.UI;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin
{
    public partial class RateMonitorTable : LendersOffice.Admin.SecuredAdminPage
    {
        class _Item
        {
            public long Id { get; set; }
            public Guid sLId { get; set; }
            public decimal Rate { get; set; }
            public decimal Point { get; set; }
            public string EmailAddresses { get; set; }
            public string CcAddresses { get; set; }
            public string BPDesc { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime? DisabledDate { get; set; }
            public string DisabledReason { get; set; }
            public bool IsEnabled { get; set; }
            public DateTime? LastRunDate { get; set; }
            public long? LastRunBatchId { get; set; }
        }
        protected int s_maxPageSize = 50;

        protected void PageLoad(object sender, EventArgs a)
        {
            //Programatically add Email label template (for dynamic # of emails)
            TemplateColumn tc = new TemplateColumn();
            tc.HeaderText = "Email Addresses";
            tc.SortExpression = "EmailAddresses";
            tc.ItemStyle.Width = Unit.Percentage(15);
            tc.ItemTemplate = new EmailCell();
            m_Grid.Columns.AddAt(6, tc);

            PageLoadBind();

            if(!IsPostBack)
            {
                UpdateCompletedToday(null, null);
            }
        }

        protected void UpdateCompletedToday(object sender, EventArgs a)
        {
            int completed = RateMonitorProcessor.CompletedToday;
            if (completed == 1)
            {
                m_completedToday.InnerText = "Yes";
                m_completedToday.Style["Color"] = "Green";
            }
            else if (completed == 0)
            {
                m_completedToday.InnerText = "No";
                m_completedToday.Style["Color"] = "Red";
            }
            else
            {
                m_completedToday.InnerText = "Unknown";
                m_completedToday.Style["Color"] = "Gray";
            }
            m_completedToday.Style["font-weight"] = "bold";
            
        }

        protected class EmailCell : ITemplate
        {
            //Fill column with PlaceHolders on load
            public void InstantiateIn(Control container)
            {
                PlaceHolder cell = new PlaceHolder();
                cell.DataBinding += new EventHandler(this.BindData);
                container.Controls.Add(cell);
            }
            
            //Add emails to PlaceHolder once data is bound
            private void BindData(object sender, EventArgs args)
            {
                PlaceHolder cell= (PlaceHolder)sender;
                string[] e = DataBinder.Eval(((DataGridItem)cell.Parent.Parent).DataItem, "EmailAddresses").ToString().Split(',', ';'),
                    c = DataBinder.Eval(((DataGridItem)cell.Parent.Parent).DataItem, "CcAddresses").ToString().Split(',', ';');
                
                if(c[0].TrimWhitespaceAndBOM()!="")
                {
                    int ccStartIndex = e.Length;
                    Array.Resize(ref e, e.Length + c.Length);
                    for (int i = 0; i < c.Length; i++)
                    {
                        e[i+ccStartIndex]=c[i]+" (cc)"; //append " (cc)" to cc addresses
                    }
                }

                //Put each email address on its own line
                Label emailLabel;
                foreach (string email in e)
                {
                    emailLabel = new Label();
                    emailLabel.Text = email+"<br />";
                    cell.Controls.Add(emailLabel);
                }
            }
        }

        private void PageLoadBind()
        {
            // Hide Error Messages; Enable as needed
            m_loanIdError.Style["visibility"] = "hidden";
            m_monitorIdError.Style["visibility"] = "hidden";
            m_BeforeDateError.Style["visibility"] = "hidden";
            m_AfterDateError.Style["visibility"] = "hidden";
            
            List<_Item> list = new List<_Item>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                //Parameters for search query
                List<SqlParameter> parameters = new List<SqlParameter>();

                //Monitor ID
                if (m_monitorIdField.Text.Length > 0)
                {
                    try
                    {
                        SqlParameter param = new SqlParameter("@monitorid", SqlDbType.BigInt);
                        param.Value = long.Parse(m_monitorIdField.Text);
                        parameters.Add(param);
                    }
                    catch (FormatException)
                    {
                        m_monitorIdError.Style["visibility"] = "visible";
                    }
                }

                //Loan ID
                if (m_loanIdField.Text.Length > 0)
                {
                    try
                    {
                        SqlParameter param = new SqlParameter("@loanid", SqlDbType.UniqueIdentifier);
                        param.Value = new Guid(m_loanIdField.Text);
                        parameters.Add(param);
                    }
                    catch (FormatException)
                    {
                        m_loanIdError.Style["visibility"] = "visible";
                    }

                }

                //BP Group
                if (m_pricingGroupField.Text.Length > 0)
                {
                    SqlParameter param = new SqlParameter("@bpgroup", SqlDbType.VarChar);
                    param.Value = m_pricingGroupField.Text;
                    parameters.Add(param);
                }


                //Created Before D
                if (m_createdBeforeEnabled.Checked)
                {
                    try
                    {
                        SqlParameter param = new SqlParameter("@createdbefore", SqlDbType.DateTime);
                        param.Value = DateTime.Parse(m_createdBefore.Text);
                        parameters.Add(param);
                    }
                    catch (FormatException)
                    {
                        m_BeforeDateError.Style["visibility"] = "visible";
                    }
                }

                //Created After D
                if (m_createdAfterEnabled.Checked)
                {
                    try
                    {
                        SqlParameter param = new SqlParameter("@createdafter", SqlDbType.DateTime);
                        param.Value = DateTime.Parse(m_createdAfter.Text);
                        parameters.Add(param);
                    }
                    catch (FormatException)
                    {
                        m_AfterDateError.Style["visibility"] = "visible";
                    }
                }

                //Monitor Status
                if (m_monitorStatusField.SelectedItem.Value != "All")
                {
                    SqlParameter param = new SqlParameter("@isenabled", SqlDbType.Bit);
                    param.Value = ("Enabled".Equals(m_monitorStatusField.SelectedItem.Value) ? 1 : 0);
                    parameters.Add(param);
                }

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "FindRateMonitors", parameters))
                {
                    while (reader.Read())
                    {
                        _Item item = new _Item();
                        item.Id = (long)reader["Id"];
                        item.sLId = (Guid)reader["sLId"];
                        item.Rate = (decimal)reader["Rate"];
                        item.Point = (decimal)reader["Point"];
                        item.EmailAddresses = (string)reader["EmailAddresses"];
                        item.CcAddresses = (string)reader["CcAddresses"];
                        item.BPDesc = (string)reader["BPDesc"];
                        item.CreatedDate = (DateTime)reader["CreatedDate"];
                        item.DisabledDate = reader.SafeDateTimeNullable("DisabledDate");
                        item.DisabledReason = (string)reader["DisabledReason"];
                        item.IsEnabled = (bool)reader["IsEnabled"];
                        item.LastRunDate = reader.SafeDateTimeNullable("LastRunDate");
                        item.LastRunBatchId = reader.GetNullableLong("LastRunBatchId");

                        list.Add(item);
                    }
                }
            }

            m_Grid.DataSource = list;
            m_Grid.DataBind();

            if (m_Grid.Items.Count > 0)
            {
                m_pageNumberInfo.Text = "Displaying 1-" + m_Grid.Items.Count;
            }
            else
            {
                m_pageNumberInfo.Text = "No rate monitors found.";
            }
        }

        protected void DeleteSelected(object sender, System.EventArgs a)
        {
            List<long> toDelete = new List<long>();
            foreach (DataGridItem item in m_Grid.Items)
            {
                CheckBox isSelected = item.FindControl("Selected") as CheckBox;
                Label idKey = item.FindControl("Key") as Label;
                Label sLIdLabel = item.FindControl("sLId") as Label;
                if (isSelected == null || idKey == null)
                {
                    continue;
                }

                if (isSelected.Checked == true)
                {
                    long monitorId = long.Parse(idKey.Text);
                    Guid sLId = new Guid(sLIdLabel.Text);

                    SqlParameter[] parameters = {
                                                    new SqlParameter("@monitorid", monitorId)
                                                };

                    Guid brokerId = Guid.Empty;
                    DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);

                    StoredProcedureHelper.ExecuteNonQuery(connInfo, "RateMonitorDelete", 3, parameters);

                }
            }

            PageLoadBind();
        }

        protected void ShowAllMonitors(object sender, System.EventArgs a)
        {
            //Clear all fields, then rebind
            m_monitorIdField.Text = "";
            m_loanIdField.Text = "";
            m_pricingGroupField.Text = "";
            m_monitorStatusField.SelectedIndex = 0;
            m_createdAfterEnabled.Checked = false;
            m_createdBeforeEnabled.Checked = false;
            PageLoadBind();
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}
}
