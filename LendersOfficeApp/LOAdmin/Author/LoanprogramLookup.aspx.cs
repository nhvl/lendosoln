using System;
using DataAccess;
using LendersOffice.Admin;
using System.Web.Services;

namespace LendersOfficeApp.LOAdmin.Author
{
	public partial class LoanprogramLookup : LendersOffice.Admin.SecuredAdminPage
	{
        protected void PageLoad(object sender, System.EventArgs e)
        {
            RegisterResources();
        }

        void RegisterResources()
        {
            RegisterJsScript("ractive-0.7.1.min.js");
            RegisterJsScript("LOAdmin.Author.LoanProgramLookUp.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        [WebMethod]
        public static object Lookup(string productId)
        {
            string corporate = string.Empty;
            string fullPath = string.Empty;
            string message = string.Empty;

            Guid gId = Guid.Empty;
            try {
                gId = new Guid(productId);

            } catch (FormatException e) {
                message = e.Message;
            }

            if (gId != Guid.Empty) {
                CLoanProductData prod = new CLoanProductData(gId);
                prod.InitLoad();
                BrokerDB broker = BrokerDB.RetrieveById(prod.BrokerId);

                corporate = broker.Name;
                fullPath = prod.FullName;
                message = "OK";
            }

            return new { corporate, fullPath, message };
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}

