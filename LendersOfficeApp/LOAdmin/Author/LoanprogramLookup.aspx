<%@ Page language="C#" Codebehind="LoanprogramLookup.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Author.LoanprogramLookup"
         MasterPageFile="~/LOAdmin/loadmin.Master" Title="Look Up Loan Program Info - LendingQB" %>

<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <div class="row"><div class="col-sm-6 col-sm-offset-3"><div class="panel panel-default">
        <div class="panel-body">
            <div id="container" class="hidden">
                <div class="form-group" on-submit="submitChange">
                    <form class="input-group input-group-sm">
                        <input type="text" class="form-control" placeholder="Program ID" required
                               value="{{ productId }}"
                               title="Please fill out the Program ID"
                               data-db="loLpe.LOAN_PROGRAM_TEMPLATEs.LLpTemplateId">
                        <span class="input-group-btn">
                            <span class="clipper">
                                <button type="submit" class="btn btn-primary btn-sm"
                                    disabled="{{isSubmitting}}">Look Up</button>
                            </span>
                        </span>
                    </form>
                </div>

                <div class="form-group">
                    <label class="control-label">Corporate:</label>
                    <input type="text" class="form-control input-sm" readonly value="{{ corporate }}">
                </div>

                <div class="form-group">
                    <label class="control-label">Full Path:</label>
                    <input type="text" class="form-control input-sm" readonly value="{{ fullPath }}">
                </div>

                <div class="form-group">
                    <label class="control-label">Message:</label>
                    <textarea class="form-control input-sm resizable-vertical"
                        rows="15" readonly value="{{ message }}"></textarea>
                </div>
            </div>
        </div>
    </div></div></div>

</asp:Content>
