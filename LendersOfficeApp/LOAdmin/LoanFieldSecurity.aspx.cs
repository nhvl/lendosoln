﻿namespace LendersOfficeApp.LOAdmin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Web;
    using System.Web.Services;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    public partial class LoanFieldSecurity : LendersOffice.Admin.SecuredAdminPage
    {
        //Rebuild by Long Nguyen.
        //Email: longn@meridianlink.net
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        void RegisterResources()
        {
            RegisterJsScript("LOAdmin.LoanFieldSecurity.js");
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            bool devBypass = false;
            
            if (null != HttpContext.Current && HttpContext.Current.Request.Url.Host.StartsWith("dev.lendersoffice.com"))
            {
                devBypass = true;
            }

            if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
            {
                if (!devBypass)
                {
                    Response.Clear();
                    Response.Output.Write("This page is not available in production.");
                    Response.Flush();
                    Response.End();
                }
                
            }
            //ShowError("");????

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterResources();
        }
        public class LoanFieldSecurityModel
        {
            public string sFieldId { get; set; }
            public string sDescription { get; set; }
            public string LastEditedD { get; set; }
            public long LastEditedDSort { get; set; }
            public bool bLockDeskPerm { get; set; }
            public bool bCloserPerm { get; set; }
            public bool bAccountantPerm { get; set; }
            public bool bInvestorInfoPerm { get; set; }
            public LoanFieldSecurityModel() { }
            public LoanFieldSecurityModel(DbDataReader reader)
            {
                sFieldId = reader["sFieldId"].ToString();
                sDescription = reader["sDescription"].ToString();
                if (reader["LastEditedD"] == DBNull.Value)
                {
                    LastEditedDSort = 0;
                    LastEditedD = reader["LastEditedD"].ToString();
                }
                else
                {
                    LastEditedDSort = DateTime.Parse(reader["LastEditedD"].ToString()).Ticks;
                    LastEditedD = reader["LastEditedD"].ToString().Split(' ')[0].ToString();//remove time. take date only
                }

                if (reader["bLockDeskPerm"] == DBNull.Value) bLockDeskPerm = false;
                else bLockDeskPerm = Boolean.Parse(reader["bLockDeskPerm"].ToString());

                if (reader["bCloserPerm"] == DBNull.Value) bCloserPerm = false;
                else bCloserPerm = Boolean.Parse(reader["bCloserPerm"].ToString());

                if (reader["bAccountantPerm"] == DBNull.Value) bAccountantPerm = false;
                else bAccountantPerm = Boolean.Parse(reader["bAccountantPerm"].ToString());

                if (reader["bInvestorInfoPerm"] == DBNull.Value) bInvestorInfoPerm = false;
                else bInvestorInfoPerm = Boolean.Parse(reader["bInvestorInfoPerm"].ToString());
            }
            

        }
        [WebMethod]
        public static IEnumerable<LoanFieldSecurityModel> Get()
        {
            var rs = new List<LoanFieldSecurityModel>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetPerFieldSecurityEntries"))
            {
                while (reader.Read())
                {
                    rs.Add(new LoanFieldSecurityModel(reader));
                }
            }
            return rs;
        }
        [WebMethod]
        public static LoanFieldSecurityModel AddNew(LoanFieldSecurityModel lfs)
        {

            //var lfs = new LoanFieldSecurityModel();
            //Validate
            if (lfs == null) return new LoanFieldSecurityModel() { sFieldId = "Invalid Data" };
            if (string.IsNullOrEmpty(lfs.sFieldId) || lfs.sFieldId.TrimWhitespaceAndBOM() == "")
            {
                return new LoanFieldSecurityModel() { sFieldId = "Field ID is invalid" };
            }
            //Check if field Id exists
            var list = Get();
            foreach (var loan in list)
            {
                if (loan.sFieldId == lfs.sFieldId)
                    return new LoanFieldSecurityModel() { sFieldId = "Field ID \"" + lfs.sFieldId + "\" already exists" };
            }
            try
            {
                LoanFieldSecurityManager.AddSecureLoanField(lfs.sFieldId.TrimWhitespaceAndBOM(), lfs.sDescription, lfs.bLockDeskPerm, lfs.bCloserPerm, lfs.bAccountantPerm, lfs.bInvestorInfoPerm);
            }
            catch (SqlException e)
            {
                return new LoanFieldSecurityModel() { sFieldId = e.ToString() };
            }
            catch
            {
                throw;
            }
            return null;
        }
        [WebMethod]
        public static LoanFieldSecurityModel Delete(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId) || fieldId.TrimWhitespaceAndBOM() == "")
            {
                return new LoanFieldSecurityModel() { sFieldId = "Field ID is invalid" };
            }
            try
            {
                LoanFieldSecurityManager.RemoveSecureLoanField(fieldId);
            }
            catch (SqlException e)
            {
                return new LoanFieldSecurityModel() { sFieldId = e.ToString() };
            }
            catch
            {
                throw;
            }
            return null;
        }
        [WebMethod]
        public static LoanFieldSecurityModel ParseMultiple(string text)
        {
            if (string.IsNullOrEmpty(text) || text.TrimWhitespaceAndBOM() == "")
            {
                return new LoanFieldSecurityModel() { sFieldId = "Value is invalid" };
            }
            char separator = DetectSeparator(text);

            string[] entries = text.Split('\n');

            var list = Get();
            for (var i = 0; i < entries.Length; i++)
            {
                string cleanString = entries[i].Replace("\r\n", "");
                string[] fields = cleanString.Split(separator);
                if (fields.Length < 1)
                {
                    return new LoanFieldSecurityModel() { sFieldId = "Error found when parsing entry: " + cleanString };
                }
                else
                {
                    var fieldId = clean(fields, 0);
                    //Check field Id in all entries
                    for (var j = 0; j < i; j++)
                    {
                        string cleanString1 = entries[j].Replace("\r\n", "");
                        string[] fields1 = cleanString.Split(separator);
                        if (fields1.Length >= 1)
                        {
                            if (fieldId == clean(fields1, 0))
                                return new LoanFieldSecurityModel() { sFieldId = "Field ID \"" + fieldId + "\" already exists" };
                        }
                    }

                    //check field Id in current database
                    foreach (var loan in list)
                    {
                        if (loan.sFieldId == fieldId)
                            return new LoanFieldSecurityModel() { sFieldId = "Field ID \"" + fieldId + "\" already exists" };
                    }
                    try
                    {
                        LoanFieldSecurityManager.AddSecureLoanField(fieldId, clean(fields, 1), tobool(fields, 2), tobool(fields, 3), tobool(fields, 4), tobool(fields, 5));
                    }
                    catch (SqlException e)
                    {
                        return new LoanFieldSecurityModel() { sFieldId = e.ToString() };
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            return null;
        }
        private static char DetectSeparator(string text)
        {
            int c1 = text.Length - text.Replace(",", "").Length;
            int c2 = text.Length - text.Replace("\t", "").Length;
            return (c1 > c2) ? ',' : '\t';
        }
        private static string clean(string[] fields, int index)
        {
            if (fields == null)
                return "";
            if (index < 0 || index >= fields.Length)
                return "";

            string field = "";
            try
            {
                field = fields[index];
            }
            catch
            {
                return "";
            }

            if (string.IsNullOrEmpty(field))
                return "";

            return field.TrimWhitespaceAndBOM();
        }

        private static bool tobool(string[] fields, int index)
        {
            if (fields == null)
                return false;
            if (index < 0 || index >= fields.Length)
                return false;

            string field = "";
            try
            {
                field = fields[index];
            }
            catch
            {
                return false;
            }

            if (string.IsNullOrEmpty(field))
                return false;

            field = field.TrimWhitespaceAndBOM();

            if (field == "x" || field == "X" || field == "1")
                return true;
            return false;
        }
        private bool removeFieldEntry(string field)
        {
            bool error = false;

            try
            {
                LoanFieldSecurityManager.RemoveSecureLoanField(field);
            }
            catch (Exception exc)
            {
                error = true;
                ShowError(exc.Message);
            }
            return error;
        }
        private bool AddEntry(string field, string desc, bool ldesck, bool closer, bool acc, bool invinfo)
        {
            bool error = false;

            try
            {
                LoanFieldSecurityManager.AddSecureLoanField(field, desc, ldesck, closer, acc, invinfo);
            }
            catch (Exception exc)
            {
                error = true;
                string f = (field != null) ? field : "";
                ShowError("Error adding entry " + f + ". Exception: " + exc.Message);
            }
            return error;
        }
        protected void ClearCache_OnClick(object sender, EventArgs e)
        {
            string clearCacheKey = "clearCacheKey";
            DataTable table = LoanFieldSecurityManager.GetSecureLoanFieldsDataTable();
            Hashtable list = new Hashtable();

            foreach (DataRow row in table.Rows)
            {
                string key = row["sFieldId"].ToString();
                list.Add(key, true);
            }

            if (list.Contains(clearCacheKey))
            {
                removeFieldEntry(clearCacheKey);
            }
            else
            {
                AddEntry(clearCacheKey, "TESTING", true, true, true, true);
                removeFieldEntry(clearCacheKey);
            }
        }
        protected void ExportCsv_OnClick(object sender, EventArgs e)
        {
            DataTable table = LoanFieldSecurityManager.GetSecureLoanFieldsDataTable();
            string output = "#Exported on " + System.DateTime.Now.ToString() + System.Environment.NewLine;
            foreach (DataRow row in table.Rows)
            {
                output += string.Format("{0},{1},{2},{3},{4},{5}{6}", row["sFieldId"], row["sDescription"], ToBool(row["bLockDeskPerm"]), ToBool(row["bCloserPerm"]), ToBool(row["bAccountantPerm"]), ToBool(row["bInvestorInfoPerm"]), System.Environment.NewLine);
            }

            Response.ContentType = "text/plain";
            Response.AddHeader("Content-Disposition", "attachment; filename=\"Report.txt\"");
            Response.Output.Write(output);
            Response.Flush();
            Response.End();

        }
        private string ToBool(object obj)
        {
            return ((bool)obj) ? "Y" : "N";
        }
        private void ShowError(string msg)
        {
            if (msg == null)
                return;
            if (msg.Length == 0)
            {
                ErrMsg.InnerText = string.Empty;
                ErrorPanel.Visible = false;
            }

            else
            {
                ErrMsg.InnerHtml = "- " + AspxTools.HtmlString(msg) + "<br/>";
                ErrorPanel.Visible = true;
            }
        }
    }
}
