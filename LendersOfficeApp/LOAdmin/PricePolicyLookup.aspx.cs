using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using DataAccess;
using LendersOfficeApp.los.RatePrice;

namespace LendersOfficeApp.LOAdmin
{
    public class PricePolicyRow
	{
		private string m_PricePolicyID;
		private string m_ParentPolicyPath;

		public PricePolicyRow(string sPricePolicyID, string sParentPolicyPath)
		{
			m_PricePolicyID = sPricePolicyID;
			m_ParentPolicyPath = sParentPolicyPath;
		}

		public string PricePolicyID
		{
			get { return m_PricePolicyID; }
		}

		public string ParentPolicyPath
		{
			get { return m_ParentPolicyPath; }
		}
	}

    /// <summary>
    /// Summary description for PricePolicyLookup.
    /// </summary>
    public partial class PricePolicyLookup : LendersOffice.Admin.SecuredAdminPage
    {
        protected ArrayList m_List = new ArrayList();

        protected void PageLoad(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            RegisterJsScript("angular-1.4.8.min.js");
        }

        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        private static PricePolicyRow[] BindDataGrid(string lookupInput)
        {
            if (string.IsNullOrEmpty(lookupInput))
            {
                return null;
            }

            CLpeReleaseInfo latestRelease = CLpeReleaseInfo.GetLatestReleaseInfo();
            var pricePolicyList = new System.Collections.Generic.List<PricePolicyRow>();

            RetrievePricePolicyList(lookupInput, latestRelease, pricePolicyList, true);
            return pricePolicyList.ToArray();
        }

        public static void RetrievePricePolicyList(string lookupInput, CLpeReleaseInfo latestRelease, System.Collections.Generic.List<PricePolicyRow> pricePolicyList, bool enableLogging)
        {
            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@like", lookupInput) };
            string sSQL = "Select PricePolicyId, PricePolicyDescription, ParentPolicyId From PRICE_POLICY Where PricePolicyXmlText Like '%' + @like + '%'";

            using (var sqlConnection = DbAccessUtils.GetConnection(latestRelease.DataSrcForSnapshot))
            {
                Action<IDataReader> outerHandler = delegate (IDataReader reader)
                {
                    int nRow = 0;
                    while (reader.Read())
                    {
                        string sPricePolicyId = reader["PricePolicyId"].ToString();
                        string sParentPolicyPath = reader["PricePolicyDescription"].ToString();
                        string sParentPolicyId = reader["ParentPolicyId"].ToString();
                        nRow++;

                        while (sParentPolicyId != "")
                        {
                            listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@id", sParentPolicyId) };
                            sSQL = "Select PricePolicyDescription, ParentPolicyId From PRICE_POLICY Where PricePolicyId=@id";
                            using (var sqlConnectionInner = DbAccessUtils.GetConnection(latestRelease.DataSrcForSnapshot))
                            {
                                Action<IDataReader> innerHandler = delegate(IDataReader readerInner)
                                {
                                    if (readerInner.Read())
                                    {
                                        sParentPolicyPath = string.Format("{0}\\{1}", readerInner["PricePolicyDescription"].ToString(), sParentPolicyPath);
                                        sParentPolicyId = readerInner["ParentPolicyId"].ToString();
                                    }
                                    else
                                    {
                                        if (enableLogging)
                                        {
                                            Tools.LogErrorWithCriticalTracking(string.Format("Can't find Policy ID '{0}' in PRICE_POLICY!", sParentPolicyId));
                                        }

                                        sParentPolicyId = "";
                                    }
                                };

                                DBSelectUtility.ProcessDBData(sqlConnectionInner, null, sSQL, null, listParams, innerHandler);
                            }
                        }

                        PricePolicyRow row = new PricePolicyRow(sPricePolicyId, sParentPolicyPath);
                        pricePolicyList.Add(row);
                    }
                };

                DBSelectUtility.ProcessDBData(sqlConnection, null, sSQL, null, listParams, outerHandler);
            }
        }

        #region Web Method
        [System.Web.Services.WebMethod]
        /// <summary>Lookup price policy based on text input</summary>
        /// <param name="lookupString">lookup text in Keyword or parameter</param>
        /// <returns>price policy array as json string</returns>
        public static PricePolicyRow[] LookupPricePolicy(string lookupString)
        {
            // 2016-11-11 - dd - Eventually I will retire this page because the text of price policy will no longer store in PricePolicyXmlText.
            //   I am going start record what type of string SAE use to search in price policy and start looking at alternative.
            Tools.LogInfo("PricePolicyLookup - Word [" + lookupString + "]");
            return BindDataGrid(lookupString.Replace("<", "&lt;").Replace(">", "&gt;"));
        }

        #endregion
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
