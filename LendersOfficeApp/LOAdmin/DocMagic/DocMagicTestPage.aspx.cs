﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Conversions;
using LendersOffice.Security;
using System.Threading;
using LendersOffice.Common;
using System.Security.Principal;
using DsiDocRequest = DocMagic.DsiDocRequest;
using DsiDocResponse = DocMagic.DsiDocResponse;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;
using DocMagic.DsiDocResponse;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using DocMagic.DsiDocRequest;

namespace LendersOfficeApp.LOAdmin.DocMagic
{
    /// <summary>
    /// 
    /// Authentication failure gives this back:
    /// <Message type="Fatal" category="Server">Authentication failed</Message>
    /// <Message type="Fatal" category="Server">Login failure</Message>
    /// <Message type="Fatal" category="Server">Please verify the customer id, user name, and password</Message>
    /// 
    /// </summary>
    public partial class DocMagicTestPage : LendersOffice.Admin.SecuredAdminPage
    {
        protected class DocMagicMessage
        {
            public string Category {get; set;}
            public string Type {get; set;}
            public string MismoRef {get; set;}
            public string InnerText {get; set;}
            public string ExtId { get; set; }
        }

        protected class DocMagicTestPageHelper
        {
            public string CustomerId { get; set; }
            public string UserId { get; set; }
            public string Password { get; set; }

            public Guid LOUserId { get; set; }
            public Guid LOLoanId { get; set; }
        }

        DocMagicTestPageHelper PageInfo;

        //Rebuild by Long Nguyen
        //Email: longn@meridianlink.net
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        private void ShowError(Exception ex)
        {
            ErrMsg.InnerText = parseException(ex);
            ErrPanel.Visible = true;
        }
        private void ShowError(string message)
        {
            ErrMsg.InnerText = message;
            ErrPanel.Visible = true;
        }
        string parseException(Exception e)
        {
            var message = "";
            if (String.IsNullOrEmpty(e.Message))
                message = e.ToString();
            else
                message = e.Message;
            if (!message.Contains("UserMessage:") && !message.Contains("DeveloperMessage:")) return message;
            var msg = message.Split(new string[] { "UserMessage:", "DeveloperMessage:" }, StringSplitOptions.RemoveEmptyEntries);
            return msg[1].Trim();
        }
        protected void PageInit(object sender, EventArgs e)
        {
            PageInfo = new DocMagicTestPageHelper();
            PageInfo.CustomerId = "231856";
            PageInfo.UserId = "david@insightlendingsolutions.com";
            PageInfo.Password = "dredge"; // shouldn't this be encrypted at least?
            
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ErrPanel.Visible = false;
            }
        }

        /// <summary>
        /// Helper function that gets the loan id and user id, then logs in at that user.
        /// </summary>
        /// <returns></returns>
        private bool InitRequest() // bad naming, can't think of a better one
        {
            string userIdLO = UserIdLO.Text.TrimWhitespaceAndBOM();
            string loanId = AuditLoanID.Text.TrimWhitespaceAndBOM();

            try
            {
                PageInfo.LOUserId = new Guid(userIdLO);
                PageInfo.LOLoanId = new Guid(loanId);
            }
            catch (FormatException)
            {
                ShowError("Please enter a GUID.");
                return false;
            }

            // Don't copy this BecomeUser stuff
            var principal = Thread.CurrentPrincipal as InternalUserPrincipal;

            if (false == principal.SetBecomeUser(PageInfo.LOUserId))
            {
                ShowError("Could not create a broker user principal from the provided user GUID.");
                return false;
            }
            return true;
        }
        
        protected void SendAudit_OnClick(object sender, EventArgs e)
        {
            if (false == this.InitRequest())
                return;

            List<string> feeDiscrepancies;
            var request = DocMagicMismoRequest.CreateAuditRequest(
                PageInfo.CustomerId,
                PageInfo.UserId,
                PageInfo.Password,
                PageInfo.LOLoanId,
                "NEW", // websheet request number sDocMagicFileId
                DsiDocRequest.E_AuditPackageType.Closing,
                out feeDiscrepancies);

            // If you want to look at the xml that was sent and received,
            //   stick some LogInfos somewhere in here
            var response = DocMagicServer2.Submit(request);

            // DocMagic will give you a Failure if the loan fails the audit
            //if (response.Status == DsiDocResponse.E_DsiDocumentServerResponseStatus.Failure)
            //{
            //
            //}

            // AuditHTML

            m_gv.DataSource = from msg in response.MessageList
                              select new DocMagicMessage()
                              {
                                  Category = msg.Category.ToString(),
                                  Type = msg.Type.ToString(),
                                  MismoRef = msg.MismoRef,
                                  InnerText = msg.InnerText,
                                  ExtId = msg.MessageExtId
                              };
                
            m_gv.DataBind();
            if (m_gv.Rows.Count != 0)
            {
                m_gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            m_extgv.DataSource = from msg in response.MessageExtList
                                 select new
                                 {
                                     Id = msg.Id,
                                     SubClass =
                                        // I've never seen more than one SubClass included in a messageExt, 
                                        // so I just pretend the list is only one element here
                                        msg.AuditInfo.AuditSubClassList != null ?
                                            string.Join(",", msg.AuditInfo.AuditSubClassList.ToArray())
                                            : "",
                                     Url = msg.AuditInfo.Url,
                                     Class = msg.AuditInfo.Class
                                 };

            m_extgv.DataBind();
            if (m_extgv.Rows.Count != 0)
            {
                m_extgv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void SendPackageTypeListRequest_OnClick(object sender, EventArgs e)
        {
            // Don't need to export a loan here, so no need for user/loan data

            var request = DocMagicMismoRequest.CreatePackageTypeListRequest(PageInfo.CustomerId, PageInfo.UserId, PageInfo.Password);

            // If you want to look at the xml that was sent and received,
            //   stick some LogInfos somewhere in here
            var response = DocMagicServer2.Submit(request);

            if (response.Status == DsiDocResponse.E_DsiDocumentServerResponseStatus.Failure)
            {
                // Hmm, what should I do here? fail silently??? okay!
                return;
            }

            m_gv.DataSource = from msg in response.InitResponse.PackageTypeList
                              select new
                              {
                                  TypeName = msg.Type.ToString(),
                                  AllowEPortalService = msg.allowEPortalService.ToString(),
                                  Description = msg.Description
                              };

            // There will also be messages in the messageList
            // response.MessageList


            m_gv.DataBind();
            if (m_gv.Rows.Count != 0)
            {
                m_gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void SendLoadCurrentPlansRequest_OnClick(object sender, EventArgs e)
        {
            if (false == this.InitRequest())
                return;

            var request = DocMagicMismoRequest.CreateLoadCurrentPlansRequest(
                PageInfo.CustomerId,
                PageInfo.UserId,
                PageInfo.Password);

            // If you want to look at the xml that was sent and received,
            //   stick some LogInfos somewhere in here
            var response = DocMagicServer2.Submit(request);
        }

        protected void SendCreatePlanCodeRequest_OnClick(object sender, EventArgs e)
        {
            if (false == this.InitRequest())
                return;

            var request = DocMagicMismoRequest.CreateAddPlanToLenderRequest(
                PageInfo.CustomerId,
                PageInfo.UserId,
                PageInfo.Password,
                PlanCode.Text);

            // If you want to look at the xml that was sent and received,
            //   stick some LogInfos somewhere in here
            var response = DocMagicServer2.Submit(request);

            m_gv.DataSource = from msg in response.MessageList
                              select new DocMagicMessage()
                              {
                                  Category = msg.Category.ToString(),
                                  Type = msg.Type.ToString(),
                                  MismoRef = msg.MismoRef,
                                  InnerText = msg.InnerText,
                                  ExtId = msg.MessageExtId
                              };

            m_gv.DataBind();
            if (m_gv.Rows.Count != 0)
            {
                m_gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        /// <summary>
        /// Send a form request for a list of eligible documents. The user would then select the desired
        /// document(s) from the list, which will then be included in a Process request.
        /// </summary>
        protected void SendFormRequest_OnClick(object sender, EventArgs e)
        {
            if (false == this.InitRequest())
                return;

            var request = DocMagicMismoRequest.CreateFormListRequest(
                PageInfo.CustomerId,
                PageInfo.UserId,
                PageInfo.Password,
                "665" // websheet request number, this is kept in the loan as sDocMagicFileId. We CANNOT use "NEW" here.
                );

            // If you want to look at the xml that was sent and received,
            //   stick some LogInfos somewhere in here
            var response = DocMagicServer2.Submit(request);

            m_gv.DataSource = from msg in response.MessageList
                              select new DocMagicMessage()
                              {
                                  Category = msg.Category.ToString(),
                                  Type = msg.Type.ToString(),
                                  MismoRef = msg.MismoRef,
                                  InnerText = msg.InnerText,
                                  ExtId = msg.MessageExtId
                              };
            m_gv.DataBind();
            if (m_gv.Rows.Count != 0)
            {
                m_gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            if (response.FormResponseList.Count > 0)
            {
                var formResponse = response.FormResponseList.First();
                m_extgv.DataSource = from format in formResponse.FormatList
                                     select new
                                     {
                                         BorrowerNumber = format.Form.BorrowerNumber,
                                         Description = format.Description
                                     };
                m_extgv.DataBind();
                if (m_extgv.Rows.Count != 0)
                {
                    m_extgv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
        }

        protected void SendProcessRequest_OnClick(object sender, EventArgs e)
        {
            if (false == this.InitRequest())
                return;

            var forms = new List<DsiDocRequest.Form>();
            forms.Add(new DsiDocRequest.Form() { BorrowerNumber = "1", InnerText = "1003.LGL" });

            DsiDocRequest.Process processOptions = DocMagicMismoRequest.CreateProcessOptions(
                DsiDocRequest.E_ProcessPackageType.FormList /* packageType */,
                forms /* forms */,
                false /* sendEDisclosures */,
                false /* allowOnlineSigning */,
                true /* emailDocuments */,
                    "matthewp@meridianlink.com" /* emailAddress */,
                    true /* emailRequirePassword */,
                    "Other" /* emailPasswordHint */,
                    "12345" /* emailPassword */,
                    true /* emailNotifyOnRetrieval */,
                false /* printAndDeliver */,
                    "attention",
                    "name",
                    "street",
                    "city",
                    "state",
                    "zip",
                    "phone",
                    "notesForDSI",
                false /* mersRegistration */,
                "matthewp@meridianlink.com", global::DocMagic.DsiDocRequest.E_DocumentFormatType.PDF);
            // Yeah, it's a ludicrous number of arguments, whatcha gonna do about it

            var request = DocMagicMismoRequest.CreateProcessRequest(
                PageInfo.CustomerId,
                PageInfo.UserId,
                PageInfo.Password,
                PageInfo.LOLoanId,
                "NEW", // websheet request number, this is kept in the loan as sDocMagicFileId
                processOptions);

            // If you want to look at the xml that was sent and received,
            //   stick some LogInfos somewhere in here
            var response = DocMagicServer2.Submit(request);

            if (response.Status == DsiDocResponse.E_DsiDocumentServerResponseStatus.Failure)
            {
                // Fatal error encountered
                // Audit failed?
                // Process failed?
                m_gv.DataSource = from msg in response.MessageList
                                  select new DocMagicMessage()
                                  {
                                      Category = msg.Category.ToString(),
                                      Type = msg.Type.ToString(),
                                      MismoRef = msg.MismoRef,
                                      InnerText = msg.InnerText,
                                      ExtId = msg.MessageExtId
                                  };

                m_gv.DataBind();
                if (m_gv.Rows.Count != 0)
                {
                    m_gv.HeaderRow.TableSection = TableRowSection.TableHeader;
                }

                return;
            }

            bool YOU_WANT_TO_SEE_THIS = false;
            if (YOU_WANT_TO_SEE_THIS)
                foreach (var websheetResponse in response.WebsheetResponseList) // there should usually only be one
                {
                    Tools.LogInfo("websheetResponse.Audit: " + websheetResponse.Audit.ToString());
                    {
                        Tools.LogInfo("websheetResponse.Audit.ProofSheetHtml: " + websheetResponse.Audit.ProofSheetHtml.ToString()); // HTML!
                        Tools.LogInfo("websheetResponse.Audit.AuditHtml: " + websheetResponse.Audit.AuditHtml.ToString());
                        Tools.LogInfo("websheetResponse.Audit.AprPaymentCalculation: " + websheetResponse.Audit.AprPaymentCalculation.ToString());
                        Tools.LogInfo("websheetResponse.Audit.Section32Calculation: " + websheetResponse.Audit.Section32Calculation.ToString());
                        Tools.LogInfo("websheetResponse.Audit.ImpoundAnalysis: " + websheetResponse.Audit.ImpoundAnalysis.ToString());
                        Tools.LogInfo("websheetResponse.Audit.GFEComparisonHtml: " + websheetResponse.Audit.GfeComparisonHtml.ToString());
                    }
                    Tools.LogInfo("websheetResponse.Process: " + websheetResponse.Process.ToString());
                    {
                        Tools.LogInfo("websheetResponse.Process.DocCode: " + websheetResponse.Process.DocCode.ToString());
                        Tools.LogInfo("websheetResponse.Process.DocHTML: " + websheetResponse.Process.DocHtml.ToString());
                        Tools.LogInfo("websheetResponse.Process.Blockument: " + websheetResponse.Process.Blockument.ToString()); // THE FILES ARE IN HERE
                        Tools.LogInfo("websheetResponse.Process.DocumentFormat: " + websheetResponse.Process.DocumentFormat.ToString());
                        Tools.LogInfo("websheetResponse.Process.WorksheetId: " + websheetResponse.Process.WorksheetId.ToString());
                        Tools.LogInfo("websheetResponse.Process.DocSet: " + websheetResponse.Process.DocSet.ToString());
                        {
                            Tools.LogInfo("websheetResponse.Process.DocSet.Type: " + websheetResponse.Process.DocSet.Type.ToString());
                            Tools.LogInfo("websheetResponse.Process.DocSet.CompressionType: " + websheetResponse.Process.DocSet.CompressionType.ToString());
                            Tools.LogInfo("websheetResponse.Process.DocSet.BundleType: " + websheetResponse.Process.DocSet.BundleType.ToString());
                            Tools.LogInfo("websheetResponse.Process.DocSet.IncludeInResponse: " + websheetResponse.Process.DocSet.IncludeInResponse.ToString());
                            Tools.LogInfo("websheetResponse.Process.DocSet.DocRetrieval: " + websheetResponse.Process.DocSet.DocRetrieval.ToString());
                            {
                                Tools.LogInfo("websheetResponse.Process.DocSet.DocRetrieval.DocCode: " + websheetResponse.Process.DocSet.DocRetrieval.DocCode.ToString());
                                Tools.LogInfo("websheetResponse.Process.DocSet.DocRetrieval.ExpirationDate: " + websheetResponse.Process.DocSet.DocRetrieval.ExpirationDate.ToString());
                                Tools.LogInfo("websheetResponse.Process.DocSet.DocRetrieval.RequestDate: " + websheetResponse.Process.DocSet.DocRetrieval.RequestDate.ToString());
                                Tools.LogInfo("websheetResponse.Process.DocSet.DocRetrieval.RequestTime: " + websheetResponse.Process.DocSet.DocRetrieval.RequestTime.ToString());
                                Tools.LogInfo("websheetResponse.Process.DocSet.DocRetrieval.PackageType: " + websheetResponse.Process.DocSet.DocRetrieval.PackageType.ToString());
                            }
                            foreach (var document in websheetResponse.Process.DocSet.DocumentList)
                            {
                                Tools.LogInfo("document.Description: " + document.Description.ToString());
                                Tools.LogInfo("document.EmbeddedContent: " + document.EmbeddedContent.ToString());
                            }
                        }
                    }
                    foreach (var websheetInfo in websheetResponse.WebsheetInfoList)
                    {
                        Tools.LogInfo("websheetInfo.WebsheetNumber: " + websheetInfo.WebsheetNumber.ToString());
                        Tools.LogInfo("websheetInfo.BorrowerName: " + websheetInfo.BorrowerName.ToString());
                        Tools.LogInfo("websheetInfo.LoanNumber: " + websheetInfo.LoanNumber.ToString());
                        Tools.LogInfo("websheetInfo.LastSaved: " + websheetInfo.LastSaved.ToString());
                        {
                            Tools.LogInfo("websheetInfo.LastSaved.Time: " + websheetInfo.LastSaved.Time.ToString());
                            Tools.LogInfo("websheetInfo.LastSaved.Date: " + websheetInfo.LastSaved.Date.ToString());
                            Tools.LogInfo("websheetInfo.LastSaved.User: " + websheetInfo.LastSaved.User.ToString());
                        }
                        Tools.LogInfo("websheetInfo.DocCode: " + websheetInfo.DocCode.ToString());
                        Tools.LogInfo("websheetInfo.ProcessedPackageType: " + websheetInfo.ProcessedPackageType.ToString());
                    }
                }

            m_gv.DataSource = from msg in response.MessageList
                              select new DocMagicMessage()
                              {
                                  Category = msg.Category.ToString(),
                                  Type = msg.Type.ToString(),
                                  MismoRef = msg.MismoRef,
                                  InnerText = msg.InnerText,
                                  ExtId = msg.MessageExtId
                              };

            m_gv.DataBind();
            if (m_gv.Rows.Count != 0)
            {
                m_gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            m_extgv.DataSource = from msg in response.MessageExtList
                                 select new
                                 {
                                     Id = msg.Id,
                                     SubClass =
                                        msg.AuditInfo.AuditSubClassList != null ?
                                            string.Join(",", msg.AuditInfo.AuditSubClassList.ToArray())
                                            : "",
                                     Url = msg.AuditInfo.Url,
                                     Class = msg.AuditInfo.Class
                                 };

            m_extgv.DataBind();
            if (m_extgv.Rows.Count != 0)
            {
                m_extgv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void SendCustomRequest_OnClick(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(ManualXML.Text))
            {
                ShowError("Please enter custom XML Request. ");
                return;
            }
            const string DOCMAGIC_BETA_SERVER_URL = "https://stage-www.docmagic.com/webservices/dmdirect/xml";
            string url = DOCMAGIC_BETA_SERVER_URL;
            
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(ManualXML.Text);
            
            Guid transactionId = Guid.NewGuid();
            string bytesString = System.Text.Encoding.UTF8.GetString(bytes);
            bytesString = Regex.Replace(bytesString, "<Password>[^ ]+</Password>", "<Password>******</Password>");
            Tools.LogInfo("ID:" + transactionId.ToString() + " Url=" + url + Environment.NewLine + "Request----" + Environment.NewLine + bytesString);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                stream.Position = 0;
                DocMagicServer2.ValidateDSIDocumentServerResponse(stream);
            }

            // Send out the request
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
            webRequest.Method = "POST";

            webRequest.ContentType = "text/xml";
            webRequest.ContentLength = bytes.Length;

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            // Get a response back
            StringBuilder sb = new StringBuilder();
            
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                using (Stream stream = webResponse.GetResponseStream())
                {
                    byte[] buffer = new byte[60000];
                    int size = stream.Read(buffer, 0, buffer.Length);
                    while (size > 0)
                    {
                        sb.Append(System.Text.Encoding.UTF8.GetString(buffer, 0, size));
                        size = stream.Read(buffer, 0, buffer.Length);
                    }
                }
            }
            var responseBytes = Encoding.ASCII.GetBytes(sb.ToString());
            Tools.LogInfo("ID:" + transactionId.ToString() + " Response-----" + Environment.NewLine + responseBytes.ToString());
            

            Response.Clear();
            Response.ClearHeaders();
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition", "attachment; filename=\"Response.xml\"");
            Response.AddHeader("Content-Length", responseBytes.Length.ToString());
            Response.OutputStream.Write(responseBytes, 0, responseBytes.Length);
            Response.Flush();
            Response.End();


            //DsiDocumentServerResponse dsiDocumentServerResponse;
            //dsiDocumentServerResponse = new DsiDocumentServerResponse();

            //XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            //xmlReaderSettings.XmlResolver = null;
            //xmlReaderSettings.ProhibitDtd = false;
            //using (XmlReader reader = XmlReader.Create(new StringReader(sb.ToString()), xmlReaderSettings))
            //{
            //    dsiDocumentServerResponse.ReadXml(reader);
            //}
        }

        protected void Gridview_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            var message = e.Row.DataItem as DocMagicMessage;
            if (message == null)
                return;

            switch(message.Type.ToUpper())
            {
                case "WARNING":
                    e.Row.CssClass = "warning";
                    break;
                case "FATAL":
                    e.Row.CssClass = "danger";
                    break;
                default:
                    break;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
