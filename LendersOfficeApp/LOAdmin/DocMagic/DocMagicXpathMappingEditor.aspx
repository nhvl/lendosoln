﻿<%@ Page language="C#" Codebehind="DocMagicXpathMappingEditor.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.DocMagic.DocMagicXpathMappingEditor"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="DocMagic XPath Mapping Editor" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <div class="container-fluid"><div class="row"><div class="panel panel-default"><div class="panel-body">
         <div class="ng-cloak" ng-app="app" ng-controller="DocManager as doc">
            <div ng-hide="!isLoadding">loading&hellip;</div>
            <div ng-show="!isLoading">
                <form runat="server" class="form-group-sm">

                    <!--Upload Dialog-->
                     <div id="importModal" class="modal v-middle fade"><div class="modal-dialog modal-lg"><div class="modal-content">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            <h4 class="modal-title">
                                Import
                            </h4>
                        </div>
                            <div class="modal-body">
                                 <div class="form-group">
                                    <div class="pull-right text-danger">
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="CsvUpload" 
                                            Display="Dynamic" ErrorMessage="Please select a file"/>
                                    </div>
                                    <label>Import a CSV file:</label>
                                    <%--<input type="file" id="CsvUpload" accept="text/csv" autocomplete="off" runat="server" />--%>
                                     <asp:FileUpload ID="CsvUpload" runat="server" required/>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <span class="clipper"><a class="btn btn-default" data-dismiss="modal">Close</a></span>
                                <span class="clipper">
                                    <asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="UploadClick" CssClass="btn btn-primary"/>
                                </span>
                            </div>
                    </div></div></div>


                <div class="form-group>
                <span class="clipper">
                    <a class="btn btn-default" data-toggle="modal" data-target="#importModal">Import</a>
                </span>
                <span class="clipper">
                    <asp:Button Text="Export" ID="btnExport" runat="server" OnClick="ExportClick" CssClass="btn btn-default" CausesValidation="false" UseSubmitBehavior="false"/>
                </span>
                <a ng-click="doc.show($event)" ng-hide="doc.isShow">Show more fields </a>
                <a ng-click="doc.show($event)" ng-show="doc.isShow">Show less fields </a>
                </div>
                </form>
                <div>&nbsp;</div>
                <table class="table table-striped table-condensed table-layout-fix">
                    <thead><tr>
                        <th><a sort-handle="doc.entrySort" key="XPath">XPath</a></th>
                        <th><a sort-handle="doc.entrySort" key="DefaultURL">URL</a></th>
                        <th ng-show="doc.isShow"><a sort-handle="doc.entrySort" key="FieldId">Field ID</a></th>
                        <th ng-show="doc.isShow"><a sort-handle="doc.entrySort" key="URLDependsOnFieldId">Depend On Field ID</a></th>
                        <th ng-show="doc.isShow"><a sort-handle="doc.entrySort" key="AltURL">Alternate URL</a></th>
                        </tr></thead>
                    <tbody>
                        <tr ng-repeat="e in doc.Docs | orderBy:doc.entrySort.by:doc.entrySort.desc track by e.XPath">
                            <!--Spaces among td tag make error in IE9-->
                             <td ng-bind="e.XPath"></td><td ng-bind="e.DefaultURL"></td><td ng-bind="e.FieldId" ng-show="doc.isShow"></td><td ng-bind="e.URLDependsOnFieldId" ng-show="doc.isShow"></td><td ng-bind="e.AltURL" ng-show="doc.isShow"></td>
                        </tr>
                    </tbody>
                </table>
                    
    </div></div></div></div></div></div>
</asp:Content>