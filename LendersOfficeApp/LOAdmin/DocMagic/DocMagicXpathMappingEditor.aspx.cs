﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.ObjLib.DocMagicLib;
using System.IO;
using System.Data;
using LendersOffice.Common.TextImport;
using System.Web.Services;

namespace LendersOfficeApp.LOAdmin.DocMagic
{
    public partial class DocMagicXpathMappingEditor : SecuredAdminPage
    {
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        void RegisterResources()
        {
            RegisterJsScript("LOAdmin.DocMagic.DocMagicXpathMappingEditor.js");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterResources();
        }

        [WebMethod]
        public static IEnumerable<DocMagicXpathMapping> Get()
        {
            return DocMagicXpathMapping.RetrieveAll();
        }
        protected void UploadClick(object sender, EventArgs e)
        {
            if (!CsvUpload.HasFile) return;
            string tempFilename = Path.GetTempFileName() + Path.GetExtension(CsvUpload.PostedFile.FileName);
            CsvUpload.PostedFile.SaveAs(tempFilename);

            ILookup<int, string> parseErrors;

            //Validate that the file we got looks okay
            DataTable uploaded = DataParsing.FileToDataTable(tempFilename, true, "", out parseErrors);
            var columnErrors = DataParsing.ValidateColumns(uploaded, new[] { "XPath", "DefaultURL", "FieldId", "URLDependsOnFieldId", "AltURL" }, // all column names
                                                                    new[] { "XPath", "DefaultURL" }, //required column names
                                                                    1); //In the source file, the header row starts at line 1

            //If we couldn't parse it, don't try salvaging anything; let the user know about the errors.
            if (parseErrors.Any() || columnErrors.Any())
            {
                var errorLines = parseErrors.Select((a) => a.Key).Union(columnErrors.Select(a => a.Key)).OrderBy(a => a);
                foreach (var lineNum in errorLines)
                {
                    var errorMessages = parseErrors[lineNum].Union(columnErrors[lineNum]);
                    foreach (var message in errorMessages)
                    {
                        //do something here
                    }
                }
            }

            DocMagicXpathMapping.DeleteAll();

            //The column names in uploaded have been selected to map onto the column names that the 
            //DataRow constructor of DocMagicXpathMapping uses.
            DataTableToMappings(uploaded).ToList().SaveAll();

            Response.Redirect(Request.RawUrl);

        }
        protected void ExportClick(object sender, EventArgs e)
        {
            var data = DocMagicXpathMapping.RetrieveAll().ToList().ToDataTable(new[] { "XPath", "DefaultURL", "FieldId", "URLDependsOnFieldId", "AltURL" }).ToCSV(true);

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AppendHeader("Content-Disposition", "attachment; filename=\"DocMagicXPathExport.csv\"");
            Response.Write(data);
            Response.Flush();
            Response.End();
        }

        private IEnumerable<DocMagicXpathMapping> DataTableToMappings(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                yield return new DocMagicXpathMapping(dr);
            }
        }
    }
}
