﻿<%@ Page language="C#" Codebehind="DocMagicTestPage.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.DocMagic.DocMagicTestPage"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="DocMagic Test Page" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <style type="text/css">
    .center-div{
            margin: 0 auto;
            width: 70%;
        }
    .text-vertical{
            resize: vertical;
        }
    </style>
    <form id="form1" runat="server">
    <div class="container-fluid"><div class="row"><div class="center-div"><div class="panel panel-default"><div class="panel-body">
        <p>This will use the "david@insightlendingsolutions.com" DocMagic test account.
        PLEASE make sure the loan borrower's name is SAMPLE SAMPLE, otherwise we may get charged.</p>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>User ID:</label>
                    <asp:TextBox ID="UserIdLO" Text="3c3fe2c0-a1f7-4933-9e22-649c8ed6cc4f" runat="server" CssClass="form-control" autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Loan ID:</label>
                    <asp:TextBox ID="AuditLoanID" Text="e9df50a8-d838-4530-9153-9fb36c862d1c" runat="server" CssClass="form-control" autocomplete="off"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form-group">
        <span class="clipper">
            <asp:Button ID="SendAudit" Text="Send Audit Request" runat="server" OnClick="SendAudit_OnClick" CssClass="btn btn-default"/>
        </span>
        <span class="clipper">
            <asp:Button ID="SendPackageListRequest" Text="Send Package List Request" runat="server" OnClick="SendPackageTypeListRequest_OnClick" CssClass="btn btn-default"/>
        </span>
        <span class="clipper">
            <asp:Button ID="SendProcessRequest" Text="Send Process Request" runat="server" OnClick="SendProcessRequest_OnClick" CssClass="btn btn-default"/>
        </span>
        <span class="clipper">
            <asp:Button ID="SendLoadCurrentPlansRequest" Text="Load Current Plans" runat="server" OnClick="SendLoadCurrentPlansRequest_OnClick" CssClass="btn btn-default"/>
        </span>
        </div>
        <div class="row">
            <div class="col-xs-3">
        <asp:TextBox ID="PlanCode" runat="server" CssClass="form-control col-xs-3"></asp:TextBox>
            </div>
            <div class="col-xs-9">
        <span class="clipper">
	        <asp:Button ID="CreatePlanCode" OnClick="SendCreatePlanCodeRequest_OnClick" Text="Create Plan Code" runat="server" CssClass="btn btn-default" />
        </span>
                </div>
	    </div>
        <div>&nbsp;</div>
        <div class="form-group">
        <span class="clipper">
	        <asp:Button ID="CreateFormRequest" OnClick="SendFormRequest_OnClick" Text="Send Form Request" runat="server"  CssClass="btn btn-default" />
        </span>
        <span class="clipper">
            <asp:Button ID="SendCustomRequest" OnClick="SendCustomRequest_OnClick" Text="Send Custom XML Request" runat="server" CssClass="btn btn-default" />
        </span>
        </div>
         <div id="ErrPanel" runat="server">
        <div class="alert alert-error alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Error Message!</strong> <span id="ErrMsg" runat="server">Numquam quos fuga quam suscipit sapiente perferendis magnam. </span>
        </div>
        </div>
        <asp:GridView id="m_gv" runat="server" AutoGenerateColumns="true" OnRowDataBound="Gridview_OnRowDataBound" EnableViewState="false"
            CssClass="table table-striped table-condensed table-layout-fix"
            CellSpacing="0" CellPadding="0" BorderWidth="0" GridLines="None" UseAccessibleHeader="true">
	    </asp:GridView>
	    
	    <asp:GridView id="m_extgv" runat="server" AutoGenerateColumns="true" EnableViewState="false"
            CssClass="table table-striped table-condensed table-layout-fix"
            CellSpacing="0" CellPadding="0" BorderWidth="0" GridLines="None" UseAccessibleHeader="true">
	    </asp:GridView>
	    <asp:TextBox ID="ManualXML" rows="20" Width="100%" TextMode="multiline" runat="server" CssClass="form-control text-vertical" placeholder="Enter Custom XML Request..."></asp:TextBox>
        </div></div></div></div></div>
    </form>
</asp:Content>