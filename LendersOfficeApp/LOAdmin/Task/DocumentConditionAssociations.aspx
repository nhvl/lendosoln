﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentConditionAssociations.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Task.DocumentConditionAssociations" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="../../common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../Common/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Document-Condition Associations Test Page</title>
    <style type="text/css">
        .main { width: 70%; position:absolute; left:15%}
        .warning { background-color: #FF3; }
        .fatal { background-color: #F33; }
    </style>
</head>
<body>
    <form id="form" runat="server">
    <UC:HEADER id="m_Header" runat="server"></UC:HEADER><UC:HEADERNAV id="m_Navigate" runat="server">
	    <MenuItem URL="~/LOAdmin/Main.aspx" Label="Main"></MenuItem>
	    <MenuItem URL="~/LOAdmin/Task/DocumentConditionAssociations.aspx" Label="Document-ConditionAssociations"></MenuItem>
	    <MenuItem URL="." Label="Blah"></MenuItem>
    </UC:HEADERNAV>
    <div class="main">
        <asp:TextBox ID="BrokerID" runat="server">Broker ID</asp:TextBox>
        <asp:TextBox ID="LoanID" runat="server">Loan ID</asp:TextBox>
        <asp:TextBox ID="TaskID" runat="server">Task ID</asp:TextBox>
        <asp:TextBox ID="DocsID" runat="server">Document ID</asp:TextBox>
        <asp:RadioButtonList ID="Status" runat="server">
            <asp:ListItem Text="Undefined" Value="0"></asp:ListItem>
            <asp:ListItem Text="Unsatisfied" Value="1"></asp:ListItem>
            <asp:ListItem Text="Satisfied" Value="2"></asp:ListItem>
        </asp:RadioButtonList>
        
        <asp:Button ID="SaveBtn" runat="server" Text="Save" OnClick="SaveBtn_Click" />

	    
	    <ml:EncodedLabel ID="Errors" runat="server"></ml:EncodedLabel>
    </div>
    </form>
</body>
</html>
