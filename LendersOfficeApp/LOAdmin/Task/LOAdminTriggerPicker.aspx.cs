﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ConfigSystem;
using LendersOffice.Constants;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Task
{
    public partial class LOAdminTriggerPicker : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        protected Guid BrokerID
        {
            get { return new Guid(RequestHelper.GetSafeQueryString("BrokerID")); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Bind
            TriggerList.DataSource = WorkflowSystemConfigModel.GetCustomVariablesForTaskTriggerTemplate(BrokerID);
            TriggerList.DataBind();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
        }
    }
}
