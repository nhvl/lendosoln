﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoleAndGroupPicker.ascx.cs" Inherits="LendersOfficeApp.LOAdmin.Task.RoleAndGroupPicker" %>



<div class="RoleGroupSection">
    <h2>
        <ml:EncodedLiteral runat="server" ID="Name"></ml:EncodedLiteral>
    </h2>
    <p class="strong">
        <ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral>
    </p>
    <asp:CheckBoxList runat="server" ID="RoleItems" CssClass="RoleItems">
    </asp:CheckBoxList>
    <asp:CheckBoxList runat="server" ID="GroupItems" CssClass="GroupItems" >
    </asp:CheckBoxList>
    <br class="clear" />
</div>

<asp:PlaceHolder runat="server" ID="ControlScript" Visible="false">
    <script type="text/javascript">
          $j(function(){
             $j('.RoleGroupSection').each(function(p,y){
                var roleBoxes = $j('.RoleItems tr input',y), groupBoxes = $j('.GroupItems tr input',y), boxes = [roleBoxes, groupBoxes];
                $j.each(boxes, function(i,o){
                    o.slice(1).click(function(){
                        o.first().attr('checked',false);
                    });
                    
                    o.first().click(function(){
                        o.slice(1).attr('checked', this.checked);
                    });
                });
            });
        });
    </script>
</asp:PlaceHolder>