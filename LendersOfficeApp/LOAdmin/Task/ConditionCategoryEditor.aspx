﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConditionCategoryEditor.aspx.cs" 
Inherits="LendersOfficeApp.LOAdmin.Task.ConditionCategoryEditor" EnableViewState="false" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--[if lt IE 7 ]> <html xmlns="http://www.w3.org/1999/xhtml" class="ie6" > <![endif]-->
<!--[if IE 7 ]>    <html xmlns="http://www.w3.org/1999/xhtml" class="ie7" > <![endif]-->
<!--[if IE 8 ]> <html xmlns="http://www.w3.org/1999/xhtml" class="ie8" > <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html xmlns="http://www.w3.org/1999/xhtml" > <!--<![endif]-->
<head runat="server">
    <title>Condition Category Editor</title>
    <style type="text/css">
        .hide { display: none; }
         h2, h1 { 
            background-color :#003366;
            font-family: Arial, Helvetica, sans-serif;
            color: White;
            font-size: 12px;
            margin-bottom: 5px;
            margin-top: 0;
        }
        h1 { font-size: 14px; padding: 1px; }
        h2{ font-size: 14px; padding: 1px; text-transform:uppercase; }
        .center { text-align: center; }
        #wrapper { padding: 5px; }
        input[type="radio"] { vertical-align: text-bottom; }
        input[type="checkbox"] { vertical-align: bottom; }
        .ie7 input[type="checkbox"] { vertical-align: baseline; }
        .ie6 input { vertical-align: text-bottom; }
        .grid { width: 500px; }
        .scroll {  max-height: 400px; overflow: auto; }
        .ie6 .scroll { height: expression(this.scrollHeight < 400 ? "400px" : "auto"); }
        .warning { color: red; }
        
    </style>
</head>
<body>
    <script type="text/x-jquery-tmpl" id="newCatTemplate">
        <tr>
			<td style="width:100px;">
			    <input type="hidden" value="${Id}" />
                <a href="#" class="editLink">edit</a>
                <a href="#" class="updateLink hide">update</a>
                <a href="#" class="hide cancelLink">cancel </a>
            </td><td style="width:200px;">
                <span class="category_text">${Category}</span>
                <input type="text" class="category_input hide"  />
            </td><td>
                <span class="level_text">${LevelText}</span>
                <select class="hide">
			        {{each Levels}}
			            <option value="${$value.Id}" >${$value.Name}</option>
			        {{/each}}
                </select>
            </td><td>
                <span class="display_text">${DisplayText}</span>
                <input type="checkbox" class="display_input hide" />
            </td><td>
                <a href="#" class="deleteLink">delete</a>
            </td>
		</tr>
    </script>
    <script type="text/javascript" src="../../inc/json.js"></script>
    <script type="text/javascript" src="../../inc/jquery.tmpl.js"></script>
    <script type="text/javascript">
        
        
        $j(function(){
            var rowCount = 0;
            var brokerId = $j('#<%= AspxTools.ClientId(IBrokerId) %>').val();
            resizeForIE6And7(730, 670);
            var grid = $j('#<%= AspxTools.ClientId(ConditionCategories) %>')[0];
            var ausConditions = $j('#<%= AspxTools.ClientId(AusDefaultConditionCategory) %>')[0];
            var newCategoryTextBox = $j('#NewCategory')[0];
            var newCategoryLevelDdl = $j('#<%= AspxTools.ClientId(NewDefaultLevel) %>')[0];
            var newCategoryDATCheckbox = $j('#NewIsDisplayAtTop')[0];
            var newRowTmpl = $j('#newCatTemplate').template();
            
            $j('#Close').click(function(){
                onClosePopup();
            });
            
            $j('#ChangeBtn').click(function(){
                var brokerId = $j('#<%= AspxTools.ClientId(IBrokerId) %>').val();
                var migrateFrom = $j('#MigrateFrom').val();
                var migrateTo = $j('#MigrateTo').val();
                
                if (migrateFrom == migrateTo) {
                    alert('No effect.');
                    return false;
                }
                
                var data= { 
                    brokerId : brokerId,
                    src : migrateFrom,
                    dst : migrateTo
                };
                data = JSON.stringify(data);
                
                callWebMethodAsync({
                    type: "POST",
                    url: "ConditionCategoryEditor.aspx/ChangeCategories",
                    data: data,
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(msg){
                        alert('Modified ' + msg.d.AffectedTaskRows + ' condition(s) and ' + msg.d.AffectedConditionChoiceRows + ' condition choice(s).');
                    },
                    error: function(){
                        alert('Could not update');
                    }
                });
                
            });
            
            $j('#AddNewCategory').click(function(){
                var details = { 
                    'id' : -1,
                    'category' : newCategoryTextBox.value,
                    'permissionLevel' : newCategoryLevelDdl.value,
                    'isDisplayAtTop': newCategoryDATCheckbox.checked,
                    'brokerId': brokerId
                };
                
                var data = JSON.stringify(details);
                
                callWebMethodAsync({
                    type: "POST",
                    url: "ConditionCategoryEditor.aspx/UpdateCategory",
                    data: data,
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(msg){
                        if( msg.d.Error ) {
                            alert(msg.d.Error );
                            return;
                        }
                        msg.d.Levels = Levels;       
                        msg.d.LevelText = FindLevelById(msg.d.Level);       
                        msg.d.DisplayText = msg.d.IsDisplayAtTop == true ? 'Yes' : 'No';
                        var content = $j.tmpl(newRowTmpl, msg.d);
                        $j(grid).append(content);
                        setupRow(0,content);
                        var newAusOption = $j('<option>').attr('value', msg.d.Id).text(msg.d.Category);
                        $j(ausConditions).append(newAusOption);
                       
                    },
                    error: function(){
                        alert('Could not update');
                    }
                });
            });
            
            $j('#SaveAusData').click(function(){
                var requestDetails = {
                    'id' : ausConditions.value,
                    'brokerId' :  brokerId
                };
                
                 callWebMethodAsync({
                        type: "POST",
                        url: "ConditionCategoryEditor.aspx/SetAusDefaultCategory",
                        data: JSON.stringify(requestDetails),
                        contentType: 'application/json; charset=utf-8',
                        dataType: "json",
                        success: function(msg){
                            if( msg.d.Error ) {
                                alert(msg.d.Error );
                                return;
                            }
                            alert('updated');
                        },
                        error: function(){
                            alert('Could not update');
                        }
                    });
                
            });
            //dont select the header row
            $j('tbody > tr', grid).each(setupRow);
            
            function setupRow(i, parentRow) {
                var id = $j('input[type=hidden]', parentRow).val();
                var editLink = $j('a.editLink',parentRow)[0];
                var updateLink = $j('a.updateLink',parentRow)[0];
                var cancelLink = $j('a.cancelLink', parentRow)[0];
                var deleteLink = $j('a.deleteLink', parentRow)[0];
                var levels = $j('select', parentRow)[0];
                var catText = $j('span.category_text', parentRow)[0];
                var catInput = $j('input.category_input', parentRow)[0];
                var levelText = $j('span.level_text', parentRow)[0];
                var displayText = $j('span.display_text', parentRow)[0];
                var displayCb = $j('input.display_input', parentRow)[0];

                $j(editLink).click(function() {
                    $j(catInput).val($j(catText).text());
                    var id = $j("option:contains('" + $j(levelText).text() +"')",levels).val();
                    $j(levels).val(id);
                    if ($j(displayText).text() == 'Yes') {
                        $j(displayCb).attr('checked', 'checked');
                    } else {
                        $j(displayCb).removeAttr('checked');
                    }
                    $j([catText, levelText, editLink, displayText]).hide();
                    $j([cancelLink, levels, catInput, updateLink, displayCb]).show();
                });

                $j(cancelLink).click(function() {
                    $j([catText, levelText, editLink, displayText]).show();
                    $j([cancelLink, levels, catInput, updateLink, displayCb]).hide();
                });
                
                
                $j(updateLink).click(function(){
                    var details = { 
                        'id' : id,
                        'category' : catInput.value,
                        'permissionLevel' : levels.value,
                        'isDisplayAtTop': displayCb.checked,
                        'brokerId': brokerId
                    };
                    var data = JSON.stringify(details);
                    callWebMethodAsync({
                        type: "POST",
                        url: "ConditionCategoryEditor.aspx/UpdateCategory",
                        data: data,
                        contentType: 'application/json; charset=utf-8',
                        dataType: "json",
                        success: function(msg){
                            if( msg.d.Error ) {
                                alert(msg.d.Error );
                                return;
                            }
                            var selectedLevelText = $j('option[value=' + msg.d.Level + ']', levels).text();
                            $j(catText).text(msg.d.Category);
                            $j(levelText).text(selectedLevelText);
                            $j(displayText).text(msg.d.IsDisplayAtTop == true ? 'Yes' : 'No');
                            $j(cancelLink).click();
                            
                            $j('option[value=' + msg.d.Id + ']', ausConditions).text(msg.d.Item2);
                     
                        },
                        error: function(){
                            alert('Could not update');
                        }
                    });
                });
                
                $j(deleteLink).click(function(){
                    if( $j(ausConditions).val() === id ) {
                        alert('Pick and save another AUS default condition.');
                        return;
                    }
                    var details = { 
                       'id' : id,
                       'brokerId' : brokerId
                    };
                    
                    var data = JSON.stringify(details);
                    
                    callWebMethodAsync({
                        type: "POST",
                        url: "ConditionCategoryEditor.aspx/DeleteCategory",
                        data: data,
                        contentType: 'application/json; charset=utf-8',
                        dataType: "json",
                        success: function(msg){
                            if( msg.d.Error ) {
                                alert(msg.d.Error );
                                return;
                            }
                            if( msg.d ) {
                                $j('option[value=' + id + ']', ausConditions).remove();
                                $j('.migrationddl option[value=' + id + ']').remove();
                                $j(parentRow).remove();
                                 FixUpRowColors();
                            } 
                            else {
                                alert('Could not delete row');
                            }
                        },
                        error: function(){
                            alert('Could not update');
                        }
                    });
                     
                });
            }
            
            function FixUpRowColors() {
                $j('tbody > tr', grid ).each(function(i,o){
                    var cssClass = i % 2 === 0 ?  'GridItem' : 'GridAlternatingItem';
                    $j(o).addClass(cssClass);
                });
            }
            
            function FindLevelById(id) {
                for( var i = 0; i < Levels.length; i++) {
                    if( Levels[i].Id == id) {
                        return Levels[i].Name;
                    }
                }
                return '';
            }
            FixUpRowColors();
        });
    </script>
    <h4 class="page-header">Condition Categories:</h4>
    <form id="form1" runat="server">
        <asp:HiddenField runat="server" ID="IBrokerId" />
    <div class="warning">
        Warning: Adding, editing, or removing Condition Categories could break workflow.
        Be especially careful if the broker's Workflow config contains rules that rely on Condition Categories (esp. "AreAllConditionsClosed").
        If changes are necessary, wait until after hours and have a user with access to the Update Database page run Update Cached ConditionXML.
    </div>
    <div id="wrapper">
    <div class="scroll">
    <asp:GridView runat="server"  HeaderStyle-CssClass="GridHeader" CssClass="grid" ID="ConditionCategories" AutoGenerateColumns="false" OnRowCreated="ConditionCategories_OnRowCreated">
            <Columns>
                <asp:TemplateField ItemStyle-Width="100">
                    <ItemTemplate>
                        <asp:HiddenField runat="server" ID="Id" />
                        <a href="#" class="editLink">edit</a>
                        <a href="#" class="updateLink hide">update</a>
                        <a href="#" class="hide cancelLink">cancel </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Category" ItemStyle-Width="200">
                    <ItemTemplate>
                        <span class="category_text"><ml:EncodedLiteral runat="server" ID="Category"></ml:EncodedLiteral></span>
                        <input type="text" class="category_input hide"  />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Default<br />Permission Level" >
                    <ItemTemplate>
                        <span class="level_text"><ml:EncodedLiteral runat="server" ID="PermissionLevelText"></ml:EncodedLiteral></span>
                        <asp:DropDownList runat="server" ID="PermissionLevels" CssClass="hide"> </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Display At Top" >
                    <ItemTemplate>
                        <span class="display_text"><ml:EncodedLiteral runat="server" ID="IsDisplayAtTopText"></ml:EncodedLiteral></span>
                        <input type="checkbox" class="display_input hide"  />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="#" class="deleteLink">delete</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
    </asp:GridView>
    </div>
    <p>
        <label for="NewCategory">New Category</label> 
        <input type="text" id="NewCategory" name="NewCategory" /> 
        <ml:EncodedLabel AssociatedControlID="NewDefaultLevel" runat="server">Default Permission Level</ml:EncodedLabel> 
        <asp:DropDownList runat="server" ID="NewDefaultLevel"></asp:DropDownList>
        <asp:CheckBox runat="server" ID="NewIsDisplayAtTop" />
        <label for="NewCategory">Display At Top</label>
        <input type="button" id="AddNewCategory" value="Add" />
    </p>
    <p>
        <ml:EncodedLabel runat="server" AssociatedControlID="AusDefaultConditionCategory">Category for AUS conditions:</ml:EncodedLabel>
        <asp:DropDownList runat="server" ID="AusDefaultConditionCategory"></asp:DropDownList>  <input type="button" id="SaveAusData" value="Save" /> 
    </p>
    <p class="center">
        <input type="button" id="Close" value="Close" /> 
    </p>
    </div>
    <fieldset>
        <legend>Migration</legend>
        <label>Source Category
        <asp:DropDownList runat="server" ID="MigrateFrom"  CssClass="migrationddl" ></asp:DropDownList>
        </label>
        <label>Destination Category
        <asp:DropDownList runat="server" ID="MigrateTo" CssClass="migrationddl"></asp:DropDownList>
        </label>
        <input type="button" value="Change" id="ChangeBtn" />
    </fieldset>
    </form>
</body>
</html>
