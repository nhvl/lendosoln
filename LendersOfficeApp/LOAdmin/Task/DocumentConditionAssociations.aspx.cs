﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.DocumentConditionAssociation;

namespace LendersOfficeApp.LOAdmin.Task
{
    public partial class DocumentConditionAssociations : LendersOffice.Admin.SecuredAdminPage
    {

        protected void PageInit(object sender, EventArgs e)
        {
            
        }

        protected void PageLoad(object sender, EventArgs e)
        {

        }

        protected void SaveBtn_Click(object sender, EventArgs e)
        {
            var assoc = new DocumentConditionAssociation(new Guid(BrokerID.Text), new Guid(LoanID.Text), TaskID.Text, new Guid(DocsID.Text));
            assoc.Status = (E_DocumentConditionAssociationStatus)int.Parse(Status.SelectedValue);
            assoc.Save();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
