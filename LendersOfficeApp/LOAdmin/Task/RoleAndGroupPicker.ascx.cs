﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using LendersOffice.Admin;
using CommonProjectLib.Common.Lib;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Task
{
    public partial class RoleAndGroupPicker : System.Web.UI.UserControl
    {
        public E_PermissionLevel Level { get; set; }

        public IEnumerable<Tuple<Guid, string>> EmployeeGroups { get; set; }
        public bool RenderScript
        {
            get;
            set;
        }

        public IEnumerable<E_RoleT> CheckedRoles { set; private get; }
        public IEnumerable<Guid> CheckedGroups { set; private get; }

                                
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                return;
            }
            Name.Text = Level.GetFriendlyName();
            Description.Text = Level.GetFriendlyDescription();


            E_RoleT[] roles = (E_RoleT[]) Enum.GetValues(typeof(E_RoleT));
            var bindableRoles = from role in roles
                                where role != E_RoleT.Consumer
                                select new
                                {
                                    RoleDesc = role.GetFriendlyName(),
                                    Id = (int)role
                                };

            RoleItems.DataSource = bindableRoles; 
            RoleItems.DataTextField = "RoleDesc";
            RoleItems.DataValueField = "Id";
            RoleItems.DataBind();
            RoleItems.Items.Insert(0, new ListItem() { 
                Value = "-1",
                Text = "Roles" 
            });

            GroupItems.DataSource = EmployeeGroups;
            GroupItems.DataTextField = "Item2";
            GroupItems.DataValueField = "Item1";
            GroupItems.DataBind();
            GroupItems.Items.Insert(0, new ListItem()
            {
                Value = Guid.Empty.ToString(),
                Text = "Employee groups"
            });

            ControlScript.Visible = RenderScript;

            if (CheckedRoles != null)
            {
                foreach (E_RoleT role in CheckedRoles)
                {
                    RoleItems.Items.FindByValue(((int)role).ToString()).Selected = true;
                }
            }

            if (CheckedGroups != null)
            {
                foreach (Guid groupId in CheckedGroups)
                {
                    GroupItems.Items.FindByValue(groupId.ToString()).Selected = true;
                }
            }
        }



        public IEnumerable<E_RoleT> GetSelectedRoles()
        {
            return from item in GetSelectedItems(RoleItems) select (E_RoleT)int.Parse(item.Value);
        }

        public IEnumerable<Guid> GetSelectedGroups()
        {
            return from item in GetSelectedItems(GroupItems) select new Guid(item.Value);
        }

        private IEnumerable<ListItem> GetSelectedItems(CheckBoxList items)
        {
            foreach ( ListItem item in items.Items)
            {
                if (item.Selected && item.Value != "-1" && item.Value != Guid.Empty.ToString())
                {
                    yield return item;
                
                }
            }
        }

    }
}