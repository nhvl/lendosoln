﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LOAdminTriggerPicker.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Task.LOAdminTriggerPicker" %>

<!DOCTYPE html>

<%@ Import Namespace="LendersOffice.AntiXss" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        #m_searchResultsMessage
        {
          display: none;	
        }
        .Trigger td {
            padding-left: 4px;
            padding-right: 4px;
            display: block;
            border-top: 0;
            border-left: 0;
            border-right: 0;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        $(document).ready(function() {
            resizeForIE6And7(480, 600);
            $('#m_searchQuery').on("keyup", function() { search(this) });
        });

        function selectTrigger(name) {
            var args = window.dialogArguments || {};
            args.triggerName = name;
            args.OK = true;
            onClosePopup(args);
        }

        function search(o) {
            var searchQuery = $(o).val();
            var count = 0;
            var triggers = $('#TriggerList tr.Trigger').each(function() {
                var trigger = $(this);
                // filter out triggers that don't match
                if (trigger.text().toLowerCase().indexOf(searchQuery.toLowerCase()) !== -1) {
                    $(this).css('display', 'block');
                    count += 1;
                } else {
                    $(this).css('display', 'none');
                }
            });
            if (count === 0) { // if none, return "No triggers matched the search parameter."
                $('#m_searchResultsMessage').css('display', 'block');
            } else {
                $('#m_searchResultsMessage').css('display', 'none');
            }
            
        }
    </script>
    <h4 class="page-header">Select Trigger</h4>
    <form id="form1" runat="server">
    <div>
        <div style="padding: 1em">
            Name:
            <input type="text" id="m_searchQuery" style="width: 275px" />
            <asp:Button ID="m_searchButton" runat="server" Text="Search"/>
        </div>
        <asp:GridView ID="TriggerList" runat="server" AutoGenerateColumns="false" Width="100%">
            <RowStyle CssClass="Trigger GridItem" />
            <AlternatingRowStyle CssClass="Trigger GridAlternatingItem" />
            <HeaderStyle />
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>Trigger</HeaderTemplate>
                    <ItemTemplate>
                        <a href="#" onclick="selectTrigger(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>)">
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div id="m_searchResultsMessage">
            No triggers matched the search parameter.
        </div>
    </div>
    </form>
</body>
</html>
