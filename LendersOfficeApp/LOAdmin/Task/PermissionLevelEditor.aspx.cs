﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;
using LendersOffice.Admin;
using System.Data;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Task
{
    public partial class PermissionLevelEditor : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker
            };
        }

        private PermissionLevel m_permissionLevel;

        private PermissionLevel CurrentPermissionLevel
        {
            get
            {
                if (m_permissionLevel == null)
                {
                    int id = RequestHelper.GetInt("id", -1);
                    if (id == -1)
                    {
                        m_permissionLevel = new PermissionLevel(BrokerId);
                    }
                    else
                    {
                        m_permissionLevel = PermissionLevel.Retrieve(BrokerId, id);
                    }

                }
                return m_permissionLevel;
            }
        }

        private int Id
        {
            get
            {
                int id = RequestHelper.GetInt("id", -1);
                return id;
            }
        }
        private Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("BrokerId");
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            this.EnableJqueryMigrate = false;

            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");

            RegisterJsScript("../common/ModalDlg/CModalDlg.js");
            if (Page.IsPostBack)
            {
                return;
            }


            if (Id != -1)
            {
               CanBeCondition.Enabled = false == ConditionCategory.GetCategories(BrokerId).Any( category => category.DefaultTaskPermissionLevelId == Id);
               CanBeCondition.ToolTip = "This cannot be changed until all condition categories do not have this level as a default.";
            }

  
            CanBeCondition.Checked = CurrentPermissionLevel.IsAppliesToConditions;
            Name.Text = CurrentPermissionLevel.Name;
            IsActive.Checked = CurrentPermissionLevel.IsActive;
            Description.Text = CurrentPermissionLevel.Description;

            var groupData = GroupDB.GetAllGroupIdsAndNames(RequestHelper.GetGuid("BrokerId"), GroupType.Employee).ToList(); 

            RolesWorkOn.EmployeeGroups = groupData;
            RolesWorkOn.CheckedGroups = CurrentPermissionLevel.GetGroupsWithPermissionLevel(E_PermissionLevel.WorkOn);
            RolesWorkOn.CheckedRoles = CurrentPermissionLevel.GetRolesWithPermissionLevel(E_PermissionLevel.WorkOn);

            RolesClose.EmployeeGroups = groupData;
            RolesClose.CheckedGroups = CurrentPermissionLevel.GetGroupsWithPermissionLevel(E_PermissionLevel.Close);
            RolesClose.CheckedRoles = CurrentPermissionLevel.GetRolesWithPermissionLevel(E_PermissionLevel.Close);

            RolesManage.EmployeeGroups = groupData;
            RolesManage.CheckedGroups = CurrentPermissionLevel.GetGroupsWithPermissionLevel(E_PermissionLevel.Manage);
            RolesManage.CheckedRoles = CurrentPermissionLevel.GetRolesWithPermissionLevel(E_PermissionLevel.Manage);
        }

        protected void Save(object sender, EventArgs args)
        {

            BrokerDB db = BrokerDB.RetrieveById(BrokerId);
            if (db.DefaultTaskPermissionLevelIdForImportedCategories.HasValue && db.DefaultTaskPermissionLevelIdForImportedCategories.Value == CurrentPermissionLevel.Id)
            {
                if (CanBeCondition.Checked == false || IsActive.Checked == false)
                {
                    ConError.Text = "Please pick another Default task permission level before altering condition bit or is active bit on this one.";
                    ConError.Visible = true;
                    return;
                }
            }

            if (false == CanBeCondition.Checked || false == IsActive.Checked)
            {
                var items = PermissionLevel.RetrieveAll(RequestHelper.GetGuid("BrokerId"));
                if (false == items.Where(level => Id != level.Id && level.IsAppliesToConditions == true && level.IsActive).Any())
                {
                    ConError.Visible = true;
                    return;
                }
            }

            if (db.IsEnableDriveIntegration && db.IsEnableDriveConditionImporting && CurrentPermissionLevel.Id == db.DriveTaskPermissionLevelID && !IsActive.Checked)
            {
                ConError.Text = "This is currently the Default Task Permission Level for imported DRIVE conditions. Set a new DRIVE Task Permission Level before deactivating this one.";
                ConError.Visible = true;
                return;
            }

            CurrentPermissionLevel.IsAppliesToConditions = CanBeCondition.Checked;
            CurrentPermissionLevel.Name = Name.Text;
            CurrentPermissionLevel.IsActive = IsActive.Checked;
            CurrentPermissionLevel.Description = Description.Text;

            foreach (RoleAndGroupPicker picker in new RoleAndGroupPicker[] { this.RolesWorkOn, this.RolesClose, this.RolesManage })
            {
                CurrentPermissionLevel.SetRoles(picker.Level, picker.GetSelectedRoles());
                CurrentPermissionLevel.SetGroups(picker.Level, picker.GetSelectedGroups());
            }


            CurrentPermissionLevel.Save();

            ClientScript.RegisterClientScriptBlock(typeof(PermissionLevelEditor), "close", "onClosePopup()", true);

        }
    }
}
