﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LOAdminTaskViewer.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Task.LOAdminTaskViewer" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Task Viewer</title>
        <style type="text/css">
        .SubHeader
        {
            padding: 3px;
            text-align: right;
            font-weight: bold;
        }
        
        .ErrorMessage
        {
            font-weight: bold;
            color:Red;
            padding:0.5em;
        }
    </style>
    
    <script type="text/javascript">
        function onload() {
            var errorD = document.getElementById("pnlError");            
            if (errorD != null)
	            window.resizeTo(200, 100);
	         else
	            window.resizeTo(750, 600);
	    }
    </script>
</head>
<body bgcolor="gainsboro" onload="onload();">
	 <script type="text/javascript">
	    
	    function openEditor(op) {
	        window.open("LOAdminTaskEditorV2.aspx" + "?"
	            + "brokerId=" + <%= AspxTools.JsString(BrokerID.ToString()) %>
	            + "&taskId=" + <%= AspxTools.JsString(TaskID.ToString()) %>
	            + "&mode=" + op,
	            '_self');
	    }
    </script>
    <form id="form1" runat="server">
        
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <div class="FormTableSubheader" style=" padding:0.25em">ERROR</div>
        
        <div style="text-align:center;width:20em; padding-top:0.5em">        
        <ml:EncodedLabel runat="server" ID="lblMessage" CssClass="ErrorMessage"></ml:EncodedLabel>
        <br />
        <br />        
        <input type="button" value="Close" onclick="javascript:self.close()" />
        </div>
        
    </asp:Panel>
    
    <asp:Panel ID="pnlTaskViewer" runat="server">
    
    <asp:HiddenField ID="PermissionDescription" runat="server" />
    
    <asp:HiddenField ID="ResolutionDenialMessage" runat="server" />
    
        <table cellSpacing="0" cellPadding="2" width="100%" >
		    <tr class="FormTableSubheader" style="padding:3px; height: 2.5em">
			    <td>
			        <ml:EncodedLabel ID="m_Header" runat="server"></ml:EncodedLabel>
			    </td>
			    <td colspan="3" align="right">
			        <a id="m_editLink" runat="server" href="#" onclick="openEditor('edit');">Edit</a>
			        <input type="button" value="Exit" onclick="self.close();" />
			    </td>
		    </tr>
            <tr>
                <td class="SubHeader" style="font-size:small">
                    Subject
                </td>
		        <td colspan="3" style="font-size:small">
		            <ml:EncodedLabel ID="Subject" runat="server"></ml:EncodedLabel>
		        </td>
		    </tr>
		    <tr>
		        <td class="SubHeader">
		            Creation Trigger
		        </td>
		        <td>
		            <ml:EncodedLabel ID="Trigger" runat="server"></ml:EncodedLabel>
		        </td>
		        <td class="SubHeader">
		            Frequency
		        </td>
		        <td>
		            <ml:EncodedLabel ID="Frequency" runat="server"></ml:EncodedLabel>
		        </td>
            </tr>
		    <tr>
		        <td class="SubHeader">
		            Resolution Block Trigger
		        </td>
		        <td>
		            <ml:EncodedLabel ID="ResolutionBlockTrigger" runat="server"></ml:EncodedLabel>
		            <a onclick="alert(document.getElementById('ResolutionDenialMessage').value);return false;">?</a>                    
		        </td>
		        <td class="SubHeader">
		            Resolution Date Setter
		        </td>
		        <td>
		            <ml:EncodedLabel ID="ResolutionDateSetter" runat="server"></ml:EncodedLabel>
		        </td>
            </tr>
		    <tr>
                <td class="SubHeader">
		            Auto-Resolve Trigger
		        </td>
		        <td>
		            <ml:EncodedLabel ID="AutoResolveTriggerName" runat="server"></ml:EncodedLabel>
		        </td>
		        <td class="SubHeader">
		            To Be Assigned To
		        </td>
		        <td>
		            <ml:EncodedLabel ID="TemplateAssigned" runat="server"></ml:EncodedLabel>
		        </td>
            </tr>
            <tr>
                <td class="SubHeader">
		            Auto-Close Trigger
		        </td>
		        <td>
		            <ml:EncodedLabel ID="AutoCloseTriggerName" runat="server"></ml:EncodedLabel>
		        </td>
                <td class="SubHeader">
		            To Be Owned By
		        </td>
		        <td>
		            <ml:EncodedLabel ID="TemplateOwner" runat="server"></ml:EncodedLabel>
		        </td>
            </tr>
		    <tr>
		        <td class="SubHeader">
		            Due Date
                </td>
                <td>
		            <ml:EncodedLabel ID="DueDate" runat="server"></ml:EncodedLabel>
		            <ml:EncodedLabel ID="DueDateCalcDescription" runat="server"></ml:EncodedLabel>
                </td>
                <td class="SubHeader">
                    Task Permission
                </td>
                <td>
		            <ml:EncodedLabel ID="TaskPermission" runat="server"></ml:EncodedLabel>
		            <a href="#" onclick="alert(document.getElementById('PermissionDescription').value);return false;">?</a>                    
                </td>
		    </tr>
		    <tbody id="m_conditionSection" runat="server">
		    <tr>
		        <td class="FormTableSubheader" colspan="4" style="padding:3px">
		            Condition Info
		        </td>
		    </tr>
		    <tr>
		        <td class="SubHeader">
		            Category 
		        </td>
		        <td>
		            <ml:EncodedLabel ID="ConditionCategoryChoice" runat="server"></ml:EncodedLabel>
			    </td>
		        <td class="SubHeader">
		            <ml:EncodedLabel ID="IsHiddenLabel" runat="server" Text="Hidden"></ml:EncodedLabel>		            
		        </td>
		        <td>
		            <ml:EncodedLabel ID="ConditionIsHidden" runat="server"></ml:EncodedLabel>
			    </td>
		    </tr>
		    <tr>
		        <td class="SubHeader">
		            Required Condition Type
		        </td>
		        <td colspan="3">
		            <ml:EncodedLabel runat="server" ID="ReqCondType"></ml:EncodedLabel>
		        </td>
		    </tr>
		    <tr>
		        <td class="SubHeader" valign="top">
		            Internal Notes
		        </td>
		        <td colspan="3">
	                <asp:TextBox ID="ConditionInternalNotes" TextMode="MultiLine" Rows="6" Width="500px" runat="server" ReadOnly="true"></asp:TextBox>
		        </td>
		    </tr>
		    </tbody>
		    <tr>
		        <td colspan="4">
		            <hr />
		        </td>
		    </tr>
		    <tr>
		        <td colspan="4">
                    <ml:EncodedLabel ID="History" runat="server"></ml:EncodedLabel>
		        </td>
		    </tr>
		    <tr>
		        <td colspan="4">
		            <hr />
		        </td>
		    </tr>
        </table>
        
        </asp:Panel>
    </form>
</body>
</html>