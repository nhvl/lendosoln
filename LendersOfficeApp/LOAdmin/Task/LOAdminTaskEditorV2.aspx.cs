﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Task
{
    public partial class LOAdminTaskEditorV2 : LendersOffice.Admin.SecuredAdminPage
    {
        protected Guid LoanID = Guid.Empty;
        protected bool IsTemplate = false;
        protected int TaskID
        {
            get { return int.Parse(RequestHelper.GetSafeQueryString("taskid")); }
        }

        protected Guid BrokerID
        {
            get { return new Guid(RequestHelper.GetSafeQueryString("BrokerID")); }
        }

        protected bool IsNewTask
        {
            get { return string.IsNullOrEmpty(RequestHelper.GetSafeQueryString("taskid")); }
        }

        protected override E_JqueryVersion GetJQueryVersion() => E_JqueryVersion._1_6_1;

        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            this.PageTitle = "Task Editor";
            ConditionCategories.DataSource = ConditionCategory.GetCategories(BrokerID);
            ConditionCategories.DataTextField = "Category";
            ConditionCategories.DataValueField = "Id";
            ConditionCategories.DataBind();

            TaskTriggerTemplate task;
            // Retrieve or create task
            if (IsNewTask)
            {
                task = new TaskTriggerTemplate(BrokerID);
                Page_Init_Add(task);
            }
            else
            {
                task = TaskTriggerTemplate.GetTaskTriggerTemplate(BrokerID, TaskID);
                Page_Init_Edit(task);
            }
            IsCondition.Disabled = !task.PermissionLevelAppliesToConditions;
            CondDocType.EnableClearLink();
            CondDocType.SetDefault("None", "-1");
        }

        private void Page_Init_Edit(TaskTriggerTemplate task)
        {
            // Fill in info
            m_Header.Text = "Edit " + (task.IsConditionTemplate ? "Condition " : "Task ") + task.AutoTaskTemplateId;
            Subject.Text = task.Subject;
            TriggerLabel.Text = task.TriggerName;
            TriggerName.Value = task.TriggerName;
            FrequencyList.SelectedValue = task.Frequency.ToString();
            TaskTemplateAssignedPersist.Value = TaskTemplateAssigned.Text = task.AssignedUserFullName;
            AssignedID.Value = task.AssignedUserId.ToString(); // The naming is off since it follows the pattern set by the other task object
            AssignedRole.Value = task.ToBeAssignedRoleId.ToString();
            TaskTemplateOwnerPersist.Value = TaskTemplateOwner.Text = task.OwnerFullName;
            OwnerID.Value = task.OwnerUserId.ToString();
            OwnerRole.Value = task.ToBeOwnedByRoleId.ToString();
            DueDateCalcDescriptionPersist.Value = DueDateCalcDescription.Text = task.DueDateFriendlyDescription;
            TaskPermissionPersist.Value = TaskPermission.Text = task.PermissionLevelName;
            PermissionDescription.Value = task.PermissionLevelDescription;
            PermissionLevelID.Value = task.TaskPermissionLevelId.ToString();
            DueDateCalcField.Value = task.DueDateCalculatedFromField;
            DueDateOffset.Value = task.DueDateCalculationOffset.ToString();
            IsCondition.Checked = task.IsConditionTemplate;
            ConditionInternalNotes.Text = task.CondInternalNotes;
            ConditionIsHidden.Checked = task.CondIsHidden;
            m_Comments.Text = task.Comments;
            if (task.CondRequiredDocTypeId.HasValue)
            {
                CondDocType.Select(task.CondRequiredDocTypeId.Value);
            }
            ResolutionBlockTriggerLabel.Text = string.IsNullOrEmpty(task.ResolutionBlockTriggerName) ? "None" : task.ResolutionBlockTriggerName;
            ResolutionBlockTriggerName.Value = task.ResolutionBlockTriggerName;
            ResolutionDenialMessage.Text = task.ResolutionDenialMessage;
            ResolutionDateSetter.Text = task.GetResolutionDateSetterFieldDescription();
            ResolutionDateSetterFieldId.Value = task.ResolutionDateSetterFieldId;

            AutoResolveTriggerLabel.Text = string.IsNullOrEmpty(task.AutoResolveTriggerName) ? "None" : task.AutoResolveTriggerName;
            AutoResolveTriggerName.Value = task.AutoResolveTriggerName;

            AutoCloseTriggerLabel.Text = string.IsNullOrEmpty(task.AutoCloseTriggerName) ? "None" : task.AutoCloseTriggerName;
            AutoCloseTriggerName.Value = task.AutoCloseTriggerName;


            if (task.ConditionCategoryId.HasValue)
            {
                Tools.SetDropDownListValue(ConditionCategories, task.ConditionCategoryId.Value);
            }
            //m_errorMessage
            //History = task.
        }

        private void Page_Init_Add(TaskTriggerTemplate task)
        {
            m_Header.Text = "Add Task";
            FrequencyList.SelectedIndex = 0;
            AssignedID.Value = Guid.Empty.ToString();
            AssignedRole.Value = Guid.Empty.ToString();
            OwnerID.Value = Guid.Empty.ToString();
            OwnerRole.Value = Guid.Empty.ToString();
            TaskPermissionPersist.Value = TaskPermission.Text = task.PermissionLevelName;
            PermissionDescription.Value = task.PermissionLevelDescription;
            PermissionLevelID.Value = task.TaskPermissionLevelId.ToString();

            ResolutionBlockTriggerLabel.Text = "None";
            ResolutionDateSetter.Text = "None";

            AutoResolveTriggerLabel.Text = "None";
            AutoCloseTriggerLabel.Text = "None";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("LQBPopup.js");

            // Persist label values
            // Since labels are not saved by the IPostBackDataHandler, but hidden values are,
            //   we can persist the labels through postbacks by putting their text in hidden
            //   controls.
            TriggerLabel.Text = TriggerName.Value;
            TaskTemplateAssigned.Text = TaskTemplateAssignedPersist.Value;
            TaskTemplateOwner.Text = TaskTemplateOwnerPersist.Value;
            DueDateCalcDescription.Text = DueDateCalcDescriptionPersist.Value;
            TaskPermission.Text = TaskPermissionPersist.Value;
        }

        protected void OnSaveClicked(Object sender, CommandEventArgs e)
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            // Get or create the task
            if (IsNewTask)
            {
                OnSaveClicked_Add();
            }
            else
            {
                OnSaveClicked_Edit();
            }
        }

        private void OnSaveClicked_Edit()
        {
            var task = TaskTriggerTemplate.GetTaskTriggerTemplate(BrokerID, TaskID);
            if (SaveTaskFields(task))
            {
                ClosePage(TaskID.ToString());
            }
        }

        private void OnSaveClicked_Add()
        {
            var task = new TaskTriggerTemplate(BrokerID);
            if (SaveTaskFields(task))
            {
                ClosePage(task.AutoTaskTemplateId.ToString());
            }
        }

        /// <summary>
        /// Save the fields
        /// </summary>
        /// <param name="task"></param>
        /// <returns>True if it completed successfully, otherwise false.</returns>
        private bool SaveTaskFields(TaskTriggerTemplate task)
        {
            if (!ValidateTaskFields())
            {
                return false;
            }

            // Change the fields
            task.Subject = Subject.Text;
            task.TriggerName = TriggerName.Value;
            task.Frequency = (E_AutoTaskFrequency)Enum.Parse(typeof(E_AutoTaskFrequency), FrequencyList.SelectedValue);

            if (AssignedID.Value != Guid.Empty.ToString())
            {
                task.AssignedUserId = new Guid(AssignedID.Value);
                task.ToBeAssignedRoleId = null;
            }
            else
            {
                task.AssignedUserId = Guid.Empty;
                task.ToBeAssignedRoleId = new Guid(AssignedRole.Value); // To be assigned to // hiddenfield
            }

            if (OwnerID.Value != Guid.Empty.ToString())
            {
                task.OwnerUserId = new Guid(OwnerID.Value);
                task.ToBeOwnedByRoleId = null;
            }
            else
            {
                task.OwnerUserId = Guid.Empty;
                task.ToBeOwnedByRoleId = new Guid(OwnerRole.Value); // To be owned by // hiddenfield
            }

            task.DueDateCalculatedFromField = DueDateCalcField.Value;
            if (!string.IsNullOrEmpty(DueDateOffset.Value))
            {
                task.DueDateCalculationOffset = int.Parse(DueDateOffset.Value);
            }
            if (!string.IsNullOrEmpty(PermissionLevelID.Value))
            {
                task.TaskPermissionLevelId = int.Parse(PermissionLevelID.Value);
            }

            task.IsConditionTemplate = IsCondition.Checked;
            if (IsCondition.Checked)
            {
                task.ConditionCategoryId = int.Parse(ConditionCategories.SelectedValue);
                task.CondIsHidden = ConditionIsHidden.Checked;
                task.CondInternalNotes = ConditionInternalNotes.Text;
                int docTypeValue;
                if (int.TryParse(CondDocType.Value, out docTypeValue) && docTypeValue > -1)
                {
                    task.CondRequiredDocTypeId = docTypeValue;
                }
                else if (docTypeValue == -1)
                {
                    task.CondRequiredDocTypeId = null;
                }
            }
            else
            {
                task.ConditionCategoryId = null;
                task.CondIsHidden = false;
                task.CondInternalNotes = "";
            }

            task.Comments = m_Comments.Text;

            task.ResolutionBlockTriggerName = ResolutionBlockTriggerName.Value;
            task.ResolutionDenialMessage = ResolutionDenialMessage.Text;
            task.ResolutionDateSetterFieldId = ResolutionDateSetterFieldId.Value;

            task.AutoResolveTriggerName = AutoResolveTriggerName.Value;
            task.AutoCloseTriggerName = AutoCloseTriggerName.Value;

            task.Save();

            return true;
        }

        private bool ValidateTaskFields()
        {
            bool valid = true;
            List<string> missingFields = new List<string>();
            if (string.IsNullOrEmpty(Subject.Text))
            {
                missingFields.Add("Subject");
                valid = false;
            }
            if (string.IsNullOrEmpty(TriggerName.Value))
            {
                missingFields.Add("Trigger");
                valid = false;
            }
            if ((string.IsNullOrEmpty(AssignedRole.Value) || AssignedRole.Value == Guid.Empty.ToString())
                && (string.IsNullOrEmpty(AssignedID.Value) || AssignedID.Value == Guid.Empty.ToString()))
            {
                missingFields.Add("Assignment");
                valid = false;
            }
            if ((string.IsNullOrEmpty(OwnerRole.Value) || OwnerRole.Value == Guid.Empty.ToString())
                && (string.IsNullOrEmpty(OwnerID.Value) || OwnerID.Value == Guid.Empty.ToString()))
            {
                missingFields.Add("Owner");
                valid = false;
            }
            if (string.IsNullOrEmpty(DueDateCalcField.Value))
            {
                missingFields.Add("Due Date");
                valid = false;
            }

            // Populate message
            if (missingFields.Count > 0)
            {
                m_errorMessage.Text = "The following fields are required: " + string.Join(", ", missingFields.ToArray());
            }

            return valid;
        }

        private void ClosePage(string taskID)
        {
            ScriptManager.RegisterClientScriptBlock(
                Page,
                Page.GetType(),
                "close",
                string.Format(@"closePage('{0}');", taskID),
                true
            );
        }
    }
}
