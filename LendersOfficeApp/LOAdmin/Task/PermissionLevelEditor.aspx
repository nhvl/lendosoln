﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PermissionLevelEditor.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Task.PermissionLevelEditor" %>
<%@ Register TagPrefix="uc" TagName="RoleGroupPicker" Src="~/LOAdmin/Task/RoleAndGroupPicker.ascx"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--[if lt IE 7 ]> <html xmlns="http://www.w3.org/1999/xhtml" class="ie6" > <![endif]-->
<!--[if IE 7 ]>    <html xmlns="http://www.w3.org/1999/xhtml" class="ie7" > <![endif]-->
<!--[if IE 8 ]> <html xmlns="http://www.w3.org/1999/xhtml" class="ie8" > <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html xmlns="http://www.w3.org/1999/xhtml" > <!--<![endif]-->

<head runat="server">
    <title>Permission Level Editor</title>
    <style type="text/css" >
        input[type="radio"] { vertical-align: text-bottom; }
        input[type="checkbox"] { vertical-align: bottom; }
        .ie7 input[type="checkbox"] { vertical-align: baseline; }
        .ie6 input { vertical-align: text-bottom; }
        .MainTable { width: 700px; }
        .RoleGroupSection { width: 350px; margin-left: 5px;  }
        .RoleItems, .GroupItems, .RoleGroupSection { float: left; }
        .clear { clear: both; }
        .RoleItems tr:first-child, .GroupItems tr:first-child, h2, h1 { 
            background-color :#003366;
            font-family: Arial, Helvetica, sans-serif;
            color: White;
            font-size: 12px;
        }
        h1 { font-size: 14px; padding: 1px; margin-top: 0px; }
        h2{ font-size: 14px; padding: 1px; text-transform:uppercase; }
        .center { text-align: center; }
        .strong { font-weight: bold; }
    </style>
     <base target="_self" />
   
</head>
<body>
    <script type="text/javascript">
        $(function(){
            resizeForIE6And7(1095, 850);
            $('#closeBtn').click(function(){
                onClosePopup();
            });
        });
    </script>
    <h4 class="page-header">Task permission level editor</h4>
    <form id="form1" runat="server" >
    <div>
        <table class="MainTable">            
                      <tr>
                      <td colspan="3">
                      &nbsp;
                  <ml:EncodedLabel runat="server" ID="ConError" Visible="false" ForeColor="Red"  Font-Bold="true"   >
                    This level has to apply to conditions and be active because this broker does not contain any other active permission levels that apply to conditions.</ml:EncodedLabel>
                    </td>
            </tr>
            <tr>
                <td>
                <asp:CheckBox runat="server" ID="IsActive" />
                </td>
                <td colspan="2">
                    Active
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="Name">Permission level name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="Name"></asp:TextBox>
                    <asp:CheckBox runat="server" ID="CanBeCondition" Text="Can be a condition" />
              
                </td>
            </tr>
            <tr>
                   <td>
                    &nbsp;
                </td>
                <td>
                    <ml:PassthroughLabel runat="server" AssociatedControlID="Description">Permission level description 
                    <br /> (shown to end users)</ml:PassthroughLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" TextMode="MultiLine" ID="Description" Rows="8" Columns="50"></asp:TextBox>
                </td>
            </tr>
        </table>
        <br />
        <uc:RoleGroupPicker runat="server" id="RolesWorkOn" Level="WorkOn"></uc:RoleGroupPicker> 
        <uc:RoleGroupPicker runat="server" id="RolesClose"  Level="Close"></uc:RoleGroupPicker> 
        <uc:RoleGroupPicker runat="server" id="RolesManage" Level="Manage" RenderScript="true"></uc:RoleGroupPicker> 

        <div class="clear center">
        <asp:Button runat="server" ID="OkayBtn" Text="OK" OnClick="Save" />
        <input type="button" value="Cancel" id="closeBtn" />
        </div>
    </div>
    </form>
</body>
</html>
