﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PermissionLevels.aspx.cs" EnableViewState="false" Inherits="LendersOfficeApp.LOAdmin.Task.PermissionLevels" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!--[if lt IE 7 ]> <html xmlns="http://www.w3.org/1999/xhtml" class="ie6" > <![endif]-->
<!--[if IE 7 ]>    <html xmlns="http://www.w3.org/1999/xhtml" class="ie7" > <![endif]-->
<!--[if IE 8 ]> <html xmlns="http://www.w3.org/1999/xhtml" class="ie8" > <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html xmlns="http://www.w3.org/1999/xhtml" > <!--<![endif]-->
<head runat="server">
    <title>Permission Levels</title>
    <style type="text/css" >
        a, img { outline: none; }
        a.moveArrow:link { text-decoration: none; }
        a img { border: 0; }
        a.moveArrow img { width: 10px; height: 10px; }
        .moveContainer { margin-left: 10px; }
        .grippy, .grippy img { float:left;  padding: 0; margin: 0; }
        .top { margin-top: 10px; }
        .hide { display: none; }
         h2, h1 { 
            background-color :#003366;
            font-family: Arial, Helvetica, sans-serif;
            color: White;
            font-size: 12px;
            margin-bottom: 5px;
            margin-top: 0px;
     
            
        }
        h1 { font-size: 14px; padding: 1px; }
        h2{ font-size: 14px; padding: 1px; text-transform:uppercase; }
        p.strong { font-weight: bold; }
        .centerContents { text-align: center; }
        .force { height: 65px; line-height: 16px; background-color: #003366;  }
        a:hover, a:active, a:focus { outline: 0; border: 0; text-decoration: none;}
        .leftmargin { margin-left: 5px; }
		.gridwith { width: 90%; }
    </style>
</head>
<body>
    <script type="text/javascript" src="../../inc/json.js"></script>
    <script type="text/javascript">
    </script>
    <h4 class="page-header">Task permission level administration</h4>
	
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="PageBrokerId" />
    <asp:HiddenField runat="server"  ID="PermissionLevelOrder" />
    <asp:HiddenField runat="server" ID="SortOrder" />
    <div>
        <p class="strong leftmargin">
        Default permission level for imported condition categories <asp:DropDownList runat="server" id="DefaultLevelForConditions"></asp:DropDownList>
        </p>
        <asp:GridView runat="server"  CssClass="leftmargin gridwith" AlternatingRowStyle-CssClass="GridAlternatingItem" 
             RowStyle-CssClass="GridItem"
             HeaderStyle-CssClass="GridHeader" 
            ID="PermissionLevelGrid" AutoGenerateColumns="false"
               OnRowCreated="PermissionLevelGiod_OnRowCreated"
            >
            <Columns>
               <asp:TemplateField  ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <a class="editlink" >edit</a>
                </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField  HeaderText="Re-Order" ItemStyle-Width="50px"  >
                    <ItemTemplate>
                    
                        <asp:HiddenField runat="server" ID="Id" />
                        
                        <a class="grippy" title="Drag and drop" ><img src="../../images/grip.png" alt="G" /> </a>
           
                        <div class="moveContainer top">
                         <a class="moveArrow Up" title="Move the condition one space up in the list"><img src="../../images/up-blue.gif"  /></a>
                         <a class="moveArrow Top"   title="Move the condition to the top of the list"><img src="../../images/up-blue-top.gif"  /></a>
                        </div>
                        <div class="moveContainer">
                         <a class="moveArrow  Down" title="Move the condition one space down in the list"><img src="../../images/down-blue.gif"  /></a>
                         <a class="moveArrow Bottom" title="Move the condition to the bottom of the list"><img src="../../images/down-blue-bottom.gif"  /></a>
                        </div>
                    </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="Name" HeaderText="Permission"   />
               <asp:BoundField DataField="DescriptionForHtml" HeaderText="Description" HtmlEncode="false"  />
               <asp:CheckBoxField HeaderText="Can be<br/>Condition" DataField="IsAppliesToConditions" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" />
               <asp:CheckBoxField HeaderText="Active" DataField="IsActive" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" />
            </Columns>
        </asp:GridView>
        
    </div>
    
    <p class="leftmargin"> 
        <input type="button" id="AddNewLevelBtn" value="Add permission level" />
    </p>
    
    <div class="centerContents">
        <input id="OkayBtn" type="button" value="OK" />
        <input id="CloseBtn" type="button" value="Cancel" />
        <asp:Button runat="server" ID="PostbackBtn" CssClass="hide" OnClick="OkayBtn_OnClick" />
    </div>
    
    <script type="text/javascript">
        $(function(){

            function getQueryString() {
              var result = {}, queryString = location.search.substring(1),
                  re = /([^&=]+)=([^&]*)/g, m;

              while (m = re.exec(queryString)) {
                result[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
              }

              return result;
            }


            resizeForIE6And7(730, 650);
            
            var table = $('#<%= AspxTools.ClientId(PermissionLevelGrid) %>')[0];
            var brokerId = $('#<%= AspxTools.ClientId(PageBrokerId) %>').val();
            var editorUrlData = '/LoAdmin/Task/PermissionLevelEditor.aspx?BrokerId=' + brokerId ;
                   
                   
            $('#CloseBtn').click(function(){
                onClosePopup();
            });
                
            $('#OkayBtn').click(function(){
                var order = [];
                $('input[type=hidden]', table ).each(function(o,i){
                   order.push($(i).val());
                });
                
                $('#<%= AspxTools.ClientId(SortOrder) %>').val(JSON.stringify(order));
                $('#<%= AspxTools.ClientId(PostbackBtn) %>').click();
            });   
            
            $('.editlink', table).each(function(o,i){
                var parentRow = $(i).parents('tr:first')[0];
                var id = $('input[type=hidden]',parentRow).val();
               
                $(i).click(function(){
                    showEditorPage(id);
                    return false;
                });
            });
            
            $('#AddNewLevelBtn').click(function(){
                showEditorPage();
            });
                
            var arrowData = { 
                'Top' : { 'Img' : [ '../../images/up-orange-top.gif','../../images/up-blue-top.gif'], 'Click' : function() { moveUp(this, true); return false; } }, 
                'Up' : { 'Img' : [ '../../images/up-orange.gif','../../images/up-blue.gif'], 'Click' : function() { moveUp(this, false); return false; } }, 
                'Bottom' : { 'Img' : [ '../../images/down-orange-bottom.gif','../../images/down-blue-bottom.gif'], 'Click' : function() { moveDown(this, true); return false; } }, 
                'Down' : { 'Img' : [ '../../images/down-orange.gif','../../images/down-blue.gif'], 'Click' : function() { moveDown(this, false); return false; } }, 
                'Types' : ['Top', 'Up', 'Down', 'Bottom' ] 
            };
            
            $.each(arrowData.Types, function(o, i){
                $('.moveArrow.' + i).mouseover(function(e){
                    $(this).children('img').attr('src', arrowData[i].Img[0]);
                }).mouseout(function(e){
			        $(this).children('img').attr('src', arrowData[i].Img[1]);
                }).click(arrowData[i].Click);
            });
            
            function moveUp(anchor, toTop) {
			    var tbody = table.tBodies[0];
			    var row = $(anchor).parents('tr:first')[0];
			    var rowIndex = row.sectionRowIndex; 
			    if( rowIndex < 1 ) {
			        return;   
			    }
			    
			    if(toTop) {
    		        moveRow(anchor,tbody,rowIndex, 0);		//the header is 0
    		        return;	            
			    }
			    moveRow(anchor,tbody,rowIndex, rowIndex -1 );
			}
			

			
			function moveRow(anchor,tbody, rowIndex, newRowIndex) 
			{
                var movingRow = tbody.rows[rowIndex]; 
			    
			    var inputs = movingRow.getElementsByTagName('input');
			    
			    for( var i = 0; i < inputs.length; i++) {
			        var input = inputs[i];
			        if(  input.type != 'checkbox' ) {
			            continue;
			        }
			        if( typeof(input.defaultChecked) != 'undefined') {
			            input.defaultChecked = input.checked;
			        }
			    }
                jQuery(anchor).triggerHandler('mouseout');
                moveRowPoly(tbody, rowIndex, newRowIndex);
			}
			
			function moveDown(anchor, toBottom) {
			    var tbody = table.tBodies[0];
			    var row = $(anchor).parents('tr:first')[0];
			    var rowIndex = row.sectionRowIndex; 
			    
			    if( rowIndex + 1 == tbody.rows.length ) {
			        return; //already at the bottom cant keep going
			    }
			    
			    if( toBottom ) {
			        moveRow(anchor,tbody,rowIndex, tbody.rows.length -1);
    			    return;
			    }
			    moveRow(anchor, tbody, rowIndex, rowIndex + 1 );
			}
			
	        function showEditorPage(id) {
	            var url = editorUrlData; 
	            if( id ) {
	                url = editorUrlData + '&id=' + id;
	            }
	            
	            showModal(url, null, null, null, function(){ reload();});
	           
	           
	        }
			
			$('tbody', table).sortable( { 
    			axis: 'y', 
    			handle : '.grippy', 
    			placeholder : 'force',
    			update : function(){},
    			start: function (e, ui) { 
    			    ui.placeholder.html('<td colspan='+ ui.item.children().length+'>&nbsp;</td>'); 
                }
			});
			
			$('.grippy').mouseover(function(){
			    $(this).css('cursor', 'move');
			});	
			
			var query = getQueryString();
			if (query['cm'] === '1' ){
                alert('Changes not saved: The default permission level for imported condition categories must be a level that has the condition bit set');
            }
            
            
        });
        
        function reload() {
            window.location = window.location;
        }
    </script>
			<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></form>
    
    </form>
</body>
</html>
