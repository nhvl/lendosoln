﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LOAdminTaskEditorV2.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Task.LOAdminTaskEditorV2" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Task Editor</title>
    <style type="text/css">
        .SubHeader
        {
            padding: 3px;
            text-align: right;
            font-weight: bold;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript">
    <!--
        var taskIsDirty = false;

        function _init() {
            if (<%= AspxTools.HtmlString((Page.IsPostBack == true).ToString()).ToLower() %>) {
                setTaskDirty();
            }
            window.resizeTo(850, 675);
            checkDueDateStatus();

            var emailBody = document.getElementById('EmailBody');
            if (emailBody) {
                emailBody.cursorPos = 0;
            }

            displayConditionFields();
            document.getElementById('Subject').focus();
        }

        function attemptTaskListRefresh() {
            if(window.opener != null && !window.opener.closed)
            {
                window.opener.document.forms[0].submit();
            }
        }

        function dateCalculationPopup() {
            var offset = document.getElementById("DueDateOffset");
            var field = document.getElementById("DueDateCalcField");
            showModal("/newlos/Tasks/DateCalculator.aspx"
                + "?duration=" + offset.value
                + "&fieldId=" + field.value
                + "&IsTaskTriggerTemplate=" + true
                , null, null, null, function(args){
                    if (args.OK) {
                        offset.value = args.offset;
                        field.value = args.id;
                        document.getElementById("DueDateCalcDescription").innerText = args.dateDescription;
                        document.getElementById("DueDateCalcDescriptionPersist").value = args.dateDescription;
                        checkDueDateStatus();
                        setTaskDirty();
                    }
            }, {hideCloseButton:true});
        }

        function permissionPopup() {
            var IsCondition = document.getElementById('IsCondition').checked;
            showModal("/newlos/Tasks/TaskPermissionPicker.aspx"
                + "?IsCond=" + IsCondition
                + '&BrokerId=' + <%= AspxTools.JsString(BrokerID.ToString()) %>
                + '&IsTemplate=' + true
                , null, null, null, function(args){
                    if (args.OK) {
                        var IsCondCb = document.getElementById("IsCondition");
                        document.getElementById("PermissionDescription").value = args.permissionDescription;
                        document.getElementById("PermissionLevelID").value = args.id;
                        if (!IsCondCb.checked) {
                            IsCondCb.disabled = args.conditionsAllowed != "True";
                        }
                        document.getElementById("TaskPermission").innerText = args.name;
                        document.getElementById("TaskPermissionPersist").value = args.name;

                        setTaskDirty();
                    }
                });
        }

        function displayConditionFields() {
            var display = document.getElementById('IsCondition').checked ? '' : 'none';
            document.getElementById('ConditionRow1').style.display = display;
            document.getElementById('ConditionRow2').style.display = display;
            document.getElementById('ConditionRow3').style.display = display;
            document.getElementById('ConditionRow4').style.display = display;
        }

        function checkDueDateStatus() {

        }

        function changeAssigned(mode) {
            var permLevel = document.getElementById('PermissionLevelID').value;
            var owner = mode == 'templateOwner';
            if (mode == 'templateOwner') {
                var id = document.getElementById("OwnerID");
                var role = document.getElementById("OwnerRole");
                var label = document.getElementById("TaskTemplateOwner");
                var persist = document.getElementById("TaskTemplateOwnerPersist");
            } else {
                var id = document.getElementById("AssignedID");
                var role = document.getElementById("AssignedRole");
                var label = document.getElementById("TaskTemplateAssigned");
                var persist = document.getElementById("TaskTemplateAssignedPersist");
            }
            var NoPML = false;
            var hiddenCb = document.getElementById('ConditionIsHidden');
            if(document.getElementById("IsCondition").checked && hiddenCb != null) {
                NoPML = hiddenCb.checked;
            }
            var queryString = '?'
                + 'loanid=' + <%=AspxTools.JsString(LoanID)%>
                + '&brokerid=' + <%=AspxTools.JsString(BrokerID)%>
                + '&IsBatch=' + true // skip security check
                + '&PermLevel=' + permLevel
                + '&IsTemplate=' + true // include roles for selection
                + '&Owner=' + owner
                + '&NoPML=' + NoPML
                + '&IsPipeline=' + true;
            showModal('/newlos/Tasks/RoleAndUserPicker.aspx' + queryString, null, null, null, function(args){
                if (args.OK) {
                    if (args.type == 'user') {
                        id.value = args.id;
                        role.value = '00000000-0000-0000-0000-000000000000';
                        label.innerText = args.name;
                        persist.value = args.name;
                        if (hiddenCb != null) {
                            if (args.pmlUser == 'True') {
                                hiddenCb.checked = false;
                                hiddenCb.disabled = true;
                            } else {
                                hiddenCb.disabled = false;
                            }
                        }
                    } else {
                        role.value = args.id;
                        id.value = '00000000-0000-0000-0000-000000000000';
                        label.innerText = args.name;
                        persist.value = args.name;
                    }
                    setTaskDirty();
                }
            });
        }

        function changeTrigger(TriggerLabel, TriggerName) {
            showModal('/LOAdmin/Task/LOAdminTriggerPicker.aspx'
                + '?brokerid=' + <%=AspxTools.JsString(BrokerID)%>, null, null, null, function(args){
                    if (args.OK) {
                        document.getElementById(TriggerLabel).innerText = args.triggerName;
                        document.getElementById(TriggerName).value = args.triggerName;
                    }
                });
        }

        function clearTrigger(TriggerLabel, TriggerName) {
            document.getElementById(TriggerLabel).innerText = 'None';
            document.getElementById(TriggerName).value = '';
        }

        function changeResolutionDateSetter() {
            showModal("/newlos/Tasks/DateCalculator.aspx"
                + "?fieldId=" + document.getElementById('ResolutionDateSetterFieldId').value
                + "&IsResolutionDateSetter=t"
                , null, null, null, function(args){
                    if (args.OK) {
                        document.getElementById('ResolutionDateSetter').innerText = args.fieldDescription;
                        document.getElementById('ResolutionDateSetterFieldId').value = args.fieldId;
                    }
                }, {hideCloseButton:true});
        }

        function clearResolutionDateSetter() {
            document.getElementById('ResolutionDateSetter').innerText = 'None';
            document.getElementById('ResolutionDateSetterFieldId').value = '';
        }

        function disabledSubmitButtons() {
            disableButton('m_Close');
            disableButton('m_Resolve');
            disableButton('m_Save');
            disableButton('m_Cancel');
        }

        function disableButton(id) {
            if (document.getElementById(id) != null) {
                document.getElementById(id).disabled = true;
            }
        }

        function setTaskDirty() {
            taskIsDirty = true;
        }

        function confirmClose() {
            if (!taskIsDirty) {
                onClosePopup();
            }
            else if (confirm('Close task editor? Any unsaved changes will be lost.')) {
                onClosePopup();
            }
        }

        function closePage(taskId) { // This is called by the codebehind in a ClientScriptBlock
            var args = window.dialogArguments || {};
            if (typeof(args) !== 'undefined') {
                args.taskId = taskId;
                args.OK = true;
            }
            attemptTaskListRefresh();
            onClosePopup(args);
        }
    -->
    </script>
    <form id="form1" runat="server">
        <asp:HiddenField ID="AssignedRole" runat="server" Value="00000000-0000-0000-0000-000000000000" />
        <asp:HiddenField ID="AssignedID" runat="server" Value="00000000-0000-0000-0000-000000000000" />
        <asp:HiddenField ID="TaskTemplateAssignedPersist" runat="server"/>

        <asp:HiddenField ID="OwnerRole" runat="server" Value="00000000-0000-0000-0000-000000000000" />
        <asp:HiddenField ID="OwnerID" runat="server" Value="00000000-0000-0000-0000-000000000000" />
        <asp:HiddenField ID="TaskTemplateOwnerPersist" runat="server"/>

        <asp:HiddenField ID="DueDateOffset" runat="server" />
        <asp:HiddenField ID="DueDateCalcField" runat="server" />
        <asp:HiddenField ID="DueDateCalcDescriptionPersist" runat="server" />

        <asp:HiddenField ID="PermissionLevelID" runat="server" />
        <asp:HiddenField ID="PermissionDescription" runat="server" />
        <asp:HiddenField ID="TaskPermissionPersist" runat="server" />

        <asp:HiddenField ID="StatusResolved" runat="server" />

        <asp:HiddenField ID="TriggerName" runat="server" />

        <asp:HiddenField ID="ResolutionBlockTriggerName" runat="server" />

        <asp:HiddenField ID="AutoResolveTriggerName" runat="server" />

        <asp:HiddenField ID="AutoCloseTriggerName" runat="server" />

        <asp:HiddenField ID="ResolutionDateSetterFieldId" runat="server" />

        <asp:HiddenField ID="SavedVersion" runat="server" />
        <table cellSpacing="0" cellPadding="2" width="100%">
            <tr>
                <td class="FormTableSubheader" colspan="4" style="padding:3px; height: 2.5em">
                    <ml:EncodedLabel ID="m_Header" runat="server"></ml:EncodedLabel>
                </td>
            </tr>
            <tr>
                <td class="SubHeader" valign="top">
                    Subject
                    <img id="SubjectR" src="~/images/error_icon_right.gif" alt="Required" visible="false" runat="server" />
                </td>
                <td colspan="3">
                    <asp:TextBox ID="Subject" Width="100%" runat="server" TextMode="MultiLine" Rows="3" onchange="setTaskDirty();"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="SubHeader" nowrap>
                    Creation Trigger
                </td>
                <td>
                    <ml:EncodedLabel ID="TriggerLabel" runat="server"></ml:EncodedLabel>&nbsp;
                    <a onclick="changeTrigger('TriggerLabel', 'TriggerName');">change</a>
                </td>
                <td class="SubHeader">
                    Frequency
                </td>
                <td>
                    <asp:RadioButtonList ID="FrequencyList" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Single Use" Value="SingleUse"></asp:ListItem>
                        <asp:ListItem Text="Recurring" Value="Recurring"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="SubHeader" nowrap>
                    Resolution Block Trigger
                </td>
                <td>
                    <ml:EncodedLabel ID="ResolutionBlockTriggerLabel" runat="server"></ml:EncodedLabel>&nbsp;
                    <a onclick="changeTrigger('ResolutionBlockTriggerLabel', 'ResolutionBlockTriggerName');">change</a>
                    <a onclick="clearTrigger('ResolutionBlockTriggerLabel', 'ResolutionBlockTriggerName');">clear</a>
                </td>
                <td class="SubHeader">
                    Resolution Date Setter
                </td>
                <td>
                    <ml:EncodedLabel ID="ResolutionDateSetter" runat="server"></ml:EncodedLabel>&nbsp;
                    <a href="#" onclick="changeResolutionDateSetter();">change</a>
                    <a href="#" onclick="clearResolutionDateSetter();">clear</a>
                </td>
            </tr>
            <tr>
                <td class="SubHeader" valign="top">
                    Resolution Denial Message
                </td>
                <td colspan="3">
                    <asp:TextBox ID="ResolutionDenialMessage" TextMode="MultiLine" Rows="2" Width="100%" runat="server" onchange="setTaskDirty();"></asp:TextBox>
                </td>
            </tr>
            <tr id="TemplateRow" runat="server">
                <td class="SubHeader" nowrap>
                    Auto-Resolve Trigger
                </td>
                <td>
                    <ml:EncodedLabel ID="AutoResolveTriggerLabel" runat="server"></ml:EncodedLabel>&nbsp;
                    <a onclick="changeTrigger('AutoResolveTriggerLabel', 'AutoResolveTriggerName');">change</a>
                    <a onclick="clearTrigger('AutoResolveTriggerLabel', 'AutoResolveTriggerName');">clear</a>
                </td>
                <td class="SubHeader">
                    To Be Assigned To
                </td>
                <td>
                    <ml:EncodedLabel ID="TaskTemplateAssigned" runat="server"></ml:EncodedLabel>&nbsp;
                    <a href="#" onclick="changeAssigned('templateAssign');">change</a>
                </td>
            </tr>
            <tr>
                <td class="SubHeader" nowrap>
                    Auto-Close Trigger
                </td>
                <td>
                    <ml:EncodedLabel ID="AutoCloseTriggerLabel" runat="server"></ml:EncodedLabel>&nbsp;
                    <a onclick="changeTrigger('AutoCloseTriggerLabel', 'AutoCloseTriggerName');">change</a>
                    <a onclick="clearTrigger('AutoCloseTriggerLabel', 'AutoCloseTriggerName');">clear</a>
                </td>
                <td class="SubHeader">
                    To Be Owned By
                </td>
                <td>
                    <ml:EncodedLabel ID="TaskTemplateOwner" runat="server"></ml:EncodedLabel>&nbsp;
                    <a href="#" onclick="changeAssigned('templateOwner');">change</a>
                </td>
            </tr>
            <tr>
                <td class="SubHeader">
                    Due Date
                    <img id="DueDateR" src="~/images/error_icon_right.gif" alt="Required" visible="false" runat="server" />
                </td>
                <td>
                    <a id="dueDateCalculation" runat="server" href="#" onclick="dateCalculationPopup();">calculate</a>
                    <ml:EncodedLabel ID="DueDateCalcDescription" runat="server"></ml:EncodedLabel>
                </td>
                <td class="SubHeader">
                    Task Permission
                </td>
                <td>
                    <ml:EncodedLabel ID="TaskPermission" runat="server"></ml:EncodedLabel>&nbsp;
                    <a href="#" onclick="alert(document.getElementById('PermissionDescription').value);return false;">?</a>
                    <a id="changePermissionLevel" runat="server" href="#" onclick="permissionPopup();">change</a>
                </td>
            </tr>
            <tr id="ConditionRow1" runat="server">
                <td colspan="4">
                    <hr />
                </td>
            </tr>
            <tr id="ConditionRow5" runat="server">
                <td class="SubHeader">
                    Is a Condition
                </td>
                <td colspan="3" style="width:10px">
                    <input id="IsCondition" type="checkbox" runat="server" onclick="displayConditionFields();setTaskDirty();" NoHighlight />
                </td>
            </tr>
            <tr id="ConditionRow2" runat="server">
                <td class="SubHeader">
                    Category
                </td>
                <td colspan="3" class="FieldLabel">
                    <asp:DropDownList ID="ConditionCategories" onchange="setTaskDirty();" runat="server"></asp:DropDownList>
                    <ml:EncodedLabel ID="IsHiddenLabel" runat="server" Text="Hidden"></ml:EncodedLabel>
                    <asp:CheckBox ID="ConditionIsHidden" onclick="setTaskDirty();" runat="server" />
                    Required Document Type :  <uc:DocTypePicker runat="server" ID="CondDocType" />

                </td>
            </tr>
            <tr id="ConditionRow3" runat="server">
                <td class="SubHeader" valign="top">
                    Internal Notes
                </td>
                <td colspan="3">
                    <asp:TextBox ID="ConditionInternalNotes" onchange="setTaskDirty();" TextMode="MultiLine" Rows="6" Width="100%" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr id="ConditionRow4" runat="server">
                <td colspan="4">
                    <hr />
                </td>
            </tr>
            <tr id="CommentsRow" runat="server">
                <td class="SubHeader" valign="top">
                    Comments
                </td>
                <td colspan="3">
                    <asp:TextBox ID="m_Comments" TextMode="MultiLine" Rows="6" Width="100%" runat="server" onchange="setTaskDirty();"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <ml:EncodedLabel ID="m_errorMessage" runat="server" ForeColor="Red"></ml:EncodedLabel>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button ID="m_Save" Text="OK" runat="server" OnCommand="OnSaveClicked" CommandArgument="SaveTask" Width="100px" UseSubmitBehavior="false" OnClientClick="disabledSubmitButtons();" />
                    <input id="m_Cancel" type="button" value="Cancel" onclick="confirmClose();" style="width:100px" />
                </td>
            </tr>
        </table>
        <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
    </form>
</body>
</html>

