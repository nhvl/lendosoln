﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

using System.Web.Services;
using System.Web.Script.Services;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Task
{
    public partial class LOAdminTaskListService : MinimalPage
    {
        
        public const int TASKLIST_EXPORT_TO_PDF_CACHE_TIME_IN_MINUTES = 10;

        [WebMethod]
        public static object DeleteTasks(string brokerID, string taskIDs)
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            ForEachServiceTask(brokerID, taskIDs, (task) =>
            {
                TaskTriggerTemplate.Delete(task.BrokerId, task.AutoTaskTemplateId);
            });
            return null;
        }

        [WebMethod]
        public static object Assign(string brokerID, string taskIDs, Guid assignmentId, string type)
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            ForEachServiceTask(brokerID, taskIDs, (task) =>
            {
                if (type == "user")
                {
                    throw new CBaseException(ErrorMessages.Generic, "User assignment not implemented");
                    //task.TaskAssignedUserId = assignmentId;
                    //task.ToBeAssignedRoleId = Guid.Empty;
                }
                else
                {
                    //task.TaskAssignedUserId = Guid.Empty;
                    task.ToBeAssignedRoleId = assignmentId;
                }
                task.Save();
            });
            return null;
        }

        [WebMethod]
        public static object SetDueDate(string brokerID, string taskIDs, string dateField, int offset)
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            ForEachServiceTask(brokerID, taskIDs, (task) =>
            {
                task.DueDateCalculationOffset = offset;
                task.DueDateCalculatedFromField = dateField;
                task.Save();
            });
            return null;
        }

        [WebMethod]
        public static object ExportToPDF(string brokerID, string taskIDs)
        {
            // Write the taskIDs to cache
            string cacheId = AutoExpiredTextCache.AddToCache(taskIDs, TimeSpan.FromMinutes(TASKLIST_EXPORT_TO_PDF_CACHE_TIME_IN_MINUTES));

            var pdfInstance = new LendersOffice.Pdf.CLOAdminTaskListPDF(); // Create an instance just to get its name? Hmm. There should be a better way to do this.

            string filepath = string.Format(
                "{0}/pdf/{1}.aspx?brokerid={2}&cacheId={3}",
                DataAccess.Tools.VRoot,
                pdfInstance.Name,
                brokerID.ToString(),
                cacheId
            );
            return new { filepath = filepath };
        }

        private static void ForEachServiceTask(string brokerID, string taskIDs, Action<TaskTriggerTemplate> Operation)
        {
            // Parse out taskIDs and brokerID
            var taskIDList = taskIDs.Split(',');
            var brokerGUID = new Guid(brokerID);
            foreach (var taskID in taskIDList)
            {
                var parsedTaskID = int.Parse(taskID);
                var task = TaskTriggerTemplate.GetTaskTriggerTemplate(brokerGUID, parsedTaskID);
                Operation(task); // Do action
            }
        }
    }
}
