﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOfficeApp.newlos.Underwriting.Conditions;
using DataAccess;
using EDocs;

namespace LendersOfficeApp.LOAdmin.Task
{
    public partial class LOAdminTaskViewer : LendersOffice.Admin.SecuredAdminPage
    {
        protected int TaskID
        {
            get { return int.Parse(RequestHelper.GetSafeQueryString("taskid")); }
        }

        protected Guid BrokerID
        {
            get { return new Guid(RequestHelper.GetSafeQueryString("BrokerID")); }
        }

        protected BrokerUserPrincipal BrokerUser
        {
            get
            {
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
                //if (null == principal)
                //    RequestHelper.Signout();

                return principal;
            }
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Task Viewer";

            // Dummy task
            DateTime? now = DateTime.Now;
            Guid? roleId = Guid.Empty;
            int? ccId = 123;

            var dummyTask = new
            {
                AutoTaskTemplateId = 123,
                BrokerId = BrokerID,
                Subject = "Hello subject",
                TriggerName = "nunyabizness",
                Frequency = E_AutoTaskFrequency.SingleUse,
                ToBeAssignedRoleId = Guid.Empty,
                ToBeOwnedByRoleId = Guid.Empty,
                DueDateCalculatedFromField = now.Value,
                TaskPermissionLevelId = "TPM",
                IsConditionTemplate = false,
                ConditionCategoryId = ccId,
                Comments = "hello world",

                DueDateFriendlyDescription = "tomorrow will be a very short day that is shorter than 3 days long okay"
            };

            //var task = dummyTask;

            TaskTriggerTemplate task;
            
            task = TaskTriggerTemplate.GetTaskTriggerTemplate(BrokerID, TaskID);
            try
            {
                task = TaskTriggerTemplate.GetTaskTriggerTemplate(BrokerID, TaskID);
            }
            catch (TaskNotFoundException)
            {
                DisplayError("The selected task does not exist.");
                return;
            }

            m_Header.Text = (task.IsConditionTemplate ? "Condition " : "Task ") + task.AutoTaskTemplateId;

            Subject.Text = task.Subject;
            TemplateOwner.Text = task.OwnerFullName;
            TemplateAssigned.Text = task.AssignedUserFullName;
            Trigger.Text = task.TriggerName;
            Frequency.Text = task.Frequency.ToString();
            
            //DueDateCalcDescription.Text = task.DueDateDescription(true);
            History.Text = task.Comments;
            DueDate.Text = task.DueDateFriendlyDescription;

            if (task.IsConditionTemplate)
            {
                ConditionCategoryChoice.Text = task.ConditionCategoryId_rep;
                ReqCondType.Text = task.CondRequiredDocTypeRep;
            }
            //ConditionInternalNotes.Text = task.;
            m_conditionSection.Visible = task.IsConditionTemplate;
            
            TaskPermission.Text = task.PermissionLevelName;
            PermissionDescription.Value = task.PermissionLevelDescription;

            ResolutionBlockTrigger.Text = string.IsNullOrEmpty(task.ResolutionBlockTriggerName) ? "None" : task.ResolutionBlockTriggerName;
            ResolutionDenialMessage.Value = task.ResolutionDenialMessage;
            ResolutionDateSetter.Text = task.GetResolutionDateSetterFieldDescription();

            AutoResolveTriggerName.Text = string.IsNullOrEmpty(task.AutoResolveTriggerName) ? "None" : task.AutoResolveTriggerName;
            AutoCloseTriggerName.Text = string.IsNullOrEmpty(task.AutoCloseTriggerName) ? "None" : task.AutoCloseTriggerName;

            ClientScript.GetPostBackEventReference(this, "");
            
        }

        protected override void OnInit(EventArgs e)
        {
            int l = VirtualRoot.Length;
            string url = Request.Path.Substring(l, Request.Path.Length - l - 5) + "Service.aspx";
            //RegisterService("loanedit", url);
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        private void DisplayError(string msg)
        {
            pnlError.Visible = true;
            pnlTaskViewer.Visible = false;
            lblMessage.Text = msg;
        }
    }
}
