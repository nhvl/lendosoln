﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;
using LendersOffice.Security;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.LOAdmin.Task
{
    public partial class ConditionCategoryEditor : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        private PermissionLevel[] BrokerPermissionLevels;
        private PermissionLevel[] AllBrokerPermissionLevels; 

        private Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("BrokerId");
            }
        }
        
        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_4_2;
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            if (IsPostBack)
            {
                return;
            }

            IBrokerId.Value = BrokerId.ToString();
            AllBrokerPermissionLevels = PermissionLevel.RetrieveAll(BrokerId).ToArray();
            BrokerPermissionLevels = AllBrokerPermissionLevels.Where(level => level.IsActive && level.IsAppliesToConditions).ToArray();
            ConditionCategory[] categories = ConditionCategory.GetCategories(BrokerId).OrderBy(p=>p.Category).ToArray();
            ConditionCategories.DataSource = categories;
            ConditionCategories.DataBind();

            PopulateLevelDropDownList(NewDefaultLevel);

            AusDefaultConditionCategory.DataSource = categories;
            AusDefaultConditionCategory.DataTextField = "Category";
            AusDefaultConditionCategory.DataValueField = "Id";
            AusDefaultConditionCategory.DataBind();

            MigrateFrom.DataSource = categories;
            MigrateFrom.DataTextField = "Category";
            MigrateFrom.DataValueField = "Id";
            MigrateFrom.DataBind();


            MigrateTo.DataSource = categories;
            MigrateTo.DataTextField = "Category";
            MigrateTo.DataValueField = "Id";
            MigrateTo.DataBind();


            var levels = from p in BrokerPermissionLevels
                           select new
                           {
                               Id = p.Id,
                               Name = p.Name
                           };

            string script = "var Levels=" + ObsoleteSerializationHelper.JavascriptJsonSerialize(levels) + ";";
            ClientScript.RegisterClientScriptBlock(typeof(ConditionCategoryEditor),"levels", script, true);

            if (categories.Count() > 0)
            {
                ConditionCategories.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            BrokerDB db = BrokerDB.RetrieveById(BrokerId);
            if (db.AusImportDefaultConditionCategoryId.HasValue)
            {
                var listItem = AusDefaultConditionCategory.Items.FindByValue(db.AusImportDefaultConditionCategoryId.Value.ToString());
                listItem.Selected = true;
            }
                                          
           
        }

        protected void ConditionCategories_OnRowCreated( object sender, GridViewRowEventArgs args)
        {
            if (args.Row.RowType != DataControlRowType.DataRow)
            {
                return;
            }

            ConditionCategory conditionCategory = (ConditionCategory)args.Row.DataItem;
            EncodedLiteral category = (EncodedLiteral)args.Row.FindControl("Category");
            EncodedLiteral permissionLevelText = (EncodedLiteral)args.Row.FindControl("PermissionLevelText");
            EncodedLiteral isDisplayAtTopText = (EncodedLiteral)args.Row.FindControl("IsDisplayAtTopText");
            DropDownList permissionLevels = (DropDownList)args.Row.FindControl("PermissionLevels");
            HiddenField id = (HiddenField)args.Row.FindControl("Id");

            category.Text = conditionCategory.Category;
            id.Value = conditionCategory.Id.ToString();
            permissionLevelText.Text = AllBrokerPermissionLevels.First(p => p.Id == conditionCategory.DefaultTaskPermissionLevelId).Name;
            PopulateLevelDropDownList(permissionLevels);
            isDisplayAtTopText.Text = conditionCategory.IsDisplayAtTop ? "Yes" : "No";
        }

        private void PopulateLevelDropDownList(DropDownList list)
        {
            list.DataSource = BrokerPermissionLevels;
            list.DataValueField = "Id";
            list.DataTextField = "Name";
            list.DataBind();
        }
                                          
        [WebMethod]
        public static object UpdateCategory(int id, string category, int permissionLevel, bool isDisplayAtTop, Guid brokerId)
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            if (string.IsNullOrEmpty(category))
            {
                return new { Error = "Please enter a category." };
            }
            ConditionCategory conditionCategory = id >= 0 ? ConditionCategory.GetCategory(brokerId, id) : new ConditionCategory(brokerId);

            conditionCategory.Category = category;
            conditionCategory.DefaultTaskPermissionLevelId = permissionLevel;
            conditionCategory.IsDisplayAtTop = isDisplayAtTop;

            conditionCategory.Save();

            return new { Id = conditionCategory.Id, Category = conditionCategory.Category, Level = conditionCategory.DefaultTaskPermissionLevelId, IsDisplayAtTop = conditionCategory.IsDisplayAtTop };
        }

        [WebMethod]
        public static object DeleteCategory(int id, Guid brokerId)
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            try
            {
                ConditionCategory.Delete(brokerId, id);
            }
            catch (SqlException e)
            {
                string errorMessage;
                if (e.Message.IndexOf("conflicted with the REFERENCE", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    errorMessage = "There are existing conditions with this category. Every such condition must be migrated to another category before this one can be deleted.";

                }
                else
                {
                    errorMessage = "Unknown error :" + e.Message;
                }

                return new { Error = errorMessage };
            }
            
            return true;
        }

        [WebMethod]
        public static bool SetAusDefaultCategory(int id, Guid brokerId)
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            var brokerDb = BrokerDB.RetrieveById(brokerId);
            brokerDb.AusImportDefaultConditionCategoryId = id;
            brokerDb.Save();
            return true;
        }

        [WebMethod]
        public static object ChangeCategories(Guid brokerId, int src, int dst)
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            var sqlParameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@CondCategorySrc", src),
                new SqlParameter("@CondCategoryDst", dst)
            };

            var affectedTaskRowsParameter = new SqlParameter("@AffectedTaskRows", SqlDbType.Int) { Direction = ParameterDirection.Output };
            var affectedCondChoiceRowsParameter = new SqlParameter("@AffectedConditionChoiceRows", SqlDbType.Int) { Direction = ParameterDirection.Output };

            sqlParameters.Add(affectedTaskRowsParameter);
            sqlParameters.Add(affectedCondChoiceRowsParameter);

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TASK_MigrateCategory", nRetry: 0, parameters: sqlParameters);

            return new
            {
                AffectedTaskRows = affectedTaskRowsParameter.Value,
                AffectedConditionChoiceRows = affectedCondChoiceRowsParameter.Value
            };
        }
    }
}
