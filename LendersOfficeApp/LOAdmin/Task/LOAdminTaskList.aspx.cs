﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Admin;
using LendersOfficeApp.newlos.Underwriting.Conditions;
using LendersOffice.Security;
using LendersOffice.AntiXss;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Text;

namespace LendersOfficeApp.LOAdmin.Task
{
    public partial class LOAdminTaskList : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        protected Guid LoanID = Guid.Empty;
        protected Guid BrokerID {
            get { return new Guid(RequestHelper.GetSafeQueryString("BrokerId")); }
        }
        protected Guid EmployeeID = Guid.Empty;
        protected Guid UserID = Guid.Empty;
        //private BrokerUserPrincipal BrokerUser = null;

        // Be sure to update these whenever you add/remove a column
        // They determine which columns get hidden when editing a template loan
        enum E_OpenResolvedTasksColumns
        {
            Checkbox, Task, CondCategoryId, Subject, Status, DueDate, FollowupDate,
            LastUpdated, AssignedTo, Owner
        }
        enum E_ClosedTasksColumns
        {
            Task, Subject, LastUpdated, Owner
        }

        protected override E_JqueryVersion GetJQueryVersion() => E_JqueryVersion._1_6_1;

        private readonly string OPENRESOLVED_GRIDVIEWNAME = "OpenResolved";
        private readonly string OPENRESOLVED_DEFAULTSORT = "TaskSubject ASC";
        private readonly string OPENRESOLVED_TASKFILTER = "";
        //private readonly string CLOSED_TASKFILTER = "";
        private const string CONDFILTER_ALL = "ANYONE";
        private const string CONDFILTER_COND = "CONDITIONS";
        private const string CONDFILTER_NONCOND = "NONCONDITIONS";
        private const string FILTER_ANYONE = "ANYONE";
        private const string FILTER_ME = "ME";
        private const string FILTER_ROLE = "ROLE";
        private const string FILTER_USERID = "USERID";
        private const string FILTER_NOBODY = "NOBODY";
        private const int FILTER_DROPDOWN_LENGTH = 40;

        /// <summary>
        /// One cookie is stored per loan for each of the dropdown filters
        /// </summary>
        private const string FilterCookie = "SelectedFilter";
        private const string ConditionFilterCookie = "SelectedCondFilter";

        protected bool IsTemplate;

        protected EmployeeDB CurrentEmployee
        {
            get
            {
                EmployeeDB currentUserEmployee = new EmployeeDB(EmployeeID, BrokerID);
                currentUserEmployee.Retrieve();
                return currentUserEmployee;
            }
        }

        protected DataTable m_data;

        protected Dictionary<Guid, Guid> m_roleToUserMap; // <ROLEID, USERID> 

        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            RegisterJsScript("LQBPopup.js");

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            // Populate condition dropdown
            m_conditionFilter.Items.Add(new ListItem("all tasks", CONDFILTER_ALL));
            m_conditionFilter.Items.Add(new ListItem("conditions", CONDFILTER_COND));
            m_conditionFilter.Items.Add(new ListItem("non-conditions", CONDFILTER_NONCOND));

            // Create DataTable
            m_data = new DataTable();

            m_data.Columns.Add(new DataColumn("TaskId"));
            m_data.Columns.Add(new DataColumn("TaskIdLength"));
            m_data.Columns.Add(new DataColumn("TriggerName"));
            m_data.Columns.Add(new DataColumn("CondCategoryId"));
            m_data.Columns.Add(new DataColumn("TaskIsCondition"));
            m_data.Columns.Add(new DataColumn("TaskSubject"));
            m_data.Columns.Add(new DataColumn("TaskDueDateCalcDisplay"));
            m_data.Columns.Add(new DataColumn("TaskDueDateCalc"));
            m_data.Columns.Add(new DataColumn("TaskLastModifiedDate", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskAssignedUserId"));
            m_data.Columns.Add(new DataColumn("AssignedUserFullName"));
            m_data.Columns.Add(new DataColumn("OwnerFullName"));
            m_data.Columns.Add(new DataColumn("TaskDueDateComparison", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskLastModifiedDateComparison", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("CondRequiredDocTypeRep"));

            if (IsTemplate)
            {
                m_title.Text = "Tasks Template";

                m_openResolvedTasksGV.Columns[(int)E_OpenResolvedTasksColumns.Status].Visible = false;
                m_openResolvedTasksGV.Columns[(int)E_OpenResolvedTasksColumns.FollowupDate].Visible = false;

            }

            this.PageID = "NewTaskList";
            this.PageTitle = "New Task List";
        }

        protected void PagePostBack(object sender, EventArgs e)
        {

        }

        #region Page Load
        protected void PageLoad(object sender, EventArgs e)
        {
            //DateTime starttime = DateTime.Now;
            if (Page.IsPostBack) // Only every subsequent load of this page
            {
                PagePostBack(sender, e);
            }
            else // Only the first load of this page
            {
                try
                {
                    // Matches the selected value to the cookie value
                    m_assignmentFilter.SelectedValue = Request.Cookies[FilterCookie].Value;
                }
                catch (IndexOutOfRangeException) // This value is not in the list : possibly the user got unassigned
                {
                    m_assignmentFilter.SelectedValue = FILTER_ANYONE; // So just set it to ANYONE
                }
                catch (NullReferenceException) // The cookie doesn't exist
                {
                    m_assignmentFilter.SelectedValue = FILTER_ANYONE;
                }

                try
                {
                    // Matches the selected value to the cookie value
                    m_conditionFilter.SelectedValue = Request.Cookies[ConditionFilterCookie].Value;
                }
                catch (IndexOutOfRangeException) // This value is not in the list : how strange! go back to the default
                {
                    m_conditionFilter.SelectedValue = CONDFILTER_ALL;
                }
                catch (NullReferenceException) // The cookie doesn't exist
                {
                    m_conditionFilter.SelectedValue = CONDFILTER_ALL;
                }


            }
            // Every single load

            // Populate gridviews
            var ds = GetPopulatedDataSet();

            m_openResolvedTasksGV_Load(ds);

            //Tools.LogInfo("Total load time: " + (DateTime.Now - starttime).TotalMilliseconds + "ms");

            ImportExportTools.Visible = HasPermissionToDo(E_InternalUserPermissions.IsDevelopment);
        }

        protected void m_openResolvedTasksGV_Load(DataSet ds)
        {
            // For refactoring considerations, here's an alternate design:
            // Create a subclass of GridView
            // Create custom fields instead of using templatefields

            // And here's the design we went with

            DataView openResolvedTasksView = new DataView(ds.Tables[0]);
            // Filtering
            openResolvedTasksView.RowFilter = PopulateRowFilter(OPENRESOLVED_TASKFILTER);
            // Sorting
            try
            {
                openResolvedTasksView.Sort = PopulateSortUpdateViewState(OPENRESOLVED_GRIDVIEWNAME, OPENRESOLVED_DEFAULTSORT);
            }
            catch (IndexOutOfRangeException)
            {
                openResolvedTasksView.Sort = OPENRESOLVED_DEFAULTSORT;
            }

            GV_Load(
                m_openResolvedTasksGV,
                OPENRESOLVED_GRIDVIEWNAME,
                openResolvedTasksView
            );
        }

        private string PopulateRowFilter(string defaultFilter)
        {
            try
            {
                return BuildRowFilter(defaultFilter, m_assignmentFilter.SelectedValue, m_conditionFilter.SelectedValue);
            }
            catch (FormatException) // It's not a Guid
            {
                return defaultFilter;
            }
            catch (IndexOutOfRangeException)
            {
                return defaultFilter;
            }
            catch (NullReferenceException)
            {
                return defaultFilter;
            }
        }

        private string PopulateSortUpdateViewState(string gvName, string defaultSort)
        {
            string sort;
            try
            {
                sort = string.Format(
                    "{0} {1}",
                    Request.Cookies[gvName + "SortExpression"].Value,
                    Request.Cookies[gvName + "SortDirection"].Value
                );

                ViewState[gvName + "SortExpression"] = Request.Cookies[gvName + "SortExpression"].Value;
                ViewState[gvName + "SortDirection"] = Request.Cookies[gvName + "SortDirection"].Value;
            }
            catch (NullReferenceException) // The cookies DO NOT EXIST! WHERE DID THE COOKIES GO?! Were they ever here in the first place?
            {
                sort = defaultSort;
            }
            return sort;
        }

        private void GV_Load(GridView gv, string gvName, DataView dv)
        {

            gv.DataSource = dv;
            gv.DataBind();

            // Can only do this after DataBind
            if (Request.Cookies[gvName + "SortedColumn"] != null && Request.Cookies[gvName + "SortDirection"] != null)
            {
                AddArrowToColumn(
                    gv,
                    Request.Cookies[gvName + "SortedColumn"].Value,
                    Request.Cookies[gvName + "SortDirection"].Value
                );
            }

        }
        #endregion

        protected void PopulateAssignmentDropdown(object sender, EventArgs e)
        {
            m_assignmentFilter.Items.Add(new ListItem("Anyone", FILTER_ANYONE));
            m_assignmentFilter.Items.Add(new ListItem("Me", FILTER_ME));

            var roles = LendersOffice.ObjLib.Task.Task.GetRoleUsersForLoan(BrokerID, LoanID);
            m_roleToUserMap = new Dictionary<Guid, Guid>(roles.Count);
            foreach (var role in roles) // Item1: Name, Item2: RoleId, Item3: UserId
            {
                string displayText = role.Item1.Length > FILTER_DROPDOWN_LENGTH ? role.Item1.Substring(0, FILTER_DROPDOWN_LENGTH) : role.Item1;
                string listValue = string.Format("{0}:{1}", FILTER_ROLE, role.Item2.ToString()); // Store the roleID
                ListItem li = new ListItem(displayText, listValue);
                li.Attributes.Add("title", role.Item1);
                m_assignmentFilter.Items.Add(li);
                m_roleToUserMap.Add(role.Item2, role.Item3);
            }

            var otherUsers = LendersOffice.ObjLib.Task.Task.GetUnassignedUsersWithTasksForLoan(BrokerID, LoanID);
            foreach (var user in otherUsers) // Item1: Name, Item2: UserId
            {
                string displayText = user.Item1.Length > FILTER_DROPDOWN_LENGTH ? user.Item1.Substring(0, FILTER_DROPDOWN_LENGTH) : user.Item1;
                string listValue = string.Format("{0}:{1}", FILTER_USERID, user.Item2.ToString()); // Store the userID
                ListItem li = new ListItem(displayText, listValue);
                li.Attributes.Add("title", user.Item1);
                m_assignmentFilter.Items.Add(li);
            }
            m_assignmentFilter.Items.Add(new ListItem("Nobody", FILTER_NOBODY));
        }

        private DataSet GetPopulatedDataSet()
        {
            //DateTime spstarttime = DateTime.Now;

            DateTime? now = DateTime.Now;
            Guid? roleId = Guid.Empty;

            var tasks = TaskTriggerTemplate.GetTaskTriggerTemplatesByBroker(BrokerID);

            foreach (var task in tasks)
            {
                var row = m_data.NewRow();

                row["TaskId"] = task.AutoTaskTemplateId;
                row["TaskIdLength"] = task.AutoTaskTemplateId.ToString().Length;
                //row["TaskRowVersion"] = task.TaskRowVersion;
                row["TriggerName"] = task.TriggerName;
                row["CondCategoryId"] = task.ConditionCategoryId_rep;
                row["TaskIsCondition"] = task.IsConditionTemplate;

                row["TaskSubject"] = task.Subject;
                //row["TaskStatus"] = task.Status;
                row["CondRequiredDocTypeRep"] = task.CondRequiredDocTypeRep;
                row["TaskDueDateCalcDisplay"] = "inherit";
                row["TaskDueDateCalc"] = task.DueDateFriendlyDescription;
                row["TaskDueDateComparison"] = DateTime.MaxValue; // Since we can't put in actual dates, this will always be the same

                row["TaskLastModifiedDate"] = task.LastModifiedDate.Date;
                row["TaskLastModifiedDateComparison"] = task.LastModifiedDate.Date;

                row["TaskAssignedUserId"] = task.AssignedUserId;
                row["AssignedUserFullName"] = task.AssignedUserFullName;
                row["OwnerFullName"] = task.OwnerFullName;

                m_data.Rows.Add(row);
            }
            var ds = new DataSet();
            ds.Tables.Add(m_data);

            //Tools.LogInfo("Row creation time: " + (DateTime.Now - rowcreatestarttime).TotalMilliseconds + "ms");

            return ds;
        }

        #region Filtering
        protected void m_openResolvedTasksGV_SelectedFilterChanged(object sender, EventArgs e)
        {
            RequestHelper.StoreToCookie(FilterCookie, m_assignmentFilter.SelectedItem.Value, SmallDateTime.MaxValue);
        }

        protected void m_openResolvedTasksGV_SelectedCondFilterChanged(object sender, EventArgs e)
        {
            RequestHelper.StoreToCookie(ConditionFilterCookie, m_conditionFilter.SelectedItem.Value, SmallDateTime.MaxValue);
        }

        protected string BuildRowFilter(string defaultFilter, string selectedValue, string selectedCondValue)
        {
            List<string> filterList = new List<string>(4);
            if ((string.IsNullOrEmpty(selectedValue) || selectedValue == FILTER_ANYONE)
                && (string.IsNullOrEmpty(selectedCondValue) || selectedCondValue == CONDFILTER_ALL))
            {
                m_filterErrorMessage.Text = "";
                return defaultFilter; // Don't need to filter
            }

            if (!string.IsNullOrEmpty(defaultFilter))
            {
                filterList.Add(defaultFilter);
            }

            // determine assignment filter
            if (!string.IsNullOrEmpty(selectedValue) && selectedValue != FILTER_ANYONE)
            {
                string[] splitString = selectedValue.Split(':');
                string filterType = splitString[0];
                Guid filterId;
                try
                {
                    filterId = new Guid(splitString[splitString.Length - 1]); // The guid is the last item, if we have a role or user
                }
                catch (FormatException)
                {
                    filterId = Guid.Empty;
                }
                Guid selectedUserId;
                switch (filterType)
                {
                    case FILTER_ME:
                        selectedUserId = UserID;
                        m_filterErrorMessage.Text = "";
                        break;
                    case FILTER_NOBODY:
                        selectedUserId = Guid.Empty;
                        m_filterErrorMessage.Text = "";
                        break;
                    case FILTER_ROLE:
                        selectedUserId = m_roleToUserMap[filterId];
                        m_filterErrorMessage.Text = "";
                        break;
                    case FILTER_USERID:
                        selectedUserId = filterId;
                        m_filterErrorMessage.Text = "";
                        break;
                    default:
                        Tools.LogError("Task List: unhandled filter type!");
                        selectedUserId = Guid.Empty;
                        m_filterErrorMessage.Text = "Could not find user id! Please pick another filter.";
                        break;
                }
                filterList.Add("TaskAssignedUserId = '" + selectedUserId.ToString() + "'");
            }
            // determine condition filter
            if (!string.IsNullOrEmpty(selectedCondValue) && selectedCondValue != CONDFILTER_ALL)
            {
                string filterType = selectedCondValue;
                string selectedConditionStatus;
                switch (filterType)
                {
                    case CONDFILTER_COND:
                        selectedConditionStatus = "True";
                        break;
                    case CONDFILTER_NONCOND:
                        selectedConditionStatus = "False";
                        break;
                    default:
                        selectedConditionStatus = "False";
                        Tools.LogError("Task List: unhandled condition filter type!");
                        break;
                }
                filterList.Add("TaskIsCondition = '" + selectedConditionStatus + "'");
            }

            return string.Join(" AND ", filterList.ToArray());
        }
        #endregion

        #region Sorting
        protected void m_openResolvedTasksGV_Sorting(object sender, CommandEventArgs e)
        {
            SortOperation(sender, e, m_openResolvedTasksGV, "OpenResolved");
        }

        private void SortOperation(object sender, CommandEventArgs e, GridView gv, string gvName)
        {
            string[] commandArguments = ((string)e.CommandArgument).Split(' ');
            string field = commandArguments[0];
            string headerID = commandArguments[1];

            string sortDirection = GetSortDirection(
                field,
                ViewState[gvName + "SortExpression"] as string,
                ViewState[gvName + "SortDirection"] as string
            );
            ViewState[gvName + "SortExpression"] = field;
            ViewState[gvName + "SortDirection"] = sortDirection;

            string sortExpression;
            if (field == "TaskId") // We want to sort by length first for taskIds
            {
                sortExpression = string.Format("TaskIdLength {0}, TaskId {0}", sortDirection);
            }
            else
            {
                sortExpression = field + " " + sortDirection;
            }

            // This is where the actual sorting happens
            ((DataView)gv.DataSource).Sort = sortExpression;
            gv.DataBind();

            // Get rid of the arrow from the last sorted column
            string lastSortedColumn = ViewState[gvName + "SortedColumn"] as string;
            if (lastSortedColumn != null)
            {
                RemoveArrowFromColumn(gv, lastSortedColumn);
            }

            AddArrowToColumn(gv, headerID, sortDirection);
            ViewState[gvName + "SortedColumn"] = headerID;

            // Now we can store the latest sort in a cookie
            RequestHelper.StoreToCookie(gvName + "SortedColumn", headerID, SmallDateTime.MaxValue);
            RequestHelper.StoreToCookie(gvName + "SortExpression", field, SmallDateTime.MaxValue);
            RequestHelper.StoreToCookie(gvName + "SortDirection", sortDirection, SmallDateTime.MaxValue);
        }

        private void RemoveArrowFromColumn(GridView gridview, string headerID)
        {
            // This might happen if there are no tasks, or if we called RemoveArrowFromColumn at the wrong time
            if (gridview.HeaderRow == null)
            {
                return;
            }
            Label header = gridview.HeaderRow.FindControl(headerID) as Label;
            if (header != null)
            {
                header.Text = "";
            }
        }
        private void AddArrowToColumn(GridView gridview, string headerID, string sortDirection)
        {
            // This might happen if there are no tasks, or if we called AddArrowToColumn at the wrong time
            if (gridview.HeaderRow == null)
            {
                return; // die silently, we don't care
            }
            // Add the arrow to the sorted column
            Label header = gridview.HeaderRow.FindControl(headerID) as Label;
            if (header != null)
            {
                header.Text = sortDirection == "DESC" ? "▼" : "▲";
            }
        }

        /// <summary>
        /// Get the sort direction based on the example from
        /// http://msdn.microsoft.com/en-us/library/system.web.ui.webcontrols.gridview.sorting.aspx
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        private string GetSortDirection(string column, string lastSortExpression, string lastDirection)
        {
            string sortDirection = "ASC";

            if (lastSortExpression != null)
            {
                if (lastSortExpression == column)
                {
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }
            return sortDirection;
        }
        #endregion

        #region Datagrid Display

        /// <summary>
        /// Check if the date in question is today or before today, then return
        /// the necessary css.
        /// </summary>
        /// <returns></returns>
        private string GetDateWarningCss(object queryDate)
        {
            if (queryDate is DateTime)
            {
                DateTime date = ((DateTime)queryDate).Date;
                // If the date is today or before today, style it differently
                return date.CompareTo(DateTime.Now.Date) <= 0 ? "DiscDue" : "DiscDueNot";
                // We take care of the row using jQuery, in _init
            }
            return "";
        }
        #endregion

        protected void ImportXMLBtn_Click(object sender, EventArgs e)
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            if (!ImportXMLFileUpload.HasFile)
            {
                return;
            }
            var postedFile = ImportXMLFileUpload.PostedFile;
            byte[] file = new byte[postedFile.ContentLength];
            postedFile.InputStream.Read(file, 0, postedFile.ContentLength);
            var encoding = System.Text.Encoding.ASCII;
            var xmlString = encoding.GetString(file);
            ImportXML(BrokerID, xmlString);
        }

        /// <summary>
        /// Put this in tasktriggertemplate
        /// </summary>
        /// <param name="brokerId"></param>
        private void ImportXML(Guid brokerId, string xml)
        {
            var doc = XDocument.Parse(xml);
            var root = doc.Element("root");
            var tasks = root.Elements();
            bool matchPermissions = true;
            bool matchCategories = true;

            Dictionary<string, PermissionLevel> permissions = null;
            try
            {
                permissions = PermissionLevel.RetrieveAll(brokerId)
                                             .ToDictionary(p => p.Name);
            }
            catch (ArgumentException)
            {
                matchPermissions = false;
            }

            Dictionary<string, ConditionCategory> conditionCategories = null;
            try
            {
                conditionCategories = ConditionCategory.GetCategories(brokerId)
                                                       .ToDictionary(c => c.Category);
            }
            catch (ArgumentException)
            {
                matchCategories = false;
            }


            foreach (var taskXML in tasks)
            {
                var createdTask = new TaskTriggerTemplate(brokerId);
                createdTask.Subject = taskXML.Element("Subject").Value;
                createdTask.TriggerName = taskXML.Element("TriggerName").Value;
                createdTask.Frequency = (E_AutoTaskFrequency) Enum.Parse(typeof(E_AutoTaskFrequency), taskXML.Element("Frequency").Value);
                createdTask.LastModifiedDate = DateTime.Now;
                createdTask.AssignedUserId = new Guid(taskXML.Element("AssignedUserId").Value);
                if (!string.IsNullOrEmpty(taskXML.Element("ToBeAssignedRoleId").Value))
                {
                    createdTask.ToBeAssignedRoleId = new Guid(taskXML.Element("ToBeAssignedRoleId").Value);
                }
                createdTask.OwnerUserId = new Guid(taskXML.Element("OwnerUserId").Value);
                if (!string.IsNullOrEmpty(taskXML.Element("ToBeOwnedByRoleId").Value))
                {
                    createdTask.ToBeOwnedByRoleId = new Guid(taskXML.Element("ToBeOwnedByRoleId").Value);
                }
                createdTask.DueDateCalculatedFromField = taskXML.Element("DueDateCalculatedFromField").Value;
                createdTask.DueDateCalculationOffset = int.Parse(taskXML.Element("DueDateCalculationOffset").Value);
                
                // Match the permission name from the imported task to any matched permission for this broker
                if (matchPermissions && taskXML.Element("TaskPermissionLevelName") != null)
                {
                    string permissionName = taskXML.Element("TaskPermissionLevelName").Value;
                    if (permissions.ContainsKey(permissionName))
                    {
                        createdTask.TaskPermissionLevelId = permissions[permissionName].Id;
                    }
                }
                // Otherwise, use the default
                
                createdTask.IsConditionTemplate = bool.Parse(taskXML.Element("IsConditionTemplate").Value);
                createdTask.CondIsHidden = bool.Parse(taskXML.Element("CondIsHidden").Value);
                createdTask.CondInternalNotes = taskXML.Element("CondInternalNotes").Value;

                // Match the category name from the imported task to any matched permission for this broker
                if (matchCategories && taskXML.Element("ConditionCategoryName") != null)
                {
                    string categoryName = taskXML.Element("ConditionCategoryName").Value;
                    if (conditionCategories.ContainsKey(categoryName))
                    {
                        createdTask.ConditionCategoryId = conditionCategories[categoryName].Id;
                    }
                }
                // Otherwise, use the default

                createdTask.Comments = taskXML.Element("Comments").Value;

                createdTask.ResolutionBlockTriggerName = taskXML.Element("ResolutionBlockTriggerName").Value;
                createdTask.ResolutionDenialMessage = taskXML.Element("ResolutionDenialMessage").Value;
                createdTask.ResolutionDateSetterFieldId = taskXML.Element("ResolutionDateSetterFieldId").Value;

                createdTask.Save();
            }

            Response.Redirect(Request.RawUrl);
        }

        protected void ExportXMLBtn_Click(object sender, EventArgs e)
        {
            string xml = GetTasksAsXML(BrokerID);

            // Clear Response buffer
            Response.Clear();
            // Set ContentType to the ContentType of our file
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition", "attachment; filename=\"tasks.xml\"");
            // Write data out of database into Output Stream
            var encoding = new System.Text.ASCIIEncoding();
            var bytes = encoding.GetBytes(xml);
            Response.OutputStream.Write(bytes, 0, bytes.Length);
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Put this in tasktriggertemplate
        /// </summary>
        /// <param name="brokerId"></param>
        private string GetTasksAsXML(Guid brokerId)
        {
            var tasks = TaskTriggerTemplate.GetTaskTriggerTemplatesByBroker(brokerId);
            XElement root = new XElement("root");
            foreach (var task in tasks)
            {
                var xTask = new XElement("Task");
                xTask.Add(new XElement("AutoTaskTemplateId", task.AutoTaskTemplateId));
                xTask.Add(new XElement("BrokerId", task.BrokerId));
                xTask.Add(new XElement("Subject", task.Subject));
                xTask.Add(new XElement("TriggerName", task.TriggerName));
                xTask.Add(new XElement("Frequency", task.Frequency));
                xTask.Add(new XElement("AssignedUserId", task.AssignedUserId));
                xTask.Add(new XElement("ToBeAssignedRoleId", task.ToBeAssignedRoleId));
                xTask.Add(new XElement("OwnerUserId", task.OwnerUserId));
                xTask.Add(new XElement("ToBeOwnedByRoleId", task.ToBeOwnedByRoleId));
                xTask.Add(new XElement("DueDateCalculatedFromField", task.DueDateCalculatedFromField));
                xTask.Add(new XElement("DueDateCalculationOffset", task.DueDateCalculationOffset));
                
                xTask.Add(new XElement("TaskPermissionLevelId", task.TaskPermissionLevelId)); // Permission levels are unique to each broker
                xTask.Add(new XElement("TaskPermissionLevelName", task.PermissionLevelName)); // We can attempt to match by name

                xTask.Add(new XElement("IsConditionTemplate", task.IsConditionTemplate));
                xTask.Add(new XElement("CondIsHidden", task.CondIsHidden));
                xTask.Add(new XElement("CondInternalNotes", task.CondInternalNotes));
                
                xTask.Add(new XElement("ConditionCategoryId", task.ConditionCategoryId)); // Condition categories are unique to each broker
                xTask.Add(new XElement("ConditionCategoryName", task.ConditionCategoryId_rep)); // We can attempt to match by name

                xTask.Add(new XElement("Comments", task.Comments));

                xTask.Add(new XElement("ResolutionBlockTriggerName", task.ResolutionBlockTriggerName));
                xTask.Add(new XElement("ResolutionDenialMessage", task.ResolutionDenialMessage));
                xTask.Add(new XElement("ResolutionDateSetterFieldId", task.ResolutionDateSetterFieldId));

                root.Add(xTask);
            }
            return root.ToString();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
