﻿<%@
    Page
    Language="C#"
    AutoEventWireup="false"
    CodeBehind="LOAdminTaskList.aspx.cs"
    Inherits="LendersOfficeApp.LOAdmin.Task.LOAdminTaskList"
    MaintainScrollPositionOnPostback="true"
%>

<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.ObjLib.Task" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Reminders" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<%-- LOAdmin Auto-Task Template List --%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>TaskList</title>
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../../css/stylesheetnew.css" type="text/css" rel="stylesheet" />
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet" />

    <style type="text/css">
        .TasksTitle
        {
            padding:    5px;
            margin:     5px 0px 5px 0px;
        }
        #m_openResolvedTasksGV
        {
            width:      110.5em;
        }
        #TopButtons
        {
        	width:      100%;
        	position:   relative;
        	padding:    5px;
        	height:     2.5em;
        }
        #TopButtonsLeftSide
        {
        	white-space:nowrap;
        }
        #TopButtonsRightSide
        {
        	white-space:nowrap;
        	text-align: right;
        }
        #BatchButtons
        {
        	padding:    5px;
        }
        .AlignLeft
        {
        	text-align: left;
        }
        .CheckboxColumn
        {
        	width:      6em;
        }
        .TaskIDColumn
        {
            width:      6em;
        }
        .TriggerColumn
        {
        	width:      6em;
        }
        .CondCategoryIdColumn
        {
        	width:      7em;
        	overflow:   hidden;
        }
        .SubjectColumn
        {
        	width:      50em;
        }
        .StatusColumn
        {
            width:      5em;
        }
        .DateColumn
        {
        	width:      11em;
        }
        .AssignedToColumn
        {
        	min-width:  8em;
        }
        .OwnerColumn
        {
            min-width:  8em;
        }

        .Errors, .ImportExportWarning
        {
            color: Red;
        }
    </style>
</head>
<body>
    <h4 class="page-header"><ml:EncodedLabel runat="server" ID="m_title" Text="Task List"></ml:EncodedLabel></h4>
    <form id="TaskList" runat="server">
    <script type="text/javascript" src="../../inc/json.js"></script>
    <script type="text/javascript">
    <!--
        var allCB = "selectAllCB";
        var CBName = "taskCB_name";
        var CBInput = "input[name='"+CBName+"']";
        var CBSelected = CBInput+":checked";
        var taskSearchTextbox = "taskSearchTextbox";
        var taskEditorLink = "/LoAdmin/Task/LOAdminTaskEditorV2.aspx";
        var taskViewerLink = "/LoAdmin/Task/LOAdminTaskViewer.aspx";
        var batchButtonsDisabled = true;
        var taskDialogOptions = 'width=850,height=600,center=yes,resizable=yes,scrollbars=yes,status=yes,help=no';

        // ##### Checkboxes! #########################
        // See Task System Front End Q&A's wiki question 75 for more
        //   information about checkboxes

        function selectAllCB_onClick() {
            if ($j("#"+allCB).attr('checked')) {
                selectAllCB()
            } else {
                deselectAllCB();
            }
        }

        function selectAllCB() {
            $j(CBInput).prop('checked', true);

            $j("#BatchButtons input").prop('disabled', false);
            batchButtonsDisabled = false;

            // highlight the rows
            $j(CBInput).each( function() {
                highlightRowByCheckbox(this);
            });
        }

        function deselectAllCB() {
            $j(CBInput).prop('checked', false);

            $j("#BatchButtons input").prop('disabled', true);
            batchButtonsDisabled = true;

            // highlight the rows
            $j(CBInput).each( function() {
                highlightRowByCheckbox(this);
            });
        }

        function taskCB_onClick(checkbox) {
            highlightRowByCheckbox(checkbox);
            $j("#"+allCB).prop('checked', false);
            if ($j(CBSelected).length == 0) {
                $j("#BatchButtons input").prop('disabled', true);
                batchButtonsDisabled = true;
            } else {
                $j("#BatchButtons input").prop('disabled', false);
                batchButtonsDisabled = false;
            }
        }

        // ##### Top operations! ####################

        // This should show the task list's new state if it's been updated outside
        function taskList_refresh() {
            TaskList.submit();
            // perform a postback, mayhaps?
        }

        function showModalTaskCreator() {
            var queryString =
                '?loanid=' + ML.sLId +
                '&IsTemplate=' + $j('#m_isTemplate').val();
            showModal(taskEditorLink + queryString, null, null, null, function(modalResult){
                if (modalResult.OK) {
                    taskList_refresh();
                } else {
                    //alert("CANCEL!");
                }
            });


        }
        function showModalTaskViewer(taskID) {
            var queryString =
                '?loanid=' + ML.sLId +
                '&IsTemplate=' + $j('#m_isTemplate').val();
            if (taskID !== null) {
                queryString += '&taskid=' + taskID;
            }
            showModal(taskViewerLink + queryString, null, null, null, function(modalResult){
                if (modalResult.OK) {
                    taskList_refresh();
                } else {
                    //alert("CANCEL!");
                }
            })
        }

        function showModelessTaskViewer(taskID) {
            showTaskDialog(taskID, false);
        }

        function showModelessTaskCreator() {
            showTaskDialog('', true);
        }

        function showTaskDialog(taskID, isNew) {
            var link;
            var windowName;
            var queryString;
            if (isNew) { // creating a new task does not require a taskID
                link = taskEditorLink;
                queryString = '?'
                    + 'brokerid=' + <%= AspxTools.JsString(BrokerID.ToString()) %>
                windowName = '_blank';
            } else {
                link = taskViewerLink;
                queryString = '?'
                    + 'brokerid=' + <%= AspxTools.JsString(BrokerID.ToString()) %>
                if (taskID !== null) {
                    queryString += '&taskid=' + taskID;
                }
                windowName = 'LOtaskWindow__' + taskID;
            }
            var url = VRoot + link + queryString;
            var handle = window.open('', windowName, taskDialogOptions);
            if (handle.location.href === 'about:blank') {
                handle.location.href = url;
            }
            handle.focus(); // if the window already exists, focus on it
        }

        function addNewTask_onClick() {
            showModelessTaskCreator();
        }

        function deleteTask_onClick(taskID) {
            batchServiceCall('DeleteTasks', {
                brokerID: <%= AspxTools.JsString(BrokerID.ToString()) %>,
                taskIDs: taskID
            });
            taskList_refresh();
        }

        function taskSearchBtn_onClick() {
            var $taskSearchTextbox = $j("#"+taskSearchTextbox);
            var taskID = $taskSearchTextbox.val();
            showTaskDialog(taskID, false);
        }

        function refresh_onClick() {
            taskList_refresh();
        }

        // ##### Batch Operations! ###################
        // Anatomy of a batch operation call
        // 1. Extract the TaskIds (first part of batchSubmit)
        // 2. Perform the operation using the function that we passed in
        // 3. Update

        function assign_onClick() {
            return batchSubmit(function(taskIDs, taskVersions, hiddenTasks) {
                var queryString = "?"
                    + 'loanid=' + <%=AspxTools.JsString(LoanID.ToString())%>
                    + '&brokerid=' + <%=AspxTools.JsString(BrokerID)%>
                    + '&IsBatch=' + true
                    + '&IsTemplate=' + true
                    + '&IsPipeline=' + true;
                var anyHidden = false;
                for (var i = 0; i < hiddenTasks.length; i++) {
                    if (hiddenTasks[i] === "True") {
                        anyHidden = true;
                        break;
                    }
                }
                if (anyHidden === true) {
                    queryString += "&nopml=true";
                }
                showModal("/newlos/Tasks/RoleAndUserPicker.aspx" + queryString, null, null, null, function(modalResult){
                    if (modalResult.OK) {
                        var serviceArgs = {
                            brokerID : <%= AspxTools.JsString(BrokerID.ToString()) %>,
                            taskIDs : taskIDs.join(','),
                            assignmentId : modalResult.id,
                            type : modalResult.type
                        };
                        return batchServiceCall('Assign', serviceArgs);
                    } else {
                        return false;
                    }
                });
            });
        }
        function setDueDate_onClick() {
            return batchSubmit(function(taskIDs, taskVersions) {
                showModal('/newlos/Tasks/DateCalculator.aspx', null, null, null, function(modalResult){
                    if (modalResult.OK) {
                        var serviceArgs = {
                            brokerID : <%= AspxTools.JsString(BrokerID.ToString()) %>,
                            taskIDs : taskIDs.join(','),
                            offset : modalResult.offset,
                            dateField : modalResult.id
                        };
                        return batchServiceCall('SetDueDate', serviceArgs);
                    } else {
                        return false;
                    }
                }, {hideCloseButton:true});
            });
        }

        function exportToPDF_onClick() {
            return batchSubmit(function(taskIDs, taskVersions) {
                var args = {
                    brokerID : <%= AspxTools.JsString(BrokerID.ToString()) %>,
                    taskIDs : taskIDs.join(',')
                };

                // We need pdfInstance.Name and VRoot, so call the service
                batchServiceCall('ExportToPDF', args,
                    function(data, textStatus, jqXHR) {
                        var filepath = data.d.filepath;
                        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(filepath);
                    }
                );
            }, true);
        }

        function batchSubmit(lambda, bRefresh) {
            if (batchButtonsDisabled) {
                return false;
            }
            $j("#BatchButtons input").prop('disabled', true);
            batchButtonsDisabled = true;
            var $CB = $j(CBSelected);
            var taskIDs = [];
            var taskVersions = [];
            var hiddenTasks = [];
            $CB.each( function() {
                var taskInfo = this.value.split(':')
                taskIDs.push(taskInfo[0]);
                taskVersions.push(taskInfo[1]);
                hiddenTasks.push(taskInfo[2]);
            });

            var operationResult = lambda(taskIDs, taskVersions, hiddenTasks);

            // Now update! Unless we explicitly don't want to update
            // Or if the operation failed
            if (typeof bRefresh == "undefined" && operationResult) {
                taskList_refresh();
            }
            $j("#BatchButtons input").prop('disabled', false);
            batchButtonsDisabled = false;
        }

        // Returns true if successful, false otherwise
        function batchServiceCall(serviceName, serviceArgs, successFunc) {
            jsonData = JSON.stringify(serviceArgs);
            callWebMethodAsync({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "LOAdminTaskListService.aspx/" + serviceName,
                data: jsonData,
                dataType: "json",
                async: false,
                success: successFunc,
                error: function (jqXHR, textStatus, errorThrown) {
                    var err = JSON.parse(jqXHR.responseText);
                    alert(err.Message);
                }
            });
            return true;
        }

        // just pretend it's the same as $(document).ready
        function _init() {
            // Stuff that should only be done when everything's loaded
            resize(1200, 900);
            $j("input[type='button']").attr("NoHighlight", "true"); // turn off button highlighting

            var $taskSearchTextbox = $j("#"+taskSearchTextbox);
            var $taskSearchBtn = $j("#taskSearchBtn");
            function updateSearchBtn (len) {
                if (len <= 0) {
                    $taskSearchBtn.prop('disabled', true);
                } else {
                    $taskSearchBtn.prop('disabled', false);
                }
            }
            $taskSearchTextbox.keypress(function(event){
                var alphanumericRegex = /^[0-9]$/i; // these tasks only contain digits
                if (!alphanumericRegex.test(String.fromCharCode(event.which))) {
                    return false;
                }
                return true;
            });
            $taskSearchTextbox.keyup(function(event) {
                // Click the search button when the user presses enter in the search textbox
                if (event.which === 13) {
                    $taskSearchBtn.click();
                }
                updateSearchBtn($taskSearchTextbox.val().length);
            });

            // If no checkboxes are selected, then disable all the batch operations
            if ($j(CBSelected).length <= 0) {
                $j("#BatchButtons input").prop('disabled', true);
            } else {
                $j("#BatchButtons input").attr('disabled', '');
            }

            updateSearchBtn($taskSearchTextbox.val().length);

            // Select the row of urgent items and bold the whole row
            // .DiscDue is given to a DueDate or FollowUpDate in the codebehind:GetDateWarningCss
            $j(".DiscDue").closest(".GridItem, .GridAlternatingItem").css('font-weight', 'bold');
        }
    // -->
    </script>
    <asp:HiddenField ID="m_isTemplate" runat="server" Value="" />
    <div>
        <table id="TopButtons">
            <tr>
                <td id="TopButtonsLeftSide">
                    <input type="button" value="Add new task" onclick="addNewTask_onClick()" />
                    <input type="button" value="Refresh" onclick="refresh_onClick()" NotForEdit />
                    &nbsp;
                    Display
                    <asp:DropDownList runat="server" ID="m_conditionFilter" AutoPostBack="true" OnSelectedIndexChanged="m_openResolvedTasksGV_SelectedCondFilterChanged">
                    </asp:DropDownList>
                    assigned to:
                    <asp:DropDownList runat="server" ID="m_assignmentFilter" OnInit="PopulateAssignmentDropdown" AutoPostBack="true" OnSelectedIndexChanged="m_openResolvedTasksGV_SelectedFilterChanged">
                    </asp:DropDownList>
                    <ml:EncodedLabel runat="server" ID="m_filterErrorMessage"></ml:EncodedLabel>
                </td>
                <td id="TopButtonsRightSide">
                    Task #
                    <asp:TextBox name="taskSearch" runat="server" EnableViewState="true" id="taskSearchTextbox" NotForEdit></asp:TextBox>
                    <input type="button" name="taskSearch" id="taskSearchBtn" value="Search" onclick="taskSearchBtn_onClick()" NotForEdit />
                </td>
            </tr>
        </table>

        <div id="openResolvedTasksDiv">

        <asp:GridView id="m_openResolvedTasksGV" runat="server" AutoGenerateColumns="false" EnableViewState="false">
            <HeaderStyle cssclass="GridHeader AlignLeft" />
            <AlternatingRowStyle cssclass="GridAlternatingItem" />
            <RowStyle cssclass="GridItem" />

            <columns>
                <asp:TemplateField>
                    <HeaderStyle CssClass="CheckboxColumn" />
                    <HeaderTemplate>
                        <input type="checkbox" name="selectAllCB_name" id="selectAllCB" onclick="selectAllCB_onClick();" NotForEdit />
                    </HeaderTemplate>
                    <ItemStyle CssClass="CheckboxColumn" />
                    <ItemTemplate>
                        <input type="checkbox" id="taskCB_id" name="taskCB_name" onclick="taskCB_onClick(this)"
                            value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>:PlaceholderRowVersion:PlaceHolderHidden'
                            NotForEdit />
                        <a href='#' onclick='deleteTask_onClick(<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>)'>delete</a>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle CssClass="TriggerColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton0"
                            runat="server"
                            CommandName="Sort"
                            CommandArgument="TriggerName openResolvedTasksGV_TriggerHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            Text="Trigger">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_TriggerHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="TriggerColumn" />
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TriggerName").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton1"
                            runat="server"
                            CommandName="Sort"
                            CommandArgument="TaskId openResolvedTasksGV_TaskHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            Text="Task">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_TaskHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="TaskIDColumn" />
                    <ItemTemplate>
                        <a href="javascript: void(0)" class="OpenTaskIDLink TaskViewerLink" title="Open the task viewer for this task."
                            onclick="showModelessTaskViewer(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>)">
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle CssClass="CondCategoryIdColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton2"
                            runat="server"
                            CommandName="Sort"
                            CommandArgument="CondCategoryId openResolvedTasksGV_CondCategoryIdHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            Text="Category">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_CondCategoryIdHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="CondCategoryIdColumn"/>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CondCategoryId").ToString())%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle CssClass="SubjectColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton3"
                            Text="Subject"
                            CommandName="Sort"
                            CommandArgument="TaskSubject openResolvedTasksGV_SubjectHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_SubjectHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="SubjectColumn" />
                    <ItemTemplate>
                        <a href="javascript: void(0)" class="OpenTaskIDLink TaskViewerLink" title="Open the task viewer for this task."
                            onclick="showModelessTaskViewer(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>)">
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskSubject").ToString()) %>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle CssClass="SubjectColumn" />
                    <HeaderTemplate>Req. DocType</HeaderTemplate>
                    <ItemStyle CssClass="SubjectColumn" />
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CondRequiredDocTypeRep").ToString())%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle CssClass="DateColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton5"
                            Text="Due Date"
                            CommandName="Sort"
                            CommandArgument="TaskDueDateComparison openResolvedTasksGV_DueDateHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_DueDateHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="DateColumn" />
                    <ItemTemplate>
                        <a
                            style="display: <%# AspxTools.HtmlString( DataBinder.Eval( Container.DataItem, "TaskDueDateCalcDisplay").ToString() ) %>"
                            title="<%# AspxTools.HtmlString( DataBinder.Eval( Container.DataItem, "TaskDueDateCalc").ToString() ) %>"
                        >
                            CALC
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Last Updated">
                    <HeaderStyle CssClass="DateColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton4"
                            Text="Last Updated"
                            CommandName="Sort"
                            CommandArgument="TaskLastModifiedDateComparison openResolvedTasksGV_LastUpdatedHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_LastUpdatedHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="DateColumn" />
			        <ItemTemplate>
				        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskLastModifiedDate", "{0:MM/dd/yyyy}"))%>
			        </ItemTemplate>
		        </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle CssClass="AssignedToColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton8"
                            Text="Assigned To"
                            CommandName="Sort"
                            CommandArgument="AssignedUserFullName openResolvedTasksGV_AssignedToHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_AssignedToHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="AssignedToColumn" />
                    <ItemTemplate>
                        <div style="width: 8em; overflow: hidden;"><!-- IE appeasement --></div>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AssignedUserFullName").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle CssClass="OwnerColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton ID="LinkButton9"
                            Text="Owner"
                            CommandName="Sort"
                            CommandArgument="OwnerFullName openResolvedTasksGV_OwnerHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_OwnerHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="OwnerColumn" />
                    <ItemTemplate>
                        <div style="width: 8em; overflow: hidden;"><!-- IE appeasement --></div>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OwnerFullName").ToString())%>
                    </ItemTemplate>
                </asp:TemplateField>
            </columns>
        </asp:GridView>

        </div>

        <div id="BatchButtons">
            <input type="button" value="Assign" id="assignBtn" onclick="assign_onClick()"/>
            <input type="button" value="Set Due Date" id="setDueDateBtn" onclick="setDueDate_onClick()"/>
            <input type="button" value="Export to PDF" id="exportToPDFBtn" onclick="exportToPDF_onClick();"/>
        </div>

        <div id="ImportExportTools" runat="server">
            <br />
            <ml:EncodedLabel ID="ImportExportWarning" runat="server" class="ImportExportWarning">DO NOT USE UNLESS YOU KNOW WHAT YOU ARE DOING!!! CONSULT THE DEVS FIRST!!!</ml:EncodedLabel>
            <br />
            <asp:FileUpload ID="ImportXMLFileUpload" runat="server" /><asp:Button Text="Import XML" id="ImportXMLBtn" runat="server" onclick="ImportXMLBtn_Click"/>
            <asp:Button Text="Export XML" id="ExportXMLBtn" runat="server" onclick="ExportXMLBtn_Click"/>
            <br />
            <ml:EncodedLabel ID="ImportErrors" runat="server"></ml:EncodedLabel>
        </div>
    </div>

    <uc1:cModalDlg ID="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cModalDlg>
    </form>
</body>
</html>

