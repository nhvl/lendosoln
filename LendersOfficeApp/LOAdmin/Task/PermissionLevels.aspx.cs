﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;
using DataAccess;
using System.Web.Services;
using LendersOffice.Admin;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Task
{
    public partial class PermissionLevels : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("BrokerId");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            this.EnableJqueryMigrate = false;

            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");

            RegisterJsScript("LQBPopup.js");
            if (Page.IsPostBack)
            {
                return;
            }

            PermissionLevel[] brokerLevels = PermissionLevel.RetrieveAll(BrokerId).ToArray();

            DefaultLevelForConditions.DataSource = brokerLevels.Where(level=>level.IsAppliesToConditions && level.IsActive);
            DefaultLevelForConditions.DataTextField = "Name";
            DefaultLevelForConditions.DataValueField = "Id";
            DefaultLevelForConditions.DataBind();

            PermissionLevelGrid.DataSource = brokerLevels;
            PermissionLevelGrid.DataBind();
            PageBrokerId.Value = BrokerId.ToString();

            if (PermissionLevelGrid.Rows.Count > 0)
            {
                PermissionLevelGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            BrokerDB db = BrokerDB.RetrieveById(BrokerId);
            if (db.DefaultTaskPermissionLevelIdForImportedCategories.HasValue)
            {
                string value = db.DefaultTaskPermissionLevelIdForImportedCategories.Value.ToString();
                ListItem item = DefaultLevelForConditions.Items.FindByValue(value);
                item.Selected = true;
            }
        }

 
        protected void OkayBtn_OnClick(object sender, EventArgs e)
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            int[] order = ObsoleteSerializationHelper.JavascriptJsonDeserializer<int[]>(SortOrder.Value);

            PermissionLevel[] levels = PermissionLevel.RetrieveAll(BrokerId).ToArray();
            foreach (PermissionLevel level in levels)
            {
                int index = Array.IndexOf(order, level.Id);
                index = index >= 0 ? index : 100;
                level.Rank = index;
            }
            BrokerDB db = BrokerDB.RetrieveById(BrokerId);
            PermissionLevel defaultLevel = levels.Where(level => level.Id ==  int.Parse(Request.Form[DefaultLevelForConditions.ClientID])).First();
            
            if( false == defaultLevel.IsAppliesToConditions )
            {
                string url = Request.RawUrl;
                if (false == Request.RawUrl.Contains("&cm=1"))
                {
                    url += "&cm=1";
                }
                Response.Redirect(url);
                return;
            }
            
            db.DefaultTaskPermissionLevelIdForImportedCategories = defaultLevel.Id;

            using (CStoredProcedureExec exec = new CStoredProcedureExec(BrokerId))
            {
                exec.BeginTransactionForWrite();

                try
                {
                    PermissionLevel.Save(exec, levels);
                    db.Save(exec);
                    exec.CommitTransaction();
                }

                catch
                {
                    exec.RollbackTransaction();
                    throw;
                }
            }

            ClientScript.RegisterClientScriptBlock(typeof(PermissionLevelEditor), "close", "onClosePopup()", true);
            
        }

        protected void PermissionLevelGiod_OnRowCreated(object sender, GridViewRowEventArgs args)
        {
            if (args.Row.RowType != DataControlRowType.DataRow && args.Row.RowType != DataControlRowType.DataRow)
            {
                return;
            }

            PermissionLevel currentLevel = (PermissionLevel)args.Row.DataItem;
            HiddenField idField = (HiddenField)args.Row.FindControl("Id");
            idField.Value = currentLevel.Id.ToString();

        }
    }
}
