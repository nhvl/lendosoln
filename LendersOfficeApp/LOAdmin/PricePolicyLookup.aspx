<%@ Page Language="C#" CodeBehind="~/LOAdmin/PricePolicyLookup.aspx.cs" MasterPageFile="~/LOAdmin/loadmin.Master" Inherits="LendersOfficeApp.LOAdmin.PricePolicyLookup" Title="Price Policy Lookup" AutoEventWireup="True" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
 <div class="container">
  <div class="row">
    <div class="col-xs-12 col-xs-offset-0">
        <div class="panel panel-default">
          <div class="panel-body">
              <script>
                  var app = angular.module('myApp', []);
                  app.factory('policyLookupService', function ($http) {
                      return {
                          lookup: function (lookupInput, successMethod, failedMethod) {
                              //return the promise directly.
                              return $http.post('PricePolicyLookup.aspx/LookupPricePolicy', { lookupString: lookupInput })
                                        .then(function (result) {
                                            //resolve the promise as the data
                                            successMethod(result.data.d);
                                        }, function (response) {
                                            failedMethod(response)
                                        });
                          }
                      }
                  });
                  app.controller('policyLookupCtrl', function ($scope, policyLookupService) {
                      $scope.policyList = null;
                      $scope.Lookup = function () {
                          var lookupTextbox = $("input[id*='m_Keyword']");
                          if (!verifyInputTextbox(lookupTextbox)) {
                              return false;
                          }
                          var lookupInput = lookupTextbox.val();
                          var lookupButton = $("#m_Lookup");
                          lookupButton.attr("disabled", "disabled");
                          var returnString = policyLookupService.lookup(lookupInput,
                                                  function (data) {
                                                      $scope.policyList = data;
                                                      lookupButton.removeAttr("disabled");
                                                  }, function (response) {
                                                      lookupButton.removeAttr("disabled");
                                                  });
                      };
                  });

                  function verifyInputTextbox(textbox) {
                      textbox.tooltip("destroy");
                      var val = textbox.val();
                      if (val == "") {
                          textbox.tooltip({ title: "Please enter a keyword." });
                          textbox.focus();
                          return false;
                      } else if (val.trim() == "") {
                          textbox.tooltip({ title: "Keyword must not be blank." });
                          textbox.focus(); return false;
                      }
                      return true;
                  }
            </script>
            <form id="stageForm" runat="server"> 
              <div ng-app="myApp" ng-controller="policyLookupCtrl">
                <div class="form-horizontal "><div class="form-group ">
                    <label class="control-label col-xs-4">Keyword or parameter: </label>
                    <div class="input-group col-xs-4">
                            <%--small text box--%><%-- DC652EDB-4D3F-4E8D-B537-A3B70189934C--%>
                        <input type="text"   AutoComplete="off"   class="form-control input-sm" id="m_Keyword" MaxLength="50" onkeydown="if (event.keyCode == 13){ document.getElementById('m_Lookup').click();return false;}" />
                        <script> $("input[id*='m_Keyword']").focus()</script>
                        <span class="input-group-btn ">
                            <span class="clipper">
                            <input type="button" class="btn btn-sm btn-primary" id="m_Lookup" ng-click="Lookup()"  value="Lookup" />
                            </span>
                        </span>
                    </div>
                </div> </div>
                
                <div class=" col-xs-10 col-xs-offset-1"  ng-show="policyList!=null">
                  <br />
                  <div class="text-center ">
		            <ml:EncodedLabel id="m_Notes" runat="server"></ml:EncodedLabel>
                    <label class="control-label text-success">{{policyList.length}} policies returned.</label>
	             </div>
                  <table  class="table table-striped table-condensed table-layout-fix " ng-show="policyList.length">
                    <thead>
                        <tr>
                          <th class="string">Price Policy ID</th>
                          <th >Parent Policy Path</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr ng-repeat="policy in policyList">
                        <td  >{{policy.PricePolicyID}}</td>
                        <td ">>{{policy.ParentPolicyPath}}</td>
                      </tr>
                    </tbody>
                  </table>  
                </div>
	            <asp:datagrid runat="server" id="m_Grid" AutoGenerateColumns="False">
		            <Columns>
			            <asp:BoundColumn DataField="PricePolicyID" HeaderText="Price Policy ID" ItemStyle-Width="400px" />
			            <asp:BoundColumn DataField="ParentPolicyPath" HeaderText="Parent Policy Path" />
		            </Columns>
	            </asp:datagrid>
              </div>
            </form>
            
          </div>
        </div >
    </div >
  </div>
</div>
</asp:Content>


