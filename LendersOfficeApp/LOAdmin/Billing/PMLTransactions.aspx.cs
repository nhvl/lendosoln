using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using DataAccess ; 

namespace LendersOfficeApp.LOAdmin.Billing
{
	/// <summary>
	/// Summary description for PMLTransactions.
	/// </summary>
	public partial class PMLTransactions : LendersOffice.Admin.SecuredAdminPage
	{
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
            {

                //add month options to combobox
                //fill in current month
                DateTime dt = DateTime.Now;
                m_ddlMonth.Items.Add(new ListItem(string.Format("{0}/{1} (MTD)", dt.Month, dt.Year), "-1"));
                //fill last 12 month
                for (int i = 0; i < 12; i++)
                {
                    dt = dt.AddMonths(-1);
                    m_ddlMonth.Items.Add(new ListItem(string.Format("{0}/{1}", dt.Month, dt.Year), i.ToString()));
                }
			}
		}
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

        protected void btnShowSummary_Click(object sender, System.EventArgs e)
        {
            //get start date and end date info
            //get combobxo value
            int nMonth = Convert.ToInt32(m_ddlMonth.SelectedItem.Value);
            DateTime dtEnd;
            DateTime dtStart;
            string startDate;
            string endDate;
            //calculate start date and end date
            dtEnd = nMonth > -1 ? DateTime.Now.AddMonths(-nMonth) : DateTime.Now.AddDays(1);
            dtStart = nMonth > -1 ? dtEnd.AddMonths(-1) : DateTime.Now;


            startDate = string.Format("{0}/1/{1}", dtStart.Month, dtStart.Year);
            endDate = string.Format("{0}/{1}/{2}", dtEnd.Month, nMonth != -1 ? 1 : dtEnd.Day, dtEnd.Year);
            //get file name from start date 
            string filename = string.Format("{0}{1:00}", dtStart.Year, dtStart.Month);
            if (nMonth == -1)
            {
                filename += "MTD";
            }

            DateTime dtAbsStart = DateTime.ParseExact(startDate,"M/d/yyyy",null);
            DateTime dtAbsEnd = DateTime.ParseExact(startDate, "M/d/yyyy", null);
            //get pml tracsaction sum from start date and end date
            //call store storeprocedure with date parameters
            Response.Clear();
            CustomerStats stat = null;

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                
                SqlParameter[] parameters = {new SqlParameter("@StartDate", startDate),
								new SqlParameter("@EndDate", endDate)
							  };

                using (DbDataReader sdr = StoredProcedureHelper.ExecuteReader(connInfo, "GetPmlTransactionSummary", parameters))
                {

                    while (sdr.Read())
                    {
                        //if see new customer 
                        if (stat == null || stat.Code != sdr["CustomerCode"].ToString())
                        {
                            //save last customer info to csv reponse
                            if (stat != null) stat.PrintStats(Response);
                            //store new customer info
                            stat = new CustomerStats(sdr["CustomerCode"].ToString(), dtAbsStart, dtAbsEnd);
                        }

                        //update current customer info from the record  
                        stat.IncrementCounters(sdr);
                    }

                    if (stat != null) stat.PrintStats(Response);
                }
            }



            Tools.LogVerbose(filename);
            //send  pml tracsaction sum to client as a csv 
            Response.ContentType = "text/comma-separated-values";
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{filename}.csv\"");
            Response.Flush();
            Response.End();
        }

        class CustomerStats
        {
            public CustomerStats(string sCode, DateTime dtStart, DateTime dtEnd)
            {
                m_sCode = sCode;
                m_dtStart = dtStart;
                m_dtEnd = dtEnd;
            }

            public void IncrementCounters(DbDataReader sdr)
            {
                //1/3/08 db - OPM 19007 - Using more efficient Stored Proc that Binh created
                /*
                if (InRange(sdr["sPml1stVisitCreditStepD"])) m_nCreditStep++ ;
                if (InRange(sdr["sPml1stVisitApplicantStepD"])) m_nApplicantStep++ ;
                if (InRange(sdr["sPml1stVisitPropLoanStepD"])) m_nPropertyStep++ ;
                if (InRange(sdr["sPml1stVisitResultStepD"])) m_nResultStep++ ;
                if (InRange(sdr["sPml1stSubmitD"])) m_nSubmitted++ ;*/

                m_nCreditStep += int.Parse(sdr["Step1"].ToString());
                m_nApplicantStep += int.Parse(sdr["Step2"].ToString());
                m_nPropertyStep += int.Parse(sdr["Step3"].ToString());
                m_nResultStep += int.Parse(sdr["Step4"].ToString());
                m_nSubmitted += int.Parse(sdr["Submit"].ToString());
            }
            public string Code
            {
                get { return m_sCode; }
            }
            public void PrintStats(HttpResponse Response)
            {
                string s = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\"\n", m_sCode, m_nCreditStep, m_nApplicantStep,
                    m_nPropertyStep, m_nResultStep, m_nSubmitted);
                Response.Write(s);
            }

            private bool InRange(object field)
            {
                if (DBNull.Value.Equals(field)) return false;

                DateTime dt = Convert.ToDateTime(field);
                return dt >= m_dtStart && dt < m_dtEnd;
            }

            string m_sCode;
            DateTime m_dtStart;
            DateTime m_dtEnd;
            int m_nCreditStep = 0;
            int m_nApplicantStep = 0;
            int m_nPropertyStep = 0;
            int m_nResultStep = 0;
            int m_nSubmitted = 0;
        }
	}
}
