<%@ Page Language="C#" CodeBehind="~/LOAdmin/Billing/PMLTransactions.aspx.cs" MasterPageFile="~/LOAdmin/loadmin.Master" Inherits="LendersOfficeApp.LOAdmin.Billing.PMLTransactions" Title="PML Transactions"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
    <div class="row">  
        <div class="col-xs-6 col-xs-offset-3">
            <div class="panel panel-default">
              <div class="panel-body">
                <form id="Form1" runat="server" >
                  <div class = "form-horizontal">
                    <div class =  "form-group">
                        <div class="col-xs-6 text-right">
                            <label class=" control-label" for="<%= AspxTools.HtmlString(m_ddlMonth.ClientID) %>"> Retrieve transaction count for:</label>
                        </div>
                        <div class="col-xs-4">
                            <asp:DropDownList  id="m_ddlMonth" runat="server" class=" form-control"></asp:DropDownList>
                        </div>
                    </div>
                  </div>
                  <div class="col-xs-5 col-xs-offset-7 text-center">
                    <span  class="clipper">
                        <asp:Button class="btn btn-primary" id="btnShowSummary" runat="server" Text="Download CSV" onclick="btnShowSummary_Click"></asp:Button>
                    </span>    
                  </div>
                </form>   
              </div>
            </div>
        </div> 
    </div>
</asp:Content>
