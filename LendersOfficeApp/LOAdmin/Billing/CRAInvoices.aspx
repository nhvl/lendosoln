<%@ Page Language="c#" CodeBehind="CRAInvoices.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Billing.CRAInvoices"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="CRA Invoices" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style type="text/css">
        .center-div
        {
            margin: 0 auto;
            min-width: 8in;
        }
    </style>
        <form id="CRAInvoices" method="post" runat="server">
        <div class="container-fluid">
            <div class="row-masonry">
                <div class="grid-sizer">
                </div>
                <div class="masonry-item center-div">
                    <div class="panel panel-default">
                        
                        <div class="panel-body div-wrap-tb">
                            <br />
                            <div class="form-group">
                                <div class="col-xs-4">
                                    <asp:DropDownList ID="m_ddlMonth" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="col-xs-12">
                            	<ML:CommonDataGrid ID="m_dgCRAs" runat="server" AutoGenerateColumns="False" ShowHeader="False"
	                                CellSpacing="0" CellPadding="0" BorderWidth="0" GridLines="None" DataKeyField="ID"
	                                EnableViewState="False" CssClass="table table-striped table-condensed">
	                                <AlternatingItemStyle CssClass="" VerticalAlign="NotSet" />
	                                <ItemStyle CssClass="" VerticalAlign="NotSet" />
	                                <Columns>
	                                    <asp:BoundColumn DataField="Name"></asp:BoundColumn>
	                                    <%--<asp:ButtonColumn CommandName="Select" HeaderStyle-CssClass="clipper" >
	                                        <button type="button" runat="server" class="btn btn-link">Download</button>
	                                    </asp:ButtonColumn>--%>
	                                    <asp:TemplateColumn>
	                                    <ItemTemplate>
	                                        <span class="clipper">
	                                            <button type="button" class="btn btn-link" runat="server" onserverclick="Download_Click">Download</button>
	                                        </span>
	                                    </ItemTemplate>
	                                    </asp:TemplateColumn>
	                                </Columns>
	                            </ML:CommonDataGrid>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
</asp:Content>
