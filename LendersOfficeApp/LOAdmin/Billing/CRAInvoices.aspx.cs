namespace LendersOfficeApp.LOAdmin.Billing
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;

    class CRAEntry
    {
        string m_sName ;
        string m_sID ;

        public CRAEntry(string sName, string sID)
        {
            m_sName = sName ;
            m_sID = sID ;
        }

        public string NAME { get { return m_sName ; } }
        public string ID { get { return m_sID ; } }
    }

    public partial class CRAInvoices : LendersOffice.Admin.SecuredAdminPage
    {
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            if (!IsPostBack)
            {
                DateTime dt = DateTime.Now;

                m_ddlMonth.Items.Add(new ListItem($"{dt.Month.ToString("00")}/{dt.Year} (IN PROGRESS)", "-1"));
                for (int i = 0; i < 12; i++)
                {
                    dt = dt.AddMonths(-1);
                    m_ddlMonth.Items.Add(new ListItem($"{dt.Month.ToString("00")}/{dt.Year}", i.ToString()));
                }

                m_ddlMonth.SelectedValue = "0";
            }

            BindDataGrid();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            notOnlySAE();
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            //this.m_dgCRAs.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.m_dgCRAs_ItemCommand);
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion

        protected void Download_Click(object sender, EventArgs e)
        {
            var rowIndex = ((DataGridItem)((Control)sender).NamingContainer).ItemIndex;
            string sID = (string)m_dgCRAs.DataKeys[rowIndex];
            int nMonth = Convert.ToInt32(m_ddlMonth.SelectedItem.Value);
            DateTime dtEnd = DateTime.Now.AddMonths(-nMonth);
            DateTime dtStart = dtEnd.AddMonths(-1);
            string startDate = string.Format("{0}/1/{1}", dtStart.Month, dtStart.Year);
            string endDate = string.Format("{0}/1/{1}", dtEnd.Month, dtEnd.Year);
            string filename = string.Format("{0}{1:00}", dtStart.Year, dtStart.Month);

            Response.Clear();

            Response.Write("\"DATE\",\"FILE ID\",\"COMPANY\",\"BORROWER\",\"CRA\",\"PRODUCT\"\n");

            var dupTable = new HashSet<Tuple<string, string, string>>();
            var serviceCompanyNameByID = SearchServiceCompanies(sID);

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                //https://lqbopm/default.asp?207992
                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@StartTime", startDate),
                    new SqlParameter("@EndTime", endDate)
                };

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListCraTransactionsInTimeFrame", parameters))
                {
                    while (reader.Read())
                    {
                        var comId = (Guid)reader["ComId"];
                        if (!serviceCompanyNameByID.ContainsKey(comId))
                        {
                            continue;
                        }

                        var key = Tuple.Create(reader["externalfileid"].ToString(), reader["BrokerNm"].ToString(), reader["FileType"].ToString());
                        if (!dupTable.Contains(key))
                        {
                            string transactionInfo = $"\"{reader["timeversion"]}\",\"{reader["externalfileid"]}\",\"{reader["BrokerNm"]}\",\"{reader["aBNm"]}\",\"{serviceCompanyNameByID[comId]}\",\"{reader["BrandedProductName"]}\"\n";
                            Response.Write(transactionInfo);

                            dupTable.Add(key);
                        }
                    }
                }
            }

            Response.ContentType = "text/comma-separated-values";
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{filename}.csv\"");
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Retrieves service company information, either searching for a specific company or for
        /// any using a given protocol.
        /// </summary>
        /// <param name="searchKey">Indicates the ComId or ProtocolType to search by.</param>
        /// <returns>A dictionary mapping the company IDs to names.</returns>
        private Dictionary<Guid, string> SearchServiceCompanies(string searchKey)
        {
            string procedureName;
            var parameters = new List<SqlParameter>();

            if (searchKey.StartsWith("BY_PROTOCOL_"))
            {
                procedureName = "SERVICE_COMPANY_ListByProtocolType";
                parameters.Add(new SqlParameter("@ProtocolType", searchKey.Substring(12)));
            }
            else
            {
                procedureName = "SERVICE_COMPANY_Retrieve";
                parameters.Add(new SqlParameter("@ComId", new Guid(searchKey)));
            }

            var serviceCompanies = new Dictionary<Guid, string>();

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShare, procedureName, parameters))
            {
                while (reader.Read())
                {
                    var comId = (Guid)reader["ComId"];
                    var comName = (string)reader["ComNm"];

                    serviceCompanies.Add(comId, comName);
                }
            }

            return serviceCompanies;
        }

        private void BindDataGrid()
        {
            var list = new List<CRAEntry>();
            list.Add(new CRAEntry("CBC (CBC INNOVIS)", "da8759e4-1607-422f-8838-b3282fe4ff35"));
            list.Add(new CRAEntry("CREDIT QUICK SERVICES", "575e8c20-0b2c-4720-a4be-af64123b07bf"));
            list.Add(new CRAEntry("CSC Cra(s)", "BY_PROTOCOL_13"));
            list.Add(new CRAEntry("CSD (CREDIT SYSTEMS DESIGN)", "ffec4699-2105-4ed3-a262-9aa9b1868847"));
            list.Add(new CRAEntry("Factual Data", "BY_PROTOCOL_4"));
            list.Add(new CRAEntry("FISERV/CHASE CREDIT", "d20f7187-a962-4468-ab96-c985a357faf4"));
            list.Add(new CRAEntry("INFO CREDIT NET", "e04d2398-e614-42a0-9a50-3276c14ab740"));
            list.Add(new CRAEntry("OLD REPUBLIC CREDIT SERVICES", "e17e0a7b-7018-44aa-835f-943834760464"));
            list.Add(new CRAEntry("SHARPER LENDING Cra(s)", "BY_PROTOCOL_11"));
            list.Add(new CRAEntry("INFORMATIVE RESEARCH", "BY_PROTOCOL_17"));
            list.Add(new CRAEntry("EQUIFAX", "BY_PROTOCOL_18"));
            ////list.Add(new CRAEntry("SARMA", "0ac80267-ea42-456e-b75e-f0cbb88790e6"));

            m_dgCRAs.DataSource = list;
            m_dgCRAs.DataBind();
        }
    }
}
