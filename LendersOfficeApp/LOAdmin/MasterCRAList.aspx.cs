using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.CreditReport;
using LendersOffice.Integration.VOXFramework;
using LendersOffice.Security;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.LOAdmin
{

    public partial class MasterCRAListPage : LendersOffice.Admin.SecuredAdminPage
    {
        private Lazy<IEnumerable<LightVOXVendor>> voxVendors = new Lazy<IEnumerable<LightVOXVendor>>(VOXVendor.LoadLightVendors);

        protected override E_InternalUserPermissions[] GetRequiredPermissions() => new[] { E_InternalUserPermissions.EditIntegrationSetup };

        protected void PageLoad(object sender, System.EventArgs e)
        {
            this.BindVendorList(this.VoxVendorId);

            List<CreditReportingAgency> cras = new List<CreditReportingAgency>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "INTERNAL_USER_ONLY_ListAllCras"))
            {
                int count = 0;
                while (reader.Read())
                {
                    ClientScript.RegisterHiddenField("ComId_" + count, reader["ComId"].ToString());
                    DateTime? sWarningStartD = null;
                    if (reader["WarningStartD"] != DBNull.Value)
                    {
                        sWarningStartD = (DateTime)reader["WarningStartD"];
                    }

                    var linkedVendorId = -1;
                    if (reader["VoxVendorId"] != DBNull.Value)
                    {
                        linkedVendorId = (int)reader["VoxVendorId"];
                    }

                    cras.Add(new CreditReportingAgency
                    {
                        ComId = (Guid)reader["ComId"],
                        Name = (string)reader["ComNm"],
                        MclCraCode = (string)reader["MclCraCode"],
                        Url = (string)reader["ComUrl"],
                        ProtocolType = (CreditReportProtocol)reader["ProtocolType"],
                        IsValid = (bool)reader["IsValid"],
                        IsInternalOnly = (bool)reader["IsInternalOnly"],
                        IsWarningOn = (bool)reader["IsWarningOn"],
                        WarningMessage = (string)reader["WarningMsg"],
                        WarningStartDate = sWarningStartD,
                        VendorId = linkedVendorId
                    });
                }
            }

            CraList.DataSource = cras;
            CraList.DataBind();

            ComId.Text = Guid.NewGuid().ToString();

            Controls.Cast<Control>().All(control => control.EnableViewState = false);
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterService("test", "/loadmin/MasterCRAListService.aspx");
        }

        private void BindVendorList(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem(string.Empty, "-1"));
            foreach (var lightVendor in this.voxVendors.Value)
            {
                var item = new ListItem(lightVendor.CompanyName, lightVendor.VendorId.ToString());
                ddl.Items.Add(item);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            notOnlySAE();
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

        protected void CraList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var CreditProvider = (CreditReportingAgency)e.Item.DataItem;

            EncodedLiteral CraComId = e.Item.FindControl("CraComId") as EncodedLiteral;
            HiddenField ComId = e.Item.FindControl("ComId") as HiddenField;
            TextBox ComNm = e.Item.FindControl("ComNm") as TextBox;
            CheckBox IsWarningOn = e.Item.FindControl("IsWarningOn") as CheckBox;
            TextBox WarningMsg = e.Item.FindControl("WarningMsg") as TextBox;
            TextBox MclCraCode = e.Item.FindControl("MclCraCode") as TextBox;
            TextBox ComUrl = e.Item.FindControl("ComUrl") as TextBox;
            TextBox ProtocolType = e.Item.FindControl("ProtocolType") as TextBox;
            CheckBox IsValid = e.Item.FindControl("IsValid") as CheckBox;
            CheckBox IsInternalOnly = e.Item.FindControl("IsInternalOnly") as CheckBox;

            CraComId.Text = CreditProvider.ComId.ToString();
            ComId.Value = CreditProvider.ComId.ToString();
            ComNm.Text = CreditProvider.Name;
            IsWarningOn.Checked = CreditProvider.IsWarningOn;
            WarningMsg.Text = CreditProvider.WarningMessage;
            MclCraCode.Text = CreditProvider.MclCraCode;
            ComUrl.Text = CreditProvider.Url;
            ProtocolType.Text = ((int)CreditProvider.ProtocolType).ToString();
            IsValid.Checked = CreditProvider.IsValid;
            IsInternalOnly.Checked = CreditProvider.IsInternalOnly;

            var vendorId = e.Item.FindControl("VoxVendorId") as DropDownList;
            this.BindVendorList(vendorId);
            vendorId.SelectedValue = CreditProvider.VendorId.ToString();
        }

        protected class CreditReportingAgency
        {
            public Guid ComId
            { get; set; }
            public string Name
            { get; set; }
            public string MclCraCode
            { get; set; }
            public string Url
            { get; set; }
            public CreditReportProtocol ProtocolType
            { get; set; }
            public bool IsValid
            { get; set; }
            public bool IsInternalOnly
            { get; set; }
            public bool IsWarningOn
            { get; set; }
            public string WarningMessage
            { get; set; }
            public DateTime? WarningStartDate
            { get; set; }
            public int VendorId
            {
                get;
                set;
            }
        }
    }
}
