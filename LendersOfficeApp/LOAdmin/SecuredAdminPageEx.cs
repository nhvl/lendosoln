using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin
{
    public static class VirtualPathUtilityEx
    {
        public static string ToAbsolute(string path)
        {
            // http://stackoverflow.com/a/13360420
            // This should be fixed when upgrade to .NET 4.5 or greater
            var madeSafe = path.Replace("?", "UNLIKELY_TOKEN");
            var absolute = VirtualPathUtility.ToAbsolute(madeSafe);
            var restored = absolute.Replace("UNLIKELY_TOKEN", "?");
            return restored;
        }
    }

    public static class DbHelper
    {
        public static DateTime? GetNullableDate(this IDataRecord reader, string key)
        {
            if (reader[key] == DBNull.Value) {  return null; }

            try { return (DateTime) reader[key]; }
            catch (InvalidCastException) { }

            DateTime d;
            if (DateTime.TryParse(reader[key].ToString(), out d)) { return d; }

            throw new FormatException($"FAIL to retrieve Date: key = {key}, data = {reader[key]}");
            //return null;
        }

        public static long? GetNullableInteger(this IDataRecord reader, string key)
        {
            if (reader[key] == DBNull.Value) { return null; }

            try { return (long) reader[key]; }
            catch (InvalidCastException) { }

            long v;
            if (long.TryParse(reader[key].ToString(), out v)) { return v; }
            
            throw new FormatException($"FAIL to retrieve integer: key = {key}, data = {reader[key]}");
            //return null;
        }

        public static char? GetNullableChar(this IDataRecord reader, string key)
        {
            if (reader[key] == DBNull.Value) { return null; }

            try { return (char)reader[key]; }
            catch (InvalidCastException) { }

            char v;
            if (char.TryParse(reader[key].ToString(), out v)) { return v; }

            throw new FormatException($"FAIL to retrieve integer: key = {key}, data = {reader[key]}");
        }

        public static bool? GetNullableBool(this IDataRecord reader, string key)
        {
            if (reader[key] == DBNull.Value) { return null; }

            try { return (bool)reader[key]; }
            catch (InvalidCastException) { }

            bool v;
            if (bool.TryParse(reader[key].ToString(), out v)) { return v; }
            
            throw new FormatException($"FAIL to retrieve integer: key = {key}, data = {reader[key]}");
        }
    }

    public static class SecuredAdminPageEx
    {
        /// <summary>
        /// Redirect to our admin error page that describes
        /// the required privilege.
        /// </summary>
        /// <param name="sRequired">
        /// Description of requirement.
        /// </param>
        public static string GetRedirectRequestorUrl(string sRequired)
        {
            return ("~/LOAdmin/AdminErrorPage.aspx?required=" + sRequired);
        }
        public static string GetRedirectRequestorUrl(E_InternalUserPermissions eOpt)
        {
            return GetRedirectRequestorUrl(eOpt.ToString());
        }

        /// <summary>
        /// Response for web method attempting an unauthorized action.
        /// </summary>
        /// <param name="url">Redirect URL (App relative)</param>
        public static void RespondWith403Forbidden(string url)
        {
            var context = HttpContext.Current;
            context.Response.Clear();
            context.Response.StatusCode = 403;
            context.Response.RedirectLocation = VirtualPathUtilityEx.ToAbsolute(url);
        }
        public static void RepondWith400BadRequest(string url)
        {
            var context = HttpContext.Current;
            context.Response.Clear();
            context.Response.StatusCode = 400;
            context.Response.RedirectLocation = VirtualPathUtilityEx.ToAbsolute(url);
        }

        /// <summary>
        /// Check CurrentPrincipal has required privilege
        /// </summary>
        /// <returns>The error page URL (App relative) or null if have required privilege</returns>
        public static string GetAccessDeniedUrlIfNoPermission(E_InternalUserPermissions[] requiredPermissions)
        {
            var principal = Thread.CurrentPrincipal as InternalUserPrincipal;
            if (principal == null) return GetRedirectRequestorUrl("Authentication");

            var permissions = new InternalUserPermissions {Opts = principal.Permissions};

            return requiredPermissions.Where(required => !permissions.Can(required)).Select(GetRedirectRequestorUrl).FirstOrDefault();
        }

        public static bool HasPermissionToDo(E_InternalUserPermissions eOpt)
        {
            var principal = (Thread.CurrentPrincipal as InternalUserPrincipal);

            var permissions = new InternalUserPermissions();
            if (principal != null) { permissions.Opts = principal.Permissions; }

            return permissions.Can(eOpt);
        }
        public static void EnforcePermission(E_InternalUserPermissions eOpt)
        {
            if (!HasPermissionToDo(eOpt))
            {
                throw new UnauthorizedAccessException($"No permission [{eOpt}]");
                //  AuthenticationException InvalidCredentialException UnauthorizedAccessException or 403
            }
        }

        /// <summary>
        /// Checks if the user is missing any of the listed permissions and, if so, redirects the user 
        /// to the error page.
        /// </summary>
        /// <param name="permissions">The list of permissions to check.</param>
        /// <returns>True if the user is missing any of the specified permissions, false otherwise.</returns>
        public static bool IsMissingPermission(E_InternalUserPermissions[] permissions)
        {
            var url = SecuredAdminPageEx.GetAccessDeniedUrlIfNoPermission(permissions);
            if (string.IsNullOrEmpty(url)) return false;

            SecuredAdminPageEx.RespondWith403Forbidden(url);
            return true;
        }
        public static bool IsMissingPermission(E_InternalUserPermissions permission)
        {
            var url = SecuredAdminPageEx.GetAccessDeniedUrlIfNoPermission(new[]{permission});
            if (string.IsNullOrEmpty(url)) return false;

            SecuredAdminPageEx.RespondWith403Forbidden(url);
            return true;
        }
    }
}
