﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataImport.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.DataImport"%>

<%@ Register TagPrefix="uc" TagName="header" Src="common/header.ascx" %>
<%@ Register TagPrefix="uc" TagName="HeaderNav" Src="../common/HeaderNav.ascx" %>
<%@ Register TagPrefix="cc"  Namespace="LendersOffice.Common.Controls"   Assembly="LendersOfficeLib" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <title>Data Importer</title>
    <style type="text/css">
    .custom-combobox {
        position: relative;
        display: inline-block;
        width: 200px;
    }
    .custom-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
        /* support: IE7 */
        *height: 1.7em;
        *top: 0.1em;
    }
    .custom-combobox-input {
        margin: 0;
        padding: 0.3em;
    }
  
    #import-settings-dialog { 
        position: absolute;
        top: 0;
        right: 0;
        z-index: 999;
        background-color: White;
        padding: 10px;
    }
  
    .modal {
        display:    none;
        position:   fixed;
        z-index:    1000;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, .8 )  
                    50% 50% 
                    no-repeat;
    }

    body.loading {
        overflow: hidden;   
    }

    body.loading .modal {
        display: block;
    }

    .noclose .ui-dialog-titlebar-close
    {
        display:none;
    }
    #ShowSettings { 
        float: right;
    }
    .main { 
        padding: 10px;
    }
    #AvailableFieldsContainer {
        height: 400px !important;
        max-height: 400px;
    }
    #AvailableFields thead th {
        text-align: left;
    }
    #AvailableFields tbody td {
        white-space: pre-wrap;
        word-break: break-all;
        word-wrap: break-word;
    }
    .ErrorMsg {
        color:red;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <UC:HEADER id=Header1 runat="server">
    </UC:Header><UC:HEADERNAV id=HeaderNav1 runat="server">
		<MenuItem URL="~/LOAdmin/main.aspx" Label="Main">
        </MenuItem>
		<MenuItem URL="~/LOAdmin/DataImport.aspx" Label="Import Data From Excel File">
        </MenuItem>
	</UC:HeaderNav>
    <asp:HiddenField runat="server" ID="FileKey" />
    
    <div class="main ui-widget">
    
        <asp:PlaceHolder runat="server" ID="InitialScript">
            <label>
                Import Destination</label>
            <asp:DropDownList runat="server" ID="ImportDestination">
            </asp:DropDownList>
            <a href="#" id="ShowFields" >View Available Fields</a>
            <br />
            <label>
                Destination Broker</label>
            <asp:DropDownList runat="server" ID="BrokerDDL">
            </asp:DropDownList>
            <br />
            <label>
                XLSX File
            </label>
            <asp:FileUpload runat="server" ID="ExcelFile" />
            
            <br />
            
            <asp:Button runat="server" ID="UploadBtn" Text="Continue..." OnClick="UploadFile_OnClick" />
            
            <div id="AvailableFieldsContainer" title="Available Fields">
                <table id="AvailableFields">
                    <thead>
                        <th>Field Id</th>
                        <th>Name</th>
                        <th>Can Match</th>
                        <th>Allows Update</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            
      <script type="text/javascript">
        (function ($j) {
    $j.widget("custom.combobox", {
        _create: function () {
            this.wrapper = $j("<span>")
                .addClass("custom-combobox")
                .insertAfter(this.element);

            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },

        _createAutocomplete: function () {
            var element = this.element,
                selected = element.children(":selected"),
                value = selected.val() ? selected.text() : "";
                

            this.input = $j("<input>")
                .appendTo(this.wrapper)
                .val(value)
                .attr("title", "")
                .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                .autocomplete({
                delay: 0,
                minLength: 0,
                source: $j.proxy(this, "_source"),
                change : function(){
                    $j(element).triggerHandler('change');
                }
            })
            /* plugin not present
            .tooltip({
                tooltipClass: "ui-state-highlight"
            })*/
;

            this.input.bind('autocompleteselect', function (event, ui) {
                ui.item.option.selected = true;
                $j(this).trigger("select", event, {
                    item: ui.item.option
                });
            });

            this.input.bind('autocompletechange', function (event, ui) {

                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }



                // Search for a match (case-insensitive)
                var value = this.input.val(),
                    valueLowerCase = value.toLowerCase(),
                    valid = false;
                this.element.children("option").each(function () {
                    if ($j(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });

                // Found a match, nothing to do
                if (valid) {
                    return;
                }

                // Remove invalid value
                this.input.val("")
                    .attr("title", value + " didn't match any item")
                /*.tooltip("open")*/;
                this.element.val("");
                this._delay(function () {
                    this.input /*.tooltip("close")*/
                    .attr("title", "");
                }, 2500);
                this.input.data("ui-autocomplete").term = "";
            });

            /* using delegate instead
            this._on( this.input, {
              autocompleteselect: function( event, ui ) {
                ui.item.option.selected = true;
                this._trigger( "select", event, {
                  item: ui.item.option
                });
              },

              autocompletechange: "_removeIfInvalid"
            });*/
        },

        _createShowAllButton: function () {
            var input = this.input,
                wasOpen = false;

            $j("<a>")
                .attr("tabIndex", -1)
                .attr("title", "Show All Items")
            //.tooltip()
            .appendTo(this.wrapper)
                .button({
                icons: {
                    primary: "ui-icon-triangle-1-s"
                },
                text: false
            })
                .removeClass("ui-corner-all")
                .addClass("custom-combobox-toggle ui-corner-right")
                .mousedown(function () {
                wasOpen = input.autocomplete("widget").is(":visible");
            })
                .click(function () {
                input.focus();

                // Close if already visible
                if (wasOpen) {
                    return;
                }

                // Pass empty string as value to search for, displaying all results
                input.autocomplete("search", "");
            });
        },

        _source: function (request, response) {
            var matcher = new RegExp($j.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $j(this).text();
                if (this.value && (!request.term || matcher.test(text))) return {
                    label: text,
                    value: text,
                    option: this
                };
            }));
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });
})(jQuery);
    </script>
        <script type="text/javascript">
        $j(function(){
            function callWebMethod(name, args, success, error){
                callWebMethodAsync({
                    type: "POST",
                    url:  name, 
                    data: JSON.stringify(args),
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(msg) {
                        success(msg.d);
                    },
                    error: function(){ 
                        error();
                    }
                });   
             }
             
        
            var $brokerDdl =  $j('#BrokerDDL'), 
                $importDestination = $j('#ImportDestination'),
                $availableFields = $j('#AvailableFields');
                
            $brokerDdl.combobox();
            $importDestination.combobox();
            
            $j('#AvailableFieldsContainer').dialog({
                'modal' : true,
                'autoOpen': false
            });

            $('#AvailableFieldsContainer').dialog({
                width: 480,
                dialogClass: "LQBDialogBox"
            });

            
            $j('#ShowFields').click(function(){
                $j('#AvailableFieldsContainer').dialog('open');
                return false;
            });
            $importDestination.change(function(){
                if (this.value.length == 0){
                    $availableFields.find('tbody').empty();
                    return;
                }
                var data = { 'id' : this.value };
                callWebMethod('DataImport.aspx/GetAvailableFields', data, 
                    function(d){
                        var i = 0, l = d.length, item, html = [];
                        var h = hypescriptDom;
                        $availableFields.find('tbody').empty().append(
                            d.map(function(item) {
                                return h("tr", {}, [
                                    h("td", {}, item.FieldId),
                                    h("td", {}, item.Name),
                                    h("td", {}, item.CanMatch != null && String(item.CanMatch)),
                                    h("td", {}, item.CanBeDestiation != null && String(item.CanBeDestiation))
                                ])
                            })
                        );
                    },
                    function (e){
                        alert('Failure');
                    });
            });
        });
    </script>
    </asp:PlaceHolder>
        
        <asp:PlaceHolder ID="LookupResult" runat="server">
            <span runat="server" id="ImportOverfiew"></span>
            <ul runat="server" id="Errors" class="ErrorMsg">
            </ul>
            <ul runat="server" id="Warnings">
            </ul>
            <input type="button" runat="server" runat="server"  value="Import" id="ImportFinalizeVisible" />
            <asp:Button style="display:none" runat="server" UseSubmitBehavior="false" ID="ImportFinalize"  Text="Import (May take a long time)"  OnClick="ImportFinalize_OnClick" />
            <asp:Panel ID="ImportWarning" runat="server" Visible="false" ForeColor="Red">Please correct errors before importing. <a href="dataimport.aspx">[Try Again]</a></asp:Panel>
            <cc:CustomHtmlControl runat="server" id="HtmlTable"></cc:CustomHtmlControl>
            <div id="Div1" title="Please Wait..">
            </div>
            <div id="Success" runat="server">Migration successful</div>
            <script type="text/javascript">
                
                $j(function(){
                    $j('#ImportFinalizeVisible').click(function(){
                            if (confirm('Are you sure? No going back.')){
                            $j(this).hide();
                            $j('#Div1').dialog({modal:true});
                            setTimeout(function(){
                                $j('#ImportFinalize').click();
                            }, 0);
                        }
                    });
                });
                
            </script>
        </asp:PlaceHolder>
        
    </div>
 


       </form>
</body>
</html>
