﻿<%@ Page language="C#" Codebehind="LoanFieldSecurity.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.LoanFieldSecurity"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Loan Per-Field Security Page" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">
<style>
.del-col{
    width:4%;
}
.field-id-col{
    width:22%;
}
.description-col{
    width:20%;
}
.lockdesk-col{
    width:11%;
}
.closerperm-col{
    width:9%;
}
.accountperm-col{
    width:12%;
}
.investorinfo-col{
    width:13%;
}
.lastedited-col{
    width:9%;
}
.text-vertical{
    resize: vertical;
}
</style>
    <div class="container-fluid"><div class="row"><div class="panel panel-default"><div class="panel-body">
         <div class="ng-cloak" ng-app="app" ng-controller="LSManager as lm">
            <div ng-hide="!isLoadding">loading&hellip;</div>
            <div ng-show="!isLoading">
                <form runat="server">
                <div class="form-group">
                    <span class="clipper">
                        <a class="btn btn-primary" ng-click="lm.OnAddNew($event)"
                        data-toggle="modal" data-target="#addModal">Add Single Field</a>
                    </span>
                    <span class="clipper">
                        <a class="btn btn-default"
                        data-toggle="modal" data-target="#parseModal">Parse Multiple Fields</a>
                    </span>
                    <span class="clipper">
                        <asp:Button CssClass="btn btn-default" ID="ClearCache" runat="server" Text="Force Cache Clear" OnClick="ClearCache_OnClick" UseSubmitBehavior="false"/>
                    </span>
                    <span class="clipper">
                        <asp:Button CssClass="btn btn-default" ID="ExportCsv" runat="server" Text="Export list to CSV" OnClick="ExportCsv_OnClick" UseSubmitBehavior="false"/>
                    </span>
                </div>
                </form>
                <div class="alert alert-error alert-dismissible hide" role="alert" id="ErrorPanel" runat="server">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Error Message!</strong> <span runat="server" id="ErrMsg">Numquam quos fuga quam suscipit sapiente perferendis magnam. </span>
                </div>
                <div class="form-group">
                     <p>
                        * Note: This is a temporary page to create a preliminary secure fields list.
                        After the first production release, this page will be disabled and any field
                        addition will be done by David using direct DB access in order to keep localhost/test/prod synchronized.
                    </p>
                    <p>
                        The localhost and dev field lists are based on the same DB content;
                        however, once the content has been loaded into the static field permission hashlist,
                        it will only be updated when clearing the cache from this page or issuing an iisreset command.
                        Example: Clearing the localhost cache won't affect the dev one.
                    </p>
                </div>
                <table class="table table-striped table-condensed table-layout-fix">
                    <thead>
                        <tr>
                            <th class="del-col">&nbsp;</th>
                            <th class="field-id-col"><a sort-handle="lm.entrySort" key="sFieldId">Field ID</a></th>
                            <th class="description-col"><a sort-handle="lm.entrySort" key="sDescription">Description</a></th>
                            <th class="lockdesk-col"><a sort-handle="lm.entrySort" key="bLockDeskPerm">LockDesk Perm</a></th>
                            <th class="closerperm-col"><a sort-handle="lm.entrySort" key="bCloserPerm">Closer Perm</a></th>
                            <th class="accountperm-col"><a sort-handle="lm.entrySort" key="bAccountantPerm">Accountant Perm</a></th>
                            <th class="investorinfo-col"><a sort-handle="lm.entrySort" key="bInvestorInfoPerm">Investor Info Perm</a></th>
                            <th class="lastedited-col"><a sort-handle="lm.entrySort" key="LastEditedDSort">Last Edited</a></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="e in lm.LSs | orderBy:lm.entrySort.by:lm.entrySort.desc track by e.sFieldId">
                            <td><a ng-click="lm.OnDelete(e,$event)" data-toggle="modal" data-target="#deleteModal">delete</a>
                            </td><td ng-bind="e.sFieldId">
                            </td><td ng-bind="e.sDescription">
                            </td><td>
                                <div ng-show="e.bLockDeskPerm">Yes</div>
                                <div ng-hide="e.bLockDeskPerm">No</div>
                            </td><td>
                                <div ng-show="e.bCloserPerm">Yes</div>
                                <div ng-hide="e.bCloserPerm">No</div>
                            </td><td>
                                <div ng-show="e.bAccountantPerm">Yes</div>
                                <div ng-hide="e.bAccountantPerm">No</div>
                            </td><td>
                                <div ng-show="e.bInvestorInfoPerm">Yes</div>
                                <div ng-hide="e.bInvestorInfoPerm">No</div>
                            </td><td ng-bind="e.LastEditedD">
                                 </td></tr></tbody></table>

                     <!--Confirm Delete Message Dialog-->
                     <div id="deleteModal" class="modal v-middle fade"><div class="modal-dialog modal-lg"><div class="modal-content">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            <h4 class="modal-title">
                                Confirm Delete Field
                            </h4>
                        </div>
                        <form name="deleteForm" class="form-group-sm form-adjust-mb" ng-submit="lm.Delete($event)">
                            <div class="modal-body">
                                Do you want to remove Field ID "<span ng-bind="lm.activeLS.sFieldId"></span>"?
                            </div>
                            <div class="modal-footer">
                                <span class="clipper"><a class="btn btn-default" data-dismiss="modal">Cancel</a></span>
                                <span class="clipper"><button type="submit" class="btn btn-primary">OK</button></span>
                            </div>
                        </form>
                    </div></div></div>


                 <!--Add New Dialog-->
                     <div id="addModal" class="modal v-middle fade"><div class="modal-dialog modal-lg"><div class="modal-content">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            <h4 class="modal-title">
                                Add Field
                            </h4>
                        </div>
                        <form name="addForm" class="form-horizontal form-horizontal-left" ng-submit="lm.Add($event)">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="control-label col-xs-2">Field ID: <span class="text-danger">*</span></label>
                                    <div class="col-xs-4">
                                        <input class="form-control" ng-model="lm.activeLS.sFieldId"
                                            data-lqb-field="sFieldId" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-xs-2">Description:</label>
                                    <div class="col-xs-10">
                                        <input class="form-control" ng-model="lm.activeLS.sDescription"
                                            data-lqb-field="sDescription"/>
                                    </div>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" ng-model="lm.activeLS.bLockDeskPerm"
                                            data-lqb-field="bLockDeskPerm"/>
                                    Has LockDesk Perm?
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" ng-model="lm.activeLS.bCloserPerm"
                                            data-lqb-field="bCloserPerm"/>
                                    Has Closer Perm?
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" ng-model="lm.activeLS.bAccountantPerm"
                                            data-lqb-field="bAccountantPerm"/>
                                    Has Accountant/Finance Perm?
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" ng-model="lm.activeLS.bInvestorInfoPerm"
                                            data-lqb-field="bInvestorInfoPerm"/>
                                    Has Investor Info Perm?
                                  </label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <span class="clipper"><a class="btn btn-default" data-dismiss="modal">Close</a></span>
                                <span class="clipper"><button type="submit" class="btn btn-primary">Save</button></span>
                            </div>
                        </form>
                    </div></div></div>

                <!--Parse Multiple Dialog-->
                     <div id="parseModal" class="modal v-middle fade"><div class="modal-dialog modal-lg"><div class="modal-content">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                            <h4 class="modal-title">
                                Parse Multiple Fields
                            </h4>
                        </div>
                        <form name="deleteForm" class="form-group-sm form-adjust-mb" ng-submit="lm.Parse($event)">
                            <div class="modal-body">
                                <label>Paste multiple Excel/CSV entries: <span class="text-danger">*</span></label>
                                <textarea class="form-control text-vertical" rows="7" ng-model="lm.parseMult"
                                            data-lqb-field="parseMult" required></textarea>
                            </div>
                            <div class="modal-footer">
                                <span class="clipper"><a class="btn btn-default" data-dismiss="modal">Close</a></span>
                                <span class="clipper"><button type="submit" class="btn btn-primary">Save</button></span>
                            </div>
                        </form>
                    </div></div></div>


                </div>
    </div></div></div></div></div>
</asp:Content>