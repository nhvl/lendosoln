using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;
using LendersOfficeApp.los.admin;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;
using LendersOffice.ObjLib.LoanFileCache;

namespace LendersOfficeApp.LOAdmin
{
	/// <summary>
	/// This class handles the displaying of exceptions that occurred when an attempt to update the cache table failed.
	/// The update can be rerun directly from the table after the problem has been fixed.
	/// </summary>
	public partial class CacheTableRequestErrors : SecuredAdminPage
	{
        #region API
        [WebMethod]
        public static IEnumerable<LoanCacheUpdateRequest> Get()
        {
            return LoanCacheUpdateRequest.GetTop100FromEachDatabase();
        }
        [WebMethod]
        public static string Delete(LoanCacheUpdateRequest[] rs)
        {
            try
            { 
                foreach (var r in rs)
                {
                    LoanCacheUpdateRequest.Delete(r);
                }
            }
            catch (SqlException   e) { return e.Message; }
            catch (CBaseException e) { return e.Message; }

            return null;
        }
        /// <summary>
        /// Re-runs the specified cache requests.
        /// </summary>
        /// <param name="rs">The requests to run.</param>
        /// <param name="numErrorsBefore">No longer used.</param>
        /// <returns></returns>
        [WebMethod]
        public static object Run(LoanCacheUpdateRequest[] rs, int numErrorsBefore)
        {
            // We can't trust the number from the client because it can be smaller
            // than the total number of records across all of the databases.
            // This number also may not correctly indicate the number of "errors"
            // because a record is inserted into / removed from this table during
            // normal loan saves.
            int errorCountPriorToRunning = LoanCacheUpdateRequest.GetCacheUpdateRequestCount();
            int numSelected = rs.Length;
            if (numSelected == 0 || errorCountPriorToRunning == 0)
            {
                return null;
            }

            var brokerEnabledById = new Dictionary<Guid, bool>();
            var loanValidById = new Dictionary<Guid, bool>();

            foreach (var r in rs)
            {
                LoanCacheUpdateRequest.Rerun(r, brokerEnabledById, loanValidById);
            }

            int numErrorsAfter = LoanCacheUpdateRequest.GetCacheUpdateRequestCount();
            int successes = errorCountPriorToRunning - numErrorsAfter;
            int failures = numSelected - successes;

            //redirect to page showing results
            return new
            {
                successes = successes,
                failures = failures,
                redirectUrl = string.Format("CacheTableRequestResults.aspx?success={0}&failure={1}", successes, failures),
            };
        }
        [WebMethod]
        public static object RunAll()
        {
            var rs = LoanCacheUpdateRequest.GetTop1000FromEachDatabase().ToArray();
            return Run(rs, rs.Length);
        } 
        #endregion

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.CacheTableRequestErrors.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        protected void PageLoad(object sender, EventArgs e)
        {
            RegisterResources();
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
            notOnlySAE();
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
	}
}
