﻿using System;
using System.Reflection;

namespace LendersOfficeApp.LOAdmin
{
    public partial class JSOnlyHotfix : LendersOffice.Admin.SecuredAdminPage
    {

        /// <summary>
        /// Resets the IncludeVersion appended to JS and CSS files to force reloading them on the client end.
        /// Intended to be used only when midday hotfixing javascript / css files only, but no harm done if used repeatedly.
        /// </summary>
        protected void ResetJSVersion(object sender, EventArgs e)
        {
            ResetBasePageIncludeVersion();
            loadmin.ResetIncludeVerion();
        }

        static void ResetBasePageIncludeVersion()
        {
            var basePageType = typeof(LendersOffice.Common.BasePage);
            var includeVersionFieldInfo = basePageType.GetField("IncludeVersion", BindingFlags.Static | BindingFlags.NonPublic);
            includeVersionFieldInfo.SetValue(null, Guid.NewGuid().ToString("N"));
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            this.GitSha1Hash.InnerText = ((AssemblyInformationalVersionAttribute)Assembly.GetAssembly(typeof(DataAccess.CPageData))
                .GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false)[0])
                .InformationalVersion;
        }

        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion

    }
}
