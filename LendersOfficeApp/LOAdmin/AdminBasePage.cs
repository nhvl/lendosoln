using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Admin
{
    /// <summary>
    /// All admin pages should inherit from this base to implement
    /// admin page security.  Each user is scrutinized, and if not
    /// internal, then kicked out.  If internal and without the
    /// proper permissions, then kicked out.  Otherwise, pass on
    /// through normally.
    /// </summary>

    public class SecuredAdminPage : LendersOffice.Common.BaseServicePage
    {
        private InternalUserPermissions m_Permissions;
        private String                  m_DisplayName;
        private Guid                         m_UserId;

        /// <summary>
        /// Every admin page that inherits from this base page
        /// should implement this query method to specify the
        /// permission set required to access this page.
        /// </summary>
        /// <returns>
        /// List of required permissions.
        /// </returns>

        protected virtual E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return an empty set.  Overload to provide
            // a page specific permission list.

            return new E_InternalUserPermissions[ 0 ];
        }

        /// <summary>
        /// We provide default access-denied handling through
        /// this method.  Override to specialize handling.
        /// </summary>
        /// <param name="eOpt">
        /// Required permission.
        /// </param>

        protected virtual void OnFailedToAccessAdminPage( E_InternalUserPermissions eOpt )
        {
            // By default, we punt and redirect to an
            // error page.  The specialized admin page
            // can override this implementation and
            // do something else.

            RedirectRequestor( eOpt.ToString() );
        }

        /// <summary>
        /// Search current user's permissions against the requested
        /// access privilege.
        /// </summary>
        /// <param name="eOpt">
        /// Permission option to check for.
        /// </param>

        protected void RequirePermission( E_InternalUserPermissions eOpt )
        {
            // Lookup using acquired permission set.

            if( m_Permissions.Can( eOpt ) == false )
            {
                OnFailedToAccessAdminPage( eOpt );
            }
        }

        /// <summary>
        /// Check current user's permissions against the requested
        /// access privilege.
        /// </summary>
        /// <param name="eOpt">
        /// Permission option to check for.
        /// </param>
        /// <returns>
        /// True is permissible.
        /// </returns>

        protected bool HasPermissionToDo( E_InternalUserPermissions eOpt )
        {
            // Lookup using acquired permission set.

            if( m_Permissions.Can( eOpt ) == true )
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Redirect to our admin error page that describes
        /// the required privilege.
        /// </summary>
        /// <param name="sRequired">
        /// Description of requirement.
        /// </param>

        protected void RedirectRequestor( String sRequired )
        {
            // Send to the error page.

            Response.Redirect( "~/LOAdmin/AdminErrorPage.aspx?required=" + sRequired , true );
        }

        protected InternalUserPrincipal CurrentPrincipal
        {
            get { return Thread.CurrentPrincipal as InternalUserPrincipal; }
        }
        /// <summary>
        /// Handling page initialization.  We verify the internal
        /// user credentials and permission set here.
        /// </summary>
        /// <param name="e">
        /// Generic events.
        /// </param>

        override protected void OnInit( EventArgs e )
        {
            // Load the current principal and check the
            // the permissions.

            InternalUserPrincipal principal = this.CurrentPrincipal;

            if( principal == null )
            {
                RedirectRequestor( "IsInternalUser" );
            }

            m_DisplayName = principal.DisplayName;
            m_UserId      = principal.UserId;
            m_Permissions.Opts = principal.Permissions;


            // Get the page's required permissions and do the
            // compare against the user's set.

            foreach( E_InternalUserPermissions required in GetRequiredPermissions() )
            {
                if( m_Permissions.Can( required ) == false )
                {
                    OnFailedToAccessAdminPage( required );

                    break;
                }
            }

            // Delegate to the base class to continue
            // initialization.

            base.OnInit( e );
        }

		//Checks to see if SAE permission is the only permission they have,
		//if so, redirect to error page, saying Requires: More Permissions
		//Only needs to be used on pages that don't have an explicit getRequiredPermissions() override
		//The call is placed in the onInit() method
		protected void notOnlySAE()
		{
			String sPermission = m_Permissions.Opts;
			for(int i =0; i<sPermission.Length; i++)
			{
				if(sPermission.ToCharArray()[i] == '1' && i != (int)E_InternalUserPermissions.IsSAE)
					return;
			}
			
			RedirectRequestor( "More Permissions" );
			
		}

        /// <summary>
        /// Construct default.
        /// </summary>

        public SecuredAdminPage()
        {
            // Initialize members.

            m_Permissions = new InternalUserPermissions();
            m_UserId      = Guid.Empty;
        }

        public override string StyleSheet
        {
            get { return Tools.VRoot + "/css/stylesheet.css"; }
        }

    }

}