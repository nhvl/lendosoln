<%@ Page language="C#" Codebehind="RateSheetVersion2.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.SAE.RateSheetVersion2"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Acceptable Ratesheets - LendingQB" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">
<style>
    .checkbox-col             { width: 20px; }
    .edit-col                 { width: 35px; }
    .del-col                  { width: 48px; }
    .versions-col             { width: 60px; }
    .output-filename          { width: 30%;  }
    .rs-col                   { width: 35px; }
    .deployment-type          { width: 20%;  }
    .both-map-bot             { width: 15%;  }
    .latest-effective-datetime{ width: 17%;  }
    [ag-grid] {
        height: 724px;
        height: calc(100vh - 242px);
        min-height: 500px;
    }
    /* IE does not issue change event, AngularJS ng-change not issue */
    input[type=text]::-ms-clear {
        display: none;
    }

</style>
<div class="container-fluid"><div class="panel panel-default"><div class="panel-body">
    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a>Acceptable Ratesheets</a></li>
        <li role="presentation"><a href="InvestorXlsFileList.aspx">Investor Rate Sheet List</a></li>
    </ul>
    <div class="tab-content"><div role="tabpanel" class="tab-pane fade in active">
        <div ng-app="app" ng-controller="DlManager as vm">
            <p></p>
            <div class="clearfix form-sm">
                <div class="pull-right">
                    <input ng-model="vm.searchSpec.LpeAcceptableRsFileId"
                        ng-model-options="{ updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } }"
                        ng-change="vm.onFilterChange()"
                        type="text" class="form-control" placeholder="Output Filename">
                </div>
                <form runat="server" class="form-inline">
                    <a class="btn btn-default" ng-click="vm.create()">Add New</a>
                    <button ng-click="vm.duplicate()" type="button"
                        ng-disabled="vm.numberOfSelected != 1"
                        class="btn btn-default">Duplicate</button>

                    <div class="form-group"></div>
                    <div class="form-group">
                        <label>Acceptable Ratesheet File
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ImportFile" Display="Dynamic" CssClass="text-danger" Text="*"/>
                        </label>
                        <asp:FileUpload ID="ImportFile" runat="server" style="display:inline-block;" required/>
                    </div>
                    <asp:Button runat="server" OnClick="Import_OnClick" Text="Import" CssClass="btn btn-default"/>
                </form>
                <p></p>
                <div class="alert alert-danger" ng-bind="vm.importError" ng-hide="!vm.importError"></div>
            </div>

            <div class="clearfix">
                <span class="clipper"><button ng-click="vm.batchEdit()" type="button"
                    ng-disabled="vm.selectedPercent <= 0"
                    class="btn btn-default">Batch Edit</button></span>
                <span class="clipper"><button ng-click="vm.batchDelete()" type="button"
                    ng-show="vm.selectedPercent > 0"
                    class="btn btn-danger">Delete</button></span>
            </div>

            <div ag-grid="vm.gridOptions" class="ag-fresh"></div>

            <!--
            <div class="table-responsive"><table class="table table-striped table-condensed table-layout-fix">
                <thead><tr>
                    <th class="checkbox-col"><input type="checkbox" ng-click="vm.onSelectAll($event)"
                        checkbox-indeterminate="vm.selectedPercent"/></th>
                    <th class="edit-col"></th>
                    <th class="del-col"></th>
                    <th class="versions-col"></th>
                    <th class="output-filename          "><a sort-handle="vm.entrySort" key="LpeAcceptableRsFileId"       >Output Filename          </a></th>
                    <th class="deployment-type          "><a sort-handle="vm.entrySort" key="DeploymentType"              >Deployment Type          </a></th>
                    <th class="both-map-bot             "><a sort-handle="vm.entrySort" key="IsBothRsMapAndBotWorking"    >Both Map &amp; Bot are OK</a></th>
                    <th class="latest-effective-datetime"><a sort-handle="vm.entrySort" key="LatestEffectiveDateTime"     >Latest Effective DateTime</a></th>
                    <th class="latest-expired           "><a sort-handle="vm.entrySort" key="IsExpirationIssuedByInvestor">Latest Expired?          </a></th>
                </tr></thead>
                <tbody><tr ng-repeat="e in vm.entries | filter:vm.searchSpec | orderBy:vm.entrySort.by:vm.entrySort.desc track by e.LpeAcceptableRsFileId"
                           ng-class="{info: e.isSelected}">
                    <td><input type="checkbox" ng-model="e.isSelected" ng-change="vm.onEntrySelect()"/></td>
                    <td><a ng-click="vm.edit        (e)">edit    </a></td>
                    <td><a ng-click="vm.deleteFile  (e)">delete  </a></td>
                    <td><a ng-click="vm.loadVersions(e)">versions</a></td>
                    <td><span ng-bind="e.LpeAcceptableRsFileId"                  select-when-clicked></span></td>
                    <td><span ng-bind="e.DeploymentType"                         select-when-clicked></span></td>
                    <td><input type="checkbox" ng-model="e.IsBothRsMapAndBotWorking" disabled></td>
                    <td><span ng-bind="e.LatestEffectiveDateTime | date:'short'    " select-when-clicked></span></td>
                    <td><span ng-bind="e.IsExpirationIssuedByInvestor ? 'Expired' : ''" select-when-clicked></span></td>
                </tr></tbody>
            </table></div>
            -->

            <file-editor    ng2-var="vm.fileEditor"   on-close="vm.reload()"></file-editor>
            <ir-file-editor ng2-var="vm.irFileEditor" on-close="vm.reload()"></ir-file-editor>

            <fv-list ng2-var="vm.fvList" on-fv-change="vm.onFvListFvChange()"></fv-list>
        </div>
    </div></div>
</div></div></div>
</asp:Content>
