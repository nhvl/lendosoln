<%@ Page language="c#" Codebehind="RateSheetVersion.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.SAE.RateSheetVersion" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>RateSheetVersion</title>

	</HEAD>
    		<script type="text/javascript">

			function onNewRsFile()
			{
				onRunDialog('/loadmin/SAE/EditRateSheetFileId.aspx');
			}

			function onEditRsFile(ratesheetId)
			{
				
				onRunDialog('/loadmin/SAE/EditRateSheetFileId.aspx?ratesheetId=' + ratesheetId);
			}
			
			function onEditInvestorXlsFile(xlsFileId)
			{
				onRunDialog('/loadmin/SAE/EditInvestorXlsFile.aspx?xlsFileId=' + xlsFileId);
			}
			
			function onEditRsFileVersion(ratesheetId, version)
			{
				onRunDialog('/loadmin/SAE/EditRateSheetFileVersion.aspx?ratesheetId=' + ratesheetId +'&version='+version);
			}
			
			function onCreateRsFileVersion()
			{
			
				if( <%= AspxTools.JsBool(CurrentRsFile != null) %> )
				{
					
					onRunDialog('/loadmin/SAE/EditRateSheetFileVersion.aspx?ratesheetId=' + <%=AspxTools.JsString(CurrentRsFile==null ? CurrentRsFile : Regex.Replace(CurrentRsFile, @"\\", "\\\\"))%>);
				}		
    		}

			function onBatchEdit()
			{
				var collection = document.getElementById("templateid");
				var sIds = "";
				
				for(var i=0; i<collection.length; i++)
				{
					if(collection[i].checked){
						sIds+= collection[i].value + ",";
					}
				}
				
				if (sIds == "")
				{
					alert('No loan program is selected.');
					return false;
				}
				
				TransferForm.RsIds.value = sIds;
				TransferForm.submit() ;
			
			}
			
			
			function onRunDialog( url )
			{
				showModal(url, null, null, null, function(arg){
					if (arg.OK) 
					{
						document.getElementById("m_btnRefresh").click();
						
					}
				});
			}

			function init()
			{
			}
			
			function checkAll(cb)
			{
				var collection = document.getElementById("templateid");
				var bchecked = cb.checked;
				$j('.cbTemplate:visible').prop('checked', bchecked); // Only perform on visible checkbox.
                /*
				if(collection != null)
				{
					for(var i=0;i<collection.length ; i++)
					{
						collection[i].checked = bchecked;
					}
				}
                */
				disableEnableBatchButton();
			}
			function disableEnableBatchButton()
			{
			    var collection = document.getElementsByClassName("cbTemplate");
			var batchButton = document.getElementById("m_batcheditFileVersion");
			
			if(collection.length)
			{
				var count = 0;
				for(var i = 0; i < collection.length; i++)
				{
					if(collection[i].checked)
						count++;
				}
				if(count>0)
				{
					if(batchButton)
						batchButton.disabled = false;
					return;
				}
			}
			if(batchButton)
				batchButton.disabled = true;
			}
			
			function onSearchClick()
			{
			    var val = $j('#searchFilterText').val();



			    var re = new RegExp(val, 'i');

			    $j('#m_Grid tr').each(function (index, value) {
			        if (index == 0)
			        {
			            return; // Skip header.
			        }
			        var text = $j(this).children("td").first().text();

			        var bShow = re.test(text);
			        if (bShow)
			        {
			            $j(this).show();
			        } else {
			            $j(this).hide();
			        }
			    });


			}

		</script>

	<body MS_POSITIONING="FlowLayout">
	<form id="TransferForm" action="RsBatchEdit.aspx" method="post" target="_blank">
		<input type="hidden" name="RsIds">
		</form>
		<form id="RateSheetVersion" method="post" runat="server">
			<TABLE style="WIDTH: 8in" cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<TD class="FormTable">
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 19px" cellSpacing="0" cellPadding="0">
							<TR>
								<TD><ml:EncodedLabel id="m_ErrorMessage" Runat="server" EnableViewState="False"></ml:EncodedLabel></TD>
							</TR>
							<TR>
								<TD align="left"><asp:button id="m_btnRefresh" runat="server" Text="Refresh" onclick="m_btnRefresh_Click"></asp:button><INPUT onclick="onNewRsFile();" type="button" value="Add new">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
									Acceptable Ratesheet&nbsp;&nbsp;&nbsp;File
									
									<asp:FileUpload  runat="server" ID="ImportFile" />
									<asp:Button runat="server"  Text="Import"  OnClick="Import_OnClick"/>
									<asp:TextBox runat="server" ID="ImportError"></asp:TextBox>
								</TD>
								<TD align="right"><a href="InvestorXlsFileList.aspx">Investor Rate Sheet List</a></TD>
							</TR>
						</TABLE>
						<HR style="WIDTH: 100%">
						<input id="m_batcheditFileVersion" onclick="onBatchEdit();" type="button" value="Batch Edit" disabled="true">
                        <hr />
                        Search <input type="text" id="searchFilterText" /> <input type="button" value="Search" onclick="onSearchClick()" />
                        <hr />
						<P><ML:COMMONDATAGRID id="m_Grid" runat="server" EnableViewState="true" OnItemDataBound="m_Grid_ItemDataBound" OnItemCommand="m_Grid_ItemCommand" AllowSorting="True" CellPadding="2" Width="100%" CssClass="DataGrid" AutoGenerateColumns="False">
								<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
								<HeaderStyle CssClass="GridHeader"></HeaderStyle>
								<Columns>
									
									<asp:BoundColumn DataField="LpeAcceptableRsFileId" HeaderText="Output Filename"></asp:BoundColumn>
									<asp:TemplateColumn>
										<ItemTemplate>
											<A href="#<%#AspxTools.HtmlString(GetXlsFileName(Container.DataItem))%>" 
												onclick='onEditInvestorXlsFile("<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "InvestorXlsFileId"))%>"); return false;'
												title='<%#AspxTools.HtmlString(GetXlsFileName(Container.DataItem))%>'
											>rs</A>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="DeploymentType" HeaderText="Deployment Type"></asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Both Map &amp; Bot are ok">
										<ItemTemplate>
                                            <input type="checkbox" onclick="return false;" disabled <%# AspxTools.HtmlString((bool)DataBinder.Eval(Container.DataItem, "IsBothRsMapAndBotWorking") ? "CHECKED" : "")%> />
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Latest Effective DateTime" ></asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Latest Expired?" />
									<asp:TemplateColumn>
										<ItemTemplate>
											<A href="#" onclick='onEditRsFile("<%#AspxTools.HtmlString(Regex.Replace(Server.UrlEncode((String)@DataBinder.Eval(Container.DataItem, "LpeAcceptableRsFileId")), @"\\", "\\\\"))%>"); return false;'>
												edit </A>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:ButtonColumn Text="drop" CommandName="DropRsFile"></asp:ButtonColumn>
									<asp:ButtonColumn Text="versions" CommandName="SetCurrentRsFile"></asp:ButtonColumn>
									<asp:TemplateColumn>
									<HeaderTemplate>
									
										<input type="checkbox" onclick="checkAll(this);" runat="server">
									</HeaderTemplate>
									<ItemTemplate>
										<INPUT id=templateid type=checkbox name=templateid onclick="disableEnableBatchButton();" value='<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "LpeAcceptableRsFileId"))%>' class="cbTemplate">
									</ItemTemplate>
								</asp:TemplateColumn>
								</Columns>
							</ML:COMMONDATAGRID></P>
					</TD>
				</TR>
			</TABLE>
			<asp:panel id="q_MessagePanel" Runat="server" Width="764px">
				<DIV style="CLEAR: both">
					<TABLE style="WIDTH: 100%">
						<TR style="PADDING-RIGHT: 0.5em; PADDING-LEFT: 0.5em; FONT-WEIGHT: bold; FONT-SIZE: 12px; PADDING-BOTTOM: 0.5em; COLOR: white; PADDING-TOP: 0.5em; FONT-FAMILY: Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #003366">
							<TD style="PADDING-RIGHT: 0.5em; PADDING-LEFT: 0.5em; PADDING-BOTTOM: 0.5em; PADDING-TOP: 0.5em" colSpan="3">Acceptable 
								Ratesheet File Version</TD>
							<TD align="right">
								
								<INPUT id="m_btnCreateRsFileVersion" onclick="onCreateRsFileVersion();" type="button" value="Create"></TD>
						</TR>
						</TR>
						<TR>
							<TD colSpan="6">
								<ml:CommonDataGrid id="m_RsFileVersions" runat="server" EnableViewState="true" AutoGenerateColumns="False" CssClass="DataGrid" Width="759px" CellPadding="2" AllowSorting="True" OnItemCommand="RsFileVersions_OnItemCommand" OnItemDataBound="RsFileVersions_ItemDataBound">
									<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
									<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
									<HeaderStyle CssClass="GridHeader"></HeaderStyle>
									<Columns>
										<asp:BoundColumn DataField="LpeAcceptableRsFileId" ReadOnly="True" HeaderText="Output Filename"></asp:BoundColumn>
										<asp:BoundColumn DataField="VersionNumber" ReadOnly="True" HeaderText="Version"></asp:BoundColumn>
										<asp:BoundColumn DataField="FirstEffectiveDateTime" HeaderText="First Effective"></asp:BoundColumn>
										<asp:BoundColumn DataField="LatestEffectiveDateTime" HeaderText="Latest Effective"></asp:BoundColumn>
										<asp:TemplateColumn HeaderText="Expired">
											<ItemTemplate>
                                                <input type="checkbox" onclick="return false; disabled" <%#AspxTools.HtmlString((bool)DataBinder.Eval(Container.DataItem, "IsExpirationIssuedByInvestor") ? "CHECKED >" : " >") %>/>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="EntryCreatedD" HeaderText="Created Date"></asp:BoundColumn>
										<asp:BoundColumn DataField="ModifiedD" HeaderText="Modified Date"></asp:BoundColumn>
										<asp:TemplateColumn>
											<ItemTemplate>
												<A href="#" onclick='onEditRsFileVersion("<%#AspxTools.HtmlString(Regex.Replace(Server.UrlEncode((String)DataBinder.Eval(Container.DataItem, "LpeAcceptableRsFileId")), @"\\", "\\\\"))%>","<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "VersionNumber"))%>" ); return false;'>
													edit </A>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:ButtonColumn Text="drop" CommandName="DropRsFileVersion"></asp:ButtonColumn>
									</Columns>
								</ml:CommonDataGrid></TD>
						</TR>
					</TABLE>
				</DIV>
			</asp:panel></form>
		</TR></TBODY>
		<DIV></DIV>
		</FORM>
		<SCRIPT language="javascript">
			document.getElementById("m_btnCreateRsFileVersion").disabled = <%= AspxTools.JsBool(CurrentRsFile == null ? true : false) %>;

		</SCRIPT>
	</body>
</HTML>