using System;
using System.Data;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using DataAccess;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;
using LendersOffice.Common;

namespace LendersOfficeApp.loadmin.SAE
{
	/// <summary>
	/// Summary description for EditRateSheetFileId.
	/// </summary>
	[Obsolete("This page is merged to AcceptableRsFile.aspx")]
	public partial class EditRateSheetFileId : LendersOffice.Admin.SecuredAdminPage
	{

		private   string m_ratesheetID;


		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsSAE
			};
		}
    
		private bool IsNewRecord
		{
			get { return m_ratesheetID == null; }
		}

		void InitDropDownList4XslFiles( long xslId )
		{
			string sSQL = "SELECT InvestorXlsFileId, InvestorXlsFileName from  INVESTOR_XLS_FILE ORDER BY InvestorXlsFileName" ;

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                ListItem item;
                while (reader.Read())
                {
                    long id = (long)reader["InvestorXlsFileId"];
                    string txt = (string)reader["InvestorXlsFileName"] + " [" + id.ToString() + "]";
                    m_ddlXlsFiles.Items.Add(item = new ListItem(txt, id.ToString()));
                    if (id == xslId)
                        item.Selected = true;

                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, sSQL, null, null, readHandler);
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
		


			m_ratesheetID = RequestHelper.GetSafeQueryString("ratesheetId");

            if (!string.IsNullOrEmpty(m_ratesheetID))
            {
                m_ratesheetID = m_ratesheetID.Replace("&amp;", "&");
            }

			if( !Page.IsPostBack ) 
			{

				AcceptableRsFile rsFile       = IsNewRecord ? new AcceptableRsFile() : new AcceptableRsFile( m_ratesheetID ) ;
				m_txtRsFileID.Text            = rsFile.RatesheetFileId;
				//m_txtDeployType.Text          = rsFile.DeploymentType;

				for (int i=0; i<m_ddlDeployType.Items.Count; i++) 
					if( m_ddlDeployType.Items[i].Value == rsFile.DeploymentType )
					{
						m_ddlDeployType.SelectedIndex = i;
						break;
					}

				InitDropDownList4XslFiles( rsFile.InvestorXlsFileId );

				m_cbUsingForMapAndBot.Checked = rsFile.UseForBothMapAndBot;
			}

			lblPageTitle.Text       = IsNewRecord ? "Create Acceptable Ratesheet File" : "Modify Acceptable Ratesheet File";
			m_txtRsFileID.ReadOnly  = !IsNewRecord;

		}

		protected void OKClick(object sender, System.EventArgs e)
		{
			if( !Page.IsValid )
				return;


			try
			{
				AcceptableRsFile rsFile    = IsNewRecord ? new AcceptableRsFile() : new AcceptableRsFile( m_ratesheetID ) ;

				rsFile.DeploymentType      = m_ddlDeployType.SelectedItem.Value;
				rsFile.UseForBothMapAndBot = m_cbUsingForMapAndBot.Checked;
				rsFile.InvestorXlsFileId   = long.Parse( m_ddlXlsFiles.SelectedItem.Value );

				if( IsNewRecord )
					rsFile.RatesheetFileId     = m_txtRsFileID.Text.TrimWhitespaceAndBOM();

				rsFile.Save();
				LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "OK", "true") ;
			}
			catch( Exception exc )
			{
				m_ErrorMessage.Text = "Unable to save the Ratesheet File. " + exc.Message;

			}

		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
