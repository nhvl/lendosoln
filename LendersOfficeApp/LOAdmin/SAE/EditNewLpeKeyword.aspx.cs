// Author : Diana Baird

using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LpeNewKeywordUtility = LendersOfficeApp.los.RatePrice.LpeNewKeywordUtility;
using System.Web.Services;


namespace LendersOfficeApp.LOAdmin.Internal
{
	/// <summary>
	/// Summary description for EditNewLpeKeyword.
	/// </summary>
	public partial class EditNewLpeKeyword : LendersOffice.Admin.SecuredAdminPage
	{
		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsSAE
			};
		}
        //Rebuilt by Long Nguyen
        //Email: longn@meridianlink.net
        public override string StyleSheet => string.Empty;

	    protected override void OnInit(EventArgs e)
        {
            RegisterCSS("newdesign.main.css");
            RegisterCSS("newdesign.lendingqb.css");
            RegisterJsScript("jquery-1.11.2.min.js");
            RegisterJsScript("bootstrap-3.3.4.min.js");
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJquery = false;
            var keyword = RequestHelper.GetSafeQueryString("keyword");

            if (!IsPostBack)
            {
                if (HasPermissionToDo(E_InternalUserPermissions.IsSAE) == false)
                {
                    //m_OK.Visible = false;
                    ShowError("Have no permission");
                }

                if (null != keyword)
                {
                    SqlParameter[] parameters = { new SqlParameter("@Keyword", keyword) };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetNewLpeKeyword", parameters))
                    {
                        if (reader.Read())
                        {
                            m_keyword.Text = reader["NewKeywordName"].ToString();
                            m_defaultVal.Text = reader["DefaultValue"].ToString();

                            bool found = false;
                            string category = reader["Category"].ToString().Trim();
                            foreach (ListItem item in m_ddlCategory.Items)
                            {
                                if (string.Compare(category, item.Value, ignoreCase: true) == 0)
                                {
                                    item.Selected = true;
                                    found = true;
                                    break;
                                }
                            }
                            if (found == false)
                            {
                                ShowError("Error : invalid data with category = " + category);
                            }
                        }
                    }
                    m_keyword.ReadOnly = true;
                }
                else
                {
                    m_keyword.ReadOnly = false;
                }
                //else
                //    Response.Redirect("~/common/AppError.aspx?errmsg=Invalid LPE keyword");
            }
        }
        void ShowError(string message)
        {
            ErrorPanel.Visible = true;
            ErrMsg.InnerText = message;
        }
        public class NewLPE
        {
            public string KeyWord { get; set; }
            public string DefaultVal { get; set; }
            public string Category { get; set; }
            public bool IsAdd { get; set; }
            public string ErrMsg { get; set; }
        }
        [WebMethod]
        public static NewLPE Save(NewLPE data)
        {
            // Place a check against the user's permission
            // set.  If failing, we return error.

            if (SecuredAdminPageEx.IsMissingPermission(E_InternalUserPermissions.IsSAE))
            {
                return new NewLPE() { ErrMsg = "Have no permission" };
            }
            double val;
            if (string.IsNullOrWhiteSpace(data.KeyWord)) { return new NewLPE() { ErrMsg = "Keyword is required" }; }
            data.KeyWord = data.KeyWord.Trim();
            if (data.KeyWord.Length <= 1) { return new NewLPE() { ErrMsg = "Keyword is too short" }; }
            if (Double.TryParse(data.KeyWord, out val)) { return new NewLPE() { ErrMsg = "Keyword must contains at least a letter" }; }

            if (string.IsNullOrWhiteSpace(data.DefaultVal)) { return new NewLPE() { ErrMsg = "Default Value is required" }; }
            data.DefaultVal = data.DefaultVal.Trim();
            if (!Double.TryParse(data.DefaultVal, out val)) { return new NewLPE() { ErrMsg = "Default Value must be a number" }; }

            if (string.IsNullOrWhiteSpace(data.Category)) { return new NewLPE() { ErrMsg = "Category is required" }; }
            data.Category = data.Category.Trim();

            string procedureName;
            if (data.IsAdd)
            {
                procedureName = "AddNewLpeKeyword";
                SqlParameter[] parameters = {
                                                new SqlParameter("@Keyword", data.KeyWord)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "GetNewLpeKeyword", parameters))
                {
                    if (reader.Read())
                    {
                        return new NewLPE() { ErrMsg = $"New LPE keyword \'{data.KeyWord}\' already in use."};
                    }
                }	
            }
            else
            {
                procedureName = "UpdateNewLpeKeyword";
            }
            int result;
            try
            {

                LpeNewKeywordUtility.CreateSymbolEntry(data.KeyWord, data.DefaultVal.ToString(), data.Category);

                SqlParameter[] parameters = {
                                                new SqlParameter( "@Keyword" , data.KeyWord),
                                                new SqlParameter( "@DefaultValue" , data.DefaultVal),
                                                new SqlParameter( "@Category" , data.Category)
                                            };
                result = StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, procedureName, 1, parameters);
            }
            catch (SqlException exc)
            {
                return new NewLPE() { ErrMsg = "Unable to save the new LPE keyword. " + exc.Message };
            }
            if (result > 0)
            {
                //success
                return null;
            }
            return new NewLPE() { ErrMsg = $"Unable to save the new LPE keyword \'{data.KeyWord}\'."};
        }
	}
}
