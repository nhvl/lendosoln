// Author : Diana Baird

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOfficeApp.los.RatePrice;
using System.Collections.Generic;
using System.Web.Services;

namespace LendersOfficeApp.LOAdmin.Internal
{
	/// <summary>
	/// Summary description for NewLpeKeywords.
	/// </summary>
	public partial class NewLpeKeywords : LendersOffice.Admin.SecuredAdminPage
	{
        //Rebuild by Long Nguyen
        //Email: longn@meridianlink.net
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsSAE
			};
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
            if (m_Command.Value.Equals("delete"))
            {
                DeleteNewLpeKeyword();
            }
			BindDataGrid();
            if (IsPostBack)
            {
                Response.Redirect(Request.Url.ToString(), false);
            }
		}
		private void DeleteNewLpeKeyword()
		{
			m_Command.Value = string.Empty;
            if (string.IsNullOrWhiteSpace(m_keyword.Value))
            {
                return;
            }
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@Keyword", m_keyword.Value) };
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DeleteNewLpeKeyword", 1, parameters);
            }
            catch (SqlException e)
            {
                Tools.LogError(e);
            }
		}

		private void BindDataGrid() 
		{
            DataSet ds = new DataSet();
            DataSetHelper.Fill( ds, DataSrc.LOShare, "ListNewLpeKeywords", null);

            DataTable table = ds.Tables[0];;
            table.Columns.Add("Note", typeof(string));
            foreach( DataRow row in table.Rows )
            {
                row["Note"] = string.Empty;
                try
                {
                    LpeNewKeywordUtility.CreateSymbolEntry((string)row["NewKeywordName"],
                                                            (string)row["DefaultValue"],
                                                            (string)row["Category"]);
                }
                catch (CBaseException exc)
                {
                    row["Note"] = exc.Message;
                }
            }
            table.AcceptChanges();
            var dataView = table.DefaultView;
            dataView.Sort = "NewKeywordName ASC";
            m_Grid.DataSource = dataView;
            m_Grid.DataBind() ;
            if (m_Grid.Rows.Count != 0)
            {
                m_Grid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            m_lstbxAllSymbol.EnableViewState = false;

            CCondition condition = CCondition.CreateTestCondition( string.Empty );
            Hashtable newKeywords = new Hashtable();
            string[] allKeywords = condition.GetValidSymbols( newKeywords ); 

            foreach( string keyword in allKeywords )
            {
                ListItem item = new ListItem(keyword);
                if( newKeywords.Contains( keyword ) )
                {
                    item.Attributes.Add("class", "text-danger");
                    item.Text = keyword + "    <- new keyword"; 
                }
                m_lstbxAllSymbol.Items.Add( item );
            }



		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
