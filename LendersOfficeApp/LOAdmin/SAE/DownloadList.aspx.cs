using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.SAE
{
	public partial class DownloadList : SecuredAdminPage
	{
        [WebMethod]
        public static DownloadListEntry[] Get(bool? active, string login, string description, string fileName)
        {
            if (SecuredAdminPageEx.IsMissingPermission(DownloadListPermission)) { return null; }

            return DownloadListEntry.Search(active, login, description, fileName);
        }

        [WebMethod]
        public static int Delete(long id)
        {
            if (SecuredAdminPageEx.IsMissingPermission(DownloadListPermission)) { return int.MinValue; }

            return DownloadListEntry.Delete(id);
        }

	    [WebMethod]
	    public static object Update(DownloadListEntry entry)
	    {
            if (SecuredAdminPageEx.IsMissingPermission(DownloadListPermission)) { return "No permissions"; }

            if (!entry.IsValidForSave) { return "Invalid data."; }

	        entry.Update();

	        return null;
	    }

        [WebMethod]
	    public static void BatchUpdate(long[] ids, DownloadListEntry inEntry)
        {
            if (SecuredAdminPageEx.IsMissingPermission(DownloadListPermission)) { return; }

            DownloadListEntry.BatchUpdate(ids, inEntry);
        }

	    internal static readonly E_InternalUserPermissions[] DownloadListPermission =
	    {
	        E_InternalUserPermissions.IsSAE
	    };

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.SAE.DownloadList.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.
            return DownloadListPermission;
		}

		protected void PageLoad(object sender, EventArgs e)
		{
		    RegisterResources();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}

    public class DownloadListEntry
    {
        public long?     Id                     { get; set; }
        public string    Description            { get; set; }
        public string    Url                    { get; set; }
        public string    FileName               { get; set; }
        public DateTime? dLastUpdated           { get; set; }
        public DateTime? dLastSuccessfulDL      { get; set; }
        public bool?     Active                 { get; set; }
        public string    LastDlResult           { get; set; }
        public int?      ErrorCount             { get; set; }
        public string    Conversion             { get; set; }
        public string    BotType                { get; set; }
        public string    Login                  { get; set; }
        public string    Password               { get; set; }
        public DateTime? LastRatesheetTimestamp { get; set; }
        public string    Token                  { get; set; }
        public string    Note                   { get; set; }

        public bool IsValidForSave
        {
            get { return (new[] { this.Description, this.Url, this.FileName }).All(f => !string.IsNullOrEmpty(f)); }
        }

        public DownloadListEntry()
        {
            // For Serialization and Deserialization
        }

        internal static DownloadListEntry[] Search(bool? active, string login, string description, string fileName)
        {
            login       = Utilities.SafeSQLString(login      .TrimWhitespaceAndBOM());
            description = Utilities.SafeSQLString(description.TrimWhitespaceAndBOM());
            fileName    = Utilities.SafeSQLString(fileName   .TrimWhitespaceAndBOM());

            var parameters = new List<SqlParameter>();
            if (active.HasValue)
            {
                parameters.Add(new SqlParameter("@Active", (active.Value) ? "T" : "F"));
            }
            if (!string.IsNullOrEmpty(login))
            {
                parameters.Add(new SqlParameter("@Login", login));
            }
            if (!string.IsNullOrEmpty(description))
            {
                parameters.Add(new SqlParameter("@Description", description));
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                parameters.Add(new SqlParameter("@FileName", fileName));
            }

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_RetrieveDownloadList", parameters))
            {
                return DownloadListEntry.FromReader(reader).ToArray();
            }
        }
        internal void Update()
        {
            Update(this);
        }
        internal static int Delete(long id)
        {
            return StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet,
                "RS_DeleteDownloadListEntryByID", 0,
                new SqlParameter("@DownloadListEntryId", id));
        }
        internal static void BatchUpdate(IEnumerable<long> ids, DownloadListEntry inEntry)
        {
            foreach (var entry in ids.Select(GetFromDb))
            {
                if (!string.IsNullOrEmpty(inEntry.Description)) { entry.Description            = inEntry.Description           ; }
                if (!string.IsNullOrEmpty(inEntry.Url)        ) { entry.Url                    = inEntry.Url                   ; }
                if (!string.IsNullOrEmpty(inEntry.FileName)   ) { entry.FileName               = inEntry.FileName              ; }
                if (inEntry.Active.HasValue                   ) { entry.Active                 = inEntry.Active                ; }
                if (inEntry.Conversion != null                ) { entry.Conversion             = inEntry.Conversion            ; }
                if (inEntry.BotType    != null                ) { entry.BotType                = inEntry.BotType               ; }
                if (inEntry.Login      != null                ) { entry.Login                  = inEntry.Login                 ; }
                if (inEntry.Password   != null                ) { entry.Password               = inEntry.Password              ; }
                if (inEntry.LastRatesheetTimestamp.HasValue   ) { entry.LastRatesheetTimestamp = inEntry.LastRatesheetTimestamp; }
                if (inEntry.Token   != null                   ) { entry.Token                  = inEntry.Token                 ; }
                if (inEntry.Note    != null                   ) { entry.Note                   = inEntry.Note                  ; }

                entry.Update();
            }
        }

        private static IEnumerable<DownloadListEntry> FromReader(DbDataReader reader)
        {
            while (reader.Read())
            {
                yield return GetFromDb(reader);
            }
        }
        private static DownloadListEntry GetFromDb(long id)
        {
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet,
                "RS_RetrieveDownloadListEntryById", new SqlParameter("@DownloadListEntryId", id)))
            {
                return reader.Read() ? GetFromDb(reader) : null;
            }
        }
        private static DownloadListEntry GetFromDb(DbDataReader reader)
        {
            return new DownloadListEntry {
                Id                     = reader.AsNullableInt("ID").Value,
                Description            = reader["Description"           ] as string,
                Url                    = reader["URL"                   ] as string,
                FileName               = reader["FileName"              ] as string,
                dLastUpdated           = reader.AsNullableDateTime("dLastUpdated"),
                dLastSuccessfulDL      = reader.AsNullableDateTime("dLastSuccessfulDL"),
                Active                 = reader["Active"                ] as string == "T", // "T" "F"
                LastDlResult           = reader["LastDlResult"          ] as string,
                ErrorCount             = reader.AsNullableInt("ErrorCount"),
                Conversion             = reader["Conversion"            ] as string,
                BotType                = reader["BotType"               ] as string,
                Login                  = reader["Login"                 ] as string,
                Password               = reader["Password"              ] as string,
                LastRatesheetTimestamp = reader.AsNullableDateTime("LastRatesheetTimestamp"),
                Token                  = reader["Token"                 ] as string,
                Note                   = reader["Note"                  ] as string,
            };
        }
        private static void Update(DownloadListEntry entry)
	    {
            var parameters = new List<SqlParameter>();

            if (entry.Id.HasValue) { parameters.Add(new SqlParameter("@ID", entry.Id.Value)); }

            parameters.Add(new SqlParameter("@Description", entry.Description));
            parameters.Add(new SqlParameter("@URL"        , entry.Url        ));
            parameters.Add(new SqlParameter("@FileName"   , entry.FileName   ));
            parameters.Add(new SqlParameter("@Active"     , (!entry.Active.HasValue || entry.Active.Value) ? "T" : "F" ));

            if (!string.IsNullOrEmpty(entry.Conversion)) { parameters.Add(new SqlParameter("@Conversion"            , entry.Conversion                  )); }
            if (!string.IsNullOrEmpty(entry.BotType   )) { parameters.Add(new SqlParameter("@BotType"               , entry.BotType                     )); }
            if (!string.IsNullOrEmpty(entry.Login     )) { parameters.Add(new SqlParameter("@Login"                 , entry.Login                       )); }
            if (!string.IsNullOrEmpty(entry.Password  )) { parameters.Add(new SqlParameter("@Password"              , entry.Password                    )); }
            if (entry.LastRatesheetTimestamp.HasValue  ) { parameters.Add(new SqlParameter("@LastRatesheetTimestamp", entry.LastRatesheetTimestamp.Value)); }

            parameters.Add(new SqlParameter("@Token", entry.Token));
            parameters.Add(new SqlParameter("@Note", entry.Note));

            var spAction = (entry.Id.HasValue) ? "RS_UpdateDownloadListEntry" : "RS_CreateDownloadListEntry";

            // Store procedure is `SET NOCOUNT ON`, return void instead to remove confusion
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, spAction, 0, parameters);
        }
    }
}
