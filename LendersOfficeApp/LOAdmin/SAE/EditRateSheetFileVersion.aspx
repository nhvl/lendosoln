<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="EditRateSheetFileVersion.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.loadmin.SAE.EditRateSheetFileVersion" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EditRateSheetFileVersion</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body onload="init();" MS_POSITIONING="FlowLayout">
		<script language="javascript">
		<!--
		function init()
		{
			resize( 500 , 560 );
		}
		//-->
		</script>
		<form id="EditRateSheetFileVersion" method="post" runat="server">
			<DIV style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px"><SPAN style="MARGIN-RIGHT: 8px"><asp:button id="m_OK" onclick="OKClick" runat="server" Text="  OK  "></asp:button><INPUT onclick="self.close();" type="button" value="Cancel">
				</SPAN>
			</DIV>
			<HR style="MARGIN-LEFT: 4px; MARGIN-RIGHT: 4px">
			<TABLE id="Table1" style="WIDTH: 418px; HEIGHT: 193px" cellSpacing="1" cellPadding="5" border="0">
				<TR>
					<TD align="center" colSpan="2">
						<P><ml:EncodedLabel id="lblPageTitle" runat="server" Width="318px" Height="16px" Font-Size="Medium" Font-Bold="True"></ml:EncodedLabel></P>
						<P>&nbsp;</P>
					</TD>
				</TR>
				<TR>
					<TD>Output Filename</TD>
					<TD><asp:textbox id="m_txtRsFileID" runat="server" Width="176px"></asp:textbox><asp:requiredfieldvalidator id="m_rfvRsFileID" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_txtRsFileID"></asp:requiredfieldvalidator></TD>
				</TR>
				<TR>
					<TD>Version</TD>
					<TD><asp:textbox id="m_txtVersionNumber" runat="server" Width="175px"></asp:textbox></TD>
				<TR>
					<TD>First Effective</TD>
					<TD><asp:textbox id="m_txtFirstEffective" runat="server" Width="175px"></asp:textbox><asp:requiredfieldvalidator id="m_rfvFirstEffective" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_txtFirstEffective"></asp:requiredfieldvalidator></TD>
				</TR>
				<TR>
					<TD>Last Effective</TD>
					<TD><asp:textbox id="m_txtLastEffective" runat="server" Width="175px"></asp:textbox><asp:requiredfieldvalidator id="m_rfvLastEffective" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_txtLastEffective"></asp:requiredfieldvalidator></TD>
				</TR>
				<TR>
					<TD>Created Date</TD>
					<TD><asp:textbox id="m_txtCreatedDate" runat="server" Width="204px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD>Modified Date</TD>
					<TD><asp:textbox id="m_txtModifiedDate" runat="server" Width="204px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD>Expired</TD>
					<TD><asp:checkbox id="m_cbExpired" runat="server"></asp:checkbox></TD>
				</TR>
			</TABLE>
			<ml:EncodedLabel id="m_ErrorMessage" runat="server" ForeColor="Red" Font-Size="12px" Height="26px"></ml:EncodedLabel>
			<uc1:cmodaldlg id="Cmodaldlg1" runat="server"></uc1:cmodaldlg></form>
		</FORM>
	</body>
</HTML>