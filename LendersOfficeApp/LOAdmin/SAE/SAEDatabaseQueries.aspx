<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="../../common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../Common/Header.ascx" %>
<%@ Page language="c#" Codebehind="SAEDatabaseQueries.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.SAE.SAEDatabaseQueries" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>SAE Database Queries</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<style type="text/css">
        /*[CDATA[*/
            .DataGridFixedHeader 
            {
                position:relative; 
                top:expression(this.offsetParent.scrollTop - 2);        
                left:-1;
            }
        /*]]*/
        </style>		
		<SCRIPT language="javascript">
		<!--
					
		//-->
		</SCRIPT>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="SAEDatabaseQueriesForm" method="post" runat="server">
			<UC:HEADER id="m_Header" runat="server"></UC:HEADER><UC:HEADERNAV id="m_Navigate" runat="server">
				<MenuItem URL="~/LOAdmin/Main.aspx" Label="Main"></MenuItem>
				<MenuItem URL="~/LOAdmin/SAE/SAEDatabaseQueries.aspx" Label="SAE Database Queries"></MenuItem>
			</UC:HEADERNAV>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="FormTable">
						<asp:DropDownList ID="m_queriesDDL" AutoPostBack="True" Runat="server"></asp:DropDownList>&nbsp;
						<asp:Button ID="m_exportResults" Runat="server" Text=" Export Results " OnClick="ExportResults"></asp:Button>
                        <asp:Button ID="m_exportPacUnion" runat="server" Text="Export Pacific Union Users" OnClick="ExportPacificUnionUsers" />
						<hr>
						<ml:EncodedLabel style="WIDTH:1100px" Font-Bold="True" Font-Italic="True" ID="m_query" Runat="server"></ml:EncodedLabel>
						<br>
						<div id="m_gridDiv" runat="server" style="OVERFLOW-Y: scroll; OVERFLOW-X: scroll; height:650px; WIDTH:1100px;">
							<ml:CommonDataGrid id="m_dg" runat="server">
								<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
								<HeaderStyle CssClass="GridHeader DataGridFixedHeader"></HeaderStyle>
							</ml:CommonDataGrid>
						</div>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
