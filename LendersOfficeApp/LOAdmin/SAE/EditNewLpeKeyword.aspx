<%@ Page language="c#" Codebehind="EditNewLpeKeyword.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Internal.EditNewLpeKeyword" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
    <body>
    <script type="text/javascript">
        <!--
        var callback;
        function transformResponse(text) {
            try {
                return JSON.parse(text, function (key, value) {
                    if (typeof value == "string") {
                        var result, d;
                        // parse ASP.NET DateTime type
                        {
                            result = /\/Date\((-?[0-9]+)(?:[a-zA-Z]|(?:\+|-)[0-9]{4})?\)\//g.exec(value);
                            if (!!result) {
                                return new Date(parseFloat(result[1]));
                            }
                        }
                    }
                    return value;
                });
            } catch (e) {
                return text;
            }
        }
        function setTooltip(selector, title) {
            selector.prop("title", title);
            selector.focus();
            selector.tooltip({ placement: 'bottom' });
            selector.tooltip('show');
            selector.one("blur change", function () { selector.tooltip("destroy"); });
            //selector.one("blur change", function(){ selector.tooltip("destroy");$("div[role='tooltip']").tooltip('destroy');});
        }
        function save(e) {
            e.preventDefault();
            var data = {
                KeyWord: $("[id$='m_keyword']").val().trim(),
                DefaultVal: $("[id$='m_defaultVal']").val().trim(),
                Category: $("[id$='m_ddlCategory']").val().trim(),
                IsAdd: !$("[id$='m_keyword']").is('[readonly]'),
            };
            if (!data.KeyWord) {
                $("[id$='m_keyword']").val('');
                setTooltip($("[id$='m_keyword']"), "Keyword is required");
                return false;
            }
            if (!data.DefaultVal) {
                $("[id$='m_defaultVal']").val('');
                setTooltip($("[id$='m_defaultVal']"), "Default Value is required");
                return false;
            }
            //data.KeyWord = '';
            $.ajax({
                type: "POST",
                url: window.location.pathname + "/Save",
                data: JSON.stringify({ data: data }),
                contentType: "application/json; charset:utf-8",
                dataType: "json",
                converters: { "text json": transformResponse }
            }).done(function (data, textStatus, jqXHR) {
                if (data.d===null) {
                    callback(data, textStatus, jqXHR);
                }
                else {
                    showError("Unable to save the new LPE keyword. " + data.d.ErrMsg);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                showError(JSON.parse(jqXHR.responseText).Message);
            });
            return false;

        }
        function sendSubmit(f) {
            callback = f;
            $("[id$='EditNewLpeKeyword']").prop("action", "");
            $('#btnSubmit').click();
        }
        function showError(message) {
            $("div[role='alert']").removeClass("hide");
            $("[id$='ErrMsg']").html(message);
        }
        //-->
    </script>
        <div class="alert alert-error alert-dismissible hide" role="alert" id="ErrorPanel" runat="server">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Error Message!</strong> <span runat="server" id="ErrMsg">Numquam quos fuga quam suscipit sapiente perferendis magnam. </span>
        </div>
        <form id="EditNewLpeKeyword" runat="server" onsubmit="return save(event);">
            <input type="submit" id="btnSubmit" class="hidden" />
             <div class="form-group">
                <label>Keyword:<span class="text-danger">*</span></label>
                <asp:TextBox runat="server" ID="m_keyword" CssClass="form-control input" AutoComplete="off" required title="Please fill out the Keyword"></asp:TextBox>
                <%--<asp:requiredfieldvalidator id="m_rfvKeyword" runat="server" ControlToValidate="m_keyword" Display="Dynamic" ErrorMessage="This field is required."></asp:requiredfieldvalidator>--%>
            </div>
            <div class="form-group">
                <label>Default Value:<span class="text-danger">*</span></label>
                <asp:TextBox runat="server" ID="m_defaultVal" CssClass="form-control input" AutoComplete="off" required title="Please fill out the Default Value"></asp:TextBox>
                <%--<asp:requiredfieldvalidator id="m_rfvDefaultVal" runat="server" ControlToValidate="m_defaultVal" Display="Dynamic" ErrorMessage="This field is required."></asp:requiredfieldvalidator>--%>
            </div>
            <div class="form-group">
                <label>Category:</label>
                <asp:dropdownlist id="m_ddlCategory" runat="server" CssClass="form-control input">
                            <asp:ListItem Value="Field">Field</asp:ListItem>
                            <asp:ListItem Value="FunctionInt">Function( int arg )</asp:ListItem>
                            <asp:ListItem Value="FunctionString">Function( string arg)</asp:ListItem>
                </asp:dropdownlist>
            </div>
        </form>
    </body>
</html>