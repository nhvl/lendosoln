using System;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using System.Data.SqlClient;
using System.Web.Services;
using System.Linq;
using LendersOffice.Security;
using LendersOffice.Admin;

namespace LendersOfficeApp.LOAdmin.SAE
{
    public partial class EmailDownloadList : SecuredAdminPage
    {
        #region API
        /*
         * All API will return a Tuple [error, result]
        */

        [WebMethod]
        public static object[] Get()
        {
            return ApiWrap(() => EmailDownloadListItem.GetList().ToArray());
        }

        [WebMethod]
        public static object[] Delete(int[] ids)
        {
            return ApiWrap(() => ids.Sum(EmailDownloadListItem.Delete));
        }

        [WebMethod]
        public static object[] Save(EmailDownloadListItem[] entries)
        {
            return ApiWrap(() =>
            {
                foreach (var item in entries)
                {
                    item.Save();
                }
                return (object) null;
            });
        }

        static object[] ApiWrap<T>(Func<T> f)
        {
            try
            {
                string error = null;
                object result = null;

                Tools.LogAndThrowIfErrors(() =>
                {
                    if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions))
                    {
                        error = "No Permission";
                        return;
                    }

                    result = f();
                });

                return new[] { error, result };
            }
            catch (CBaseException e)
            {
                return new[] { e.Message, null };
            }
        }
        #endregion

        void RegisterResources()
        {
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.SAE.EmailDownloadList.js");
        }
        public override string StyleSheet => string.Empty;

        static readonly E_InternalUserPermissions[] RequiredPermissions = { E_InternalUserPermissions.IsSAE };
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return RequiredPermissions;
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            RegisterResources();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion

        public class EmailDownloadListItem
        {
            public int?      ID             { get; set; }
            public string    Sender         { get; set; }
            public string    Body           { get; set; }
            public string    MailFileName   { get; set; }
            public string    TargetFileName { get; set; }
            public bool      Active         { get; set; }
            public string    BotType        { get; set; }
            public string    Subject        { get; set; }
            public string    Subject1       { get; set; }
            public string    Subject2       { get; set; }
            public string    Subject3       { get; set; }
            public string    Subject4       { get; set; }
            public string    Subject5       { get; set; }
            public string    Subject6       { get; set; }
            public string    Subject7       { get; set; }
            public string    Subject8       { get; set; }
            public string    Subject9       { get; set; }
            public DateTime? dLastUpdated   { get; set; }
            public string    Description    { get; set; } = string.Empty;
            public string    Note           { get; set; } = string.Empty;

            public static List<EmailDownloadListItem> GetList()
            {
                var list = new List<EmailDownloadListItem>();
                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_RetrieveEmailDownloadList"))
                {
                    while (reader.Read())
                    {
                        list.Add(FromReader(reader));
                    }
                }
                return list;
            }

            public static EmailDownloadListItem GetById(int id)
            {
                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_RetrieveEmailDownloadListEntryById", new SqlParameter("@Id", id)))
                {
                    if (!reader.Read())
                    {
                        throw new ApplicationException("Download list entry does not exist.");
                    }

                    return EmailDownloadListItem.FromReader(reader);
                }
            }

            public static int Delete(int id)
            {
                return StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_DeleteEmailDownloadListEntryByID", 0, new SqlParameter("@Id", id));
            }

            static EmailDownloadListItem FromReader(IDataRecord reader)
            {
                return new EmailDownloadListItem
                {
                    ID = (int) reader["ID"],
                    Sender = reader["Sender"].ToString(),
                    Body = reader["Body"].ToString(),
                    MailFileName = reader["MailFileName"].ToString(),
                    TargetFileName = reader["TargetFileName"].ToString(),
                    dLastUpdated = DbFieldToDate(reader["dLastUpdated"]),
                    Active = reader["Active"].ToString().Equals("T"),
                    BotType = reader["BotType"].ToString(),
                    Subject  = reader["Subject" ].ToString(),
                    Subject1 = reader["Subject1"].ToString(),
                    Subject2 = reader["Subject2"].ToString(),
                    Subject3 = reader["Subject3"].ToString(),
                    Subject4 = reader["Subject4"].ToString(),
                    Subject5 = reader["Subject5"].ToString(),
                    Subject6 = reader["Subject6"].ToString(),
                    Subject7 = reader["Subject7"].ToString(),
                    Subject8 = reader["Subject8"].ToString(),
                    Subject9 = reader["Subject9"].ToString(),
                    Description = reader["Description"] as string,
                    Note = string.IsNullOrEmpty(reader["Note"] as string) ? string.Empty : (string) reader["Note"],
                };
            }

            static DateTime? DbFieldToDate(object value)
            {
                DateTime d;
                return (value == null || value == DBNull.Value || string.IsNullOrEmpty(value.ToString()))
                    ? null
                    : (DateTime.TryParse(value.ToString(), out d) ? (DateTime?) d : null);
            }

            public void Save()
            {
                var parameters = new[]
                    {
                        this.ID.HasValue ? new SqlParameter("@ID", this.ID) : null,
                        string.IsNullOrEmpty(this.Sender) ? null : new SqlParameter("@Sender", this.Sender),
                        string.IsNullOrEmpty(this.Body) ? null : new SqlParameter("@Body", this.Body),
                        string.IsNullOrEmpty(this.MailFileName) ? null : new SqlParameter("@MailFileName", this.MailFileName),
                        string.IsNullOrEmpty(this.TargetFileName) ? null : new SqlParameter("@TargetFileName", this.TargetFileName),
                        new SqlParameter("@Active", this.Active ? "T" : "F"),
                        string.IsNullOrEmpty(this.BotType) ? null : new SqlParameter("@BotType", this.BotType),
                        this.Subject  == null ? null : new SqlParameter("@Subject" , this.Subject ),
                        this.Subject1 == null ? null : new SqlParameter("@Subject1", this.Subject1),
                        this.Subject2 == null ? null : new SqlParameter("@Subject2", this.Subject2),
                        this.Subject3 == null ? null : new SqlParameter("@Subject3", this.Subject3),
                        this.Subject4 == null ? null : new SqlParameter("@Subject4", this.Subject4),
                        this.Subject5 == null ? null : new SqlParameter("@Subject5", this.Subject5),
                        this.Subject6 == null ? null : new SqlParameter("@Subject6", this.Subject6),
                        this.Subject7 == null ? null : new SqlParameter("@Subject7", this.Subject7),
                        this.Subject8 == null ? null : new SqlParameter("@Subject8", this.Subject8),
                        this.Subject9 == null ? null : new SqlParameter("@Subject9", this.Subject9),
                        new SqlParameter("@Description", this.Description),
                        new SqlParameter("@Note", this.Note),
                    }
                    .Where(p => p != null)
                    .ToList();

                string storedProcName = this.ID.HasValue ? "RS_UpdateEmailDownloadListEntry" : "RS_CreateEmailListEntry";

                // since the Stored Procedure use `SET NOCOUNT ON`, the return value is -1, return void to remove the confusion
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, storedProcName, 0, parameters);
            }

            public int Delete()
            {
                return EmailDownloadListItem.Delete(this.ID.Value);
            }
        }
    }
}
