<%@ Page language="c#" Codebehind="OFBatchEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.SAE.OFBatchEdit" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>OFBatchEdit</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body onload="init();" onunload="window.opener.document.getElementById('m_btnRefresh').click();" MS_POSITIONING="FlowLayout">
		<script language="javascript">
		<!--
		function init()
		{
			resize( 500 , 560 );
			changeValidators();
		}
		
		function changeValidators()
		{
			var val = document.EditAdmin.m_fileNameValChk.checked;
			document.getElementById("m_updateFileName").checked = val;
			ValidatorEnable(document.getElementById("m_fileNameVal"), val);
			
			var val2 = document.EditAdmin.m_exportTypeValChk.checked;
			document.getElementById("m_updateExportType").checked = val2;			
			ValidatorEnable(document.getElementById("m_exportTypeVal"), val2);			
			
			var val3 = document.EditAdmin.m_wsNameValChk.checked;
			document.getElementById("m_updateWorksheetName").checked = val3;
			ValidatorEnable(document.getElementById("m_wsNameVal"), val3);
			
			var val4 = document.EditAdmin.m_enabledValChk.checked;
			document.getElementById("m_updateEnabled").checked = val4;			
			ValidatorEnable(document.getElementById("m_enabledVal"), val4);	
			
			var val5 = document.EditAdmin.m_dirtyValChk.checked;
			document.getElementById("m_updateDirty").checked = val5;
			ValidatorEnable(document.getElementById("m_dirtyVal"), val5);					
		}
		
		//-->
		</script>
		<FORM id="EditAdmin" method="post" runat="server">
			<input type="hidden" name="RsIds"><input type="hidden" name="FNs">
			<TABLE id="Table1" style="WIDTH: 534px" cellSpacing="1" cellPadding="5" border="0">
				<TR>
					<TD align="center" colSpan="2">
						<P><ml:EncodedLabel id="lblPageTitle" runat="server" Font-Size="Medium" Font-Bold="True" Width="401px" Height="15px"></ml:EncodedLabel></P>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 117px" valign="top"><P>ID - File Name</P>
					</TD>
					<TD align="center">
						<asp:datagrid ID="m_customerIds" Runat="server" ShowHeader="False" BorderWidth="0" CellPadding="4">
							<ItemStyle CssClass="GridItem" VerticalAlign="Top" HorizontalAlign="Left"></ItemStyle>
						</asp:datagrid>
					</TD>
					<TD valign="bottom">Modify</TD>
				</TR>
				<tr>
					<TD align="left" style="WIDTH: 229px">File Name</TD>
					<td style="WIDTH: 676px"><asp:TextBox id="m_fileName" Runat="server" Width="200px"></asp:TextBox>
						<asp:requiredfieldvalidator id="m_fileNameVal" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_fileName"></asp:requiredfieldvalidator>
					<td><input onclick="changeValidators();" id="m_fileNameValChk" type="checkbox"></td>
					<td style="DISPLAY:none"><asp:CheckBox ID="m_updateFileName" Runat="server"></asp:CheckBox></td>
				</tr>
				<TR>
					<TD align="left" style="WIDTH: 229px">Export Type</TD>
					<td style="WIDTH: 676px"><asp:RadioButtonList id="m_exportType" RepeatDirection="Horizontal" runat="server">
							<asp:ListItem Value="Rate Options"></asp:ListItem>
							<asp:ListItem Value="Adjust Table"></asp:ListItem>
						</asp:RadioButtonList>
						<asp:requiredfieldvalidator id="m_exportTypeVal" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_exportType"></asp:requiredfieldvalidator>
					<td><input onclick="changeValidators();" id="m_exportTypeValChk" type="checkbox"></td>
					<td style="DISPLAY:none"><asp:CheckBox ID="m_updateExportType" Runat="server"></asp:CheckBox></td>
				</TR>
				<tr>
					<TD align="left" style="WIDTH: 229px">Worksheet Name</TD>
					<td style="WIDTH: 676px"><asp:TextBox id="m_worksheetName" Runat="server" Width="200px"></asp:TextBox>
						<asp:requiredfieldvalidator id="m_wsNameVal" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_worksheetName"></asp:requiredfieldvalidator>
					<td><input onclick="changeValidators();" id="m_wsNameValChk" type="checkbox"></td>
					<td style="DISPLAY:none"><asp:CheckBox ID="m_updateWorksheetName" Runat="server"></asp:CheckBox></td>
				</tr>
				<TR>
					<TD align="left" style="WIDTH: 229px">Enabled?</TD>
					<td style="WIDTH: 676px"><asp:RadioButtonList id="m_enabled" RepeatDirection="Horizontal" runat="server">
							<asp:ListItem Value="Yes"></asp:ListItem>
							<asp:ListItem Value="No"></asp:ListItem>
						</asp:RadioButtonList>
						<asp:requiredfieldvalidator id="m_enabledVal" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_enabled"></asp:requiredfieldvalidator>
					<td><input onclick="changeValidators();" id="m_enabledValChk" type="checkbox"></td>
					<td style="DISPLAY:none"><asp:CheckBox ID="m_updateEnabled" Runat="server"></asp:CheckBox></td>
				</TR>
				<TR>
					<TD align="left" style="WIDTH: 229px">Dirty?</TD>
					<td style="WIDTH: 676px"><asp:RadioButtonList id="m_dirty" RepeatDirection="Horizontal" runat="server">
							<asp:ListItem Value="Yes"></asp:ListItem>
							<asp:ListItem Value="No"></asp:ListItem>
						</asp:RadioButtonList>
						<asp:requiredfieldvalidator id="m_dirtyVal" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_dirty"></asp:requiredfieldvalidator>
					<td><input onclick="changeValidators();" id="m_dirtyValChk" type="checkbox"></td>
					<td style="DISPLAY:none"><asp:CheckBox ID="m_updateDirty" Runat="server"></asp:CheckBox></td>
				</TR>
				<tr>
					<TD align="left" style="WIDTH: 229px">Update Monitor Emails Forwarded To:</TD>
					<td style="WIDTH: 676px"><asp:TextBox id="m_updateMonitorEmails" Runat="server" Width="200px"></asp:TextBox>
					<td><asp:CheckBox ID="m_updateUpdateMonitorEmails" Runat="server"></asp:CheckBox></td>
				</tr>
			</TABLE>
			<HR style="MARGIN-LEFT: 4px; MARGIN-RIGHT: 4px">
			<DIV align="center" style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px"><SPAN style="MARGIN-RIGHT: 8px"><asp:button id="m_OK" onclick="OKClick" runat="server" Text="  OK  "></asp:button><INPUT onclick="self.close();" type="button" value="Cancel">
				</SPAN>
			</DIV>
			<ml:EncodedLabel id="m_ErrorMessage" runat="server" ForeColor="Red" Font-Size="12px"></ml:EncodedLabel>
			<uc1:cmodaldlg id="Cmodaldlg1" runat="server"></uc1:cmodaldlg>
		</FORM>
	</body>
</HTML>
