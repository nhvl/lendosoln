<%@ Page language="C#" Codebehind="InvestorXlsFileList.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.SAE.InvestorXlsFileList"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Investor Ratesheets - LendingQB" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">
<!--[if lt IE 9]>
<div class="alert alert-danger" role="alert">
    You are using an <strong>outdated</strong> browser.
    Please <strong>upgrade</strong> your browser to improve your experience.<br/>
</div>
<![endif]-->
<div class="container-fluid"><div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="RateSheetVersion2.aspx">Acceptable Ratesheets</a></li>
            <li role="presentation" class="active"><a>Investor Rate Sheet List</a></li>
        </ul>
    </div>
    <div class="tab-content"><div class="tab-pane fade in active ng-cloak" ng-app="app">
        <xls-list></xls-list>
    </div></div>
</div></div>
<form runat="server"></form>

</asp:Content>
