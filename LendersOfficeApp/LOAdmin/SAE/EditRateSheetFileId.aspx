<%@ Page language="c#" Codebehind="EditRateSheetFileId.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.loadmin.SAE.EditRateSheetFileId" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>EditRateSheetFileId</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body onload="init();" MS_POSITIONING="FlowLayout">
		<script language="javascript">
		<!--
		function init()
		{
			resize( 600 , 560 );
		}
		//-->
		</script>
		<FORM id="EditAdmin" method="post" runat="server">
			<DIV style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px"><SPAN style="MARGIN-RIGHT: 8px"><asp:button id="m_OK" onclick="OKClick" runat="server" Text="  OK  "></asp:button><INPUT onclick="self.close();" type="button" value="Cancel">
				</SPAN>
			</DIV>
			<HR style="MARGIN-LEFT: 4px; MARGIN-RIGHT: 4px">
			<TABLE id="Table1" style="WIDTH: 534px; HEIGHT: 245px" cellSpacing="1" cellPadding="5" border="0">
				<TR>
					<TD align="center" colSpan="2">
						<P><ml:EncodedLabel id="lblPageTitle" runat="server" Font-Size="Medium" Font-Bold="True" Width="401px" Height="15px"></ml:EncodedLabel></P>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 114px"><P>Output Filename<BR>
							(unique name)</P>
					</TD>
					<TD><asp:textbox id="m_txtRsFileID" runat="server" Width="386px"></asp:textbox><asp:requiredfieldvalidator id="m_rfvRsFileID" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_txtRsFileID" Width="112px"></asp:requiredfieldvalidator>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 114px">Deployment type</TD>
					<TD><asp:dropdownlist id="m_ddlDeployType" runat="server" Width="390px">
							<asp:ListItem Value="AlwaysBlocked">AlwaysBlocked</asp:ListItem>
							<asp:ListItem Value="UseVersion">UseVersion</asp:ListItem>
							<asp:ListItem Value="UseVersionAfterGeneratingAnOutputFileOk">UseVersionAfterGeneratingAnOutputFileOk</asp:ListItem>
						</asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 114px">Both Map &amp; Bot Are OK</TD>
					<TD><asp:checkbox id="m_cbUsingForMapAndBot" runat="server"></asp:checkbox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 114px">Investor&nbsp;Xls Id</TD>
					<TD><asp:dropdownlist id="m_ddlXlsFiles" runat="server" Width="385px"></asp:dropdownlist><asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_ddlXlsFiles" Width="112px"></asp:requiredfieldvalidator></TD>
				</TR>
			</TABLE>
			<ml:EncodedLabel id="m_ErrorMessage" runat="server" ForeColor="Red" Font-Size="12px"></ml:EncodedLabel>
			<uc1:cmodaldlg id="Cmodaldlg1" runat="server"></uc1:cmodaldlg></FORM>
	</body>
</HTML>
