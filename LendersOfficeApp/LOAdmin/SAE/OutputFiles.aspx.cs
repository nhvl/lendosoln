using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.SAE
{
    public partial class OutputFiles : SecuredAdminPage
    {
        #region API
        [WebMethod]
        public static IEnumerable<OutputFile> Get(string FileName, bool? Enabled, bool? Dirty)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            return OutputFile.Get(FileName, Enabled, Dirty).ToArray();
        }

        [WebMethod]
        public static string Delete(long id)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return "No Permission"; }

            return OutputFile.Delete(id);
        }

        [WebMethod]
        public static string Update(OutputFile entry)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return "No Permission"; }

            if (!entry.IsValidForSave) { return "Invalid Data"; }

            try { entry.Update(); }
            catch (SqlException e) { return e.Message; }

            return null;
        }

        [WebMethod]
        public static string BatchUpdate(long[] ids, OutputFile inEntry)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return "No Permission"; }

            if (!inEntry.IsValidForBatchUpdate) { return "Invalid Data"; }

            try { OutputFile.BatchUpdate(ids, inEntry); }
            catch (SqlException e) { return e.Message; }
            
            return null;
        }
        #endregion

        public class OutputFile
        {
            public long    ? ID                  { get; set; }
            public string    FileName            { get; set; }
            public char    ? ExportType          { get; set; }
            public string    Worksheet           { get; set; }
            public bool    ? Enabled             { get; set; }
            public bool    ? Dirty               { get; set; }
            public DateTime? dUpdated            { get; set; }
            public string    LastUlResult        { get; set; }
            public long    ? ErrorCount          { get; set; }
            public string    UpdateMonitorEmails { get; set; }

            public bool IsValidForSave
            {
                get
                {
                    return !string.IsNullOrEmpty(this.FileName) 
                        && !string.IsNullOrEmpty(this.Worksheet) 
                        && (this.ExportType == 'R' || this.ExportType == 'A') 
                        && this.Enabled.HasValue 
                        && this.Dirty.HasValue
                        ;
                }
            }
            public bool IsValidForBatchUpdate
            {
                get
                {
                    return
                        (!this.ExportType.HasValue || this.ExportType == 'R' || this.ExportType == 'A')
                        ;
                }
            }

            public OutputFile()
            {
                // for Serialization
            }

            public OutputFile(IDataRecord record)
            {
                this.ID                  = record.GetNullableInteger("ID"                 );
                this.FileName            =        record["FileName"           ] as string;
                this.ExportType          = record.GetNullableChar("ExportType");
                this.Worksheet           =        record["Worksheet"          ] as string;
                this.Enabled             = record.GetNullableChar("Enabled") == 'T';
                this.Dirty               = record.GetNullableChar("Dirty") == 'T';
                this.dUpdated            = record.GetNullableDate("dUpdated"          );
                this.LastUlResult        =        record["LastUlResult"       ] as string;
                this.ErrorCount          = record.GetNullableInteger("ErrorCount"     );
                this.UpdateMonitorEmails =        record["UpdateMonitorEmails"] as string;
            }

            public void Update()
            {
                OutputFile.Update(this);
            }

            public static IEnumerable<OutputFile> Get(string fileName, bool? isEnabled, bool? isDirty)
            {
                fileName = fileName.TrimWhitespaceAndBOM();

                var parameters = new List<SqlParameter>();

                if (!string.IsNullOrEmpty(fileName)) { parameters.Add(new SqlParameter("@FileName", Utilities.SafeSQLString(fileName))); }
                if (isEnabled.HasValue) { parameters.Add(new SqlParameter("@Enabled", isEnabled.Value ? 'T' : 'F')); }
                if (isDirty.HasValue) { parameters.Add(new SqlParameter("@Dirty", isDirty.Value ? 'T' : 'F')); }

                
                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, SP_RetrieveOutputFileList, parameters))
                {
                    while (reader.Read())
                    {
                        yield return new OutputFile(reader);
                    }
                }
            }

            static OutputFile GetById(long id)
            {
                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, SP_RetrieveOutputFileEntryById, new SqlParameter("@Id", id)))
                {
                    if (!reader.Read()) { throw new ApplicationException(string.Format("Output File entry ({0}) does not exist", id)); }

                    return new OutputFile(reader);
                }
            }

            public static string Delete(long id)
            {
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_DeleteOutputFileEntryByID", 0, new SqlParameter("@Id", id));

                return null;
            }

            public static int Update(OutputFile f)
            {
                if (!f.IsValidForSave) throw new ArgumentException("Invalid data", "f");

                var parameters = new List<SqlParameter>(7)
                {
                    new SqlParameter("@FileName", f.FileName),
                    new SqlParameter("@ExportType", f.ExportType),
                    new SqlParameter("@Worksheet", f.Worksheet),
                    new SqlParameter("@Enabled", f.Enabled.Value ? 'T' : 'F'),
                    new SqlParameter("@Dirty", f.Dirty.Value ? 'T' : 'F'),
                };

                if (f.ID.HasValue) { parameters.Add(new SqlParameter("@ID", f.ID.Value)); }
                if (!string.IsNullOrEmpty(f.UpdateMonitorEmails)) { parameters.Add(new SqlParameter("@UpdateMonitorEmails", f.UpdateMonitorEmails)); }

                var storedProcName = f.ID.HasValue ? SP_UpdateOutputFileEntry : SP_CreateOutputFileEntry;
                return StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, storedProcName, 0, parameters);

                // EditOutputFileEntry.aspx?entryId=
            }

            public static void BatchUpdate(long[] ids, OutputFile inEntry)
            {
                if (!inEntry.IsValidForBatchUpdate) throw new ArgumentException("Invalid data", "inEntry");
                
                foreach (var id in ids)
                {
                    var f = GetById(id);
                    if (!string.IsNullOrEmpty(inEntry.FileName)) f.FileName = inEntry.FileName;
                    if (inEntry.ExportType.HasValue) f.ExportType = inEntry.ExportType.Value;
                    if (!string.IsNullOrEmpty(inEntry.Worksheet)) f.Worksheet = inEntry.Worksheet;
                    if (inEntry.Enabled.HasValue) f.Enabled = inEntry.Enabled.Value;
                    if (inEntry.Dirty.HasValue) f.Dirty = inEntry.Dirty.Value;
                    if (inEntry.UpdateMonitorEmails != null) f.UpdateMonitorEmails = inEntry.UpdateMonitorEmails;

                    f.Update();
                }
                
                // OFBatchEdit.aspx
            }

            const string SP_RetrieveOutputFileList = "RS_RetrieveOutputFileList";
            const string SP_RetrieveOutputFileEntryById = "RS_RetrieveOutputFileEntryById";
            const string SP_UpdateOutputFileEntry = "RS_UpdateOutputFileEntry";
            const string SP_CreateOutputFileEntry = "RS_CreateOutputFileEntry";
        }

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.SAE.OutputFiles.js");
        }

        public override string StyleSheet => string.Empty;

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            RegisterResources();
        }

        static readonly E_InternalUserPermissions[] RequiredPermissions = { E_InternalUserPermissions.IsSAE };
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return RequiredPermissions;
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion
    }
}
