<%@ Page language="c#" Codebehind="RsBatchEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.SAE.RsBatchEdit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head runat="server">
    <title>RsBatchEdit</title>
  </head>
  
 <body onload="init();" onunload="window.opener.document.getElementById('m_btnRefresh').click();" MS_POSITIONING="FlowLayout">
		<script type="text/javascript">
		<!--
		function init()
		{
			resizeTo( 600 , 560 );
		}
		
		//-->
		</script>
		
		<FORM id="EditAdmin" method="post" runat="server">
		<input type="hidden" name="RsIds">
			<br>
			<TABLE id="Table1" style="WIDTH: 534px; HEIGHT: 245px" cellSpacing="1" cellPadding="5" border="0">
				<TR>
					<TD align="center" colSpan="3">
						<P><ml:EncodedLabel id="lblPageTitle" runat="server" Font-Size="Medium" Font-Bold="True" Width="401px" Height="15px"></ml:EncodedLabel></P>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 114px" valign="top"><P>Output Filenames<BR>
							</P>
					</TD>
					<TD align="center">
					<asp:datagrid ID="m_dgPrograms" Runat="server" ShowHeader="False" BorderWidth="0" CellPadding=4>			
					<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle></asp:datagrid>
					</TD>
				<TD valign="bottom">Modify</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 114px">Deployment type</TD>
					<TD><asp:dropdownlist id="m_ddlDeployType" runat="server" Width="390px">
							<asp:ListItem Value="AlwaysBlocked">AlwaysBlocked</asp:ListItem>
							<asp:ListItem Value="UseVersion">UseVersion</asp:ListItem>
							<asp:ListItem Value="UseVersionAfterGeneratingAnOutputFileOk">UseVersionAfterGeneratingAnOutputFileOk</asp:ListItem>
						</asp:dropdownlist></TD>
						<td> <asp:CheckBox ID="m_updateDeployment" Runat="server"></asp:CheckBox></td>
				</TR>
				<TR>
					<TD style="WIDTH: 114px">Both Map &amp; Bot Are OK</TD>
					<TD><asp:checkbox id="m_cbUsingForMapAndBot" runat="server"></asp:checkbox></TD>
					<td> <asp:CheckBox ID="m_updateMapAndBot" Runat="server"></asp:CheckBox></td>
				</TR>
				<TR>
					<TD style="WIDTH: 114px">Investor&nbsp;Xls Id</TD>
					<TD><asp:dropdownlist id="m_ddlXlsFiles" runat="server" Width="385px"></asp:dropdownlist><asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_ddlXlsFiles" Width="112px"></asp:requiredfieldvalidator></TD>
					<td> <asp:CheckBox ID="m_updateInvestorList" Runat="server"></asp:CheckBox></td>
				</TR>
                <tr>
                    <td>Latest Version Expired?</td>
                    <td><asp:CheckBox ID="m_latestVersionExpired" runat="server" /></td>
                    <td><asp:CheckBox ID="m_updateLatestVersionExpired" runat="server" /></td>
                </tr>
				<tr><td colspan=3 width=100%>
				<HR style="MARGIN-LEFT: 4px; MARGIN-RIGHT: 4px">
			<DIV align=center style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px"><SPAN style="MARGIN-RIGHT: 8px"><asp:button id="Button1" onclick="OKClick" runat="server" Text="  OK  "></asp:button><INPUT onclick="self.close();" type="button" value="Cancel">
				</SPAN>
			</DIV>
			</td></tr>
			</TABLE>
			
			
			<ml:EncodedLabel id="m_ErrorMessage" runat="server" ForeColor="Red" Font-Size="12px"></ml:EncodedLabel>

			</form>
	</body>
</html>
