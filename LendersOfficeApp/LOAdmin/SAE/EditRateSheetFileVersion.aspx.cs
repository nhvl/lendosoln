using System;
using System.Drawing;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.loadmin.SAE
{
	/// <summary>
	/// Summary description for EditRateSheetFileVersion.
	/// </summary>
	[Obsolete("This page is merged to `RateSheetVersion.aspx`")]
	public partial class EditRateSheetFileVersion : LendersOffice.Admin.SecuredAdminPage
	{

		private   string m_ratesheetID;
		private   long   m_version = -1;
    
		private bool IsNewRecord
		{
			get { return m_version == -1; }
		}

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return new[]
			{
				E_InternalUserPermissions.IsSAE
			};
		}

    
		protected void PageLoad(object sender, System.EventArgs e)
		{



            m_ratesheetID = RequestHelper.GetSafeQueryString("ratesheetId");

            if (m_ratesheetID == null)
            {
                Response.Redirect("~/common/AppError.aspx?errmsg=require ratesheet file id");
                return;
            }
            else
            {
                m_ratesheetID = m_ratesheetID.Replace("&amp;", "&");
            }
            
            
			string version = RequestHelper.GetSafeQueryString("version");
			if( version != null )
			{
				try 
				{
					m_version = int.Parse(version);
				} 
				catch (FormatException) 
				{
					Response.Redirect("~/common/AppError.aspx?errmsg=Invalid Version");
					return;
				}
                
			}

			if( !Page.IsPostBack ) 
			{
				if( IsNewRecord == false)
				{
					AcceptableRsFileVersion rsFileVersion   = new AcceptableRsFileVersion( m_ratesheetID, this.m_version ) ;
					m_txtRsFileID.Text       = rsFileVersion.RatesheetFileId ;
					m_txtVersionNumber.Text  = rsFileVersion.VersionNumber.ToString() + " (auto generated)";
					m_txtCreatedDate.Text    = rsFileVersion.CreatedD.ToString() + " (auto generated)";
					m_txtModifiedDate.Text   = rsFileVersion.ModifiedD.ToString() + " (auto generated)";

					m_txtFirstEffective.Text = rsFileVersion.FirstEffective.ToString();
					m_txtLastEffective.Text  = rsFileVersion.LastestEffective.ToString();
					m_cbExpired.Checked      = rsFileVersion.IsExpirationIssuedByInvestor;
				}
				else 
				{
					if( m_ratesheetID != null )
						m_txtRsFileID.Text   = m_ratesheetID;
					m_txtVersionNumber.Text  = "auto generated";
					m_txtCreatedDate.Text    = "auto generated";
					m_txtModifiedDate.Text   = "auto generated";
				}
			}

			lblPageTitle.Text       = IsNewRecord ? "Create Acceptable Ratesheet File Version" : "Modify Acceptable Ratesheet File Version";
			m_txtRsFileID.ReadOnly  = m_ratesheetID != null;
			m_txtRsFileID.ForeColor = m_ratesheetID != null ? Color.Gray : Color.Black;

			m_txtVersionNumber.ReadOnly  = m_txtCreatedDate.ReadOnly  = m_txtModifiedDate.ReadOnly  = true;
			m_txtVersionNumber.ForeColor = m_txtCreatedDate.ForeColor = m_txtModifiedDate.ForeColor = Color.Gray;

            
		}

		protected void OKClick(object sender, System.EventArgs e)
		{
			if( !Page.IsValid )
				return;

			try
			{
				AcceptableRsFileVersion rsFileVersion  = IsNewRecord ? new AcceptableRsFileVersion() : new AcceptableRsFileVersion( m_ratesheetID, m_version ) ;
				if( IsNewRecord )
				{
					rsFileVersion.RatesheetFileId = m_txtRsFileID.Text;                    
				}
				rsFileVersion.FirstEffective   = DateTime.Parse( m_txtFirstEffective.Text );
				rsFileVersion.LastestEffective = DateTime.Parse( m_txtLastEffective.Text );
				rsFileVersion.IsExpirationIssuedByInvestor = m_cbExpired.Checked;

				rsFileVersion.Save();
				LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "OK", "true") ;
			}
			catch( Exception exc )
			{
				m_ErrorMessage.Text = "Unable to save the Ratesheet File Version. " + exc.ToString();

			}


		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
