using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;

namespace LendersOfficeApp.LOAdmin.SAE
{
	public partial class InvestorXlsFileList : SecuredAdminPage
	{
        #region API
        [WebMethod]
        public static IEnumerable<InvestorXls> Get()
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            return InvestorXls.Get();
        }
        [WebMethod]
        public static InvestorXls GetById(long fileId)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            return InvestorXls.GetById(fileId);
        }

        [WebMethod]
        public static string Save(InvestorXls file)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            return file.Save();
        }
        [WebMethod]
        public static string Delete(long fileId)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }
            if (fileId < 0) { throw new ArgumentException("Invalid fileId"); }

            return InvestorXls.Delete(fileId);
        }
        #endregion

        public class InvestorXls
        {
            public long    ? InvestorXlsFileId              { get; set; }
            public string    InvestorXlsFileName            { get; set; }
            public string    EffectiveDateTimeWorksheetName { get; set; }
            public string    EffectiveDateTimeLandMarkText  { get; set; }
            public int     ? EffectiveDateTimeRowOffset     { get; set; }
            public int     ? EffectiveDateTimeColumnOffset  { get; set; }
            public DateTime? FileVersionDateTime            { get; set; }
            public bool      ExcludeFromInvPricingWarnings  { get; set; }

            public InvestorXls()
            {
                // for serialization
            }

            public InvestorXls(IDataRecord reader)
            {
                this.InvestorXlsFileId              = (long)     reader["InvestorXlsFileId"             ];
                this.InvestorXlsFileName            =            reader["InvestorXlsFileName"           ] as string;
                this.EffectiveDateTimeWorksheetName =            reader["EffectiveDateTimeWorksheetName"] as string;
                this.EffectiveDateTimeLandMarkText  =            reader["EffectiveDateTimeLandMarkText" ] as string;
                this.EffectiveDateTimeRowOffset     = (int)      reader["EffectiveDateTimeRowOffset"    ];
                this.EffectiveDateTimeColumnOffset  = (int)      reader["EffectiveDateTimeColumnOffset" ];
                this.FileVersionDateTime            = (DateTime) reader["FileVersionDateTime"           ];
                this.ExcludeFromInvPricingWarnings  = (bool)     reader["ExcludeFromInvPricingWarnings" ];
            }
            public InvestorXls(InvestorXlsFile f)
            {
                this.InvestorXlsFileId              = f.InvestorXlsFileId;
                this.InvestorXlsFileName            = f.InvestorXlsFileName;
                this.EffectiveDateTimeWorksheetName = f.EffectiveDateTimeWorksheetName;
                this.EffectiveDateTimeLandMarkText  = f.EffectiveDateTimeLandMarkText;
                this.EffectiveDateTimeRowOffset     = f.EffectiveDateTimeRowOffset;
                this.EffectiveDateTimeColumnOffset  = f.EffectiveDateTimeColumnOffset;
                this.ExcludeFromInvPricingWarnings  = f.ExcludeFromInvPricingWarnings;
            }

            public bool IsValid()
            {
                return
                    !string.IsNullOrEmpty(this.InvestorXlsFileName) &&
                    !string.IsNullOrEmpty(this.EffectiveDateTimeWorksheetName) &&
                    !string.IsNullOrEmpty(this.EffectiveDateTimeLandMarkText) &&
                    this.EffectiveDateTimeRowOffset.HasValue &&
                    this.EffectiveDateTimeColumnOffset.HasValue;
            }
            public string Save()
            {
                if (!this.IsValid()) return "Invalid data.";

                var isNewRecord = !this.InvestorXlsFileId.HasValue || (this.InvestorXlsFileId.Value == -1L);
                try
                {
                    var xlsFile = isNewRecord ? new InvestorXlsFile() : new InvestorXlsFile(this.InvestorXlsFileId.Value);

                    xlsFile.InvestorXlsFileName            = this.InvestorXlsFileName;
                    xlsFile.EffectiveDateTimeWorksheetName = this.EffectiveDateTimeWorksheetName;
                    xlsFile.EffectiveDateTimeLandMarkText  = this.EffectiveDateTimeLandMarkText;
                    xlsFile.EffectiveDateTimeRowOffset     = this.EffectiveDateTimeRowOffset.Value;
                    xlsFile.EffectiveDateTimeColumnOffset  = this.EffectiveDateTimeColumnOffset.Value;
                    xlsFile.ExcludeFromInvPricingWarnings  = this.ExcludeFromInvPricingWarnings;

                    xlsFile.Save();
                    this.InvestorXlsFileId = xlsFile.InvestorXlsFileId;
                }
                catch (SqlException e) { return e.Message; }

                return null;
            }

            public static IEnumerable<InvestorXls> Get()
            {
                const string sSQL =
                    @"SELECT
                        InvestorXlsFileId             ,
                        InvestorXlsFileName           ,
                        EffectiveDateTimeWorksheetName,
                        EffectiveDateTimeLandMarkText ,
                        EffectiveDateTimeRowOffset    ,
                        EffectiveDateTimeColumnOffset ,
                        ExcludeFromInvPricingWarnings ,
                        FileVersionDateTime
                    FROM INVESTOR_XLS_FILE
                    ORDER BY InvestorXlsFileName";

                // NOTE: Since the yield statement cannot be executed within
                //       a lambda expression, we cannot take advantage of
                //       the DBInsertUtility methods.
                var sqlQuery = SQLQueryString.Create(sSQL);

                using (var sqlConnection = DbAccessUtils.GetConnection(DataSrc.RateSheet))
                {
                    using (var reader = SqlServerHelper.Select(sqlConnection, null, sqlQuery.Value, null, TimeoutInSeconds.Thirty))
                    {
                        while (reader.Read())
                        {
                            yield return new InvestorXls(reader);
                        }
                    }
                }
            }

            public static string Delete(long xlsFileId)
	        {
                try { InvestorXlsFile.RemoveFromDB(xlsFileId); }
                catch (SqlException e) { return e.Message; }

	            return null;
	        }

            public static InvestorXls GetById(long xlsFileId)
            {
                var xlsFile = new InvestorXlsFile(xlsFileId);
                return new InvestorXls(xlsFile);
            }
        }

        static readonly E_InternalUserPermissions[] RequiredPermissions = { E_InternalUserPermissions.IsSAE };
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return RequiredPermissions;
		}

        void RegisterResources()
        {
            RegisterJsScript("LOAdmin.SAE.InvestorXlsFileList.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        protected void PageLoad(object sender, EventArgs e)
        {
            RegisterResources();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
        #endregion
    }
}
