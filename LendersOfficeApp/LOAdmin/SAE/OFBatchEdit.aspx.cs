using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.IO;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.SAE
{
     [Obsolete("This page is merged top OutputFiles.aspx page.")]
	public partial class OFBatchEdit : LendersOffice.Admin.SecuredAdminPage
	{

		private ArrayList m_OFIds;

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsSAE
			};
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			lblPageTitle.Text = "Batch Edit Output Files";

			//Load the Ids from post, and if the page is a postback, get the Id's from the ViewState
			string sIds = Request["RsIds"];
			string sFNs = Request["FNs"];

			if( Page.IsPostBack )
			{
				sIds = (String)ViewState["sIds"];
				sFNs = (String)ViewState["sFNs"];
			}

			//Parse the Ids from the Sids string into the m_OFIds arraylist
			m_OFIds = new ArrayList();
			string sTemp = "";
			StringReader sr = new StringReader(sIds);
			int nextChar = sr.Read();
			while(nextChar != -1)
			{
				if((char)nextChar == ',')
				{
					m_OFIds.Add(sTemp.TrimWhitespaceAndBOM());
					sTemp = "";
				}
				else
				{
					sTemp += (char)nextChar;
				}
				nextChar = sr.Read();
			}

			//Parse the File Names from the FNs string into the m_FNs Arraylist
			ArrayList m_FNs = new ArrayList();
			string sTemp2 = "";
			StringReader sr2 = new StringReader(sFNs);
			int nextChar2 = sr2.Read();
			while(nextChar2 != -1)
			{
				if((char)nextChar2 == ',')
				{
					m_FNs.Add(sTemp2.TrimWhitespaceAndBOM());
					sTemp2 = "";
				}
				else
				{
					sTemp2 += (char)nextChar2;
				}
				nextChar2 = sr2.Read();
			}

			ArrayList entries = new ArrayList();

			// Combine the two lists to display both the ids and the Descriptions
			for(int i=0; i< m_OFIds.Count; i++)
			{
				entries.Add(m_OFIds[i] + " - " + m_FNs[i]);
			}

			if( !Page.IsPostBack )
			{
				ViewState["sIds"] = sIds;
				ViewState["sFNs"] = sFNs;
				m_customerIds.DataSource = entries;
				m_customerIds.DataBind();
			}
		}

		protected void OKClick(object sender, System.EventArgs e)
		{
			// If the update checkboxes aren't checked, then that field
			// should be validated
			if(!m_updateFileName.Checked)
			{
				m_fileNameVal.IsValid = true;
			}
			if(!m_updateExportType.Checked)
			{
				m_exportTypeVal.IsValid = true;
			}
			if(!m_updateWorksheetName.Checked)
			{
				m_wsNameVal.IsValid = true;
			}
			if(!m_updateEnabled.Checked)
			{
				m_enabledVal.IsValid = true;
			}
			if(!m_updateDirty.Checked)
			{
				m_dirtyVal.IsValid = true;
			}

			if( !Page.IsValid )
			{
				return;
			}

			m_customerIds.DataBind();

			try
			{
				//Iterate through each ID, updating only the boxes that are checked
				for(int i=0;i<m_OFIds.Count; i++)
				{

					string id = m_OFIds[i].ToString();
					string fileName = null;
					string exportType = null;
					string worksheet = null;
					string enabled = null;
					string dirty = null;
					string updateMonitorEmails = null;

					// Gets all of the info for the Output File Entry
					using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_RetrieveOutputFileEntryById", new SqlParameter("@Id", id) ) )
					{
						if(reader.Read())
						{
							fileName = reader["FileName"].ToString();
							exportType = reader["ExportType"].ToString();
							worksheet = reader["Worksheet"].ToString();
							enabled = reader["Enabled"].ToString();
							dirty = reader["Dirty"].ToString();
							updateMonitorEmails = reader["UpdateMonitorEmails"].ToString();
						}
					}

					if(m_updateFileName.Checked)
					{
						fileName = m_fileName.Text;
					}

					if(m_updateExportType.Checked)
					{
						exportType = (m_exportType.SelectedIndex == 0)?"R":"A";
					}

					if(m_updateWorksheetName.Checked)
					{
						worksheet = m_worksheetName.Text;
					}

					if(m_updateEnabled.Checked)
					{
						enabled = (m_enabled.SelectedIndex == 0)?"T":"F";
					}

					if(m_updateDirty.Checked)
					{
						dirty = (m_dirty.SelectedIndex == 0)?"T":"F";
					}

					if(m_updateUpdateMonitorEmails.Checked)
					{
						updateMonitorEmails = m_updateMonitorEmails.Text;
					}

                    List<SqlParameter> parameters = new List<SqlParameter>();

					parameters.Add(new SqlParameter("@ID", id));
					parameters.Add(new SqlParameter("@FileName", fileName));
					parameters.Add(new SqlParameter("@ExportType", exportType));
					parameters.Add(new SqlParameter("@Worksheet", worksheet));
					parameters.Add(new SqlParameter("@Enabled", enabled));
					parameters.Add(new SqlParameter("@Dirty", dirty));
					parameters.Add(new SqlParameter("@UpdateMonitorEmails", updateMonitorEmails));

					string storedProcName = "RS_UpdateOutputFileEntry";
					StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, storedProcName, 0, parameters);
				}

				LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this) ;
			}
			catch( Exception exc )
			{
				m_ErrorMessage.Text = "Unable to save the Output File Entry. " + exc.Message;
			}


		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
