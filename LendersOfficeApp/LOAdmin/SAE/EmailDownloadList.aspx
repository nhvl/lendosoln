<%@ Page Language="C#" CodeBehind="EmailDownloadList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.SAE.EmailDownloadList"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Email Download List - LendingQB" %>

<asp:Content ContentPlaceHolderID="Main" runat="Server">

    <div class="ng-cloak container-fuild" ng-app="app">
        <email-download-manager>loading&hellip;</email-download-manager>
    </div>

    <form runat="server"></form>
</asp:Content>
