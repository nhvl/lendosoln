<%@ Page language="C#" Codebehind="DownloadList.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.SAE.DownloadList"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Download List - LendingQB" %>

<asp:Content ContentPlaceHolderID="Main" Runat="Server">

    <div class="ng-cloak container-fuild" ng-app="app">
        <download-manager>loading&hellip;</download-manager>
    </div>

    <form runat="server"></form>
</asp:Content>
