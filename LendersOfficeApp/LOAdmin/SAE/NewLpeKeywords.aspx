<%@ Page language="C#" Codebehind="NewLpeKeywords.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Internal.NewLpeKeywords"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="New Lpe Keywords" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <style type="text/css">
    .center-div{
            margin: 0 auto;
            width: 50%;
        }
    /*Show loading animate*/.glyphicon-refresh-animate
        {
            -animation: spin .7s infinite linear;
            -ms-animation: spin .7s infinite linear;
            -webkit-animation: spinw .7s infinite linear;
            -moz-animation: spinm .7s infinite linear;
        }
        @keyframes spin{from{transform:scale(1)rotate(0deg);}
        to
        {
            transform: scale(1) rotate(360deg);
        }
        }@-webkit-keyframes spinw{from{-webkit-transform:rotate(0deg);}
        to
        {
            -webkit-transform: rotate(360deg);
        }
        }@-moz-keyframes spinm{from{-moz-transform:rotate(0deg);}
        to
        {
            -moz-transform: rotate(360deg);
        }
        }/******/
    </style>
    <script>        
        function onNew()
        {
	        //showModal('/LOAdmin/SAE/EditNewLpeKeyword.aspx', null, null, null, function(arg){ 
	        //if (arg.OK) 
            //    document.NewLpeKeywords.submit();	// refresh
            // })

            $('#popup_content').attr('src', './EditNewLpeKeyword.aspx');
            $('#myModal .modal-title').html("Add New LPE Keyword");
	        $('#myModal').modal('show');
        }
        
        function onEdit(keyword)
        {
	        //showModal('/LOAdmin/SAE/EditNewLpeKeyword.aspx?keyword=' + keyword, null, null, null, function(arg){ 
        	//if (arg.OK) 
            //    document.NewLpeKeywords.submit();	// refresh
            //})

        	$('#loadingModal').show();
        	$('#popup_content').css('visibility', 'hidden');
        	$('#popup_content').attr('src', './EditNewLpeKeyword.aspx?keyword=' + keyword);
        	$('#myModal .modal-title').html("Edit New LPE Keyword");
        	$('#myModal').modal('show');
        	return false;
        }
        
        function onDelete(keyword)
        {
            confirmDialog("Delete New LPE Keyword " + keyword + "?", keyword);
            return false;
        }
        //Call this function when get back from modal popup
        function callback(data, textStatus, jqXHR) {
            if (data.d === null) {
                $('#myModal').modal('hide');
                location.reload();
            }
            else
                alert(textStatus);
        }
        function OnLoadedModal() {
            $('#loadingModal').hide();
            $('#popup_content').css('visibility', 'visible');
        }
        function confirmDialog(message, data) {
            $('#myConfirm .modal-body').html(message);
            $('#myConfirm #btnOK').click(function () {
                $('#myConfirm').modal('hide');
                <%= AspxTools.JsGetElementById(m_Command) %>.value = "delete";
                <%= AspxTools.JsGetElementById(m_keyword) %>.value = data;
                //$("[id$='DeleteVendor']").val(data);
                $("form:first").submit();
                return false;
            });
            $('#myConfirm').modal('show');
        }
        
        </SCRIPT>
    <div class="container-fluid"><div class="row"><div class="center-div"><div class="panel panel-default"><div class="panel-body">
        <form id="NewLpeKeywords" method="post" runat="server">
            <input type="hidden" runat="server" id="m_Command" NAME="m_Command"> <input type="hidden" runat="server" id="m_keyword" NAME="m_keyword">
            <div class="form-group">
                <span class="clipper">
                    <a class="btn btn-primary" onclick="onNew();">Add New</a>
                </span>
                <span class="clipper">
                    <a class="btn btn-default" onclick="location.reload();">Refresh</a>
                </span>
                <span class="clipper">
                    <a class="btn btn-default" data-toggle="modal" data-target="#helpModal">Help</a>
                </span>
            </div>
            <hr />
            <asp:GridView runat="server" ID="m_Grid" CssClass="table table-striped table-condensed table-layout-fix"
                            CellSpacing="0" CellPadding="0" BorderWidth="0" GridLines="None" UseAccessibleHeader="true"
                            AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField >
                                    <ItemTemplate>
                                        <a href="javascript:void(0)" onclick=<%# AspxTools.HtmlAttribute("return onEdit(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "NewKeywordName").ToString()) + ");")%>>edit</a>&nbsp;&nbsp;
                                        <a href="javascript:void(0)" onclick=<%# AspxTools.HtmlAttribute("return onDelete(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "NewKeywordName").ToString()) + ");")%>>delete</a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Keyword" DataField="NewKeywordName" HeaderStyle-Width="20%"/>
                                <asp:BoundField HeaderText="Default Value" DataField="DefaultValue"/>
                                <asp:BoundField HeaderText="Category" DataField="Category" HeaderStyle-Width="20%"/>
                                <asp:TemplateField HeaderText="Note">
                                    <ItemTemplate>
                                        <span class="text-danger">
                                            <%#AspxTools.HtmlString(DataBinder.Eval(Container, "DataItem.Note"))%>
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
            <hr />
            <b>The list of official keywords and new keywords.</b>
            <p>
            <select id="m_lstbxAllSymbol" runat="server" NAME="m_lstbxAllSymbol" size="20" class="form-control"></select>
            </p>
            <p>Note : The new keywords are reloaded every 10 minutes. Therefore the above list <b>maybe 
                                doesn't contain&nbsp;new&nbsp;information that user just added or modified</b>.</p>
            </form>
            <!--help Dialog-->
            <div id="helpModal" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">
                                            &times;</button>
                                        <h4 class="modal-title">
                                            New LPE Keyword</h4>
                                    </div>
                                    <div class="modal-body">
                                        <iframe width="100%" frameborder="0" height="320px" onload="OnLoadedModal();"
                                            src='./NewLpeKeywordsHelp.html'></iframe>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                        Close</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
            <!--Confirm Delete-->
                        <div id="myConfirm" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog modal-width">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">
                                             Confirm Delete
                                        </h4>
                                    </div>
                                    <div class="modal-body text-center">

                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel</button></span> <span class="clipper">
                                                    <button type="button" class="btn btn-primary" id="btnOK">
                                                        Delete</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
            <!-- Modal -->
                        <div id="myModal" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">
                                            &times;</button>
                                        <h4 class="modal-title">
                                            Appraisal Vendor</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div id="loadingModal">
                                            <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>Loading...</div>
                                        <iframe id="popup_content" width="100%" frameborder="0" height="320px" onload="OnLoadedModal();"
                                            src='./EditNewLpeKeyword.aspx'></iframe>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel</button></span> <span class="clipper">
                                                    <button type="button" class="btn btn-primary" onclick="document.getElementById('popup_content').contentWindow.sendSubmit(callback);">
                                                        Save</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
        
        </div></div></div></div></div>
</asp:Content>
