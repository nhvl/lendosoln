<%@ Page Language="C#" CodeBehind="OutputFiles.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.SAE.OutputFiles"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Investor Ratesheets - LendingQB" %>

<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <div class="container-fluid" ng-app="app">
        <of-manager></of-manager>
    </div>
    <form runat="server"></form>
</asp:Content>
