<%@ Page language="C#" Codebehind="PlanCodeEditor.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.SAE.PlanCodeEditor"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Edit DocMagic Plan Codes - LendingQB" %>

<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <div class="ng-cloak container-fluid" ng-app="app" ng-controller="DlManager as vm">
        <div class="panel">
            <div class="panel-body">
                <form runat="server">
                    <div id="importModal" class="modal fade"><div class="modal-dialog modal-sm_"><div class="modal-content">
                        <div class="modal-header"><h4 class="modal-title">
                            Import a CSV file:
                        </h4></div>
                        <div class="modal-body">
                            <input type="file" id="CsvUpload" runat="server"
                                accept=".csv" pattern=".*\.[cC][sS][vV]"/>
                        </div>
                        <div class="modal-footer">
                            <span class="clipper"><a class="btn btn-default" data-dismiss="modal">Close</a></span>
                            <span class="clipper">
                                <asp:Button OnClick="UploadClick" runat="server"
                                    Text="Upload" CssClass="btn btn-primary"/>
                            </span>
                        </div>
                    </div></div></div>

                    <div class="clearfix">
                        <span class="clipper"><a class="btn btn-default"
                            data-toggle="modal" data-target="#importModal">Import CSV</a></span>
                        <span class="clipper"><asp:Button OnClick="DownloadClick" runat="server"
                            Text="Download CSV" CssClass="btn btn-default"/></span>
                        <span class="clipper"><a ng-click="vm.continueImport()"
                            ng-show="vm.isImporting"
                            ng-class="{disabled:vm.isLoading.import}"
                            class="btn btn-primary">Continue Import</a></span>

                        <div class="form-inline form-group-sm pull-right">
                            <div class="form-group">
                                <label>Investor:</label>
                                <select ng-model="vm.filterSpec.InvestorNm"
                                    ng-options="i.v as i.t for i in vm.investors"
                                    class="form-control"></select>
                            </div>

                            <div class="form-group">
                                <label>Plan Code:</label>
                                <input ng-model="vm.filterSpec.PlanCodeNm"
                                    ng-model-options="{ updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } }"
                                    type="text"
                                    class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Description:</label>
                                <input ng-model="vm.filterSpec.PlanCodeDesc"
                                    ng-model-options="{ updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } }"
                                    type="text"
                                    class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="panel-body">
                <div ng-show="vm.isImporting && (vm.importErrors != null) && vm.importErrors.length > 0">
                    <h4>The following import errors occurred:</h4>
                    <ul><li ng-repeat="e in vm.importErrors track by e.PlanCodeNm" ng-attr-id="el_{{e.PlanCodeNm}}">
                        <div ng-hide="!(e.PlanCodeNm)">
                            <a ng-bind="e.PlanCodeNm" ng-href="#tr_{{e.PlanCodeNm}}"></a>:
                            <span ng-repeat="m in e.Messages">
                                <span ng-bind="m"></span><span ng-hide="$last">; </span>
                            </span>
                        </div>
                        <div ng-show="!(e.PlanCodeNm)">
                            <div ng-show="e.PlanCodeNm != null">
                                Line number <span ng-bind="e.LineNum"></span>:
                                <span ng-bind="e.Messages[0]"></span>
                            </div>
                            <div ng-show="e.PlanCodeNm == null">
                                An unspecified error occurred
                            </div>
                        </div>
                    </li></ul>
                </div>
                <div ng-show="!vm.isImporting && vm.saveErrors"><h4>The following save errors occurred:</h4>
                    <ul><li ng-repeat="e in vm.saveErrors track by e.PlanCodeNm" ng-attr-id="el_{{e.PlanCodeNm}}">
                        <div ng-hide="!(e.PlanCodeNm)">
                            <a ng-bind="e.PlanCodeNm" ng-href="#tr_{{e.PlanCodeNm}}"></a>:
                            <span ng-repeat="m in e.Messages">
                                <span ng-bind="m"></span><span ng-hide="$last">; </span>
                            </span>
                        </div>
                        <div ng-show="!(e.PlanCodeNm)">
                            <div ng-show="e.PlanCodeNm != null">
                                Line number <span ng-bind="e.LineNum"></span>:
                                <span ng-bind="e.Messages[0]"></span>
                            </div>
                            <div ng-show="e.PlanCodeNm == null">
                                An unspecified error occurred
                            </div>
                        </div>
                    </li></ul>
                </div>
            </div>

            <div class="table-responsive"><table class="table table-striped table-condensed table-layout-fix">
                <thead><tr>
                    <th style="width: 5%" title="edit"></th>
                    <th style="width:10%"><a sort-handle="vm.entrySort" key="PlanCodeNm"    >Plan Code          </a></th>
                    <th style="width:25%"><a sort-handle="vm.entrySort" key="PlanCodeDesc"  >Program Description</a></th>
                    <th style="width:10%"><a sort-handle="vm.entrySort" key="InvestorNm"    >Investor           </a></th>
                    <th style="width:50%"><a sort-handle="vm.entrySort" key="RuleXmlContent">Rules              </a></th>
                </tr></thead>
                <tbody><tr ng-repeat="e in vm.entries | filter:vm.filterSpec | orderBy:vm.entrySort.by:vm.entrySort.desc track by $index"
                    ng-attr-id="tr_{{e.PlanCodeNm}}" ng-class="{info: e.isSelected, danger:e.HasError}">
                    <td><a ng-click="vm.edit(e)" class="btn btn-link"
                            data-toggle="modal" data-target="#editEntryModal">edit</a></td>
                    <td><div ng-bind="e.PlanCodeNm    " select-when-clicked></div>
                        <a ng-show="e.HasError" ng-href="#el_{{e.PlanCodeNm}}">(Back to error list)</a>
                    </td>
                    <td><span ng-bind="e.PlanCodeDesc  " select-when-clicked></span></td>
                    <td><span ng-bind="e.InvestorNm    " select-when-clicked></span></td>
                    <td><samp ng-bind="e.RuleXmlContent" select-when-clicked></samp></td>
                </tr></tbody>
            </table></div>

            <div class="panel-body">
                <p>Current investor synonyms: <span ng-bind="vm.investorNameSynonyms"></span></p>
            </div>
        </div>

        <div id="editEntryModal" class="modal fade"><div class="modal-dialog modal-lg"><div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                <h4 class="modal-title">
                    <span ng-show="!vm.isBatchEdit" ng-attr-title="vm.eEntry.PlanCodeId">
                        <span ng-hide="!vm.eEntry.PlanCodeId">Modify</span>
                        <span ng-show="!vm.eEntry.PlanCodeId">Create</span>
                        Plan Code
                    </span>
                    <span ng-hide="!vm.isBatchEdit">Batch Edit Plan Codes</span>
                </h4>
            </div>
            <form name="editEntryForm" class="form-group-sm form-adjust-mb" ng-submit="vm.save($event)">
                <div class="modal-body">
                    <div class="form-group" ng-hide="(!vm.eEntry.PlanCodeId) || vm.isBatchEdit">
                        <label>Plan Code:</label>
                        <input ng-model="vm.eEntry.PlanCodeNm"
                            ng-required="vm.isBatchEdit" readonly
                            type="text"
                            class="form-control"/>
                    </div>

                    <div class="form-group">
                        <div ng-class="{checkbox:vm.isBatchEdit}"><label>
                            <input type="checkbox" ng-show="vm.isBatchEdit" ng-model="vm.batToggle.PlanCodeDesc"/>
                            Program Description:</label></div>
                        <input ng-model="vm.eEntry.PlanCodeDesc"
                            type="text" readonly
                            ng-disabled="vm.isBatchEdit && !vm.batToggle.PlanCodeDesc"
                            class="form-control"/>
                    </div>

                    <div class="form-group">
                        <div ng-class="{checkbox:vm.isBatchEdit}"><label>
                            <input type="checkbox" ng-show="vm.isBatchEdit" ng-model="vm.batToggle.InvestorNm"/>
                            Investor:</label></div>
                        <input ng-model="vm.eEntry.InvestorNm"
                            type="text" readonly
                            ng-disabled="vm.isBatchEdit && !vm.batToggle.InvestorNm"
                            class="form-control"/>
                    </div>

                    <div class="form-group">
                        <div ng-class="{checkbox:vm.isBatchEdit}"><label>
                            <input type="checkbox" ng-show="vm.isBatchEdit" ng-model="vm.batToggle.RuleXmlContent"/>
                            Rules:</label></div>
                        <textarea ng-model="vm.eEntry.RuleXmlContent"
                            ng-disabled="vm.isBatchEdit && !vm.batToggle.RuleXmlContent"
                            class="form-control resizable-vertical" rows="5"></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <span class="clipper"><a class="btn btn-default" data-dismiss="modal">Close</a></span>
                    <span class="clipper"><button type="submit" class="btn btn-primary">Save</button></span>
                </div>
            </form>
        </div></div></div>
    </div>
</asp:Content>
