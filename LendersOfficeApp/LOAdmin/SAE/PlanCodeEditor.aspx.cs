using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Services;
using CommonProjectLib.Common.Lib;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common.TextImport;
using LendersOffice.ObjLib.DocMagicLib;

namespace LendersOfficeApp.LOAdmin.SAE
{
    public partial class PlanCodeEditor : SecuredAdminPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var cs = DocMagicPlanCode.ListAllDocMagicPlanCodes(BrokerDB.NULL)
                    .Select(c => new PlanCodeDTO(c))
                    .ToArray();

                (new PlanCodeEditorRespone
                {
                    planCodes = cs,
                    investorNameSynonyms = LendersOffice.Constants.ConstStage.InvestorNameSynonymsRaw,
                }).RegisterToPageAsJSObject(this);
            }
            RegisterResources();
        }

        [WebMethod]
        public static string[] Update(PlanCodeDTO planCode)
        {
            var allPlanCodesBeforeSave = DocMagicPlanCode.ListAllDocMagicPlanCodes(BrokerDB.NULL).ToArray();
            var id2PlanCode = allPlanCodesBeforeSave.ToDictionary(a => a.PlanCodeId);

            var plans = new[] {planCode};
            var validationErrors = ValidateDTOs(plans).ToArray();
            if (validationErrors.Length > 0) return validationErrors.SelectMany(e => e.Messages).ToArray();

            var pcPairToSave = GetChangedPlanCodes(plans, id2PlanCode).ToArray();

            Debug.Assert(pcPairToSave.Length == 1);
            var pair = pcPairToSave[0];
            var planCodeToSave = pair.Item1;
            var inputPlanCode = pair.Item2;

            var isNewPlanCode = planCodeToSave.PlanCodeId == Guid.Empty;
            Debug.Assert(!isNewPlanCode);

            string errorMessage = inputPlanCode.ValidateError();
            if (!string.IsNullOrEmpty(errorMessage)){ return new[]{errorMessage}; }

            inputPlanCode.ApplyTo(planCodeToSave);

            try { planCodeToSave.Save(); }
            catch (SqlException)
            {
                var log = new SavePlanCodeLogger();
                log.LogSqlExceptionWhileSaving(inputPlanCode);
                Tools.LogInfo(log.ToString());

                return new[]{"Could not save plan code: " + inputPlanCode.PlanCodeNm};
            }

            return null;
        }

        [WebMethod]
        public static PlanCodeEditorRespone Import(PlanCodeDTO[] plans)
        {
            var log = new SavePlanCodeLogger();

            var allPlanCodesBeforeSave = DocMagicPlanCode.ListAllDocMagicPlanCodes(BrokerDB.NULL).ToArray();
            var id2PlanCode = allPlanCodesBeforeSave.ToDictionary(a => a.PlanCodeId);
            log.LogAllCodesBeforeSave(allPlanCodesBeforeSave);

            var validationErrors = ValidateDTOs(plans);
            log.LogCodesToSave(plans);

            var pcPairToSave = GetChangedPlanCodes(plans, id2PlanCode);

            var newIds = new List<PlanCodeDTO>();
            var saveErrors = new List<PlanCodeError>();
            foreach (var pair in pcPairToSave)
            {
                var planCodeToSave = pair.Item1;
                var inputPlanCode = pair.Item2;
                try
                {
                    var isNewPlanCode = planCodeToSave.PlanCodeId == Guid.Empty;

                    inputPlanCode.ApplyTo(planCodeToSave);
                    planCodeToSave.Save();

                    if (isNewPlanCode)
                    {
                        newIds.Add(new PlanCodeDTO
                        {
                            PlanCodeId = planCodeToSave.PlanCodeId,
                            PlanCodeNm = planCodeToSave.PlanCodeNm
                        });
                    }
                }
                catch (SqlException)
                {
                    log.LogSqlExceptionWhileSaving(inputPlanCode);
                    saveErrors.Add(new PlanCodeError
                    {
                        Messages = new []{ "Could not save plan code: " + inputPlanCode.PlanCodeNm },
                        PlanCodeNm = inputPlanCode.PlanCodeNm
                    });
                }
            }

            #region Delete missing plan codes
            {
                var planCodesToDelete = allPlanCodesBeforeSave.ToDictionary(a => a.PlanCodeId);
                foreach (var plan in plans)
                {
                    if ((plan.PlanCodeId == Guid.Empty) || (!planCodesToDelete.ContainsKey(plan.PlanCodeId))) { continue; }
                    planCodesToDelete.Remove(plan.PlanCodeId);
                }

                log.BeforeDeleting();
                foreach (var plan in planCodesToDelete.Values)
                {
                    log.LogDeleteingPlan(plan);
                    DocMagicPlanCode.Delete(plan.PlanCodeId);
                }
            }
            #endregion

            Tools.LogInfo(log.ToString());

            return new PlanCodeEditorRespone
            {
                saveErrors = validationErrors.Concat(saveErrors).ToArray(),
                newPlanCodes = newIds.ToArray(),
            };
        }

        protected void UploadClick(object sender, EventArgs e)
        {
            string tempFilename; // save posted file to temp file
            {
                var postedFile = CsvUpload.PostedFile;
                tempFilename = Path.GetTempFileName() + Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(tempFilename);
            }

            PlanCodeError[] errors;
            var cs = ImportCsv(tempFilename, out errors);

            (new PlanCodeEditorRespone
            {
                planCodes = cs,
                importErrors = errors,
                investorNameSynonyms = LendersOffice.Constants.ConstStage.InvestorNameSynonymsRaw,
                isImporting = true,
            }).RegisterToPageAsJSObject(this);
        }

        static PlanCodeDTO[] ImportCsv(string csvFile, out PlanCodeError[] errors)
        {
            DataTable dataTable;
            {
                ILookup<int, string> parseErrors;
                dataTable = DataParsing.FileToDataTable(csvFile, true, "", out parseErrors);

                //Validate that the file we got looks okay
                errors = GetError(dataTable, parseErrors).ToArray();
                if (errors.Length > 0) { return null; }
            }

            var inPlanCodes =
                dataTable.Rows.Cast<DataRow>()
                .Select(row => new PlanCodeDTO(row))
                .ToArray();

            errors = ValidateDTOs(inPlanCodes).ToArray();

            // Add PlanCodeId to import data (look up form DB)
            {
                var name2Id = DocMagicPlanCode.ListAllDocMagicPlanCodes(BrokerDB.NULL)
                    .GroupBy(a => a.PlanCodeNm).Select(a => a.First())
                    .ToDictionary(a => a.PlanCodeNm, a => a.PlanCodeId);
                foreach (var c in inPlanCodes) { if (name2Id.ContainsKey(c.PlanCodeNm)) { c.PlanCodeId = name2Id[c.PlanCodeNm]; } }
            }
            return inPlanCodes;

            //var dbPlanCode = DocMagicPlanCode.ListAllDocMagicPlanCodes()
            //// OPM 87440: Don't crash if there's two plan codes with the same name. This shouldn't ever happen.
            //            .GroupBy(a => a.PlanCodeNm).Select(a => a.First())
            //            .ToDictionary(a => a.PlanCodeNm);
            //var transferBack = new List<DocMagicPlanCode>(dataTable.Rows.Count);
            //foreach (var dto in inPlanCodes)
            //{
            //    var dmpc = !dbPlanCode.ContainsKey(dto.PlanCodeNm) ? DocMagicPlanCode.Create() : dbPlanCode[dto.PlanCodeNm];

            //    dto.ApplyTo(dmpc);
            //    transferBack.Add(dmpc);
            //}

            // return transferBack;
        }

        /// <summary>
        /// Validates Doc Magic Plan Code data transfer objects.
        /// If object's Rules field doesn't pass validation, it's cleared out.
        /// </summary>
        /// <param name="toVerify"></param>
        /// <returns></returns>
        static IEnumerable<PlanCodeError> ValidateDTOs(PlanCodeDTO[] toVerify)
        {
            var validationErrors = DocMagicPlanCode.ValidatePlanCodeData(BrokerDB.NULL /* This is internal admin edit page. Okay to pass null as broker. */,
                toVerify.Select(a => Tuple.Create(a.InvestorNm, a.RuleXmlContent, a.PlanCodeNm)).ToArray());

            foreach (var idx in validationErrors.Keys)
            {
                var planCode = toVerify[idx];
                planCode.HasError = true;

                //This is kind of a hack, but the only way around it would be to return something else
                //that indicates which field errored and I don't think we need the extra complexity.
                if (!validationErrors[idx].All(a => new[] {"Plan code [", "Investor name ["}.Any(a.StartsWith)))
                {
                    //We need to wipe out the rules if they don't validate because otherwise, when we
                    //go to apply this DTO to a real object, the object will throw an exception.
                    planCode.RuleXmlContent = string.Empty;
                }

                yield return (new PlanCodeError { PlanCodeNm = planCode.PlanCodeNm, Messages = validationErrors[idx].ToArray() });
            }
        }

        static IEnumerable<Tuple<DocMagicPlanCode, PlanCodeDTO>> GetChangedPlanCodes(
            IEnumerable<PlanCodeDTO> plans, Dictionary<Guid, DocMagicPlanCode> id2PlanCode)
        {
            return plans.Where(planCode => !planCode.HasError)
                .Select(planCode => new
                {
                    planCode,
                    dbPlanCode = id2PlanCode.ContainsKey(planCode.PlanCodeId)
                        ? id2PlanCode[planCode.PlanCodeId]
                        : DocMagicPlanCode.Create()
                })
                .Where(pair => pair.planCode.NeedsSave(pair.dbPlanCode))
                .Select(pair => Tuple.Create(pair.dbPlanCode, pair.planCode));
        }

        protected void DownloadClick(object sender, EventArgs e)
        {
            var columnNameMap = new Dictionary<string, string>
            {
                {"PlanCodeNm"    , "Plan Code"},
                {"PlanCodeDesc"  , "Program Description"},
                {"InvestorNm"    , "Investor"},
                {"RuleXmlContent", "Rules"}
            };
            var dt = DocMagicPlanCode.ListAllDocMagicPlanCodes(BrokerDB.NULL).ToDataTable(columnNameMap.Keys.ToList());

            foreach (var kvp in columnNameMap)
            {
                dt.Columns[kvp.Key].ColumnName = kvp.Value;
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AppendHeader("Content-Disposition", "attachment; filename=\"DocMagicPlanCodeExport.csv\"");
            Response.Write(dt.ToCSVWithHeader());
            Response.End();
        }

        static IEnumerable<PlanCodeError> GetError(DataTable dataTable, ILookup<int, string> parseErrors)
        {
            var columnErrors = DataParsing.ValidateColumns(dataTable,
                new[] {"Plan Code", "Program Description", "Investor", "Rules"}, // all column names
                new[] {"Plan Code"}, //required column names
                1); //In the source file, the header row starts at line 1

            if (parseErrors.Count + columnErrors.Count < 1) yield break;

            //If we couldn't parse it, don't try salvaging anything; let the user know about the errors.

            var errorLines = parseErrors.Select((a) => a.Key)
                .Union(columnErrors.Select(a => a.Key))
                .OrderBy(a => a);

            foreach (var lineNum in errorLines)
            {
                var errorMessages = parseErrors[lineNum].Union(columnErrors[lineNum]);
                foreach (var message in errorMessages)
                {
                    yield return new PlanCodeError
                    {
                        LineNum = lineNum,
                        Messages = new[] {message}
                    };
                }
            }
        }

        public class PlanCodeEditorRespone
        {
            public PlanCodeError[] importErrors { get; set; } // Errors of importing plan codes
            public PlanCodeError[] saveErrors   { get; set; } // Errors while save plan codes
            public PlanCodeDTO[]   newPlanCodes { get; set; } // only PlanCodeId - PlanCodeNm
            public PlanCodeDTO[]   planCodes    { get; set; } // The Plan code on DB or importing file
            public bool            isImporting  { get; set; } // indicate that is middle of importing
            public string investorNameSynonyms  { get; set; }

            const string JsObjectName = "pceData";

            internal void RegisterToPageAsJSObject(PlanCodeEditor page)
            {
                page.RegisterJsObjectWithJsonNetSerializer(JsObjectName, this);
            }
        }

        class SavePlanCodeLogger
        {
            readonly StringBuilder log;

            public SavePlanCodeLogger()
            {
                log = new StringBuilder("Saving new doc magic plan codes" + Environment.NewLine);
            }

            public void LogAllCodesBeforeSave(DocMagicPlanCode[] codes)
            {
                log.AppendLine("All plan codes before save: ");
                if (codes.Length < 1) { log.AppendLine("No plan codes currently present"); }
                else
                {
                    foreach (var pc in codes)
                    {
                        log.AppendLine(pc.PlanCodeNm);
                    }
                }
            }

            public void LogCodesToSave(PlanCodeDTO[] plans)
            {
                log.AppendLine("Plan codes to save: ");
                if (plans.Length < 1) { log.AppendLine("No plan codes to save"); }
                else
                {
                    foreach (var pc in plans)
                    {
                        log.AppendLine(pc.PlanCodeNm);
                    }
                }
            }

            public void LogSqlExceptionWhileSaving(PlanCodeDTO planCode)
            {
                log.AppendLine("SQL exception while saving " + planCode.PlanCodeNm);
            }

            public void BeforeDeleting()
            {
                log.AppendLine("Deleting: ");
            }

            public void LogDeleteingPlan(DocMagicPlanCode plan)
            {
                log.Append(plan.PlanCodeNm);
            }

            public override string ToString()
            {
                return log.ToString();
            }
        }

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.SAE.PlanCodeEditor.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        public class PlanCodeError
        {
            public string[] Messages   { get; set; }
            public string   PlanCodeNm { get; set; }
            public int      LineNum    { get; set; }
        }

        //This class's sole purpose in life is to translate client side json requests into a doc magic plan code
        public class PlanCodeDTO
        {
            public Guid   PlanCodeId        { get; set; }
            public string PlanCodeNm        { get; set; }
            public string PlanCodeDesc      { get; set; }
            public string InvestorNm        { get; set; }
            public string RuleXmlContent    { get; set; }

            internal bool HasError          { get; set; }

            public PlanCodeDTO()
            {
                PlanCodeId = Guid.Empty;
            }

            public PlanCodeDTO(DataRow row):this()
            {
                PlanCodeNm     = row["Plan Code"          ] as string;
                PlanCodeDesc   = row["Program Description"] as string;
                InvestorNm     = row["Investor"           ] as string;
                RuleXmlContent = row["Rules"              ] as string;
            }

            public PlanCodeDTO(DocMagicPlanCode c):this()
            {
                PlanCodeId     = c.PlanCodeId;
                PlanCodeNm     = c.PlanCodeNm;
                PlanCodeDesc   = c.PlanCodeDesc;
                InvestorNm     = c.InvestorNm;
                RuleXmlContent = c.RuleXmlContent;
            }

            /// <summary>
            /// Copies the contents of this PlanCode DTO into a real PlanCode object.
            ///
            /// Throws:
            /// ArgumentException if the ids don't match
            /// CBaseException if the rules won't validate
            /// </summary>
            /// <param name="other"></param>
            public void ApplyTo(DocMagicPlanCode other)
            {
                if (this.PlanCodeId != Guid.Empty && other.PlanCodeId != this.PlanCodeId)
                {
                    throw new ArgumentException("Ids don't match!");
                }

                other.InvestorNm     = this.InvestorNm;
                other.PlanCodeDesc   = this.PlanCodeDesc;
                other.PlanCodeNm     = this.PlanCodeNm;
                other.RuleXmlContent = this.RuleXmlContent;
            }

            public bool NeedsSave(DocMagicPlanCode other)
            {
                var equivalent = this.InvestorNm.Equals(other.InvestorNm)         &&
                                 this.PlanCodeDesc.Equals(other.PlanCodeDesc)     &&
                                 this.RuleXmlContent.Equals(other.RuleXmlContent) &&
                                 this.PlanCodeNm.Equals(other.PlanCodeNm);
                return !equivalent;
            }

            public string ValidateError()
            {
                string errorMessage;
                return DocMagicPlanCode.ValidateRuleContent(this.RuleXmlContent, out errorMessage) ? null : errorMessage;
            }
        }
    }
}
