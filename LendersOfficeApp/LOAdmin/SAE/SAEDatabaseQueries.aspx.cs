namespace LendersOfficeApp.LOAdmin.SAE
{
    using System;
    using System.Web;
    using System.Web.UI;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.LOAdmin.SAE;
    using LendersOffice.Security;
    
    /// <summary>
    /// Provides a means for SAE users to execute database queries.
    /// </summary>
    public partial class SAEDatabaseQueries : SecuredAdminPage
    {
        /// <summary>
        /// Represents the extension for the export file.
        /// </summary>
        private const string ExportFileExtension = ".csv";

        /// <summary>
        /// Represents the content type header for the export file sent to the user
        /// on export.
        /// </summary>
        private const string ExportFileContentType = "application/csv";

        /// <summary>
        /// Return this page's required permissions set.
        /// </summary>
        /// <returns>
        /// The required permissions for the page, <see cref="E_InternalUserPermissions.IsSAE"/>.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[] { E_InternalUserPermissions.IsSAE };
        }

        /// <summary>
        /// Load this page.
        /// </summary>
        /// <param name="sender">
        /// The activator of the page load.
        /// </param>
        /// <param name="e">
        /// The event arguments for the page load.
        /// </param>
        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                this.PopulateQueriesDDL();
            }

            var queryInfo = SAEDatabaseQueryExecutor.ExecuteQuery(
                this.m_queriesDDL.SelectedItem.ToString(), 
                this.m_dg);

            this.m_query.Text = queryInfo.Description;

            this.m_gridDiv.Visible = queryInfo.ShowDataGridWithExportResultsButton;

            this.m_exportResults.Visible = queryInfo.ShowDataGridWithExportResultsButton;

            this.m_exportPacUnion.Visible = queryInfo.ShowExportPacUnionButton;
        }

        /// <summary>
        /// Initialize this page.
        /// </summary>
        /// <param name="e">
        /// The event arguments for the page initialization.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            this.InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Export the results from the data grid.
        /// </summary>
        /// <param name="sender">
        /// The activator of the button click event.
        /// </param>
        /// <param name="e">
        /// The event arguments for the button click.
        /// </param>
        protected void ExportResults(object sender, EventArgs e)
        {
            var tempFileName = SAEDatabaseQueryExecutor.ExportDataGrid(
                this.m_queriesDDL.SelectedItem.ToString(),
                this.m_dg);

            this.SendExportFileToClient(tempFileName);
        }

        /// <summary>
        /// Export the results from the <code>PacUnion</code>
        /// query.
        /// </summary>
        /// <param name="sender">
        /// The activator of the button click event.
        /// </param>
        /// <param name="e">
        /// The event arguments for the button click.
        /// </param>
        protected void ExportPacificUnionUsers(object sender, EventArgs e)
        {
            var tempFileName = SAEDatabaseQueryExecutor.ExportPacUnionUsers();

            this.SendExportFileToClient(tempFileName);
        }

        /// <summary>
        /// Lists the queries that can be performed.
        /// </summary>
        private void PopulateQueriesDDL()
        {
            foreach (var query in SAEDatabaseQueryExecutor.GetQueryNamesList())
            {
                this.m_queriesDDL.Items.Add(query);
            }
        }

        /// <summary>
        /// Sends the export file with the specified <paramref name="tempFilePath"/>
        /// to the user and then deletes the temporary file.
        /// </summary>
        /// <param name="tempFilePath">
        /// The path to the temporary export file.
        /// </param>
        private void SendExportFileToClient(string tempFileName)
        {
            RequestHelper.SendFileToClient(
                HttpContext.Current,
                tempFileName,
                SAEDatabaseQueries.ExportFileContentType,
                tempFileName + "_" + DateTime.Today.ToShortDateString() + SAEDatabaseQueries.ExportFileExtension);

            HttpContext.Current.Response.SuppressContent = true;

            HttpContext.Current.ApplicationInstance.CompleteRequest();

            FileOperationHelper.Delete(tempFileName);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
    }
}
