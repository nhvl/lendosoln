using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.ObjLib.ExcelHelpers;
using LendersOffice.Security;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;

namespace LendersOfficeApp.LOAdmin.SAE
{
    /// <summary>
    /// Summary description for RateSheetVersion.
    /// </summary>
    public partial class RateSheetVersion : LendersOffice.Admin.SecuredAdminPage
	{
    
		protected string CurrentRsFile
		{
			get { return ViewState["CurrentRsFile"] as string; }
			set { ViewState["CurrentRsFile"] = value; }
		}

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsSAE
			};
		}
        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_11_2;
        }

        protected void PageLoad(object sender, System.EventArgs e)
		{
            this.EnableJquery = true;
            RegisterJsScript("LQBPopup.js");

			if( ! IsPostBack ) 
				BindData();
		}

		private void BindData() 
		{
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_File_GetAll"))
			{
				m_Grid.DataSource = reader ;
				m_Grid.DataBind() ;
			}

			if( CurrentRsFile != null && CurrentRsFile != "")
			{
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_File_Version_GetAll", new SqlParameter("@LpeAcceptableRsFileId", CurrentRsFile)))
				{
					m_RsFileVersions.DataSource = reader ;
					m_RsFileVersions.DataBind() ;
				}
			}
		}

		protected void RsFileVersions_OnItemCommand( object sender, DataGridCommandEventArgs e) 
		{
			switch ( e.CommandName ) 
			{
				case "DropRsFileVersion" :  
					try 
					{
						AcceptableRsFileVersion.RemoveFromDB( e.Item.Cells[0].Text, long.Parse(e.Item.Cells[1].Text) );
					}
					catch (Exception exception) 
					{
						m_ErrorMessage.Text = "Error:" + exception.Message; 
						return;
					}
					break;

			}
			BindData();

		}


		protected void RsFileVersions_ItemDataBound(object sender, DataGridItemEventArgs e) 
		{
			const int DropCol = 8;
			if ( e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer ) 
			{
				LinkButton butn = e.Item.Cells[DropCol].Controls[0] as LinkButton; 
				butn.Attributes["onclick"] = "return confirm(" + AspxTools.JsString("Are you sure you want to delete Acceptable Ratesheet File Version : " + e.Item.Cells[0].Text + ", version : " + e.Item.Cells[1].Text + "?") + ");";
			}
		}

        protected void Import_OnClick(object sender, EventArgs args)
        {
            if (!ImportFile.HasFile)
            {
                return;
            }
            string path = TempFileUtils.NewTempFilePath();
            ImportFile.SaveAs(path);
            DataTable table = ClosedXMLHelper.GenerateDataTable(path, 1);


            HashSet<string> allowedDeploymentTypes = new HashSet<string>()
            {
                "AlwaysBlocked",
                "UseVersion",
                "UseVersionAfterGeneratingAnOutputFileOk"
            };

            Dictionary<string,long> investors = GetInvestors();

            List<AcceptableRsFile> rsFilesToSave = new List<AcceptableRsFile>();
            bool fail = false;

            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];

                string filename = (string)row["Output Filename"];
                string deploymentType = (string)row["Deployment type"];
                string botAndMap = (string)row["Both Map & Bot Are OK"];
                string investor = (string)row["Investor Xls Id"];


                if (string.IsNullOrEmpty(filename))
                {
                    ImportError.Text = "Output Filename at line " + i + " is missing or null.";
                    fail = true;
                    break;
                }

                if (string.IsNullOrEmpty(deploymentType) || !allowedDeploymentTypes.Contains(deploymentType))
                {
                    ImportError.Text = "deploymentType " + i + " is missing or null or not accepted value";
                    fail = true;
                    break;
                }

                if (botAndMap == null  )
                {
                    ImportError.Text = "BotAndMap missing " + i;
                    fail = true;
                    break;
                }

                if (string.IsNullOrEmpty(investor) || !investors.ContainsKey(investor))
                {
                    ImportError.Text = "Investor XLS ID is missing " + i;
                    fail = true;
                    break;
                }

                AcceptableRsFile rs = new AcceptableRsFile();
                rs.DeploymentType = deploymentType;
                rs.UseForBothMapAndBot = botAndMap.Equals("X", StringComparison.OrdinalIgnoreCase);
                rs.RatesheetFileId = filename;
                rs.InvestorXlsFileId = investors[investor];
                rsFilesToSave.Add(rs);
            }

            if (fail)
            {
                return;
            }

            foreach (var rs in rsFilesToSave)
            {
                rs.Save();
            }

            Response.Redirect(Request.Url.ToString()); 
        }
        public Dictionary<string, long> GetInvestors()
        {
            Dictionary<string, long> investors = new Dictionary<string, long>();
            string sSQL = "SELECT InvestorXlsFileId, InvestorXlsFileName from  INVESTOR_XLS_FILE ORDER BY InvestorXlsFileName";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    long id = (long)reader["InvestorXlsFileId"];
                    investors.Add((string)reader["InvestorXlsFileName"] + " [" + id + "]", id);
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, sSQL, null, null, readHandler);

            return investors;
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void m_Grid_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			switch( e.CommandName )
			{
				case "SetCurrentRsFile" :
					CurrentRsFile = e.Item.Cells[0].Text;
					break;

				case "DropRsFile" :
					try 
					{
						AcceptableRsFile.RemoveFromDB( e.Item.Cells[0].Text );
					}
					catch (Exception exception) 
					{
						m_ErrorMessage.Text = "Error:" + exception.Message; 
						return;
					}
					break;
			}
			BindData();            
		}

        private Tuple<string, bool> GetLatestEffectiveDatTimeAndExpire(string fileID)
        {
            string latestEffectiveDateTime = string.Empty;
            bool isExpired = false;

            if (!string.IsNullOrEmpty(fileID))
            {
                SqlParameter[] parameters =
                {
                        new SqlParameter("@LpeAcceptableRsFileId", fileID)
                    };

                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_FileVersion_GetLatestEffectiveDate", new SqlParameter("@LpeAcceptableRsFileId", fileID)))
                {
                    if (reader.Read())
                    {
                        latestEffectiveDateTime = reader["LatestEffectiveDateTime"].ToString();
                        isExpired = (bool)reader["IsExpirationIssuedByInvestor"];
                    }
                }
            }

            return new Tuple<string, bool>(latestEffectiveDateTime, isExpired);
        }

        private string GetLatestEffectiveDateTime(string fileID)
		{
			string latestEffectiveDateTime = "";
			if( fileID != null && fileID != "")
			{
				try
				{
					using(DbDataReader reader = StoredProcedureHelper.ExecuteReader( DataSrc.RateSheet, "RS_FileVersion_GetLatestEffectiveDate", new SqlParameter("@LpeAcceptableRsFileId", fileID)))
					{
						if (reader.Read())
						{
							latestEffectiveDateTime = reader["LatestEffectiveDateTime"].ToString();
						}
					}
				}
				catch{}
			}

			return latestEffectiveDateTime;
		}

		protected void m_Grid_ItemDataBound(object sender, DataGridItemEventArgs e) 
		{
			const int DropCol = 7;
			const int LabelCol = 4;
            const int ExpireCol = 5;
			if ( e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer ) 
			{
				string rsId = e.Item.Cells[0].Text;
				LinkButton butn = e.Item.Cells[DropCol].Controls[0] as LinkButton; 
				butn.Attributes["onclick"] = "return confirm(" + AspxTools.JsString("Are you sure you want to delete Acceptable Ratesheet File : " + rsId + "?")+ ");";

                var item = GetLatestEffectiveDatTimeAndExpire(rsId);
				e.Item.Cells[LabelCol].Text = item.Item1;
                e.Item.Cells[ExpireCol].Text = item.Item2 ? "Expired" : string.Empty;
			}
		}

		Hashtable m_xlsFileNames = null;
		protected string GetXlsFileName(  object dataItem  )
		{
			if( m_xlsFileNames == null )
				m_xlsFileNames = InvestorXlsFile.GetAllIdAndFileName();
            
			long rsFileId = (long)DataBinder.Eval(dataItem, "InvestorXlsFileId");
			string xlsFileName = (string)m_xlsFileNames[rsFileId];

			return xlsFileName == null ? "" : xlsFileName;

		}


		protected void m_btnRefresh_Click(object sender, System.EventArgs e)
		{
			BindData();        
        
		}


	}

}
