using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.ExcelHelpers;
using LendersOffice.Security;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;
using System.Data.Common;

namespace LendersOfficeApp.LOAdmin.SAE
{
    public partial class RateSheetVersion2 : SecuredAdminPage
	{
        [WebMethod]
        public static IEnumerable<RsFile> GetFiles()
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            return RsFile.GetAll();
        }
        [WebMethod]
        public static string CreateFile(RsFile file)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            return file.Create();
        }
        [WebMethod]
        public static string UpdateFile(RsFile file)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            return file.Update();
        }
        [WebMethod]
        public static string BatchEditFile(string[] ids, RsFile file)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            foreach (var id in ids)
            {
                var rsFile = new AcceptableRsFile(id);
                if (!string.IsNullOrEmpty(file.DeploymentType))
                {
                    rsFile.DeploymentType = file.DeploymentType;
                }
                if (file.IsBothRsMapAndBotWorking.HasValue)
                {
                    rsFile.UseForBothMapAndBot = file.IsBothRsMapAndBotWorking.Value;
                }
                if (file.InvestorXlsFileId.HasValue)
                {
                    rsFile.InvestorXlsFileId = file.InvestorXlsFileId.Value;
                }
                

                try { rsFile.Save(); }
                catch (SqlException e) { return e.Message; }

                if (file.IsExpirationIssuedByInvestor.HasValue)
                {
                    var error = RsFile.UpdateFileVersionExpired(id, file.IsExpirationIssuedByInvestor.Value);
                    if (error != null) return error;
                }
            }
            return null;
        }
        
        [WebMethod]
        public static string DeleteFiles(string[] ids)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            return string.Join(",", ids.Select(RsFile.Delete).Where(error => error != null));
        }
        [WebMethod]
        public static IEnumerable<FileVersion> GetFileVersions(string fileId)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            return FileVersion.Get(fileId).ToArray();
        }
        [WebMethod]
        public static string SaveFileVersion(FileVersion fileVersion)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            if (!fileVersion.IsValid()) return "Invalid Data";

            try { fileVersion.Save(); }
            catch (SqlException e) { return e.Message; }

            return null;
        }
        [WebMethod]
        public static string DeleteFileVersion(string fileId, long version)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            try { FileVersion.Delete(fileId, version); }
            catch (SqlException e) { return e.Message; }

            return null;
        }

        [WebMethod]
        public static object GetConfig()
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            return new
            {
                InvestorXslFiles= InvestorXlsFileList.InvestorXls.Get().ToArray(),
                DeploymentTypes,
                DefaultDeploymentType = DeploymentTypes[1],
            };
        }

        protected void Import_OnClick(object sender, EventArgs args)
        {
            if (!ImportFile.HasFile) { return; }

            var path = TempFileUtils.NewTempFilePath();
            ImportFile.SaveAs(path);

            var investors = GetInvestors();

            string importError;

            try
            {
                var rsFilesToSave = LoadRsFilesFromCsv(path, out importError, investors);

                if (!string.IsNullOrEmpty(importError))
                {
                    RegisterJsObjectWithJavascriptSerializer("importError", importError);
                    return;
                }

                foreach (var rs in rsFilesToSave) { rs.Save(); }
                Response.Redirect(Request.Url.ToString());
            }
            catch (SqlException e) { importError = e.Message; }

            if (!string.IsNullOrEmpty(importError))
            {
                RegisterJsObjectWithJavascriptSerializer("importError", importError);
            }
        }

        static IEnumerable<AcceptableRsFile> LoadRsFilesFromCsv(string csvFile, out string importError, IDictionary<string, long> investors)
        {
            var table = ClosedXMLHelper.GenerateDataTable(csvFile, 1);
            var allowDeploymentTypes = new HashSet<string>(DeploymentTypes);

            var rsFilesToSave = new List<AcceptableRsFile>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                var row = table.Rows[i];

                string filename       = (string)row["Output Filename"];
                string deploymentType = (string)row["Deployment type"];
                string botAndMap      = (string)row["Both Map & Bot Are OK"];
                string investor       = (string)row["Investor Xls Id"];

                if (string.IsNullOrEmpty(filename))
                {
                    importError = $"Output Filename at line {i} is missing or null.";
                    return null;
                }

                if (string.IsNullOrEmpty(deploymentType) || !allowDeploymentTypes.Contains(deploymentType))
                {
                    importError = $"deploymentType {i} is missing or null or not accepted value";
                    return null;
                }

                if (botAndMap == null)
                {
                    importError = "BotAndMap missing " + i;
                    return null;
                }

                if (string.IsNullOrEmpty(investor) || !investors.ContainsKey(investor))
                {
                    importError = "Investor XLS ID is missing " + i;
                    return null;
                }

                var rs = new AcceptableRsFile
                {
                    DeploymentType      = deploymentType,
                    UseForBothMapAndBot = botAndMap.Equals("X", StringComparison.OrdinalIgnoreCase),
                    RatesheetFileId     = filename,
                    InvestorXlsFileId   = investors[investor]
                };
                rsFilesToSave.Add(rs);
            }

            importError = null;
            return rsFilesToSave;
        }

        public class RsFile
        {
            // dbo.LPE_ACCEPTABLE_RS_FILE

            public string    LpeAcceptableRsFileId        { get; set; }
            public string    DeploymentType               { get; set; }
            public bool    ? IsBothRsMapAndBotWorking     { get; set; }
            public long    ? InvestorXlsFileId            { get; set; }
            public bool    ? IsExpirationIssuedByInvestor { get; set; }

            public DateTime? LatestEffectiveDateTime      { get; set; }
            public string    XlsFileName                  { get; set; }

            public RsFile()
            {
                // for Serialization
            }

            public RsFile(DbDataReader reader)
            {
                this.LpeAcceptableRsFileId    = reader["LpeAcceptableRsFileId"] as string;
                this.DeploymentType           = reader["DeploymentType"] as string;
                this.IsBothRsMapAndBotWorking = (bool) reader["IsBothRsMapAndBotWorking"];
                this.InvestorXlsFileId        = (long) reader["InvestorXlsFileId"];
                try
                {
                    this.LatestEffectiveDateTime = reader["LatestEffectiveDateTime"] == DBNull.Value ? (DateTime?) null: (DateTime) reader["LatestEffectiveDateTime"];
                }
                catch (IndexOutOfRangeException)
                {
                    this.LatestEffectiveDateTime = null;
                }

                try
                {
                    this.IsExpirationIssuedByInvestor = reader["IsExpirationIssuedByInvestor"] == DBNull.Value ? (bool?)null : (bool)reader["IsExpirationIssuedByInvestor"];
                }
                catch (IndexOutOfRangeException)
                {
                    this.IsExpirationIssuedByInvestor = null;
                }

                if (this.LpeAcceptableRsFileId == null) throw new ApplicationException("LpeAcceptableRsFileId is null");
            }

            public bool IsValid()
            {
                this.LpeAcceptableRsFileId = this.LpeAcceptableRsFileId.TrimWhitespaceAndBOM();
                return !string.IsNullOrEmpty(this.LpeAcceptableRsFileId) &&
                       !string.IsNullOrEmpty(this.DeploymentType) &&
                       this.IsBothRsMapAndBotWorking.HasValue &&
                       (this.InvestorXlsFileId.HasValue && this.InvestorXlsFileId >= 0);
            }

            public string Create() { return this.Save(true); }

            public string Update() { return this.Save(false); }

            string Save(bool IsNewRecord)
            {
                if (!this.IsValid()) { return "Data Invalid"; }

                var rsFile = IsNewRecord ? new AcceptableRsFile() : new AcceptableRsFile(this.LpeAcceptableRsFileId);

                if (IsNewRecord)
                {
                    rsFile.RatesheetFileId = this.LpeAcceptableRsFileId;
                }
                rsFile.DeploymentType      = this.DeploymentType;
                rsFile.UseForBothMapAndBot = this.IsBothRsMapAndBotWorking.Value;
                rsFile.InvestorXlsFileId   = this.InvestorXlsFileId.Value;

                try { rsFile.Save(); }
                catch (SqlException e) { return "Unable to save the Ratesheet File. " + e.Message; }
                catch (CBaseException e) {  return e.Message; }

                if (!string.IsNullOrEmpty(this.LpeAcceptableRsFileId) && this.IsExpirationIssuedByInvestor.HasValue)
                {
                    var error = UpdateFileVersionExpired(this.LpeAcceptableRsFileId, this.IsExpirationIssuedByInvestor.Value);
                    if (!string.IsNullOrEmpty(error)) return error;
                }

                return null;
            }

            public static IEnumerable<RsFile> GetAll()
            {
                var d = InvestorXlsFile.GetAllIdAndFileName2();

                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_File_GetAll2"))
                {
                    while (reader.Read())
                    {
                        var f = new RsFile(reader);
                        //if (!f.LatestEffectiveDateTime.HasValue || !f.IsExpirationIssuedByInvestor.HasValue)
                        //{
                        //    var p = GetLatestEffectiveDatTimeAndExpire(f.LpeAcceptableRsFileId);
                        //    if (p != null)
                        //    {
                        //        f.LatestEffectiveDateTime = p.Item1;
                        //        f.IsExpirationIssuedByInvestor = p.Item2;
                        //    }
                        //}

                        string xlsFileName;
                        if (d.TryGetValue(f.InvestorXlsFileId.Value, out xlsFileName)) { f.XlsFileName = xlsFileName; }

                        yield return f;
                    }
                }
            }

            public static string UpdateFileVersionExpired(string LpeAcceptableRsFileId, bool IsExpirationIssuedByInvestor)
            {
                try
                {
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet,
                        "RS_File_Version_SetExpirationByFileId", 2,
                        new SqlParameter("@IsExpirationIssuedByInvestor", IsExpirationIssuedByInvestor),
                        new SqlParameter("@LpeAcceptableRsFileId", LpeAcceptableRsFileId));
                }
                catch (SqlException e) { return "Unable to update `IsExpirationIssuedByInvestor` of Ratesheet File.\n" + e.Message; }

                return null;
            }

            static Tuple<DateTime, bool> GetLatestEffectiveDatTimeAndExpire(string fileID)
            {
                try
                {
                    if (!string.IsNullOrEmpty(fileID))
                    {
                        using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet,
                            "RS_FileVersion_GetLatestEffectiveDate",
                            new SqlParameter("@LpeAcceptableRsFileId", fileID)))
                        {
                            if (reader.Read())
                            {
                                return new Tuple<DateTime, bool>((DateTime) reader["LatestEffectiveDateTime"],
                                    (bool) reader["IsExpirationIssuedByInvestor"]);
                            }
                        }
                    }
                }
                catch (SqlException)
                {

                }

                return null;
            }

            public static string Delete(string fileId)
            {
                try { AcceptableRsFile.RemoveFromDB(fileId); }
                catch (SqlException e) { return e.Message; }

                return null;
            }
        }

        public class FileVersion
        {
            // dbo.LPE_ACCEPTABLE_RS_FILE_VERSION

            public string    LpeAcceptableRsFileId        { get; set; }
            public long    ? VersionNumber                { get; set; }
            public DateTime? FirstEffectiveDateTime       { get; set; }
            public DateTime? LatestEffectiveDateTime      { get; set; }
            public bool      IsExpirationIssuedByInvestor { get; set; }
            public DateTime? EntryCreatedD                { get; set; }
            public DateTime? ModifiedD                    { get; set; }

            public FileVersion()
            {
                // for Serialzation
            }
            public FileVersion(IDataRecord reader)
            {
                this.LpeAcceptableRsFileId        =            reader["LpeAcceptableRsFileId"       ] as string;
                this.VersionNumber                = (long    ) reader["VersionNumber"               ];
                this.FirstEffectiveDateTime       = (DateTime) reader["FirstEffectiveDateTime"      ];
                this.LatestEffectiveDateTime      = (DateTime) reader["LatestEffectiveDateTime"     ];
                this.IsExpirationIssuedByInvestor = (bool    ) reader["IsExpirationIssuedByInvestor"];
                this.EntryCreatedD                = (DateTime) reader["EntryCreatedD"               ];
                this.ModifiedD                    = (DateTime) reader["ModifiedD"                   ];
            }

            public bool IsValid()
            {
                this.LpeAcceptableRsFileId = this.LpeAcceptableRsFileId.TrimWhitespaceAndBOM();
                return !string.IsNullOrEmpty(this.LpeAcceptableRsFileId) &&
                    this.FirstEffectiveDateTime.HasValue &&
                    this.LatestEffectiveDateTime.HasValue;
            }

            public void Save()
            {
                bool isNewRecord = !(this.VersionNumber.HasValue);

                Debug.Assert(this.IsValid());

                var rsFileVersion = isNewRecord ?
                    new AcceptableRsFileVersion { RatesheetFileId = this.LpeAcceptableRsFileId } :
                    new AcceptableRsFileVersion(this.LpeAcceptableRsFileId, this.VersionNumber.Value);

                rsFileVersion.FirstEffective               = this.FirstEffectiveDateTime.Value;
                rsFileVersion.LastestEffective             = this.LatestEffectiveDateTime.Value;
                rsFileVersion.IsExpirationIssuedByInvestor = this.IsExpirationIssuedByInvestor;

                rsFileVersion.Save();
            }

            public static IEnumerable<FileVersion> Get(string fileId)
            {
                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_File_Version_GetAll", new SqlParameter("@LpeAcceptableRsFileId", fileId)))
                {
                    while (reader.Read())
                    {
                        yield return new FileVersion(reader);
                    }
                }
            }

            public static void Delete(string rateId, long version)
            {
                AcceptableRsFileVersion.RemoveFromDB(rateId, version);
            }
        }

        static Dictionary<string, long> GetInvestors()
        {
            return InvestorXlsFileList.InvestorXls.Get().ToDictionary(
                f => $"{f.InvestorXlsFileName} [{f.InvestorXlsFileId.Value}]",
                f => f.InvestorXlsFileId.Value);
        }

        static readonly string[] DeploymentTypes =
        {
            "AlwaysBlocked",
            "UseVersion",
            "UseVersionAfterGeneratingAnOutputFileOk"
        };

        static readonly E_InternalUserPermissions[] RequiredPermissions = { E_InternalUserPermissions.IsSAE };

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.
            return RequiredPermissions;
        }

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.SAE.RateSheetVersion.js");
        }
        public override string StyleSheet => string.Empty;

	    protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            RegisterResources();
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
