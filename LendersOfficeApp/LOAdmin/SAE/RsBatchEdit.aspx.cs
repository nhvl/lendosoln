using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;

namespace LendersOfficeApp.LOAdmin.SAE
{
    /// <summary>
    /// Summary description for RsBatchEdit.
    /// </summary>
    public partial class RsBatchEdit : LendersOffice.Admin.SecuredAdminPage
	{

		protected System.Web.UI.WebControls.TextBox m_txtRsFileID;
		protected System.Web.UI.WebControls.RequiredFieldValidator m_rfvRsFileID;
		protected System.Web.UI.WebControls.Button m_OK;

		private ArrayList m_ratesheetIds;


		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsSAE
			};
		}


		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			lblPageTitle.Text = "Batch Ratesheet Edit";
			
			//Load the Ids from post, and if the page is a postback, get the Id's from the ViewState
			string sIds = Request["RsIds"];
			if( Page.IsPostBack )
				sIds = (String)ViewState["sIds"];

			//Parse the Ids from the Sids string into the m_ratesheetIds arraylist
			m_ratesheetIds = new ArrayList();
			string sTemp = "";
			StringReader sr = new StringReader(sIds);
			int nextChar = sr.Read();
			while(nextChar != -1)
			{	
				if((char)nextChar == ','){
					m_ratesheetIds.Add(sTemp.TrimWhitespaceAndBOM());
					sTemp = "";
				}
				else{
					sTemp += (char)nextChar;
				}
				nextChar = sr.Read();
			}
			

			if( !Page.IsPostBack ) 
			{
				ViewState["sIds"] = sIds;
				m_dgPrograms.DataSource = m_ratesheetIds;
				m_dgPrograms.DataBind();

				AcceptableRsFile rsFile    = new AcceptableRsFile( (String)m_ratesheetIds[0] ) ;
			
				InitDropDownList4XslFiles( rsFile.InvestorXlsFileId );
			}
		}

		protected void OKClick(object sender, System.EventArgs e)
		{
			if( !Page.IsValid )
				return;
			String ratesheetId;
			m_dgPrograms.DataBind();
			
				try
				{
					//Iterate through each ID, loading each AcceptableRsFile and updating only the boxes that
					//are checked
					for(int i=0;i<m_ratesheetIds.Count; i++)
					{
						ratesheetId = (String)m_ratesheetIds[i];
						AcceptableRsFile rsFile    = new AcceptableRsFile( ratesheetId ) ;

						if(m_updateDeployment.Checked)
							rsFile.DeploymentType = m_ddlDeployType.SelectedItem.Value;
						else
							rsFile.DeploymentType = rsFile.DeploymentType;
						
						if(m_updateMapAndBot.Checked)
							rsFile.UseForBothMapAndBot = m_cbUsingForMapAndBot.Checked;
						else
							rsFile.UseForBothMapAndBot = rsFile.UseForBothMapAndBot;

						if(m_updateInvestorList.Checked)
							rsFile.InvestorXlsFileId   = long.Parse( m_ddlXlsFiles.SelectedItem.Value );
						else
							rsFile.InvestorXlsFileId = rsFile.InvestorXlsFileId;
						

						rsFile.Save();

                    if (m_updateLatestVersionExpired.Checked)
                    {
                        SqlParameter[] parameters =
                        {
                            new SqlParameter("@IsExpirationIssuedByInvestor", m_latestVersionExpired.Checked),
                            new SqlParameter("@LpeAcceptableRsFileId", ratesheetId)
                        };
                        StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_File_Version_SetExpirationByFileId", 2, parameters);
                    }
					}
					


					LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this) ;
					
				}
				catch( Exception exc )
				{
					m_ErrorMessage.Text = "Unable to save the Ratesheet File. " + exc.Message;

				}

			
		}

		void InitDropDownList4XslFiles( long xslId )
		{
			string sSQL = "SELECT InvestorXlsFileId, InvestorXlsFileName from  INVESTOR_XLS_FILE ORDER BY InvestorXlsFileName" ;

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                ListItem item;
                while (reader.Read())
                {
                    long id = (long)reader["InvestorXlsFileId"];
                    string txt = (string)reader["InvestorXlsFileName"] + " [" + id.ToString() + "]";
                    m_ddlXlsFiles.Items.Add(item = new ListItem(txt, id.ToString()));
                    if (id == xslId)
                        item.Selected = true;

                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.RateSheet, sSQL, null, null, readHandler);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
