﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendersOffice.Common;
using LendersOffice.Integration.GenericFramework;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class GenericFrameworkBrokerConfig : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        /// <summary>
        /// Saves a broker-specific copy of the vendor to the database.
        /// </summary>
        /// <param name="brokerID">The broker ID the lender using the new or exitisting vendor instance.</param>
        /// <param name="providerID">The provider ID of the vendor.</param>
        /// <param name="credentialXML">The credential XML of the broker, or an unused value if <paramref name="isEditCredentialXML"/> is false.</param>
        /// <param name="enabled">Indicates whether the vendor is enabled for the broker.</param>
        /// <returns>The provider ID of the vendor that was saved.</returns>
        [System.Web.Services.WebMethod]
        public static object SaveVendor(Guid brokerID, string providerID, bool includeUsername, string credentialXML, string launchLinkConfigXML, bool enabled)
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            string errorMessage = null;
            var vendor = GenericFrameworkVendor.LoadVendor(brokerID, providerID);

            if (vendor == null)
            {
                errorMessage = "The specified provider ID (" + providerID + ") does not correspond to a valid generic framework vendor.";
            }
            else if (enabled)
            {
                try
                {
                    if (!vendor.IsBrokerInstance)
                    {
                        vendor.MakeBrokerInstance(brokerID);
                    }

                    vendor.CredentialXML = vendor.ValidateXml(credentialXML, XmlType.Credential);
                    vendor.LaunchLinkConfigXML = vendor.ValidateXml(launchLinkConfigXML, XmlType.LaunchLink);
                    vendor.Save();
                }
                catch (System.Xml.XmlException exc)
                {
                    errorMessage = exc.Data.Contains("ErrorMessage") ? exc.Data["ErrorMessage"].ToString() : exc.Message;
                }
                catch (LaunchLinkConfiguration.LaunchLinkConfigurationException exc)
                {
                    errorMessage = "Invalid Configuration: " + exc.DeveloperMessage; // Expose developer message only because this is within LOAdmin.
                }
            }
            else if (vendor.IsBrokerInstance)
            {
                vendor.Delete();
            }

            return new { ProviderID = providerID, Error = errorMessage };
        }

        private List<GenericFrameworkVendor> allVendors;

        public Guid BrokerID
        {
            get { return RequestHelper.GetGuid("BrokerID"); }
        }

        /// <summary>
        /// Gets a list of all Generic Framework vendors with broker-specific instances when they exist.
        /// </summary>
        /// <value>A list of all Generic Framework vendors with broker-specific instances when they exist.</value>
        public List<GenericFrameworkVendor> AllVendors
        {
            get
            {
                if (this.allVendors == null)
                {
                    this.allVendors = GenericFrameworkVendor.LoadAllVendors();
                    var brokerVendors = GenericFrameworkVendor.LoadVendors(this.BrokerID);
                    foreach (var brokerVendor in brokerVendors)
                    {
                        int index = this.allVendors.FindIndex(v => v.ProviderID == brokerVendor.ProviderID);
                        this.allVendors[index] = brokerVendor; // if the index is out of range, we want the exception since LoadAllVendors() missed it.
                    }
                }

                return this.allVendors;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.VendorTable.DataSource = this.AllVendors.Select(vendor =>
                new
                {
                    Enabled = vendor.IsBrokerInstance,
                    vendor.ProviderID,
                    vendor.Name,
                    vendor.IncludeUsername,
                    vendor.CredentialXML,
                    LaunchLinkConfigXML = vendor.IsBrokerInstance ? vendor.LaunchLinkConfigXML : LaunchLinkConfiguration.NewXML(vendor.Name)
                });
            this.VendorTable.DataBind();
        }

        /// <summary>
        /// Generates a new &lt;textarea /&gt;.
        /// </summary>
        /// <param name="eval">Maps <paramref name="name"/> to a string value for the TextArea's value.</param>
        /// <param name="name">The class to which the TextArea belongs.</param>
        /// <returns>A new TextArea with class set to <paramref name="name"/> and value given by <paramref name="eval"/>(<paramref name="name"/>).</returns>
        protected static System.Web.UI.HtmlControls.HtmlTextArea GenerateTextArea(Func<string, object> eval, string name)
        {
            var textArea = new System.Web.UI.HtmlControls.HtmlTextArea();
            textArea.Attributes.Add("class", name);
            textArea.Value = (eval(name) ?? string.Empty).ToString();
            return textArea;
        }

        /// <summary>
        /// Generates a new &lt;input type="checkbox" /&gt;.
        /// </summary>
        /// <param name="eval">Maps <paramref name="name"/> to a boolean value for the TextArea's value.</param>
        /// <param name="name">The class to which the input belongs.</param>
        /// <param name="disable">Indicates whether to permanently disable the control.</param>
        /// <returns>A new input with class set to <paramref name="name"/> and checked if the boolean given by <paramref name="eval"/>(<paramref name="name"/>) is true.</returns>
        protected static System.Web.UI.HtmlControls.HtmlInputCheckBox GenerateCheckboxInput(Func<string, object> eval, string name, bool disable)
        {
            var checkboxInput = new System.Web.UI.HtmlControls.HtmlInputCheckBox();
            checkboxInput.Attributes.Add("class", name);
            checkboxInput.Checked = (bool)eval(name);
            checkboxInput.Disabled = disable;
            return checkboxInput;
        }
    }
}
