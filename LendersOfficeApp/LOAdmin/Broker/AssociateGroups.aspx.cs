﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Window that shows the groups associated with a client certificate.
    /// </summary>
    public partial class AssociateGroups : SecuredAdminPage
    {
        /// <summary>
        /// Gets a value indicating whether or not this page is opened in edit mode.
        /// </summary>
        /// <value>Whether this page is opened in edit mode or not.</value>
        protected bool IsEditMode
        {
            get
            {
                return RequestHelper.GetBool("IsEdit");
            }
        }

        /// <summary>
        /// Gets the broker id from the query parameters.
        /// </summary>
        /// <value>The broker id from the query paramters.</value>
        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("bId");
            }
        }

        /// <summary>
        /// Gets the group type to load from the query parameters.
        /// </summary>
        /// <value>The group type to load from the query parameters.</value>
        protected string Type
        {
            get
            {
                return RequestHelper.GetSafeQueryString("GroupType");
            }
        }

        /// <summary>
        /// Gets the certificate id of the certificate to load.
        /// </summary>
        /// <value>The certificate id of the certificate to load.</value>
        protected Guid CertId
        {
            get
            {
                return RequestHelper.GetGuid("certId", Guid.Empty);
            }
        }

        /// <summary>
        /// The Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            this.PageID = "AssociateGroups";
            this.PageTitle = "Associate Groups";
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("stylesheet.css");
        }

        /// <summary>
        /// Gets the required permissions to access this page.
        /// </summary>
        /// <returns>The permissions associated with this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker
            };
        }

        /// <summary>
        /// The Page_Load function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool isEditMode = this.IsEditMode;
            this.ApplyBtn.Visible = isEditMode;
            this.IsEditModeValue.Value = isEditMode.ToString();
            this.HeaderText.Text = isEditMode ? "Associate Groups" : "View Groups";

            IEnumerable<Group> groupList;
            Guid certId = this.CertId;
            Guid brokerId = this.BrokerId;
            string groupString = this.Type;
            GroupType groupType;

            if (groupString != null && certId == Guid.Empty)
            {
                // Loading up the groups directly.
                groupType = (GroupType)Enum.Parse(typeof(GroupType), groupString);
                groupList = GroupDB.GetAllGroups(brokerId, groupType);
            }
            else if (groupString == null && certId != Guid.Empty)
            {
                // Loading up the groups stored in the certificate.
                LenderClientCertificate cert = LenderClientCertificate.GetLenderClientCertificate(brokerId, certId);
                if (cert.Level == ClientCertificateLevelTypes.BranchGroup)
                {
                    groupType = GroupType.Branch;
                }
                else if (cert.Level == ClientCertificateLevelTypes.EmployeeGroup)
                {
                    groupType = GroupType.Employee;
                }
                else if (cert.Level == ClientCertificateLevelTypes.OcGroup)
                {
                    groupType = GroupType.PmlBroker;
                }
                else
                {
                    throw new UnhandledEnumException(cert.Level);
                }

                groupList = GroupDB.GetAllGroups(brokerId, groupType);
                this.SelectedGroupIds.Value = string.Join(",", cert.GroupIds);
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "GroupType and certId parameters defined. Not sure what to load.");
            }

            switch (groupType)
            {
                case GroupType.Branch:
                    this.NameHeaderLabel.Text = "Branch Groups";
                    break;
                case GroupType.Employee:
                    this.NameHeaderLabel.Text = "Employee Groups";
                    break;
                case GroupType.PmlBroker:
                    this.NameHeaderLabel.Text = "OC Groups";
                    break;
                default:
                    throw new UnhandledEnumException(groupType);
            }

            this.GroupsRepeater.DataSource = groupList;
            this.GroupsRepeater.DataBind();
        }

        /// <summary>
        /// This method is called once every time a row is bound for the Groups Repeater.  It sets the groupName and description values.
        /// </summary>
        /// <param name="sender">The object triggering the method.</param>
        /// <param name="e">The event arguments.</param>
        protected void GroupsRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var group = e.Item.DataItem as Group;

            EncodedLabel groupName = e.Item.FindControl("groupName") as EncodedLabel;
            EncodedLabel groupDesc = e.Item.FindControl("groupDesc") as EncodedLabel;

            groupName.Text = group.GroupName;
            groupDesc.Text = group.Description;
        }
    }
}