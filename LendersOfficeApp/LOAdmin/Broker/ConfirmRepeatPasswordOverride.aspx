<%@ Page language="c#" Codebehind="ConfirmRepeatPasswordOverride.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.ConfirmRepeatPasswordOverride" %>
<%@ Register TagPrefix="uc1" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>Confirm Repeat Password Override</title>
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </HEAD>
	<body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" onload="init();">
		<script language="javascript">
<!--
	function init() 
	{
		resize(580, 230);
	}

	function f_choice(choice) 
	{
		var args = window.dialogArguments || {};
		args.choice = choice;
		onClosePopup(args);
	}
//-->
		</script>
	<h4 class="page-header">Confirm Repeat Password Override</h4>
    <form id="ConfirmRepeatPasswordOverride" method="post" runat="server">
		<table width="100%">
			<tr>
				<td style="FONT-WEIGHT:bold;COLOR:red">
					<P>Caution:</P>
				</td>
			</tr>
			<tr>
				<td style="FONT-WEIGHT:bold;COLOR:black">
					You are attempting to apply an old password that has previously been used for this account.&nbsp; 
					<br>For security reasons, we strongly recommend that you go back and specify a new password.&nbsp; 
					<br><br>What would you like to do?<BR><br><br>
				</td>
			</tr>
			<tr>
				<td align="center">
					<input type="button" value="Apply the Password" class="buttonstyle" onclick="f_choice(0);" style="WIDTH: 150px">
						&nbsp;<input type="button" value="Cancel" class="buttonstyle" onclick="f_choice(1);" style="WIDTH: 150px">
				</td>
			</tr>
		</table>
     </form>
	<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
  </body>
</HTML>
