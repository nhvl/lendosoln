﻿//-----------------------------------------------------------------------
// <copyright file="BrokerReplicate.aspx.cs" company="Meridianlink">
//     Copyright (c) Sprocket Enterprises. All rights reserved.
// </copyright>
// <author>Zijing Jia</author>
// <summary>Single class file for broker export import.</summary>
//-----------------------------------------------------------------------
namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.Conversions.LoanFileImportExport;
    using LendersOffice.Security;

    /// <summary>
    /// The replicate tool specially for broker.
    /// </summary>
    public partial class BrokerReplicate : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// The relative address of white list. Need further transformation to get the real address according to the setting of server.
        /// </summary>
        protected static readonly string SenderWhitelistMapAddr = @"~/senderWhiteList.csv.config";

        /// <summary>
        /// The relative address of the implicit identity keys file. Need further transformation to get the real address according to the setting of server.
        /// </summary>
        protected static readonly string ImplicitIdentityKeyFileAddress = @"~/implicitIdentityKeyRefs.csv.config";

        /// <summary>
        /// The type of export we are running on the page.
        /// </summary>
        private enum ExportType
        {
            /// <summary>
            /// For broker exports.
            /// </summary>
            Broker,

            /// <summary>
            /// For custom (user-specified table rows) export.
            /// </summary>
            Custom
        }
        
        /*/// <summary>
        /// The options of radio for user to select for the export tool.
        /// </summary>
        public enum RadioType
        {
            /// <summary>
            /// Export one branch by branch name entered by user.
            /// </summary>
            NoUser = 0,

            /// <summary>
            /// Export all branches.
            /// </summary>
            ExportUsersByBranchName = 1,

            /// <summary>
            /// Export by some entries entered by user.
            /// </summary>
            AllUsers = 2,

            /// <summary>
            /// Export loan templates by broker id.
            /// </summary>
            LoanTemplate = 3,

            /// <summary>
            /// Export System Tables.
            /// </summary>
            SystemTables = 4
        }*/

        /// <summary>
        /// Gets or sets the white list dictionary.
        /// Whitelist only has one file. The main tag is white list. The sub-tag is table name. 
        /// For each table, they have three tag: Review, Protect, Export.
        /// </summary>
        /// <value>The white list data dictionary.</value>
        protected static Dictionary<string, WhitelistData> SenderWhitelistDictionary { get; set; } // Dictionary[table] -> reviewList/protectList/protectVlList/exportList

        /// <summary>
        /// Gets or sets broker id that at current page.
        /// </summary>
        /// <value>Return broker id.</value>
        protected Guid BrokerId
        {
            get
            {
                if (this.ViewState["brokerid"] == null)
                {
                    this.ViewState["brokerid"] = RequestHelper.GetGuid("brokerid", Guid.Empty);
                }

                return (Guid)this.ViewState["brokerid"];
            }

            set
            {
                this.ViewState["brokerid"] = value;
            }
        }

        /*/// <summary>
        /// Read white list from file and then save it into the dictionary. Also, send the white list to the remote server so that the remote server will share the same white list.
        /// </summary>
        /// <param name="errorList">List stores errors.</param>
        /// <param name="path">White list file address.</param>
        public static void ReadLoadWhitelistFile_BothLocalRemote(ref List<string> errorList, string path)
        {
            // for local server
            Dictionary<string, string> reconstructMap = null;
            Dictionary<string, WhitelistData> whitelistContainer;
            ShareImportExport.ReadCsvWhiteList(ref errorList, out reconstructMap, out whitelistContainer, path, false);
            BrokerReplicate.SenderWhitelistDictionary = whitelistContainer;
            if (errorList.Any())
            {
                return;
            }

            // for remote server
            byte[] whitelistByteData = TableReplicateTool.GetFileAsByte(path);
            DBTableImportService.DBTableImportService service = new DBTableImportService.DBTableImportService();
            service.Url = ConstSite.TableReplicateServiceUrl;
            service.GetAndLoadWhitelist(whitelistByteData);
        }*/

        /// <summary>
        /// Gets the required permission for the current LOAdmin page.
        /// </summary>
        /// <returns>Permission required to access this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker
            };
        }

        /// <summary>
        /// Function for loading the page. Load drop down list.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsObjectWithJavascriptSerializer("LoanFileNamesForExport", ShareImportExport.ExportTableNames.Where(s => !s.StartsWith("Application")));

            this.UpdateWhiteListHashText();
            this.UpdateImplicitKeysLatestText();

            if (ConstAppDavid.CurrentServerLocation != ServerLocation.Production)
            {
                XmlImportPlaceHolder.Visible = true;
            }

            if (this.BrokerId == Guid.Empty)
            {
                BrokerExportPlaceHolder.Visible = false;
                NonBrokerExportPlaceHolder.Visible = true;
            }

            /*if (this.ServerSelectToImportXmlId.Items.FindByText("Normal(OnlyOnes-MustBeExported)") == null)
            {
                //this.ServerSelectToImportXmlId.Items.Add(new ListItem("Normal(OnlyOnes-MustBeExported)", "1"));
            }

            if (this.ServerSelectToImportXmlId.Items.FindByText("ExportAllThing") == null)
            {
                this.ServerSelectToImportXmlId.Items.Add(new ListItem("ExportAllThing", "2"));
            }*/

            /*if (this.FullReplicateSelectStarterTablesId.Items.Count == 0)
            {
                this.FullReplicateSelectStarterTablesId.Items.Add(new ListItem("Broker and Branch and all Users", "2"));
                this.FullReplicateSelectStarterTablesId.Items.Add(new ListItem("Broker and branch", "3"));
            }*/
        }

        /// <summary>
        /// A method for exporting file to the user.
        /// </summary>
        /// <param name="fullFileName">The full path of the file to export.</param>
        /// <param name="contentType">The MIME content type for the file.</param>
        protected void ExportFile(string fullFileName, string contentType)
        {
            var fi = new FileInfo(fullFileName);
            
            RequestHelper.SendFileToClient(this.Context, fi.FullName, contentType, fi.Name, false);

            this.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// Check if there is any columns need to be reviewed.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Click_CheckWhitelistStatus(object sender, EventArgs e)
        {
            if (!this.UserHas(E_InternalUserPermissions.DownloadReplicationWhitelist))
            {
                throw new AccessDenied();
            }

            List<string> errorList = new List<string>();
            string fileName = Tools.GetServerMapPath(BrokerReplicate.SenderWhitelistMapAddr);
            if (!FileOperationHelper.Exists(fileName))
            {
                var msg = "The whitelist does not exist. You can import the example white list instead.";
                string warningAlert = this.GetAlertForMessage(msg);
                Response.Write(warningAlert);
                return;
            }

            Dictionary<string, WhitelistData> whitelistDictionary;
            Dictionary<string, string> reconstructMap;

            bool needReview = !ShareImportExport.CheckWhitelistStatusForAllTables(ref errorList, out whitelistDictionary, out reconstructMap, fileName);

            if (needReview)
            {
                string newWhiteListPath = TempFileUtils.NewTempFilePath() + ShareImportExport.WhiteListFileExtension;
                ShareImportExport.WriteCsvWhistList(ref errorList, ref reconstructMap, whitelistDictionary, newWhiteListPath, false);
                this.ExportFile(newWhiteListPath, MIMEContentType.CSV);
            }
            else
            {
                string msg = this.GetAlertForMessage("All tables and columns have been reviewed.");
                Response.Write(msg);
            }
        }

        /// <summary>
        /// Export the current white list.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void ExportMainWhiteList(object sender, EventArgs e)
        {
            if (!this.CheckIfCorrectProductionSite())
            {
                return;
            }

            if (!this.UserHas(E_InternalUserPermissions.DownloadReplicationWhitelist))
            {
                throw new AccessDenied();
            }

            string fileName = Tools.GetServerMapPath(BrokerReplicate.SenderWhitelistMapAddr);
            if (!FileOperationHelper.Exists(fileName))
            {
                var msg = "The whitelist does not exist. You can import the example white list instead.";
                string warning = this.GetAlertForMessage(msg);
                Response.Write(warning);
            }
            else
            {
                this.ExportFile(fileName, MIMEContentType.CSV);
            }
        }

        /// <summary>
        /// Export the current implicit identity key file.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Click_ExportImplicitIdentityKeysFile(object sender, EventArgs e)
        {
            if (!this.CheckIfCorrectProductionSite())
            {
                return;
            }

            if (!this.UserHas(E_InternalUserPermissions.DownloadReplicationWhitelist))
            {
                throw new AccessDenied();
            }

            string fileName = Tools.GetServerMapPath(BrokerReplicate.ImplicitIdentityKeyFileAddress);
            if (!FileOperationHelper.Exists(fileName))
            {
                var msg = "The implicit identity key file does not exist.";
                string warning = this.GetAlertForMessage(msg);
                Response.Write(warning);
            }
            else
            {
                this.ExportFile(fileName, MIMEContentType.CSV);
            }
        }

        /// <summary>
        /// Export default white list where all fields are set to be exportable.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void ExportMainDefaultWhiteList(object sender, EventArgs e)
        {
            if (!this.UserHas(E_InternalUserPermissions.DownloadReplicationWhitelist))
            {
                throw new AccessDenied();
            }

            List<string> allTableNm = new List<string>();

            // string tableNmQuery = "SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME NOT LIKE 'view_%'";
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_GetAllNonViewTableNames"))
            {
                while (reader.Read())
                {
                    allTableNm.Add(Convert.ToString(reader["TABLE_NAME"]).TrimWhitespaceAndBOM());
                }
            }

            string fileName = ShareImportExport.GetDefaultCsvWhitelistByList(allTableNm);
            this.ExportFile(fileName, MIMEContentType.CSV);
        }

        /// <summary>
        /// Import white list.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Click_UploadMainWhitelist(object sender, EventArgs e)
        {
            if (!this.UserHas(E_InternalUserPermissions.UploadReplicationWhitelist))
            {
                throw new AccessDenied();
            }

            string savePath = Tools.GetServerMapPath(BrokerReplicate.SenderWhitelistMapAddr);

            if (!this.FileUpload_MainWhiteList.HasFile)
            {
                this.UploadMainWhiteStatusLabel.Text = "You did not specify a file to upload.";
                return;
            }

            int fileSize = this.FileUpload_MainWhiteList.PostedFile.ContentLength;
            this.FileUpload_MainWhiteList.SaveAs(savePath);

            List<string> errorMsgList = new List<string>();
            Dictionary<string, string> reconstructMap = null;
            Dictionary<string, WhitelistData> whitelistContainer;
            ShareImportExport.ReadCsvWhiteList(ref errorMsgList, out reconstructMap, out whitelistContainer, savePath, false);
            BrokerReplicate.SenderWhitelistDictionary = whitelistContainer;

            if (errorMsgList.Any())
            {
                foreach (string msg in errorMsgList)
                {
                    string error = this.GetAlertForMessage(msg);
                    Response.Write(error);
                }

                this.UploadMainWhiteStatusLabel.Text = "Whitelist was not imported. Please correct the error and try again.";
            }
            else
            {
                var msg = "Your whitelist was uploaded successfully.";
                Response.Write(this.GetAlertForMessage(msg));
                this.UploadMainWhiteStatusLabel.Text = msg;
                this.UpdateWhiteListHashText();
            }
        }

        /// <summary>
        /// Import implicit identity key reference file.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="ea">The event argument.</param>
        protected void Click_UploadImplicitIdentityKeys(object sender, EventArgs ea)
        {
            if (!this.UserHas(E_InternalUserPermissions.UploadReplicationWhitelist))
            {
                throw new AccessDenied();
            }

            this.ImplicitIdentityKeysStatusLabel.Text = string.Empty;
            string savePath = Tools.GetServerMapPath(BrokerReplicate.ImplicitIdentityKeyFileAddress);
            string tmpSavePath = savePath + ".tmp.csv";

            if (!this.FileUpload_ImplicitIdentityKeys.HasFile)
            {
                this.ImplicitIdentityKeysStatusLabel.Text = "You did not specify a file to upload.";
                return;
            }

            int fileSize = this.FileUpload_ImplicitIdentityKeys.PostedFile.ContentLength;
            this.FileUpload_ImplicitIdentityKeys.SaveAs(tmpSavePath);

            ILookup<int, string> errors;
            try
            {
                var implicitIdentityKeyReferences =
                    ShareImportExport.ReadImplicitIdentityKeyReferencesFile(tmpSavePath, out errors);
            }
            catch (ArgumentException e)
            {
                Tools.LogError(e);
                errors = new List<Tuple<int, string>>()
                {
                    new Tuple<int, string>(0, e.ToString())
                }.ToLookup(k => k.Item1, k => k.Item2);
            }

            if (errors != null && errors.Any())
            {
                File.Delete(tmpSavePath);

                foreach (string msg in errors.SelectMany(kvp => kvp).ToList())
                {
                    string alert = this.GetAlertForMessage(msg);
                    Response.Write(alert);
                }

                this.ImplicitIdentityKeysStatusLabel.Text = "Implicit identity keys file was not imported. Please correct the error and try again.";
            }
            else
            {
                if (File.Exists(savePath))
                {
                    File.Delete(savePath);
                }

                File.Move(tmpSavePath, savePath);

                var msg = "Your file was uploaded successfully.";
                Response.Write(this.GetAlertForMessage(msg));
                this.ImplicitIdentityKeysStatusLabel.Text = msg;
                this.UpdateImplicitKeysLatestText();
            }
        }

        /*/// <summary>
        /// Function to import the table xml called by page file button.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        /// <param name="savePath">The address for saving the uploaded file.</param>
        /// <param name="tableOption">Select the starter table for import. If it is loan, it will need to import file database data also.</param>
        protected void ImportMainXmlByService(object sender, EventArgs e, string savePath, string tableOption)
        {
            string whitelistAddr = Tools.GetServerMapPath(BrokerReplicate.SenderWhitelistMapAddr);
            if (!FileOperationHelper.Exists(whitelistAddr))
            {
                Response.Write(this.GetAlertForMessage("Cannot find white list at " + whitelistAddr.Replace("\\", "\\\\")));
                return;
            }

            DBTableImportService.DBTableImportService service = new DBTableImportService.DBTableImportService();
            service.Url = ConstSite.TableReplicateServiceUrl;

            byte[] whitelistByteDate = TableReplicateTool.GetFileAsByte(whitelistAddr);
            service.GetAndLoadWhitelist(whitelistByteDate);

            byte[] xmlByteData = TableReplicateTool.GetFileAsByte(savePath);
            string[] errorArr = null;
            this.UploadMainXmlStatusLabel.Text = "Your file is in process. Please wait...";
            this.UploadMainXmlStatusLabel.ForeColor = Color.Yellow;

            service.ImportXmlFile(ref errorArr, xmlByteData, tableOption, string.Empty);
            if (errorArr.Length > 0)
            {
                foreach (string err in errorArr)
                {
                    Response.Write(this.GetAlertForMessage(err));
                }

                this.UploadMainXmlStatusLabel.Text = "File is not exported. Please correct the error and try again.";
            }
            else
            {
                var msg = "Your Xml file was uploaded successfully.";
                Response.Write(this.GetAlertForMessage(msg));
                this.UploadMainXmlStatusLabel.ForeColor = Color.Green;
                this.UploadMainXmlStatusLabel.Text = msg;
            }
        }
        */

        /// <summary>
        /// Function to import the table xml called by page file button.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Click_UploadBrokerXml(object sender, EventArgs e)
        {
            if (!this.UserHas(E_InternalUserPermissions.ImportBroker))
            {
                throw new AccessDenied();
            }

            if (this.NeedDebugInfoCheckBoxId.Checked)
            {
                Tools.LogInfo($"ConstStage.UseLibForBrokerReplication is {ConstStage.UseLibForBrokerReplication} so using "
                    + (ConstStage.UseLibForBrokerReplication ? nameof(this.Click_UploadBrokerXml_New) : nameof(this.Click_UploadBrokerXml_Old)));
            }

            if (ConstStage.UseLibForBrokerReplication)
            {
                this.Click_UploadBrokerXml_New(sender, e);
            }
            else
            {
                this.Click_UploadBrokerXml_Old(sender, e);
            }
        }

        /// <summary>
        /// The actual one that do the writing of exported file for broker and its related tables.
        /// </summary>
        /// <param name="fileDBKeysForDirectTransfer">The file db entries to directly add to the exported zip.</param>
        /// <returns>The file address of broker export.</returns>
        protected string GetMainXmlFile_BySp(out IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer)
        {
            string mainDirectory = Tools.GetServerMapPath(@"~/");
            string whitelistPath = Tools.GetServerMapPath(BrokerReplicate.SenderWhitelistMapAddr);

            List<string> errorList = new List<string>();
            List<string> warningList = new List<string>();
            
            string filename = null;
            int justinOneSqlQueryTimeOutInSeconds = ConstStage.JustinOneSqlQueryTimeOutInSeconds;

            string userOption = this.m_MainExport_RadioOption.SelectedValue;
            string branchName = userOption == "ONE_BRANCH_USERS" ? this.m_branchNameInputBox.Value : string.Empty;
            bool isSystemTableExport = userOption == "SYS_TABLES" ? true : false;
            bool includeFiledb = this.BrokerExportIncludeFiledbCheckBoxId.Checked;
            if (userOption == "LOAN_TEMPLATES")
            {
                fileDBKeysForDirectTransfer = new List<FileDBEntryInfoForExport>();
                filename = TableReplicateTool.ExportLoanTemplatesByBrokerId(
                    ref errorList,
                    ref warningList,
                    out fileDBKeysForDirectTransfer,
                    whitelistPath,
                    mainDirectory,
                    this.BrokerId.ToString(),
                    includeFiledb,
                    userOption,
                    this.BrokerExport_ShowDebugInfo.Checked);
            }
            else
            {
                filename = TableReplicateTool.ExportAllBrokerRelatedTablesBySp(
                    ref errorList,
                    ref warningList,
                    out fileDBKeysForDirectTransfer,
                    whitelistPath,
                    mainDirectory,
                    this.BrokerId.ToString(),
                    this.BrokerExportIncludeFiledbCheckBoxId.Checked,
                    userOption,
                    branchName,
                    justinOneSqlQueryTimeOutInSeconds,
                    this.BrokerExport_ShowDebugInfo.Checked);
            }

            if (errorList.Any())
            {
                foreach (string msg in errorList)
                {
                    string error = this.GetAlertForMessage(msg);
                    Tools.LogError(msg);

                    Response.Write(error);
                }

                return null;
            }
            else
            {
                if (warningList.Any())
                {
                    foreach (string msg in warningList)
                    {
                        string warning = this.GetAlertForMessage(msg);
                        Tools.LogWarning(msg);
                        Response.Write(warning);
                    }
                }

                return filename;
            }
        }

        /// <summary>
        /// This function generates an xml file that contains the exported entries. <para/>
        /// Related tables are tables that the tables foreign key to, or that we have determined to be related.
        /// </summary>
        /// <param name="fileDbKeyEntriesForDirectTransfer">Additional filedb entries for direct export.</param>
        /// <param name="starterConnBrokerIdCustomerCodeComboList">A list of connection information to find the data source.</param>
        /// <param name="includeFiledb">A boolean value decide whether to include file db entries.</param>
        /// <param name="starterDataSrc">A data source for the entries. All entries must belong to the same data source.</param>
        /// <param name="starterTableNmList">The starter table name list.</param>
        /// <param name="starterTableEntryList">The starter table entry list.</param>
        /// <param name="needExportRelatedTable">True iff exporting related tables.</param>
        /// <returns>Return the path to the xml file that contains the exported entries.</returns>
        protected string GetEntryXmlFile_BySp(
            out IEnumerable<FileDBEntryInfoForExport> fileDbKeyEntriesForDirectTransfer,
            List<string> starterConnBrokerIdCustomerCodeComboList, 
            bool includeFiledb, 
            DataSrc starterDataSrc, 
            List<string> starterTableNmList, 
            List<string> starterTableEntryList,
            bool needExportRelatedTable)
        {
            string mainDirectory = Tools.GetServerMapPath(@"~/");
            string whitelistPath = Tools.GetServerMapPath(BrokerReplicate.SenderWhitelistMapAddr);

            List<string> errorList = new List<string>();
            List<string> warningList = new List<string>();
            fileDbKeyEntriesForDirectTransfer = null;

            string filename = null;
            if (!errorList.Any())
            {
                List<List<string>> missingConnBrokerIdCustomerCodeComboListList = new List<List<string>>();
                HashSet<string> allExportedTableNmSet = new HashSet<string>();
                List<List<string>> allMissingEntryListList = this.GetRelatedTableEntries(
                    ref errorList,
                    ref missingConnBrokerIdCustomerCodeComboListList,
                    out allExportedTableNmSet,
                    starterDataSrc,
                    starterConnBrokerIdCustomerCodeComboList,
                    starterTableNmList,
                    starterTableEntryList,
                    needExportRelatedTable,
                    this.CustomExport_ShowDebugInfo.Checked);

                bool needUpdateWhiteList;
                bool needReview;
                Dictionary<string, WhitelistData> whitelistDictionary;
                Dictionary<string, string> reconstructMap;

                if (starterTableEntryList.Count == 0)
                {
                    var msg = "No entries found at this broker.";
                    errorList.Add(msg);
                    Response.Write(this.GetAlertForMessage(msg));
                    return null;
                }

                filename = TableReplicateTool.ExportTablesBySpAndEntryList(
                    ref errorList,
                    ref warningList,
                    out whitelistDictionary,
                    out reconstructMap,
                    out needReview,
                    out needUpdateWhiteList,
                    out fileDbKeyEntriesForDirectTransfer,
                    whitelistPath,
                    mainDirectory,
                    this.CustomExport_RadioOption.SelectedValue,
                    includeFiledb,
                    allExportedTableNmSet,
                    isLoanOverwrite: false,
                    listOfListsOfBrokerConnectionInfoStrings: missingConnBrokerIdCustomerCodeComboListList,
                    listOfListsOfTableRecordStrings: allMissingEntryListList,
                    needDebugInfo: this.CustomExport_ShowDebugInfo.Checked);
            }

            if (errorList.Any())
            {
                foreach (string msg in errorList)
                {
                    string error = this.GetAlertForMessage(msg);
                    Tools.LogError(msg);
                    Response.Write(error);
                }

                return null;
            }
            else
            {
                if (warningList.Any())
                {
                    foreach (string msg in warningList)
                    {
                        string warning = this.GetAlertForMessage(msg);
                        Tools.LogWarning(msg);
                        Response.Write(warning);
                    }
                }

                return filename;
            }
        }

        /// <summary>
        /// Generate a list of table names, and full entries (all primary keys) and associated broker id and customer code pairs.
        /// If UseOnlyThisBrokersDb is checked, it skips any entries from other brokers.
        /// tdsk: 
        ///     - Instead of returning separate lists, return one list of objects that encapsulates all the things.
        ///     - Instead of storing things as pipe separated strings, return objects.
        /// </summary>
        /// <param name="errorList">Record the list of errors.</param>
        /// <param name="starterDataSrc">A data source for the entries. All entries must belong to the same data source.</param>
        /// <param name="starterConnBrokerIdCustomerCodeComboList">A list of connection information to find the data source.</param>
        /// <param name="starterTableNmList">A list of table names to start with the export.</param>
        /// <param name="starterTableEntryList">A list of entries correspond to the table name list.</param>
        /// <param name="textboxContent">The textbox content user enters.</param>
        protected void GetInputEntries(ref List<string> errorList, out DataSrc starterDataSrc, out List<string> starterConnBrokerIdCustomerCodeComboList, out List<string> starterTableNmList, out List<string> starterTableEntryList, string textboxContent)
        {
            starterDataSrc = DataSrc.LOShare;
            starterTableNmList = new List<string>();
            starterTableEntryList = new List<string>();
            starterConnBrokerIdCustomerCodeComboList = new List<string>();

            if (textboxContent.Length == 0)
            {
                string errorMsg = "Please enter your table entry to export.";
                errorList.Add(this.GetAlertForMessage(errorMsg));
                return;
            }
            
            // unfiltered here means the list could have entries from all brokers.
            var unfilteredStarterTableNmList = new List<string>();
            var unfilteredStarterTableEntryList = new List<string>();
            
            DataSrc dataSource = (this.CustomExport_RadioOption.SelectedValue == "RATE_SHEET") ? DataSrc.RateSheet : DataSrc.LOShare;
            starterDataSrc = dataSource;
            Dictionary<string, List<string>> primaryKeyColumnNamesByTableName = ShareImportExport.GetPrimaryKeyDictionary(dataSource);
            Guid? brokerIdToFilterIn = UseOnlyThisBrokersDb.Checked ? this.BrokerId : (Guid?)null;

            // By entry we mean an inputted line separated by semicolons.  It could be one record or multiple depending on how many results are associated with it.
            List<string> inputEntryList = textboxContent.Replace("\r", string.Empty).Replace("\n", string.Empty).Trim('\t').TrimEnd(';').ToUpper().Split(';').ToList();
            HashSet<string> inputEntrySet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            var databaseConnectionInfos = new List<DbConnectionInfo>();
            foreach (string inputEntry in inputEntryList)
            {
                if (inputEntrySet.Contains(inputEntry))
                {
                    continue;
                }

                inputEntrySet.Add(inputEntry);
                string formattedEntry = inputEntry.Trim('\t');
                formattedEntry = formattedEntry.Replace('\t', '|');
                ShareImportExport.TryGetConnInfoAndStandardEntryByOneInputEntryAndDataSrc(
                    ref errorList, 
                    ref databaseConnectionInfos, 
                    ref unfilteredStarterTableEntryList, 
                    ref unfilteredStarterTableNmList,
                    dataSource, 
                    primaryKeyColumnNamesByTableName, 
                    formattedEntry,
                    brokerIdToFilterIn);
            }

            if (errorList.Any())
            {
                return;
            }

            int numEntries = unfilteredStarterTableEntryList.Count;
            if (dataSource != DataSrc.LOShare)
            {
                for (int i = 0; i < numEntries; i++)
                {
                    starterConnBrokerIdCustomerCodeComboList.Add("|");
                }

                starterTableEntryList = unfilteredStarterTableEntryList;
                starterTableNmList = unfilteredStarterTableNmList;
                return;
            }
            
            // Get the associated broker id and customer code with each record. Filter out other brokers if the user requested that.
            starterTableEntryList = new List<string>(unfilteredStarterTableEntryList.Count);
            starterTableNmList = new List<string>(unfilteredStarterTableNmList.Count);
            for (int i = 0; i < numEntries; i++)
            {
                Guid connBrokerId;
                string customerCode;
                string entry = unfilteredStarterTableEntryList[i];
                ShareImportExport.TryGetBrokerIdAndCustomerCodeByEntryAndDbConnInfo(
                    ref errorList, 
                    out connBrokerId, 
                    out customerCode,
                    databaseConnectionInfos[i], 
                    entry);

                if (brokerIdToFilterIn.HasValue)
                {
                    if (connBrokerId != brokerIdToFilterIn.Value)
                    {
                        continue;
                    }
                }

                starterTableEntryList.Add(entry);
                starterTableNmList.Add(unfilteredStarterTableNmList[i]);
                starterConnBrokerIdCustomerCodeComboList.Add(connBrokerId.ToString() + "|" + customerCode);
            }
        }

        /// <summary>
        /// Search and generated a complete list of entries by searching the related tables of the starter table list.
        /// </summary>
        /// <param name="errorList">Record errors into the list.</param>
        /// <param name="relatedConnBrokerIdCustomerCodeComboListList">A list of connection information to find the data source. This is one of the result you would want for related entries.</param>
        /// <param name="allTableNmSet">Record every unique table name in the entries.</param>
        /// <param name="starterDataSrc">A data source for the entries. All entries must belong to the same data source.</param>
        /// <param name="starterConnBrokerIdCustomerCodeComboList">A list of connection information to find the data source, a list that corresponding to each entry in the start entry list.</param>
        /// <param name="starterTableNmList">The names of a list of tables to start with.</param>
        /// <param name="starterTableEntryList">Entries correspond to the table name list.</param>
        /// <param name="needExportRelatedTable">If yes, it will search related tables. If no, it will only export the starter tables.</param>
        /// <param name="showDebugInfo">A boolean value decide whether to print out progress message or not.</param>
        /// <returns>Return the related table entries list.</returns>
        protected List<List<string>> GetRelatedTableEntries(ref List<string> errorList, ref List<List<string>> relatedConnBrokerIdCustomerCodeComboListList, out HashSet<string> allTableNmSet, DataSrc starterDataSrc, List<string> starterConnBrokerIdCustomerCodeComboList, List<string> starterTableNmList, List<string> starterTableEntryList, bool needExportRelatedTable, bool showDebugInfo)
        {
            allTableNmSet = new HashSet<string>();
            List<List<string>> missingTableNmListList = new List<List<string>>();
            List<List<string>> missingTableEntryListList = new List<List<string>>();
            if (!needExportRelatedTable)
            {
                string currConnInfo = string.Empty;
                int currConnInfoInd = 0;
                int starterConnCnt = starterConnBrokerIdCustomerCodeComboList.Count();
                for (int i = 0; i < starterConnCnt; i++)
                {
                    string tableNm = starterTableEntryList[i].Substring(0, starterTableEntryList[i].IndexOf('|'));
                    if (!allTableNmSet.Contains(tableNm))
                    {
                        allTableNmSet.Add(tableNm);
                    }

                    if (currConnInfo != starterConnBrokerIdCustomerCodeComboList[i])
                    {
                        currConnInfo = starterConnBrokerIdCustomerCodeComboList[i];
                        relatedConnBrokerIdCustomerCodeComboListList.Add(new List<string>());
                        missingTableEntryListList.Add(new List<string>());
                        currConnInfoInd = relatedConnBrokerIdCustomerCodeComboListList.Count() - 1;
                    }

                    relatedConnBrokerIdCustomerCodeComboListList[currConnInfoInd].Add(currConnInfo);
                    missingTableEntryListList[currConnInfoInd].Add(starterTableEntryList[i]);
                }

                return missingTableEntryListList;
            }

            Dictionary<string, WhitelistData> whitelistDictionary = null;
            Dictionary<string, string> m_reconstructMap = null;
            if (errorList.Any() == false)
            {
                string whitelistPath = Tools.GetServerMapPath(BrokerReplicate.SenderWhitelistMapAddr);
                ShareImportExport.ReadCsvWhiteList(ref errorList, out m_reconstructMap, out whitelistDictionary, whitelistPath, false);
            }

            if (errorList.Any() == false)
            {
                List<List<string>> fkeyRelationshipTable = TableReplicateTool.GetFkRelationshipTable(starterDataSrc);
                Dictionary<string, List<string>> primaryKeyDictionary = ShareImportExport.GetPrimaryKeyDictionary(starterDataSrc);
                Dictionary<string, string> defaultForeignKeyValDictionary = ShareImportExport.GetDefaultForeignKeyValDictionary();
                Dictionary<string, List<string>> foreignKeyDictionary = TableReplicateTool.GetForeignKeyDictionary(fkeyRelationshipTable);
                Dictionary<string, List<string>> fkeyRealNmDictionary = TableReplicateTool.GetFkRealNmDictionary(fkeyRelationshipTable);
                Dictionary<string, List<string>> tableMappingKeyList = TableReplicateTool.GetTablePairMappingKeyPairList(fkeyRelationshipTable);
                ////Dictionary<string, string> identityDictionary = ShareImportExport.HardcodeIdentityTableKeyDictionary();
                ////HashSet<string> findNextWaiveSet = TableReplicateTool.HardCodeWaiveFindNextTableSet();
                ////TableReplicateTool.AddProtectFieldOfWhitelistIntoWaiveFindNextSet(ref findNextWaiveSet, whitelistDictionary);
                string missingTableEntryFilepath = Tools.GetServerMapPath(@"~/") + "missTableEntry.txt";
                HashSet<string> allSentFindSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                Dictionary<string, int> tableRankMapping = TableReplicateTool.HardCodeRankingForInsertionSeq();

                int size = starterTableEntryList.Count();
                for (int i = 0; i < size; i++)
                {
                    if (allSentFindSet.Contains(starterTableEntryList[i]))
                    {
                        continue;
                    }
                    else
                    {
                        allSentFindSet.Add(starterTableEntryList[i]);
                    }

                    List<string> missingTableNmList = new List<string>();
                    List<string> missingTableEntryList = new List<string>();
                    List<string> missingConnBrokerIdCustomerCodeComboList = new List<string>();
                    List<string> oneStarterTableNm = new List<string>();
                    List<string> oneStarterTableEntry = new List<string>();
                    List<string> oneConnInfoCombo = new List<string>();

                    oneStarterTableNm.Add(starterTableNmList[i]);
                    oneStarterTableEntry.Add(starterTableEntryList[i]);
                    oneConnInfoCombo.Add(starterConnBrokerIdCustomerCodeComboList[i]);

                    if (showDebugInfo)
                    {
                        Tools.LogInfo("Start to find related table for entry[" + i + "]");
                    }

                    TableReplicateTool.FindAllRelatedTableEntry_NoService(
                        ref errorList,
                        ref missingConnBrokerIdCustomerCodeComboList,
                        ref missingTableNmList,
                        ref missingTableEntryList,
                        ref allSentFindSet,
                        starterDataSrc,
                        oneConnInfoCombo,
                        oneStarterTableNm,
                        oneStarterTableEntry,
                        primaryKeyDictionary,
                        foreignKeyDictionary,
                        fkeyRealNmDictionary,
                        tableMappingKeyList,
                        whitelistDictionary,
                        missingTableEntryFilepath,
                        needExportRelatedTable);

                    List<int> orderedIndex = TableReplicateTool.GetInsertSeqLevelMapping_EasyUse(tableRankMapping, missingTableNmList);
                    List<string> sortedTableNmList = new List<string>();
                    List<string> srotedTableEntryList = new List<string>();
                    for (int ind = 0; ind < orderedIndex.Count(); ind++)
                    {
                        sortedTableNmList.Add(missingTableNmList[orderedIndex[ind]]);
                        srotedTableEntryList.Add(missingTableEntryList[orderedIndex[ind]]);
                    }

                    missingTableNmListList.Add(sortedTableNmList);
                    missingTableEntryListList.Add(srotedTableEntryList);
                    relatedConnBrokerIdCustomerCodeComboListList.Add(missingConnBrokerIdCustomerCodeComboList);
                }
            }

            foreach (List<string> missingTableNmList in missingTableNmListList)
            {
                foreach (string tableNm in missingTableNmList)
                {
                    if (!allTableNmSet.Contains(tableNm))
                    {
                        allTableNmSet.Add(tableNm);
                    }
                }
            }

            return missingTableEntryListList;
        }

        /// <summary>
        /// Export broker and all its related tables according to the options user chooses.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Click_ExportBrokerXml(object sender, EventArgs e)
        {
            if (!this.UserHas(E_InternalUserPermissions.ExportBroker))
            {
                throw new AccessDenied();
            }

            if (ConstStage.UseLibForBrokerReplication)
            {
                this.Click_ExportBrokerXmlNew(sender, e);
            }
            else
            {
                this.Click_ExportBrokerXmlOld(sender, e);
            }
        }

        /// <summary>
        /// Do the patch export that export also the related entries.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Click_ExportRelatedEntriesXml(object sender, EventArgs e)
        {
            if (!this.UserHas(E_InternalUserPermissions.ExportBroker))
            {
                throw new AccessDenied();
            }

            this.ExportCustomEntriesXml(includeRelated: true);
        }

        /// <summary>
        /// Do the patch export that export only the input entries.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Click_ExportOnlyInputEntriesXml(object sender, EventArgs e)
        {
            if (!this.UserHas(E_InternalUserPermissions.ExportBroker))
            {
                throw new AccessDenied();
            }

            this.ExportCustomEntriesXml(includeRelated: false);
        }

        /// <summary>
        /// Preview the list of entries for patch export.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Click_PreviewRelatedEntries(object sender, EventArgs e)
        {
            if (!this.UserHas(E_InternalUserPermissions.DownloadReplicationWhitelist))
            {
                throw new AccessDenied();
            }

            HttpContext.Current.Server.ScriptTimeout = ConstStage.JustinBrokerExportTimeOutInSeconds;
            List<string> errorList = new List<string>();
            List<string> starterTableNmList = new List<string>();
            List<string> starterTableEntryList = new List<string>();
            DataSrc starterDataSrc = DataSrc.LOShare;
            List<string> starterConnBrokerIdCustomerCodeComboList = new List<string>();
            HashSet<string> allExportedTableNmSet = new HashSet<string>();
            List<List<string>> missingConnBrokerIdCustomerCodeComboListList = new List<List<string>>();
            this.GetInputEntries(ref errorList, out starterDataSrc, out starterConnBrokerIdCustomerCodeComboList, out starterTableNmList, out starterTableEntryList, this.m_PatchExportTextBox.Text);
            List<List<string>> missingTableEntryListList = this.GetRelatedTableEntries(
                ref errorList,
                ref missingConnBrokerIdCustomerCodeComboListList,
                out allExportedTableNmSet,
                starterDataSrc,
                starterConnBrokerIdCustomerCodeComboList,
                starterTableNmList,
                starterTableEntryList,
                needExportRelatedTable: true,
                showDebugInfo: this.CustomExport_ShowDebugInfo.Checked);

            if (errorList.Any())
            {
                foreach (string msg in errorList)
                {
                    string error = this.GetAlertForMessage(msg);
                    Response.Write(error);
                }
            }
            else
            {
                string filepath = TempFileUtils.NewTempFilePath() + ".csv";
                using (StreamWriter writer = new StreamWriter(filepath))
                {
                    //// int step = 0;
                    foreach (List<string> missingTableEntryList in missingTableEntryListList)
                    {
                        //// writer.WriteLine("step #" + step++);
                        foreach (string entry in missingTableEntryList)
                        {
                            writer.WriteLine(entry.Replace('|', ',') + ";");
                        }

                        writer.WriteLine();
                    }
                }

                this.ExportFile(filepath, MIMEContentType.CSV);
            }
        }

        /// <summary>
        /// Test file database.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Click_ExportFiledbByKey(object sender, EventArgs e)
        {
            string content = string.Empty;
            if (this.ExportFileDbSwitchOldNewCheckBox.Checked)
            {
                ////content = FileDBTools.ReadDataText(E_FileDB.Normal, this.InputFiledb_FiledbKey.Value);
                byte[] bytes = FileDBTools.ReadData(E_FileDB.Normal, this.InputFiledb_FiledbKey.Value);
                content = Encoding.UTF8.GetString(bytes);
            }
            else
            {
                Guid loanId = new Guid(this.InputFiledb_LoanId.Value);
                Guid filedbKey = new Guid(this.InputFiledb_FiledbKey.Value);
                LargeFieldStorage fileDb = new LargeFieldStorage(loanId);
                content = fileDb.GetText(filedbKey);
            }

            string filepath = TempFileUtils.NewTempFilePath() + ".txt";
            using (StreamWriter writer = new StreamWriter(filepath))
            {
                writer.WriteLine(content);
            }

            this.ExportFile(filepath, MIMEContentType.Text);
        }

        /// <summary>
        /// Test file database.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void UploadFiledbContent_Click(object sender, EventArgs e)
        {
            string filename = TempFileUtils.NewTempFilePath() + ".txt";
            if (this.FileUpload_ImportFiledb.HasFile)
            {
                int fileSize = this.FileUpload_ImportFiledb.PostedFile.ContentLength;
                this.FileUpload_MainXml.SaveAs(filename);
            }
            else
            {
            }

            Guid loanId = new Guid(this.importFiledb_LoanId.Value);
            Guid filedbKey = new Guid(this.importFiledb_FiledbKey.Value);
            LargeFieldStorage filedb = new LargeFieldStorage(loanId);
            string content = TextFileHelper.ReadFile(filename);
            filedb.SetText(filedbKey, content);
            filedb.Flush();
        }        

        /// <summary>
        /// A simple test function.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Click_GetCompileCode(object sender, EventArgs e)
        {
            string filename = TempFileUtils.NewTempFilePath() + ".xml";
            if (this.CompileCode_XmlFile.HasFile)
            {
                int fileSize = this.CompileCode_XmlFile.PostedFile.ContentLength;
                this.CompileCode_XmlFile.SaveAs(filename);
            }
            else
            {
            }

            string compileCodeXmlFileContent = TextFileHelper.ReadFile(filename);
            DateTime versionDate = DateTime.Now;
            string hexVal = BrokerReplicator.GetCompiledCodeHexStr(compileCodeXmlFileContent, versionDate);
            this.CompileCodeResultBox.Text = hexVal + "\n\n" + versionDate;
        }

        /// <summary>
        /// Do the patch export that export only the input entries or the related entries depending on the value of includeRelated. <para></para>
        /// Note: related means the tables that the requested tables depend on, not the tables that depend on the requested tables.
        /// </summary>
        /// <param name="includeRelated">Whether or not to include related tables in the export XML.</param>
        private void ExportCustomEntriesXml(bool includeRelated)
        {
            if (this.NeedDebugInfoCheckBoxId.Checked)
            {
                Tools.LogInfo($"ConstStage.UseLibForBrokerReplication is {ConstStage.UseLibForBrokerReplication} so using "
                    + (ConstStage.UseLibForBrokerReplication ? nameof(this.ExportCustomEntriesXml_New) : nameof(this.ExportCustomEntriesXml_Old)));
            }

            if (ConstStage.UseLibForBrokerReplication)
            {
                this.ExportCustomEntriesXml_New(includeRelated);
            }
            else
            {
                this.ExportCustomEntriesXml_Old(includeRelated);
            }
        }

        /// <summary>
        /// Do the patch export that export only the input entries or the related entries depending on the value of includeRelated. <para></para>
        /// Note: related means the tables that the requested tables depend on, not the tables that depend on the requested tables. <para></para>
        /// This uses the library to do the heavy lifting.
        /// </summary>
        /// <param name="includeRelated">Whether or not to include related tables in the export XML.</param>
        private void ExportCustomEntriesXml_New(bool includeRelated)
        {
            this.Status.Text = string.Empty;
            this.WarningMessages.Text = string.Empty;
            this.ErrorMessages.Text = string.Empty;
            
            // tdsk: consider making these options part of the broker export options.
            var options =
            new BrokerExportOptions()
            {
                BrokerId = this.BrokerId,
                IncludeFileDbEntries = this.CustomExportIncludeFiledbCheckBoxId.Checked,
                ExportType = ReplicationExportType.Custom,
                Compress = this.CompressCustomExport.Checked,
                SelectedDB = this.CustomExport_RadioOption.SelectedValue,
                LogDebugInfo = this.CustomExport_ShowDebugInfo.Checked, // default to be false.
                CustomOptions = new CustomBrokerExportOptions()
                {
                    Text = this.m_PatchExportTextBox.Text,
                    IncludeRelatedTables = includeRelated,
                    UseOnlyThisBrokersDb = this.UseOnlyThisBrokersDb.Checked // default this to be true.
                }
            };

            var result = BrokerReplicator.ExportBrokerXml(PrincipalFactory.CurrentPrincipal, options);
            
            if (result.Succeeded)
            {
                this.ExportFile(TempFileUtils.Name2Path(result.TempFileName), result.ContentType);
                return;
            }

            foreach (string msg in result.ErrorMessages)
            {
                string error = this.GetAlertForMessage(msg);
                Response.Write(error);
            }

            return;
        }

        /// <summary>
        /// Do the patch export that export only the input entries or the related entries depending on the value of includeRelated. <para></para>
        /// Note: related means the tables that the requested tables depend on, not the tables that depend on the requested tables. <para></para>
        /// This uses the page to do the heavy lifting, not the library.
        /// </summary>
        /// <param name="includeRelated">Whether or not to include related tables in the export XML.</param>
        private void ExportCustomEntriesXml_Old(bool includeRelated)
        {
            if (!this.CheckIfCorrectProductionSite())
            {
                return;
            }

            this.Status.Text = string.Empty;
            this.WarningMessages.Text = string.Empty;
            this.ErrorMessages.Text = string.Empty;

            HttpContext.Current.Server.ScriptTimeout = ConstStage.JustinBrokerExportTimeOutInSeconds;
            List<string> errorList = new List<string>();
            List<string> starterTableNmList = new List<string>();
            List<string> starterTableEntryList = new List<string>();
            DataSrc starterDataSrc = DataSrc.LOShare;
            List<string> starterConnBrokerIdCustomerCodeComboList = new List<string>();
            this.GetInputEntries(ref errorList, out starterDataSrc, out starterConnBrokerIdCustomerCodeComboList, out starterTableNmList, out starterTableEntryList, this.m_PatchExportTextBox.Text);

            if (errorList.Any())
            {
                foreach (string msg in errorList)
                {
                    string error = this.GetAlertForMessage(msg);
                    Response.Write(error);
                }

                return;
            }

            IEnumerable<FileDBEntryInfoForExport> fileDbEntryInfosForDirectTransfer;
            string filename = this.GetEntryXmlFile_BySp(
                out fileDbEntryInfosForDirectTransfer,
                starterConnBrokerIdCustomerCodeComboList,
                this.CustomExportIncludeFiledbCheckBoxId.Checked,
                starterDataSrc,
                starterTableNmList,
                starterTableEntryList,
                needExportRelatedTable: includeRelated);

            if (filename == null)
            {
                return;
            }

            if (!this.CustomExportIncludeFiledbCheckBoxId.Checked)
            {
                fileDbEntryInfosForDirectTransfer = null;
            }

            this.CompressAndExport(filename, MIMEContentType.XML, ExportType.Custom, fileDbEntryInfosForDirectTransfer);
        }

        /// <summary>
        /// Function to import the table xml called by page file button.
        /// </summary>
        /// <param name="savePath">The address for saving the uploaded file.</param>
        /// <param name="tableOption">The option tells the starter table. If it is loan, need to import file database data.</param>
        /// <param name="fileInfos">The file infos that may or may not be imported depending on the imported xml file.</param>
        /// <param name="needDebugInfo">Whether or not debug info is needed.</param>
        private void ImportMainXmlNoService(string savePath, string tableOption, IEnumerable<FileInfo> fileInfos, bool needDebugInfo)
        {
            var stageSiteSource = ConstAppDavid.CurrentServerLocation;
            if (stageSiteSource == ServerLocation.Production)
            {
                Response.Write(this.GetAlertForMessage("Warning: You are not allowed to import brokers into product server!"));
                return;
            }

            string whitelistAddr = Tools.GetServerMapPath(BrokerReplicate.SenderWhitelistMapAddr);
            if (!FileOperationHelper.Exists(whitelistAddr))
            {
                Response.Write(this.GetAlertForMessage("Cannot find white list at " + whitelistAddr.Replace("\\", "\\\\")));
                return;
            }

            List<string> errorList = new List<string>();
            List<string> warningList = new List<string>();
            Dictionary<string, string> reconstructMap = null;
            Dictionary<string, WhitelistData> whitelistDictionary;
            ShareImportExport.ReadCsvWhiteList(ref errorList, out reconstructMap, out whitelistDictionary, whitelistAddr, false);

            this.UploadMainXmlStatusLabel.Text = "Your file is in process. Please wait...";
            this.UploadMainXmlStatusLabel.ForeColor = Color.Yellow;

            int justinOneSqlQueryTimeOutInSeconds = ConstStage.JustinOneSqlQueryTimeOutInSeconds;
            TableReplicateTool.ImportOneXmlFileBySp(
                ref errorList,
                ref warningList,
                savePath,
                whitelistDictionary,
                fileInfos,
                tableOption,
                string.Empty,
                this.CombineConfigReleaseCheckBoxId.Checked,
                justinOneSqlQueryTimeOutInSeconds,
                needDebugInfo,
                this.DefaultExternalBrokerFields.Checked);

            if (warningList.Any() && this.CombineConfigReleaseCheckBoxId.Checked)
            {
                foreach (string warning in warningList)
                {
                    Response.Write(this.GetAlertForMessage(warning));
                }
            }

            if (!errorList.Any() && this.CombineConfigReleaseCheckBoxId.Checked)
            {
                BrokerReplicator.ImportSystemSystemConfigFromXml(PrincipalFactory.CurrentPrincipal, errorList, savePath);
            }

            if (errorList.Any())
            {
                Response.Write(this.GetAlertForMessage("One or more error(s) occurred in your broker import.  Please check the error section."));

                this.UploadMainXmlStatusLabel.Text = "File is not exported. Please correct the error and try again.";
                this.UploadBrokerXmlErrorMessages.InnerHtml = string.Join("<br/>", errorList.Select(s => AspxTools.HtmlString(s)).ToArray());
                return;
            }

            Response.Write(this.GetAlertForMessage("Your Xml file was uploaded successfully."));
            this.UploadMainXmlStatusLabel.ForeColor = Color.Green;
            this.UploadMainXmlStatusLabel.Text = "Your file was uploaded successfully.";
        }

        /// <summary>
        /// May compress the file, then exports it to the client.
        /// </summary>
        /// <param name="filename">The original full file name.</param>
        /// <param name="originalContentType">The original MIME content type of the file.</param>
        /// <param name="exportType">The type of export we are running.</param>
        /// <param name="fileDBKeysForDirectTransfer">The files to be directly exported from filedb.  If there are any values, it will compress regardless of user preference.</param>
        private void CompressAndExport(string filename, string originalContentType, ExportType exportType, IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer)
        {
            var fi = new FileInfo(filename);

            if (fileDBKeysForDirectTransfer == null)
            {
                fileDBKeysForDirectTransfer = new List<FileDBEntryInfoForExport>();
            }

            bool doCompression = false;
            switch (exportType)
            {
                case ExportType.Broker:
                    doCompression = CompressBrokerExport.Checked;
                    break;
                case ExportType.Custom:
                    doCompression = CompressCustomExport.Checked;
                    break;
                default:
                    throw new UnhandledEnumException(exportType);
            }

            if (fileDBKeysForDirectTransfer.Any())
            {
                doCompression = true;
            }            

            string contentType = originalContentType;
            if (doCompression)
            {
                var targetFullFileName = Path.Combine(ConstApp.TempFolder, this.GetMeaningfulFileName(exportType) + ".zip");
                var targetFileInfo = new FileInfo(targetFullFileName);

                var fileDBEntriesThatExist = new List<FileDBEntryInfo>();
                foreach (var fileInfo in fileDBKeysForDirectTransfer)
                {
                    var entry = fileInfo.FileDBEntry;
                    if (!FileDBTools.DoesFileExist(entry.FileDBType, entry.Key))
                    {
                        this.WarningMessages.Text += AspxTools.HtmlString($"No file existing for type: {entry.FileDBType} key: {entry.Key}") + "<br>";
                    }
                    else
                    {
                        fileDBEntriesThatExist.Add(entry);
                    }
                }

                CompressionHelper.CompressWithIOCompression(
                    fileDBEntriesThatExist,
                    new FileInfo[] { fi },
                    targetFileInfo, 
                    deleteFileIfExists: true);

                filename = targetFullFileName;
                contentType = MIMEContentType.ZIP;
            }

            this.ExportFile(filename, contentType);
        }

        /// <summary>
        /// Function to import the table xml called by page file button.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        private void Click_UploadBrokerXml_New(object sender, EventArgs e)
        {
            // tdsk - have this check the database, not by context name.
            if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
            {
                this.UploadMainXmlStatusLabel.Text = "Cannot import into production.";
                return;
            }

            if (!this.FileUpload_MainXml.HasFile)
            {
                this.UploadMainXmlStatusLabel.Text = "You did not specify a file to upload.";
                return;
            }

            string fileNameOnServerToImport = TempFileUtils.NewTempFilePath() + ".xml";
            int fileSize = this.FileUpload_MainXml.PostedFile.ContentLength;
            string srcFileName = this.FileUpload_MainXml.FileName;
            this.FileUpload_MainXml.SaveAs(fileNameOnServerToImport);

            var options = new BrokerImportOptions(fileNameOnServerToImport)
            {
                CombineConfigReleased = this.CombineConfigReleaseCheckBoxId.Checked,
                DefaultExternalBrokerField = this.DefaultExternalBrokerFields.Checked,
                EmailToNotifyOnFinish = null,
                LogDebugInfo = this.NeedDebugInfoCheckBoxId.Checked,
                SourceFileNameIncludingExtension = srcFileName
            };

            var result = BrokerReplicator.ImportBrokerXml(PrincipalFactory.CurrentPrincipal, options);

            if (result.Succeeded)
            {
                Response.Write(this.GetAlertForMessage("Your file was uploaded successfully."));
                this.UploadMainXmlStatusLabel.ForeColor = Color.Green;
                this.UploadMainXmlStatusLabel.Text = "Your file was uploaded successfully.";
                return;
            }

            Response.Write(this.GetAlertForMessage("One or more error(s) occurred in your broker import.  Please check the error section."));
            this.UploadMainXmlStatusLabel.Text = "The file was not imported. Please correct the error(s) and try again.";
            this.UploadBrokerXmlErrorMessages.InnerHtml = string.Empty;
            this.UploadBrokerXmlErrorMessages.InnerHtml += string.Join("<br/>", result.ErrorMessages.Select(s => AspxTools.HtmlString(s)));
            this.UploadBrokerXmlErrorMessages.InnerHtml += "========= warnings ===========<br/>";
            this.UploadBrokerXmlErrorMessages.InnerHtml += string.Join("<br/>", result.WarningMessages.Select(s => AspxTools.HtmlString(s)));
        }

        /// <summary>
        /// Function to import the table xml called by page file button.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        private void Click_UploadBrokerXml_Old(object sender, EventArgs e)
        {
            if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
            {
                this.UploadBrokerXmlErrorMessages.InnerHtml += this.GetAlertForMessage("Cannot import into production.");
                return;
            }

            HttpContext.Current.Server.ScriptTimeout = ConstStage.JustinBrokerImportTimeOutInSeconds;
            string fileNameOnServer = TempFileUtils.NewTempFilePath() + ".xml";
            string tableOption = null;

            if (!this.FileUpload_MainXml.HasFile)
            {
                this.UploadMainXmlStatusLabel.Text = "You did not specify a file to upload.";
                return;
            }

            int fileSize = this.FileUpload_MainXml.PostedFile.ContentLength;
            string srcFileName = this.FileUpload_MainXml.FileName;
            this.FileUpload_MainXml.SaveAs(fileNameOnServer);

            bool needDebugInfo = this.NeedDebugInfoCheckBoxId.Checked;

            if (!srcFileName.ToLower().EndsWith(".zip"))
            {
                if (needDebugInfo)
                {
                    Tools.LogInfo("srcFileName.ToLower() was " + srcFileName + " so didn't decompress.");
                }

                this.ImportMainXmlNoService(fileNameOnServer, tableOption, null, needDebugInfo);
                return;
            }

            if (needDebugInfo)
            {
                Tools.LogInfo("srcFileName.ToLower() was " + srcFileName + " so decompressing.");
            }

            var targetDirectoryFullName = Path.Combine(ConstApp.TempFolder, Path.GetFileNameWithoutExtension(srcFileName));
            var fileOnServerInfo = new FileInfo(fileNameOnServer);
            var targetDirectoryInfo = new DirectoryInfo(targetDirectoryFullName);

            if (needDebugInfo)
            {
                Tools.LogInfo(string.Format("decompressing with fileOnServerInfo {0} and targetDirectoryInfo {1}", fileOnServerInfo.FullName, targetDirectoryInfo.FullName));
            }

            CompressionHelper.DecompressIOCompressedFile(fileOnServerInfo, targetDirectoryInfo, deleteExisting: true);

            var allDecompressedFiles = Directory.EnumerateFiles(targetDirectoryFullName, "*").Select(s => new FileInfo(s));

            if (needDebugInfo)
            {
                Tools.LogInfo(string.Format("decompression contained the following files: " + string.Join(", ", allDecompressedFiles.Select(a => a.FullName))));
            }

            foreach (var fileFullname in Directory.EnumerateFiles(targetDirectoryFullName, "*.xml", SearchOption.AllDirectories))
            {
                this.ImportMainXmlNoService(fileFullname, tableOption, allDecompressedFiles, needDebugInfo);
            }
        }

        /// <summary>
        /// Export broker and all its related tables according to the options user chooses.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        private void Click_ExportBrokerXmlOld(object sender, EventArgs e)
        {
            if (!this.CheckIfCorrectProductionSite())
            {
                return;
            }

            HttpContext.Current.Server.ScriptTimeout = ConstStage.JustinBrokerExportTimeOutInSeconds;

            IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer;
            string filename = this.GetMainXmlFile_BySp(out fileDBKeysForDirectTransfer);

            if (filename == null)
            {
                return;
            }

            if (!this.BrokerExportIncludeFiledbCheckBoxId.Checked)
            {
                fileDBKeysForDirectTransfer = null;
            }

            this.CompressAndExport(filename, MIMEContentType.XML, ExportType.Broker, fileDBKeysForDirectTransfer);
        }

        /// <summary>
        /// Export broker and all its related tables according to the options user chooses.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        private void Click_ExportBrokerXmlNew(object sender, EventArgs e)
        {
            var exportOptions = new BrokerExportOptions()
            {
                Compress = this.CompressBrokerExport.Checked,
                IncludeFileDbEntries = this.BrokerExportIncludeFiledbCheckBoxId.Checked,
                ExportType = ReplicationExportType.Broker,
                BrokerId = this.BrokerId,
                LogDebugInfo = this.BrokerExport_ShowDebugInfo.Checked,
                SpecificExportType = this.m_MainExport_RadioOption.SelectedValue,
                BranchNameToGetUsers = this.m_MainExport_RadioOption.SelectedValue == "ONE_BRANCH_USERS" ? this.m_branchNameInputBox.Value : string.Empty,
                SelectedDB = this.CustomExport_RadioOption.SelectedValue,
            };

            var result = BrokerReplicator.ExportBrokerXml(PrincipalFactory.CurrentPrincipal, exportOptions);
            
            if (result.Succeeded)
            {
                this.ExportFile(TempFileUtils.Name2Path(result.TempFileName), result.ContentType);
                return;
            }

            Response.Write(this.GetAlertForMessage("One or more error(s) occurred in your broker export.  Please check the error section."));
            this.UploadMainXmlStatusLabel.Text = "The file was not exported. Please correct the error(s) and try again.";
            this.UploadBrokerXmlErrorMessages.InnerHtml = string.Empty;
            this.UploadBrokerXmlErrorMessages.InnerHtml += string.Join("<br/>", result.ErrorMessages.Select(s => AspxTools.HtmlString(s)));
            this.UploadBrokerXmlErrorMessages.InnerHtml += "========= warnings ===========<br/>";
            this.UploadBrokerXmlErrorMessages.InnerHtml += string.Join("<br/>", result.WarningMessages.Select(s => AspxTools.HtmlString(s)));
        }

        /// <summary>
        /// Determines if the current internal user has the requested permission.
        /// </summary>
        /// <param name="permission">The permission the user has or does not have.</param>
        /// <returns>True iff the current internal user has the requested permission.</returns>
        private bool UserHas(E_InternalUserPermissions permission)
        {
            InternalUserPrincipal principal = this.CurrentPrincipal;
            InternalUserPermissions permissions = new InternalUserPermissions();
            permissions.Opts = principal.Permissions;
            return permissions.Can(permission);
        }

        /// <summary>
        /// Updates the text that displays the md5 of the main whitelist.
        /// </summary>
        private void UpdateWhiteListHashText()
        {
            var mainWhitelistFile = new FileInfo(Tools.GetServerMapPath(BrokerReplicate.SenderWhitelistMapAddr));
            if (mainWhitelistFile.Exists)
            {
                this.MainWhitelistLastModifiedDate.Text = mainWhitelistFile.LastWriteTime.ToString("dddd, MMMM dd, yyyy HH:mm:ss PST");
            }
            else
            {
                this.MainWhitelistLastModifiedDate.Text = "No whitelist found.";
            }
        }

        /// <summary>
        /// Updates the text that displays the latest modified date of the implicit identity key file .
        /// </summary>
        private void UpdateImplicitKeysLatestText()
        {
            // tdsk: DRY this relative to above.
            var file = new FileInfo(Tools.GetServerMapPath(BrokerReplicate.ImplicitIdentityKeyFileAddress));
            if (file.Exists)
            {
                this.ImplicitIdentityKeysLastModifiedDate.Text = file.LastWriteTime.ToString("dddd, MMMM dd, yyyy HH:mm:ss PST");
            }
            else
            {
                this.ImplicitIdentityKeysLastModifiedDate.Text = "No implicit identity keys file found.";
            }
        }

        /// <summary>
        /// Gets a more meaningful name for the file to be exported.
        /// </summary>
        /// <param name="exportType">The type of export we are running.</param>
        /// <returns>A more meaningful string for the filename than a random string.</returns>
        private string GetMeaningfulFileName(ExportType exportType)
        {
            string prefix = null;
            switch (exportType)
            {
                case ExportType.Broker:
                    prefix = "brx";
                    break;
                case ExportType.Custom:
                    prefix = "cux";
                    break;
                default:
                    throw new UnhandledEnumException(exportType);
            }

            var nameParts = new string[]
            {
                prefix,
                this.GetStageName(),
                this.GetWhitelistName(),
                this.GetBrokerName(),
                this.GetNameFromOptions(exportType),
                this.GetDateAsFileName() 
            };

            return string.Join("_", nameParts);
        }

        /// <summary>
        /// Gets the current date in string format for files and folders.
        /// </summary>
        /// <returns>The current date in string format for files and folders.</returns>
        private string GetDateAsFileName()
        {
            return DateTime.Now.ToString("yy-MM-dd-HH-mm");
        }
        
        /// <summary>
        /// Gets a name from some of the options for the particular export.
        /// </summary>
        /// <param name="exportType">The type of export we are running.</param>
        /// <returns>A name from some of the options for the particular export.</returns>
        private string GetNameFromOptions(ExportType exportType)
        {
            switch (exportType)
            {
                case ExportType.Broker:
                    return this.GetNameFromBrokerRadioOptions() + "-" + this.GetNameFromBrokerIncludeFile();
                case ExportType.Custom:
                    return this.GetDBName() + "-" + this.GetNameFromCustomIncludeFile();
                default:
                    throw new UnhandledEnumException(exportType);
            }
        }

        /// <summary>
        /// Gets a name for the db we are using when using custom export.
        /// </summary>
        /// <returns>A name for the db we are using when using custom export.</returns>
        private string GetDBName()
        {
            switch (CustomExport_RadioOption.SelectedValue)
            {
                case "MAIN_DB":
                    return "main";
                case "RATE_SHEET":
                    return "rs";
                default:
                    throw new ArgumentException("CustomExport_RadioOption.SelectedValue was " + CustomExport_RadioOption.SelectedValue);
            }
        }

        /// <summary>
        /// Gets a name for whether or not we are including files in the custom export.
        /// </summary>
        /// <returns>A name for whether or not we are including files in the custom export.</returns>
        private string GetNameFromCustomIncludeFile()
        {
            return this.CustomExportIncludeFiledbCheckBoxId.Checked ? "if" : "nf";
        }

        /// <summary>
        /// Gets a name for whether or not we are including files in the broker export.
        /// </summary>
        /// <returns>A name for whether or not we are including files in the broker export.</returns>
        private string GetNameFromBrokerIncludeFile()
        {
            return this.BrokerExportIncludeFiledbCheckBoxId.Checked ? "if" : "nf";
        }

        /// <summary>
        /// Gets a name for the radio options associated with the broker export.
        /// </summary>
        /// <returns>A name for the radio options associated with the broker export.</returns>
        private string GetNameFromBrokerRadioOptions()
        {
            switch (this.m_MainExport_RadioOption.SelectedValue)
            {
                case "MAIN_DB_PRICING":
                    return "pricing";
                case "NO_USER":
                    return "nousr";
                case "ALL_BRANCH_USERS":
                    return "allusrs";
                case "LOAN_TEMPLATES":
                    return "lts";
                case "SYS_TABLES":
                    return "sys";
                case "RATE_SHEET":
                    return "rs";
                case "ONE_BRANCH_USERS":
                    return "1br-usrs";
                default:
                    throw new ArgumentException(this.m_MainExport_RadioOption.SelectedValue);
            }
        }

        /// <summary>
        /// Gets a name for the broker (the customer code), or none if no broker.
        /// </summary>
        /// <returns>A name for the broker.</returns>
        private string GetBrokerName()
        {
            if (this.BrokerId != Guid.Empty)
            {
                var broker = BrokerDB.RetrieveById(this.BrokerId);
                return broker.CustomerCode;
            }
            else
            {
                return "none";
            }
        }

        /// <summary>
        /// Gets a name associated with the current whitelist file.
        /// </summary>
        /// <returns>A name associated with the current whitelist file.</returns>
        private string GetWhitelistName()
        {
            // at somepoint might be nice to replace this with a version / checksum.
            var whiteListFile = new FileInfo(Tools.GetServerMapPath(BrokerReplicate.SenderWhitelistMapAddr));
            var lastUpdateTime = whiteListFile.LastWriteTime;
            return "wl" + lastUpdateTime.ToString("MM-dd-HH-mm");
        }

        /// <summary>
        /// Gets the name associated with the stage.
        /// </summary>
        /// <returns>The name associated with the stage.</returns>
        private string GetStageName()
        {
            switch (ConstAppDavid.CurrentServerLocation)
            {
                case ServerLocation.Production:
                    return "loauth";
                case ServerLocation.Beta:
                    return "beta";
                case ServerLocation.Alpha:
                    return "alpha";
                case ServerLocation.Development:
                case ServerLocation.LocalHost:
                    return "dev";
                case ServerLocation.Demo:
                    return "demo";
                case ServerLocation.DevCopy:
                    return "devcopy";
                default:
                    throw new UnhandledEnumException(ConstAppDavid.CurrentServerLocation);
            }
        }

        /// <summary>
        /// Check if the current url is secure site. If so, give warning because the secure site's white list address is not up-to-date.
        /// </summary>
        /// <returns>If is secure production site, return false.</returns>
        private bool CheckIfCorrectProductionSite()
        {
            string stageSiteSourceName = ConstAppDavid.CurrentServerLocation.ToString();
            if (stageSiteSourceName == "Production")
            {
                string loauthPattern = ConstStage.JustinLoauthUrlPatternChecking;
                string[] patterns = (loauthPattern == string.Empty) ? new string[] { } : loauthPattern.Split(';');
                bool isLoauthPattern = false;
                string url = HttpContext.Current.Request.Url.Host.ToLower();
                foreach (string pattern in patterns)
                {
                    if (url.StartsWith(pattern))
                    {
                        isLoauthPattern = true;
                    }
                }

                if (isLoauthPattern || HttpContext.Current.Request.Url.Host.ToLower().StartsWith("loauth"))
                {
                }
                else
                {
                    string msg = "Please do not export production data from secure url but from loauth url.";
                    string error = this.GetAlertForMessage(msg);
                    Response.Write(error);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Creates an alert string from the message without checking the message for Javascript safety.
        /// </summary>
        /// <param name="message">The message to be alerted.</param>
        /// <returns>A string for a script that will alert with the provided message.</returns>
        private string GetAlertForMessage(string message)
        {
            return $@"<script language='javascript'>alert({AspxTools.JsString(message)});</script>";
        }

        /// <summary>
        /// A small collection of mime content type string for ease of export.
        /// </summary>
        internal static class MIMEContentType
        {
            /// <summary>
            /// Plain text.
            /// </summary>
            internal const string Text = "text/plain";

            /// <summary>
            /// Textual CSV.
            /// </summary>
            internal const string CSV = "text/csv";

            /// <summary>
            /// Textual XML.
            /// </summary>
            internal const string XML = "text/xml";

            /// <summary>
            /// Zipped or compressed file.
            /// </summary>
            internal const string ZIP = "application/zip, application/octet-stream";
        }
    }
}
