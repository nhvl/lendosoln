using System;
using System.Linq;
using System.Threading;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
	/// <summary>
	/// Summary description for BrokerList.
	/// </summary>
	public partial class BrokerList : LendersOffice.Admin.SecuredAdminPage
    {
        #region Permission
        static readonly E_InternalUserPermissions[] _brokerListPermissions = new[] { E_InternalUserPermissions.ViewBrokers };

        /// <summary>
        /// Return this page's required permissions set.  This
        /// test occurs with each page initialization.
        /// </summary>
        /// <returns>
        /// Array of required permissions.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return _brokerListPermissions;
        }
        #endregion

        #region Web service
        /// <summary>
        /// Search broker
        /// </summary>
        /// <returns>Search result (Broker[]) or Error message (string)</returns>
        [WebMethod]
        public static object Search(string company, bool? isActive, string customerCode, string duUserId, string brokerId, BrokerSuiteType? suiteType)
        {
            if (SecuredAdminPageEx.IsMissingPermission(_brokerListPermissions)) { return null; }

            Guid gBrokerId = Guid.Empty;
            {
                brokerId = brokerId.TrimWhitespaceAndBOM();
                if (!string.IsNullOrEmpty(brokerId))
                {
                    try { gBrokerId = new Guid(brokerId.Replace("'", "''").TrimWhitespaceAndBOM()); }
                    catch (FormatException) { return ErrorMessage_BrokerID_Invalid; }
                }
            }

            return BrokerDbSearch.Search(company, duUserId, isActive, customerCode, gBrokerId, suiteType)
                .Select(b => new
                {
                    b.BrokerNm,
                    b.Notes,
                    b.BrokerId,
                    b.BrokerAddress,
                    b.BrokerPhone,
                    b.BrokerFax,
                    b.CustomerCode,
                    b.CreditMornetPlusUserId,
                    b.IsActive,
                    b.SuiteType
                });
        }
        const string ErrorMessage_BrokerID_Invalid = "BrokerID is not a valid GUID";
        #endregion

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.Broker.BrokerList.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        /// <summary>
        /// Initialize page.
        /// </summary>
        protected void PageLoad(object sender, System.EventArgs a)
        {
            RegisterResources();

            // We update the data grid with every postback.  The
            // current query fields will determine whether we filter
            // the results or return everything.  Filtering is done
            // on the server side within the stored procedure.            
        }

        private string ErrorMessage
        {
            // Access member.
            set
            {
                if (value.EndsWith(".") == false)
                {
                    ClientScript.RegisterHiddenField("m_ErrorMessage", value + ".");
                }
                else
                {
                    ClientScript.RegisterHiddenField("m_ErrorMessage", value);
                }
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
