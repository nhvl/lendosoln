﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchUpdateOCStateLicenses.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.BatchUpdateOCStateLicenses" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="ml" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <script language="javascript">
        function _init() {
            resize(500, 400);
        }
    </script>
    <h4 class="page-header">Batch Update OC State Licenses</h4>
    <form id="form1" method="post" runat="server">
        <table cellpadding="3" cellspacing="2" border="0">
            <tr>
                <td>
                    <table>
                        <tr>
			                <td>
			                    Pick a .csv file with the OC state licenses
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <input id="m_File" type="file" runat="server" autocomplete="off" title="Browse for csv file to upload" />
								<asp:Button id="m_Upload" runat="server" Text="Upload" OnClick="UploadClick"></asp:Button>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <asp:TextBox ID="m_errorData" TextMode="MultiLine" Width="420" Rows="6" runat="server"></asp:TextBox>
			                </td>
			            </tr>
			            <tr>
			                <td style="font-weight:bold">
			                    Instructions:
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    Saving Errors may occur and will be listed at the bottom of the CSV result file <br /><br />
			                    
			                    CSV file format<br />
			                    Column 1 - Company Name (No quotes around names with commas)<br />
                                Column 2 - Company ID<br />
                                Column 3 - License #<br />
                                Column 4 - State (2-letter)<br />
                                Column 5 - Expiration Date (mm/dd/yyyy) <br />
                                Note: First row in the CSV file is assumed to be the header row and is ignored. <br />
			                    Note: Only the company name should have commas
			                </td>
			            </tr>
                    </table>
                </td>
            </tr>
        </table>
        <ml:CModalDlg id="m_ModalDlg" runat="server"></ml:CModalDlg>
    </form>
</body>
</html>
