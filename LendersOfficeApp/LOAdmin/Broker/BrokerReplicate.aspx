﻿<%@ Page Language="C#" Debug="true" AutoEventWireup="true" CodeBehind="BrokerReplicate.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.BrokerReplicate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Table Replicate Tool</title>
    <style rel="stylesheet" type="text/css" media="screen">
        legend {
            font-size:  140%;
            font-weight:  bold;
        }
        .error
        {
            color:red;
        }
        .warning
        {
            color:yellow
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
        <div>
            <div id="Div2" class="MainRightHeader"><font size="5" color="red">Warning: This is a sensitive tool. Please notify Thinh before you try to use it. </font></div> 
            <div id="Header" class="MainRightHeader"><font size="5">Table Replicate Tool </font></div> 
            <br />
            
            <div id="Main">    		    
    		    <fieldset>
                    <ml:EncodedLabel runat="server" ID="Status"></ml:EncodedLabel>
                    <span class="error"><ml:PassthroughLiteral runat="server" ID="ErrorMessages"></ml:PassthroughLiteral></span>
                    <span class="warning"><ml:PassthroughLiteral runat="server" ID="WarningMessages"></ml:PassthroughLiteral></span>
    		    </fieldset>
    		    <fieldset>
                    <legend>Whitelist Section</legend>
                    <asp:Button ID="CheckWhitelistStatusID" Text="Check Missing Tables && Columns" onclick="Click_CheckWhitelistStatus" runat="server">
                    </asp:Button>
                    
                    <asp:Button ID="ExportMainWhiteListID" Text="Export Main White List" onclick="ExportMainWhiteList" runat="server">
                    </asp:Button>
                    
                    <asp:Button ID="ExportMainDefaultWhiteListID" Text="Export Full (nonsecure) White List" onclick="ExportMainDefaultWhiteList" runat="server">
                    </asp:Button>
                    (Main's Last Modified Date: <ml:EncodedLabel ID="MainWhitelistLastModifiedDate" runat="server"></ml:EncodedLabel>)
                    
                    <div>Select one rule file to upload(Field Name, Is Protected, Default Value):</div>
                    <asp:FileUpload id="FileUpload_MainWhiteList" runat="server">
                    </asp:FileUpload>
                    
                    <br/><br/>
                    
                    <asp:Button id="UploadMainWhitelistID" Text="Import White List" OnClick="Click_UploadMainWhitelist" runat="server">
                    </asp:Button>
                    <br/>
                    <ml:EncodedLabel id="UploadMainWhiteStatusLabel" runat="server"></ml:EncodedLabel>
                </fieldset>

                <fieldset>
                    <legend>Implicit Identity Key Reference Section</legend>
                    <asp:Button ID="buttonToExportImplicitIdentityKeysFile" Text="Export Implicit Keys File" onclick="Click_ExportImplicitIdentityKeysFile" runat="server">
                    </asp:Button>
                    <asp:FileUpload id="FileUpload_ImplicitIdentityKeys" runat="server">
                    </asp:FileUpload>
                    (Implicit Identity Key File's Last Modified Date: <ml:EncodedLabel ID="ImplicitIdentityKeysLastModifiedDate" runat="server"></ml:EncodedLabel>)
                    
                    <br/><br/>
                    
                    <asp:Button id="buttonToUploadImplicitIdentityKeys" Text="Import Implicit Keys File" OnClick="Click_UploadImplicitIdentityKeys" runat="server">
                    </asp:Button>
                    <br/>
                    <ml:EncodedLabel id="ImplicitIdentityKeysStatusLabel" runat="server"></ml:EncodedLabel>
                </fieldset>

                <asp:PlaceHolder runat="server" ID="BrokerExportPlaceHolder">
                <br /><br />
                <!--<fieldset>
                <legend>Test FileDB Export(Dangerous, will be deleted afterwards.)</legend>
                Loan Id: <input type="text" id="InputFiledb_LoanId" size="55" runat="server" /><br />
                Filedb Id: <input type="text" id="InputFiledb_FiledbKey" size="55" runat="server" /><br />
                <asp:Button ID="ExportFileDbContentID" Text="Export FileDB" OnClick="Click_ExportFiledbByKey" runat="server" />
                <asp:CheckBox ID="ExportFileDbSwitchOldNewCheckBox" Text="Use New Method" runat="server" />
                </fieldset>
                
                <br /><br />
                
                
                <fieldset>
                    <legend>Test FileDB Import</legend>
                    Loan Id: <input type="text" id="importFiledb_LoanId" size="55" runat="server" /><br />
                    Filedb Id: <input type="text" id="importFiledb_FiledbKey" size="55" runat="server" /><br />
                    <div>Select one table xml file to upload:</div>
                    <asp:FileUpload id="FileUpload_ImportFiledb" runat="server">
                    </asp:FileUpload>
                    <br/><br/>
                    <asp:Button id="ImportFiledbTestId" Text="Import Filedb Content" OnClick="UploadFiledbContent_Click" runat="server">
                    </asp:Button>
                 </fieldset>
                 
                 <br /><br />-->
                
                <fieldset>
                    
                    <legend>Broker Export</legend>
		            <asp:RadioButtonList name="test" ID="m_MainExport_RadioOption"  runat="server" RepeatDirection="Vertical">
                        <asp:ListItem Text="Export Pricing related settings (Price groups…) in main database" Value="MAIN_DB_PRICING" OnClick="f_onClickExportToolRadio(this);"></asp:ListItem>
                        <asp:ListItem Text="No user" Value="NO_USER" OnClick="f_onClickExportToolRadio(this);" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="All users of all branches" Value="ALL_BRANCH_USERS" OnClick="f_onClickExportToolRadio(this);"></asp:ListItem>
                        <asp:ListItem Text="Export Loan Templates Only" Value="LOAN_TEMPLATES" OnClick="f_onClickExportToolRadio(this);"></asp:ListItem>
                        <asp:ListItem Text="Export System Tables" Value="SYS_TABLES" OnClick="f_onClickExportToolRadio(this);"></asp:ListItem>
                        <asp:ListItem Text="Export Ratesheet Data" Value="RATE_SHEET" OnClick="f_onClickExportToolRadio(this);"></asp:ListItem>
                        <asp:ListItem Value="ONE_BRANCH_USERS" OnClick="f_onClickExportToolRadio(this);">Include all users of this Branch's Name&nbsp;&nbsp;</asp:ListItem>
                    </asp:RadioButtonList>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="m_branchNameInputBox" readonly="readonly" size = "45" runat="server" />
        			<br />
        			<asp:CheckBox ID="BrokerExportIncludeFiledbCheckBoxId" Text = "Include filedb content" Checked="true" runat="server" />
        			<br />
        			<br />
        			Note: User passwords are randomly generated but are never overwritten at import.
        			<br />
        			<br />
                    <asp:CheckBox runat="server" ID="CompressBrokerExport" Text="Compress Export?" Checked="true" />
                    <br />
                    <br />
    			    <asp:Button ID="ExportAllXmlRelatedToOptionID" Text="Export Xml" OnClick="Click_ExportBrokerXml" runat="server">
                    </asp:Button>
                    
                    <asp:CheckBox ID="BrokerExport_ShowDebugInfo" Text="Show Debug Info" runat="server" />
                    
                </fieldset>
                </asp:PlaceHolder>

                <asp:PlaceHolder runat="server" ID="NonBrokerExportPlaceHolder" Visible="false">
                    <br />
                    <span style="color:red;font-size:medium">
                        Did you want to export a specific broker?  Try finding a specific broker in loadmin, then go to their page to edit, then hit "broker replicate" at the top right! :-)
                    </span>
                </asp:PlaceHolder>
                
                <br /><br />
                
                <fieldset>
                    <legend>Loan Export</legend>
                    <div>Loan IDs separated by commas.  Spacing doesn't matter.</div>
                    <asp:TextBox runat="server" ID="LoanIDsForExport" Width="99%" TextMode="MultiLine" Height="100px"></asp:TextBox>
                    <input type="button" value="Send to Custom Export" onclick="f_sendToCustomExport()"/>
                </fieldset>

                <br /><br />

                <fieldset>
                <legend>Custom Export</legend>
                    <!--<div><font size="4">CUSTOM EXPORT</font></div><br />-->
		            <div>Table entries(one entry per line, ending with ';'; separate by tab or pipe |, not space; Case Insensitive). Example: BROKER[tab]BrokerId[tab]00000000-0000-0000-0000-000000000000;</div>
			        <!--<div>Warning: If one entry contains rows from different databases, try to split them and make sure each entry only contains rows from one database.</div>-->
			        <ASP:TextBox id="m_PatchExportTextBox" runat="server" Width="99%" TextMode="MultiLine" Height="250px" Wrap = "false" >
                    </ASP:TextBox>
                    <br />
                    <asp:RadioButtonList name="test" ID="CustomExport_RadioOption"  runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Main Database" Value="MAIN_DB" OnClick="f_onClickExportToolRadio(this);" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="RateSheet Database" Value="RATE_SHEET" OnClick="f_onClickExportToolRadio(this);" ></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:CheckBox ID="CustomExportIncludeFiledbCheckBoxId" Text = "Include filedb content" Checked="true" runat="server" /><br />
                    <asp:CheckBox ID="CompressCustomExport" Text="Compress Export?" Checked="true" runat="server" /> <br />
                    <asp:CheckBox ID="UseOnlyThisBrokersDb" Text="Use this brokers db only?" Checked="true" runat="server" />
                    <br />
                    <asp:Button ID="Button2" Text="Show Related* Tables" OnClick="Click_PreviewRelatedEntries" runat="server">
                    </asp:Button>
                    <asp:Button ID="Button3" Text="Export Related* Table" OnClick="Click_ExportRelatedEntriesXml" runat="server">
                    </asp:Button>
                    <asp:Button ID="Button4" Text="Export Only Input Table" OnClick="Click_ExportOnlyInputEntriesXml" runat="server">
                    </asp:Button>
                    
                    <asp:CheckBox ID="CustomExport_ShowDebugInfo" Text="Show Debug Info" runat="server" />
                    <br />
                    <span style="color:green">
                        *Related = tables your ^table^ relies on (foreign keys to), and the tables they rely on, ad nauseum. It also includes extras like loan->loan_user_assignments see TRT.PushQueryResult.<br />
                        Ex: Group depends on broker (fk's to it), so exporting group causes export of broker, which depends on many tables which will also get exported, etc.<br />
                        Ex: Nothing foreign keys to Group_X_Employee, but it is related because it was manually added as related to "Employee" table. <br />
                        (select * from sys.foreign_keys where referenced_object_id = object_id('group_x_employee') -- gives nothing.)<br />                        
                    </span>
                </fieldset>
                <br /><br />
                
        
                
                <fieldset>
                    <asp:PlaceHolder runat="server" ID="XmlImportPlaceHolder" Visible="false">
                    <legend>Xml Import</legend>
                    <div>Select one table xml (OR ZIP!) file to upload:</div>
                    <asp:FileUpload id="FileUpload_MainXml" runat="server">
                    </asp:FileUpload>
                    <br/><br/>
                    
                    <asp:Button id="UploadMainXmlID" Text="Import Xml Table File" OnClick="Click_UploadBrokerXml" runat="server">
                    </asp:Button>
                    
                    <asp:CheckBox ID="NeedDebugInfoCheckBoxId" Text="Show Debug Info" runat="server" />
                    <asp:CheckBox ID="DefaultExternalBrokerFields" Text="Default External Broker Fields?" runat="server" />
                    <br/ >
                    <asp:CheckBox ID="CombineConfigReleaseCheckBoxId" Text="Combine System Workflow(if xml contains one.)" runat="server" />
                    
                    <br />
                    </asp:PlaceHolder>
                    <ml:EncodedLabel id="UploadMainXmlStatusLabel" runat="server"></ml:EncodedLabel>
                    <div runat="server" id="UploadBrokerXmlErrorMessages" style="color:red"></div>
                    
                </fieldset>
                
                
                <fieldset>
                    <div>Select one xml file to get its compile code:</div>
                    <asp:FileUpload id="CompileCode_XmlFile" runat="server">
                    </asp:FileUpload>
                    <br/><br/>
                    
                    <asp:Button id="GetCompileCodeButton" Text="Get Compile Code of Workflow" OnClick="Click_GetCompileCode" runat="server">
                    </asp:Button>
                    Result:<br/><ASP:TextBox id="CompileCodeResultBox" runat="server" Width="99%" TextMode="MultiLine" Height="40px" Wrap = "false" >
                    </ASP:TextBox>                
                    <br />
                </fieldset>
            </div>
            
            
        <!--<div id="Div2" class="MainRightHeader">Debugging Tool </div>    
        <div>-->
            
            
        </div>    
    
    </form>
    
    
    <br/><br/>
    <div id="Div1" class="MainRightHeader" >FAQ </div>
    <div id = "loan_tool_faq">
        <p>
        1. Details about checkbox for export.<br />&nbsp;&nbsp;&nbsp; other tables related to brokerId: CREDIT_REPORT_ACCOUNT_PROXY, SERVICE_COMPANY <br /> &nbsp;&nbsp;&nbsp; other tables unrelated to brokerId: DOCUMENT_VENDOR_CONFIGURATION, APPRAISAL_VENDOR_CONFIGURATION, DATA_RETRIEVAL_PARTNER_BROKER, IRS_4506T_VENDOR_CONFIGURATION <br /><br />    
        2. The difference between loan-overwrite tool and loan-replicate tool.<br />&nbsp;&nbsp;&nbsp; The loan-overwrite tool will only overwrite the current loan data when user try to import the xml into current loan that s/he opened. It will not touch foreign key or primary key.<br />&nbsp;&nbsp;&nbsp; The loan-replicate tool is kind of a database table transportation tool. For example, user can transport a loan in production server, together with all its environment into test server. This includes the user account, broker and all related table information that support this loan to run. Even if the broker or user account doesn't exist in test server, the program will create one that is exactly the same as the one in production server. The target of this tool is to mimic the loan environment in production server as much as it could for testing purpose.<br />&nbsp;&nbsp; &nbsp;<br />
        3. New columns need to be reviewed. Please export white list and check them.<br />&nbsp;&nbsp;&nbsp; To solve this, you can export the whitelist first. It is a csv file and can be open by excel. Some fields at the top rows has blank column "export-replicate" and "export-overwrite". If you don't know, you can just set both to be "Yes" and remember to add "test-SetAsYes" at note column of the same row. This will help us to know that this column still need to be reviewed.<br /><br />
        4. Explain the value of "export-replicate" and "export-overwrite".<br />&nbsp;&nbsp;&nbsp; "Yes" means this value can be exported and seen by others and can be imported.<br />&nbsp;&nbsp;&nbsp; "No" means this value need to be protected. Use also need to fill the "substitute-..." column at right side, next to the "export-..."&nbsp; column with the substitute value. This value will be seen during export. In loan-replicate tool, this value will be inserted if there is no value in the original database with the substitute value. In loan-overwrite tool, the loan already exists and the program has no need to enforce the value so the program will ignore the protected fields and import other fields. This is suitable for fields like password.<br />&nbsp;&nbsp;&nbsp;&nbsp; "NoExportNoImport" means this field should be exported and surely will not be imported in both loan-overwrite tool and loan-replicate tool. Fields like 'ssn' is suitable under this protection. It requires the field to be nullable before setting it to this value since the program will have to ignore it during insertion or update.<br /><br />
        5. Why two export and substitute columns?<br />&nbsp;&nbsp;&nbsp; The loan-overwrite tool and loan-replicate tool share the same white list.<br />&nbsp;&nbsp;&nbsp; As you can see, there are two sets of export-substitute columns: ( export-replicate, substitution-replicate ) and (export-overwrite, substitution-overwrite). The former one is for loan-replicate tool which export the loan from current server and import it into remote one, together with all the related data to the loan table, such as user account, broker. The latter one is for loan-overwrite which will overwrite the current loan you import the xml into.<br /><br />
        6. Explain the nuke fields.<br />&nbsp;&nbsp;&nbsp; When user use loan-overwrite tool and try to import xml into a loan at production server, this restriction will be activated.<br />&nbsp;&nbsp;&nbsp; It requires user to set certain fields at production server database to be certain value to enable the import process for safety purpose. When user import loan at other server like test/dev server, this restriction is waived.<br />&nbsp;&nbsp;&nbsp; Set primary borrower's first and last name to be "nuke": that is "aBFirstNm" and "aBLastNm" at APPLICATION_A.<br />&nbsp;&nbsp;&nbsp; Set loan name to be start with "@_@ NUKE THIS LOAN": that is "sLNm" in LOAN_FILE_E.&nbsp; Since loan name is required to be unique, you cannot just overwrite the whole value with "@_@ NUKE THIS LOAN".<br />Instead, add "@_@ NUKE THIS LOAN" in front of the original loan name. Consider that there is length limit over loan name as 36 characters, you can delete some original loan name characters if it is too long. But just make sure it start with "@_@ NUKE THIS LOAN" and make sure the whole thing is unique.<br /><br />
        7. Error of "The process cannot access the file 'c:\LendOSoln\StandAlonePricingEngine\pml_shared\findNextLog.txt' because it is being used by another process. "<br />&nbsp;&nbsp;&nbsp; When seeing this error, it is probably because somebody else is also using the tool and it causes conflict. Try to refresh the page and do it again. <br />&nbsp; <br /><br />
        </p>
    </div>
    
</body>
<script type="text/javascript">
    function f_onClickExportToolRadio(e) {
        if (e.value == "ONE_BRANCH_USERS") {
            document.getElementById("m_branchNameInputBox").readOnly = false;
            document.getElementById("m_branchNameInputBox").style.backgroundColor = "white";
        } else {
            document.getElementById("m_branchNameInputBox").readOnly = true;
            document.getElementById("m_branchNameInputBox").style.backgroundColor = "silver";
        }
    }

    function f_sendToCustomExport()
    {
        var loanIDsInput = document.getElementById('LoanIDsForExport');
        var customExportInput = document.getElementById('m_PatchExportTextBox');
        var loanIDsWithoutSpaces = loanIDsInput.value.replace(/\s/g, '');

        if (loanIDsWithoutSpaces.length === 0) {
            alert('Congratulations you successfully exported nothing.  -_-');
            return false;
        }

        var customExportText = customExportInput.value;
        var customExportTextWithoutSpaces = customExportText.replace(/\s/g, '')
        if (customExportTextWithoutSpaces.length !== 0) {
            var overwrrite = confirm('Do you want to overwrrite the existing custom export?');
            if (!overwrrite) {
                return false;
            }
        }
        
        var loanIDs = loanIDsWithoutSpaces.split(',');
        var newText = '';
        var numLoanIDs = loanIDs.length;
        var numTables = LoanFileNamesForExport.length;
        var delimiter = '\t';
        var returnString = '\r\n';
        var endLine = ';' + returnString;
        var existingLoanIDs = {};
        var duplicateLoanIDs = [];
        var hasAnyDuplicates = false;
        for (var loanIndex = 0; loanIndex < numLoanIDs; loanIndex++) {
            var loanID = loanIDs[loanIndex].toUpperCase();
            if (loanID === '') {
                continue;
            }
            
            if (existingLoanIDs[loanID] === 1) {
                duplicateLoanIDs.push(loanID)
                hasAnyDuplicates = true;
                continue;
            }
            else {
                existingLoanIDs[loanID] = 1;
            }

            for (var tableIndex = 0; tableIndex < numTables; tableIndex++) {
                var tableName = LoanFileNamesForExport[tableIndex];
                newText += tableName + delimiter + 'sLId' + delimiter + loanID + endLine;
            }
        }

        if (hasAnyDuplicates === true) {            
            alert('Didnt export duplicates:' + duplicateLoanIDs);
        }
        customExportInput.value = newText;
        return false;
    }
</script>
</html>
