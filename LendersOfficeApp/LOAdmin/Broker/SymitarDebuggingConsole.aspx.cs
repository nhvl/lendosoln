﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Linq;
    using System.Web.Services;
    using System.Web.UI.WebControls;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.Symitar;

    /// <summary>
    /// A page to allow simple debugging inquiries into Symitar's database.
    /// </summary>
    public partial class SymitarDebuggingConsole : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Sends messages to Symitar.
        /// </summary>
        /// <param name="brokerId">The broker id of the lender with a valid configuration.  If null, a valid <paramref name="manualLenderConfig"/> is expected.</param>
        /// <param name="loanFileType">The loan file type to load the lender's config from.</param>
        /// <param name="manualLenderConfig">A manually entered lender config.  If not valid, <paramref name="brokerId"/> should point to a lender with a valid configuration.</param>
        /// <param name="messages">The messages to send to Symitar.</param>
        /// <returns>A complex object containing a list of messages and an error message.</returns>
        [WebMethod]
        public static object SendMessages(Guid? brokerId, DataAccess.E_sLoanFileT? loanFileType, string manualLenderConfig, string messages)
        {
            var messageExchanges = SymitarManager.SendDebugInquiries(GetConfig(brokerId, loanFileType, manualLenderConfig), messages);

            return new
            {
                ErrorMessage = messageExchanges.Count == 0 ? "No valid Inquiries were found." : null,
                Messages = messageExchanges.Select(exchange => new
                {
                    Request = exchange.Key,
                    Response = exchange.Value,
                    Success = System.Text.RegularExpressions.Regex.IsMatch(exchange.Value, "~K0(~|\n$|$)"),
                    Fields = exchange.Value.Split('~').Where(str => str.StartsWith("J") && str.Contains("=")).Select(str => str.Substring(1).Split(new[] { '=' }, 2)).ToList(),
                }).ToList()
            };
        }

        /// <summary>
        /// Creates the base of an inquiry from the information provided.
        /// </summary>
        /// <param name="brokerId">The broker id of the lender with a valid configuration.  If null, a valid <paramref name="manualLenderConfig"/> is expected.</param>
        /// <param name="loanFileType">The loan file type to load the lender's config from.</param>
        /// <param name="manualLenderConfig">A manually entered lender config.  If not valid, <paramref name="brokerId"/> should point to a lender with a valid configuration.</param>
        /// <param name="accountNumber">The account number to read from.  It should be all digits, and this parameter will be validated.</param>
        /// <param name="recordPath">The record path to read from.</param>
        /// <param name="fields">The list of fields to read values from.</param>
        /// <returns>A string representation of the message.</returns>
        [WebMethod]
        public static string CreateInquiryMessage(Guid? brokerId, DataAccess.E_sLoanFileT? loanFileType, string manualLenderConfig, string accountNumber, string recordPath, string[] fields)
        {
            return SymitarManager.CreateInquiryMessageString(GetConfig(brokerId, loanFileType, manualLenderConfig), accountNumber, recordPath, fields);
        }

        /// <summary>
        /// Handles the <c>Page_Init</c> event.
        /// </summary>
        /// <param name="sender">An object representing the object that raised the event.</param>
        /// <param name="e">An event object containing any event-specific information.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;

            var brokerId = RequestHelper.GetGuid("BrokerId", Guid.Empty);
            if (brokerId == Guid.Empty)
            {
                var customerCode = RequestHelper.GetSafeQueryString("CustomerCode");
                if (!string.IsNullOrEmpty(customerCode))
                {
                    brokerId = DataAccess.Tools.GetBrokerIdByCustomerCode(customerCode);
                }
            }

            this.RegisterJsGlobalVariables("BrokerId", (brokerId == Guid.Empty ? default(Guid?) : brokerId).ToString());

            var brokerDb = brokerId == Guid.Empty ? null : BrokerDB.RetrieveById(brokerId);
            this.RegisterJsStruct(
                "AvailableLoanFileTypes",
                new[] { DataAccess.E_sLoanFileT.Loan, DataAccess.E_sLoanFileT.Test }
                    .Where(fileType => brokerDb?.IsSymitarIntegrationEnabled(fileType) ?? false)
                    .Select(fileType => new { Name = fileType.ToString("G"), Value = fileType.ToString("D") }));
        }

        /// <summary>
        /// Resolves whether to read the config from the specified broker or from manually entered data.
        /// </summary>
        /// <param name="brokerId">The broker id of the lender to read configuration from.</param>
        /// <param name="loanFileType">The loan file type to load the lender's config from.</param>
        /// <param name="manualLenderConfig">The manual configuration to parse and use.</param>
        /// <returns>A lender configuration for Symitar, or null if either was incomplete.</returns>
        private static LenderConfiguration GetConfig(Guid? brokerId, DataAccess.E_sLoanFileT? loanFileType, string manualLenderConfig)
        {
            if (brokerId.HasValue && loanFileType.HasValue)
            {
                return BrokerDB.RetrieveById(brokerId.Value).GetEnabledSymitarLenderConfig(loanFileType.Value);
            }
            else
            {
                string name = "SymitarDebuggingConsole";
                return BrokerDB.ParseTemporaryOptionForSymitarConfig(manualLenderConfig, "SymitarIntegration", name)
                    ?? BrokerDB.ParseTemporaryOptionForSymitarConfig(manualLenderConfig, "SymitarTestIntegration", name);
            }
        }
    }
}