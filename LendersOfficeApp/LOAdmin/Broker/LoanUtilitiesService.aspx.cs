﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class LoanUtilitiesService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "UpdateInitialApr":
                    UpdateInitialApr();
                    break;
                case "UpdateLoanFileCache":
                    UpdateLoanFileCache();
                    break;
                case "LinkLoans":
                    LinkLoans();
                    break;
                case "LookupLoanIDFromCustomerCodeAndLoanNumber":
                    LookupLoanIDFromCustomerCodeAndLoanNumber();
                    break;
                case "UpdateCacheForMultipleLoans":
                    UpdateCacheForMultipleLoans();
                    break;
            }
        }

        private void UpdateInitialApr()
        {
            try
            {
                Guid LoanId = GetGuid("sLId");
                string sInitApr = GetString("sInitApr");

                CPageData dataLoan = new CFullAccessPageData(LoanId, new string[] { "sInitAPR" });
                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.AllowSaveWhileQP2Sandboxed = true;
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                dataLoan.sInitAPR_rep = sInitApr;
                dataLoan.Save();
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                SetResult("error", true);
            }
        }

        private void UpdateLoanFileCache()
        {
            try
            {
                Guid LoanId = GetGuid("sLId");
                Tools.UpdateCacheTable(LoanId, null);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                SetResult("error", true);
            }
        }

        private void LinkLoans()
        {
            try
            {
                Guid LoanId = GetGuid("sLId");
                Guid LoanIdToLink = GetGuid("LoanIdToLink", Guid.Empty);
                Boolean ScrapExistingLinkagesField = GetBool("ScrapExistingLinkagesField");

                if (LoanIdToLink == Guid.Empty)
                {
                    throw new CBaseException("Loan ID To Link must be a valid guid.", "Loan To Link must be a valid guid.");
                }

                Tools.LinkLoansForLOAdmin(LoanId, LoanIdToLink, ScrapExistingLinkagesField);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                SetResult("errorMessage", e.UserMessage);
            }
        }

        private void LookupLoanIDFromCustomerCodeAndLoanNumber()
        {
            string customerCode = this.GetString("CustomerCode");
            string[] loanNumbers = SerializationHelper.JsonNetDeserialize<string[]>(this.GetString("LoanNumbers", "[]"));

            Guid brokerId = Tools.GetBrokerIdByCustomerCode(customerCode);
            List<Guid> loanIds = new List<Guid>(loanNumbers.Length);
            List<string> missingLoanNumbers = new List<string>();
            foreach (string loanNumber in loanNumbers.Where(s => !string.IsNullOrEmpty(s)))
            {
                Guid loanId = Tools.GetLoanIdByLoanName(brokerId, loanNumber);
                if (loanId != Guid.Empty)
                {
                    loanIds.Add(loanId);
                }
                else
                {
                    missingLoanNumbers.Add(loanNumber);
                }
            }

            var result = new
            {
                LoanIDs = loanIds,
                Message = missingLoanNumbers.Any() ? "Unable to find '" + string.Join("'; '", missingLoanNumbers) + "'" : "Done"
            };

            this.SetResult("Result", SerializationHelper.JsonNetAnonymousSerialize(result));
        }

        private void UpdateCacheForMultipleLoans()
        {
            string[] loanIdStrings = SerializationHelper.JsonNetDeserialize<string[]>(this.GetString("LoanIDs", "[]"));

            List<string> invalidLoanIds = new List<string>();
            foreach (string loanIdString in loanIdStrings.Where(s => !string.IsNullOrEmpty(s)))
            {
                Guid? loanId = loanIdString.ToNullable<Guid>(Guid.TryParse);
                if (loanId.HasValue)
                {
                    Tools.UpdateCacheTable(loanId.Value, null);
                }
                else
                {
                    invalidLoanIds.Add(loanIdString);
                }
            }

            var result = new
            {
                Message = invalidLoanIds.Any() ? "Unable to parse '" + string.Join("'; '", invalidLoanIds) + "'" : "Done"
            };
            this.SetResult("Result", SerializationHelper.JsonNetAnonymousSerialize(result));
        }
    }
}