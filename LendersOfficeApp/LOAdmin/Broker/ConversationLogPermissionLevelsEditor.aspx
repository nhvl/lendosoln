﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConversationLogPermissionLevelsEditor.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.ConversationLogPermissionLevelsEditor" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title runat="server">Conversation Log Permission Levels Editor</title>
    <style type="text/css">
        tbody > tr:nth-child(odd)
        {
            font-size: 11px;
            color: black;
            font-family: Arial, Helvetica, sans-serif;
            background-color: white;
        }
        tbody > tr:nth-child(even)
        {
	        font-size: 11px;
	        font-family: Arial, Helvetica, sans-serif;
	        color: Black ;
	        background-color: #CCCCCC ;
        }
        a, img { outline: none; }
        a:hover, a:active, a:focus { outline: 0; border: 0; text-decoration: none;}
        a.moveArrow:link { text-decoration: none; }
        a img { border: 0; }
        a.moveArrow img { width: 10px; height: 10px; }
        .moveContainer { margin-left: 10px; }
        .addNewBtnSection
        {
            float: left;
            padding: 10px;
        }
        .windowBtnSection
        {
            margin: 10px;
        }
    </style>
</head>
<body ng-app="mainApp" ng-controller="permissionLevelsListCtrl">
    <form id="form1" runat="server">        
    <script type="text/javascript">
        var app = angular.module('mainApp', []);
        var windowById = {}; // need this in case user clicks edit twice, so we don't clobber the changes.

        function arrayOfObjectsToDictionary(array, keyGetter, valueGetter)
        {
            var result = array.reduce(function(result, item){
                result[keyGetter(item)] = valueGetter(item);
                return result;
            }, {});

            return result;
        }

        function idGetter(level)
        {
            return level.id;
        }

        jQuery(function ($) {
            resize(600, 600);

            app.factory('permissionLevelsSaveService', function ($http) {
                return {
                    save: function (orderedPermissionLevelIds, successMethod, failedMethod) {
                        //return the promise directly.
                        return $http.post(
                            'ConversationLogPermissionLevelsEditor.aspx/SavePermissionLevelsOrdering?brokerid=' + ML.brokerId, 
                            { 
                                orderedPermissionLevelIdsString: angular.toJson(orderedPermissionLevelIds) // instead of JSON.stringify to avoid $$hashObject props.
                            }
                            )
                            .then(function (result) {
                                      //resolve the promise as the data
                                    successMethod(result.data.d);
                                }, function (response) {
                                    failedMethod(response)
                                });
                    }
                }
            });

            app.controller('permissionLevelsListCtrl', function ($scope, permissionLevelsSaveService) {
                $scope.permissionLevels = angular.copy(permissionLevels);

                window.onbeforeunload = function(e) {
                    if(!angular.equals(permissionLevels, $scope.permissionLevels))
                    {
                        e.returnValue = 'You have unsaved changes.  Really close without saving?';
                        return e.returnValue;
                    }
                };

                $scope.openPermissionLevelEditor = function(index)
                {
                    var permissionLevel = {id:null};
                    if(typeof(index) !== 'undefined')
                    {
                        permissionLevel = $scope.permissionLevels[index];
                    }

                    if(typeof(windowById[permissionLevel.id]) !== 'undefined' && !windowById[permissionLevel.id].closed)
                    {
                        windowById[permissionLevel.id].focus();
                    }
                    else
                    {
                        var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("~/LOAdmin/Broker/ConversationLogPermissionLevelEditor.aspx"))%>;
                        var lastId = permissionLevel.id === null ? ML.brokerId.replace(/-/g, '') + 'NULL' : permissionLevel.id;
                        var windowNameParts = ['PL', ML.stageName, lastId];
                        var windowName = windowNameParts.join('');
                        var fullUrl = url + '?brokerid=' + ML.brokerId + '&permissionLevelId=' + permissionLevel.id;
                        var w = window.open(fullUrl, windowName, 'height=800,width=1050,menubar=no,scrollbars=yes,resizable=yes');
                        windowById[permissionLevel.id] = w;
                    }
                }

                $scope.movePermissionLevelUp = function(index){
                    if(index === 0)
                    {
                        // nothing to do, it's already at the top.
                        return;
                    }
                    
                    swapPermissionLevels(index, index-1);
                }

                $scope.movePermissionLevelDown = function(index){
                    var lastIndex = $scope.permissionLevels.length -1;
                    if(index === lastIndex)
                    {
                        // nothing to do, it's already at the bottom.
                        return;
                    }
                    
                    swapPermissionLevels(index, index+1);
                }

                $scope.movePermissionLevelToTop = function(index)
                {
                    if(index === 0)
                    {
                        return; // already at top.
                    }
                    var removedPermissionLevels = $scope.permissionLevels.splice(index, 1);
                    $scope.permissionLevels.splice(0, 0, removedPermissionLevels[0]);
                }

                $scope.movePermissionLevelToBottom = function(index)
                {
                    var lastIndex = $scope.permissionLevels.length -1;
                    if(index === lastIndex)
                    {
                        return; // already at bottom.
                    }
                    var removedPermissionLevels = $scope.permissionLevels.splice(index, 1);
                    $scope.permissionLevels.push(removedPermissionLevels[0]);
                }

                function swapPermissionLevels(index1, index2)
                {
                    var cat1 = $scope.permissionLevels[index1];
                    $scope.permissionLevels[index1] = $scope.permissionLevels[index2];
                    $scope.permissionLevels[index2] = cat1;
                }
                                
                $scope.okClick = function () {
                    var orderedPermissionLevelIds = $scope.permissionLevels.map(function(p){return p.id});
                    var originalOrderedPermissionLevelIds = permissionLevels.map(function(p){return p.id});
                    if(angular.equals(orderedPermissionLevelIds, originalOrderedPermissionLevelIds))
                    {
                        // just close since nothing has changed.
                        window.top.close();
                        return;
                    }

                    var $okBtn = $('#OkBtn');
                    $okBtn.attr('disabled', 'disabled');
                    permissionLevelsSaveService.save(
                        orderedPermissionLevelIds,
                        function (data) {
                            window.onbeforeunload = function(){};
                            window.top.close();
                        }, function (response) {
                            alert('failed to save: ' + response.data.Message);
                            $okBtn.removeAttr('disabled');
                        });
                };

                $scope.closeClick = function()
                {
                    window.top.close();
                }

                $scope.addOrUpdatePermissionLevel = function(newOrUpdatedPermissionLevel)
                {
                    // need to copy the object as it's coming from child window.
                    var copyOfNewOrUpdatedPermissionLevel = angular.copy(newOrUpdatedPermissionLevel);

                    // loop through and update if it's there.
                    for(var i = 0; i < $scope.permissionLevels.length; i++)
                    {
                        var level = $scope.permissionLevels[i];
                        if(copyOfNewOrUpdatedPermissionLevel.id === level.id)
                        {
                            $scope.permissionLevels[i] = copyOfNewOrUpdatedPermissionLevel;
                            $scope.$apply();
                            return;
                        }
                    }
                    
                    $scope.permissionLevels.push(copyOfNewOrUpdatedPermissionLevel);
                    $scope.$apply();
                }

                window.addOrUpdatePermissionLevel = $scope.addOrUpdatePermissionLevel;
            });
        });
    </script>
    <div class="MainRightHeader" nowrap="true" runat="server" id="titleHeader">
        Conversation Log Permission Levels
    </div>
    <br />
    <div id="PermissionLevelsSection">
        <table>
            <thead>
                <tr class="GridHeader">
                    <th>
                        <!-- edit link -->
                    </th>
                    <th>Re-Order</th>
                    <th>Permission</th>
                    <th>Description</th>
                    <th>Active</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="permissionLevel in permissionLevels">
                    <td>
                        <a class="editLink" ng-click="openPermissionLevelEditor($index)">edit</a>
                    </td>
                    <td>           
                        <div class="moveContainer top">
                         <a class="moveArrow Up" title="Move the condition one space up in the list" ng-click="movePermissionLevelUp($index)"><img src="../../images/up-blue.gif"  /></a>
                         <a class="moveArrow Top" title="Move the condition to the top of the list" ng-click="movePermissionLevelToTop($index)"><img src="../../images/up-blue-top.gif"  /></a>
                        </div>
                        <div class="moveContainer">
                         <a class="moveArrow  Down" title="Move the condition one space down in the list" ng-click="movePermissionLevelDown($index)"><img src="../../images/down-blue.gif"  /></a>
                         <a class="moveArrow Bottom" title="Move the condition to the bottom of the list" ng-click="movePermissionLevelToBottom($index)"><img src="../../images/down-blue-bottom.gif"  /></a>
                        </div>
                    </td>
                    <td>{{permissionLevel.name}}</td>
                    <td>{{permissionLevel.description}}</td>
                    <td><input type="checkbox" disabled="disabled" ng-model="permissionLevel.isActive"/></td>
                </tr>
            </tbody>
            
        </table>
    </div>
    <div class="addNewBtnSection">
        <input type="button" id="AddNewBtn" value="Add New" ng-click="openPermissionLevelEditor()"/>
    </div> <br /><br />
    <div class="windowBtnSection">
        <input type="button" id="OkBtn" value="OK" ng-click="okClick();"/>
        <input type="button" id="CloseBtn" value="Close" ng-click="closeClick();"/>
    </div>
    </form>
</body>
</html>
