using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.LOAdmin.Broker
{
	/// <summary>
	/// Summary description for EditBranch.
	/// </summary>

	public partial class EditDefaultRolePermissions : LendersOffice.Admin.SecuredAdminPage
	{
		protected LendersOfficeApp.los.admin.DefaultRolePermissionsEdit m_Edit;

		private String ErrorMessage
		{
			// Access member.

			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		/// <summary>
		/// Initialize this page.
		/// </summary>
		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Bind the passed in broker and branch to our control.

			try
			{
				m_Edit.BrokerId = RequestHelper.GetGuid( "brokerId"   , Guid.Empty );
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to load web form." , e );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}

}
