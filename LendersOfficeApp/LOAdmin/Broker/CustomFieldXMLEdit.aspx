﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomFieldXMLEdit.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.CustomFieldXMLEdit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Build Custom Field XML</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet"/>
</head>
<body>
	<script type="text/javascript">
		function _init() {
			resize(300, 550);
			var args = getModalArgs() ;

			if (args == null || args.inputDoc === '') {
			    return;
			}

			var xmlDoc = $j.parseXML(args.inputDoc);
			var $xml = $j(xmlDoc);
			var $customFields = $xml.find("customfield");

			$customFields.each(function () {
			    var id = $j(this).attr('id');
			    var description = $j(this).attr('desc');
			    $j('#' + id).val(description);
			});
		}

		function updateClick() {
		    var doc = '';
		    var hasAtLeastOneElement = false;

		    $j('.datapoint').each(function () {
		        var id = $j(this).attr('id');
		        var description = $j(this).val();

		        if (description === '')
		        {
		            return;
		        }

		        doc += '\t<customfield id="' + id + '" desc="' + escapeHtml(description) + '"/>\n';
		        hasAtLeastOneElement = true;
		    });
				
		    if (hasAtLeastOneElement) {
		        doc = '<CustomFieldXml>\n' + doc + '</CustomFieldXml>';
		    }
		
			var args = window.dialogArguments || {};
			args.OK = true;
			args.outputDoc = doc;
			onClosePopup(args);
		}

		// Simple HTML escape method found on StackOverflow (https://stackoverflow.com/a/13371349).
		// Frontend sanitization should be sufficient as this page is not accessible to users.
		function escapeHtml(text) {
		    'use strict';
		    return text.replace(/[\"&'\/<>]/g, function (a) {
		        return {
		            '"': '&quot;',
		            '&': '&amp;',
		            "'": '&#39;',
		            '/': '&#47;',
		            '<': '&lt;',
		            '>': '&gt;'
		        }[a];
		    });
		}
	</script>
	<h4 class="page-header">Build Custom Field XML</h4>
	<form id="form1" runat="server">
		<table class="FormTable" cellspacing="0" cellpadding="0" width="100%" border="0">
			<tr>
				<td class="FieldLabel no-wrap">
					Field Name
				</td>
				<td class="FieldLabel">
					Description
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Custom Event #1
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sU1LStatDesc" runat="server" MaxLength="21" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Custom Event #2
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sU2LStatDesc" runat="server" MaxLength="21" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Custom Event #3
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sU3LStatDesc" runat="server" MaxLength="21" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Custom Event #4
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sU4LStatDesc" runat="server" MaxLength="21" />
				</td>
			</tr>
			<tr>
			    <td>
			        &nbsp;
			    </td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 1
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField1Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 2
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField2Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 3
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField3Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 4
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField4Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 5
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField5Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 6
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField6Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 7
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField7Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 8
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField8Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 9
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField9Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 10
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField10Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 11
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField11Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 12
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField12Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 13
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField13Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 14
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField14Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 15
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField15Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 16
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField16Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 17
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField17Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 18
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField18Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 19
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField19Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 20
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField20Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 21
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField21Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 22
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField22Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 23
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField23Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 24
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField24Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 25
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField25Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 26
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField26Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 27
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField27Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 28
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField28Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 29
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField29Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 30
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField30Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 31
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField31Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 32
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField32Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 33
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField33Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 34
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField34Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 35
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField35Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 36
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField36Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 37
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField37Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 38
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField38Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 39
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField39Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 40
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField40Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 41
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField41Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 42
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField42Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 43
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField43Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 44
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField44Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 45
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField45Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 46
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField46Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 47
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField47Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 48
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField48Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 49
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField49Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 50
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField50Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 51
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField51Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 52
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField52Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 53
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField53Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 54
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField54Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 55
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField55Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 56
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField56Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 57
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField57Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 58
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField58Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 59
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField59Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Field Set 60
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sCustomField60Desc" runat="server" MaxLength="50" />
				</td>
			</tr>
			<tr>
			    <td>
			        &nbsp;
			    </td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 1
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust1Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 2
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust2Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 3
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust3Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 4
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust4Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 5
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust5Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 6
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust6Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 7
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust7Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 8
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust8Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 9
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust9Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 10
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust10Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 11
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust11Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 12
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust12Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 13
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust13Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 14
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust14Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 15
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust15Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
				<td class="no-wrap">
					Trust Description 16
				</td>
				<td>
					<asp:TextBox class="datapoint" ID="sTrust16Desc" runat="server" MaxLength="36" />
				</td>
			</tr>
			<tr>
			    <td>
			        &nbsp;
			    </td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="button" value="Build XML" onclick="updateClick();" />
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
