﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomCocFieldList.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.CustomCocFieldList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Custom CoC Field List</title>
</head>
<body>
    <script type="text/javascript">
        $(function () {
            $("#template").tmpl({ data: customFields }).appendTo($("#results"));

            if (ML.UseCustomCocFields) {
                $("#customCocWarningMsg").hide();
            }
        });

        function deleteRow(index) {
            customFields.splice(index, 1);
            $("#results").empty();
            $("#template").tmpl({ data: customFields }).appendTo($("#results"));

            if (ML.UseCustomCocFields) {
                $("#customCocWarningMsg").hide();
            }
        }

        function onAdd() {
            LQBPopup.ShowElement($("<div></div>").append($.tmpl($("#addTemplate"))), {
                hideCloseButton: true,
                popupClasses: 'Popup',
                width: 600,
                height: 20,
                isCopyHTML: false,
                onReturn: function (returnObj) {
                    if (returnObj != null) {
                        var customField = { FieldId: returnObj.FieldId, Description: returnObj.Description, IsInstant: returnObj.IsInstant };
                        customFields.push(customField);
                        $("#results").empty();
                        $("#template").tmpl({ data: customFields }).appendTo($("#results"));

                        if (ML.UseCustomCocFields) {
                            $("#customCocWarningMsg").hide();
                        }
                    }
                }
            });            
        }

        function onSave() {
            var data = {
                Fields: JSON.stringify(customFields),
                BrokerId: ML.BrokerId
            };

            gService.main.callAsyncSimple("Save", data, function (result) {
                if (!result.error) {
                    alert("Save complete");
                }
                else {
                    alert(result.UserMessage);
                }
            });
        }

        function onAddSave() {
            var fieldId = $("#fieldId").val();

            gService.main.callAsyncSimple("VerifyFieldId", { Fields: JSON.stringify(customFields), FieldId: fieldId }, function (result) {
                if (!result.error) {
                    var description = $("#description").val();
                    var isInstant = $("#isInstant").val() === "Instant";

                    LQBPopup.Return({ FieldId: result.value.FieldId, Description: description, IsInstant: isInstant });
                }
                else {
                    alert(result.UserMessage);
                }
            });            
        }

        function onAddCancel() {
            LQBPopup.Return(null);
        }
    </script>

    <script id="addTemplate" type="text/x-jquery-tmpl">        
        <table>
            <tr>
                <td>
                    Field Id:
                </td>
                <td>
                    <input type="text" id="fieldId" tabindex="0" maxlength="100"/>
                </td>                
                <td>
                    Description:
                </td>
                <td>
                    <input type="text" id="description" tabindex="0" maxlength="100"/>
                </td>
                <td>
                    Instant/Delayed
                </td>
                <td>
                    <select id="isInstant" tabindex="0">
                        <option value="Instant">Instant</option>
                        <option value="Delayed">Delayed</option>
                    </select>
                </td>
            </tr>            
        </table>
        <input type="button" onclick="onAddSave()" value="Save" />
        <input type="button" onclick="onAddCancel()" value="Cancel" />
    </script>

    <script id="template" type='text/x-jquery-tmpl'>
        <span class="FieldLabel" id="customCocWarningMsg">
            <br />Custom Coc Fields are currently not enabled. <br /><br />
        </span>
        <table class="FormTable Table">
            <tr class="FormTableHeader">
                <th class="width-200">
                    Field ID
                </th>
                <th class="width-500">
                    Description
                </th>
                <th>
                    Instant/Delayed
                </th>
                <th>                            
                </th>
            </tr>
            {{each data}}
            <tr class="GridAutoItem">
                <td>
                    ${FieldId}
                </td>
                <td>
                    ${Description}
                </td>
                <td>
                    {{if IsInstant == true}}
                        Instant
                    {{else}}
                        Delayed
                    {{/if}}
                </td>
                <td>
                    <input type="button" onclick="deleteRow('${ $index }');" value="Delete"/>
                </td>
            </tr>
            {{/each}}
        </table>
    </script>

    <form id="form1" runat="server">
        <h4 class="page-header">Custom CoC Field List</h4>
        <div id="results">            
        </div>
        <input type="button" onclick="onAdd()" value="Add" />
        <input type="button" onclick="onSave()" value="Save" />
    </form>
</body>
</html>