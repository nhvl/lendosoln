using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Admin;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
	public partial class UserList : LendersOffice.Admin.SecuredAdminPage
	{
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        protected Guid BrokerId
        {
            get { return RequestHelper.GetGuid("brokerid"); }
        }

        private Guid m_lastAccountOwner 
        {
            get { 
                try 
                {
                    return (Guid) ViewState["LastAccountOwner"]; 
                } 
                catch 
                {
                    return Guid.Empty;
                }
            }
            set { ViewState["LastAccountOwner"] = value; }
        }
	
        protected string CheckRadioBox(EmployeeDesc employeeDesc)
        {
            if (employeeDesc.IsAccountOwner)
            {
                m_lastAccountOwner = employeeDesc.EmployeeId;
                return "checked";
            }
            else
            {
                return String.Empty;
            }
        }

        protected string SetRadioBoxValue(EmployeeDesc employeeDesc)
        {
            // 4/20/2006 mf - OPM 4669 Only "B" users can be Account Owners
            return (employeeDesc.IsActive && employeeDesc.Type == "B") ? employeeDesc.EmployeeId.ToString() : String.Empty;
        }

        protected HtmlInputRadioButton GenerateRadioButton(EmployeeDesc employeeDesc, int itemIndex)
        {
            if (employeeDesc.IsAccountOwner)
            {
                m_lastAccountOwner = employeeDesc.EmployeeId;
            }

            // 4/20/2006 mf - OPM 4669 Only "B" users can be Account Owners
            if (employeeDesc.IsActive && employeeDesc.Type == "B")
            {
                HtmlInputRadioButton radioBtn = new HtmlInputRadioButton();
                radioBtn.ID = "rb" + itemIndex;
                radioBtn.Name = "rb";
                radioBtn.Value = employeeDesc.EmployeeId.ToString();
                radioBtn.Checked = employeeDesc.IsAccountOwner;
                return radioBtn;
            }
            else
            {

                return null;
            }
        }

        protected HtmlAnchor GenerateBecomeLink(EmployeeDesc employeeDesc)
        {
            if (employeeDesc.Type == "B" && employeeDesc.StatusDescription == "Active")
            {
                HtmlAnchor link = new HtmlAnchor();
                link.Attributes.Add("href", "#");
                link.Attributes.Add("onclick", "becomeUser(\"" + employeeDesc.UserId.ToString() + "\", " + AspxTools.JsString(employeeDesc.LoginNm) + ");");
                link.InnerText = "become";
                return link;
            }

            return null;
        }

        protected bool IsPerTransactionBilling
        {
            get 
            { 
                BrokerDB broker = BrokerDB.RetrieveById(this.BrokerId);
                return E_BrokerBillingVersion.PerTransaction == broker.BillingVersion && broker.IsEnablePTMDocMagicSeamlessInterface;
            }
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
            RegisterJsScript("jquery-1.6.min.js");
            RegisterJsScript("LQBPopup.js");
            // Initialize this data grid with the current broker's
            // users.  We fixup the table with a role column.
			
			ViewState["brokerid"] = this.BrokerId;
            ViewState["brokernm"] = RequestHelper.GetSafeQueryString("brokernm") ?? "UNKNOWN";

			DataSet       ds = new DataSet();
			DataSet       rs = new DataSet();

            List<EmployeeDesc> employeeList = new List<EmployeeDesc>();
            Dictionary<Guid, EmployeeDesc> lookup = new Dictionary<Guid, EmployeeDesc>();
			
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", this.BrokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "INTERNAL_USER_ONLY_ListUsersBelongingToBroker", parameters))
            {
                while (reader.Read())
                {
                    EmployeeDesc employeeDesc = new EmployeeDesc(reader);

                    if (lookup.ContainsKey(employeeDesc.EmployeeId) == false)
                    {
                        lookup.Add(employeeDesc.EmployeeId, employeeDesc);
                        employeeList.Add(employeeDesc);
                    }
                }
            }

            parameters = new SqlParameter[] {
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "INTERNAL_USER_ONLY_ListRoleAssignmentsByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    Guid employeeId = (Guid)reader["EmployeeId"];
                    Guid roleId = (Guid)reader["RoleId"];

                    EmployeeDesc employeeDesc = null;

                    if (lookup.TryGetValue(employeeId, out employeeDesc) == true)
                    {
                        Role role = Role.Get(roleId);

                        if (role.ImportanceRank < employeeDesc.Rank)
                        {
                            employeeDesc.Role = role.ModifiableDesc;
                            employeeDesc.Rank = role.ImportanceRank;
                        }
                    }
                }
            }
			
			m_userGrid.BorderColor=Color.White;
			m_userGrid.CellSpacing = 0; 
			m_userGrid.CellPadding = 0;
            m_userGrid.DataSource = employeeList;
			m_userGrid.DataBind();
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

        protected void OnUpdateClick(object sender, System.EventArgs a)
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            Guid newID = new Guid(Request["rb"]);
            if (newID != m_lastAccountOwner)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@OldAccountOwnerEmployeeId", m_lastAccountOwner),
                                                new SqlParameter("@NewAccountOwnerEmployeeId", newID)
                                            };

                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "INTERNAL_USER_ONLY_UpdateAccountOwner", 2, parameters);
            }

            Response.Redirect("UserList.aspx?brokerid=" + this.BrokerId);
        }
	}

	/// <summary>
	/// Combine grid data into one descriptor.
	/// </summary>

	public class EmployeeDesc
	{


		public Guid EmployeeId { get; private set; }

		public Guid UserId { get; private set;}

		public string FullName { get; private set;}

		public string LoginNm { get; private set; }

		public string BranchNm { get; private set; }

		public string Type { get; private set; }

		public string Role { get; set; }

		public int Rank { get; set; }

		public bool IsAccountOwner { get; private set;}

		public bool IsActive { get; private set; }

        public bool CanLogin { get; private set; }

        public string StatusDescription 
        {
            get 
            {
                if (this.IsActive) 
                {
                    if (this.CanLogin) 
                    {
                        return "Active";
                    } 
                    else 
                    {
                        return "Disabled";
                    }
                } 
                else 
                {
                    return "Inactive";
                }
            }
        }

        internal EmployeeDesc(DbDataReader reader)
        {

            this.EmployeeId = reader.SafeGuid("EmployeeId");
            this.UserId = reader.SafeGuid("UserId");
            this.FullName = reader.SafeString("FullName");
            this.LoginNm = reader.SafeString("LoginNm");
            this.BranchNm = reader.SafeString("BranchNm");
            this.Type = reader.SafeString("Type");
            this.IsAccountOwner = reader.SafeBool("IsAccountOwner");
            this.IsActive = reader.SafeBool("IsActive");
            this.CanLogin = reader.SafeBool("CanLogin");
            this.Role = string.Empty;
            this.Rank = 100;

        }
	}

}
