﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using System.IO;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class ImportBranchDMLogins : LendersOffice.Admin.SecuredAdminPage
    {

        private ImportErrorSet errSet = new ImportErrorSet();
        private ImportErrorSet warningSet = new ImportErrorSet(); 

        private string ErrorMessage
        {
            set { ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + "."); }
        }

        private string FeedBack
        {
            set { ClientScript.RegisterHiddenField("m_feedBack", value.TrimEnd('.') + "."); }
        }


        private Guid BrokerId
        {
            // Access request.

            get
            {
                try
                {
                    return new Guid(Request["brokerId"]);
                }
                catch (HttpException)
                {
                    return Guid.Empty;
                }
                catch (ArgumentNullException)
                {
                    return Guid.Empty;
                }
                catch (FormatException)
                {
                    return Guid.Empty;
                }
            }
        }

        #region Handlers
        /// <summary>
        /// Initialize this page.
        /// </summary>
        protected void PageLoad(object sender, System.EventArgs a)
        {
            var broker = BrokerDB.RetrieveById(BrokerId);
            if (E_BrokerBillingVersion.PerTransaction != broker.BillingVersion)
            {
                string msg = "can't access branch docmagic importer if billing version isn't " + E_BrokerBillingVersion.PerTransaction;
                throw new CBaseException(msg, msg);
            }

            if (!IsPostBack)
            {
                m_Upload.ToolTip = "Loads text into box below";
                m_tbBranchDMLogins.Text = "Branch Name,DocMagic Username,DocMagic Password,CompanyID";
            }
        }        
        
        protected void UploadClick(object sender, EventArgs e)
        {
            // Bring in the specified text file and display it in our
            // editor.

            try
            {
                StreamReader sR = new StreamReader(m_File.PostedFile.InputStream);
                m_tbBranchDMLogins.Text = sR.ReadToEnd();
            }
            catch (ArgumentException ae)
            {
                ErrorMessage = ae.Message;
            }
            catch (IOException iex)
            {
                Tools.LogError(ErrorMessage = "Failed to upload: " + iex.Message + ".", iex);
            }
        }

        protected void ImportClick(object sender, EventArgs e)
        {
            Dictionary<string, BranchDB> branchByName;
            try
            {
               branchByName = GetBrokerBranchesByName();
            }
            catch(ArgumentException)
            {
                errSet.Add("", "this broker has mutliple branches by the same name, thus importing will not work until this is resolved.",-1);
                WriteAllFeedback();
                return;
            }
            var importedBranchNames = new HashSet<string>();

            
            string[] lines = m_tbBranchDMLogins.Text.Split(new string[]{"\n"}, StringSplitOptions.RemoveEmptyEntries);

            if (false == HasGoodHeader(lines))
            {
                errSet.Add("", "Bad header line.", 0);
                WriteAllFeedback();
                return;
            }
            if (lines.Length < 2)
            {
                errSet.Add("", "Nothing to import (no data rows)", 1);
                WriteAllFeedback();
                return;
            }
            
            for(int lineInd = 1; lineInd < lines.Length; lineInd++)
            {
                string line = lines[lineInd];
                bool lineHasErrors;
                var branchDocMagicLineInfo = GetBranchDocMagicInfoFromLine(line, lineInd, out lineHasErrors);  // may add errors if line is not properly formatted.
                lineHasErrors = WriteBranchLoginErrors(branchDocMagicLineInfo, branchByName) || lineHasErrors; // adds missing info errors / missing branches errors.
                if (!lineHasErrors)
                {
                    UpdateBranchDocMagicInfo(branchDocMagicLineInfo, branchByName[branchDocMagicLineInfo.Name]);
                }
                CheckForDuplicateBranch(branchDocMagicLineInfo, importedBranchNames);   // adds an error if that branch already was in the table.
            }

            WriteAllFeedback();
        }

        private bool HasGoodHeader(string[] lines)
        {
            if (lines.Length < 1)
            {
                errSet.Add("", "No header line", 0);
                return false;
            }
            var header = lines[0];
            var headers = header.Split(',');
            if (headers.Length != 4)
            {
                errSet.Add("", "\""+header + "\" has wrong number of columns", 0);
                return false;
            }
            if (string.Compare(headers[0].TrimWhitespaceAndBOM(), "Branch Name", true) != 0)
            {
                errSet.Add("", string.Format("header's first column is \"{0}\" and should be \"{1}\"", headers[0], "Branch Name"), 0);
                return false;
            }
            if (string.Compare(headers[1].TrimWhitespaceAndBOM(), "DocMagic Username", true) != 0)
            {
                errSet.Add("", string.Format("header's second column is \"{0}\" and should be \"{1}\"", headers[1], "DocMagic Username"), 0);
                return false;
            }
            if (string.Compare(headers[2].TrimWhitespaceAndBOM(), "DocMagic Password", true) != 0)
            {
                errSet.Add("", string.Format("header's third column is \"{0}\" and should be \"{1}\"", headers[2], "DocMagic Password"), 0);
                return false;
            }
            if (string.Compare(headers[3].TrimWhitespaceAndBOM(), "CompanyID", true) != 0)
            {
                errSet.Add("", string.Format("header's fourth column is \"{0}\" and should be \"{1}\"", headers[3], "CompanyID"), 0);
                return false;
            }

            return true;
        }
        #endregion

        private void WriteAllFeedback()
        {
            //! add stuff for feedback?
            if (errSet.Count > 0)
            {
                m_Errors.DataSource = errSet;
                m_Errors.DataBind();

                m_ErrorList.Visible = true;
            }
            else
            {
                m_ErrorList.Visible = false;
            }

            if (warningSet.Count > 0)
            {
                m_WarningList.DataSource = warningSet;
                m_WarningList.DataBind();

                m_Warning.Visible = true;
            }
            else
            {
                m_Warning.Visible = false;
            }
        }

        private void CheckForDuplicateBranch(BranchDocMagicInfo branchDocMagicLineInfo, HashSet<string> importedBranchNames)
        {
            if (branchDocMagicLineInfo == null)
                return;

            if (importedBranchNames.Contains(branchDocMagicLineInfo.Name))
            {
                warningSet.Add(branchDocMagicLineInfo.Name, "duplicate branch name.", branchDocMagicLineInfo.Line);
            }
            else
            {
                importedBranchNames.Add(branchDocMagicLineInfo.Name);
            }
        }

        private void UpdateBranchDocMagicInfo(BranchDocMagicInfo branchDocMagicLineInfo, BranchDB branch)
        {
            DocumentVendorLoginCredentials cred = new DocumentVendorLoginCredentials(
                Guid.Empty, //DocMagic VendorId 
                branchDocMagicLineInfo.DocMagicUserName,
                branchDocMagicLineInfo.DocMagicPassword,
                branchDocMagicLineInfo.DocMagicCustomerID);
            branch.SaveSingleDocumentVendorCredentials(cred);
        }

        private bool WriteBranchLoginErrors(BranchDocMagicInfo branchDocMagicLineInfo, Dictionary<string, BranchDB> branchByName)
        {
            if (branchDocMagicLineInfo == null)
            {
                return true;
            }

            bool hasErrors = false;
            if (false == branchByName.ContainsKey(branchDocMagicLineInfo.Name)) // it is case-sensitive.
            {
                errSet.Add(branchDocMagicLineInfo.Name,
                    "broker doesn't have a branch with this name. (also note branchnames are case sensitive)",
                    branchDocMagicLineInfo.Line);
                hasErrors = true;
            }            
            if(string.IsNullOrEmpty(branchDocMagicLineInfo.DocMagicPassword))
            {
                errSet.Add(branchDocMagicLineInfo.Name, "missing password", branchDocMagicLineInfo.Line);
                hasErrors = true;
            }
            if(string.IsNullOrEmpty(branchDocMagicLineInfo.DocMagicCustomerID))
            {
                errSet.Add(branchDocMagicLineInfo.Name, "missing company id", branchDocMagicLineInfo.Line);
                hasErrors = true;
            }
            if(string.IsNullOrEmpty(branchDocMagicLineInfo.DocMagicUserName))
            {
                errSet.Add(branchDocMagicLineInfo.Name, "missing username", branchDocMagicLineInfo.Line);
                hasErrors = true;
            }
            return hasErrors;
        }

        private BranchDocMagicInfo GetBranchDocMagicInfoFromLine(string line, int lineInd, out bool lineHasErrors)
        {
            var split = line.Split(',');
            lineHasErrors = false;
            if (split.Length != 4)
            {
                errSet.Add("unknown", "line: \""+line+ "\" does not have the appropriate number of commas (3)", lineInd);
                lineHasErrors = true;
                return null;
            }
            return new BranchDocMagicInfo()
            {
                Name = split[0].TrimWhitespaceAndBOM(),
                DocMagicUserName = split[1].TrimWhitespaceAndBOM(),
                DocMagicPassword = split[2].TrimWhitespaceAndBOM(),
                DocMagicCustomerID = split[3].TrimWhitespaceAndBOM(),
                Line = lineInd
            };
        }

        private Dictionary<string, BranchDB> GetBrokerBranchesByName()
        {
            var branchByName = new Dictionary<string, BranchDB>();  // case sensitive.
            foreach (var branch in BranchDB.GetBranchObjects(BrokerId))
            {
                branchByName.Add(branch.Name, branch);
            }
            return branchByName;
        }

        private class BranchDocMagicInfo
        {
            public string Name;
            public string DocMagicUserName;
            public string DocMagicPassword;
            public string DocMagicCustomerID;
            public int Line;
        }

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.EditBroker ,
				E_InternalUserPermissions.EditEmployee
			};
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
