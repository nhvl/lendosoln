﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssociateGroups.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.AssociateGroups" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AssociateGroups</title>
    <style type="text/css">
        .BtnSection
        {
            text-align: center;
        }
        .TableSection
        {
            padding: 5px;
        }
        table 
        {
            border-collapse: collapse;
            border: 1px solid black;
        }
        td
        {
            border: 1px solid black;
        }
        #GroupsTable
        {
            width: 100%;
        }
        .CBCell
        {
            width: 2%;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            function PopulateSelectedGroups()
            {
                var args = getModalArgs();
                if (args != null)
                {
                    // Group ids were passed in from the parent window. We'll populate using those.
                    var groupsFromEditPage = args.selectedGroups;
                    if (typeof (groupsFromEditPage) !== 'undefined' && groupsFromEditPage.length > 0) {
                        $.each(groupsFromEditPage, function (i, val) {
                            $("input[value='" + val + "']").closest('td').find("input[id$='groupCb']").prop('checked', true);
                        });
                    }
                }
                else
                {
                    var groupsFromRegistryPage = $("#SelectedGroupIds");
                    if (groupsFromRegistryPage !== '') {
                        var groupIds = groupsFromRegistryPage.val().split(',');
                        if (typeof groupIds !== 'undefined' && groupIds.length !== 0) {
                            $.each(groupIds, function (i, val) {
                                $("input[value='" + val + "']").closest('td').find("input[id$='groupCb']").prop('checked', true);
                            });
                        }
                    }
                }
            }

            function InitializeReadOnly()
            {
                var isEditMode = $("#IsEditModeValue").val() === "True";

                if(!isEditMode)
                {
                    $("input[id$='groupCb']").attr('disabled', true);
                }
            }

            $("#CloseBtn").click(function () {
                onClosePopup();
            });

            $("#ApplyBtn").click(function () {
                // Apply button can only be selected if this is loaded up in a modal.

                var groupIdsSelected = [];
                $("input[id$='groupCb']:checked").each(function () {
                    var groupId = $(this).closest("td").find("input[id$='selectedId']").val();
                    groupIdsSelected.push(groupId);
                });

                var args = window.dialogArguments || {};
                args.selectedGroups = groupIdsSelected;
                args.OK = true;

                onClosePopup(args);
            });

            resize(800, 900);

            PopulateSelectedGroups();

            InitializeReadOnly();
        });
    </script>
    <h4 class="page-header"><ml:EncodedLabel runat="server" ID="HeaderText"></ml:EncodedLabel></h4>
    <form id="form1" runat="server">
        <div class="TableSection">
            <asp:HiddenField runat="server" ID="IsEditModeValue" />
            <asp:HiddenField runat="server" ID="SelectedGroupIds" />
            <table id="GroupsTable">
                <tr class="GridHeader">
                    <td>&nbsp;</td>
                    <td>
                        <ml:EncodedLabel ID="NameHeaderLabel" runat="server"></ml:EncodedLabel>
                    </td>
                    <td>Description</td>
                </tr>
                <asp:Repeater runat="server" ID="GroupsRepeater" OnItemDataBound="GroupsRepeater_ItemDataBound">
                    <ItemTemplate>
                        <tr class="GridAutoItem">
                            <td class="CBCell">
                                <asp:CheckBox runat="server" ID="groupCb" class="GroupCB" />
                                <asp:HiddenField runat="server" ID="selectedId" Value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "GroupId").ToString()) %>' />
                            </td>
                            <td>
                                <ml:EncodedLabel runat="server" ID="groupName" class="GroupName"></ml:EncodedLabel>
                            </td>
                            <td>
                                <ml:EncodedLabel runat="server" ID="groupDesc" class="GroupDesc"></ml:EncodedLabel>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
        <div class="BtnSection">
            <input id="ApplyBtn" type="button" runat="server" value="Apply" />
            <input id="CloseBtn" type="button" value="Close" />
        </div>
    </form>
</body>
</html>
