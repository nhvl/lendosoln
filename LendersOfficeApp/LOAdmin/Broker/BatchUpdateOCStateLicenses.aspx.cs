﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DataAccess;
using LendersOfficeApp.los.admin;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using LendersOffice.Common;
using LendersOffice.Admin;
using LendersOffice.Common.TextImport;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class BatchUpdateOCStateLicenses : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker
            };
        }

        private Guid BrokerId
        {
            get
            {
                return new Guid(RequestHelper.GetSafeQueryString("BrokerId"));
            }
        }

        private int AttemptUpdate(string licenseNum, string state, string expirD, List<PmlBroker> pBrokerList)
        {
            int count = 0;
            PmlBroker selectedPB = null;
            LicenseInfo selectedlInfo = null;
            foreach (PmlBroker pb in pBrokerList)
            {
                LicenseInfo lInfo = pb.LicenseInfoList.Exists(licenseNum, state);
                if (lInfo != null)
                {
                    // We found a matching license. Hold onto it for now.
                    selectedPB = pb;
                    selectedlInfo = lInfo;
                    count++;
                    if (count > 1)
                    {
                        // Same license found in multiple brokers. Skip
                        return 0;
                    }
                }
            }

            if (selectedPB == null)
            {
                // No license found. Skip
                return 1;
            }
            else
            {
                // Only one matching company with this license. We can finally update
                selectedlInfo.ExpD = expirD;
                selectedPB.isDirty = true;
                return 2;
            }
        }
        
        protected void UploadClick(object sender, System.EventArgs a)
        {
            int companyNameCol = 0;
            int companyIdCol = 1;
            int licenseNumCol = 2;
            int stateCol = 3;
            int expirDCol = 4;

            string[] formats = { "MM/dd/yyyy", "MM/d/yyyy", "M/dd/yyyy", "M/d/yyyy" };
            StringBuilder errorList = new StringBuilder();
            
            if (Path.GetExtension(m_File.PostedFile.FileName) != ".csv")
            {
                m_errorData.Text = "Please use a .csv file following the format in the instructions below";
                return;
            }

            // Grab the file contents
            string tempFilename = TempFileUtils.NewTempFilePath() + Path.GetExtension(m_File.PostedFile.FileName);
            m_File.PostedFile.SaveAs(tempFilename);
            ILookup<int, string> parseErrors;
            DataTable fileContents = DataParsing.FileToDataTable(tempFilename, true, "", out parseErrors);
            if (parseErrors.Any())
            {
                foreach (var error in parseErrors)
                {
                    foreach (string errorMsg in error)
                    {
                        errorList.AppendLine("Error on line " + error.Key + ": " + errorMsg);
                    }
                }

                m_errorData.Text = errorList.ToString();
                return;
            }

            string outputFile = TempFileUtils.NewTempFilePath();
            Dictionary<string, List<PmlBroker>> pBrokers = PmlBroker.GetUniqueBrokersByCompanyIdAndName(BrokerId);
            using (StreamWriter writer = new StreamWriter(outputFile, false))
            {
                writer.WriteLine("CompanyName,CompanyID,License#,State,ExpirD,Action,Reason");

                foreach (DataRow row in fileContents.Rows)
                {
                    string companyName = row[companyNameCol].ToString().TrimWhitespaceAndBOM();
                    string companyNameCSV = DataParsing.sanitizeForCSV(companyName);
                    string companyId = row[companyIdCol].ToString().TrimWhitespaceAndBOM();
                    string licenseNum = row[licenseNumCol].ToString().TrimWhitespaceAndBOM();
                    string state = row[stateCol].ToString().TrimWhitespaceAndBOM().ToUpper();
                    string expirD = row[expirDCol].ToString().TrimWhitespaceAndBOM();

                    // Validate inputs
                    DateTime temp;
                    if (string.IsNullOrEmpty(companyName) || string.IsNullOrEmpty(licenseNum)
                        || !LicenseInfoList.IsValidState(state) 
                        || !DateTime.TryParseExact(expirD, formats, null, DateTimeStyles.None, out temp))
                    {
                        writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5},{6}",
                                         companyNameCSV, companyId, licenseNum, state, expirD, "Skip", "Field requirements not met"));
                        continue;
                    }

                    string nameId = companyName + companyId;
                    if (pBrokers.ContainsKey(nameId))
                    {
                        List<PmlBroker> brokerList = pBrokers[nameId];
                        int res = AttemptUpdate(licenseNum, state, expirD, brokerList);
                        if (res == 0)
                        {
                            writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5},{6}",
                                             companyNameCSV, companyId, licenseNum, state, expirD, "Skip", "Multiple matching companies with same license"));
                        }
                        else if (res == 1)
                        {
                            writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5},{6}",
                                             companyNameCSV, companyId, licenseNum, state, expirD, "Skip", "Matching company found but no matching license"));
                        }
                        else if (res == 2)
                        {
                            writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}",
                                             companyNameCSV, companyId, licenseNum, state, expirD, "Update"));
                        }
                    }
                    else
                    {
                        writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5},{6}",
                                         companyNameCSV, companyId, licenseNum, state, expirD, "Skip", "No matching company"));
                    }
                }

                writer.WriteLine();
                writer.WriteLine("Save Errors. Please retry these OCs");
                var enumerator = pBrokers.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    List<PmlBroker> pbList = enumerator.Current.Value;
                    foreach (PmlBroker pb in pbList)
                    {
                        if (pb.isDirty)
                        {
                            try
                            {
                                pb.Save();
                            }
                            catch (CBaseException)
                            {
                                writer.WriteLine("Could not save licenses for company with name " + pb.Name + " and id " + pb.CompanyId);
                            }
                        }
                    }
                }
            }
            RequestHelper.SendFileToClient(Context, outputFile, "text/csv", "OCUpdateResults.csv");
            Response.End();
        }
    }
}
