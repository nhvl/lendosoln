using System;
using System.Collections;
using System.ComponentModel;
using LendersOffice.Constants;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.common;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOfficeApp.los.admin;

namespace LendersOfficeApp.LOAdmin.Broker
{
	/// <summary>
	/// Summary description for ShowUsersAssociatedWithBranch.
	/// </summary>
	public partial class ShowUsersAssociatedWithBranch : LendersOffice.Admin.SecuredAdminPage
	{
		protected System.Web.UI.WebControls.Button					m_SelectAll;
		protected System.Web.UI.WebControls.Panel					CheckBox;
		private Hashtable											m_BranchHash = new Hashtable();
		private ArrayList											m_Branches = new ArrayList();
		private ArrayList											m_Selected = new ArrayList();


		protected Guid m_BrokerID 
		{
			get {  return new Guid( Request[ "brokerId" ] ); }
		}

		private Guid m_BranchId 
		{
			get { return new Guid( Request[ "branchId" ] ); }
		}

		private string m_BranchName
		{
			get { return Request[ "branchName" ]; }
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			RegisterJsScript("jquery-1.6.min.js");
            RegisterJsScript("LQBPopup.js");
			BindDataGrid();
			m_branchNameLabel.Text = "Users associated with branch:  " + m_BranchName;
			m_noneToShowLabel.Visible = (m_dg.Items.Count == 0)?true:false;

			if(m_dg.Items.Count >= 100)
				m_tooManyUsers.Text = "(Too many users to display - displaying the first 100)";
			else
				m_tooManyUsers.Text = "";
		}

		private void BindDataGrid()
		{
			DataSet ds = new DataSet();
			SqlParameter[] parameters = {
											new SqlParameter("@BrokerId", m_BrokerID),
											new SqlParameter("@BranchId", m_BranchId)
										};
			DataSetHelper.Fill(ds, m_BrokerID, "ListUsersAssociatedWithBranch", parameters );
			m_dg.DataSource = ds.Tables[0].DefaultView;
			m_dg.DataBind();

            SqlParameter[] brokerIdParameter = {
                                                   new SqlParameter("@BrokerId", m_BrokerID)
                                               };
            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(m_BrokerID, "ListBranchByBrokerId", brokerIdParameter))
            {
                string b_Name;
                string b_Id;
                m_BranchHash.Clear();
                m_Branches.Clear();
                while (sR.Read() == true)
                {
                    b_Name = sR["Name"].ToString();
                    b_Id = sR["BranchID"].ToString();
                    m_BranchHash.Add(b_Name, b_Id);
                    m_Branches.Add(b_Name);
                }
            }

			if( !Page.IsPostBack )
				InitBranchDropdownList();
		}

		//OPM 18349 -jMorse
		//Initializes List of branches
		// 08/26/08 ck - OPM 23916. Remove current branch from drop down list
		protected void InitBranchDropdownList()
		{
			m_BranchDropdownList.Items.Clear();
			foreach( String s in m_Branches )
			{
				if(!s.Equals(m_BranchName))
				{
					m_BranchDropdownList.Items.Add( s );	
				}	
			}	
		}

		// 08/26/08 ck - OPM 23916. Instead of using the SelectedIndex of the 
		// drop down list, used the SelectedItem
		protected void MoveUsersClick( object sender, System.EventArgs a )
		{
			string guid = (string)m_BranchHash[m_BranchDropdownList.SelectedItem.ToString()];

			foreach( System.Web.UI.WebControls.DataGridItem item in m_dg.Items )
			{				
				CheckBox cbox = item.FindControl( "m_CheckBox" ) as CheckBox;
				
				if( cbox.Checked )
				{				
					Label eid = item.FindControl( "m_EID" ) as Label;					
					m_Selected.Add( eid.Text );				
				}				
			}	

			Guid BranchMovingFrom = new Guid( m_BranchId.ToString() );
			Guid BranchMovingTo = new Guid( guid );			
			
			MoveUsersFromBranch( BranchMovingFrom, BranchMovingTo );

			PageLoad(sender,a); //Refresh page.			
		}

		protected void MoveUsersFromBranch( Guid BranchFromId, Guid BranchToId )
		{
			Guid employeeId;
			
			foreach( string eid in m_Selected )
			{
				employeeId = new Guid( eid );
				EmployeeDB edb = new EmployeeDB( employeeId, m_BrokerID );
				
				try
				{
					edb.Retrieve();
					edb.BranchID = BranchToId;
					edb.Save(PrincipalFactory.CurrentPrincipal);
				}
				catch
				{
					throw;
				}				
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
        #endregion

        protected void m_dg_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            var employeeIdLabel = e.Item.FindControl("m_EID") as MeridianLink.CommonControls.EncodedLabel;
            DataRowView row = e.Item.DataItem as DataRowView;
            employeeIdLabel.Text = row["EmployeeId"].ToString();
        }
    }
}