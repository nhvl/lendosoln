﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InstallClientCertificate.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.InstallClientCertificate" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Install Lender Client Certificate</title>
    <style type="text/css">
        #MainDiv
        {
            padding: 10px;
        }
        #PasswordSection
        {
            font-size: 16px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            $("#GetCertBtn").click(function () {
                var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("~/LOAdmin/Broker/InstallClientCertificate.aspx")) %> + '?cmd=download&key=' + ML.Keys;
                window.open(url, '_self');
                $(this).prop('disabled', true);
                return false;
            });

            resize(600, 200);
        });
    </script>
    <form id="form1" runat="server">
    <div class="MainRightHeader" nowrap="true">
        Install Client Certificate
    </div>
    <div id="MainDiv">
        <div id="Warning">
            The client certificate can be installed to authenticate a user and bypass the authentication code process. This should only be installed on a trusted machine. You will be required to enter the password below during installation.
        </div>
        <br /><br />
        <div id="PasswordSection">
            <span class="FieldLabel">Certificate Password: </span>
            <ml:EncodedLiteral runat="server" ID="CertPassword"></ml:EncodedLiteral>
        </div>
        <br /><br />
        <div>
            <input type="button" id="GetCertBtn" value="Get Certificate"/>
        </div>
    </div>
    </form>
</body>
</html>
