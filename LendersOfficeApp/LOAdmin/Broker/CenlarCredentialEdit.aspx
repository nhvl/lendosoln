﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CenlarCredentialEdit.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.CenlarCredentialEdit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Cenlar Credentials</title>
    <style type="text/css">
        input[type=text]
        {
            width: 200px;
        }

        .align-center
        {
            text-align: center;
        }

        td
        {
            padding-right: 10px;
        }

        #CenlarErrorMessage
        {
            color: #FF0000;
        }
    </style>
    <script type="text/javascript">
        jQuery(function ($) {
            $('#SaveCredentialsBtn').click(function () {
                if (!validateCredentials()) {
                    return;
                }

                var data = {
                    BrokerId: $('#BrokerId').val(),
                    CenlarEnvironment: $('#CenlarEnvironment').val(),
                    CenlarUserId: $('#CenlarUserId').val(),
                    CenlarCustomerId: $('#CenlarCustomerId').val(),
                    CenlarApiKey: $('#CenlarApiKey').val(),
                    CachedApiKey: $('#CachedApiKey').val(),
                    CachedApiKeyReset: $('#CachedApiKeyReset').val()
                };

                gService.main.callAsyncSimple("SaveCredentials", data, function (result) {
                    if (result.error) {
                        alert(result.UserMessage);
                    }
                    closePopup();
                });
            });

            $('#CancelBtn').click(function () {
                closePopup();
            });

            function validateCredentials() {
                var $userId = $('#CenlarUserId');
                var $customerId = $('#CenlarCustomerId');
                var $apiKey = $('#CenlarApiKey');

                var valid = $userId.val() !== '' && $customerId.val() !== '' && $apiKey.val() !== '';
                $('#CenlarErrorSection').toggle(!valid);
                return valid;
            }

            function closePopup() {
                window.parent.LQBPopup.Return();
            }

            $('#CenlarErrorSection').hide();
        });
    </script>
</head>
<body>
    <form id="CenlarCredentialEditForm" runat="server">
    <table id="CenlarCredentialsTable">
        <tr>
            <td class="FieldLabel">
                Cenlar Environment
            </td>
            <td>
                <asp:TextBox ID="CenlarEnvironment" runat="server" ReadOnly="true"></asp:TextBox>
                <asp:HiddenField ID="BrokerId" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                User ID
            </td>
            <td>
                <asp:TextBox ID="CenlarUserId" runat="server"></asp:TextBox>
                <img id="CenlarUserIdRequiredIcon" alt="required" runat="server" src="~/images/require_icon.gif" class="CenlarRequiredIcon">
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Customer ID
            </td>
            <td>
                <asp:TextBox ID="CenlarCustomerId" runat="server"></asp:TextBox>
                <img id="CenlarCustomerIdRequiredIcon" alt="required" runat="server" src="~/images/require_icon.gif" class="CenlarRequiredIcon">
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                API Key
            </td>
            <td>
                <asp:TextBox ID="CenlarApiKey" runat="server"></asp:TextBox>
                <img id="CenlarApiKeyRequiredIcon" alt="required" runat="server" src="~/images/require_icon.gif" class="CenlarRequiredIcon">
                <asp:HiddenField ID="CachedApiKey" runat="server" />
                <asp:HiddenField ID="CachedApiKeyReset" runat="server" />
            </td>
        </tr>
        <tbody id="CenlarErrorSection">
            <tr>
                <td colspan="2" class="align-center">
                    <span id="CenlarErrorMessage">
                        Save failed. Please fill out all fields.
                    </span>
                </td>
            </tr>
        </tbody>
        <tr>
            <td colspan="2" class="align-center">
                <input type="button" id="SaveCredentialsBtn" value="Save" />
                <input type="button" id="CancelBtn" value="Cancel" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
