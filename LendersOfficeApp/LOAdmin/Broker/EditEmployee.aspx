<%@ Page language="c#" Codebehind="EditEmployee.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.EditEmployee" %>
<%@ Register TagPrefix="uc" TagName="EmployeeEdit" Src="../../los/admin/EmployeeEdit.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head runat="server">
		<title>Edit Employee</title>
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
        <link href="../../css/stylesheetnew.css" type="text/css" rel="stylesheet">
	</head>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto; BACKGROUND-COLOR: dimgray;" onload="onInit();">
		<script type="text/javascript">
	
		function onInit()
		{
			if( document.getElementById("m_errorMessage") != null )
			{
				alert( document.getElementById("m_errorMessage").value );
			}
			else
			if( document.getElementById("m_commandToDo") != null )
			{
				if( document.getElementById("m_commandToDo").value == "Close" )
				{
					self.close();
				}
			}

			if( window.dialogArguments != null )
			{
				<% if( IsPostBack == false ) { %>
				
				resize( 960 , 770 ); 
				
				<% } %>
			}
			
			if ( typeof(setUIStatus) != 'undefined' )
			{
				setUIStatus();
			}
		}
		
		function PopulateDefaultRolePermissions(selectedRoles, selectedValues)
		{	
			var args = new Object();
			for(var i = 0; i < selectedRoles.length; ++i)
			{
				var name = selectedRoles[i];
				var value = selectedValues[i];
				args[name] = value;
			}
			args["brokerId"] = <%= AspxTools.JsString(m_Edit.DataBroker) %>;
			return gService.main.call("PopulateDefaultRolePermissions", args);
		}
		
		function PopulateDefaultNonBoolRolePermissions(selectedRoles, selectedValues)
		{	
			var args = new Object();
			for(var i = 0; i < selectedRoles.length; ++i)
			{
				var name = selectedRoles[i];
				var value = selectedValues[i];
				args[name] = value;
			}
			args["brokerId"] = <%= AspxTools.JsString(m_Edit.DataBroker) %>;
			return gService.main.call("PopulateRSENonBoolPermission", args);
		}
		
		</script>
		<form id="EditEmployee" method="post" runat="server" class="expand-w-content">
        <h4 class="page-header">Edit Employee</h4>
			<table cellspacing="0" cellpadding="8" border="0" width="100%" height="100%">
				<tr>
					<td height="100%">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
							<tr>
								<td height="100%" style="BORDER: 2px outset; BACKGROUND-COLOR: gainsboro;">
									<uc:EmployeeEdit id="m_Edit" runat="server"></uc:EmployeeEdit>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
