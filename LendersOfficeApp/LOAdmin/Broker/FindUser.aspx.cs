using System;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class FindUser : LendersOffice.Admin.SecuredAdminPage
    {
        #region Permission
        static readonly E_InternalUserPermissions[] _findUserPermission = new[] { E_InternalUserPermissions.ViewEmployees };

        /// <summary>
        /// Return this page's required permissions set.  This
        /// test occurs with each page initialization.
        /// </summary>
        /// <returns>
        /// Array of required permissions.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.
            return _findUserPermission;
        }
        #endregion

        const int MaxUserQueryCount = 100;

        #region WebService: BecomeUser BecomePml GetPermission Search
        [WebMethod]
        public static BecomeRequestResult BecomeUser(string brokerId, string userId)
        {
            return BecomeLqbUser(brokerId, userId, isRetail: false);
        }

        [WebMethod]
        public static BecomeRequestResult BecomeRetail(string brokerId, string userId)
        {
            return BecomeLqbUser(brokerId, userId, isRetail: true);
        }

        [WebMethod]
        public static object BecomePml(string userId)
        {
            SecuredAdminPageEx.EnforcePermission(E_InternalUserPermissions.BecomeUser);

            Guid gUserId;
            {
                try { gUserId = new Guid(userId); }
                catch (FormatException) { return BecomeRequestResult.Error("Invalid User ID"); }
            }

            var pmlsiteid = new Guid(userId);
            return BecomeRequestResult.Success(RequestHelper.BecomePmlUserUrl(gUserId, pmlsiteid, PrincipalFactory.CurrentPrincipal.UserId));
        }

        public class BecomeRequestResult
        {
            public bool IsSuccess { get; set; }
            public string Message { get; set; }
            public string RedirectUrl { get; set; }

            public static BecomeRequestResult Error(string message)
            {
                return new BecomeRequestResult { IsSuccess = false, Message = message };
            }

            public static BecomeRequestResult Success(string url)
            {
                if (!url.StartsWith("http://") && !url.StartsWith("https://"))
                {
                    try { url = VirtualPathUtilityEx.ToAbsolute(url); }
                    catch (HttpException e) { Tools.LogError("BecomeRequestResult.Success", e); throw; }
                }

                return new BecomeRequestResult { IsSuccess = true, RedirectUrl = url };
            }
        }

        [WebMethod]
        public static object GetPermission()
        {
            var permission =  new {
                EditEmployee = SecuredAdminPageEx.HasPermissionToDo(E_InternalUserPermissions.EditEmployee),
                BecomeUser   = SecuredAdminPageEx.HasPermissionToDo(E_InternalUserPermissions.BecomeUser),
                CreateBroker = SecuredAdminPageEx.HasPermissionToDo(E_InternalUserPermissions.CreateBroker), // add new
                EditBroker   = SecuredAdminPageEx.HasPermissionToDo(E_InternalUserPermissions.EditBroker),   // col 0
                ViewActivity = SecuredAdminPageEx.HasPermissionToDo(E_InternalUserPermissions.ViewActivity), // col 1
                ViewBrokers  = SecuredAdminPageEx.HasPermissionToDo(E_InternalUserPermissions.ViewBrokers),
            };
            return permission;
        }

        // 3/22/2006 mf - OPM 4322. We can now filter by broker status
        /// <summary>
        /// Find User
        /// </summary>
        /// <returns>Search result (User[]) or Error message (string)</returns>
        [WebMethod]
        public static object Search(
            string login, string employeeName, string email, string broker,
            string userId, string status, string type, string employeeId, bool? brokerStatus)
        {
            if (SecuredAdminPageEx.IsMissingPermission(_findUserPermission)) { return null; }

            Guid gUserId = Guid.Empty;
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    try { gUserId = new Guid(userId.Replace("'", "''").TrimWhitespaceAndBOM()); }
                    catch (FormatException) { return "Invalid UserId."; }
                }
            }

            Guid gEmployeeId = Guid.Empty;
            if (!string.IsNullOrEmpty(employeeId))
            {
                try { gEmployeeId = new Guid(employeeId.Replace("'", "''").TrimWhitespaceAndBOM()); }
                catch (FormatException) { return "Invalid EmployeeId."; }
            }

            return UserDbSearch.Search(
                status, type, login, employeeName, email, broker,
                brokerStatus, gUserId, gEmployeeId)
                .Take(MaxUserQueryCount)
                .Select(u => new
                {
                    u.EmployeeId,
                    u.BrokerId,
                    u.UserId,

                    u.LoginNm,
                    u.FullName,
                    u.Email,
                    u.BrokerNm,
                    u.BranchNm,
                    u.RecentLoginD,
                    u.Type,
                    u.IsActive,
                    u.CanLogin,
                    u.EnableRetailTpoPortalMode
                });
        }

        private static BecomeRequestResult BecomeLqbUser(string brokerId, string userId, bool isRetail)
        {
            SecuredAdminPageEx.EnforcePermission(E_InternalUserPermissions.BecomeUser);

            Guid gBrokerId, gUserId;
            {
                try { gBrokerId = new Guid(brokerId); }
                catch (FormatException) { return BecomeRequestResult.Error("Invalid Broker ID"); }

                try { gUserId = new Guid(userId); }
                catch (FormatException) { return BecomeRequestResult.Error("Invalid User ID"); }
            }

            if (!RequestHelper.BecomeUser(gBrokerId, gUserId, willRedirect: false, use2016Ui: false, initialUserId: PrincipalFactory.CurrentPrincipal.UserId, isRetailBUser: isRetail))
            {
                return BecomeRequestResult.Error("Unable to become user.");
            }

            var url = isRetail ? RequestHelper.BecomePmlUserUrl(gUserId, pmlLenderSiteId: Guid.Empty, initialUserId: PrincipalFactory.CurrentPrincipal.UserId, isRetail: true) : "~/los/main.aspx";
            return BecomeRequestResult.Success(url);
        }
        #endregion

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.Broker.FindUser.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        protected void PageLoad(object sender, System.EventArgs a)
        {
            RegisterResources();            
        }
        private string ErrorMessage
        {
            // Access member.
            set
            {
                if (value.EndsWith(".") == false)
                    ClientScript.RegisterHiddenField("m_ErrorMessage", value + ".");
                else
                    ClientScript.RegisterHiddenField("m_ErrorMessage", value);
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
		#endregion
    }
}
