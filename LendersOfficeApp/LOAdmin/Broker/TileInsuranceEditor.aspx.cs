﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.ObjLib.TitleProvider;
using LendersOffice.Common;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using MeridianLink.CommonControls;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class TileInsuranceEditor : SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        private Guid BrokerId
        {
            get { return RequestHelper.GetGuid("brokerid"); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            RegisterJsScript("utilities.js");
            TitleVendorAssociations.DataSource = TitleProvider.GetAllAssociationsWithBrokerInfo(BrokerId);
            TitleVendorAssociations.DataBind();
            EnableJquery = true;
        }

        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_7_1;
        }

        protected void TitleVendorAssociations_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }
            BrokerVendorAssociation info = (BrokerVendorAssociation)args.Item.DataItem;
            string cssClass = args.Item.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem";
            HtmlTableRow row = args.Item.FindControl("r") as HtmlTableRow;
            row.Attributes.Add("class", cssClass);
            row.Attributes.Add("data-id", info.VendorId.ToString());
            row.Attributes.Add("data-requirescc", info.VendorRequiresAccountId.ToString());

            EncodedLiteral name = (EncodedLiteral)args.Item.FindControl("Name");
            EncodedLiteral loginQuote = (EncodedLiteral)args.Item.FindControl("LoginQuote");
            EncodedLiteral passwordQuote = (EncodedLiteral)args.Item.FindControl("PasswordQuote");
            EncodedLiteral accountidQuote = (EncodedLiteral)args.Item.FindControl("AccountIDQuote");

            EncodedLiteral loginTitle = (EncodedLiteral)args.Item.FindControl("LoginTitle");
            EncodedLiteral passwordTitle = (EncodedLiteral)args.Item.FindControl("PasswordTitle");
            EncodedLiteral accountidTitle = (EncodedLiteral)args.Item.FindControl("AccountIDTitle");

            name.Text = info.VendorName;
            loginQuote.Text = info.UsernameQuote;
            accountidQuote.Text = info.AccountIdQuote; 

            if (info.HasBrokerInformation)
            {
                passwordQuote.Text = "******";
                passwordTitle.Text = "******";
            }

            loginTitle.Text = info.UsernameTitle;
            accountidTitle.Text = info.AccountIdTitle; 

        }

        [WebMethod]
        public static void SaveAssociation(Guid brokerId, int titleVendorId, string loginQuote, string passwordQuote, string accountIdQuote, string loginTitle, string passwordTitle, string accountIdTitle)
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            BrokerVendorAssociation association = new BrokerVendorAssociation(brokerId, titleVendorId);

            if (loginQuote.Length == 0 && loginTitle.Length == 0)
            {
                TitleProvider.DeleteAssociation(brokerId, titleVendorId);
                return;
            }

            if (passwordQuote != "******")
            {
                association.SetPassword(true, passwordQuote);
            }
            if (passwordTitle != "******")
            {
                association.SetPassword(false, passwordTitle);
            }


            association.AccountIdQuote = accountIdQuote;
            association.UsernameQuote = loginQuote;
            association.AccountIdTitle = accountIdTitle;
            association.UsernameTitle = loginTitle;

            TitleProvider.UpdateAssociation(association);
        }
    
    }
}
