﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="ImportBranchDMLogins.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.ImportBranchDMLogins" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="ml" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Import Branch-Level DocMagic Logins</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto;" onload="onInit();">
    <script type="text/javascript">
	 <!--

			function onInit()
			{
				<% if( IsPostBack == false ) { %>
				
				resize( 1024 , 768 );

				<% } %>

				if( document.getElementById("m_errorMessage") != null && document.getElementById("m_errorMessage").value != "" )
				{
					alert( document.getElementById("m_errorMessage").value );
					document.getElementById("m_errorMessage").value = "";
				}

				if( document.getElementById("m_feedBack") != null && document.getElementById("m_feedBack").value != "" )
				{
					alert( document.getElementById("m_feedBack").value );
					document.getElementById("m_feedBack").value = "";
				}
			}
			
			// -->

		</script>
		<h4 class="page-header">Import Branch-Level DocMagic Logins</h4>
		<form id="ImportBranchDMLogins" method="post" runat="server">
			<table cellpadding="3" cellspacing="2" width="1000px" height="700px" border="0">
				<tr>
					<td height="0%" colspan="2">
						<table cellspacing="0" cellpadding="0" width="1000px">
							<tr>
								<td width="0px" nowrap>
									
								</td>
								<td width="24px" nowrap>
								</td>
								<td width="0px" nowrap>
									<table cellpadding="0" cellspacing="4">
										<tr>
											<td class="FieldLabel" nowrap>
												Upload branch login data:
											</td>
											<td>
												<input id="m_File" type="file" style="width:400" runat="server" autocomplete="off" title="Browse for csv file to upload">
												<asp:Button id="m_Upload" runat="server" Text="Upload" OnClick="UploadClick"></asp:Button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="1000px" height="100%">
						<table cellpadding="0" cellspacing="0" height="100%" width="1000px">
							<tr>
								<td height="0%">
									<div style="PADDING: 8px; COLOR: dimgray; FONT: 11px arial;">
										Paste your branch docmagic login list as a comma-separated blob. &nbsp;<font color="red"><b>Don't 
												modify the first line</b></font> -- just follow that format.
									</div>
								</td>
							</tr>
							<tr>
								<td width=1000px height="100%">
									<asp:TextBox id="m_tbBranchDMLogins" runat="server" style="WIDTH: 1000px; HEIGHT: 100%; PADDING: 4px; FONT: 11px arial; BORDER: 2px solid lightgrey; BORDER-RIGHT: 0px; OVERFLOW-Y: scroll; " TextMode="MultiLine"></asp:TextBox>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="1000px" height="0%" ondblclick="copyStringToClipboard(this.innerText ); alert( 'Contents copied.' );">
						<asp:Panel id="m_ErrorList" runat="server" style="WIDTH: 100%; HEIGHT: 200px; PADDING: 4px; BORDER: 2px solid lightgrey; BORDER-RIGHT: 0px; OVERFLOW-Y: scroll;" Visible="False">
							<ml:CommonDataGrid id="m_Errors" runat="server">
								<Columns>
									<asp:BoundColumn DataField="LineNo" DataFormatString="{0} " HeaderText="Line #" ItemStyle-Width="40px"></asp:BoundColumn>
									<asp:BoundColumn DataField="Login" DataFormatString="{0} " HeaderText="Login"></asp:BoundColumn>
									<asp:BoundColumn DataField="Reason" HeaderText="Reason"></asp:BoundColumn>
								</Columns>
							</ml:CommonDataGrid>
						</asp:Panel>
					</td>
				</tr>
								<tr>
					<td width="1000px" height="0%" ondblclick="copyStringToClipboard( this.innerText ); alert( 'Contents copied.' );">
						<asp:Panel id="m_Warning" runat="server" style="WIDTH: 100%; HEIGHT: 200px; PADDING: 4px; BORDER: 2px solid yellow; BORDER-RIGHT: 0px; OVERFLOW-Y: scroll;" Visible="False">
							<ml:CommonDataGrid id="m_WarningList"  runat="server">
								<Columns>
									<asp:BoundColumn DataField="LineNo" DataFormatString="{0} " HeaderText="Line #" ItemStyle-Width="40px"></asp:BoundColumn>
									<asp:BoundColumn DataField="Login" DataFormatString="{0} " HeaderText="Login"></asp:BoundColumn>
									<asp:BoundColumn DataField="Reason" HeaderText="Reason"></asp:BoundColumn>
								</Columns>
							</ml:CommonDataGrid>
						</asp:Panel>
					</td>
				</tr>
				<tr>
					<td width=1000px height="0%" colspan="2">
						<div style="PADDING: 0px; TEXT-ALIGN: right;">
							<asp:Button id="m_Import" runat="server" style="DISPLAY: none;" OnClick="ImportClick"></asp:Button>
							<input type="button" value="Import" style="WIDTH: 50px;" onclick="if( parentElement.children[ 0 ].disabled == false ) { parentElement.children[ 0 ].click(); disabled = true; }">
							<input type="button" value="Close" style="WIDTH: 50px;" onclick="onClosePopup();">
						</div>
					</td>
				</tr>
			</table>
			<ml:CModalDlg id="m_ModalDlg" runat="server"></ml:CModalDlg>
		</form>
</body>
</html>
