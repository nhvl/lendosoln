﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DataAccess;
using LendersOfficeApp.los.admin;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using LendersOffice.Common;
using LendersOffice.Admin;
using LendersOffice.Common.TextImport;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class BatchUpdateUserStateLicenses : LendersOffice.Admin.SecuredAdminPage
    {
#if LQB_NET45
        protected global::System.Web.UI.HtmlControls.HtmlIframe exportToPdfIframe;
#else
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl exportToPdfIframe;
#endif

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker
            };
        }

        private Guid BrokerId
        {
            get
            {
                return new Guid(RequestHelper.GetSafeQueryString("BrokerId"));
            }
        }

        private EmployeeDB FindEmployee(string loginNm)
        {
            string userType;
            if (m_empTypeLO.Checked)
            {
                userType = "B";
            }
            else
            {
                userType = "P";
            }

            List<SqlParameter> spParams = new List<SqlParameter>();
            spParams.Add(new SqlParameter("@LoginNm", loginNm));
            spParams.Add(new SqlParameter("@Type", userType));

            Guid entryEmployeeId = Guid.Empty;
            Guid entryBrokerId = Guid.Empty;
            bool foundUser = false;

            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "FindUser", spParams))
            {
                while (reader.Read())
                {
                    string entryLoginNm = reader["LoginNm"].ToString();
                    entryBrokerId = (Guid)reader["BrokerId"];
                    if (string.Compare(loginNm, entryLoginNm, true) == 0 && BrokerId == entryBrokerId)
                    {
                        foundUser = true;
                        entryEmployeeId = (Guid)reader["EmployeeId"];
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            if (foundUser)
            {
                EmployeeDB empDb = new EmployeeDB(entryEmployeeId, entryBrokerId);
                empDb.Retrieve();
                return empDb;
            }
            else
            {
                return null;
            }
        }

        protected void UploadClick(object sender, System.EventArgs a)
        {
            StringBuilder errorList = new StringBuilder();
            string[] formats = { "MM/dd/yyyy", "M/dd/yyyy", "MM/d/yyyy", "M/d/yyyy" };
            // Check if csv is used
            if (Path.GetExtension(m_File.PostedFile.FileName) != ".csv")
            {
                m_errorData.Text = "Please use a .csv file following the format in the instructions below";
                return;
            }

            int loginNmCol = 0;
            int licenseNumCol = 1;
            int stateCol = 2;
            int expirationDateCol = 3;

            // Grab the file contents
            string tempFilename = TempFileUtils.NewTempFilePath() + Path.GetExtension(m_File.PostedFile.FileName);
            m_File.PostedFile.SaveAs(tempFilename);
            ILookup<int, string> parseErrors;
            DataTable fileContents = DataParsing.FileToDataTable(tempFilename, true, "", out parseErrors);

            if (parseErrors.Any())
            {
                foreach (var error in parseErrors)
                {
                    foreach (string errorMsg in error)
                    {
                        errorList.AppendLine("Error on line " + error.Key + ": " + errorMsg);
                    }
                }

                m_errorData.Text = errorList.ToString();
                return;
            }

            string outputFilename = TempFileUtils.NewTempFilePath();
            Dictionary<string, EmployeeDB> empDBList = new Dictionary<string, EmployeeDB>();
            
            using (StreamWriter sW = new StreamWriter(outputFilename, false))
            {
                sW.WriteLine("LoginNm,LicenseNum,State,ExpirationDate,Action,Reason");

                foreach (DataRow row in fileContents.Rows)
                {
                    string loginNm = row[loginNmCol].ToString().TrimWhitespaceAndBOM();
                    string licenseNum = row[licenseNumCol].ToString().TrimWhitespaceAndBOM();
                    string state = row[stateCol].ToString().TrimWhitespaceAndBOM().ToUpper();
                    string expirDate = row[expirationDateCol].ToString().TrimWhitespaceAndBOM();

                    // Check for invalid inputs
                    DateTime temp;
                    if (!LicenseInfoList.IsValidState(state) || !DateTime.TryParseExact(expirDate, formats, null, DateTimeStyles.None, out temp) 
                      || string.IsNullOrEmpty(licenseNum))
                    {
                        
                        sW.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", loginNm, licenseNum, state, expirDate, "Skip", "Field requirements not met"));
                        continue;
                    }

                    // Find user
                    EmployeeDB curEmpDb = null;
                    if (empDBList.ContainsKey(loginNm.ToLower()))
                    {
                        curEmpDb = empDBList[loginNm.ToLower()];
                    }
                    else
                    {
                        curEmpDb = FindEmployee(loginNm);
                        if (curEmpDb == null)
                        {
                            sW.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", loginNm, licenseNum, state, expirDate, "Skip", "User not found"));
                            continue;
                        }
                        empDBList.Add(loginNm.ToLower(), curEmpDb);
                    }


                    LicenseInfoList lList = curEmpDb.LicenseInformationList;        
                    if (lList.UpdateExpirationDate(licenseNum, state, expirDate))
                    {
                        // License and state match, just update
                        curEmpDb.IsDirty = true;
                        sW.WriteLine(string.Format("{0},{1},{2},{3},{4}", loginNm, licenseNum, state, expirDate, "Update"));
                    }
                    else
                    {
                        if (lList.IsFull)
                        {
                            sW.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", loginNm, licenseNum, state, expirDate, "Skip", "License list is full"));
                        }
                        else
                        {
                            try
                            {
                                LicenseInfo lInfo = new LicenseInfo(state, expirDate, licenseNum);
                                lList.Add(lInfo);
                                curEmpDb.IsDirty = true;
                                sW.WriteLine(string.Format("{0},{1},{2},{3},{4}", loginNm, licenseNum, state, expirDate, "Add"));
                            }
                            catch (CBaseException) // Unable to add to list.
                            {
                                sW.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", loginNm, licenseNum, state, expirDate, "Skip", "Unable to add license"));
                            }
                        }
                    }
                }

                sW.WriteLine();
                sW.WriteLine("Save Errors. Please retry these users again.");
                // Go through and save all changes at the end to keep from hitting DB per change made
                var enumerator = empDBList.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    EmployeeDB empDb = enumerator.Current.Value;
                    if (empDb.IsDirty)
                    {
                        try
                        {
                            empDb.Save(PrincipalFactory.CurrentPrincipal);
                        }
                        catch (CBaseException)
                        {
                            sW.WriteLine("Unable to save for user " + empDb.LoginName);
                        }
                    }
                }
            }
            RequestHelper.SendFileToClient(Context, outputFilename, "text/csv", "userUpdateResults.csv");
            Response.End();
        }
    }
}
