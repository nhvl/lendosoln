﻿using System;
using System.Collections.Generic;
using LendersOffice.Common;
using System.Xml;
using DataAccess;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class BrokerExport : LendersOffice.Admin.SecuredAdminPage
    {
        protected static Hashtable tableMap = new Hashtable();
        protected static bool IsColumnInfoUpdated = false;
        protected static bool IsWhitelistInit = false;
        protected static string[] tableNames = { "broker", "branch" };

        protected static ArrayList reviewWhiteList;
        protected static ArrayList protectedWhiteList;
        protected static ArrayList exportWhiteList;
        protected static ArrayList protectedDefaultValue;
        public static string whitelistMapAddr = "~/pml_shared/BrokerAndBranchWhitelist.xml";

        class ColumnInfo
        {
            public string dataType;
            public bool nullable;
            public bool isReadonly; // is readonly if true; WRITABLE if false
            public bool isForeignKey;
            public int curExportOption = 0;
            public ColumnInfo(string tDataType, bool tNullAble, bool tReadOnly) { dataType = tDataType; nullable = tNullAble; isReadonly = tReadOnly; this.isForeignKey = false; }
        }

        public static bool CheckReviewStatus()
        {
            return (reviewWhiteList.Count > 0) ? true : false;
        }

        public static void initWhiteList(string whitelistAddr)
        {
            using (XmlReader xmlReader = XmlReader.Create(whitelistAddr))
            {
                protectedWhiteList = new ArrayList();
                exportWhiteList = new ArrayList();
                reviewWhiteList = new ArrayList();
                protectedDefaultValue = new ArrayList();
                ArrayList curList = null;
                string curType = "";
                xmlReader.Read();
                while (xmlReader.Read())
                {
                    switch (xmlReader.Name)
                    {
                        case "Protect":
                            curList = protectedWhiteList; curType = "Protect"; break;
                        case "Export":
                            curList = exportWhiteList; curType = "Export"; break;
                        case "Review":
                            curList = reviewWhiteList; curType = "Review"; break;
                        default:
                            if (xmlReader.NodeType == System.Xml.XmlNodeType.Element && curList != null)
                            {
                                if (curType == "Protect")
                                {
                                    curList.Add(xmlReader.Name);
                                    protectedDefaultValue.Add(xmlReader.ReadElementString());
                                }
                                else
                                {
                                    curList.Add(xmlReader.Name);
                                }
                            }
                            break;
                    }
                }
            }

        }

        protected static void updateReviewSectionOfWhiteList(string whitelistAddr)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = ("    ");
            using (XmlWriter writer = XmlWriter.Create(whitelistAddr, settings))
            {
                writer.WriteStartElement("WhiteList");
                writer.WriteStartElement("Review");
                for (int i = 0; i < reviewWhiteList.Count; i++)
                {
                    writer.WriteElementString((string)reviewWhiteList[i], "");
                }
                writer.WriteEndElement();

                writer.WriteStartElement("Protect");
                for (int i = 0; i < protectedWhiteList.Count; i++)
                {
                    writer.WriteElementString((string)protectedWhiteList[i], (string)protectedDefaultValue[i]);
                }
                writer.WriteEndElement();

                writer.WriteStartElement("Export");
                for (int i = 0; i < exportWhiteList.Count; i++)
                {
                    writer.WriteElementString((string)exportWhiteList[i], "");
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static void initColumnInfo()
        {
            
            Hashtable columnMap;
            DataTable schema;//for checking if column is expression or readonly
            string tColName = "", tDataType = "";
            bool tNullable = false, tReadonly = false;
            for (int i = 0; i < tableNames.Length; i++)
            {
                columnMap = new Hashtable();

                // tableNames is hardcoded to good values, so we don't need to validate them
                string sql = "select Top 1 * from " + tableNames[i];

                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    while (reader.Read()) { }

                    schema = reader.GetSchemaTable();

                    foreach (DataRow row in schema.Rows)
                    {
                        foreach (DataColumn col in schema.Columns)
                        {
                            switch (col.ColumnName)
                            {
                                case "ColumnName":
                                    tColName = (string)row[col];
                                    break;
                                case "AllowDBNull":
                                    tNullable = (bool)row[col];
                                    break;
                                case "IsReadOnly":
                                    tReadonly = (bool)row[col];
                                    break;
                                case "DataTypeName":
                                    tDataType = (string)row[col];
                                    break;
                            }
                        }
                        if (!columnMap.Contains(tColName))
                            columnMap.Add(tColName, new ColumnInfo(tDataType, tNullable, tReadonly));

                    }
                };

                DBSelectUtility.ProcessDBData(DataSrc.LOShareROnly, sql, null, null, readHandler);

                SqlParameter[] parameters = { new SqlParameter("TableName", tableNames[i]) };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader_OBSOLETE_DO_NOT_USE("LoanFileColumnInfo", parameters))
                {
                    reader.NextResult();
                    while (reader.Read())
                    {
                        //Tools.LogInfo("ForeignKey: " + (string)reader["ForeignKey"] );
                        ((ColumnInfo)columnMap[(string)reader["ForeignKey"]]).isForeignKey = true;
                    }
                }

                tableMap.Add(tableNames[i], columnMap);
            }
        }

        public static string clearNullCharacter(string item)
        {
            int i;
            for (i = item.Length - 1; i >= 0; i--)
            {
                if (item[i] != 0x00)
                {
                    break;
                }
            }
            return item.Substring(0, i + 1);
        }

        private void ExportXml(string filename)
        {
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", $"attachment; filename=\"{filename}\"");
            Response.TransmitFile(filename);
            Response.End();
        }

        protected void UIBrokerXmlExport(object sender, EventArgs e)
        {
            bool review = false;
            string whitelistAddr = Tools.GetServerMapPath(BrokerExport.whitelistMapAddr);
            string xmlFileName = BrokerExport.ExportCurrentBrokerXml(ref review, whitelistAddr);
            if (review)
            {
                Response.Write(@"<script language='javascript'>alert('New columns need to be reviewed and decide whether they should be protected or exported. Please check ""Export White List""');</script>");
            }
            else
            {
                ExportXml(xmlFileName);
            }
        }

        protected static string ExportCurrentBrokerXml(ref bool beReview, string whitelistAddr)
        {
            if (!IsColumnInfoUpdated)
            {
                initColumnInfo();
                IsColumnInfoUpdated = true;
            }
            if (!IsWhitelistInit)
            {
                initWhiteList(whitelistAddr);
                IsWhitelistInit = true;
            }

            Guid brokerId = RequestHelper.GetGuid("brokerid");
            string brokerSqlCmd = "SELECT * FROM broker where brokerId = '" + brokerId + "'";
            const string CDataPattern = @"<.+>";
            string uniqueFileName = TempFileUtils.NewTempFilePath() + ".xml";
            List<string> columnNameList = null;
            Hashtable curTableMap = null;
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = ("    ");

            bool reviewSet = beReview;
            using (XmlWriter writer = XmlWriter.Create(uniqueFileName, settings))
            {
                writer.WriteStartElement("BrokerAndBranch");
                
                for (int fileIndex = 0; fileIndex < tableNames.Length; fileIndex++)
                {
                    curTableMap = (Hashtable)tableMap[tableNames[fileIndex]];

                    // Read column info
                    SqlParameter[] parameters = { new SqlParameter("TableName", tableNames[fileIndex]) };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader_OBSOLETE_DO_NOT_USE("LoanFileColumnInfo", parameters))
                    {
                        columnNameList = new List<string>();
                        while (reader.Read())
                        {
                            columnNameList.Add((string)reader["COLUMN_NAME"]);
                        }
                    }

                    var listParams = new SqlParameter[] { new SqlParameter("@brokerid", brokerId) };
                    var sql = "SELECT * FROM " + tableNames[fileIndex] + " WHERE brokerId = @brokerid";

                    Action<IDataReader> readHandler = delegate(IDataReader reader)
                    {
                        while (reader.Read())
                        {
                            writer.WriteStartElement(tableNames[fileIndex]);
                            if (tableNames[fileIndex].Equals("broker"))
                                writer.WriteAttributeString("brokerid", brokerId.ToString());
                            else
                                writer.WriteAttributeString("branchid", (reader["BranchId"]).ToString());
                            foreach (string colName in columnNameList)
                            {
                                string item = (reader[colName]).ToString();
                                int protectIndex = protectedWhiteList.IndexOf(colName);
                                if (protectIndex >= 0)
                                {
                                    item = (string)protectedDefaultValue[protectIndex];
                                }
                                int exportIndex = exportWhiteList.IndexOf(colName);
                                if (protectIndex < 0 && exportIndex < 0 && (!reviewWhiteList.Contains(colName)))
                                {
                                    reviewWhiteList.Add(colName);
                                    reviewSet = true;
                                    continue;
                                }

                                writer.WriteStartElement("tag");
                                writer.WriteAttributeString("n", colName);

                                /*if (colName.Contains("Password"))
                                {
                                    writer.WriteAttributeString("v", "");
                                }
                                else */
                                if (reader.IsDBNull(reader.GetOrdinal((string)colName)))
                                {
                                    if (!curTableMap.Contains((string)colName))
                                    {
                                        Tools.LogError("Justin: unable to find your column name " + (string)colName);
                                    }
                                    else if (((ColumnInfo)curTableMap[(string)colName]).nullable)
                                    {
                                        writer.WriteAttributeString("v", "null");
                                        writer.WriteAttributeString("isNullable", "1");
                                    }
                                    else
                                    {
                                        writer.WriteAttributeString("v", "");
                                    }
                                }
                                else if (item.TrimWhitespaceAndBOM() == "")
                                {
                                    writer.WriteAttributeString("v", "");
                                }
                                else if (System.Text.RegularExpressions.Regex.IsMatch(item, CDataPattern))
                                {
                                    try
                                    {
                                        item = clearNullCharacter(item);
                                        writer.WriteCData(item);
                                    }
                                    catch (Exception e)
                                    {
                                        Tools.LogInfo("Justin Test Exception" + e.ToString());
                                        Tools.LogInfo("The error CDATA TAG NAME->" + (string)colName);
                                        Tools.LogInfo("The error CDATA[value]->" + item);
                                        throw e;
                                    }
                                }
                                else writer.WriteAttributeString("v", item);

                                if (((ColumnInfo)curTableMap[(string)colName]).isReadonly)
                                {
                                    writer.WriteAttributeString("IsReadonly", "1");
                                }
                                writer.WriteEndElement();
                            }
                            writer.WriteEndElement();
                            writer.Flush();
                        }
                    };

                    DBSelectUtility.ProcessDBData(DataSrc.LOShareROnly, sql, null, listParams, readHandler);
                    beReview = reviewSet;
                }
                writer.WriteEndElement();
            }
            if (reviewWhiteList.Count > 0)
            {
                beReview = true;
            }
            updateReviewSectionOfWhiteList(whitelistAddr);
            return uniqueFileName;
        }


        //must set certain fields as symbol to indicate that update is allowed
        public static bool checkIfEligibleForUpdate(Guid BrokerId)
        {
            bool isEligible = true;

            var sqlParams = new SqlParameter[] { new SqlParameter("@brokerid", BrokerId) };
            var brokerEligibleSql = "SELECT BrokerNm, BrokerAddr FROM broker WHERE BrokerId = @brokerid";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    if (!((string)reader["BrokerNm"]).ToUpper().StartsWith("NUKE"))
                    {
                        isEligible = false;
                    }
                    if (!((string)reader["BrokerAddr"]).ToUpper().StartsWith("@_@ NUKE THIS"))
                    {
                        isEligible = false;
                    }
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LOShareROnly, brokerEligibleSql, null, sqlParams, readHandler);

            return isEligible;
        }

        static public byte[] ConvertBinaryLiteral(string binaryLiteral)
        {
            // I've made this public in order to run a unit test.
            // This is the sort of method that belongs in a library
            // class somewhere.  If I find more code that needs this
            // then I'll move it.  -- AD
            if (string.IsNullOrEmpty(binaryLiteral)) return null;
            if (binaryLiteral.Length < 3) return null;
            if (!binaryLiteral.StartsWith("0x")) return null; // SQL Server expects lower-case 'x', even if the DB is case-insensitive
            if ((binaryLiteral.Length % 2) > 0) return null;

            // I cannot locate a library method for this, and google gives
            // a bunch of sites that demonstrate code similar to this...
            int sourceIndex = 2;
            int resultIndex = 0;
            var converted = new byte[(binaryLiteral.Length - 2) / 2];
            while (sourceIndex < binaryLiteral.Length)
            {
                converted[resultIndex++] = byte.Parse(binaryLiteral.Substring(sourceIndex, 2), System.Globalization.NumberStyles.HexNumber);
                sourceIndex += 2;
            }
            return converted;
        }

        static private SqlParameter DataConvert(string paramName, string tableName, string columnName, object dataValue)
        {
            if (Convert.IsDBNull(dataValue)) return new SqlParameter(paramName, DBNull.Value);

            string dataType = ((ColumnInfo)(((Hashtable)(tableMap[tableName]))[columnName.ToString()])).dataType.ToLower();
            if (dataType.Contains("varbinary"))
            {
                var param = new SqlParameter(paramName, SqlDbType.VarBinary);
                param.Value = ConvertBinaryLiteral(dataValue as string);
                return param;
            }
            else
            {
                return DbAccessUtils.SqlParameterForVarchar(paramName, dataValue.ToString());
            }
        }

        /*
         * If the broker id exist, if not add a new broker with such id.
         * If so, keep on.
         */
        protected static bool importBroker(ref string returnMsg, string whitelistAddr, string importedFilePath)
        {
            if (!IsColumnInfoUpdated)
            {
                initColumnInfo();
                IsColumnInfoUpdated = true;
            }
            if (!IsWhitelistInit)
            {
                initWhiteList(whitelistAddr);
                IsWhitelistInit = true;
            }
            string committedStr = "";
            string importXmlBrokerId = null;
            XmlTextReader reader = new XmlTextReader(importedFilePath);
            try
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.HasAttributes)
                    {
                        reader.MoveToNextAttribute();
                        if (reader.Name.Equals("brokerid"))
                        {
                            importXmlBrokerId = reader.Value;
                            break;
                        }
                    }
                }
                if (importXmlBrokerId == null)
                {
                    returnMsg = "the brokerid attribute is missing in the broker tag";
                    return false;
                }
                else if (importXmlBrokerId != null && !checkIfEligibleForUpdate(new Guid(importXmlBrokerId)))
                {
                    returnMsg = "Please set the flag fields before any update";
                    return false;
                }

                

                bool isReadOnly;
                bool isNullable;
                bool isNoValue;
                string curTableName = "broker";
                string curFieldName = "";
                string curFieldValue = "";

                
                Hashtable curColMap = (Hashtable)tableMap["broker"];
                List<SqlParameter> curParaList = new List<SqlParameter>();
                List<List<SqlParameter>> sqlParaLists = new List<List<SqlParameter>>();
                while (reader.Read())
                {
                    switch(reader.NodeType)
                    {
                        case XmlNodeType.CDATA:
                            curParaList.Add(new SqlParameter(curFieldName, reader.Value));
                            break;

                        case XmlNodeType.EndElement:
                            if (reader.Name.ToLower() == "broker" || reader.Name.ToLower() == "branch")
                            {
                                sqlParaLists.Add(curParaList);
                                curParaList = new List<SqlParameter>();
                            }
                            break;

                        case XmlNodeType.Element:
                            if (reader.Name.ToLower() == "broker" || reader.Name.ToLower() == "branch")
                            {
                                curTableName = reader.Name.ToLower();
                                curColMap = (Hashtable) tableMap[curTableName];
                            }
                            else if (reader.HasAttributes)
                            {
                                isReadOnly = false;
                                isNullable = false;
                                isNoValue = true;
                                while (reader.MoveToNextAttribute())
                                {
                                    switch(reader.Name)
                                    {
                                        case "n":
                                            curFieldName = reader.Value;
                                            if (!curColMap.Contains(curFieldName))
                                            {
                                                returnMsg = "Field " + curFieldName + " is not found in Database";
                                                return false;
                                            }
                                            break;
                                        case "v":
                                            isNoValue = false;
                                            curFieldValue = reader.Value;
                                            break;
                                        case "isNullable":
                                            isNullable = true;
                                            break;
                                        case "IsReadonly":
                                            isReadOnly = true;
                                            break;
                                    }
                                }
                                if (isNoValue) { }
                                else if (curFieldValue.ToLower() == "null" && isNullable)
                                {
                                    curParaList.Add(new SqlParameter(curFieldName, DBNull.Value));
                                }
                                else if (!isReadOnly)
                                {
                                    if ( (curTableName == "broker" && curFieldName.ToLower() == "brokerid") ||
                                         (curTableName == "branch" && curFieldName.ToLower() == "branchid") )
                                         curParaList.Insert(0, new SqlParameter(curFieldName, Guid.Parse(curFieldValue)));
                                    else curParaList.Add(new SqlParameter(curFieldName, curFieldValue));
                                }
                            }
                            break;
                    }
                }

                foreach (List<SqlParameter> curlist in sqlParaLists)
                {
                    int cnt = 0;
                    string id = curlist[0].ToString().ToLower();
                    string checkIdExistSql = null;
                    StringBuilder headStr = new StringBuilder();
                    string tailStr = "";
                    StringBuilder importSqlCmd = new StringBuilder();
                    curTableName = (id == "brokerid") ? "broker" : "branch";

                    var listParams = new List<SqlParameter>();
                    if (id == "brokerid")
                    {
                        listParams.Add(new SqlParameter("@brokerid", curlist[0].Value));
                        checkIdExistSql = @"select count(*) as cnt from Broker where brokerid = @brokerid";
                    }
                    else
                    {
                        listParams.Add(new SqlParameter("@branchid", curlist[0].Value));
                        checkIdExistSql = @"select count(*) as cnt from Branch where branchid = @branchid";
                    }

                    Action<IDataReader> readHandler = delegate(IDataReader readerCheck)
                    {
                        readerCheck.Read();
                        cnt = Convert.ToInt32(readerCheck["cnt"].ToString());
                    };

                    DBSelectUtility.ProcessDBData(DataSrc.LOShareROnly, checkIdExistSql, null, listParams, readHandler);

                    if (cnt > 0) //exists, so just update
                    {
                        listParams = new List<SqlParameter>();
                        if (id == "brokerid")
                        {
                            listParams.Add(new SqlParameter("@brokerid", curlist[0].Value));
                            headStr.Append("UPDATE broker SET ");
                            tailStr = " WHERE brokerid = @brokerid";
                        }
                        else
                        {
                            listParams.Add(new SqlParameter("@branchid", curlist[0].Value));
                            headStr.Append("UPDATE branch SET ");
                            tailStr = " WHERE branchid = @branchid";
                        }

                        importSqlCmd.Append(headStr);
                        for (int i = 1; i < curlist.Count; i++)
                        {
                            int protectIndex = protectedWhiteList.IndexOf(curlist[i].ToString());
                            if (protectIndex >= 0) continue;

                            string paramName = "@p" + i.ToString();
                            listParams.Add(DataConvert(paramName, curTableName, curlist[i].ToString(), curlist[i].Value));
                            importSqlCmd.Append(curlist[i].ToString() + " = " + paramName + ", \n");
                        }
                        committedStr = importSqlCmd.ToString().TrimEnd().TrimEnd(',') + tailStr;

                        DBUpdateUtility.Update(DataSrc.LOShare, committedStr, null, listParams);
                    }
                    else //the id does not exist, insert it.
                    {
                        listParams = new List<SqlParameter>();

                        headStr.Append( (id == "brokerid") ? ("INSERT INTO broker ( ") : ("INSERT INTO branch ( ") );
                        tailStr = "";
                        importSqlCmd.Append(" VALUES ( ");
                        for (int i = 1; i < curlist.Count; i++ )
                        {
                            int protectIndex = protectedWhiteList.IndexOf(curlist[i].ToString());
                            if (protectIndex >= 0) continue;

                            string paramName = "@p" + i.ToString();
                            headStr.Append(curlist[i].ToString() + ", ");
                            listParams.Add(DataConvert(paramName, curTableName, curlist[i].ToString(), curlist[i].Value));
                            importSqlCmd.Append(paramName + ", ");
                        }
                        committedStr = headStr.ToString().TrimEnd().TrimEnd(',') + " )" + importSqlCmd.ToString().TrimEnd().TrimEnd(',') + " )" + tailStr;

                        DBInsertUtility.InsertGetGuid(DataSrc.LOShare, committedStr, null, listParams);
                    }
                }


            }
            catch (Exception e)
            {
                Tools.LogError("Justin: " + e.StackTrace + "\n\n" );
                Tools.LogError("Justin Sql: \n" + committedStr );
                throw e;
            }

            return true;
        }

        protected void ExportWhiteList(object sender, EventArgs e)
        {
            string FileName = Tools.GetServerMapPath(BrokerExport.whitelistMapAddr);
            ExportXml(FileName);
        }

        protected void ExportDefaultWhiteList(object sender, EventArgs e)
        {
            string FileName = BrokerExport.CreateDefaultWhiteList();
            ExportXml(FileName);
        }

        protected void UploadWhitelist_Click(object sender, EventArgs e)
        {
            string savePath = Tools.GetServerMapPath(BrokerExport.whitelistMapAddr);

            if (FileUpload3.HasFile)
            {
                int fileSize = FileUpload3.PostedFile.ContentLength;

                FileUpload3.SaveAs(savePath);

                UploadStatusLabel.Text = "Your file was uploaded successfully.";

                BrokerExport.initWhiteList(savePath);
            }
            else
            {
                UploadStatusLabel.Text = "You did not specify a file to upload.";
            }
        }

        public static string mainTag = "WhiteList";
        public static string CreateDefaultWhiteList()
        {
            ArrayList FieldNameList = new ArrayList();

            for (int i = 0; i < tableNames.Length; i++)
            {
                // system data is held in nvarchar strings
                var sql = string.Format("select COLUMN_NAME from information_schema.columns where table_name = N'{0}'", tableNames[i]);

                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    while (reader.Read())
                    {
                        if (!FieldNameList.Contains((string)reader["COLUMN_NAME"]))
                            FieldNameList.Add((string)reader["COLUMN_NAME"]);
                    }
                };

                DBSelectUtility.ProcessDBData(DataSrc.LOShareROnly, sql, null, null, readHandler);
            }

            //add field name && create a white list
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = ("    ");
            string uniqueFileName = TempFileUtils.NewTempFilePath() + ".xml";

            using (XmlWriter writer = XmlWriter.Create(uniqueFileName, settings))
            {
                writer.WriteStartElement(mainTag);
                writer.WriteElementString("Review", "");
                writer.WriteElementString("Protect", "");
                writer.WriteStartElement("Export");

                foreach (string tag in FieldNameList)
                    writer.WriteElementString(tag, "");

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.Flush();
            }

            return uniqueFileName;
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            string savePath = "";

            List<SqlParameter> sqlParameterList = new List<SqlParameter>();
            if (FileUpload1.HasFile)
            {
                int fileSize = FileUpload1.PostedFile.ContentLength;

                savePath = TempFileUtils.NewTempFilePath() + "_upload.xml";

                FileUpload1.SaveAs(savePath);

                UploadStatusLabel.Text = "Your file was uploaded successfully.";

                string msg = "";
                string whitelistPath = Tools.GetServerMapPath(BrokerExport.whitelistMapAddr);
                bool noError = BrokerExport.importBroker(ref msg, whitelistPath, savePath);
                if (!noError)
                {
                    Response.Write(@"<script language='javascript'>alert('The following errors have occurred: \n" + msg + " .');</script>");
                    UploadStatusLabel.Text = "File is not updated. Please correct the error and try again.";
                }
            }
            else
            {
                UploadStatusLabel.Text = "You did not specify a file to upload.";
            }
        }
    
    }
}
