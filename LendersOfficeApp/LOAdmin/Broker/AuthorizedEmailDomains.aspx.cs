﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;

    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Authorized Email Domains page.
    /// </summary>
    public partial class AuthorizedEmailDomains : SecuredAdminPage
    {        
        /// <summary>
        /// Gets the Broker ID from the query string.
        /// </summary>
        /// <value>The Broker ID.</value>
        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("brokerid", Guid.Empty);
            }
        }

        /// <summary>
        /// Gets a value indicating whether a javascript brower check
        /// requiring IE is added to the page.
        /// </summary>
        /// <remarks>
        /// Set to false because the browser check fails in IE11 edge mode, which
        /// is required by angular. This page should be cross compatable anyway.
        /// </remarks>
        /// <value>If true, add IE only check to page.</value>
        protected override bool EnableIEOnlyCheck
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Gets the required permission for the current LOAdmin page.
        /// </summary>
        /// <returns>Permission required to access this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        /// <summary>
        /// Handles Page_Init event.
        /// </summary>
        /// <param name="sender">Object that caused this event.</param>
        /// <param name="e">Event Arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;

            this.RegisterService("main", "/LOAdmin/Broker/AuthorizedEmailDomainsService.aspx");

            this.RegisterCSS("Broker/AuthorizedEmailDomains.css");
            this.RegisterJsScript("Broker/AuthorizedEmailDomains.js");

            this.RegisterJsScript("angular-1.5.5.min.js");
        }

        /// <summary>
        /// Handles Page_Load event.
        /// </summary>
        /// <param name="sender">Object that caused this event.</param>
        /// <param name="e">Event Arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsGlobalVariables("BrokerId", this.BrokerId);
        }
    }
}