<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx"%>
<%@ Page language="c#" Codebehind="BrokerEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.BrokerEdit" validateRequest="false" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOfficeApp.los.admin"%>
<%@ Import namespace="LendersOffice.Admin"%>
<%@ Import namespace="LendersOffice.Constants"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>BrokerEdit</title>
		<style type="text/css">
		    .indent
		    {
		        position: relative;
		        left: 30px;
		    }

            .indented {
                margin-left: 20px;
            }

            .clear
            {
                clear: both;
            }

            .section-left
            {
                width: 450px;
                float: left;
                border-right: 1px solid white;
            }

            .section-right
            {
                float: left;
                position: relative;
                left: 5px;
            }

		    .doc-vendor
		    {
		        margin-top: 10px;
		    }

		    .doc-vendor ul
		    {
		        list-style-type: none;
		        margin: 0px;
		        margin-left: 6px;
		        margin-top: 5px;
		    }

		    .doc-vendor div, .margin-left
		    {
		        margin-left: 10px;
		    }

		    .doc-vendor span.login-info
		    {
		        display: inline-block;
		        margin-right: 10px;
		    }

            .hidden {
                display: none;
            }

            .width50 {
                width: 50px;
            }

            #CenlarConfigurationTable {
                width: 878px;
            }

            #CenlarCustomReportName, #CenlarBatchExportName {
                width: 175px;
            }

            #CenlarDummyUserId {
                width: 250px;
            }

            #CenlarNotificationEmails {
                width: 400px;
                height: 50px;
            }
		</style>
	</HEAD>

	<BODY scroll="yes" MS_POSITIONING="FlowLayout">

		<script type="text/javascript">

            jQuery(function($){
                $('input:radio[name="EnableMfa"]').change(function()
		        {
		            calculateMfaUi(true);
		        });

		        $('#m_EnableOCR').click(function(){
		            var checked = $(this).is(':checked');
		            $('#OCRVendorsSpan').toggle(checked);

		            if (!checked){
		                $('#OCRVendors').val('00000000-0000-0000-0000-000000000000');
		            }
		        });
		        $('#m_EnableOCR').triggerHandler('click');
                $('.doc-vendor .enable input').change(function(){
                    $(this).closest('.doc-vendor')
                    .toggleClass('enabled', $(this).is(':checked'))
                    .find('div.vendor-config').toggle($(this).is(':checked'));
                }).change();

                $('.doc-vendor.primary .enable input').change(function(){
                    $(this).closest('.doc-vendor')
                           .next('.secondary')
                           .toggle($(this).is(':checked'));
                }).change();


                <%= AspxTools.JQuery(this.m_IsCreditUnion) %>.click(function (event) {
                    $(".creditUnionOptions, #SymitarCredentials").toggle(event.target.checked);
                });

                <%= AspxTools.JQuery(this.m_IsFederalCreditUnion) %>.click(function (event) {
                    if (event.target.checked) {
                        <%= AspxTools.JQuery(this.m_IsTaxExemptCreditUnion) %>.prop({ checked: true, disabled: true });
                    }
                    else {
                        <%= AspxTools.JQuery(this.m_IsTaxExemptCreditUnion) %>.prop('disabled', false);
                    }
                });

                $(".creditUnionOptions, #SymitarCredentials").toggle(<%= AspxTools.JsGetElementById(this.m_IsCreditUnion) %>.checked);
                if (<%= AspxTools.JsGetElementById(this.m_IsFederalCreditUnion) %>.checked) {
                    <%= AspxTools.JQuery(this.m_IsTaxExemptCreditUnion) %>.prop({ checked: true, disabled: true });
                }
                $('.rateMonitorService input[type="checkbox"]').change(f_IsEnableRateMonitorServiceChangeHandler).trigger("change");
                var $enableConversationLogForLoans = $j(<%= AspxTools.JsGetElementById(m_EnableConversationLogForLoans) %>);
                $enableConversationLogForLoans.change(f_OnToggleConversationLog).trigger('change');
                $('#docuSignRsaKeySection').hide();
            });

		function f_IsEnableRateMonitorServiceChangeHandler()
		{
		    var $needsRateMonitorService = $j('.needsRateMonitorService input[type="checkbox"]');
		    var $rateMonitorServiceCheckbox = $j('.rateMonitorService input[type="checkbox"]');

		    var rateMonitorServiceChecked = $rateMonitorServiceCheckbox.prop('checked');

		    $needsRateMonitorService.prop('disabled', false == rateMonitorServiceChecked); // attr only works 1.6.1+.
		    if(false === rateMonitorServiceChecked)
		    {
		        $needsRateMonitorService.prop("checked", false);
		    }
		}

		function f_OnToggleConversationLog()
		{
		    var $enableConversationLogElement =  $j(<%= AspxTools.JsGetElementById(m_EnableConversationLogForLoans) %>);
            var $enableConversationLogInTpoElement = $j(<%= AspxTools.JsGetElementById(m_EnableConversationLogForLoansInTpo) %>);

		    var enableConversationLog = $enableConversationLogElement.prop('checked');

		    if (!enableConversationLog)
		    {
		        $enableConversationLogInTpoElement.prop('checked', false).change();
		    }

		    $enableConversationLogInTpoElement.prop('disabled', !enableConversationLog);
		}

		function f_OnToggleConversationLogDisplayElement()
		{
            var $enableConversationLogValueElement = $j(<%= AspxTools.JsGetElementById(m_EnableConversationLogForLoans) %>);
		    var $enableConversationLogDisplayElement = $j('#viewOfEnableConversationLogForLoans');

		    var enableConversationLog = $enableConversationLogDisplayElement.prop('checked');

		    $enableConversationLogValueElement.val(enableConversationLog ? 'checked' : 'unchecked');
		}

		function onCalculateClosingCostInPml(el)
		{
		    var applyToGfe = <%= AspxTools.JsGetElementById(m_ApplyClosingCostsToGfeOnSubmission) %>;
		    if (el.checked) {
		        applyToGfe.disabled = false;
		    }
		    else {
		        applyToGfe.disabled = true;
		        applyToGfe.checked = false;
		    }
		}

		function getLockPeriodTextBoxes()
		{
		    return [
				<%=AspxTools.JsGetElementById(m_AvailableLockPeriod_1) %>,
				<%=AspxTools.JsGetElementById(m_AvailableLockPeriod_2) %>,
				<%=AspxTools.JsGetElementById(m_AvailableLockPeriod_3) %>,
				<%=AspxTools.JsGetElementById(m_AvailableLockPeriod_4) %>,
				<%=AspxTools.JsGetElementById(m_AvailableLockPeriod_5) %>,
				<%=AspxTools.JsGetElementById(m_AvailableLockPeriod_6) %>,
				<%=AspxTools.JsGetElementById(m_AvailableLockPeriod_7) %>,
				<%=AspxTools.JsGetElementById(m_AvailableLockPeriod_8) %>,
				<%=AspxTools.JsGetElementById(m_AvailableLockPeriod_9) %>
			];
		}
		function initLockPeriodTextBoxes()
		{
		    var numTextBoxes = <%=AspxTools.JsNumeric(ConstApp.BROKER_MAX_NUMBER_OF_LOCK_PERIODS) %>;
		    var tbWidth = 50;
		    var lps = getLockPeriodTextBoxes();
	        var cbEnabled = <%=AspxTools.JsGetElementById(m_IsEnabledLockPeriodDropdown)%>;

	        for(var i = 0; i < numTextBoxes; i++)
	        {
				var lp = lps[i];
	            lp.style.width = tbWidth;
	            lp.onblur = checkAllLockPeriodTBsForDisabling;
                lp.maxLength = 3;
	            lp.setAttribute('mask', '###');
				addEventHandler(lp, "keyup", mask_keyup, false);
                lp.readOnly = false === cbEnabled.checked;
	        }
	        onLockPeriodDropDownCheckboxClick(true);

	        cbEnabled.onclick = onLockPeriodDropDownCheckboxClick;
		}
		function checkAllLockPeriodTBsForDisabling(sender, args)
		{
		    //   if it's blank, and the others are blank, uncheck the "enable lock period dropdown" cb.
		    if(false == <%=AspxTools.JsGetElementById(m_IsEnabledLockPeriodDropdown)%>.checked)
		    {
		        return;
		    }
		    if (retrieveEventTarget(event).value == '' || retrieveEventTarget(event).value == '0')
		    {
		        var lps = getLockPeriodTextBoxes();
		        for(var i = 0; i < lps.length; i++)
		        {
		            if(lps[i].value != ''  && lps[i].value !='0')
		            {
		                return;
		            }
		        }
		        <%=AspxTools.JsGetElementById(m_IsEnabledLockPeriodDropdown)%>.click();
		    }
		}
		function onLockPeriodDropDownCheckboxClick(isInit)
		{
		    var cb = <%=AspxTools.JsGetElementById(m_IsEnabledLockPeriodDropdown)%>;
		    var lps = getLockPeriodTextBoxes();
		    var resetValues = [];
		    var disable = true;
		    if(cb.checked)
		    {
		        // set to defaults.
		       resetValues = ['15', '30', '45', '60', '75', '90', '', '', ''];
		       disable = false;
		    }
		    else{
		        // set to blanks.
		        resetValues = ['', '', '', '', '', '', '', '',''];
		    }
		    for(var i = 0; i < resetValues.length; i++)
		    {
		        if (!isInit){
		            lps[i].value = resetValues[i];
                }
		        lps[i].readOnly = disable;
		    }
		}
		function invalidate(msg, args)
		{
		    var msgLbl = <%=AspxTools.JsGetElementById(m_Message) %>;
		    msgLbl.textContent = msg;
		    msgLbl.style.color = "red";
		    args.IsValid = false;
		}

		function validateDriveFields(sender, args)
		{
		    var driveEnabledCB = <%=AspxTools.JsGetElementById(m_cbEnableDriveIntegration) %>;
		    var conditionImportingEnabledCB = <%=AspxTools.JsGetElementById(m_cbEnableDriveConditionImporting) %>;
		    var conditionCategoryID =<%=AspxTools.JsGetElementById(m_ddlDriveConditionCategory) %>.value;
            var driveDueDateCalcFieldVal =<%=AspxTools.JsGetElementById(DriveDueDateCalcField) %>.value;
            var permissionLevelID =<%=AspxTools.JsGetElementById(DrivePermissionLevelID) %>.value;
            var msgLbl = <%=AspxTools.JsGetElementById(m_Message) %>;
            var msgToShow = 'Save Failed. Please fill out all of the DRIVE integration fields.';
            if(driveEnabledCB.checked && conditionImportingEnabledCB.checked)
            {
                if(conditionCategoryID === null || conditionCategoryID === ''
                   || driveDueDateCalcFieldVal === null || driveDueDateCalcFieldVal === ''
                   || permissionLevelID === null || permissionLevelID === '')
                {
                    invalidate(msgToShow, args);
                    return;
                }
            }
		}
		function selectDriveDocType() {
		    var docTypePickerLink = "/newlos/ElectronicDocs/DocTypePicker.aspx";
            var queryString = "?showSystemFolders=true&brokerid="+g_brokerId;
            queryString += "&isDrive=true";
            showModal(docTypePickerLink + queryString, null, null, null, function(result){
				if (result && result.docTypeId && result.docTypeName) { // New doc type selected
					<%= AspxTools.JsGetElementById(m_selectedDriveDocType)%>.value = result.docTypeId;
					document.getElementById('lSelectedDocTypeSpan').textContent = result.docTypeName;
				}
			 });
        }


		function driveTaskPermissionPopup() {
            showModal("/newlos/Tasks/TaskPermissionPicker.aspx"
                + "?IsCond=" + true
                + '&brokerid=' + <%= AspxTools.JsString(m_brokerId.ToString()) %>
				+ '&IsTemplate=' + true
				,null, null, null, function(args){
					if (args.OK) {
						document.getElementById("DrivePermissionDescription").value = args.permissionDescription;
						document.getElementById("DrivePermissionLevelID").value = args.id;
						document.getElementById("DriveTaskPermission").innerText = args.name;
						// setTaskDirty(); //! update validators
					}
				});
		}

        function validateFloodFields(sender, args)
		{
		    var floodEnabledCB = <%=AspxTools.JsGetElementById(m_cbEnableFloodIntegration) %>;
		    var floodProviderDDLVal = <%=AspxTools.JsGetElementById(m_ddlFloodProvider) %>.value;
		    var floodDocType = <%=AspxTools.JsGetElementById(m_selectedFloodDocType) %>.value;
            var msgLbl = <%=AspxTools.JsGetElementById(m_Message) %>;
            var msgToShow = 'Save Failed. Please select a Flood Provider and an EDoc DocType for the Flood integration.';
            if(floodEnabledCB.checked)
            {
                if(floodProviderDDLVal === null || floodProviderDDLVal === ''
                   || floodDocType === null || floodDocType === '')
                {
                    invalidate(msgToShow, args);
                    return;
                }
            }
		}

        function selectFloodDocType() {
		    var docTypePickerLink = "/newlos/ElectronicDocs/DocTypePicker.aspx";
            var queryString = "?showSystemFolders=true&brokerid="+g_brokerId;
            queryString += "&isFlood=true";
            showModal(docTypePickerLink + queryString, null, null, null, function(result){
				if (result && result.docTypeId && result.docTypeName) { // New doc type selected
					<%= AspxTools.JsGetElementById(m_selectedFloodDocType)%>.value = result.docTypeId;
					document.getElementById('lSelectedFloodDocTypeSpan').textContent = result.docTypeName;
				}
			});
        }

        function editMIVendorConfiguration() {
            var MIVendorLink = "/LOAdmin/Broker/MIVendorEdit.aspx";
            var queryString = "?brokerid="+g_brokerId;
            showModal(MIVendorLink + queryString);
        }

        function onCenlarToggled() {
            if ($j('#IsCenlarEnabled').is(':checked')) {
                $j('#CenlarCustomReportNameRequiredIcon').show();
                $j('#CenlarBatchExportNameRequiredIcon').show();
                $j('#CenlarDummyUserIdRequiredIcon').show();
                $j('#CenlarNotificationEmailsRequiredIcon').show();
            } else {
                $j('#CenlarCustomReportNameRequiredIcon').hide();
                $j('#CenlarCustomReportNameErrorIcon').hide();

                $j('#CenlarBatchExportNameRequiredIcon').hide();
                $j('#CenlarBatchExportNameErrorIcon').hide();

                $j('#CenlarDummyUserIdRequiredIcon').hide();
                $j('#CenlarDummyUserIdErrorIcon').hide();

                $j('#CenlarNotificationEmailsRequiredIcon').hide();
                $j('#CenlarNotificationEmailsErrorIcon').hide();
            }
        }

        function validateCenlarFields(sender, args) {
            var hasError = false;
            if ($j('#IsCenlarEnabled').is(':checked')) {
                var cenlarCustomReportName = $j('#CenlarCustomReportName').val();
                if (cenlarCustomReportName === null || cenlarCustomReportName === '') {
                    $j('#CenlarCustomReportNameRequiredIcon').hide();
                    $j('#CenlarCustomReportNameErrorIcon').show();
                    hasError = true;
                }

                var cenlarBatchExportName = $j('#CenlarBatchExportName').val();
                if (cenlarBatchExportName === null || cenlarBatchExportName === '') {
                    $j('#CenlarBatchExportNameRequiredIcon').hide();
                    $j('#CenlarBatchExportNameErrorIcon').show();
                    hasError = true;
                }

                var cenlarDummyUserId = $j('#CenlarDummyUserId').val();
                if (cenlarDummyUserId === null || cenlarDummyUserId === '') {
                    $j('#CenlarDummyUserIdRequiredIcon').hide();
                    $j('#CenlarDummyUserIdErrorIcon').show();
                    hasError = true;
                }

                var cenlarNotificationEmails = $j('#CenlarNotificationEmails').val();
                if (cenlarNotificationEmails === null || cenlarNotificationEmails === '') {
                    $j('#CenlarNotificationEmailsRequiredIcon').hide();
                    $j('#CenlarNotificationEmailsErrorIcon').show();
                    hasError = true;
                }

                if (hasError) {
                    var msgToShow = 'Save Failed. Please ensure that all required fields are configured for the Cenlar integration.';
                    invalidate(msgToShow, args);
                }
                else {
                    $j('.CenlarRequiredIcon').show();
                    $j('.CenlarErrorIcon').hide();
                }
            }
        }

        function editCenlarCredentials(isBeta) {
            var url = ML.VirtualRoot
                + "/LOAdmin/Broker/CenlarCredentialEdit.aspx"
                + "?brokerId=" + g_brokerId
                + "&isBetaEnvironment=" + isBeta;

            LQBPopup.Show(url, {
                    hideCloseButton: true,
                    popupClasses: 'Popup',
                    width: 375,
                    height: 175
            }, null);
        }

		function editPmiConfiguration() {
		    showModal("/LOAdmin/Broker/PMIProviderEdit.aspx"
		        + "?genworth=" + document.getElementById('m_GenworthRanking').value
		        + "&mgic=" + document.getElementById('m_MgicRanking').value
		        + "&radian=" + document.getElementById('m_RadianRanking').value
		        + "&essent=" + document.getElementById('m_EssentRanking').value
		        + "&arch=" + document.getElementById('archRanking').value
		        + "&national=" + document.getElementById('m_NationalRanking').value
                + "&masshousing=" + document.getElementById('m_MassHousingRanking').value
		        + "&highwinner=" + document.getElementById('m_IsHighestPmiProviderWins').value
		        , null, null, null, function(args){
					if (args.OK) {
						document.getElementById("m_GenworthRanking").value = args.genworth;
						document.getElementById("m_MgicRanking").value = args.mgic;
						document.getElementById("m_RadianRanking").value = args.radian;
						document.getElementById("m_EssentRanking").value = args.essent;
						document.getElementById("archRanking").value = args.arch;
						document.getElementById("m_NationalRanking").value = args.national;
						document.getElementById("m_MassHousingRanking").value = args.masshousing;
						document.getElementById("m_IsHighestPmiProviderWins").value = args.highWinner;
						document.getElementById("m_Save").click();
					}
			});
		}

		function editCustomFieldXML() {
			var args = new cGenericArgumentObject();
			args.OK = false;
			args.inputDoc = document.getElementById("m_sCustomFieldDescriptionXml").value

		    showModal("/LOAdmin/Broker/CustomFieldXMLEdit.aspx", args, null, null, function(args){
				if (args.OK) {
					document.getElementById("m_sCustomFieldDescriptionXml").value = args.outputDoc;
				}
			});
		}

		function editConversationLogCategories()
		{
		    var baseLink = VRoot + '/LOAdmin/Broker/ConversationLogCategoriesEditor.aspx';
		    var queryString = "brokerid=" + g_brokerId;
		    var fullLink = baseLink + '?' + queryString;
		    window.open(fullLink, "Conversation Log Categories Editor");
		}

		function editConversationLogPermissionLevels()
		{
		    var baseLink = ML.convLogPermissionLevelsLink;
		    var queryString = "brokerid=" + g_brokerId;
		    var fullLink = baseLink + '?' + queryString;

		    var windowNameParts = ['PLs', ML.stageName, g_brokerId.replace(/-/g, '')];
		    var windowName = windowNameParts.join('');
		    window.open(fullLink, windowName, 'height=800,width=1050,menubar=no,scrollbars=yes,resizable=yes');
		    return false;
		}

		function driveDateCalculationPopup() {
            var offset = document.getElementById("DriveDueDateOffset");
            var field = document.getElementById("DriveDueDateCalcField");
            showModal("/newlos/Tasks/DateCalculator.aspx"
                + "?duration=" + offset.value
                + "&fieldId=" + field.value
				+ "&IsTaskTriggerTemplate=" + true
				, null, null, null, function(args){
				if (args.OK) {
					offset.value = args.offset;
					field.value = args.id;
					document.getElementById("DriveDueDateCalcDescription").innerText = args.dateDescription;
					//checkDueDateStatus();
					//setTaskDirty(); //! update validators
				}
			}, {hideCloseButton:true});
        }
        function f_ComplianceEagleLoginValidationFunction(source, args)
        {
            args.IsValid = f_ComplianceEagleValidationFunction(<%=AspxTools.JsGetElementById(ComplianceEagleLogin)%>, 'ComplianceEagleLogin_error_icon');
        }
        function f_ComplianceEaglePasswordValidationFunction(source, args)
        {
            args.IsValid = f_ComplianceEagleValidationFunction(<%=AspxTools.JsGetElementById(ComplianceEaglePassword)%>, 'ComplianceEaglePassword_error_icon');
        }
        function f_ComplianceEagleCompanyIDValidationFunction(source, args)
        {
            args.IsValid = f_ComplianceEagleValidationFunction(<%=AspxTools.JsGetElementById(ComplianceEagleCompanyID)%>, 'ComplianceEagleCompanyID_error_icon');
        }
        function f_ComplianceEagleValidationFunction(controlObject, errorIconID) { return f_ComplianceIntegrationValidationFunction(controlObject, errorIconID, <%=AspxTools.JsGetElementById(IsEnableComplianceEagleIntegration)%>.checked); }

        function f_CELoginValidationFunction(source, args)
        {
            args.IsValid = !<%=AspxTools.JsGetElementById(m_IsEnablePTMComplianceEaseIndicator) %>.checked || f_CEValidationFunction(<%=AspxTools.JsGetElementById(m_CELogin)%>, 'ceLogin_error_icon');
        }
        function f_CEPasswordValidationFunction(source, args)
        {
            args.IsValid = !<%=AspxTools.JsGetElementById(m_IsEnablePTMComplianceEaseIndicator) %>.checked || f_CEValidationFunction(<%=AspxTools.JsGetElementById(m_CEPassword)%>, 'cePassword_error_icon');
        }
        function f_CEValidationFunction(controlObject, errorIconID) { return f_ComplianceIntegrationValidationFunction(controlObject, errorIconID, <%=AspxTools.JsGetElementById(IsEnableComplianceEaseIntegration)%>.checked); }

		function f_ComplianceIntegrationValidationFunction(controlObject, errorIconID, isIntegrationEnabled) // returns isValid
		{
		    if(isIntegrationEnabled)
	        {
	            if(controlObject.value == '')
	            {
	                var errorIcon = document.getElementById(errorIconID);
	                errorIcon.style.display = '';
	                return false;
	            }
	            else{
	                return true;
	            }
	        }
	        else{
	            return true;
	        }
		}

		function onBillingVersionChange(){
	        var displayType = '';
	        if(<%=AspxTools.JsGetElementById(m_ddlBillingVersion)%>.value == <%=AspxTools.JsString(E_BrokerBillingVersion.PerTransaction.ToString())%>)
	        {
	            $j('#PtmOptions').show();
	        }
	        else {
	            $j('#PtmOptions').hide();
	        }
		}
		function onIsUseNewConditionChange() {
		    var newConditionCB = <%=AspxTools.JsGetElementById(m_IsUseNewCondition)%>;
		    var staticConditionCB = <%=AspxTools.JsGetElementById(m_IsUseNewTaskSystemStaticConditionIds)%>;
		    if (newConditionCB.checked) {
		        staticConditionCB.disabled = false;
		        staticConditionCB.parentNode.disabled = "";
		    } else {
		        staticConditionCB.disabled = true;
		        staticConditionCB.parentNode.disabled = "disabled";
		    }

		}


    function importRedir(type) {
        try {
            var redirect = "/LOAdmin/Broker/ImportBrokerAttributes.aspx?brokerid=" + <%= AspxTools.JsString(m_brokerId) %> + "&brokernm=" + <%=AspxTools.JsString(m_Name.Text) %>+ "&type=" + type;
            showModal(redirect, null, null, null, function(result){
				if (result.OK == true) {
					document.location.reload();
				}
			});
        }
        catch (e) {
            window.alert(e.message);
        }
    }

    var g_brokerId = <%= AspxTools.JsString(m_brokerId) %>;
    function copyToClipboard(o) {
	    o.focus();
	    o.select();
	    o.createTextRange().execCommand("copy");
    }


  function copyTextToClipboard(txt)
  {
      copyStringToClipboard(txt);
  }
function removeCRA(index) {
  document.getElementById("CRA" + index).selectedIndex = 0;
  document.getElementById("IsPdfViewNeeded" + index).checked = false;
  document.getElementById("IsPdfViewNeeded" + index).disabled = true;
}
function onCreditReportProtocolChange(ddl, index)
{
  var cbIsPdfViewNeeded = document.getElementById("IsPdfViewNeeded" + index);


  var bIsNonMcl = f_isNonMcl(ddl.value);
  cbIsPdfViewNeeded.disabled = bIsNonMcl;

  if (bIsNonMcl) {
    cbIsPdfViewNeeded.checked = false;
  }
}
function f_isNonMcl(id) {
  for (var i = 0; i < g_aNonMclList.length; i++)
    if (id == g_aNonMclList[i]) return true;
  return false;
}


    function onShowUsers() { try
    {
        // Redirect.

	    showModal('/LOAdmin/Broker/UserList.aspx?brokerid=' + g_brokerId + "&brokernm=" + <%=AspxTools.JsString(m_Name.Text) %>, null, null, null, function(result){
			if( result.OK == true && result.commandName != null && result.commandArgs != null )
			{
				// Propagate to top level window.

				var args = window.dialogArguments || {};

				args.commandName = result.commandName;
				args.commandArgs = result.commandArgs;

				args.OK = true;

				onClosePopup(args);
			}
		 });
    }
    catch( e )
    {
    }}

    function onShowBranches() { try
    {
        // Redirect.

	    showModal('/LOAdmin/Broker/EditBranch.aspx?brokerId=' + g_brokerId);
    }
    catch( e )
    {
    }}


	function onRoundLpeFeeClick()
	{
		if(<%= AspxTools.JsGetElementById(m_roundUpLpeFee) %>.checked == true)
			<%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>.readOnly = false;
		else
		{
			var rangeValidator = <%= AspxTools.JsGetElementById(m_rangeValRoundLpeFeeInterval) %>;
			if (rangeValidator && !rangeValidator.isvalid)
			{
				<%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>.value = ".050";
				if(typeof(ValidatorValidate) == 'function')
					ValidatorValidate(rangeValidator);
			}
			<%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>.readOnly = true;
		}
	}

	function FormatRoundInterval()
	{
		var interval = <%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>;
		var intervalVal = interval.value;
		var rangeValidator = <%= AspxTools.JsGetElementById(m_rangeValRoundLpeFeeInterval) %>;
		if( (intervalVal.replace(/^\s*|\s*$/g,"") == ""))
		{
			interval.value = "0.000";

			if (rangeValidator && typeof(ValidatorValidate) == 'function')
				ValidatorValidate(rangeValidator);
		}
		else
		{
			var index = intervalVal.indexOf('.');
			if(index >= 0)
			{
				if(intervalVal.substring(0, index) > 0 || intervalVal.indexOf('-') >= 0)
					return;
				var intervalLength = intervalVal.length;
				interval.value = ("0" + intervalVal.substring(index, intervalLength)).substring(0, 5);
			}
		}
	}

	// This link should be enabled when the pricing engine is enabled
	// with the new pml ui and the broker is saved.
	var bValidBrokerId = g_brokerId != <%= AspxTools.JsString(Guid.Empty) %>;
	function toggleConfigureCustomPMLFieldsLink() {
	    var bPEEnabled = document.getElementById("m_enableFeaturePriceMyLoanCB").checked;
	    var bNewPmlEnabled = document.getElementById("m_IsNewPmlUIEnabled_Yes").checked;
	    setDisabledAttr(document.getElementById("m_ConfigureCustomPMLFieldsLink"), !(bPEEnabled && bNewPmlEnabled && bValidBrokerId));
	}

	function calculateMfaUi(isChange)
	{
	    var optInMfaBtn = $j("#IsOptInMultiFactorAuthentication");
	    var optOutMfaBtn = $j("#IsOptOutMultiFactorAuthentication");
	    var employeeMfaBox = $j("#IsEnableMultiFactorAuthentication");
	    var tpoMfaBox = $j("#IsEnableMultiFactorAuthenticationTPO");
	    var registerBox = $j("#AllowDeviceRegistrationViaAuthCodePage");
	    var optOutRequestedBy = $j("#OptOutMfaName");
	    var optOutDate = $j("#OptOutMfaDate");

	    var isMfaEnabled = optInMfaBtn.is(":checked");
	    optOutMfaBtn.prop('checked', !isMfaEnabled);
	    employeeMfaBox.prop('disabled', !isMfaEnabled);
	    employeeMfaBox.prop('checked', isMfaEnabled);
	    tpoMfaBox.prop('disabled', !isMfaEnabled);
	    tpoMfaBox.prop('checked', isMfaEnabled);
	    registerBox.prop('disabled', !isMfaEnabled);
	    optOutRequestedBy.prop('readonly', isMfaEnabled);
	    optOutDate.prop('readonly', isMfaEnabled);

	    $j(".optOutRequired").toggle(!isMfaEnabled);

	    if(isChange)
	    {
	        registerBox.prop('checked', isMfaEnabled);
	    }
	}

	function Validate()
	{
	    var optInMfaBtn = $j("#IsOptInMultiFactorAuthentication");
	    var optOutRequestedBy = $j("#OptOutMfaName");
	    var optOutDate = $j("#OptOutMfaDate");

	    var isMfaEnabled = optInMfaBtn.is(":checked");

	    if(!isMfaEnabled && (optOutRequestedBy.val() == "" || optOutDate.val() == ""))
	    {
	        alert("Please fill out all fields under the Opt Out of MFA setting.");
	        return;
	    }

	    var pmlEnabled = $j("#m_enableFeaturePriceMyLoanCB").is(":checked");
	    var newPmlEnabled = $j("#m_IsNewPmlUIEnabled_Yes").is(":checked");
	    var qpEnabled = $j("#m_IsQuickPriceEnabled").is(":checked");
	    var isUseQP2 = $j("#m_Pml2AsQuickPricerMode").val() == "2";
	    var enabledAdvancedFilter = $j("#TempOptionXmlContent").val().indexOf("<option name=\"EnableAdvancedFilterOptionsForPriceEngineResults\" value=\"true\" />");
	    if(enabledAdvancedFilter != -1)
	    {
	        if((pmlEnabled && !newPmlEnabled) ||
               (qpEnabled && !isUseQP2))
	        {
	            alert("CANNOT SAVE SETTINGS!! \n\nThe temporary broker bit to enable the advanced filter options requires that the New PML UI be enabled and that, if Quick Pricer is enabled, then QP 2.0 should be used.");
	            return;
	        }
	    }

	    var suiteTypeValue = $j('#SuiteType').val();
	    if (suiteTypeValue === ML.BlankSuiteType) {
	        alert(ML.InvalidBrokerSuiteTypeMessage);
            return;
        }

        var docVendorWarning = "Modifying Doc Vendor info can affect Workflow. Please check this broker's workflow configuration before making any changes. Are you certain you want to save these changes?"
        if (IsDocVendorListDirty() && !confirm(docVendorWarning)) {
            return;
        }

	    $j("#m_Save").click();
	}



	function _init() { try
	{
	    var el = <%= AspxTools.JsGetElementById(m_CalculateclosingCostInPML) %>;
	    if (el) {
	        onCalculateClosingCostInPml(el);
	    }
	    initLockPeriodTextBoxes();
		f_renderTurnBrokerOnOff();
		f_renderNhcButtons();
		onRoundLpeFeeClick();
		onUseCustomISInfoClick();
		calculateMfaUi(false);
		f_onEdmsClick( true );

		    addEventHandler(<%=AspxTools.JsGetElementById(m_ddlBillingVersion)%>, 'change', onBillingVersionChange, false);
		    onBillingVersionChange();

		    addEventHandler(<%=AspxTools.JsGetElementById(m_IsUseNewCondition)%>, 'click', onIsUseNewConditionChange, false);
		onIsUseNewConditionChange();

        handleOriginationData();
		for (var index = 0; index < 10; index++)
		{
			onCreditReportProtocolChange(document.getElementById("CRA" + index), index);
		}

		// Look for error and report if found.

		if( document.getElementById("m_ErrorMessage") != null )
		{
			alert( "Error: " + document.getElementById("m_ErrorMessage").value );
		}

		resize(1024, 1024); // Internal user should have better resolution than 800x600

		<% if (m_enableFeaturePriceMyLoanCB.Checked) { %>
			var lenderDefaultBox = <%= AspxTools.JsGetElementById(m_hasLenderDefaultFeatures) %>;
			lenderDefaultBox.disabled = true;
		<% } %>

		if (document.getElementById("m_IsForceLeadToLoanNewLoanNumber").checked) {
		    document.getElementById("m_IsForceLeadToUseLoanCounter").disabled = true;
		    document.getElementById("m_IsForceLeadToLoanRemoveLeadPrefix").disabled = true;
		}
		else if (document.getElementById("m_IsForceLeadToLoanRemoveLeadPrefix").checked) {
		    document.getElementById("m_IsForceLeadToUseLoanCounter").disabled = true;
		    document.getElementById("m_IsForceLeadToLoanNewLoanNumber").disabled = true;
		}
		else if (document.getElementById("m_IsForceLeadToUseLoanCounter").checked) {
		    document.getElementById("m_IsForceLeadToLoanRemoveLeadPrefix").disabled = true;
		    document.getElementById("m_IsForceLeadToLoanNewLoanNumber").disabled = true;
		}

		//OPM 112346
		/*
		// If they have the new PML UI enabled, then disable the QuickPricer CB
		// and enable the Configure Custom PML Fields link
		if (document.getElementById("m_IsNewPmlUIEnabled_Yes").checked &&
		    document.getElementById("m_enableFeaturePriceMyLoanCB").checked) {
		        document.getElementById("m_IsQuickPriceEnabled").disabled = true;
		        document.getElementById("m_IsQuickPriceEnabled").checked = false;
		}
		*/

		toggleConfigureCustomPMLFieldsLink();
		setDisabledAttr($j("#m_isTrackModifiedFile_Configure"), !bValidBrokerId);

		on_cbDMSeamless_clicked();
		<%=AspxTools.JsGetElementById(IsEnableComplianceEagleIntegration)%>.onclick = on_cbComplianceEagle_clicked;
		on_cbComplianceEagle_clicked();
		<%=AspxTools.JsGetElementById(m_IsEnablePTMComplianceEaseIndicator)%>.onclick = on_cbCESeamless_clicked;
		on_cbCESeamless_clicked();

		f_onEnableNightlySeqNum();
		f_onIsAutoGenerateTradeNumsClick();

		setSymitarRowVisibility();

		onCenlarToggled();
	}
	catch( e )
	{
		window.status = e.message;
	}}

	function on_cbDMSeamless_clicked()
	{
	    var seamlessDocInterfaceEnabled = <%=AspxTools.JsGetElementById(m_IsEnablePTMDocMagicSeamlessInterface) %>;
	    var docVendorListDiv = document.getElementById('DocVendorListDiv');
	    if(false == seamlessDocInterfaceEnabled.checked)
	    {
	        docVendorListDiv.style.display = "none";
	    }
	    else
	    {
	        docVendorListDiv.style.display = "inline";
	    }
	}

    function on_cbComplianceEagle_clicked()
    {
        var cb = <%=AspxTools.JsGetElementById(IsEnableComplianceEagleIntegration)%>;
        var requiredIcons = [ document.getElementById('ComplianceEagleLogin_required_icon'),
                              document.getElementById('ComplianceEaglePassword_required_icon'),
                              document.getElementById('ComplianceEagleCompanyID_required_icon')];
        var displaySetting = cb.checked ? 'inline' : 'none';
        for (var i = 0; i < requiredIcons.length; ++i) {
            if (requiredIcons[i]) requiredIcons[i].style.display = displaySetting;
        }
    }

	function on_cbCESeamless_clicked()
	{
	    var cb = <%=AspxTools.JsGetElementById(m_IsEnablePTMComplianceEaseIndicator)%>;
	    var lgn = document.getElementById('ceLogin_required_icon');
	    var pw = document.getElementById('cePassword_required_icon');
	    if(false == cb.checked)
	    {
	        lgn.style.display = 'none';
	        if(pw){pw.style.display = 'none'};
	    }
	    else{
	        lgn.style.display = 'inline';
	        if(pw){pw.style.display = 'inline'};
	    }
	}

    function handleOriginationData(){

		var migrationDate = document.getElementById('<%= AspxTools.ClientId(m_OriginatorCompensationMigrationDate) %>');
		var applyTpo = document.getElementById('<%= AspxTools.ClientId(m_OriginatorCompensationApplyMinYSPForTPOLoan) %>');

		if( migrationDate.value == '' ){
		    applyTpo.checked = false;
		    applyTpo.disabled = true;
		}

		else {
		    applyTpo.disabled = false;
		}
    }
  function f_enableBroker(bEnable) {
    var action = bEnable ? 'TURN ON' : 'TURN OFF';
    if (confirm('Do you want to ' + action + ' this broker? This operation might take a long time for broker with large number of users. It best to perform after normal business hour.')) {
      var args = new Object();
      args["BrokerId"] = <%= AspxTools.JsString(m_brokerId) %>;
      args["Status"] = bEnable ? '1' : '0';

      var result = gService.main.call("ChangeBrokerStatus", args, true, true);
      if (!result.error) {
        document.forms[0]["BrokerStatus"].value = bEnable ? "1" : "0";
        f_renderTurnBrokerOnOff();
        alert('Successful. You still need to save other information for broker.');
      } else {
        alert('Error: ' + result.UserMessage);
      }
    }
  }

  function f_renderTurnBrokerOnOff() {
    var isActive = document.forms[0]["BrokerStatus"].value == "1";
    document.getElementById("StatusLabel").innerText = isActive ? "Yes" : "No";
    if (!isActive)
      document.getElementById("InactiveMsg").style.display = "";
    else
		document.getElementById("InactiveMsg").style.display = "none";

    <% if (((Guid)m_brokerId) == Guid.Empty) { %>
      document.getElementById("btnEnabled").disabled = true;
      document.getElementById("btnDisabled").disabled = true;
    <% } else { %>
      document.getElementById("btnEnabled").disabled = isActive;
      document.getElementById("btnDisabled").disabled = !isActive;

    <% } %>
  }
  function f_testDataTracConnection()
  {
	var args = new Object();
	args["pathToDataTrac"] = document.getElementById("m_pathToDataTrac").value;
	args["webserviceURL"] = document.getElementById("m_dataTracWebserviceURL").value;
	var result = gService.main.call("TestDataTracConnection", args, true, true);
    if(result.value && result.value["ErrorMessage"])
	{
		alert("FAILED: \n\n" + result.value["ErrorMessage"]);
	}
    else
    {
      alert("Success");
    }
  }

  function f_generateGuid() {
    var args = new Object();

    var result = gService.main.call("GenerateGuid", args, true, true);
    if (!result.error) {
      <%= AspxTools.JsGetElementById(m_NHCKey) %>.value = result.value["Guid"];
    }
    f_renderNhcButtons();
  }
  function f_putGuidEmpty() {
    <%= AspxTools.JsGetElementById(m_NHCKey) %>.value = <%= AspxTools.JsString(Guid.Empty) %>;
    f_renderNhcButtons();
  }
  function f_renderNhcButtons() {
    var value = <%= AspxTools.JsGetElementById(m_NHCKey) %>.value;

    if (value == <%= AspxTools.JsString(Guid.Empty) %>) {
      document.getElementById("btnGenerateNHCKey").disabled = false;
      document.getElementById("btnClearNHCKey").disabled = true;
    } else {
      document.getElementById("btnGenerateNHCKey").disabled = true;
      document.getElementById("btnClearNHCKey").disabled = false;
    }
  }
	function showLicenseList(num)
	{
	    if ( num === 1 )
	    {
		    showModal('/loadmin/broker/LicenseList.aspx?brokerId=' + g_brokerId) ;
        }
	}
	function onPricingEngineEnabledChange(isChecked) {
	    // If the user unchecks this and the Enable Quick Pricer link was previously disabled
	    // because of New PML UI selection, enable it
	    // If it is checked and the New Pml UI checked, then disable
	    if (!isChecked && document.getElementById("m_IsNewPmlUIEnabled_Yes").checked) {
	        document.getElementById("m_IsQuickPriceEnabled").disabled = false;
	    }
	    else if (isChecked && document.getElementById("m_IsNewPmlUIEnabled_Yes").checked) {
	        document.getElementById("m_IsQuickPriceEnabled").disabled = true;
	    }

	    toggleConfigureCustomPMLFieldsLink();

	    checkLenderDefault(isChecked);
	}

	function checkLenderDefault(isChecked)
	{
		var lenderDefaultBox = <%= AspxTools.JsGetElementById(m_hasLenderDefaultFeatures) %>;
		if(isChecked)
		{
			if(!lenderDefaultBox.checked)
			{
				lenderDefaultBox.click();
			}
			lenderDefaultBox.disabled = true;
		}
		else
		{
			if(lenderDefaultBox.checked)
			{
				var msg = "Warning - You have disabled the PriceMyLoan/PricingEngine Feature, but Lender Default Features are still enabled.";
				alert(msg);
			}
			lenderDefaultBox.disabled = false;
		}
	}

	function onUseCustomISInfoClick()
	{
		var useCustomInfo = <%= AspxTools.JsGetElementById(m_UseCustomISInfo) %>.checked;
		<%= AspxTools.JsGetElementById(m_CustomISPhone) %>.readOnly = !useCustomInfo;
		<%= AspxTools.JsGetElementById(m_CustomISEmail) %>.readOnly = !useCustomInfo;
	}

	function f_show(showMsg, event)
    {
		var msg = document.getElementById("showDetails");

		var label = document.getElementById("detailLabel");

	    if(showMsg == "LockDeskExpiredDetails")
			label.innerText = "This is only for LendingQB users, such as Lock Desks, with \"Rate lock requests for possibly expired rates\" permission set to \"Not allowed after investor cut-off time\" (see permissions tab of Employee editor).  This message will be displayed as a warning but will not prevent a rate lock.";
		else if(showMsg == "LockDeskCutOffDetails")
			label.innerText = "This is only for LendingQB users, such as Lock Desks, with \"Rate lock requests for possibly expired rates\" permission set to \"Not allowed after investor cut-off time\" (see permissions tab of Employee editor).";
		else
			label.innerText = "";

		msg.style.display = "";
		msg.style.visibility = 'visible';
			msg.style.top = (event.clientY + document.body.scrollTop - 10)+ "px";
			msg.style.left = (event.clientX + 10 ) + "px";

    }
    function f_close(closeMsg)
    {

		var msg = document.getElementById(closeMsg);
		msg.style.display = 'none';
		msg.style.visibility='hidden';

    }
    function f_showDefault(showMsg, event)
    {
		var label = document.getElementById("defaultLabel");
		label.innerText="This is the default warning";
		var msg = document.getElementById("defaultView");

		if(showMsg == "NormalUserExpiredDetails")
			label.innerText = <%= AspxTools.JsString(m_defaultNormalUserExpiredMessage) %>;
		else if(showMsg == "LockDeskExpiredDetails")
			label.innerText = <%= AspxTools.JsString(m_defaultLockDeskExpiredMessage) %>;
		else if(showMsg == "NormalUserCutOffDetails")
			label.innerText = <%= AspxTools.JsString(m_defaultNormalUserCutOffMessage)%> + '\nNote: {0} is used to include investor cut-off time';
		else if(showMsg == "LockDeskCutOffDetails")
			label.innerText = <%= AspxTools.JsString(m_defaultLockDeskCutOffMessage)%> + '\nNote: {0} is used to include investor cut-off time';
		else if(showMsg == "OutsideNormalHoursDetails")
			label.innerText = <%= AspxTools.JsString(m_defaultOutsideNormalHoursMessage)%> + '\nNote: {0} is used to include regular lock desk hours';
		else if(showMsg == "OutsideClosureDayHourDetails")
			label.innerText = <%= AspxTools.JsString(m_defaultOutsideClosureDayHourMessage) %>;
		else if(showMsg == "OutsideNormalHoursAndPassInvestorCutOffDetails")
			label.innerText = <%= AspxTools.JsString(m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage)%>;
		else
			label.innerText = "";

		msg.style.display = "";
		msg.style.visibility = 'visible';
		msg.style.top = (event.clientY + document.body.scrollTop - 10)+ "px";
		msg.style.left = (event.clientX + 10 ) + "px";
    }
    function f_setDefault(showMsg)
    {


		if(showMsg == "NormalUserExpiredDetails")
			document.getElementById("m_newNormalUserExpiredMessage").value = <%= AspxTools.JsString(m_defaultNormalUserExpiredMessage) %>;
		else if(showMsg == "LockDeskExpiredDetails")
			document.getElementById("m_newLockDeskExpiredMessage").value = <%= AspxTools.JsString(m_defaultLockDeskExpiredMessage) %>;
		else if(showMsg == "NormalUserCutOffDetails")
			document.getElementById("m_newNormalUserCutOffMessage").value = <%= AspxTools.JsString(m_defaultNormalUserCutOffMessage) %>;
		else if(showMsg == "LockDeskCutOffDetails")
			document.getElementById("m_newLockDeskCutOffMessage").value = <%= AspxTools.JsString(m_defaultLockDeskCutOffMessage) %>;
		else if(showMsg == "OutsideNormalHoursDetails")
			document.getElementById("m_newOutsideNormalHoursMessage").value = <%= AspxTools.JsString(m_defaultOutsideNormalHoursMessage) %>;
		else if(showMsg == "OutsideClosureDayHourDetails")
			document.getElementById("m_newOutsideClosureDayHourMessage").value = <%= AspxTools.JsString(m_defaultOutsideClosureDayHourMessage) %>;

    }

    function replace() {
    if (document.getElementById("m_newNormalUserExpiredMessage").value.match("<") == "<" || document.getElementById("m_newNormalUserExpiredMessage").value.match(">") == ">" ||
        document.getElementById("m_newLockDeskExpiredMessage").value.match("<") == "<" || document.getElementById("m_newLockDeskExpiredMessage").value.match(">") == ">" ||
        document.getElementById("m_newNormalUserCutOffMessage").value.match("<") == "<" || document.getElementById("m_newNormalUserCutOffMessage").value.match(">") == ">" ||
        document.getElementById("m_newLockDeskCutOffMessage").value.match("<") == "<" || document.getElementById("m_newLockDeskCutOffMessage").value.match(">") == ">" ||
        document.getElementById("m_newOutsideNormalHoursMessage").value.match("<") == "<" || document.getElementById("m_newOutsideNormalHoursMessage").value.match(">") == ">" ||
        document.getElementById("m_newOutsideClosureDayHourMessage").value.match("<") == "<" || document.getElementById("m_newOutsideClosureDayHourMessage").value.match(">") == ">") {

        document.getElementById("m_newNormalUserExpiredMessage").value = document.getElementById("m_newNormalUserExpiredMessage").value.replace(/</g, "").replace(/>/g, "");
        document.getElementById("m_newLockDeskExpiredMessage").value = document.getElementById("m_newLockDeskExpiredMessage").value.replace(/</g, "").replace(/>/g, "");
        document.getElementById("m_newNormalUserCutOffMessage").value = document.getElementById("m_newNormalUserCutOffMessage").value.replace(/</g, "").replace(/>/g, "");
        document.getElementById("m_newLockDeskCutOffMessage").value = document.getElementById("m_newLockDeskCutOffMessage").value.replace(/</g, "").replace(/>/g, "");
        document.getElementById("m_newOutsideNormalHoursMessage").value = document.getElementById("m_newOutsideNormalHoursMessage").value.replace(/</g, "").replace(/>/g, "");
        document.getElementById("m_newOutsideClosureDayHourMessage").value = document.getElementById("m_newOutsideClosureDayHourMessage").value.replace(/</g, "").replace(/>/g, "");


        alert("The characters < and > are not permitted within the warning messages and have been removed. You may want to check your messages again now that the characters have been removed.");
    }
}


    function f_onEdmsClick( bInit )
    {
        var sharedFaxRb = <%= AspxTools.JsGetElementById(m_faxShared) %>;
        var privateFaxRb = <%= AspxTools.JsGetElementById(m_faxPrivate) %>;
        var faxNumTb = <%= AspxTools.JsGetElementById(m_FaxNumber) %>;
        var docRouterLoginTb = <%= AspxTools.JsGetElementById(m_DocRouterLogin) %>;
        var docRouterPasswordTb = <%= AspxTools.JsGetElementById(m_DocRouterPassword) %>;
        var edocsPmlStatus = <%= AspxTools.JsGetElementById(m_EDocsPmlEnabledStatus) %>;
        var bEdmsEnabled = <%= AspxTools.JsGetElementById(m_IsEDocsEnabled) %>.checked;

        if (bEdmsEnabled)
        {
            sharedFaxRb.disabled = false;
            privateFaxRb.disabled = false;
            faxNumTb.readOnly = false;
            docRouterLoginTb.readOnly = false;
            docRouterPasswordTb.readOnly = false;
            edocsPmlStatus.disabled = false;
        }
        else
        {
            sharedFaxRb.disabled = true;
            privateFaxRb.disabled = true;
            faxNumTb.readOnly = true;
            docRouterLoginTb.readOnly = true;
            docRouterPasswordTb.readOnly = true;
            edocsPmlStatus.selectedIndex = "0";
            edocsPmlStatus.disabled = true;
        }

        if (!bInit && !bEdmsEnabled && privateFaxRb.checked == true )
        {
           privateFaxRb.checked = false;
           sharedFaxRb.checked = true;
        }

        {
          f_onFaxTypeChange(bInit);
        }
    }

    function f_onFaxTypeChange(bInit)
    {
        var bIsPrivate = <%= AspxTools.JsGetElementById(m_faxPrivate) %>.checked;

        var faxNumTb = <%= AspxTools.JsGetElementById(m_FaxNumber) %>;
        var docRouterLoginTb = <%= AspxTools.JsGetElementById(m_DocRouterLogin) %>;
        var docRouterPasswordTb = <%= AspxTools.JsGetElementById(m_DocRouterPassword) %>;


        if (bIsPrivate)
        {
          faxNumTb.readOnly = false;
          docRouterLoginTb.readOnly = false;
          docRouterPasswordTb.readOnly = false;
        }
        else
        {
          // We have just changed to shared, clear 'em, and disable 'em
          faxNumTb.readOnly = true;
          docRouterLoginTb.readOnly = true;
          docRouterPasswordTb.readOnly = true;

          if (!bInit)
          {
            // Someone clicked, so we get to change it.
            faxNumTb.value = '';
            docRouterLoginTb.value = '';
            docRouterPasswordTb.value = '';
          }
        }

    }

	function f_onLeadToLoanChange(e) {
	    var sender = (e && e.target) || (window.event && retrieveEventTarget(window.event));

	    if (!sender.checked)
	    {
	        document.getElementById("m_IsForceLeadToLoanNewLoanNumber").disabled = false;
	        document.getElementById("m_IsForceLeadToLoanRemoveLeadPrefix").disabled = false;
	        document.getElementById("m_IsForceLeadToUseLoanCounter").disabled = false;
	        return;
	    }

	    switch (sender.id)
	    {
	        case "m_IsForceLeadToLoanNewLoanNumber":
	            document.getElementById("m_IsForceLeadToLoanRemoveLeadPrefix").disabled = true;
	            document.getElementById("m_IsForceLeadToUseLoanCounter").disabled = true;
	            break;
	        case "m_IsForceLeadToLoanRemoveLeadPrefix":
	            document.getElementById("m_IsForceLeadToLoanNewLoanNumber").disabled = true;
	            document.getElementById("m_IsForceLeadToUseLoanCounter").disabled = true;
	            break;
	        case "m_IsForceLeadToUseLoanCounter":
	            document.getElementById("m_IsForceLeadToLoanNewLoanNumber").disabled = true;
	            document.getElementById("m_IsForceLeadToLoanRemoveLeadPrefix").disabled = true;
	            break;
	    }
	}

    function f_onDisplayNewPmlChanged(newSelectedIndex) {
        // If the new pml UI is selected and PML Pricing Engine enabled
        // uncheck and disable the Enable Quick Pricer checkbox.
        // If it is the classic UI, then disable the Configure Custom Fields link
        if (!document.getElementById("m_enableFeaturePriceMyLoanCB").checked) {
            return;
        }

        //OPM 112346
        /*
        if (newSelectedIndex == 0) {
            document.getElementById("m_IsQuickPriceEnabled").checked = false;
            document.getElementById("m_IsQuickPriceEnabled").disabled = true;
        }
        else {
            document.getElementById("m_IsQuickPriceEnabled").disabled = false;
        }
        */

        toggleConfigureCustomPMLFieldsLink();
    }

    var customizablePmlFieldsWindow = null;
    function f_configureLenderCustomizablePMLFields() {
        if (hasDisabledAttr(document.getElementById("m_ConfigureCustomPMLFieldsLink"))) {
            if (!bValidBrokerId) {
                alert('Please save the broker before configuring Custom PML Fields.');
            }
            else {
                alert('New PML UI must be enabled to configure Custom PML Fields.');
            }
            return;
        }

        window.open(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/LOAdmin/Broker/CustomPmlFields.aspx")) %> + '?BrokerId=' + g_brokerId, 'customPmlFields', 'height=800,width=1050,menubar=no,scrollbars=yes,resizable=yes');
        return false;
    }

    function f_onEnableNightlySeqNum() {
        var enableCB = <%= AspxTools.JsGetElementById(m_EnableSequentialNumbering) %>;

        var counterSeed = <%= AspxTools.JsGetElementById(m_SequentialNumberingSeed) %>;
        var fieldId = <%= AspxTools.JsGetElementById(m_SequentialNumberingFieldId) %>;

        counterSeed.disabled = !enableCB.checked;
        fieldId.disabled = !enableCB.checked;
    }

    function f_onIsAutoGenerateTradeNumsClick() {
        var IsAutoGenerateTradeNums = <%= AspxTools.JsGetElementById(m_IsAutoGenerateTradeNums) %>;
        var divTradeCounter = document.getElementById('divTradeCounter');

        setDisabledAttr($j(divTradeCounter).find('input'), !IsAutoGenerateTradeNums.checked);
    }

    function f_onCopyPatternClick(){
        var leadNamingPattern = <%= AspxTools.JsGetElementById(m_leadNamingPattern) %>;
        var namingPattern = <%= AspxTools.JsGetElementById(m_namingPattern) %>;

        leadNamingPattern.value = namingPattern.value;
        return false;
    }

    function SetDocVendorsDirty()
    {
        document.getElementById('IsDocVendorListDirty').value = "True";
    }

    function IsDocVendorListDirty()
    {
        return document.getElementById('IsDocVendorListDirty').value == "True";
    }

    function ValidateConfiguredDocumentVendors(sender, args)
    {
        var docVendorsAreValid = true;
        var fieldsToValidate = $j(".doc-vendor-field-validated");

        $j.each(fieldsToValidate, function (index, docVendor) {
            if (docVendor.value === "") {
                docVendorsAreValid = false;
            }
        });

        if (!docVendorsAreValid)
        {
            var msgToShow = 'Save Failed. Please fill out all required document vendor fields.';
            invalidate(msgToShow, args);
        }
    }

    function openSymitarDebuggingConsole()
    {
        window.open(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/LOAdmin/Broker/SymitarDebuggingConsole.aspx")) %> + '?BrokerId=' + g_brokerId);
    }

    function setSymitarRowVisibility()
    {
        var isCreditUnion = <%=AspxTools.JsGetElementById(m_IsCreditUnion)%>.checked;

        $j("#SymitarCredentials").toggle(isCreditUnion);
    }

    function onCustomDisclosurePipelineEditClick() {
        var url = ML.VirtualRoot
                + "/LOAdmin/Broker/CustomCocFieldList.aspx"
                + "?brokerId=" + g_brokerId;

        LQBPopup.Show(url, {
            popupClasses: 'Popup',
            width: 900,
            height: 600
        }, null);
    }

		</script>

		<FORM id="BrokerEdit" method="post" runat="server">
		    <asp:HiddenField ID="m_GenworthRanking" runat="server" Value="-1" />
		    <asp:HiddenField ID="m_MgicRanking" runat="server" Value="-1" />
		    <asp:HiddenField ID="m_RadianRanking" runat="server" Value="-1" />
		    <asp:HiddenField ID="m_EssentRanking" runat="server" Value="-1" />
		    <asp:HiddenField ID="archRanking" runat="server" Value="-1" />
		    <asp:HiddenField ID="m_IsHighestPmiProviderWins" runat="server" Value="0" />
		    <asp:HiddenField ID="m_NationalRanking" runat="server" Value="-1" />
            <asp:HiddenField ID="m_MassHousingRanking" runat="server" Value="-1" />
            <asp:ScriptManager runat="server"></asp:ScriptManager>
		    <div id="InactiveMsg" style="DISPLAY: none; FONT-WEIGHT: bold; FONT-SIZE: 20px; WIDTH: 100%; COLOR: red; TEXT-ALIGN: center">INACTIVE
				BROKER</div>
			<TABLE cellSpacing="0" cellPadding="4" width="100%" border="0">
				<TR>
					<TD noWrap>
                        <input type="button" onclick="return Validate();" value="Save" />
                        <ml:nodoubleclickbutton style="display: none;" id="m_Save" OnClientClick="Page_ClientValidate()" onclick="SaveClick" runat="server" Text="Save"></ml:nodoubleclickbutton>
                        <INPUT onclick="onClosePopup();" type="button" value="Close">
					</TD>
					<TD><ml:EncodedLabel id="m_Message" runat="server"></ml:EncodedLabel></TD>
					<TD align="right">
					    <asp:panel id="panSubOptions" runat="server">
					        <a onclick="showModal('/LOAdmin/Task/LOAdminTaskList.aspx?BrokerId=' + g_brokerId); return false;" href="#">Task Templates</a>
					        <a onclick="window.open('Workflow/EditWorkflowConfiguration.aspx?brokerId=' + g_brokerId, 'WorkflowConfiguration'); return false;" href="#">Show Workflow Configuration</a>
					        <a onclick="showModal('/LOAdmin/Task/PermissionLevels.aspx?BrokerId=' + g_brokerId); return false;" href="#">Permission Levels</a>
					        <a onclick="showModal('/LOAdmin/Task/ConditionCategoryEditor.aspx?BrokerId=' + g_brokerId); return false;" href="#">Condition Category</a>
					        <a onclick="onShowBranches(); return false;" href="#">Show Branches</a>
					        <a onclick="onShowUsers(); return false;" href="#">Show Users</a>
							<a onclick="showLicenseList(1); return false;" href="#">Show Licenses</a>
							<a onclick="showModal('/LOAdmin/Broker/TileInsuranceEditor.aspx?brokerid=' + g_brokerId); return false;"  href="#">Title Vendors</a>
							<a onclick="showModal('/LOAdmin/Broker/LoanStatusconfig.aspx?brokerid=' + g_brokerId, '', 'dialogHeight:700px; dialogWidth:800px'); return false;" href="#">Loan Status Config</a>
						    <a onclick="window.open(VRoot + '/LOAdmin/Broker/ExportLoanByLoanId.aspx?BrokerId=' + g_brokerId);" href="#">Export Loan By Loan Id</a>
						    <a onclick="window.open(VRoot + '/LOAdmin/Broker/BrokerReplicate.aspx?BrokerId=' + g_brokerId);" href="#">Broker Replicate</a>
						    <a onclick="onGenericFrameworkVendors();" href="#">Generic Framework Vendors</a>
                            <a onclick="window.open(VRoot + '/LOAdmin/Broker/AuthorizedEmailDomains.aspx?BrokerId=' + g_brokerId);" href="#">Authorized Email Domains</a>
						</asp:panel>
					</TD>
				</TR>
			</TABLE>
			<HR>
			<div style="margin-bottom:2em">
			    <div style="float:left">
			        <input id="btnEnabled" onclick="f_enableBroker(true);" type="button" value="Turn broker on" NoHighlight>&nbsp;&nbsp;
			        <input id="btnDisabled" onclick="f_enableBroker(false);" type="button" value="Turn broker off" NoHighlight>
			    </div>
			    <div style="float:right">
                    <input id="btnShippingTemplateImport" onclick="importRedir('ST');" type="button" value="Import Shipping Templates"/>
			        <input id="btnStackImportExport" onclick="importRedir('SO');" type="button" value="Import/Export Stacking Order" />
			        <input id="btnTaskImport" onclick="importRedir('LT');" type="button" value="Import Loan Tasks" />
			        <input id="btnConditionImport" onclick="importRedir('CC');" type="button" value="Import Condition Choices" />
			        <input id="btnOrigCompanyImport" onclick="importRedir('OC');" type="button" value="Import Origination Companies"/>
                    <asp:Button id="m_AddSampleReports" onclick="AddSampleReportsClick" runat="server" Text="Add sample reports" OnClientClick="return confirm('Add sample reports for broker?');"></asp:Button>
			    </div>
			</div>
			<hr>
			<TABLE class="FormTable" cellSpacing="0" cellPadding="0" width="100%" border="0">
                <tr>
                    <td colspan="2" style="color:red;">
                        <span runat="server" ID="DeterminationMessageError" Visible="false"></span>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Suite Type
                    </td>
                    <td>
                        <asp:DropDownList ID="SuiteType" runat="server"></asp:DropDownList>
                        <img alt="Required" src="../../images/require_icon.gif">
                    </td>
                </tr>
				<tr>
                    <td class="FieldLabel">Company Name</td>
					<td width="100%"><asp:textbox id="m_Name" runat="server" maxlength="100" nouppercase="true" Width="340px" /><img src="../../images/require_icon.gif"/>
						<asp:requiredfieldvalidator id="valBrokerName" runat="server" ControlToValidate="m_Name">
							<img runat="server" src="../../images/error_icon.gif"/>
						</asp:requiredfieldvalidator></td>

				</tr>
                <tr>
                    <td class="FieldLabel">Customer Code</td>
                    <td>
                        <asp:TextBox ID="m_CustomerCode" runat="server" Width="120" MaxLength="10" /><img src="../../images/require_icon.gif">
                        <asp:RequiredFieldValidator ID="validatorCustomerCode" runat="server" ControlToValidate="m_CustomerCode" Display="Dynamic">
							<img runat="server" src="../../images/error_icon.gif">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Database</td>
                    <td>
                        <asp:TextBox ID="m_database" runat="server" Width="340px" ReadOnly="true" />
                        <asp:DropDownList ID="m_databaseList" runat="server" Width="340px" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Broker ID</td>
                    <td>
                        <asp:TextBox ID="m_ID" Style="padding-left: 4px" runat="server" Width="340px" ReadOnly="True" Wrap="False" BackColor="Gainsboro" />
                        <input accesskey="B" onclick="copyToClipboard( document.getElementById('m_ID') );" type="button" value="Copy (Alt + B)">
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Pml Site ID</td>
                    <td>
                        <asp:TextBox ID="m_PmlSiteID" Style="padding-left: 4px" runat="server" Width="340px" ReadOnly="True" Wrap="False" BackColor="Gainsboro" />
                        <input accesskey="P" onclick="copyToClipboard( document.getElementById('m_PmlSiteID') );" type="button" value="Copy (Alt + P)">
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" valign="top">Internal Notes</td>
                    <td><asp:TextBox ID="m_Notes" Style="padding-left: 4px" runat="server" Width="700px" nouppercase="true" TextMode="MultiLine" Height="300px" /></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Actual Pricing Broker Id</td>
                    <td><asp:TextBox ID="m_ActualPricingBrokerId" runat="server" Width="340px" /> * This will only be use for client on AWS. Ask David Dao if curious.</td>
                </tr>
				<tr>
					<td class="FieldLabel">Max Web Service Daily Call Volume</td>
					<td><asp:textbox id="m_maxWebServiceDailyCallVolume" style="PADDING-LEFT: 4px" runat="server" Width="80"/>
                        <asp:rangevalidator id="m_rangeDailyCallVolume" runat="server" ControlToValidate="m_maxWebServiceDailyCallVolume" Display="Dynamic" ErrorMessage="Please enter a valid whole value between 0 and 10,000,000." MinimumValue="0" MaximumValue="10000000" Type="Integer"></asp:rangevalidator>
					</td>
				</tr>
				<TR>
                    <td class="FieldLabel">Broker/Banker License #</td>
					<TD><asp:textbox id="m_LicenseNumber" nouppercase="true" runat="server"/>
					</TD>
				</TR>
                <TR>
                    <td class="FieldLabel">Credit Union Status</td>
                    <TD>
                        <label><asp:CheckBox ID="m_IsCreditUnion" runat="server" onclick="setSymitarRowVisibility();" /> Company is a Credit Union?</label>
                        <div class="creditUnionOptions indented"><label><asp:CheckBox ID="m_IsFederalCreditUnion" runat="server" /> Federal Credit Union</label></div>
                        <div class="creditUnionOptions indented"><label><asp:CheckBox ID="m_IsTaxExemptCreditUnion" runat="server" /> Tax-Exempt Credit Union</label></div>
                    </TD>
                </TR>
				<TR>
					<TD class="FieldLabel">Address
					</TD>
					<TD><asp:textbox id="m_Street" runat="server" nouppercase="true" maxlength="60" Width="340"/><IMG src="../../images/require_icon.gif">
						<asp:requiredfieldvalidator id="valAddress" runat="server" ControlToValidate="m_Street">
							<img runat="server" src="../../images/error_icon.gif">
						</asp:requiredfieldvalidator></TD>
				</TR>
				<TR>
					<TD class="FieldLabel"></TD>
					<TD class="FieldLabel">Zipcode
						<ml:zipcodetextbox id="m_Zipcode" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox>&nbsp;<IMG src="../../images/require_icon.gif">
						<asp:requiredfieldvalidator id="valZip" runat="server" ControlToValidate="m_Zipcode" Display="Dynamic">
							<img runat="server" src="../../images/error_icon.gif">
						</asp:requiredfieldvalidator>City
						<asp:textbox id="m_City" runat="server" maxlength="36" nouppercase="true" Width="225"/><asp:requiredfieldvalidator id="valCity" runat="server" ControlToValidate="m_City" Display="Dynamic">
							<img runat="server" src="../../images/error_icon.gif">
						</asp:requiredfieldvalidator>&nbsp;State<ml:statedropdownlist id="m_State" runat="server"></ml:statedropdownlist></TD>
				</TR>
				<TR>
					<TD class="FieldLabel">Phone
					</TD>
					<TD><ml:phonetextbox id="m_PhoneTF" runat="server" preset="phone" width="120"></ml:phonetextbox><IMG src="../../images/require_icon.gif"><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="m_PhoneTF" Display="Dynamic">
							<img runat="server" src="../../images/error_icon.gif">
						</asp:requiredfieldvalidator></TD>
				</TR>
				<TR>
					<TD class="FieldLabel">Fax
					</TD>
					<TD><ml:phonetextbox id="m_FaxTF" runat="server" preset="phone" width="120"></ml:phonetextbox></TD>
				</TR>
				<TR>
					<TD class="FieldLabel">Url
					</TD>
					<TD><asp:textbox id="m_Url" runat="server" Width="300" nouppercase="true"/></TD>
				</TR>
				<TR>
					<TD class="FieldLabel" noWrap>Email Address For Notifications
					</TD>
					<TD><asp:textbox id="m_EmailAddrSendFromForNotif" runat="server" Width="300" nouppercase="true"/></TD>
				</TR>
				<tr>
				    <td class="FieldLabel" > Email Alias for Task Notifications </td>
				    <td><asp:TextBox runat="server" ID="m_sTaskEmailAlias" Width="300"/></td>
				</tr>
				<TR>
					<TD class="FieldLabel">Label For Notifications
					</TD>
					<TD><asp:textbox id="m_NameSendFromForNotif" runat="server" nouppercase="true"/></TD>
				</TR>
				<TR>
					<TD class="FieldLabel">Account Manager
					</TD>
					<TD><asp:textbox id="m_AccountManager" runat="server" Width="120" MaxLength="10"/></TD>
				</TR>

				<tr>
					<td class="FieldLabel">
						Path to DataTrac
					</td>
					<td>
						<asp:textbox id="m_pathToDataTrac" runat="server" Width="340" MaxLength="248"/>&nbsp;&nbsp;
						<input id="m_testDataTracPathConnection" type="button" value="Test Connection" NoHighlight onclick="f_testDataTracConnection();">
					</td>
				</tr>
				<tr>
					<td class="FieldLabel">
						URL for DataTrac Webservice
					</td>
					<td>
						<asp:textbox id="m_dataTracWebserviceURL" runat="server" Width="466" MaxLength="248"/>&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
				    <td class="FieldLabel">URL For LPQ Server </td>
				    <td>
				        <asp:TextBox ID="m_sLPQExportUrl" runat="server" Width="466" MaxLength="200"/>
				    </td>
				</tr>
				<tr>
					<td class="FieldLabel">
						Active Directory Webservice URL
					</td>
					<td>
						<asp:textbox id="m_sActiveDirectoryUrl" runat="server" Width="466" MaxLength="1000"/>&nbsp;&nbsp;
					</td>
				</tr>
				<TR>
					<TD class="FieldLabel">NHC Key
					</TD>
					<TD><asp:textbox id="m_NHCKey" runat="server" Width="250px" ReadOnly="True"/><input id="btnGenerateNHCKey" onclick="f_generateGuid();" type="button" value="Generate Key" NoHighlight><input id="btnClearNHCKey" onclick="f_putGuidEmpty();" type="button" value="Clear NHC" NoHighlight><INPUT accessKey="N" onclick="copyToClipboard( document.getElementById('m_NHCKey') );" type="button" value="Copy (Alt + N)">
					</TD>
				</TR>
				<tr>
				    <td class="FieldLabel">Barcode Script Key</td>
				    <td>
				        <asp:TextBox runat="server" ID="BarcodeKey" ReadOnly="true"> </asp:TextBox>
				    </td>
				</tr>
				<tr>
				    <td class="FieldLabel">
				        PMI Providers
				    </td>
				    <td>
                        <a href="#" onclick="editPmiConfiguration(); return false;">configure PMI quoting in PML</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="#" onclick="editMIVendorConfiguration(); return false;">enable direct PMI integrations</a>
				    </td>
				</tr>
				<tr>
				    <td class="FieldLabel">CBC DataVerify Integration Info</td>
				    <td>
				        <asp:CheckBox id="m_cbEnableDriveIntegration" runat="server" /> <b>Enable DRIVE Integration</b>
				        <asp:CustomValidator ID="m_cvDriveIntegration" runat="server" ClientValidationFunction="validateDriveFields"></asp:CustomValidator>
				        <asp:CheckBox ID="m_cbEnableDriveConditionImporting" runat="server" />  <b>Import DRIVE Conditions</b>
				    </td>
				</tr>
				<tr>
				    <td></td>
				    <td colspan=2>
				        <b>
				        Condition Category:
				        <asp:DropDownList ID="m_ddlDriveConditionCategory" runat="server">
				        </asp:DropDownList>

				        EDocs:
				        <span id="lSelectedDocTypeSpan"><ml:EncodedLiteral ID="m_lSelectedDriveDocType" runat="server" ></ml:EncodedLiteral></span>
				        </b>
				        <asp:HiddenField ID="m_selectedDriveDocType" runat="server" />
				        [ <a href="#" onclick="selectDriveDocType(); return false;" >select Doctype</a> ]
				    </td>
				</tr>
				<tr>
				    <td></td>
				    <td>
				        <b>Due Date</b>
                        <img id="DriveDueDateR" src="~/images/error_icon_right.gif" alt="Required" visible="false" runat="server" />

                        <a id="DriveDueDateCalculation" runat="server" onclick="driveDateCalculationPopup();">calculate</a>
                        <asp:HiddenField runat="server" ID="DriveDueDateOffset" />
                        <asp:HiddenField runat="server" ID="DriveDueDateCalcField" />
                        <ml:EncodedLabel ID="DriveDueDateCalcDescription" runat="server"></ml:EncodedLabel>

                        <b>
                        Task Permission&nbsp;&nbsp;
                        <ml:EncodedLabel ID="DriveTaskPermission" runat="server"></ml:EncodedLabel>&nbsp;</b>
                        <a href="#" onclick="alert(document.getElementById('DrivePermissionDescription').value || 'no permission selected.');return false;">?</a>
                        <asp:HiddenField ID="DrivePermissionDescription" runat="server" />
                        <asp:HiddenField ID="DrivePermissionLevelID" runat="server" />
                        <a id="driveChangePermissionLevel" runat="server" onclick="driveTaskPermissionPopup();">change</a>

				    </td>
				</tr>
                <tbody id="SymitarCredentials">
                    <tr>
                        <td class="FieldLabel">Symitar / Episys Integration</td>
                        <td>
                            To change Symitar settings, please use the temporary option editor.
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">&nbsp;</td>
                        <td>
                            <asp:CheckBox runat="server" ID="SymitarEnableCb" Enabled="false" /> Enable &nbsp;

                            <input type="button" onclick="openSymitarDebuggingConsole();" value="Open Debugging Console"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">&nbsp;</td>
                        <td>
                            Device Number &nbsp;
                            <asp:TextBox runat="server" ID="SymitarLenderConfigDeviceNumber" ReadOnly="True"/>&nbsp;

                            Device Type &nbsp;
                            <asp:TextBox runat="server" ID="SymitarLenderConfigDeviceType" ReadOnly="True"/>&nbsp;

                            Virtual Card Prefix &nbsp;
                            <asp:TextBox runat="server" ID="SymitarLenderConfigVirtualCardPrefix" ReadOnly="True"/>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">&nbsp;</td>
                        <td>
                            IP Address &nbsp;
                            <asp:TextBox runat="server" ID="SymitarLenderConfigIpAddress" ReadOnly="True"/>&nbsp;

                            Ports (comma separated) &nbsp;
                            <asp:TextBox runat="server" ID="SymitarLenderConfigIpPorts" Width="300" ReadOnly="True"/>&nbsp;
                        </td>
                    </tr>
                </tbody>
                <tr>
                    <td colspan="3">
                        <table ID="CenlarConfigurationTable" class="InsetBorder">
                            <tr>
                                <td class="FieldLabel">Cenlar Integration</td>
                                <td><span class="width50">&nbsp;</span></td>
                                <td colspan="4">
                                    <label for="IsCenlarEnabled">
                                        <asp:CheckBox ID="IsCenlarEnabled" runat="server" onclick="onCenlarToggled();" />
                                        Enable Cenlar Integration
                                    </label>
                                    <asp:CustomValidator ID="CenlarIntegrationValidator" ClientValidationFunction="validateCenlarFields" runat="server"></asp:CustomValidator>&nbsp;&nbsp;
                                    <label for="CenlarEnableAutomaticTransmissions">
                                        <asp:CheckBox ID="CenlarEnableAutomaticTransmissions" runat="server" />
                                        Enable Automatic Transmissions
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" />
                                <td>
                                    Custom Report Name
                                </td>
                                <td>
                                    <asp:TextBox ID="CenlarCustomReportName" nouppercase="true" runat="server"></asp:TextBox>
                                    <img id="CenlarCustomReportNameRequiredIcon" alt="required" src="../../images/require_icon.gif" class="CenlarRequiredIcon">
                                    <img id="CenlarCustomReportNameErrorIcon" alt="required" src="../../images/error_icon.gif" class="hidden CenlarErrorIcon">
                                </td>
                                <td>
                                    Batch Export Name
                                </td>
                                <td>
                                    <asp:TextBox ID="CenlarBatchExportName" nouppercase="true" runat="server"></asp:TextBox>
                                    <img id="CenlarBatchExportNameRequiredIcon" alt="required" src="../../images/require_icon.gif" class="CenlarRequiredIcon">
                                    <img id="CenlarBatchExportNameErrorIcon" alt="required" src="../../images/error_icon.gif" class="hidden CenlarErrorIcon">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" />
                                <td>
                                    Dummy User ID
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="CenlarDummyUserId" nouppercase="true" runat="server"></asp:TextBox>
                                    <img id="CenlarDummyUserIdRequiredIcon" alt="required" src="../../images/require_icon.gif" class="CenlarRequiredIcon">
                                    <img id="CenlarDummyUserIdErrorIcon" alt="required" src="../../images/error_icon.gif" class="hidden CenlarErrorIcon">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" />
                                <td>
                                    Cenlar Credentials
                                </td>
                                <td colspan="3">
                                    <a onclick="editCenlarCredentials(true/* isBeta */);">edit beta credentials</a>
                                    &nbsp;&nbsp;&nbsp;
                                    <a onclick="editCenlarCredentials(false/* isBeta */);">edit production credentials</a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" />
                                <td valign="top">
                                    Notification Emails
                                    <br />
                                    (semicolon-delimited)
                                </td>
                                <td colspan="3">
                                    <asp:TextBox id="CenlarNotificationEmails" runat="server" nouppercase="true" TextMode="MultiLine" />
                                    <img id="CenlarNotificationEmailsRequiredIcon" alt="required" src="../../images/require_icon.gif" class="CenlarRequiredIcon">
                                    <img id="CenlarNotificationEmailsErrorIcon" alt="required" src="../../images/error_icon.gif" class="hidden CenlarErrorIcon">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" />
                                <td colspan="3">
                                    <label for="CenlarShouldSuppressAcceptedLoanNotifications">
                                        <asp:CheckBox ID="CenlarShouldSuppressAcceptedLoanNotifications" runat="server" />
                                        Suppress Accepted Loans in Email Notifications
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" />
                                <td colspan="3">
                                    <label for="CenlarRemoveProblematicPayloadCharacters">
                                        <asp:CheckBox ID="CenlarRemoveProblematicPayloadCharacters" runat="server" />
                                            Strip problematic characters from Cenlar payloads
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
				<tr>
				    <td colspan=3>
				    <table class="InsetBorder" cellSpacing="0" cellPadding="5" border="0">
				        <tr>
				            <td class="FieldLabel">Flood Integration Info</td>
				            <td><span style="width:36px">&nbsp;</span></td>
				            <td>
				            <asp:CheckBox id="m_cbEnableFloodIntegration" runat="server" />Enable Flood Integration<span style="width:72px">&nbsp;</span>
				            <asp:CustomValidator ID="m_cvFloodIntegration" runat="server" ClientValidationFunction="validateFloodFields"></asp:CustomValidator>
				            Flood Provider: <asp:DropDownList ID="m_ddlFloodProvider" runat="server"></asp:DropDownList><span style="width:36px">&nbsp;</span>
				            Account ID:<asp:TextBox ID="FloodIntegrationAccountID" runat="server" />
				            </td>
				        </tr>
				        <tr>
				            <td></td>
				            <td></td>
				            <td>
				            &nbsp;Flood Reseller: <asp:DropDownList ID="m_ddlFloodReseller" runat="server"></asp:DropDownList>
				            </td>
				        </tr>
				        <tr>
				            <td></td>
				            <td></td>
				            <td>
				            <asp:CheckBox id="m_cbIsCCPmtRequiredForFloodIntegration" runat="server" />Is payment by credit card required?<span style="width:14px">&nbsp;</span>
				            EDocs:
				            <span id="lSelectedFloodDocTypeSpan"><ml:EncodedLiteral ID="m_lSelectedFloodDocType" runat="server"></ml:EncodedLiteral></span>
				            <asp:HiddenField ID="m_selectedFloodDocType" runat="server" />
				            [ <a href="#" onclick="selectFloodDocType(); return false;" >select Doctype</a> ]
				            </td>
				        </tr>
				    </table>
				    </td>
				</tr>
                <tr>
                    <td></td>
                    <td>
                        <fieldset>
                            <legend>
                                <asp:CheckBox ID="IsEnableComplianceEaseIntegration" runat="server" Text="Enable ComplianceEase Integration" />
                                <asp:CheckBox ID="m_IsEnablePTMComplianceEaseIndicator" runat="server" Text="Enable ComplianceEase Indicator" />
                            </legend>
                            <div>
                                <span style="width: 18px">&nbsp;</span>
                                Username: <asp:TextBox ID="m_CELogin" runat="server" nouppercase="true"/><img
                                    id="ceLogin_required_icon" src="../../images/require_icon.gif">&nbsp;&nbsp;
                                <asp:CustomValidator ID="m_CustomValidatorCELogin" runat="server" ClientValidationFunction="f_CELoginValidationFunction">
                                    <img runat="server" id="ceLogin_error_icon" src="../../images/error_icon.gif" style="display:none">
                                </asp:CustomValidator>
                                Password: <asp:TextBox ID="m_CEPassword" runat="server" TextMode="Password" nouppercase="true"/>
                                <asp:PlaceHolder ID="m_phCEPasswordUpdating" runat="server">(leave blank if not changing)</asp:PlaceHolder>
                                <asp:PlaceHolder ID="m_phCEPasswordRequiring" runat="server">
                                    <img id="cePassword_required_icon" src="../../images/require_icon.gif">&nbsp;&nbsp;
                                    <asp:CustomValidator ID="m_CustomValidatorCEPassword" runat="server" ClientValidationFunction="f_CEPasswordValidationFunction">
                                        <img runat="server" id="cePassword_error_icon" src="../../images/error_icon.gif" style="display:none">
                                    </asp:CustomValidator>
                                </asp:PlaceHolder>
                            </div>
                        </fieldset>
                    </td>
                </tr>
				<tr>
				    <td class="FieldLabel">Billing Version</td>
				    <td>
				        DO NOT MODIFY <asp:DropDownList runat="server" ID="m_ddlBillingVersion">
				        </asp:DropDownList>
				    </td>
				</tr>
                <tbody id="PtmOptions">
                    <tr>
                        <td rowspan="8">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <legend>
                                    <asp:CheckBox ID="IsEnableComplianceEagleIntegration" runat="server" Text="Enable ComplianceEagle Integration" />
                                    <asp:CheckBox ID="IsEnablePTMComplianceEagleAuditIndicator" runat="server" Text="Enable ComplianceEagle Audit Indicator" />
                                    <asp:CheckBox ID="ComplianceEagleEnableMismo34" runat="server" Text="Enable MISMO 3.4" />
                                </legend>
                                <div>
                                    <span style="width: 18px">&nbsp;</span>
                                    Username:<asp:TextBox ID="ComplianceEagleLogin" runat="server" /><img id="ComplianceEagleLogin_required_icon" src="../../images/require_icon.gif">&nbsp;&nbsp;
                                    <asp:CustomValidator ID="CustomValidatorComplianceEagleLogin" runat="server" ClientValidationFunction="f_ComplianceEagleLoginValidationFunction">
                                        <img runat="server" id="ComplianceEagleLogin_error_icon" src="../../images/error_icon.gif" style="display:none">
                                    </asp:CustomValidator>
                                    Password:<asp:TextBox ID="ComplianceEaglePassword" runat="server" TextMode="Password" />
                                    <asp:PlaceHolder ID="phComplianceEaglePasswordUpdating" runat="server">(leave blank if not changing)&nbsp;</asp:PlaceHolder>
                                    <asp:PlaceHolder ID="phComplianceEaglePasswordRequiring" runat="server"><img id="ComplianceEaglePassword_required_icon" src="../../images/require_icon.gif">&nbsp;&nbsp;
                                        <asp:CustomValidator ID="CustomValidatorComplianceEaglePassword" runat="server" ClientValidationFunction="f_ComplianceEaglePasswordValidationFunction">
                                            <img runat="server" id="ComplianceEaglePassword_error_icon" src="../../images/error_icon.gif" style="display:none">
                                        </asp:CustomValidator>
                                    </asp:PlaceHolder>
                                    CompanyID:<asp:TextBox ID="ComplianceEagleCompanyID" runat="server" /><img id="ComplianceEagleCompanyID_required_icon" src="../../images/require_icon.gif" />&nbsp;&nbsp;
                                    <asp:CustomValidator ID="CustomValidatorComplianceEagleCompanyID" runat="server" ClientValidationFunction="f_ComplianceEagleCompanyIDValidationFunction">
                                        <img runat="server" id="ComplianceEagleCompanyID_error_icon" src="../../images/error_icon.gif" style="display:none">
                                    </asp:CustomValidator>
                                </div>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <asp:CheckBox ID="EnableKtaIntegration" runat="server" Text="Enable KTA Document Capture Integration" CssClass="margin-left" />
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <legend>
                                    <asp:CheckBox ID="m_IsEnablePTMDocMagicSeamlessInterface" runat="server" OnClientClick="on_cbDMSeamless_clicked();" Text="Enable Seamless Document Interface"/>
                                </legend>
                                <asp:HiddenField ID="IsDocVendorListDirty" runat="server" Value="False" />
                                <div ID="DocVendorListDiv">
                                    <asp:UpdatePanel runat="server" ID="DocVendorListUpdatePanel">
                                        <ContentTemplate>
                                            <asp:Repeater runat="server" ID="DocVendorList" OnItemDataBound="BindDocVendor" OnItemCommand="CommandDocVendor">
                                                <HeaderTemplate>
                                                    <asp:CustomValidator runat="server" ID="DocVendorValidator" ClientValidationFunction="ValidateConfiguredDocumentVendors" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="doc-vendor">
                                                        <div class="section-left">
                                                            <div>
                                                                Doc Vendor:&nbsp;
                                                                <asp:TextBox ID="DocConfigVendorName" nouppercase="true" runat="server" CssClass="vendor"/>
                                                                <asp:HiddenField ID="DocConfigVendorId" runat="server" />
                                                                <asp:Button ID="DocConfigRemoveVendor" Text="Remove Vendor" runat="server" CausesValidation="false" OnClientClick="SetDocVendorsDirty()" CommandName="RemoveDocVendor" />
                                                            </div>
                                                            <div class="login-info">
                                                                <table>
                                                                    <tbody class="login-info" runat="server" id="DocConfigLoginSection">
                                                                        <tr>
                                                                            <td>
                                                                                Login:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="DocConfigLogin" runat="server" Width="100" nouppercase="true" data-type="Username"
                                                                                    CssClass="doc-vendor-field-validated" onchange="SetDocVendorsDirty();" />
                                                                                <img id="dmLogin_required_icon" alt="Required" src="../../images/require_icon.gif">&nbsp;&nbsp;
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody class="login-info" runat="server" id="DocConfigPasswordSection">
                                                                        <tr>
                                                                            <td>
                                                                                Password:
                                                                            </td>
                                                                            <td>
                                                                                <asp:HiddenField ID="DocConfigCurrentPassword" runat="server" />
                                                                                <asp:TextBox ID="DocConfigNewPassword" runat="server" Width="100" TextMode="Password" nouppercase="true"
                                                                                    data-type="Password" onchange="SetDocVendorsDirty();"/>
                                                                                (leave blank if not changing)
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <tbody class="login-info" runat="server" id="DocConfigCompanyIdSection">
                                                                        <tr>
                                                                            <td>
                                                                                CompanyID:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="DocConfigCompanyId" runat="server" Width="100" nouppercase="true" data-type="AccountId"
                                                                                    CssClass="doc-vendor-field-validated" onchange="SetDocVendorsDirty();" />
                                                                                <img id="dmCompanyID_required_icon" alt="Required" src="../../images/require_icon.gif">
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div>
                                                                Services Link Name:
                                                                <asp:TextBox ID="DocConfigServicesLinkName" nouppercase="true" runat="server" Width="200px" onchange="SetDocVendorsDirty();"/>
                                                            </div>
                                                            <table>
                                                                <tbody runat="server" ID="DocConfigTestingEnvironmentSection">
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <asp:CheckBox ID="DocConfigUseTestingEnvironment" runat="server" Text="Use Testing Environment" onclick="SetDocVendorsDirty();" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="DocConfigEnablePartnerBilling" runat="server" Text="Enable Partner Billing" onclick="SetDocVendorsDirty();" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="DocConfigEnableForProductionLoans" runat="server" Text="Enable for Production Loans" onclick="SetDocVendorsDirty();" />
                                                                    </td>
                                                                </tr>
                                                                <tr id="m_IsEnablePTMDocMagicOnlineInterfaceRow">
                                                                    <td>
                                                                        <asp:CheckBox ID="DocConfigEnableOnlineInterface" runat="server" Text="Enable Online Interface" onclick="SetDocVendorsDirty();" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="DocConfigEnableForTestLoans" runat="server" Text="Enable for Test Loans" onclick="SetDocVendorsDirty();" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="section-right">
                                                            <div>
                                                                    <asp:CheckBox ID="DocConfigHasCustomPackageData" class="custom-package-lock" Text="Use Custom Package Data" OnCheckedChanged="ToggleCustomPackageData" CausesValidation="false" onclick="SetDocVendorsDirty();" AutoPostBack="true" runat="server" />
                                                            </div>
                                                            <div>
                                                                    &nbsp;<asp:TextBox ID="DocConfigCustomPackageData" nouppercase="true" style="resize:both;" runat="server" onclick="SetDocVendorsDirty();" TextMode="MultiLine" Rows="10" Columns="80" />
                                                                    <asp:HiddenField ID="DocConfigCustomPackageDataCache" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear">&nbsp;</div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                Configure Additional Document Vendor
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="DocListAddVendorList" runat="server" CssClass="vendor"/>
                                                                <asp:Button ID="DocListAddNewVendor" Text="Add New" runat="server" CausesValidation="false" OnClientClick="SetDocVendorsDirty()" CommandName="AddDocVendor" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </fieldset>
                        </td>
                    </tr>
                </tbody>
				<tr>
				    <td class="FieldLabel">QuickPricer </td>
				    <td>
				        <ml:EncodedLabel ID="Label1" runat="server" Text="Template Id" AssociatedControlID="m_quickPricerTemplateId" ></ml:EncodedLabel>
				        <asp:DropDownList runat="server" id="m_quickPricerTemplateId" Width="250px"> </asp:DropDownList>

				    </td>
				</tr>
				<TR>
					<TD class="FieldLabel">Integrated w/ LON?
					</TD>
					<TD><asp:radiobuttonlist id="m_isLONRBL" runat="server" RepeatDirection="Horizontal" CellSpacing="0" CellPadding="0">
							<asp:ListItem Value="0" Selected="True">No</asp:ListItem>
							<asp:ListItem Value="1">Yes</asp:ListItem>
						</asp:radiobuttonlist>
					</TD>
				</TR>
				<TR>
					<TD class="FieldLabel">Display PML Fee in 100 Format?
					</TD>
					<TD>
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td>
									<asp:radiobuttonlist id="m_displayPmlFeeIn100Format" runat="server" RepeatDirection="Horizontal" CellSpacing="0" CellPadding="0">
										<asp:ListItem Value="0" Selected="True">No</asp:ListItem>
										<asp:ListItem Value="1">Yes</asp:ListItem>
									</asp:radiobuttonlist>
								</td>
								<td>&nbsp;(designed for Citi only)</td>
							</tr>
						</table>
					</TD>
				</TR>
				<tr style="DISPLAY:none">
					<TD class="FieldLabel">Custom Instant Support Info?
					</TD>
					<td>
						<asp:RadioButton ID="m_DontUseCustomISInfo" runat="server" checked="true" onclick="onUseCustomISInfoClick();" Text="No" GroupName="customISInfo"></asp:RadioButton>
						<asp:RadioButton ID="m_UseCustomISInfo" runat="server" Text="Yes" onclick="onUseCustomISInfoClick();" GroupName="customISInfo" style="PADDING-RIGHT:8px"></asp:RadioButton>
						Phone:
						<ml:phonetextbox id="m_CustomISPhone" readonly="true" runat="server" preset="phone" Width="120px"></ml:phonetextbox>&nbsp;
						Email:
						<asp:textbox id="m_CustomISEmail" runat="server" readonly="true" nouppercase="true"/>
					</td>
				</tr>
				<TR>
					<TD class="FieldLabel">Is Active?
					</TD>
					<TD>
						<div id="StatusLabel"></div>
					</TD>
				</TR>
				<TR>
					<TD class="FieldLabel" vAlign="top">Features
					</TD>
					<TD><asp:checkbox id="m_enableFeaturePriceMyLoanCB" onclick="onPricingEngineEnabledChange(this.checked);" runat="server" Text="Price My Loan, Pricing Engine enabled?"></asp:checkbox><br>
					    <input name="IsNewPmlUiEnabled" id="m_IsNewPmlUIEnabled_Yes" type="radio" runat="server" value="0" onclick="f_onDisplayNewPmlChanged(0);" class="indent"/><label id="m_isDisplayNewPmlUi0Label" class="indent">New PML UI</label>
					    <input name="IsNewPmlUiEnabled" id="m_IsNewPmlUIEnabled_No" type="radio" runat="server" value="1" onclick="f_onDisplayNewPmlChanged(1);" class="indent"/><label id="m_isDisplayNewPmlUi1Label" class="indent">Classic PML UI</label><br />
					    <a id="m_ConfigureCustomPMLFieldsLink" onclick="f_configureLenderCustomizablePMLFields();" class="indent">Configure Lender's Custom PML Fields</a><br />
					    <asp:CheckBox ID="m_IsQuickPriceEnabled" Text="Enable Quick Pricer?" runat="server" /><br />
					    Use QP 2.0? <asp:DropDownList ID="m_Pml2AsQuickPricerMode" runat="server"></asp:DropDownList><br />
						<asp:checkbox id="m_enableFeatureMarketingToolsCB" runat="server" Text="Marketing Tools enabled?" Checked="True"></asp:checkbox> <br />
                        New Loan Editor UI: <asp:DropDownList ID="m_SelectedNewLoanEditorUIPermissionType" runat="server"></asp:DropDownList><br />
						<asp:checkbox id="m_EnableAutoPriceEligibilityProcess" runat="server" Text="Enable Overnight Process for Detecting Price and Eligibility Changes?"></asp:checkbox> <br />
                        <asp:CheckBox ID="m_EnableRetailTpoPortalMode" runat="server" Text="Enable Retail Mode of the Originator Portal?" /> <br />
                        <asp:CheckBox ID="m_EnableRenovationLoanSupport" runat="server" Text="Enable Renovation Loan Support?" /> <br />
					</TD>
				</TR>
				<TR>
					<TD class="FieldLabel" vAlign="top">Options
					</TD>
					<TD>
					<fieldset>
					    <legend>System Access</legend>
                        <asp:RadioButton runat="server" ID="IsOptInMultiFactorAuthentication" Text="Enable Multi-Factor Authentication" GroupName="EnableMfa" />
                        <a id="LenderCertBtn" href="javascript:void(0);">Client Certificate Registry</a>
                        <div class="indent">
                            <asp:CheckBox runat="server" ID="IsEnableMultiFactorAuthentication" Text="Enable Multi-Factor Authentication for LendingQB users" /> <br />
						    <asp:CheckBox runat="server" ID="IsEnableMultiFactorAuthenticationTPO" Text="Enable Multi-Factor Authentication for Originating Company users" /> <br />
                            <asp:CheckBox runat="server" ID="AllowDeviceRegistrationViaAuthCodePage" Text="Allow device registration via authentication code entry page" />
                        </div>
                        <asp:RadioButton runat="server" ID="IsOptOutMultiFactorAuthentication" Text="Opt out of MFA:" GroupName="EnableMfa" />
                        Requested By <asp:TextBox runat="server" nouppercase="true" ID="OptOutMfaName"/> <img class="optOutRequired" src="../../images/require_icon.gif"/>
                        Opt-out Form Date <ml:DateTextBox runat="server" ID="OptOutMfaDate"></ml:DateTextBox> <img class="optOutRequired" src="../../images/require_icon.gif"/>
                        <br />
                        <asp:CheckBox runat="server" ID="EnableLqbMobileApp" Text="Enable authenticating into the LQB Mobile App?" /> <br />
					</fieldset>
					<fieldset>
						<legend>Integrations and Services</legend>

						<div class = "section-left">
							<asp:CheckBox ID="m_IsTotalAutoPopulateAUSResponse" Text="Auto-populate TOTAL Scorecard response" runat="server"/><br />
							Enable Barcode Scanner?
							<div class="indent InsetBorder" style="width:200px">
								<asp:CheckBox runat="server" ID="m_IsDocMagicBarcodeScannerEnabled" Text="DocMagic"/> <br />
								<asp:CheckBox runat="server" ID="m_IsDocuTechBarcodeScannerEnabled" Text="DocuTech"/> <br />
								<asp:CheckBox runat="server" ID="m_IsIDSBarcodeScannerEnabled" Text="IDS" />
							</div>
							<asp:CheckBox id="m_IsDataTracIntegrationEnabled" Text="Enable DataTrac Integration? (Remember to verify all setup requirements)" Runat="server" /><br />
							<asp:CheckBox ID="m_IsDocuTechEnabled" Text="Enable DocuTech Document Services - Production?" onclick="DocuTechOnlyProductionOrStage(true);" runat="server" /><br />
							<asp:CheckBox ID="m_IsDocuTechStageEnabled" Text="Enable DocuTech Document Services - Stage?" onclick="DocuTechOnlyProductionOrStage(false);" runat="server" /><br />
							<asp:CheckBox ID="m_IsEncompassIntegrationEnabled" Text="Enable Encompass integration?" runat="server" /><br />
							<asp:CheckBox runat="server" ID="m_IsEnableNewConsumerPortal" Text="Enable new consumer portal (DO NOT TURN OFF AFTER USE)" Checked="True" /> <br />
							<asp:CheckBox runat="server" ID="m_EnableOCR" Text="Enable OCR" />
                            <div id="OCRVendorsSpan">
                                <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;OCR Vendor <asp:DropDownList runat="server" ID="OCRVendors" ></asp:DropDownList>
                                </div>
                             <br />
                            	<asp:CheckBox runat="server" ID="m_EnableSequentialNumbering" Text="Enable nightly sequential numbering?" onclick="f_onEnableNightlySeqNum();" /> <br />


								<label>Field Id:</label><asp:DropDownList runat="server" ID="m_SequentialNumberingFieldId"></asp:DropDownList>
								<label>Counter seed value:</label><asp:TextBox runat="server" ID="m_SequentialNumberingSeed"/>
								<asp:RegularExpressionValidator
									id="m_SequentialNumberingSeedRegExVal"
									runat="server"
									ControlToValidate="m_SequentialNumberingSeed"
									EnableClientScript="true"
									ValidationExpression="^\d+$">
									<img runat="server" src="../../images/error_icon.gif" />
								</asp:RegularExpressionValidator>
							</div>
						<div class = "section-right">

							<asp:CheckBox runat='server' ID="m_IsEnableRateMonitorService" Text='Enable Rate Monitor Service?' class="rateMonitorService"/><br />
							&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat='server' ID="m_AllowRateMonitorForQP2FilesInTpoPml" class="needsRateMonitorService" Text='Enable QuickPricer 2.0 Rate Monitor in Originator Portal?'/><br />
							&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat='server' ID="m_AllowRateMonitorForQP2FilesInEmdeddedPml" class="needsRateMonitorService" Text='Enable QuickPricer 2.0 Rate Monitor in LendingQB?'/><br />
							<asp:CheckBox runat='server' ID="m_IsEnableProvidentFundingSubservicingExport" Text='Enable "Provident Funding Subservicing Export"?'/><br />
							<asp:checkbox id="m_hasLenderDefaultFeatures" runat="server" Text="Has lender default features?" /><br>
							<asp:checkbox id="m_hasManyUsers" runat="server" Text="Has many users?" Checked="True" /><br>
							<asp:CheckBox id="m_IsExportPricingInfoTo3rdParty" Text="Send pricing information (including adjustments) to 3rd Party?" Runat="server" /><br />
							<label><asp:checkbox id="m_isTrackModifiedFile" runat="server" Enabled="false" />Track modified files? <a href="#" id="m_isTrackModifiedFile_Configure">Configure</a></label><br />
							<asp:CheckBox ID="m_UseFHATOTALProductionAccount" Text="Use FHA TOTAL Production Account?" runat="server" Checked="false" /><br />
							<asp:CheckBox runat="server" ID="m_IsUseNewCondition" Text="Use New Task and Condition System?" /><br />
							<asp:CheckBox runat="server" ID="m_IsImportDuFindings" Text="When importing DO/DU/LP findings, import them as conditions as well?" /> <br />
							<asp:CheckBox runat="server" ID="m_IsEnabledGfeFeeService" Text="Enable GFE Fee Service" /> <br />
							<asp:CheckBox runat="server" ID="m_IsEnableFannieMaeEarlyCheck" Text="Enable Fannie Mae EarlyCheck" Checked=true/><br />
							<asp:CheckBox runat="server" ID="m_IsEnableFreddieMacLoanQualityAdvisor" Text="Enable Freddie Mac Loan Quality Advisor" /><br />
                            <asp:CheckBox runat="server" ID="m_EnableConversationLogForLoans" Text="Enable Conversation Log" Checked="true"/>
                            <a href="#" onclick="editConversationLogCategories(); return false;">Category Editor</a>
                            <a href="#" onclick="editConversationLogPermissionLevels(); return false;">Permission Editor</a><br />
                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" ID="m_EnableConversationLogForLoansInTpo" Text="Enable in Originator Portal" /><br />
                            <asp:CheckBox runat="server" ID="IsKivaIntegrationEnabled" Text="Enable KIVA Integration for export to core system" /><br />
						</div>

					</fieldset>
					<br/>
                    <fieldset>
                        <legend>DocuSign</legend>
                        <label>DocuSign Integrator Key <asp:TextBox nouppercase="true" ID="docuSignIntegratorKey" Width="230px" Font-Names="Consolas, monospace" runat="server" preset="guid"></asp:TextBox></label>
                        <label>Secret Key <asp:TextBox nouppercase="true" ID="docuSignSecretKey" Width="230px" Font-Names="Consolas, monospace" runat="server" preset="guid"></asp:TextBox></label>
                        <div><input type="button" value="Show/Hide RSA Key" onclick="jQuery('#docuSignRsaKeySection').toggle()" /></div>
                        <div id="docuSignRsaKeySection" class="hidden">
                            <div>
                                <ml:EncodedLabel runat="server" AssociatedControlID="docuSignPrivateRsaKey" style="display:block">Private RSA Key</ml:EncodedLabel>
                                <asp:TextBox TextMode="MultiLine" nouppercase="true" Font-Names="Consolas, monospace" Width="420px" Height="360px" ID="docuSignPrivateRsaKey" runat="server" onclick="this.focus();this.select();"></asp:TextBox>
                            </div>
                        </div>
                    </fieldset>
					<fieldset>
						<legend>Loan Editing</legend>
						<div class = "section-left">
						<asp:CheckBox runat="server" ID="m_IsDisplayChoiceUfmipInLtvCalc" Text="Add option to include financed UFMIP in LTV calculation" /><br />
						<asp:Checkbox id="m_allowCreatingLoanFromBlankTemplate" runat="server" Text="Allow creating loan from blank template?" /><br>
						<asp:CheckBox ID="m_IsIncomeAssetsRequiredFhaStreamline" Text="Are income and assets required for non-credit qualifying FHA streamlines?" runat="server" /><br />
						<asp:CheckBox runat="server" ID="m_EnableDuplicateEDocProtectionChecking" Text="Check for duplicate Doc Types when protecting EDocs?" /> <br />
                        <asp:CheckBox runat="server" ID="m_IsForceLeadToUseLoanCounter" Text="Use single loan number counter for both leads and loans?" onclick="f_onLeadToLoanChange(event);"/><br />
						<asp:CheckBox runat="server" ID="m_IsForceLeadToLoanNewLoanNumber" Text="Force new loan number when converting lead to loan?" onclick="f_onLeadToLoanChange(event);"/><br />
						<asp:CheckBox runat="server" ID="m_IsForceLeadToLoanRemoveLeadPrefix" Text="Force remove lead prefix when converting lead to loan?" onclick="f_onLeadToLoanChange(event);"/><br />
                        <asp:CheckBox runat="server" ID="m_IsEditLeadsInFullLoanEditor" Text="Edit leads in full loan editor?"/><br />
                        <asp:CheckBox runat="server" ID="m_IsForceLeadToLoanTemplate" Text="Force users to use template when converting lead to loan?" /><br />
						<asp:Checkbox id="m_isLOAllowedToEditProcessorAssignedFile" runat="server" Text="Is loan officer allowed to edit processor assigned file?" Checked="true"/><br>
						<asp:Checkbox id="m_isOthersAllowedToEditUnderwriterAssignedFile" runat="server" Text="Is others allowed to edit underwriter assigned file?" Checked="true"/><br>
						<asp:CheckBox ID="m_ForceTemplateOnImport" runat="server" text=" Enable require template when importing loans" /><br>
						<asp:CheckBox ID="m_allowNonExcludableDiscountPoints" runat="server" text=" Allow non-excludable discount points on QM page" /><br />
                        <asp:CheckBox runat="server" ID="EnableCustomaryEscrowImpoundsCalculation" Text="Enable Customary Escrow Impounds Calculation" /><br />
                            &nbsp;
						</div>

						<div class = "section-right">
						<asp:CheckBox runat="server" ID="m_SendAutoLockToLockDesk" Text="Send auto-lock notification to lock desk" /> <br />
						<asp:CheckBox runat="server" ID="m_SaveLockConfOnAutoLock" Text="Save lock confirmation on auto-lock" /> <br />
						<asp:CheckBox id="m_ShowProcessorInPMLCertificate" Text="Show Processor in Certificate" Runat="server" /><br />
						<asp:CheckBox id="m_ShowJuniorProcessorInPMLCertificate" Text="Show Junior Processor in Certificate" Runat="server" /><br />
						<asp:CheckBox id="m_ShowUnderwriterInPMLCertificate" Text="Show Underwriter in Certificate" Runat="server" /><br />
						<asp:CheckBox id="m_ShowJuniorUnderwriterInPMLCertificate" Text="Show Junior Underwriter in Certificate" Runat="server" /><br />
						<asp:CheckBox runat="server" ID="m_IsUseLayeredFinancing" Text="Use Layered Financing" /> <br />
						<asp:CheckBox runat="server" ID="m_IsRemovePreparedDates" Text="Remove Prepared Dates- Default to current day" /><br />
						<asp:CheckBox runat="server" ID="m_IsProtectDisclosureDates" Text="Protect Disclosure/Redisclosure Dates" /> <br />
						<asp:CheckBox runat="server" ID="m_IsEnableDTPkgBsdRedisclosureDatePop" Text="Enable DocuTech package based redisclosure date population" /><br />
						<asp:CheckBox runat="server" ID="m_IsB4AndB6DisabledIn1100And1300OfGfe" Text="Disable B6 and B4 in GFE 1100s and 1300s" /><br />
						<asp:CheckBox runat="server" ID="m_IsUseNewTaskSystemStaticConditionIds" Text="Use static condition row IDs? (run the OPM 87499 migration BEFORE turning this on, unless this is a new broker)" /><br />
                        <asp:CheckBox runat="server" ID="EnableEConsentMonitoringAutomation" Enabled="false" /> Enable E-Consent Expiration Monitoring For Disclosure Pipeline (OPM 475638)<br />
                        <span class="indented">
                            <asp:LinkButton runat="server" ID="DownloadAffectedEConsentExpirationFiles" OnClick="DownloadAffectedEConsentExpirationFiles_Click" title="Downloads a CSV of files that must be migrated internally before this feature can be disabled. See case 475638 for more information." Text="view affected files" />
                        </span>
						</div>
					</fieldset>

                    <br />
                    <fieldset>
                        <legend>Custom Disclosure Pipeline Behavior</legend>
                        <span class="FieldLabel">
                            Ideally this should be edited outside of working hours. <br />
                            If it is edited mid-day, it may result in false positives, especially for delayed triggers.<br />
                        </span>
                        <asp:CheckBox runat="server" id="m_EnableCocRedisclosureTriggersForHelocLoans" Text="Enable CoC Redisclosure Triggers for HELOC Loans" /><br />
                        <asp:CheckBox runat="server" id="m_UseCustomCocFieldList" Text="Use Custom CoC Field List" /><br />
                        <input type="button" value="Edit Custom Fields" onclick="onCustomDisclosurePipelineEditClick()"/>
                    </fieldset>

					<br/>
					<fieldset>
						<legend>OBSOLETE</legend>
						<asp:CheckBox ID="m_IsAllowSharedPipeline" Text="Allow shared pipeline? (OBSOLETE setting - Always enabled)" runat="server" Checked="true" Enabled="false" /><br />
                        <asp:CheckBox ID="m_CombineGFEAndSettlementChargesClosingCostData_Obsolete" Text ="Combine GFE and Settlement Charges closing cost data (OBSOLETE setting - Always enabled)" Enabled="false" Checked="true" runat="server" /> <br />
                        <asp:CheckBox ID="m_EnableExpandedLeadEditor" Text="Enable Expanded Lead Editor? (OBSOLETE setting - Always enabled per OPM 214844)" runat="server" Checked="true" Enabled="false" /><br />
                        <asp:CheckBox ID="m_IsUsingOldLoanEditorUI" Text="Use the Old Loan Editor? (OBSOLETE setting - Always disabled per OPM 214844)" runat="server" Checked="false" Enabled="false" /><br />
                        <asp:CheckBox ID="m_EnableNewSupportCenter" Text="Enable the new Kayako support center? (OBSOLETE setting - Always enabled per OPM 230834)" runat="server" Checked="true" Enabled="false" /><br />
						<asp:CheckBox ID="m_showROPricedWorseBPPPOn" Text="Show rate options priced worse than lower rates when using 'Best Prices Per Program' (OBSOLETE - Replaced by Show Rate Options Worse)" runat="server" Checked="false" Enabled="false" /> <br />
						<asp:CheckBox ID="m_ShowROPricedworseBPPPOff"  Text="Show rate options priced worse than lower rates when NOT using 'Best Prices Per Program' (OBSOLETE - Show Rate Options Worse)" runat="server"  Checked="true" Enabled="false" /> <br />
					</fieldset>

					<br/>
					<fieldset>
						<legend>Originator Comp</legend>
						<div class = "section-left">
					    4/1/2011 Originator compensation rules mandatory for loans opened after <ml:DateTextBox Text="4/1/2011" runat="server" ID="m_OriginatorCompensationMigrationDate" onchange="handleOriginationData()" ></ml:DateTextBox>
					    <br />
						<asp:CheckBox runat="server" ID="m_IsAddPointWithoutOriginatorCompensationToRateLock" Text="Add Point w/o originator compensation to rate lock confirmation?" /> <br />
						<asp:CheckBox runat="server" ID="m_IsAlwaysDisplayExactParRateOption" Text="Always Display Exact Par Rate Option by Default for Price Groups" /> <br />
						<asp:CheckBox runat="server" ID="m_IsHideLOCompPMLCert" Text="Always hide LO Comp table on PML Cert" /> <br />
						<asp:CheckBox runat="server" ID="m_IsHidePricingwithoutLOCompPMLCert" Text="Always hide Pricing w/o LO Comp on PML Cert" /> <br />
						</div>

						<div class = "section-right">
						<asp:CheckBox runat="server" ID="m_IsApplyPricingEngineRoundingAfterLoComp" Text="Apply Pricing Engine Rounding after LO Comp" /> <br />
						<asp:CheckBox runat="server" ID="m_IsDisplayCompensationChoiceInPml" Text="Display Compensation Choice In Pml" /> <br />
						<asp:CheckBox runat="server" ID="m_IsUsePriceIncludingCompensationInPricingResult" Text="Include originator comp in price results" Checked="true" /> <br />
						<asp:CheckBox runat="server" ID="m_BorrPdCompMayNotExceedLenderPdComp" Text="Borrower-paid compensation may not exceed lender-paid compensation" /> <br />
						</div>
					</fieldset>

					<br/>
					<fieldset>
						<legend>PML</legend>

						<div class = "section-left">
						<asp:checkbox id="m_is8020ComboAllowedInPml" Text="Allow 80/20 Combos in PML?" Checked="True" Runat="server" /><br>
						<asp:checkbox id="m_isPmlPointImportAllowed" runat="server" Text="Allow PML users to import from Point?" /><br>
						<asp:checkbox id="m_AllowPmlOrderNewCreditReport" Text="Allow Price My Loan user to order new credit reports" Runat="server" /><br>
						<asp:checkbox id="m_isStandAloneSecondAllowedInPml" Text="Allow Standalone 2nd Liens in PML?" Checked="True" Runat="server" /><br>
						<asp:checkbox id="m_isLpeManualSubmissionAllowed" runat="server" Text="Allow users to submit loans for an exception?" Checked="True" /><br>
						<asp:checkbox id="m_isAllPrepaymentPenaltyAllowed" Text="Comply with State Prepay Laws?" Checked="True" Runat="server" /><br>
						<asp:checkbox id="m_isShowPriceGroupInEmbeddedPml" runat="server" Text="Display price group for internal users?" /><br>
						<asp:CheckBox id="m_ShowQMStatusInPml2" runat="server" Text="Show QM Info On PML 2.0 and Certificate? (Excludes correspondent channel files)" />
						</div>

						<div class = "section-right">
						<asp:CheckBox ID="m_IsForceChangePasswordForPml" Text="Force Password Change Upon Login When Manager Changes Password in PML?" runat="server" /><br />
						<asp:checkbox id="m_isLpeDisqualificationEnabled" Text="Has disqualifications enabled in LPE?" Checked="True" Runat="server" /><br>
						<asp:CheckBox ID="m_IsaBEmailRequiredInPml" Text="Is Borrower E-mail required in PML?" runat="server" /><br />
						<asp:checkbox id="m_isOptionARMUsedInPml" Text="Is Option ARM Used in PML?" Runat="server" /><br>
						<asp:checkbox id="m_PmlRequireEstResidualIncome" Text="Require Estimated Residual Income in PML" Runat="server" /><br>
						<asp:checkbox id="m_RequireVerifyLicenseByState" disabled Text="Require validating PML user lending licenses by state (FOR FUTURE USE)" Runat="server" /><br>
						</div>
					</fieldset>


					<br/>
					<fieldset>
						<legend>Pricing Engine</legend>

						<div class = "section-left">
						<asp:CheckBox id="m_isBestPriceEnabled" Text="Allow 'Best Prices per Program' in PML in PML 1.0" Runat="server" /><br />
						<asp:CheckBox id="m_IsAlwaysUseBestPrice" Text="Always use 'Best Prices per Program' in PML 2.0" Runat="server" /><br />
						<asp:CheckBox id="m_IsShowRateOptionsWorseThanLowerRate" Text="Show Rate Options Worse" Runat="server" /><br />
						<asp:PlaceHolder runat="server" ID="CalculateClosingCostPanel">
					        <asp:CheckBox runat="server" ID="m_CalculateclosingCostInPML" Text="Calculate closing costs in PML" onclick="onCalculateClosingCostInPml(this)" /> <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" ID="m_ApplyClosingCostsToGfeOnSubmission" Text="Apply Closing Costs To Loan File On Submission" /><br />
					    </asp:PlaceHolder>

						</div>

						<div class = "section-right">
						&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox runat="server" ID="m_OriginatorCompensationApplyMinYSPForTPOLoan" /> Apply Min YSP of 0 to lender paid TPO loans. <br />
						<asp:CheckBox ID="m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSP"  Text="Display Rate Options Above Lowest Triggering MaxYSP?" runat="server"  Checked="false" /> <br />
						<asp:CheckBox ID="m_IsPricingMultipleAppsSupported" Text="Enable Pricing Multiple Apps?" runat="server" Checked="true" /><br />
						<asp:checkbox id="m_isRoundUpLpeRateTo1Eighth" runat="server" Text="Is round up lpe rate to 1/8?" /><br>
						</div>
					</fieldset>

					<br/>
					<fieldset>
						<legend>User Defaults</legend>
						<div class="section-left">
						    <asp:checkbox id="m_canRunPricingEngineWithoutCreditReportByDefault" runat="server" Text="Can run pricing engine without credit report by default?" /><br>
						    <asp:checkbox id="m_cannotCreateLoanTemplateByDefault" runat="server" Text="Cannot create loan template by default?" /><br>
						    <asp:checkbox id="m_cannotDeleteLoanByDefault" runat="server" Text="Cannot delete loan by default?" /><br>
						    <asp:checkbox id="m_AllowAllUsersToUseKayako" runat="server" Text="Can contact support through Kayako in any role?" /><br>
						</div>

						<div class="section-right">
						    <asp:checkbox id="m_restrictReadingRolodexByDefault" runat="server" Text="Cannot read from Contacts by default?" /><br>
						    <asp:checkbox id="m_restrictWritingRolodexByDefault" runat="server" Text="Cannot write to Contacts by default?" /><br>
						    <asp:checkbox id="m_isAllowViewLoanInEditorByDefault" runat="server" Text="Is allow view loan in editor by default?" /><br />
                            Minimum default Closing Cost Data Set settings: <asp:DropDownList ID="m_minimumDefaultClosingCostDataSet" runat="server"></asp:DropDownList>
						</div>
					</fieldset>


					</TD>
					</TR>
				<tr style="PADDING-TOP: 1em">
					<td class="FieldLabel" valign="top">
						Automated Underwriting (AUS) Configuration
					</td>
					<td>
					    <table class="InsetBorder" cellSpacing="0" cellPadding="5" border="0">
					    <tr>
					        <td class="FieldLabel" valign="top">
					            Default Import Options
					        </td>
					    </tr>
					    <tr>
					        <td>
					        <asp:CheckBox runat="server" ID="m_IsImportDu1003ByDefault" Text='Import 1003 Data' /><br />
					        <asp:CheckBox runat="server" ID="m_IsImportDuFindingsByDefault" Text='AUS Findings' /><br />
					        <asp:CheckBox runat="server" ID="m_IsImportDuCreditReportByDefault" Text='Credit Report' /><br />
					        </td>
					    </tr>
					    </table>
					</td>
				</tr>
				<tr style="PADDING-TOP: 1em">
				    <td class="FieldLabel" valign="top">
				        Active Appraisal Integrations
				    </td>
				    <td>
				        <div style="width:300px; height:100px; overflow-y:scroll" class="InsetBorder">
				            <asp:Repeater runat="server" ID="m_AppraisalIntegrations" OnItemDataBound="BindAppraisalVendor">
				                <ItemTemplate>
				                    <asp:HiddenField runat="server" ID="m_vendorId" />
				                    <asp:CheckBox runat="server" ID="m_EnableAppraisalVendor" />
				                    <br />
				                </ItemTemplate>
				            </asp:Repeater>
				        </div>
				    </td>
				</tr>
				<tr>
				    <td class="FieldLabel" valign="top">Active 4506-T Integrations</td>
				    <td>
				        <div style="width:300px; height:100px; overflow-y:scroll" class="InsetBorder">
				            <asp:CheckBoxList ID="m_4506VendorList" runat="server" AutoPostBack="false" />
				        </div>
				    </td>
				</tr>
				<tr>
				    <td class="FieldLabel" valign="top">Data Retrieval Framework Partners</td>
				    <td>
				        <div style="width:300px; height:100px; overflow-y:scroll" class="InsetBorder">
				            <asp:CheckBoxList ID="m_DRFPartnerList" runat="server" AutoPostBack="false" />
				        </div>
				    </td>
				</tr>

				<tr>
				    <td class="FieldLabel" valign="top">Trade Numbering</td>
				    <td>
				        <div style="width:300px; height:100px; overflow-y:scroll" class="InsetBorder">
				            <asp:CheckBox ID="m_IsAutoGenerateTradeNums" runat="server" Text="Automatically Number Trades" onclick="f_onIsAutoGenerateTradeNumsClick();" />
				            <div id="divTradeCounter" class="indent">
				                <label>Trade Number Counter:</label>
					            <asp:textbox id="m_TradeCounter" style="PADDING-LEFT: 4px" runat="server" Width="100px" />
					            <asp:RegularExpressionValidator
									id="m_TradeCounterRegExVal"
									runat="server"
									ControlToValidate="m_TradeCounter"
									EnableClientScript="true"
									ValidationExpression="^\d+$">
									<img runat="server" src="../../images/error_icon.gif" />
								</asp:RegularExpressionValidator>
				            </div>
				        </div>
				    </td>
				</tr>

				<tr>
				    <td class="FieldLabel" valign="top">Mortgage Pool ID Numbering</td>
				    <td>
				        <div style="width:300px; height:100px; overflow-y:scroll" class="InsetBorder">
			                <label>Pool ID Counter:</label>
				            <asp:textbox id="m_PoolCounter" style="PADDING-LEFT: 4px" runat="server" Width="100px" />
				            <asp:RegularExpressionValidator
								id="RegularExpressionValidator1"
								runat="server"
								ControlToValidate="m_PoolCounter"
								EnableClientScript="true"
								ValidationExpression="^\d+$">
								<img runat="server" src="../../images/error_icon.gif" />
							</asp:RegularExpressionValidator>
				        </div>
				    </td>
				</tr>
				<tr>
				    <td class="FieldLabel" style="padding-top:1em">
				        Closing Cost Lender fee Setup
				    </td>

				    <td>
				        <asp:Button runat="server" id="DownloadClosingCostSetup" Text="Download Closing Cost Setup" OnClick="DownloadClosingCostSetup_OnClick" />
				        &nbsp;&nbsp;&nbsp;&nbsp;Import New Setup <asp:FileUpload runat="server" ID="ClosingCostSetFile" Width="200" /> <asp:Button runat="server" ID="UploadClosingCostSetBtn" Text="Import" OnClick="UploadClosingCostSetup_OnCLick"  />
				    </td>
				</tr>
				<tr style="padding-top:1em;">
				    <td class="FieldLabel" valign="top">
						Custom Field XML
					</td>
					<td>
					    <asp:TextBox runat="server" ID="m_sCustomFieldDescriptionXml" TextMode="MultiLine" nouppercase="true" Width="450px" Height="100"/>
					    <br />
					    <a href="#" onclick="editCustomFieldXML(); return false;">Build XML</a>
					</td>
				</tr>
				<tr style="PADDING-TOP: 1em">
					<td class="FieldLabel" valign="top">Sandbox Setup
					LOXML</td>
					<td>
					    <asp:TextBox id="m_SandboxScratchLOXml" runat="server" Width="450px" nouppercase="true" TextMode="MultiLine" Height="300px" />
					</td>
				</tr>
				<tr style="PADDING-TOP: 1em">
					<td class="FieldLabel" valign="top">
						DDL Configuration
					</td>
					<td>
						<asp:textbox id="m_ddlXmlConfig" style="PADDING-LEFT: 4px" runat="server" Width="450px" nouppercase="true" TextMode="MultiLine" Height="90px"/>
						<br>
						Changes here will not be loaded until asp.net process restart.
						<br>
						<b>Schema </b><code>
							<pre>&lt;?xml version="1.0" encoding="utf-16"?&gt;
&lt;fields&gt;
	&lt;field Name="sLPurposeT" NameCount="1" Name0= "sLPurposeTPe"&gt;
		&lt;hide Value="3" /&gt;
		&lt;hide Value="4" /&gt;
		&lt;hide Value="5" /&gt;
	&lt;/field&gt;
&lt;/fields&gt;</pre>
						</code>
					</td>
				</tr>

                <tr>
                    <td class="FieldLabel" valign="top">PML certificate determination configuration</td>
                    <td>
                        <asp:TextBox ID="DeterminationRulesXMLContent" runat="server" style="width:450px; height: 250px;" TextMode="MultiLine" nouppercase="true"/>
                    </td>
                </tr>

				<tr id="trNamingPattern" runat="server">
				    <td>
				        <b>Naming Pattern</b>
				    </td>
				    <td>
	                    <asp:TextBox ID="m_namingPattern" runat="server" MaxLength="50" nouppercase="true"/>
	                </td>
				</tr>
				<tr id="trLeadNamingPattern" runat="server">
				    <td>
                        <b>Lead Naming Pattern</b>
                    </td>
                    <td>
	                    <asp:TextBox ID="m_leadNamingPattern" runat="server" MaxLength="50" nouppercase="true"/>
	                    <asp:Button ID="m_copyPattern" runat="server" Text="Copy From Naming Pattern" OnClientClick="f_onCopyPatternClick(); return false;" />
	                </td>
				</tr>
				<tr colspan="4">
				    <td class="FieldLabel">
				        EDMS Configuration
				    </td>
				    <td>
				        <asp:CheckBox ID="m_IsEDocsEnabled" Text="Enable Electronic Document Management System" runat="server" onclick="f_onEdmsClick( false )" />

				    </td>
				    </tr>
				    <tr>
				    <td class="FieldLabel">Enable EDMS in PML?</td>
					<td>
						<asp:DropDownList Runat="server" ID="m_EDocsPmlEnabledStatus" >
							<asp:ListItem Value="0" > No </asp:ListItem>
							<asp:ListItem Value="1" > Yes </asp:ListItem>
						</asp:DropDownList>
						(will eventually have more options, per case 46783)
					</td>
					</tr>
				<tr>
				<td>
				</td>
				<td>
				Fax Number Type:<br />
				<asp:RadioButton id="m_faxShared"  runat="server" Text="Shared" GroupName="FaxNumberType" style="padding-left:12px" onclick="f_onFaxTypeChange()" /><br />
				<asp:RadioButton id="m_faxPrivate" runat="server" Text="Private" GroupName="FaxNumberType" style="padding-left:12px" onclick="f_onFaxTypeChange()" />
				</td>
				</tr>
                <tr>
                    <td></td><td>
                        <table class="InsetBorder" cellspacing="0" cellpadding="5" border="0">
                            <tr>
                                <td colspan="2" class="FieldLabel">
                                    Configure Private Fax Number
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Fax Number
                                </td>
                                <td>
                                    <asp:TextBox ID="m_FaxNumber" runat="server" MaxLength="14"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Doc Router Login
                                </td>
                                <td>
                                    <asp:TextBox ID="m_DocRouterLogin" runat="server" MaxLength="50"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Doc Router Password
                                </td>
                                <td>
                                    <asp:TextBox ID="m_DocRouterPassword" runat="server" MaxLength="50"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
				<tr>
				    <td class="FieldLabel" valign="top">
                        <ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="m_quickPricerDisclaimerAtResult" >Quick Pricer Disclaimer at Result </ml:EncodedLabel>
                    </td>
				    <td>
				        <asp:textbox id="m_quickPricerDisclaimerAtResult" style="PADDING-LEFT: 4px" runat="server" Width="450px" nouppercase="true" TextMode="MultiLine" Height="90px"/>
				    </td>
				</tr>
				<tr>
				    <td class="FieldLabel" valign="top">
                        <ml:EncodedLabel ID="Label3" runat="server" AssociatedControlID="m_quickPricerNoResultMessage" >Quick Pricer No Result Message </ml:EncodedLabel>
                    </td>
				    <td>
				        <asp:textbox id="m_quickPricerNoResultMessage" style="PADDING-LEFT: 4px" runat="server" MaxLength="500" Width="450px" nouppercase="true" TextMode="MultiLine" Height="90px"/>
				    </td>
				</tr>
				<tr>
					<td class="FieldLabel">LPE Fee Rounding</td>
					<td><asp:radiobutton id="m_dontRoundUpLpeFee" onclick="onRoundLpeFeeClick();" Text="No Rounding" Runat="server" GroupName="roundLpeFee"></asp:radiobutton><asp:radiobutton id="m_roundUpLpeFee" onclick="onRoundLpeFeeClick();" Text="Round up LPE Fee to Nearest: " Runat="server" GroupName="roundLpeFee"></asp:radiobutton>&nbsp;<asp:textbox id="m_roundUpLpeFeeInterval" onblur="FormatRoundInterval();" runat="server" Width="50px" MaxLength="5"/>
						<asp:rangevalidator id="m_rangeValRoundLpeFeeInterval" runat="server" ControlToValidate="m_roundUpLpeFeeInterval" Display="Dynamic" ErrorMessage="Please enter a valid value between .002 and .999." MinimumValue=".002" MaximumValue=".999" Type="Double"></asp:rangevalidator></td>
				</tr>
				<tr>
				    <td class="FieldLabel">Lock Period Dropdown Enabled?</td>
				    <td>
				        <asp:CheckBox ID="m_IsEnabledLockPeriodDropdown" runat="server" Text="Available Lock Periods (days)"/>
				        <asp:TextBox ID="m_AvailableLockPeriod_1" runat="server" Width="50"/>
				        <asp:TextBox ID="m_AvailableLockPeriod_2" runat="server"/>
				        <asp:TextBox ID="m_AvailableLockPeriod_3" runat="server"/>
				        <asp:TextBox ID="m_AvailableLockPeriod_4" runat="server"/>
				        <asp:TextBox ID="m_AvailableLockPeriod_5" runat="server"/>
				        <asp:TextBox ID="m_AvailableLockPeriod_6" runat="server"/>
				        <asp:TextBox ID="m_AvailableLockPeriod_7" runat="server"/>
				        <asp:TextBox ID="m_AvailableLockPeriod_8" runat="server"/>
				        <asp:TextBox ID="m_AvailableLockPeriod_9" runat="server"/>
				    </td>
				</tr>
				<tr>
				    <td colspan="2"> <asp:CheckBox ID="m_IsDay1DayOfRateLock" Text="Broker lock 'Day One' is the day of the lock (broker locks expire one day early)" runat="server" /></td>
				</tr>
				<tr>
				    <td colspan="2">
				    When rate lock expiration falls on a weekend/holiday then:
				    <asp:RadioButtonList runat="server" ID="m_rateLockExpirationWeekendHolidayBehavior"></asp:RadioButtonList>
				    </td>
				</tr>
				<tr>
					<td class="FieldLabel">Number of bad pw before lock</td>
					<td><asp:textbox id="m_numberBadPasswordsAllowed" style="PADDING-LEFT: 4px" runat="server" Width="40" MaxLength="3"/> (requested by Citi, PML only)
						<asp:rangevalidator id="m_rangeNumBadPw" runat="server" ControlToValidate="m_numberBadPasswordsAllowed" Display="Dynamic" ErrorMessage="Please enter a valid whole value between 1 and 100." MinimumValue="1" MaximumValue="100" Type="Integer"></asp:rangevalidator></td>
				</tr>
				</TD></TR>
				<tr>
					<td class="FieldLabel">PML User DO/DU Auto Login</td>
					<td>
						<asp:DropDownList Runat="server" ID="m_doduAutoLoginOption" >
							<asp:ListItem Value="Off" > Off </asp:ListItem>
							<asp:ListItem Value="MustBeUnique" > Must be unique for each user </asp:ListItem>
							<asp:ListItem Value="Sharable"> Sharable	</asp:ListItem>

						</asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="FieldLabel">PML Default Template</td>
					<td><asp:dropdownlist id="PmlLoanTemplateId" runat="server"></asp:dropdownlist></td>
				</tr>

				<TR>
					<TD class="FieldLabel">Naming Counter
					</TD>
					<TD><asp:textbox id="m_NamingCounter" style="PADDING-LEFT: 4px" runat="server" Width="120px" ReadOnly="True"/><INPUT onclick="<%= AspxTools.JsGetElementById(m_NamingCounter) %>.value = '0';" type=button value="Reset counter">
						</INPUT>
					</TD>
				</TR>
				<tr>
				    <td class="FieldLabel">Linked Loan Update fields: <input type="button" value="Check Fields" id="CheckLinkedLoanUpdateFieldsString" /></td>
				    <td><asp:TextBox TextMode="MultiLine" ID="LinkedLoanUpdateFieldsString" runat="server" NoUppercase="true" Width="450px" Height="90px" /></td>
				</tr>
			</TABLE>
			<DIV style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; WIDTH: 100%; PADDING-TOP: 4px">
				<table cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tr>
						<td class="FieldLabel">Credit Agency Name</td>
					</tr>
					<tr>
						<td><asp:dropdownlist id="CRA0" runat="server" onchange="onCreditReportProtocolChange(this, 0);" EnableViewState="False"></asp:dropdownlist><asp:checkbox id="IsPdfViewNeeded0" runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick="removeCRA(0);" type="button" value="Clear"></td>
					</tr>
					<tr>
						<td style="HEIGHT: 13px"><asp:dropdownlist id="CRA1" runat="server" onchange="onCreditReportProtocolChange(this, 1);" EnableViewState="False"></asp:dropdownlist><asp:checkbox id="IsPdfViewNeeded1" runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick="removeCRA(1);" type="button" value="Clear"></td>
					</tr>
					<tr>
						<td><asp:dropdownlist id="CRA2" runat="server" onchange="onCreditReportProtocolChange(this, 2);" EnableViewState="False"></asp:dropdownlist><asp:checkbox id="IsPdfViewNeeded2" runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick="removeCRA(2);" type="button" value="Clear"></td>
					</tr>
					<tr>
						<td><asp:dropdownlist id="CRA3" runat="server" onchange="onCreditReportProtocolChange(this, 3);" EnableViewState="False"></asp:dropdownlist><asp:checkbox id="IsPdfViewNeeded3" runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick="removeCRA(3);" type="button" value="Clear"></td>
					</tr>
					<tr>
						<td><asp:dropdownlist id="CRA4" runat="server" onchange="onCreditReportProtocolChange(this, 4);" EnableViewState="False"></asp:dropdownlist><asp:checkbox id="IsPdfViewNeeded4" runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick="removeCRA(4);" type="button" value="Clear"></td>
					</tr>
					<tr>
						<td><asp:dropdownlist id="CRA5" runat="server" onchange="onCreditReportProtocolChange(this, 5);" EnableViewState="False"></asp:dropdownlist><asp:checkbox id="IsPdfViewNeeded5" runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick="removeCRA(5);" type="button" value="Clear"></td>
					</tr>
					<tr>
						<td><asp:dropdownlist id="CRA6" runat="server" onchange="onCreditReportProtocolChange(this, 6);" EnableViewState="False"></asp:dropdownlist><asp:checkbox id="IsPdfViewNeeded6" runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick="removeCRA(6);" type="button" value="Clear"></td>
					</tr>
					<tr>
						<td><asp:dropdownlist id="CRA7" runat="server" onchange="onCreditReportProtocolChange(this, 7);" EnableViewState="False"></asp:dropdownlist><asp:checkbox id="IsPdfViewNeeded7" runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick="removeCRA(7);" type="button" value="Clear"></td>
					</tr>
					<tr>
						<td><asp:dropdownlist id="CRA8" runat="server" onchange="onCreditReportProtocolChange(this, 8);" EnableViewState="False"></asp:dropdownlist><asp:checkbox id="IsPdfViewNeeded8" runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick="removeCRA(8);" type="button" value="Clear"></td>
					</tr>
					<tr>
						<td><asp:dropdownlist id="CRA9" runat="server" onchange="onCreditReportProtocolChange(this, 9);" EnableViewState="False"></asp:dropdownlist><asp:checkbox id="IsPdfViewNeeded9" runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick="removeCRA(9);" type="button" value="Clear"></td>
					</tr>
					<TR>
						<TD class="FieldLabel">&nbsp;</TD>
					</TR>
					<tr>
						<td class="FieldLabel">Credco CRA Option
							<asp:dropdownlist id="ShowCredcoOptionT" runat="server"></asp:dropdownlist></td>
					</tr>
					<TR>
						<TD class="FieldLabel">DU User Name&nbsp;
							<asp:textbox id="CreditMornetPlusUserID" runat="server" NoUppercase="True"/></TD>
					</TR>
					<TR>
						<TD class="FieldLabel">DU Password&nbsp;
							<asp:textbox id="CreditMornetPlusPassword" runat="server" NoUppercase="True"/></TD>
					</TR>
				</table>
			</DIV>
			<table class="InsetBorder" cellSpacing="0" cellPadding="5" border="0">
				<tr>
					<td class="FieldLabel">Lender defined CRA</td>
					<td class="FieldLabel">NOTE: There is an option to hide the Lender defined CRA from
						"P" users. The setting is in Broker General settings</td>
				</tr>
				<tr>
					<td class="FieldLabel" colSpan="2"><asp:checkbox id="m_AllowPmlViewReport" runat="server" Text="Allow users to  view credit reports pulled through this connection in PML"></asp:checkbox></td>
				</tr>
				<tr>
					<td class="FieldLabel">Lender CRA</td>
					<td class="FieldLabel"><asp:dropdownlist id="CreditProxy_CraId" runat="server" EnableViewState="False"></asp:dropdownlist></td>
				</tr>
				<TR>
					<TD class="FieldLabel">DU User ID (For DU CRA only)</TD>
					<TD class="FieldLabel"><asp:textbox id="CreditProxy_CreditMornetPlusUserId" runat="server" NoUppercase="True"/></TD>
				</TR>
				<TR>
					<TD class="FieldLabel">DU Password (For DU CRA Only)</TD>
					<TD class="FieldLabel"><asp:textbox id="CreditProxy_CreditMornetPlusPassword" runat="server" NoUppercase="True"/></TD>
				</TR>
				<tr>
					<td class="FieldLabel">Description</td>
					<td class="FieldLabel"><asp:textbox id="CreditProxy_CrAccProxyDesc" runat="server" Width="475px" MaxLength="100"/></td>
				</tr>
				<tr>
					<td class="FieldLabel">Account ID</td>
					<td class="FieldLabel"><asp:textbox id="CreditProxy_CraAccId" runat="server" maxlength="100" NoUppercase="True"/></td>
				</tr>
				<tr>
					<td class="FieldLabel">User Name</td>
					<td class="FieldLabel"><asp:textbox id="CreditProxy_CraUserName" runat="server" maxlength="100" NoUppercase="True"/></td>
				</tr>
				<tr>
					<td class="FieldLabel">Password</td>
					<td class="FieldLabel"><asp:textbox id="CreditProxy_CraPassword" runat="server" maxlength="100" NoUppercase="True"/></td>
				</tr>
				<tr>
					<td class="FieldLabel"></td>
					<td class="FieldLabel"><asp:checkbox id="CreditProxy_IsExperianPulled" runat="server" Text="Experian"></asp:checkbox><asp:checkbox id="CreditProxy_IsEquifaxPulled" runat="server" Text="Equifax"></asp:checkbox><asp:checkbox id="CreditProxy_IsTransunionPulled" runat="server" Text="TransUnion"></asp:checkbox><asp:checkbox id="CreditProxy_IsFannieMaePulled" runat="server" Text="Fannie Mae"></asp:checkbox></td>
				</tr>
			</table>
			<table id="Table1" cellSpacing="0" cellPadding="0" width="1200" border="0" class="InsetBorder">
				<tr>
					<td colSpan="2"><b>Broker Temporary Options</b></td>
				<tr>
				<tbody>
				<asp:Repeater ID="m_tempOptionsListRepeater" runat="server" EnableViewState=false>
				<HeaderTemplate>
				<tr>
				  <td style="font-weight:bold">Text to paste in OPTION input text</td>
				  <td>&nbsp;</td>
				</tr>
				</HeaderTemplate>
				<ItemTemplate>

				<tr>
				  <td class="TempOptionKey"><%# AspxTools.HtmlString(Eval("Key").ToString()) %></td>
				  <td>
				    <input type="button" onclick="return copyTextToClipboard(<%# AspxTools.JsString(Eval("Key").ToString()) %>);" value="Copy" />
				    <%# AspxTools.HtmlString(Eval("Value").ToString()) %>
				  </td>
				</tr>
				</ItemTemplate>
				</asp:Repeater>
				</tbody>
				<tr>
					<td colspan="2"><asp:textbox id="TempOptionXmlContent" style="PADDING-LEFT: 4px" runat="server" Width="100%" nouppercase="true" Height="300px" TextMode="MultiLine" /></td>
				</tr>
			</table>
			<br />
			<table width=100% border=0>
			<tr><td><b>Rate Expiration Warning Messages</b></td><td></td></tr>
    <tr><td class="FieldLabel"> Stale Pricing  <br> <a href='#"' onclick='f_showDefault("NormalUserExpiredDetails", event);'?>View Default</a>  <a href='#"' onclick='f_setDefault("NormalUserExpiredDetails");'?>Set to Default</a></td><td><asp:TextBox id="m_newNormalUserExpiredMessage" nouppercase="true" TextMode=MultiLine rows="3" Columns=90 runat=server/></td></tr>
    <tr><td class="FieldLabel"> Stale Pricing for Lock Desk Users  <a href='#"' onclick='f_show("LockDeskExpiredDetails", event);'?>?</a><br> <a href='#"' onclick='f_showDefault("LockDeskExpiredDetails", event);'?>View Default</a>  <a href='#"' onclick='f_setDefault("LockDeskExpiredDetails");'?>Set to Default</a></td><td><asp:TextBox id="m_newLockDeskExpiredMessage" nouppercase="true" TextMode=MultiLine rows="3" Columns=90 runat=server/></td></tr>
    <tr><td class="FieldLabel"> Passed Investor Cut-off  <br> <a href='#"' onclick='f_showDefault("NormalUserCutOffDetails", event);'?>View Default</a>  <a href='#"' onclick='f_setDefault("NormalUserCutOffDetails");'?>Set to Default</a></td><td><asp:TextBox id="m_newNormalUserCutOffMessage" nouppercase="true" TextMode=MultiLine rows="3" Columns=90 runat=server/></td></tr>
    <tr><td class="FieldLabel"> Passed Investor Cut-off for Lock Desk Users <a href='#"' onclick='f_show("LockDeskCutOffDetails", event);'?>?</a><br /> <a href='#"' onclick='f_showDefault("LockDeskCutOffDetails", event);'?>View Default</a>  <a href='#"' onclick='f_setDefault("LockDeskCutOffDetails");'?>Set to Default</a></td><td><asp:TextBox id="m_newLockDeskCutOffMessage" nouppercase="true" TextMode=MultiLine rows="3" Columns=90 runat=server/></td></tr>
    <tr><td class="FieldLabel"> Lock Desk is Closed <br /> <a href='#"' onclick='f_showDefault("OutsideNormalHoursDetails", event);'?>View Default</a>  <a href='#"' onclick='f_setDefault("OutsideNormalHoursDetails");'?>Set to Default</a></td><td><asp:TextBox id="m_newOutsideNormalHoursMessage" nouppercase="true" TextMode=MultiLine rows="3" Columns=90 runat=server/></td></tr>
    <tr><td class="FieldLabel"> Holiday Closure <br> <a href='#"' onclick='f_showDefault("OutsideClosureDayHourDetails", event);'?>View Default</a>  <a href='#"' onclick='f_setDefault("OutsideClosureDayHourDetails");'?>Set to Default</a></td><td><asp:TextBox id="m_newOutsideClosureDayHourMessage" nouppercase="true" TextMode=MultiLine rows="3" Columns=90 runat=server/></td></tr>


    </table>
	</FORM>
	</BODY>
<div id="showDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:330px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; HEIGHT:50px; BACKGROUND-COLOR:whitesmoke">
		<table width="100%"><tr><td><label id="detailLabel"></label></td></tr>
		<tr><td align=center>[ <a href="#" onclick="f_close('showDetails');">Close</a> ]</td></tr>
		</table>
		</div>
<div id="defaultView" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:330px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; HEIGHT:50px; BACKGROUND-COLOR:whitesmoke">
			<table width="100%"><tr><td><label id="defaultLabel"></label></td></tr>
			<tr><td align=center>[ <a href="#" onclick="f_close('defaultView');">Close</a> ]</td></tr>
			</table>
			</div>
<script type="text/javascript">
    function DocuTechOnlyProductionOrStage(isProduction)
    {
        if (isProduction) { document.getElementById('m_IsDocuTechStageEnabled').checked = false; }
        else { document.getElementById('m_IsDocuTechEnabled').checked = false; }
    }

    $j("#LenderCertBtn").click(function(e) {
        window.open(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/LOAdmin/Broker/ClientCertificateRegistry.aspx")) %> + '?BrokerId=' + g_brokerId, 'clientCertificateRegistry', 'height=500,width=925,menubar=no,scrollbars=yes,resizable=yes');
    });

    $j("#m_isTrackModifiedFile_Configure").click(function(e){
        if (hasDisabledAttr($j("#m_isTrackModifiedFile_Configure"))) {
            return;
        }
        e.preventDefault();
        window.open(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/LOAdmin/Broker/TrackModifiedFileConfiguration.aspx")) %> + '?BrokerId=' + g_brokerId, 'trackModifiedFileConfiguration', 'height=500,width=925,menubar=no,scrollbars=yes,resizable=yes');
    });

    function onGenericFrameworkVendors()
    {
        window.open(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/LOAdmin/Broker/GenericFrameworkBrokerConfig.aspx")) %> + '?BrokerID=' + g_brokerId, '', 'height=500,width=1100,menubar=no,scrollbars=yes,resizable=yes');
    }
    $j('#CheckLinkedLoanUpdateFieldsString').click(function(){
        callWebMethodAsync({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'BrokerEdit.aspx/CheckLinkedLoanUpdateFieldsString',
            data: JSON.stringify({ fieldString: $j('#LinkedLoanUpdateFieldsString').val() }),
            dataType: 'json',
            async: false,
            success: function(msg) {
                prompt("Copy to clipboard: Ctrl+C, Enter", msg.d);
            },
            failure: function(response) { alert("error - " + response); },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus + errorThrown);
            }
        });
    });
</script>
</HTML>
