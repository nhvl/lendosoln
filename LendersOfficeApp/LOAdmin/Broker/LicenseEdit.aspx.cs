using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
	public partial class LicenseEdit : LendersOffice.Admin.SecuredAdminPage
	{
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker
            };
        }

        protected Guid brokerId
		{
			get
			{
				if (ViewState["brokerId"] == null)
					ViewState["brokerId"] = RequestHelper.GetGuid("brokerId") ;
				return (Guid)ViewState["brokerId"] ;
			}
		}
		protected Guid licenseId
		{
			get
			{
				if (ViewState["licenseId"] == null)
					ViewState["licenseId"] = RequestHelper.GetGuid("licenseId", Guid.Empty) ;
				return (Guid)ViewState["licenseId"] ;
			}
		}
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (!IsPostBack)
				LoadData() ;
		}
		private void LoadData()
		{
			if (licenseId == Guid.Empty)
			{
				// init the expiration date based on the licensee info
				//m_expDate.Text = licensee.CalcExpDate().ToShortDateString() ;
				LendersOfficeApp.ObjLib.Licensing.Licensee licensee = new LendersOfficeApp.ObjLib.Licensing.Licensee(brokerId) ;
				LendersOfficeApp.ObjLib.Licensing.LicenseEstimate estimate = licensee.GetLicenseEstimate(1, false, null) ;
				m_expDate.Text = estimate.ExpirationDate.ToShortDateString() ;

				return ; // done
			}

            SqlParameter[] parameters = { new SqlParameter("@LicenseId", licenseId) };

			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveLicenseByLicenseId", parameters))
			{
                if (reader.Read()) 
                {
                    m_description.Text = SafeConvert.ToString(reader["Description"]) ;
                    m_seatCount.Text = SafeConvert.ToString(reader["SeatCount"]) ;
                    m_expDate.Text = SafeConvert.ToDateTime(reader["ExpirationDate"]).ToShortDateString() ;
                }
			}
		}
		private void SaveData()
		{
			if (licenseId == Guid.Empty)
			{
				LendersOfficeApp.ObjLib.Licensing.Licensee licensee = new LendersOfficeApp.ObjLib.Licensing.Licensee(brokerId) ;
				licensee.AddLicense(Convert.ToInt32(m_seatCount.Text), m_description.Text, Guid.Empty, Guid.Empty, Convert.ToDateTime(m_expDate.Text))  ;
			}
			else
			{
				LendersOfficeApp.ObjLib.Licensing.Licensee licensee = new LendersOfficeApp.ObjLib.Licensing.Licensee(brokerId) ;
				licensee.UpdateLicense(licenseId, Convert.ToInt32(m_seatCount.Text), m_description.Text, Convert.ToDateTime(m_expDate.Text))  ;
			}

			LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "change", "'T'") ;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void m_submit_Click(object sender, System.EventArgs e)
		{
			if (IsValid)
				SaveData() ;
		}
	}
}
