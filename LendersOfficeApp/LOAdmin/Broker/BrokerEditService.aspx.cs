namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Data.SqlClient;
    using DataAccess;
    using DataTrac;
    using LendersOffice.Security;

    public partial class BrokerEditService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "ChangeBrokerStatus":
                    ChangeBrokerStatus();
                    break;
                case "GenerateGuid":
                    GenerateGuid();
                    break;
				case "TestDataTracConnection":
					TestDataTracConnection();
					break;
            }
        }

		private void TestDataTracConnection()
		{
			try
			{
				string pathToDataTrac = GetString("pathToDataTrac");
                string webserviceURL = GetString("webserviceURL");
				if(!string.IsNullOrEmpty(pathToDataTrac))
				{
                    DataTracServer dtServer = new DataTracServer("dmd", "support", pathToDataTrac, webserviceURL);
                    dtServer.TestByCheckIfLoanExistsInDataTrac("PMLTEST");
				}
				else
				{
					SetResult("ErrorMessage", "Parameter 'pathToDataTrac' not supplied.");
				}
			}
			catch(Exception e)
			{
                // Getting the login information is invalid error is actually indicative of success,
                // because it means the webservice was able to connect to the client's server and was able to connect
                // the client's DataTrac installation, which is required in order to get a login validation message.
                if (!e.Message.ToLower().Contains("login information is invalid"))
                {
                    Tools.LogError("Error testing DataTrac connection: " + e);
                    SetResult("ErrorMessage", e.ToString());
                }
			}
		}

        private void ChangeBrokerStatus() 
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            Guid brokerId = GetGuid("BrokerId");
            int status = GetInt("Status");
            Guid userId = ((LendersOffice.Security.AbstractUserPrincipal) this.User).UserId;

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@Status", status),
                                            new SqlParameter("@UserId", userId)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ChangeBrokerStatus", 0, parameters);
        }

        private void GenerateGuid() 
        {
            SetResult("Guid", Guid.NewGuid().ToString());
        }
    }
}
