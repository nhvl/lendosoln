<%@ Page language="c#" Codebehind="PmlLogos.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.PmlLogos" EnableViewState="false" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<%@ Register TagPrefix="uc1" TagName="headernav" Src="../../common/headernav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../common/header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>PML Logo</title>
<link href="../../css/stylesheet.css" rel="stylesheet"/>
<script type="text/javascript" src="..\..\inc\common.js"></script>
<script type="text/javascript" src="..\..\inc\utilities.js"></script>
<script type="text/javascript">
	var Logo = {
		Set : function(pmlsiteid, name, event) {
			document.getElementById('pmlsiteid').value = pmlsiteid;
			document.getElementById('m_brNm').textContent = name;
			Modal.ShowPopup('imguploaded', 'closeLink', event );
		},
		Clear : function(pmlsiteid) {
			var answer = confirm('Are you sure you want to clear this logo?');
			if ( answer == 0  ) return;
			if ( gService.logo == null ) {
				alert('Services have not been registered. If this problem persist use the "Force Service Load" below the header');
				return;
			}
			var args = new Object();
			args["pmlsiteid"] = pmlsiteid;
			var res = gService.logo.call("ClearPmlLogo", args );

			if ( res.error  ) {
				alert( res.UserMessage );
				return;
			}
			else if ( res.value.okay ) {
				var imgid = 'img'+pmlsiteid.replace(/-/g,'');
				var img = document.getElementById(imgid);
				var mydate = new Date();
				img.src +=  '&' + mydate.getSeconds() + mydate.getMinutes() + mydate.getHours();
				alert('Image removed from filedb.');
			}
			else {
				alert('There was an error processing your request.');
			}
		},

		Submit : function() {
			if ( document.getElementById('img').value == '' ) {
				alert('Please chose an image first.');
				document.getElementById('img').focus();
				return;
			}
			document.getElementById('PmlLogos').submit();
		},

		Show : function(pmlsiteid) {
			pmlsiteid = 'img'+pmlsiteid.replace(/-/g,'');
			document.getElementById(pmlsiteid).style.display = "block";
		},
		Hide : function(pmlsiteid) {
			pmlsiteid = 'img'+pmlsiteid.replace(/-/g,'');
			document.getElementById(pmlsiteid).style.display = "none";
		},
		ShowAll : function() {
			var nodes = document.getElementById('brokers').getElementsByTagName('img');
			for(var i = 0; i < nodes.length; i++ )
			{
				if ( nodes[i].showMe == "true" ) nodes[i].style.display = "block";
			}
		},
		HideAll : function() {
			var nodes = document.getElementsByTagName('img');
			for(var i = 0; i < nodes.length; i++ ) {
				if ( nodes[i].showMe == "true" ) nodes[i].style.display = "none";
			}
		},
		IframeOnLoad : function() {
			if ( Modal.activeModal == '' ) {
				return;
			}
			if ( document.getElementById('submitFrame').contentWindow.document.getElementById('status').innerHTML != "OKAY" ) {
				alert ( document.getElementById('submitFrame').contentWindow.document.getElementById('status').innerHTML );
				return;
			}
			Modal.Hide();
			var pmlsiteid = document.getElementById('pmlsiteid').value ;
			var imgid = 'img'+pmlsiteid.replace(/-/g,'');
			var img = document.getElementById(imgid);
			var mydate = new Date();
			img.src += '&' + mydate.getSeconds() + mydate.getMinutes() + mydate.getHours();
			img.style.display = "block";
		},

		ServiceReload : function () {
			_init_utilsService();
			document.getElementById('t').style.display = 'none';
		}
	};
</script>
<style>
	#brokers { margin: 1em; }
</style>
</head>
<body>

<form id="PmlLogos" method="post" runat="server" target="submitFrame" enctype="multipart/form-data" >
	<iframe style="display:none" id="submitFrame" name="submitFrame" src="javascript:false" onload="Logo.IframeOnLoad()"> </iframe>
	<UC:Header id="m_headerTag" runat="server"></UC:Header>
	<uc1:headernav id="Header1" runat="server">
		<menuitem label="Main" url="~/LOAdmin/main.aspx"></menuitem>
		<menuitem label="Price My Loan Brokers' Logo"></menuitem>
	</uc1:headernav>
	<br/>
	<div style="margin-left: 1em;" >
		[<a href="javascript:void(0)" onclick="Logo.ShowAll()" >Show all logos</a>]
		[<a href="javascript:void(0)" onclick="Logo.HideAll()">Hide all logos</a>]
		<span id='t' > [<a href="javascript:void(0)" onclick="Logo.ServiceReload()"> Force Service Load </a>] </span>
	</div>
	<table id="brokers" cellpadding="0" cellspacing="0" >
		<tr style="color: white; background-color: navy;">
			<th> Broker Name </th>
			<th> Customer Code</th>
			<th> Actions </th>
			<th> Logo </th>
		</tr>
		<asp:Repeater Runat="server" ID="m_brokerList"  >
			<ItemTemplate>
				<tr bgcolor="LightGrey">
					<td>
						<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerNm" ))%>
					</td>
					<td>
						<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CustomerCode"))%>
					</td>
					<td>

						[<a href="javascript:void(0);" onclick="Logo.Show('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId"))%>');"> View </a>]
						[<a href="javascript:void(0);" onclick="Logo.Hide('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId")) %>')" > Hide  </a>]
						[<a href="javascript:void(0);" onclick="Logo.Set('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId"))%>','<%#AspxTools.HtmlString(((String)DataBinder.Eval(Container.DataItem, "BrokerNm")).Replace("'","\\'"))%>', event);"> Set </a>]
						[<a href="javascript:void(0);" onclick="Logo.Clear('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId"))%>');">Clear</a>]
					</td>
					<td>
						<img src="<%#AspxTools.HtmlString(VirtualRoot)%>/LogoPL.aspx?id=<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId"))%>&<%=AspxTools.JsNumeric(DateTime.Now.Minute)%><%=AspxTools.JsNumeric(DateTime.Now.Second)%>"  showMe="true" id="img<%#AspxTools.HtmlString(((Guid)DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId")).ToString().Replace("-",string.Empty))%>" />
					</td>
				</tr>
			</ItemTemplate>
			<AlternatingItemTemplate>
				<tr bgcolor="whitesmoke">
					<td>
						<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerNm")) %>
					</td>
					<td>
						<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CustomerCode"))%>
					</td>
					<td>

						[<a href="javascript:void(0);" onclick="Logo.Show('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId"))%>');"> View </a>]
						[<a href="javascript:void(0);" onclick="Logo.Hide('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId"))%>')" > Hide  </a>]
						[<a href="javascript:void(0);" onclick="Logo.Set('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId"))%>','<%#AspxTools.HtmlString(((String)DataBinder.Eval(Container.DataItem, "BrokerNm")).Replace("'","\\'"))%>', event);"> Set </a>]
						[<a href="javascript:void(0);" onclick="Logo.Clear('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId"))%>');">Clear</a>]
					</td>
					<td>
						<img src=<%#AspxTools.SafeUrl(VirtualRoot + @"/LogoPL.aspx?id=" + DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId") + "&" + DateTime.Now.Minute + DateTime.Now.Second)%>  showMe="true" id="img<%#AspxTools.HtmlString(((Guid)DataBinder.Eval(Container.DataItem, "BrokerPmlSiteId")).ToString().Replace("-",string.Empty))%>" />
					</td>
				</tr>
			</AlternatingItemTemplate>
		</asp:Repeater>
	</table>
	<div id="imguploaded" style="z-index: 700; font-size: 14px; text-align:left; BORDER: black 3px solid; width:400px; PADDING: 5px;  DISPLAY: none; POSITION: absolute; HEIGHT:50px; background-color: whitesmoke; font-size: 12px">
		<b>Upload logo for <em><span id="m_brNm" > </span> </em></b><br/>
		<input type="file" style="width:390px" id="img" name="img" />
		<input type="hidden" id="pmlsiteid"  name="pmlsiteid" />
		<br/>
		<input type="button" onclick="Logo.Submit()" name="sub" value="Upload"/>
		<input type="button" onclick="Modal.Hide()" id="closeLink" name="close" value="Close"/>
	</div>
</form>

<!--[if lte IE 6.5]><iframe id="selectmask" style="display:none; position:absolute;top:0px;left:0px;filter:mask()" src="javascript:void(0)" ></iframe><![endif]-->
</body>
</html>
