﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomPmlFields.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.CustomPmlFields" %>
<%@ Import Namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Custom PML Field Configuration</title>
    <style type="text/css">
        .warning
        {
            color: red;
            margin: 5px;
            font-weight: bold;
        }
        .hidden { display: none; }
        textarea { width: 220px; }
        .center { text-align: center; }
    </style>
</head>
<body bgcolor="gainsboro" style="padding: 5px;">
    <form id="form1" runat="server">
    <div class="warning">
        ***Changes to a lender's custom PML fields should ideally occur outside of the lender's lock desk hours.***
        <br />
        ***When entering values for a dropdown use the following format [value]:[label] (ie 0:5 acres or less) and have each entry on a new line***
        <br />
        *** PLEASE CHECK WORKFLOW FOR RULES BASED ON THESE CUSTOM PML FIELDS BEFORE CHANGING THEM! ***
        <br />
        *** If you want to change a Custom PML Field and there are rules involving it, DON'T! ***
        <br />
        *** If you do need to make changes, please make a case for the SDEs to talk about a plan, most likely involving a migration. ***
    </div>
    <div>
        <div>
            <asp:Repeater runat="server" ID="customPmlFields" OnItemDataBound="CustomField_OnItemDataBound">
            <HeaderTemplate>
                <table id="customPmlFieldsTable" class="tablesorter customFieldsTable">
                    <thead>
                        <tr class="GridHeader">
                            <th id="reorder" style="width: 90px;">Re-Order</th>
                            <th style="width: 120px;">Visibility Type</th>
                            <th style="width: 350px;">Custom Field Description</th>
                            <th style="width: 130px;">Field Type</th>
                            <th style="width: 220px;">Dropdown Enumeration Map</th>
                            <th style="width: 90px;"></th>
                            <th style="width: 120px;">Keyword</th>
                            <th class="hidden">Rank</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="row" runat="server" id="row">
                    <td class="center">
                        <a href="#" id="moveUp" class="moveUp" runat="server" >
                            <img src="../../images/up-blue.gif" alt="" height="13" width="13" style="border: none;" />
                        </a>
                        <div style="height: 2px;"></div>
                        <a href="#" id="moveDown" class="moveDown" runat="server">
                            <img src="../../images/down-blue.gif" alt="" height="13" width="13" style="border: none;" />
                        </a>
                    </td>
                    <td>
                        <span id="visibilityTypeLabel" class="visibilityTypeLabel" runat="server"></span>
                        <asp:DropDownList runat="server" class="visibilityType" ID="visibilityType"></asp:DropDownList>
                    </td>
                    <td>
                        <span id="fieldDescriptionLabel" class="fieldDescriptionLabel" runat="server"></span>
                        <input type="text" id="fieldDescription" class="fieldDescription" runat="server" />
                    </td>
                    <td>
                        <span id="fieldTypeLabel" class="fieldTypeLabel" runat="server"></span>
                        <asp:DropDownList runat="server" class="fieldType" ID="fieldType"></asp:DropDownList>
                    </td>
                    <td>
                        <span id="enumMapLabel" class="enumMapLabel" runat="server">N/A</span>
                        <input type="hidden" id="enumMapBackup" class="enumMapBackup" runat="server" />
                        <textarea id="enumMap" runat="server" class="enumMap" cols="40" rows="3"></textarea>
                    </td>
                    <td class="center">
                        <a href="#" id="edit" class="edit" runat="server">edit</a>
                        <a href="#" id="clear" class="clear" runat="server">clear</a>
                        <a href="#" id="update" class="update" runat="server">update</a>
                        <a href="#" id="cancel" class="cancel" runat="server">cancel</a>
                    </td>
                    <td class="center">
                        <span id="keywordLabel" runat="server"></span>
                        <input type="hidden" id="keywordNum" runat="server"/>
                    </td>
                    <td class="hidden">
                        <input type="hidden" id="rank" class="rank" runat="server" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
        <div>
            <asp:Repeater runat="server" ID="customPricingPolicyFields" OnItemDataBound="CustomField_OnItemDataBound">
            <HeaderTemplate>
                <table id="customPricingPolicyFieldsTable" class="tablesorter customFieldsTable">
                    <thead>
                        <tr class="GridHeader">
                            <th id="reorder" style="width: 90px;">Re-Order</th>
                            <th style="width: 120px;"></th>
                            <th style="width: 350px;">Custom Field Description</th>
                            <th style="width: 130px;">Field Type</th>
                            <th style="width: 220px;">Dropdown Enumeration Map</th>
                            <th style="width: 90px;"></th>
                            <th style="width: 120px;">Keyword</th>
                            <th class="hidden">Rank</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="row" runat="server" id="row">
                    <td class="center">
                        <a href="#" id="moveUp" class="moveUp" runat="server" >
                            <img src="../../images/up-blue.gif" alt="" height="13" width="13" style="border: none;" />
                        </a>
                        <div style="height: 2px;"></div>
                        <a href="#" id="moveDown" class="moveDown" runat="server">
                            <img src="../../images/down-blue.gif" alt="" height="13" width="13" style="border: none;" />
                        </a>
                    </td>
                    <td>
                    </td>
                    <td>
                        <span id="fieldDescriptionLabel" class="fieldDescriptionLabel" runat="server"></span>
                        <input type="text" id="fieldDescription" class="fieldDescription" runat="server" />
                    </td>
                    <td>
                        <span id="fieldTypeLabel" class="fieldTypeLabel" runat="server"></span>
                        <asp:DropDownList runat="server" class="fieldType" ID="fieldType"></asp:DropDownList>
                    </td>
                    <td>
                        <span id="enumMapLabel" class="enumMapLabel" runat="server">N/A</span>
                        <input type="hidden" id="enumMapBackup" class="enumMapBackup" runat="server" />
                        <textarea id="enumMap" runat="server" class="enumMap" cols="40" rows="3"></textarea>
                    </td>
                    <td class="center">
                        <a href="#" id="edit" class="edit" runat="server">edit</a>
                        <a href="#" id="clear" class="clear" runat="server">clear</a>
                        <a href="#" id="update" class="update" runat="server">update</a>
                        <a href="#" id="cancel" class="cancel" runat="server">cancel</a>
                    </td>
                    <td class="center">
                        <span id="keywordLabel" runat="server"></span>
                        <input type="hidden" id="keywordNum" runat="server"/>
                    </td>
                    <td class="hidden">
                        <input type="hidden" id="rank" class="rank" runat="server" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>

        <asp:Button ID="ReleaseButton" runat="server" OnClick="Save" OnClientClick="return f_setReleasing();" Text="Release Custom Field Settings" />
        <input type="button" onclick="window.close();" value="Close"/>
        <input type="hidden" id="errorMsg" class="errorMsg" runat="server" />
    </div>
    </form>
    <script type="text/javascript">
        var bDirty = false;
        var bReleasing = false;
        var E_CustomPmlFieldType_Dropdown = <%= AspxTools.JsString(E_CustomPmlFieldType.Dropdown) %>;
        var E_CustomPmlFieldType_Blank = <%= AspxTools.JsString(E_CustomPmlFieldType.Blank) %>;
        
        /******************** Selector Helpers ********************/
        function f_getRankSelector(customFieldNum) {
            return '.rank.cf' + customFieldNum;
        }
        
        function f_getRowSelector(customFieldNum) {
            return '.row.cf' + customFieldNum;
        }
        
        function f_getFieldDescLabelSelector(customFieldNum) {
            return '.fieldDescriptionLabel.cf' + customFieldNum;
        }
        
        function f_getFieldDescSelector(customFieldNum) {
            return '.fieldDescription.cf'+customFieldNum;
        }
        
        function f_getFieldTypeLabelSelector(customFieldNum) {
            return '.fieldTypeLabel.cf' + customFieldNum;
        }
        
        function f_getFieldTypeSelector(customFieldNum) {
            return '.fieldType.cf' + customFieldNum;
        }
        
        function f_getEnumMapLabelSelector(customFieldNum) {
            return '.enumMapLabel.cf' + customFieldNum;
        }
        
        function f_getEnumMapBackupSelector(customFieldNum) {
            return '.enumMapBackup.cf' + customFieldNum;
        }
        
        function f_getEnumMapSelector(customFieldNum) {
            return '.enumMap.cf' + customFieldNum;
        }
        
        function f_getUpdateLinkSelector(customFieldNum) {
            return '.update.cf' + customFieldNum;
        }
        
        function f_getCancelLinkSelector(customFieldNum) {
            return '.cancel.cf' + customFieldNum;
        }
        
        function f_getEditLinkSelector(customFieldNum) {
            return '.edit.cf' + customFieldNum;
        }
        
        function f_getClearLinkSelector(customFieldNum) {
            return '.clear.cf' + customFieldNum;
        }
        
        function f_getMoveUpSelector(customFieldNum) {
            return '.moveUp.cf' + customFieldNum;
        }
        
        function f_getMoveDownSelector(customFieldNum) {
            return '.moveDown.cf' + customFieldNum;
        }
        
        function f_getVisibilityTypeLabelSelector(customFieldNum) {
            return '.visibilityTypeLabel.cf' + customFieldNum;
        }
        
        function f_getVisibilityTypeSelector(customFieldNum) {
            return '.visibilityType.cf' + customFieldNum;
        }
        
        function f_getTableSelector(customFieldNum) {
            return f_isCustomPMLField(customFieldNum) ? '#customPmlFieldsTable' : '#customPricingPolicyFieldsTable';
        }

        /******************** End Selector Helpers ********************/
        
        function f_isCustomPMLField(customFieldNum) {
            return customFieldNum.indexOf('CustomPMLField') !== -1;
        }
        
        function f_getMaxRank(customFieldNum) {
            return f_isCustomPMLField(customFieldNum) ?
                    <%= AspxTools.JsNumeric(customPmlFields.Items.Count) %> : 
                    <%= AspxTools.JsNumeric(customPricingPolicyFields.Items.Count) %>;
        }
        
        // When we are moving a row up, its rank will decrease by 1.
        // The rank of the element above it will increase by 1.
        function f_moveUp(customFieldNum, bBypassDisableCheck) {
            // If the up link is disabled, don't do anything.
            var upSelector = f_getMoveUpSelector(customFieldNum);
            if (!bBypassDisableCheck && $j(upSelector).attr('disabled')) {
                return;
            }

            // If the current row already has rank 1, don't do anything.
            var rankSelector = f_getRankSelector(customFieldNum);
            var callingRowRank = parseInt($j(rankSelector).val(), 10);
            if (callingRowRank == 1) return;
            
            bDirty = true;
            var rowSelector = f_getRowSelector(customFieldNum);
            
            // Increase the rank of the row above by 1.
            var customFieldNumAbove = $j(rowSelector).prev().data('cf');
            var rowAboveSelector = f_getRowSelector(customFieldNumAbove);
            var rankAboveSelector = f_getRankSelector(customFieldNumAbove);
            $j(rankAboveSelector).val(callingRowRank);
            
            // Decrease the rank of the row that is being moved up by 1.
            $j(rankSelector).val(callingRowRank - 1);
            
            // Update the td tags that contain the rank fields for the tablesorter
            // and trigger resort
            var currCell = $j(rankSelector).closest('td').get(0);
            var cellAbove = $j(rankAboveSelector).closest('td').get(0);
            var tableSelector = f_getTableSelector(customFieldNum)
            $j(tableSelector).trigger('updateCell',[currCell, false]);
            $j(tableSelector).trigger('updateCell',[cellAbove, false]);
            f_resortTable();
            
            f_toggleReleaseButton();
        }
        
        // If you try to move a row down and the row is empty,
        // do not allow it.
        function f_moveDown(customFieldNum, bBypassDisableCheck) {
            // If the down link is disabled, don't do anything.
            var downSelector = f_getMoveDownSelector(customFieldNum);
            if (!bBypassDisableCheck && $j(downSelector).attr('disabled')) {
                return;
            }
            
            // If the rank of the current row is already equal to the number of
            // items in the repeater, return.
            var rankSelector = f_getRankSelector(customFieldNum);
            var callingRowRank = parseInt($j(rankSelector).val(), 10);
            if (callingRowRank == f_getMaxRank(customFieldNum)) return;
            
            // If the row below the current row is not valid, return
            var rowSelector = f_getRowSelector(customFieldNum);
            var customFieldNumBelow = $j(rowSelector).next().data('cf');
            var bRowBelowInvalid = f_customFieldHasEmptyDescription(customFieldNumBelow);
            if (bRowBelowInvalid) return;

            // Set dirty bit for row reordering.
            bDirty = true;
            var rowBelowSelector = f_getRowSelector(customFieldNumBelow);
            
            // Decrease the rank of the row below by 1.
            var rankBelowSelector = f_getRankSelector(customFieldNumBelow);
            $j(rankBelowSelector).val(callingRowRank);

            // Increase the rank of the row that is being moved down by 1.
            $j(rankSelector).val(callingRowRank + 1);
            
            // Update the td tags that contain the rank fields for the tablesorter
            // and trigger resort
            var currCell = $j(rankSelector).closest('td').get(0);
            var cellBelow = $j(rankBelowSelector).closest('td').get(0);
            var tableSelector = f_getTableSelector(customFieldNum);
            $j(tableSelector).trigger('updateCell',[currCell, false]);
            $j(tableSelector).trigger('updateCell',[cellBelow, false]);
            f_resortTable();
            
            f_toggleReleaseButton();
        }
        
        // Will move a field's row up until there is no longer an invalid row above it.
        function f_moveAboveAllInvalidRows(customFieldNum) {
            // If this is the first row, then don't do anything.
            var currentRowInvalid = f_customFieldHasEmptyDescription(customFieldNum);
            var rankSelector = f_getRankSelector(customFieldNum);
            var currRank = parseInt($j(rankSelector).val(), 10);
            if (currentRowInvalid || (currRank == 1)) return;
            
            // While the row above this is invalid, move it up
            var rowSelector = f_getRowSelector(customFieldNum);
            var customFieldNumAbove = $j(rowSelector).prev().data('cf');
            var bRowAboveInvalid = f_customFieldHasEmptyDescription(customFieldNumAbove);

            while(bRowAboveInvalid) {
                f_moveUp(customFieldNum, true);
                
                var $rowAbove = $j(rowSelector).prev();
                customFieldNumAbove = $rowAbove.data('cf');
                bRowAboveInvalid = ($rowAbove.length != 0 && f_customFieldHasEmptyDescription(customFieldNumAbove));
            }
        }
        
        function f_moveBelowAllValidRows(customFieldNum) {
            // If this is already the last row, don't do anything.
            var rankSelector = f_getRankSelector(customFieldNum);
            var currRank = parseInt($j(rankSelector).val(), 10);
            if (currRank == f_getMaxRank(customFieldNum)) return;
            
            // While the row below this row is valid, move it down
            var rowSelector = f_getRowSelector(customFieldNum);
            var customFieldNumBelow = $j(rowSelector).next().data('cf');
            var bRowBelowValid = !f_customFieldHasEmptyDescription(customFieldNumBelow);
            
            while (bRowBelowValid) {
                f_moveDown(customFieldNum, true);
                
                var $rowBelow = $j(rowSelector).next();
                customFieldNumBelow = $rowBelow.data('cf');
                bRowBelowValid = ($rowBelow.length != 0 && !f_customFieldHasEmptyDescription(customFieldNumBelow));
            }
        }
        
        function f_resortTable() {
            var sorting = [[7,0]];
            $j('.customFieldsTable').trigger('sorton', [sorting]);
        }
        
        function f_customFieldHasEmptyDescription(customFieldNum) {
            var fieldDescSelector = f_getFieldDescSelector(customFieldNum);
            return $j.trim($j(fieldDescSelector).val()) == '';
        }
        
        // Want to hide the labels and show the editable fields/links.
        // Hide the span element for the row and display the edit version
        function f_editRow(customFieldNum) {
            // Show the editing fields
            var descSelector = f_getFieldDescSelector(customFieldNum);
            var typeSelector = f_getFieldTypeSelector(customFieldNum);
            var enumMapSelector = f_getEnumMapSelector(customFieldNum);
            var visibilityTypeSelector = f_getVisibilityTypeSelector(customFieldNum);
            $j(descSelector).show();
            $j(typeSelector).show();
            $j(visibilityTypeSelector).show();
            
            
            // Hide all of the label fields.
            var descLabelSelector = f_getFieldDescLabelSelector(customFieldNum);
            var typeLabelSelector = f_getFieldTypeLabelSelector(customFieldNum);
            var enumMapLabelSelector = f_getEnumMapLabelSelector(customFieldNum);
            var visibilityTypeLabelSelector = f_getVisibilityTypeLabelSelector(customFieldNum);
            $j(descLabelSelector).hide();
            $j(typeLabelSelector).hide();
            $j(visibilityTypeLabelSelector).hide();
            
            // If the type is Dropdown, then make the text area visible/not readonly
            // otherwise, just show the span
            // backup the value of the map in the hidden field.
            var enumMapBackupSelector = f_getEnumMapBackupSelector(customFieldNum);
            if ($j(typeSelector).val() == E_CustomPmlFieldType_Dropdown) {
                $j(enumMapSelector).show();
                $j(enumMapSelector).attr('readonly', false);
                $j(enumMapLabelSelector).hide();
            }
            else {
                $j(enumMapLabelSelector).show();
                $j(enumMapSelector).hide();
            }
            // backup the map value to the hidden field.
            $j(enumMapBackupSelector).val($j(enumMapSelector).val());
            
            
            // Show the update/cancel links, hide edit/clear links
            var updateSelector = f_getUpdateLinkSelector(customFieldNum);
            var cancelSelector = f_getCancelLinkSelector(customFieldNum);
            var editSelector = f_getEditLinkSelector(customFieldNum);
            var clearSelector = f_getClearLinkSelector(customFieldNum);
            $j(updateSelector).show();
            f_toggleUpdateLink(customFieldNum);
            $j(cancelSelector).show();
            $j(editSelector).hide();
            $j(clearSelector).hide();
            
            f_toggleReorderLinks(customFieldNum);
            f_toggleReleaseButton();
        }

        // Copy the value from the editable area to the span for the row
        // and hide the editable elements.
        function f_updateRow(customFieldNum, bIsInitLoad) {
            if (!bIsInitLoad) {
                bDirty = true;
            }

            var descSelector = f_getFieldDescSelector(customFieldNum);
            var typeSelector = f_getFieldTypeSelector(customFieldNum);
            var enumMapSelector = f_getEnumMapSelector(customFieldNum);
            var visibilityTypeSelector = f_getVisibilityTypeSelector(customFieldNum);
            // Hide the editables.
            $j(descSelector).hide();
            $j(typeSelector).hide();
            $j(visibilityTypeSelector).hide();
            
            var descLabelSelector = f_getFieldDescLabelSelector(customFieldNum);
            var typeLabelSelector = f_getFieldTypeLabelSelector(customFieldNum);
            var enumMapLabelSelector = f_getEnumMapLabelSelector(customFieldNum);
            var visibilityTypeLabelSelector = f_getVisibilityTypeLabelSelector(customFieldNum);
            // Update and show the labels.
            $j(descLabelSelector).text($j(descSelector).val());
            $j(descLabelSelector).show();
            // For the DDL, want the label text to be the selected option text
            $j(typeLabelSelector).text($j(typeSelector + ' option:selected').text());
            $j(typeLabelSelector).show();
            
            $j(visibilityTypeLabelSelector).text($j(visibilityTypeSelector + ' option:selected').text());
            $j(visibilityTypeLabelSelector).show();

            // Only want to hide this if the type is something other than DDL
            // If it is a DDL, then make this readonly
            // else, hide it and show the span
            if ($j(typeSelector).val() == E_CustomPmlFieldType_Dropdown) {
                $j(enumMapSelector).show();
                $j(enumMapSelector).attr('readonly', true);
                $j(enumMapLabelSelector).hide();
            }
            else {
                $j(enumMapSelector).hide();
                var bValidDesc = $j.trim($j(descSelector).val()) !== '';
                $j(enumMapLabelSelector).toggle(bValidDesc);
            }

            // Toggle links
            var updateSelector = f_getUpdateLinkSelector(customFieldNum);
            var cancelSelector = f_getCancelLinkSelector(customFieldNum);
            var editSelector = f_getEditLinkSelector(customFieldNum);
            var clearSelector = f_getClearLinkSelector(customFieldNum);
            $j(updateSelector).hide();
            $j(cancelSelector).hide();
            $j(editSelector).show();
            f_toggleClearLink(customFieldNum);
            
            f_toggleReorderLinks(customFieldNum);
            f_toggleReleaseButton();
            
            // Want to move this row to it's appropriate position in the list.
            if (!bIsInitLoad) {
                f_moveAboveAllInvalidRows(customFieldNum);
            }
        }
        
        // Hide the editable fields and restore their values.
        // Show all of the labels.
        function f_cancelRow(customFieldNum) {
            // Show labels.
            var descLabelSelector = f_getFieldDescLabelSelector(customFieldNum);
            var typeLabelSelector = f_getFieldTypeLabelSelector(customFieldNum);
            var enumMapLabelSelector = f_getEnumMapLabelSelector(customFieldNum);
            var visibilityTypeLabelSelector = f_getVisibilityTypeLabelSelector(customFieldNum);
            $j(descLabelSelector).show();
            $j(typeLabelSelector).show();
            $j(visibilityTypeLabelSelector).show();
            
            // Hide editables and restore values.
            var descSelector = f_getFieldDescSelector(customFieldNum);
            var typeSelector = f_getFieldTypeSelector(customFieldNum);
            var enumMapSelector = f_getEnumMapSelector(customFieldNum);
            var visibilityTypeSelector = f_getVisibilityTypeSelector(customFieldNum);
            $j(descSelector).hide();
            $j(descSelector).val($j(descLabelSelector).text());
            $j(typeSelector).hide();
            
            // We have stored the option text in the label, so we need 
            // cannot set DDL value with .val([new_val])
            $j(typeSelector + ' option').filter(function() {
                return this.text == $j(typeLabelSelector).text();
            }).attr('selected', true);

            $j(visibilityTypeSelector).hide();
            $j(visibilityTypeSelector + ' option').filter(function() {
                return this.text == $j(visibilityTypeLabelSelector).text();
            }).attr('selected', true);
 
            // If type is DDL, then show the text area as read-only
            // Otherwise show hide DDL, show label
            // Then restore the value from the backup.
            var enumMapBackupSelector = f_getEnumMapBackupSelector(customFieldNum);
            if ($j(typeSelector).val() == E_CustomPmlFieldType_Dropdown) {
                $j(enumMapSelector).show();
                $j(enumMapSelector).attr('readonly', true);
                $j(enumMapLabelSelector).hide();
            }
            else {
                // hide the dropdown and restor its value from the hidden field.
                // restore the value from the hidden field
                $j(enumMapSelector).hide();
                var bValidDesc = $j.trim($j(descSelector).val()) !== '';
                $j(enumMapLabelSelector).toggle(bValidDesc);
            }
            // Finally, restore the dropdown value.
            $j(enumMapSelector).val($j(enumMapBackupSelector).val());
        
            // Toggle links.
            var updateSelector = f_getUpdateLinkSelector(customFieldNum);
            var cancelSelector = f_getCancelLinkSelector(customFieldNum);
            var editSelector = f_getEditLinkSelector(customFieldNum);
            var clearSelector = f_getClearLinkSelector(customFieldNum);
            $j(updateSelector).hide();
            $j(cancelSelector).hide();
            $j(editSelector).show();
            f_toggleClearLink(customFieldNum);
            f_toggleReleaseButton();
            f_toggleReorderLinks(customFieldNum);
        }

        function f_confirmClear(customFieldNum) {
            var msg = 'Are you sure you want to clear the custom field setting? ' +
                      'Any rules remaining which depend on the associated keyword ' +
                      'will cause an evaluation error.'
            if (confirm(msg)) {
                f_clearRow(customFieldNum);
            }
        }

        // Clear out all of the values in a row
        // Reset the labels and editable fields
        function f_clearRow(customFieldNum) {
            bDirty = true;
            // Reset labels
            var descLabelSelector = f_getFieldDescLabelSelector(customFieldNum);
            var typeLabelSelector = f_getFieldTypeLabelSelector(customFieldNum);
            var enumMapLabelSelector = f_getEnumMapLabelSelector(customFieldNum);
            $j(descLabelSelector).text('');
            $j(typeLabelSelector).text('');
            $j(enumMapLabelSelector).hide(); // Hide N/A

            // Reset editable fields
            var descSelector = f_getFieldDescSelector(customFieldNum);
            var typeSelector = f_getFieldTypeSelector(customFieldNum);
            var enumMapSelector = f_getEnumMapSelector(customFieldNum);
            $j(descSelector).val('');
            $j(typeSelector).val(<%= AspxTools.JsString(E_CustomPmlFieldType.Blank) %>);
            $j(enumMapSelector).val('');
            // Also want to hide the enum map.
            $j(enumMapSelector).hide();
        
            // Toggle links
            var updateSelector = f_getUpdateLinkSelector(customFieldNum);
            var cancelSelector = f_getCancelLinkSelector(customFieldNum);
            var editSelector = f_getEditLinkSelector(customFieldNum);
            var clearSelector = f_getClearLinkSelector(customFieldNum);
            $j(updateSelector).hide();
            $j(cancelSelector).hide();
            $j(editSelector).show();
            f_toggleClearLink(customFieldNum);
            
            f_toggleReorderLinks(customFieldNum);
            f_toggleReleaseButton();
            
            f_moveBelowAllValidRows(customFieldNum);
        }

        function f_setReleasing() {
            var msg = 'You have made changes that may require a migration ' +
                'at the Employee and Originating Company Level. Are you sure you ' +
                'want to continue? PLEASE BE CAREFUL.';
            if (!f_detectPotentiallyBreakingFieldChanges() || confirm(msg)) {
                bReleasing = true;
                return true;
            }
            return false;
        }
        
        function f_getCurrentFieldInfo(keyword) {
            return { 
                Keyword: keyword,
                FieldType: $j(f_getFieldTypeSelector(keyword)).val(),
                DropdownEnumerationValue: $j(f_getEnumMapSelector(keyword)).val() 
            };
        }
        
        function f_detectPotentiallyBreakingFieldChanges() {
            for (var i = 0; i < originalFieldSettings.length; i++) {
                var previousFieldSetting = originalFieldSettings[i];
                var currentFieldSetting = f_getCurrentFieldInfo(previousFieldSetting.Keyword);
                // If the field types are different, or the previous enumeration map is not 
                // a substring of the current enumeration map then this may be breaking.
                if (previousFieldSetting.FieldType !== currentFieldSetting.FieldType ||
                        currentFieldSetting.DropdownEnumerationValue.indexOf(previousFieldSetting.DropdownEnumerationValue) === -1) {
                    return true;    
                }
            }
            return false;
        }

        function f_close() {
            if (!bDirty || bReleasing) {
                return;
            }
            
            var msg = 'Exit "Custom PML Field Configuration" without Releasing changes? ' +
                      'Note: Any changes made will not be saved.';
            return msg;
        }

        function f_onchange_fieldDescription() {
            var $this = $j(this);
            var customFieldNum = $this.data('cf');
            f_toggleUpdateLink(customFieldNum);
        }
        
        function f_onchange_fieldType() {
            var $this = $j(this);
            var customFieldNum = $this.data('cf');
            
            // If the selected value is Dropdown, want to show the enum map
            // and make it editable
            // otherwise, we just want to hide it
            var enumMapSelector = f_getEnumMapSelector(customFieldNum);
            var enumMapLabelSelector = f_getEnumMapLabelSelector(customFieldNum);
            if ($this.val() == E_CustomPmlFieldType_Dropdown) {
                $j(enumMapSelector).show();
                $j(enumMapSelector).attr('readonly', false);
                $j(enumMapLabelSelector).hide();
            }
            else {
                $j(enumMapSelector).hide();
                $j(enumMapLabelSelector).show();
            }
            
            f_toggleUpdateLink(customFieldNum);
        }
        
        function f_onchange_enumMap() {
            var $this = $j(this);
            var customFieldNum = $this.data('cf');
            f_toggleUpdateLink(customFieldNum);
        }
        
        function f_toggleUpdateLink(customFieldNum) {
            var updateSelector = f_getUpdateLinkSelector(customFieldNum);
            var fieldDescSelector = f_getFieldDescSelector(customFieldNum);
            var fieldTypeSelector = f_getFieldTypeSelector(customFieldNum);
            var enumMapSelector = f_getEnumMapSelector(customFieldNum);
            
            // Disable the update link as long as the required information has not been filled out.
            var bFieldDescValid = $j.trim($j(fieldDescSelector).val()) != '';
            var bFieldTypeValid = $j(fieldTypeSelector).val() != E_CustomPmlFieldType_Blank;
            
            var bDropdown = ($j(fieldTypeSelector).val() == E_CustomPmlFieldType_Dropdown);
            var bEnumMapValid = false;
            if (bDropdown) {
                bEnumMapValid = ($j.trim($j(enumMapSelector).val()) != '');
            }
            else {
                bEnumMapValid = true;
            }
            
            var bEnableUpdateLink = (bFieldDescValid && bFieldTypeValid && bEnumMapValid);
            $j(updateSelector).attr('disabled', !bEnableUpdateLink);
        }

        function f_initializeRows() {
            for (var i = 0; i < customFieldSets.length; i++) {
                var customFieldSet = customFieldSets[i];
                for (var j = 1; j <= customFieldSet.NumFields; j++) {
                    keyword = customFieldSet.CustomFieldPrefix + j;
                    f_updateRow(keyword, true);
                    f_toggleReorderLinks(keyword);
                    f_toggleClearLink(keyword);
                }
            }
        }

        function f_toggleClearLink(customFieldNum) {
            var labelVal = "";
            // check the field description to see if it is blank.
            var fieldDescSelector = f_getFieldDescSelector(customFieldNum);
            var hideClearLink = $j.trim($j(fieldDescSelector).val()) == '';
            
            var clearLinkSelector = f_getClearLinkSelector(customFieldNum);
            $j(clearLinkSelector).toggle(hideClearLink == false);
        }
        
        // For now, just disable the up/down links based on whether
        // the field is valid or in edit mode.
        function f_toggleReorderLinks(customFieldNum) {
            var upSelector = f_getMoveUpSelector(customFieldNum);
            var $upLink = $j(upSelector);
            var downSelector = f_getMoveDownSelector(customFieldNum);
            var $downLink = $j(downSelector);
            var updateSelector = f_getUpdateLinkSelector(customFieldNum);
            
            var bRowInEditMode = $j(updateSelector).is(':visible');
            
            if (bRowInEditMode) {
                $upLink.attr('disabled', true);
                $downLink.attr('disabled', true);
                return;
            }
            
            // If the link has an empty description, disable both
            var fieldDescSelector = f_getFieldDescSelector(customFieldNum);
            var bInvalidDesc = $j.trim($j(fieldDescSelector).val()) == '';
            $upLink.attr('disabled', bInvalidDesc);
            $downLink.attr('disabled', bInvalidDesc);
        }
        
        // If it is dirty and no row is currently in edit mode, then 
        // enable the button.
        function f_toggleReleaseButton() {
            var $releaseButton = $j('#' + <%= AspxTools.JsGetClientIdString(ReleaseButton)%>);
            if (!bDirty) {
                $releaseButton.attr('disabled', true);
                return;
            }
            
            var bRowInEditMode = false;
            $j('tr').each(function(index, element) {
                // We will say that a row is in edit mode if the update link is visible.
                var currentFieldNum = $j(this).data('cf');
                var updateSelector = f_getUpdateLinkSelector(currentFieldNum);
                
                if ($j(updateSelector).is(':visible')) {
                    bRowInEditMode = true;
                    return false;
                }
            });
            
            var bDisabled = bRowInEditMode ? true : false;
            $releaseButton.attr('disabled', bDisabled);
        }

        function f_attachEvents() {
            // links
            $j('a.moveUp').click(function() {
                f_moveUp($j(this).data('cf'));
            });
            $j('a.moveDown').click(function() {
                f_moveDown($j(this).data('cf'));
            });
            $j('.edit').click(function() {
                f_editRow($j(this).data('cf'));
            });
            $j('.clear').click(function() {
                f_confirmClear($j(this).data('cf'));
            });
            $j('.cancel').click(function() {
                f_cancelRow($j(this).data('cf'));
            });
            $j('.update').click(function() {
                f_updateRow($j(this).data('cf'));
            });
            
            // field event handlers
            $j('.fieldType').change(f_onchange_fieldType);
            $j('.fieldDescription').keyup(f_onchange_fieldDescription);
            $j('.enumMap').keyup(f_onchange_enumMap);
            
            // table stuff
            $j('.customFieldsTable').bind('sortEnd', function() {
//                alert('sort done');
                var table = this;
                var rows = table.tBodies[0].rows;
                var isAlt = false;
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (isAlt) {
                        $j(row).removeClass('GridItem').addClass('GridAlternatingItem');
                    }
                    else {
                        $j(row).removeClass('GridAlternatingItem').addClass('GridItem');
                    }
                    isAlt = !isAlt;
                }
            });
        }
        
        var customFieldSets = [
            { CustomFieldPrefix: 'CustomPMLField_', NumFields: 20 },
            { CustomFieldPrefix: 'CustomLOPPField_', NumFields: 5 }
        ];
        
        var originalFieldSettings = [];
        
        $j(document).ready(function() {
            var i = 0,
                j = 1,
                keyword = '';
        
            // lazy way of making all of the inputs bigger
            $j('input[type="text"]').css('width', '350px');

            // For each custom field, attach its number as data to make 
            // getting selectors easier.
            for (var i = 0; i < customFieldSets.length; i++) {
                var customFieldSet = customFieldSets[i];
                for (var j = 1; j <= customFieldSet.NumFields; j++) {
                    keyword = customFieldSet.CustomFieldPrefix + j;
                    $j('.cf' + keyword).data('cf', keyword);
                    
                    if (customFieldSet.CustomFieldPrefix === 'CustomLOPPField_') {
                        if (!f_customFieldHasEmptyDescription(keyword)) {
                            originalFieldSettings.push(f_getCurrentFieldInfo(keyword));
                        }
                    }
                }
            }
            
            f_attachEvents();
            f_initializeRows();
            f_toggleReleaseButton();
            
            // set up the table sorter, want it to sort based on the "rank" hidden field.
            $j('.customFieldsTable').tablesorter({
                headers: {
                    0: { sorter: false},
                    1: { sorter: false},
                    2: { sorter: false},
                    3: { sorter: false},
                    4: { sorter: false},
                    5: { sorter: false},
                    6: { sorter: false}
                },
                sortList: [[7,0]],
                textExtraction: function(node) {
                    var $rankField = $j(node).find('.rank');
                    if ($rankField.length == 0) return '';
                    return parseInt($rankField.val());
                }
            });
            
            var errorMsg = $j.trim($j('.errorMsg').val());
            if (errorMsg != '') {
                alert(errorMsg);
            }
            $j('.errorMsg').val('');
            
            window.onbeforeunload = f_close;
        });
    </script>
</body>
</html>
