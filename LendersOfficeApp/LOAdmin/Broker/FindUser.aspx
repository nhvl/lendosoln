<%@ Page language="C#" Codebehind="FindUser.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.FindUser"
         MasterPageFile="~/LOAdmin/loadmin.Master" Title="Find User - LendingQB" %>

<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <div id="container">
        <div class="container"><div class="alert alert-info" role="alert">Loading&hellip;</div></div>
    </div>

    <form method="post" runat="server">
        <input id="m_EventToProcess" type="hidden" name="m_EventToProcess" runat="server">
    </form>
</asp:Content>
