using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using LendersOffice.Security;
using LendersOffice.Events;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOfficeApp.los.admin;

namespace LendersOfficeApp.LOAdmin.Broker
{
	/// <summary>
	/// 
	/// </summary>
	public partial class DefaultRolePermissionsService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
		private Guid m_brokerId;
		private Guid BrokerID 
		{
			set { m_brokerId = value; }
			get { return m_brokerId; }
		}
		
		protected override void Process(string methodName) 
		{
			BrokerID = GetGuid("brokerId");
			switch (methodName) 
			{
				case "PopulateDefaultRolePermissions":
					PopulateDefaultRolePermissions();
					break;
                case "PopulateRSENonBoolPermission":
                    PopulateRSENonBoolPermission();
                    break;
                default:
                    Tools.LogBug("Unhandled method call in DefaultRolePermissionsService: " + methodName + " does not exist.");
                    break;
			}
		}

        public void PopulateRSENonBoolPermission()
        {
            try
            {
                ArrayList rolePermissions = new ArrayList();

                foreach (var role in Role.LendingQBRoles)
                {
                    if (HasValue(role.Id.ToString()))
                    {
                        BrokerUserPermissionsNonBool bUp = new BrokerUserPermissionsNonBool();
                        bUp.RetrieveRolePermissions(BrokerID, role.Id);
                        rolePermissions.Add(bUp);
                    }
                }

                char rsePerm = GetRSEPermissionFromAllRoles(rolePermissions);

                SetResult("DefaultRSENonBoolPermission", rsePerm.ToString());
            }
            catch (CBaseException c)
            {
                Tools.LogError("Unable to populate default role permissions" + c);
                throw c;
            }
        }

        private char GetRSEPermissionFromAllRoles(ICollection rolePermissions)
        {
            BrokerUserPermissionsNonBool bUp = new BrokerUserPermissionsNonBool();
            return bUp.GetPermission(NonBoolPermission.RateLockRequestForExpiredRate, rolePermissions);
        }

		public void PopulateDefaultRolePermissions()
		{
            BrokerDB db = BrokerDB.RetrieveById(BrokerID);
            BrokerFeatures m_bFs = new BrokerFeatures(BrokerID);
            ArrayList rolePermissions = new ArrayList();

            foreach (Role role in Role.LendingQBRoles)
            {
                if (HasValue(role.Id.ToString()))
                {
                    BrokerUserPermissions bUp = new BrokerUserPermissions();
                    bUp.RetrieveRolePermissions(BrokerID, role.Id);
                    rolePermissions.Add(bUp);
                }
            }

            bool pmlEnabled = m_bFs.HasFeature(E_BrokerFeatureT.PricingEngine);
            bool marketingToolsEnabled = m_bFs.HasFeature(E_BrokerFeatureT.MarketingTools);
            BrokerUserPermissions bUpRoles = new BrokerUserPermissions();
            System.Text.StringBuilder permissions = new System.Text.StringBuilder();

            int numElementsInEnum = Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription)).Length;
            BrokerUserPermissionTable.ControlDescription cd;
            for (int i = 0; i < numElementsInEnum; ++i)
            {
                cd = (BrokerUserPermissionTable.ControlDescription)Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription)).GetValue(i);
                permissions.Append(BuildPermissionsString(BrokerUserPermissionTable.GetPermissionTable(cd, pmlEnabled, db.IsDataTracIntegrationEnabled, db.IsEDocsEnabled, db.IsTotalScorecardEnabled, db.IsOCREnabled, marketingToolsEnabled), bUpRoles, rolePermissions));
            }

            SetResult("DefaultPermissions", permissions.ToString().TrimStart(','));

		}

		private System.Text.StringBuilder BuildPermissionsString(ICollection rolePermissions, BrokerUserPermissions bUp, ICollection rolebUps )
		{
			System.Text.StringBuilder permissions = new System.Text.StringBuilder();
			IEnumerator traverser = rolePermissions.GetEnumerator();
			while(traverser.MoveNext())
			{
				permissions.Append( "," + bUp.HasPermission( int.Parse( traverser.Current.ToString() ), rolebUps));
			}
			return permissions;
		}
	}
}
