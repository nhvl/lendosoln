﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Pdf;
using LendersOffice.Admin;
using LendersOfficeApp.los.admin;


namespace LendersOfficeApp.LoAdmin.PrintGroupPermissions
{
    public partial class LoAdminPrintGroupPermAssignment : SecuredAdminPage
    {
        protected Guid UserId
        {
            get
            {
                return RequestHelper.GetGuid("userid", default(Guid));
            }
        }

        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("brokerid");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var brokerGroups = PrintListGroup.ListAllGroups(BrokerId);

            IEnumerable<PrintListGroup> userGroups = new List<PrintListGroup>();
            if (UserId != default(Guid))
            {
                var user = EmployeeDB.RetrieveByUserId(BrokerId, UserId);

                //OPM 111008: if the user currently does not have any restrictions, start out with no print groups assigned.
                if (user.RestrictPrintGroups)
                {
                    userGroups = user.AvailablePrintGroups;
                }
            }

            var brokerGroupsMinusUserGroups = from g in brokerGroups
                                              where !userGroups.Any(a => a.Id == g.Id)
                                              select g;



            Picker.AvailableSrc = brokerGroupsMinusUserGroups.OrderBy(a => a.Name);
            Picker.AssignedSrc = userGroups.OrderBy(a => a.Name);

            Picker.TextField = "Name";
            Picker.ValueField = "Id";

            Picker.DataBind();
        }
    }
}
