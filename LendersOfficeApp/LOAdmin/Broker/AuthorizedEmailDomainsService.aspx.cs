﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Email;
    using LendersOffice.Security;

    /// <summary>
    /// Authorized Email Domains Service Page.
    /// </summary>
    public partial class AuthorizedEmailDomainsService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Processes a serive call by method name.
        /// </summary>
        /// <param name="methodName">The name of the method to invoke.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadDomains":
                    this.LoadDomains();
                    break;
                case "SaveDomain":
                    this.SaveDomain();
                    break;
                case "DeleteDomain":
                    this.DeleteDomain();
                    break;
                default:
                    throw new UnhandledCaseException(methodName);
            }
        }

        /// <summary>
        /// Load all authorized email domains for a broker.
        /// </summary>
        private void LoadDomains()
        {
            Guid brokerId = GetGuid("BrokerId");

            IEnumerable<string> domains = AuthorizedEmailDomainUtilities.GetAuthorizedEmailDomains(brokerId);
            IEnumerable view = domains.OrderBy(s => s).Select(s => new { Name = s });

            this.SetResult("Domains", SerializationHelper.JsonNetAnonymousSerialize(view));
        }

        /// <summary>
        /// Save an authorized email domain.
        /// </summary>
        private void SaveDomain()
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            Guid brokerId = GetGuid("BrokerId");
            string domainName = GetString("DomainName");

            AuthorizedEmailDomainUtilities.SaveAuthorizedEmailDomain(brokerId, domainName);

            // Return refreshed view.
            this.LoadDomains();
        }

        /// <summary>
        /// Delete an authorized email domain.
        /// </summary>
        private void DeleteDomain()
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            Guid brokerId = GetGuid("BrokerId");
            string domainName = GetString("DomainName");

            AuthorizedEmailDomainUtilities.DeleteAuthorizedEmailDomain(brokerId, domainName);

            // Return refreshed view.
            this.LoadDomains();
        }
    }
}