﻿<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditHistoryList.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.AuditHistoryList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<HEAD>
	<TITLE>Audit History</TITLE>
<META content="Microsoft Visual Studio 7.0" name=GENERATOR>
<META content=C# name=CODE_LANGUAGE>
<META content=JavaScript name=vs_defaultClientScript>
<META content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><LINK href="../../css/stylesheet.css" type=text/css rel=stylesheet >
  </HEAD>
<body>

<script language=javascript>
<!--
    function f_viewAuditDetail(id) {
        var loanId = <%= AspxTools.JsString(LoanId) %>;
        window.open("AuditHistoryDetail.aspx?loanid=" + loanId + "&auditid=" + id, "AuditDetail", "toolbar=no,menubar=no,location=no,status=no,resizable=yes");
    }

    function f_viewAuditDetailVer2(key) {
        window.open("AuditHistoryDetail.aspx?key=" + key, "AuditDetail", "toolbar=no,menubar=no,location=no,status=no,resizable=yes");

        return false;
    }
//-->
</script>
    <form id="form1" runat="server">
    <div>
    <table width="100%" border=0 cellpadding=0 cellspacing=0>
  <tr><td class="Header" nowrap><b>Audit History</b></td></tr>
  <tr><td style="padding-left:5px;padding-top:10px">
<ml:CommonDataGrid id=m_dg runat="server">
<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>

<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>

<HeaderStyle CssClass="GridHeader"></HeaderStyle>
<columns>
<asp:BoundColumn DataField="TimestampDescription" HeaderText="Timestamp"/>
<asp:BoundColumn DataField="UserName" HeaderText = "User Name" />
<asp:BoundColumn DataField="Description" HeaderText= "Event"/>
<asp:TemplateColumn>
<iTemTemplate>
<%# AspxTools.HtmlControl(RenderDetailsLink((LendersOffice.Audit.LightWeightAuditItem)Container.DataItem)) %>
</iTemTemplate>
</asp:TemplateColumn>
</columns>
</ml:CommonDataGrid>
</td></tr>
</table>
    </div>
    </form>
</body>
</html>
