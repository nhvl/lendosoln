using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.LOAdmin.Broker
{
	public partial class ShowLoansAssociatedWithBranch : LendersOffice.Admin.SecuredAdminPage
	{
		protected System.Web.UI.WebControls.Label ForMoreInfo;
		private Hashtable											m_BranchHash = new Hashtable();
		private ArrayList											m_Branches = new ArrayList();
		private ArrayList											m_Selected = new ArrayList();


		protected Guid m_BrokerId
		{
			get {  return new Guid( Request[ "brokerId" ] ); }
		}

		private Guid m_BranchId 
		{
			get { return new Guid( Request[ "branchId" ] ); }
		}

		private string m_BranchName
		{
			get { return Request[ "branchName" ]; }
		}

        private int selectedTabIndex = 0;

		protected void PageLoad(object sender, System.EventArgs e)
		{
            

            if (Page.Request.Params["__EVENTTARGET"] == "changeTab")
                selectedTabIndex = Int32.Parse(Page.Request.Params["__EVENTARGUMENT"]);
            else if (ViewState["selectedTabIndex"] != null)
                selectedTabIndex = (Int32)ViewState["selectedTabIndex"];

            ViewState["selectedTabIndex"] = selectedTabIndex;

			if( selectedTabIndex == 0 )
			{
                tab0.Attributes.Add("class", "selected");
                tab1.Attributes.Remove("class");
				BindDataGrid(1);
				m_invalidLoansLabel.Visible = false;
			}
			else
			{
                tab0.Attributes.Remove("class");
                tab1.Attributes.Add("class", "selected");
				BindDataGrid(0);
				m_invalidLoansLabel.Visible = true;
			}

			m_branchNameLabel.Text = "Loans associated with branch:  " + m_BranchName;
			m_tooManyLoans.Text = "- Too many loans to display - displaying the first " + ConstAppDavid.MaxViewableRecordsInPipeline;
			m_noneToShowLabel.Visible = (m_dg.Items.Count == 0)?true:false;
			m_tooManyLoans.Visible = (m_dg.Items.Count >= ConstAppDavid.MaxViewableRecordsInPipeline)?true:false;
            IncludeStyleSheet("~/css/Tabs.css");
		}

		private void BindDataGrid(int validity)
		{
			DataSet                 dS = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_BrokerId),
                                            new SqlParameter("@BranchId", m_BranchId),
                                            new SqlParameter("@Validity", validity)
                                        };
			DataSetHelper.Fill( dS , m_BrokerId, "ListLoansAssociatedWithBranch", parameters);

			m_dg.DefaultSortExpression = "sLNm ASC";

			m_dg.DataSource = dS.Tables[0].DefaultView;
			m_dg.DataBind();

            SqlParameter[] brokerIdParameter = {
                                                   new SqlParameter("@BrokerId", m_BrokerId)
                                               };
            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(m_BrokerId, "ListBranchByBrokerId", brokerIdParameter))
            {
                string b_Name;
                string b_Id;
                m_BranchHash.Clear();
                m_Branches.Clear();
                while (sR.Read() == true)
                {
                    b_Name = sR["Name"].ToString();
                    b_Id = sR["BranchID"].ToString();
                    m_BranchHash.Add(b_Name, b_Id);
                    m_Branches.Add(b_Name);
                }
            }
            if (!Page.IsPostBack)
            {
                InitBranchDropdownList();
            }
		}

		//OPM 18349 -jMorse
		//Initializes List of branches
		// 08/26/08 ck - OPM 23916. Remove current branch from drop down list
		protected void InitBranchDropdownList()
		{
			m_BranchDropdownList.Items.Clear();
			foreach( String s in m_Branches )
			{
				if(!s.Equals(m_BranchName))
				{
					m_BranchDropdownList.Items.Add( s );	
				}
			}		
		}

		// 08/26/08 ck - OPM 23916. Instead of using the SelectedIndex of the 
		// drop down list, used the SelectedItem
		protected void MoveLoansClick( object sender, System.EventArgs a )
		{
			string guid = (string)m_BranchHash[m_BranchDropdownList.SelectedItem.ToString()];

			foreach( System.Web.UI.WebControls.DataGridItem item in m_dg.Items )
			{				
				CheckBox cbox = item.FindControl( "m_CheckBox" ) as CheckBox;
				
				if( cbox.Checked )
				{				
					Label lid = item.FindControl( "m_LID" ) as Label;					
					m_Selected.Add( lid.Text );				
				}				
			}	

			Guid BranchMovingFrom = new Guid( m_BranchId.ToString() );
			Guid BranchMovingTo = new Guid( guid );			
			
			MoveLoansFromBranch( BranchMovingFrom, BranchMovingTo );

			PageLoad(sender,a); //Refresh page.			
		}
	
		protected void MoveLoansFromBranch( Guid BranchFromId, Guid BranchToId )
		{
			Guid loanId;

			foreach( string lid in m_Selected )
			{
				loanId = new Guid( lid );
				AssignBranchToLoans( BranchToId, loanId );
				
			}
		}

		protected void AssignBranchToLoans( Guid BranchToId, Guid LoanId )
		{
        	
			CPageData pg = GetPageWithBranchId( LoanId );
            pg.AllowSaveWhileQP2Sandboxed = true;
			pg.InitSave(ConstAppDavid.SkipVersionCheck);
            pg.AssignBranch(BranchToId);
			pg.Save();			
		}

		protected CPageData GetPageWithBranchId( Guid loanId )
		{
            CPageData pgData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(ShowLoansAssociatedWithBranch));
            pgData.AllowLoadWhileQP2Sandboxed = true;
			pgData.InitLoad();
			return pgData;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void m_dg_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
    }
}
