using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;


namespace LendersOfficeApp.LOAdmin.Broker
{
	/// <summary>
	/// Summary description for PasswordValidationService.
	/// </summary>
	public partial class PasswordValidationService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
		protected override void Process(string methodName) 
		{
			switch (methodName) 
			{
				case "ValidatePassword":
					ValidatePassword();
					break;
				case "ValidateFields":
					ValidateFields();
					break;
			}
		}

		private void ValidateFields()
		{
			Guid userId, brokerId;
			string hasLogin = "";
			string pw = "";
			string firstName = "";
			string lastName = "";
			string login = "";
            string passwordProblem = "";
            string originatorCompensationMinAmount = "";
            string originatorCompensationMaxAmount = "";
            bool originatorCompensationMinAmountEnabled = false;
            bool originatorCompensationMaxAmountEnabled = false;
			brokerId = GetGuid("BrokerId");
			string expirationDate = "";
			bool expiresOnChecked = false;
			bool changeLoginChecked = false;
            bool isPml = false;
            bool bIsActiveDirectoryUser = false;

			try 
			{
				userId = GetGuid("id");
				hasLogin = GetString("hasLogin");
				firstName = GetString("firstName");
				lastName = GetString("lastName");
                login = GetString("login");
                originatorCompensationMinAmount = GetString("originatorCompensationMinAmount");
                originatorCompensationMaxAmount = GetString("originatorCompensationMaxAmount");
                originatorCompensationMinAmountEnabled = GetBool("originatorCompensationMinAmountEnabled");
                originatorCompensationMaxAmountEnabled = GetBool("originatorCompensationMaxAmountEnabled");
				changeLoginChecked = GetBool("changeLoginChecked");
                isPml = GetBool("isPml");

                if (IsActiveDirectoryAuthEnabledForLender(brokerId))
                {
                    bIsActiveDirectoryUser = (isPml) ? false : GetBool("isADUser");
                }

                if (!bIsActiveDirectoryUser)
                {
                    pw = GetString("pw");
                    expirationDate = GetString("expirationDate");
                    expiresOnChecked = GetBool("passwordExpiresOnChecked");
                }

				if(changeLoginChecked)
				{
					EmployeeDB testDb = new EmployeeDB( userId , brokerId );

					if( userId == Guid.Empty )
						testDb = new EmployeeDB();
					else
						testDb.Retrieve();

					testDb.FirstName = firstName;
					testDb.LastName  = lastName;
					testDb.LoginName = login;

                    if (isPml)
                    {
                        BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
                        if (Tools.IsPmlLoginUnique(brokerDB, login.TrimWhitespaceAndBOM(), testDb.UserID) == false)
                            passwordProblem = ErrorMessages.LoginNameAlreadyInUseFunction(login.TrimWhitespaceAndBOM()) + Environment.NewLine;
                    }
                    else
                    {
                        if (Tools.IsLoginUnique(login.TrimWhitespaceAndBOM(), testDb.UserID) == false)
                            passwordProblem = ErrorMessages.LoginNameAlreadyInUseFunction(login.TrimWhitespaceAndBOM()) + Environment.NewLine;
                    }

                    if (!bIsActiveDirectoryUser)
                    {
                        if (pw.TrimWhitespaceAndBOM() == "" && (testDb.IsNew || hasLogin == "false" || testDb.IsActiveDirectoryUser))
                            passwordProblem = ErrorMessages.SpecifyPassword + Environment.NewLine;

                        // if this is a new user, check to make sure the password does not violate
                        // the PasswordContainsLogin condition for the login name that is currently typed in
                        // but has not yet been given to the user officially
                        if (testDb.IsStrongPassword(pw) == StrongPasswordStatus.PasswordContainsLogin)
                            passwordProblem = ErrorMessages.PwCannotContainLoginOrName + Environment.NewLine;
                    }
				}

				if( expiresOnChecked == true )
				{
					try
					{
						DateTime.Parse( expirationDate );
					}
					catch
					{
                        passwordProblem = ErrorMessages.SpecifyValidPasswordExpirationDate;
					}
                }

                LosConvert m_losConvert = new LosConvert();
                if (originatorCompensationMinAmountEnabled && (string.IsNullOrEmpty(originatorCompensationMinAmount) || m_losConvert.ToMoney(originatorCompensationMinAmount) <= 0.0M))
                {
                    passwordProblem += ErrorMessages.OriginatorCompensationMinAmountRequired + Environment.NewLine;
                }

                if (originatorCompensationMaxAmountEnabled && (string.IsNullOrEmpty(originatorCompensationMaxAmount) || m_losConvert.ToMoney(originatorCompensationMaxAmount) <= 0.0M))
                {
                    passwordProblem += ErrorMessages.OriginatorCompensationMaxAmountRequired + Environment.NewLine;
                }

                if (originatorCompensationMaxAmountEnabled && originatorCompensationMinAmountEnabled &&
                    !string.IsNullOrEmpty(originatorCompensationMinAmount) && !string.IsNullOrEmpty(originatorCompensationMaxAmount) &&
                    (m_losConvert.ToMoney(originatorCompensationMinAmount) > m_losConvert.ToMoney(originatorCompensationMaxAmount)))
                {
                    passwordProblem += ErrorMessages.OriginatorCompensationMaxAmountLessThanMinAmount + Environment.NewLine;
                }
			}
			catch( Exception e )
			{
				SetResult("ErrorMessage", e.Message);
				return;
            }

            if (!String.IsNullOrEmpty(passwordProblem))
            {
                SetResult("ErrorMessage", passwordProblem);
                return;
            }
		}

		private void ValidatePassword() 
		{
			string pw = "";
			string retype = "";
            string passwordProblem = "";
			Guid userId, brokerId;

			try 
			{
				pw = GetString("pw");
				retype = GetString("retype");
				userId = GetGuid("id");
				brokerId = GetGuid("BrokerId");

                if (IsActiveDirectoryAuthEnabledForLender(brokerId))
                {
                    bool bIsPMLUser = GetBool("isPml");
                    bool bIsActiveDirectoryUser = (bIsPMLUser) ? false : GetBool("isADUser");

                    if (bIsActiveDirectoryUser)
                    {
                        return;
                    }
                }
                
                EmployeeDB empDB = new EmployeeDB(userId, brokerId);
				empDB.Retrieve();

				// 3/20/2006 mf - OPM 4226 - We do not change existing password if fields are blank
                if (empDB.IsNew || pw != "" || retype != "" || empDB.IsActiveDirectoryUser) 
				{
					if( pw == "" )
                        passwordProblem = ErrorMessages.SpecifyPassword;

					if( pw != retype )
                        passwordProblem = ErrorMessages.PasswordsDontMatch;

					switch( empDB.IsStrongPassword( pw ) )
					{
						case StrongPasswordStatus.PasswordContainsLogin:
                            passwordProblem = ErrorMessages.PwCannotContainLoginOrName;
                            break;

						case StrongPasswordStatus.PasswordMustBeAlphaNumeric:
                            passwordProblem = ErrorMessages.PasswordMustBeAlphaNumeric;
                            break;

						case StrongPasswordStatus.PasswordTooShort:
                            passwordProblem = ErrorMessages.PasswordTooShort;
                            break;

						case StrongPasswordStatus.PasswordRecycle:
                            passwordProblem = ErrorMessages.PasswordsShouldNotBeRecycled;
                            break;

						// OPM 24614
						case StrongPasswordStatus.PasswordContainsConsecutiveIdenticalChars:
                            passwordProblem = ErrorMessages.PasswordContainsConsecIdenticalChars;
                            break;

						// OPM 24614
						case StrongPasswordStatus.PasswordContainsConsecutiveAlphanumericChars:
                            passwordProblem = ErrorMessages.PasswordContainsConsecCharsInAlphaOrNumericOrder;
                            break;

                        case StrongPasswordStatus.PasswordHasUnverifiedCharacters:
                            passwordProblem = ErrorMessages.PasswordHasUnverifiedCharacters;
                            break;
					}
				}
			}
			catch( Exception e )
			{
				SetResult("ErrorMessage", e.Message);
				return;
            }

            if (!String.IsNullOrEmpty(passwordProblem))
            {
                SetResult("ErrorMessage", passwordProblem);
                return;
            }
		}

        private bool IsActiveDirectoryAuthEnabledForLender(Guid sBrokerId)
        {
            if (sBrokerId != Guid.Empty)
            {
                BrokerDB lender = BrokerDB.RetrieveById(sBrokerId);
                return lender.IsActiveDirectoryAuthenticationEnabled;
            }
            return false;
        }
    }
}