using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Broker
{
	/// <summary>
	/// Summary description for BrokerEmailList.
	/// </summary>
	public partial class BrokerEmailList : LendersOffice.Admin.SecuredAdminPage
    {
        #region Permission
        static readonly E_InternalUserPermissions[] _brokerEmailListPermissions =
            new[] { E_InternalUserPermissions.ViewBrokers };

		/// <summary>
		/// Return this page's required permissions set.  This
		/// test occurs with each page initialization.
		/// </summary>
		/// <returns>
		/// Array of required permissions.
		/// </returns>
		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

            return _brokerEmailListPermissions;
        }
        #endregion

        #region Web service: Get Save
        [WebMethod]
        public static IEnumerable Get()
        {
            if (SecuredAdminPageEx.IsMissingPermission(_brokerEmailListPermissions)) { return null; }

            const bool isActive = true;
            var brokerId2Email = GetBrokerId2Email();

            var ys = BrokerDbSearch.Search(string.Empty, string.Empty, isActive, string.Empty, Guid.Empty, suiteType: null)
                .Select(b => new Record(b, brokerId2Email));
            return ys;
        }

        /// <summary>
        /// Save email changes.
        /// </summary>
        [WebMethod]
        public static List<string> Save(Record[] brokers)
        {
            if (SecuredAdminPageEx.IsMissingPermission(_brokerEmailListPermissions)) { return null; }

            try
            {
                var failedBrokerIds = new List<string>(brokers.Length);

                // Collect the selected brokers' emails.

                // 7/20/2005 kb - Walk the grid and commit the content to the
                // database for each broker.  We don't share a transaction.
                // See case 2423 for more info.

                foreach (var broker in brokers)
                {
                    BrokerDB brokerDb = null;

                    try { brokerDb = BrokerDB.RetrieveById(new Guid(broker.BrokerId)); }
                    catch {
                        failedBrokerIds.Add(broker.BrokerId);
                        throw;
                        //continue;
                    }

                    brokerDb.EmailAddressesForSystemChangeNotif = (broker.IsInclude) ? broker.GetEmails() : "X";

                    bool saveSuccessed = false;
                    saveSuccessed = brokerDb.Save();

                    if (!saveSuccessed) failedBrokerIds.Add(broker.BrokerId);
                }

                return failedBrokerIds;
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to load web form.", e);
                throw;
            }
        }
        static IDictionary<Guid, string> GetBrokerId2Email()
        {
            var brokerId2Email = new Dictionary<Guid, string>();

            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListBrokersAccountOwners", null))
                {
                    while (reader.Read())
                    {
                        var brokerId = new Guid(reader["BrokerId"].ToString());
                        brokerId2Email.Add(brokerId, reader["Email"].ToString());
                    }
                }
            }
            return brokerId2Email;
        }
        #endregion

        /// <summary>
        /// Initialize this page.
        /// </summary>
        protected void PageLoad(object sender, System.EventArgs a)
        {
            RegisterResources();
        }
        void RegisterResources()
        {
            RegisterJsScript("ractive-0.7.1.min.js");
            RegisterJsScript("LOAdmin.Broker.BrokerEmailList.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        public class Record
        {
            public string BrokerId { get; set; }
            public string BrokerNm { get; set; }
            public string[] Emails { get; set; }
            public bool IsInclude { get; set; }

            void SetEmails(string emails)
            {
                this.Emails = (string.IsNullOrEmpty(emails)) ? new string[0] :
                    emails.Split(';', ',').Select(e => e.TrimWhitespaceAndBOM()).ToArray();
            }
            public string GetEmails()
            {
                return string.Join("; ", Emails);
            }

            public Record(){}
            public Record(BrokerDbSearchItem b)
            {
                this.BrokerId = b.BrokerId.ToString();
                this.BrokerNm = b.BrokerNm;

                var emails = b.EmailAddressesForSystemChangeNotif;
                if (emails.Equals("x", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.IsInclude = false;
                    this.SetEmails(null);
                }
                else { this.SetEmails(emails); }
            }

            public Record(BrokerDbSearchItem b, IDictionary<Guid, string> brokerId2Email)
                : this(b)
            {
                var emails = b.EmailAddressesForSystemChangeNotif;

                if (emails.Equals("x", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.IsInclude = false;
                    this.SetEmails(null);
                }
                else
                {
                    emails = emails.TrimWhitespaceAndBOM();
                    if (string.IsNullOrEmpty(emails) && brokerId2Email.ContainsKey(b.BrokerId))
                    {
                        emails = brokerId2Email[b.BrokerId];
                    }

                    this.SetEmails(emails);

                    this.IsInclude = true;
                }
            }
        }

        private string ErrorMessage
        {
            // Access member.
            set
            {
                ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + ".");
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
