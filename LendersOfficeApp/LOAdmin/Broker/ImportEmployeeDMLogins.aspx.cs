﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using System.IO;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Data.SqlClient;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class ImportEmployeeDMLogins : LendersOffice.Admin.SecuredAdminPage
    {

        private ImportErrorSet errSet = new ImportErrorSet();
        private ImportErrorSet warningSet = new ImportErrorSet();

        private string ErrorMessage
        {
            set { ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + "."); }
        }

        private string FeedBack
        {
            set { ClientScript.RegisterHiddenField("m_feedBack", value.TrimEnd('.') + "."); }
        }


        private Guid BrokerId
        {
            // Access request.

            get
            {
                try
                {
                    return new Guid(Request["brokerId"]);
                }
                catch (HttpException)
                {
                    return Guid.Empty;
                }
                catch (ArgumentNullException)
                {
                    return Guid.Empty;
                }
                catch (FormatException)
                {
                    return Guid.Empty;
                }
            }
        }

        #region Handlers
        /// <summary>
        /// Initialize this page.
        /// </summary>
        protected void PageLoad(object sender, System.EventArgs a)
        {
            var broker = BrokerDB.RetrieveById(BrokerId);
            if (E_BrokerBillingVersion.PerTransaction != broker.BillingVersion)
            {
                string msg = "can't access docMagicUser docmagic importer if billing version isn't " + E_BrokerBillingVersion.PerTransaction;
                throw new CBaseException(msg, msg);
            }

            if (!IsPostBack)
            {
                m_Upload.ToolTip = "Loads text into box below";
                m_tbDocMagicUserDMLogins.Text = "LendingQB Login Name,DocMagic Username,DocMagic Password";
            }
        }

        protected void UploadClick(object sender, EventArgs e)
        {
            // Bring in the specified text file and display it in our
            // editor.

            try
            {
                StreamReader sR = new StreamReader(m_File.PostedFile.InputStream);
                m_tbDocMagicUserDMLogins.Text = sR.ReadToEnd();
            }
            catch (ArgumentException ae)
            {
                ErrorMessage = ae.Message;
            }
            catch (IOException iex)
            {
                Tools.LogError(ErrorMessage = "Failed to upload: " + iex.Message + ".", iex);
            }
        }

        protected void ImportClick(object sender, EventArgs e)
        {
            var docMagicUserIDByName = GetBrokerDocMagicUserIDsByLoginName();
            Guid brokerId = RequestHelper.GetGuid("brokerid");
            var importedDocMagicUserNames = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase); // case insensitive.

            string[] lines = m_tbDocMagicUserDMLogins.Text.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (false == HasGoodHeader(lines))
            {
                errSet.Add("", "Bad header line.", 0);
                WriteAllFeedback();
                return;
            }
            if (lines.Length < 2)
            {
                errSet.Add("", "Nothing to import (no data rows)", 1);
                WriteAllFeedback();
                return;
            }

            for (int lineInd = 1; lineInd < lines.Length; lineInd++)
            {
                string line = lines[lineInd];
                bool lineHasErrors;
                var docMagicUserDocMagicLineInfo = GetDocMagicUserInfoFromLine(line, lineInd, out lineHasErrors);  // may add errors if line is not properly formatted.
                lineHasErrors = WriteDocMagicUserLoginErrors(docMagicUserDocMagicLineInfo, docMagicUserIDByName) || lineHasErrors; // adds missing info errors / missing docMagicUseres errors.
                if (!lineHasErrors)
                {
                    UpdateDocMagicUserInfo(docMagicUserDocMagicLineInfo, docMagicUserIDByName[docMagicUserDocMagicLineInfo.LOLoginName], brokerId);
                }
                CheckForDuplicateDocMagicUser(docMagicUserDocMagicLineInfo, importedDocMagicUserNames);   // adds an error if that docMagicUser already was in the table.
            }

            WriteAllFeedback();
        }
        #endregion

        private bool HasGoodHeader(string[] lines)
        {
            if (lines.Length < 1)
            {
                errSet.Add("", "No header line.", 0);
                return false;
            }
            var header = lines[0];
            var headers = header.Split(',');
            if (headers.Length != 3)
            {
                errSet.Add("", "\""+header + "\" has wrong number of columns", 0);
                return false;
            }
            if (string.Compare(headers[0].TrimWhitespaceAndBOM(), "LendingQB Login Name", true) != 0)
            {
                errSet.Add("", string.Format("header's first column is \"{0}\" and should be \"{1}\"", headers[0], "LendingQB Login Name"), 0);
                return false;
            }
            if (string.Compare(headers[1].TrimWhitespaceAndBOM(), "DocMagic Username", true) != 0)
            {
                errSet.Add("", string.Format("header's second column is \"{0}\" and should be \"{1}\"", headers[1], "DocMagic Username"), 0);
                return false;
            }
            if (string.Compare(headers[2].TrimWhitespaceAndBOM(), "DocMagic Password", true) != 0)
            {
                errSet.Add("", string.Format("header's third column is \"{0}\" and should be \"{1}\"", headers[2], "DocMagic Password"), 0);
                return false;
            }            

            return true;
        }

        private void WriteAllFeedback()
        {
            //! add stuff for feedback?
            if (errSet.Count > 0)
            {
                m_Errors.DataSource = errSet;
                m_Errors.DataBind();

                m_ErrorList.Visible = true;
            }
            else
            {
                m_ErrorList.Visible = false;
            }

            if (warningSet.Count > 0)
            {
                m_WarningList.DataSource = warningSet;
                m_WarningList.DataBind();

                m_Warning.Visible = true;
            }
            else
            {
                m_Warning.Visible = false;
            }
        }

        private void CheckForDuplicateDocMagicUser(DocMagicUserInfo docMagicUserDocMagicLineInfo, HashSet<string> importedDocMagicUserNames)
        {
            if (docMagicUserDocMagicLineInfo == null)
                return;

            if (importedDocMagicUserNames.Contains(docMagicUserDocMagicLineInfo.LOLoginName))
            {
                warningSet.Add(docMagicUserDocMagicLineInfo.LOLoginName, 
                    "duplicate employee login name. (note logins are case insensitive)",
                    docMagicUserDocMagicLineInfo.Line);
            }
            else
            {
                importedDocMagicUserNames.Add(docMagicUserDocMagicLineInfo.LOLoginName);
            }
        }

        private void UpdateDocMagicUserInfo(DocMagicUserInfo docMagicUserDocMagicLineInfo, Guid docMagicUserID, Guid docMagicUserBrokerId)
        {
            DocMagicSavedAuthentication docMagicUser = new DocMagicSavedAuthentication(docMagicUserID, docMagicUserBrokerId);
            docMagicUser.UserName = docMagicUserDocMagicLineInfo.DocMagicUserName;
            docMagicUser.Password = docMagicUserDocMagicLineInfo.DocMagicPassword;
            docMagicUser.Save();
        }

        private bool WriteDocMagicUserLoginErrors(DocMagicUserInfo docMagicUserDocMagicLineInfo, Dictionary<string, Guid> docMagicUserIDByName)
        {
            if (docMagicUserDocMagicLineInfo == null)
            {
                return true;
            }

            bool hasErrors = false;
            if (false == docMagicUserIDByName.ContainsKey(docMagicUserDocMagicLineInfo.LOLoginName)) // it is case-insensitive.
            {
                errSet.Add(docMagicUserDocMagicLineInfo.LOLoginName,
                    "broker doesn't have an employee with this login. (note employee logins are case insensitive)",
                    docMagicUserDocMagicLineInfo.Line);
                hasErrors = true;
            }
            if (string.IsNullOrEmpty(docMagicUserDocMagicLineInfo.DocMagicPassword))
            {
                errSet.Add(docMagicUserDocMagicLineInfo.LOLoginName, "missing password", docMagicUserDocMagicLineInfo.Line);
                hasErrors = true;
            }
            if (string.IsNullOrEmpty(docMagicUserDocMagicLineInfo.DocMagicUserName))
            {
                errSet.Add(docMagicUserDocMagicLineInfo.LOLoginName, "missing username", docMagicUserDocMagicLineInfo.Line);
                hasErrors = true;
            }
            return hasErrors;
        }

        private DocMagicUserInfo GetDocMagicUserInfoFromLine(string line, int lineInd, out bool lineHasErrors)
        {
            var split = line.Split(',');
            lineHasErrors = false;
            if (split.Length != 3)
            {
                errSet.Add("unknown", "line: \"" + line + "\" does not have the appropriate number of commas (2)", lineInd);
                lineHasErrors = true;
                return null;
            }
            return new DocMagicUserInfo()
            {
                LOLoginName = split[0].TrimWhitespaceAndBOM(),
                DocMagicUserName = split[1].TrimWhitespaceAndBOM(),
                DocMagicPassword = split[2].TrimWhitespaceAndBOM(),
                Line = lineInd
            };
        }

        private Dictionary<string, Guid> GetBrokerDocMagicUserIDsByLoginName()
        {
            Guid brokerId = RequestHelper.GetGuid("brokerid");
            SqlParameter[] parameters = {
                                 new SqlParameter("@BrokerId", brokerId),
                                 new SqlParameter("@IsActiveFilter", true) // SK - note only taking active users.
                             };
            var authByName = new Dictionary<string, Guid>(StringComparer.InvariantCultureIgnoreCase); // logins are case insensitive.
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeeByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    if (reader["EmployeeUserId"] != DBNull.Value && reader["LoginNm"] != DBNull.Value)
                    {
                        Guid userID = (Guid)reader["EmployeeUserId"];
                        string loginName = (string)reader["LoginNm"];
                        authByName.Add(loginName, userID);
                    }
                }
            }
            return authByName;
        }

        private class DocMagicUserInfo
        {
            public string LOLoginName;
            public string DocMagicUserName;
            public string DocMagicPassword;
            public int Line;
        }

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.EditBroker ,
				E_InternalUserPermissions.EditEmployee
			};
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
