using System;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
	public partial class EditEmployee : LendersOffice.Admin.SecuredAdminPage
	{

		protected LendersOfficeApp.los.admin.EmployeeEdit m_Edit;


		/// <summary>
		/// Return this page's required permissions set.  This
		/// test occurs with each page initialization.
		/// </summary>
		/// <returns>
		/// Array of required permissions.
		/// </returns>

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.EditEmployee
			};
		}

        protected void PageLoad( object sender , System.EventArgs a )
		{
			// Bind the passed in employee to our control.


            if (HasPermissionToDo(E_InternalUserPermissions.IsDevelopment) == true)
            {
                m_Edit.IsDeveloper = true;
            }
            else
            {
                m_Edit.IsDeveloper = false;
                
            }
            m_Edit.IsInternal = true;
            m_Edit.DataEditor = CurrentPrincipal.UserId;
            m_Edit.DataBroker = RequestHelper.GetGuid("brokerId", Guid.Empty);

            if (IsPostBack == false)
            {
                m_Edit.DataSource = RequestHelper.GetGuid("employeeId", Guid.Empty);
                m_Edit.DataBranch = RequestHelper.GetGuid("branchId", Guid.Empty);

                m_Edit.DataBind();
            }
		}

        private void PageInit(object sender, System.EventArgs args)
        {
            this.RegisterJsScript("utilities.js");
            this.RegisterService("utilities", "/common/UtilitiesService.aspx");
            this.RegisterService("main", "/loadmin/Broker/DefaultRolePermissionsService.aspx");
            this.RegisterService("passwordvalidate", "/loadmin/Broker/PasswordValidationService.aspx");
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion

	}

}
