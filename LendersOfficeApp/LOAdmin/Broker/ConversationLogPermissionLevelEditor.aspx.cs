﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Linq;
    using System.Web;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Security.Authorization.ConversationLog;
    using LendersOffice.ObjLib.Views;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;    

    /// <summary>
    /// Page that allows changing a conversation log permissionLevel for a lender.
    /// </summary>
    public partial class ConversationLogPermissionLevelEditor : SecuredAdminPage
    {
        /// <summary>
        /// Gets the passed in broker id.
        /// </summary>
        /// <value>The passed in broker id.</value>
        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("BrokerId");
            }
        }

        /// <summary>
        /// Gets the id of the permission level being edited, or null if it's a new permission level.
        /// </summary>
        /// <value>The id of the permission level, or null if new.</value>
        protected long? PermissionLevelId
        {
            get
            {
                string key = "permissionLevelId";
                long value;
                if (long.TryParse(HttpContext.Current.Request.QueryString[key], out value))
                {
                    return value;
                }

                return null;
            }
        }

        /// <summary>
        /// Saves the permission level.
        /// </summary>
        /// <param name="permissionLevelString">The json form of the permission level.</param>
        /// <returns>Reasons the save couldn't take place, or the permission level itself, in json form.</returns>
        [System.Web.Services.WebMethod]
        public static string SavePermissionLevel(string permissionLevelString)
        {
            return Tools.LogAndThrowIfErrors(() =>
            {
                string brokerIdString = RequestHelper.GetSafeQueryString("BrokerId");

                var viewOfPermissionLevel = SerializationHelper.JsonNetDeserialize<ViewOfPermissionLevelForLoAdminEdit>(permissionLevelString);
                var permissionLevel = viewOfPermissionLevel.AsPermissionLevel();                

                var reasonsInvalid = permissionLevel.Save();
                if (reasonsInvalid.Any())
                {
                    return SerializationHelper.JsonNetAnonymousSerialize(new
                    {
                        userMessage = reasonsInvalid
                    });
                }
                else
                {
                    var viewOfPermissionLevelForLevels = ViewOfPermissionLevelForLoAdmin.FromPermissionLevel(permissionLevel);
                    return SerializationHelper.JsonNetSerialize(viewOfPermissionLevelForLevels);
                }
            });
        }

        /// <summary>
        /// The Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.Response.Headers.Add("X-UA-Compatible", "IE=edge");
            this.EnableJquery = true;
            this.PageID = "ConversationLogPermissionLevelEditor";
            this.PageTitle = "Conversation Log Permission Level Editor";
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("stylesheet.css");

            this.RegisterJsScript("angular-1.4.8.min.js");
            this.RegisterJsGlobalVariables(
                "permissionLevelNameRegexString",
                string.Format("^({0})$", RegularExpressionString.ConversationLogPermissionLevelName.ToString()));

            this.RegisterJsGlobalVariables(
                "permissionLevelDescriptionRegexString",
                string.Format("^({0})$", RegularExpressionString.ConversationLogPermissionLevelDescription.ToString()));

            var allRoles = Role.AllRoles.Select(role => new { description = role.ModifiableDesc, id = role.Id }).OrderBy(a => a.description);
            var allEmployeeGroups = GroupDB.GetAllGroupIdsAndNames(this.BrokerId, GroupType.Employee).Select(group => new { description = group.Item2, id = group.Item1 }).OrderBy(a => a.description);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer(
                "instancesOfTypeByTypeName", 
                new
                {
                    role = allRoles,
                    group = allEmployeeGroups
                });

            var brokerId = BrokerIdentifier.Create(this.BrokerId.ToString()).Value;

            ConversationLogPermissionLevel level;
            if (this.PermissionLevelId.HasValue)
            {
                level = ConversationLogPermissionLevel.GetPermissionLevelById(brokerId, this.PermissionLevelId.Value);
            }
            else
            {
                level = new ConversationLogPermissionLevel(brokerId);
            }

            var viewOfLevel = ViewOfPermissionLevelForLoAdminEdit.FromPermissionLevel(level);
            this.RegisterJsObjectWithJsonNetSerializer("initialPermissionLevel", viewOfLevel);
        }
    }
}
