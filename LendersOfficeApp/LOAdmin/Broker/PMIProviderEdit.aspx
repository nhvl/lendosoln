﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PMIProviderEdit.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.PMIProviderEdit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Edit Configuration for PMI Providers</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">	
</head>
<body>
    <script language="javascript">
        function _init() {             
            resize(450, 350);
        }

        function updateClick() {
            var args = window.dialogArguments || {};
            args.OK = true;
            args.genworth = document.getElementById('PmiProviderGenworthRanking').value;
            args.mgic = document.getElementById('PmiProviderMgicRanking').value;
            args.radian = document.getElementById('PmiProviderRadianRanking').value;
            args.essent = document.getElementById('PmiProviderEssentRanking').value;
            args.arch = document.getElementById('PmiProviderArchRanking').value;
            args.masshousing = document.getElementById('PmiProviderMassHousingRanking').value;
            args.highWinner = document.getElementById('winnerHighest').checked ? "1" : "0";
            args.national = document.getElementById('PmiProviderNationalMIRanking').value; 
            onClosePopup(args);
        }
    </script>
    <h4 class="page-header">Edit Configuration for PMI Providers</h4>
    <form id="form1" runat="server">    
    <table>		    
        <tr>
            <td class="FieldLabel">
                Supported PMI Providers
            </td>
            <td class="FieldLabel">
                Ranking*
            </td>
        </tr>
        <tr>
            <td>Arch</td>
            <td>
	            <asp:DropDownList id="PmiProviderArchRanking" runat="server"></asp:DropDownList>
            
            </td>
        </tr>
	    <tr>
	        <td>
	            Essent
	        </td>
	        <td>
	            <asp:DropDownList id="PmiProviderEssentRanking" runat="server"></asp:DropDownList>
	        </td>
	    </tr>
	    <tr>
	        <td>
	            Genworth
	        </td>
	        <td>
	            <asp:DropDownList id="PmiProviderGenworthRanking" runat="server"></asp:DropDownList>
	        </td>
	    </tr>
	    <tr>
	        <td>
	            MGIC
	        </td>
	        <td>
	            <asp:DropDownList id="PmiProviderMgicRanking" runat="server"></asp:DropDownList>
	        </td>
	    </tr>
	    <tr>
	        <td>
	            Radian
	        </td>
	        <td>
	            <asp:DropDownList id="PmiProviderRadianRanking" runat="server"></asp:DropDownList>
	        </td>
	    </tr>
	    <tr>
	        <td>National MI</td>
	        <td>
	            <asp:DropDownList runat="server" ID="PmiProviderNationalMIRanking"></asp:DropDownList>
	        </td>
	    </tr>
        <tr>
	        <td>
	            MassHousing
	        </td>
	        <td>
	            <asp:DropDownList id="PmiProviderMassHousingRanking" runat="server"></asp:DropDownList>
	        </td>
	    </tr>
	    <tr>
            <td colspan="2">
                *PML will perform a best execution of results from providers of equal ranking. Providers with worse ranking will only be used when results of more preferred providers are not available.
            </td>
        </tr>
	    <tr>
            <td colspan="2">
            <span class="FieldLabel">PMI Winner is Provider with: </span>
            <input type=radio  id="winnerLowest" name="winningPML" value="low" runat="server" /><label for="winnerLowest">Lowest Premium</label>
            <input type=radio id="winnerHighest" name="winningPML" value="high" runat="server" /><label for="winnerHighest">Highest Premium</label>
            </td>
        </tr>
        <tr>
	        <td colspan="2" align="center">
	            <input type="button" value="Update" onclick="updateClick();" />
	        </td>
	    </tr>
	</table>
    </form>
</body>
</html>
