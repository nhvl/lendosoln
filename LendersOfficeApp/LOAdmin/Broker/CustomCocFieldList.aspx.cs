﻿namespace LendersOfficeApp.LOAdmin.Broker
{    
    using System;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.CustomCocField;

    /// <summary>
    /// Custom coc field list page.
    /// </summary>
    public partial class CustomCocFieldList : SecuredAdminPage
    {
        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">EventArgs object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {            
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterService("main", "/LOAdmin/Broker/CustomCocFieldListService.aspx");

            var brokerId = RequestHelper.GetGuid("brokerId");
            var fields = CustomCocFieldListHelper.RetrieveByBrokerId(brokerId);
            bool useCustomCocFields = BrokerDB.RetrieveUseCustomCocFieldListBit(brokerId);

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("customFields", fields);
            this.RegisterJsGlobalVariables("BrokerId", brokerId);
            this.RegisterJsGlobalVariables("UseCustomCocFields", useCustomCocFields);
        }
    }
}