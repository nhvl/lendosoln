﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Security;

    /// <summary>
    /// Client Certificate editor for lender client certificates.
    /// </summary>
    public partial class EditClientCertificate : SecuredAdminPage
    {
        /// <summary>
        /// Gets the passed in broker id.
        /// </summary>
        /// <value>The passed in broker id.</value>
        private Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("BrokerId");
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not this is a new certificate.
        /// </summary>
        /// <value>A parameter indicating if this is a new certificate or not.</value>
        private bool IsNew
        {
            get
            {
                return RequestHelper.GetBool("IsNew");
            }
        }

        /// <summary>
        /// Gets the required permissions to access this page.
        /// </summary>
        /// <returns>The permissions associated with this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker
            };
        }

        /// <summary>
        /// The Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            this.PageID = "EditClientCertificate";
            this.PageTitle = "Register Client Certificate";
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("stylesheet.css");
            this.RegisterService("certService", "/loadmin/Broker/BrokerClientCertificateService.aspx");
        }

        /// <summary>
        /// The Page_Load function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            bool isNew = this.IsNew;
            Guid brokerId = this.BrokerId;
            this.IsNewValue.Value = isNew.ToString();
            this.BrokerIdValue.Value = brokerId.ToString();

            if (isNew)
            {
                this.IndividualBtn.Checked = true;
                this.TypeLqbBtn.Checked = true;
                this.NextButton.Value = "Next";
            }
            else
            {
                // We need to populate the fields here from the value in the DB.
                this.NextButton.Value = "Save";

                Guid certId = new Guid(RequestHelper.GetSafeQueryString("certificateId"));

                LenderClientCertificate cert = LenderClientCertificate.GetLenderClientCertificate(brokerId, certId);
                if (cert == null)
                {
                    throw new CBaseException("Certificate not found.", "Certificate not found.");
                }

                this.CorporateBtn.Checked = cert.Level == ClientCertificateLevelTypes.Corporate;
                this.BranchGroupBtn.Checked = cert.Level == ClientCertificateLevelTypes.BranchGroup;
                this.OcGroupBtn.Checked = cert.Level == ClientCertificateLevelTypes.OcGroup;
                this.EmployeeGroupBtn.Checked = cert.Level == ClientCertificateLevelTypes.EmployeeGroup;
                this.IndividualBtn.Checked = cert.Level == ClientCertificateLevelTypes.Individual;

                this.TypeLqbBtn.Checked = cert.UserType == ClientCertificateUserTypes.Lqb;
                this.TypeTpoBtn.Checked = cert.UserType == ClientCertificateUserTypes.Tpo;
                this.TypeAnyBtn.Checked = cert.UserType == ClientCertificateUserTypes.Any;

                this.Description.Text = cert.Description;
                this.LoginNm.Text = cert.AssociatedUserLogin;
                this.Notes.Text = cert.Notes;
                this.CertificateId.Value = cert.CertificateId.ToString();

                this.SelectedGroupIds.Value = string.Join(",", cert.GroupIds.Select(id => id.ToString()));
            }

            // We can disable the radio buttons here when we are opening the page in edit mode.
            this.CorporateBtn.Enabled = isNew;
            this.BranchGroupBtn.Enabled = isNew;
            this.OcGroupBtn.Enabled = isNew;
            this.EmployeeGroupBtn.Enabled = isNew;
            this.IndividualBtn.Enabled = isNew;

            this.TypeLqbBtn.Enabled = isNew;
            this.TypeTpoBtn.Enabled = isNew;
            this.TypeAnyBtn.Enabled = isNew;

            this.AssocBranchGroupLink.Disabled = !this.BranchGroupBtn.Checked;
            this.AssocOcGroupLink.Disabled = !this.OcGroupBtn.Checked;
            this.AssocEmployeeGroupLink.Disabled = !this.EmployeeGroupBtn.Checked;

            this.LoginNm.Enabled = isNew && this.IndividualBtn.Checked;
            this.LoginNmReqIcon.Style["display"] = this.IndividualBtn.Checked ? string.Empty : "none";
        }
    }
}