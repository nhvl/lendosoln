﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using LendersOffice.Audit;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOffice.HttpModule;
using LqbGrammar.DataTypes;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class AuditHistoryDetail : LendersOffice.Admin.SecuredAdminPage
    {
        protected void PageLoad(object sender, System.EventArgs e)
        {

            try
            {
                PerformanceMonitorItem item = this.Context.Items[ConstAppDavid.ContextItemKey_PerformanceMonitorItem] as PerformanceMonitorItem;
                if (null != item)
                    item.IsEnabledDebugOutput = false;
            }
            catch { }


            string key = RequestHelper.GetSafeQueryString("key");
            if (string.IsNullOrEmpty(key))
            {
                Guid sLId = RequestHelper.LoanID;
                Guid auditId = RequestHelper.GetGuid("auditid");

                Guid brokerId;
                DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);

                if (!Tools.GetAuditHistoryMigrated(sLId, brokerId)) // old audit format
                {
                    AuditManager.RenderAuditDetail(sLId, auditId, Response.OutputStream);
                }
                else
                {
                    AuditManager.RenderAuditDetailVer2(brokerId, sLId, auditId, Response.OutputStream);
                }
            }
            else
            {
                string data = LendersOffice.Drivers.Encryption.EncryptionHelper.DecodeAndDecrypt(EncryptionKeyIdentifier.AuditKeyPassword, key);
                var tokens = data.Split('/');
                Guid sLId = new Guid(tokens[0]);
                long auditSeq = long.Parse(tokens[1]);

                Guid brokerId;
                DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);

                AuditManager.RenderAuditDetailVer2(brokerId, sLId, auditSeq, Response.OutputStream);
            }

            Response.Flush();
            Response.End();
           
        }

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.ViewBrokers
			};
        }


        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

       
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }

    }
}
