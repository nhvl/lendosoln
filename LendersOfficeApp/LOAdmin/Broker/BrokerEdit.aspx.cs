namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Admin.DocuSign;
    using LendersOffice.Common;
    using LendersOffice.Common.TextImport;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.Integration.Cenlar;
    using LendersOffice.Integration.DataRetrievalFramework;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.Irs4506T;
    using LendersOffice.Integration.OCR;
    using LendersOffice.ObjLib.Conversions.Flood;
    using LendersOffice.ObjLib.Disclosure.EConsentMonitoring;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;

    public partial class BrokerEdit : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        #region Variables
        //12/12/08 jk - OPM 20030
        protected TextBox m_newNormalUserExpiredMessage;
        protected TextBox m_newLockDeskExpiredMessage;
        protected TextBox m_newNormalUserCutOffMessage;
        protected TextBox m_newLockDeskCutOffMessage;
        protected TextBox m_newOutsideNormalHoursMessage;
        protected TextBox m_newOutsideClosureDayHourMessage;
        protected string m_newOutsideNormalHoursAndPassInvestorCutOffMessage;

        protected string m_defaultNormalUserExpiredMessage;
        protected string m_defaultLockDeskExpiredMessage;
        protected string m_defaultNormalUserCutOffMessage;
        protected string m_defaultLockDeskCutOffMessage;
        protected string m_defaultOutsideNormalHoursMessage;
        protected string m_defaultOutsideClosureDayHourMessage;
        protected string m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage;
        private OCRService m_ocrService;

        protected Boolean nothingLoaded;
 // 3/22/2006 mf - OPM 4441
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.Button btnSave;
        protected System.Web.UI.WebControls.Button btnAddSampleReports;
        protected System.Web.UI.WebControls.Button m_ResetCounter;

        private System.Web.UI.WebControls.CheckBox[] m_isPdfViewNeededList;

        protected System.Web.UI.WebControls.Literal m_isActiveLiteral;

        private string m_brokerStatus = "1";
        private System.Web.UI.WebControls.DropDownList[] m_craList;
        private List<Guid> m_AppraisalIntegrationsForBroker;
        private List<Guid> AppraisalIntegrationsForBroker
        {
            get
            {
                if (m_AppraisalIntegrationsForBroker == null)
                {
                    m_AppraisalIntegrationsForBroker = AppraisalVendorFactory.GetAvailableVendorsForBroker(m_brokerId).Select(v => v.VendorId).ToList();
                }
                return m_AppraisalIntegrationsForBroker;
            }
        }

        private Guid m_userID 
        {
            get { return ((InternalUserPrincipal) Page.User).UserId; }
        }

        protected Guid m_brokerId
        {
            get
            {
                if (ViewState["brokerid"] == null)
                {
                    ViewState["brokerid"] = RequestHelper.GetGuid("brokerid", Guid.Empty);
                }
                return (Guid)ViewState["brokerid"] ;
            }
            set
            {
                ViewState["brokerid"] = value ;
            }
        }

        private List<DocumentVendorBrokerSettings> EnabledDocVendors
        {
            get
            {
                if (ViewState["enabledDocVendors"] == null)
                {
                    ViewState["enabledDocVendors"] = DocumentVendorBrokerSettings.ListAllForBroker(m_brokerId);
                }

                return (List<DocumentVendorBrokerSettings>)ViewState["enabledDocVendors"];
            }
            set
            {
                ViewState["enabledDocVendors"] = value;
            }
        }

        
        #endregion

        protected void DownloadClosingCostSetup_OnClick(object sender, EventArgs arg)
        {
            RequirePermission(E_InternalUserPermissions.ViewBrokers);

            if (this.m_brokerId == Guid.Empty)
            {
                m_Message.Text = "Cannot download closing cost set for new Broker.";
                m_Message.ForeColor = Color.Red;
                return;
            }
            BrokerDB db = BrokerDB.RetrieveByIdForceRefresh(this.m_brokerId);

            if (String.IsNullOrEmpty(db.ClosingCostFeeSetupJsonContent))
            {
                m_Message.Text = "Error: There is no closing cost fee setup.";
                m_Message.ForeColor = Color.Red;
                return;
            }

            string path = TempFileUtils.NewTempFilePath();
            TextFileHelper.WriteString(path, db.ClosingCostFeeSetupJsonContent, true);
            string filename = String.Format("LenderClosingCostSetup_{0}_{1}_{2}.json", db.CustomerCode, ConstAppDavid.CurrentServerLocation, DateTime.Now.ToString("yyyyMMddHHmmss"));
            RequestHelper.SendFileToClient(this.Context, path, "application/json", filename);
            this.Response.End();
        }

        protected void UploadClosingCostSetup_OnCLick(object sender, EventArgs args)
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            if (this.m_brokerId == Guid.Empty)
            {
                m_Message.Text = "Cannot import closing cost set on unsaved new broker.";
                m_Message.ForeColor = Color.Red;
                return;
            }

            if (!ClosingCostSetFile.HasFile)
            {
                m_Message.Text = "You did not upload any file.";
                m_Message.ForeColor = Color.Red;
                return;
            }

            string json; 
            using (StreamReader sr = new StreamReader(ClosingCostSetFile.FileContent))
            {
                json = sr.ReadToEnd();
            }

            BrokerDB db = BrokerDB.RetrieveByIdForceRefresh(this.m_brokerId);
            if (db.SetClosingCostJSON(json))
            {
                db.Save();
                m_Message.Text = "Closing Cost Set Updated.";
            }
            else
            {
                m_Message.Text = "There was an error in setting the closing cost set.";
                m_Message.ForeColor = Color.Red;
            }

        }

        private void Bind_ShowCredcoOption(DropDownList ddl) 
        {
            ddl.Items.Add(new ListItem("Display both direct and Fannie Mae connection", E_ShowCredcoOptionT.DisplayBoth.ToString("D")));
            ddl.Items.Add(new ListItem("Display only direct connection to Credco", E_ShowCredcoOptionT.DisplayOnlyDirect.ToString("D")));
            ddl.Items.Add(new ListItem("Display only Fannie Mae connection to Credco", E_ShowCredcoOptionT.DisplayOnlyFNMA.ToString("D")));
        }

        private void Bind_SequentialNumberingFieldIds(DropDownList ddl)
        {
            // When updating which fields can be used for this feature,
            // update target fields of CSequentialNumberingData.
            ddl.Items.Add(new ListItem("", ""));
            ddl.Items.Add(new ListItem("sCustomField1Notes", "sCustomField1Notes"));
            ddl.Items.Add(new ListItem("sCustomField2Notes", "sCustomField2Notes"));
            ddl.Items.Add(new ListItem("sCustomField3Notes", "sCustomField3Notes"));
            ddl.Items.Add(new ListItem("sCustomField4Notes", "sCustomField4Notes"));
            ddl.Items.Add(new ListItem("sCustomField5Notes", "sCustomField5Notes"));
            ddl.Items.Add(new ListItem("sCustomField6Notes", "sCustomField6Notes"));
            ddl.Items.Add(new ListItem("sCustomField7Notes", "sCustomField7Notes"));
            ddl.Items.Add(new ListItem("sCustomField8Notes", "sCustomField8Notes"));
            ddl.Items.Add(new ListItem("sCustomField9Notes", "sCustomField9Notes"));
            ddl.Items.Add(new ListItem("sCustomField10Notes", "sCustomField10Notes"));
            ddl.Items.Add(new ListItem("sCustomField11Notes", "sCustomField11Notes"));
            ddl.Items.Add(new ListItem("sCustomField12Notes", "sCustomField12Notes"));
            ddl.Items.Add(new ListItem("sCustomField13Notes", "sCustomField13Notes"));
            ddl.Items.Add(new ListItem("sCustomField14Notes", "sCustomField14Notes"));
            ddl.Items.Add(new ListItem("sCustomField15Notes", "sCustomField15Notes"));
            ddl.Items.Add(new ListItem("sCustomField16Notes", "sCustomField16Notes"));
            ddl.Items.Add(new ListItem("sCustomField17Notes", "sCustomField17Notes"));
            ddl.Items.Add(new ListItem("sCustomField18Notes", "sCustomField18Notes"));
            ddl.Items.Add(new ListItem("sCustomField19Notes", "sCustomField19Notes"));
            ddl.Items.Add(new ListItem("sCustomField20Notes", "sCustomField20Notes"));

            ddl.Items.Add(new ListItem("sCustomField21Notes", "sCustomField21Notes"));
            ddl.Items.Add(new ListItem("sCustomField22Notes", "sCustomField22Notes"));
            ddl.Items.Add(new ListItem("sCustomField23Notes", "sCustomField23Notes"));
            ddl.Items.Add(new ListItem("sCustomField24Notes", "sCustomField24Notes"));
            ddl.Items.Add(new ListItem("sCustomField25Notes", "sCustomField25Notes"));
            ddl.Items.Add(new ListItem("sCustomField26Notes", "sCustomField26Notes"));
            ddl.Items.Add(new ListItem("sCustomField27Notes", "sCustomField27Notes"));
            ddl.Items.Add(new ListItem("sCustomField28Notes", "sCustomField28Notes"));
            ddl.Items.Add(new ListItem("sCustomField29Notes", "sCustomField29Notes"));

            ddl.Items.Add(new ListItem("sCustomField30Notes", "sCustomField30Notes"));
            ddl.Items.Add(new ListItem("sCustomField31Notes", "sCustomField31Notes"));
            ddl.Items.Add(new ListItem("sCustomField32Notes", "sCustomField32Notes"));
            ddl.Items.Add(new ListItem("sCustomField33Notes", "sCustomField33Notes"));
            ddl.Items.Add(new ListItem("sCustomField34Notes", "sCustomField34Notes"));
            ddl.Items.Add(new ListItem("sCustomField35Notes", "sCustomField35Notes"));
            ddl.Items.Add(new ListItem("sCustomField36Notes", "sCustomField36Notes"));
            ddl.Items.Add(new ListItem("sCustomField37Notes", "sCustomField37Notes"));
            ddl.Items.Add(new ListItem("sCustomField38Notes", "sCustomField38Notes"));
            ddl.Items.Add(new ListItem("sCustomField39Notes", "sCustomField39Notes"));

            ddl.Items.Add(new ListItem("sCustomField40Notes", "sCustomField40Notes"));
            ddl.Items.Add(new ListItem("sCustomField41Notes", "sCustomField41Notes"));
            ddl.Items.Add(new ListItem("sCustomField42Notes", "sCustomField42Notes"));
            ddl.Items.Add(new ListItem("sCustomField43Notes", "sCustomField43Notes"));
            ddl.Items.Add(new ListItem("sCustomField44Notes", "sCustomField44Notes"));
            ddl.Items.Add(new ListItem("sCustomField45Notes", "sCustomField45Notes"));
            ddl.Items.Add(new ListItem("sCustomField46Notes", "sCustomField46Notes"));
            ddl.Items.Add(new ListItem("sCustomField47Notes", "sCustomField47Notes"));
            ddl.Items.Add(new ListItem("sCustomField48Notes", "sCustomField48Notes"));
            ddl.Items.Add(new ListItem("sCustomField49Notes", "sCustomField49Notes"));

            ddl.Items.Add(new ListItem("sCustomField50Notes", "sCustomField50Notes"));
            ddl.Items.Add(new ListItem("sCustomField51Notes", "sCustomField51Notes"));
            ddl.Items.Add(new ListItem("sCustomField52Notes", "sCustomField52Notes"));
            ddl.Items.Add(new ListItem("sCustomField53Notes", "sCustomField53Notes"));
            ddl.Items.Add(new ListItem("sCustomField54Notes", "sCustomField54Notes"));
            ddl.Items.Add(new ListItem("sCustomField55Notes", "sCustomField55Notes"));
            ddl.Items.Add(new ListItem("sCustomField56Notes", "sCustomField56Notes"));
            ddl.Items.Add(new ListItem("sCustomField57Notes", "sCustomField57Notes"));
            ddl.Items.Add(new ListItem("sCustomField58Notes", "sCustomField58Notes"));
            ddl.Items.Add(new ListItem("sCustomField59Notes", "sCustomField59Notes"));
            ddl.Items.Add(new ListItem("sCustomField60Notes", "sCustomField60Notes"));
        }

        private void Bind_RateLockExpirationWeekendHolidayBehavior(RadioButtonList list)
        {
            list.Items.Add(new ListItem("Allow the expiration date to fall on the weekend/holiday.", E_RateLockExpirationWeekendHolidayBehavior.AllowWeekendHoliday.ToString("d")));
            list.Items.Add(new ListItem("Set lock expiration date to be first preceding business day.", E_RateLockExpirationWeekendHolidayBehavior.PrecedingBusinessDay.ToString("d")));
            list.Items.Add(new ListItem("Set lock expiration date to be first following business day.", E_RateLockExpirationWeekendHolidayBehavior.FollowingBusinessDay.ToString("d")));
        }

        private void Bind_Pml2AsQuickPricerMode(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("No", E_Pml2AsQuickPricerMode.Disabled.ToString("D")));
            ddl.Items.Add(new ListItem("Yes - User level access", E_Pml2AsQuickPricerMode.PerUser.ToString("D")));
            ddl.Items.Add(new ListItem("Yes", E_Pml2AsQuickPricerMode.Enabled.ToString("D")));
        }

        private void Bind_SelectedNewLoanEditorUIPermissionType(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("Hard Disabled (for all)", E_PermissionType.HardDisable.ToString("D")));
            ddl.Items.Add(new ListItem("Hard Enabled  (for all)", E_PermissionType.HardEnable.ToString("D")));
            ddl.Items.Add(new ListItem("Defer to user permission", E_PermissionType.DeferToMoreGranularLevel.ToString("D")));
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            EnableJquery = true;
            RegisterJsScript("LQBPopup.js");
            m_Zipcode.SmartZipcode(m_City, m_State);
            m_isPdfViewNeededList = new System.Web.UI.WebControls.CheckBox[10];
            m_craList = new System.Web.UI.WebControls.DropDownList[10];

            m_isPdfViewNeededList[0] = IsPdfViewNeeded0;
            m_craList[0] = CRA0;

            m_isPdfViewNeededList[1] = IsPdfViewNeeded1;
            m_craList[1] = CRA1;

            m_isPdfViewNeededList[2] = IsPdfViewNeeded2;
            m_craList[2] = CRA2;

            m_isPdfViewNeededList[3] = IsPdfViewNeeded3;
            m_craList[3] = CRA3;

            m_isPdfViewNeededList[4] = IsPdfViewNeeded4;
            m_craList[4] = CRA4;

            m_isPdfViewNeededList[5] = IsPdfViewNeeded5;
            m_craList[5] = CRA5;

            m_isPdfViewNeededList[6] = IsPdfViewNeeded6;
            m_craList[6] = CRA6;

            m_isPdfViewNeededList[7] = IsPdfViewNeeded7;
            m_craList[7] = CRA7;

            m_isPdfViewNeededList[8] = IsPdfViewNeeded8;
            m_craList[8] = CRA8;

            m_isPdfViewNeededList[9] = IsPdfViewNeeded9;
            m_craList[9] = CRA9;


            var list = MasterCRAList.RetrieveAvailableCras(m_brokerId);
            for (int i = 0; i < 10; i++)
                BindCRA(m_craList[i], list);

            BindCRA(CreditProxy_CraId, list);

            string str = "";
            foreach (CRA o in list) 
            {
                if (o.Protocol != CreditReportProtocol.Mcl) 
                {
                    str += "'" + o.ID + "',";
                }
            }
            str += "'" + Guid.Empty + "'";
            ClientScript.RegisterArrayDeclaration("g_aNonMclList", str);


            if (m_brokerId != Guid.Empty)
            {
                IDictionary<string, string> templateIDsByName = new Dictionary<string, string>();
                SqlParameter[] parameters = {
                                                 new SqlParameter( "@BrokerID" , m_brokerId ),
                                                  new SqlParameter( "@IsTemplate" , 1 ),
                                                  new SqlParameter( "@IsValid" , 1 )
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "ListLoansByBrokerId", parameters))
                {
                    while (reader.Read())
                    {
                        templateIDsByName.Add(reader["LoanNm"].ToString(), reader["LoanId"].ToString());
                    }
                }

                var templateIDsSortedByName = templateIDsByName.OrderBy((kvp) => kvp.Key).ToArray();

                PmlLoanTemplateId.DataTextField  = "Key";
                m_quickPricerTemplateId.DataTextField = "Key";
                m_quickPricerTemplateId.DataValueField = "Value";
                PmlLoanTemplateId.DataValueField = "Value"; 
                PmlLoanTemplateId.DataSource = templateIDsSortedByName;
                m_quickPricerTemplateId.DataSource = templateIDsSortedByName;
                m_quickPricerTemplateId.DataBind(); 
                PmlLoanTemplateId.DataBind();
            }

            Bind_ShowCredcoOption(ShowCredcoOptionT);
            PmlLoanTemplateId.Items.Insert(0, new ListItem("<-- None -->", Guid.Empty.ToString()));
            m_quickPricerTemplateId.Items.Insert(0, new ListItem("<-- None -->", Guid.Empty.ToString()));
            this.RegisterService("main", "/loadmin/Broker/BrokerEditService.aspx");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsGlobalVariables("stageName", ConstAppDavid.CurrentServerLocation.ToString("G"));
            this.RegisterJsGlobalVariables("convLogPermissionLevelsLink", Page.ResolveUrl("~/LOAdmin/Broker/ConversationLogPermissionLevelsEditor.aspx"));

            foreach (var name in Enum.GetNames(typeof(E_BrokerBillingVersion)))
            {
                if (name == "New")
                {
                    // 4/20/2015 dd - Will deprecate this Billing Version.
                    continue;
                }
                m_ddlBillingVersion.Items.Add(new ListItem(name));
            }

            Bind_SequentialNumberingFieldIds(m_SequentialNumberingFieldId);

            Bind_RateLockExpirationWeekendHolidayBehavior(m_rateLockExpirationWeekendHolidayBehavior);

            BindBrokerTempOptionList();

            Bind_Pml2AsQuickPricerMode(m_Pml2AsQuickPricerMode);

            Bind_SelectedNewLoanEditorUIPermissionType(m_SelectedNewLoanEditorUIPermissionType);

            #region OPM 88442 - IRS 4506-T Vendor
            m_4506VendorList.DataSource = Irs4506TVendorConfiguration.ListActiveVendor();
            m_4506VendorList.DataValueField = "VendorId";
            m_4506VendorList.DataTextField = "VendorName";
            m_4506VendorList.DataBind();
            #endregion

            #region OPM 126000 - Data Retrieval Framework Partners
            m_DRFPartnerList.DataSource = DataRetrievalPartner.ListPartners();
            m_DRFPartnerList.DataValueField = "PartnerID";
            m_DRFPartnerList.DataTextField = "Name";
            m_DRFPartnerList.DataBind();
            #endregion

            #region DB Split
            m_databaseList.DataSource = DbConnectionInfo.ListAll();
            m_databaseList.DataValueField = "Id";
            m_databaseList.DataTextField = "FriendlyDisplay";
            m_databaseList.DataBind();
            #endregion

            Tools.BindGenericEnum<E_sClosingCostFeeVersionT>(m_minimumDefaultClosingCostDataSet);

            #region Flood Resellers.
            m_ddlFloodReseller.DataSource = FloodReseller.ListAll().Values.OrderBy(i => i.Name).ToArray(); ;
            m_ddlFloodReseller.DataTextField = "Name";
            m_ddlFloodReseller.DataValueField = "Id";
            m_ddlFloodReseller.DataBind();
            m_ddlFloodReseller.Items.Insert(0, new ListItem("<--None Selected-->", Guid.Empty.ToString()));
            #endregion

            this.RegisterJsGlobalVariables("BlankSuiteType", BrokerSuiteType.Blank.ToString("D"));
            this.RegisterJsGlobalVariables("InvalidBrokerSuiteTypeMessage", ErrorMessages.InvalidBrokerSuiteType);
            Tools.BindGenericEnum<BrokerSuiteType>(this.SuiteType);
        }

        private void BindBrokerTempOptionList()
        {
            var tempOptionList = new[] {
                new KeyValuePair<string, string> ("<option name=\"CustomPdf\" value=\"true\" />", "Turn on Custom PDF")
                , new KeyValuePair<string, string> ("<option name=\"enablepmldu\" value=\"true\" />", "Turn on DU in PML")
                , new KeyValuePair<string, string> ("<option name=\"enablepmldo\" value=\"true\" />", "Turn on DO in PML")
                , new KeyValuePair<string, string> ("<option name=\"enableembeddedpmldo\" value=\"true\" />", "Turn on DO in Embedded PML")
                , new KeyValuePair<string, string> ("<option name=\"enableembeddedpmldu\" value=\"true\" />", "Turn on DU in Embedded PML")

                , new KeyValuePair<string, string> ("<option name=\"disableimportdodupml\" value=\"true\" />", "DISABLE import from DO/DU in PML")
                , new KeyValuePair<string, string> ("<option name=\"enablepmllp\" value=\"true\" />", "Turn on LP in PML")
                , new KeyValuePair<string, string> ("<option name=\"enablembeddedepmllp\" value=\"true\" />", "Turn on LP in Embedded PML")
                , new KeyValuePair<string, string>("<option name=\"disableimportfromlpainpml\" value=\"true\" />", "DISABLE import from LPA in PML")
                , new KeyValuePair<string, string> ("<option name=\"usingtestlpaccount\" value=\"true\" />", "Use test account for LP")
                , new KeyValuePair<string, string> ("<option name=\"enabletotalscorecard\" value=\"true\" />", "Turn on FHA TOTAL Scorecard Feature")
                , new KeyValuePair<string, string> ("<option name=\"disableedocnotifications\" value=\"true\" />", "DISABLE new edoc notification emails")
                , new KeyValuePair<string, string> ("<option name=\"useNoLeadPrefix\" value=\"true\" />", "Enable no lead prefix")
                , new KeyValuePair<string, string> ("<option name=\"enabledocmagicesign\" value=\"true\" />", "Enable ESigning for DM Doc Generation")
                , new KeyValuePair<string, string> ("<option name=\"enabledocmagiclinkwhenptm\" value=\"true\" />", "Enable DocMagic link when PTM billing")
                , new KeyValuePair<string, string> ("<option name=\"enablerenovationcheckboxinpml\" value=\"true\" />", "[OBSOLETE, use 'Enable Renovation Loan Support' checkbox in 'Features' section instead] Enable Renovation checkbox in PML")
                , new KeyValuePair<string, string> ("<option name=\"enablegfeandcocversioning\" value=\"true\" />", "Enable GFE and COC versioning")
                , new KeyValuePair<string, string> ("<option name=\"automatedclosingcost\" value=\"true\" />", "Enable Auto Closing Cost UI")
                , new KeyValuePair<string, string> ("<option name=\"enableloanrepurchase\" value=\"true\" />", "Enable Loan Repurchase Page")
                , new KeyValuePair<string, string> ("<option name=\"defaultfhaannualmibasetype\" value=\"loanamount\" />", "Use loan amount for FHA annual MI calculation. OBSOLETE (OPM 232490)")
                , new KeyValuePair<string, string> ("<option name=\"disablepmlduplicatesubmissionblocker\" value=\"true\" />", "Disable PML Duplicate Submission blocker")
                , new KeyValuePair<string, string> ("<option name=\"disabledocmagiclastdiscapr\" value=\"true\" />", "Disable Doc Magic Last Disclosed APR")
                , new KeyValuePair<string, string> ("<option name=\"enableexpandedleadeditordo\" value=\"true\" />", "Turn on DO in expanded lead editor")
                , new KeyValuePair<string, string> ("<option name=\"enableexpandedleadeditordu\" value=\"true\" />", "Turn on DU in expanded lead editor")
                , new KeyValuePair<string, string> ("<option name=\"enableexpandedleadeditorlp\" value=\"true\" />", "Turn on LP in expanded lead editor")
                , new KeyValuePair<string, string> ("<option name=\"enableexpandedleadeditortotal\" value=\"true\" />", "Turn on TOTAL in expanded lead editor")
                , new KeyValuePair<string, string> ("<option name=\"enableexpandedleadeditorcustomfields\" value=\"true\" />", "Turn on Custom Fields in expanded lead editor")
                , new KeyValuePair<string, string> ("<option name=\"EnableHomeEquityInLoanPurpose\" value=\"True\" />", "Add Home Equity to Loan Purpose (OPM 136658)")
                , new KeyValuePair<string, string> ("<option name=\"populatelatefeesfrompml_139391\" value=\"true\" />", "Populate Late Fees from PML (OPM 139391)")
                , new KeyValuePair<string, string> ("<option name=\"AllowHelocZeroInitialDraw_141171\" value=\"true\" />", "Allow HELOC 0.00 Initial Draw (OPM 141171)")
                , new KeyValuePair<string, string> ("<option name=\"EnableHELOC\" value=\"True\" />", "Allow HELOC Feature (OPM 141801)")
                , new KeyValuePair<string, string> ("<option name=\"UseSeamlessIntegrationForConformX\" value=\"true\" />", "Enable Seamless ConformX integration (OPM 138004)")
                , new KeyValuePair<string, string> ("<option name=\"IsEnableSeamlessDu\" value=\"true\" />", "Enable Seamless DU Submission")
                , new KeyValuePair<string, string> ("<option name=\"nonuwcanexcludeliab_130395\" value=\"true\" />", "Allow other roles to edit the 'Exclude from Underwriting' checkbox.")
                , new KeyValuePair<string, string> ("<option name=\"isemployeestartandterminationdenabled\" value=\"true\" />", "Enable employee start and termination dates")
                , new KeyValuePair<string, string> ("<option name=\"LenderHasMigratedToNewRoles_Case135241\" value=\"true\" />", "Lender has Migrated to New Roles (OPM 135241)")
                , new KeyValuePair<string, string> ("<option name=\"IsFundedDatePopulatedToHmdaActionTakenDisabled\" value =\"true\" />", "**DISABLE** population of Funded Date to HMDA Action Taken")
                , new KeyValuePair<string, string> ("<option name=\"IsCanceledDatePopulatedToHmdaActionTakenDisabled\" value =\"true\" />", "**DISABLE** population of Canceled Date to HMDA Action Taken")
                , new KeyValuePair<string, string> ("<option name=\"IsDeniedDatePopulatedToHmdaActionTaken\" value =\"true\" />", "***ENABLE*** population of Denied Date to HMDA Action Taken")
                , new KeyValuePair<string, string> ("<option name=\"UseIrregularFirstPeriodAprCalc_69485\" value =\"true\" />", "Use irregular first period APR calculation (case 69485)")
                , new KeyValuePair<string, string> ("<option name=\"automatedclosingcost\" value=\"true\" />", "Enable Closing Cost Automation")
                , new KeyValuePair<string, string> ("<option name=\"isenablehistoricalpricing\" value=\"true\" />", "Enable Historical Pricing")
                , new KeyValuePair<string, string> ("<option name=\"customlopricingpolicyfieldsenabled\" value=\"true\" />", "Enable Loan Officer Pricing Policy Fields")
                , new KeyValuePair<string, string> ("<option name=\"BlockExcelFilesInEDocs\" value=\"true\" />", "Block Excel Files In Edocs")
                , new KeyValuePair<string, string> ("<option name=\"EnableLqbNonCompliantDocumentSignatureStamp\" />", "Enable Non-Compliant EDoc Signature stamping (OPM 465194; to be removed)")
                , new KeyValuePair<string, string> ("<option name=\"UseCustomXsltCertificate\" value=\"true\" />", "Use Custom Xslt File")
                , new KeyValuePair<string, string> ("<option name=\"CopyInternalUsersToOfficalContactsByDefault\" value=\"true\" />", "Copy Internal Users To Official Contacts By Default")
                , new KeyValuePair<string, string> ("<option name=\"EnableAdditionalSection1000CustomFees\" value=\"true\" />", "Enable Custom HUD Lines 1010, 1011")
                , new KeyValuePair<string, string> ("<option name=\"Disable1003EditorInTPOPortal\" value=\"true\" />", "Disable 1003 Editor In Originator Portal")
                , new KeyValuePair<string, string> ("<option name=\"EnableLenderFeeBuyout\" value=\"true\" />", "Enable Lender Buyout OPM 149469")
                , new KeyValuePair<string, string> ("<option name=\"disablenameediting\" value=\"true\" />", "Don't allow users to edit their own names")
                , new KeyValuePair<string, string> ("<option name=\"AddLeadSourceDropdownToInternalQuickPricer_Case173059\" value=\"true\" />", "Add Lead Source Dropdown To Internal Quick Pricer")
                , new KeyValuePair<string, string> ("<option name=\"ishideprequaltextinpml\" value=\"true\" />", "Hide Pre-qual text in PML (OPM 172065)")
                , new KeyValuePair<string, string> ("<option name=\"RemoveMessageToLenderTPOPortal_Case171312\" value=\"true\" />", "Remove 'Message To Lender' from Originator Portal")
                , new KeyValuePair<string, string> ("<option name=\"RemoveMessageToLenderEmbeddedPML_Case215651\" value=\"true\" />", "Remove 'Message To Lender' from Embedded PML")
                , new KeyValuePair<string, string> ("<option name=\"HideAllowOnlineSigningOptionForFHA\" value=\"true\" />", "Hide Allow Online Signing Option in Doc Generation for FHA Loans (OPM 174489)")
                , new KeyValuePair<string, string> ("<option name=\"EnableActiveDirectoryAuthentication\" value=\"true\" />", "Enable Active Directory Authentication")
                , new KeyValuePair<string, string> ("<option name=\"ShowAllowOnlineSigningOptionForUSDA\" value=\"true\" />", "Show Allow Online Signing Option in Doc Generation for USDA Loans (OPM 174724)")
                , new KeyValuePair<string, string> ("<option name=\"createleadfilesfromquickpricertemplate\" value=\"true\" />", "Create Lead Files From QuickPricer Template")
                , new KeyValuePair<string, string>("<option name=\"DocumentValidationOptionT\" value=\"BlockOnMismatch\" />", "(OPM 174204) - Data Validation Mismatch Option. To display warning change value to WarningOnMismatch")
                , new KeyValuePair<string, string>("<option name=\"ShowCRMID\" value=\"true\">", "(OPM 169478) - Show CRM Now Lead ID In This Loan Info Page")
                , new KeyValuePair<string, string>("<option name=\"ShowGetRecordingChargesTransferTaxesAndTitleQuoteButton\" value=\"true\" />", "(OPM 175668) - Show the 'Get Recording Charges, Transfer Taxes, amd Title Quote...' Button in GFE")
                , new KeyValuePair<string, string>("<option name=\"AppendCompanyIDtoCompanyListInUserAdmin\" value=\"true\">", "(OPM 178044) - Append company ID to company list in user admin")
                , new KeyValuePair<string, string>("<option name=\"ExpandSubservicerContacts\" value=\"true\">", "(OPM 171421) - Expand Subservicer Contacts")
                , new KeyValuePair<string, string>("<option name=\"UseLegacyBehaviorForSubjPropStateInNewCPORTAL\" value=\"true\">", "(OPM 180532) - Use Legacy Behavior for SubjPropState In New CPORTAL")
                , new KeyValuePair<string, string>("<option name=\"EnabledBPMISinglePremium_Case115836\" value=\"true\" />", "(OPM 115836) - Enable BPMI Single Premium")
                , new KeyValuePair<string, string>("<option name=\"EnableBPMISplitPremium_Case115836\" value=\"true\" />", "(OPM 115836) - Enable BPMI Split Premium")
                , new KeyValuePair<string, string>("<option name=\"ExcludeCashDepositFromAssetTotals\" value=\"true\" />", "(OPM 168999) - Exclude Cash Deposit From Asset Totals")
                , new KeyValuePair<string, string>("<option name=\"DisplayNoteRateAndDtiOnCertificate\" value=\"true\" />", "(OPM 182199) - Display the note rate and DTI in the Loan Scenario on the PML certificate")
                , new KeyValuePair<string, string>("<option name=\"EnableSandbox\" value=\"true\" />", "(OPM 184058) - Interim sandbox mode solution")
                , new KeyValuePair<string, string>("<option name=\"RenameAlternativeDocType\" value=\"true\" />", "Rename Alt 12 month doc type to Alt")
                , new KeyValuePair<string, string>("<option name=\"EnableTeamsUI\" value=\"true\" />", "Show the Teams UI")
                , new KeyValuePair<string, string>("<option name=\"EnableDropboxBarcodeScanning\" value=\"true\" />", "Enable Dropbox Barcode Scanning")
                , new KeyValuePair<string, string>("<option name=\"IsEnableHelocToggleInQP2\" value=\"true\" />", "Enable Heloc Toggle in QP 2.0")
                , new KeyValuePair<string, string>("<option name=\"IsEnableLienTToggleInQP2\" value=\"true\" />", "Enable Lien Type Toggle in QP 2.0")
                //, new KeyValuePair<string, string>("<option name=\"IsEnableLoanPurposeTRadioInQP2\" value=\"true\" />", "Enable Loan Purpose Radio in QP 2.0") // replaced by ConstStage.AllowLoanPurposeToggleInQP2
                , new KeyValuePair<string, string>("<option name=\"UseRespaForNonRetailInitialDisclosures\" value=\"true\" />", "Use RESPA for Non-Retail Initial Disclosure Trigger.")
                , new KeyValuePair<string, string>("<option name=\"ReqDocCheckDAndRespaForNonRetailInitialDisclosures\" value=\"true\" />", "Require existence of Document Check Date in addition to RESPA for Non-Retail Initial Disclosure Trigger. (REQUIRES ENABLING 'Use RESPA for Non-Retail Initial Disclosure Trigger')")
                , new KeyValuePair<string, string>("<option name=\"DisableFeeEditorInTPOPortalCase183419\" value=\"true\" />", "Disable Fee Editor in Originator Portal")
                , new KeyValuePair<string, string>("<option name=\"ApplyCompWhenDocCheckDateEntered\" value=\"true\" />", "(OPM 184828) Write the comp plan to the loan file when Doc Check date is entered instead of submission.")
                , new KeyValuePair<string, string>("<option name=\"ApplyCompWhenRespa6Collected\" value=\"true\" />", "(OPM 457457) Write the comp plan to the loan file when the RESPA 6 have been collected (in addition to any other options).")
                , new KeyValuePair<string, string>("<option name=\"ApplyCompAtRegistration\" value=\"false\" />", "(OPM 457457) Disable writing the comp plan to the loan file at submission.")
                , new KeyValuePair<string, string>("<option name=\"ApplyCompAtRateLock\" value=\"false\" />", "(OPM 457457) Disable writing the comp plan to the loan file at rate lock.")
                , new KeyValuePair<string, string>("<option name=\"MigrateToLegacyButMigratedOnPricing\" value=\"true\" />", "Migrate loan to LegacyByMigrated mode (instead of ClosingCostFee2015 mode) when pricing is run on the loan.")
                , new KeyValuePair<string, string>("<option name=\"AssumePmlQmEligibleForGse\" value=\"true\" />", "Assume eligible for GSE purchase for QM.")
                , new KeyValuePair<string, string>("<option name=\"ExposeBulkMiniBulkCorrProcessTypeEnums\" value=\"true\" />", "Expose Bulk & Mini-Bulk Correspondent Process Types.")
                , new KeyValuePair<string, string>("<option name=\"Export_sHas1stTimeBuyer_as_FTHB_for_ULDD\" value=\"true\" />", "Use sHas1stTimeBuyer instead of aDecPastOwnership in the ULDD export")
                , new KeyValuePair<string, string>("<option name=\"IsSetAllFieldsOnPoolAssignmentByDefault\" value=\"true\" />", "Set all fields on pool assignment by default.")
                , new KeyValuePair<string, string>("<option name=\"EnableDocuTechAutoDisclosure\" value=\"true\" />", "Enable DocuTech Auto-Disclosure (FirstTech)")
                , new KeyValuePair<string, string>("<option name=\"HideServicesDashboardInTPOPortal\" value=\"true\" />", "Hide the Services Dashboard In the Originator Portal")
                , new KeyValuePair<string, string>("<option name=\"AllowManualApr\" value=\"true\" />", "Allow Manual APR Entry")
                , new KeyValuePair<string, string>("<option name=\"DisplayQMResultsForCorrespondentFiles\" value=\"true\" />", "Display QM Results in PML and Loan Certificate for Correspondent Files")
                , new KeyValuePair<string, string>("<option name=\"IsDisableAutoCalcOfTexas50a6\" value=\"true\" />", "Disable Auto-Calculation of Texas 50a6 Checkbox")
                , new KeyValuePair<string, string>("<option name=\"EnableDeletedPageEDocCreation\" value=\"true\" />", "Enable Creating EDocs Out of Deleted Pages")
                , new KeyValuePair<string, string>("<option name=\"IsUsing2015GFEUITPO\" value=\"true\" />", "Enable the 2015 GFE UI for Originator Portal Users")
                , new KeyValuePair<string, string>("<option name=\"AllowQMEditTPO\" value=\"true\" />", "Allow Originator Portal Users to edit the QM")
                , new KeyValuePair<string, string>("<option name=\"DisableGfe2015\" value=\"true\" />", "Disable the 2015 GFE UI")
                , new KeyValuePair<string, string>("<option name=\"AllowCitrixVPrinterDownload\" value=\"true\" />", "Enable the Citrix VPrinter in the Software Download window")
                , new KeyValuePair<string, string>("<option name=\"UseUpdatedCompanyAndPersonTemplateForDRIVE\" value=\"true\" />", "Enable 2017 DRIVE participant mappings (Case 454289)")
                , new KeyValuePair<string, string>("<option name=\"EnableBigLoanPage\" value=\"true\"/>", "Enable Big Loan Page")
                , new KeyValuePair<string, string>("<option name=\"UseMergeGroupNameInsteadOfLoanProgramNameOnPrintedIFW\" value=\"true\" />", "Use the Merge Group Name instead of the Loan Program Name when printing the IFW.")
                , new KeyValuePair<string, string>("<option name=\"EnableNewFeeService\" value=\"true\" />", "Enable New Fee Service UI for use with New GFE closing cost fees.")
                , new KeyValuePair<string, string>("<option name=\"ShowEmployeeResourcesLinkInLeadEditor\" value=\"true\" />", "Show Employee Resources link in Lead Editor.")
                , new KeyValuePair<string, string>("<option name=\"IsBarcodeUploadScriptEnabled\" value=\"true\" />", "Enable Barcode Upload Feature Permission")
                , new KeyValuePair<string, string>("<option name=\"EnableLenderCreditsWhenInLegacyClosingCostMode\" value=\"true\" />","Enable Lender Credits in Legacy Closing Cost Mode")
                , new KeyValuePair<string, string>("<option name=\"ComplianceEaseAllowOverridePaidToForHOEPAFeeTest\" value=\"true\" />", "ComplianceEase: Override Section G and Hud 902-903 fee paid to Lender/Broker and set to Default instead. New Data layer only.")
                , new KeyValuePair<string, string>("<option name=\"DontLink1003ToAdjustmentsOnPull\" value=\"true\" />", "Allows data to be migrated from 1003 Details to Adjustments, but does not sync them afterwords.")
                , new KeyValuePair<string, string>("<option name=\"AllowMoveFilesBackToLegacyWithArchives\" value=\"true\" />", "(OPM 229468) Allow moving files back to Legacy mode while having migrated archives.")
                , new KeyValuePair<string, string>("<option name=\"UseCustomField60AsEsignNotificationList\" value=\"true\" />", "(OPM 232719) Allow users to overwrite the ESign notification list using custom field 60.")
                , new KeyValuePair<string, string>("<option name=\"EnableStyleEditor\" value=\"true\" />", "Enable the Style Editor for all users?")
                , new KeyValuePair<string, string>("<option name=\"DisableAutoPopulationOfCdReceivedDate\" value=\"true\" />", "Disable auto-population of CD received date.")
                , new KeyValuePair<string, string>("<option name=\"IsResetSystemFeesOnRemoval\" value=\"false\" />", "Prevent resetting system fee properties from the Fee Setup when removed from the Set.")
                , new KeyValuePair<string, string>("<option name=\"EnablePDDExport\" value=\"true\" />", "Enable the new PDD export format for GNMA.")
                , new KeyValuePair<string, string>("<option name=\"EnableCashToCloseAndReservesDebugInPML\" value=\"true\" />", "CHECK WITH DAVID W BEFORE USE: (OPM 233679) Show the calculations for Cash to Close and Reserve Months in PML")
                , new KeyValuePair<string, string>("<option name=\"DisplayBreakEvenMthsInsteadOfReserveMthsInPML2ForNonPurLoans\" value=\"true\" />", "(OPM 233679) Show Break-even months in PML 2.0 UI for non-Purchase loans (Legacy Behavior)")
                , new KeyValuePair<string, string>("<option name=\"SymitarIntegration\" DeviceNumber=\"\" DeviceType=\"\" VirtualCardPrefix=\"\" IpAddress=\"\" Ports=\"\" BatchExportReportName=\"\" AllowImportFromSymitar=\"true\" AllowExportToSymitar=\"true\"/>", "Enables Symitar integration with the specified settings. Ports are comma separated.")
                , new KeyValuePair<string, string>("<option name=\"Mismo33SidebarLinkDocumentVendor\" value=\"Simplifile\" />", "(OPM 242176) Sets the vendor target for the \"Services/Export/To MISMO 3.3\" sidebar link in the loan editor.")
                , new KeyValuePair<string, string>("<option name=\"TitleInterfaceTitleRecordingOnly\" value=\"\" />", "(Opm 244338) Declares a list of states for which the title quote should request title + recording fees and not closing/escrow fees. Use state acronyms delimited by semicolons.")
                , new KeyValuePair<string, string>("<option name=\"EnableEnhancedTitleQuotes\" value=\"true\" />", "Enable ordering title/closing/recording quptes from different Title Vendors.")
                , new KeyValuePair<string, string>("<option name=\"DisableNewTitleFramework\" value=\"true\" />", "(OPM 457453) Disables the Title Framework.")
                , new KeyValuePair<string, string>("<option name=\"EnabledLONBranchChannels\" value=\"\" />", "(OPM 242336) Limits LON export to specific branch channels. Valid values (comma separated): " + string.Join(", ", Enum.GetNames(typeof(E_BranchChannelT))))
                , new KeyValuePair<string, string>("<option name=\"Force2016ComboAndProrationUpdatesTo1003Details\" value=\"true\" />", "(Opm 237078) Create new files with 2016 combo / proration calculation updates enabled.")
                , new KeyValuePair<string, string>("<option name=\"AllowShowingZeroAmountHousingExp\" value=\"true\" />", "(OPM235669) Will allow showing the 'Other Housing' expenses despite having $0 annual amount. Only in CC2015 mode.")
                , new KeyValuePair<string, string>("<option name=\"IsEnableMobileDeviceRegistrationTPO\" value=\"true\" />", "(OPM 248906) Enable Mobile Device Registration for TPO Users. Requires 'Enable authenticating into the LQB Mobile App' to be checked.")
                , new KeyValuePair<string, string>("<option name=\"DoNotUnlockDOTFieldsDuringPMLSubmission\" value=\"true\" />", "(OPM 251650) Prevent unlocking DOT fields during PML submission regardless of branch channel.")
                , new KeyValuePair<string, string>("<option name=\"AllowUserMfaToBeDisabled\" value=\"true\" />", "(Opm 248905) Allow user-level MFA setting to be disabled.")
                , new KeyValuePair<string, string>("<option name=\"DisableVPrinterOCRFrameworkCapture\" value=\"true\" />", "(Opm 314910) Disable VPrinter OCR Framework Capture.")
                , new KeyValuePair<string, string>("<option name=\"DisableArchiveHousingExpenseRefresh\" value=\"true\" />", "Disables the refresh of the Housing Expense JSON content with live data on archive creation and refresh")
                , new KeyValuePair<string, string>("<option name=\"EnableAdvancedFilterOptionsForPriceEngineResults\" EmployeeGroup=\"\" BranchGroup=\"\" value=\"true\" />", "(OPM 175724) Enable Advanced Filter Options for Price Engine Results. For B Users, the user must be in the listed Employee or Branch groups. For P Users, they must be in the listed Branch Groups.")
                , new KeyValuePair<string, string>("<option name=\"EnablePhase1HistoricalPricingAndUIEnhancements\" EmployeeGroup=\"GroupName\" FeeService=\"Current\" PriceGroups=\"Historical\" LenderFees=\"Historical\" />", "(Opm 365562) Enable Phase 1 Historical Pricing for the given EmployeeGroup. An empty EmployeeGroup attribute will enable it for all.")
                , new KeyValuePair<string, string>("<option name=\"EnablePhase1InternalPricerAndCheckEligibilityUIEnhancements\" EmployeeGroup=\"GroupName\" />", "(Opm 365562) Enable Internal Pricer and Check Eligibility UI updates for the given EmployeeGroup. An empty EmployeeGroup attribute will enable it for all.")
                , new KeyValuePair<string, string>("<option name=\"DisableESignNotificationEmailFromDocumentFrameworkRequests\" value=\"true\" />", "Disable hardcoded DocMagic Esign notification email during document generation.")
                , new KeyValuePair<string, string>("<option name=\"IncludeLenderPaidOriginatorCompensationInMismo33Payload\" value=\"true\" />", "Include Lender Paid Originator Compensation in Mismo 3.3 Payload")
                , new KeyValuePair<string, string>("<option name=\"IncludeLiabilitiesPaidBeforeClosingInDocMagicMismo33Payload\"/>", "Include liabilities marked as paid before closing in Mismo 3.3 payload for DocMagic (OPM 453306)")
                , new KeyValuePair<string, string>("<option name=\"DisableNewTPODisclosurePage\" OCGroupExclusion=\"\" value=\"true\" />", "(OPM 449638) Disable the Originator portal 'Disclosures' page. To exclude a specific OC group, enter the name of the group as the value for the 'OCGroupExclusion' attribute.")
                , new KeyValuePair<string, string>("<option name=\"DisableEDocsMIQuoteAutoObsoleteAndUnObsolete\" value=\"true\" />", "(OPM 252290) Disable EDocs MI Quote Auto-Obsolete And UnObsolete.")
                , new KeyValuePair<string, string>("<option name=\"DisableLendingLicenseStips\" value=\"true\" />", "(OPM 179515) Do not populate lending license stipulations in pricing.")
                , new KeyValuePair<string, string>("<option name=\"DisableCreditStips\" value=\"true\" />", "(OPM 179515) Do not populate credit report stipulations in pricing.")
                , new KeyValuePair<string, string>("<option name=\"HideUploadAndFaxLinksInTpoLoanNavigationEdocs\" value=\"true\" />", "(OPM 451666) Hide the 'Upload Docs' and 'Fax Docs' links on the Originator Portal Loan Navigation 'E-Docs' page.")
                , new KeyValuePair<string, string>("<option name=\"TpoLoanNavigationHeaderLinkAlignmentSettings\" CenterAlignment=\"\" RightAlignment=\"\" />", "(OPM 451666) Sets the alignment of generic framework vendor links in the Originator Portal loan navigation header by page name. Page names are comma separated. Available page names are: " + string.Join(", ", ConstApp.TpoLoanNavigationPageNames))
                , new KeyValuePair<string, string>("<option name=\"DisableInitialDisclosuresButtonOnTpoDisclosuresPage\" OCGroupExclusion=\"\" value=\"true\" />", "(OPM 449715) Disable the 'Order Initial Disclosures' button on the Originator portal 'Disclosures' page. To exclude a specific OC group, enter the name of the group as the value for the 'OCGroupExclusion' attribute.")
                , new KeyValuePair<string, string>("<option name=\"TpoInitialDisclosureDocumentVendor\" vendorname=\"\" packagenumber=\"\" enableedisclosure=\"\" enableesign=\"\" />", "(OPM 449715) Sets the document vendor used for initial disclosures in the Originator Portal pipeline. Values are case insensitive. The 'enable' attributes accept 'Yes' or 'No'. Terminology is package number for DocMagic, package ID for other vendors.")
                , new KeyValuePair<string, string>("<option name=\"EnableSettingInterviewDateForConsumerPortalSubmission\" value=\"true\" />", "(OPM 455566) Enable Setting Interview Date for Consumer Portal Submission")
                , new KeyValuePair<string, string>("<option name=\"EnablePmlUiResults3\" EmployeeGroup=\"EmptyGroup\" OCGroup=\"EmptyGroup\" ForcePML3=\"false\" />", "Enables the 3rd version of the UI for the given EmployeeGroup. An empty EmployeeGroup or OCGroup attribute will enable it for all, along with setting ForcePML3 to true. Group names are case sensitive.")
                , new KeyValuePair<string, string>("<option name=\"Case455957_SetRESPA6DateForNoIncomeLoans\" value=\"true\" />", "(OPM 455957) Set RESPA 6 Date for No Income Loans")
                , new KeyValuePair<string, string>("<option name=\"HideStatusColumnInTpoEdocs\" value=\"true\" />", "(OPM 451666) Hide the 'Status' column on the Originator portal 'E-docs' page.")
                , new KeyValuePair<string, string>("<option name=\"EnableSubFees\" value=\"true\" />", "Enable Sub Fees for First American Fee Import.")
                , new KeyValuePair<string, string>("<option name=\"UsePOT61ToPullFirstAmericanLocalRefinanceRates\" value=\"true\" />", "(OPM 456467) Enable to pull local refinance rates from First American when Account ID (Client ID) > 1 is already being required to pull client-specific equity rates.")
                , new KeyValuePair<string, string>("<option name=\"EnableSendingDocuTechMISMO33\" vendorname=\"\" />", "(OPM 456143) Send MISMO 3.3 payloads to DocuTech instead of MISMO 2.6.")
                , new KeyValuePair<string, string>("<option name=\"EnableNewSeamlessDu\" OCGroupWithFallback=\"\" EmployeeGroupWithFallback=\"\" value=\"true\" />", "(OPM 126253) Enable Seamless DU (new) and specify OC and Employee groups for fallback (both seamless and non-seamless).")
                , new KeyValuePair<string, string>("<option name=\"IsEnableVOXConfiguration\" value=\"true\" />", "(OPM 450156) Enables the lender-level Services tab for VOX Configuration.")
                , new KeyValuePair<string, string>("<option name=\"LcrHideHorizon\" value=\"0\" />", "Hide the Horizon Rows for Combo Scenarios in the Loan Comparison Report. Set the value attribute to the following to hide for which files the section is hidden for: 0 for Both, 1 for Lead, and 2 for Loan")
                , new KeyValuePair<string, string>("<option name=\"UsingLegacyDUCredentials\" value=\"true\" />", "(OPM 452915) Is using old DU Login credentials.")
                , new KeyValuePair<string, string>("<option name=\"LcrHideEstimatedDTI\" value=\"0\" />", "Hide the Estimated DTI section in the LCR. Set the value attribute to the following to hide for which files the section is hidden for: 0 for Both, 1 for Lead, and 2 for Loan")
                , new KeyValuePair<string, string>("<option name=\"LcrHide1stLienLifetimeFinanceCharge\" value=\"0\" />", "Hide the 1st Lien Lifetime Finance Charge section in the LCR. Set the value attribute to the following to hide for which files the section is hidden for: 0 for Both, 1 for Lead, and 2 for Loan")
                , new KeyValuePair<string, string>("<option name=\"LcrHide2ndLienLifetimeFinanceCharge\" value=\"0\" />", "Hide the 2nd Lien Lifetime Finance Charge section in the LCR. Set the value attribute to the following to hide for which files the section is hidden for: 0 for Both, 1 for Lead, and 2 for Loan")
                , new KeyValuePair<string, string>("<option name=\"LcrHideEstimatedReserves\" value=\"0\" />", "Hide the Estimated Reserves section in the lcr. Set the value attribute to the following to hide for which files the section is hidden for: 0 for Both, 1 for Lead, and 2 for Loan")
                , new KeyValuePair<string, string>("<option name=\"AllowAnyXmlToValidateAsUcd\" value=\"true\" />", "When uploading a generic EDoc, the validation will pass any XML instead of requiring MISMO 3.3 compliance.")
                , new KeyValuePair<string, string>(@"<option name=""ExcludeZeroAdjustmentsForDocGenInLoXml"" value=""true"" />", "")
                , new KeyValuePair<string, string>("<option name=\"HideRateLockHistoryGridInTPOPortal\" value=\"true\" />", "(OPM 462963) Hide the 'Rate Lock History' table on the Originator portal 'Rate Lock' page.")
                , new KeyValuePair<string, string>("<option name=\"AutoExtensionForceWorstOptionForLockExtensionDateTies\" value=\"true\" />", "(OPM 463798) Force the worst option to be selected when the new Lock Extension Dates are tied.")
                , new KeyValuePair<string, string>("<option name=\"DefaultBorrPaidOrigCompSourceToTotalLoanAmount\" value=\"true\" />", "(OPM 462287) Defaults the base type for borrower-paid originator compensation to 'Total Loan Amount'.")
                , new KeyValuePair<string, string>("<option name=\"DisablePageDisplayChangesViaWorkflow\" value=\"true\" />", "(OPM 457374) Prevents changes to a page's UI (disabling fields, etc.) based on workflow rule evaluation.")
                , new KeyValuePair<string, string>($"<option name=\"EnableNewSeamlessLPA\" OCGroupWithFallback=\"\" EmployeeGroupWithFallback=\"\" value=\"true\" />", "(OPM 463482) Enable Seamless LPA and enable Non-Seamless LPA fallback")
                , new KeyValuePair<string, string>("<option name=\"AddUliTo1003Pages\" value=\"true\" />", "(OPM 464735) Append the ULI number to the Loan Number in the 1003 page footers.")
                , new KeyValuePair<string, string>("<option name=\"UseClearToCloseStatusInsteadOfApprovedForLoanCanceledActionTaken\" value=\"true\" />", "Use Clear To Close status instead of Approved status for Loan Canceled Action Taken.")
                , new KeyValuePair<string, string>("<option name=\"DisableTpoLoanEditorSaveButton\" value=\"true\" ExceptForThisEmployeeGroup=\"\" ExceptForThisOcGroup=\"\" />", "(OPM 463731) Disable Originator Portal loan editor Save button.")
                , new KeyValuePair<string, string>("<option name=\"EnableCustomWordForms\" value=\"true\" />", "(OPM 465484)(Obselete) Enable Custom Word Forms for a broker.  DO NOT enable this for new brokers.")
                , new KeyValuePair<string, string> ("<option name=\"isGenerateDailyIISReport\" value=\"true\" />", "Turn on Generate Daily IIS Report")
                , new KeyValuePair<string, string>("<option name=\"ExposeClientFacingWorkflowConfiguration\" value=\"true\" />", "Expose client-facing workflow configuration UI.")
                , new KeyValuePair<string, string> ("<option name=\"EnableULDDPhase3\" forGseTarget=\"\" part=\"\" value=\"true\"/>", "Enable ULDD Phase 3 for the given GSE and part (1 or 2).")
                , new KeyValuePair<string, string> ("<option name=\"EnableULDDPhase3ForTestFiles\" forGseTarget=\"\" part=\"\" value=\"true\"/>", "Enable ULDD Phase 3 for test loans with the given GSE and part (1 or 2).")
                , new KeyValuePair<string, string>("<option name=\"IsCrossBrowserEnabled\" EmployeeGroup=\"EmptyGroup\" />", "Enables the broker to log in with modern browsers. An empty EmployeeGroup attribute will enable it for all. Group names are case sensitive.")
                , new KeyValuePair<string, string>("<option name=\"IsAllowOldIe\" value=\"true\" />", "Allow users to log in with versions of IE 10 and lower.")
                , new KeyValuePair<string, string>("<option name=\"ExportFirstNonBoughtDownPaymentAsInitialPaymentInUldd\" value=\"true\" />", "Export first non bought-down payment as the initial principal/interest payment in ULDD.")
                , new KeyValuePair<string, string>("<option name=\"UsingLegacyFindingsForSeamlessDU\" value=\"true\" />", "Use the old findings HTML for Seamless DU and convert HTML to PDF for the findings doc, rather than the DU-provided PDF. (Case 473888)")
                , new KeyValuePair<string, string>("<option name=\"EnableBorrowerLevelDisclosureTracking\" value=\"true\" />", "(OPM 472356) Enable borrower-level disclosure tracking.")
                , new KeyValuePair<string, string>("<option name=\"UsingLegacyDocTypes\" value=\"true\" />", "(OPM 236747) Allow using legacy documentation types.")
                , new KeyValuePair<string, string>("<option name=\"HideEnhancedDocTypes\" value=\"true\" />", "(OPM 236747) Hide enhanced documentation types.")
                , new KeyValuePair<string, string> ("<option name=\"LPQCodes\" OrgCode=\"\" LenderCode=\"\" />", "LPQ Integration codes. Need to pull these from LPQ account.")
                , new KeyValuePair<string, string>("<option name=\"HideStatusAndAgentsPageInOriginatorPortal\" value=\"true\" />","(OPM 477146) Hides the Status and Agents page in the Originator portal.")
                , new KeyValuePair<string, string>("<option name=\"HideStatusSubmissionButtonsInOriginatorPortal\" value=\"true\" />","(OPM 477146) Hides status submission buttons like 'Submit to Document Check' in the Originator portal.")
                , new KeyValuePair<string, string>("<option name=\"HideRateLockPageLoanStatusInOriginatorPortal\" value=\"true\" />","(OPM 477146) Hides the loan status row on the 'Rate Lock' page in the Originator portal.")
                , new KeyValuePair<string, string>("<option name=\"EnableNonQmPortal\" EmployeeGroup=\"\" OCGroup=\"\"/>", "Enables the Non-QM for Originator Portal. An empty EmployeeGroup or OCGroup attribute will enable it for all. Group names are case sensitive.")
                , new KeyValuePair<string, string>("<option name=\"HideEdocDropboxInSettingsPortlet\" value=\"true\" />", "(OPM 477227) Hides the 'EDocs Dropbox' link in the 'Your Settings' section of the pipeline if the user has no documents in their dropbox.")
            };

            m_tempOptionsListRepeater.DataSource = tempOptionList;
            m_tempOptionsListRepeater.DataBind();
        }

        private void BindCRA(DropDownList ddl, IEnumerable<CRA> list) 
        {
            ddl.Items.Add(new ListItem("", Guid.Empty.ToString()));
            foreach (CRA o in list) 
            {
                ddl.Items.Add(new ListItem(o.VendorName, o.ID.ToString()));
            }
        }

        /// <summary>
        /// Return this page's required permissions set.  This
        /// test occurs with each page initialization.
        /// </summary>
        /// <returns>
        /// Array of required permissions.
        /// </returns>

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            if( RequestHelper.GetSafeQueryString( "cmd" ).ToLower() == "add" )
            {
                return new E_InternalUserPermissions[]
                {
                    E_InternalUserPermissions.CreateBroker
                };
            }

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            this.m_ocrService = new OCRService(PrincipalFactory.CurrentPrincipal);

            if (!Visible)
            {
                return;
            }

            if (!IsPostBack)
            {
                LoadData();
                ClientScript.RegisterHiddenField("BrokerStatus", m_brokerStatus);
                
                if (m_brokerId == Guid.Empty)
                {
                    Tools.SetDropDownListValue(ShowCredcoOptionT, E_ShowCredcoOptionT.DisplayBoth);
                    m_databaseList.Visible = true;
                    m_database.Visible = false;
                }
                else
                {
                    m_databaseList.Visible = false;
                    m_database.Visible = true;
                }
                panSubOptions.Visible = ( m_brokerId != Guid.Empty );
            }
            else
            {
                FormUtils.UCaseWebTextBox(this) ;
                panSubOptions.Visible = true ;
                 ClientScript.RegisterHiddenField("BrokerStatus", Request.Form["BrokerStatus"]);
            }

            // Check for embedded command to respond to.  Right
            // now, we're looking for becoming a given user.
            //
            // 4/29/2004 kb - This ui now pops up in its own window, so
            // handling events like become user screws up by becoming
            // the user within the popup window.  We need to close the
            // broker user list window and forward the event to the
            // base, broker listing page.

            if( Request[ "__EVENTARGUMENT" ] != null )
            {
                string[] command = Request[ "__EVENTARGUMENT" ].Split( '*' );

                if( command.Length == 2 )
                {
                    HandleCommand( command[ 0 ] , command[ 1 ].Split( ':' ) );
                }
            }

           
        }

        private void LoadAddNewDefaultValues()
        {
            //TODO - make a set of defaults that need to be set when new broker is created
            BrokerDB brokerDB = new BrokerDB();
            this.OCRVendors.SelectedValue = Guid.Empty.ToString();
            m_AllowPmlOrderNewCreditReport.Checked = brokerDB.AllowPmlUserOrderNewCreditReport;

            foreach (ListItem item in m_databaseList.Items)
            {
                if(item.Text.Contains("[02]"))
                {
                    item.Selected = true;
                    break;
                }
            }

            if(brokerDB.RoundUpLpeFee)
                m_roundUpLpeFee.Checked = true;
            else
                m_dontRoundUpLpeFee.Checked = true;

            m_OriginatorCompensationMigrationDate.Text = "4/11/2011";
            m_IsNewPmlUIEnabled_Yes.Checked = true;
            m_IsForceLeadToLoanNewLoanNumber.Checked = true;
            m_IsEditLeadsInFullLoanEditor.Checked = true;
            m_IsUseNewCondition.Checked = true;
            m_IsUseNewTaskSystemStaticConditionIds.Checked = true;
            m_IsImportDu1003ByDefault.Checked = false;
            m_IsImportDuFindingsByDefault.Checked = true;
            m_IsImportDuCreditReportByDefault.Checked = false;
            m_IsImportDuFindings.Checked = true;
            m_IsEnabledGfeFeeService.Checked = true;

            m_IsApplyPricingEngineRoundingAfterLoComp.Checked = true;

            m_isBestPriceEnabled.Checked = true;
            m_IsAlwaysUseBestPrice.Checked = true;

            m_roundUpLpeFee.Checked = true;
            m_roundUpLpeFeeInterval.Text = BrokerDB.ToDecimalString(brokerDB.RoundUpLpeFeeInterval);
            m_IsEnabledLockPeriodDropdown.Checked = true;

            this.EnableLqbMobileApp.Checked = true;

            // various PMI provider rankings are set to 1.
            archRanking.Value = "1";
            m_EssentRanking.Value = "1";
            m_GenworthRanking.Value = "1";
            m_MgicRanking.Value = "1";
            m_RadianRanking.Value = "1";
            m_NationalRanking.Value = "1";

            trNamingPattern.Visible = false;
            trLeadNamingPattern.Visible = false;
            // 12/2/2013 gf - Assume new lenders are deployed utilizing the new roles.
            TempOptionXmlContent.Text = "<option name=\"LenderHasMigratedToNewRoles_Case135241\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"useperratepricing\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"enablegfeandcocversioning\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"EnabledBPMISinglePremium_Case115836\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"EnableBPMISplitPremium_Case115836\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"EnableNewFeeService\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"CopyInternalUsersToOfficalContactsByDefault\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"IsTestingNonDestructiveEdocs\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"IsTestingAmazonEdocs\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"EnableCashToCloseAndReservesDebugInPML\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"EnableAdvancedFilterOptionsForPriceEngineResults\" EmployeeGroup=\"\" BranchGroup=\"\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"enabledocmagicesign\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"EnableSandbox\" value=\"true\" />"
                + Environment.NewLine + "<option name=\"enableexpandedleadeditorcustomfields\" value=\"true\" />"
                // tdsk: figure out how to add this back in for new brokers... need to remove it then add it back after we give them the group.
                // + Environment.NewLine + "<option name=\"EnablePhase1HistoricalPricingAndUIEnhancements\" EmployeeGroup=\"Allow accessing historical pricing?\" FeeService=\"Current\" PriceGroups=\"Historical\" LenderFees=\"Historical\" />"
                ;

            this.m_SandboxScratchLOXml.Text =
@"<LOXmlFormat version=""1.0"">
  <loan>
    <field id=""sStatusTLckd"">1</field>
    <field id=""sStatusT"">0</field>
    <field id=""sNoteIRSubmitted"">0</field>
    <field id=""BreakRateLock"" reason=""For Sandbox Mode.""></field>
    <field id=""sRateLockStatusT"">0</field>
    <field id=""sDuCaseId"">Sandbox File: DO NOT RUN DU</field>
    <field id=""sAgencyCaseNum""></field>
    <field id=""sFreddieLoanId"">Sandbox File: DO NOT RUN LP</field>
  </loan>
</LOXmlFormat>";

            this.m_ddlXmlConfig.Text = 
@"<?xml version=""1.0"" encoding=""utf-16""?> 
<fields> 
    <field Name=""sProd3rdPartyUwResultT"" NameCount=""0"" >
        <hide Value=""7"" />
        <hide Value=""8"" />
        <hide Value=""9"" />
    </field>
</fields>";

            Tools.SetRadioButtonListValue(m_rateLockExpirationWeekendHolidayBehavior, E_RateLockExpirationWeekendHolidayBehavior.PrecedingBusinessDay);

            // OPM 220493
            this.m_IsForceLeadToLoanTemplate.Checked = true;
            this.m_BorrPdCompMayNotExceedLenderPdComp.Checked = true;
            this.m_CalculateclosingCostInPML.Checked = true;
            this.m_ApplyClosingCostsToGfeOnSubmission.Checked = true;
            this.m_IsEDocsEnabled.Checked = true;
            this.m_faxShared.Checked = true;

            this.m_AvailableLockPeriod_1.Text = "15";
            this.m_AvailableLockPeriod_2.Text = "30";
            this.m_AvailableLockPeriod_3.Text = "45";
            this.m_AvailableLockPeriod_4.Text = "60";
            this.m_AvailableLockPeriod_5.Text = "75";
            this.m_AvailableLockPeriod_6.Text = "90";

            Tools.SetDropDownListValue(this.m_minimumDefaultClosingCostDataSet, E_sClosingCostFeeVersionT.ClosingCostFee2015);
            Tools.SetDropDownListValue(this.m_EDocsPmlEnabledStatus, "1");

            // OPM 246472
            this.m_ForceTemplateOnImport.Checked = true;
            this.m_ShowProcessorInPMLCertificate.Checked = true;
            this.m_IsQuickPriceEnabled.Checked = true;
            this.m_enableFeaturePriceMyLoanCB.Checked = true;

            this.m_isShowPriceGroupInEmbeddedPml.Checked = false;
            this.m_ShowUnderwriterInPMLCertificate.Checked = false;
            this.m_ShowJuniorUnderwriterInPMLCertificate.Checked = false;
            this.m_isPmlPointImportAllowed.Checked = false;

            this.m_quickPricerNoResultMessage.Text = "No eligible program found. Please contact your AE for more information.";
            this.m_quickPricerDisclaimerAtResult.Text = "Rate and Price may change. Please contact your AE for more information.";

            Tools.SetDropDownListValue(this.m_Pml2AsQuickPricerMode, E_Pml2AsQuickPricerMode.Enabled);

            Tools.SetDropDownListValue(this.m_SelectedNewLoanEditorUIPermissionType, E_PermissionType.HardDisable);

            // We'll call this price engine change handler
            // when the page loads to ensure the UI syncs
            // after enabling both PML and QP.
            this.ClientScript.RegisterStartupScript(
                typeof(BrokerEdit),
                Guid.NewGuid().ToString("N"),
                "onPricingEngineEnabledChange(true);",
                addScriptTags: true);

            this.PmlLoanTemplateId.Enabled = false;
            this.PmlLoanTemplateId.Items.Clear();
            this.PmlLoanTemplateId.Items.Add(new ListItem("PML Default Template", Guid.Empty.ToString()));

            this.m_quickPricerTemplateId.Enabled = false;
            this.m_quickPricerTemplateId.Items.Clear();
            this.m_quickPricerTemplateId.Items.Add(new ListItem("QuickPricer Template", Guid.Empty.ToString()));

            this.IsOptInMultiFactorAuthentication.Checked = true;
            this.IsOptOutMultiFactorAuthentication.Checked = false;
            this.IsEnableMultiFactorAuthentication.Checked = true;
            this.IsEnableMultiFactorAuthenticationTPO.Checked = true;
            this.AllowDeviceRegistrationViaAuthCodePage.Checked = true;


        }

        public void LoadData()
        {

            var vendors = this.m_ocrService.GetVendors();
            this.OCRVendors.Items.Clear();
            this.OCRVendors.Items.Add(new ListItem("none", Guid.Empty.ToString()));

            foreach(var vendor in vendors)
            {
                this.OCRVendors.Items.Add(new ListItem(vendor.DisplayName, vendor.Id.ToString()));
            }

            if( m_brokerId == Guid.Empty ) 
            {
                LoadAddNewDefaultValues();
                return; // do nothing
            }

  

            BrokerDB brokerDB = BrokerDB.RetrieveById(m_brokerId);

            Guid? vendorId = brokerDB.OCRVendorId;
            if (vendorId.HasValue)
            {
                this.OCRVendors.SelectedValue = vendorId.Value.ToString();
                this.m_EnableOCR.Checked = true;
            }
            else
            {
                this.m_EnableOCR.Checked = false;
            }

            m_database.Text = DbConnectionInfo.GetConnectionInfo(m_brokerId).FriendlyDisplay;

            if (!IsPostBack)
            {
                DeterminationRulesXMLContent.Text = brokerDB.DeterminationRulesXMLContent;
            }

            m_ID.Text = brokerDB.BrokerID.ToString();
            m_Name.Text = brokerDB.Name.ToString();
            m_Url.Text = brokerDB.Url;
            m_IsTotalAutoPopulateAUSResponse.Checked = brokerDB.IsTotalAutoPopulateAUSResponse;
            m_isLOAllowedToEditProcessorAssignedFile.Checked = brokerDB.IsLOAllowedToEditProcessorAssignedFile;
            m_isOthersAllowedToEditUnderwriterAssignedFile.Checked = brokerDB.IsOthersAllowedToEditUnderwriterAssignedFile;
            m_ForceTemplateOnImport.Checked = brokerDB.ForceTemplateOnImport;
            m_isAllowViewLoanInEditorByDefault.Checked = brokerDB.IsAllowViewLoanInEditorByDefault;
            m_canRunPricingEngineWithoutCreditReportByDefault.Checked = brokerDB.CanRunPricingEngineWithoutCreditReportByDefault;
            m_restrictWritingRolodexByDefault.Checked = brokerDB.RestrictWritingRolodexByDefault;
            m_restrictReadingRolodexByDefault.Checked = brokerDB.RestrictReadingRolodexByDefault;
            m_cannotDeleteLoanByDefault.Checked = brokerDB.CannotDeleteLoanByDefault;
            m_cannotCreateLoanTemplateByDefault.Checked = brokerDB.CannotCreateLoanTemplateByDefault;
            m_hasLenderDefaultFeatures.Checked = brokerDB.HasLenderDefaultFeatures;
            m_AllowPmlOrderNewCreditReport.Checked = brokerDB.AllowPmlUserOrderNewCreditReport;
            m_PmlRequireEstResidualIncome.Checked = brokerDB.PmlRequireEstResidualIncome;
            //m_RequireVerifyLicenseByState.Checked = brokerDB.RequireVerifyLicenseByState; //OPM 2703
            m_isAllPrepaymentPenaltyAllowed.Checked = !brokerDB.IsAllPrepaymentPenaltyAllowed; //OPM 12943
            m_isLpeDisqualificationEnabled.Checked = brokerDB.IsLpeDisqualificationEnabled; // OPM 16132
            m_isStandAloneSecondAllowedInPml.Checked = brokerDB.IsStandAloneSecondAllowedInPml;
            m_is8020ComboAllowedInPml.Checked = brokerDB.Is8020ComboAllowedInPml; // OPM 19784
            m_allowCreatingLoanFromBlankTemplate.Checked = !brokerDB.IsBlankTemplateInvisibleForFileCreation;
            m_isRoundUpLpeRateTo1Eighth.Checked = brokerDB.IsRoundUpLpeRateTo1Eighth;
            m_hasManyUsers.Checked = brokerDB.HasManyUsers;
            m_isTrackModifiedFile.Checked = brokerDB.IsTrackModifiedFile;
            m_isShowPriceGroupInEmbeddedPml.Checked = brokerDB.IsShowPriceGroupInEmbeddedPml;
            Tools.SetDropDownListValue(ShowCredcoOptionT, brokerDB.ShowCredcoOptionT);
            Tools.SetDropDownListValue(m_EDocsPmlEnabledStatus, brokerDB.EDocsEnabledStatusT); // OPM 46783
            CreditMornetPlusUserID.Text = brokerDB.CreditMornetPlusUserID;
            CreditMornetPlusPassword.Text = brokerDB.CreditMornetPlusPassword.Value;
            m_isLpeManualSubmissionAllowed.Checked = brokerDB.IsLpeManualSubmissionAllowed;
            m_isShowPriceGroupInEmbeddedPml.Checked = brokerDB.IsShowPriceGroupInEmbeddedPml;
            m_isPmlPointImportAllowed.Checked = brokerDB.IsPmlPointImportAllowed;
            m_isOptionARMUsedInPml.Checked = brokerDB.IsOptionARMUsedInPml;
            m_UseFHATOTALProductionAccount.Checked = brokerDB.UseFHATOTALProductionAccount;
            m_IsDisplayCompensationChoiceInPml.Checked = brokerDB.IsDisplayCompensationChoiceInPml;
            m_IsHideLOCompPMLCert.Checked = brokerDB.IsHideLOCompPMLCert;
            m_IsHidePricingwithoutLOCompPMLCert.Checked = brokerDB.IsHidePricingwithoutLOCompPMLCert;
            m_IsAlwaysDisplayExactParRateOption.Checked = brokerDB.IsAlwaysDisplayExactParRateOption;
            m_sTaskEmailAlias.Text = BrokerDB.TaskAliasEmailAddress;
            m_sTaskEmailAlias.ReadOnly = true;
            m_IsUseNewCondition.Checked = brokerDB.IsUseNewTaskSystem;
            m_IsUseNewTaskSystemStaticConditionIds.Checked = brokerDB.IsUseNewTaskSystemStaticConditionIds;
            m_IsDocMagicBarcodeScannerEnabled.Checked = brokerDB.IsDocMagicBarcodeScannerEnabled;
            m_IsDocuTechBarcodeScannerEnabled.Checked = brokerDB.IsDocuTechBarcodeScannerEnabled;

            IsOptInMultiFactorAuthentication.Checked = brokerDB.IsOptInMultiFactorAuthentication;
            IsOptOutMultiFactorAuthentication.Checked = !brokerDB.IsOptInMultiFactorAuthentication;
            IsEnableMultiFactorAuthentication.Checked = brokerDB.IsEnableMultiFactorAuthentication;
            IsEnableMultiFactorAuthenticationTPO.Checked = brokerDB.IsEnableMultiFactorAuthenticationTPO;
            AllowDeviceRegistrationViaAuthCodePage.Checked = brokerDB.AllowDeviceRegistrationViaAuthCodePage;
            OptOutMfaName.Text = brokerDB.OptOutMfaName;
            OptOutMfaDate.Text = brokerDB.OptOutMfaDate == DateTime.MinValue ? string.Empty : brokerDB.OptOutMfaDate.ToShortDateString();

            EnableCustomaryEscrowImpoundsCalculation.Checked = brokerDB.EnableCustomaryEscrowImpoundsCalculation;
            Tools.SetDropDownListValue(m_minimumDefaultClosingCostDataSet, brokerDB.MinimumDefaultClosingCostDataSet);

            m_IsIDSBarcodeScannerEnabled.Checked = brokerDB.IsIDSBarcodeScannerEnabled;
            m_IsApplyPricingEngineRoundingAfterLoComp.Checked = brokerDB.IsApplyPricingEngineRoundingAfterLoComp;
            m_IsAddPointWithoutOriginatorCompensationToRateLock.Checked = brokerDB.IsAddPointWithoutOriginatorCompensationToRateLock;
            m_IsDisplayChoiceUfmipInLtvCalc.Checked = brokerDB.IsDisplayChoiceUfmipInLtvCalc;
            m_ShowQMStatusInPml2.Checked = brokerDB.ShowQMStatusInPml2; 
            m_sLPQExportUrl.Text = brokerDB.LPQBaseUrl;
            m_CalculateclosingCostInPML.Checked = brokerDB.CalculateclosingCostInPML;
            m_ApplyClosingCostsToGfeOnSubmission.Checked = brokerDB.ApplyClosingCostToGFE;
            m_IsEnableProvidentFundingSubservicingExport.Checked = brokerDB.IsEnableProvidentFundingSubservicingExport;
            m_IsEnableRateMonitorService.Checked = brokerDB.IsEnableRateMonitorService;
            m_AllowRateMonitorForQP2FilesInTpoPml.Checked = brokerDB.AllowRateMonitorForQP2FilesInTpoPml;
            m_AllowRateMonitorForQP2FilesInEmdeddedPml.Checked = brokerDB.AllowRateMonitorForQP2FilesInEmdeddedPml;
            m_IsForceLeadToLoanTemplate.Checked = brokerDB.IsForceLeadToLoanTemplate;
            m_IsForceLeadToLoanNewLoanNumber.Checked = brokerDB.IsForceLeadToLoanNewLoanNumber;
            m_IsEditLeadsInFullLoanEditor.Checked = brokerDB.IsEditLeadsInFullLoanEditor;
            m_IsForceLeadToLoanRemoveLeadPrefix.Checked = brokerDB.IsForceLeadToLoanRemoveLeadPrefix;
            m_IsForceLeadToUseLoanCounter.Checked = brokerDB.IsForceLeadToUseLoanCounter;
            m_IsCreditUnion.Checked = brokerDB.IsCreditUnion;
            m_IsFederalCreditUnion.Checked = brokerDB.IsFederalCreditUnion;
            m_IsTaxExemptCreditUnion.Checked = brokerDB.IsTaxExemptCreditUnion;
            m_IsEnableNewConsumerPortal.Checked = brokerDB.IsEnableNewConsumerPortal;
            //m_PopulateGFEWithLockDatesFromFrontEndLock.Checked = brokerDB.PopulateGFEWithLockDatesFromFrontEndLock;
            //m_IsAllowOrderingGlobalDmsAppraisals.Checked = brokerDB.IsAllowOrderingGlobalDmsAppraisals; OPM 108957
            m_IsEnabledGfeFeeService.Checked = brokerDB.IsEnabledGfeFeeService;

            m_allowNonExcludableDiscountPoints.Checked = brokerDB.AllowNonExcludableDiscountPoints;

            m_sCustomFieldDescriptionXml.Text = brokerDB.CustomFieldDescriptionXml;
            m_sCustomFieldDescriptionXml.Attributes.Add("readonly", "readonly");	// Set ReadOnly here or we won't be able to read clientside changes

            trNamingPattern.Visible = true;
            m_namingPattern.Text = brokerDB.NamingPattern;
            m_namingPattern.Attributes.Add("readonly", "readonly");

            trLeadNamingPattern.Visible = true;
            m_leadNamingPattern.Text = brokerDB.LeadNamingPattern;
            m_leadNamingPattern.Attributes.Add("readonly", "readonly");

            LenderDocuSignSettings docuSignSettings = LenderDocuSignSettings.Retrieve(brokerDB.BrokerID);
            if (docuSignSettings != null)
            {
                this.docuSignIntegratorKey.Text = docuSignSettings.IntegratorKey;
                this.docuSignSecretKey.Text = docuSignSettings.SecretKey;
                if (!string.IsNullOrWhiteSpace(docuSignSettings.RsaPrivateKey))
                {
                    this.docuSignPrivateRsaKey.Text = docuSignSettings.RsaPrivateKey;
                }
            }

            switch ( brokerDB.PmlUserAutoLoginOption ) 
            {
                case "Off" : 
                    m_doduAutoLoginOption.Items.FindByValue("Off").Selected = true; 
                    break;
                case "Sharable" : 
                    m_doduAutoLoginOption.Items.FindByValue("Sharable").Selected = true; 
                    break;
                case "MustBeUnique" : 
                    m_doduAutoLoginOption.Items.FindByValue("MustBeUnique").Selected = true;
                    break; 
            }
            m_ddlXmlConfig.Text = brokerDB.DDLSpecialConfigXmlContent; 
            //OPM 19104
            /*if(brokerDB.UseCustomInstantSupportInfo)
                m_UseCustomISInfo.Checked = true;
            else
                m_DontUseCustomISInfo.Checked = true;

            m_CustomISPhone.Text = brokerDB.CustomInstantSupportPhone;
            m_CustomISEmail.Text = brokerDB.CustomInstantSupportEmail;
            */

            if(brokerDB.RoundUpLpeFee)
                m_roundUpLpeFee.Checked = true;
            else
                m_dontRoundUpLpeFee.Checked = true;

            m_roundUpLpeFeeInterval.Text = BrokerDB.ToDecimalString(brokerDB.RoundUpLpeFeeInterval);

            this.PmlLoanTemplateId.Enabled = true;

            if ( brokerDB.PmlLoanTemplateID != Guid.Empty )
            {
                // If this a new broker, we will have disabled the PML 
                // template dropdown and set the value to Guid.Empty. 
                // Once we save, we can then reenable the dropdown 
                // and set the value to the actual template ID.
                if (this.IsPostBack &&
                    this.m_quickPricerTemplateId.Items.Count == 1 &&
                    this.PmlLoanTemplateId.Items[0].Value == Guid.Empty.ToString())
                {
                    this.PmlLoanTemplateId.Items[0].Value = brokerDB.PmlLoanTemplateID.ToString();
                }
                else
                {
                    Tools.SetDropDownListValue(PmlLoanTemplateId, brokerDB.PmlLoanTemplateID.ToString());
                }
            }

            this.m_quickPricerTemplateId.Enabled = true;

            if (brokerDB.QuickPricerTemplateId != Guid.Empty)
            {
                // As with the default PML template, for new brokers we want
                // to reenable the dropdown and set the value to the new QP
                // template.
                if (this.IsPostBack &&
                    this.m_quickPricerTemplateId.Items.Count == 1 &&
                    this.m_quickPricerTemplateId.Items[0].Value == Guid.Empty.ToString())
                {
                    this.m_quickPricerTemplateId.Items[0].Value = brokerDB.QuickPricerTemplateId.ToString();
                }
                else
                {
                    Tools.SetDropDownListValue(m_quickPricerTemplateId, brokerDB.QuickPricerTemplateId.ToString());
                }
            }

            m_OriginatorCompensationMigrationDate.Text = brokerDB.OriginatorCompensationMigrationDate.HasValue ? brokerDB.OriginatorCompensationMigrationDate.Value.ToShortDateString() : "";
            m_OriginatorCompensationApplyMinYSPForTPOLoan.Checked = brokerDB.OriginatorCompensationApplyMinYSPForTPOLoan;
            m_IsUsePriceIncludingCompensationInPricingResult.Checked = brokerDB.IsUsePriceIncludingCompensationInPricingResult;
            m_BorrPdCompMayNotExceedLenderPdComp.Checked = brokerDB.BorrPdCompMayNotExceedLenderPdComp;

            m_IsQuickPriceEnabled.Checked = brokerDB.IsQuickPricerEnable;
            m_quickPricerDisclaimerAtResult.Text = brokerDB.QuickPricerDisclaimerAtResult;
            m_quickPricerNoResultMessage.Text = brokerDB.QuickPricerNoResultMessage;
            m_PmlSiteID.Text = brokerDB.PmlSiteID.ToString();

            m_LicenseNumber.Text = brokerDB.LicenseNumber;

            m_Street.Text = brokerDB.Address.StreetAddress;
            m_City.Text = brokerDB.Address.City;
            m_State.Value = brokerDB.Address.State;
            m_Zipcode.Text = brokerDB.Address.Zipcode;
            m_PhoneTF.Text = brokerDB.Phone;
            m_FaxTF.Text = brokerDB.Fax;

            m_EmailAddrSendFromForNotif.Text = brokerDB.EmailAddrSendFromForNotif;
            m_NameSendFromForNotif.Text      = brokerDB.NameSendFromForNotif;

            m_AccountManager.Text = brokerDB.AccountManager;
            m_CustomerCode.Text = brokerDB.CustomerCode;

            m_NHCKey.Text = brokerDB.NHCKey.ToString();
            m_NamingCounter.Text = brokerDB.NamingCounter.ToString();
            m_numberBadPasswordsAllowed.Text = brokerDB.NumberBadPasswordsAllowed.ToString();
            m_maxWebServiceDailyCallVolume.Text = brokerDB.MaxWebServiceDailyCallVolume.ToString();
            m_Notes.Text = brokerDB.Notes;
            m_ActualPricingBrokerId.Text = brokerDB.ActualPricingBrokerId.ToString();
            m_brokerStatus = brokerDB.Status.ToString();
            TempOptionXmlContent.Text = brokerDB.TempOptionXmlContent.Value;


            m_isLONRBL.Items[ brokerDB.HasLONIntegration ? 1 : 0 ].Selected = true;

            m_displayPmlFeeIn100Format.Items[ brokerDB.DisplayPmlFeeIn100Format ? 1 : 0 ].Selected = true; //OPM 19525

            BrokerFeatures brokerFT = new BrokerFeatures(m_brokerId);

            m_enableFeaturePriceMyLoanCB.Checked = brokerFT.HasFeature(E_BrokerFeatureT.PriceMyLoan);
            m_enableFeatureMarketingToolsCB.Checked = brokerFT.HasFeature(E_BrokerFeatureT.MarketingTools);

            if (brokerDB.IsNewPmlUIEnabled)
            {
                m_IsNewPmlUIEnabled_Yes.Checked = true;
            }
            else
            {
                m_IsNewPmlUIEnabled_No.Checked = true;
            }

            m_pathToDataTrac.Text = brokerDB.PathToDataTrac;
            m_dataTracWebserviceURL.Text = brokerDB.DataTracWebserviceURL;

            m_IsPricingMultipleAppsSupported.Checked = brokerDB.IsPricingMultipleAppsSupported; //opm 33530 fs 07/29/09
            m_IsaBEmailRequiredInPml.Checked = brokerDB.IsaBEmailRequiredInPml; //opm 33208 fs 08/21/09

            m_IsForceChangePasswordForPml.Checked = brokerDB.IsForceChangePasswordForPml; // opm 34274

            m_IsIncomeAssetsRequiredFhaStreamline.Checked = brokerDB.IsIncomeAssetsRequiredFhaStreamline; // opm 42579 fs 11/17/09

            m_IsEncompassIntegrationEnabled.Checked = brokerDB.IsEncompassIntegrationEnabled; //opm 45012 fs 01/21/10

            m_isBestPriceEnabled.Checked = brokerDB.IsBestPriceEnabled; //av opm 22180 05 28 08

            m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSP.Checked = brokerDB.IsDisplayRateOptionsAboveLowestTriggeringMaxYSP;
            m_IsUseLayeredFinancing.Checked = brokerDB.IsUseLayeredFinancing;
            m_EnableAutoPriceEligibilityProcess.Checked = brokerDB.EnableAutoPriceEligibilityProcess;
            m_EnableRetailTpoPortalMode.Checked = brokerDB.EnableRetailTpoPortalMode;
            m_EnableRenovationLoanSupport.Checked = brokerDB.EnableRenovationLoanSupport;
            this.EnableLqbMobileApp.Checked = brokerDB.EnableLqbMobileApp;

            m_IsRemovePreparedDates.Checked = brokerDB.IsRemovePreparedDates;
            m_IsProtectDisclosureDates.Checked = brokerDB.IsProtectDisclosureDates;
            m_IsEnableDTPkgBsdRedisclosureDatePop.Checked = brokerDB.IsEnableDTPkgBsdRedisclosureDatePop;

            m_IsB4AndB6DisabledIn1100And1300OfGfe.Checked = brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe;

            this.EnableEConsentMonitoringAutomation.Checked = brokerDB.EnableEConsentMonitoringAutomation;
            this.DownloadAffectedEConsentExpirationFiles.Visible = this.EnableEConsentMonitoringAutomation.Checked;

            m_EnableCocRedisclosureTriggersForHelocLoans.Checked = brokerDB.EnableCocRedisclosureTriggersForHelocLoans;
            m_UseCustomCocFieldList.Checked = brokerDB.UseCustomCocFieldList;

            m_EnableConversationLogForLoans.Checked = brokerDB.EnableConversationLogForLoans;
            m_EnableConversationLogForLoansInTpo.Checked = brokerDB.EnableConversationLogForLoansInTpo;
            this.IsKivaIntegrationEnabled.Checked = brokerDB.IsKivaIntegrationEnabled;

            m_SendAutoLockToLockDesk.Checked = brokerDB.SendAutoLockToLockDesk; //opm 111418
            m_SaveLockConfOnAutoLock.Checked = brokerDB.SaveLockConfOnAutoLock; //opm 184181
            m_EnableDuplicateEDocProtectionChecking.Checked = brokerDB.EnableDuplicateEDocProtectionChecking; //OPM 115047

            m_ShowUnderwriterInPMLCertificate.Checked = brokerDB.ShowUnderwriterInPMLCertificate; //opm 12711
            m_ShowJuniorUnderwriterInPMLCertificate.Checked = brokerDB.ShowJuniorUnderwriterInPMLCertificate; // 11/21/2013 gf - opm 145015
            m_ShowProcessorInPMLCertificate.Checked = brokerDB.ShowProcessorInPMLCertificate;	  //opm 12711
            m_ShowJuniorProcessorInPMLCertificate.Checked = brokerDB.ShowJuniorProcessorInPMLCertificate; // 11/21/2013 gf - opm 145015

            m_IsAlwaysUseBestPrice.Checked = brokerDB.IsAlwaysUseBestPrice; //opm 24214
            m_IsDataTracIntegrationEnabled.Checked = brokerDB.IsDataTracIntegrationEnabled; // OPM 24590
            m_IsExportPricingInfoTo3rdParty.Checked = brokerDB.IsExportPricingInfoTo3rdParty; // OPM 34444

            m_showROPricedWorseBPPPOn.Checked = brokerDB.IsRateOptionsWorseThanLowerRateShownWithBestPriceView;
            m_ShowROPricedworseBPPPOff.Checked = brokerDB.IsRateOptionsWorseThanLowerRateShownWithNonbestPriceView;
            
            m_IsShowRateOptionsWorseThanLowerRate.Checked = brokerDB.IsShowRateOptionsWorseThanLowerRate;
            
            m_ddlBillingVersion.SelectedValue = brokerDB.BillingVersion.ToString();

            this.EnableKtaIntegration.Checked = brokerDB.EnableKtaIntegration;

            m_IsEDocsEnabled.Checked = brokerDB.IsEDocsEnabled;
            m_IsDay1DayOfRateLock.Checked = brokerDB.IsDay1DayOfRateLock;
            Tools.SetRadioButtonListValue(m_rateLockExpirationWeekendHolidayBehavior, brokerDB.RateLockExpirationWeekendHolidayBehavior);
            m_IsDocuTechEnabled.Checked = brokerDB.IsDocuTechEnabled;
            m_IsDocuTechStageEnabled.Checked = brokerDB.IsDocuTechStageEnabled;
            m_IsImportDuFindings.Checked = brokerDB.IsImportDoDuLpFindingsAsConditions;

            m_IsEnableFannieMaeEarlyCheck.Checked = brokerDB.IsEnableFannieMaeEarlyCheck;
            m_IsEnableFreddieMacLoanQualityAdvisor.Checked = brokerDB.IsEnableFreddieMacLoanQualityAdvisor;
            LoadCRAList();
            LoadFaxInformation();

            if (!brokerDB.IsNew)
            {
                BarcodeKey.Text = EncryptionHelper.GetBarcodeUploadToken(brokerDB.BrokerID, brokerDB.CustomerCode);
            }
            
            Tools.SetDropDownListValue(m_Pml2AsQuickPricerMode, brokerDB.Pml2AsQuickPricerMode);

            Tools.SetDropDownListValue(m_SelectedNewLoanEditorUIPermissionType, brokerDB.SelectedNewLoanEditorUIPermissionType);

            // 4/7/2005 dd - OPM #1094
            var firstProxy = CreditReportAccountProxy.Helper.RetrieveCreditProxyByBrokerID(m_brokerId);
            ClientScript.RegisterHiddenField("CreditProxy_CrAccProxyId", (firstProxy?.CrAccProxyId ?? Guid.Empty).ToString());
            if (firstProxy != null)
            {
                Tools.SetDropDownListValue(CreditProxy_CraId, firstProxy.CraId.ToString());
                CreditProxy_CraUserName.Text = firstProxy.CraUserName;
                CreditProxy_CraPassword.Text = firstProxy.CraPassword;
                CreditProxy_CraAccId.Text = firstProxy.CraAccId;
                CreditProxy_IsEquifaxPulled.Checked = firstProxy.IsEquifaxPulled;
                CreditProxy_IsExperianPulled.Checked = firstProxy.IsExperianPulled;
                CreditProxy_IsTransunionPulled.Checked = firstProxy.IsTransUnionPulled;
                CreditProxy_IsFannieMaePulled.Checked = firstProxy.IsFannieMaePulled;
                CreditProxy_CreditMornetPlusPassword.Text = firstProxy.CreditMornetPlusPassword;
                CreditProxy_CreditMornetPlusUserId.Text = firstProxy.CreditMornetPlusUserId;
                CreditProxy_CrAccProxyDesc.Text = firstProxy.CrAccProxyDesc;
                m_AllowPmlViewReport.Checked = firstProxy.AllowPmlUserToViewReports;
                } 

            //12-18-08 jk OPM 20030 - Load Custom Ratesheet Expiration Messages from the database

            nothingLoaded = false;
            m_newOutsideNormalHoursAndPassInvestorCutOffMessage = m_newOutsideNormalHoursMessage.Text;
            SqlParameter[] warningMessageParameters = {
                                                          new SqlParameter("@Broker", m_brokerId)
                                                      };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "getRSWarningMessageById", warningMessageParameters))
            {
                if (reader.Read())
                {
                  
                    if (!Page.IsPostBack)
                    {

                        m_newNormalUserExpiredMessage.Text = (string)reader["NormalUserExpiredCustomMsg"];
                        m_newLockDeskExpiredMessage.Text = (string)reader["LockDeskExpiredCustomMsg"];
                        m_newNormalUserCutOffMessage.Text = (string)reader["NormalUserCutOffCustomMsg"];
                        m_newOutsideNormalHoursMessage.Text = (string)reader["OutsideNormalHrCustomMsg"];
                        m_newLockDeskCutOffMessage.Text = (string)reader["LockDeskCutOffCustomMsg"];
                        m_newOutsideClosureDayHourMessage.Text = (string)reader["OutsideClosureDayHrCustomMsg"];
                        m_newOutsideNormalHoursAndPassInvestorCutOffMessage = (string)reader["OutsideNormalHrandPassInvestorCutOffCustomMsg"];
                    }
                }
                else
                {
                    nothingLoaded = true;
                    
                }

            }

            m_defaultNormalUserExpiredMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage, m_brokerId);
            m_defaultLockDeskExpiredMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.ByPassAll, ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage, m_brokerId);
            m_defaultNormalUserCutOffMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_CutOffMessage + "{0}", m_brokerId);
            m_defaultLockDeskCutOffMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.ByPassAll, ConstAppDavid.BlockedRateLockSubmission_CutOffMessage + "{0}", m_brokerId);
            m_defaultOutsideNormalHoursMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHour + "{0}", m_brokerId);
            m_defaultOutsideClosureDayHourMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_OutsideClosureDayHour + "{0}", m_brokerId);
            m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff + "{0}" + "#" + "{1}", m_brokerId);


            if (m_newNormalUserExpiredMessage.Text.Equals(""))
                m_newNormalUserExpiredMessage.Text = m_defaultNormalUserExpiredMessage;
            if (m_newLockDeskExpiredMessage.Text.Equals(""))
                m_newLockDeskExpiredMessage.Text = m_defaultLockDeskExpiredMessage;
            if (m_newNormalUserCutOffMessage.Text.Equals(""))
                m_newNormalUserCutOffMessage.Text = m_defaultNormalUserCutOffMessage;
            if (m_newOutsideNormalHoursMessage.Text.Equals(""))
                m_newOutsideNormalHoursMessage.Text = m_defaultOutsideNormalHoursMessage;
            if (m_newLockDeskCutOffMessage.Text.Equals(""))
                m_newLockDeskCutOffMessage.Text = m_defaultLockDeskCutOffMessage;
            if (m_newOutsideClosureDayHourMessage.Text.Equals(""))
                m_newOutsideClosureDayHourMessage.Text = m_defaultOutsideClosureDayHourMessage;
            if (m_newOutsideNormalHoursAndPassInvestorCutOffMessage.Equals(""))
                m_newOutsideNormalHoursAndPassInvestorCutOffMessage = m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage;

            m_CELogin.Text = brokerDB.ComplianceEaseUserName ?? "";

            this.IsEnableComplianceEagleIntegration.Checked = brokerDB.IsEnableComplianceEagleIntegration;
            this.IsEnablePTMComplianceEagleAuditIndicator.Checked = brokerDB.IsEnablePTMComplianceEagleAuditIndicator;
            this.ComplianceEagleLogin.Text = brokerDB.ComplianceEagleUserName;
            bool hasComplianceEaglePassword = !string.IsNullOrEmpty(brokerDB.ComplianceEaglePassword.Value);
            this.phComplianceEaglePasswordRequiring.Visible = !hasComplianceEaglePassword;
            this.phComplianceEaglePasswordUpdating.Visible = hasComplianceEaglePassword;
            this.ComplianceEagleEnableMismo34.Checked = brokerDB.ComplianceEagleEnableMismo34;

            this.ComplianceEaglePassword.Text = null;
            this.ComplianceEagleCompanyID.Text = brokerDB.ComplianceEagleCompanyID;

            this.IsEnableComplianceEaseIntegration.Checked = brokerDB.IsEnableComplianceEaseIntegration;
            this.m_IsEnablePTMComplianceEaseIndicator.Checked = brokerDB.IsEnablePTMComplianceEaseIndicator;

            m_IsEnablePTMDocMagicSeamlessInterface.Checked = brokerDB.IsEnablePTMDocMagicSeamlessInterface;

            DocVendorList.DataSource = this.EnabledDocVendors;
            DocVendorList.DataBind();

            m_GenworthRanking.Value = brokerDB.PmiProviderGenworthRanking.ToString();
            m_MgicRanking.Value = brokerDB.PmiProviderMgicRanking.ToString();
            m_RadianRanking.Value = brokerDB.PmiProviderRadianRanking.ToString();
            m_EssentRanking.Value = brokerDB.PmiProviderEssentRanking.ToString();
            archRanking.Value = brokerDB.PmiProviderArchRanking.ToString();
            m_IsHighestPmiProviderWins.Value = brokerDB.IsHighestPmiProviderWins ? "1" : "0";
            m_NationalRanking.Value = brokerDB.PmiProviderNationalMIRanking.ToString();
            m_MassHousingRanking.Value = brokerDB.PmiProviderMassHousingRanking.ToString();
            m_AllowAllUsersToUseKayako.Checked = brokerDB.IsAllowAllUsersToUseKayako;
            Tools.SetDropDownListValue(this.SuiteType, brokerDB.SuiteType);
            m_SandboxScratchLOXml.Text = brokerDB.SandboxScratchLOXml;

            if (false == string.IsNullOrEmpty(brokerDB.ComplianceEasePassword.Value))
            {
                m_phCEPasswordRequiring.Visible = false;
                m_phCEPasswordUpdating.Visible = true;
            }
            else
            {
                m_phCEPasswordUpdating.Visible = false;
                m_phCEPasswordRequiring.Visible = true;
            }

            ConditionCategory[] categories = ConditionCategory.GetCategories(m_brokerId).OrderBy(p => p.Category).ToArray();

            m_ddlDriveConditionCategory.Items.Clear();
            m_ddlDriveConditionCategory.Items.Add(new ListItem("<--None Selected-->", ""));
            foreach (var cc in categories)
            {
                m_ddlDriveConditionCategory.Items.Add(new ListItem(cc.Category, cc.Id.ToString()));
            }
            m_cbEnableDriveIntegration.Checked = brokerDB.IsEnableDriveIntegration;

            if (brokerDB.IsEnableDriveIntegration)
            {
                m_cbEnableDriveConditionImporting.Checked = brokerDB.IsEnableDriveConditionImporting;
                if (brokerDB.IsEnableDriveConditionImporting)
                {
                    if (null != brokerDB.DriveConditionCategoryID)
                    {
                        m_ddlDriveConditionCategory.SelectedValue = brokerDB.DriveConditionCategoryID.ToString();
                    }                    
                    if (null != brokerDB.DriveTaskPermissionLevelID)
                    {
                        var permissionLevel = PermissionLevel.RetrieveAll(m_brokerId).Where(p => p.IsActive && p.Id == brokerDB.DriveTaskPermissionLevelID).FirstOrDefault();
                        DrivePermissionDescription.Value = permissionLevel?.Description ?? "The DRIVE Task Permission Level is inactive. Please change it.";
                        DriveTaskPermission.Text = permissionLevel?.Name ?? "INACTIVE - Change Immediately!";
                        DrivePermissionLevelID.Value = brokerDB.DriveTaskPermissionLevelID.ToString();
                    }
                    DriveDueDateCalcDescription.Text = Tools.GetTaskDueDateDescription(brokerDB.DriveDueDateCalculationOffset, brokerDB.DriveDueDateCalculatedFromField, false);
                    DriveDueDateCalcField.Value = brokerDB.DriveDueDateCalculatedFromField;
                    DriveDueDateOffset.Value = brokerDB.DriveDueDateCalculationOffset.ToString();
                }
                if (null != brokerDB.DriveDocTypeID)
                {
                    m_selectedDriveDocType.Value = brokerDB.DriveDocTypeID.ToString();
                    var driveDoc = EDocumentDocType.GetDocTypeById(brokerDB.BrokerID, (int)brokerDB.DriveDocTypeID);
                    m_lSelectedDriveDocType.Text = driveDoc.FolderAndDocTypeName;
                }
            }

            // OPM 230273
            var symitarLenderConfig = brokerDB.GetEnabledSymitarLenderConfig(E_sLoanFileT.Loan);
            if (symitarLenderConfig != null)
            {
                this.SymitarEnableCb.Checked = true;

                this.SymitarLenderConfigDeviceNumber.Text = symitarLenderConfig.DeviceNumber.Value.ToString();
                this.SymitarLenderConfigDeviceType.Text = symitarLenderConfig.DeviceType.Value;
                this.SymitarLenderConfigIpAddress.Text = symitarLenderConfig.IpAddress.Value.ToString();
                this.SymitarLenderConfigIpPorts.Text = string.Join(",", symitarLenderConfig.Ports.Value);
                this.SymitarLenderConfigVirtualCardPrefix.Text = symitarLenderConfig.VirtualCardPrefix.Value;
            }
            else
            {
                this.SymitarEnableCb.Checked = false;
            }

            if (brokerDB.IsEnabledLockPeriodDropDown)
            {
                string[] options = brokerDB.AvailableLockPeriodOptionsCSV.Split(new char[] { ',' });

                for (int i = 1; i <= ConstApp.BROKER_MAX_NUMBER_OF_LOCK_PERIODS; i++)
                {
                    ((TextBox)Page.FindControl("m_AvailableLockPeriod_" + i)).Text = options[i - 1];
                }
                m_IsEnabledLockPeriodDropdown.Checked = true;
            }

            this.IsCenlarEnabled.Checked = brokerDB.IsCenlarEnabled;

            var cenlarConfig = CenlarLenderConfiguration.Retrieve(this.m_brokerId);
            if (cenlarConfig != null)
            {
                this.CenlarEnableAutomaticTransmissions.Checked = cenlarConfig.EnableAutomaticTransmissions;
                this.CenlarCustomReportName.Text = cenlarConfig.CustomReportName;
                this.CenlarBatchExportName.Text = cenlarConfig.BatchExportName;
                this.CenlarDummyUserId.Text = cenlarConfig.DummyUserId.ToString();
                this.CenlarNotificationEmails.Text = Tools.JoinEmailList(cenlarConfig.NotificationEmails);
                this.CenlarShouldSuppressAcceptedLoanNotifications.Checked = cenlarConfig.ShouldSuppressAcceptedLoanNotifications;
                this.CenlarRemoveProblematicPayloadCharacters.Checked = cenlarConfig.RemoveProblematicPayloadCharacters;
            }

            #region Flood Integration //OPM 12758
            m_ddlFloodProvider.Items.Clear(); // add MCL flood providers
            m_ddlFloodProvider.Items.Add(Tools.CreateEnumListItem("<--None Selected-->", E_FloodCertificationPreparerT.Other));
            m_ddlFloodProvider.Items.Add(Tools.CreateEnumListItem("CBC Innovis - FZDS", E_FloodCertificationPreparerT.CBCInnovis_FZDS)); // 11/20/2015 BS - Case 228701
            m_ddlFloodProvider.Items.Add(Tools.CreateEnumListItem("Certified Credit Reporting", E_FloodCertificationPreparerT.CertifiedCredit));
            m_ddlFloodProvider.Items.Add(Tools.CreateEnumListItem("CoreLogic", E_FloodCertificationPreparerT.FirstAm_Corelogic));
            m_ddlFloodProvider.Items.Add(Tools.CreateEnumListItem("Kroll Factual Data", E_FloodCertificationPreparerT.KrollFactualData));
            m_ddlFloodProvider.Items.Add(Tools.CreateEnumListItem("Lereta", E_FloodCertificationPreparerT.Leretta));
            // 10/16/2015 BS - Case 228700. Update LPS to ServiceLink
            m_ddlFloodProvider.Items.Add(Tools.CreateEnumListItem("ServiceLink", E_FloodCertificationPreparerT.ServiceLink));
            m_ddlFloodProvider.Items.Add(Tools.CreateEnumListItem("MDA", E_FloodCertificationPreparerT.MDA));

            m_cbEnableFloodIntegration.Checked = brokerDB.IsEnableFloodIntegration;
            m_cbIsCCPmtRequiredForFloodIntegration.Checked = brokerDB.IsCCPmtRequiredForFloodIntegration;
            m_ddlFloodReseller.SelectedValue = Guid.Empty.ToString();
            if (brokerDB.IsEnableFloodIntegration)
            {
                m_ddlFloodProvider.SelectedValue = brokerDB.FloodProviderID.ToString();
                m_ddlFloodReseller.SelectedValue = brokerDB.FloodResellerId.ToString();
                FloodIntegrationAccountID.Text = brokerDB.FloodAccountId;

                if (null != brokerDB.FloodDocTypeID)
                {
                    m_selectedFloodDocType.Value = brokerDB.FloodDocTypeID.ToString();
                    var floodDoc = EDocumentDocType.GetDocTypeById(brokerDB.BrokerID, (int)brokerDB.FloodDocTypeID);
                    m_lSelectedFloodDocType.Text = floodDoc.FolderAndDocTypeName;
                }
            }
            #endregion

            #region DU Import Options //OPM 94140
            m_IsImportDuFindingsByDefault.Checked = brokerDB.ImportAusFindingsByDefault;
            m_IsImportDu1003ByDefault.Checked = brokerDB.ImportAus1003ByDefault;
            m_IsImportDuCreditReportByDefault.Checked = brokerDB.ImportAusCreditReportByDefault;
            #endregion

            #region Appraisal Integrations //OPM 108957
            
            m_AppraisalIntegrations.DataSource = AppraisalVendorConfig.ListAll().OrderBy(v => v.VendorName);
            m_AppraisalIntegrations.DataBind();

            #endregion

            #region Sequential Numbering OPM 118600
            m_EnableSequentialNumbering.Checked = brokerDB.EnableSequentialNumbering;
            m_SequentialNumberingFieldId.Text = brokerDB.SequentialNumberingFieldId;
            m_SequentialNumberingSeed.Text = brokerDB.SequentialNumberingSeed.ToString("d");
            #endregion


            if (m_brokerId != Guid.Empty)
            {
                foreach (var _4506Vendor in Irs4506TVendorConfiguration.ListActiveVendorByBrokerId(m_brokerId))
                {
                    m_4506VendorList.Items.FindByValue(_4506Vendor.VendorId.ToString()).Selected = true;
                }

                foreach (DataRetrievalPartner partner in DataRetrievalPartner.ListPartnersByBrokerId(m_brokerId))
                {
                    m_DRFPartnerList.Items.FindByValue(partner.PartnerID.ToString()).Selected = true;
                }
            }

            m_IsAutoGenerateTradeNums.Checked = brokerDB.IsAutoGenerateTradeNums;
            m_TradeCounter.Text = (brokerDB.TradeCounter + 1).ToString();

            m_PoolCounter.Text = (brokerDB.PoolCounter + 1).ToString();

            m_sActiveDirectoryUrl.Text = brokerDB.ActiveDirectoryURL;

            m_Save.Attributes.Add("onclick", "replace();");
            this.LinkedLoanUpdateFieldsString.Text = brokerDB.LinkedLoanUpdateFieldsString;
        }

        private static ListItem GetVendorListItem(VendorConfig vendor)
        {
            ListItem item = new ListItem(vendor.VendorName, vendor.VendorId.ToString());
            item.Attributes.Add("UsesAccountId", vendor.UsesAccountId.ToString());
            item.Attributes.Add("UsesPassword", vendor.UsesPassword.ToString());
            item.Attributes.Add("UsesUsername", vendor.UsesUsername.ToString());
            item.Attributes.Add("SupportsOnlineInterface", vendor.SupportsOnlineInterface.ToString());
            return item;
        }

        string GetUniqueKey()
        {
            return Guid.NewGuid().ToString().Replace("-", "") ;
        }

        public void SaveData()
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            string determinationRuleErrorMsg = "";
            var areRulesValid = (DeterminationRulesXMLContent.Text.Equals(string.Empty) || DeterminationRuleSet.ValidateDeterminationRulesXMLContent(DeterminationRulesXMLContent.Text, out determinationRuleErrorMsg));

            DeterminationMessageError.Visible = !areRulesValid;
            DeterminationMessageError.InnerText = determinationRuleErrorMsg;

            if (areRulesValid)
            {

                if (!Page.IsValid) throw new IAutoLoadUserControlSaveDataException("Page is not valid.");

                if (m_brokerId != Guid.Empty)
                {
                    HttpContext.Current.Cache.Remove("FeaturesByBroker" + m_brokerId.ToString());
                }

                // 8/26/2004 kb - We may need to do a before and after comparison
                // of broker settings to properly track changes in an event or audit
                // log.  To do so, we need the before snapshot.  Only activation
                // cap changes are recorded in this way at this time.
                //
                // 2/22/2005 kb - We now prefetch the broker, so updates will detect new saves and previous activation caps with little
                // trouble.

                BrokerDB brokerDB = new BrokerDB();

                if (m_brokerId != Guid.Empty)
                {
                    brokerDB = BrokerDB.RetrieveById(m_brokerId);
                }

                brokerDB.DeterminationRulesXMLContent = DeterminationRulesXMLContent.Text;

                bool isNeedSetupDefaultValues = brokerDB.IsNew;
                // Build up the update commands (create or update) using the sql
                // builders and the current ui state.

                brokerDB.Name = m_Name.Text;
                brokerDB.Url = m_Url.Text;
                brokerDB.LicenseNumber = m_LicenseNumber.Text;

                if (brokerDB.Address == null)
                {
                    brokerDB.Address = new CommonLib.Address();
                }

                brokerDB.IsNewPmlUIEnabled = m_IsNewPmlUIEnabled_Yes.Checked;
                brokerDB.IsQuickPricerEnable = m_IsQuickPriceEnabled.Checked && !isNeedSetupDefaultValues;
                if (brokerDB.IsQuickPricerEnable && m_quickPricerTemplateId.SelectedValue == Guid.Empty.ToString())
                {
                    var msg = "Sorry, you must have a quick pricer template selected if you are going to enable quick pricer.";
                    throw new CBaseException(msg, msg);
                }
                if (m_quickPricerDisclaimerAtResult.Text != string.Empty)
                {
                    if (m_quickPricerDisclaimerAtResult.Text.Length > 500)
                    {
                        throw new CBaseException("Sorry, m_quickPricerDisclaimerAtResult limit is 500", "m_quickPricerDisclaimerAtResult is too large,");
                    }
                    brokerDB.QuickPricerDisclaimerAtResult = m_quickPricerDisclaimerAtResult.Text;
                }

                if (m_quickPricerNoResultMessage.Text != string.Empty)
                {
                    if (m_quickPricerNoResultMessage.Text.Length > 500)
                    {
                        throw new CBaseException("Sorry, m_quickPricerNoResultMessage limit is 500", "m_quickPricerNoResultMessage is too large,");
                    }
                    brokerDB.QuickPricerNoResultMessage = m_quickPricerNoResultMessage.Text;
                }

                brokerDB.QuickPricerTemplateId = m_quickPricerTemplateId.SelectedItem.Value != Guid.Empty.ToString() ? new Guid(m_quickPricerTemplateId.SelectedItem.Value) : Guid.Empty;
                brokerDB.IsAddPointWithoutOriginatorCompensationToRateLock = m_IsAddPointWithoutOriginatorCompensationToRateLock.Checked;
                brokerDB.IsDisplayChoiceUfmipInLtvCalc = m_IsDisplayChoiceUfmipInLtvCalc.Checked;
                brokerDB.IsApplyPricingEngineRoundingAfterLoComp = m_IsApplyPricingEngineRoundingAfterLoComp.Checked;
                brokerDB.IsDocMagicBarcodeScannerEnabled = m_IsDocMagicBarcodeScannerEnabled.Checked;
                brokerDB.IsDocuTechBarcodeScannerEnabled = m_IsDocuTechBarcodeScannerEnabled.Checked;

                brokerDB.IsEnableMultiFactorAuthentication = IsEnableMultiFactorAuthentication.Checked;
                brokerDB.IsEnableMultiFactorAuthenticationTPO = IsEnableMultiFactorAuthenticationTPO.Checked;
                brokerDB.IsOptInMultiFactorAuthentication = this.IsOptInMultiFactorAuthentication.Checked;
                brokerDB.AllowDeviceRegistrationViaAuthCodePage = this.AllowDeviceRegistrationViaAuthCodePage.Checked;
                brokerDB.OptOutMfaName = this.OptOutMfaName.Text;
                DateTime optOutMfaDate;
                if (DateTime.TryParse(this.OptOutMfaDate.Text, out optOutMfaDate))
                {
                    brokerDB.OptOutMfaDate = optOutMfaDate;
                }

                brokerDB.EnableCustomaryEscrowImpoundsCalculation = EnableCustomaryEscrowImpoundsCalculation.Checked;
                brokerDB.MinimumDefaultClosingCostDataSet = (E_sClosingCostFeeVersionT)Tools.GetDropDownListValue(m_minimumDefaultClosingCostDataSet);

                brokerDB.IsIDSBarcodeScannerEnabled = m_IsIDSBarcodeScannerEnabled.Checked;
                brokerDB.IsTotalAutoPopulateAUSResponse = m_IsTotalAutoPopulateAUSResponse.Checked;
                brokerDB.IsUseNewTaskSystem = m_IsUseNewCondition.Checked;
                brokerDB.IsUseNewTaskSystemStaticConditionIds = m_IsUseNewTaskSystemStaticConditionIds.Checked;
                brokerDB.IsEnableProvidentFundingSubservicingExport = m_IsEnableProvidentFundingSubservicingExport.Checked;
                brokerDB.IsEnableRateMonitorService = m_IsEnableRateMonitorService.Checked;
                brokerDB.AllowRateMonitorForQP2FilesInTpoPml = m_AllowRateMonitorForQP2FilesInTpoPml.Checked;
                brokerDB.AllowRateMonitorForQP2FilesInEmdeddedPml = m_AllowRateMonitorForQP2FilesInEmdeddedPml.Checked;
                brokerDB.IsForceLeadToLoanTemplate = m_IsForceLeadToLoanTemplate.Checked;
                brokerDB.IsForceLeadToLoanNewLoanNumber = m_IsForceLeadToLoanNewLoanNumber.Checked;
                brokerDB.IsEditLeadsInFullLoanEditor = m_IsEditLeadsInFullLoanEditor.Checked;
                brokerDB.IsForceLeadToLoanRemoveLeadPrefix = m_IsForceLeadToLoanRemoveLeadPrefix.Checked;
                brokerDB.IsForceLeadToUseLoanCounter = m_IsForceLeadToUseLoanCounter.Checked;
                brokerDB.Address.StreetAddress = m_Street.Text;
                brokerDB.Address.City = m_City.Text;
                brokerDB.Address.State = m_State.Value;
                brokerDB.Address.Zipcode = m_Zipcode.Text;
                brokerDB.Phone = m_PhoneTF.Text;
                brokerDB.Fax = m_FaxTF.Text;
                brokerDB.ShowCredcoOptionT = (E_ShowCredcoOptionT)Tools.GetDropDownListValue(ShowCredcoOptionT);
                brokerDB.CreditMornetPlusUserID = CreditMornetPlusUserID.Text;
                brokerDB.CreditMornetPlusPassword = CreditMornetPlusPassword.Text;
                brokerDB.EmailAddrSendFromForNotif = m_EmailAddrSendFromForNotif.Text;
                brokerDB.NameSendFromForNotif = m_NameSendFromForNotif.Text;
                brokerDB.BorrPdCompMayNotExceedLenderPdComp = m_BorrPdCompMayNotExceedLenderPdComp.Checked;
                brokerDB.IsUsePriceIncludingCompensationInPricingResult = m_IsUsePriceIncludingCompensationInPricingResult.Checked;
                brokerDB.IsAlwaysDisplayExactParRateOption = m_IsAlwaysDisplayExactParRateOption.Checked;
                brokerDB.LPQBaseUrl = m_sLPQExportUrl.Text;
                brokerDB.IsEnabledGfeFeeService = m_IsEnabledGfeFeeService.Checked;
                brokerDB.IsEnableFannieMaeEarlyCheck = m_IsEnableFannieMaeEarlyCheck.Checked;
                brokerDB.IsEnableFreddieMacLoanQualityAdvisor = m_IsEnableFreddieMacLoanQualityAdvisor.Checked;

                brokerDB.PmiProviderGenworthRanking = Convert.ToInt32(m_GenworthRanking.Value);
                brokerDB.PmiProviderMgicRanking = Convert.ToInt32(m_MgicRanking.Value);
                brokerDB.PmiProviderRadianRanking = Convert.ToInt32(m_RadianRanking.Value);
                brokerDB.PmiProviderEssentRanking = Convert.ToInt32(m_EssentRanking.Value);
                brokerDB.PmiProviderArchRanking = Convert.ToInt32(archRanking.Value);
                brokerDB.PmiProviderNationalMIRanking = Convert.ToInt32(m_NationalRanking.Value);
                brokerDB.PmiProviderMassHousingRanking = Convert.ToInt32(m_MassHousingRanking.Value);
                brokerDB.IsHighestPmiProviderWins = m_IsHighestPmiProviderWins.Value == "1";
                brokerDB.CustomFieldDescriptionXml = m_sCustomFieldDescriptionXml.Text;
                brokerDB.IsEnableNewConsumerPortal = m_IsEnableNewConsumerPortal.Checked;
                brokerDB.ShowQMStatusInPml2 = m_ShowQMStatusInPml2.Checked;
                brokerDB.IsAllowAllUsersToUseKayako = m_AllowAllUsersToUseKayako.Checked;
                brokerDB.SuiteType = (BrokerSuiteType)Tools.GetDropDownListValue(this.SuiteType);
                brokerDB.SandboxScratchLOXml = m_SandboxScratchLOXml.Text;

                //brokerDB.PopulateGFEWithLockDatesFromFrontEndLock = m_PopulateGFEWithLockDatesFromFrontEndLock.Checked;
                brokerDB.IsCreditUnion = m_IsCreditUnion.Checked;
                brokerDB.IsFederalCreditUnion = m_IsCreditUnion.Checked && m_IsFederalCreditUnion.Checked;
                brokerDB.IsTaxExemptCreditUnion = m_IsCreditUnion.Checked && (m_IsFederalCreditUnion.Checked || m_IsTaxExemptCreditUnion.Checked);

                if (this.trLeadNamingPattern.Visible)
                {
                    brokerDB.LeadNamingPattern = m_leadNamingPattern.Text;
                }

                brokerDB.IsImportDoDuLpFindingsAsConditions = m_IsImportDuFindings.Checked;

                DateTime originatorCompensationMigrationDate;

                if (string.IsNullOrEmpty(m_OriginatorCompensationMigrationDate.Text))
                {
                    brokerDB.OriginatorCompensationMigrationDate = null;
                    brokerDB.OriginatorCompensationApplyMinYSPForTPOLoan = false;

                }
                else if (DateTime.TryParse(m_OriginatorCompensationMigrationDate.Text, out originatorCompensationMigrationDate))
                {
                    brokerDB.OriginatorCompensationMigrationDate = originatorCompensationMigrationDate;
                    brokerDB.OriginatorCompensationApplyMinYSPForTPOLoan = m_OriginatorCompensationApplyMinYSPForTPOLoan.Checked;
                }
                else
                {
                    throw new CBaseException("Invalid Origination Compensation Migration Date. It has to be empty or a valid date.", "User Error");
                }

                if (m_numberBadPasswordsAllowed.Text.TrimWhitespaceAndBOM() != "")
                    brokerDB.NumberBadPasswordsAllowed = int.Parse(m_numberBadPasswordsAllowed.Text); // OPM 19920

                if (m_maxWebServiceDailyCallVolume.Text.TrimWhitespaceAndBOM() != "")
                    brokerDB.MaxWebServiceDailyCallVolume = int.Parse(m_maxWebServiceDailyCallVolume.Text);

                brokerDB.PmlUserAutoLoginOption = m_doduAutoLoginOption.SelectedItem.Value; //  19618
                if (m_IsEDocsEnabled.Checked == false)
                {
                    brokerDB.EDocsEnabledStatusT = E_EDocsEnabledStatusT.No;
                }
                else
                {
                    brokerDB.EDocsEnabledStatusT = (E_EDocsEnabledStatusT)Tools.GetDropDownListValue(m_EDocsPmlEnabledStatus);//  46783
                }

                brokerDB.AccountManager = m_AccountManager.Text;
                brokerDB.CustomerCode = m_CustomerCode.Text;

                brokerDB.IsLOAllowedToEditProcessorAssignedFile = m_isLOAllowedToEditProcessorAssignedFile.Checked;
                brokerDB.IsOthersAllowedToEditUnderwriterAssignedFile = m_isOthersAllowedToEditUnderwriterAssignedFile.Checked;
                brokerDB.ForceTemplateOnImport = m_ForceTemplateOnImport.Checked;
                brokerDB.IsAllowViewLoanInEditorByDefault = m_isAllowViewLoanInEditorByDefault.Checked;
                brokerDB.CanRunPricingEngineWithoutCreditReportByDefault = m_canRunPricingEngineWithoutCreditReportByDefault.Checked;
                brokerDB.RestrictWritingRolodexByDefault = m_restrictWritingRolodexByDefault.Checked;
                brokerDB.RestrictReadingRolodexByDefault = m_restrictReadingRolodexByDefault.Checked;
                brokerDB.CannotDeleteLoanByDefault = m_cannotDeleteLoanByDefault.Checked;
                brokerDB.CannotCreateLoanTemplateByDefault = m_cannotCreateLoanTemplateByDefault.Checked;
                brokerDB.HasLenderDefaultFeatures = m_hasLenderDefaultFeatures.Checked;

                brokerDB.AllowNonExcludableDiscountPoints = m_allowNonExcludableDiscountPoints.Checked;

                //if the Price My Loan, Pricing Engine enabled box is checked, Lender default features are turned on automatically
                if (m_enableFeaturePriceMyLoanCB.Checked)
                    brokerDB.HasLenderDefaultFeatures = true;

                brokerDB.IsBlankTemplateInvisibleForFileCreation = !m_allowCreatingLoanFromBlankTemplate.Checked;
                brokerDB.IsRoundUpLpeRateTo1Eighth = m_isRoundUpLpeRateTo1Eighth.Checked;
                brokerDB.HasManyUsers = m_hasManyUsers.Checked;
                //brokerDB.IsTrackModifiedFile = m_isTrackModifiedFile.Checked;
                brokerDB.IsShowPriceGroupInEmbeddedPml = m_isShowPriceGroupInEmbeddedPml.Checked;
                brokerDB.IsLpeManualSubmissionAllowed = m_isLpeManualSubmissionAllowed.Checked;
                brokerDB.IsPmlPointImportAllowed = m_isPmlPointImportAllowed.Checked;
                brokerDB.AllowPmlUserOrderNewCreditReport = m_AllowPmlOrderNewCreditReport.Checked;
                brokerDB.PmlRequireEstResidualIncome = m_PmlRequireEstResidualIncome.Checked;
                brokerDB.IsAllPrepaymentPenaltyAllowed = !m_isAllPrepaymentPenaltyAllowed.Checked;
                brokerDB.IsLpeDisqualificationEnabled = m_isLpeDisqualificationEnabled.Checked;
                brokerDB.RequireVerifyLicenseByState = m_RequireVerifyLicenseByState.Checked;
                brokerDB.RoundUpLpeFee = m_roundUpLpeFee.Checked;
                brokerDB.RoundUpLpeFeeInterval = decimal.Parse(m_roundUpLpeFeeInterval.Text);
                brokerDB.IsOptionARMUsedInPml = m_isOptionARMUsedInPml.Checked;
                brokerDB.IsStandAloneSecondAllowedInPml = m_isStandAloneSecondAllowedInPml.Checked;
                brokerDB.Is8020ComboAllowedInPml = m_is8020ComboAllowedInPml.Checked;
                brokerDB.DDLSpecialConfigXmlContent = m_ddlXmlConfig.Text;
                brokerDB.UseFHATOTALProductionAccount = m_UseFHATOTALProductionAccount.Checked;
                brokerDB.IsDisplayCompensationChoiceInPml = m_IsDisplayCompensationChoiceInPml.Checked;
                brokerDB.IsHideLOCompPMLCert = m_IsHideLOCompPMLCert.Checked;
                brokerDB.IsHidePricingwithoutLOCompPMLCert = m_IsHidePricingwithoutLOCompPMLCert.Checked;

                brokerDB.IsBestPriceEnabled = m_isBestPriceEnabled.Checked; // av 22180 05 28 08 

                brokerDB.IsDisplayRateOptionsAboveLowestTriggeringMaxYSP = m_IsDisplayRateOptionsAboveLowestTriggeringMaxYSP.Checked;
                brokerDB.IsUseLayeredFinancing = m_IsUseLayeredFinancing.Checked;
                brokerDB.EnableAutoPriceEligibilityProcess = m_EnableAutoPriceEligibilityProcess.Checked;
                brokerDB.EnableRetailTpoPortalMode = m_EnableRetailTpoPortalMode.Checked;
                brokerDB.SendAutoLockToLockDesk = m_SendAutoLockToLockDesk.Checked;
                brokerDB.SaveLockConfOnAutoLock = m_SaveLockConfOnAutoLock.Checked; //opm 184181
                brokerDB.EnableRenovationLoanSupport = m_EnableRenovationLoanSupport.Checked;
                brokerDB.EnableLqbMobileApp = this.EnableLqbMobileApp.Checked;

                brokerDB.EnableDuplicateEDocProtectionChecking = m_EnableDuplicateEDocProtectionChecking.Checked;

                brokerDB.IsRemovePreparedDates = m_IsRemovePreparedDates.Checked;
                brokerDB.IsProtectDisclosureDates = m_IsProtectDisclosureDates.Checked;
                brokerDB.IsEnableDTPkgBsdRedisclosureDatePop = m_IsEnableDTPkgBsdRedisclosureDatePop.Checked;

                brokerDB.IsB4AndB6DisabledIn1100And1300OfGfe = m_IsB4AndB6DisabledIn1100And1300OfGfe.Checked;

                brokerDB.EnableCocRedisclosureTriggersForHelocLoans = m_EnableCocRedisclosureTriggersForHelocLoans.Checked;
                brokerDB.UseCustomCocFieldList = m_UseCustomCocFieldList.Checked;

                brokerDB.IsKivaIntegrationEnabled = this.IsKivaIntegrationEnabled.Checked;
                if (!isNeedSetupDefaultValues)
                {
                    if (m_EnableConversationLogForLoans.Checked)
                    {
                        brokerDB.EnableConversationLog(m_EnableConversationLogForLoansInTpo.Checked);
                    }
                    else //if !m_EnableConversationLogForLoans.Checked
                    {
                        if (!m_EnableConversationLogForLoansInTpo.Checked)
                        {
                            brokerDB.DisableConversationLog();
                        }
                        else // if m_EnableConversationLogForLoansInTpo.Checked
                        {
                            var message = "Can't disable conversation log in loan editor and enable in tpo.";
                            throw new CBaseException(message, message);
                        }
                    }
                }
                else
                {
                    // if it is new, we will first attempt to give some default permission levels and categories, then enable it.
                }

                brokerDB.ShowUnderwriterInPMLCertificate = m_ShowUnderwriterInPMLCertificate.Checked; //OPM 12711
                brokerDB.ShowJuniorUnderwriterInPMLCertificate = m_ShowJuniorUnderwriterInPMLCertificate.Checked; // 11/21/2013 gf - opm 145015
                brokerDB.ShowProcessorInPMLCertificate = m_ShowProcessorInPMLCertificate.Checked;	  //OPM 12711
                brokerDB.ShowJuniorProcessorInPMLCertificate = m_ShowJuniorProcessorInPMLCertificate.Checked; // 11/21/2013 gf - opm 145015

                brokerDB.IsAlwaysUseBestPrice = m_IsAlwaysUseBestPrice.Checked; //OPM 24214
                brokerDB.IsDataTracIntegrationEnabled = m_IsDataTracIntegrationEnabled.Checked; // OPM 24590
                brokerDB.IsExportPricingInfoTo3rdParty = m_IsExportPricingInfoTo3rdParty.Checked; // OPM 34444
                brokerDB.PathToDataTrac = m_pathToDataTrac.Text; // OPM 24590
                brokerDB.DataTracWebserviceURL = m_dataTracWebserviceURL.Text; // OPM 25798

                brokerDB.IsPricingMultipleAppsSupported = m_IsPricingMultipleAppsSupported.Checked; //opm 33530 fs 07/29/09;
                brokerDB.IsaBEmailRequiredInPml = m_IsaBEmailRequiredInPml.Checked; //opm 33208 fs 08/21/09;
                //brokerDB.IsAllowSharedPipeline = m_IsAllowSharedPipeline.Checked; //opm 34381 fs 08/31/09

                brokerDB.IsIncomeAssetsRequiredFhaStreamline = m_IsIncomeAssetsRequiredFhaStreamline.Checked; // opm 42579 fs 11/17/09

                brokerDB.IsEncompassIntegrationEnabled = m_IsEncompassIntegrationEnabled.Checked; // opm 45012 fs 01/21/10


                brokerDB.IsForceChangePasswordForPml = m_IsForceChangePasswordForPml.Checked; // opm 34274

                brokerDB.IsShowRateOptionsWorseThanLowerRate = m_IsShowRateOptionsWorseThanLowerRate.Checked;

                brokerDB.IsEDocsEnabled = m_IsEDocsEnabled.Checked;
                brokerDB.IsDocuTechEnabled = m_IsDocuTechEnabled.Checked;
                brokerDB.IsDocuTechStageEnabled = m_IsDocuTechStageEnabled.Checked;

                Guid? ocrVendorId = null;

                if (this.OCRVendors.SelectedValue != Guid.Empty.ToString())
                {
                    ocrVendorId = new Guid(this.OCRVendors.SelectedValue);             
                }

                if (brokerDB.OCRVendorId != ocrVendorId)
                {
                    brokerDB.OCRVendorId = ocrVendorId;
                    brokerDB.OCRUsername = string.Empty;
                    brokerDB.OCRPassword = string.Empty;
                }

                brokerDB.IsDay1DayOfRateLock = m_IsDay1DayOfRateLock.Checked;
                brokerDB.RateLockExpirationWeekendHolidayBehavior = (E_RateLockExpirationWeekendHolidayBehavior)Tools.GetRadioButtonListValue(m_rateLockExpirationWeekendHolidayBehavior);

                brokerDB.IsAutoGenerateTradeNums = m_IsAutoGenerateTradeNums.Checked;

                brokerDB.ActiveDirectoryURL = m_sActiveDirectoryUrl.Text; // OPM 106855
                brokerDB.LinkedLoanUpdateFieldsString = this.LinkedLoanUpdateFieldsString.Text;

                string TradeCounterText = m_TradeCounter.Text;
                long counter;
                if (!string.IsNullOrEmpty(TradeCounterText))
                {
                    if (long.TryParse(TradeCounterText, out counter))
                    {
                        brokerDB.TradeCounter = counter - 1;
                    }
                    else
                    {
                        var msg = "Trade Number Counter value must be numeric.";
                        throw new CBaseException(msg, msg);
                    }
                }

                string PoolCounterText = m_PoolCounter.Text;
                long poolCounter;
                if (!string.IsNullOrEmpty(PoolCounterText))
                {
                    if (long.TryParse(PoolCounterText, out poolCounter))
                    {
                        brokerDB.PoolCounter = poolCounter - 1;
                    }
                    else
                    {
                        var msg = "Pool ID Counter value must be numeric.";
                        throw new CBaseException(msg, msg);
                    }
                }

                if (m_NamingCounter.Text == "0")
                    brokerDB.NamingCounter = 0;

                if (m_NHCKey.Text != "")
                    brokerDB.NHCKey = new Guid(m_NHCKey.Text);
                else
                    brokerDB.NHCKey = Guid.Empty;

                if (m_isLONRBL.SelectedIndex == 1)
                    brokerDB.HasLONIntegration = true;
                else
                    brokerDB.HasLONIntegration = false;

                if (m_displayPmlFeeIn100Format.SelectedIndex == 1)
                    brokerDB.DisplayPmlFeeIn100Format = true;
                else
                    brokerDB.DisplayPmlFeeIn100Format = false;

                brokerDB.Notes = m_Notes.Text;
                brokerDB.TempOptionXmlContent = TempOptionXmlContent.Text;

                brokerDB.BillingVersion = (E_BrokerBillingVersion)Enum.Parse(typeof(E_BrokerBillingVersion), m_ddlBillingVersion.SelectedValue);
                if (brokerDB.BillingVersion == E_BrokerBillingVersion.PerTransaction)
                {
                    brokerDB.IsEnablePTMDocMagicSeamlessInterface = m_IsEnablePTMDocMagicSeamlessInterface.Checked;
                    if (IsDocVendorListDirty.Value == bool.TrueString)
                    {
                        UpdateDocVendorList();
                        brokerDB.AllActiveDocumentVendors = EnabledDocVendors;
                    }

                    brokerDB.IsEnableComplianceEagleIntegration = IsEnableComplianceEagleIntegration.Checked;
                    brokerDB.IsEnablePTMComplianceEagleAuditIndicator = IsEnablePTMComplianceEagleAuditIndicator.Checked;
                    brokerDB.ComplianceEagleUserName = ComplianceEagleLogin.Text;
                    brokerDB.ComplianceEagleCompanyID = ComplianceEagleCompanyID.Text;
                    brokerDB.ComplianceEagleEnableMismo34 = this.ComplianceEagleEnableMismo34.Checked;
                    if (!string.IsNullOrEmpty(ComplianceEaglePassword.Text))
                    {
                        brokerDB.ComplianceEaglePassword = ComplianceEaglePassword.Text;
                    }
                }

                brokerDB.EnableKtaIntegration = this.EnableKtaIntegration.Checked;
                brokerDB.IsEnableComplianceEaseIntegration = this.IsEnableComplianceEaseIntegration.Checked;
                brokerDB.IsEnablePTMComplianceEaseIndicator = m_IsEnablePTMComplianceEaseIndicator.Checked;
                brokerDB.ComplianceEaseUserName = m_CELogin.Text.TrimWhitespaceAndBOM();
                if (!string.IsNullOrEmpty(m_CEPassword.Text))
                {
                    brokerDB.ComplianceEasePassword = m_CEPassword.Text;
                }

                if (PmlLoanTemplateId.SelectedItem.Value != Guid.Empty.ToString())
                {
                    brokerDB.PmlLoanTemplateID = new Guid(PmlLoanTemplateId.SelectedItem.Value);
                }
                else
                {
                    brokerDB.PmlLoanTemplateID = Guid.Empty;
                }
                if (m_cbEnableDriveIntegration.Checked != brokerDB.IsEnableDriveIntegration)
                {
                    brokerDB.IsEnableDriveIntegration = m_cbEnableDriveIntegration.Checked;
                }
                if (m_cbEnableDriveIntegration.Checked)
                {
                    if (m_cbEnableDriveConditionImporting.Checked)
                    {
                        if (m_ddlDriveConditionCategory.SelectedValue != "")
                        {
                            brokerDB.DriveConditionCategoryID = Int32.Parse(m_ddlDriveConditionCategory.SelectedValue);
                        }
                        if (!string.IsNullOrEmpty(DriveDueDateCalcField.Value))
                        {
                            brokerDB.DriveDueDateCalculatedFromField = DriveDueDateCalcField.Value;
                            brokerDB.DriveDueDateCalculationOffset = Int32.Parse(DriveDueDateOffset.Value);
                        }
                        if (!string.IsNullOrEmpty(DrivePermissionLevelID.Value))
                        {
                            brokerDB.DriveTaskPermissionLevelID = Int32.Parse(DrivePermissionLevelID.Value);
                        }
                    }
                    if (!string.IsNullOrEmpty(m_selectedDriveDocType.Value))
                    {
                        brokerDB.DriveDocTypeID = Int32.Parse(m_selectedDriveDocType.Value);
                    }
                    if (m_cbEnableDriveConditionImporting.Checked != brokerDB.IsEnableDriveConditionImporting)
                    {
                        brokerDB.IsEnableDriveConditionImporting = m_cbEnableDriveConditionImporting.Checked;
                    }
                }

                brokerDB.IsCenlarEnabled = this.IsCenlarEnabled.Checked;

                #region Flood Integration //OPM 12758
                if (m_cbEnableFloodIntegration.Checked != brokerDB.IsEnableFloodIntegration)
                {
                    brokerDB.IsEnableFloodIntegration = m_cbEnableFloodIntegration.Checked;
                }
                if (m_cbEnableFloodIntegration.Checked)
                {
                    if (m_ddlFloodProvider.SelectedValue != "")
                    {
                        brokerDB.FloodProviderID = short.Parse(m_ddlFloodProvider.SelectedValue);
                    }

                    brokerDB.FloodResellerId = new Guid(m_ddlFloodReseller.SelectedValue);

                    if (!string.IsNullOrEmpty(m_selectedFloodDocType.Value))
                    {
                        brokerDB.FloodDocTypeID = Int32.Parse(m_selectedFloodDocType.Value);
                    }
                    if (m_cbIsCCPmtRequiredForFloodIntegration.Checked != brokerDB.IsCCPmtRequiredForFloodIntegration)
                    {
                        brokerDB.IsCCPmtRequiredForFloodIntegration = m_cbIsCCPmtRequiredForFloodIntegration.Checked;
                    }
                    if (!string.Equals(FloodIntegrationAccountID.Text, brokerDB.FloodAccountId))
                    {
                        brokerDB.FloodAccountId = FloodIntegrationAccountID.Text;
                    }
                }
                #endregion

                #region DU Import Options //OPM 94140
                brokerDB.ImportAusFindingsByDefault = m_IsImportDuFindingsByDefault.Checked;
                brokerDB.ImportAus1003ByDefault = m_IsImportDu1003ByDefault.Checked;
                brokerDB.ImportAusCreditReportByDefault = m_IsImportDuCreditReportByDefault.Checked;
                #endregion
                #region Appraisal Integrations //OPM 108957
                var enabledVendors = new List<Guid>();
                var disabledVendors = new List<Guid>();
                bool anyVendorEnabled = false;
                foreach (RepeaterItem item in m_AppraisalIntegrations.Items)
                {
                    if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
                    {
                        continue;
                    }
                    var field = (HiddenField)item.FindControl("m_vendorId");
                    var vendorId = new Guid(field.Value);

                    var cb = (CheckBox)item.FindControl("m_EnableAppraisalVendor");
                    if (cb.Checked && !AppraisalIntegrationsForBroker.Contains(vendorId))
                    {
                        enabledVendors.Add(vendorId);
                        m_AppraisalIntegrationsForBroker.Add(vendorId);
                    }
                    else if (!cb.Checked && AppraisalIntegrationsForBroker.Contains(vendorId))
                    {
                        disabledVendors.Add(vendorId);
                        m_AppraisalIntegrationsForBroker.Remove(vendorId);
                    }
                    anyVendorEnabled = anyVendorEnabled || cb.Checked;
                }
                AppraisalVendorConfig.EnableVendorsForBroker(enabledVendors, m_brokerId);
                AppraisalVendorConfig.DisableVendorsForBroker(disabledVendors, m_brokerId);

                brokerDB.IsAllowOrderingGlobalDmsAppraisals = anyVendorEnabled;
                #endregion

                brokerDB.CalculateclosingCostInPML = m_CalculateclosingCostInPML.Checked;
                brokerDB.ApplyClosingCostToGFE = m_ApplyClosingCostsToGfeOnSubmission.Checked;

                #region Sequential Numbering OPM 118600
                brokerDB.EnableSequentialNumbering = m_EnableSequentialNumbering.Checked;
                brokerDB.SequentialNumberingFieldId = m_SequentialNumberingFieldId.Text;
                var seedText = m_SequentialNumberingSeed.Text;
                long seed;
                if (!string.IsNullOrEmpty(seedText))
                {
                    if (long.TryParse(seedText, out seed))
                    {
                        brokerDB.SequentialNumberingSeed = seed;
                    }
                    else
                    {
                        var msg = "Sequential numbering seed value must be numeric.";
                        throw new CBaseException(msg, msg);
                    }
                }
                #endregion

                if (m_IsEnabledLockPeriodDropdown.Checked)
                {
                    string[] options = new string[ConstApp.BROKER_MAX_NUMBER_OF_LOCK_PERIODS];

                    string option = "";
                    for (int i = 1; i <= options.Length; i++)
                    {
                        option = ((TextBox)Page.FindControl("m_AvailableLockPeriod_" + i)).Text;
                        if (option.TrimWhitespaceAndBOM() != "")
                        {
                            var val = UInt32.Parse(option);
                            if (val == 0)
                            {
                                options[i - 1] = "";
                            }
                            else
                            {
                                options[i - 1] = val.ToString();
                            }
                        }
                        else
                        {
                            options[i - 1] = "";
                        }
                    }
                    brokerDB.AvailableLockPeriodOptionsCSV = string.Join(",", options);
                }
                else
                {
                    brokerDB.AvailableLockPeriodOptionsCSV = "";
                }

                if (!isNeedSetupDefaultValues)
                {
                brokerDB.Pml2AsQuickPricerMode = (E_Pml2AsQuickPricerMode)Tools.GetDropDownListValue(m_Pml2AsQuickPricerMode);
                }

                brokerDB.SelectedNewLoanEditorUIPermissionType = (E_PermissionType)Tools.GetDropDownListValue(m_SelectedNewLoanEditorUIPermissionType);

                Guid actualPricingBrokerId = Guid.Empty;
                if (Guid.TryParse(m_ActualPricingBrokerId.Text, out actualPricingBrokerId))
                {
                    brokerDB.ActualPricingBrokerId = actualPricingBrokerId;
                }

                // Now save the broker's new state to the database.  What you
                // see in the ui is what you get on disk.
                bool checkPTMFolders = false;

                DbConnectionInfo dbConnectionInfo = null;

                if (brokerDB.IsNew == false)
                {
                    dbConnectionInfo = DbConnectionInfo.GetConnectionInfo(brokerDB.BrokerID);
                }
                else
                {
                    dbConnectionInfo = DbConnectionInfo.ListAll().First(o => o.Id == new Guid(m_databaseList.SelectedValue));
                }

                using (CStoredProcedureExec exec = new CStoredProcedureExec(dbConnectionInfo))
                {
                    try
                    {
                        // Open a transaction for writing.  If we fail, we need
                        // to have all saves get undone.
                        exec.BeginTransactionForWrite();

                        // Start by saving the broker itself.  We need this to
                        // complete so we can get the broker's id if he is just
                        // being created for the first time.

                        if (brokerDB.Save(exec) == false)
                        {
                            throw new CBaseException(ErrorMessages.FailedToSaveBroker, ErrorMessages.FailedToSaveBroker);
                        }

                        m_brokerId = brokerDB.BrokerID;

                        if (brokerDB.BillingVersion == E_BrokerBillingVersion.PerTransaction)
                        {
                            checkPTMFolders = true;
                        }
                        
                        m_PmlSiteID.Text = brokerDB.PmlSiteID.ToString();
                        m_ID.Text = brokerDB.BrokerID.ToString();

                        DeleteFeatureSubscription(exec, brokerDB.BrokerID, ConstApp.Feature_PricingEngine);
                        CreateFeatureSubscription(exec, brokerDB.BrokerID, ConstApp.Feature_PricingEngine, (m_enableFeaturePriceMyLoanCB.Checked ? true : false));

                        DeleteFeatureSubscription(exec, brokerDB.BrokerID, ConstApp.Feature_PriceMyLoan);
                        CreateFeatureSubscription(exec, brokerDB.BrokerID, ConstApp.Feature_PriceMyLoan, (m_enableFeaturePriceMyLoanCB.Checked ? true : false));

                        DeleteFeatureSubscription(exec, brokerDB.BrokerID, ConstApp.Feature_MarketingTools);
                        CreateFeatureSubscription(exec, brokerDB.BrokerID, ConstApp.Feature_MarketingTools, (m_enableFeatureMarketingToolsCB.Checked ? true : false));

                        exec.CommitTransaction();

                    }
                    catch (Exception e)
                    {
                        // 9/3/2004 kb - We remove any reference to the broker's id
                        // if it is new and failed to be created.  If you leave the
                        // view state's version of the id intact, even though the
                        // broker doesn't exist in the database, you'll trick the
                        // page into thinking the broker is an existing one.

                        exec.RollbackTransaction();

                        if (brokerDB.IsNew == true)
                        {
                            // 9/3/2004 kb - We remove any reference to the broker's id
                            // if it is new and failed to be created.  If you leave the
                            // view state's version of the id intact, even though the
                            // broker doesn't exist in the database, you'll trick the
                            // page into thinking the broker is an existing one.

                            ViewState.Remove("brokerid");
                        }

                        Tools.LogError(e);

                        throw;
                    }
                }

                if (isNeedSetupDefaultValues)
                {
                    DbConnectionInfo.AddConnectionInfoMapping(new Guid(m_databaseList.SelectedValue), brokerDB.BrokerID, brokerDB.CustomerCode);

                    var defaultValueSetter = new BrokerDefaultValueSetter(
                        brokerId:brokerDB.BrokerID,
                        editingUserId:this.CurrentPrincipal.UserId,
                        pmlMode: (E_Pml2AsQuickPricerMode)Tools.GetDropDownListValue(m_Pml2AsQuickPricerMode),
                        enableConversationLogForLoans:this.m_EnableConversationLogForLoans.Checked,
                        enableConversationLogInTpo:this.m_EnableConversationLogForLoansInTpo.Checked);

                    var errorLog = defaultValueSetter.SetDefaultValues();

                    if (!string.IsNullOrWhiteSpace(errorLog))
                    {
                        ClientScript.RegisterStartupScript(
                            typeof(BrokerEdit),
                            key: "BrokerDefaultValueError",
                            script: $"alert('Created broker but could not set the following defaults: {errorLog}');",
                            addScriptTags: true);
                    }
                }

                if (m_IsEDocsEnabled.Checked)
                {
                    using (var executor = new CStoredProcedureExec(brokerDB.BrokerID))
                    {
                        executor.BeginTransactionForWrite();

                        try
                        {
                            EDocumentFolder.CreateUnclassifiedFolderAndSave(brokerDB.BrokerID, executor);

                            executor.CommitTransaction();
                        }
                        catch
                        {
                            executor.RollbackTransaction();

                            throw;
                        }
                    }
                }

                bool hasDmEnabled = brokerDB.AllActiveDocumentVendors.Exists(p => p.VendorId == Guid.Empty);
                if (checkPTMFolders && (m_IsEnablePTMComplianceEaseIndicator.Checked || hasDmEnabled))
                {
                    EDocumentDocType edocs = new EDocumentDocType();
                    var docTypesInBrokerById = EDocumentDocType.GetDocTypesByBroker(m_brokerId, E_EnforceFolderPermissions.True).ToLookup(p => p.Folder.FolderId);

                    if (hasDmEnabled)
                    {
                        int docMagicFolderId = EDocumentFolder.CreateAndGetDocMagicFolder(m_brokerId);
                        string[] dmDocTypes = new string[]
                    {
                        "GENERATEDDOCUMENTS",
                        "APRPAYMENTSCHEDULE",
                        "IMPOUNDANALYIS",
                        "SECTION 32",
                        "GFE COMPARISON",
                        "ESIGNEDDOCUMENTS"
                    };

                        List<DocType> dmCurentDocTypes = docTypesInBrokerById[docMagicFolderId].ToList();

                        foreach (string dmDocType in dmDocTypes)
                        {
                            if (dmCurentDocTypes.Where(p => p.DocTypeName.Equals(dmDocType, StringComparison.OrdinalIgnoreCase)).Any())
                            {
                                continue;
                            }

                            Tools.LogInfo("Creating doctyype " + dmDocType);

                            edocs.AddDocType(dmDocType, docMagicFolderId, m_brokerId);
                            if (dmDocType == dmDocTypes[0])
                            {
                                bool isDeleted;
                                int id = DocType.GetDocTypeIdFromName(m_brokerId, dmDocType, out isDeleted);
                                BrokerDB tempBroker = BrokerDB.RetrieveByIdForceRefresh(m_brokerId);
                                tempBroker.DocMagicDefaultDocTypeID = id;
                                tempBroker.Save();
                            }
                        }
                    }

                    if (m_IsEnablePTMComplianceEaseIndicator.Checked)
                    {
                        int complianceEaseId = EDocumentFolder.CreateAndGetComplianceEaseFolder(m_brokerId);
                        string[] ceDocTypes = new string[]
                    {
                        "COMPLIANCEREPORT"
                    };

                        List<DocType> ceCurentDocTypes = docTypesInBrokerById[complianceEaseId].ToList();
                        foreach (string ceDocType in ceDocTypes)
                        {
                            if (ceCurentDocTypes.Where(p => p.DocTypeName.Equals(ceDocType, StringComparison.OrdinalIgnoreCase)).Any())
                            {
                                continue;
                            }
                            Tools.LogInfo("Creating doctyype " + ceDocType);
                            edocs.AddDocType(ceDocType, complianceEaseId, m_brokerId);
                        }
                    }
                }

                if (m_brokerId != Guid.Empty)
                {
                    Guid craId = new Guid(CreditProxy_CraId.SelectedItem.Value);
                    Guid crAccProxyId = Request.Form["CreditProxy_CrAccProxyId"].ToNullable<Guid>(Guid.TryParse) ?? Guid.Empty;

                    if (craId == Guid.Empty && crAccProxyId != Guid.Empty)
                    {
                        // Delete Existing Mapping.
                        CreditReportAccountProxy.Helper.DeleteCreditProxy(m_brokerId, crAccProxyId);
                    }
                    else if (craId != Guid.Empty)
                    {
                        CreditReportAccountProxy proxy;
                        if (crAccProxyId == Guid.Empty)
                        {
                            proxy = new CreditReportAccountProxy(this.m_brokerId);
                        }
                        else
                        {
                            proxy = CreditReportAccountProxy.Helper.RetrieveCreditProxyByProxyID(this.m_brokerId, crAccProxyId);
                        }

                        proxy.CraUserName = CreditProxy_CraUserName.Text;
                        proxy.CraPassword = CreditProxy_CraPassword.Text;
                        proxy.CrAccProxyDesc = CreditProxy_CrAccProxyDesc.Text;
                        proxy.CreditMornetPlusUserId = CreditProxy_CreditMornetPlusUserId.Text;
                        proxy.CreditMornetPlusPassword = CreditProxy_CreditMornetPlusPassword.Text;

                        proxy.CraId = new Guid(CreditProxy_CraId.SelectedItem.Value);
                        proxy.CraAccId = CreditProxy_CraAccId.Text;
                        proxy.IsExperianPulled = CreditProxy_IsExperianPulled.Checked;
                        proxy.IsEquifaxPulled = CreditProxy_IsEquifaxPulled.Checked;
                        proxy.IsTransUnionPulled = CreditProxy_IsTransunionPulled.Checked;
                        proxy.IsFannieMaePulled = CreditProxy_IsFannieMaePulled.Checked;
                        proxy.AllowPmlUserToViewReports = m_AllowPmlViewReport.Checked;
                        CreditReportAccountProxy.Helper.UpdateCreditProxyByBrokerID(proxy);
                    }
                }
                SaveCRAList();
                SaveFaxInformation();

                if (brokerDB.BrokerID != Guid.Empty)
                {
                    LenderDocuSignSettings brokerDocuSignSettings = LenderDocuSignSettings.Retrieve(brokerDB.BrokerID);
                    if (brokerDocuSignSettings == null)
                    {
                        brokerDocuSignSettings = new LenderDocuSignSettings
                        {
                            BrokerId = m_brokerId
                        };
                    }

                    brokerDocuSignSettings.IntegratorKey = docuSignIntegratorKey.Text;
                    brokerDocuSignSettings.RsaPrivateKey = docuSignPrivateRsaKey.Text;
                    brokerDocuSignSettings.SecretKey = docuSignSecretKey.Text;
                    brokerDocuSignSettings.Save();
                }

                //12-18-08 jk OPM 20030 - Saves Custom Ratesheet Expiration messages to the database
                String procedure = "";
                m_newOutsideNormalHoursAndPassInvestorCutOffMessage = m_newOutsideNormalHoursMessage.Text;

                if (IsPostBack)
                {
                    m_defaultNormalUserExpiredMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage, m_brokerId);
                    m_defaultLockDeskExpiredMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.ByPassAll, ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage, m_brokerId);
                    m_defaultNormalUserCutOffMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_CutOffMessage + "{0}", m_brokerId);
                    m_defaultLockDeskCutOffMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.ByPassAll, ConstAppDavid.BlockedRateLockSubmission_CutOffMessage + "{0}", m_brokerId);
                    m_defaultOutsideNormalHoursMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHour + "{0}", m_brokerId);
                    m_defaultOutsideClosureDayHourMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_OutsideClosureDayHour + "{0}", m_brokerId);
                    m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage = RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(Guid.Empty, E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff + "{0}" + "#" + "{1}", m_brokerId);
                }

                SqlParameter[] warningParameters = {
                                                   new SqlParameter("@Broker", m_brokerId)
                                               };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "getRSWarningMessageById", warningParameters))
                {
                    if (!reader.Read())
                    {
                        nothingLoaded = true;
                    }

                }

                if (m_newNormalUserExpiredMessage.Text.Equals(m_defaultNormalUserExpiredMessage))
                    m_newNormalUserExpiredMessage.Text = "";
                if (m_newLockDeskExpiredMessage.Text.Equals(m_defaultLockDeskExpiredMessage))
                    m_newLockDeskExpiredMessage.Text = "";
                if (m_newNormalUserCutOffMessage.Text.Equals(m_defaultNormalUserCutOffMessage))
                    m_newNormalUserCutOffMessage.Text = "";
                if (m_newLockDeskCutOffMessage.Text.Equals(m_defaultLockDeskCutOffMessage))
                    m_newLockDeskCutOffMessage.Text = "";
                if (m_newOutsideNormalHoursMessage.Text.Equals(m_defaultOutsideNormalHoursMessage))
                    m_newOutsideNormalHoursMessage.Text = "";
                if (m_newOutsideClosureDayHourMessage.Text.Equals(m_defaultOutsideClosureDayHourMessage))
                    m_newOutsideClosureDayHourMessage.Text = "";
                if (m_newOutsideNormalHoursAndPassInvestorCutOffMessage.Equals(m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage))
                    m_newOutsideNormalHoursAndPassInvestorCutOffMessage = "";

                if (nothingLoaded && m_newNormalUserExpiredMessage.Text.Equals("") && m_newLockDeskExpiredMessage.Text.Equals("") && m_newNormalUserCutOffMessage.Text.Equals("") && m_newLockDeskCutOffMessage.Text.Equals("") && m_newOutsideNormalHoursMessage.Text.Equals("") && m_newOutsideClosureDayHourMessage.Text.Equals("") && m_newOutsideNormalHoursAndPassInvestorCutOffMessage.Equals(""))
                {
                    nothingLoaded = false;
                }

                List<SqlParameter> parameters1 = new List<SqlParameter>();
                parameters1.Add(new SqlParameter("@Broker", m_brokerId));
                parameters1.Add(new SqlParameter("@NormalUserExpiredMsg", m_newNormalUserExpiredMessage.Text));
                parameters1.Add(new SqlParameter("@LockDeskExpiredMsg", m_newLockDeskExpiredMessage.Text));
                parameters1.Add(new SqlParameter("@NormalUserCutOffMsg", m_newNormalUserCutOffMessage.Text));
                parameters1.Add(new SqlParameter("@LockDeskCutOffMsg", m_newLockDeskCutOffMessage.Text));
                parameters1.Add(new SqlParameter("@OutsideNormalHrMsg", m_newOutsideNormalHoursMessage.Text));
                parameters1.Add(new SqlParameter("@OutsideClosureDayHrMsg", m_newOutsideClosureDayHourMessage.Text));
                //parameters1.Add(new SqlParameter("@OutsideNormalHrAndPassInvestorCutOffMsg", m_newOutsideNormalHoursAndPassInvestorCutOffMessage.Text));
                parameters1.Add(new SqlParameter("@OutsideNormalHrAndPassInvestorCutOffMsg", m_newOutsideNormalHoursMessage.Text));


                if (nothingLoaded)
                {
                    procedure = "CreateRSWarningMsgById";
                }
                else
                {
                    procedure = "UpdateRSWarningMsgById";
                }
                int result = 0;
                try
                {
                    result = StoredProcedureHelper.ExecuteNonQuery(m_brokerId, procedure, 1, parameters1);

                }
                catch (Exception exc)
                {
                    Tools.LogWarning(exc.Message);
                    result = 0;
                }

                if (m_brokerId != null)
                {
                    List<Guid> vendorIdList = new List<Guid>();
                    foreach (ListItem listItem in m_4506VendorList.Items)
                    {
                        if (listItem.Selected)
                        {
                            vendorIdList.Add(new Guid(listItem.Value));
                        }
                    }
                    Irs4506TVendorConfiguration.AssociateVendorWithBrokerId(m_brokerId, vendorIdList);

                    List<Guid> DRFPartnerIDList = new List<Guid>();
                    foreach (ListItem partner in m_DRFPartnerList.Items)
                    {
                        if (partner.Selected)
                        {
                            DRFPartnerIDList.Add(new Guid(partner.Value));
                        }
                    }
                    DataRetrievalPartner.AssociatePartnersWithBrokerId(m_brokerId, DRFPartnerIDList);

                    if (this.IsCenlarEnabled.Checked)
                    {
                        var cenlarConfig = new CenlarLenderConfiguration();
                        cenlarConfig.BrokerId = this.m_brokerId;
                        cenlarConfig.EnableAutomaticTransmissions = this.CenlarEnableAutomaticTransmissions.Checked;

                        if (!string.IsNullOrEmpty(this.CenlarCustomReportName.Text))
                        {
                            cenlarConfig.CustomReportName = this.CenlarCustomReportName.Text;
                        }

                        if (!string.IsNullOrEmpty(this.CenlarBatchExportName.Text))
                        {
                            cenlarConfig.BatchExportName = this.CenlarBatchExportName.Text;
                        }

                        if (!string.IsNullOrEmpty(this.CenlarDummyUserId.Text))
                        {
                            cenlarConfig.DummyUserId = Guid.Parse(this.CenlarDummyUserId.Text);
                        }

                        if (!string.IsNullOrEmpty(this.CenlarNotificationEmails.Text))
                        {
                            cenlarConfig.NotificationEmails = Tools.SplitEmailList(this.CenlarNotificationEmails.Text);
                        }

                        cenlarConfig.ShouldSuppressAcceptedLoanNotifications = this.CenlarShouldSuppressAcceptedLoanNotifications.Checked;
                        cenlarConfig.RemoveProblematicPayloadCharacters = this.CenlarRemoveProblematicPayloadCharacters.Checked;
                        CenlarLenderConfiguration.Save(cenlarConfig);
                    }
                }
            }
        }

        private void CreateFeatureSubscription(CStoredProcedureExec exec, Guid BrokerID, Guid FeatureID, bool IsActive)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerID),
                                            new SqlParameter("@FeatureID", FeatureID),
                                            new SqlParameter("@IsActive", IsActive)
                                        };

            exec.ExecuteNonQuery("CreateFeatureSubscription", 0, parameters);
        }

        private void DeleteFeatureSubscription(CStoredProcedureExec exec, Guid BrokerID, Guid FeatureID)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerID),
                                            new SqlParameter("@FeatureID", FeatureID)
                                        };
            exec.ExecuteNonQuery("DeleteFeatureSubscription", 0, parameters);	
        }

        private void LoadFaxInformation()
        {
            if( m_brokerId == Guid.Empty )
            {
                return; // do nothing
            }

            EDocsFaxNumber number = EDocsFaxNumber.RetrievePrivate(m_brokerId);
            if (number != null && number.HasNumber && !number.IsSharedLine && m_IsEDocsEnabled.Checked)
            {
                m_faxPrivate.Checked = true;
                m_faxShared.Checked = false;
                m_FaxNumber.Text = number.FaxNumber;
                m_DocRouterLogin.Text = number.DocRouterLogin;
                m_DocRouterPassword.Text = number.DocRouterPassword;
            }
            else
            {
                m_faxPrivate.Checked = false;
                m_faxShared.Checked = true;
            }
        }

        private void SaveFaxInformation()
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            if (m_brokerId == Guid.Empty)
            {
                return;	// do nothing
            }

            bool hasPrivateLine = m_faxPrivate.Checked;

            // We need to load the existing number first(if it exists) in order to keep the encryption key.
            EDocsFaxNumber number = EDocsFaxNumber.RetrievePrivate(m_brokerId);
            if (number == null)
            {
                number = new EDocsFaxNumber(m_brokerId, isSharedLine: false);
            }

            number.FaxNumber = hasPrivateLine ? m_FaxNumber.Text : string.Empty;
            number.DocRouterLogin = hasPrivateLine ? m_DocRouterLogin.Text : string.Empty;
            number.DocRouterPassword = hasPrivateLine ? m_DocRouterPassword.Text : string.Empty;

            number.Save();    
        }
            

        private void CreateUsageEvent(CStoredProcedureExec exec, Guid FeatureID, Guid UserID, Guid BrokerID, Guid WhoDoneIt, char EventType)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@FeatureID", FeatureID),
                                            new SqlParameter("@UserID", UserID),
                                            new SqlParameter("@BrokerID", BrokerID),
                                            new SqlParameter("@WhoDoneIt", WhoDoneIt),
                                            new SqlParameter("@EventType", EventType)
                                        };

            exec.ExecuteNonQuery("CreateUsageEvent", 0, parameters);
        }

        private void LoadCRAList() 
        {
            if( m_brokerId == Guid.Empty )
            {
                return ;	// do nothing
            }

            SqlParameter[] parameters = { new SqlParameter("@BrokerID", m_brokerId) };
            int index = 0;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerId, "ListBrokerCRA", parameters)) 
            {
                while (reader.Read()) 
                {
                    m_isPdfViewNeededList[index].Checked = (bool) reader["IsPdfViewNeeded"];
                    Tools.SetDropDownListValue(m_craList[index], reader["ServiceComId"].ToString());
                    index++;
                }
            }
        }

        private void SaveCRAList() 
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            if( m_brokerId == Guid.Empty )
            {
                return ;	// do nothing
            }

            SqlParameter[] deleteParameters = {
                                            new SqlParameter("@BrokerID", m_brokerId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(m_brokerId, "DeleteAllBrokerCRA", 0, deleteParameters);
            for (int index = 0; index < 10; index++) 
            {
                
                bool isPdfViewNeeded = m_isPdfViewNeededList[index].Checked;
                Guid craId = Guid.Empty;
                try 
                {
                    craId = new Guid(m_craList[index].SelectedItem.Value);
                } 
                catch {}

                try 
                {
                    if (craId != Guid.Empty) 
                    {
                        DateTime currentDateTime = DateTime.Now;
                        SqlParameter[] parameters = {
                                                        new SqlParameter("@BrokerID", m_brokerId),
                                                        new SqlParameter("@CreatedD", currentDateTime),
                                                        new SqlParameter("@ModifiedD", currentDateTime),
                                                        new SqlParameter("@ServiceComId", craId),
                                                        new SqlParameter("@IsPdfViewNeeded", isPdfViewNeeded)
                                                    };
                        StoredProcedureHelper.ExecuteNonQuery(m_brokerId, "CreateBrokerCRA", 0, parameters);
                    }
                } 
                catch 
                {
                    // 6/16/2005 dd - 
                    // Reason I ignore the exception is that if user enter in duplicate entry I will just ignore extra record.s
                }
            }
            LoadCRAList();
        }
        public void HandleCommand( string commandName , string[] commandArgs )
        {
            // Wait for postback to notify us that a command has
            // been embedded in the event target form element.
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

        protected void SaveClick( object sender , System.EventArgs a )
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            SaveData();
            m_Message.Text = "Data saved.";
            m_Message.ForeColor = Color.Blue;
            LoadData(); // Refresh the screen.

            BrokerFeatures brokerFT = new BrokerFeatures(m_brokerId);
        }

        protected void AddSampleReportsClick( object sender , System.EventArgs a )
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            // Handle adding of all sample reports.

            try
            {
                // Locate broker admin ids for looking up sample reports.
                object adminbrokerId = Tools.GetPmlDojoBrokerId();
                object adminreportId = Tools.GetPmlDojoBrokerReportQueryTemplateId();


                if( adminbrokerId == null )
                {
                    throw new InvalidOperationException( "Can't lookup admin broker account." );
                }

                if( adminreportId == null )
                {
                    throw new InvalidOperationException( "Can't locate blank admin report." );
                }

                // Get the admin broker id to use its assests as templates
                // for initializing this new broker.  We bail if the current
                // broker hasn't been saved yet.

                Guid ownerId = Guid.Empty , blankId , adminId;

                adminId = new Guid( adminbrokerId.ToString() );
                blankId = new Guid( adminreportId.ToString() );

                if( m_brokerId == Guid.Empty )
                {
                    m_Message.Text = "Please save this broker before finalizing.";
                    m_Message.ForeColor = Color.Red;

                    return;
                }

                // Fetch the current broker's current account owner employee.
                // We associate all new assets with this person.

                ArrayList reports = new ArrayList();
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", m_brokerId),
                                                new SqlParameter("@IncludeDisabledUsers", 1)
                                            };
                using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( m_brokerId, "ListReportQuery" , parameters ) )
                {
                    while( reader.Read() )
                    {
                        // Remember the broker's current report names.

                        reports.Add( reader[ "QueryName" ].ToString() );
                    }
                }

                parameters = new SqlParameter[] {
                    new SqlParameter("@BrokerID", m_brokerId)
                };
                using( IDataReader sR = StoredProcedureHelper.ExecuteReader( m_brokerId, "ListBrokersAccountOwners" , parameters ) )
                {
                    if( sR.Read() == true )
                    {
                        ownerId = ( Guid ) sR[ "EmployeeId" ];
                    }
                    else
                    {
                        throw new InvalidOperationException( "Can't find valid account owner." );
                    }
                }

                // We start by gathering the report queries associated with
                // this admin broker and copy them into the new broker's
                // report stash.

                int ncopied = 0;

                parameters = new SqlParameter[] {
                        new SqlParameter("@BrokerID", adminId),
                        new SqlParameter("@IncludeDisabledUsers", 1)
                };
                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(adminId, "ListReportQuery", parameters ))
                {
                    while( sR.Read() == true )
                    {
                        // Check if we already have one.  If so, skip it to
                        // keep from colliding or overwriting.

                        string name = sR[ "QueryName" ].ToString();

                        if( reports.Contains( name ) == true )
                        {
                            continue;
                        }

                        // Save a new query with the given name and a unique id.

                        Query query = sR[ "XmlContent" ].ToString(); Guid queryId = Guid.NewGuid();

                        if( query.Id == blankId )
                        {
                            // We don't copy the reserved blank report template.

                            continue;
                        }

                        query.Id = queryId;

                        SqlParameter[] parameters2 = {
                                                         new SqlParameter( "@BrokerID", m_brokerId)
                        , new SqlParameter( "@QueryID"    , queryId          )
                        , new SqlParameter( "@EmployeeID" , ownerId          )
                        , new SqlParameter( "@QueryName"  , name             )
                        , new SqlParameter( "@XmlContent" , query.ToString())
                                                     };

                        StoredProcedureHelper.ExecuteNonQuery( m_brokerId, "CreateReportQuery", 0, parameters2 );

                        ++ncopied;
                    }
                }

                // Updated the confirmation text display.

                m_Message.Text = ncopied + " reports copied.";
                m_Message.ForeColor = Color.Blue;
            }
            catch( InvalidOperationException e )
            {
                // Record the error.

                m_Message.Text = "Error: " + e.Message;
                m_Message.ForeColor = Color.Red;

                Tools.LogError( e );
            }
            catch( Exception e )
            {
                // Record the error.

                m_Message.Text = "Error: Failed to add sample reports.";
                m_Message.ForeColor = Color.Red;

                Tools.LogError( e );
            }
        }

        private void btnShowUsers_Click(object sender, System.EventArgs e)
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            SaveData() ;
            Response.Redirect("Employees.aspx?brokerid=" + m_brokerId.ToString()) ;
        }

        protected void BindAppraisalVendor(object sender, RepeaterItemEventArgs args)
        {
            var vendor = (AppraisalVendorConfig)args.Item.DataItem;
            var field = (HiddenField)args.Item.FindControl("m_vendorId");
            var cb = (CheckBox)args.Item.FindControl("m_EnableAppraisalVendor");
            field.Value = vendor.VendorId.ToString();
            cb.Checked = AppraisalIntegrationsForBroker.Contains(vendor.VendorId);
            cb.Text = vendor.VendorName;
        }

        /// <summary>
        /// Binds the list of document vendors in the page's view state to the ASP repeater in the UI.
        /// </summary>
        protected void BindDocVendorList()
        {
            DocVendorList.DataSource = EnabledDocVendors;
            DocVendorList.DataBind();
        }

        /// <summary>
        /// Retrieves the document vendor settings in the UI and saves them to the page's view state.
        /// </summary>
        protected void UpdateDocVendorList()
        {
            EnabledDocVendors = new List<DocumentVendorBrokerSettings>();

            foreach (RepeaterItem item in DocVendorList.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    var vendorSettings = new DocumentVendorBrokerSettings();

                    var docVendorId = (HiddenField)item.FindControl("DocConfigVendorId");
                    vendorSettings.VendorId = Guid.Parse(docVendorId.Value);

                    var login = (TextBox)item.FindControl("DocConfigLogin");
                    vendorSettings.Login = login != null ? login.Text : string.Empty;

                    var currentPassword = (HiddenField)item.FindControl("DocConfigCurrentPassword");
                    var newPassword = (TextBox)item.FindControl("DocConfigNewPassword");
                    if (newPassword != null && !string.IsNullOrEmpty(newPassword.Text))
                    {
                        vendorSettings.Password = newPassword.Text;
                    }
                    else
                    {
                        vendorSettings.EPassword = currentPassword != null ? currentPassword.Value : string.Empty;
                    }

                    var companyId = (TextBox)item.FindControl("DocConfigCompanyId");
                    vendorSettings.AccountId = companyId != null ? companyId.Text : string.Empty;

                    var servicesLinkName = (TextBox)item.FindControl("DocConfigServicesLinkName");
                    vendorSettings.ServicesLinkNm = servicesLinkName != null ? servicesLinkName.Text : string.Empty;

                    var useTestingEnvironment = (CheckBox)item.FindControl("DocConfigUseTestingEnvironment");
                    vendorSettings.DisclosureInTest = useTestingEnvironment != null ? useTestingEnvironment.Checked : false;

                    var enablePartnerBilling = (CheckBox)item.FindControl("DocConfigEnablePartnerBilling");
                    vendorSettings.DisclosureBilling = enablePartnerBilling != null ? enablePartnerBilling.Checked : false;

                    var enableForProductionLoans = (CheckBox)item.FindControl("DocConfigEnableForProductionLoans");
                    vendorSettings.IsEnableForProductionLoans = enableForProductionLoans != null ? enableForProductionLoans.Checked : false;

                    var enableOnlineInterface = (CheckBox)item.FindControl("DocConfigEnableOnlineInterface");
                    vendorSettings.DisclosureOnlineInterface = enableOnlineInterface != null ? enableOnlineInterface.Checked : false;

                    var enableForTestLoans = (CheckBox)item.FindControl("DocConfigEnableForTestLoans");
                    vendorSettings.IsEnableForTestLoans = enableForTestLoans != null ? enableForTestLoans.Checked : false;

                    var hasCustomPackageData = (CheckBox)item.FindControl("DocConfigHasCustomPackageData");
                    vendorSettings.HasCustomPackageData = hasCustomPackageData != null ? hasCustomPackageData.Checked : false;

                    var customPackageData = (TextBox)item.FindControl("DocConfigCustomPackageData");
                    vendorSettings.CustomPackageDataJSON = customPackageData != null ? customPackageData.Text : string.Empty;

                    EnabledDocVendors.Add(vendorSettings);
                }
            }
        }

        /// <summary>
        /// Handles a command from the ASP repeater holding the list of document vendors.
        /// </summary>
        /// <param name="sender">The DOM element calling this function.</param>
        /// <param name="args">Information about the command being made.</param>
        protected void CommandDocVendor(object sender, RepeaterCommandEventArgs args)
        {
            if (args.CommandName == "AddDocVendor")
            {
                var newVendor = new DocumentVendorBrokerSettings();
                var addVendorList = (DropDownList)args.Item.FindControl("DocListAddVendorList");
                newVendor.VendorId = Guid.Parse(addVendorList.SelectedValue);

                UpdateDocVendorList();
                EnabledDocVendors.Add(newVendor);
                BindDocVendorList();
            }
            else if (args.CommandName == "RemoveDocVendor")
            {
                var vendorSelection = (HiddenField)args.Item.FindControl("DocConfigVendorId");

                UpdateDocVendorList();
                var itemToDelete = EnabledDocVendors.First(v => v.VendorId == Guid.Parse(vendorSelection.Value));
                if (itemToDelete != null)
                {
                    EnabledDocVendors.Remove(itemToDelete);
                    BindDocVendorList();
                }
            }
        }

        /// <summary>
        /// Toggles whether the broker is using custom package data for the given vendor.
        /// </summary>
        /// <param name="sender">The "Use Custom Package Data" checkbox for this vendor.</param>
        /// <param name="e">Th event arguments.</param>
        protected void ToggleCustomPackageData(object sender, EventArgs e)
        {
            var useCustomPackageData = sender as CheckBox;
            var configurationRow = useCustomPackageData.Parent as RepeaterItem;

            var vendorId = (HiddenField)configurationRow.FindControl("DocConfigVendorId");
            var vendor = DocumentVendorFactory.AvailableVendors().First(v => v.VendorId == Guid.Parse(vendorId.Value));

            var customPackageData = (TextBox)configurationRow.FindControl("DocConfigCustomPackageData");
            var customPackageDataCache = (HiddenField)configurationRow.FindControl("DocConfigCustomPackageDataCache");

            if (useCustomPackageData.Checked)
            {
                customPackageData.Enabled = true;

                if (!string.IsNullOrWhiteSpace(customPackageDataCache.Value))
                {
                    customPackageData.Text = customPackageDataCache.Value;
                }
            }
            else
            {
                customPackageData.Enabled = false;

                if (!string.Equals(vendor.PackageDataListJSON, customPackageData.Text))
                {
                    customPackageDataCache.Value = customPackageData.Text;
                }

                customPackageData.Text = vendor.PackageDataListJSON;
            }
        }

        /// <summary>
        /// Binds a <see cref="DocumentVendorBrokerSettings"/> object to the Document Vendor List repeater.
        /// </summary>
        /// <param name="sender">The DOM element calling this method.</param>
        /// <param name="args">Information about the ASP repeater.</param>
        protected void BindDocVendor(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                var settings = (DocumentVendorBrokerSettings)args.Item.DataItem;
                VendorConfig vendor = DocumentVendorFactory.AvailableVendors().Where(v => v.VendorId == settings.VendorId).First();

                var docVendorName = (TextBox)args.Item.FindControl("DocConfigVendorName");
                docVendorName.Text = vendor.VendorName;
                docVendorName.ReadOnly = true;

                var docVendorId = (HiddenField)args.Item.FindControl("DocConfigVendorId");
                docVendorId.Value = settings.VendorId.ToString();

                var login = (TextBox)args.Item.FindControl("DocConfigLogin");
                login.Text = settings.Login;
                args.Item.FindControl("DocConfigLoginSection").Visible = vendor.UsesUsername || vendor.UsesPassword || vendor.UsesAccountId;

                var currentPassword = (HiddenField)args.Item.FindControl("DocConfigCurrentPassword");
                currentPassword.Value = settings.EPassword;
                args.Item.FindControl("DocConfigPasswordSection").Visible = vendor.UsesPassword;

                var companyId = (TextBox)args.Item.FindControl("DocConfigCompanyId");
                companyId.Text = settings.AccountId;
                args.Item.FindControl("DocConfigCompanyIdSection").Visible = vendor.UsesAccountId;

                var servicesLinkName = (TextBox)args.Item.FindControl("DocConfigServicesLinkName");
                servicesLinkName.Text = settings.ServicesLinkNm;

                var useTestingEnvironment = (CheckBox)args.Item.FindControl("DocConfigUseTestingEnvironment");
                useTestingEnvironment.Checked = settings.DisclosureInTest;
                args.Item.FindControl("DocConfigTestingEnvironmentSection").Visible = settings.VendorId != Guid.Empty;

                var enablePartnerBilling = (CheckBox)args.Item.FindControl("DocConfigEnablePartnerBilling");
                enablePartnerBilling.Checked = settings.DisclosureBilling;

                var enableForProductionLoans = (CheckBox)args.Item.FindControl("DocConfigEnableForProductionLoans");
                enableForProductionLoans.Checked = settings.IsEnableForProductionLoans;

                var enableOnlineInterface = (CheckBox)args.Item.FindControl("DocConfigEnableOnlineInterface");
                enableOnlineInterface.Checked = settings.DisclosureOnlineInterface;
                args.Item.FindControl("DocConfigEnableOnlineInterface").Visible = vendor.SupportsOnlineInterface;

                var enableForTestLoans = (CheckBox)args.Item.FindControl("DocConfigEnableForTestLoans");
                enableForTestLoans.Checked = settings.IsEnableForTestLoans;

                var hasCustomPackageData = (CheckBox)args.Item.FindControl("DocConfigHasCustomPackageData");
                hasCustomPackageData.Checked = settings.HasCustomPackageData;

                var customPackageData = (TextBox)args.Item.FindControl("DocConfigCustomPackageData");
                customPackageData.Text = settings.HasCustomPackageData ? settings.CustomPackageDataJSON : vendor.PackageDataListJSON;
                customPackageData.Enabled = settings.HasCustomPackageData;
            }
            else if (args.Item.ItemType == ListItemType.Footer)
            {
                var docVendorList = (DropDownList)args.Item.FindControl("DocListAddVendorList");

                // Only show vendors that are not yet configured in the ASP repeater.
                foreach (var vendor in DocumentVendorFactory.AvailableVendors())
                {
                    bool showVendor = true;
                    foreach (RepeaterItem item in DocVendorList.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            var itemVendorId = (HiddenField)item.FindControl("DocConfigVendorId");
                            if (itemVendorId.Value == vendor.VendorId.ToString())
                            {
                                showVendor = false;
                            }
                        }
                    }

                    if (showVendor)
                    {
                        docVendorList.Items.Add(new ListItem(vendor.VendorName, vendor.VendorId.ToString()));
                    }
                }
            }
        }

        [System.Web.Services.WebMethod]
        public static string[] CheckLinkedLoanUpdateFieldsString(string fieldString)
        {
            return PageDataUtilities.GetSettableFieldsFromString(fieldString);
        }

        protected void DownloadAffectedEConsentExpirationFiles_Click(object sender, EventArgs e)
        {
            var helper = new DisableEConsentMonitoringMigrationHelper(this.m_brokerId);
            var affectedFiles = helper.GetAffectedFileList();

            var disclosureNeedTypeAuditName = new Dictionary<E_sDisclosureNeededT, string>()
            {
                [E_sDisclosureNeededT.EConsentDec_PaperDiscReqd] = "E-Consent Declined - Paper Disclosure Required",
                [E_sDisclosureNeededT.EConsentNotReceived_PaperDiscReqd] = "E-Consent Not Received - Paper Disclosure Required",
                [E_sDisclosureNeededT.AwaitingEConsent] = "Awaiting E-Consent"
            };

            var tempFile = TempFileUtils.NewTempFile().Value;
            using (var writer = new StreamWriter(tempFile))
            {
                var headers = new[]
                {
                    "Loan Number",
                    "Need Initial Disclosures",
                    "Need Redisclosures",
                    "Disclosure Needed Type",
                    "Disclosures Due Date"
                };

                writer.WriteLine(string.Join(",", headers.Select(header => DataParsing.sanitizeForCSV(header))));

                foreach (var file in affectedFiles)
                {
                    writer.Write(DataParsing.sanitizeForCSV(file.sLNm) + ",");
                    writer.Write(file.sNeedInitialDisc + ",");
                    writer.Write(file.sNeedRedisc + ",");
                    writer.Write(DataParsing.sanitizeForCSV(disclosureNeedTypeAuditName[file.sDisclosureNeededT]) + ",");
                    writer.Write(DataParsing.sanitizeForCSV(file.sDisclosuresDueD_rep));
                    writer.WriteLine();
                }
            }

            RequestHelper.SendFileToClient(HttpContext.Current, tempFile, "application/csv", "AffectedEConsentMonitoringFiles.csv");

            this.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
}
