﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Security.Authorization.ConversationLog;
    using LendersOffice.ObjLib.Views;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    
    /// <summary>
    /// Page that allows editing the set of conversation log permissionLevels for a lender.
    /// </summary>
    public partial class ConversationLogPermissionLevelsEditor : SecuredAdminPage
    {
        /// <summary>
        /// Gets the passed in broker id.
        /// </summary>
        /// <value>The passed in broker id.</value>
        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("BrokerId");
            }
        }

        /// <summary>
        /// Saves the order of the permission levels as seen in the permission levels (plural) editor.
        /// </summary>
        /// <param name="orderedPermissionLevelIdsString">The json serialized form of the permission level ids in the UI.</param>
        [System.Web.Services.WebMethod]
        public static void SavePermissionLevelsOrdering(string orderedPermissionLevelIdsString)
        {
            Tools.LogAndThrowIfErrors(() =>
            {
                string brokerIdString = RequestHelper.GetSafeQueryString("BrokerId");
                var brokerId = BrokerIdentifier.Create(brokerIdString).Value;

                long[] orderedPermissionLevelIds = SerializationHelper.JsonNetDeserialize<long[]>(orderedPermissionLevelIdsString);
                ConversationLogPermissionLevelOrderingManager.SetOrderingForPermissionLevelsForBrokerId(brokerId, orderedPermissionLevelIds);
            });
        }

        /// <summary>
        /// Gets the required permissions to access this page.
        /// </summary>
        /// <returns>The permissions associated with this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker
            };
        }

        /// <summary>
        /// The Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.Response.Headers.Add("X-UA-Compatible", "IE=edge");
            this.m_loadLOdefaultScripts = true; // to stop loading `common.js` from `BasePage`
            this.EnableJquery = true;
            this.PageID = "ConversationLogPermissionLevelsEditor";
            this.PageTitle = "Conversation Log Permission Levels Editor";
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("stylesheet.css");
            this.RegisterJsScript("angular-1.4.8.min.js");

            this.titleHeader.InnerText = "Conversation Log Permission Levels for " + BrokerDB.RetrieveById(this.BrokerId).CustomerCode;
        }

        /// <summary>
        /// Event handler for page load event.
        /// </summary>
        /// <param name="sender">The object that triggered the load event.</param>
        /// <param name="e">The info about the load event.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var brokerId = BrokerIdentifier.Create(this.BrokerId.ToString()).Value;
            var permissionLevels = ConversationLogPermissionLevel.GetOrderedPermissionLevelsForBroker(brokerId);
            var viewOfPermissionLevels = permissionLevels.Select(p => ViewOfPermissionLevelForLoAdmin.FromPermissionLevel(p)).ToList();
            this.RegisterJsGlobalVariables("brokerId", this.BrokerId);
            this.RegisterJsGlobalVariables("stageName", ConstAppDavid.CurrentServerLocation.ToString("G"));
            this.RegisterJsObjectWithJsonNetSerializer<List<ViewOfPermissionLevelForLoAdmin>>("permissionLevels", viewOfPermissionLevels);
        }
    }
}