﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LOAdmin/loadmin.Master" AutoEventWireup="true" CodeBehind="FeatureAdoptionClientView.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.Features.FeatureAdoptionClientView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Feature Adoption Client View</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">       
    <style type="text/css">
        .table-responsive{
            width: 1000px;
            margin-left: auto;
            margin-right: auto;
        }
        .head{
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        var globalRactive;

        $j(function ($j) {            
            globalRactive = new Ractive({
                debug: true,
                append: true,
                template: '#PageTemplate',
                el: '#container',
                data: clientView,
                init:
                    function () {
                        var self = this;
                    },
            });
        });
    </script>

    <script id="PageTemplate" type="text/ractive">
        <h1 class="head">{{this[0].clientName}}</h1>
        <div class="table-responsive">
            <table class="table table-striped table-link-ws">
                <thead>
                    <tr>
                        <th>Priority</th>
                        <th>Feature name</th>
                        <th>Client type target</th>
                        <th>Goal</th>
                        <th>Meets goal?</th>
                    </tr>
                </thead>
                {{#this[0].featureResults:i}}
                    {{#if !isHidden}}
                    <tr>
                        <th>{{priority}}</th>
                        <th>{{featureName}}</th>
                        <th>{{clientTypeTarget}}</th>
                        <th>{{goal}}</th>
                        <th>{{meetsGoal}}</th>
                    <tr>
                    {{/if}}
                {{/#this}}
            </table>        
        </div>
    </script>

    <form id="FeatureAdoption" runat="server">
    <div id="container">
    </div>
    </form>    
</asp:Content>
