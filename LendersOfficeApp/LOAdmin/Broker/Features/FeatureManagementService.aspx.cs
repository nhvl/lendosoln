﻿namespace LendersOfficeApp.LOAdmin.Broker.Features
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;    

    /// <summary>
    /// The service page that will handle feature management.
    /// </summary>
    public partial class FeatureManagementService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Runs the service method called from the page.
        /// </summary>
        /// <param name="methodName">The method to run.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.Save):
                    this.Save();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, $"Method not implemented {methodName}");
            }
        }

        /// <summary>
        /// Save the FeatureManagement changes.
        /// </summary>
        private void Save()
        {
            List<FeatureData> featureDataList = SerializationHelper.JsonNetDeserialize<List<FeatureData>>(GetString("features"));

            FeatureData.CheckAndAddNewTrackedFeatures();

            foreach (FeatureData fd in featureDataList)
            {
                fd.UpdateTrackedFeature();
            }
        }
    }
}