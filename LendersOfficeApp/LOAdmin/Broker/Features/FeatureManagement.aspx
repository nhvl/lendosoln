﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LOAdmin/loadmin.Master" AutoEventWireup="true" CodeBehind="FeatureManagement.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.Features.FeatureManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <style type="text/css">
        .table-responsive{
            width: 1000px;
            margin-left: auto;
            margin-right: auto;
        }
        .head{
            text-align: center;
        }
        .modal-footer{
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        var globalRactive;
        var onChangedList;

        function Changed(index) {
            onChangedList[index] = true;
        }

        $j(function ($j) {           
            function submit(ractive, event, featureList) {
                var featureDataList = [];
                for (var i = 0; i < featureList.length; i++) {
                    if (onChangedList[i] === false) {
                        continue;
                    }
                    var priorityStr = featureList[i].Priority.toString();
                    if (!priorityStr.match("^[0-9]{1,3}(?:\.[0-9]{1,3})?$")){
                        alert("Priority must be of type decimal(6,3)");
                        return;
                    }
                    featureDataList.push({ featureType: featureList[i].FeatureType, priority: featureList[i].Priority, isHidden: featureList[i].IsHidden });
                }
                if (featureDataList.length === 0) {
                    alert("No changes have been made");
                    return;
                }
                var data = { features: JSON.stringify(featureDataList) };
                var result = gService.main.call("Save", data);
                if (!result.error) {
                    alert("Changes updated successfully");
                }
                else {
                    alert("Error while updating features");
                }
            }

            globalRactive = new Ractive({
                debug: true,
                append: true,
                template: '#PageTemplate',
                partials: '#PageLinksTemplate',
                el: '#container',
                data: features,
                init:
                    function () {
                        var self = this;

                        onChangedList = new Array(features.length);
                        for (var i = 0; i < onChangedList.length; i++) {
                            onChangedList[i] = false;
                        }

                        this.on("submit", function (event, featureList) {
                            submit(self, event, featureList);
                        });

                        this.on("cancel", function (event) {
                            location.reload();
                        });
                    },
            });
        });
    </script>

    <script id="PageTemplate" type="text/ractive">
        <form on-submit="save">
            <div class="table-responsive">
                <table class="table table-striped table-link-ws">
                    <thead>
                        <tr>
                            <th>Priority</th>
                            <th>Hide</th>
                            <th>Feature name</th>
                            <th>Client type target</th>
                            <th>Goal</th>
                        </tr>
                    </thead>
                    {{#this:i}}
                        <tr>
                            <th><input type="text" value="{{Priority}}" onchange="Changed({{i}})"></th>
                            <th><input type="checkbox" checked="{{IsHidden}}" onchange="Changed({{i}})"></th>
                            <th>{{FeatureName}}</th>
                            <th>{{ClientTypeTarget}}</th>
                            <th>{{Goal}}</th>
                        <tr>
                    {{/#this}}
                </table>   
                <div class="modal-footer">
                    <span class="clipper"><a on-click="cancel" href="javascript:void(0);" class="btn btn-default" data-dismiss="modal">Cancel</a></span>
                    <span class="clipper"><a on-click="submit:{{this}}" href="javascript:void(0);"><button type="submit" class="btn btn-primary">Save</button></a></span>
                </div>     
            </div>
        </form>
    </script>
    <h1 class="head">Feature Management</h1>
    <form id="FeatureAdoption" runat="server">
    <div id="container">
    </div>
    </form>   
</asp:Content>
