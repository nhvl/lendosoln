﻿namespace LendersOfficeApp.LOAdmin.Broker.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Page that shows feature management.
    /// </summary>
    public partial class FeatureAdoptionClientView : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Permission used for this page.
        /// </summary>
        private static readonly E_InternalUserPermissions[] BrokerListPermissions = new[] { E_InternalUserPermissions.FeatureAdoption };

        /// <summary>
        /// Gets the forced compatibility mode for the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode <see cref="E_XUAComaptibleValue.Edge"/>.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Return this page's required permissions set.  This
        /// test occurs with each page initialization.
        /// </summary>
        /// <returns>
        /// Array of required permissions.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.
            return BrokerListPermissions;
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Guid brokerId = RequestHelper.GetGuid("brokerid");
            BrokerDB db = BrokerDB.RetrieveById(brokerId);
            FeatureAdoptionClientViewModel clientView = FeatureAdoptionClientViewModel.GetFeatureAdoptionClientViewModel(db);
            List<FeatureAdoptionClientViewModel> clientViewModelList = new List<FeatureAdoptionClientViewModel>();
            clientViewModelList.Add(clientView);
            this.RegisterJsObjectWithJsonNetSerializer("clientView", clientViewModelList);
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args object.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterCSS("newdesign.main.css");
            this.RegisterCSS("newdesign.lendingqb.css");
            this.EnableJquery = true;
            this.RegisterJsScript("ractive-0.7.1.min.js");
        }
    }
}