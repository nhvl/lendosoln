﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LOAdmin/loadmin.Master" AutoEventWireup="true" CodeBehind="FeatureAdoption.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.Features.FeatureAdoption" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Feature Adoption</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <style type="text/css">
        th{
            vertical-align:top;
        }
        .width7{
            width:7%;
        }
        .width10{
            width:10%;
        }
        .width31{
            width:31%;
        }
    </style>
    <script type="text/javascript">
        var globalRactive;
        var sort = { by: "", asc: true };
        var property;

        $j(function ($j) {
            function ExpandClick(ractive, event) {
                var isCollapsed = ractive.get(event.keypath + ".IsCollapsed");
                if (isCollapsed) {
                    ractive.set(event.keypath + ".IsCollapsed", false);
                    event.node.innerText = "less...";
                }
                else {
                    ractive.set(event.keypath + ".IsCollapsed", true);
                    event.node.innerText = "more...";
                }
            }
            
            function mySortAsc(a,b)
            {
                return ((a[property] < b[property]) ? -1 : ((a[property] > b[property]) ? 1 : 0));
            }

            function Sort(ractive, event, featureList, sortBy) {
                property = sortBy;
                if (sort.asc) {
                    featureList.sort(mySortAsc);
                    sort.asc = false;
                }
                else {
                    featureList.sort(mySortAsc);
                    featureList.reverse();
                    sort.asc = true;
                }                
            }

            function CopyToClipboard(ractive, event, clientsList) {
                var strToCopy = "";

                for (var i = 0; i < clientsList.length; i++) {
                    strToCopy += clientsList[i].Item2;
                    if (i < clientsList.length - 1) {
                        strToCopy += "\n";
                    }
                }

                if (strToCopy == "") {
                    alert("Nothing to copy.");
                    return;
                }

                var x = document.createTextNode(strToCopy);

                var clipboardElement = document.createElement("textarea");

                clipboardElement.value = strToCopy;
                document.body.appendChild(clipboardElement);
                clipboardElement.select();

                try {
                    var result = document.execCommand("copy");

                    if (!result) {
                        alert("copy error");
                    }
                    else {
                        alert("copy successful");
                    }
                }
                catch (error) {
                    alert("copy unsucessful");
                }

                document.body.removeChild(clipboardElement);
            }

            globalRactive = new Ractive({
                debug: true,
                append: true,
                template: '#PageTemplate',
                partials: '#PageLinksTemplate',
                el: '#container',
                data: features,
                init:
                    function () {
                        var self = this;

                        this.on("expandClick", function (event) {
                            ExpandClick(self, event);
                        });

                        this.on("sort", function (event, featureList, sortBy) {
                            Sort(self, event, featureList, sortBy);
                        });

                        this.on("copyToClipboard", function (event, clientsMeetingGoal) {
                            CopyToClipboard(self, event, clientsMeetingGoal);
                        });

                        Sort(null, null, features, "priority");
                    },
            });
        });
    </script>
    
    <script id="PageTemplate" type="text/ractive">
        <div class="table-responsive">
            <table class="table table-striped table-link-ws">
                <thead>
                    <tr>
                        <th class="width7"><a on-click='sort:{{this}},{{"priority"}}'>Priority</a></th>
                        <th class="width10"><a on-click='sort:{{this}},{{"featureName"}}'>Feature name</a></th>
                        <th class="width7"><a on-click='sort:{{this}},{{"PercentageMeetingGoal"}}'>% meeting goal</a></th>
                        <th class="width7"><a on-click='sort:{{this}},{{"goal"}}'>Goal</a></th>
                        <th class="width7"><a on-click='sort:{{this}},{{"clientTypeTarget"}}'>Client type target</a></th>
                        <th class="width31"><a on-click='sort:{{this}},{{"ClientsMeetingGoalCount"}}'>Clients meeting goal</a></th>
                        <th class="width31"><a on-click='sort:{{this}},{{"ClientsNotMeetingGoalCount"}}'>Clients not meeting goal</a></th>
                    </tr>
                </thead>
                {{#this:i}}
                    {{#if !isHidden}}
                    <tr>
                        <th>{{priority}}</th>
                        <th>{{featureName}}</th>
                        <th>{{PercentageMeetingGoal}}</th>
                        <th>{{goal}}</th>
                        <th>{{clientTypeTarget}}</th>
                        <th>{{ClientsMeetingGoalCount}}
                            {{>PageLinksTemplate { IsCollapsed: true, fullPageSet: clientsMeetingGoal } }}
                        </th>
                        <th>{{ClientsNotMeetingGoalCount}}
                            {{>PageLinksTemplate { IsCollapsed: true, fullPageSet: clientsNotMeetingGoal } }}
                        </th>
                    <tr>
                    {{/if}}
                {{/#this}}
            </table>        
        </div>
    </script>

    <script id="PageLinksTemplate" type="text/ractive">
        <li>
            {{#if fullPageSet == clientsMeetingGoal}}
                <a on-click="expandClick:{{clientsMeetingGoal}}">more...</a>
            {{else}}
                <a on-click="expandClick:{{clientsNotMeetingGoal}}">more...</a>
            {{/if}}
        </li>
        {{#if !IsCollapsed}}
            {{#if fullPageSet == clientsMeetingGoal}}
                <a on-click="copyToClipboard:{{clientsMeetingGoal}}">Copy list of clients to clipboard</a>
            {{else}}
                <a on-click="copyToClipboard:{{clientsNotMeetingGoal}}">Copy list of clients to clipboard</a>
            {{/if}}
            {{#fullPageSet:i}}
                <li>
                    <a href="FeatureAdoptionClientView.aspx?brokerid={{Item1}}">{{Item2}}</a>
                </li>
            {{/#fullPageSet}}
        {{/if}}
    </script>
        
    <h1>Feature Adoption</h1>
    <form id="FeatureAdoption" runat="server">
    <div id="container">
    </div>
    </form>    
</asp:Content>
