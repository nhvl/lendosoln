﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanStatusConfig.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.LoanStatusconfig" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Loan Status Config</title>
    <style type="text/css">
        table
        {
            border-collapse:collapse;
        }
        table, th, td
        {
            border: 1px solid black;
        }     
        
    </style>
    
    <script type="text/javascript">
    </script>
</head>
<body>
    <h4 class="page-header">Loan Status Config</h4>
    <form id="form1" runat="server" style="margin:10px">
    <div>
    <br />
    <p style="font-size:12px">For each loan channel, please mark which statuses a loan file can be placed into.</p>
    
    <table runat="server" id="StatusTable" style="width: 100%;">
        <thead>
            <tr class="GridHeader" style="color: Black">
                <td colspan="4"></td>
                <td colspan="5" align="center">Correspondent</td>
                <td></td>
            </tr>
            <tr class="GridHeader" style="color: Black">
                <td>Status Description</td>
                <td>Retail</td>
                <td>Wholesale</td>
                <td>Broker</td>
                <td>Delegated</td>
                <td>Prior Approved</td>
                <td>Mini-Correspondent</td>
                <td>Bulk</td>
                <td>Mini-Bulk </td>
                <td>Channel not Specified</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="FieldLabel">Active Loan Statuses</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
<tr id='Loan_Open'>
                <td>Loan Open</td>
            </tr>
<tr id= 'Loan_Prequal'>
                <td>Pre-Qual</td>
            </tr>
<tr id= 'Loan_Registered'>
                <td>Registered</td>
            </tr>
<tr id= 'Loan_PreProcessing'>
                <td> Pre-Processing</td>
            </tr>
<tr id= 'Loan_Processing'>
                <td> Processing</td>
            </tr>
<tr id='Loan_DocumentCheck'>
                <td> Document Check </td>
            </tr>
<tr id='Loan_DocumentCheckFailed'>
                <td> Document Check Failed </td>
            </tr>
<tr id= 'Loan_LoanSubmitted'>
                <td>Submitted</td>
            </tr>
<tr id= 'Loan_PreUnderwriting'>
                <td>Pre-Underwriting</td>
            </tr>
<tr id= 'Loan_Underwriting'>
                <td>In Underwriting</td>
            </tr>
<tr id= 'Loan_Preapproval'>
                <td>Pre-Approved</td>
            </tr>
<tr id= 'Loan_Approved'>
                <td>Approved</td>
            </tr>
<tr id='Loan_ConditionReview'>
                <td>Condition Review</td>
            </tr>
<tr id= 'Loan_FinalUnderwriting'>
                <td>Final Underwriting</td>
            </tr>
<tr id= 'Loan_PreDocQC'>
                <td>Pre-Doc QC</td>
            </tr>
<tr id= 'Loan_ClearToClose'>
                <td>Clear to Close</td>
            </tr>
<tr id= 'Loan_DocsOrdered'>
                <td>Docs Ordered</td>
            </tr>
<tr id='Loan_DocsDrawn'>
                <td>Docs Drawn</td>
            </tr>
<tr id='Loan_Docs'>
                <td>Docs Out</td>
            </tr>
<tr id= 'Loan_DocsBack'>
                <td>Docs Back</td>
            </tr>
<tr id= 'Loan_FundingConditions'>
                <td>Funding Conditions</td>
            </tr>
<tr id= 'Loan_Funded'>
                <td>Funded</td>
            </tr>
<tr id= 'Loan_Recorded'>
                <td>Recorded</td>
            </tr>
<tr id= 'Loan_FinalDocs'>
                <td>Final Docs</td>
            </tr>
<tr id= 'Loan_Closed'>
                <td>Loan Closed</td>
            </tr>
            <tr id='Loan_SubmittedForPurchaseReview'>
                <td>Submitted for Purchase Review</td>
            </tr>
<tr id='Loan_InPurchaseReview'>
                <td>In Purchase Review</td>
            </tr>
<tr id='Loan_PrePurchaseConditions'>
                <td>Pre-Purchase Conditions</td>
            </tr>
<tr id='Loan_SubmittedForFinalPurchaseReview'>
                <td>Submitted For Final Purchase Review</td>
            </tr>
<tr id='Loan_InFinalPurchaseReview'>
                <td>In Final Purchase Review</td>
            </tr>
<tr id='Loan_ClearToPurchase'>
                <td>Clear to Purchase</td>
            </tr>
<tr id= 'Loan_Purchased'>
                <td>Loan Purchased</td>
            </tr>
<tr id='Loan_ReadyForSale'>
                <td>Ready For Sale</td>
            </tr>
<tr id= 'Loan_Shipped'>
                <td>Loan Shipped</td>
            </tr>
<tr id='Loan_InvestorConditions'>
                <td>Investor Conditions</td>
            </tr>
<tr id='Loan_InvestorConditionsSent'>
                <td>Investor Conditions Sent</td>
            </tr>
<tr id='Loan_LoanPurchased'>
                <td>Loan Sold</td>
            </tr>
            
            <tr>
                <td>&nbsp</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="FieldLabel">Inactive Loan Statuses</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>
<tr id='Loan_CounterOffer'>
                <td>Counter Offer Approved</td>
            </tr>
<tr id= 'Loan_OnHold'>
                <td>Loan On-Hold</td>
            </tr>
<tr id= 'Loan_Canceled'>
                <td>Loan Canceled</td>
            </tr>
<tr id= 'Loan_Suspended'>
                <td>Loan Suspended</td>
            </tr>
<tr id='Loan_Rejected'>
                <td>Loan Denied</td>
            </tr>
<tr id='Loan_Withdrawn'>
                <td>Loan Withdrawn</td>
            </tr>
<tr id='Loan_Archived'>
                <td>Loan Archived</td>
            </tr>
            
        </tbody>
    </table>
    
    <br />
    
    <asp:Button style="float:right" runat="server" Text="Update Enabled Loan Statuses" onclick="Save" />
    
    </div>
    </form>
</body>
</html>
