<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="UserAssocWithLicenseList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.UserAssocWithLicenseList" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>UserAssocWithLicenseList</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<script type="text/javascript">
		function _init()
		{
			resize( 500 , 500 );
		}
		function unassignUser(userId)
		{
			var confirmText = "WARNING: By removing this user from this license, he/she will no longer be able to login until they get assigned to another license. Hit OK if you're certain you want to do this." ;
			if (confirm(confirmText))
			{
				document.UserAssocWithLicenseList.userid.value = userId ;
				document.UserAssocWithLicenseList.submit() ;
			}
		}
		</script>
	</HEAD>
	<body onload="_init();" MS_POSITIONING="FlowLayout">
		<h4 class="page-header">Users assigned to License #<%=AspxTools.HtmlString(ViewState["licenseNumber"])%></h4>
		<form id="UserAssocWithLicenseList" method="post" runat="server">
			<input type="hidden" name="userid">
			<ml:commondatagrid id="m_dgUsers" runat="server" AutoGenerateColumns="False" CssClass="DataGrid" EnableViewState="False" AllowSorting="True" Width="100%" DataKeyField="UserId">
				<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
				<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
				<HeaderStyle CssClass="GridHeader"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="DisplayName" HeaderText="User"></asp:BoundColumn>
					<asp:BoundColumn DataField="LoginNm" HeaderText="Login"></asp:BoundColumn>
					<asp:BoundColumn DataField="LastLoginDate" HeaderText="Last Login"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Unassigned user">
						<ItemTemplate>
							<a href="#" onclick=<%#AspxTools.HtmlAttribute("unassignUser(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "userId").ToString()) + "); return false ;")%> >
								Unassign</a>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</ml:commondatagrid>
			<center>
				<asp:Button id="btnClose" runat="server" Text="Close" onclick="btnClose_Click"></asp:Button>
			</center>
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg></form>
	</body>
</HTML>
