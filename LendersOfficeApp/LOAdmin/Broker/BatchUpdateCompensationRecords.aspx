﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchUpdateCompensationRecords.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.BatchUpdateCompensationRecords" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="ml" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <script language="javascript">
        function _init() {
            resize(500, 400);
        }
    </script>
    <h4 class="page-header">Batch Update Compensation Records</h4>
	<form id="form1" method="post" runat="server">
		<table cellpadding="3" cellspacing="2" border="0">
			<tr>
			    <td>
			        <table>
			            <tr>
			                <td>
			                    Pick a .csv file with the compensation records
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    Employee type:
			                    <asp:RadioButton ID="m_empTypePML" runat="server" GroupName="empType" Checked="true" Text="PML" />
			                    <asp:RadioButton ID="m_empTypeLO" runat="server" GroupName="empType" Text="LO" />
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <input id="m_File" type="file" runat="server" autocomplete="off" title="Browse for csv file to upload" />
								<asp:Button id="m_Upload" runat="server" Text="Upload" OnClick="UploadClick"></asp:Button>
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <asp:TextBox ID="m_errorData" TextMode="MultiLine" Width="420" Rows="6" runat="server"></asp:TextBox>
			                </td>
			            </tr>
			            <tr>
			                <td style="font-weight:bold">
			                    Instructions:
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    CSV file format<br />
			                    Column 1 - loginNm<br />
                                Column 2 - originatorCompensationPercent<br />
                                Column 3 - originatorCompensationBaseT [should be 0(Loan Amount) or 4(Total Loan Amount)]<br />
                                Column 4 - originatorCompensationMinAmount<br />
                                Column 5 - originatorCompensationMaxAmount<br />
                                Column 6 - originatorCompensationFixedAmount<br />
                                Column 7 - originatorCompensationNotes<br />
                                Column 8 - originatorCompensationIsOnlyPaidForFirstLienOfCombo [Yes or No]<br />
                                Note: First row in the CSV file is assumed to be the header row and is ignored.
			                </td>
			            </tr>
			        </table>
			    </td>
			</tr>
		</table>
		<ml:CModalDlg id="m_ModalDlg" runat="server"></ml:CModalDlg>
	</form>
</body>
</html>
