﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Security;

    /// <summary>
    /// Page that holds a table for this lender's lender-level certificates.
    /// </summary>
    public partial class ClientCertificateRegistry : SecuredAdminPage
    {        
        /// <summary>
        /// The grid id for the active grid.
        /// </summary>
        private const string ActiveGridName = "ActiveCertGrid";

        /// <summary>
        /// The grid id for the revoked grid.
        /// </summary>
        private const string RevokedGridName = "RevokedCertGrid";

        /// <summary>
        /// The default sort order for the active table.
        /// </summary>
        private const string ActiveDefaultSortExpression = "CreatedDate DESC";

        /// <summary>
        /// The default sort order for the revoked table.
        /// </summary>
        private const string RevokedDefaultSortExpression = "RevokedDate DESC";

        /// <summary>
        /// The data table that will hold the info for the active certs.
        /// </summary>
        private DataTable activeCertTable;

        /// <summary>
        /// The data table that will hold the info for the revoked certs.
        /// </summary>
        private DataTable revokedCertTable;

        /// <summary>
        /// Gets the passed in broker id.
        /// </summary>
        /// <value>The passed in broker id.</value>
        protected Guid BrokerId
        {
            get
            {
                return new Guid(RequestHelper.GetSafeQueryString("BrokerId"));
            }
        }

        /// <summary>
        /// Gets the required permissions to access this page.
        /// </summary>
        /// <returns>The permissions associated with this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        /// <summary>
        /// The Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            this.PageID = "ClientCertificateRegistry";
            this.PageTitle = "Client Certificate Registry";
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("stylesheet.css");

            this.activeCertTable = new DataTable();
            this.activeCertTable.Columns.Add(new DataColumn("CertId", typeof(Guid)));
            this.activeCertTable.Columns.Add(new DataColumn("Description"));
            this.activeCertTable.Columns.Add(new DataColumn("Notes"));
            this.activeCertTable.Columns.Add(new DataColumn("Level"));
            this.activeCertTable.Columns.Add(new DataColumn("GroupUser"));
            this.activeCertTable.Columns.Add(new DataColumn("UserType"));
            this.activeCertTable.Columns.Add(new DataColumn("CreatedDate", typeof(DateTime)));

            this.revokedCertTable = new DataTable();
            this.revokedCertTable.Columns.Add(new DataColumn("CertId", typeof(Guid)));
            this.revokedCertTable.Columns.Add(new DataColumn("Description"));
            this.revokedCertTable.Columns.Add(new DataColumn("Notes"));
            this.revokedCertTable.Columns.Add(new DataColumn("Level"));
            this.revokedCertTable.Columns.Add(new DataColumn("GroupUser"));
            this.revokedCertTable.Columns.Add(new DataColumn("UserType"));
            this.revokedCertTable.Columns.Add(new DataColumn("RevokedDate", typeof(DateTime)));

            this.RegisterService("certService", "/loadmin/Broker/BrokerClientCertificateService.aspx");
        }

        /// <summary>
        /// The Page_Load function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.PopulateDataTable();
            this.LoadGridView();

            if (!this.Page.IsPostBack)
            {
                this.BrokerIdString.Value = this.BrokerId.ToString();
                this.CurrentTab.Value = "Active";
            }
        }

        /// <summary>
        /// Handles the sorting event.
        /// </summary>
        /// <param name="sender">The caller of this function.</param>
        /// <param name="e">Event arguments.</param>
        protected void ClientCertificateGrid_Sort(object sender, CommandEventArgs e)
        {
            string[] arguments = ((string)e.CommandArgument).Split(',');
            string chosenGridName = arguments[0] == "Active" ? ActiveGridName : RevokedGridName;
            string fieldToSort = arguments[1];
            string newSortDirection = "ASC";
            string currentSortExpression = (string)this.ViewState[chosenGridName + "SortExpression"];
            string currentSortedField;

            if (!string.IsNullOrEmpty(currentSortExpression))
            {
                string[] expressionSplit = currentSortExpression.Split(' ');
                currentSortedField = expressionSplit[0];
                string currentSortDirection = expressionSplit[1];

                if (fieldToSort == currentSortedField)
                {
                    newSortDirection = currentSortDirection == "ASC" ? "DESC" : "ASC";
                }
            }
            else
            {
                currentSortedField = chosenGridName == ActiveGridName ? "CreatedDate" : "RevokedDate";
            }

            string newSortExpression = fieldToSort + " " + newSortDirection;
            GridView chosenView = chosenGridName == ActiveGridName ? this.ActiveCertGrid : this.RevokedCertGrid;
            ((DataView)chosenView.DataSource).Sort = newSortExpression;
            chosenView.DataBind();

            var currentArrow = chosenView.HeaderRow.FindControl(currentSortedField + "HeaderArrow") as Label;
            if (currentArrow != null)
            {
                currentArrow.Text = string.Empty;
            }

            var newSortFieldArrow = chosenView.HeaderRow.FindControl(fieldToSort + "HeaderArrow") as Label;
            if (newSortFieldArrow != null)
            {
                newSortFieldArrow.Text = newSortDirection == "ASC" ? "&#9650;" : "&#9660;";
            }

            this.ViewState[chosenGridName + "SortExpression"] = newSortExpression;
        }

        /// <summary>
        /// Modifies the grid row on data bind.
        /// </summary>
        /// <param name="sender">The caller of this function.</param>
        /// <param name="e">The even arguments.</param>
        protected void CertGrid_DataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                string rowLevel = rowView["Level"].ToString();

                if (rowLevel == LenderClientCertificate.GetLevelAsString(ClientCertificateLevelTypes.Corporate) ||
                    rowLevel == LenderClientCertificate.GetLevelAsString(ClientCertificateLevelTypes.Individual))
                {
                    HtmlAnchor anchor = e.Row.FindControl("groupLink") as HtmlAnchor;
                    anchor.Visible = false;
                }
            }
        }

        /// <summary>
        /// Populates the certificate data table.
        /// </summary>
        private void PopulateDataTable()
        {
            IEnumerable<LenderClientCertificate> certificates = LenderClientCertificate.GetLenderClientCertificatesByBrokerId(this.BrokerId, isRevoked: null);

            var activeCerts = certificates.Where(cert => !cert.IsRevoked);
            var revokedCerts = certificates.Where(cert => cert.IsRevoked);

            foreach (LenderClientCertificate activeCert in activeCerts)
            {
                string groupUserDisplay;
                if (activeCert.Level == ClientCertificateLevelTypes.Individual)
                {
                    groupUserDisplay = activeCert.AssociatedUserLogin;
                }
                else if (activeCert.Level == ClientCertificateLevelTypes.Corporate)
                {
                    groupUserDisplay = string.Empty;
                }
                else
                {
                    groupUserDisplay = string.Join(",", activeCert.Groups.Values.Select<Group, string>(group => group.Name));
                }

                var row = this.activeCertTable.NewRow();
                row["CertId"] = activeCert.CertificateId;
                row["Description"] = activeCert.Description;
                row["Notes"] = activeCert.Notes;
                row["Level"] = LenderClientCertificate.GetLevelAsString(activeCert.Level);
                row["GroupUser"] = groupUserDisplay;
                row["UserType"] = activeCert.UserTypeAsString;
                row["CreatedDate"] = activeCert.CreatedDate;
                this.activeCertTable.Rows.Add(row);
            }

            foreach (LenderClientCertificate revokedCert in revokedCerts)
            {
                string groupUserDisplay;
                if (revokedCert.Level == ClientCertificateLevelTypes.Individual)
                {
                    groupUserDisplay = revokedCert.AssociatedUserLogin;
                }
                else if (revokedCert.Level == ClientCertificateLevelTypes.Corporate)
                {
                    groupUserDisplay = string.Empty;
                }
                else
                {
                    groupUserDisplay = string.Join(",", revokedCert.Groups.Values.Select<Group, string>(group => group.Name));
                }

                var row = this.revokedCertTable.NewRow();
                row["CertId"] = revokedCert.CertificateId;
                row["Description"] = revokedCert.Description;
                row["Notes"] = revokedCert.Notes;
                row["Level"] = LenderClientCertificate.GetLevelAsString(revokedCert.Level);
                row["GroupUser"] = groupUserDisplay;
                row["UserType"] = revokedCert.UserTypeAsString;
                row["RevokedDate"] = revokedCert.RevokedDate;
                this.revokedCertTable.Rows.Add(row);
            }
        }

        /// <summary>
        /// Populates the grid view.
        /// </summary>
        private void LoadGridView()
        {
            var activeView = new DataView(this.activeCertTable);
            activeView.Sort = ActiveDefaultSortExpression;

            var revokedView = new DataView(this.revokedCertTable);
            revokedView.Sort = RevokedDefaultSortExpression;

            this.ActiveCertGrid.DataSource = activeView;
            this.ActiveCertGrid.DataBind();

            this.RevokedCertGrid.DataSource = revokedView;
            this.RevokedCertGrid.DataBind();
        }
    }
}