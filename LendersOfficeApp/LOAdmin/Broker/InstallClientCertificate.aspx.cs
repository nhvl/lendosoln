﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Security;

    /// <summary>
    /// Creates the lender certificate.
    /// </summary>
    public partial class InstallClientCertificate : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// The Page_Load function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string cmd = RequestHelper.GetSafeQueryString("cmd");
                if (!string.IsNullOrEmpty(cmd) && cmd == "download")
                {
                    this.DownloadCertificate();
                    return;
                }
                else
                {
                    string key = RequestHelper.GetSafeQueryString("key");

                    string certPass = MultiFactorAuthCodeUtilities.GenerateRandom(6);
                    this.CertPassword.Text = certPass;

                    string encryptedVals = EncryptionHelper.Encrypt(certPass + ";" + key);
                    encryptedVals = encryptedVals.Replace("+", ".").Replace("=", "-");
                    this.RegisterJsGlobalVariables("Keys", encryptedVals);
                }
            }
        }

        /// <summary>
        /// The Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            this.PageID = "InstallClientCertificate";
            this.PageTitle = "Install Client Certificate";
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("stylesheet.css");
        }

        /// <summary>
        /// Returns the certificate for downloading.
        /// </summary>
        private void DownloadCertificate()
        {
            string encryptedString = RequestHelper.GetSafeQueryString("key").Replace(".", "+").Replace("-", "=");

            byte[] certifcate = null;
            string decrypted = EncryptionHelper.Decrypt(encryptedString);

            string[] parts = decrypted.Split(new char[] { ';' }, 2);
            if (parts.Length != 2)
            {
                throw new CBaseException(ErrorMessages.Generic, "Unexpected number of parts in decrypted string.");
            }

            string password = parts[0];
            Guid guidUsedForCache = Guid.Parse(parts[1]);

            certifcate = LenderClientCertificate.CreateLenderClientCertificate(password, guidUsedForCache, CurrentPrincipal.UserId);

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=\"certificate.p12\"");
            Response.ContentType = "application/octet-stream";

            Response.OutputStream.Write(certifcate, 0, certifcate.Length);
            Response.Flush();
            this.Context.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// Gets the auto expire cache key.
        /// </summary>
        /// <param name="key">The main key to use.</param>
        /// <param name="userId">The user id.</param>
        /// <returns>The composite key for the cache.</returns>
        private string GetAutoExpireCacheKey(string key, Guid userId)
        {
            return $"{key}_{userId.ToString()}";
        }
    }
}
