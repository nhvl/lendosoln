﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Admin;
using System.Web.Services;
using LendersOffice.ObjLib.QuickPricer;
using LendersOffice.Drivers.NetFramework;
using LqbGrammar.DataTypes;
using System.Data.SqlClient;
using LendersOffice.Drivers.SqlServerDB;
using System.Data;

namespace LendersOfficeApp.LOAdmin.Broker
{
    /// <summary>
    /// TODO: 
    /// Created case 186854 for this, but anyone can do it.
    /// Update the sql query to use sqlparameters.
    /// Make the page controls dyanamic based on the column type.  
    /// (i.e. if it's a bit, have a checkbox, if it's an enum, a dropdown)
    /// 
    /// GOALS:
    /// Make it faster to add fields to broker_user.
    /// The developer should only have to update the columns needed on this page in one place. (Like a Columns list.)
    /// 
    /// NOTES: 
    /// The developer has to update the ajax calls and the saving to account for the field type.
    /// If validation is needed between columns, they will have to change it to not call save on each individual click, but rather save
    /// those fields in pairs / etc.
    /// 
    /// REASON:
    /// A user has a userid and can access the system in some way.  An employee does not necessarily have access.
    /// The goal of this page is to make it possible to skip the employee editor when adding new columns to the broker_user table.
    /// 
    /// If we want to skip the EmployeeDB.Retrieve(...) call, we may also want to put this bit in abstractuserprincipal instead.
    /// But we might have to weigh the relative performances.
    /// </summary>
    public partial class EditUser : LendersOffice.Admin.SecuredAdminPage
    {
        #region ( Page Overrides )
        /// <summary>
        /// Return this page's required permissions set.  This
        /// test occurs with each page initialization.
        /// </summary>
        /// <returns>
        /// Array of required permissions.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.EditEmployee
			};
        }
        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_7_1;
        }
        #endregion

        #region ( Request Info )
        private Guid UserID
        {
            get
            {
                return RequestHelper.GetGuid("userid");
            }
        }
        private Guid BrokerID
        {
            get
            {
                return RequestHelper.GetGuid("brokerid");
            }
        }
        #endregion


        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJquery = true;
        }
        private void LoadData()
        {
            bool isNewPmlUIEnabled = false;
            bool isQuickPricerEnabled = false;
            bool isUsePml2AsQuickPricer = false;
            bool isQP2UserCheckboxEnabled = false;
            string userType = null;
            var userInfoSelectString =
@"SELECT 
  bu.IsQuickPricerEnabled
, bu.IsNewPmlUIEnabled
, bu.IsUsePml2AsQuickPricer
, au.Type
FROM BROKER_USER bu
JOIN ALL_USER au
ON au.userid = bu.userid
WHERE bu.USERID = @id";

            var listParams = new SqlParameter[] { new SqlParameter("@id", this.UserID) };

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    isUsePml2AsQuickPricer = (bool)reader["IsUsePml2AsQuickPricer"];
                    isNewPmlUIEnabled = (bool)reader["IsNewPmlUIEnabled"];
                    isQuickPricerEnabled = (bool)reader["IsQuickPricerEnabled"];
                    userType = (string)reader["Type"];
                    if (reader.Read())
                    {
                        var msg = "Found more than one user with userid: " + UserID;
                        throw new CBaseException(msg, msg);
                    }
                }
                else
                {
                    throw new NotFoundException("User with userid: " + UserID + " was not found");
                }
            };

            DBSelectUtility.ProcessDBData(BrokerID, userInfoSelectString, null, listParams, readHandler);

            isQP2UserCheckboxEnabled = Tools.IsQP2UserCheckboxEnabled(
                  BrokerDB.RetrieveById(BrokerID) // ideally this is loaded from the DB, not passed in...
                , isQuickPricerEnabled
                , isNewPmlUIEnabled
                , isUsePml2AsQuickPricer
                , userType
                );

            IsUsePml2AsQuickPricer.Checked = isUsePml2AsQuickPricer;
            IsUsePml2AsQuickPricer.Enabled = isQP2UserCheckboxEnabled;
            if (false == isQP2UserCheckboxEnabled)
            {
                m_Pml2AsQuickPricerDisabledReason.Text = "(Disabled because: " + string.Join(", ",
                    Tools.ReasonsUserIsntUsingQP2(
                        BrokerDB.RetrieveById(BrokerID) //! ideally this is loaded from the DB, not passed in...
                        , isQuickPricerEnabled
                        , isNewPmlUIEnabled
                        , isUsePml2AsQuickPricer
                        , userType
                    ).ToArray()
                ) + ")";
            }
            else
            {
                m_Pml2AsQuickPricerDisabledReason.Text = string.Empty;
            }

            this.RegisterJsGlobalVariables("UserId", this.UserID);
            this.RegisterJsGlobalVariables("BrokerId", this.BrokerID);
            this.RegisterJsGlobalVariables("UserType", userType);
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                return;
            }

            LoadData();
        }


        [WebMethod]
        public static void UpdateValue(Guid userID, Guid brokerId, string userType, string id, string inputType, string jsonValue)
        {
            ThrowIfInsufficientAccess();            

            string dbColumnName;
            string dbValue;
            GetValidInputsOrThrow(id, inputType, jsonValue, out dbColumnName, out dbValue);

            var modCount = HandleUpdateUserInDB(userID, brokerId, dbColumnName, dbValue);
            if (modCount.Value == 0)
            {
                var msg = "No users with id " + userID + " were updated";
                throw new CBaseException(msg, msg);
            }
            else if (modCount.Value > 1)
            {
                var msg = "There were multiple users with id " + userID + " that were updated.";
                throw new CBaseException(msg, msg);
            }
            else 
            {
                // if numAffected == 1;
                if (dbColumnName == "IsUsePml2AsQuickPricer" && dbValue == "1")
                {
                    var principal = (AbstractUserPrincipal)PrincipalFactory.RetrievePrincipalForUser(brokerId, userID, userType);
                    if(principal.IsActuallyUsePml2AsQuickPricer)
                    {
                        QuickPricer2LoansCreationEnqueuer.EnqueueUserLoanCreation(principal, null);
                    }
                }
            }
        }
        private static ModifiedRowCount HandleUpdateUserInDB(Guid userID, Guid brokerId, string dbColumnName, string dbValue)
        {
            // Since the column name is input to this function, we need to validate it's format.
            TestColumnName(dbColumnName);

            var sqlQuery = FormulateUpdateUserQuery(dbColumnName);

            var pVal = new SqlParameter("@value", dbValue);
            var pID = new SqlParameter("@id", userID);
            var sqlParams = new SqlParameter[] { pVal, pID };

            return ExecuteUpdateUserQuery(brokerId, sqlQuery, sqlParams);
        }
        public static void TestColumnName(string dbColumnName)
        {
            // This method is marked as public for the usage of unit test.
            // If/when that unit test is deleted, this can be safely
            // marked as private.
            bool isMatch = RegularExpressionHelper.IsMatch(RegularExpressionString.DBColumnName, dbColumnName);
            if (!isMatch)
            {
                throw new AccessDenied("Input data improperly formatted.");
            }
        }
        public static SQLQueryString FormulateUpdateUserQuery(string dbColumnName)
        {
            // This method is marked as public for the usage of unit test.
            // If/when that unit test is deleted, this can be safely
            // marked as private.
            var queryString = string.Format("UPDATE BROKER_USER SET {0} = @value WHERE UserID = @id;", dbColumnName);
            SQLQueryString? sqlQuery = SQLQueryString.Create(queryString);
            if (sqlQuery == null)
            {
                // this should never happen since we've already validated the columnn name,
                // but we include it in case there is a bug in our code.
                throw new AccessDenied("Input data improperly formatted.");
            }
            return sqlQuery.Value;
        }
        public static ModifiedRowCount ExecuteUpdateUserQuery(Guid brokerId, SQLQueryString sqlQuery, IEnumerable<SqlParameter> sqlParams)
        {
            // This method is marked as public for the usage of unit test.
            // If/when that unit test is deleted, this can be safely
            // marked as private.
            using (var sqlConnection = DbConnectionInfo.GetConnection(brokerId))
            {
                return SqlServerHelper.Update(sqlConnection, null, sqlQuery, sqlParams, TimeoutInSeconds.Default);
            }
        }
        private static HashSet<string> UpdateColumnWhiteList = new HashSet<string>()
        {
            "IsUsePml2AsQuickPricer" // note, it is ok to update this regardless of if it is enabled.
        };
        private static void ThrowIfInsufficientAccess()
        {
            var internalPrincipal = (InternalUserPrincipal)PrincipalFactory.CurrentPrincipal; // require internal user.
            var permissions = new InternalUserPermissions();
            permissions.Opts = internalPrincipal.Permissions;
            if (false == permissions.Can(E_InternalUserPermissions.EditEmployee))
            {
                throw new AccessDenied("You do not have permissions to edit users.");
            }
        }
        private static void GetValidInputsOrThrow(string id, string inputType, string jsonValue, out string dbColumnName, out string dbValue)
        {
            dbValue = string.Empty;
            dbColumnName = id;
            switch (inputType)
            {
                case "checkbox":
                    dbValue = bool.Parse(jsonValue) ? "1" : "0";
                    break;
                default:
                    var msg = inputType + " is not a supported input type.  Programmer needs to update.";
                    throw new CBaseException(msg, msg);
            }
            if (false == UpdateColumnWhiteList.Contains(id))
            {
                var msg = "unable to update " + id + " as it is not in the white list.";
                throw new CBaseException(msg, msg);
            }
        }

        #region Web Form Designer generated code
        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion

    }
}
