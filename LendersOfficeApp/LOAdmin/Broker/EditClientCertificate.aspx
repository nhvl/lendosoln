﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditClientCertificate.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.EditClientCertificate" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Register Client Certificate</title>
    <style type="text/css">
        #Description
        {
            width: 600px;
        }
        .MainDiv
        {
            padding: 10px;
        }
        #Notes
        {
            width: 600px;
            height: 200px;
        }
        .ButtonsSection
        {
            padding: 10px;
            float: left;
        }
        .TopAlign
        {
            vertical-align: top;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        var selectedGroups = [];
        var wasSaved = false;
        jQuery(function ($) {
            function OnLevelButtonClick(selectedLevel) {
                if ($("#IsNewValue").val() === "False") {
                    return;
                }

                setDisabledAttr($("#AssocBranchGroupLink"), selectedLevel !== '2');
                setDisabledAttr($("#AssocOcGroupLink"), selectedLevel !== '3');
                setDisabledAttr($("#AssocEmployeeGroupLink"), selectedLevel !== '1');
                $("#LoginNm").prop('disabled', selectedLevel !== '0');

                $("#TypeLqbBtn").prop('disabled', selectedLevel === '3');
                $("#TypeTpoBtn").prop('disabled', selectedLevel === '1');
                $("#TypeAnyBtn").prop('disabled', selectedLevel !== '4' && selectedLevel !== '2');

                if (selectedLevel === "0" || selectedLevel === "1" || selectedLevel === "2" || selectedLevel === "4") {
                    $("#TypeLqbBtn").prop('checked', true);
                }
                else {
                    $("#TypeTpoBtn").prop('checked', true);
                }

                if (selectedLevel === "0") {
                    $("#LoginNmReqIcon").show();
                }
                else {
                    $("#LoginNmReqIcon").hide();
                }
            }

            function InitializeSelectedGroups()
            {
                var groups = $("#SelectedGroupIds").val();
                if(groups !== "")
                {
                    selectedGroups = groups.split(',');
                }
            }

            $("#NextButton").click(function () {
                var userTypeChecked = $("input[name='UserTypeBtns']:checked");
                var levelBtnsVal = $("input[name='LevelButtons']:checked").val();
                var userTypeBtnsVal = $("input[name='UserTypeBtns']:checked").val();
                var notes = $("#Notes").val();
                var brokerId = $('#BrokerIdValue').val();
                var description = $("#Description").val();
                var loginName = $('#LoginNm').val();
                var stringIds = selectedGroups.toString();
                var certId = $("#CertificateId").val();
                var brokerId = $("#BrokerIdValue").val();

                if (levelBtnsVal === 'undefined') {
                    alert("Please select a level.");
                    return;
                }

                if (userTypeBtnsVal === 'undefined') {
                    alert("Please select a user type.")
                    return;
                }

                if (description === "") {
                    alert("Please enter a description");
                    return;
                }

                if (levelBtnsVal === "0" && loginName === "") {
                    alert("Individual Login level specified. Please enter a user login name.");
                    return;
                }

                if ((levelBtnsVal !== '4' && levelBtnsVal !== '0') && selectedGroups.length === 0) {
                    alert("No groups have been associated. Please associate at least one group.")
                    return;
                }

                var isNew = $("#IsNewValue").val() === "True";
                if (isNew) {
                    var args =
                    {
                        BrokerId: brokerId,
                        Description: description,
                        Level: levelBtnsVal,
                        UserType: userTypeBtnsVal,
                        Notes: notes,
                        LoginName: loginName,
                        SelectedGroups: stringIds
                    };

                    var result = gService.certService.call("InstallCertificate", args);
                    if (!result.error) {
                        wasSaved = true;
                        if(result.value["IsValid"] === "True")
                        {
                            var key = result.value["Key"];

                            var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("/LOAdmin/Broker/InstallClientCertificate.aspx")) %> + '?key=' + key;
                            showModal(url, null, null, null, function(){
                                onClosePopup();
                            });
                        }
                        else
                        {
                            alert(result.value["ErrorMessage"]);
                        }
                    }
                    else {
                        alert("Unable to begin creating a lender client certificate.");
                    }
                }
                else
                {
                    var args =
                    {
                        Description: description,
                        Notes: notes,
                        SelectedGroups: stringIds,
                        CertificateId: certId,
                        BrokerId: brokerId
                    };

                    var result = gService.certService.call("EditCertificate", args);
                    if (!result.error) {
                        wasSaved = true;
                        if(result.value["IsValid"] === "True")
                        {
                            alert("Save successful.");
                        }
                        else
                        {
                            alert(result.value["Errors"]);
                        }
                    }
                    else {
                        alert("Unable to begin creating a lender client certificate.");
                    }
                }
            });

            $("#CancelButton").click(function () {
                onClosePopup();
            });

            $("input[name='LevelButtons']").change(function (e) {
                var selectedLevel = $(this).val();
                OnLevelButtonClick(selectedLevel);
            });

            $("a[class='AssociatedValue']").click(function (e) {
                var isEdit = "t";
                var groupType = $(this).attr('groupType');
                var brokerId = $("#BrokerIdValue").val();

                var selectedLevelVal = $("input[name='LevelButtons']:checked").val();
                if (groupType !== selectedLevelVal)
                {
                    return;
                }

                var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("/LOAdmin/Broker/AssociateGroups.aspx")) %> + '?bId=' + brokerId + '&IsEdit=' + isEdit + '&GroupType=' + groupType;
                var inputArgs = [];
                inputArgs.selectedGroups = selectedGroups;
                showModal(url, inputArgs, null, null, function(args){ 
                    if(args.OK)
                    {
                        selectedGroups = [];
                        $.each(args.selectedGroups, function (i, val) {
                            selectedGroups.push(val);
                        });
                    }
                });
            });

            resize(800, 550);
            OnLevelButtonClick($("input[name='LevelButtons']:checked").val());
            InitializeSelectedGroups();
        });
    </script>
    <h4 class="page-header">Register Client Certificate</h4>
    <form id="form1" runat="server">
    <div class="MainDiv">
        <asp:HiddenField runat="server" ID="IsNewValue" />
        <asp:HiddenField runat="server" ID="BrokerIdValue" />
        <asp:HiddenField runat="server" ID="SelectedGroupIds" />
        <asp:HiddenField runat="server" ID="CertificateId" />
        <table border="0" width="100%">
            <tr>
                <td class="TopAlign FieldLabel">Description</td>
                <td>
                    <asp:TextBox ID="Description" runat="server"></asp:TextBox>
                    <img src="../../images/require_icon.gif">
                </td>
            </tr>
            <tr>
                <td class="TopAlign FieldLabel">Level</td>
                <td>
                    <asp:RadioButton ID="CorporateBtn" Text="Corporate" runat="server" GroupName="LevelButtons" value="4" /> 
                    <br />
                    
                    <asp:RadioButton ID="BranchGroupBtn" Text="Branch Group" runat="server" GroupName="LevelButtons" value="2" /> 
                    <a href="javascript:void(0);" groupType="2" class="AssociatedValue" id="AssocBranchGroupLink" runat="server">associate branch groups</a>
                    <br />
                    
                    <asp:RadioButton ID="OcGroupBtn" Text="OC Group" runat="server" GroupName="LevelButtons" value="3" /> 
                    <a href="javascript:void(0);" groupType="3" class="AssociatedValue" id="AssocOcGroupLink" runat="server">associate OC groups</a>
                    <br />
                    
                    <asp:RadioButton ID="EmployeeGroupBtn" Text="Employee Group" runat="server" GroupName="LevelButtons" value="1" /> 
                    <a href="javascript:void(0);" groupType="1" class="AssociatedValue" id="AssocEmployeeGroupLink" runat="server">associate employee groups</a>
                    <br />
                    
                    <asp:RadioButton ID="IndividualBtn" Text="Individual Login" runat="server" GroupName="LevelButtons" value="0"/>
                    <asp:TextBox runat="server" class="AssociatedValue" ID="LoginNm"></asp:TextBox>
                    <img alt="Required" runat="server" id="LoginNmReqIcon" src="../../images/require_icon.gif">
                </td>
            </tr>
            <tr>
                <td class="TopAlign FieldLabel">User Type</td>
                <td>
                    <asp:RadioButton ID="TypeLqbBtn" Text="LQB" runat="server" GroupName="UserTypeBtns" value="0"/> <br />
                    <asp:RadioButton ID="TypeTpoBtn" Text="TPO" runat="server" GroupName="UserTypeBtns" value="1"/> <br />
                    <asp:RadioButton ID="TypeAnyBtn" Text="Any" runat="server" GroupName="UserTypeBtns" value="2"/> <br />
                </td>
            </tr>
            <tr>
                <td class="TopAlign FieldLabel">Notes</td>
                <td>
                    <asp:TextBox TextMode="MultiLine" runat="server" ID="Notes"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div class="ButtonsSection">
        <input type="button" id="NextButton" runat="server"/>
        <input type="button" id="CancelButton" value="Cancel"/>
    </div>
    </form>
</body>
</html>
