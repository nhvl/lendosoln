﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConversationLogPermissionLevelEditor.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.ConversationLogPermissionLevelEditor" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Conversation Log Permission Level Editor</title>
    <style type="text/css">
        .windowBtnSection
        {
            padding: 10px;
        }
    </style>
</head>
<body ng-app="mainApp" ng-controller="permissionLevelEditCtrl">
    <script type="text/javascript">
        var app = angular.module('mainApp', []);

        function arrayOfObjectsToDictionary(array, keyGetter, valueGetter)
        {
            var result = array.reduce(function (result, item) {
                result[keyGetter(item)] = valueGetter(item);
                return result;
            }, {});

            return result;
        }

        function opTypeNameGetter(opType)
        {
            return opType.name;
        }

        function opTypeIdGetter(opType)
        {
            return opType.id;
        }

        function identity(item)
        {
            return item;
        }

        var opTypes =[
            { name:'View', id:1, extraText:'view posts with' },
            { name: 'Post', id: 0, extraText:'post to'},
            { name: 'Reply', id: 2, extraText: 'reply to posts with' },
            { name:'Hide', id:3, extraText:'hide posts with' }
        ];        

        // these are the checkstates of the checkboxes that control the checkboxes in their column.
        var controlCheckStates = {}; // controlCheckStates[groupOrRole][opTypeName][instanceOfRoleOrGroupId] is true/false.
        var types = [{ name: 'role', friendlyName: 'Roles' }, { name: 'group', friendlyName: 'Employee Groups' }];
        for (var i = 0; i < types.length; i++)
        {
            var type = types[i];

            if (typeof (controlCheckStates[type.name]) == 'undefined')
            {
                controlCheckStates[type.name] = {};
            }

            var checkedStatesForType = controlCheckStates[type.name];
            for(var j = 0; j < opTypes.length; j++)
            {
                var opType = opTypes[j];
                checkedStatesForType[opType.name] = false;
            }
        }

        jQuery(function ($) {
            var permissionLevelNameRegex = new RegExp(ML.permissionLevelNameRegexString, 'g');
            var permissionLevelDescriptionRegex = new RegExp(ML.permissionLevelDescriptionRegexString, 'g');
            
            resize(1000, 800);

            app.factory('permissionLevelSaveService', function ($http) {
                return {
                    save: function (permissionLevel, successMethod, failedMethod) {
                        //return the promise directly.
                        return $http.post(
                            'ConversationLogPermissionLevelEditor.aspx/SavePermissionLevel?brokerid=' + ML.brokerId,
                            {
                                permissionLevelString: angular.toJson(permissionLevel) // instead of JSON.stringify to avoid $$hashObject props.
                            }
                            )
                            .then(function (result) {
                                //resolve the promise as the data
                                successMethod(result.data.d);
                            }, function (response) {
                                failedMethod(response)
                            });
                    }
                }
            });

            app.controller('permissionLevelEditCtrl', function ($scope, permissionLevelSaveService) {
                // clone it so we don't update it if they click cancel.
                $scope.permissionLevel = angular.copy(initialPermissionLevel); //! angular.copy(permissionLevel);
                $scope.controlCheckStates = controlCheckStates;
                $scope.types = types;
               
                $scope.opTypes = opTypes;
                $scope.instancesOfTypeByTypeName = instancesOfTypeByTypeName;
                

                window.onbeforeunload = function (e) {
                    if (!angular.equals(initialPermissionLevel, $scope.permissionLevel)) {
                        e.returnValue = 'You have unsaved changes.  Really close without saving?';
                        return e.returnValue;
                    }
                };

                $scope.saveClick = function () {
                    var $saveBtn = $('#saveBtn');
                    $saveBtn.attr('disabled', 'disabled');

                    var match = permissionLevelNameRegex.exec($scope.permissionLevel.name);
                    permissionLevelNameRegex.lastIndex = 0;
                    if (match == null || match.length === 0) {
                        alert('Your permission level name is invalid. The validating regex is ' + ML.permissionLevelNameRegexString + ' .');
                        $saveBtn.removeAttr('disabled');
                        return;
                    }

                    match = permissionLevelDescriptionRegex.exec($scope.permissionLevel.description);
                    permissionLevelDescriptionRegex.lastIndex = 0;
                    if (match == null || match.length === 0) {
                        alert('Your permission level description is invalid. The validating regex is ' + ML.permissionLevelDescriptionRegexString + ' .');
                        $saveBtn.removeAttr('disabled');
                        return;
                    }

                    if (initialPermissionLevel.id !== null && angular.equals(initialPermissionLevel, $scope.permissionLevel)) {
                        // just close since nothing has changed and it's not new.
                        window.close();
                        return;
                    }
                    
                    //! disable everything on save.
                    permissionLevelSaveService.save(
                        $scope.permissionLevel,
                        function (data) {
                            var responseObj = angular.fromJson(data);

                            if (typeof (responseObj.userMessage) !== 'undefined')
                            {
                                alert(responseObj.userMessage);
                                $saveBtn.removeAttr('disabled');
                                return;
                            }

                            window.onbeforeunload = function () { };

                            var newOrUpdatedPermissionLevel = responseObj;

                            if (window.opener) {
                                window.opener.addOrUpdatePermissionLevel(newOrUpdatedPermissionLevel);
                            }

                            window.close();
                        }, function (response) {
                            alert('failed to save: ' + response.data.Message);
                            $saveBtn.removeAttr('disabled');
                        });
                    
                };

                $scope.enableOrDisableAll = function(typeName, opName)
                {
                    var checked = controlCheckStates[typeName][opName];
                    var checkedStatesOfInterest = $scope.permissionLevel.checkedStates[typeName][opName];

                    for (var k in checkedStatesOfInterest)
                    {
                        if (checkedStatesOfInterest.hasOwnProperty(k))
                        {
                            checkedStatesOfInterest[k] = checked;
                        }
                    }
                }

                $scope.closeClick = function () {
                    self.close();
                };
            });
        });
    </script>
    <form id="form1" runat="server">
        <input type="checkbox" ng-model="permissionLevel.isActive"/> Active<br />
        Name: <input id="permissionLevelNameElement" type="text" ng-model="permissionLevel.name"  maxlength="40"/> * The permission level name is unique per broker.<br />
        Description (shown to end users): <input id="permissionLevelDescriptionElement" type="text" ng-model="permissionLevel.description" maxlength="1000"/> <br />
        <table>
            <tbody>
                <tr id="viewAndReplyRow">
                    <td ng-repeat="opType in opTypes">
                        <div>{{opType.name}}</div>
                        <div>Users with any of the below roles or groups can {{opType.extraText}} this permission level.</div>
                        <table>
                            <tbody>
                                <tr>
                                    <td ng-repeat="type in types">
                                        <table>
                                            <thead><th><input type="checkbox" ng-model="controlCheckStates[type.name][opType.name]" ng-change="enableOrDisableAll(type.name, opType.name)"/>{{type.friendlyName}}</th></thead>
                                            <tbody>
                                                <tr ng-repeat="instanceOfType in instancesOfTypeByTypeName[type.name]">
                                                    <td><input type="checkbox" ng-model="permissionLevel.checkedStates[type.name][opType.name][instanceOfType.id]" />{{instanceOfType.description}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>                                    
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr id="PostAndHideRow">

                </tr>
            </tbody>
        </table>
        
        <div class="windowBtnSection">
            <input type="button" id="saveBtn" value="Save" ng-click="saveClick();" ng-disabled="permissionLevel.name == null || permissionLevel.name == ''"/>
            <input type="button" id="closeBtn" value="Close" ng-click="closeClick();"/>
        </div>
    </form>
</body>
</html>
