﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.Cenlar;
    using LendersOffice.Security;

    /// <summary>
    /// Allows an administrative user to manage a client's Cenlar credentials.
    /// </summary>
    public partial class CenlarCredentialEdit : SecuredAdminPage
    {
        /// <summary>
        /// Gets the Cenlar environment.
        /// </summary>
        public CenlarEnvironmentT EnvironmentT
        {
            get
            {
                return RequestHelper.GetBool("isBetaEnvironment")
                    ? CenlarEnvironmentT.Beta
                    : CenlarEnvironmentT.Production;
            }
        }

        /// <summary>
        /// Gets the required permissions to access this page.
        /// </summary>
        /// <returns>The permissions associated with this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sending element.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                return;
            }

            var brokerId = RequestHelper.GetGuid("brokerId");

            this.BrokerId.Value = brokerId.ToString();
            this.CenlarEnvironment.Text = this.EnvironmentT.ToString();

            var credentials = CenlarCredentialSet.Retrieve(brokerId, this.EnvironmentT);
            if (credentials != null)
            {
                this.CachedApiKey.Value = credentials.ApiKey;
                this.CachedApiKeyReset.Value = credentials.LastApiKeyReset.ToString();

                this.CenlarUserId.Text = credentials.UserId;
                this.CenlarCustomerId.Text = credentials.CustomerId;
                this.CenlarApiKey.Text = credentials.ApiKey;
            }
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The sending element.</param>
        /// <param name="e">Arguments associated with the event.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            this.RegisterCSS("stylesheet.css");
            this.RegisterService("main", "/loadmin/broker/CenlarCredentialEditService.aspx");
        }
    }
}