﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Security;

    /// <summary>
    /// Service methods for the BrokerClientCertificateService page.
    /// </summary>
    public partial class BrokerClientCertificateService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Chooses which service method to perform.
        /// </summary>
        /// <param name="methodName">The method to perform.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "InstallCertificate":
                    this.InstallCertificate();
                    break;
                case "RevokeCertificate":
                    this.RevokeCertificate();
                    break;
                case "RestoreCertificate":
                    this.RestoreCertificate();
                    break;
                case "EditCertificate":
                    this.EditCertificate();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled method call in EditClientCertificateService");
            }
        }

        /// <summary>
        /// Stores the certificate information in the auto expire text cache for use by another page.
        /// </summary>
        private void InstallCertificate()
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            Guid brokerId = this.GetGuid("BrokerId");
            string description = this.GetString("Description");
            string notes = this.GetString("Notes");
            string loginName = this.GetString("LoginName");
            string[] stringIds = this.GetString("SelectedGroups").Split(',');
            IEnumerable<Guid> groupIds = string.IsNullOrEmpty(this.GetString("SelectedGroups")) ? new List<Guid>() : stringIds.Select(id => new Guid(id));
            ClientCertificateLevelTypes level = (ClientCertificateLevelTypes)this.GetInt("Level");
            ClientCertificateUserTypes userType = (ClientCertificateUserTypes)this.GetInt("UserType");

            if (level == ClientCertificateLevelTypes.Individual)
            {
                string type = userType == ClientCertificateUserTypes.Lqb ? "B" : "P";
                SqlParameter[] parameters =
                {
                    new SqlParameter("@LoginName", loginName),
                    new SqlParameter("@Type", type),
                    new SqlParameter("@BrokerId", brokerId)
                };

                bool foundUser = false;
                using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetUserByLoginName", parameters))
                {
                    if (reader.Read())
                    {
                        foundUser = true;
                    }
                }

                if (!foundUser)
                {
                    this.SetResult("IsValid", false);
                    this.SetResult("ErrorMessage", $"User {loginName} does not exist.");
                    return;
                }
            }

            XDocument certFieldDoc = new XDocument();
            XElement fieldsEl = new XElement(
                                             "fields", 
                                             new XElement("brokerId", brokerId.ToString()),
                                             new XElement("desc", description),
                                             new XElement("notes", notes), 
                                             new XElement("level", level), 
                                             new XElement("userType", userType),
                                             new XElement("loginNm", loginName));

            XElement groups = new XElement("groups");
            foreach (Guid groupId in groupIds)
            {
                groups.Add(new XElement("group", groupId));
            }

            fieldsEl.Add(groups);
            certFieldDoc.Add(fieldsEl);

            Guid cacheIdPart = Guid.NewGuid();
            string fullCacheKey = LenderClientCertificate.ConstructFieldsCacheKey(PrincipalFactory.CurrentPrincipal.UserId, cacheIdPart);
            AutoExpiredTextCache.AddToCache(certFieldDoc.ToString(), new TimeSpan(0, 5, 0), fullCacheKey);

            this.SetResult("IsValid", true);
            this.SetResult("Key", cacheIdPart.ToString());
        }

        /// <summary>
        /// Revokes the client certificate.
        /// </summary>
        private void RevokeCertificate()
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            Guid brokerId = GetGuid("BrokerId");
            Guid certificateId = GetGuid("CertificateId");

            LenderClientCertificate.RevokeClientCertificate(brokerId, certificateId);
        }

        /// <summary>
        /// Restores the client certificate.
        /// </summary>
        private void RestoreCertificate()
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            Guid brokerId = GetGuid("BrokerId");
            Guid certificateId = GetGuid("CertificateId");

            LenderClientCertificate.RestoreClientCertificate(brokerId, certificateId);
        }

        /// <summary>
        /// Updates the certificate.
        /// </summary>
        private void EditCertificate()
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            Guid brokerId = GetGuid("BrokerId");
            Guid certId = GetGuid("CertificateId");

            LenderClientCertificate certificate = LenderClientCertificate.GetLenderClientCertificate(brokerId, certId);
            if (certificate == null)
            {
                throw new CBaseException("Certificate not found.", "Certificate not found");
            }

            string description = GetString("Description");
            string notes = GetString("Notes");
            string[] stringIds = this.GetString("SelectedGroups").Split(',');
            IEnumerable<Guid> groupIds = string.IsNullOrEmpty(this.GetString("SelectedGroups")) ? new List<Guid>() : stringIds.Select(id => new Guid(id));

            string errors;
            bool result = certificate.UpdateCertificate(description, notes, groupIds, out errors);
            if (result)
            {
                this.SetResult("IsValid", true);
            }
            else
            {
                this.SetResult("IsValid", false);
                this.SetResult("Errors", errors);
            }
        }
    }
}