﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DataAccess;
using LendersOffice.Admin;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class BatchUpdateCompensationRecords : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker
            };
        }

        private Guid BrokerId
        {
            get 
            { 
                return new Guid(RequestHelper.GetSafeQueryString("BrokerId")); 
            }
        }

        protected void UploadClick(object sender, System.EventArgs a)
        {
            LosConvert los_convert = new LosConvert();

            int loginNm = 0;
            int originatorCompensationPercent = 1;
            int originatorCompensationBaseT = 2;
            int originatorCompensationMinAmount = 3;
            int originatorCompensationMaxAmount = 4;
            int originatorCompensationFixedAmount = 5;
            int originatorCompensationNotes = 6;
            int originatorCompensationIsOnlyPaidForFirstLienOfCombo = 7;

            using (StreamReader sR = new StreamReader(m_File.PostedFile.InputStream))
            {
                StringBuilder errorList = new StringBuilder();
                string line = sR.ReadLine();
                string[] data;
                int totalUsers = 0;
                int successCount = 0;

                while (!sR.EndOfStream)
                {
                    line = sR.ReadLine();
                    data = line.Split(',');

                    totalUsers++;
                    List<SqlParameter> pA = new List<SqlParameter>();
                    string loginNmStr = data[loginNm].ToString().TrimWhitespaceAndBOM();
                    pA.Add(new SqlParameter("@LoginNm", loginNmStr));
                    string type;
                    if (m_empTypePML.Checked)
                    {
                        type = "P";
                    }
                    else
                    {
                        type = "B";
                    }
                    pA.Add(new SqlParameter("@Type", type));

                    using (IDataReader dr = StoredProcedureHelper.ExecuteReader(BrokerId, "FindUser", pA))
                    {
                        bool userNotFound = true;
                        while (dr.Read() && userNotFound)
                        {
                            Guid entryEmployeeId = (Guid)dr["EmployeeId"];
                            Guid entryBrokerId = (Guid)dr["BrokerId"];
                            string entryLoginNm = dr["LoginNm"].ToString();

                            if (String.Compare(entryLoginNm, loginNmStr, true) == 0 && BrokerId == entryBrokerId)
                            {
                                userNotFound = false;
                            }
                            else
                            {
                                continue;
                            }

                            try
                            {
                                EmployeeDB empDB = new EmployeeDB(entryEmployeeId, BrokerId);
                                empDB.Retrieve();
                                string baseT = data[originatorCompensationBaseT].ToString();
                                if (!string.IsNullOrEmpty(baseT))
                                {
                                    empDB.OriginatorCompensationBaseT = (E_PercentBaseT)Convert.ToInt32(baseT);
                                }
                                empDB.OriginatorCompensationFixedAmount = los_convert.ToMoney(data[originatorCompensationFixedAmount].ToString());
                                empDB.OriginatorCompensationMaxAmount = los_convert.ToMoney(data[originatorCompensationMaxAmount].ToString());
                                empDB.OriginatorCompensationMinAmount = los_convert.ToMoney(data[originatorCompensationMinAmount].ToString());
                                empDB.OriginatorCompensationNotes = data[originatorCompensationNotes].ToString();
                                empDB.OriginatorCompensationPercent = los_convert.ToRate(data[originatorCompensationPercent].ToString());
                                empDB.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = data[originatorCompensationIsOnlyPaidForFirstLienOfCombo] == "Yes";
                                empDB.Save(PrincipalFactory.CurrentPrincipal);
                                successCount++;
                            }
                            catch (CBaseException e)
                            {
                                errorList.AppendLine(string.Format("Entry {0}: Unable to save user {1} of id {2} because {3}.", totalUsers, loginNmStr, entryEmployeeId, e.DeveloperMessage));
                            }
                        }
                        
                        if (userNotFound)
                        {
                            errorList.AppendLine(string.Format("Entry {0}: Unable to find user {1}.", totalUsers, loginNmStr));
                        }
                    }
                }

                m_errorData.Text = string.Format("{0} users out {1} successfully updated.\n\r{2}", successCount, totalUsers, errorList);
            }
        }
    }
}
