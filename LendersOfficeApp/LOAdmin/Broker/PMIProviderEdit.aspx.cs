﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class PMIProviderEdit : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Gets the required permission for the current LOAdmin page.
        /// </summary>
        /// <returns>Permission required to access this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Tools.Bind_PmiProviderRanking(PmiProviderGenworthRanking);
            Tools.Bind_PmiProviderRanking(PmiProviderMgicRanking);
            Tools.Bind_PmiProviderRanking(PmiProviderRadianRanking);
            Tools.Bind_PmiProviderRanking(PmiProviderEssentRanking);
            Tools.Bind_PmiProviderRanking(PmiProviderArchRanking);
            Tools.Bind_PmiProviderRanking(PmiProviderNationalMIRanking);
            Tools.Bind_PmiProviderRanking(PmiProviderMassHousingRanking);

            Tools.SetDropDownListValue(PmiProviderGenworthRanking, RequestHelper.GetInt("genworth"));
            Tools.SetDropDownListValue(PmiProviderMgicRanking, RequestHelper.GetInt("mgic"));
            Tools.SetDropDownListValue(PmiProviderRadianRanking, RequestHelper.GetInt("radian"));
            Tools.SetDropDownListValue(PmiProviderEssentRanking, RequestHelper.GetInt("essent"));
            Tools.SetDropDownListValue(PmiProviderArchRanking, RequestHelper.GetInt("arch"));
            Tools.SetDropDownListValue(PmiProviderNationalMIRanking, RequestHelper.GetInt("national"));
            Tools.SetDropDownListValue(PmiProviderMassHousingRanking, RequestHelper.GetInt("masshousing"));

            winnerHighest.Checked = RequestHelper.GetBool("highwinner");
            winnerLowest.Checked = !winnerHighest.Checked;
        }
    }
}
