<%@ Page language="c#" Codebehind="ImportBrokerAttributes.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.ImportBrokerAttributes" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Import <%= AspxTools.HtmlString(CurrentSettings.Title)%></title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</head>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto;" onload="onInit();">
		<script>
			function onInit()
			{
				<% if( IsPostBack == false ) { %>

				resize( 1024 , 768 );

				<% } %>

				if( document.getElementById("m_errorMessage") != null && document.getElementById("m_errorMessage").value != "" )
				{
					alert( document.getElementById("m_errorMessage").value );
					document.getElementById("m_errorMessage").value = "";
				}

				if( document.getElementById("m_feedBack") != null && document.getElementById("m_feedBack").value != "" )
				{
					alert( document.getElementById("m_feedBack").value );
					document.getElementById("m_feedBack").value = "";
				}
			}
		</script>
		<h4 class="page-header">Import <%= AspxTools.HtmlString(CurrentSettings.Plural)%> for broker: <%=AspxTools.HtmlString(Request["brokernm"])%></h4>
		<form id="Import" method="post" runat="server">
			<table cellpadding="3" cellspacing="2" width="1000px" height="700px" border="0">
				<tr>
					<td height="0%" colspan="2">
						<table cellspacing="0" cellpadding="0" width="1000px">
							<tr>
								<td width="24px" nowrap>
								</td>
								<td width="0px" nowrap>
									<table cellpadding="0" cellspacing="4">
										<tr>
											<td class="FieldLabel" nowrap>
												Upload <%= AspxTools.HtmlString(CurrentSettings.Title)%> data:
											</td>
											<td>
												<input id="m_File" type="file" style="width:400" runat="server" autocomplete="off" title="Browse for csv file to upload">
												<asp:Button id="m_Upload" runat="server" Text="Upload" OnClick="UploadClick"></asp:Button>
											</td>
										</tr>
									</table>
									All <%= AspxTools.HtmlString(CurrentSettings.Plural)%> will be associated with <strong><%=AspxTools.HtmlString(Request["brokernm"])%></strong>.
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="1000px" height="100%">
						<table cellpadding="0" cellspacing="0" height="100%" width="1000px">
							<tr>
								<td height="0%">
									<div style="PADDING: 8px; COLOR: dimgray; FONT: 11px arial;">
										Paste your <%= AspxTools.HtmlString(CurrentSettings.Short)%> list as a comma-separated blob, or upload as csv. &nbsp;<font color="red"><b>The first line will always be ignored</b></font>.
									</div>
								</td>
							</tr>
							<tr>
								<td width=1000px height="100%">
									<asp:TextBox id="m_ImportInput" runat="server" style="WIDTH: 1000px; HEIGHT: 100%; PADDING: 4px; FONT: 11px arial; BORDER: 2px solid lightgrey; BORDER-RIGHT: 0px; OVERFLOW-Y: scroll; " TextMode="MultiLine"></asp:TextBox>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="1000px" height="0%" ondblclick="copyStringToClipboard( this.innerText ); alert( 'Contents copied.' );">
						<asp:Panel id="m_ErrorList" runat="server" style="WIDTH: 100%; HEIGHT: 200px; PADDING: 4px; BORDER: 2px solid lightgrey; BORDER-RIGHT: 0px; OVERFLOW-Y: scroll;" Visible="False">
							<ml:CommonDataGrid id="m_Errors" AutoGenerateColumns="true" runat="server">
							</ml:CommonDataGrid>
						</asp:Panel>
					</td>
				</tr>
				<tr>
					<td width=1000px height="0%" colspan="2">
						<div style="PADDING: 0px; TEXT-ALIGN: right;">
							<asp:Button id="m_Import" runat="server" style="DISPLAY: none;" OnClick="ImportClick"></asp:Button>
		                    <asp:Button Text="Export" ID="exportBtn" runat="server" OnClick="OnExportClick" Visible = "false" />
							<input type="button" value="Import" style="WIDTH: 50px;" onclick="if( parentElement.children[ 0 ].disabled == false ) { parentElement.children[ 0 ].click(); disabled = true; }">
							<input type="button" value="Close" style="WIDTH: 50px;" onclick="onClosePopup();">
						</div>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
