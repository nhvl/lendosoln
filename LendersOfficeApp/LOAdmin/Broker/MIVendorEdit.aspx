﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MIVendorEdit.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.MIVendorEdit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Configuration For MI Vendors</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">	
	<style type="text/css">
	    #BrokerSaveWarning
	    {
	        text-align: center;
	        background-color: Yellow;
	        font-weight: bold;
	        font-size: small;
	    }
        #BtnHolder 
        {
            border: 0;
            padding: 1em;
            text-align: center; 
        }
        #VendorTable
        {
            border: 0;
        }
        #VendorTable th, #VendorTable td
        {
            border: 0;
            padding: 0.5em 0.75em;
        }
    </style>
    <script language="javascript">
            function _init() {
                resize(350, 350);
            }
    </script>
</head>
<body>
    <form id="VendorForm" runat="server">
    <div>
        <asp:GridView ID="VendorTable" runat="server" AutoGenerateColumns="false" DataKeyNames="VendorId" cssclass="GroupTable" RowStyle-CssClass="GridAlternatingItem" HeaderStyle-CssClass="GridHeader" Width="100%">
            <Columns>
                <asp:BoundField HeaderText="MI Vendors" DataField="MIVendor" HeaderStyle-Width="40%" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Enable" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <asp:DropDownList runat="server" ID="MIVendorBrokerSettingStatus" SelectedValue='<%# AspxTools.HtmlString(Eval("Enabled")) %>'>
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes - Test Submissions" Value="1" />
                        <asp:ListItem Text="Yes - Produdction Submissions" Value="2" />
                    </asp:DropDownList>
                </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <br />
    <div id="BrokerSaveWarning">
        ***Disabling vendor will clear broker credentials***
    </div>
    <div id="BtnHolder">
        <asp:Button ID="m_save" Text="Save" OnClick="Save" runat="server" />
        <input type="button" value="Cancel" onclick="onClosePopup()" />
    </div>
    </form>
</body>
</html>
