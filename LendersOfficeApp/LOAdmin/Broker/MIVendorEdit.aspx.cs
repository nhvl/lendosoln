﻿// <copyright file="MIVendorEdit.aspx.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Isaac Ribakoff
//  Date:   9/23/2014 11:37:51 AM
// </summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Integration.MortgageInsurance;
using System.Xml.Linq;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class MIVendorEdit : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Gets the required permission for the current LOAdmin page.
        /// </summary>
        /// <returns>Permission required to access this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        private IEnumerable<MortgageInsuranceVendorConfig> allVendors;
        private IEnumerable<MortgageInsuranceVendorBrokerSettings> enabledVendors;

        public Guid BrokerID
        {
            get { return RequestHelper.GetGuid("BrokerID"); }
        }

        /// <summary>
        /// Gets a list of all active Mortgage Insurance Vendors.
        /// </summary>
        /// <value>An IEnumerable of active Mortgage Insurance Vendors.</value>
        private IEnumerable<MortgageInsuranceVendorConfig> AllVendors
        {
            get
            {
                if (this.allVendors == null)
                {
                    this.allVendors = MortgageInsuranceVendorConfig.ListActiveVendors();
                }

                return this.allVendors;
            }
        }

        /// <summary>
        /// Gets a list of enabled Mortgage Insurance Vendors for the given broker.
        /// </summary>
        /// <value>An IEnumerable of enabled Mortgage Insurance Vendors for the given broker.</value>
        private IEnumerable<MortgageInsuranceVendorBrokerSettings> EnabledVendors
        {
            get
            {
                if (this.enabledVendors == null)
                {
                    this.enabledVendors = MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(this.BrokerID);
                }

                return this.enabledVendors;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                this.VendorTable.DataSource = this.AllVendors.Select(vendor =>
                new
                {
                    VendorId = vendor.VendorId,
                    MIVendor = vendor.VendorTypeFriendlyDisplay,
                    Enabled = (int)this.Get_E_MIVendorBrokerSettingStatusT(vendor)
                });
                this.VendorTable.DataBind();
            }
        }

        
        protected void Save(object sender, EventArgs e)
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            Guid brokerId = this.BrokerID;

            var gridViewRows = VendorTable.Rows;
            var dataKeys = VendorTable.DataKeys;

            int rows = gridViewRows.Count;
            int selectedRow = 0;

            try
            {
                for (; selectedRow < rows; selectedRow++)
                {
                    Guid vendorId = (Guid)dataKeys[selectedRow].Value;
                    DropDownList ddl = (DropDownList)gridViewRows[selectedRow].FindControl("MIVendorBrokerSettingStatus");
                    E_MIVendorBrokerSettingStatusT setting = (E_MIVendorBrokerSettingStatusT)Tools.GetDropDownListValue(ddl);
                    MortgageInsuranceVendorBrokerSettings.SetEntry(vendorId, brokerId, setting);
                }
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.onload = function(){onClosePopup();}", true);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string message = string.Format("Cannot Save Row {0}. SqlException: {1}", selectedRow + 1, ex.ToString());
                this.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('"+message+"')",true);
            }
            catch (UnhandledEnumException ex)
            {
                string message = string.Format("Cannot Save Row {0}. Unhandled Enum in dropdown: {1}", selectedRow + 1, ex.DeveloperMessage);
                this.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + message + "')",true);
            }

        }

        /// <summary>
        /// Determines broker setting status of specific MI Vendor.
        /// </summary>
        /// <param name="vendor">Unique Identifier of MI Vendor.</param>
        /// <returns>Enum representing the broker setting status of given MI Vendor.</returns>
        private E_MIVendorBrokerSettingStatusT Get_E_MIVendorBrokerSettingStatusT(MortgageInsuranceVendorConfig vendor)
        {

            MortgageInsuranceVendorBrokerSettings bs = this.EnabledVendors.FirstOrDefault(
                                                  ev => (ev.BrokerId == this.BrokerID) && (ev.VendorId == vendor.VendorId));
            if (bs == null)
            {
                return E_MIVendorBrokerSettingStatusT.Disabled;
            }
            else
            {
                return bs.IsProduction ? E_MIVendorBrokerSettingStatusT.Production : E_MIVendorBrokerSettingStatusT.Test;
            }
        }
    }
}
