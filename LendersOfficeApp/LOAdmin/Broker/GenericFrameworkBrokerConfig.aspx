﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenericFrameworkBrokerConfig.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.GenericFrameworkBrokerConfig" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Broker Generic Framework Vendor Configuration</title>
    <style type="text/css">
        #BtnHolder { text-align: center; }
        #ErrorMsg { color: Red; }
        tr.Dirty { background-color: LightSalmon; }
        th, td
        {
            border: 0;
            text-align: center;
            vertical-align: middle;
            border-bottom: solid 1px Gray;
        }
        th { padding: 1em; }
        table { border: 0; margin: 0.75em; }
        .Name
        {
            padding: 0.5em;
        }
        .CredentialXML, .LaunchLinkConfigXML
        {
            width: 30em;
            height: 7em;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="VendorTable" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField HeaderText="Enabled?">
                    <ItemTemplate><%# AspxTools.HtmlControl(GenerateCheckboxInput(Eval, "Enabled", false)) %></ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Provider ID" DataField="ProviderID" ItemStyle-CssClass="ProviderID" />
                <asp:BoundField HeaderText="Name" DataField="Name" ItemStyle-CssClass="Name" />
                <asp:TemplateField HeaderText="Include Username">
                    <ItemTemplate>
                        <%# AspxTools.HtmlControl(GenerateCheckboxInput(Eval, "IncludeUsername", true)) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Credential XML">
                    <ItemTemplate>
                        <%# AspxTools.HtmlControl(GenerateTextArea(Eval, "CredentialXML")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Launch Link Configuration XML">
                    <ItemTemplate>
                        <%# AspxTools.HtmlControl(GenerateTextArea(Eval, "LaunchLinkConfigXML"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate><input type="button" class="SaveRowBtn" value="Save" /></ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div id="BtnHolder">
        <input type="button" id="CloseBtn" value="Close" />
        <span id="ErrorMsg" />
    </div>
    </form>
</body>
<script type="text/javascript">
    function GetTR(el) {
        return $j(el).closest('tr');
    }

    function ToggleDisabled(tr, classNameToFind, isDisabled) {
        DisableElement(tr.find('.' + classNameToFind), isDisabled);
    }
    function DisableElement(el, isDisabled) {
        $j(el).prop('disabled', isDisabled);
    }

    // represents a row in the table
    function VendorRecord(tr) {
        this.providerID          = tr.find('.ProviderID').text();
        this.name                = tr.find('.Name').text();
        this.includeUsername     = tr.find('.IncludeUsername').prop('checked');
        this.credentialXML       = tr.find('.CredentialXML').val();
        this.launchLinkConfigXML = tr.find('.LaunchLinkConfigXML').val();
        this.enabled             = tr.find('.Enabled').prop('checked');
    }

    $j('#CloseBtn').click(CloseWindow);
    DisableElement('.SaveRowBtn', true);
    $j('.SaveRowBtn').click(function() { SaveRow(GetTR(this)); });
    $j('.Enabled, .IncludeUsername, .CredentialXML, .LaunchLinkConfigXML').change(function() { OnItemChange(this); });
    $j('.Enabled').not(':checked').each(function()
    {
        var tr = GetTR(this);
        DisableElement(tr.find('.IncludeUsername'), true);
        DisableElement(tr.find('.CredentialXML'), true);
        DisableElement(tr.find('.LaunchLinkConfigXML'), true);
    });

    function OnItemChange(el) {
        var tr = GetTR(el);
        tr.addClass('Dirty');
        DisableElement(tr.find('.SaveRowBtn'), false);
        if ($j(el).is('.Enabled')) {
            var checked = $j(el).is(':checked');
            DisableElement(tr.find('.CredentialXML'), !checked);
            DisableElement(tr.find('.LaunchLinkConfigXML'), !checked);
            if (checked) {
                StashExistingTextToHiddenInput(tr, 'CredentialXML');
                StashExistingTextToHiddenInput(tr, 'LaunchLinkConfigXML');
            } else {
                UnStashOldTextFromHiddenInput(tr, 'CredentialXML');
                UnStashOldTextFromHiddenInput(tr, 'LaunchLinkConfigXML');
                DisableElement(tr.find('.CredentialXML'), true);
                DisableElement(tr.find('.LaunchLinkConfigXML'), true);
            }
        }
    }

    function StashExistingTextToHiddenInput(tr, classNameToRead) {
        var item = tr.find('.' + classNameToRead);
        var input = document.createElement('input');
        input.type = 'hidden';
        input.className = classNameToRead + '_old';
        input.value = item.text();
        item[0].parentNode.appendChild(input);
    }

    function UnStashOldTextFromHiddenInput(tr, classNameToWrite) {
        var input = tr.find('.' + classNameToWrite + '_old')[0];
        if (!!input) {
            tr.find('.' + classNameToWrite).val(input.value);
            var parent = input.parentNode;
            parent.removeChild(input);
        }
    }

    function CloseWindow() {
        var names = '';
        $j('tr.Dirty').each(function(i, item) {
            var vendor = new VendorRecord(GetTR(item));
            if (names.length > 0) names += ', ';
            names += vendor.name + ' (' + vendor.providerID + ')';
        });
        if (names.length === 0 || confirm('The following vendors are waiting to be saved: ' + names + '\nDo you wish to close without saving?'))
            window.close();
    }

    function UpdateErrorMessage(message) {
        $j('#ErrorMsg').val(message);
        if (message != null && message.length > 0) alert(message);
    }

    function SaveRow(tr) {
        UpdateErrorMessage();
        var item = new VendorRecord(tr);
        callWebMethodAsync({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "GenericFrameworkBrokerConfig.aspx/SaveVendor",
            data:
                JSON.stringify({
                    brokerID: <%= AspxTools.JsString(BrokerID) %>,
                    providerID: item.providerID,
                    includeUsername: item.includeUsername,
                    credentialXML: item.credentialXML,
                    launchLinkConfigXML: item.launchLinkConfigXML,
                    enabled: item.enabled
                }),
            dataType: 'json',
            async: false,
            success: function(response) {
                if (!response.d || response.d.Error || !response.d.ProviderID) {
                    UpdateErrorMessage(!response.d ? 'Invalid save response' : (response.d.Error ? response.d.Error : 'Missing Provider ID in save response'));
                } else if (item.providerID === response.d.ProviderID) {
                    tr.removeClass('Dirty');
                    DisableElement(tr.find('.SaveRowBtn'), true);
                } else {
                    UpdateErrorMessage("Provider ID from save [" + response.d.ProviderID + "] did not match saving item's ProviderID [" + item.providerID + "]");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                UpdateErrorMessage(textStatus);
            }
        });
    }
</script>
</html>
