﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.ConversationLog;
    using LendersOffice.ObjLib.Views;
    using LendersOffice.Security;
    using LqbGrammar.Drivers.ConversationLog;

    /// <summary>
    /// Page that allows editing the set of conversation log categories for a lender.
    /// </summary>
    public partial class ConversationLogCategoriesEditor : SecuredAdminPage
    {
        /// <summary>
        /// Gets the passed in broker id.
        /// </summary>
        /// <value>The passed in broker id.</value>
        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("BrokerId");
            }
        }

        /// <summary>
        /// Saves the categories in the order seen in the UI.
        /// </summary>
        /// <param name="categoriesString">The json serialized form of the categories in the UI.</param>
        [System.Web.Services.WebMethod]
        public static void SaveCategories(string categoriesString)
        {
            Tools.LogAndThrowIfErrors(() =>
            {
                string brokerIdString = RequestHelper.GetSafeQueryString("BrokerId");
                var securityToken = SecurityService.CreateToken();
                var viewsOfCategories = SerializationHelper.JsonNetDeserialize<List<ViewOfCategoryForLoAdmin>>(categoriesString);
                var categoriesForSave = viewsOfCategories.Select(v => v.ToCategory(brokerIdString));

                var existingCategoriesFromDriver = ConversationLogHelper.GetAllCategories(securityToken, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
                var orderedCategoryRefs = new List<CategoryReference>();
                foreach (var category in categoriesForSave)
                {
                    var updatedOrCreatedCategory = ConversationLogHelper.SetCategory(securityToken, category, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
                    var categoryRef = updatedOrCreatedCategory.Identity;
                    orderedCategoryRefs.Add(categoryRef);
                }

                ConversationLogHelper.SetCategoryOrder(securityToken, orderedCategoryRefs, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
            });
        }

        /// <summary>
        /// Gets the required permissions to access this page.
        /// </summary>
        /// <returns>The permissions associated with this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker
            };
        }

        /// <summary>
        /// Gets Edge as the page's compatibility mode.
        /// </summary>
        /// <returns>Edge mode.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// The Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.Response.Headers.Add("X-UA-Compatible", "IE=edge");
            this.EnableJquery = true;
            this.PageID = "ConversationLogCategoriesEditor";
            this.PageTitle = "Conversation Log Categories Editor";
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("stylesheet.css");
            
            this.RegisterJsScript("angular-1.4.8.min.js");
        }

        /// <summary>
        /// Event handler for page load event.
        /// </summary>
        /// <param name="sender">The object that triggered the load event.</param>
        /// <param name="e">The info about the load event.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            var categoriesFromDriver = ConversationLogHelper.GetAllCategories(SecurityService.CreateToken(), ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
            var viewOfCategories = categoriesFromDriver.Select(c => new ViewOfCategoryForLoAdmin(c)).ToList();
            this.RegisterJsObjectWithJsonNetSerializer<List<ViewOfCategoryForLoAdmin>>("initialCategories", viewOfCategories);
            this.RegisterJsGlobalVariables("brokerId", this.BrokerId);
        }
    }
}