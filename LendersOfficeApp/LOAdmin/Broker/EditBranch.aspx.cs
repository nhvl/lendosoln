using System;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{

	public partial class EditBranch : LendersOffice.Admin.SecuredAdminPage
	{
		protected LendersOfficeApp.los.admin.BranchAdmin m_Edit;

		/// <summary>
		/// Return this page's required permissions set.  This
		/// test occurs with each page initialization.
		/// </summary>
		/// <returns>
		/// Array of required permissions.
		/// </returns>

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.ViewBrokers
			};
		}

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
            this.EnableJqueryMigrate = false;
            RegisterJsScript("LQBPopup.js");
            m_Edit.DataEditor = CurrentPrincipal.UserId;
            m_Edit.IsInternal = true;
            if (IsPostBack == false)
            {
                m_Edit.DataBroker = RequestHelper.GetGuid("brokerId", Guid.Empty);
                m_Edit.DataSource = RequestHelper.GetGuid("branchId", Guid.Empty);
                m_Edit.DataBind();
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}

}
