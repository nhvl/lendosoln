﻿<%@ Page Language="C#"   AutoEventWireup="true" EnableViewState="false" CodeBehind="TileInsuranceEditor.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.TileInsuranceEditor" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage Vendor Configuration Information</title>
</head>
<body>
    <h4 class="page-header">Manage Vendor Configuration Information</h4>
    <form id="form1" runat="server">
    <div>
        
        <asp:Repeater runat="server" ID="TitleVendorAssociations" OnItemDataBound="TitleVendorAssociations_OnItemDataBound">
            <HeaderTemplate>
                <table id="vendors">
                    <thead>
                        <tr class="GridHeader">
                            <td width="200">Title Vendor</td>
                            <td width="130">Quote Login</td>
                            <td width="130">Quote Password</td>
                            <td width="130">Quote Account ID</td>
                            <td width="130">Title Login</td>
                            <td width="130">Title Password</td>
                            <td width="130">Title Account ID</td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
            <ItemTemplate>
                <tr runat="server" id="r">
                    <td><ml:EncodedLiteral runat="server" ID="Name"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="LoginQuote"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="PasswordQuote"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="AccountIDQuote"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="LoginTitle"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="PasswordTitle"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="AccountIDTitle"></ml:EncodedLiteral></td>
                    <td><a href="#" class="edit" >edit</a></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        
        <br />
        
        <input type="button" value="Close" onclick="onClosePopup();" />
        
        <script type="text/javascript">
            $j(function(){
                function getParameterByName(name)
                {
                  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
                  var regexS = "[\\?&]" + name + "=([^&#]*)";
                  var regex = new RegExp(regexS);
                  var results = regex.exec(window.location.search);
                  if(results == null)
                    return "";
                  else
                    return decodeURIComponent(results[1].replace(/\+/g, " "));
                }
                var $templateRow = $j("<tr id='editrow'><td class='name'></td>"  +
                    "<td class='loginQuote'><input type='text' name='loginQuote'/></td>" + 
                    "<td><input type='password' name='passwordQuote'/></td>" + 
                    "<td><input type='text' name='accountidQuote'/></td>" + 
                    "<td><input type='text' name='loginTitle'/></td>" + 
                    "<td><input type='password' name='passwordTitle'/></td>" + 
                    "<td><input type='text' name='accountidTitle'/></td>" + 
                    
                    "<td><a href='#' class='update'>update</a>" + 
                    " <a href='#' class='cancel'>cancel</a></td>");
                
                
                
                
                $j('#vendors').on('click', 'a.edit', function(){
                    var $this = $j(this), $row = $j(this).parents('tr'), 
                        vendorId = $row.attr('data-id'), 
                        reqAccountId = $row.attr('data-requirescc') == 'True',
                        $tds = $row.children('td'), 
                        name = $tds.first().html(),
                        loginQuote = $tds.eq(1).html(), 
                        accountIdQuote = $tds.eq(3).html(), 
                        loginTitle = $tds.eq(4).html(), 
                        accountIdTitle = $tds.eq(6).html(), 
                        passwordQuote = $tds.eq(2).html(),
                        passwordTitle = $tds.eq(5).html(),
                        editRow = $templateRow.clone();
                    
                    editRow.attr('data-id', vendorId);
                    editRow[0].className = $row[0].className;
                    
                    editRow.find('td.name').html(name);
                    
                    editRow.find('input[name=loginQuote]').val(loginQuote);
                    editRow.find('input[name=loginTitle]').val(loginTitle);
                    editRow.find('input[name=passwordQuote]').val(passwordQuote);
                    editRow.find('input[name=passwordTitle]').val(passwordTitle);
                    
                    if (!reqAccountId) {
                        editRow.find('input[name=accountidQuote]').prop('readonly', 'readonly').val('N/A').css('background-color', 'gray');
                        editRow.find('input[name=accountidTitle]').prop('readonly', 'readonly').val('N/A').css('background-color', 'gray');
                    }
                    else {
                        editRow.find('input[name=accountidQuote]').val(accountIdQuote);
                        editRow.find('input[name=accountidTitle]').val(accountIdTitle);
                    }
                    editRow.insertAfter($row);
                    $row.hide();
                }).on('click', 'a.cancel', function(){
                    var $row = $j(this).parents('tr'), vendorId = $row.attr('data-id'); 
                        hiddenRow = $j('tr[data-id=' + vendorId + ']').show();
                    $row.remove();
                }).on('click', 'a.update', function(){
                    var $row = $j(this).parents('tr'), vendorId = $row.attr('data-id'), 
                        hiddenRow = $j('tr[data-id=' + vendorId + ']'),
                        loginQuote = $row.find('input[name=loginQuote]').val(),
                        accountidQuote = $row.find('input[name=accountidQuote]').val(),
                        passwordQuote = $row.find('input[name=passwordQuote]').val(),
                        loginTitle = $row.find('input[name=loginTitle]').val(),
                        accountidTitle = $row.find('input[name=accountidTitle]').val(),
                        passwordTitle = $row.find('input[name=passwordTitle]').val(),
                        $tds = hiddenRow.children('td'),
                        rem = false;
                        
                    if (loginQuote.length == 0  && loginTitle.length == 0)
                    {
                        rem = true;
                    }
                    else {
                    
                    if (loginQuote.length <= 0 || loginTitle.length <= 0){
                        alert('login is required.');
                        return;
                    }
                    
                    if (passwordQuote.length <= 0 || passwordTitle.length <= 0){
                        alert('password is required.');
                        return;
                    }
                    }
                    
                    var data = {
                        brokerId : getParameterByName('brokerid'),
                        titleVendorId : vendorId,
                        loginQuote : loginQuote,
                        passwordQuote : passwordQuote, 
                        accountIdQuote :accountidQuote, 
                        loginTitle : loginTitle,
                        passwordTitle: passwordTitle, 
                        accountIdTitle :accountidTitle
                    };  
                    callWebMethodAsync({
                        type: 'POST', 
                        url : 'TileInsuranceEditor.aspx/SaveAssociation',
                        data : JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        success : function(){
                            if(rem) {
                                location.reload(true);
                                return;
                            }
                            $row.remove();
                            $tds.eq(1).text(loginQuote);
                            $tds.eq(2).text('******');
                            $tds.eq(3).text(accountidQuote);               
                            $tds.eq(4).text(loginTitle);
                            $tds.eq(5).text('******');
                            $tds.eq(6).text(accountidTitle); 
                            hiddenRow.show();    
                        },
                        error : function(e){
                            alert('Error : ' + JSON.stringify(e));
                        }
                    });
                        

                    
                        
                });
                     Dialog.resizeForIE6And7(600, 300);
                
            });
        </script>
    </div>
    </form>
</body>
</html>
