﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.ObjLib.AcceptableRatesheet;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using System.Data.SqlClient;
using LendersOffice.ConfigSystem;

namespace LendersOfficeApp.LOAdmin.Broker.Workflow
{
    public partial class ImportWorkflowConfigurationFromOtherAccount : LendersOffice.Admin.SecuredAdminPage
    {
        protected string m_errorMessage = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Page.IsPostBack == false)
            {
                Guid editingBrokerId = RequestHelper.GetGuid("editingBrokerId");
                ViewState["EditingBrokerId"] = editingBrokerId;
                BindData();
            }
            else
            {
                if (!string.IsNullOrEmpty(m_SelectedBrokerId.Value))
                {
                    Guid selectedBrokerId = new Guid(m_SelectedBrokerId.Value);
                    try
                    {
                        ImportWorkflow.Import(selectedBrokerId, new Guid(ViewState["EditingBrokerId"].ToString()));
                    }
                    catch (CBaseException c)
                    {
                        m_errorMessage = c.UserMessage;
                        Tools.LogError(c);
                        return;
                    }
                    catch (Exception exc)
                    {
                        m_errorMessage = "Unable to import workflow";
                        Tools.LogError(exc);
                        return;
                    }
                    
                    m_errorMessage = "";
                    ClientScript.RegisterStartupScript(this.GetType(), "_close", " <script>window.close(this);</script>");
                }
            }
        }

        protected void OnSearchClick(object sender, EventArgs e)
        {
            BindData();
        }

        private void BindData()
        {
            var list = ImportWorkflow.Load(m_accName.Text, m_ccode.Text, false);
            m_Brokers.DataSource = list;
            m_Brokers.DataBind();
        }
    }
}
