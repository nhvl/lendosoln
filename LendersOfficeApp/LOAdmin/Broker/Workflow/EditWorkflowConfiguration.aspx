﻿<%@ Page Language="C#" AutoEventWireup="true"
    CodeBehind="EditWorkflowConfiguration.aspx.cs"
    Inherits="LendersOfficeApp.LOAdmin.Broker.Workflow.EditWorkflowConfiguration"
    MaintainScrollPositionOnPostback="true"
%>
<%@ Register TagPrefix="uc" TagName="EditWorkflowConfigurationControl" Src="~/common/Workflow/EditWorkflowConfigurationControl.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Workflow Config Item Editor</title>
    <style type="text/css">
        #m_conditionsGrid tr.ConditionsHeader td:last-child {
            white-space: normal;
        }
        #m_conditionsGrid tr td:last-child {
            white-space: nowrap;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        function _init()
        {
            if (typeof (_initControl) != "undefined")
            {
                _initControl();
            }
        }
    </script>
    <form id="EditWorkflowConfiguration" runat="server">
	    <uc:EditWorkflowConfigurationControl id="editWorkflowConfigurationControl" runat="server" />
    </form>
</body>
</html>
