﻿namespace LendersOfficeApp.LOAdmin.Broker.Workflow
{
    using System;
    using LendersOffice.Common;
    using LendersOffice.Admin;

    public partial class EditWorkflowConfigFieldProtectionRule : SecuredAdminPage
    {
        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// The Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
        }
    }
}