﻿namespace LendersOfficeApp.LOAdmin.Broker.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Web.Services;
    using LendersOffice.Common;
    using LendersOffice.Admin;
    using LendersOfficeApp.common.Workflow;
    using LendersOffice.Security;

    public partial class EditWorkflowConfiguration : SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        [WebMethod]
        public static IEnumerable<object> GetParameterNamesFromRules(Guid brokerId, bool draftMode, string conditionIds, string constraintIds,
                                                                     string restraintIds, string triggerIds)
        {
            return EditWorkflowConfigurationControl.GetParameterNamesFromRules(brokerId, draftMode, conditionIds, constraintIds, restraintIds, triggerIds);
        }

        [WebMethod]
        public static object DeleteParametersValidate(Guid brokerId, bool draftMode, string conditionIds, string constraintIds,
                                                      string restraintIds, string triggerIds, string removedParameterNames)
        {
            return EditWorkflowConfigurationControl.DeleteParametersValidate(brokerId, draftMode, conditionIds, constraintIds,
                                                      restraintIds, triggerIds, removedParameterNames);
        }
    }
}
