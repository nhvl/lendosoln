﻿<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportWorkflowConfigurationFromOtherAccount.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.Workflow.ImportWorkflowConfigurationFromOtherAccount" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Import Workflow Configuration from Other Account</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
	<meta content="C#" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body onload="init();" MS_POSITIONING="FlowLayout">
    <script language="javascript">
		<!--
		function init()
		{
			resize( 700 , 800 );
			
			if(<%= AspxTools.JsString(m_errorMessage) %>.length > 0)
			{
			    alert(<%= AspxTools.JsString(m_errorMessage) %>);
			}
        }

        function f_onSelectAccount(brokerId, brokerName, customerCode) 
        {
            if (confirm("Do you want to import the workflow configuration from " + brokerName + "(" + customerCode + ")?")) 
            {
                <%= AspxTools.JsGetElementById(m_SelectedBrokerId) %>.value = brokerId;
                document.ImportWCForm.submit();
            }
        }

		//-->
    </script>
    <h4 class="page-header">Import Workflow Configuration from Other Account</h4>
    <form id="ImportWCForm" method="post" runat="server">
        <table class="Header" cellSpacing="0" cellPadding="5" width="100%">
	        <tr>
		        <td><b>Select account to import from</b></td>
	        </tr>
        </table>
        <table id="Table1" cellSpacing="1" cellPadding="5" border="0" style="WIDTH: 100%">
            <input type="hidden" runat="server" id="m_SelectedBrokerId" name="m_SelectedBrokerId">
            <tr>
                <td style="width:100px">
                    Account Name: 
                </td>
                <td>
                    <asp:TextBox runat="server" ID="m_accName"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Customer Code:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="m_ccode"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:button ID="m_searchButton" runat="server" Text="Search" OnClick="OnSearchClick"/>
                </td>
            </tr>
        </table>
        <table style="WIDTH: 100%">
            <tr>
                <td>
                    <asp:Repeater ID="m_Brokers" runat="server">
                        <HeaderTemplate>
                            <table id="ResultsTable" cellpadding="2" cellspacing="1"  >
                            <thead>
                                <tr style="background-color:lightblue">
                                    <th style="width:300px"><a id="accNameLink" href="#">Account Name</a></th>
                                    <th style="width:300px"><a id="cCodeLink" href="#">Customer Code</a></th>
                                </tr>
                            </thead>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr style="background-color:white">
                                <td style="width:300px" valign="top">
                                    <DIV style="COLOR: blue; CURSOR: hand; WIDTH: 240px;" nowrap onmouseover="style.color = 'orange';" onmouseout="style.color = 'blue';" onclick="f_onSelectAccount( <%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "BrokerId").ToString())%>, <%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "BrokerNm").ToString())%>, <%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "CustomerCode").ToString())%> ); return false;")>
                                        <U>
                                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerNm").ToString())%>
                                        </U>
                                    </DIV>
                                </td>
                                <td style="width:300px" valign="top">
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CustomerCode").ToString())%>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr style="background-color:#C0C0C0">
                                <td style="width:300px" valign="top">
                                   <DIV style="COLOR: blue; CURSOR: hand; WIDTH: 240px;" nowrap onmouseover="style.color = 'orange';" onmouseout="style.color = 'blue';" onclick="f_onSelectAccount( <%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "BrokerId").ToString())%>, <%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "BrokerNm").ToString())%>, <%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "CustomerCode").ToString())%> ); return false;")>
                                        <U>
                                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerNm").ToString())%>
                                        </U>
                                    </DIV>
                                </td>
                                <td style="width:300px" valign="top">
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CustomerCode").ToString())%>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
        <uc1:cmodaldlg id="Cmodaldlg1" runat="server"></uc1:cmodaldlg>
    </form>
</body>
</html>
