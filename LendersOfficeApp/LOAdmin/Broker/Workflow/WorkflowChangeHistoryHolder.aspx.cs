﻿namespace LendersOfficeApp.LOAdmin.Broker.Workflow
{
    using LendersOffice.Admin;

    /// <summary>
    /// Holder page for workflow history.
    /// </summary>
    public partial class WorkflowChangeHistoryHolder : SecuredAdminPage
    {
    }
}