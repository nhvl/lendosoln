﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkflowConfigComparison.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.Workflow.WorkflowConfigComparison" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Workflow Configuration Comparison Tool</title>
    <link href="../../css/stylesheetnew.css" type=text/css rel=stylesheet>
    <style type="text/css">
        .header {color:#FFFFFF; padding:2px 0px 2px 6px; font-weight:bold}
                
        .StaticHeader
        {
            color: white;
            position:relative;
        }
        .StaticHeader td { background-color:#999999; }
        .DiffGridHeader { top: expression(document.getElementById('m_diffGridContainer').scrollTop - 3); }
        .ReverseGridHeader { top: expression(document.getElementById('m_reverseGridContainer').scrollTop - 3); }

        td.SectionHeader
        {
            background-color: gray;
        }

        td.PrivilegesHeader
        {
            background-color:#00BB00;
        }

        td.RestraintsHeader
        {
            background-color:#BB0000;
        }
    </style>
</head>
<body onload="resize(1200, 960);">
    <form id="form1" runat="server">
        <table>
            <tr>
                <td class="MainRightHeader header" colspan="2">
                    Workflow Configuration Comparison Tool
                </td>
            </tr>
            <tr>
                <td>
                    Config A
                </td>
                <td>
                    Config B
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButtonList ID="m_configOption" runat="server">
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:DropDownList ID="m_brokerOption" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="Compare" Text="Perform Comparison" OnClick="OnCompareClicked" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLiteral ID="m_userMessage" runat="server"></ml:EncodedLiteral> 
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header SectionHeader">
                    New or different in Config A (matching by condition ID)
                </td>
            </tr>
            <tr>
                <td class="header PrivilegesHeader" colspan="2">
                    Privileges - All privileges that do not explicitly include Read Loan operation only apply to users that otherwise have Read Loan privilege
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="m_diffGridContainer" style="overflow:scroll; width:1150px; height:380px">
                        <asp:DataGrid runat="server" id="m_diffGrid" AutoGenerateColumns="true">
				            <headerstyle cssclass="StaticHeader DiffGridHeader" Wrap="true"></headerstyle>
				            <itemstyle cssclass="GridItem" Wrap="true"></itemstyle>
            	            <alternatingitemstyle cssclass="GridAlternatingItem" Wrap="true"></alternatingitemstyle>
						</asp:DataGrid>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td class="header RestraintsHeader" colspan="2">
                    Restraints - These rules take precedence in workflow evaluation - If these rules evaluate to true, the user will not be able to execute a given operation
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="m_diffGridContainer" style="overflow:scroll; width:1150px; height:380px">
                        <asp:DataGrid runat="server" id="Restraints_AvB" AutoGenerateColumns="true">
				            <headerstyle cssclass="StaticHeader DiffGridHeader" Wrap="true"></headerstyle>
				            <itemstyle cssclass="GridItem" Wrap="true"></itemstyle>
            	            <alternatingitemstyle cssclass="GridAlternatingItem" Wrap="true"></alternatingitemstyle>
						</asp:DataGrid>
                    </div>
                </td>
            </tr>
            <td colspan="2">&nbsp;</td>
            <tr>
                <td colspan="2" class="header SectionHeader">
                    New or different in Config B (matching by condition ID)
                </td>
            </tr>
            <tr>
                <td class="header PrivilegesHeader" colspan="2">
                    Privileges - All privileges that do not explicitly include Read Loan operation only apply to users that otherwise have Read Loan privilege
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="m_reverseGridContainer" style="overflow:scroll; width:1150px; height:380px">
                        <asp:DataGrid runat="server" id="m_reverseGrid" AutoGenerateColumns="true">
				            <headerstyle cssclass="StaticHeader ReverseGridHeader" Wrap="true"></headerstyle>
				            <itemstyle cssclass="GridItem" Wrap="true"></itemstyle>
            	            <alternatingitemstyle cssclass="GridAlternatingItem" Wrap="true"></alternatingitemstyle>
						</asp:DataGrid>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td class="header RestraintsHeader" colspan="2">
                    Restraints - These rules take precedence in workflow evaluation - If these rules evaluate to true, the user will not be able to execute a given operation
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="m_diffGridContainer" style="overflow:scroll; width:1150px; height:380px">
                        <asp:DataGrid runat="server" id="Restraints_BvA" AutoGenerateColumns="true">
				            <headerstyle cssclass="StaticHeader DiffGridHeader" Wrap="true"></headerstyle>
				            <itemstyle cssclass="GridItem" Wrap="true"></itemstyle>
            	            <alternatingitemstyle cssclass="GridAlternatingItem" Wrap="true"></alternatingitemstyle>
						</asp:DataGrid>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
