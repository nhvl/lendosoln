﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ConfigSystem;
using System.Data;
using ConfigSystem;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Broker.Workflow
{
    public partial class WorkflowConfigComparison : LendersOffice.Admin.SecuredAdminPage
    {
        private const string DRAFT = "Draft";
        private const string RELEASE = "Release";
        private const string SYSTEM = "System";

        protected Guid m_brokerId
        {
            get
            {
                return RequestHelper.GetGuid("brokerid", Guid.Empty);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var list = ImportWorkflow.Load("", "", false);
                m_brokerOption.DataSource = list;
                m_brokerOption.DataTextField = "BrokerNm";
                m_brokerOption.DataValueField = "BrokerId";
                m_brokerOption.DataBind();
                m_brokerOption.Items.Insert(0, new ListItem(DRAFT, DRAFT));
                m_brokerOption.Items.Insert(1, new ListItem(RELEASE, RELEASE));
                m_brokerOption.Items.Insert(2, new ListItem(SYSTEM, Guid.Empty.ToString()));
                m_configOption.Items.Add(new ListItem(DRAFT, DRAFT));
                m_configOption.Items.Add(new ListItem(RELEASE, RELEASE));
                m_configOption.SelectedValue = DRAFT;
            }
        }

        protected void OnCompareClicked(object sender, EventArgs e)
        {
            m_userMessage.Text = "";
            if (m_configOption.SelectedValue == m_brokerOption.SelectedValue)
            {
                m_userMessage.Text = "Both selections are the same";
                return;
            }

            WorkflowSystemConfigModel configA = new WorkflowSystemConfigModel(m_brokerId, m_configOption.SelectedValue == DRAFT);
            WorkflowSystemConfigModel configB;
            if (m_brokerOption.SelectedValue == DRAFT || m_brokerOption.SelectedValue == RELEASE)
                configB = new WorkflowSystemConfigModel(m_brokerId, m_brokerOption.SelectedValue == DRAFT);
            else
                configB = new WorkflowSystemConfigModel(new Guid(m_brokerOption.SelectedValue), false);
            
            ConditionSet configARules = configA.GetFullConditionSet();
            ConditionSet configBRules = configB.GetFullConditionSet();
            m_diffGrid.DataSource = configA.GenerateDataTable(ConditionSetDiff.DetectExternalCombinations(configARules, configBRules), WorkflowSystemConfigModel.TableType.Conditions);
            m_diffGrid.DataBind();
            m_reverseGrid.DataSource = configA.GenerateDataTable(ConditionSetDiff.DetectExternalCombinations(configBRules, configARules), WorkflowSystemConfigModel.TableType.Conditions);
            m_reverseGrid.DataBind();

            Restraints_AvB.DataSource = configA.GenerateDataTable(ConditionSetDiff.DetectExternalCombinations(configA.GetFullRestraintSet(), configB.GetFullRestraintSet()), WorkflowSystemConfigModel.TableType.Conditions);
            Restraints_AvB.DataBind();
            Restraints_BvA.DataSource = configA.GenerateDataTable(ConditionSetDiff.DetectExternalCombinations(configB.GetFullRestraintSet(), configA.GetFullRestraintSet()), WorkflowSystemConfigModel.TableType.Conditions);
            Restraints_BvA.DataBind();
        }
    }
}
