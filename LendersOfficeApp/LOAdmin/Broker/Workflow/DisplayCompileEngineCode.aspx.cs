﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using ConfigSystem.DataAccess;
using LendersOffice.ConfigSystem;
using ConfigSystem.Engine;

namespace LendersOfficeApp.LOAdmin.Broker.Workflow
{
    public partial class DisplayCompileEngineCode : LendersOffice.Admin.SecuredAdminPage
    {
        private Guid m_brokerId
        {
            get
            {
                return RequestHelper.GetGuid("brokerid", Guid.Empty);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            IConfigRepository configRepository = ConfigHandler.GetUncachedRepository(m_brokerId);
            ExecutingEngine engine = ExecutingEngine.GetEngineByBrokerId(configRepository, m_brokerId);

            if (engine != null)
            {
                m_code.InnerText = engine.DisplayDebugCompiledCode();
            }
        }
    }
}
