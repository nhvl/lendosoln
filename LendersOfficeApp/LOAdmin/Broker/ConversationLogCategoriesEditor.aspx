﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConversationLogCategoriesEditor.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.ConversationLogCategoriesEditor" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Conversation Log Categories Editor</title>
    <style type="text/css">
        tbody > tr:nth-child(odd)
        {
            font-size: 11px;
            color: black;
            font-family: Arial, Helvetica, sans-serif;
            background-color: white;
        }
        tbody > tr:nth-child(even)
        {
	        font-size: 11px;
	        font-family: Arial, Helvetica, sans-serif;
	        color: Black ;
	        background-color: #CCCCCC ;
        }
        a, img { outline: none; }
        a:hover, a:active, a:focus { outline: 0; border: 0; text-decoration: none;}
        a.moveArrow:link { text-decoration: none; }
        a img { border: 0; }
        a.moveArrow img { width: 10px; height: 10px; }
        .moveContainer { margin-left: 10px; }
        .addNewBtnSection
        {
            float: left;
            padding: 10px;
        }
        .windowBtnSection
        {
            margin: 10px;
        }
    </style>
</head>
<body ng-app="mainApp" ng-controller="categoriesListCtrl">
    <form id="form1" runat="server">        
    <script type="text/javascript">
        var app = angular.module('mainApp', []);

        jQuery(function ($) {
            resize(600, 600);

            app.factory('categoriesSaveService', function ($http) {
                return {
                    save: function (categories, successMethod, failedMethod) {
                        //return the promise directly.
                        return $http.post(
                            'ConversationLogCategoriesEditor.aspx/SaveCategories?brokerid=' + ML.brokerId, 
                            { 
                                categoriesString: angular.toJson(categories) // instead of JSON.stringify to avoid $$hashObject props.
                            }
                            )
                            .then(function (result) {
                                      //resolve the promise as the data
                                    successMethod(result.data.d);
                                }, function (response) {
                                    failedMethod(response)
                                });
                    }
                }
            });

            app.controller('categoriesListCtrl', function ($scope, categoriesSaveService) {
                $scope.categories = angular.copy(initialCategories);

                window.onbeforeunload = function(e) {
                    if(!angular.equals(initialCategories, $scope.categories))
                    {
                        e.returnValue = 'You have unsaved changes.  Really close without saving?';
                        return e.returnValue;
                    }
                };

                $scope.addOrUpdateCategory = function(index)
                {
                    var category = {id:null};
                    var otherCategoryNames = $scope.categories.map(function(c) { return c.name.toUpperCase()});
                    var otherCategoryDisplayNames = $scope.categories.map(function(c) { return c.displayName.toUpperCase()});
                    if(typeof(index) !== 'undefined')
                    {
                        category = $scope.categories[index];
                        otherCategoryNames.splice(index, 1);
                        otherCategoryDisplayNames.splice(index,1)
                    }
                
                    var args = new cGenericArgumentObject();
                    args.model = {};
                    args.model.category = category;
                    args.model.otherCategoryNames = otherCategoryNames;
                    args.model.otherCategoryDisplayNames = otherCategoryDisplayNames;
                    var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("/LOAdmin/Broker/ConversationLogCategoryEditor.aspx")) %>;
                    var fullUrl = url + '?brokerId='+ ML.brokerId;
                
                    showModal(fullUrl, args, null, true, function(result){ 
                        if(result.OK)
                        {
                            if(typeof(index) !== 'undefined')
                            {
                                $scope.categories[index] = result.model.category;
                            }
                            else
                            {
                                $scope.categories.push(result.model.category);
                            }

                            $scope.$apply();
                        }
                    });
                }

                $scope.moveCategoryUp = function(index){
                    if(index === 0)
                    {
                        // nothing to do, it's already at the top.
                        return;
                    }
                    
                    swapCategories(index, index-1);
                }

                $scope.moveCategoryDown = function(index){
                    var lastIndex = $scope.categories.length -1;
                    if(index === lastIndex)
                    {
                        // nothing to do, it's already at the bottom.
                        return;
                    }
                    
                    swapCategories(index, index+1);
                }

                $scope.moveCategoryToTop = function(index)
                {
                    if(index === 0)
                    {
                        return; // already at top.
                    }
                    var removedCategories = $scope.categories.splice(index, 1);
                    $scope.categories.splice(0, 0, removedCategories[0]);
                }

                $scope.moveCategoryToBottom = function(index)
                {
                    var lastIndex = $scope.categories.length -1;
                    if(index === lastIndex)
                    {
                        return; // already at bottom.
                    }
                    var removedCategories = $scope.categories.splice(index, 1);
                    $scope.categories.push(removedCategories[0]);
                }

                function swapCategories(index1, index2)
                {
                    var cat1 = $scope.categories[index1];
                    $scope.categories[index1] = $scope.categories[index2];
                    $scope.categories[index2] = cat1;
                }
                                
                $scope.okClick = function () {
                    if(angular.equals(initialCategories, $scope.categories))
                    {
                        // just close since nothing has changed.
                        onClosePopup();
                        return;
                    }

                    var $okBtn = $('#OkBtn');
                    $okBtn.attr('disabled', 'disabled');
                    categoriesSaveService.save(
                        $scope.categories,
                        function (data) {
                            window.onbeforeunload = function(){};
                            onClosePopup();
                        }, function (response) {
                            alert('failed to save: ' + response.data.Message);
                            $okBtn.removeAttr('disabled');
                        });
                };

                $scope.closeClick = function()
                {
                    onClosePopup();
                }
            });
        });
    </script>
    <div class="MainRightHeader" nowrap="true">
        Conversation Log Categories
    </div>
    <br />
    <div id="CategoriesSection">
        <table>
            <thead>
                <tr class="GridHeader">
                    <th>
                        <!-- edit link -->
                    </th>
                    <th>Re-Order</th>
                    <%--<th>Category (DEBUG-ONLY)</th>--%>
                    <th>Display Name</th>
                    <th>Active</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="category in categories">
                    <td>
                        <a class="editLink" ng-click="addOrUpdateCategory($index)">edit</a>
                    </td>
                    <td>           
                        <div class="moveContainer top">
                         <a class="moveArrow Up" title="Move the condition one space up in the list" ng-click="moveCategoryUp($index)"><img src="../../images/up-blue.gif"  /></a>
                         <a class="moveArrow Top" title="Move the condition to the top of the list" ng-click="moveCategoryToTop($index)"><img src="../../images/up-blue-top.gif"  /></a>
                        </div>
                        <div class="moveContainer">
                         <a class="moveArrow  Down" title="Move the condition one space down in the list" ng-click="moveCategoryDown($index)"><img src="../../images/down-blue.gif"  /></a>
                         <a class="moveArrow Bottom" title="Move the condition to the bottom of the list" ng-click="moveCategoryToBottom($index)"><img src="../../images/down-blue-bottom.gif"  /></a>
                        </div>
                    </td>
                    <%--<td>{{category.name}}</td>--%>
                    <td>{{category.displayName}}</td>
                    <td><input type="checkbox" disabled="disabled" ng-model="category.isActive"/></td>
                </tr>
            </tbody>
            
        </table>
    </div>
    <div class="addNewBtnSection">
        <input type="button" id="AddNewBtn" value="Add Category" ng-click="addOrUpdateCategory()"/>
    </div> <br /><br />
    <div class="windowBtnSection">
        <input type="button" id="OkBtn" value="OK" ng-click="okClick();"/>
        <input type="button" id="CloseBtn" value="Close" ng-click="closeClick();"/>
    </div>
    </form>
</body>
</html>
