﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditEmployeeGroup.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.EditEmployeeGroup" %>
<%@ Register TagPrefix="uc" TagName="EditEmployeeGroupControl" Src="~/common/Workflow/EditEmployeeGroupControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Edit Employee Group</title>    
</head>
<body>
    <script type="text/javascript">
        function _init()
        {
            if (typeof (_initControl) != "undefined")
            {
                _initControl();
            }
        }
      </script>
    <form id="form1" runat="server">    
        <uc:EditEmployeeGroupControl id="editEmployeeGroupControl" runat="server" />
    </form>
</body>
