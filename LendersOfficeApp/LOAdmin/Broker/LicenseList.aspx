<%@ Page language="c#" Codebehind="LicenseList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.LicenseList" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LicenseList</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function _init()
		{
			resize( 700 , 500 );
		}
		function showUserList(licenseId)
		{
			showModal("/loadmin/broker/UserAssocWithLicenseList.aspx?licenseId=" + licenseId, null, null, null, function(arg){ 
				if (arg.change == "T")
				{
					document.OpenLicensesList.submit() ;
				}
			});
		}
		function onAddLicense()
		{
			showModal("/loadmin/broker/LicenseEdit.aspx?brokerId=<%=AspxTools.HtmlString(m_brokerId.ToString())%>") ;
		}
		function editLicense(licenseId)
		{
			showModal("/loadmin/broker/LicenseEdit.aspx?brokerId=<%=AspxTools.HtmlString(m_brokerId.ToString())%>&licenseId=" + licenseId, null, null, null, function(arg){ 
				if (arg.change == "T")
				{
					document.OpenLicensesList.submit() ;
				}
			});
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" onload="_init();">
		<h4 class="page-header">License List</h4>
		<FORM id="OpenLicensesList" method="post" runat="server">
			<table cellSpacing="2" cellPadding="3" width="100%" border="0">
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" border="0" width="100%">
							<tr>
								<td>
									<input type="button" onclick="onAddLicense();" value="Add a license"> <input type="button" onclick="self.close();" value="Close">
									<hr>
								</td>
							</tr>
							<tr>
								<td>
								    <div class="Tabs">
								        <ul class="tabnav">
								            <li id="tab0" runat="server"><a href="#" onclick="__doPostBack('changeTab', 0);">Active/Open Licenses</a></li>
								            <li id="tab1" runat="server"><a href="#" onclick="__doPostBack('changeTab', 1);">All Active Licenses</a></li>
								            <li id="tab2" runat="server"><a href="#" onclick="__doPostBack('changeTab', 2);">All Licenses</a></li>
								        </ul>
								    </div>
								</td>
							</tr>
							<TR>
								<TD>
									<ml:CommonDataGrid id="dgLicenses" runat="server" AutoGenerateColumns="False" Width="100%" AllowSorting="True" CssClass="DataGrid" DataKeyField="LicenseId" EnableViewState="False">
										<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
										<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
										<HeaderStyle CssClass="GridHeader"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="LicenseNumber" SortExpression="LicenseNumber" HeaderText="Lic #"></asp:BoundColumn>
											<asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="License Description"></asp:BoundColumn>
											<asp:BoundColumn DataField="PurchaseDate" SortExpression="PurchaseDate" HeaderText="Purchase" DataFormatString="{0:d}"></asp:BoundColumn>
											<asp:BoundColumn DataField="ExpirationDate" SortExpression="ExpirationDate" HeaderText="Expiration" DataFormatString="{0:d}"></asp:BoundColumn>
											<asp:BoundColumn DataField="SeatCount" HeaderText="Total Seats"></asp:BoundColumn>
											<asp:BoundColumn DataField="OpenSeatCount" SortExpression="OpenSeatCount" HeaderText="Open Seats"></asp:BoundColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<A onclick=<%#AspxTools.HtmlAttribute("editLicense(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "LicenseId").ToString()) + "); return false;")%> href="#">
														edit</A>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<A onclick=<%#AspxTools.HtmlAttribute("showUserList(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "LicenseId").ToString()) + "); return false;")%> href="#">
														users</A>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:LinkButton id="m_SelectLicense" runat="server">select</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</ml:CommonDataGrid></TD>
							</TR>
							<TR>
								<TD align="center">&nbsp;
								</TD>
							</TR>
						</table>
					</td>
				</tr>
			</table>
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
		</FORM>
	</body>
</HTML>
