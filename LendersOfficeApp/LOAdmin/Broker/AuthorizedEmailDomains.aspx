﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuthorizedEmailDomains.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.AuthorizedEmailDomains" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" ng-app="AuthorizedEmailDomains">
    <head runat="server">
        <title>Authorized Email Domains</title>
    </head>
    <body>
        <form id="form1" runat="server">
            <div id="AuthorizedEmailDomainsDiv" ng-controller="AuthorizedEmailDomainsController" >
                <div id="domainList">
                    <div ng-repeat="domain in domains">
                        {{domain.Name}}
                        <a class="RemoveLink" ng-click="removeDomain($index)">remove</a>
                    </div>
                </div>
                <br />
                <input type="text" id="AddDomainText" maxlength="50" ng-keypress="addOnEnter($event)" ng-keydown="addOnEnter($event)" /> <input type="button" value="Add" ng-click="addDomain()"/>
                <br />
                <br />
                <input type="button" id="CloseBtn" ng-click="closeDomain()"  value="Close" />
            </div>
        </form>
    </body>
</html>
