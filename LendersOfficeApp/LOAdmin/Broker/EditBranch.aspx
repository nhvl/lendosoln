<%@ Register TagPrefix="uc" TagName="BranchAdmin" Src="../../los/admin/BranchAdmin.ascx" %>
<%@ Page language="c#" Codebehind="EditBranch.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.EditBranch" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD runat="server">
	<title>Edit Branch</title>
</HEAD>
<body style="OVERFLOW-Y: auto; BACKGROUND-COLOR: #003366" onload="onInit();">
	<script type="text/javascript">
		function onInit()
		{
			if( document.getElementById("m_errorMessage") != null )
			{
				alert( document.getElementById("m_errorMessage").value );
			}
			else
			if( document.getElementById("m_commandToDo") != null )
			{
				if( document.getElementById("m_commandToDo").value == "Close" )
				{
					self.close();
				}
			}
			else
			if( document.getElementById("m_addToBranch") != null )
			{
				showModal( "/loadmin/broker/EditEmployee.aspx?branchId=" + document.getElementById("m_addToBranch").value + "&brokerId=" + document.getElementById("m_addToBroker").value );
			}

			if( window.dialogArguments != null )
			{
				<% if( IsPostBack) { %>				
				    resize( 760 , 760 );				
				<% } else{%>
				    resize( 720 , 620 );
				<% }%>
			}
		}
	</script>
	<form id="EditBranch" method="post" runat="server" class="expand-w-content">
		<h4 class="page-header">Edit Branches</h4>
		<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
			<tr>
				<td height="100%" style="BORDER-RIGHT: 2px outset; BORDER-TOP: 2px outset; BORDER-LEFT: 2px outset; BORDER-BOTTOM: 2px outset; BACKGROUND-COLOR: gainsboro">
					<uc:BranchAdmin id="m_Edit" runat="server"/>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
