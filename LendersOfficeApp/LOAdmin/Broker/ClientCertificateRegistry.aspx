﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClientCertificateRegistry.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.ClientCertificateRegistry" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Client Certificate Registry</title>
    <style type="text/css">
        .AlignLeft
        {
        	text-align: left;
        }
        #ClientCertificateGrid
        {
            max-height: 300px;
            overflow: auto;
        }
        .addNewBtnSection
        {
            float: left;
            padding: 10px;
        }
        .closeBtnSection
        {
            float: right;
            padding: 10px;
        }
        .CertTable
        {
            padding-left: 5px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            function switchTab(tab)
            {
                $(".CertTable").hide();
                $j('#Tabs').children('li').removeClass('selected');

                $("#" + tab + "Section").show();
                $j('#' + tab + "Tab").addClass('selected');

                if(tab === "Revoked")
                {
                    $(".addNewBtnSection").hide();
                }
                else
                {
                    $(".addNewBtnSection").show();
                }

                $("#CurrentTab").val(tab);
            }

            
            function openEditCertificateWindow(isNew, certId)
            {
                var isNewParam;
                var certIdParam;
                var brokerId = $("#BrokerIdString").val();
                if(isNew === true)
                {
                    isNewParam = "t";
                    certIdParam = "";
                }
                else
                {
                    isNewParam = "f";
                    certIdParam = certId;
                }

                var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("/LOAdmin/Broker/EditClientCertificate.aspx")) %> + '?BrokerId=' + brokerId + '&certificateId=' + certIdParam + '&isNew=' + isNewParam; 
                showModal(url, null, null, null, function(args){ 
                    __doPostBack(null, null);
                });
            }

            $(".tab").click(function (e) {
                switchTab($(this).attr("tab"));
            });

            $("#AddNewBtn").click(function (e) {
                openEditCertificateWindow(true);
            });

            $(".EditLink").click(function (e) {
                openEditCertificateWindow(false, $(this).attr('certid'));
            });

            $(".ViewGroupsLink").click(function (e) {
                var isEdit = "f";
                var brokerId = $("#BrokerIdString").val();
                var certId = $(this).attr('certid');
                var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("~/LOAdmin/Broker/AssociateGroups.aspx")) %> + '?bId=' + brokerId + '&IsEdit=' + isEdit + '&certId=' + certId;
                window.open(url, 'viewGroups', 'height=800,width=600,menubar=no,scrollbars=yes,resizable=yes');
            });

            $(".RevokeLink").click(function (e) {
                var certName = $(this).attr('certName');

                if (!confirm("Do you want to revoke the " + certName + " certificate?"))
                {
                    return;
                }

                var args =
                {
                    BrokerId: $("#BrokerIdString").val(),
                    CertificateId: $(this).attr('certid')
                };

                gService.certService.callAsyncSimple("RevokeCertificate", args, RevokeCertificateCallback);
            });

            function RevokeCertificateCallback() {
                __doPostBack(null, null)
            }

            $(".RestoreLink").click(function (e) {
                var certName = $(this).attr('certName');

                if (!confirm("Do you want to restore the " + certName + " certificate?")) {
                    return;
                }

                var args = {
                    BrokerId: $("#BrokerIdString").val(),
                    CertificateId: $(this).attr('certid')
                };

                gService.certService.call("RestoreCertificate", args);
                __doPostBack(null, null)
            });

            switchTab($("#CurrentTab").val());
        });
    </script>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="BrokerIdString" runat="server" />
        <asp:HiddenField ID="CurrentTab" runat="server" />
    </div>
    <div class="MainRightHeader" nowrap="true">
        Client Certificate Registry
    </div>
    <div class="TabSection">
        <ul id="Tabs" class="tabnav">
            <li id="ActiveTab">
                <a href="javascript:void(0);" tab="Active" id="ActiveTabLink" class="tab">Active</a>
            </li>
            <li id="RevokedTab">
                <a href="javascript:void(0);" tab="Revoked" id="RevokedTabLink" class="tab">Revoked</a> 
            </li>
        </ul>
    </div>
    <br />
    <div runat="server" id="ActiveSection" class="CertTable">
        <asp:GridView runat="server" ID="ActiveCertGrid" AutoGenerateColumns="false" EnableViewState="false" OnRowDataBound="CertGrid_DataBound">
            <HeaderStyle cssclass="GridHeader AlignLeft" />
            <AlternatingRowStyle cssclass="GridAlternatingItem" />
            <RowStyle cssclass="GridItem" />

            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="javascript:void(0);" class="EditLink" certid='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CertId").ToString()) %>'>edit</a>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton ID="DescriptionHeaderLink" runat="server" Text="Description"
                            CommandName="Sort"
                            CommandArgument="Active,Description"
                            OnCommand="ClientCertificateGrid_Sort"></asp:LinkButton>
                        <ml:PassthroughLabel ID="DescriptionHeaderArrow" runat="server"></ml:PassthroughLabel>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Notes
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Notes").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Level
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Level").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Group/User
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "GroupUser").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        User Type
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "UserType").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton runat="server" ID="CreatedDateHeaderLink" Text="Created Date"
                            CommandName="Sort"
                            CommandArgument="Active,CreatedDate"
                            OnCommand="ClientCertificateGrid_Sort"></asp:LinkButton>
                        <ml:PassthroughLabel ID="CreatedDateHeaderArrow" runat="server"></ml:PassthroughLabel>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CreatedDate", "{0:MM/dd/yyyy}").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="javascript:void(0);" class="RevokeLink" certName='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString()) %>'
                                                                         certid='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CertId").ToString()) %>'>revoke</a>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="javascript:void(0);" runat="server" id="groupLink" class="ViewGroupsLink" certid='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CertId").ToString()) %>'>view groups</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

    <div runat="server" id="RevokedSection" class="CertTable">
        <asp:GridView runat="server" ID="RevokedCertGrid" AutoGenerateColumns="false" EnableViewState="false" OnRowDataBound="CertGrid_DataBound">
            <HeaderStyle cssclass="GridHeader AlignLeft" />
            <AlternatingRowStyle cssclass="GridAlternatingItem" />
            <RowStyle cssclass="GridItem" />

            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="javascript:void(0);" class="EditLink" certid='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CertId").ToString()) %>'>edit</a>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton ID="DescriptionHeaderLink" runat="server" Text="Description"
                            CommandName="Sort"
                            CommandArgument="Revoked,Description"
                            OnCommand="ClientCertificateGrid_Sort"></asp:LinkButton>
                        <ml:PassthroughLabel ID="DescriptionHeaderArrow" runat="server"></ml:PassthroughLabel>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Notes
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Notes").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Level
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Level").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Group/User
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "GroupUser").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        User Type
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "UserType").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton runat="server" ID="RevokedDateHeaderLink" Text="Revoked Date"
                            CommandName="Sort"
                            CommandArgument="Revoked,RevokedDate"
                            OnCommand="ClientCertificateGrid_Sort"></asp:LinkButton>
                        <ml:PassthroughLabel ID="RevokedDateHeaderArrow" runat="server"></ml:PassthroughLabel>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "RevokedDate", "{0:MM/dd/yyyy}").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="javascript:void(0);" class="RestoreLink" certName='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString()) %>'
                                                                          certid='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CertId").ToString()) %>'>restore</a>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="javascript:void(0);" runat="server" id="groupLink" class="ViewGroupsLink" certid='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CertId").ToString()) %>'>view groups</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div class="addNewBtnSection">
        <input type="button" id="AddNewBtn" value="Add New Registration"/>
    </div>
    <div class="closeBtnSection">
        <input type="button" value="Close" onclick="window.close();" />
    </div>
    </form>
</body>
</html>
