<%@ Page language="c#" Codebehind="UserList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.UserList" EnableEventValidation="true" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOfficeApp.LOAdmin.Broker" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>UserList</title>
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<script type="text/javascript">
		    function onNew()
		    {
			    showModal( '/LOAdmin/Broker/EditEmployee.aspx?brokerID=' + <%= AspxTools.JsString(BrokerId) %>, null, null, null, function(){
					self.location = self.location;
				});
		    }
		    <% if(IsPerTransactionBilling){ %>
		    function onImportEmployeeDMLogins()
		    {
		        showModal( '/LOAdmin/Broker/ImportEmployeeDMLogins.aspx?brokerID='  + <%= AspxTools.JsString(BrokerId) %>, null, null, null, function(){
					self.location = self.location;
				});
			}
			<%} %>
		    function onImportExternalEmployees()
		    {
			    showModal( '/LOAdmin/Broker/ImportExternalEmployees.aspx?brokerID=' + <%= AspxTools.JsString(BrokerId) %>, null, null, null, function(){
					self.location = self.location;
				});
			}
			
			function onImportInternalEmployees()
		    {
			    try {
                    var redirect = '/LOAdmin/Broker/ImportBrokerAttributes.aspx?brokerid=' + <%= AspxTools.JsString(BrokerId) %> + '&brokernm='+ <%=AspxTools.JsString(ViewState["brokernm"].ToString())%> +"&type=IE";
                    showModal(redirect, null, null, null, function(result){
						if (result.OK == true) {
							document.location.reload();
						}
					});
                }
                catch (e) {
                    window.alert(e.message);
                }
            }
            
			function onBatchUpdateCompensationRecords() {
			    showModal( '/LOAdmin/Broker/BatchUpdateCompensationRecords.aspx?brokerID='  + <%= AspxTools.JsString(BrokerId) %>, null, null, null, function(){
					self.location = self.location;
				});
			    
			}
			function onBatchUpdateUserStateLicenses() {
			    showModal('/LOAdmin/Broker/BatchUpdateUserStateLicenses.aspx?brokerID='+<%= AspxTools.JsString(BrokerId) %>, null, null, null, function(){
					self.location = self.location;
				});
			} 
			function onBatchUpdateOCStateLicenses()
			{
			    showModal('/LOAdmin/Broker/BatchUpdateOCStateLicenses.aspx?brokerID='+<%= AspxTools.JsString(BrokerId) %>, null, null, null, function(){
					self.location = self.location;
				});
			}
			function onEdit(employeeID)
		    {
			    showModal( '/LOAdmin/Broker/EditEmployee.aspx?brokerID=<%=AspxTools.JsStringUnquoted(ViewState["brokerid"].ToString())%>&employeeID=' + employeeID, null, null, null, function(){
					self.location = self.location;
				});
		    }
            function becomeUser(userID, loginName) 
            {
                var args = window.dialogArguments || {};

                args.commandName = "become";
                args.commandArgs = userID + ":" + loginName;

                args.OK = true;

                onClosePopup(args);
            }
		    function init()
            {
                resize(900, 600);
				filterUsers(); 
		    }
		    function filterUsers() 
		    {
				var select = document.getElementById("typeFilter");
				var table = <%= AspxTools.JsGetElementById(m_userGrid) %>; 
                var rows = table.rows; 
                var cssClass = "GridItem"; 
                for ( var i = 1;  i < rows.length; i++ ) 
                {
					if ( select.value != "A" && rows[i].childNodes[6].innerHTML != select.value  )	
					{
						rows[i].style.display = "none";
					}
					else 
					{
						rows[i].style.display = "";
						rows[i].className = cssClass; 
						cssClass = cssClass == "GridItem" ? "GridAlternatingItem" : "GridItem";
					}
                }
				
		    }
		</script>
	</HEAD>
	<body onload="init();">
        <h4 class="page-header">Broker Employee/User List</h4>
		<FORM id="UserList" method="post" runat="server">
			<INPUT onclick="onNew();" type="button" value="Add new"> 
			<input onclick="onImportExternalEmployees();" type="button" value="Import external employees" style="width:140px">
			<input onclick="onImportInternalEmployees();" type="button" value="Import LQB:LOS users" style="width:140px">
			<input onclick="onBatchUpdateCompensationRecords();" type="button" value="Batch update comp records" style="width:150px">
			<input onclick="onBatchUpdateUserStateLicenses();" type="button" value="Batch update user state licenses" style="width:150px">
			<input onclick="onBatchUpdateOCStateLicenses();" type="button" value="Batch update OC state licenses" style="width:150px">
			<INPUT onclick="onClosePopup();" type="button" value="Close">
			<% if(IsPerTransactionBilling){ %>
			<input onclick="onImportEmployeeDMLogins()" type="button" value="Import Employee DocMagic Logins" />
			<%} %>
			<asp:button id="m_updateBtn" runat="server" Text="Update account owner" onclick="OnUpdateClick" style="width:130px"></asp:button>
			<label for="typeFilter">Type </label>:
			<select id="typeFilter" onchange="filterUsers()">
				<option value="B" selected>
					B
				</option>
				<option value="P">
					P
				</option>
				<option value="A">
					All
				</option>
			</select>
			&nbsp; <IMG src="../../images/profile.gif">&nbsp; - Account Owner
			<BR>
			<ml:CommonDataGrid id="m_userGrid" runat="server">
				<Columns>
					<asp:TemplateColumn>
						<ItemTemplate>
							<a href="#" onclick='onEdit("<%#AspxTools.HtmlString(Eval("EmployeeID"))%>"); return false;'>
								edit</a>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="<img src='../../images/profile.gif'>" HeaderStyle-HorizontalAlign="center" ItemStyle-Width="10">
						<ItemTemplate>
                            <%#AspxTools.HtmlControl(GenerateRadioButton((EmployeeDesc)Container.DataItem, Container.ItemIndex))%>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Full Name" SortExpression="FullName">
                      <itemtemplate>  
                        <%# AspxTools.HtmlString(Eval("FullName").ToString())%>
                      </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Login">
                      <itemtemplate>  
                        <%# AspxTools.HtmlString(Eval("LoginNm").ToString())%>
                      </ItemTemplate>
                    </asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Role" SortExpression="Role">
                      <itemtemplate>  
                        <%# AspxTools.HtmlString(Eval("Role").ToString())%>
                      </ItemTemplate>
                    </asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Branch" SortExpression="BranchNm">
                      <itemtemplate>  
                        <%# AspxTools.HtmlString(Eval("BranchNm").ToString())%>
                      </ItemTemplate>
                    </asp:TemplateColumn>
					<asp:BoundColumn DataField="Type" HeaderText="T" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
					<asp:BoundColumn DataField="StatusDescription" HeaderText="Status?" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
					<asp:TemplateColumn ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
                            <%#AspxTools.HtmlControl(GenerateBecomeLink((EmployeeDesc)Container.DataItem))%>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</ml:CommonDataGrid>

		</FORM>
	</body>
</HTML>
