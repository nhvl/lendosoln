﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.CustomCocField;
    using LendersOffice.Security;    

    /// <summary>
    /// Service page for custom coc field list page.
    /// </summary>
    public partial class CustomCocFieldListService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Chooses which service method to perform.
        /// </summary>
        /// <param name="methodName">The method to perform.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Save":
                    this.Save();
                    break;
                case "VerifyFieldId":
                    this.VerifyFieldId();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled method call in CustomCocFieldListService page");
            }
        }

        /// <summary>
        /// Saves the custom coc fields.
        /// </summary>
        private void Save()
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            string fieldsJson = GetString("Fields");
            Guid brokerId = GetGuid("BrokerId");
            var fields = SerializationHelper.JsonNetDeserialize<List<CustomCocField>>(fieldsJson);
            CustomCocFieldListHelper.Save(brokerId, fields);
        }

        /// <summary>
        /// Verifies that the field id input is a valid field.
        /// </summary>
        private void VerifyFieldId()
        {
            string fieldsJson = GetString("Fields");
            var fields = SerializationHelper.JsonNetDeserialize<HashSet<CustomCocField>>(fieldsJson);
            string fieldId = GetString("FieldId");            

            var propInfo = Tools.GetProperty(fieldId, typeof(CPageBase), typeof(CPageHelocBase));
            if (propInfo == null)
            {
                throw new CBaseException($"FieldId {fieldId} is not valid", $"FieldId {fieldId} is not valid");
            }

            if (fields.Any(f => f.FieldId == propInfo.Name))
            {
                throw new CBaseException($"Cannot add duplicate field of {fieldId}", $"Cannot add duplicate field of {fieldId}");
            }

            this.SetResult("FieldId", propInfo.Name);
        }
    }
}