<%@ Page language="c#" Codebehind="EditDefaultRolePermissions.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.EditDefaultRolePermissions" %>
<%@ Register TagPrefix="uc" TagName="DefaultRolePermissionsEdit" Src="../../los/admin/DefaultRolePermissionsEdit.ascx" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Edit Default Role Permissions</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</head>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto; BACKGROUND-COLOR: dimgray">
	<script>
		function _init()
		{
			if( document.getElementById("m_errorMessage") != null )
			{
				alert( document.getElementById("m_errorMessage").value );
			}

			if( window.dialogArguments != null )
			{
				<% if( IsPostBack == false ) { %>

				resize( 470 , 600 );

				<% } %>
			}
		}
	</script>
		<form id="EditDefaultRolePerms" method="post" runat="server" class="expand-w-content">
			<uc:DefaultRolePermissionsEdit id="m_Edit" runat="server"></uc:DefaultRolePermissionsEdit>
			<uc:CModalDlg runat="server" ID="Cmodaldlg1"></uc:CModalDlg>
		</form>
	</body>
</html>
