﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Linq;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Security.Authorization.ConversationLog;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Page that allows changing a conversation log category for a lender.
    /// </summary>
    public partial class ConversationLogCategoryEditor : SecuredAdminPage
    {
        /// <summary>
        /// Gets the passed in broker id.
        /// </summary>
        /// <value>The passed in broker id.</value>
        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("BrokerId");
            }
        }

        /// <summary>
        /// The Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            this.PageID = "ConversationLogCategoryEditor";
            this.PageTitle = "Conversation Log Category Editor";
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("stylesheet.css");

            this.RegisterJsScript("angular-1.4.8.min.js");
            this.RegisterJsGlobalVariables(
                "categoryNameRegexString", 
                string.Format("^({0})$", RegularExpressionString.CommentCategoryName.ToString()));

            var brokerId = BrokerIdentifier.Create(this.BrokerId.ToString()).Value;
            var permissionLevels = ConversationLogPermissionLevel.GetOrderedPermissionLevelsForBrokerIncludingDefault(brokerId);
            var viewOfPermissionLevelsForDd = permissionLevels.Where(l => l.IsActive).Select(l => new { name = l.Name, id = l.Id }).ToList();

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("permissionLevelNamesAndIds", viewOfPermissionLevelsForDd);
        }
    }
}