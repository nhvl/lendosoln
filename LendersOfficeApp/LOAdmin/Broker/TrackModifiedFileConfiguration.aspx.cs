﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class TrackModifiedFileConfiguration : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Gets the required permissions to access this page.
        /// </summary>
        /// <returns>The permissions associated with this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        /// <summary>
        /// Saves the data associated with the AppCode to the AppCode table. Can be used to create a new AppCode.
        /// </summary>
        /// <param name="brokerId">BrokerId of the AppCode.</param>
        /// <param name="appCode">Unique identifier of the AppCode, or null if creating a new AppCode.</param>
        /// <param name="appName">Name for the AppCode.</param>
        /// <param name="contactName">Name to contact regarding this AppCode and its integration.</param>
        /// <param name="contactEmail">Email to contact regarding this AppCode and its integration.</param>
        /// <param name="notes">Notes describing/explaining this AppCode.</param>
        /// <param name="enabled">Boolean value for whether the AppCode is enabled and recording modified loans.</param>
        /// <returns>The unique identifier AppCode value.</returns>
        [System.Web.Services.WebMethod]
        public static Guid SaveAppCode(Guid brokerId, Guid? appCode, string appName, string contactName, string contactEmail, string notes, bool enabled)
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@AppCode", appCode),
                                            new SqlParameter("@AppName", appName),
                                            new SqlParameter("@ContactName", contactName),
                                            new SqlParameter("@ContactEmail", contactEmail),
                                            new SqlParameter("@Notes", notes),
                                            new SqlParameter("@Enabled", enabled)
                                        };
            return (Guid)StoredProcedureHelper.ExecuteScalar(brokerId, "BROKER_WEBSERVICE_APP_CODE_Save", parameters);
        }

        /// <summary>
        /// Generates a new &lt;input type="text" /&gt;.
        /// </summary>
        /// <param name="eval">Maps <paramref name="name"/> to a string value for the input's value.</param>
        /// <param name="name">The class to which the input belongs.</param>
        /// <returns>A new input with class set to <paramref name="name"/> and value given by <paramref name="eval"/>(<paramref name="name"/>).</returns>
        protected static System.Web.UI.HtmlControls.HtmlInputText GenerateTextInput(Func<string, object> eval, string name)
        {
            var textInput = new System.Web.UI.HtmlControls.HtmlInputText();
            textInput.Attributes.Add("class", name);
            textInput.Value = eval(name).ToString();
            return textInput;
        }

        /// <summary>
        /// Generates a new &lt;textarea /&gt;.
        /// </summary>
        /// <param name="eval">Maps <paramref name="name"/> to a string value for the TextArea's value.</param>
        /// <param name="name">The class to which the TextArea belongs.</param>
        /// <returns>A new TextArea with class set to <paramref name="name"/> and value given by <paramref name="eval"/>(<paramref name="name"/>).</returns>
        protected static System.Web.UI.HtmlControls.HtmlTextArea GenerateTextArea(Func<string, object> eval, string name)
        {
            var textArea = new System.Web.UI.HtmlControls.HtmlTextArea();
            textArea.Attributes.Add("class", name);
            textArea.Value = eval(name).ToString();
            return textArea;
        }

        /// <summary>
        /// Generates a new &lt;input type="checkbox" /&gt;.
        /// </summary>
        /// <param name="eval">Maps <paramref name="name"/> to a boolean value for the TextArea's value.</param>
        /// <param name="name">The class to which the input belongs.</param>
        /// <returns>A new input with class set to <paramref name="name"/> and checked if the boolean given by <paramref name="eval"/>(<paramref name="name"/>) is true.</returns>
        protected static System.Web.UI.HtmlControls.HtmlInputCheckBox GenerateCheckboxInput(Func<string, object> eval, string name)
        {
            var checkboxInput = new System.Web.UI.HtmlControls.HtmlInputCheckBox();
            checkboxInput.Attributes.Add("class", name);
            checkboxInput.Checked = (bool)eval(name);
            return checkboxInput;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            Guid brokerId = RequestHelper.GetGuid("BrokerId", Guid.Empty);
            if (brokerId == Guid.Empty)
            {
                this.ErrorMsg.InnerText = "Please save/create broker before configuring modified file tracking.";
                return;
            }

            this.RegisterJsGlobalVariables("BrokerId", brokerId);

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "BROKER_WEBSERVICE_APP_CODE_Fetch", parameters ))
            {
                this.AppCodeTable.DataSource = reader.Select(r =>
                    new
                    {
                        AppCode = r.SafeGuid("AppCode"),
                        AppName = r.SafeString("AppName"),
                        ContactName = r.SafeString("ContactName"),
                        ContactEmail = r.SafeString("ContactEmail"),
                        Notes = r.SafeString("Notes"),
                        Enabled = r.SafeBool("Enabled")
                    });

                this.AppCodeTable.DataBind();
            }
        }
    }
}
