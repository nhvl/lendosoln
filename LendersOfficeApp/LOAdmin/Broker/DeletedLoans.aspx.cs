using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Security;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;
using LendersOffice.ObjLib.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class DeletedLoans : SecuredAdminPage
    {
        /// <returns>return Loan[] or errorMessage:string </returns>
        [WebMethod]
        public static object Search(Guid sBrokerID, int daysSinceDeletion,
            string sOldLNm, string borrowerName, string aBSsn,
            Guid? sLId)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            try
            {
                return Loan.Search(sBrokerID, daysSinceDeletion, borrowerName, aBSsn, sOldLNm, sLId);
            }
            catch (SqlException exc)
            {
                Tools.LogError(exc);
                return exc.Message;
            }
        }

        [WebMethod]
        public static IEnumerable GetBrokers()
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return null; }

            const bool isActive = true;
            return BrokerDbSearch.Search(string.Empty, string.Empty, isActive, string.Empty, Guid.Empty, suiteType: null)
                .Select(b => new { b.BrokerId, b.BrokerNm })
                .ToArray()
                ;
        }

        [WebMethod]
        public static bool CheckValid(string restoreUserFirstName, string restoreUserLastName, string brokerName, Guid brokerId)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return false; }

            var parameters = new[]
            {
                new SqlParameter("@BrokerNm"    , brokerName            ),
                new SqlParameter("@UserFirstNm" , restoreUserFirstName  ),
                new SqlParameter("@UserLastNm"  , restoreUserLastName   )
            };

            // Checks to see if the given user name is recognized as users in the given broker.
            // If they are not, the user still has the choice to ignore the warning and continue with the given user name
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "FindActiveLOUser", parameters))
            {
                return reader.Read() && reader["FullName"].ToString() == restoreUserFirstName + " " + restoreUserLastName;
            }
        }

        [WebMethod]
        public static bool UndeleteLoan(Guid sLId, string restoreUserFirstName, string restoreUserLastName)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) { return false; }

            var isUndoSuccess = UndeleteLoan(sLId);

            //OPM 12066 jk 3/23/09 - Add an additional event to audit history, Loan Restore, that tracks when loans are undeleted
            var audit = new LoanRestoreAuditItem(restoreUserFirstName, restoreUserLastName);
            AuditManager.RecordAudit(sLId, audit);

            return isUndoSuccess;

            //BindDataGrid();
        }
        static bool UndeleteLoan(Guid sLId)
        {
            var parameters = new[]
            {
                new SqlParameter("@sLId", sLId),
                new SqlParameter("@IsValid", true)
            };

            Guid brokerId;
            DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);

            return StoredProcedureHelper.ExecuteNonQuery(brokerId, "DeclareLoanFileValid", 2, parameters) > 0;
        }

        public class Loan
        {
            public Guid          sLId                 { get; set; }
            public string        sOldLNm              { get; set; }
            public DateTime?     DeletedD             { get; set; }
            public string        LoanStatus           { get; set; }
            public string        sPrimBorrowerFullNm  { get; set; }
            public string        aBSsn                { get; set; }
            public string        FullAddr             { get; set; }
            public string        sEmployeeLoanRepName { get; set; }
            public int     ?     sLienPosT            { get; set; }
            public bool    ?     IsTemplate           { get; set; }
            public string        sLoanFileT           { get; set; }

            public Loan()
            {
                // for Serialization
            }

            public Loan(IReadOnlyDictionary<string, object> record)
            {
                sLId                 = (Guid)record["sLId"];
                sOldLNm              = record["sOldLNm"] as string;
                DeletedD             = record["DeletedD"] as DateTime?;
                LoanStatus           = record["LoanStatus"] as string;
                sPrimBorrowerFullNm  = record["sPrimBorrowerFullNm"] as string;
                FullAddr             = record["FullAddr"] as string;
                sEmployeeLoanRepName = record["sEmployeeLoanRepName"] as string;
                sLienPosT            = record["sLienPosT"] as int?;
                IsTemplate           = record["IsTemplate"] as bool?;

                var databaseloanFileType = record["sLoanFileT"] as int?;
                if (databaseloanFileType.HasValue)
                {
                    var loanTypeType = (E_sLoanFileT)databaseloanFileType.Value;
                    sLoanFileT = loanTypeType.ToString();
                }
                else
                {
                    sLoanFileT = string.Empty;
                }
            }

            public static Loan[] Search(Guid brokerId, int daysSinceDeletion,
                string borrowerName, string borrowerSsn, string oldLoanName,
                Guid? loanId)
            {
                var search = new DeletedLoanSearch(brokerId) { DaysSinceDeletion = daysSinceDeletion };
                {
                    if (!string.IsNullOrEmpty(borrowerName)) search.BorrowerName = borrowerName;
                    if (!string.IsNullOrEmpty(borrowerSsn )) search.BorrowerSsn = borrowerSsn;
                    if (!string.IsNullOrEmpty(oldLoanName )) search.OldLoanName = oldLoanName;

                    if (loanId.HasValue) search.LoanID = loanId.Value;
                }

                return search.ExecuteSearch(brokerId);
            }
        }

        class DeletedLoanSearch
        {
            #region Properties
            private Guid   m_brokerId;
            private int    m_daysSinceDeletion = -1;
            private string m_sOldLNm;
            private string m_aBFirstNm;
            private string m_aBLastNm;
            private string m_borrowerName;
            private string m_aBSsn;
            private Guid   m_sLId = Guid.Empty;

            public int DaysSinceDeletion
            {
                set
                {
                    m_daysSinceDeletion = value;
                }
            }

            public string OldLoanName
            {
                set
                {
                    m_sOldLNm = value;
                }
            }

            public string BorrowerName
            {
                set
                {
                    string fullName = value;
                    if (string.IsNullOrEmpty(fullName)) return;

                    var nameParts = fullName.TrimWhitespaceAndBOM().Split( ' ' );

                    if( nameParts.Length >= 2 )
                    {
                        m_aBFirstNm = nameParts[ 0 ];
                        m_aBLastNm = nameParts[ 1 ];
                    }
                    else if( nameParts.Length == 1 )
                    {
                        m_borrowerName = nameParts[ 0 ];
                    }
                }
            }

            public string BorrowerSsn
            {
                set
                {
                    m_aBSsn = value;
                }
            }
            public Guid LoanID
            {
                set
                {
                    m_sLId = value;
                }
            }

            public Guid BrokerId
            {
                get { return m_brokerId; }
            }
            #endregion

            public DeletedLoanSearch( Guid brokerId )
            {
                m_brokerId = brokerId;
            }

            public Loan[] ExecuteSearch(Guid brokerId)
            {
                return this.GetSearchResult(brokerId).ToArray();
            }

            private IEnumerable<Loan> GetSearchResult(Guid brokerId)
            {
                // 08/16/06 mf - For OPM 7019, we dynamically create the query, since the
                // stored procedure times out too often.

                var ps = new List<SqlParameter>(9) { new SqlParameter("sBrokerID", m_brokerId.ToString()) };
                #region Conditions
                var conditions = new List<string>(10)
                {
                    "(cache.IsValid = 0)",
                    "(cache.sBrokerID = @sBrokerID)"
                };

                if (m_daysSinceDeletion != -1 && m_daysSinceDeletion != -2 && m_sLId == Guid.Empty)
                {
                    conditions.Add(@"( ABS(DATEDIFF(DAY,a.DeletedD,GETDATE())) <= @daysSinceDeletetion )");
                    ps.Add(new SqlParameter("daysSinceDeletetion", m_daysSinceDeletion));
                }

                if (m_daysSinceDeletion == -2 && m_sLId == Guid.Empty)
                {
                    conditions.Add(@"( DATEDIFF(DAY,a.DeletedD,GETDATE()) > 60 )");
                }

                if (!string.IsNullOrEmpty(m_aBFirstNm))
                {
                    conditions.Add(@"( cache.aBFirstNm LIKE @aBFirstNm + '%' )");
                    ps.Add(new SqlParameter("aBFirstNm", m_aBFirstNm));
                }

                if (!string.IsNullOrEmpty(m_aBLastNm))
                {
                    conditions.Add(@"( cache.aBLastNm LIKE @aBLastNm + '%' )");
                    ps.Add(new SqlParameter("aBLastNm", m_aBLastNm));
                }

                if (!string.IsNullOrEmpty(m_borrowerName))
                {
                    conditions.Add(@"( cache.aBFirstNm LIKE @borrowerName + '%' OR cache.aBLastNm LIKE @borrowerName + '%' )");
                    ps.Add(new SqlParameter("borrowerName", m_borrowerName));
                }

                if (!string.IsNullOrEmpty(m_sOldLNm))
                {
                    conditions.Add(@"( e.sOldLNm LIKE @sOldLNm + '%' )");
                    ps.Add(new SqlParameter("sOldLNm", m_sOldLNm));
                }

                if (m_sLId != Guid.Empty)
                {
                    conditions.Add("( a.sLId = @sLId )");
                    ps.Add(new SqlParameter("sLId", m_sLId.ToString()));
                }
                #endregion

                var query = string.Format(
                    @"SELECT TOP 100
                        a.sLId
                    , e.sOldLNm
                    , a.DeletedD
                    , cache.sPrimBorrowerFullNm
                    , dbo.GetLoanStatusName(cache.sStatusT) AS LoanStatus
                    , cache.aBAddr + ', ' + cache.aBCity + ', ' + cache.aBState + ' ' + cache.aBZip AS FullAddr
                    , cache.sLienPosT
                    , cache.IsTemplate
                    , cache.sLoanFileT
                    , cache.sEmployeeLoanRepName
                    , cache4.sEncryptionKey
                    , cache4.aBSsnEncrypted
                    FROM     Loan_File_A     a
                        JOIN Loan_File_E     e                  ON a.sLId = e.sLId
                        JOIN Loan_File_Cache cache with(nolock) ON a.sLId = cache.sLId
                        JOIN Loan_File_Cache_4 cache4 with(nolock) ON cache4.sLId = cache.sLId
                    WHERE {0}", string.Join(" AND ", conditions));

                var sqlQuery = SQLQueryString.Create(query);

                var records = new List<Dictionary<string, object>>();
                using (var sqlConnection = DbAccessUtils.GetConnection(brokerId))
                using (var reader = SqlServerHelper.Select(sqlConnection, null, sqlQuery.Value, ps, TimeoutInSeconds.Thirty))
                {
                    while (reader.Read())
                    {
                        records.Add(reader.ToDictionary());
                    }
                }

                return FilterRecordsWithoutMatchingSsn(records);
            }

            private IEnumerable<Loan> FilterRecordsWithoutMatchingSsn(List<Dictionary<string, object>> records)
            {
                if (records.Count == 0)
                {
                    return Enumerable.Empty<Loan>();
                }

                var loans = new List<Loan>(records.Count);

                foreach (var record in records)
                {
                    string aBSsn = null;

                    var borrowerSsnEncrypted = record["aBSsnEncrypted"];
                    if (borrowerSsnEncrypted != DBNull.Value)
                    {
                        var keyId = (Guid)record["sEncryptionKey"];
                        var encryptionKey = EncryptionKeyIdentifier.Create(keyId);

                        aBSsn = LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKey.Value, (byte[])borrowerSsnEncrypted);
                    }

                    if (string.IsNullOrEmpty(this.m_aBSsn) ||
                        !string.IsNullOrEmpty(aBSsn) && aBSsn.IndexOf(this.m_aBSsn, StringComparison.Ordinal) != -1)
                    {
                        var loan = new Loan(record);
                        loan.aBSsn = aBSsn;
                        loans.Add(loan);
                    }
                }

                return loans;
            }
        }

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.Broker.DeletedLoans.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            RegisterResources();
        }

        static readonly E_InternalUserPermissions[] RequiredPermissions = { E_InternalUserPermissions.ViewBrokers };
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return RequiredPermissions;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }



}
