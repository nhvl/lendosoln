using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using DataAccess;
using EDocs;
using LendersOffice.BrokerAdmin.Importers;
using LendersOffice.Common.TextImport;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class ImportBrokerAttributes : LendersOffice.Admin.SecuredAdminPage
    {
        protected LendersOffice.Admin.BrokerDB m_brokerDB;

        protected class ImportSkin
        {
            public AbstractImporter Importer;
            public string General;
            public string Plural;
            public string Title;
            public string Short;
        }

        protected static Dictionary<string, ImportSkin> Skins = new Dictionary<string, ImportSkin>()
        {
            {"SO", new ImportSkin()
                { 
                    Importer = new StackOrderImporter(),
                    General = "stack order", 
                    Plural = "stack order", 
                    Title = "Stack Order", 
                    Short = "order"
                }
            },
            {"IE", new ImportSkin()
                { 
                    Importer = new InternalEmployeeImporter(),
                    General = "internal employee", 
                    Plural = "internal employees", 
                    Title = "Internal Employees", 
                    Short = "employee"
                }
            },

            {"OC", new ImportSkin()
                { 
                    Importer = new OriginationCompanyImporter(),
                    General = "originating company",
                    Plural = "originating companies",
                    Title = "Originating Companies",
                    Short = "company"
                }
            },

            {"CC", new ImportSkin()
                {
                    Importer = new ConditionChoiceImporter(),
                    General = "condition choice",
                    Plural = "condition choices",
                    Title = "Condition Choices",
                    Short = "condition"
                }
            },

            {"LT", new ImportSkin()
                {
                    Importer = new LoanTaskImporter(),
                    General = "loan task",
                    Plural = "loan tasks",
                    Title = "Loan Tasks",
                    Short = "task"
                }
            },

            {"LS", new ImportSkin()
                {
                    Importer = new LicensesImporter(),
                    General = "license",
                    Plural = "licenses",
                    Title = "Licenses",
                    Short = "license"
                }
            },

            {"ST", new ImportSkin()
                {
                    Importer = new ShippingTemplateImporter(),
                    General = "shipping template",
                    Plural = "shipping templates",
                    Title = "Shipping Templates",
                    Short = "templates"
                }
            },
        };

        protected ImportSkin CurrentSettings { get; private set; }

        private string ErrorMessage
        {
            set { ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + "."); }
        }

        private string FeedBack
        {
            set { ClientScript.RegisterHiddenField("m_feedBack", value.TrimEnd('.') + "."); }
        }

        /// <summary>
        /// Return this page's required permissions set.  This
        /// test occurs with each page initialization.
        /// </summary>
        /// <returns>
        /// Array of required permissions.
        /// </returns>

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.EditBroker
			};
        }

        /// <summary>
        /// Initialize this page.
        /// </summary>

        protected void PageLoad(object sender, System.EventArgs a)
        {
            // Initialize this page.
            if (!string.IsNullOrEmpty(Request["type"]) && Skins.ContainsKey(Request["type"]))
            {
                CurrentSettings = Skins[Request["type"]];
            }
            else
            {
                throw new ArgumentException("Invalid import type was provided");
            }
            try
            {
                // Setup type selection and load cached entries from viewstate.
                if (Request["Type"] == "SO")
                    exportBtn.Visible = true;
                if (IsPostBack == false)
                {
                    m_ImportInput.Text = CurrentSettings.Importer.HeaderText;
                    m_Upload.ToolTip = "Loads text into box below";
                }
            }

            catch (Exception e)
            {
                Tools.LogError(ErrorMessage = "Failed to load web form: " + e.Message + ".", e);
                throw;
            }
        }

        /// <summary>
        /// Render this page.
        /// </summary>

        protected void PagePreRender(object sender, System.EventArgs a)
        {
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.PreRender += new System.EventHandler(this.PagePreRender);

        }
        #endregion

        /// <summary>
        /// Handle UI click.
        /// </summary>

        protected void UploadClick(object sender, System.EventArgs a)
        {
            // Bring in the specified text file and display it in our
            // editor.
            try
            {
                string tempFilename = Path.GetTempFileName() + Path.GetExtension(m_File.PostedFile.FileName);
                m_File.PostedFile.SaveAs(tempFilename);
                string uploaded = DataParsing.FileToCSV(tempFilename, false, CurrentSettings.Importer.SheetName);
                m_ImportInput.Text = CurrentSettings.Importer.HeaderText + Environment.NewLine + uploaded;
            }
            catch (System.InvalidOperationException)
            {
                string error = "Don't use Excel spreadsheets for importing; make sure you save as csv first.";
                ErrorMessage = error;
            }
            catch (OleDbException)
            {
                string error = "Don't use Excel spreadsheets for importing; make sure you save as csv first.";
                ErrorMessage = error;
            }
            catch (ArgumentException e)
            {
                ErrorMessage = e.Message;
            }
            catch (Exception e)
            {
                Tools.LogError(ErrorMessage = "Failed to upload: " + e.Message + ".", e);
                throw;
            }
        }

        /// <summary>
        /// Show the errors in errorList
        /// </summary>
        /// <param name="errorList">A data table containing a single column of errors</param>
        private void ShowErrors(DataTable errorList)
        {
            m_Errors.DataSource = errorList;
            m_Errors.DataBind();
            m_ErrorList.Visible = true;
        }

        /// <summary>
        /// Import was clicked, so parse the user-provided csv list in the input box
        /// </summary>
        protected void ImportClick(object sender, System.EventArgs args)
        {
            string csv;
            Guid BrokerId = new Guid(Request["brokerid"]);
            using (StringReader sr = new StringReader(m_ImportInput.Text))
            {
                string firstLine = sr.ReadLine();
                //Did they include our header row?
                if (firstLine != null && firstLine.TrimWhitespaceAndBOM().Equals(CurrentSettings.Importer.HeaderText, StringComparison.InvariantCultureIgnoreCase))
                {
                    //Yes, so just parse it
                    csv = m_ImportInput.Text;
                }
                else
                {
                    //They messed with it, so prepend a new one
                    csv = CurrentSettings.Importer.HeaderText + "\n" + m_ImportInput.Text;
                }
            }

            string displayResult;
            DataTable ErrorList;
            CurrentSettings.Importer.Import(csv, BrokerId, out displayResult, out ErrorList);
            if (ErrorList.Rows.Count > 0)
            {
                ShowErrors(ErrorList);
            }
            m_ImportInput.Text = displayResult;
        }

        //this is only available for the StackOrder
        //export was clicked
        protected void OnExportClick(Object sender, EventArgs e)
        {
            Guid brokerId = new Guid(Request["brokerid"]);
            m_brokerDB = LendersOffice.Admin.BrokerDB.RetrieveById(brokerId);
            
            IEnumerable<DocType> brokerOrderedDocTypes = EDocumentDocType.GetStackOrderedDocTypesByBroker(m_brokerDB);
            String currentStackOrder = m_brokerDB.EdocsStackOrderList;

            var csvBuilder = new StringBuilder();
            csvBuilder.Append("DocType Id,Folder,DocType Name");
            csvBuilder.AppendLine();
            foreach (DocType docType in brokerOrderedDocTypes)
            {
                csvBuilder.Append("\"" + docType.DocTypeId + "\",\"" + docType.Folder.FolderNm + "\"," + "\"" + docType.DocTypeName + "\"");
                csvBuilder.AppendLine();
            }

            Response.ClearHeaders();
            Response.Filter = null; // Need to remove any compression filtering, otherwise it comes out garbled.
            Response.AppendHeader("Content-Type", "text/plain");
            Response.AppendHeader("Content-Disposition", $"attachment; filename=\"{m_brokerDB.Name} Stack Order.csv\"");
            Response.Output.Write(csvBuilder.ToString());
            Response.Flush();
            Response.End();
        }

    }
}