﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DataAccess;
using LendersOfficeApp.los.admin;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Text;
using LendersOffice.Common;
using LendersOffice.Admin;
using LendersOffice.Common.TextImport;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class BatchUpdateBranchStateLicenses : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditBroker,
                E_InternalUserPermissions.EditEmployee
            };
        }

        private Guid BrokerId
        {
            get
            {
                return new Guid(RequestHelper.GetSafeQueryString("BrokerId"));
            }
        }

        private Dictionary<string, BranchDB> GetBranches()
        {
            // Get all branches for this broker
            Dictionary<string, BranchDB> branchList = new Dictionary<string, BranchDB>();
            foreach (var branch in BranchDB.GetBranchObjects(BrokerId))
            {
                branchList.Add(branch.Name, branch);
            }
            return branchList;
        }

        protected void UploadClick(object sender, System.EventArgs a)
        {
            StringBuilder errorList = new StringBuilder();
            string[] formats = { "MM/dd/yyyy", "MM/d/yyyy", "M/dd/yyyy", "M/d/yyyy" };
            // Check if csv is used
            if (Path.GetExtension(m_File.PostedFile.FileName) != ".csv")
            {
                m_errorData.Text = "Please use a .csv file following the format in the instructions below";
                return;
            }

            int branchNmCol = 0;
            int licenseNumCol = 1;
            int stateCol = 2;
            int expirationDateCol = 3;

            // Grab the file contents
            string tempFilename = TempFileUtils.NewTempFilePath() + Path.GetExtension(m_File.PostedFile.FileName);
            m_File.PostedFile.SaveAs(tempFilename);
            ILookup<int, string> parseErrors;
            DataTable fileContents = DataParsing.FileToDataTable(tempFilename, true, "", out parseErrors);
            if (parseErrors.Any())
            {
                foreach (var error in parseErrors)
                {
                    foreach (string errorMsg in error)
                    {
                        errorList.AppendLine("Error on line " + error.Key + ": " + errorMsg);
                    }
                }

                m_errorData.Text = errorList.ToString();
                return;
            }

            string outputFile = TempFileUtils.NewTempFilePath();
            Dictionary<string, BranchDB> branchList = GetBranches();

            using (StreamWriter writer = new StreamWriter(outputFile, false))
            {
                writer.WriteLine("BranchNm,LicenseNum,State,ExpirationDate,Action,Reason");

                foreach (DataRow row in fileContents.Rows)
                {
                    string branchNm = row[branchNmCol].ToString().TrimWhitespaceAndBOM();
                    string branchNmCSV = DataParsing.sanitizeForCSV(branchNm); // For branch names with commas
                    string licenseNum = row[licenseNumCol].ToString().TrimWhitespaceAndBOM();
                    string state = row[stateCol].ToString().TrimWhitespaceAndBOM().ToUpper();
                    string expirDate = row[expirationDateCol].ToString().TrimWhitespaceAndBOM();

                    // Check for invalid inputs
                    DateTime temp;
                    if (!LicenseInfoList.IsValidState(state) || !DateTime.TryParseExact(expirDate, formats, null, DateTimeStyles.None, out temp) 
                      || string.IsNullOrEmpty(licenseNum))
                    {
                        writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", branchNmCSV, licenseNum, state, expirDate, "Skip", "Field requirements not met"));
                        continue;
                    }

                    if (branchList.ContainsKey(branchNm))
                    {
                        BranchDB curBranch = branchList[branchNm];
                        LicenseInfoList lList = LicenseInfoList.ToObject(curBranch.LicenseXmlContent);

                        if (lList.UpdateExpirationDate(licenseNum, state, expirDate))
                        {
                            // License and state match, just update
                            writer.WriteLine(string.Format("{0},{1},{2},{3},{4}", branchNmCSV, licenseNum, state, expirDate, "Update"));
                            curBranch.LicenseXmlContent = lList.ToString();
                            curBranch.IsDirty = true;
                        }
                        else 
                        {
                            // Add the license since no match found
                            if (lList.IsFull)
                            {
                                writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", branchNmCSV, licenseNum, state, expirDate, "Skip", "License list is full"));
                            }
                            else
                            {
                                try
                                {
                                    LicenseInfo lInfo = new LicenseInfo(state, expirDate, licenseNum);
                                    lList.Add(lInfo);
                                    writer.WriteLine(string.Format("{0},{1},{2},{3},{4}", branchNmCSV, licenseNum, state, expirDate, "Add"));
                                    curBranch.LicenseXmlContent = lList.ToString();
                                    curBranch.IsDirty = true;
                                }
                                catch (CBaseException) // Couldn't add to list
                                {
                                    writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", branchNmCSV, licenseNum, state, expirDate, "Skip", "Unable to add license"));
                                }
                            }
                        }
                    }
                    else
                    {
                        // No branch with this name for the broker
                        writer.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", branchNmCSV, licenseNum, state, expirDate, "Skip", "Branch not found"));
                    }
                }

                writer.WriteLine();
                writer.WriteLine("Save Errors. Please retry these branches.");
                var enumerator = branchList.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    BranchDB branchDb = enumerator.Current.Value;
                    if (branchDb.IsDirty)
                    {
                        try
                        {
                            branchDb.Save();
                        }
                        catch (CBaseException)
                        {
                            writer.WriteLine("Unable to save licenses for branch " + DataParsing.sanitizeForCSV(branchDb.Name));
                        }
                    }  
                }
            }
            RequestHelper.SendFileToClient(Context, outputFile, "text/csv", "branchUpdateResults.csv");
            Response.End();
        }
    }
}
