﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.Cenlar;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// The background service for the Cenlar Credential Edit modal.
    /// </summary>
    public partial class CenlarCredentialEditService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Determines which process should be called.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.SaveCredentials):
                    this.SaveCredentials();
                    break;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Method not implemented {methodName}"));
            }
        }

        /// <summary>
        /// Saves a set of Cenlar credentials to the database.
        /// </summary>
        private void SaveCredentials()
        {
            Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);

            var credentials = new CenlarCredentialSet();
            
            credentials.BrokerId = this.GetGuid("BrokerId");
            credentials.EnvironmentT = (CenlarEnvironmentT)Enum.Parse(typeof(CenlarEnvironmentT), this.GetString("CenlarEnvironment"));

            credentials.UserId = this.GetString("CenlarUserId");
            credentials.CustomerId = this.GetString("CenlarCustomerId");

            var cachedApiKey = this.GetString("CachedApiKey");

            string unparsedCachedApiKeyReset = this.GetString("CachedApiKeyReset");
            var cachedApiKeyReset = string.IsNullOrEmpty(unparsedCachedApiKeyReset)
                ? default(DateTime)
                : DateTime.Parse(this.GetString("CachedApiKeyReset"));

            var newApiKey = this.GetString("CenlarApiKey");
            if (newApiKey.Equals(cachedApiKey))
            {
                credentials.ApiKey = cachedApiKey;
                credentials.LastApiKeyReset = cachedApiKeyReset;
            }
            else
            {
                credentials.ApiKey = newApiKey;
                credentials.LastApiKeyReset = DateTime.Now;
            }

            CenlarCredentialSet.Save(credentials);
        }
    }
}