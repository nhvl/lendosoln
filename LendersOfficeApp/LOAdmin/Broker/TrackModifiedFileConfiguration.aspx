﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrackModifiedFileConfiguration.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.TrackModifiedFileConfiguration" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Track Modified File Configuration</title>
    <style type="text/css">
        #BtnHolder { text-align: center; }
        #ErrorMsg { color: Red; }
        tr.Dirty { background-color: LightSalmon; }
        tr:hover > td { background-color: WhiteSmoke; }
        th, td
        {
            border: 0;
            text-align: center;
            vertical-align: middle;
            border-bottom: solid 1px Gray;
        }
        th { padding: 1em; }
        table { border: 0; margin: 0.25em; }
        
        .AppCode, #AddNew { width: 7.5em; }
        .New.AppCode { display: none; }
        .AppName
        {
            width: 20em;
        }
        .ContactName
        {
            width: 10em;
        }
        .ContactEmail
        {
            width: 15em;
        }
        .Notes
        {
            width: 20em;
            height: 4.5em;
        }
        .Enabled
        {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="BtnHolder">
        <input type="button" id="SaveBtn" value="Save" />
        <input type="button" id="SaveAndCloseBtn" value="Save & Close" />
        <input type="button" id="CloseBtn" value="Close" />
        <span id="ErrorMsg" runat="server"></span>
    </div>
    <asp:GridView ID="AppCodeTable" runat="server" AutoGenerateColumns="false" ShowFooter="true">
        <EmptyDataTemplate>
            <tr>
                <th>App Code</th>
                <th>App Name</th>
                <th>Contact Name</th>
                <th>Contact Email</th>
                <th>Notes</th>
                <th>Enabled</th>
            </tr>
            <tr>
                <td><input type="button" id="AddNew" value="Add new" title="Adds new App Code & saves altered App Code data" /><input type="text" class="New AppCode" /></td>
                <td><input type="text" class="New AppName" /></td>
                <td><input type="text" class="New ContactName" /></td>
                <td><input type="text" class="New ContactEmail" /></td>
                <td><textarea class="New Notes"></textarea></td>
                <td><input type="checkbox" class="New Enabled" /></td>
            </tr>
        </EmptyDataTemplate>
        <Columns>
            <asp:TemplateField HeaderText="App Code">
                <ItemTemplate><%# AspxTools.HtmlControl(GenerateTextInput(Eval, "AppCode")) %></ItemTemplate>
                <FooterTemplate><input type="button" id="AddNew" value="Add new" title="Adds new App Code & saves altered App Code data" /><input type="text" class="New AppCode" /></FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="App Name">
                <ItemTemplate><%# AspxTools.HtmlControl(GenerateTextInput(Eval, "AppName")) %></ItemTemplate>
                <FooterTemplate><input type="text" class="New AppName" /></FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contact Name">
                <ItemTemplate><%# AspxTools.HtmlControl(GenerateTextInput(Eval, "ContactName")) %></ItemTemplate>
                <FooterTemplate><input type="text" class="New ContactName" /></FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Contact Email">
                <ItemTemplate><%# AspxTools.HtmlControl(GenerateTextInput(Eval, "ContactEmail")) %></ItemTemplate>
                <FooterTemplate><input type="text" class="New ContactEmail" /></FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Notes">
                <ItemTemplate><%# AspxTools.HtmlControl(GenerateTextArea(Eval, "Notes")) %></ItemTemplate>
                <FooterTemplate><textarea class="New Notes"></textarea></FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Enabled">
                <ItemTemplate><%# AspxTools.HtmlControl(GenerateCheckboxInput(Eval, "Enabled")) %></ItemTemplate>
                <FooterTemplate><input type="checkbox" class="New Enabled" /></FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    </form>
</body>
<script type="text/javascript">
var modifiedItemsToSave = []; //Holds the most recent copy of each App Code record

function AddItemToBeSaved(el, isNew) {
    var tr = $j(el).closest("tr");
    var appCode = null;
    if (!isNew) {
        appCode = tr.find(".AppCode").val();
        tr.addClass("Dirty");
    }
    RemoveAppCodeFromArray(modifiedItemsToSave, appCode);
    modifiedItemsToSave.push(new AppCodeRecord(appCode, tr));
}

// represents a row in the table
function AppCodeRecord(appCode, tr) {
    this.brokerId = ML.BrokerId;
    this.appCode = appCode;
    this.appName = tr.find(".AppName").val();
    this.contactName = tr.find(".ContactName").val();
    this.contactEmail = tr.find(".ContactEmail").val();
    this.notes = tr.find(".Notes").text();
    this.enabled = tr.find(".Enabled").is(":checked");
}
$j('#SaveBtn').click(Save);
$j('#SaveAndCloseBtn').click(function() { Save(); CloseWindow(); });
$j('#CloseBtn').click(CloseWindow);

$j('#AddNew').click(function() { AddItemToBeSaved(this, true); AppendNewlySavedRowToTable(Save()); });
$j('.AppCode').prop('readonly', true);
$j('.AppName, .ContactName, .ContactEmail, .Notes, .Enabled').not(".New").change(function() { AddItemToBeSaved(this); });

function CloseWindow() {
    var names = ToAppNameString(modifiedItemsToSave);
    var newAppName = $j('.New.AppName').val();
    if (newAppName) names += (names.length > 0 ? ', ' : '') + newAppName;
    if (names.length === 0 || confirm("The following app codes are waiting to be saved: " + names + "\nDo you wish to close without saving?"))
        window.close();
}

function ToAppNameString(array) {
    var names = '';
    for (var i = 0; i < modifiedItemsToSave.length; ++i) {
        if (names.length > 0) names += ', ';
        names += modifiedItemsToSave[i].appName;
    }
    return names;
}

function RemoveAppCodeFromArray(array, appCode) {    
    for (var i = 0; i < array.length; ++i) {
        if (appCode === array[i].appCode)
            array.splice(i, 1);
    }
}

function AppendNewlySavedRowToTable(appCode) {
    if (!IsValidGuid(appCode)) return;
    
    var currentRow = $j('#AddNew').closest('tr');
    // Copy the current row above the new row, removing the button and inputting the new appCode
    currentRow.clone()
        .find('input[type="button"]').remove().end()
        .find('.AppCode').val(appCode).end()
        .find('input, textarea').removeClass('New').end()
        .find('.AppName, .ContactName, .ContactEmail, .Notes, .Enabled').change(function() { AddItemToBeSaved(this); }).end()
    .insertBefore(currentRow);
    // Clear out the add new row
    currentRow.find('input:not([type="button"])').val('').prop('checked', false).end().find('textarea').text('').end();
}

function IsValidGuid(uniqueidentifier) {
    var regex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i;
    return null != regex.exec(uniqueidentifier);
}

function UpdateErrorMessage(message) {
    $j('#ErrorMsg').val(message);
    if (message != null && message.length > 0) alert(message);
}

function Save() {
    UpdateErrorMessage();
    var newAppCode = null;
    var failedItems = [];
    while (0 < modifiedItemsToSave.length) {
        var item = modifiedItemsToSave.shift();
        callWebMethodAsync({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "TrackModifiedFileConfiguration.aspx/SaveAppCode",
            data: JSON.stringify(item),
            dataType: 'json',
            async: false,
            success: function(response) {
                if (!IsValidGuid(response.d)) {
                    failedItems.push(item);
                } else if (item.appCode === null) {
                    newAppCode = response.d;
                } else {
                    $j('#AppCodeTable tr')
                        .filter(function() { return $j(this).find('.AppCode').val() === response.d; })
                        .removeClass('Dirty');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(JSON.parse(jqXHR.responseText).Message);
                failedItems.push(item);
            }
        });
    }
    if (failedItems.length === 0) UpdateParentWindowCheckbox();
    var failedAppNames = ToAppNameString(failedItems);
    UpdateErrorMessage(failedAppNames);
    
    modifiedItemsToSave = failedItems;
    return newAppCode;
}

function UpdateParentWindowCheckbox() {
    if (modifiedItemsToSave.length > 0) throw "Cannot update parent window while some values are unsaved";
    var existsEnableAppCode = $j('.Enabled:checked').size() > 0;
    window.opener.$j('#m_isTrackModifiedFile').prop('checked', existsEnableAppCode);
}
</script>
</html>