﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="EditUser.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.EditUser" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Edit User</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:CheckBox runat="server" ID="IsUsePml2AsQuickPricer" Text="Use QP 2.0?" />
    <ml:EncodedLiteral runat="server" ID="m_Pml2AsQuickPricerDisabledReason"></ml:EncodedLiteral>
    </div>
    </form>
    <script type="text/javascript">
        $j(document).ready(function() {
            $j('input').on('change', function() {
                var $this = $j(this);
                var inputType = $this.attr('type');
                var data = 
                {
                    id: $this.attr('id'),
                    userID: ML.UserId,
                    brokerId: ML.BrokerId,
                    userType: ML.UserType,
                    inputType: inputType
                };
                switch (inputType) {
                    case 'checkbox':
                        data.jsonValue = $this.is(':checked');
                        break;
                    default:
                        alert('unhandled input type');
                        return;
                }
                var jsonData = JSON.stringify(data);
                callWebMethodAsync({
                    type: "POST",
                    url: "EditUser.aspx/UpdateValue",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(data) {
                    },
                    error: function(e) {
                        alert('Error : ' + JSON.stringify(e));
                    }
                });
            });
        });
    </script>
</body>
</html>
