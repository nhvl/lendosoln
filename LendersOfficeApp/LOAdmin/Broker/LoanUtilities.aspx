<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../common/header.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="LoanUtilities.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.LoanUtilities" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
	<head runat="server">
		<title>Loan Utilities</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR"/>
		<meta content="C#" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet"/>
        <style type="text/css">
            .operation {
                border-bottom: 1px solid black;
                padding-left: 10px;
                padding-top: 5px;
            }
            #ResultFromLookupLoanIDFromCustomerCodeAndLoanNumber {
                display: none;
            }
        </style>
	</head>
	<body onload="onInit();" MS_POSITIONING="FlowLayout">
        <script type="text/javascript">
            function _init()
            {
                var $numberOfLoansRadioOptions = $j('input[name="numberOfLoans"]');
                $numberOfLoansRadioOptions.change(function(e){
                    var displaySingleLoan = $numberOfLoansRadioOptions.filter(':checked').val() === 'SingleLoan';
                    $j('.SingleLoan').toggle(displaySingleLoan);
                    $j('.MultipleLoans').toggle(!displaySingleLoan);
                });
                $numberOfLoansRadioOptions.trigger('change');


                $j('#UpdateCacheForMultipleLoans').click(function(e) {
                    var $button = $j(this);
                    $button.prop('disabled', true);
                    $j('#MessageFromUpdateCacheForMultipleLoans').text('Working...');
                    var loanList = $j('#sLIdsForUpdateCacheForMultipleLoans').val().split(/\r\n|\r|\n/);
                    UpdateCacheForMultipleLoans(loanList, function(result){
                        $button.prop('disabled', false);
                        $j('#MessageFromUpdateCacheForMultipleLoans').text(result.Message);
                    });
                });
                $j('#LookupLoanIDFromCustomerCodeAndLoanNumber').click(function(e){
                    var $button = $j(this);
                    $button.prop('disabled', true);
                    $j('#LoanIDsFromLookupLoanIDFromCustomerCodeAndLoanNumber').val('');
                    $j('#MessageFromLookupLoanIDFromCustomerCodeAndLoanNumber').text('Working...');
                    var customerCode = $j('#CustomerCodeForLookupLoanIDFromCustomerCodeAndLoanNumber').val();
                    var loanNumberList = $j('#LoanNumbersForLookupLoanIDFromCustomerCodeAndLoanNumber').val().split(/\r\n|\r|\n/);
                    LookupLoanIDFromCustomerCodeAndLoanNumber(customerCode, loanNumberList, function(result){
                        $button.prop('disabled', false);
                        if (result.LoanIDs.length > 0) {
                            $j('#ResultFromLookupLoanIDFromCustomerCodeAndLoanNumber').show();
                        }
                        $j('#LoanIDsFromLookupLoanIDFromCustomerCodeAndLoanNumber').val(result.LoanIDs.join('\r\n'));
                        $j('#MessageFromLookupLoanIDFromCustomerCodeAndLoanNumber').text(result.Message);
                    });
                });
            }

            function UpdateCacheForMultipleLoans(loanList, onFinish)
            {
                var args = {
                    LoanIDs: JSON.stringify(loanList)
                };

                var handler = function(rawResult) {
                    if (rawResult.error)
                    {
                        onFinish({ Message: rawResult.UserMessage });
                    }
                    else
                    {
                        onFinish(JSON.parse(rawResult.value.Result));
                    }
                };

                gService.main.callAsync('UpdateCacheForMultipleLoans', args, true, null, true, true, handler, handler);
            }
            function LookupLoanIDFromCustomerCodeAndLoanNumber(customerCode, loanNumberList, onFinish)
            {
                var args = {
                    CustomerCode: customerCode,
                    LoanNumbers: JSON.stringify(loanNumberList)
                };

                var handler = function(rawResult) {
                    if (rawResult.error)
                    {
                        onFinish({ Message: rawResult.UserMessage, LoanIDs: [] });
                    }
                    else
                    {
                        onFinish(JSON.parse(rawResult.value.Result));
                    }
                };

                gService.main.callAsync('LookupLoanIDFromCustomerCodeAndLoanNumber', args, true, null, true, true, handler, handler);
            }


			function onInit()
			{

				try
				{
					// Looks to see if there is an errorMessage to be displayed

					if(document.getElementById("m_ErrorMessage") != null)
					{
						alert( "Error: " + document.getElementById("m_ErrorMessage").value);
					}

					document.getElementById("m_LoanId").focus()

				}
				catch( e )
				{
					window.status = e.message;
				}

				$j("UpdateInitialAprMessage").hide();
				$j("UpdateCacheMsg").hide();
				$j("LoanLinkageMsg").hide();

				searchRadio_clicked()
			}


			function btnUpdateInitialApr_clicked()
            {
			    var args = {
			        sLId:  <%= AspxTools.JsString (LoanId) %>,
			        sInitApr: <%= AspxTools.JsGetElementById (sInitAPR) %>.value
			    };

		        var result = gService.main.call("UpdateInitialApr", args);

		        var msgLbl = $j('#UpdateInitialAprMessage');
		        msgLbl.hide();
			    if (result.value.error)
			    {
			        msgLbl.text('Failed to updated loan.');
			        msgLbl.css('color', 'red');
			    }
			    else
			    {
			        msgLbl.text('Successfully updated loan!');
			        msgLbl.css('color', 'green');
			    }

			    msgLbl.show();
		    }

			function btnUpdateCache_clicked()
			{
			    var args = {
			        sLId:  <%= AspxTools.JsString (LoanId) %>
                };

			    var result = gService.main.call("UpdateLoanFileCache", args);

			    var msgLbl = $j('#UpdateCacheMsg');
                msgLbl.hide();
                if (result.value.error)
                {
                    msgLbl.text('Update failed.');
                    msgLbl.css('color', 'red');
                }
                else
                {
                    msgLbl.text('Successfully updated cache!');
                    msgLbl.css('color', 'green');
                }

                msgLbl.show();
			}

		    function btnLinkLoans_clicked()
		    {
		        var args = {
		            sLId:  <%= AspxTools.JsString (LoanId) %>,
		            LoanIdToLink: <%= AspxTools.JsGetElementById (LoanIdToLink) %>.value,
		            ScrapExistingLinkagesField: $j('#ScrapExistingLinkagesField').prop('checked')
			    };

		        var result = gService.main.call("LinkLoans", args);

		        var msgLbl = $j('#LoanLinkageMsg');
                msgLbl.hide();
                if (result.value.errorMessage)
                {
                    msgLbl.text('Update failed: ' + result.value.errorMessage);
                    msgLbl.css('color', 'red');
                }
                else
                {
                    msgLbl.text('Successfully linked the loans!');
                    msgLbl.css('color', 'green');
                }

                msgLbl.show();
		    }

            function searchRadio_clicked()
            {
                var val = $j('input[name="searchRadio"]:checked').val();

                $j('#tr_sLId').toggle(val == 0);
                $j('#tr_sLNm_CC').toggle(val == 1);
                $j('#tr_sLNm_BId').toggle(val == 2);
                $j('#tr_prop_app').toggle(val == 3);
            }

			// All three of these are caught by the custom handler "OnGridSort" in the codebehind
			function becomeUser(brokerId, userId, loginName)
			{
				__doPostBack( <%= AspxTools.JsString(m_Grid.ClientID) %> , 'becomeUser:' + brokerId + ',' +userId+ ',' +loginName);
			}

			function becomePMLUser(userId, lenderPMLSiteId)
			{
				__doPostBack( <%= AspxTools.JsString(m_Grid.ClientID) %> , 'becomePMLUser:' +userId+ ',' +lenderPMLSiteId);
			}

			function becomeDualUser(userId, loginName)
			{
				__doPostBack( <%= AspxTools.JsString(m_Grid.ClientID) %> , 'becomeDualUser:' +userId+ ',' +loginName);
			}
		</script>
		<form id="LoanUtilities" method="post" runat="server">
			<uc:header id="Header1" runat="server"></uc:header><uc:headernav id="HeaderNav1" runat="server">
				<MenuItem URL="~/LOAdmin/main.aspx" Label="Main" />
				<MenuItem URL="~/LOAdmin/Broker/LoanUtilities.aspx" Label="Loan Utilities" />
			</uc:headernav>
			<table style="WIDTH: 8in" cellSpacing="0" cellPadding="0" border="0">
                <tr>
                    <td>
                        <label><input type="radio" name="numberOfLoans" value="SingleLoan" checked="checked" /> Single Loan</label>
                        <label><input type="radio" name="numberOfLoans" value="MultipleLoans"/> Multiple Loans</label>
                    </td>
                </tr>
                <tr class="SingleLoan">
					<td class="FormTable"><br/>
						<table style="WIDTH: 8in" cellSpacing="0" cellPadding="0" border="0">
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="3" style="white-space: nowrap">
                                    <label class="FieldLabel">Search By:</label>
                                    <asp:RadioButtonList ID="searchRadio" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" onclick="searchRadio_clicked();" AutoPostBack="false" >
                                        <asp:ListItem Text="Loan ID" Value="0" Selected />
                                        <asp:ListItem Text="Loan Number & Customer Code" Value="1" />
                                        <asp:ListItem Text="Loan Number & Broker ID" Value="2" />
                                        <asp:ListItem Text="Loan Details & Broker" Value="3" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr><td colspan="4">&nbsp;</td></tr>
							<tr id="tr_sLId">
								<td><asp:button ValidationGroup="LoanSearchValidationGroup" id="m_search" CommandName="sLID" OnCommand="SearchClick" Runat="server" Text="Search"></asp:button></td>
                                <td>&nbsp;</td>
								<td class="FieldLabel" style="white-space: nowrap">Loan ID:
									<asp:textbox id="m_LoanId" Runat="server" columns="37" MaxLength="36"></asp:textbox>
                                    <asp:regularexpressionvalidator ValidationGroup="LoanSearchValidationGroup" id="guidRegex" runat="server" ControlToValidate="m_LoanId" Display="Dynamic" ErrorMessage="* Incorrect Loan ID format" ValidationExpression="^([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}$"></asp:regularexpressionvalidator>
                                    <asp:requiredfieldvalidator ValidationGroup="LoanSearchValidationGroup" id="loanIdReq" Runat="server" ControlToValidate="m_LoanId" Display="Dynamic" ErrorMessage="* Must provide Loan ID"></asp:requiredfieldvalidator>
								</td>
                                <td>&nbsp;</td>
							</tr>

                            <tr id="tr_sLNm_CC">
								<td><asp:button ValidationGroup="LoanSearchValidationGroup2" id="m_search_sLNm_CC" OnCommand="SearchClick" CommandName="sLNm_CC" Runat="server" Text="Search"></asp:button></td>
                                <td></td>
								<td class="FieldLabel" style="white-space: nowrap">Loan Number:
									<asp:textbox id="sLNm" Runat="server" columns="37" MaxLength="36"></asp:textbox>
                                    <asp:requiredfieldvalidator ValidationGroup="LoanSearchValidationGroup2" id="sLNmReq" Runat="server" ControlToValidate="sLNm" Display="Dynamic" ErrorMessage="* Must provide Loan Number"></asp:requiredfieldvalidator>
								</td>
                                <td class="FieldLabel" style="white-space: nowrap">Customer Code:
									<asp:textbox id="CustomerCode" Runat="server" columns="37" MaxLength="36"></asp:textbox>
                                    <asp:requiredfieldvalidator ValidationGroup="LoanSearchValidationGroup2" id="CustomerCodeReq" Runat="server" ControlToValidate="CustomerCode" Display="Dynamic" ErrorMessage="* Must provide Customer Code"></asp:requiredfieldvalidator>
								</td>
							</tr>

                            <tr id="tr_sLNm_BId">
								<td><asp:button ValidationGroup="LoanSearchValidationGroup3" id="m_search_sLNm_BId" CommandName="sLNm_BId" OnCommand="SearchClick" Runat="server" Text="Search"></asp:button></td>
                                <td></td>
                                <td class="FieldLabel" style="white-space: nowrap">Loan Number:
									<asp:textbox id="sLNm2" Runat="server" columns="37" MaxLength="36"></asp:textbox>
                                    <asp:requiredfieldvalidator ValidationGroup="LoanSearchValidationGroup3" id="sLNm2Req" Runat="server" ControlToValidate="sLNm2" Display="Dynamic" ErrorMessage="* Must provide Loan Number"></asp:requiredfieldvalidator>
								</td>
								<td class="FieldLabel" style="white-space: nowrap">Broker ID:
									<asp:textbox id="m_BrokerId" Runat="server" columns="37" MaxLength="36"></asp:textbox>
                                    <asp:regularexpressionvalidator ValidationGroup="LoanSearchValidationGroup3" id="BrokerIdRegEx" runat="server" ControlToValidate="m_BrokerId" Display="Dynamic" ErrorMessage="* Incorrect Broker ID format" ValidationExpression="^([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}$"></asp:regularexpressionvalidator>
                                    <asp:requiredfieldvalidator ValidationGroup="LoanSearchValidationGroup3" id="BrokerIdReq" Runat="server" ControlToValidate="m_BrokerId" Display="Dynamic" ErrorMessage="* Must provide Broker ID"></asp:requiredfieldvalidator>
								</td>
							</tr>

                            <tr id="tr_prop_app">
								<td><asp:button  CausesValidation="false" id="Button1" CommandName="propSearch" OnCommand="SearchClick" Runat="server" Text="Search"></asp:button></td>
                                <td colspan="3">
                                    <span style="font-weight: bolder"> Broker </span>
                             		<asp:textbox id="propBroker" Runat="server" columns="37" MaxLength="36"></asp:textbox>
                                    <br/>
                                    <span style="font-weight: bolder"> Property Info:</span> <ml:EncodedLabel Text="Address" AssociatedControlID="PropAddress" runat="server"> Address:</ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="PropAddress"></asp:TextBox>
                                    <ml:EncodedLabel Text="City" runat="server" AssociatedControlID="PropCity"></ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="PropCity"></asp:TextBox>
                                                  <ml:EncodedLabel Text="State" runat="server" AssociatedControlID="PropState"></ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="PropState"></asp:TextBox>
                                                             <ml:EncodedLabel Text="Zip" runat="server" AssociatedControlID="PropZip"></ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="PropZip"></asp:TextBox>
                                    <br />
                                     <span style="font-weight: bolder"> Applicant Info:</span>
                                      <ml:EncodedLabel Text="First Name" runat="server" AssociatedControlID="BorrFirstName"></ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="BorrFirstName"></asp:TextBox>
                                    <ml:EncodedLabel Text="Last Name" runat="server" AssociatedControlID="BorrLastName"></ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="BorrLastName"></asp:TextBox>
                                             <ml:EncodedLabel Text="SSN" runat="server" AssociatedControlID="BorrSSN"></ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="BorrSSN"></asp:TextBox>

                                </td>
                            </tr>
						</table>
						<hr />
					</td>
				</tr>

                <asp:panel id="m_loanInfo" Runat="server">
				<tr class="SingleLoan">
				    <td>
					    <table id="m_loanInfoTable" align="left" border="0">
							<TR>
								<TD vAlign="bottom">
									<DIV>
										<ml:EncodedLabel id="m_titleLoanNumber" Runat="server">Loan Number: </ml:EncodedLabel></DIV>
								</TD>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLiteral id="m_LoanNumber" Runat="server"></ml:EncodedLiteral></DIV>
								</TD>
							</TR>
							<TR>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLabel id="m_titleBrokerName" Runat="server">Company Name: </ml:EncodedLabel></DIV>
								</TD>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLiteral id="m_BrokerName" Runat="server"></ml:EncodedLiteral></DIV>
								</TD>
							</TR>
							<TR>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLabel id="m_titleCustomerCode" Runat="server">Customer Code: </ml:EncodedLabel></DIV>
								</TD>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLiteral id="m_CustomerCode" Runat="server"></ml:EncodedLiteral></DIV>
								</TD>
							</TR>
							<TR>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLabel id="m_titleBorrower" Runat="server">Borrower Name: </ml:EncodedLabel></DIV>
								</TD>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLiteral id="m_BorrowerName" Runat="server"></ml:EncodedLiteral></DIV>
								</TD>
							</TR>
							<TR>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLabel id="m_titleSubjectPropertyAddress" Runat="server">Subject Property Address: </ml:EncodedLabel></DIV>
								</TD>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLiteral id="m_SubjectPropertyAddress" Runat="server"></ml:EncodedLiteral></DIV>
								</TD>
							</TR>
							<TR>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLabel id="m_titleBranchName" Runat="server">Branch Name: </ml:EncodedLabel></DIV>
								</TD>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLiteral id="m_BranchName" Runat="server"></ml:EncodedLiteral></DIV>
								</TD>
							</TR>
							<TR>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLabel id="m_titleLoanStatus" Runat="server">Loan Status: </ml:EncodedLabel></DIV>
								</TD>
								<TD vAlign="bottom">
									<DIV style="TEXT-ALIGN: left">
										<ml:EncodedLiteral id="m_LoanStatus" Runat="server"></ml:EncodedLiteral></DIV>
								</TD>
							</TR>
							<TR>
								<TD vAlign="bottom" colSpan="2">
									<ml:PassthroughLabel id="m_becomeInternalBroker" Runat="server"></ml:PassthroughLabel></TD>
							</TR>
							<TR>
								<TD vAlign="bottom" colSpan="2">
									<ml:PassthroughLabel id="m_becomeAccountOwner" Runat="server"></ml:PassthroughLabel></TD>
							</TR>
							<tr>
								<td colSpan="2">
									<div style="COLOR: red"><ml:EncodedLabel id="m_LoanDeleted" Runat="server"></ml:EncodedLabel></div>
								</td>
							</tr>
							<tr>
								<td colspan="2">
								    <a href="#" id="m_auditHistoryLink" runat="server" target="_blank">Audit History</a> (Note different ordering and not all links work.)
								</td>
							</tr>

                            <tr>
								<td colspan="2">
								    <input type="button" id="btnUpdateCache" onclick="btnUpdateCache_clicked();" value="Update Loan File Cache" /> <span id="UpdateCacheMsg" />
								</td>
							</tr>

						</table>
					</td>
				</tr>
				<tr class="SingleLoan">
					<td>
						<hr />
					</td>
				</tr>
				</asp:panel>
				<tr>
					<td colSpan="2">
						<div style="COLOR: red; TEXT-ALIGN: center"><ml:EncodedLabel id="m_Notes" Runat="server"></ml:EncodedLabel></div>
					</td>
				</tr>

                <asp:panel id="m_RoleGrid" Runat="server">
				<tr class="SingleLoan">
					<td>
						<ML:COMMONDATAGRID id="m_Grid" runat="server" width="100%" CssClass="DataGrid"
                            OnItemDataBound="GridItemDataBound"
                            AllowSorting="true" AutoGenerateColumns="false" CellPadding="2" Style="Top_padding: 10">
							<columns>
								<ASP:TemplateColumn SortExpression="RoleModifiableDesc" HeaderText="Role">
									<ItemTemplate>
                                        <%# AspxTools.HtmlString(AsObject<string>(DataBinder.Eval(Container.DataItem, "RoleModifiableDesc")) ?? "NULL") %>
									</ItemTemplate>
								</ASP:TemplateColumn>
								<ASP:TemplateColumn SortExpression="fullName" HeaderText="Full Name">
									<ItemTemplate>
                                        <%# AspxTools.HtmlString(AsObject<string>(DataBinder.Eval(Container.DataItem, "fullName")) ?? "NULL") %>
									</ItemTemplate>
								</ASP:TemplateColumn>
								<ASP:TemplateColumn SortExpression="LoginNm" HeaderText="Login">
									<ItemTemplate>
                                        <%# AspxTools.HtmlString(AsObject<string>(DataBinder.Eval(Container.DataItem, "LoginNm")) ?? "NULL") %>
									</ItemTemplate>
								</ASP:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <td></td>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
							</columns>
						</ML:COMMONDATAGRID>
					</td>
				</tr>
                <tr class="SingleLoan">
					<td>
						<hr />
					</td>
				</tr>
                </asp:panel>
            <asp:PlaceHolder runat="server" ID="LoanSearch">
                <tr class="SingleLoan">
                    <td>
                        <asp:Repeater runat="server" ID="LoanHits" OnItemDataBound="LoanHits_OnItemDataBound">
                            <HeaderTemplate>
                                <table>
                                    <thead>
                                        <tr class="GridHeader">
                                            <td>Loan Name</td>
                                            <td>Purpose</td>
                                            <td>Status</td>
                                            <td>Status Date</td>
                                            <td>
                                                Lien Position
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>

                            <ItemTemplate>
                                <tr runat="server" id="row">
                                    <td>
                                        <a runat="server" id="LoadLoan"></a>
                                    </td>
                                    <td>
                                        <ml:EncodedLiteral runat="server" ID="sLPurposeT"></ml:EncodedLiteral>
                                    </td>
                                    <td>
                                        <ml:EncodedLiteral runat="server" ID="sStatusT"></ml:EncodedLiteral>
                                    </td>
                                    <td>
                                        <ml:EncodedLiteral runat="server" ID="sStatusD"></ml:EncodedLiteral>
                                    </td>
                                    <td>
                                        <ml:EncodedLiteral runat="server" ID="sLienPosT"></ml:EncodedLiteral>
                                    </td>
                                </tr>
                                <asp:Repeater runat="server" ID="Apps" OnItemDataBound="LoanHits_Apps_OnItemDataBound">
                                    <ItemTemplate>
                                        <tr runat="server" id="row">
                                            <td colspan="5">
                                                <span style="font-weight:bold; margin-left: 15px;"> Borrower: </span>
                                                <ml:EncodedLiteral runat="server" ID="BorrowerName"></ml:EncodedLiteral>
                                                <span style="font-weight:bold; margin-left: 15px;" > Coborrower :</span>
                                                <ml:EncodedLiteral runat="server" ID="CoborrowerName"></ml:EncodedLiteral>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
            </asp:PlaceHolder>
                <asp:panel id="m_LoanLinkageSection" Runat="server">
                <tr class="SingleLoan">
                    <td>
                        <b>Loan Linkage:</b>
                        <div style="padding-left:10px;" id="LoanLinkageSpacer">
                            Loan ID to Link: <asp:TextBox runat="server" ID="LoanIdToLink" Columns="45"></asp:TextBox>
                            <br />
                            <input type="checkbox" ID="ScrapExistingLinkagesField" />
                            <label>Delete existing linkages?</label>
                            <br />
                            <span id="LoanLinkageMsg" />
                        </div>
                        <input type="button" id="btnLinkLoans" onclick="btnLinkLoans_clicked();" value="Link Loans"/>
                    </td>
                </tr>
                <tr class="SingleLoan">
					<td>
						<hr />
					</td>
				</tr>
                </asp:panel>

                <asp:panel id="m_sInitApr" Runat="server">
                <tr class="SingleLoan">
                    <td>
                        <b>Set Initial APR:</b>
                        <div style="padding-left:10px;" id="Div1">
                            <label>Initial APR</label>&nbsp;
                            <ml:percenttextbox id="sInitAPR" runat="server" width="60px" preset="percent"></ml:percenttextbox>
                            <br />
                            <span id="UpdateInitialAprMessage" />
                        </div>
                        <input type="button" id="btnUpdateInitialApr" value="Update Loan" onclick="btnUpdateInitialApr_clicked();"/>
                    </td>
                </tr>
                <tr class="SingleLoan">
					<td>
						<hr />
					</td>
				</tr>
                </asp:panel>
                <tr class="MultipleLoans">
                    <td>
                        <div class="operation">
                            <table>
                                <tr>
                                    <td colspan="2"><h4>Update Cache for Loan IDs</h4></td>
                                </tr>
                                <tr>
                                    <td>Loan IDs:</td>
                                    <td><textarea rows="5" cols="50" id="sLIdsForUpdateCacheForMultipleLoans"></textarea></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="button" id="UpdateCacheForMultipleLoans" value="Update Cache" />
                                        <span id="MessageFromUpdateCacheForMultipleLoans" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="operation">
                            <table>
                                <tr>
                                    <td colspan="2"><h4>Lookup Loan IDs from Loan Number &amp; Customer Code</h4></td>
                                </tr>
                                <tr>
                                    <td>Customer Code:</td>
                                    <td><input type="text" id="CustomerCodeForLookupLoanIDFromCustomerCodeAndLoanNumber" /></td>
                                </tr>
                                <tr>
                                    <td>Loan Numbers:</td>
                                    <td><textarea rows="5" cols="50" id="LoanNumbersForLookupLoanIDFromCustomerCodeAndLoanNumber"></textarea></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="button" id="LookupLoanIDFromCustomerCodeAndLoanNumber" value="Lookup Loan IDs" />
                                        <span id="MessageFromLookupLoanIDFromCustomerCodeAndLoanNumber" />
                                    </td>
                                </tr>
                                <tr id="ResultFromLookupLoanIDFromCustomerCodeAndLoanNumber">
                                    <td>Loan IDs:</td>
                                    <td><textarea rows="5" cols="50" id="LoanIDsFromLookupLoanIDFromCustomerCodeAndLoanNumber"></textarea></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
			</table>
		</form>
	</body>
</html>
