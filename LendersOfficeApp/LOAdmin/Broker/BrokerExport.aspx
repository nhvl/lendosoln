﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BrokerExport.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.BrokerExport" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Broker Export</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body>

<script type="text/javascript">
<!--

function f_LoanUpdateWarning() {
    var code = Math.floor(Math.random() * 1000) + '';
    var ans = prompt("WARNING: Data replaced by import will be PERMANENTLY and irrevocably LOST forever without ability to restore it.  " +
	  "Please type '" + code + "' below to confirm it:", "");
    var doIt = (ans != null && ans == code);

    if (doIt == false && ans != null)
        alert("Code does not match.  Deletions will not be performed.");

    return doIt;
}

//-->
</script>

<h3>Export/Import Broker and Branch</h3>
<form id="BrokerEditForm" method="post" runat="server">
    <ul>
        <li>
            <div>
            <asp:Button ID="ExportBrokerButtonId" Text="Export Broker XML" onclick="UIBrokerXmlExport" runat="server">
                    </asp:Button>
            </div>
        </li>
        <li>
		    <div>Select one xml file to upload:</div>

            <asp:FileUpload id="FileUpload1" runat="server">
            </asp:FileUpload>

            <br/><br/>
            <span onclick="f_LoanUpdateWarning();">
                <asp:Button id="UploadButton" Text="Import Loan File" OnClick="UploadButton_Click" runat="server">
                </asp:Button>
            </span>

            <hr />
        </li>
        
        <li>
            <asp:Button ID="ExportWhiteListID" Text="Export  White List" onclick="ExportWhiteList" runat="server">
            </asp:Button>
            
            <asp:Button ID="ExportDefaultWhiteListID" Text="Export Default  White List" onclick="ExportDefaultWhiteList" runat="server">
            </asp:Button>
        </li>
        <li>    
            <div>Select one rule file to upload(Field Name, Is Protected, Default Value):</div>
            <asp:FileUpload id="FileUpload3" runat="server">
            </asp:FileUpload>
            
            <br/><br/>
            
            <asp:Button id="UploadWhitelistID" Text="Import  White List" OnClick="UploadWhitelist_Click" runat="server">
            </asp:Button>
            <ml:EncodedLabel id="UploadStatusLabel" runat="server">            </ml:EncodedLabel>
        </li>
    </ul>
</form>
</body>
</html>
