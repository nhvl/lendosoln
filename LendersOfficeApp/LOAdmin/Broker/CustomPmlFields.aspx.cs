﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.CustomPmlFields;
    using LendersOffice.ObjLib.ConsumerPortal;

    public partial class CustomPmlFields : LendersOffice.Admin.SecuredAdminPage
    {
        BrokerDB m_brokerDB = null;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            EnableJquery = true;
            // jquery.tablesorter.min.js won't support row reordering
            // so use .dev.js.
            RegisterJsScript("jquery.tablesorter.dev.js");

            Guid brokerId = new Guid(RequestHelper.GetSafeQueryString("BrokerId"));
            if (brokerId == Guid.Empty)
            {
                errorMsg.Value = "Please save/create broker before configuring Custom PML Fields.";
                return;
            }
            m_brokerDB = BrokerDB.RetrieveById(brokerId);

            // Need to bind the data here or else it won't be available for save...            
            customPmlFields.DataSource = m_brokerDB.CustomPmlFieldList.Fields;

            customPmlFields.DataBind();

            customPricingPolicyFields.DataSource = m_brokerDB.CustomPricingPolicyFieldList.Fields;
            customPricingPolicyFields.DataBind();
        }

        protected void CustomField_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            CustomPmlField dataItem = args.Item.DataItem as CustomPmlField;
            if (dataItem == null) return;

            bool isCustomPmlFields = sender == customPmlFields;
            string keyword = isCustomPmlFields ? string.Format("CustomPMLField_{0}", dataItem.KeywordNum)
                : string.Format("CustomLOPPField_{0}", dataItem.KeywordNum);
            
            string cssClass = String.Format("cf{0}", keyword);

            HtmlTableRow row = args.Item.FindControl("row") as HtmlTableRow;
            row.Attributes["class"] = String.Format("{0} {1}", row.Attributes["class"], cssClass);
            
            HtmlAnchor moveUp = args.Item.FindControl("moveUp") as HtmlAnchor;
            HtmlAnchor moveDown = args.Item.FindControl("moveDown") as HtmlAnchor;
            HtmlInputHidden rank = args.Item.FindControl("rank") as HtmlInputHidden;
            moveUp.Attributes["class"] = String.Format("{0} {1}", moveUp.Attributes["class"], cssClass);
            moveDown.Attributes["class"] = String.Format("{0} {1}", moveDown.Attributes["class"], cssClass);
            rank.Attributes["class"] = String.Format("{0} {1}", rank.Attributes["class"], cssClass);
            rank.Value = dataItem.Rank.ToString();

            HtmlGenericControl visibilityTypeLabel = args.Item.FindControl("visibilityTypeLabel") as HtmlGenericControl;
            DropDownList visibilityType = args.Item.FindControl("visibilityType") as DropDownList;
            if (visibilityTypeLabel != null && visibilityType != null)
            {
                visibilityTypeLabel.Attributes["class"] = String.Format("{0} {1}", visibilityTypeLabel.Attributes["class"], cssClass);
                visibilityType.Attributes["class"] = String.Format("{0} {1}", visibilityType.Attributes["class"], cssClass);
                Tools.Bind_CustomPmlFieldVisibilityType(visibilityType);
                Tools.SetDropDownListValue(visibilityType, dataItem.VisibilityType);
            }

            HtmlGenericControl fieldDescriptionLabel = args.Item.FindControl("fieldDescriptionLabel") as HtmlGenericControl;
            HtmlInputControl fieldDescription = args.Item.FindControl("fieldDescription") as HtmlInputControl;
            fieldDescriptionLabel.Attributes["class"] = String.Format("{0} {1}", fieldDescriptionLabel.Attributes["class"], cssClass);
            fieldDescription.Attributes["class"] = String.Format("{0} {1}", fieldDescription.Attributes["class"], cssClass);
            fieldDescription.Value = dataItem.Description;

            HtmlGenericControl fieldTypeLabel = args.Item.FindControl("fieldTypeLabel") as HtmlGenericControl;
            DropDownList fieldType = args.Item.FindControl("fieldType") as DropDownList;
            fieldTypeLabel.Attributes["class"] = String.Format("{0} {1}", fieldTypeLabel.Attributes["class"], cssClass);
            fieldType.Attributes["class"] = String.Format("{0} {1}", fieldType.Attributes["class"], cssClass);
            Tools.Bind_CustomPmlFieldType(fieldType);
            Tools.SetDropDownListValue(fieldType, dataItem.Type);

            HtmlGenericControl enumMapLabel = args.Item.FindControl("enumMapLabel") as HtmlGenericControl;
            HtmlInputHidden enumMapBackup = args.Item.FindControl("enumMapBackup") as HtmlInputHidden;
            HtmlTextArea enumMap = args.Item.FindControl("enumMap") as HtmlTextArea;
            enumMap.Value = dataItem.EnumMapString;
            enumMapLabel.Attributes["class"] = String.Format("{0} {1}", enumMapLabel.Attributes["class"], cssClass);
            enumMapBackup.Attributes["class"] = String.Format("{0} {1}", enumMapBackup.Attributes["class"], cssClass);
            enumMap.Attributes["class"] = String.Format("{0} {1}", enumMap.Attributes["class"], cssClass);
            

            HtmlAnchor edit = args.Item.FindControl("edit") as HtmlAnchor;
            HtmlAnchor clear = args.Item.FindControl("clear") as HtmlAnchor;
            HtmlAnchor update = args.Item.FindControl("update") as HtmlAnchor;
            HtmlAnchor cancel = args.Item.FindControl("cancel") as HtmlAnchor;
            edit.Attributes["class"] = String.Format("{0} {1}", edit.Attributes["class"], cssClass);
            clear.Attributes["class"] = String.Format("{0} {1}", clear.Attributes["class"], cssClass);
            update.Attributes["class"] = String.Format("{0} {1}", update.Attributes["class"], cssClass);
            cancel.Attributes["class"] = String.Format("{0} {1}", cancel.Attributes["class"], cssClass);
            
            HtmlGenericControl keywordLabel = args.Item.FindControl("keywordLabel") as HtmlGenericControl;
            HtmlInputHidden keywordNum = args.Item.FindControl("keywordNum") as HtmlInputHidden;
            keywordLabel.InnerText = keyword;
            keywordNum.Value = dataItem.KeywordNum.ToString();
        }

        private CustomPmlFieldList ExtractCustomFields(Repeater repeater)
        {
            CustomPmlFieldList list = new CustomPmlFieldList();
            CustomPmlField field = null;
            
            foreach (RepeaterItem item in repeater.Items)
            {
                HtmlInputHidden keywordNum = item.FindControl("keywordNum") as HtmlInputHidden;
                field = new CustomPmlField(int.Parse(keywordNum.Value));

                HtmlInputHidden rank = item.FindControl("rank") as HtmlInputHidden;
                field.Rank = int.Parse(rank.Value);

                DropDownList visibilityType = item.FindControl("visibilityType") as DropDownList;
                if (visibilityType != null)
                {
                    field.VisibilityType = (E_CustomPmlFieldVisibilityT)Tools.GetDropDownListValue(visibilityType);
                }

                HtmlInputControl fieldDescription = item.FindControl("fieldDescription") as HtmlInputControl;
                field.Description = fieldDescription.Value;
                
                DropDownList fieldType = item.FindControl("fieldType") as DropDownList;
                field.Type = (E_CustomPmlFieldType)Tools.GetDropDownListValue(fieldType);
                
                HtmlTextArea enumMap = item.FindControl("enumMap") as HtmlTextArea;
                try
                {
                    field.SetEnumMapFromString(enumMap.Value);
                }
                catch (CBaseException exc)
                {
                    errorMsg.Value = exc.UserMessage + " Your changes were not saved.";
                    return null;
                }

                list.Add(field);
            }

            return list;
        }

        //Set the custom field list of the broker
        protected void Save(object sender, EventArgs e)
        {
            RequirePermission(LendersOffice.Security.E_InternalUserPermissions.EditBroker);

            // Just update the entire XML chunk based on what is in the view.
            var customPmlFieldList = ExtractCustomFields(customPmlFields);
            var customPricingPolicyFieldList = ExtractCustomFields(customPricingPolicyFields);
            if (customPmlFieldList == null || customPricingPolicyFieldList == null)
            {
                return;
            }

            m_brokerDB.CustomPmlFieldList = customPmlFieldList;
            m_brokerDB.CustomPricingPolicyFieldList = customPricingPolicyFieldList;
            m_brokerDB.Save();

            // 8/29/2013 GF - OPM 136980 - If any of the broker's consumer portals
            // used a field which is now disabled, remove it from the portal config.
            var invalidCustomPmlFieldIds = m_brokerDB.CustomPmlFieldList.Fields
                    .Where(a => !a.IsValid)
                    .Select(a => a.KeywordNum);

            foreach (var consumerPortalIdInfo in ConsumerPortalConfig.RetrievePortalsForBroker(m_brokerDB.BrokerID))
            {
                var portal = ConsumerPortalConfig.Retrieve(m_brokerDB.BrokerID, consumerPortalIdInfo.Id);

                var setWithoutInvalidFields = new HashSet<int>(portal.EnabledCustomPmlFields_FullApp.Except(invalidCustomPmlFieldIds));
                if (!portal.EnabledCustomPmlFields_FullApp.SetEquals(setWithoutInvalidFields))
                {
                    portal.EnabledCustomPmlFields_FullApp = setWithoutInvalidFields;
                    portal.Save();
                }

                setWithoutInvalidFields = new HashSet<int>(portal.EnabledCustomPmlFields_QuickPricer.Except(invalidCustomPmlFieldIds));
                if (!portal.EnabledCustomPmlFields_QuickPricer.SetEquals(setWithoutInvalidFields))
                {
                    portal.EnabledCustomPmlFields_QuickPricer = setWithoutInvalidFields;
                    portal.Save();
                }
            }

        }
    }
}
