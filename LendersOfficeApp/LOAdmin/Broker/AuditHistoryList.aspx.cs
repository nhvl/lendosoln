﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Audit;
using System.Collections;
using LendersOfficeApp.newlos;
using LendersOffice.Security;

using DataAccess;
using System.Web.UI.HtmlControls;
using LqbGrammar.DataTypes;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class AuditHistoryList : LendersOffice.Admin.SecuredAdminPage
    {
        protected string LoanId;
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.ViewBrokers
			};
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            LoanId = Request.QueryString["loanid"];
            m_dg.DataSource = AuditManager.RetrieveAuditList(new Guid(LoanId)); ;
            m_dg.DataBind();
        }

        protected HtmlControl RenderDetailsLink(LightWeightAuditItem audit)
        {
            if (audit.HasDetails)
            {
                //<a href="#" onclick='return f_viewAuditDetail("e73eab24-a32d-4072-a555-d4577af27fc6");'>view detail</a>
                var anchor = new HtmlAnchor();
                anchor.InnerText = "view detail";
                anchor.HRef = "#";
                if (audit.AuditSeq == 0)
                {
                    anchor.Attributes.Add("onclick", $"return f_viewAuditDetail(\"{audit.ID}\");");
                }
                else
                {
                    string data = $"{LoanId}/{audit.AuditSeq}";
                    string key = LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptAndEncode(EncryptionKeyIdentifier.AuditKeyPassword, data);

                    anchor.Attributes.Add("onclick", $"return f_viewAuditDetailVer2(\"{HttpUtility.UrlEncode(key)}\");");
                }

                return anchor;
            }

            return null;
        }

    }
}
