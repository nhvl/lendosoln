﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PmlBrokerGroups.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.PmlBrokerGroups" %>
<%@ Register TagPrefix="uc" TagName="PmlBrokerGroupsControl" Src="~/common/Workflow/PmlBrokerGroupsControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Originating Company Groups</title>    
</head>
<body>
    <script type="text/javascript">
        function _init()
        {
            if (typeof (_initControl) != "undefined")
            {
                _initControl();
            }
        }
      </script>
    <form id="form1" runat="server">    
        <uc:PmlBrokerGroupsControl id="pmlBrokerGroupsControl" runat="server" />
    </form>
</body>
