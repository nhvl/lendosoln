using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{

    /// <summary>
    /// Summary description for PmlLogos.
    /// </summary>
    public partial class PmlLogos : LendersOffice.Admin.SecuredAdminPage
	{
        private class _Item
        {
            public string BrokerNm { get; set; }
            public Guid BrokerPmlSiteId { get; set; }
            public string CustomerCode { get; set; }
        }

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsSAE
			};
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			Response.CacheControl = "no-cache"; 
			Response.AddHeader("Pragma", "no-cache");
			Response.Expires = -1; 
			if ( Page.IsPostBack ) 
			{
                Guid pmlSiteID = Guid.Empty;
                Guid.TryParse(Request.Form["pmlsiteid"], out pmlSiteID); 
				
				if ( pmlSiteID ==  Guid.Empty  || Request.Files.Count == 0 || !Request.Files[0].FileName.ToLower().EndsWith(".gif"))  
				{
					WriteStatusMessage("There was an error uploading your image. Please ensure that it is a gif image.");
					return;
				}

                FileDBTools.WriteData(E_FileDB.Normal, pmlSiteID.ToString() + ".logo.gif", stream =>
                {
                    Request.Files[0].InputStream.CopyTo(stream);
                });

				WriteStatusMessage("OKAY"); 
			}
			else 
			{
                List<_Item> list = new List<_Item>();
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerStatus", 1)
                                                };
                    using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListPriceMyLoanBrokers", parameters))
                    {
                        while (reader.Read())
                        {
                            _Item item = new _Item();
                            item.BrokerNm = (string)reader["BrokerNm"];
                            item.BrokerPmlSiteId = (Guid)reader["BrokerPmlSiteId"];
                            item.CustomerCode = (string)reader["CustomerCode"];

                            list.Add(item);
                        }
                    }
                }
                m_brokerList.DataSource = list.OrderBy(o => o.BrokerNm);
                m_brokerList.DataBind();

				this.RegisterService("logo", "/LoAdmin/Manage/UtilityService.aspx" );
			}
		}

		private void WriteStatusMessage(string message) 
		{
			Response.Clear();
			Response.Write(@"<span id=""status"">" +  message + "</span>");
			Response.Flush();
			Response.End();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
