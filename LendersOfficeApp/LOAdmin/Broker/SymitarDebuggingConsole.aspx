﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SymitarDebuggingConsole.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.SymitarDebuggingConsole" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Symitar Debugging Console</title>
    <style type="text/css">
        textarea {
            resize: both !important;
        }
        .errorMsg {
            color: Red;
            white-space: pre-wrap;
        }
        #form1 { position: center; }
        #MessageEditor, #ManualLenderConfig {
            display: block;
            height: 5em;
            width: 100em;
            resize: both;
            overflow: auto;
        }
        #MessageLog {
            white-space: pre-wrap;
            height: 20em;
            overflow-y: auto;
        }
        .message {
            display:flex;
        }
        .indicator {
            float: left;
            height: 2.3em;
            width: 2.3em;
        }
        .message.failure:hover, .failure > .indicator {
            background-color: salmon;
        }
        .message.successful:hover, .successful > .indicator {
            background-color: lightgreen;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="MessageBuilder">
            <label>Account # <input type="text" id="AccountNumber"/></label>
            <label>Record Path: <input type="text" id="RecordPath"/></label>
            <label>Fields (newline separated): <textarea id="FieldList"></textarea></label>
            <input type="button" id="CreateInquiryMessage" value="Build Inquiry" />
        </div>
        <input id="SendMessages" type="button" value="Send Messages" />
        <span id="ConfigurationChoice"></span>
        <textarea id="MessageEditor"></textarea>
        <hr />
        <div id="MessageLog"></div>
        <hr />
        <div id="ManualLenderConfigContainer">
            <div>Manually input Symitar Lender Config XML:</div>
            <textarea id="ManualLenderConfig"></textarea>
        </div>
    </div>
    </form>
</body>
<script type="text/javascript">
    var escapeHtml = (function () {
        // from https://github.com/janl/mustache.js/blob/master/mustache.js#L82
        var entityMap = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#39;',
            '/': '&#x2F;',
            '`': '&#x60;',
            '=': '&#x3D;'
        };

        return function (string) {
            return String(string).replace(/[&<>"'`=\/]/g, function fromEntityMap(s) {
                return entityMap[s];
            });
        }
    })();

    $j('#SendMessages').click(SendMessages);
    $j('#CreateInquiryMessage').click(CreateInquiryMessage);
    $j('textarea, input').attr('NoHighlight', 'true');
    var allowManualLenderConfig = !ML.BrokerId || AvailableLoanFileTypes.length === 0
    $j('#ManualLenderConfigContainer').toggle(allowManualLenderConfig);
    if (!allowManualLenderConfig) {
        InitializeConfigurationOptions($j('#ConfigurationChoice'));
    }

    function InitializeConfigurationOptions($configurationContainer) {
        function AssertMatch(value, pattern) {
            if (!value.match(pattern)) {
                throw 'Failed to match "' + value + '" to pattern ' + pattern;
            }
        }
        
        var h = hypescriptDom;
        $configurationContainer.append("Configuration to use: ").
            append($.map(AvailableLoanFileTypes, function(config, i){
                AssertMatch(config.Value, /^\d+$/);
                AssertMatch(config.Name, /^\w+$/);
                return h("label", {}, [
                    h("input", {type:"radio", name:"sLoanFileT", value:config.Value}),
                    config.Name
                ]);
            }));
        $configurationContainer.find('input[name="sLoanFileT"]').first().prop('checked', true);
    }

    function SendMessages() {
        var payload = {
            brokerId: ML.BrokerId,
            loanFileType: $j('#ConfigurationChoice input[name="sLoanFileT"]:checked').val() | null,
            manualLenderConfig: $j('#ManualLenderConfig').val(),
            messages: $j('#MessageEditor').val()
        };

        AppendMessageToLog("Sending to server " + escapeHtml(JSON.stringify(payload)));
        CallWebservice("SendMessages", payload, {
            success: function (responseBody) {
                var response = responseBody.d;
                if (response.ErrorMessage) {
                    AppendMessageToLog('Server responded "' + escapeHtml(response.ErrorMessage) + '"');
                } else {
                    var messagesHtml = '';
                    for (var i = 0; i < response.Messages.length; ++i) {
                        var message = response.Messages[i];
                        messagesHtml += CreateMessageLogHtml(message.Request, message.Response, message.Success, message.Fields);
                    }

                    AppendMessageToLog(messagesHtml);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                AppendMessageToLog('Error Response received:<br/><span class="errorMsg">' + escapeHtml(ReadToErrorMessage(jqXHR, textStatus, errorThrown)) + '</span>')
            }
        });
    }

    function CreateMessageLogHtml(request, response, isSuccessful, fieldList) {
        return '<div class="message ' + (isSuccessful ? 'successful' : 'failure') + '">' +
            '<div class="indicator"></div>' +
            '<div>' +
                '<div>' + escapeHtml(request.trim()) + '</div>' +
                '<div>' + escapeHtml(response.trim()) + '</div>' +
            '</div>' +
        '</div>';
    }

    function AppendMessageToLog(message) {
        var messageLog = $j('#MessageLog');
        messageLog.append('<div>' + escapeHtml((new Date()).toLocaleString()) + ": " + message + '</div>');
        messageLog.scrollTop(messageLog[0].scrollHeight)
    }

    function CreateInquiryMessage() {
        var payload = {
            brokerId: ML.BrokerId,
            loanFileType: $j('#ConfigurationChoice input[name="sLoanFileT"]:checked').val() | null,
            manualLenderConfig: $j('#ManualLenderConfig').val(),
            accountNumber: $j('#AccountNumber').val(),
            recordPath: $j('#RecordPath').val(),
            fields: (function() {
                var value = $j('#FieldList').val();
                return value ? value.split(/[\r\n]+/g) : [];
            })()
        };

        CallWebservice('CreateInquiryMessage', payload, {
            success: function (responseBody) {
                $j('#MessageEditor').val(function (index, existingValue) { return responseBody.d + existingValue });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(ReadToErrorMessage(jqXHR, textStatus, errorThrown));
            }
        });
    }

    function ReadToErrorMessage(jqXHR, textStatus, errorThrown) {
        var response = JSON.parse(jqXHR.responseText);
        return response.ExceptionType + "\r\n" + response.Message + "\r\n" + response.StackTrace;
    }

    function CallWebservice(webmethod, payload, params) {
        if (!webmethod || !webmethod.match(/^\w+$/)) {
            throw 'Must specify a valid webmethod to call';
        }

        var sendObject = {
            type: "POST",
            url: 'SymitarDebuggingConsole.aspx/' + webmethod,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(payload),
            dataType: 'json',
            async: true
        };

        for (var attributeName in params) {
            sendObject[attributeName] = params[attributeName];
        }

        callWebMethodAsync(sendObject);
    }
</script>
</html>
