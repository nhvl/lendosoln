﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConversationLogCategoryEditor.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.ConversationLogCategoryEditor" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Conversation Log Category Editor</title>
    <style type="text/css">
        .windowBtnSection
        {
            padding: 10px;
        }
    </style>
</head>
<body ng-app="mainApp" ng-controller="categoryEditCtrl">
    <script type="text/javascript">
        var app = angular.module('mainApp', []);

        jQuery(function ($) {
            var categoryNameRegex = new RegExp(ML.categoryNameRegexString, 'g');
            var args = getModalArgs()

            if (!args || !args.model){
                return
            }

            var otherCategoryNames = args.model.otherCategoryNames;
            
            resize(300, 300);

            app.controller('categoryEditCtrl', function ($scope) {
                // clone it so we don't update it if they click cancel.
                $scope.category = angular.copy(args.model.category);
                $scope.permissionLevelNamesAndIds = angular.copy(permissionLevelNamesAndIds);
                $scope.selectedPermissionLevel = $scope.permissionLevelNamesAndIds.filter(function(l) {return l.id === $scope.category.defaultPermissionLevelId})[0]

                $scope.okClick = function () {
                    if (otherCategoryNames.indexOf($scope.category.name.toUpperCase()) >= 0)
                    {
                        alert('This category name already exists.');
                        return;
                    }

                    var otherCategoryDisplayNames = args.model.otherCategoryDisplayNames;
                    if (otherCategoryDisplayNames.indexOf($scope.category.displayName.toUpperCase()) >= 0) {
                        alert('This category display name already exists.');
                        return;
                    }

                    var match = categoryNameRegex.exec($scope.category.name);
                    categoryNameRegex.lastIndex = 0;
                    if (match == null || match.length === 0) {
                        alert('Your category name is invalid. The validating regex is ' + ML.categoryNameRegexString + ' .');
                        return;
                    }

                    match = categoryNameRegex.exec($scope.category.displayName);
                    categoryNameRegex.lastIndex = 0;
                    if (match == null || match.length === 0) {
                        alert('Your category name is invalid. The validating regex is ' + ML.categoryNameRegexString + ' .');
                        return;
                    }

                    if ($scope.selectedPermissionLevel == null)
                    {
                        alert('Please select a default permission level.');
                        return;
                    }
                    else {
                        $scope.category.defaultPermissionLevelId = $scope.selectedPermissionLevel.id;
                    }

                    var returnedArgs = getModalArgs() || {};
                    returnedArgs.OK = true;
                    returnedArgs.model.category = $scope.category;
                    onClosePopup(returnedArgs);
                };

                $scope.onDisplayNameKeyUp = function()
                {
                    if ($scope.category.id != null) {
                        return;
                    }

                    var displayName = $scope.category.displayName;
                    var candidateCategoryName = displayName;
                    var index = 1;
                    while (otherCategoryNames.indexOf(candidateCategoryName.toUpperCase()) >= 0)
                    {
                        candidateCategoryName = displayName + '_' + index;
                        index++;
                        if(index > 5)
                        {
                            return;
                        }
                    }

                    $scope.category.name = candidateCategoryName;
                    
                }

                $scope.closeClick = function () {
                    var args = getModalArgs() || {};
                    args.OK = false;
                    onClosePopup(args);
                };
            });
        });
    </script>
    <form id="form1" runat="server">
        <input type="checkbox" ng-model="category.isActive"/> Active<br />
        Display Name: <input type="text" ng-model="category.displayName" ng-keyup="onDisplayNameKeyUp();" maxlength="25"/> <br />
        Category Name: <input id="categoryNameElement" type="text" ng-model="category.name"  ng-disabled="true" maxlength="25"/> <br />
        * The category name is unique per broker in the db, is only for debug purposes, and cannot be changed. <br />
        Default Permission Level <select ng-model="selectedPermissionLevel" 
            ng-options="level.name for level in permissionLevelNamesAndIds track by level.id">
                                        <option value=""></option>
                                 </select><br />
        <div class="windowBtnSection">
            <input type="button" id="okBtn" value="OK" ng-click="okClick();" ng-disabled="category.displayName == null || category.displayName == '' || category.name == null || category.name == ''"/>
            <input type="button" id="closeBtn" value="Close" ng-click="closeClick();"/>
        </div>
    </form>
</body>
</html>
