// OPM Case: 23131
// Made by: Andrew Hatfield (Summer Intern)
// Date: August 7, 2008

using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Audit;
using LendersOffice.Constants;
using MeridianLink.CommonControls;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.LOAdmin.Broker
{
	public partial class LoanUtilities : LendersOffice.Admin.SecuredAdminPage
	{

        /// <summary>
        /// Loan Id for the loan currently being looked at.
        /// </summary>
        protected Guid LoanId
        {
            get
            {
                if (ViewState["LoanId"] != null)
                {
                    return (Guid)ViewState["LoanId"];
                }

                return Guid.Empty;
            }
            set
            {
                ViewState["LoanId"] = value;
            }
        }

        /// <summary>
        /// Error messages specifically for Link Loan function.
        /// </summary>
        private List<string> m_LinkLoanErrorMessages = new List<string>();
	
		/// <summary>
		/// Provides a simple error message for any errors that are encountered.
		/// </summary>
		private string ErrorMessage
		{
			// Access member.
			set
			{
				if( value.EndsWith( "." ) == false )
					ClientScript.RegisterHiddenField( "m_ErrorMessage" , value + "." );
				else
					ClientScript.RegisterHiddenField( "m_ErrorMessage" , value );
			}
		}

        private void ResetVisibility()
        {
            m_loanInfo.Visible = false;
            m_LoanDeleted.Visible = false;
            m_Notes.Visible = false;
            m_LoanLinkageSection.Visible = false;
            m_RoleGrid.Visible = false;
            m_sInitApr.Visible = false;
        }

		/// <summary>
		/// Gets the permissions of the user
		/// </summary>
		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.
			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.ViewEmployees
			};
		}

		/// <summary>
		/// Initializes Page attributes
		/// </summary>
		protected void PageLoad(object sender, System.EventArgs a)
		{
            try
			{
				//remove the whitespace from the fields
                if (IsPostBack)
                {
                    m_LoanId.Text = m_LoanId.Text.TrimWhitespaceAndBOM();
                    sLNm.Text = sLNm.Text.TrimWhitespaceAndBOM();
                    CustomerCode.Text = CustomerCode.Text.TrimWhitespaceAndBOM();
                    sLNm2.Text = sLNm2.Text.TrimWhitespaceAndBOM();
                    m_BrokerId.Text = m_BrokerId.Text.TrimWhitespaceAndBOM();
                }
                else //!IsPostBack
                {

                    ResetVisibility();

                    // Allows you to press enter to sumit the loanId and do the search instead of
                    // requiring that you "click" the search button
                    m_LoanId.Attributes.Add("OnKeyPress", "javascript:if (event.keyCode == 13) document.getElementById('m_search').click();");

                    sLNm.Attributes.Add("OnKeyPress", "javascript:if (event.keyCode == 13) document.getElementById('m_search_sLNm_CC').click();");
                    CustomerCode.Attributes.Add("OnKeyPress", "javascript:if (event.keyCode == 13) document.getElementById('m_search_sLNm_CC').click();");

                    sLNm2.Attributes.Add("OnKeyPress", "javascript:if (event.keyCode == 13) document.getElementById('m_search_sLNm_BId').click();");
                    m_BrokerId.Attributes.Add("OnKeyPress", "javascript:if (event.keyCode == 13) document.getElementById('m_search_sLNm_BId').click();");

                    Guid sLId = RequestHelper.GetGuid("sLId", Guid.Empty);

                    if (sLId != Guid.Empty)
                    {
                        LoanId = sLId;
                        BindDataGrid();
                    }
                }
			}
			catch(Exception e)
			{
				if(e is ThreadAbortException)
					throw e;

				Tools.LogError( "Loan List: Failed while loading..." );
				Tools.LogError( e );

				ErrorMessage = e.Message;
			}
		}

		/// <summary>
		///  Executed when the user clicks the search button. Input checking is done client side for IE users
		/// </summary>
		protected void SearchClick( object sender , CommandEventArgs a )
		{
            ResetVisibility();

            LoanId = Guid.Empty;
            switch (a.CommandName)
            {
                case "sLID":
                {
                    LoanId = new Guid(m_LoanId.Text.TrimWhitespaceAndBOM());
                    break;
                }
                case "sLNm_CC":
                {
                    string LoanNumber = sLNm.Text.TrimWhitespaceAndBOM();
                    string customerCode = CustomerCode.Text.TrimWhitespaceAndBOM();

                    Guid BrokerId = Tools.GetBrokerIdByCustomerCode(customerCode);
                    if (BrokerId != Guid.Empty)
                    {
                        LoanId = Tools.GetLoanIdByLoanName(BrokerId, LoanNumber);
                    }

                    break;
                }
                case "sLNm_BId":
                {
                    Guid BrokerId = new Guid(m_BrokerId.Text.TrimWhitespaceAndBOM());
                    string LoanNumber = sLNm2.Text.TrimWhitespaceAndBOM();

                    LoanId = Tools.GetLoanIdByLoanName(BrokerId, LoanNumber);
                    break;
                }
                case "propSearch":
                    {
                        Guid BrokerId = new Guid(propBroker.Text.TrimWhitespaceAndBOM());
                        var search = new LendersOffice.ObjLib.LoanSearch.Basic.LoanSearch(BrokerId);
                        var criteria = new LendersOffice.ObjLib.LoanSearch.Basic.LoanCriteria();
                        criteria.ApplicantCriterias = new List<LendersOffice.ObjLib.LoanSearch.Basic.ApplicantCriteria>();
                        criteria.PropertyAddress = this.PropAddress.Text;
                        criteria.PropertyCity = this.PropCity.Text;
                        criteria.PropertyState = this.PropState.Text;
                        criteria.PropertyZip = this.PropZip.Text;
                        criteria.ApplicantCriterias.Add(new LendersOffice.ObjLib.LoanSearch.Basic.ApplicantCriteria() {
                            FirstName = this.BorrFirstName.Text,
                            LastName = this.BorrLastName.Text,
                            SSN = this.BorrSSN.Text
                        });

                        this.LoanHits.DataSource = search.Search(criteria);
                        this.LoanHits.DataBind();

                        return;
                    }
            }

            if (LoanId == Guid.Empty)
            {
                m_Notes.Text = "Could not find a loan matching the provided parameters.";
                m_Notes.Visible = true;

                LoanId = Guid.Empty;
                return;
            }

            BindDataGrid();
		}

		/// <summary>
		/// Custom event handler
		/// </summary>

		protected void OnGridSort( object sender , CommandEventArgs a )
		{
			switch(a.CommandName)
			{
				// Bind the datagrid for a sort
				case "sort":
				{
					BindDataGrid();
				}
					break;
				case "becomeUser":
				{
					// Logs in as a user
					// CommandArgument comes as brokerId,userId,login name
					string[] args = a.CommandArgument.ToString().Split(',');
					RequirePermission(E_InternalUserPermissions.BecomeUser);
                    if (RequestHelper.BecomeUser(new Guid(args[0]), new Guid(args[1]), PrincipalFactory.CurrentPrincipal.UserId) == false)
						ErrorMessage = "Unable to become user.";
				}
					break;
				case "becomePMLUser":
				{
					string[] args = a.CommandArgument.ToString().Split(',');
					RequirePermission(E_InternalUserPermissions.BecomeUser);
					RequestHelper.BecomePmlUser(new Guid(args[0]), new Guid(args[1]), PrincipalFactory.CurrentPrincipal.UserId);
				}
					break;
			}
		
		}

		/// <summary>
		/// Delivers the controls (links / text )  for the last column in the list of users associated with the loan.
		/// </summary>
        private IEnumerable<Control> RenderBecomeLinkAsControls(object container)
        {
            var controls = new List<Control>();

            Guid brokerId = (Guid)DataBinder.Eval(container, "BrokerId");
            string userId = DataBinder.Eval(container, "UserId").ToString();
            string loginName = DataBinder.Eval(container, "LoginNm").ToString();
            string type = DataBinder.Eval(container, "Type").ToString();
            string canLogin = DataBinder.Eval(container, "canLogin").ToString();
            string isActive = DataBinder.Eval(container, "isActive").ToString();
            string lenderPMLSiteId = DataBinder.Eval(container, "BrokerPmlSiteId").ToString();
            string status = DataBinder.Eval(container, "Status").ToString();

            if (status != "1")
            {
                controls.Add(new EncodedLiteral() { Text = "Broker is inactive" });
                return controls;
            }

            if (canLogin != "True" || isActive != "True")
            {
                controls.Add(new EncodedLiteral() { Text = "No active User Account" });
                return controls;
            }
            
            if (type == "B")
            {
                var becomeUserLink = new HyperLink();
                becomeUserLink.Text = "become";
                becomeUserLink.ToolTip = "become broker only";
                becomeUserLink.NavigateUrl = string.Format("javascript:becomeUser({0},{1},{2})", AspxTools.JsString(brokerId), AspxTools.JsString(userId), AspxTools.JsString(loginName));
                
                controls.Add(becomeUserLink);

                return controls;
            }

            if (type == "P")
            {
                var link = new HyperLink();
                link.NavigateUrl = string.Format("javascript:becomePMLUser({0},{1})", AspxTools.JsString(userId), AspxTools.JsString(lenderPMLSiteId));
                link.Text = "become PML user";
                link.ToolTip = "become PML user";
                controls.Add(link);
                return controls;
            }

            return controls;
        }

        private ListItemType CurrentType;

        protected void LoanHits_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }
            CurrentType = args.Item.ItemType;
            HtmlTableRow row = (HtmlTableRow)args.Item.FindControl("row");
            row.Attributes.Add("class", CurrentType == ListItemType.Item ? "GridItem" : "GridAlternatingItem");

            HtmlAnchor anchor = (HtmlAnchor)args.Item.FindControl("LoadLoan");
            EncodedLiteral sLPurposeT = (EncodedLiteral)args.Item.FindControl("sLPurposeT");
            EncodedLiteral sStatusT = (EncodedLiteral)args.Item.FindControl("sStatusT");
            EncodedLiteral sStatusD = (EncodedLiteral)args.Item.FindControl("sStatusD");
            EncodedLiteral sLienPosT = (EncodedLiteral)args.Item.FindControl("sLienPosT");
            Repeater apps = (Repeater)args.Item.FindControl("Apps");
            var details = (LendersOffice.ObjLib.LoanSearch.Basic.LoanDetails)args.Item.DataItem;
            anchor.HRef = Page.ResolveClientUrl("~/Loadmin/Broker/LoanUtilities.aspx") + "?sLId=" + AspxTools.HtmlString(details.sLId);
            anchor.Target = "_blank";
            anchor.InnerText = details.sLNm;
            sLPurposeT.Text = details.sLPurposeT.ToString();
            if (details.sStatusD.HasValue)
            {
                sStatusD.Text = details.sStatusD.Value.ToShortDateString();
            }

            sLienPosT.Text = details.sLienPosT.ToString();
            sStatusT.Text = details.sStatusT.ToString();
            apps.DataSource = details.Applicants;
            apps.DataBind();
        }

        protected void LoanHits_Apps_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            HtmlTableRow row = (HtmlTableRow)args.Item.FindControl("row");
            row.Attributes.Add("class", CurrentType == ListItemType.Item ? "GridItem" : "GridAlternatingItem");

            var details = (LendersOffice.ObjLib.LoanSearch.Basic.ApplicantDetails)args.Item.DataItem;
            EncodedLiteral borrName = (EncodedLiteral)args.Item.FindControl("BorrowerName");
            EncodedLiteral coName = (EncodedLiteral)args.Item.FindControl("CoborrowerName");

            borrName.Text = string.Format("{0} {1} {2}", details.aBFirstNm, details.aBLastNm, details.aBSsn);
            coName.Text = string.Format("{0} {1} {2}", details.aCFirstNm, details.aCLastNm, details.aCSsn);

        }

        protected void GridItemDataBound(object sender, DataGridItemEventArgs e)
        {
            var item = e.Item;
            if (item.ItemIndex == -1)
            {
                // no need to bind here for header/footer.
                return; 
            }

            var tc = item.Cells[3];
            var cellControls = RenderBecomeLinkAsControls(item.DataItem);
            foreach (var control in cellControls)
            {
                tc.Controls.Add(control);
            }
        }

		/// <summary>
		/// Get data from the DB and re/bind the datagrid.
		/// </summary>
		private void BindDataGrid()
		{
			// Searches the DB for loan info and the assignments
			// associated with the loan. 

            Guid brokerId = Guid.Empty;

            try
            {
                DbConnectionInfo.GetConnectionInfoByLoanId(LoanId, out brokerId);
            }
            catch (LoanNotFoundException)
            {
                string inputLoanId = LoanId.ToString();
                m_Notes.Text = $"Could not find DB connection info for this loan id <{inputLoanId}>.";
                m_Notes.Visible = true;

                LoanId = Guid.Empty;
                return;
            }

            //List<SqlParameter> pA = new List<SqlParameter>();
            //pA.Add( new SqlParameter("@loanId", LoanId));

			DataSet das = new DataSet();
            SqlParameter[] parameters = {
                                            new SqlParameter("@loanId", LoanId)
                                        };
			DataSetHelper.Fill(das, brokerId, "FindLoanAssignmentInfoByLoanId", parameters);

			DataSet dls = new DataSet();
            parameters = new SqlParameter[] {
                                                new SqlParameter("@loanId", LoanId)
                                            };
			DataSetHelper.Fill(dls, brokerId, "FindLoanInfoByLoanId", parameters);

			// Makes a temporary dataset to be used to render the become account owner and PML0XXX account links
			DataSet dt = new DataSet();


            das.Tables[0].Columns.Add("BrokerId", typeof(Guid));
            foreach (DataRow row in das.Tables[0].Rows)
            {
                row["BrokerId"] = brokerId;
            }

			m_Grid.DataSource = das.Tables[0].DefaultView;
			m_Grid.DataBind();

			// Checks to see if no loan was found. Else, display it's information
			if(dls.Tables[0] == null || dls.Tables[0].DefaultView.Count < 1)
			{
                string inputLoanId = LoanId.ToString();
                m_Notes.Text = $"Loan info for loan id <{inputLoanId}> not found.";
				m_Notes.Visible = true;

                LoanId = Guid.Empty;
			}
			else
			{
				// Renders the PML0XXX becomelink or hides it if customer code is null for any reason
				if(dls.Tables[0].Rows[0]["CustomerCode"] != null)
				{
                    parameters = new SqlParameter[] {
                                                        new SqlParameter("@LoginNm", dls.Tables[0].Rows[0]["CustomerCode"])
                                                    };

					dt.Clear();
					DataSetHelper.Fill(dt, brokerId, "FindActiveLOUser", parameters);

					// Checks that an internal account was returned by the query
					if(dt.Tables[0].DefaultView.Count > 0)
					{
                        string userId = AspxTools.HtmlString(dt.Tables[0].Rows[0]["UserId"].ToString());
						string loginName = AspxTools.HtmlString(dt.Tables[0].Rows[0]["LoginNm"].ToString());
                        var customerCode = AspxTools.HtmlString(dls.Tables[0].Rows[0]["CustomerCode"].ToString());
						m_becomeInternalBroker.Text = string.Format("<a href=\"#\" onclick=\"becomeUser('{0}','{1}','{2}')\">Become internal {3}</a>", brokerId, userId, loginName, customerCode);
					}
					else
					{
						m_becomeInternalBroker.Text = "No internal account was found for this company.";
					}

					m_becomeInternalBroker.Visible = true;
				}
				else
				{
					m_becomeInternalBroker.Visible = false;
					m_CustomerCode.Visible = false;
				}

                m_auditHistoryLink.HRef = "AuditHistoryList.aspx?loanid=" + AspxTools.HtmlString(LoanId);

				// Render the Account Owner becomelink or hide it if broker Id is null for any reason
				if(dls.Tables[0].Rows[0]["BrokerId"] != null)
				{
                    parameters = new SqlParameter[] {
                                                        new SqlParameter("@BrokerId", dls.Tables[0].Rows[0]["BrokerId"])
                                                    };
					dt.Clear();
					DataSetHelper.Fill(dt, brokerId, "GetAccountOwnerLoginInfoByBrokerId", parameters);

					// Checks that an account owner was returned by the query
					if(dt.Tables[0].DefaultView.Count > 0)
					{
						string userId = AspxTools.HtmlString(dt.Tables[0].Rows[0]["UserId"].ToString());
						string loginNm = AspxTools.HtmlString(dt.Tables[0].Rows[0]["LoginNm"].ToString());
						string fullNm = AspxTools.HtmlString(dt.Tables[0].Rows[0]["UserFullNm"].ToString());
						m_becomeAccountOwner.Text = string.Format("<a href=\"#\" onclick=\"becomeUser('{0}','{1}','{2}')\">Become {3} (Account Owner)</a>", brokerId, userId, loginNm, fullNm);
                    }
					else
					{
						m_becomeAccountOwner.Text = "No active account owner was found for this company.";
					}

					m_becomeAccountOwner.Visible = true;
				}
				else
				{
					m_becomeAccountOwner.Visible = false;
				}

                // Populate editable loan fields
                CPageData dataLoan = new CFullAccessPageData(LoanId, new string[] { "sInitAPR", "sLinkedLoanInfo" });
                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();

                sInitAPR.Text = dataLoan.sInitAPR_rep;

                if (dataLoan.sLinkedLoanInfo.IsLoanLinked)
                {
                    LoanIdToLink.Text = dataLoan.sLinkedLoanInfo.LinkedLId.ToString();
                }
                
				// Show the Loan info and get it from the DataSet
				m_loanInfo.Visible = true;
                m_RoleGrid.Visible = true;
                m_LoanLinkageSection.Visible = true;
                m_sInitApr.Visible = true;

				m_LoanNumber.Text = dls.Tables[0].Rows[0]["sLNm"].ToString();
				m_BrokerName.Text = dls.Tables[0].Rows[0]["BrokerNm"].ToString();
				m_CustomerCode.Text = dls.Tables[0].Rows[0]["CustomerCode"].ToString();
				m_BorrowerName.Text = dls.Tables[0].Rows[0]["sPrimBorrowerFullNm"].ToString();
				m_SubjectPropertyAddress.Text = dls.Tables[0].Rows[0]["sSpFullAddr"].ToString();
				m_BranchName.Text = dls.Tables[0].Rows[0]["BranchNm"].ToString();
				m_LoanStatus.Text = dls.Tables[0].Rows[0]["LoanStatus"].ToString();

				// Hide the Becomes links if the broker is inactive
				if(dls.Tables[0].Rows[0]["Status"].ToString() != "1")
				{
					m_becomeAccountOwner.Visible = false;
					m_becomeInternalBroker.Text = "This broker is inactive.";
                    if (m_Grid.Items.Count >= 1)
                    {
				    	m_Grid.Columns[ 3 ].Visible = false;
                    }
                }

				// Display if loan has been deleted
                if (dls.Tables[0].Rows[0]["IsValid"].ToString() == "False")
                {
                    m_LoanDeleted.Visible = true;
                    m_LoanDeleted.Text = "Loan cannot be accessed. Deleted: " + dls.Tables[0].Rows[0]["DeletedD"].ToString();
                    m_LoanNumber.Text = dls.Tables[0].Rows[0]["sOldLNm"].ToString();
                    m_titleLoanNumber.Text = "Previous Loan Number: ";

                    // Get the name of the person who deleted this loan by searching the 
                    // AuditItems returned by the Auditor. Due to certain
                    foreach (LightWeightAuditItem auditItem in AuditManager.RetrieveAuditList(new Guid(m_LoanId.Text)))
                    {

                        if (string.Compare(auditItem.Description, "Loan Delete", true) == 0)
                        {
                            m_LoanDeleted.Text += " by " + auditItem.UserName;
                        }
                    }
                }
				else
				{
					m_LoanDeleted.Visible = false;
					m_titleLoanNumber.Text = "Loan Number: ";
				}

				// Hide become links if the user either doesn't have permission or if the loan was deleted (nobody can access a deleted loan)
				if( HasPermissionToDo( E_InternalUserPermissions.BecomeUser ) == false || dls.Tables[0].Rows[0]["IsValid"].ToString() == "False")
				{
                    if (m_Grid.Items.Count >= 1)
                    {
				    	m_Grid.Columns[ 3 ].Visible = false;
                    }
                }
			}
		}

        /// <summary>
        /// This method probably belongs elsewhere.
        /// </summary>
        public static T AsObject<T>(object o) where T: class
        {
            if (o == DBNull.Value)
            {
                return null;
            }
            else
            {
                return (T)o;
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            EnableJquery = true;
            this.RegisterService("main", "/LoAdmin/Broker/LoanUtilitiesService.aspx");

			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.m_Grid.ItemCustomCommand += new System.Web.UI.WebControls.CommandEventHandler(this.OnGridSort);
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
