﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Broker
{
    public partial class LoanStatusconfig : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        private BrokerDB db
        {
            get
            {
                return BrokerDB.RetrieveById(RequestHelper.GetGuid("BrokerId"));
            }
        }



        protected void Initialize()
        {
            IncludeStyleSheet(StyleSheet);
            Array statusVals = Enum.GetNames(typeof(E_sStatusT));

            for(int i = 2; i < StatusTable.Rows.Count; i++)
            {
                if(i % 2 == 0)
                    StatusTable.Rows[i].Attributes.Add("class", "GridItem");
                else
                    StatusTable.Rows[i].Attributes.Add("class", "GridAlternatingItem");
            }

            for (int i = 0; i < statusVals.Length; i++)
            {
                String statusName = (String)statusVals.GetValue(i);
                System.Web.UI.HtmlControls.HtmlTableRow row = (System.Web.UI.HtmlControls.HtmlTableRow)FindControl(statusName);

                if (row != null)
                {

                    //create the checkboxes
                    CheckBox retail = new CheckBox();
                    CheckBox wholesale = new CheckBox();
                    CheckBox broker = new CheckBox();
                    //CheckBox correspondent = new CheckBox();
                    CheckBox channelNotSpecified = new CheckBox();

                    // OPM 185961
                    CheckBox delegated = new CheckBox();
                    CheckBox priorApproved = new CheckBox();
                    CheckBox miniCorrespondent = new CheckBox();
                    CheckBox bulk = new CheckBox();
                    CheckBox miniBulk = new CheckBox();

                    retail.ID = statusName + "Retail";
                    wholesale.ID = statusName + "Wholesale";
                    broker.ID = statusName + "Broker";
                    //correspondent.ID = statusName + "Correspondent";
                    channelNotSpecified.ID = statusName + "ChannelNotSpecified";

                    // OPM 185961
                    delegated.ID = statusName + "Delegated";
                    priorApproved.ID = statusName + "PriorApproved";
                    miniCorrespondent.ID = statusName + "MiniCorrespondent";
                    bulk.ID = statusName + "Bulk";
                    miniBulk.ID = statusName + "MiniBulk";

                    System.Web.UI.HtmlControls.HtmlTableCell th = new System.Web.UI.HtmlControls.HtmlTableCell();
                    th.Controls.Add(retail);
                    th.Style.Add("text-align", "center");
                    row.Controls.Add(th);

                    th = new System.Web.UI.HtmlControls.HtmlTableCell();
                    th.Style.Add("text-align", "center");
                    th.Controls.Add(wholesale);
                    row.Controls.Add(th);

                    th = new System.Web.UI.HtmlControls.HtmlTableCell();
                    th.Style.Add("text-align", "center");
                    th.Controls.Add(broker);
                    row.Controls.Add(th);

                    //th = new System.Web.UI.HtmlControls.HtmlTableCell();
                    //th.Style.Add("text-align", "center");
                    //th.Controls.Add(correspondent);
                    //row.Controls.Add(th);

                    // OPM 185961
                    th = new System.Web.UI.HtmlControls.HtmlTableCell();
                    th.Style.Add("text-align", "center");
                    th.Controls.Add(delegated);
                    row.Controls.Add(th);

                    th = new System.Web.UI.HtmlControls.HtmlTableCell();
                    th.Style.Add("text-align", "center");
                    th.Controls.Add(priorApproved);
                    row.Controls.Add(th);

                    th = new System.Web.UI.HtmlControls.HtmlTableCell();
                    th.Style.Add("text-align", "center");
                    th.Controls.Add(miniCorrespondent);
                    row.Controls.Add(th);

                    th = new System.Web.UI.HtmlControls.HtmlTableCell();
                    th.Style.Add("text-align", "center");
                    th.Controls.Add(bulk);
                    row.Controls.Add(th);

                    th = new System.Web.UI.HtmlControls.HtmlTableCell();
                    th.Style.Add("text-align", "center");
                    th.Controls.Add(miniBulk);
                    row.Controls.Add(th);

                    th = new System.Web.UI.HtmlControls.HtmlTableCell();
                    th.Style.Add("text-align", "center");
                    th.Controls.Add(channelNotSpecified);
                    row.Controls.Add(th);
                }

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Initialize();

            //load the checkboxes
            HashSet<E_sStatusT> retail = db.GetEnabledStatusesByChannel(E_BranchChannelT.Retail);
            HashSet<E_sStatusT> wholesale = db.GetEnabledStatusesByChannel(E_BranchChannelT.Wholesale);
            HashSet<E_sStatusT> broker = db.GetEnabledStatusesByChannel(E_BranchChannelT.Broker);

            // OPM 185961
            HashSet<E_sStatusT> delegated = db.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.Delegated);
            HashSet<E_sStatusT> priorApproved = db.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.PriorApproved);
            HashSet<E_sStatusT> miniCorrespondent = db.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.MiniCorrespondent);
            HashSet<E_sStatusT> bulk = db.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.Bulk);
            HashSet<E_sStatusT> miniBulk = db.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.MiniBulk);

            HashSet<E_sStatusT> channelNotSpecified = db.GetEnabledStatusesByChannel(E_BranchChannelT.Blank);

            foreach (E_sStatusT status in retail)
            {
                CheckBox checkbox = ((CheckBox)FindControl(status.ToString() + "Retail"));
                if(checkbox != null)
                    checkbox.Checked = true;
            }

            foreach (E_sStatusT status in wholesale)
            {
                CheckBox checkbox = ((CheckBox)FindControl(status.ToString() + "Wholesale"));
                if (checkbox != null)
                    checkbox.Checked = true;
            }

            foreach (E_sStatusT status in broker)
            {
                CheckBox checkbox = ((CheckBox)FindControl(status.ToString() + "Broker"));
                if (checkbox != null)
                    checkbox.Checked = true;
            }

            // OPM 185961
            foreach (E_sStatusT status in delegated)
            {
                CheckBox checkbox = ((CheckBox)FindControl(status.ToString() + "Delegated"));
                if (checkbox != null)
                    checkbox.Checked = true;
            }

            foreach (E_sStatusT status in priorApproved)
            {
                CheckBox checkbox = ((CheckBox)FindControl(status.ToString() + "PriorApproved"));
                if (checkbox != null)
                    checkbox.Checked = true;
            }

            foreach (E_sStatusT status in miniCorrespondent)
            {
                CheckBox checkbox = ((CheckBox)FindControl(status.ToString() + "MiniCorrespondent"));
                if (checkbox != null)
                    checkbox.Checked = true;
            }

            foreach (E_sStatusT status in bulk)
            {
                CheckBox checkbox = ((CheckBox)FindControl(status.ToString() + "Bulk"));
                if (checkbox != null)
                    checkbox.Checked = true;
            }

            foreach (E_sStatusT status in miniBulk)
            {
                CheckBox checkbox = ((CheckBox)FindControl(status.ToString() + "MiniBulk"));
                if (checkbox != null)
                    checkbox.Checked = true;
            }

            foreach (E_sStatusT status in channelNotSpecified)
            {
                CheckBox checkbox = ((CheckBox)FindControl(status.ToString() + "ChannelNotSpecified"));
                if (checkbox != null)
                    checkbox.Checked = true;
            }
        }

        protected void Save(object sender, System.EventArgs e)
        {
            RequirePermission(E_InternalUserPermissions.EditBroker);

            Array statusVals = Enum.GetNames(typeof(E_sStatusT));

            for (int i = 0; i < statusVals.Length; i++)
            {
                CheckBox check = (CheckBox)FindControl(statusVals.GetValue(i) + "Retail");
                if(check != null && check.Checked)
                    db.AddEnabledStatusToBranchChannel((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_BranchChannelT.Retail);
                else
                    db.RemoveEnabledStatusFromBranchChannel((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_BranchChannelT.Retail);

                check = (CheckBox)FindControl(statusVals.GetValue(i) + "Wholesale");
                if (check != null && check.Checked)
                    db.AddEnabledStatusToBranchChannel((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_BranchChannelT.Wholesale);
                else
                    db.RemoveEnabledStatusFromBranchChannel((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_BranchChannelT.Wholesale);

                check = (CheckBox)FindControl(statusVals.GetValue(i) + "Broker");
                if (check != null && check.Checked)
                    db.AddEnabledStatusToBranchChannel((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_BranchChannelT.Broker);
                else
                    db.RemoveEnabledStatusFromBranchChannel((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_BranchChannelT.Broker);

                // OPM 185961
                check = (CheckBox)FindControl(statusVals.GetValue(i) + "Delegated");
                if (check != null && check.Checked)
                    db.AddEnabledStatusToCorrespondentProcessType((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_sCorrespondentProcessT.Delegated);
                else
                    db.RemoveEnabledStatusFromCorrespondentProcessType((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_sCorrespondentProcessT.Delegated);

                check = (CheckBox)FindControl(statusVals.GetValue(i) + "PriorApproved");
                if (check != null && check.Checked)
                    db.AddEnabledStatusToCorrespondentProcessType((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_sCorrespondentProcessT.PriorApproved);
                else
                    db.RemoveEnabledStatusFromCorrespondentProcessType((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_sCorrespondentProcessT.PriorApproved);

                check = (CheckBox)FindControl(statusVals.GetValue(i) + "MiniCorrespondent");
                if (check != null && check.Checked)
                    db.AddEnabledStatusToCorrespondentProcessType((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_sCorrespondentProcessT.MiniCorrespondent);
                else
                    db.RemoveEnabledStatusFromCorrespondentProcessType((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_sCorrespondentProcessT.MiniCorrespondent);

                check = (CheckBox)FindControl(statusVals.GetValue(i) + "Bulk");
                if (check != null && check.Checked)
                    db.AddEnabledStatusToCorrespondentProcessType((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_sCorrespondentProcessT.Bulk);
                else
                    db.RemoveEnabledStatusFromCorrespondentProcessType((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_sCorrespondentProcessT.Bulk);

                check = (CheckBox)FindControl(statusVals.GetValue(i) + "MiniBulk");
                if (check != null && check.Checked)
                    db.AddEnabledStatusToCorrespondentProcessType((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_sCorrespondentProcessT.MiniBulk);
                else
                    db.RemoveEnabledStatusFromCorrespondentProcessType((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_sCorrespondentProcessT.MiniBulk);

                check = (CheckBox)FindControl(statusVals.GetValue(i) + "ChannelNotSpecified");
                if (check != null && check.Checked)
                    db.AddEnabledStatusToBranchChannel((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_BranchChannelT.Blank);
                else
                    db.RemoveEnabledStatusFromBranchChannel((E_sStatusT)Enum.Parse(typeof(E_sStatusT), (String)statusVals.GetValue(i)), E_BranchChannelT.Blank);
            }
           
            db.Save();
        }
    }
}
