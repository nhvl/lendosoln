using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.UI;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Common.TextImport;
using LendersOffice.Constants;
using LendersOffice.Security;
using System.Linq;

namespace LendersOfficeApp.LOAdmin.Broker
{
	public partial class ImportExternalEmployees : LendersOffice.Admin.SecuredAdminPage
	{
		private Hashtable m_MgrSet = new Hashtable();
		private Hashtable m_LocSet = new Hashtable();
		private Hashtable m_LenSet = new Hashtable();
		private Hashtable m_Branch = new Hashtable();
		private Hashtable m_PriceGroup = new Hashtable();
		private Hashtable m_UwSet = new Hashtable();
		private Hashtable m_JuniorUwSet = new Hashtable();
		private Hashtable m_ProcSet = new Hashtable();
		private Hashtable m_JuniorProcSet = new Hashtable();
		private Hashtable m_SupSet = new Hashtable();

		private string ErrorMessage
		{
			set { ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." ); }
		}

		private string FeedBack
		{
			set { ClientScript.RegisterHiddenField( "m_feedBack" , value.TrimEnd( '.' ) + "." ); }
		}

		private Guid BrokerId
		{
			get
			{
                return RequestHelper.GetGuid("brokerId", Guid.Empty);
			}
        }

        #region OPM 32417   av 7 14 09 
        /// <summary>
        /// Don't use directly. Use CurrentPmlBrokers.
        /// </summary>
        private SortedDictionary<string, PmlBroker> x_PmlCompanies = null;
        private IList<string> m_DuplicateCompanies = new List<string>();
        private IList<string> m_VerifyCompanyUserNames = new List<string>(); 

        private SortedDictionary<string, PmlBroker> CurrentPmlBrokers
        {
            get
            {
                if (x_PmlCompanies == null)
                {
                    var pmlBrokers = PmlBroker.ListAllPmlBrokerByBrokerId(BrokerId);
                    x_PmlCompanies = new SortedDictionary<string, PmlBroker>(StringComparer.OrdinalIgnoreCase);
                    foreach (var pmlBroker in pmlBrokers)
                    {
                        if (x_PmlCompanies.ContainsKey(pmlBroker.Name.TrimWhitespaceAndBOM()))
                        {
                            if (!m_DuplicateCompanies.Contains(pmlBroker.Name.TrimWhitespaceAndBOM().ToLower()))
                            {
                                m_DuplicateCompanies.Add(pmlBroker.Name.TrimWhitespaceAndBOM().ToLower());
                            }
                        }
                        else
                        {
                            x_PmlCompanies.Add(pmlBroker.Name.TrimWhitespaceAndBOM(), pmlBroker);
                        }

                    }
                }
                return x_PmlCompanies;
            }
        }
        #endregion 

        /// <summary>
		/// Return this page's required permissions set.  This
		/// test occurs with each page initialization.
		/// </summary>
		/// <returns>
		/// Array of required permissions.
		/// </returns>

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return this page's required permissions set.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.EditBroker ,
				E_InternalUserPermissions.EditEmployee
			};
		}

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Initialize this page.

			try
			{
				// Setup type selection and load cached entries from viewstate.

				if( IsPostBack == false )
				{
					// 7/13/2005 kb - Fixing up the loading of employees by role
					// to use a helper that loads everything (it's quicker than
					// by role).  We pull the full names for matching (see
					// case 2372).
					//
					// 7/22/2005 kb - Reworked role loading to use new
					// view.  It seems that loading each role individually
					// is better than loading the whole broker when you
					// expect to have *many* loan officers because of pml.

					BrokerLoanAssignmentTable roleTable = new BrokerLoanAssignmentTable();
                    m_Employees.Text = "firstname,middlename,lastname,companyname,branch,pricegroup,street,city,state,zipcode,phone,fax,cellphone,iscellphoneprivate,enableauthcodeviasms,pager,email,createwholesaleloans,createminicorrloans,createcorrloans,applyforineligiblelp,runwithoutreport,submitwithoutreport,issupervisor,supervisorname,login,password,mustchangepasswordnextlogin,passwordneverexpires,passwordexpireson,expirepasswordsevery,manager,lockdesk,lenderaccountexec,underwriter,processor,nmlsidentifier,statelicenses,juniorunderwriter,juniorprocessor";
					m_Upload.ToolTip = "Loads text into box below";

					try
					{
						roleTable.Retrieve( BrokerId , E_RoleT.LenderAccountExecutive);
						roleTable.Retrieve( BrokerId , E_RoleT.LockDesk );
						roleTable.Retrieve( BrokerId , E_RoleT.Manager );
						roleTable.Retrieve( BrokerId , E_RoleT.Underwriter );
						roleTable.Retrieve( BrokerId , E_RoleT.JuniorUnderwriter );
						roleTable.Retrieve( BrokerId , E_RoleT.Processor );
						roleTable.Retrieve( BrokerId , E_RoleT.JuniorProcessor );
					}
					catch( Exception e )
					{
						Tools.LogError( "Unable to load role table." , e );
					}

                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@BrokerId" ,  BrokerId )
                                                };

					using( IDataReader sR = StoredProcedureHelper.ExecuteReader( BrokerId, "ListPriceMyLoanManagerByBrokerId" , parameters ) )
					{
						while( sR.Read() )
						{
							try
							{
								m_SupSet.Add(  sR["UserFullName"].ToString().TrimWhitespaceAndBOM().ToLower() , sR["EmployeeId"] );
							}
							catch { }
						}
					}

                    if (roleTable[E_RoleT.Underwriter] != null)
					{
						foreach( BrokerLoanAssignmentTable.Spec eSpec in roleTable[ E_RoleT.Underwriter ] )
						{
							try
							{
								m_UwSet.Add( eSpec.FullName.TrimWhitespaceAndBOM().ToLower() , eSpec.EmployeeId );
							}
							catch { }
						}
					}

                    if (roleTable[E_RoleT.JuniorUnderwriter] != null)
					{
						foreach( BrokerLoanAssignmentTable.Spec eSpec in roleTable[ E_RoleT.JuniorUnderwriter ] )
						{
							try
							{
								m_JuniorUwSet.Add( eSpec.FullName.TrimWhitespaceAndBOM().ToLower() , eSpec.EmployeeId );
							}
							catch { }
						}
					}

					if( roleTable[ E_RoleT.Processor ] != null )
					{
						foreach( BrokerLoanAssignmentTable.Spec eSpec in roleTable[ E_RoleT.Processor ] )
						{
							try
							{
								m_ProcSet.Add( eSpec.FullName.TrimWhitespaceAndBOM().ToLower() , eSpec.EmployeeId );
							}
							catch { }
						}
					}

					if( roleTable[ E_RoleT.JuniorProcessor ] != null )
					{
						foreach( BrokerLoanAssignmentTable.Spec eSpec in roleTable[ E_RoleT.JuniorProcessor ] )
						{
							try
							{
								m_JuniorProcSet.Add( eSpec.FullName.TrimWhitespaceAndBOM().ToLower() , eSpec.EmployeeId );
							}
							catch { }
						}
					}
					
					if( roleTable[ E_RoleT.LenderAccountExecutive ] != null )
					{
						foreach( BrokerLoanAssignmentTable.Spec eSpec in roleTable[ E_RoleT.LenderAccountExecutive ] )
						{
							try
							{
								m_LenSet.Add( eSpec.FullName.TrimWhitespaceAndBOM().ToLower() , eSpec.EmployeeId );
							}
							catch { }
						}
					}

					if( roleTable[ E_RoleT.LockDesk ] != null )
					{
						foreach( BrokerLoanAssignmentTable.Spec eSpec in roleTable[ E_RoleT.LockDesk ] )
						{
							try
							{
								m_LocSet.Add( eSpec.FullName.TrimWhitespaceAndBOM().ToLower() , eSpec.EmployeeId );
							}
							catch {}
						}
					}

					if( roleTable[ E_RoleT.Manager ] != null )
					{
						foreach( BrokerLoanAssignmentTable.Spec eSpec in roleTable[ E_RoleT.Manager ] )
						{
							try
							{
								m_MgrSet.Add( eSpec.FullName.TrimWhitespaceAndBOM().ToLower() , eSpec.EmployeeId );
							}
							catch {}
						}
					}

                    parameters = new SqlParameter[] {
                                                    new SqlParameter( "@BrokerId" ,  BrokerId )
                                                };

					using( IDataReader sR = StoredProcedureHelper.ExecuteReader( BrokerId, "ListBranchByBrokerId" , parameters ) )
					{
						while( sR.Read() )
						{
							try
							{
								m_Branch.Add( sR[ "Name" ].ToString().TrimWhitespaceAndBOM().ToLower() , sR[ "BranchId" ] );
							}
							catch {}
						}
					}

                    parameters = new SqlParameter[] {
                                                    new SqlParameter( "@BrokerId" ,  BrokerId )
                                                };

					using( IDataReader sR = StoredProcedureHelper.ExecuteReader( BrokerId, "ListPricingGroupByBrokerId" , parameters ) )
					{
						while( sR.Read() )
						{
							try
							{
								m_PriceGroup.Add( sR[ "LpePriceGroupName" ].ToString().TrimWhitespaceAndBOM().ToLower() , sR[ "LpePriceGroupId" ]);
							}
							catch {}
						}
					}
				}
				else
				{
					if( ViewState[ "MgrSet" ] != null )
						m_MgrSet = ViewState[ "MgrSet" ] as Hashtable;

					if( ViewState[ "LocSet" ] != null )
						m_LocSet = ViewState[ "LocSet" ] as Hashtable;
				
					if( ViewState[ "LenSet" ] != null )
						m_LenSet = ViewState[ "LenSet" ] as Hashtable;

					if( ViewState[ "UwSet" ] != null )
						m_UwSet = ViewState[ "UwSet" ] as Hashtable;

					if( ViewState[ "JuniorUwSet" ] != null )
						m_JuniorUwSet = ViewState[ "JuniorUwSet" ] as Hashtable;

					if( ViewState[ "SupSet" ] != null )
						m_SupSet = ViewState[ "SupSet" ] as Hashtable;

					if( ViewState[ "ProcSet" ] != null )
						m_ProcSet = ViewState[ "ProcSet" ] as Hashtable;

					if( ViewState[ "JuniorProcSet" ] != null )
						m_JuniorProcSet = ViewState[ "JuniorProcSet" ] as Hashtable;

					if( ViewState[ "Branch" ] != null )
						m_Branch = ViewState[ "Branch" ] as Hashtable;

					if( ViewState[ "PriceGroup" ] != null )
						m_PriceGroup = ViewState[ "PriceGroup" ] as Hashtable;
				}
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to load web form: " + e.Message + "." , e );
			}
		}

		/// <summary>
		/// Render this page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Render this page.

			try
			{
				// Bind to our working set and save the cached listing.

				ViewState.Add( "MgrSet" , m_MgrSet );
				ViewState.Add( "LocSet" , m_LocSet );
				ViewState.Add( "LenSet" , m_LenSet );
				ViewState.Add( "UwSet" , m_UwSet );
				ViewState.Add( "JuniorUwSet" , m_JuniorUwSet );
				ViewState.Add( "ProcSet" , m_ProcSet );
				ViewState.Add( "JuniorProcSet" , m_JuniorProcSet );
				ViewState.Add( "SupSet" , m_SupSet );
				ViewState.Add( "Branch" , m_Branch );
				ViewState.Add( "PriceGroup" , m_PriceGroup );
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to render web form: " + e.Message + "." , e );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void UploadClick( object sender , System.EventArgs a )
		{
			// Bring in the specified text file and display it in our
			// editor.

			try
			{
				StreamReader sR = new StreamReader( m_File.PostedFile.InputStream );
				m_Employees.Text = sR.ReadToEnd();
			}
			catch( ArgumentException e )
			{
				ErrorMessage = e.Message;
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to upload: " + e.Message + "." , e );
			}
		}

		private bool isValueTrue(string val)
		{
			val = val.TrimWhitespaceAndBOM().ToLower();
			try
			{
				if(val.Equals("y") || val.Equals("yes") || val.Equals("true") || val.Equals("t"))
					return true;
				else if(Int32.Parse(val) == 1)
					return true;
				else
					return false;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void ImportClick( object sender , System.EventArgs a )
		{
			// Process specified employees.

			try
			{
				// Process each entry first, verifying that they are valid,
				// then import into the given broker.

				EmployeeImportSet      impSet = new EmployeeImportSet();
				BrokerEmployeeLoginSet logIns = new BrokerEmployeeLoginSet();
				ImportErrorSet         errSet = new ImportErrorSet();
                ImportErrorSet warningSet = new ImportErrorSet(); 

				Int32 nLine = -1;			// line number
				Int32 iFnam = -1;			// first name
                Int32 iMnam = -1;           // middle name
				Int32 iLnam = -1;			// last name
				Int32 iCnam = -1;			// company name
				Int32 iBran = -1;			// branch name
				Int32 iStrt = -1;			// street
				Int32 iCity = -1;			// city
				Int32 iStat = -1;			// state
				Int32 iZipc = -1;			// zip code
				Int32 iPhon = -1;			// phone #
				Int32 iFaxn = -1;			// fax #
				
				Int32 iCelln = -1;			// cell phone #
				Int32 iPagern = -1;			// pager # 
				Int32 iEmail = -1;			// email address
                Int32 iCreateWholesaleLoans = -1; // permission: Allow creating wholesale channel loans.
                Int32 iCreateMiniCorrLoans = -1;  // permission: Allow creating mini-correspondent channel loans.
                Int32 iCreateCorrLoans = -1;      // permission: Allow creating correspondent channel loans.
				Int32 iCanApply = -1;		// permission: can apply for ineligible loan programs
				Int32 iOpt1 = -1;			// permission: can run pricing engine without credit report
				Int32 iSubLoanWoCr = -1;	// permission: can submit loans without credit report
				Int32 iIsSup = -1;			// permission: is a supervisor
				Int32 iSup = -1;			// supervised by
				
				Int32 iLogn = -1;			// login name
				Int32 iPass = -1;			// password
				Int32 iChPwNextLogin = -1;	// must change password at next login
				Int32 iPwNeverExp = -1;		// password never expires
				Int32 iPwExpOn = -1;		// password expires on [date]
				Int32 iExpPwEvery = -1;		// expire passwords every [15,30,45,60] days

				Int32 iMngr = -1;			// manager
				Int32 iLock = -1;			// lock desk
				Int32 iLend = -1;			// account exec
				Int32 iUw = -1;				// underwriter
				Int32 iJuniorUw = -1;	    // junior underwriter
				Int32 iProc = -1;			// processor
				Int32 iJuniorProc = -1;		// junior processor
				Int32 iPriceG = -1;			// price group

                int iNmlsId = -1;           // index to NMLS id
                int iStateLicenses = -1;    // index to the state licenses

                int iSmsAuthEnabled = -1;    // Enable auth code transmission via SMS
                int iIsCellphonePrivate = -1; // Whether the cellphone is for SMS authentication only

				Int32 nCols = -1; 

				try
				{
					logIns.Retrieve( BrokerId );
				}
				catch{}

				foreach( String sLine in m_Employees.Text.Split( '\n' ) )
				{
					DelimitedListParser sData = new DelimitedListParser();

					if( sLine.TrimWhitespaceAndBOM().Trim(',').Equals("") )
					{
						++nLine;
						continue;
					}

					sData.Eat( sLine );

					if( nLine >= 0 && sData.Length != nCols )
					{
						if( sLine.Length > 6 )
						{
							if(sData.Length < nCols)
							{
								StringBuilder sBuf = new StringBuilder(sLine);
								for(int i = 0; i < nCols - sData.Length; ++i)
								{
									sBuf.Append(",");
								}
								sData.Eat( sBuf.ToString() );
								if(sData.Length != nCols)
								{
									errSet.Add( sLine.Substring( 0 , 6 ) + "..." , "Invalid column count" , nLine );
									++nLine;
									continue;
								}
							}
						}
						else
						{
							errSet.Add( sLine , "Invalid column count" , nLine + 1 );
							++nLine;
							continue;
						}
					}
					if( nLine++ >= 0 )
					{
						bool isValid = true;
						bool branchNotFound = false;
						StringBuilder invalidFormatFields = new StringBuilder();
						StringBuilder invalidFields = new StringBuilder();
						StringBuilder errorMessage = new StringBuilder();
                        string unparsedLicenses = null;

						EmployeeImportSet.Desc eD = new EmployeeImportSet.Desc();

						if( iFnam != -1 ) eD.FirstName        = sData[ iFnam ].TrimWhitespaceAndBOM();
                        if( iMnam != -1 ) eD.MiddleName       = sData[ iMnam ].TrimWhitespaceAndBOM();
						if( iLnam != -1 ) eD.LastName         = sData[ iLnam ].TrimWhitespaceAndBOM();
						if( iCnam != -1 ) eD.CompanyName      = sData[ iCnam ].TrimWhitespaceAndBOM();
						if( iStrt != -1 ) eD.Street           = sData[ iStrt ].TrimWhitespaceAndBOM();
						if( iCity != -1 ) eD.City             = sData[ iCity ].TrimWhitespaceAndBOM();
						if( iStat != -1 ) eD.State            = sData[ iStat ].TrimWhitespaceAndBOM().ToUpper();
						if( iZipc != -1 ) eD.Zipcode          = sData[ iZipc ].TrimWhitespaceAndBOM();
						if( iPhon != -1 ) eD.Phone            = sData[ iPhon ].TrimWhitespaceAndBOM();
						if( iFaxn != -1 ) eD.Fax              = sData[ iFaxn ].TrimWhitespaceAndBOM();
						if( iCelln != -1 ) eD.CellPhone		  = sData[ iCelln ].TrimWhitespaceAndBOM(); 
						if( iPagern != -1 ) eD.Pager		  = sData[ iPagern ].TrimWhitespaceAndBOM(); 
						if( iEmail != -1 ) eD.Email           = sData[ iEmail ].TrimWhitespaceAndBOM();
                        if (iCreateWholesaleLoans != -1) eD.CreateWholesaleChannelLoans = sData[iCreateWholesaleLoans].TrimWhitespaceAndBOM();
                        if (iCreateMiniCorrLoans != -1) eD.CreateMiniCorrespondentChannelLoans = sData[iCreateMiniCorrLoans].TrimWhitespaceAndBOM();
                        if (iCreateCorrLoans != -1) eD.CreateCorrespondentChannelLoans = sData[iCreateCorrLoans].TrimWhitespaceAndBOM();
						if( iCanApply != -1 ) eD.ApplyForIneligibleLoanPrograms	= sData[ iCanApply ].TrimWhitespaceAndBOM();
						if( iOpt1 != -1 ) eD.RunWithoutReport = sData[ iOpt1 ].TrimWhitespaceAndBOM();
						if( iLogn != -1 ) eD.Login            = sData[ iLogn ].TrimWhitespaceAndBOM();
						if( iPass != -1 ) eD.Password         = sData[ iPass ].TrimWhitespaceAndBOM();
                        if (iNmlsId != -1 && !string.IsNullOrEmpty(sData[iNmlsId].TrimWhitespaceAndBOM())) eD.NmlsIdentifier = sData[iNmlsId].TrimWhitespaceAndBOM();
                        if (iStateLicenses != -1) unparsedLicenses = sData[iStateLicenses].TrimWhitespaceAndBOM();
                        if (iSmsAuthEnabled != -1)
                        {
                            // Don't set this unless we for sure have a value in the csv.
                            eD.EnableAuthCodeViaSms = null;
                            var stringVal = sData[iSmsAuthEnabled].TrimWhitespaceAndBOM();
                            if (!string.IsNullOrEmpty(stringVal))
                            {
                                eD.EnableAuthCodeViaSms = this.isValueTrue(stringVal);
                            }
                        }
                        if (iIsCellphonePrivate != -1)
                        {
                            eD.IsCellphoneForMultiFactorOnly = null;
                            var stringVal = sData[iSmsAuthEnabled].TrimWhitespaceAndBOM();
                            if (!string.IsNullOrEmpty(stringVal))
                            {
                                eD.IsCellphoneForMultiFactorOnly = this.isValueTrue(stringVal);
                            }
                        }

                        if(!string.IsNullOrEmpty(unparsedLicenses))
                        {
                            LicenseInfoList licenses = new LicenseInfoList();
                            foreach(var unparsedLicense in unparsedLicenses.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries))
                            {
                                var licenseParts = (from part in unparsedLicense.Split(':') where !string.IsNullOrEmpty(part.TrimWhitespaceAndBOM()) select part.TrimWhitespaceAndBOM()).ToArray();
                                
                                if (licenseParts.Length != 3)
                                {
                                    errorMessage.Append((errorMessage.Length == 0 ? "" : ", ") + "License string [" + unparsedLicense + "] is invalid, must be in the format state:expiration:number");
                                    continue;
                                }

                                string state = licenseParts[0].ToUpper();
                                string expDate = licenseParts[1];
                                string licenseNum = licenseParts[2];

                                DateTime temp;
                                if (!DateTime.TryParse(expDate, out temp))
                                {
                                    errorMessage.Append((errorMessage.Length == 0 ? "" : ", ") + "Expiration date  [" + expDate + "] for license [" + unparsedLicense + "] doesn't look like a date, format must be state:expiration:number");
                                    continue;
                                }

                                //normalize the date - otherwise something like 1/1/12 will get interpreted as literally 1/1/0012,
                                //instead of 1/1/2012.
                                expDate = temp.ToShortDateString();

                                LicenseInfo li = new LicenseInfo(state, expDate, licenseNum);
                                licenses.Add(li);
                            }

                            eD.LicenseInformationList = licenses;
                        }
						
						// Order of precedence = order which these appear in PML editor as of 5/1/07
						if( iChPwNextLogin != -1 && isValueTrue(sData[ iChPwNextLogin ]))
						{
							eD.PasswordExpirationDate = SmallDateTime.MinValue;
							
							if( iExpPwEvery != -1 && (!sData[ iExpPwEvery ].TrimWhitespaceAndBOM().Equals("")))
							{
								bool valueValid = false;
								try
								{
									int expiresEvery = Int32.Parse(sData[ iExpPwEvery ].TrimWhitespaceAndBOM());
									if( (expiresEvery == 15) || (expiresEvery == 30) || (expiresEvery == 45) || (expiresEvery == 60))
									{
										eD.PasswordExpiresEvery = expiresEvery;
										valueValid = true;
									}
								}
								catch{}
								
								if(!valueValid)
									invalidFormatFields.Append((invalidFormatFields.Length == 0)?"ExpirePasswordsEvery" :", ExpirePasswordsEvery");
							}
						}
						else if( iPwNeverExp != -1 && isValueTrue(sData[ iPwNeverExp ]))
						{
							eD.PasswordExpirationDate = SmallDateTime.MaxValue; 
						}
						else if( iPwExpOn != -1 && (!sData[ iPwExpOn ].TrimWhitespaceAndBOM().Equals("")))
						{
							try
							{
								eD.PasswordExpirationDate = DateTime.Parse( sData[ iPwExpOn ] ); 
							}
							catch( System.FormatException )
							{
								isValid = false;
								invalidFormatFields.Append((invalidFormatFields.Length == 0)?"PasswordExpiresOn" :", PasswordExpiresOn");
							}

							if( iExpPwEvery != -1 && (!sData[ iExpPwEvery ].TrimWhitespaceAndBOM().Equals("")))
							{
								bool valueValid = false;
								try
								{
									int expiresEvery = Int32.Parse(sData[ iExpPwEvery ].TrimWhitespaceAndBOM());
									if( (expiresEvery == 15) || (expiresEvery == 30) || (expiresEvery == 45) || (expiresEvery == 60))
									{
										eD.PasswordExpiresEvery = expiresEvery; 
										valueValid = true;
									}
								}
								catch{}
								
								if(!valueValid)
									invalidFormatFields.Append((invalidFormatFields.Length == 0)?"ExpirePasswordsEvery" :", ExpirePasswordsEvery");
							}
						}
						else
						{
							// default to must change password at next login and check if they've entered
							// a value for passwords expires every [] days
							
							eD.PasswordExpirationDate = SmallDateTime.MinValue; 
							if( iExpPwEvery != -1 && (!sData[ iExpPwEvery ].TrimWhitespaceAndBOM().Equals("")))
							{
								bool valueValid = false;
								try
								{
									int expiresEvery = Int32.Parse(sData[ iExpPwEvery ].TrimWhitespaceAndBOM());
									if( (expiresEvery == 15) || (expiresEvery == 30) || (expiresEvery == 45) || (expiresEvery == 60))
									{
										eD.PasswordExpiresEvery = expiresEvery; 
										valueValid = true;
									}
								}
								catch{}
								
								if(!valueValid)
									invalidFormatFields.Append((invalidFormatFields.Length == 0)?"ExpirePasswordsEvery" :", ExpirePasswordsEvery");
							}
						}
						
						if(iSubLoanWoCr != -1)
						{
							if(isValueTrue(eD.RunWithoutReport))
								eD.SubmitWithoutReport = sData[ iSubLoanWoCr ].TrimWhitespaceAndBOM();
							else
								eD.SubmitWithoutReport = "n";
						}

						if( iCanApply != -1 ) eD.ApplyForIneligibleLoanPrograms = sData[ iCanApply ].TrimWhitespaceAndBOM();
						
						if( iIsSup != -1 && !(sData[ iIsSup ].TrimWhitespaceAndBOM().Equals("")))
						{
							eD.IsSupervisor    = isValueTrue(sData[ iIsSup ].TrimWhitespaceAndBOM()); 
						}
						
						if( iPriceG != -1 && !(sData[ iPriceG ].TrimWhitespaceAndBOM().Equals("")))
						{
							if( m_PriceGroup.Contains( sData[ iPriceG ].TrimWhitespaceAndBOM().ToLower() ) == true )
							{
								eD.PriceGroup = ( Guid ) m_PriceGroup[ sData[ iPriceG ].TrimWhitespaceAndBOM().ToLower() ];
							}
							else
							{
								isValid = false;
								invalidFields.Append((invalidFields.Length == 0)?"Price Group" :", Price Group");
							}
						}

						if( iBran != -1 && !(sData[ iBran ].TrimWhitespaceAndBOM().Equals("")) )
						{
							if( m_Branch.Contains( sData[ iBran ].TrimWhitespaceAndBOM().ToLower() ) == true )
							{
								eD.BranchId = ( Guid ) m_Branch[ sData[ iBran ].TrimWhitespaceAndBOM().ToLower() ];
							}
							else
							{
								isValid = false;
								branchNotFound = true;
								invalidFields.Append((invalidFields.Length == 0)?"Branch" :", Branch");
							}
						}

						if( iLend != -1 && !(sData[ iLend ].TrimWhitespaceAndBOM().Equals("")) )
						{
							if( m_LenSet.Contains( sData[ iLend ].TrimWhitespaceAndBOM().ToLower() ) == true )
							{
								eD.LenderAccountExecId = ( Guid ) m_LenSet[ sData[ iLend ].TrimWhitespaceAndBOM().ToLower() ];
							}
							else
							{
								isValid = false;
								invalidFields.Append((invalidFields.Length == 0)?"Lender AE" :", Lender AE");
							}
						}

						//only attempt to set a supervisor if this user is not a supervisor him/herself
						if( iSup != -1 && !(sData[ iSup ].TrimWhitespaceAndBOM().Equals("")) && !eD.IsSupervisor)
						{
							if( m_SupSet.Contains( sData[ iSup ].TrimWhitespaceAndBOM().ToLower() ) == true )
							{
								eD.SupervisorId = ( Guid ) m_SupSet[ sData[ iSup ].TrimWhitespaceAndBOM().ToLower() ];
							}
							else
							{
								isValid = false;
								invalidFields.Append((invalidFields.Length == 0)?"Supervisor" :", Supervisor");
							}
						}

						if( iUw != -1 && !(sData[ iUw ].TrimWhitespaceAndBOM().Equals("")) )
						{
							if( m_UwSet.Contains( sData[ iUw ].TrimWhitespaceAndBOM().ToLower() ) == true )
							{
								eD.UnderwriterId = ( Guid ) m_UwSet[ sData[ iUw ].TrimWhitespaceAndBOM().ToLower() ];
							}
							else
							{
								isValid = false;
								invalidFields.Append((invalidFields.Length == 0)?"Underwriter" :", Underwriter");
							}
						}

						if( iJuniorUw != -1 && !(sData[ iJuniorUw ].TrimWhitespaceAndBOM().Equals("")) )
						{
							if( m_JuniorUwSet.Contains( sData[ iJuniorUw ].TrimWhitespaceAndBOM().ToLower() ) == true )
							{
								eD.JuniorUnderwriterId = ( Guid ) m_JuniorUwSet[ sData[ iJuniorUw ].TrimWhitespaceAndBOM().ToLower() ];
							}
							else
							{
								isValid = false;
								invalidFields.Append((invalidFields.Length == 0)?"Junior Underwriter" :", Junior Underwriter");
							}
						}

						if( iProc != -1 && !(sData[ iProc ].TrimWhitespaceAndBOM().Equals("")) )
						{
							if( m_ProcSet.Contains( sData[ iProc ].TrimWhitespaceAndBOM().ToLower() ) == true )
							{
								eD.ProcessorId = ( Guid ) m_ProcSet[ sData[ iProc ].TrimWhitespaceAndBOM().ToLower() ];
							}
							else
							{
								isValid = false;
								invalidFields.Append((invalidFields.Length == 0)?"Processor" :", Processor");
							}
						}

						if( iJuniorProc != -1 && !(sData[ iJuniorProc ].TrimWhitespaceAndBOM().Equals("")) )
						{
							if( m_JuniorProcSet.Contains( sData[ iJuniorProc ].TrimWhitespaceAndBOM().ToLower() ) == true )
							{
								eD.JuniorProcessorId = ( Guid ) m_JuniorProcSet[ sData[ iJuniorProc ].TrimWhitespaceAndBOM().ToLower() ];
							}
							else
							{
								isValid = false;
								invalidFields.Append((invalidFields.Length == 0)?"Junior Processor" :", Junior Processor");
							}
						}

						if( iLock != -1 && !(sData[ iLock ].TrimWhitespaceAndBOM().Equals("")) )
						{
							if( m_LocSet.Contains( sData[ iLock ].TrimWhitespaceAndBOM().ToLower() ) == true )
							{
								eD.LockDeskId = ( Guid ) m_LocSet[ sData[ iLock ].TrimWhitespaceAndBOM().ToLower() ];
							}
							else
							{
								isValid = false;
								invalidFields.Append((invalidFields.Length == 0)?"Lock Desk" :", Lock Desk");
							}
						}

						if( iMngr != -1 && !(sData[ iMngr ].TrimWhitespaceAndBOM().Equals("")) )
						{
							if( m_MgrSet.Contains( sData[ iMngr ].TrimWhitespaceAndBOM().ToLower() ) == true )
							{
								eD.ManagerId = ( Guid ) m_MgrSet[ sData[ iMngr ].TrimWhitespaceAndBOM().ToLower() ];
							}
							else
							{
								isValid = false;
								invalidFields.Append((invalidFields.Length == 0)?"Manager" :", Manager");
							}
						}
						if(!isValid && !invalidFields.ToString().TrimWhitespaceAndBOM().Equals(""))
						{
							errorMessage.Append((errorMessage.Length == 0)?"Not Found: [ " + invalidFields.ToString() + " ]":", Not Found: [ " + invalidFields.ToString() + " ]");
						}

						eD.UserType = (m_IsP.Checked)?"P":"B";

						eD.RoleId = CEmployeeFields.s_LoanRepRoleId.ToString();

						try
						{
							if( eD.FirstName != "" && eD.LastName != "" && eD.Phone != "" && eD.Email != "" && eD.CompanyName != "" && eD.Login != "" && eD.Password != "" && eD.BranchId != Guid.Empty )
							{
								if( logIns.IsValid( eD.Login , eD.UserType ) == true )
								{
									errorMessage.Append((errorMessage.Length == 0)?"Login already taken" :", Login already taken");
									isValid = false;
								}
							}
							else
							{
								StringBuilder missingKeyDataFields = new StringBuilder();
								if(eD.FirstName == "")
								{
									missingKeyDataFields.Append(", FirstName"); 
								}
								if(eD.LastName == "")
								{
									missingKeyDataFields.Append(", LastName"); 
								}
								if(eD.Phone == "")
								{
									missingKeyDataFields.Append(", Phone"); 
								}
								if(eD.Email == "")
								{
									missingKeyDataFields.Append(", Email"); 
								}
								if(eD.CompanyName == "")
								{
									missingKeyDataFields.Append(", Company"); 
								}
								if(eD.Login == "")
								{
									missingKeyDataFields.Append(", Login"); 
								}
								if(eD.Password == "")
								{
									missingKeyDataFields.Append(", Password"); 
								}
								if(eD.BranchId == Guid.Empty && (branchNotFound == false))
								{
									missingKeyDataFields.Append(", Branch"); 
								}

								if(missingKeyDataFields.Length > 0)
								{
									errorMessage.Append((errorMessage.Length == 0)?"Missing required field(s) [ " + missingKeyDataFields.ToString().TrimStart(',') + " ]":", Missing required field(s) [ " + missingKeyDataFields.ToString().TrimStart(',') + " ]");
									isValid = false;
								}
							}

                            //OPM 32417 Modify batch import "P" users process to use PML Originating Company av 07 14 2009
                            if (eD.UserType[0] == 'P')
                            {
                                PmlBroker broker;
                                if (CurrentPmlBrokers.TryGetValue(eD.CompanyName.TrimWhitespaceAndBOM(), out broker))
                                {
                                    eD.PmlBrokerId = broker.PmlBrokerId;
                                }
                                else
                                {
                                    if (errorMessage.Length != 0) { errorMessage.Append(" "); }
                                    errorMessage.Append("Company does not exist.");
                                    isValid = false;
                                }
                                if (m_DuplicateCompanies.Contains(eD.CompanyName.TrimWhitespaceAndBOM().ToLower()))
                                {
                                    m_VerifyCompanyUserNames.Add( eD.Login );
                                    warningSet.Add(eD.Login, "Warning : Duplicate company name ", nLine +1 );
                                }
                            }
							//if e-mail field is blank, it will get caught as a missing required field
							if(!eD.Email.TrimWhitespaceAndBOM().Equals("") && !CommonLib.SafeString.IsValidEmail(eD.Email))
							{
								invalidFormatFields.Append((invalidFormatFields.Length == 0)?"Email" :", Email");
								isValid = false;
							}
							
							if(invalidFormatFields.Length > 0)
							{
								errorMessage.Append((errorMessage.Length == 0)?"Invalid Format: [ " + invalidFormatFields.ToString().TrimStart(',') + " ]":", Invalid Format: [ " + invalidFormatFields.ToString().TrimStart(',') + " ]");
								isValid = false;
							}
						}
						catch( ArgumentException e )
						{
							errorMessage = new StringBuilder();
							errorMessage.Append(e.Message.TrimEnd( '.' ));
						}
						catch
						{
							errorMessage = new StringBuilder();
							errorMessage.Append("Failed to add to working set");
						}

						if(isValid)
						{
							try
							{
								eD.LineNumber = nLine + 1;
								impSet.Add( eD );
							}
							catch( ArgumentException e )
							{
								errSet.Add
									( eD.Login
									, e.Message
									, nLine + 1
									);
							}
						}
						else
						{
							errSet.Add
								( eD.Login
								, errorMessage.ToString()
								, nLine + 1
								);
						}
					}
					else
					{
						// Consider this row to be the column mapping row.
						// We gather the columns by position and then cache
						// the placement.  All columns not defined are left
						// in their uninitialized position.

						for( int i = 0 ; i < sData.Length ; ++nCols , ++i )
						{
							switch( sData[ i ].TrimWhitespaceAndBOM().ToLower() )
							{
								case "firstname":			 iFnam = i;
								break;
                                case "middlename":
                                    iMnam = i;
                                    break;
								case "lastname":			iLnam = i;
								break;

								case "companyname":			 iCnam = i;
								break;

								case "branch":				iBran = i;	
								break;
								
								case "pricegroup":			iPriceG = i;
								break;
								
								case "street":				iStrt = i;
								break;

								case "city":				 iCity = i;
								break;

								case "state":				iStat = i;
								break;

								case "zipcode":				iZipc = i;
								break;

								case "phone":				iPhon = i;
								break;

								case "fax":					iFaxn = i;
								break;

								case "cellphone":			iCelln = i;
								break;
                                case "enableauthcodeviasms":
                                    iSmsAuthEnabled = i;
                                    break;
                                case "iscellphoneprivate":
                                    iIsCellphonePrivate = i;
                                    break;
                                case "pager":				iPagern = i;
								break;
							
								case "email":				iEmail = i;
								break;

                                case "createwholesaleloans": iCreateWholesaleLoans = i;
                                break;

                                case "createminicorrloans": iCreateMiniCorrLoans = i;
                                break;

                                case "createcorrloans": iCreateCorrLoans = i;
                                break;
                                
                                case "applyforineligiblelp": iCanApply = i;
								break;

								case "runwithoutreport":	iOpt1 = i;
								break;
								
								case "submitwithoutreport":	iSubLoanWoCr = i;	
								break;

								case "issupervisor":		iIsSup = i;
								break;
								
								case "supervisorname":		iSup = i;
								break;
								
								case "login":				iLogn = i;
								break;

								case "password":			iPass = i;
								break;

								case "mustchangepasswordnextlogin":	iChPwNextLogin  = i;
								break;

								case "passwordneverexpires":	iPwNeverExp  = i;
								break;

								case "passwordexpireson":	iPwExpOn  = i;
								break;

								case "expirepasswordsevery":	iExpPwEvery  = i;
								break;

								case "manager":           iMngr = i;
								break;

								case "lockdesk":          iLock = i;
								break;

								case "lenderaccountexec": iLend = i;
								break;

								case "underwriter":			iUw = i;
								break;
								
								case "processor":			iProc = i;
								break;

                                case "nmlsidentifier": iNmlsId = i;
                                break;

                                case "statelicenses": iStateLicenses = i;
                                break;

								case "juniorunderwriter": iJuniorUw = i;
								break;

								case "juniorprocessor": iJuniorProc = i;
								break;
							}
						}

						if( iFnam == -1 || iLnam == -1 || iCnam == -1 || iBran == -1 || iEmail == -1 || iLogn == -1 || iPass == -1 )
						{
							throw new ArgumentException( "Required columns not specified.  Nothing loaded." );
						}

						++nCols;
					}
				}

				if( impSet.Count == 0 && errSet.Count == 0 )
				{
					throw new ArgumentException( "No employees specified.  Nothing to do." );
				}

				// 6/8/2005 kb - Latch in the specified employees and
				// bind them to this broker.

				Int32 nImported = 0;
				
				impSet.Save
					( BrokerId
					, CurrentPrincipal.UserId
                    , CurrentPrincipal
                    );

				foreach( EmployeeImportSet.Desc eD in impSet )
				{
					if( eD.IsSaved == true )
					{
						++nImported;
					}
					else
					{
						errSet.Add
							( eD.Login
							, "Unable to save"
							, eD.LineNumber
							);
					}
				}

				if( nImported > 1 || nImported == 0 )
				{
					FeedBack = String.Format
						( "Imported {0} employees."
						, nImported
						);
				}
				else
				{
					FeedBack = String.Format
						( "Imported {0} employee."
						, nImported
						);
				}

    

				if( errSet.Count > 0 )
				{
					m_Errors.DataSource = errSet;
					m_Errors.DataBind();

					m_ErrorList.Visible = true;
				}
				else
				{
					m_ErrorList.Visible = false;
				}

                if (warningSet.Count > 0)
                {
                    m_WarningList.DataSource = warningSet;
                    m_WarningList.DataBind();

                    m_Warning.Visible = true;
                }
                else
                {
                    m_Warning.Visible = false;
                }
			}
			catch( ArgumentException e )
			{
				ErrorMessage = e.Message;
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to import employees: " + e.Message + "." , e );
			}
		}
	}

	/// <summary>
	/// Track individual errors during import operations.
	/// </summary>

	public class ImportErrorSet : IEnumerable
	{
		/// <summary>
		/// Track individual errors.
		/// </summary>

		private class Desc
		{
			/// <summary>
			/// Track individual errors.
			/// </summary>
			
			private string m_Reason = String.Empty;
			private string  m_Login = String.Empty;
			private int  m_LineNo = 0;

			#region ( Descriptor properties )

			public string Reason
			{
				set { m_Reason = value; }
				get { return m_Reason; }
			}

			public string Login
			{
				set { m_Login = value; }
				get { return m_Login; }
			}

			public int LineNo
			{
				set { m_LineNo = value; }
				get { return m_LineNo; }
			}

			#endregion

		}

		/// <summary>
		/// Track individual errors.
		/// </summary>

		private ArrayList m_Set = new ArrayList();

		public int Count
		{
			get { return m_Set.Count; }
		}

		/// <summary>
		/// Add a new error to the record.
		/// </summary>

		public void Add( String sLogin , String sReason , Int32 lineNo )
		{
			// Add a new error to the record.

			Desc eD = new Desc();

			eD.Reason = sReason;
			eD.Login  = sLogin;
			eD.LineNo = lineNo;

			m_Set.Add( eD );
		}

		/// <summary>
		/// Get the looping interface for reporting recorded errors.
		/// </summary>

		public IEnumerator GetEnumerator()
		{
			// Access looping interface for our set.

			return m_Set.GetEnumerator();
		}
	}
}
