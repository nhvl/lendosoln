﻿namespace LendersOfficeApp.LOAdmin.Broker
{
    using LendersOffice.Security;
    using System;

    public partial class CustomFieldXMLEdit : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Gets the required permissions to access this page.
        /// </summary>
        /// <returns>The permissions associated with this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.ViewBrokers
            };
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            EnableJquery = true;
            base.OnInit(e);
        }
    }
}
