<%@ Page language="c#" Codebehind="LicenseEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Broker.LicenseEdit" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LicenseEdit</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript">
    <!--
    function _init() {
      resize(500, 250);
    }
    //-->
		</script>
	</HEAD>
	<h4 class="page-header">Edit License</h4>
	<body MS_POSITIONING="FlowLayout" onload="_init();">
		<form id="LicenseEdit" method="post" runat="server">
			<table cellSpacing="2" cellPadding="3" width="100%" border="0">
				<tr>
					<td>
			<table width="100%">
				<tr>
					<td class="fieldlabel">Description</td>
					<td>
						<asp:TextBox id="m_description" runat="server" Width="400px" MaxLength="50"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="fieldlabel">Seat Count</td>
					<td>
						<asp:TextBox id="m_seatCount" runat="server" Width="50px" MaxLength="5"></asp:TextBox></td>
				</tr>
				<tr>
					<td class="fieldlabel">Expiration Date</td>
					<td>
						<ml:DateTextBox id="m_expDate" runat="server" width="75" preset="date"></ml:DateTextBox></td>
				</tr>
				<tr>
					<td colspan="2" align="right">
						<ml:NoDoubleClickButton id="m_submit" runat="server" Text="Submit" onclick="m_submit_Click"></ml:NoDoubleClickButton><input type="button" onclick="onClosePopup();" value="Cancel"></td>
				</tr>
			</table></td></tr></table>
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
			<P></P>
		</form>
	</body>
</HTML>
