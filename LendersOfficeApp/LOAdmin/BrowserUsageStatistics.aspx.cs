﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;

namespace LendersOfficeApp.LOAdmin
{
    public partial class BrowserUsageStatistics : SecuredAdminPage
    {
        [WebMethod]
        public static object Get()
        {
            int totalInactive;
            int totalActive;
            int totalNotLoggedInSinceLogging;

            var userAgents = GetUserAgents(out totalInactive, out totalActive, out totalNotLoggedInSinceLogging);

            var versionCount = new Dictionary<string, int>();
            var osCount = new Dictionary<string, int>();

            foreach (var userAgent in userAgents)
            {
                LogVersionFromUserAgent(userAgent, versionCount);
                LogOSFromUserAgent(userAgent, osCount);
            }

            var versionUsage = versionCount
                .Select(pair => new BrowserUsageItem
                {
                    Version = pair.Key,
                    Users = pair.Value,
                    Percent = pair.Value * 100d / totalActive
                })
                .OrderBy(b => b.Version)
                .ToArray();
            var osUsage = osCount.Select(pair => new
                {
                    os = pair.Key,
                    userCount = pair.Value,
                })
                .ToArray();

            return new
            {
                totalInactive,
                totalActive,
                versionUsage,
                osUsage,
            };
        }

        static IEnumerable<string> GetUserAgents(out int totalInactive, out int totalActive, out int totalNotLoggedInSinceLogging)
        {
            totalInactive = 0;
            totalActive = 0;
            totalNotLoggedInSinceLogging = 0;

            var userAgents = new List<string>();
            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetLastUserAgentStringAndIsActive", null))
                {
                    while (reader.Read())
                    {
                        var isActive = (bool) reader["IsActive"];
                        var ua = reader["LastUserAgentString"] as string;

                        if (!isActive) { totalInactive++; continue; }

                        //If they're a legacy user that hasn't logged in since we added UA logging,
                        //just skip them.
                        if (string.IsNullOrEmpty(ua)) { totalNotLoggedInSinceLogging++; continue; }

                        totalActive++;
                        userAgents.Add(ua);
                    }
                }
            }
            return userAgents;
        }

        static void LogOSFromUserAgent(string userAgent, IDictionary<string, int> osDictionary)
        {
            userAgent = userAgent.ToLower();
            var fp = oslist.FirstOrDefault(p => userAgent.Contains(p[0].ToLower()));
            var os = fp == null ? unknown : fp[1];

            if (osDictionary.ContainsKey(os)) { osDictionary[os] += 1; }
            else { osDictionary.Add(os, 1); }
        }

        static void LogVersionFromUserAgent(string userAgent, IDictionary<string, int> versionCounter)
        {
            var version = ParseVersionInUserAgent(userAgent);

            if (versionCounter.ContainsKey(version)) versionCounter[version]++;
            else versionCounter.Add(version, 1);
        }

        static string ParseVersionInUserAgent(string UserAgent)
        {
            string version;
            try
            {
                // The 5 below refers to spaces to the start of version number in user agent from indexof MSIE
                int startIndex = UserAgent.IndexOf("MSIE") + 5;
                int endIndex = UserAgent.IndexOf(';', startIndex);

                if(endIndex == -1)
                    endIndex = UserAgent.IndexOf(')', startIndex);

                version = UserAgent.Contains("Trident/7") ?
                    "11 (CompatMode - IE7)" :
                    UserAgent.Substring(startIndex, endIndex - startIndex);
            }
            catch
            {
                return "Unknown";
            }
            return version;
        }

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.BrowserUsageStatistics.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            RegisterResources();
        }

        const string unknown = "Not Known Windows";

        static readonly string[][] oslist = {
                new [] { "Win16"         , "Windows 3.11 "},
                new [] { "Windows 95"    , "Windows 95"},
                new [] { "Win95"         , "Windows 95"},
                new [] { "Windows_95"    , "Windows 95"},
                new [] { "Windows 98"    , "Windows 98"},
                new [] { "Win98"         , "Windows 98"},
                new [] { "Windows NT 5.0", "Windows 2000"},
                new [] { "Windows 2000"  , "Windows 2000"},
                new [] { "Windows NT 5.1", "Windows XP"},
                new [] { "Windows XP"    , "Windows XP"},
                new [] { "Windows NT 5.2", "Windows Server 2003"},
                new [] { "Windows NT 6.0", "Windows Vista"},
                new [] { "Windows NT 6.1", "Windows 7"},
                new [] { "Windows NT 6.2", "Windows 8"},
                new [] { "Windows NT 4.0", "Windows NT 4.0"},
                new [] { "WinNT4.0"      , "Windows NT 4.0"},
                new [] { "WinNT"         , "Windows NT 4.0"},
                new [] { "Windows NT"    , "Windows NT 4.0"},
                new [] { "Windows ME"    , "Windows ME"}
            };
    }

    class BrowserUsageItem
    {
        private double percent;

        public string Version { get; set; }
        public int Users { get; set; }
        public double Percent
        {
            get
            {
                return percent;
            }
            set
            {
                percent = Convert.ToDouble(value.ToString("N2"));
            }
        }
    }
}
