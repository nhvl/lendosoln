﻿<%@ Page Language="C#" CodeBehind="~/LOAdmin/CPortal/FindConsumers.aspx.cs" MasterPageFile="~/LOAdmin/loadmin.Master" Inherits="LendersOfficeApp.LOAdmin.CPortal.FindConsumer" Title="Find Consumers"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
  <div class="row">
    <div class="col-xs-10 col-xs-offset-1">
      <div class="panel panel-default">
        <div class="panel-body">
          <form id="stageForm" runat="server"> <asp:Panel ID="p" runat="server" DefaultButton="Button1">
            <div class="row">
            <div class="col-xs-3  col-xs-offset-2">
               <div class="form-group ">
                    <label class="control-label">E-mail:</label>
                    <div class="text-right-noerror">
                      <span id="error" class="text-danger"/>
                  </div>
                  <asp:TextBox  AutoComplete="off"  class="form-control" ID="m_EmailAddr" runat="server" ></asp:TextBox>
                    <script> $("input[id*='Email'").focus()</script>
                </div>
            </div>
            <div class="col-xs-3 ">
              <div class="form-group ">
                    <label class="control-label">Broker:</label>
                    <div class="text-right-noerror">
                      <span id="Span1" class="text-danger"/>
                  </div>
                  <asp:TextBox  AutoComplete="off"   class="form-control" ID="m_BrokerName" runat="server" ></asp:TextBox>
                </div>
            </div>
            <div class="col-xs-2">
            <div class="form-group ">
                    <label class="control-label">Status:</label>
                  <asp:DropDownList  class="form-control" ID="m_Status"  runat="server">
                    <asp:ListItem Value="0">Any</asp:ListItem>
                    <asp:ListItem Value="1">Active</asp:ListItem>
                    <asp:ListItem Value="2">Locked</asp:ListItem>
                  </asp:DropDownList>
                  
                </div>
          </div>
          </div>
          <div class="row">
            <div class="col-xs-offset-6 col-xs-4 text-right">
             <span  class="clipper">
                <asp:Button  ID="CreateBtn" class="btn btn-default" Text="Reset"  onclick="ReloadClick"  runat="server"/>
              </span >
              <span   class="clipper">
                <asp:Button   ID="Button1" class="btn btn-primary" Text="Search"  onclick="SearchClick"  runat="server"/>
              </span >
            </div>
          </div>
              <br />
            <asp:panel id="m_consumerInfo" Runat="server">
            <div id="loansTable">
                <script id='template' type='text/ractive'> 
                  <table  class="table table-striped table-condensed">
                        <%--show header--%>
                    <thead>
                      <tr>
                        <th class="view"></th>
                        {{#each headers}}
                        <th class={{cssclass}}>
                            <a  on-click="sortBy:{{key}}">
                                {{value}}
          	                        {{#if sortByKey == key}}
          	                            {{#if sortAsc}}
                                            <span class="glyphicon glyphicon-sort-by-attributes"></span>
                                        {{else}}
                                            <span class="glyphicon glyphicon glyphicon-sort-by-attributes-alt"></span>
                                        {{/if}}
                                    {{/if}}
                            </a>
                        </th>
                        {{/each}}
                       </tr>
                    </thead>
                    <tbody>
                    {{#each loans}}
                      <tr>
                        <td class="text-center" ><a class="btn btn-link" href="ViewConsumer.aspx?consumerid={{ConsumerId}}&brokerid={{BrokerId}}">view</td>
                        <td >{{ConsumerId}}</td>
                        <td>{{Email}}</td>
                        <td><a target="_blank" href="../Broker/BrokerEdit.aspx?cmd=edit&brokerid={{BrokerId}}">{{BrokerNm}}</a></td>
                        <td>{{LastLoggedInString}}</td>
                        <td class="text-center" >{{ IsLocked}}</td>
                      </tr>
                     {{/each}}
                  </tbody>
                </table>
                {{#if loans.length == 0}}
                 <p class="text-center">0 Result Found.</p>
                {{/if}}
              </script>  
              </div>
            <asp:HiddenField id="consumerDataKey" runat="server"/>
             <script>
                 data = JSON.parse($("[id*=consumerDataKey").val());
                var ractive = new Ractive({
                  el: '#loansTable',
                  template: '#template',
                  data: {
                    
                    sortByKey: '',
                    sortAsc: true,
                    loans: data,
                      
                    headers: [
                      {value: "ConsumerID", key: "ConsumerId", cssclass : ""},
                      {value: "Email", key: "Email", cssclass : ""},
                      {value: "Broker", key: "BrokerNm", cssclass : ""},
                      {value: "LastLogin", key: "LastLoggedInString", cssclass : "text-right"},
                      {value: "Is Locked?", key: "IsLocked", cssclass : "text-center"}
                    ]
                  }
                });

                ractive.on('sortBy', function (event, key) {
                  var lastKey = ractive.get("sortByKey");
                  var isAsc = ractive.get("sortAsc");
                  if (key === lastKey) {
                    isAsc = !isAsc;
                  } else {
                    isAsc = true;
                    lastKey = key;
                  }

                  var ls = data.slice().sort(function (x, y) {
                    
                    if (key == "ConsumerId" )
                    {
                        var u = parseInt(x[key],10);
                        var v = parseInt(y[key],10);
                    }
                    else if (key == "LastLoggedInString" )
                    {
                        var u =  (x["LastLoggedInD"]);
                        var v =  (y["LastLoggedInD"]);
                    }
                    else if (key == "IsLocked" )
                    {
                        var u = x[key];
                        var v = y[key];
                    }
                    else{
                        var u = x[key].toLowerCase();
                        var v = y[key].toLowerCase();
                    }
                    if (u.length == 0){ 
                        return isAsc?-1:1;
                    }
                    else
                    if (v.length == 0){ 
                        return isAsc?1:-1;
                    }
                    else{
                        var x_Minus_y = ((u > v) ? 1 : ((u < v) ? -1 : 0));
                        return (isAsc ? x_Minus_y : (-x_Minus_y));
                    }
                  });
                  ractive.set({sortByKey: lastKey, sortAsc: isAsc, loans: ls});
                });
        </script>
            </asp:panel>
          </asp:Panel> </form>
        </div>
      </div>
    </div>
  </div>
</asp:Content>