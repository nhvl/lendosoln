﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data.SqlClient;
using System.Security.Cryptography;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.CPortal
{
    public partial class CreateConsumer : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.BecomeUser
                //E_InternalUserPermissions.ConsumerPortal

            };
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private void SetErrorMsg(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                ErrorMsg.Text = "";
                return;
            }

            ErrorMsg.Visible = true;
            ErrorMsg.Text = msg;
            SuccessMsg.Text = "";
        }
        
        private void SetSuccessMsg(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                SuccessMsg.Text = "";
                return;
            }

            SuccessMsg.Visible = true;
            SuccessMsg.Text = msg;
            ErrorMsg.Text = "";
        }


        protected void CreateBtn_OnClick(object sender, System.EventArgs a)
        {
            Guid broker = Guid.Empty;

            if (TempPass.Text.Length < 6)
            {
                SetErrorMsg("Consumer password needs to be at least 6 characters long.");
                return;
            }

            if (!Email.Text.Contains("@") || !Email.Text.Contains("."))
            {
                SetErrorMsg("Invalid e-mail address.");
                return;
            }

            //Current HashMethod is:
            SHA256 sha1 = new SHA256Managed();
            string passwordHash = Convert.ToBase64String(sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(TempPass.Text)));

            try{
                broker = new Guid(BrokerId.Text);
                SqlParameter[] parameters = {   new SqlParameter("@Email", Email.Text),
                                                new SqlParameter("@BrokerId", broker.ToString()),
                                                new SqlParameter("@PasswordHash", passwordHash),
                                                new SqlParameter("@IsLocked", IsLocked.Checked)
                                            };
                Int64 ConsumerId = StoredProcedureHelper.ExecuteNonQuery(broker, "CP_CreateConsumerId", 1, parameters);
                SetSuccessMsg("User " + Email.Text + " created with consumerId " + ConsumerId + ".");

                Email.Text = "";
                BrokerId.Text = "";
                TempPass.Text = "";
                IsLocked.Checked = false;
            }
            catch(Exception e)
            {
                SetErrorMsg(e.Message);
                Tools.LogError(e);
            }
        }
    }
}
