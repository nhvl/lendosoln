﻿<%@ Page Language="C#" CodeBehind="FindLoansConsumer.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.CPortal.FindLoansConsumer"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Find Loans By Consumer" %>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style type="text/css">
        .center-div
        {
            margin: 0 auto;
            width: 80%;
        }
        .padding-div
        {
            padding-top: 20px;
            padding-bottom: 10px;
        }
         .LoanPanelTop
        {
            padding-top: 10px;
        }
        .min-width-table
        {
        	min-width:120px;
        }
    </style>
    <div class="container-fluid"><div class="row"><div class="center-div">
        <div class="alert alert-error alert-dismissible " role="alert" id="ErrorPanel" runat="server">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <strong>Error: </strong>
                        <ml:EncodedLabel ID="ErrorMsg" runat="server"></ml:EncodedLabel>
                    </div>
        <div class="panel panel-default"><div class="panel-body">
    <form id="form1" runat="server">
        <div id="QueryPanel" runat="server">
        <div class="row">
            <div class="col-xs-6">
                <label>Broker ID:</label><asp:TextBox ID="m_BrokerId" CssClass="form-control input" runat="server" Columns="37" MaxLength="40" AutoComplete="off"/>
                <asp:requiredfieldvalidator id="Requiredfieldvalidator1" Runat="server" ControlToValidate="m_BrokerId" Display="Dynamic" ErrorMessage="* Must provide Broker ID"></asp:requiredfieldvalidator>
            </div>
            <div class="col-xs-6">
                <label>Consumer ID:</label>
                <asp:textbox id="m_ConsumerId" Runat="server" CssClass="form-control input" columns="37" MaxLength="36" AutoComplete="off"></asp:textbox>
                <asp:requiredfieldvalidator id="loanIdReq" Runat="server" ControlToValidate="m_ConsumerId" Display="Dynamic" ErrorMessage="* Must provide Consumer ID"></asp:requiredfieldvalidator>
            </div>
        </div>
        <div class="form-group text-right padding-div">
                            <span class="clipper"><asp:button id="m_search" onclick="SearchClick" Runat="server" Text="Search" CssClass="btn btn-primary"></asp:button></span>
        </div>
            </div>
    			<div id="LoansPanel" runat="server" class="LoanPanelTop">
                                <asp:Repeater ID="LoansGrid" runat="server" OnItemDataBound="LoansGrid_ItemBound">
                                    <HeaderTemplate>
                                        <table class="table table-striped table-condensed">
                                            <thead>
                                                <tr>
                                                    <th>
                                                    <asp:LinkButton runat="server" ID="sLNm" OnClick="SortClick" Text="Loan Number">
                                                    </asp:LinkButton>
                                                    <span id="sLNmSpan" runat="server" class="" aria-hidden="true"></span>    
                                                    </th>
                                                    <th class="min-width-table">
                                                        <asp:LinkButton runat="server" ID="BrokerNm" OnClick="SortClick" Text="Broker Name"></asp:LinkButton>
                                                        <span id="BrokerNmSpan" runat="server" class="" aria-hidden="true"></span>
                                                    </th>
                                                    <th class="min-width-table">
                                                        <asp:LinkButton runat="server" ID="sPrimBorrowerFullNm" OnClick="SortClick" Text="Borrower Name"></asp:LinkButton>
                                                        <span id="sPrimBorrowerFullNmSpan" runat="server" class="" aria-hidden="true"></span>
                                                    </th>
                                                    <th class="min-width-table">
                                                        <asp:LinkButton runat="server" ID="BranchNm" OnClick="SortClick" Text="Branch Name"></asp:LinkButton>
                                                        <span id="BranchNmSpan" runat="server" class="" aria-hidden="true"></span>
                                                    </th>
                                                    <th>
                                                        <asp:LinkButton runat="server" ID="sSpFullAddr" OnClick="SortClick" Text="Property Address"></asp:LinkButton>
                                                        <span id="sSpFullAddrSpan" runat="server" class="" aria-hidden="true"></span>
                                                    </th>
                                                    <th class="min-width-table">
                                                        <asp:LinkButton runat="server" ID="LoanStatus" OnClick="SortClick" Text="Loan Status"></asp:LinkButton>
                                                        <span id="LoanStatusSpan" runat="server" class="" aria-hidden="true"></span>
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <ml:EncodedLabel ID="m_LoanNumber" runat="server"></ml:EncodedLabel>
                                            </td>
                                            <td>
                                                <ml:EncodedLabel ID="m_BrokerName" runat="server"></ml:EncodedLabel>
                                            </td>
                                            <td>
                                                <ml:EncodedLabel ID="m_BorrowerName" runat="server"></ml:EncodedLabel>
                                            </td>
                                            <td>
                                                <ml:EncodedLabel ID="m_BranchName" runat="server"></ml:EncodedLabel>
                                            </td>
                                            <td>
                                                <ml:EncodedLabel ID="m_PropertyAddress" runat="server"></ml:EncodedLabel>
                                            </td>
                                            <td>
                                                <ml:EncodedLabel ID="m_LoanStatus" runat="server"></ml:EncodedLabel>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
    </form>       
    </div>
    
    <script type="text/javascript">
        <!--
            function onInit() { try
            {
                if( window.dialogArguments != null )
			        {
				        <% if( IsPostBack == false ) { %>
				        resize( 1050 , 300 );        				
				        <% } %>
			        }
            }
            catch( e )
            {
                window.status = e.message;
            }}
        
        //-->
    </script>
        </div></div></div></div>
</asp:Content>