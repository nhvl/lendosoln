﻿<%@ Page Language="C#" CodeBehind="~/LOAdmin/CPortal/ViewConsumer.aspx.cs" MasterPageFile="~/LOAdmin/loadmin.Master" Inherits="LendersOfficeApp.LOAdmin.CPortal.ViewConsumer" Title="Find Consumers"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">

  <div class="row " >
    
    <div class="col-xs-10 col-xs-offset-1">
      <form runat="server">
       <div id="content" runat="server">
           <div ID="responseContainer" role="alert" runat="server" class="alert alert-dismissible" Visible="false">
               <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <strong><ml:EncodedLiteral ID="responseSubject" runat="server" /></strong>
               <ml:EncodedLiteral ID="responseMsg" runat="server" ></ml:EncodedLiteral>
           </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-xs-10 col-xs-offset-1"><div class="col-xs-6 col-xs-offset-3">
                <table  class="table table-striped table-condensed">
                    <tbody>
                      <tr>
                        <td class="col-md-6">Consumer id:</td>
                        <td class="col-md-6" colspan="2" ><ml:EncodedLabel ID="Label0" runat="server"></ml:EncodedLabel></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td class="col-md-6">Email:</td>
                        <td class="col-md-6" colspan="2"><ml:EncodedLabel ID="Label1" runat="server"></ml:EncodedLabel></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td class="col-md-6">Broker Name:</td>
                        <td class="col-md-6" colspan="2"><ml:EncodedLabel ID="Label2" runat="server"></ml:EncodedLabel></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td class="col-md-6">Last Logged In:</td>
                        <td class="col-md-6" colspan="2"><ml:EncodedLabel ID="Label3" runat="server"></ml:EncodedLabel></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td class="col-md-6">Password Reset Code:</td>
                        <td class="col-md-6" colspan="2"><ml:EncodedLabel ID="Label4" runat="server"></ml:EncodedLabel></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td class="col-md-6">Password Reset Date:</td>
                        <td class="col-md-6" colspan="2"><ml:EncodedLabel ID="Label5" runat="server"></ml:EncodedLabel></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td class="col-md-6">Num of Failed Attempts:</td>
                        <td class="col-md-6" colspan="2"><ml:EncodedLabel ID="Label6" runat="server"></ml:EncodedLabel></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td class="col-md-6">Is Temp Password:</td>
                        <td class="col-md-6" colspan="2"><ml:EncodedLabel ID="Label7" runat="server"></ml:EncodedLabel></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td class="col-md-6">Is Locked?</td>
                        <td  class="col-md-3"><ml:EncodedLabel ID="Label8" runat="server"></ml:EncodedLabel></td>
                        <td class="col-md-3 text-center"><asp:Button ID="LockBtn" Text="Lock" class="btn btn-warning" OnClick="LockBtn_OnClick" runat="server"></asp:Button></td>
                      </tr>
                  </tbody>
                </table>
              </div> </div> 
            </div>
            <div class="row">
              <div class="col-xs-10 col-xs-offset-1"><div class="col-xs-6 col-xs-offset-3 form-label-noerror">
                <div class="form-group ">
                  <label class="control-label">Set Temp Password To:</label>
                  <div class="text-right-noerror">
                    <span id="error" class="text-danger"></span>
                  </div>
                  <div class="input-group">
                    <asp:TextBox  AutoComplete="off"   class="form-control input" ID="tempPass" runat="server" ></asp:TextBox>
                    <script> $("input[id*='tempPass'").focus()</script>
                      <span class="input-group-btn">
                      <span class="clipper">
                        <asp:Button  class="btn  btn-primary" OnClientClick="return verifyPasswordInput('tempPass');"  OnClick="SetPwdBtn_OnClick"   Text="Set" runat="server" ></asp:Button>
                      </span>
                    </span>
                  </div>
                </div>
              </div></div> 
            </div>
            <div class="row">
              <div  class="col-xs-12">
    			<asp:panel id="LoansPanel" Runat="server">
    			    <asp:Repeater ID="LoansGrid" runat="server" OnItemDataBound="LoansGrid_ItemBound">
    			        <HeaderTemplate>
    			           <table  class="table table-striped table-condensed">
    			             <thead>
    			               <tr>
    			                 <th>Loan Number</th>
    			                 <th>Broker Name</th>
    			                 <th>Borrower Name</th>
    			                 <th>Branch Name</th>
    			                 <th>Property Address</th>
    			                 <th>Loan Status</th>
    			               </tr>
    			             </thead>
    			             <tbody>
    			        </HeaderTemplate>
    			        <ItemTemplate>
    			            <tr>
    			                <td><ml:EncodedLiteral id="m_LoanNumber" runat="server"></ml:EncodedLiteral></td>
    			                <td><ml:EncodedLiteral id="m_BrokerName" runat="server"></ml:EncodedLiteral></td>
    			                <td><ml:EncodedLiteral id="m_BorrowerName" runat="server"></ml:EncodedLiteral></td>
    			                <td><ml:EncodedLiteral id="m_BranchName" runat="server"></ml:EncodedLiteral></td>
    			                <td><ml:EncodedLiteral id="m_PropertyAddress" runat="server"></ml:EncodedLiteral></td>
    			                <td><ml:EncodedLiteral id="m_LoanStatus" runat="server"></ml:EncodedLiteral></td>
    			            </tr>
    			        </ItemTemplate>
    			        <FooterTemplate>
    			            </tbody>
    			          </table>
    			        </FooterTemplate>
    			    </asp:Repeater>
                     <p class="text-center"><ml:EncodedLiteral runat="server" ID="noRecordsLiteral" Text="No loan found."></ml:EncodedLiteral></p>
				</asp:panel>
              </div> 
            </div>
          </div>
        </div>
       </div> 
          <script>
              function verifyPasswordInput(inputId) {
                  var inputItem = $("input[id*='" + inputId + "']");
                  return validateInputByRegex(inputItem, /^[^\s]{6,}$/i, "Password must be at least 6 characters.");
              }
              function validateInputByRegex(inputItem, testRegex, errorString) {
                  var valueText = inputItem.val().trim();
                  if (valueText.length > 0 && testRegex.test(valueText)) {
                      return true;
                  }
                  else {
                      inputItem.parent().closest('div').addClass("has-error has-feedback");
                      $("#error").text(errorString);
                      inputItem.focus();
                      return false;
                  }
              }
          </script>
      </form>  
    </div> 
  </div>
</asp:Content>