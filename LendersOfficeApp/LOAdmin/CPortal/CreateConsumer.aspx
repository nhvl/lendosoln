﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateConsumer.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.CPortal.CreateConsumer" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../common/header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Create Consumer [ONLY FOR TESTING]</title>
    <link href="../../css/stylesheet.css" type=text/css rel=stylesheet >
</head>
<body>
    <form id="form1" runat="server">
    <UC:HEADER id="Header1" runat="server"></UC:HEADER>
    <UC:HEADERNAV id="HeaderNav1" runat="server">
	    <MenuItem URL="~/LOAdmin/main.aspx" Label="Main" />
		<MenuItem URL="~/LOAdmin/CPortal/CreateConsumer.aspx" Label="Create Consumer" />
	</UC:HEADERNAV>
    <div>
            <table class="grid">
                <tr class="alt">
                    <td class="FieldLabel">E-mail:</td>
                    <td><asp:TextBox ID="Email" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Broker Id: </td>
                    <td><asp:TextBox ID="BrokerId" runat="server"></asp:TextBox></td>
                </tr>
                <tr class="alt">
                    <td class="FieldLabel">Temp Password: </td>
                    <td><asp:TextBox ID="TempPass" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Is Locked?</td>
                    <td><asp:CheckBox ID="IsLocked" runat="server" /></td>
                </tr>
                <tr>
                    <td colspan="2"><asp:Button ID="CreateBtn" Text="Create Consumer" OnClick="CreateBtn_OnClick" runat="server" /></td>
                </tr>
            </table>
            <ml:EncodedLabel ID="ErrorMsg" ForeColor="Red" Visible="false" runat="server"></ml:EncodedLabel>
            <ml:EncodedLabel ID="SuccessMsg" ForeColor="Green" Visible="false" runat="server"></ml:EncodedLabel>
    </div>
    </form>
</body>
</html>
