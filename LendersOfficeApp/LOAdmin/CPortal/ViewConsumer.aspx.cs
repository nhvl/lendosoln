﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Security.Cryptography;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.LOAdmin.CPortal
{
    public partial class ViewConsumer : LendersOffice.Admin.SecuredAdminPage
    {
        protected Int64 ConsumerId
        {
            get
            {
                return RequestHelper.GetInt("consumerid", -1);
            }
        }

        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("BrokerId");
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //BecomBtn.Enabled = false;
            if (ConsumerId == -1)
            {
                content.Visible = false;
                ShowErrorMessage("Invalid consumer Id.");
                return;
            }
            if (!IsPostBack)
            {
                BindData();
            }
            RegisterJsScript("ractive-0.7.1.min.js");
            m_loadLOdefaultScripts = false;
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        #region Public Method
        protected void SetPwdBtn_OnClick(object sender, System.EventArgs a)
        {
            if (string.IsNullOrEmpty(tempPass.Text) || tempPass.Text.Length < 6)
            {
                ShowErrorMessage("Invalid temp password. It needs to be at least 6 characters long.");
                return;
            }

            //Current HashMethod is:
            SHA256 sha1 = new SHA256Managed();
            string passwordHash = Convert.ToBase64String(sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(tempPass.Text)));

            try
            {
                SqlParameter[] parameters = {
                                             new SqlParameter("@ConsumerId", ConsumerId),
                                             new SqlParameter("@PasswordHash", passwordHash),
                                             new SqlParameter("@IsTemporaryPassword", true)
                                         };

                StoredProcedureHelper.ExecuteNonQuery(BrokerId, "CP_UpdateConsumerById", 1, parameters);
                tempPass.Text = "";
                BindData();
            }
            catch (Exception e)
            {
                ShowErrorMessage("Unable to set password.");
                Tools.LogError(e);
            }

        }
        protected void LockBtn_OnClick(object sender, System.EventArgs a)
        {
            try
            {
                Button LockBtn = (Button)sender;
                LockConsumer(ConsumerId, LockBtn.Text == "Lock");
                BindData();
            }
            catch (Exception e)
            {
                ShowErrorMessage("Unable to lock consumer.");
                Tools.LogError(e);
            }
        }
        
        protected void LoansGrid_ItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            EncodedLiteral m_LoanNumber = e.Item.FindControl("m_LoanNumber") as EncodedLiteral;
            EncodedLiteral m_BorrowerName = e.Item.FindControl("m_BorrowerName") as EncodedLiteral;
            EncodedLiteral m_PropertyAddress = e.Item.FindControl("m_PropertyAddress") as EncodedLiteral;
            EncodedLiteral m_BrokerName = e.Item.FindControl("m_BrokerName") as EncodedLiteral;
            EncodedLiteral m_BranchName = e.Item.FindControl("m_BranchName") as EncodedLiteral;
            EncodedLiteral m_LoanStatus = e.Item.FindControl("m_LoanStatus") as EncodedLiteral;

            m_LoanNumber.Text = DataBinder.Eval(e.Item.DataItem, "sLNm").ToString();
            m_BorrowerName.Text = DataBinder.Eval(e.Item.DataItem, "sPrimBorrowerFullNm").ToString();
            m_PropertyAddress.Text = DataBinder.Eval(e.Item.DataItem, "sSpFullAddr").ToString();
            m_BrokerName.Text = DataBinder.Eval(e.Item.DataItem, "BrokerNm").ToString();
            m_BranchName.Text = DataBinder.Eval(e.Item.DataItem, "BranchNm").ToString();
            m_LoanStatus.Text = DataBinder.Eval(e.Item.DataItem, "LoanStatus").ToString();

        }
        #endregion

        #region Internal Methods

        private void BindData()
        {
            SqlParameter[] parameters = { new SqlParameter("@ConsumerId", ConsumerId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "CP_RetrieveConsumerBrokerById", parameters))
            {
                if (reader.Read())
                {
                    Label0.Text = reader["ConsumerId"].ToString();
                    Label1.Text = reader["Email"].ToString();
                    Label2.Text = reader["BrokerNm"].ToString();
                    Label3.Text = reader["LastLoggedInD"].ToString();
                    Label4.Text = reader["PasswordResetCode"].ToString();
                    Label5.Text = reader["PasswordRequestD"].ToString();
                    Label6.Text = reader["LoginFailureNumber"].ToString();
                    Label7.Text = reader["IsTemporaryPassword"].ToString();
                    Label8.Text = reader["IsLocked"].ToString();
                    if (Label8.Text == "True")
                    {
                        LockBtn.Text = "Unlock";
                    }
                    else
                    {
                        LockBtn.Text = "Lock";
                    }
                    TableDataBind(BrokerId, ConsumerId);
                }
                else
                {
                    //not found error
                    //content.Visible = false;
                    ShowErrorMessage("Unable to retrieve user with consumerId: " + ConsumerId);
                }
            }
        }

        private void TableDataBind(Guid brokerId, Int64 consumer)
        {
            //TODO:get permission internal user
            SqlParameter[] parameters = {
                                                new SqlParameter("@ConsumerId", consumer)
                                             };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "CP_FindLoanListByConsumerId", parameters))
            {
                noRecordsLiteral.Visible = !reader.HasRows;
                LoansGrid.DataSource = reader;
                LoansGrid.DataBind();
            }

            
        }
        private int LockConsumer(Int64 consumerid, bool lck)
        {
            SqlParameter[] parameters = {
                                             new SqlParameter("@ConsumerId", ConsumerId),
                                             new SqlParameter("@IsLocked", lck)
                                         };

            return StoredProcedureHelper.ExecuteNonQuery(BrokerId, "CP_UpdateConsumerById", 1, parameters);
        }
        private void ShowSuccessMessage(string msg)
        {
            SetAlertMsg(msg, "Success!", "alert-success");
        }
        private void ShowErrorMessage(string msg)
        {
            SetAlertMsg(msg, "Error!", "alert-error");
        }
        private void SetAlertMsg(string msg, string subject, string alertClass)
        {
            responseContainer.Visible = true;
            responseContainer.Attributes.Add("class", responseContainer.Attributes["class"] + " " + alertClass);
            responseSubject.Text = subject;
            responseMsg.Text = msg;
        }
        
        #endregion
    }
}
