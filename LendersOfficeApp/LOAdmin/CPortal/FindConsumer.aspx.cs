﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using DataAccess;
using LendersOffice.Common;
using System.ComponentModel;
using System.Drawing;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using CommonProjectLib.Common.Lib;

namespace LendersOfficeApp.LOAdmin.CPortal
{
    public partial class FindConsumer : LendersOffice.Admin.SecuredAdminPage
    {
        class _Item
        {
            public long ConsumerId { get; set; }
            public string Email { get; set; }
            public string BrokerNm { get; set; }
            public Guid BrokerId { get; set; }
            public DateTime? LastLoggedInD { get; set; }
            public string LastLoggedInString { get { return LastLoggedInD == null ? @"" : ((DateTime)LastLoggedInD).ToString("MM/dd/yyyy hh:mm:ss tt", new System.Globalization.CultureInfo("en-US")); } set { } }
            public bool IsLocked { get; set; }
        }

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            
            // Return this page's required permissions set.
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.BecomeUser
            };
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            m_consumerInfo.Visible = false;
            RegisterJsScript("ractive-0.7.1.min.js");
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            if (!IsPostBack) {
                if ( Request.UrlReferrer != null && Request.UrlReferrer.ToString().Contains("/LOAdmin/CPortal/ViewConsumer.aspx"))
                {
                    if (Request.Cookies.AllKeys.Contains("m_BrokerName"))
                    {
                        m_BrokerName.Text = Request.Cookies["m_BrokerName"].Value;
                    }
                    if (Request.Cookies.AllKeys.Contains("m_EmailAddr"))
                    {
                        m_EmailAddr.Text = Request.Cookies["m_EmailAddr"].Value;
                    }
                    if (Request.Cookies.AllKeys.Contains("m_Status"))
                    {
                        m_Status.Text = Request.Cookies["m_Status"].Value;
                    }
                    BindDataGrid();
                }
            }


        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }


        protected void SearchClick(object sender, System.EventArgs a)
        {
            // Rebind the grid.
            BindDataGrid();
        }

        /// <summary>
        /// Handle reload request.
        /// </summary>

        protected void ReloadClick(object sender, System.EventArgs a)
        {

            Response.Redirect("FindConsumer.aspx");

        }

        /// <summary>
        /// Handle grid command.
        /// </summary>



        private void BindDataGrid()
        {
            // Validate permissions.  If the user can't access
            // each broker to edit, then don't show the edit
            // column.  We also need to block the page so that
            // straight url typing can't bypass security.  As
            // well, we do the same for the other link columns.

            if (HasPermissionToDo(E_InternalUserPermissions.EditEmployee) == false)
            {
                //m_Grid.Columns[0].Visible = false;
            }

            List<_Item> list = new List<_Item>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                List<SqlParameter> parameters = new List<SqlParameter>();

                if (m_EmailAddr.Text.Length > 0)
                {
                    parameters.Add(new SqlParameter("@Email", m_EmailAddr.Text.Replace("'", "''").TrimWhitespaceAndBOM()));
                }

                if (m_Status.SelectedItem.Value != "0")
                {
                    if (m_Status.SelectedItem.Value == "1")
                    {
                        parameters.Add(new SqlParameter("@IsLocked", false));
                    }
                    else if (m_Status.SelectedItem.Value == "2")
                    {
                        parameters.Add(new SqlParameter("@IsLocked", true));
                    }
                }

                if (m_BrokerName.Text.Length > 0)
                {
                    parameters.Add(new SqlParameter("@BrokerNm", m_BrokerName.Text.Replace("'", "''").TrimWhitespaceAndBOM()));
                }

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "CP_FindConsumer", parameters))
                {
                    while (reader.Read())
                    {
                        _Item item = new _Item();
                        item.BrokerId = (Guid)reader["BrokerId"];
                        item.BrokerNm = (string)reader["BrokerNm"];
                        item.ConsumerId = (long)reader["ConsumerId"];
                        item.Email = (string)reader["Email"];
                        item.IsLocked = (bool)reader["IsLocked"];
                        item.LastLoggedInD = reader.SafeDateTimeNullable("LastLoggedInD");

                        list.Add(item);
                    }
                }
                //store request data to cookies
                Response.Cookies["m_BrokerName"].Value = m_BrokerName.Text;
                Response.Cookies["m_BrokerName"].Expires = DateTime.MaxValue;
                Response.Cookies["m_EmailAddr"].Value = m_EmailAddr.Text;
                Response.Cookies["m_EmailAddr"].Expires = DateTime.MaxValue;
                Response.Cookies["m_Status"].Value = m_Status.SelectedItem.Value.ToString();
                Response.Cookies["m_Status"].Expires = DateTime.MaxValue;
            }

            string returnData = ObsoleteSerializationHelper.JavascriptJsonSerialize(list);
            if (string.IsNullOrEmpty(returnData))
            {
                returnData = "{}";
            }
            consumerDataKey.Value = returnData;

            m_consumerInfo.Visible = true; ;

        }
        
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
