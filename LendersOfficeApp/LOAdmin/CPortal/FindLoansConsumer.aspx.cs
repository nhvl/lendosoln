﻿using System;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Collections;
using System.Data;
using System.Web.UI.HtmlControls;

namespace LendersOfficeApp.LOAdmin.CPortal
{
    public partial class FindLoansConsumer : LendersOffice.Admin.SecuredAdminPage
    {
        //Rebuild by Long Nguyen
        //Email: longn@meridianlink.net
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.BecomeUser
            };
        }

        protected Int64 QueryConsumerId
        {
            get
            {
                return RequestHelper.GetInt("consumerid", -1);
            }
        }

        private Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("brokerid");
            }
        }

        private void SetErrorMsg(string msg)
        {
            if (string.IsNullOrEmpty(msg))
            {
                ErrorPanel.Visible = false;
                ErrorMsg.Text = string.Empty;
                return;
            }
            LoansPanel.Visible = false;
            ErrorPanel.Visible = true;
            ErrorMsg.Text = msg;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (QueryConsumerId != -1)
            {
                ErrorPanel.Visible = false;
                QueryPanel.Visible = false;
                LoansPanel.Visible = true;
                //HeaderPanel.Visible = false;
                //Bind Directly
                DataBind(BrokerId, QueryConsumerId);
            }
            else
            {
                if (!IsPostBack)
                {
                    ErrorPanel.Visible = false;
                    QueryPanel.Visible = true;
                    LoansPanel.Visible = false;
                }                
            }
            

        }
        protected class SortStyle
        {
            public string Name;
            public bool Ascending;
            public SortStyle(string _name, bool isascend)
            {
                Name = _name;
                Ascending = isascend;
            }
        }
        private static SortStyle sort;
        private void HandleSortType(object sender)
        {
            LinkButton btn = (LinkButton)sender;
            if (sort != null && sort.Name == btn.ID)
            {
                //Change state
                sort.Ascending = !sort.Ascending;
                return;
            }
            sort = new SortStyle(btn.ID, true);
        }
        protected void SortClick(object sender, System.EventArgs a)
        {
            //Show results
            if (IsValidValue())
            {
                HandleSortType(sender);
                DataBind(new Guid(m_BrokerId.Text), Int64.Parse(m_ConsumerId.Text));
                LoansPanel.Visible = true;
            }
            else
            {
                LoansPanel.Visible = false;
            }
        }
        protected void SearchClick(object sender, System.EventArgs a)
        {
            sort = null;
            //Show results
            if (IsValidValue())
            {
                DataBind(new Guid(m_BrokerId.Text), Int64.Parse(m_ConsumerId.Text));
                LoansPanel.Visible = true;
            }
            else
            {
                LoansPanel.Visible = false;
            }
        }
        private bool IsValidValue()
        {
            var s = m_BrokerId.Text.Trim();
            if (s == string.Empty)
            {
                ErrorMsg.Text = "Must provide Broker ID";
                ErrorPanel.Visible = true;
                return false;
            }
            Guid guid1;
            bool res = Guid.TryParse(s, out guid1);
            if (res == false)
            {
                // String is not a guid.
                ErrorMsg.Text = "Broker ID is invalid";
                ErrorPanel.Visible = true;
                return false;
            }
            s = m_ConsumerId.Text.Trim();
            if (s == string.Empty)
            {
                ErrorMsg.Text = "Must provide Consumer ID";
                ErrorPanel.Visible = true;
                return false;
            }
            Int64 num1;
            res = Int64.TryParse(s, out num1);
            if (res == false)
            {
                // String is not a number.
                ErrorMsg.Text = "Consumer ID is invalid";
                ErrorPanel.Visible = true;
                return false;
            }
            ErrorMsg.Text = string.Empty;
            ErrorPanel.Visible = false;
            return true;
        }
        protected void DataBind(Guid brokerId, Int64 consumer)
        {
            SqlParameter[] parameters = { new SqlParameter("@ConsumerId", consumer) };
            DataSet ds = new DataSet();
            string procName = "CP_FindLoanListByConsumerId";
            DataSetHelper.Fill(ds, brokerId, procName, parameters);
            var dataView = ds.Tables[0].DefaultView;
            if (sort != null && sort.Name != string.Empty)
            {
                if (sort.Ascending)
                    dataView.Sort = sort.Name + " ASC";
                else
                    dataView.Sort = sort.Name + " DESC";
                //  dataView = dataView.ToTable().DefaultView;
            }
            LoansGrid.DataSource = dataView;
            LoansGrid.DataBind();

            // Checks to see if no loan was found. Else, display it's information
            if (LoansGrid.Items.Count < 1 && ds.Tables[0] != null)
            {
                SetErrorMsg("No loans matching that id were found.");
                LoansGrid.Visible = false;
            }
            else
                LoansGrid.Visible = true;
        }

        protected void LoansGrid_ItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                if (sort == null || sort.Name == string.Empty)
                    //No need to sort
                    return;
                //Modify Header to sortable
                var span = e.Item.FindControl(sort.Name + "Span") as HtmlGenericControl;
                if (span == null) return;
                //http://getbootstrap.com/components/
                if (sort.Ascending)
                    span.Attributes["class"] = "glyphicon glyphicon-sort-by-attributes";
                else
                    span.Attributes["class"] = "glyphicon glyphicon-sort-by-attributes-alt";
                return;
            }

            Label m_LoanNumber = e.Item.FindControl("m_LoanNumber") as Label;
            Label m_BorrowerName = e.Item.FindControl("m_BorrowerName") as Label;
            Label m_PropertyAddress = e.Item.FindControl("m_PropertyAddress") as Label;
            Label m_BrokerName = e.Item.FindControl("m_BrokerName") as Label;
            Label m_BranchName = e.Item.FindControl("m_BranchName") as Label;
            Label m_LoanStatus = e.Item.FindControl("m_LoanStatus") as Label;

            m_LoanNumber.Text = DataBinder.Eval(e.Item.DataItem, "sLNm").ToString();
            m_BorrowerName.Text = DataBinder.Eval(e.Item.DataItem, "sPrimBorrowerFullNm").ToString();
            m_PropertyAddress.Text = DataBinder.Eval(e.Item.DataItem, "sSpFullAddr").ToString();
            m_BrokerName.Text = DataBinder.Eval(e.Item.DataItem, "BrokerNm").ToString();
            m_BranchName.Text = DataBinder.Eval(e.Item.DataItem, "BranchNm").ToString();
            m_LoanStatus.Text = DataBinder.Eval(e.Item.DataItem, "LoanStatus").ToString();

        }
    }
}
