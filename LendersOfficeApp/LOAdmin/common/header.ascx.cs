using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Threading;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;

namespace LendersOfficeApp.LOAdmin.common
{
    /// <summary>
    ///		Summary description for header.
    /// </summary>
    public partial  class header : System.Web.UI.UserControl
    {

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);


        }
		
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion


        protected override void OnPreRender( System.EventArgs a )
        {
            base.OnPreRender( a );
        }        
    }
}
