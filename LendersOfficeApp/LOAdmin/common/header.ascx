<%@ Control Language="c#" AutoEventWireup="false" Codebehind="header.ascx.cs" Inherits="LendersOfficeApp.LOAdmin.common.header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<TABLE class="Header" cellSpacing="0" cellPadding="5" width="100%">
	<tr>
		<td><b>LENDINGQB ADMINISTRATION CENTER</b></td>
		<TD style="BACKGROUND-COLOR: white" vAlign="bottom" align="right">
			<a href=<%=AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/logout.aspx")%> class="HeaderMenu">Logout</a>
		</TD>
	</tr>
</TABLE>
