<%@ Page language="C#" Codebehind="DRIVETestPage.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.DRIVE.DRIVETestPage" ValidateRequest="false"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="DRIVETestPage - LendingQB" %>

<asp:Content ContentPlaceHolderID="Main" Runat="Server">
<div class="container"><div class="panel"><div class="panel-body">
    <form runat="server" class="form-sm">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>User ID:</label>
                    <asp:TextBox ID="UserIdLO" Text="3c3fe2c0-a1f7-4933-9e22-649c8ed6cc4f" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Loan ID:</label>
                    <asp:TextBox ID="AuditLoanID" Text="54fd14b3-c5f9-4944-9fc1-ec291f364361" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form-group">
            <asp:Button OnClick="SendRequest_OnClick"  Text="Send DRIVE Request"            CssClass="btn btn-default" runat="server" />
            <asp:Button OnClick="LoadLoanData_OnClick" Text="Load most recent xml response" CssClass="btn btn-default" runat="server"  />
        </div>
        <div class="form-group">
            <asp:TextBox ID="xmlContent" runat="server" TextMode="MultiLine" Rows="20" CssClass="form-control resizable-vertical"></asp:TextBox>
        </div>
        <ml:EncodedLabel ID="Errors" runat="server"></ml:EncodedLabel>
    </form>
</div></div></div>
</asp:Content>
