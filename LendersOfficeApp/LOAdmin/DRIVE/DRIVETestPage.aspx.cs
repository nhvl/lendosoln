using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using Drive = LendersOffice.Conversions.Drive;

namespace LendersOfficeApp.LOAdmin.DRIVE
{
    public partial class DRIVETestPage : SecuredAdminPage
    {
        protected class DriveTestPageHelper
        {
            public string UserId { get; set; }
            public string Password { get; set; }

            public Guid LOUserId { get; set; }
            public Guid LOLoanId { get; set; }
        }

        DriveTestPageHelper PageInfo;

        protected void PageInit(object sender, EventArgs e)
        {
            // shouldn't this be encrypted at least?
            PageInfo = new DriveTestPageHelper
            {
                UserId = "lendqb59",
                Password = "Lending2"
            };
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            RegisterResources();
        }

        /// <summary>
        /// Helper function that gets the loan id and user id, then logs in at that user.
        /// </summary>
        /// <returns></returns>
        private bool InitRequest() // bad naming, can't think of a better one
        {
            string userIdLO = UserIdLO.Text.TrimWhitespaceAndBOM();
            string loanId = AuditLoanID.Text.TrimWhitespaceAndBOM();

            try
            {
                PageInfo.LOUserId = new Guid(userIdLO);
                PageInfo.LOLoanId = new Guid(loanId);
            }
            catch (FormatException)
            {
                Errors.Text = "Please enter a GUID.";
                return false;
            }

            // Don't copy this BecomeUser stuff
            var principal = Thread.CurrentPrincipal as InternalUserPrincipal;

            if (false == principal.SetBecomeUser(PageInfo.LOUserId))
            {
                Errors.Text = "Could not create a broker user principal from the provided user GUID.";
                return false;
            }
            return true;
        }

        protected void LoadLoanData_OnClick(object sender, EventArgs eventArgs)
        {
            if (false == this.InitRequest())
                return;

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(PageInfo.LOLoanId, typeof(DRIVETestPage));
            dataLoan.InitLoad();
            xmlContent.Text = dataLoan.sDriveXmlContent;
        }

        protected void SendRequest_OnClick(object sender, EventArgs eventArgs)
        {
            if (false == this.InitRequest())
                return;

            AbstractUserPrincipal currentUser = Thread.CurrentPrincipal as AbstractUserPrincipal;
            BrokerDB currentBroker = BrokerDB.RetrieveById(currentUser.BrokerId);
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(PageInfo.LOLoanId, typeof(DRIVETestPage));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck); // sFileVersion

            // Get ssssssss
            DriveSavedAuthentication auth = DriveSavedAuthentication.Retrieve(currentUser); // This is different from the CBC auth, which is handled in the request

            var request = Drive.DriveServer.CreateRequest(auth.UserName, auth.Password, PageInfo.LOLoanId);

            bool success = true;
            string responseXml;
            Drive.Response.DRIVEResponse response;

            try
            {
                response = Drive.DriveServer.Submit(request, out responseXml);
            }
            catch (WebException e) // What should we do if we fail to connect?
            {
                throw e; // Probably not this
            }

            Drive.Response.Loan loan = new Drive.Response.Loan();

            if (response.Errors.ErrorsList.Count > 0)
                success = false;

            if (response.LoanList.Count > 0)
            {
                loan = response.LoanList.First();

                if (loan.Errors.ErrorList.Count > 0)
                    success = false;

            }

            if (success)
            {
                // Generate the html through xslt transform
                StringBuilder htmlStringBuilder = new StringBuilder();
                using (TextReader inputReader = new StringReader(responseXml))
                {
                    using (TextWriter outputWriter = new StringWriter(htmlStringBuilder))
                    {
                        XslTransformHelper.TransformFromEmbeddedResource("LendersOffice.ObjLib.Conversions.DRIVE.DriveSummaryHtml.xslt", inputReader, outputWriter, null);
                    }
                }

                string encodedEDoc = response.LoanList.First().EncodedEDoc;
                string htmlContent = htmlStringBuilder.ToString();

                // Conditions
                var conditions = loan.Conditions.ConditionList;
                Drive.DriveImporter.ImportConditions(conditions, PageInfo.LOLoanId, currentUser.BrokerId);

                // Audit log
                AbstractAuditItem auditItem = new DriveSubmissionAuditItem(Thread.CurrentPrincipal as AbstractUserPrincipal, htmlContent);
                AuditManager.RecordAudit(PageInfo.LOLoanId, auditItem);

                AbstractAuditItem xmlAuditItem = new DriveSubmissionXmlAuditItem(Thread.CurrentPrincipal as AbstractUserPrincipal, responseXml);
                AuditManager.RecordAudit(PageInfo.LOLoanId, xmlAuditItem);

                // Edocs
                Drive.DriveImporter.UploadReportToEDocs(encodedEDoc, dataLoan.BrokerDB.DriveDocTypeID.Value, dataLoan.GetAppData(0).aAppId, PageInfo.LOLoanId);

                // XmlContent
                dataLoan.sDriveXmlContent = responseXml;
            }

            // 

            // Errors

            // Save
            dataLoan.Save();

            // Show the response
            byte[] responseBytes = Encoding.ASCII.GetBytes(responseXml);
            Response.Clear();
            Response.ClearHeaders();
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition", "attachment; filename=\"Response.xml\"");
            Response.AddHeader("Content-Length", responseBytes.Length.ToString());
            Response.OutputStream.Write(responseBytes, 0, responseBytes.Length);
            Response.Flush();
            Response.End();
        }

        void RegisterResources()
        {
            //RegisterJsScript("LOAdmin.SAE.DownloadList.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
