﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using System.Runtime.Serialization;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOfficeApp.LOAdmin
{


    public partial class ExecutingEngineTestPage : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("json.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterService("main", "/LoAdmin/ExecutingEngineTestPageService.aspx");
            BindBrokerDropDownList();
        }

        private void BindBrokerDropDownList()
        {
            ddlBrokerList.Items.Add(new ListItem("-- Choose Broker --", Guid.Empty.ToString()));
            ddlBrokerList.Items.Add(new ListItem("-- SYSTEM CONFIGURATION --", "11111111-1111-1111-1111-111111111111"));

            // The user (internal or otherwise) should not have to know about how we split the db to find the broker 
            // in the drop down list. OPM 233782
            var brokersSortedByName = new SortedList<string, ListItem>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "INTERNAL_USE_ONLY_ListActiveBrokers", null))
                {
                    while (reader.Read())
                    {
                        string brokerId = reader["BrokerId"].ToString();
                        string description = reader["BrokerNm"] + " (" + reader["CustomerCode"] + ")";
                        brokersSortedByName.Add(description, new ListItem(description, brokerId));
                    }
                }
            }

            ddlBrokerList.Items.AddRange(brokersSortedByName.Values.ToArray());
        }
    }
}
