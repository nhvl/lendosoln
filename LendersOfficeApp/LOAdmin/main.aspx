<%@ Page Language="c#" CodeBehind="main.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.main" %>

<%@ Register TagPrefix="uc1" TagName="CModalDlg" Src="../Common/ModalDlg/CModalDlg.ascx" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LendingQB</title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
		<script src="../inc/html5shiv-3.7.0.js"></script>
		<script src="../inc/respond-1.4.2.min.js"></script>
	<![endif]-->

    <script>
        function openNonModal(url)
        {
            window.open(url);
            return false;
        }
    </script>
</head>
<body>
    <form method="post" runat="server">
        <div class="wrap">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="navbar-header"><a class="navbar-brand">
                                <img alt="LendingQB" src="~/css/images/newdesign.logo.png" runat="server"/>
                            </a></div>
                        </div>
                        <div class="col-xs-2 text-center">
                            <p class="navbar-text">Administration Center</p>
                        </div>
                        <div class="col-xs-5">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="navbar-form">
                                    <span class="clipper">
                                        <a href="Test/TestCenter.aspx" class="btn btn-warning">Smoke Test</a></span>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <ml:EncodedLabel ID="adminName" runat="server"/>
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="Internal/ChangePassword.aspx">Change password</a></li>
                                    </ul>
                                </li>
                                <li><a href="~/logout.aspx" runat="server">
                                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                                    Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>

            <div class="container-fluid">
                <ol class="breadcrumb">
                    <li class="active"><a>Main Menu</a></li>
                </ol>

                <div class="row-masonry">
                    <div class="grid-sizer"></div>

                    <div class="masonry-item">
                        <div class="panel panel-default">
                            <div class="panel-heading">Author</div>
                            <div class="list-group">
                                <a href="Author/LoanProgramLookup.aspx" class="list-group-item">Look up loan program info</a>
                            </div>
                        </div>
                    </div>

                    <div class="masonry-item">
                        <div class="panel panel-default">
                            <div class="panel-heading">Billing</div>
                            <div class="list-group">
                                <a href="Billing/CRAInvoices.aspx"     class="list-group-item">CRA Invoices    </a>
                                <a href="Billing/PMLTransactions.aspx" class="list-group-item">PML Transactions</a>
                            </div>
                        </div>
                    </div>

                    <div class="masonry-item">
                        <div class="panel panel-default">
                            <div class="panel-heading">Build And Release</div>
                            <div class="list-group">
                                <a href="JSOnlyHotfix.aspx" class="list-group-item">JS-only hotfix</a>
                            </div>
                        </div>
                    </div>

                    <div class="masonry-item">
                        <div class="panel panel-default">
                            <div class="panel-heading">Consumer Portal</div>
                            <div class="list-group">
                                <a href="Manage/CustomPDFFieldProperties.aspx" class="list-group-item">Custom PDF Field Properties</a>
                                <a href="CPortal/CreateConsumer.aspx"          class="list-group-item">Create Consumer <span class="label label-info">For Testing Only</span></a>
                                <a href="CPortal/FindConsumer.aspx"            class="list-group-item">Find Consumers</a>
                                <a href="CPortal/FindLoansConsumer.aspx"       class="list-group-item">Find Loan by Consumer</a>
                            </div>
                        </div>
                    </div>

                    <div class="masonry-item">
                        <div class="panel panel-default">
                            <div class="panel-heading">Consumer Portal 2.0</div>
                            <div class="list-group">
                                <a href="ConsumerPortal/FindConsumers.aspx" class="list-group-item">Find 2.0 Consumers</a>
                            </div>
                        </div>
                    </div>

                    <div class="masonry-item">
                        <div class="panel panel-default">
                            <div class="panel-heading">Developer</div>
                            <div class="list-group">
                                <a href="Broker/BrokerReplicate.aspx?BrokerId=00000000-0000-0000-0000-000000000000"
                                    target="_blank"                                class="list-group-item rp-new    ">Broker Replicate</a>
                                <a href="BrowserUsageStatistics.aspx"              class="list-group-item           ">Browser Usage Statistics</a>
                                <a href="CacheTableRequestErrors.aspx"             class="list-group-item           ">Cache Table Request Errors</a>
                                <a href="Manage/ListConstSite.aspx"                class="list-group-item           ">Constants List</a>
                                <a href="Dislcosure/DisclosureAutomation.aspx"     class="list-group-item           ">Disclosure Automation</a>
                                <asp:LinkButton OnClick="GenerateError_OnClick"    class="list-group-item           " Text="Display Error Page" runat="server"/>
                                <a href="Task/DocumentConditionAssociations.aspx"  class="list-group-item           ">Document-Condition Associations </a>
                                <a href="DocMagic/DocMagicTestPage.aspx"           class="list-group-item           ">DocMagic Test Page              </a>
                                <a href="DocMagic/DocMagicXpathMappingEditor.aspx" class="list-group-item           ">DocMagic XPath mappings         </a>
                                <a href="DRIVE/DRIVETestPage.aspx"                 class="list-group-item           ">DRIVE Test Page                 </a>
                                <a href="Dislcosure/EConsentExpiration.aspx"       class="list-group-item           ">E-Consent Expiration            </a>
                                <a href="Manage/EDMSFaxPanel.aspx"                 class="list-group-item           ">EDMS Fax Tools                  </a>
                                <a href="ExecutingEngineTestPage.aspx"             class="list-group-item           ">Executing Engine Test Page      </a>
                                <a href="test/ExportImportCustomReport.aspx"       class="list-group-item rp-new    ">Import/Export Custom Report     </a>
                                <a href="Broker/PmlLogos.aspx"                     class="list-group-item           ">List PML Logos from FileDB      </a>
                                <a href="../test/dependencylookup.aspx"            class="list-group-item           ">Loan Field Dependency Lookup    </a>
                                <a href="LoanFieldSecurity.aspx"                   class="list-group-item           ">Loan Per-Field Security         </a>
                                <a href="Manage/LpeAccessControl.aspx"             class="list-group-item           ">Lpe Access Blocking Management  </a>
                                <a href="Broker/DeletedLoans.aspx"                 class="list-group-item           ">Manage Deleted Loans            </a>
                                <a href="MasterCRAList.aspx"                       class="list-group-item           ">Master CRA List                 </a>
                                <a href="PricePolicyLookup.aspx"                   class="list-group-item           ">Price Policy Lookup             </a>
                                <a href="QuickPricer2FeatureInDB.aspx"             class="list-group-item rp-new    ">QuickPricer 2.0 DB              </a>
                                <a href="Manage/SiteConfigEdit.aspx"               class="list-group-item           ">Random Utilities                </a>
                                <a href="RateMonitorTable.aspx"                    class="list-group-item rp-new    ">Rate Monitor Table Management   </a>
                                <a href="Internal/EdocsEstimation.aspx"            class="list-group-item           ">Recently Underestimates EDocs   </a>
                                <a href="test/SetFieldNameForXmlExport.aspx"       class="list-group-item rp-new    ">Setting For Xml Import/Export   </a>
                                <a href="Test/TestCenter.aspx"                     class="list-group-item           ">Test Center                     </a>
                                <a href="Test/TestCreditPage.aspx"                 class="list-group-item           ">Test Credit Report Ordering     </a>
                            </div>
                        </div>
                    </div>

                    <div class="masonry-item">
                        <div class="panel panel-default">
                            <div class="panel-heading">Internal Admin</div>
                            <div class="list-group">
                                <a href="Internal/AdminList.aspx"        			class="list-group-item">Admin List           			</a>
								<a href="Internal/QueueBacklogAutoEscalation.aspx" 	class="list-group-item">Queue Backlog Auto-Escalation   </a>
                                <a href="Internal/StageConfigAdmin.aspx" 			class="list-group-item">Stage Config Admin   			</a>
                            </div>
                        </div>
                    </div>

                    <div class="masonry-item">
                        <div class="panel panel-default">
                            <div class="panel-heading">Manage Accounts</div>
                            <div class="list-group">
                                <a href="Broker/BrokerList.aspx"             class="list-group-item       ">Brokers List                                 </a>
                                <a href="Manage/ImportEdocs.aspx"            class="list-group-item rp-new">Edocs Batch Importer                         </a>
                                <a href="Broker/FindUser.aspx"               class="list-group-item       ">Find User                                    </a>
                                <a href="Broker/LoanUtilities.aspx"          class="list-group-item       ">Loan Utilities (previously Find Loan)        </a>
                                <a href="dataimport.aspx"                    class="list-group-item       ">Import Data From Excel File                  </a>
                                <a href="Manage/MBSDescriptions.aspx"        class="list-group-item       ">MBS Descriptions                             </a>
                                <a href="Manage/SecurityQuestions.aspx"      class="list-group-item       ">Password Security Questions                  </a>
                                <a onclick="return openNonModal('Broker/Workflow/EditWorkflowConfiguration.aspx?brokerId=<%= AspxTools.HtmlString(ConstAppDavid.SystemBrokerGuid.ToString()) %>');"
                                                                             class="list-group-item       ">System Configuration                         </a>
                                <a href="Broker/Features/FeatureAdoption.aspx" class="list-group-item     ">Feature Adoption                             </a>
                                <a href="Broker/Features/FeatureManagement.aspx" class="list-group-item   ">Feature Management                           </a>
                            </div>
                        </div>
                    </div>

                    <div class="masonry-item">
                        <div class="panel panel-default">
                            <div class="panel-heading">Manage Services</div>
                            <div class="list-group">
                                <a href="Manage/LostEDocs.aspx"                          class="list-group-item           ">Browse Lost Electronic Documents               </a>
                                <a href="Manage/AdminPanel.aspx"                         class="list-group-item           ">Check Admin                                    </a>
                                <a href="Manage/EmailPanel.aspx"                         class="list-group-item           ">Check Email                                    </a>
                                <a href="Manage/DBMQManager.aspx"                        class="list-group-item           ">Check Message Queues                           </a>
                                <a href="Manage/CheckUrl.aspx"                           class="list-group-item           ">Check URL Connectivity                         </a>
                                <a href="Manage/EDMSPanel.aspx"                          class="list-group-item           ">Configure Electronic Document Management System</a>
                                <a href="Manage/DeriveJob.aspx"                          class="list-group-item           ">Derivation Jobs                                </a>
                                <a href="Manage/GenericFrameworkVendors.aspx"            class="list-group-item           ">Generic Framework Vendors                      </a>
                                <a href="Manage/ManageIrs4506TVendors.aspx"              class="list-group-item           ">Manage 4506-T Vendors                          </a>
                                <a href="Manage/ManageAppraisalVendors.aspx"             class="list-group-item           ">Manage Appraisal Vendors                       </a>
                                <div>&nbsp&nbsp <b>Manage Barcode for</b>&nbsp&nbsp
                                    <a href="Manage/ManageDocBarcode.aspx?docType=DocMagic" >DocMagic</a> &nbsp&nbsp
                                    <a href="Manage/ManageDocBarcode.aspx?docType=DocuTech" >DocuTech</a> &nbsp&nbsp
                                    <a href="Manage/ManageDocBarcode.aspx?docType=IDS" >IDS</a>
                                </div>
                                <a href="Manage/ManageCenlarIntegration.aspx"            class="list-group-item rp-new    ">Manage Cenlar Integration                      </a>
                                <a href="Manage/ManageFloodResellers.aspx"                class="list-group-item rp-new   ">Manage Flood Resellers                         </a>
                                <a href="Manage/ManageDataRetrievalPartners.aspx"        class="list-group-item           ">Manage Data Retrieval Framework Partners       </a>
                                <a href="Manage/ManageDocumentVendors.aspx"              class="list-group-item           ">Manage Document Vendors                        </a>
                                <a href="Manage/ManageModifiedLoans.aspx"                class="list-group-item           ">Manage Modified Loan Files                     </a>
                                <a href="Manage/ManageMIVendors.aspx"                    class="list-group-item           ">Manage Mortgage Insurance Vendors              </a>
                                <a href="Manage/ManageOcrDocTypes.aspx"                  class="list-group-item           ">Manage OCR Document Types                      </a>
                                <a href="Manage/ManageOCRVendors.aspx"                   class="list-group-item           ">Manage OCR Vendors                             </a>
                                <a href="Manage/ManageSFTPConfiguration.aspx"            class="list-group-item           ">Manage SFTP Configuration                      </a>
                                <a href="Manage/ManageThirdPartyClientCertificates.aspx" class="list-group-item           ">Manage Third Party Client Certificates         </a>
                                <a href="Manage/ManageTitleVendors.aspx"                 class="list-group-item           ">Manage Title Vendors                           </a>
                                <a href="Manage/ManageVOXVendors.aspx"                   class="list-group-item           ">Manage VOX Vendors                             </a>
                                <a href="Manage/TrackPanel.aspx"                         class="list-group-item           ">Track Users                                    </a>
                                <a href="Manage/Update.aspx"                             class="list-group-item           ">Update Database                                </a>
                                <a href="Manage/VendorAdoption.aspx"                     class="list-group-item rp-new    ">Vendor Adoption                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="masonry-item">
                        <div class="panel panel-default">
                            <div class="panel-heading">SAE</div>
                            <div class="list-group">
                                <a href="SAE/RateSheetVersion2.aspx"    class="list-group-item rp-new    ">Acceptable Ratesheets    </a>
                                <a href="SAE/RateSheetVersion.aspx"     class="list-group-item           ">Acceptable Ratesheets    </a>
                                <a href="SAE/PlanCodeEditor.aspx"       class="list-group-item           ">DocMagic Plan Code Editor</a>
                                <a href="SAE/DownloadList.aspx"         class="list-group-item           ">Download List            </a>
                                <a href="SAE/EmailDownloadList.aspx"    class="list-group-item           ">Email Download List      </a>
                                <a href="SAE/InvestorXlsFileList.aspx"  class="list-group-item           ">Investor Ratesheets      </a>
                                <a href="SAE/NewLpeKeywords.aspx"       class="list-group-item           ">New LPE Keywords         </a>
                                <a href="SAE/OutputFiles.aspx"          class="list-group-item           ">Output Files             </a>
                                <a href="SAE/SAEDatabaseQueries.aspx"   class="list-group-item           ">SAE Database Queries     </a>
                            </div>
                        </div>
                    </div>

                    <div class="masonry-item w2">
                        <div class="panel panel-default">
                            <div class="panel-heading">Queue Status</div>

                            <div id="loansTable">
                                <div class="panel-body">
                                    <p>loading&hellip;</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer navbar-inverse">
            <div class="container-fluid">
                <div class="text-muted">
                    <ml:EncodedLiteral ID="copyright" runat="server" />
                </div>
            </div>
        </div>
    </form>
    <uc1:CModalDlg ID="m_ModalDlg" runat="server" />
</body>
</html>
