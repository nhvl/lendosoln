﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin
{
    public partial class loadmin : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterResources();

            if (!IsPostBack) { PullAdminNameAndCopyright(); }
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterAllIncludeScript();
        }

        protected void OnSiteMapNodeItemDataBound(object sender, SiteMapNodeItemEventArgs e)
        {
            SiteMapNodeItem item = e.Item;
            if (item.ItemType == SiteMapNodeItemType.PathSeparator)
            {
                return;
            }

            var listItem = new HtmlGenericControl("li");
            System.Web.SiteMapNode node = item.SiteMapNode;
            if (item.ItemType == SiteMapNodeItemType.Current)
            {
                listItem.Attributes.Add("class", "active");
                listItem.Controls.Add(new MeridianLink.CommonControls.EncodedLabel()
                {
                    Text = node.Title,
                    ToolTip = node.Description
                });
            }
            else
            {
                listItem.Controls.Add(new HyperLink()
                {
                    Text = AspxTools.HtmlString(node.Title),
                    NavigateUrl = node.Url,
                    ToolTip = node.Description
                });
            }

            item.Controls.Add(listItem);
        }

        void PullAdminNameAndCopyright()
        {
            var principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

            // if pricipal doesn't exist, log out user
            if (principal == null)
            {
                Response.Redirect("~/logout.aspx");
                return;
            }

            //if display name exist, show it as admin name
            //if display name doesn't exist, show admin name as "Administrator"
            adminName.Text = !string.IsNullOrEmpty(principal.DisplayName) ? principal.DisplayName : "Administrator";

            //add copyright to footer
            copyright.Text = ConstAppDavid.CopyrightMessage;
        }
        void RegisterResources()
        {
            RegisterCSS("newdesign.main.css");
            RegisterCSS("newdesign.lendingqb.css");

            // for IE <= 8
            RegisterJsScript("html5shiv-3.7.0.js");
            RegisterJsScript("es5-shim-4.1.0.min.js");
            RegisterJsScript("respond-1.4.2.min.js");
            // for IE <= 9
            RegisterJsScript("console-stub.js");

            RegisterJsScript("modernizr-2.8.3.js");

            RegisterJsScript("bootstrap-3.3.4.min.js");
        }

        readonly List<string> m_includeOrderedKeys = new List<string>();
        readonly IDictionary<string, HtmlControl> m_includeDictionary =
            new Dictionary<string, HtmlControl>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Register to include a `.css` file on this page.
        /// CSS file must be in the `{VirtualRoot}/css/` folder.
        /// </summary>
        public void RegisterCSS(string css)
        {
            if (string.IsNullOrEmpty(css)) return;
            if (m_includeDictionary.ContainsKey(css)) return;

            var path = "~/css/" + css;

            var element = new HtmlGenericControl("link");
            element.Attributes.Add("href", AppendVersionToInclude(path));
            element.Attributes.Add("rel", "stylesheet");

            m_includeDictionary.Add(css, element);
            m_includeOrderedKeys.Add(css);
        }

        /// <summary>
        /// Register to include `.js` file on this page.
        /// `.js` file must be in `{VirtualRoot}/inc/` folder
        /// </summary>
        void RegisterJsScript(string js)
        {
            if (string.IsNullOrEmpty(js)) { return; }
            if (m_includeDictionary.ContainsKey(js)) { return; }

            var path = "~/inc/" + js;
            var element = new HtmlGenericControl("script");
                element.Attributes.Add("src", AppendVersionToInclude(path));

                m_includeDictionary.Add(js, element);
            m_includeOrderedKeys.Add(js);
        }

        void RegisterAllIncludeScript()
        {
            foreach (var scriptKey in m_includeOrderedKeys)
            {
                this.head.Controls.Add(
                    m_includeDictionary[scriptKey]
                );
            }
        }

        string AppendVersionToInclude(string path)
        {
            string url = ResolveUrl(path);

            if (url.Contains("?"))
            {
                Tools.LogError(string.Format("WHY is js [{0}] contains ?", path));
                return url; // 4/17/2012 dd - Return as is.
            }

            return string.Format("{0}?v={1}", url, IncludeVersion);
        }
        static string IncludeVersion = Guid.NewGuid().ToString("N");

        internal static void ResetIncludeVerion()
        {
            IncludeVersion = Guid.NewGuid().ToString("N");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
    }
}
