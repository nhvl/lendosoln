<%@ Page language="c#" Codebehind="AdminErrorPage.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.AdminErrorPage" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD runat="server">
    <TITLE>AdminErrorPage</title>
</HEAD>
<BODY MS_POSITIONING="FlowLayout" bgcolor="#003366" onload="onInit();">
    <script>
        function onInit() { try
        {
            // Resize window if we're rendering in a dialog box
            // that's too small.

            if( window.dialogWidth != null && parseInt( window.dialogWidth , 10 ) < 300 )
            {
                window.dialogLeft  = screen.availWidth / 2 - 350 + "px";
                window.dialogWidth = 700 + "px";
            }

            if( window.dialogHeight != null && parseInt( window.dialogHeight , 10 ) < 300 )
            {
                window.dialogTop    = screen.availHeight / 2 - 250 + "px";
                window.dialogHeight = 500 + "px";
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}
    </SCRIPT>
    <FORM id="AdminErrorPage" method="post" runat="server">
        <DIV style="MARGIN: 64px; PADDING: 48px; WIDTH: 100%; BACKGROUND: white; BORDER: 1px solid gray; FONT: 12px arial;">
            <IMG src="../Images/Logo.gif">
            <BR>
            <BR>
            <BR>
            You don't have permission to access this admin page.
            <BR>
            <SPAN style="COLOR: red;">
                Requires: <%= AspxTools.HtmlString(RequestHelper.GetSafeQueryString( "required" )) %>
            </SPAN>
            <BR>
            <BR>
            Please contact the LendingQB&#8482; administrator
            to grant access to the desired resource.
            <BR>
            <BR>
            <BR>
            <BR>
            <A href="Main.aspx">Back to the adminstrative center...
            </A>
        </DIV>
     </FORM>
</BODY>
</HTML>
