﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LOAdmin/loadmin.Master" AutoEventWireup="true" CodeBehind="ManageFloodResellers.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageFloodResellers" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="server">
    <style type="text/css">
        .modal-dialog
        {
            width: 800px;
        }
        .modal-dialog .dialog-input
        {
            width: 250px;
        }
        .form-inline .MiniInput
        {
            width: 60px;
        }
        .form-inline .LargeInput 
        {
            width: 400px;
        }
        .form-inline .SuperLargeInput 
        {
            width: 500px;
        }
        .modal-dialog .form-inline
        {
            margin: 3px;
            min-height: 34px;
        }
        .input-label
        {
            width: 20%;
        }
        .BaseCopy
        {
            display: none;
        }
        table.table-striped tr.invalid:nth-of-type(2n+1)
        {
            background-color: #f9dada;
        }

        table.table-striped tr.invalid
        {
            background-color: #ffe0e0;
        }
    </style>

    <script type="text/javascript">

        var app = angular.module('FloodResellerApp', []);
        app.controller('FloodResellerController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
            $scope.items = [];
            $scope.resellerId = "";
            $scope.orgResellerId = "";
            $scope.resellerName = "";
            $scope.resellerUrl = "";
            $scope.titleDlg = "Add New Reseller";


            $scope.init = function () {
                $http.post("ManageFloodResellers.aspx/GetFloodResellers", {})
                    .success(function (data) {
                        $scope.items = data.d;
                    })
                    .error(function (data, status) {
                        alert("error");
                    });
            }

            $scope.addReseller = function () {
                $scope.titleDlg = "Add New Flood Reseller";
                $scope.orgResellerId = $scope.resellerId = "";
                $scope.resellerName = "";
                $scope.resellerUrl = "";
                var x = $('#resellerIdArea');
                $('#resellerIdArea').show();
            }

            $scope.editReseller = function (item) {
                $scope.titleDlg = "Update Flood Reseller";
                $scope.orgResellerId = $scope.resellerId = item.Id;
                $scope.resellerName = item.Name;
                $scope.resellerUrl = item.Url;
                var x = $('#resellerIdArea');
                $('#resellerIdArea').hide();
            }

            $scope.saveReseller = function () {
                var name = $scope.resellerName;
                var url = $scope.resellerUrl;

                if (!name || !url) {
                    alert("Cannot create/update a flood reseller with name/url is empty!");
                    return;
                }

                if (!$scope.orgResellerId) {
                    $http.post("ManageFloodResellers.aspx/AddNewFloodReseller", {"id":$scope.resellerId, "name": name, "url":url})
                        .success(function (data) {
                            $("#FloodResellerPopupDlg").modal("hide");
                            $scope.items = [];
                            $scope.init();
                        })
                        .error(function (data, status) {
                            $("#FloodResellerPopupDlg").modal("hide");
                            alert("Error: cannot add new flood reseller.");
                        });
                } else {
                    if (confirm("Do you want to update flood reseller: " + name + "?") == false) {
                        return;
                    }

                    $http.post("ManageFloodResellers.aspx/UpdateFloodReseller", { "id": $scope.orgResellerId, "name": name, "url": url })
                        .success(function (data) {
                            $("#FloodResellerPopupDlg").modal("hide");
                            $scope.items = [];
                            $scope.init();
                        })
                        .error(function (data, status) {
                            $("#FloodResellerPopupDlg").modal("hide");
                            alert("Error: cannot update the flood reseller.");
                        });
                }
            }

            $scope.init();

        }]);
    </script>

    <div ng-app="FloodResellerApp"><div ng-controller="FloodResellerController" class="panel-body div-wrap-tb">

    <div>
        <div class="container-fluid">
            <div class="row">
                <div class="center-div">
                    <div class="panel panel-default">
                        <div class="panel-body div-wrap-tb">
                            <span class="clipper">
                                <button type="button" class="btn btn-primary" ng-click="addReseller()"
                                        data-toggle="modal" data-target="#FloodResellerPopupDlg">Add New</button>
                            </span>
                            
                            <table class="table table-striped table-condensed table-layout-fix">
                                <thead>
                                    <tr>
                                        <th width="60"></th>
                                        <th>Name</th>
                                        <th>Service Url</th>
                                        <th>Id</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in items">
                                        <td>
                                            <a class="Edit" ng-click="editReseller(item)" data-toggle="modal" data-target="#FloodResellerPopupDlg">edit</a>
                                        </td>
                                        <td>{{item.Name}}</td>
                                        <td>{{item.Url}}</td>
                                        <td>{{item.Id}}</td>
                                    </tr>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="FloodResellerPopupDlg" class="modal v-middle fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{titleDlg}}</h4>
                </div>

                <div id="Inputs" class="modal-body">
                    <div id="resellerIdArea" class="form-inline">
                        <label class="input-label">Reseller Guid Id</label>
                        <input type="text" ng-model=resellerId class="LargeInput form-control" />
                        (leave blank to auto-generate Id)
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Name</label>
                        <input type="text" ng-model=resellerName class="LargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Url</label>
                        <input type="text" ng-model=resellerUrl class="LargeInput form-control" />
                    </div>
                </div>

                <div class="modal-footer">
                    <span class="clipper">
                        <input type="button" class="btn btn-default" id="CancelBtn" data-dismiss="modal" value="Cancel"/>
                    </span>
                    <span class="clipper">
                        <input type="button" class="btn btn-primary" ng-click="saveReseller()" value="Save" />
                    </span>
                </div>
            </div>
        </div>
    </div>


    </div></div>
</asp:Content>
