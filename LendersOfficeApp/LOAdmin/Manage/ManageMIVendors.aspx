﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ManageMIVendors.aspx.cs"
    Inherits="LendersOfficeApp.LOAdmin.Manage.ManageMIVendors" MasterPageFile="~/LOAdmin/loadmin.Master" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style type="text/css">
        /*Show loading animate*/
        .glyphicon-refresh-animate {
            -animation: spin .7s infinite linear;
            -ms-animation: spin .7s infinite linear;
            -webkit-animation: spinw .7s infinite linear;
            -moz-animation: spinm .7s infinite linear;
        }

        @keyframes spin {
            from {
                transform: scale(1)rotate(0deg);
            }

            to {
                transform: scale(1) rotate(360deg);
            }
        }

        @-webkit-keyframes spinw {
            from {
                -webkit-transform: rotate(0deg);
            }

            to {
                -webkit-transform: rotate(360deg);
            }
        }

        @-moz-keyframes spinm {
            from {
                -moz-transform: rotate(0deg);
            }

            to {
                -moz-transform: rotate(360deg);
            }
        }
        /******/
        .btn:focus, .btn:active {
            outline: none !important;
        }

        .center-div {
            margin: 0 auto;
            min-width: 200px;
            max-width: 500px;
            width: 30%;
        }

        .width-200 {
            width: 200px;
        }

        .mismo-value {
            width: 10px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#VType').html($("[id$='VendorType']").html());

            $('#UsesSplitPremiumPlans').on('change', function () {
                $('#SplitPremiumOptionsSection').toggle();
            }).trigger('change');

            $('.AddOption').on('click', function () {
                createSplitPremiumOption('', '');
            });

            $('#SplitPremiumOptions').on('click', '.DeleteOption', function () {
                $(this).closest('tr').remove();
            });
        });

        function isUrlValid(url) {
            return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
        }

        function f_clearDialogInput() {
            $('#VendorId').val('');
            $('#VType').val('0');
            $('#ExportPath_Test').val('');
            $('#ExportPath_Production').val('');
            $('#RequiredAccountId').prop('checked', false);
            $('#SendAuthTicketWithOrders').prop('checked', false);
            $('#EnableConnectionTest').prop('checked', false);
            $('#UsesSplitPremiumPlans').prop('checked', false);

            $('#SplitPremiumOptionsSection').hide();
            $('.DeleteOption').trigger('click');
        }

        function f_launchDialog(sTitle) {
            $('#myModal .modal-title').html(sTitle);
            $('#loadingModal').hide();
            $('#myModal').modal('show');
        }

        function f_newVendor() {
            f_clearDialogInput();
            f_launchDialog('Add New MI Vendor');
        }

        function setTooltip(selector, title) {
            selector.prop("title", title);
            selector.focus();
            selector.tooltip({ placement: 'bottom' });
            selector.tooltip('show');
            selector.one("blur change", function () { selector.tooltip("destroy"); });
        }

        function f_saveVendor(e) {
            e.preventDefault();
            var args = {
                VendorId: $('#VendorId').val(),
                VendorType: $('#VType').val(),
                ExportPath_Test: $('#ExportPath_Test').val().trim(),
                ExportPath_Production: $('#ExportPath_Production').val().trim(),
                RequiredAccountId: $('#RequiredAccountId').prop('checked') ? 'True' : 'False',
                SendAuthTicketWithOrders: $('#SendAuthTicketWithOrders').prop('checked') ? 'True' : 'False',
                EnableConnectionTest: $('#EnableConnectionTest').prop('checked') ? 'True' : 'False',
                UsesSplitPremiumPlans: $('#UsesSplitPremiumPlans').prop('checked') ? 'True' : 'False',
                SplitPremiumOptions: serializeSplitPremiumOptions()
            };

            if (!args.VendorType) {
                $('#VType').val('');
                setTooltip($('#VType'), "Please fill out the Vendor Company");
                return false;
            }

            if (!args.ExportPath_Test) {
                $('#ExportPath_Test').val('');
                setTooltip($('#ExportPath_Test'), "Please fill out the Test Export Path");
                return false;
            }

            if (!isUrlValid(args.ExportPath_Test)) {
                setTooltip($('#ExportPath_Test'), "Test Export Path must be a valid URL.");
                return false;
            }

            if (!args.ExportPath_Production) {
                $('#ExportPath_Production').val('');
                setTooltip($('#ExportPath_Production'), "Please fill out the Production Export Path");
                return false;
            }

            if (!isUrlValid(args.ExportPath_Production)) {
                setTooltip($('#ExportPath_Production'), "Production Export Path must be a valid URL.");
                return false;
            }

            var result = gService.main.call("Update", args, true, false, false, true, successCallback, errorCallback);
        }

        function f_edit(vendorId) {
            var args = {
                VendorId: vendorId
            }

            $('#VendorId').val(vendorId);
            var result = gService.main.call("Retrieve", args, true, false, false, true, loadSuccessCallback, errorCallback);
            return false;
        }

        function f_delete(vendorId) {
            var args = {
                VendorId: vendorId
            }

            confirmDialog('Do you want to delete this Mortgage Insurance Vendor?', args);
            return false;
        }

        function confirmDialog(message, data) {
            $('#myConfirm .modal-body').html(message);
            $('#myConfirm #btnOK').click(function () {
                var result = gService.main.call("Delete", data, true, false, false, true, successCallback, errorCallback);
                return false;
            });
            $('#myConfirm').modal('show');
        }

        function successCallback(result) {
            if (result.error) {
                alert(result.UserMessage);
            } else if (result.value['Success'] === 'True') {
                $('#myModal').modal('hide');
                location.reload();
            } else {
                var errorMessage = result.value['Error'];
                if (errorMessage === '') {
                    errorMessage = 'Unable to save vendor configuration.';
                }

                alert(errorMessage);
            }
        }

        function loadSuccessCallback(result) {
            if (result.error) {
                alert(result.UserMessage);
                return false;
            }

            $('#VType').val(result.value['VendorType']);
            $('#ExportPath_Test').val(result.value['ExportPath_Test']);
            $('#ExportPath_Production').val(result.value['ExportPath_Production']);
            $('#RequiredAccountId').prop('checked', result.value['RequiredAccountId'] === 'True');
            $('#SendAuthTicketWithOrders').prop('checked', result.value['SendAuthTicketWithOrders'] === 'True');
            $('#EnableConnectionTest').prop('checked', result.value['EnableConnectionTest'] === 'True');

            var usesSplitPremiumPlans = result.value['UsesSplitPremiumPlans'] === 'True';
            $('#UsesSplitPremiumPlans').prop('checked', usesSplitPremiumPlans);
            $('#SplitPremiumOptionsSection').toggle(usesSplitPremiumPlans);

            $('.DeleteOption').trigger('click');
            var splitPremiumOptions = JSON.parse(result.value['SplitPremiumOptions']);
            splitPremiumOptions.forEach(function (element) {
                createSplitPremiumOption(element.UpfrontPercentage, element.MismoValue);
            });

            f_launchDialog('Edit MI Vendor');
        }

        function errorCallback(result) {
            alert(result.UserMessage);
        }

        function createSplitPremiumOption(upfrontPercentage, mismoValue) {
            var upfrontPercentageInput = $('<input>', { type: 'text', class: "form-control MiniInput upfront-percentage", value: upfrontPercentage });
            var upfrontPercentageCell = $('<td>').append(upfrontPercentageInput);

            var mismoValueInput = $('<input>', { type: 'text', class: "form-control MiniInput mismo-value", value: mismoValue });
            var mismoValueCell = $('<td>').append(mismoValueInput);

            var deleteButton = $('<input>', { type: 'button', class: "btn btn-default DeleteOption", value: '-' });
            var deleteButtonCell = $('<td>').append(deleteButton);

            var newRow = $('<tr>', { class: "split-premium-option" }).append(upfrontPercentageCell).append(mismoValueCell).append(deleteButtonCell);

            $('#SplitPremiumOptions').append(newRow);
        }

        function serializeSplitPremiumOptions() {
            var options = [];
            $('.split-premium-option').each(function () {
                var upfrontPercentage = $(this).find('.upfront-percentage').val();
                var mismoValue = $(this).find('.mismo-value').val();
                options.push({ UpfrontPercentage: upfrontPercentage || 0, MismoValue: mismoValue });
            });

            var serializedOptions = JSON.stringify(options);
            return serializedOptions;
        }
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="center-div">
                <div class="panel panel-default">
                    <div class="panel-body div-wrap-tb">
                        <form id="form1" runat="server">
                            <div>
                                <span class="clipper">
                                    <button type="button" class="btn btn-primary" onclick="f_newVendor();">
                                        Add New</button></span>
                                <asp:GridView runat="server" ID="Vendors" CssClass="table table-striped table-condensed table-layout-fix"
                                    CellSpacing="0" CellPadding="0" BorderWidth="0" GridLines="None" UseAccessibleHeader="true"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="javascript:void(0)" onclick="return f_edit('<%# AspxTools.HtmlString(Eval("VendorId").ToString()) %>');">edit</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="javascript:void(0)" onclick="return f_delete('<%# AspxTools.HtmlString(Eval("VendorId").ToString()) %>');">delete</a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField></asp:TemplateField>
                                        <asp:BoundField HeaderText="MI Company" DataField="VendorTypeFriendlyDisplay" HeaderStyle-Width="50%" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <asp:DropDownList runat="server" ID="VendorType" class="hide" />
                        </form>
                        <!-- Modal -->
                        <div id="myModal" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->

                                <form action="javascript:void(0);" onsubmit="return f_saveVendor(event);">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                &times;</button>
                                            <h4 class="modal-title"></h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" id="VendorId" name="VendorId" />
                                            <div class="form-group">
                                                <label>
                                                    Vendor Company:<span class="text-danger">*</span></label>
                                                <select id="VType" class="form-control" required>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                NOTE: Adding new url REQUIRES making a request to IT to allow our production servers
                                            to connect to it.
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Test Export Path:<span class="text-danger">*</span></label>
                                                <input type="text" id="ExportPath_Test" class="form-control" required title="Please fill out the Test Export Path"
                                                    autocomplete="off" />
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Production Export Path:<span class="text-danger">*</span></label>
                                                <input type="text" id="ExportPath_Production" class="form-control" required title="Please fill out the Production Export Path"
                                                    autocomplete="off" />
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="RequiredAccountId" />Require Account ID
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="SendAuthTicketWithOrders" />Add Generic Auth Ticket to MI Orders
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="EnableConnectionTest" />Enable associated connection tests
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="UsesSplitPremiumPlans" />Offers split premium plans
                                                </label>
                                            </div>
                                            <div class="form-inline" id="SplitPremiumOptionsSection">
                                                <div class="OptionDiv">
                                                    <table id="SplitPremiumOptions">
                                                        <tr>
                                                            <th class="width-200" id="UpfrontPercentageHeader">
                                                                Upfront Percentage
                                                            </th>
                                                            <th class="width-200" id="MismoValueHeader">
                                                                MISMO Key Value
                                                            </th>
                                                            <th></th>
                                                        </tr>
                                                    </table>
                                                    <input type="button" class="btn btn-default AddOption" data-target="SplitPremiumOptions" value="Add option" />
                                                </div>
                                            </div>
                                            <div id="loadingModal">
                                                <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>Loading...
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <span class="clipper">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Cancel
                                                </button>
                                            </span>
                                            <span class="clipper">
                                                <button type="submit" class="btn btn-primary">
                                                    Save
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--Confirm Dialog-->
                        <div id="myConfirm" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog modal-width">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Confirm Delete MI Vendor
                                        </h4>
                                    </div>
                                    <div class="modal-body text-center">
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel
                                            </button>
                                        </span> 
                                        <span class="clipper">
                                            <button type="button" class="btn btn-primary" id="btnOK">
                                                Delete
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>