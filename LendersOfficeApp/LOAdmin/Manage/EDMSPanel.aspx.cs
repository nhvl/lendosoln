﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class EDMSPanel : LendersOffice.Admin.SecuredAdminPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("LQBPopup.js");
            if (Page.IsPostBack)
            {
                if (m_hiddenCmd.Value == "deleteDocType")
                    onDeleteDocTypeClicked();
                else if (m_hiddenCmd.Value == "deleteShippingTemplate")
                    onDeleteShippingTemplateClicked();
            }
            else
            {
                BindFaxNumber();
            }
                                                          //we no longer have standard doctypes
            //EDocumentDocType docType = new EDocumentDocType();
            //m_dgDocTypes.DataSource = docType.StandardDocTypeList;
            //m_dgDocTypes.DataBind();

            EDocumentShippingTemplate templates = new EDocumentShippingTemplate();
            m_dgShippingTemplates.DataSource = templates.StandardShippingTemplateList;
            m_dgShippingTemplates.DataBind();

            
        }

        private void BindFaxNumber()
        {
            EDocsFaxNumber number = EDocsFaxNumber.RetrieveShared();
            if (number != null && number.HasNumber && number.IsSharedLine)
            {
                m_FaxNumber.Text = number.FaxNumber;
                m_DocRouterLogin.Text = number.DocRouterLogin;
                m_DocRouterPassword.Text = number.DocRouterPassword;
            }
        }

        private void onDeleteDocTypeClicked()
        {
            m_hiddenCmd.Value = "";
            EDocumentDocType docType = new EDocumentDocType();
            docType.DeleteDocType(Convert.ToInt32(m_hiddenId.Value));
        }

        private void onDeleteShippingTemplateClicked()
        {
            m_hiddenCmd.Value = "";
            EDocumentShippingTemplate st = new EDocumentShippingTemplate();
            st.DeleteShippingTemplate(Convert.ToInt32(m_hiddenId.Value));
        }

        protected void ApplyFaxNumber(Object sender, EventArgs e)
        {
            EDocsFaxNumber number = EDocsFaxNumber.RetrieveShared();
            if (number == null)
            {
                number = new EDocsFaxNumber(null, isSharedLine: true);
            }

            number.FaxNumber = m_FaxNumber.Text;
            number.DocRouterLogin = m_DocRouterLogin.Text;
            number.DocRouterPassword = m_DocRouterPassword.Text;

            number.Save();
        }
    }
}
