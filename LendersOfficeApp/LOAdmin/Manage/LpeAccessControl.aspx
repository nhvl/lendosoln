<%@ Page language="c#" Codebehind="LpeAccessControl.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Manage.LpeAccessControl" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../common/header.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LpeAccessControl</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../css/stylesheet.css" type=text/css rel=stylesheet >
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="LpeAccessControl" method="post" runat="server">
		 <UC:Header id="m_headerTag" runat="server"></UC:Header>
		<UC:HeaderNav id="HeaderNav" runat="server">
			<MenuItem URL="~/LOAdmin/main.aspx" Label="Main"></MenuItem>
			<MenuItem URL="~/LOAdmin/Manage/LpeAccessControl.aspx" Label="Lpe Access Blocking Management"></MenuItem>
		</UC:HeaderNav>
    
			<P>
				<asp:Button id="RefreshBtn" runat="server" Text="Refresh" onclick="RefreshBtn_Click"></asp:Button></P>
			<P>
				<asp:CheckBox id="HasBlockChk" runat="server" Text="Is Currently Blocked By" Font-Bold="True" Enabled="False"></asp:CheckBox>&nbsp;
				<asp:TextBox id="CurrentBlockerEd" runat="server" ReadOnly="True" Width="539px"></asp:TextBox>&nbsp;</P>
			<P>Started On
				<asp:TextBox id="StartDEd" runat="server" ReadOnly="True"></asp:TextBox></P>
			<P>Expired By
				<asp:TextBox id="ExpireDEd" runat="server" ReadOnly="True"></asp:TextBox></P>
			<STRONG>
				<P>
					<asp:Button id="RemoveBlockBtn" runat="server" Text="Remove Author Block" Width="147px" onclick="RemoveBlockBtn_Click"></asp:Button>&nbsp;&nbsp;
					<asp:Button id="RemoveAnyBlockBtn" runat="server" Text="Force Removing Current Block Of Any Type" Width="275px" onclick="RemoveAnyBlockBtn_Click"></asp:Button></P>
				<P>&nbsp;</P>
				<P>
				Your Name To Be Recorded&nbsp;With Blocking Request</STRONG>:
			<asp:TextBox id="requestorNameEd" runat="server"></asp:TextBox></P>
			<P>
				<asp:Button id="SaveBtn" runat="server" Text="Request Author Block" Width="154px" onclick="RequestAuthorBlock_Click"></asp:Button>&nbsp;</P>
			<P>&nbsp;</P>
			<P><STRONG>Error message:</STRONG>
			</P>
			<P>
				<asp:TextBox id="ErrMsgEd" runat="server" Width="736px" Height="103px" ReadOnly="True" TextMode="MultiLine"></asp:TextBox></P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
			<P>&nbsp;</P>
		</form>
	</body>
</HTML>
