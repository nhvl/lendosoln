﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using EDocs;
using DataAccess;
using System.Net;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class EDMSFaxPanel : LendersOffice.Admin.SecuredAdminPage
    {
        //Rebuild by Long Nguyen
        //Email: longn@meridianlink.net
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        // small pdf in base64.
        private const string TEST_PDF = "JVBERi0xLjQKJeLjz9MKMyAwIG9iajw8L1N1YnR5cGUvVHlwZTEvQmFzZUZvbnQvSGVsdmV0aWNhL0VuY29kaW5nL1dpbkFuc2lFbmNvZGluZy9UeXBlL0ZvbnQ+PgplbmRvYmoKNCAwIG9iaiA8PC9GaWx0ZXIvRmxhdGVEZWNvZGUvTGVuZ3RoIDEwPj5zdHJlYW0KeJwr5AIAAO4AfAplbmRzdHJlYW0KZW5kb2JqCjUgMCBvYmogPDwvRmlsdGVyL0ZsYXRlRGVjb2RlL0xlbmd0aCA3MT4+c3RyZWFtCnicUwjkKuRyCuHSj8g0VDA0UAhJ4zJUMABCQwVjMwVzc6BILpdGQGJ6qoKhjkJwbmJOjkJafl6JnmZIFpdrCFcgFwDoqQ+gCmVuZHN0cmVhbQplbmRvYmoKMiAwIG9iajw8L1Jlc291cmNlczw8L0ZvbnQ8PC9YaTEgMyAwIFI+Pi9Qcm9jU2V0Wy9QREYvVGV4dC9JbWFnZUIvSW1hZ2VDL0ltYWdlSV0+Pi9Sb3RhdGUgMC9QYXJlbnQgNiAwIFIvTWVkaWFCb3hbMCAwIDYxMiA3OTJdL0NvbnRlbnRzWzQgMCBSIDUgMCBSXS9UeXBlL1BhZ2U+PgplbmRvYmoKOSAwIG9iaiA8PC9GaWx0ZXIvRmxhdGVEZWNvZGUvTGVuZ3RoIDEwPj5zdHJlYW0KeJwr5AIAAO4AfAplbmRzdHJlYW0KZW5kb2JqCjEwIDAgb2JqIDw8L0ZpbHRlci9GbGF0ZURlY29kZS9MZW5ndGggNzE+PnN0cmVhbQp4nFMI5Crkcgrh0o/INFIwMlEISeMyVDAAQkMFQ3NzBWNzU4WQXC6NgMT0VAUjHQWnzHSFtPy8Ej3NkCwu1xCuQC4A0r8O9wplbmRzdHJlYW0KZW5kb2JqCjggMCBvYmo8PC9SZXNvdXJjZXM8PC9Gb250PDwvWGkyIDMgMCBSPj4vUHJvY1NldFsvUERGL1RleHQvSW1hZ2VCL0ltYWdlQy9JbWFnZUldPj4vUm90YXRlIDAvUGFyZW50IDYgMCBSL01lZGlhQm94WzAgMCA2MTIgNzkyXS9Db250ZW50c1s5IDAgUiAxMCAwIFJdL1R5cGUvUGFnZT4+CmVuZG9iago2IDAgb2JqPDwvS2lkc1syIDAgUiA4IDAgUl0vQ291bnQgMi9UeXBlL1BhZ2VzPj4KZW5kb2JqCjExIDAgb2JqPDwvUGFnZXMgNiAwIFIvVHlwZS9DYXRhbG9nPj4KZW5kb2JqCjEyIDAgb2JqPDwvUHJvZHVjZXIoaVRleHRTaGFycCA0LjEuMiBcKGJhc2VkIG9uIGlUZXh0IDIuMS4ydVwpKS9Nb2REYXRlKEQ6MjAxMDAxMjcxMzE1NTEtMDgnMDAnKS9DcmVhdGlvbkRhdGUoRDoyMDEwMDEyNzEzMTU1MS0wOCcwMCcpPj4KZW5kb2JqCnhyZWYKMCAxMwowMDAwMDAwMDAwIDY1NTM1IGYgCjAwMDAwMDAwMDAgNjU1MzYgbiAKMDAwMDAwMDMxNSAwMDAwMCBuIAowMDAwMDAwMDE1IDAwMDAwIG4gCjAwMDAwMDAxMDIgMDAwMDAgbiAKMDAwMDAwMDE3OCAwMDAwMCBuIAowMDAwMDAwODY2IDAwMDAwIG4gCjAwMDAwMDAwMDAgNjU1MzYgbiAKMDAwMDAwMDY5NyAwMDAwMCBuIAowMDAwMDAwNDgzIDAwMDAwIG4gCjAwMDAwMDA1NTkgMDAwMDAgbiAKMDAwMDAwMDkyMiAwMDAwMCBuIAowMDAwMDAwOTY3IDAwMDAwIG4gCnRyYWlsZXIKPDwvSW5mbyAxMiAwIFIvUm9vdCAxMSAwIFIvU2l6ZSAxMy9JRCBbPGJjZTdkZGQxZWQyZDJkMzVkYTQyMWMwOWI2MTQxY2E5PjxmYjc3ZWNmMWU5N2ZmMmQ5YmUzNGJiNjdlZmM1ZTBkZj5dPj4Kc3RhcnR4cmVmCjExMTEKJSVFT0YK";
        private string m_sPdfContent = TEST_PDF;
        private E_FaxCoverType m_postType = E_FaxCoverType.EDocs;
        //protected global::System.Web.UI.WebControls.TextBox m_appNum;

        private string ActionResponse
        {
            set
            {
                m_response.Text = value;
                m_response.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            m_response.Visible = false;

            if (IsPostBack == false)
            {
                if (LendersOffice.Constants.ConstAppDavid.CurrentServerLocation == LendersOffice.Constants.ServerLocation.LocalHost)
                {
                    m_urlToPostTb.Text = "http://localhost/DocReceiver/Receiver.aspx";
                    m_timeOutTb.Text = "30000";
                    m_loginTb.Text = "PML_TEST";
                    m_passwordTb.Text = "AAABBBCCCDDD";
                }
            }
        }

        private string BuildDocRouterPost()
        {
            string barcode = string.Empty;
            switch ( m_postType )
            {
                case E_FaxCoverType.ConsumerPortalRequest: barcode = BuildBarcodeStringForConsumerPortal(); break;
                case E_FaxCoverType.EDocs: barcode = BuildBarcodeStringForEdocs(); break;
                case E_FaxCoverType.ObsoleteEdocs: barcode = BuildBarcodeStringForEdocsOld(); break;
            }

            return String.Format(
                @"<ROUTE>
                  <LOGIN>{0}</LOGIN>
                  <PASSWORD>{1}</PASSWORD>
                  <DOCUMENT>
                    <SENDER>LoAdminTest</SENDER>
                    <TIME_RECEIVED>{2}</TIME_RECEIVED>
                    <BARCODES>
                      <BARCODE page=""1"">{3}</BARCODE>
                    </BARCODES>
                    <PAGES>3</PAGES>
                    <DATA dt=""base64Binary"">{4}</DATA>
                  </DOCUMENT>
                </ROUTE>
                "
                , m_loginTb.Text
                , m_passwordTb.Text
                , DateTime.Now.ToUniversalTime().ToString()
                , barcode
                , m_sPdfContent
                );
        }

        private string BuildBarcodeStringForEdocs()
        {
            return EdocFaxCover.BuildCompositeStringForEDocs(new Guid(m_loanId.Text), new Guid(m_appId.Text), int.Parse(m_docTypeTb.Text), m_descriptionTB.Text);
        }

        private string BuildBarcodeStringForEdocsOld()
        {
            return EdocFaxCover.BuildCompositeStringForObsoleteEDocs(new Guid( m_loanId.Text), int.Parse(m_docTypeTb.Text), m_descriptionTB.Text);
        }

        private string BuildBarcodeStringForConsumerPortal()
        {
            return EdocFaxCover.BuildCompositeStringForConsumerPortalRequest(long.Parse(m_cPid.Text), Guid.Parse(this.brokerIdForCP.Text));
        }

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.IsDevelopment
            };
        }

        protected void PullFileDbClick(object sender, EventArgs e)
        {
            byte[] pdfFile;

            try
            {
                pdfFile = FileDBTools.ReadData(E_FileDB.EDMS, m_lookupId.Text.Trim());
            }
            catch (Exception exc)
            {
                ActionResponse = exc.Message;
                Tools.LogError(exc);
                return;
            }

            SendFile(pdfFile);
        }

        protected void PullRequestClick(object sender, EventArgs e)
        {
            ConsumerActionItem item;
            try
            {
                 item = new ConsumerActionItem(long.Parse(m_lookupId.Text.Trim()), Guid.Parse(this.brokerIdForCP.Text));
            }
            catch (Exception exc)
            {
                ActionResponse = exc.Message;
                Tools.LogError(exc);
                return;
            }

            byte[] pdfFile;

            try
            {
                pdfFile = FileDBTools.ReadData(E_FileDB.EDMS,item.PdfFileDbKey.ToString());
            }
            catch (Exception exc)
            {
                ActionResponse = exc.Message;
                Tools.LogError(exc);
                return;
            }

            SendFile(pdfFile);
        }

        protected void SimulateFaxClick(object sender, EventArgs e)
        {
            ConsumerActionItem item;
            try
            {
                byte[] pdfFile = ReadFile();

                item = new ConsumerActionItem(long.Parse(m_lookupId.Text.Trim()), Guid.Parse(this.brokerIdForCP.Text));
                item.AttachArrivedFax("555-5555", pdfFile);
                item.Save();
            }
            catch (Exception exc)
            {
                ActionResponse = exc.Message;
                Tools.LogError(exc);
                return;
            }

            ActionResponse = "Attached Fax to " + item.ConsumerActionRequestId;
        }

        protected void DownloadPdfClick(object sender, EventArgs e)
        {
            byte[] pdfFile;
            try
            {
                byte[] pdfToAttach = ReadFile();
                if (m_cPid.Text != string.Empty)
                {
                    EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                    {
                        Type = E_FaxCoverType.ConsumerPortalRequest,
                        ConsumerRequestId = long.Parse(m_cPid.Text),
                        BrokerId = Guid.Parse(this.brokerIdForCP.Text),
                        LenderName = "Test Lender",
                        Description = m_descriptionTB.Text,
                        DocTypeDescription = "Doc type description",
                        ApplicationDescription = "Test Applicant",
                        LoanNumber = "Test Loan",
                        FaxNumber = "555-5555",
                        DocumentToAttach = pdfToAttach
                    };

                    pdfFile = EdocFaxCover.GenerateBarcodePackagePdf(new EDocs.EdocFaxCover.FaxCoverData[] { coverData });
                }
                else
                {
                    EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                    {
                        Type = E_FaxCoverType.EDocs,
                        LoanId = new Guid(m_loanId.Text),
                        AppId = new Guid(m_appId.Text),
                        LenderName = "Test Lender",
                        Description = m_descriptionTB.Text,
                        DocTypeId = int.Parse(m_docTypeTb.Text),
                        DocTypeDescription = "Doc type description",
                        FaxNumber = "555-5555",
                        ApplicationDescription = "Test Applicant",
                        LoanNumber = "Test Loan",
                        DocumentToAttach = pdfToAttach
                    };


                    pdfFile = EdocFaxCover.GenerateBarcodePackagePdf(new EDocs.EdocFaxCover.FaxCoverData[] { coverData });
                }
            }
            catch (Exception exc)
            {
                ActionResponse = exc.Message;
                Tools.LogError(exc);
                return;
            }

            SendFile(pdfFile);
        }

        protected void DownloadPdfCoverClick(object sender, EventArgs e)
        {
            byte[] pdfFile;
            try
            {
                if (m_cPid.Text != string.Empty)
                {

                    EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                    {
                        Type = E_FaxCoverType.ConsumerPortalRequest,
                        ConsumerRequestId = long.Parse(m_cPid.Text),
                        BrokerId = Guid.Parse(this.brokerIdForCP.Text),
                        LenderName = "Test Lender",
                        Description = m_descriptionTB.Text,
                        DocTypeDescription = "Doc type description.",
                        ApplicationDescription = "Test Applicant",
                        LoanNumber = "Test Loan",
                        FaxNumber = "555-5555",
                    };

                    pdfFile = EdocFaxCover.GenerateBarcodePackagePdf(new EDocs.EdocFaxCover.FaxCoverData[] { coverData });

                }
                else
                {
                    EdocFaxCover.FaxCoverData coverData = new EdocFaxCover.FaxCoverData()
                    {
                        Type = E_FaxCoverType.EDocs,
                        LoanId = new Guid(m_loanId.Text),
                        AppId = new Guid(m_appId.Text),
                        LenderName = "Test Lender",
                        Description = m_descriptionTB.Text,
                        DocTypeId = int.Parse(m_docTypeTb.Text),
                        DocTypeDescription = "Doc type description",
                        FaxNumber = "555-5555",
                        ApplicationDescription = "Test Applicant",
                        LoanNumber = "555-5555",
                    };

                    pdfFile = EdocFaxCover.GenerateBarcodePackagePdf(new EDocs.EdocFaxCover.FaxCoverData[] { coverData });
                }
            }
            catch (Exception exc)
            {
                ActionResponse = exc.Message;
                Tools.LogError(exc);
                return;
            }

            SendFile(pdfFile);
        }


        protected void FaxCoverClick(object sender, EventArgs e)
        {

            byte[] pdfFile;

            try
            {
                pdfFile = ConsumerActionItem.GetFaxCover(long.Parse(m_lookupId.Text.Trim()), Guid.Parse(this.brokerIdForCP.Text));
            }
            catch (Exception exc)
            {
                ActionResponse = exc.Message;
                Tools.LogError(exc);
                return;
            }

            SendFile(pdfFile);
        }

        private void SendFile(byte[] file)
        {
            Response.Clear();
            Response.ClearContent();
            Response.OutputStream.Write(file, 0, file.Length);
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "attachment; filename=\"File.pdf\"");
            Response.Flush();
            Response.End();
        }

        private byte[] ReadFile()
        {
            if (FileUpload.HasFile)
            {
                return Tools.ReadFully(FileUpload.PostedFile.InputStream, FileUpload.PostedFile.ContentLength);
            }
            else
            {
                return Convert.FromBase64String(TEST_PDF);
            }
        }

        private void PostToUrl()
        {
            WebRequest request = null;
            try
            {
                string stringToPost = BuildDocRouterPost();

                string uri = m_urlToPostTb.Text;
                request = WebRequest.Create(uri);
                request.Timeout = int.Parse(m_timeOutTb.Text);
                request.Method = "POST";
                request.ContentType = "text/xml";

                StreamWriter writer = new StreamWriter(request.GetRequestStream());
                writer.WriteLine(stringToPost);
                writer.Close();

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    const int BUFFER_SIZE = 50000;
                    byte[] buffer = new byte[BUFFER_SIZE];
                    int n = 0;

                    StringBuilder sb = new StringBuilder();

                    using (Stream stream = response.GetResponseStream())
                    {
                        while ((n = stream.Read(buffer, 0, BUFFER_SIZE)) > 0)
                        {
                            sb.Append(System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, n));
                        }
                    }
                    ActionResponse = "Response from " + m_urlToPostTb.Text + ":" + Environment.NewLine + Environment.NewLine + sb.ToString();
                }
            }
            finally
            {
                if (request != null) request.GetRequestStream().Close();
            }
        }

        protected void PostObsoleteClick(object sender, EventArgs e)
        {
            try
            {
                m_postType = E_FaxCoverType.ObsoleteEdocs;

                m_sPdfContent = Convert.ToBase64String(ReadFile());

                PostToUrl();
            }
            catch (Exception exc)
            {
                ActionResponse = exc.Message;
                Tools.LogError(exc);
                return;
            }
        }

        protected void PostClick(object sender, EventArgs e)
        {
            try
            {
                // Right now, just post the uploaded file directly--do not attach another coversheet
                //m_sPdfContent = (m_cPid.Text == string.Empty) ?
                //    Convert.ToBase64String(EdocFaxCover.GeneratePdfForEdocs(new Guid(m_loanId.Text), new Guid(m_appId.Text), "Test Coversheet", m_descriptionTB.Text, m_docTypeTb.Text, "Document", "555-5555", ReadFile() ))
                //    : Convert.ToBase64String(EdocFaxCover.GeneratePdfForConsumerPortalRequest(long.Parse(m_cPid.Text), "Test Coversheet", m_descriptionTB.Text, m_docTypeTb.Text, "555-5555", ReadFile()));

                if (m_cPid.Text != string.Empty)
                {
                    m_postType = E_FaxCoverType.ConsumerPortalRequest;
                }
                else
                {
                    m_postType = E_FaxCoverType.EDocs;
                }

                m_sPdfContent = Convert.ToBase64String(ReadFile() );


                PostToUrl();
            }
            catch (Exception exc)
            {
                ActionResponse = exc.Message;
                Tools.LogError(exc);
                return;
            }
        }
    }


}
