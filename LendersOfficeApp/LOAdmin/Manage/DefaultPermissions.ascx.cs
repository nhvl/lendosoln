namespace LendersOfficeApp.LOAdmin.Broker
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using LendersOffice.Security;
	using LendersOffice.Admin;
	using LendersOffice.Common;

	/// <summary>
	///		Summary description for DefaultPermissions.
	/// </summary>
	public partial  class DefaultPermissions : System.Web.UI.UserControl
	{

		#region variables 
		#endregion       
        
		protected void PageLoad(object sender, System.EventArgs e)
		{
			BaseServicePage page = this.Page as BaseServicePage; 
			if ( page != null ) 
			{
				page.RegisterService( "loadmin_utilities", "/LoAdmin/Manage/UtilityService.aspx" ); 
			
			}
			else 
				throw new DataAccess.CBaseException( "Problem rendering the page.", "Containing Page was not a form of BaseServicePage!"  ) ;
			// Put user code to initialize the page here
            m_Roles.DataSource = Role.LendingQBRoles;
			m_Roles.DataTextField  = "ModifiableDesc";
			m_Roles.DataValueField = "Id";
			m_Roles.DataBind();

			m_Permissions.DataSource =Enum.GetValues( typeof( Permission )); 
			m_Permissions.DataBind(); 
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
