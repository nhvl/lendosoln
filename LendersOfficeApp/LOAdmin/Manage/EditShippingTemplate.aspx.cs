﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOfficeApp.common.ModalDlg;
using EDocs;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class EditShippingTemplate : LendersOffice.Admin.SecuredAdminPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            ClientScript.GetPostBackEventReference(this, ""); // DO NOT REMOVE THIS LINE.
            RegisterService("main", "/LOAdmin/Manage/EditShippingTemplateService.aspx");
            this.EnableJquery = true;
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string id = RequestHelper.GetSafeQueryString("id");

                if (!String.IsNullOrEmpty(id))
                {
                    int shippingTemplateId = Convert.ToInt32(id);
                    m_PageHeader.Text = "Edit Shipping Template";
                    EDocumentShippingTemplate template = new EDocumentShippingTemplate();
                    m_StackingOrder.DataBind(template.GetShippingTemplateData(shippingTemplateId));
                    m_ShippingTemplateName.Text = template.GetShippingTemplateName(shippingTemplateId);
                    m_ShippingTemplateId.Value = id;
                }
                else
                    m_PageHeader.Text = "Add Shipping Template";
                
                //EDocumentDocType docType = new EDocumentDocType();                
                //foreach (DocType dt in EDocumentDocType.GetDocTypesByBroker(BrokeUserPrinci
                //    m_ddlDocTypes.Items.Add(new ListItem(dt.DocTypeName, dt.DocTypeId));
            }
        }
    }
}
