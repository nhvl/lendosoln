using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Toolbox.Distributed;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Manage
{
	/// <summary>
	/// Summary description for LpeAccessControl.
	/// </summary>
	public partial class LpeAccessControl : LendersOffice.Admin.SecuredAdminPage
	{
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here	
			RetrieveStatus();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

		private void RetrieveStatus()
		{
			ErrMsgEd.Text = "";
			try
			{
				// TODO: Move this code to Toolbox.Distributed.CSingletonDistributed
				using( DbDataReader r = DataAccess.StoredProcedureHelper.ExecuteReader( DataSrc.LOShareROnly, "RetrieveSingletonDistributedStatusActiveOnly",
						   new SqlParameter[] { new SqlParameter( "@ObjId", Toolbox.Distributed.CSingletonDistributed.LPE_DATA_ACCESS_TICKET ) } ) )
				{
					if( r.Read() )
					{
						CurrentBlockerEd.Text = r["HolderId"].ToString();
						HasBlockChk.Checked = true;
						ExpireDEd.Text = r["ExpirationTime"].ToString();
                        StartDEd.Text = r["StartTime"].ToString();
						return;
					}
				}

				CurrentBlockerEd.Text = "";
				HasBlockChk.Checked = false;
				ExpireDEd.Text = "";
                StartDEd.Text = "";
			}
			catch( CBaseException ex )
			{
				ErrMsgEd.Text = "Failed to retrieve block status. " + ex.DeveloperMessage + ex.StackTrace;
			}
			catch( Exception ex )
			{
				ErrMsgEd.Text = "Failed to retrieve block status. " + ex.Message + ex.StackTrace;
			}
		}
		protected void RequestAuthorBlock_Click(object sender, System.EventArgs e)
		{

			string requestorNm = requestorNameEd.Text.TrimWhitespaceAndBOM();
			if( "" == requestorNm )
			{
				ErrMsgEd.Text = "Request failed. Please enter you name";
				return;
			}

			try
			{
				Toolbox.Distributed.CSingletonDistributed.RequestSingletonNoExpiration(
					CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket, requestorNameEd.Text );
				RetrieveStatus();
				ErrMsgEd.Text = "ok";
			}
			catch( CBaseException ex )
			{
				ErrMsgEd.Text = ex.DeveloperMessage + "\r\n" + ex.StackTrace;
			}
			catch( Exception ex )
			{
				ErrMsgEd.Text = ex.Message;
			}
			
		}

		protected void RemoveBlockBtn_Click(object sender, System.EventArgs e)
		{
			try
			{
				Toolbox.Distributed.CSingletonDistributed.RemoveSingletonNoExpiration( 
					CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket );
				RetrieveStatus();
				ErrMsgEd.Text = "ok";
			}
			catch( CBaseException ex )
			{
				ErrMsgEd.Text = ex.DeveloperMessage + "\r\n" + ex.StackTrace;
			}
			catch( Exception ex )
			{
				ErrMsgEd.Text = ex.Message;
			}
		}

		protected void RemoveAnyBlockBtn_Click(object sender, System.EventArgs e)
		{
			try
			{
				Toolbox.Distributed.CSingletonDistributed.RemoveSingletonOfAnyType( 
					CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket );
				RetrieveStatus();
				ErrMsgEd.Text = "ok";
			}
			catch( CBaseException ex )
			{
				ErrMsgEd.Text = ex.DeveloperMessage + "\r\n" + ex.StackTrace;
			}
			catch( Exception ex )
			{
				ErrMsgEd.Text = ex.Message;
			}
		}

		protected void RefreshBtn_Click(object sender, System.EventArgs e)
		{
			RetrieveStatus();
		}
	}
}
