<%@ Page Language="C#" CodeBehind="AdminPanel.aspx.cs" AutoEventWireup="false" Inherits="LendersOffice.Admin.AdminPanel"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Admin Panel" %>

<%@ Register TagPrefix="uc" TagName="TraceLogging" Src="TraceLogging.ascx" %>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style>
         .center-div
        {
            margin: 0 auto;

            width: 60%;
        }
         .text-vertical{
            resize: vertical;
        }
    </style>
    <script>
		function init()
		{
			if( document.getElementById("m_errorMessage") != null )
			{
				document.getElementById("m_Error").innerText = document.getElementById("m_errorMessage").value;
			}
		}
    </script>
    <div class="container-fluid"><div class="row"><div class="center-div"><div class="panel panel-default"><div class="panel-body">
        <div class="form-group">
                <div class="radio"><label class="col-xs-3"><input type="radio" name="Panel" onclick="location.href = 'TrackPanel.aspx';"/>Track</label></div>
                <div class="radio"><label class="col-xs-3"><input type="radio" name="Panel" onclick="location.href = 'EmailPanel.aspx';"/>Email</label></div>
                <div class="radio"><label class="col-xs-3"><input type="radio" name="Panel" checked onclick="location.href = 'AdminPanel.aspx';"/>Admin</label></div>
                <div class="radio"><label class="col-xs-3"><input type="radio" name="Panel" onclick="location.href = 'DBMQManager.aspx';"/>Message Queue</label></div>
        </div>
        <br />
		<form id="AdminPanel" method="post" runat="server">
            <legend>Trace history</legend>
            <uc:TraceLogging id="m_TraceLog" runat="server"></uc:TraceLogging>
		</form>
        </div></div></div></div></div>
</asp:Content>
