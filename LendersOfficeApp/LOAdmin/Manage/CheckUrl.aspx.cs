namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Services;
    using System.Web.UI;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

	/// <summary>
    /// Summary description for CheckUrl.
	/// </summary>
	public partial class CheckUrl : LendersOffice.Admin.SecuredAdminPage
    {
        #region Constants
        /// <summary>
        /// The regular expression pattern used to determine if a URL has specified a protocol, either
        /// HTTP or HTTPS.
        /// </summary>
        private const string ProtocolSearchString = @"^https?:\/\/.+$";

        /// <summary>
        /// The format for displaying the list of headers for a web response.
        /// </summary>
        private const string HeaderPrintFormat = "\t<header key=\"{0}\" value=\"{1}\" />";

        /// <summary>
        /// The format for displaying a message body line for a web response.
        /// </summary>
        private const string BodyLinePrintFormat = "\t<line>{0}</line>";

        /// <summary>
        /// The format for displaying the name of the responding server in the XML file.
        /// </summary>
        private const string ServerPrintFormat = "\t<server>{0}</server>";

        /// <summary>
        /// The format for displaying the response code received from the server.
        /// </summary>
        private const string ResponsePrintFormat = "\t<response>{0}</response>";

        /// <summary>
        /// The XML doctype added to the XML file downloaded form the page.
        /// </summary>
        private const string XmlDoctype = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

        /// <summary>
        /// The name of the XML file that can be downloaded from the page.
        /// </summary>
        private const string XMLFilename = "{0}_result.xml";

        /// <summary>
        /// The format for naming the XML files downloaded from the page.
        /// </summary>
        private const string DatePrintFormat = "MM-dd-yyyy_HH-mm-ss";

        /// <summary>
        /// The error message to display when the server was not able to complete
        /// the request and did not send back a response.
        /// </summary>
        private const string ResponseErrorMessage =
            "An error occurred while testing for connectivity. Please see the stack " +
            "trace below and check with the logs in PB for more information.";

        /// <summary>
        /// The separator used to split the lines from the message header and message
        /// body to write to the XML file.
        /// </summary>
        private readonly string[] LineSeparator = new string[] { Environment.NewLine };
        #endregion

        /// <summary>
        /// Gets or sets a value indicating whether the page completed transferring the XML results file.
        /// </summary>
        private bool HasCompletedFileTransfer { get; set; }

        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // don't load common.js from BasePage
            m_loadDefaultStylesheet = false;
        }

        /// <summary>
        /// Initialize this page.
        /// </summary>
        protected void PageLoad(object sender, System.EventArgs e)
        {
            DownloadBtn.Enabled = false;

            this.HasCompletedFileTransfer = false;

            RespServer.InnerText = System.Environment.MachineName;
        }

        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            if (!this.HasCompletedFileTransfer)
            {
                base.RaisePostBackEvent(sourceControl, eventArgument);
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!this.HasCompletedFileTransfer)
            {
                base.Render(writer);
            } 
        }

        protected void RunCheck_Click(object sender, EventArgs e)
        {
            // We'll set the text content to a blank space because <pre> 
            // tags will render the text "&nbsp" instead of the space the
            // entity represents and we need at least an empty space in a 
            // span for it to render correctly on the page.
            Status.InnerText = " ";

            MsgHeaders.InnerText = " ";

            MsgBody.InnerText = " ";

            var enteredUrl = UrlTextbox.Text;

            if (!Regex.IsMatch(enteredUrl, CheckUrl.ProtocolSearchString, RegexOptions.IgnoreCase))
            {
                // To validate and then create a request, the URI passed
                // to Tools.IsValidUrl must start with http:// or https://, 
                // which may be left off if a URL such as example.com or 
                // www.example.com is specified.
                enteredUrl = "http://" + enteredUrl;
            }

            if (!Tools.IsValidUrl(enteredUrl, Uri.UriSchemeHttp, Uri.UriSchemeHttps))
            {
                HasError.Value = "True";

                return;
            }

            HasError.Value = "False";

            GetAndDisplayResponse(enteredUrl);

            this.Context.ApplicationInstance.CompleteRequest();
        }

        private void GetAndDisplayResponse(string enteredUrl)
        {
            var request = (HttpWebRequest)WebRequest.Create(enteredUrl);
            request.Method = "GET";
            LendersOffice.ObjLib.Security.ThirdParty.ThirdPartyClientCertificateManager.AppendCertificate(request);

            HttpWebResponse response;

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException exc)
            {
                // If we are unable to obtain a 200 OK status from the server,
                // this exception will be thrown. If the response from the 
                // server is not null (such as in the case of an internal server
                // error that returns the 500 status code), we still want to 
                // display the result, so continue with execution.
                if (exc.Response != null)
                {
                    response = (HttpWebResponse)exc.Response;
                }
                else
                {
                    // An error occurred that prevented the server from returning
                    // a response. In this case, we'll show the exception's stack trace
                    // to the user and log it to PB.
                    Status.InnerText = CheckUrl.ResponseErrorMessage;

                    MsgBody.InnerText = exc.ToString();

                    Tools.LogError(exc);

                    return;
                }
            }

            using (response)
            {
                DisplayResponseInfo(response);
            }
        }

        private void DisplayResponseInfo(HttpWebResponse response)
        {
            Status.InnerText = (int)response.StatusCode + " " + response.StatusDescription;

            MsgHeaders.InnerText = BuildHeaderString(response.Headers);

            MsgBody.InnerText = BuildBodyString(response.GetResponseStream());

            DownloadBtn.Enabled = true;
        }

        private string BuildHeaderString(WebHeaderCollection headers)
        {
            var msgHeaderBuilder = new StringBuilder();

            msgHeaderBuilder.AppendLine("<headers>");

            foreach (var key in headers.AllKeys)
            {
                msgHeaderBuilder.AppendLine(string.Format(CheckUrl.HeaderPrintFormat, key, headers.Get(key)));
            }

            msgHeaderBuilder.Append("</headers>");

            return msgHeaderBuilder.ToString();
        }

        private string BuildBodyString(Stream responseStream)
        {
            var msgBodyBuilder = new StringBuilder();

            msgBodyBuilder.AppendLine("<message>");

            using (var sr = new StreamReader(responseStream, detectEncodingFromByteOrderMarks: true))
            {
                while (!sr.EndOfStream)
                {
                    msgBodyBuilder.AppendLine(string.Format(CheckUrl.BodyLinePrintFormat, sr.ReadLine()));
                }
            }

            msgBodyBuilder.Append("</message>");

            return msgBodyBuilder.ToString();
        }

        protected void DownloadBtn_Click(object sender, System.EventArgs e)
        {
            var path = TempFileUtils.NewTempFilePath();

            WriteToFile(path);

            var contentType = "text/xml";

            var filename = string.Format(CheckUrl.XMLFilename, DateTime.Now.ToString(CheckUrl.DatePrintFormat));

            RequestHelper.SendFileToClient(this.Context, path, contentType, filename);

            this.HasCompletedFileTransfer = true;

            this.Context.ApplicationInstance.CompleteRequest();
        }

        private void WriteToFile(string path)
        {
            using (var writer = new StreamWriter(path, false))
            {
                writer.WriteLine(CheckUrl.XmlDoctype);

                writer.WriteLine("<result>");

                writer.WriteLine(string.Format(CheckUrl.ServerPrintFormat, RespServer.InnerText));

                writer.WriteLine(string.Format(CheckUrl.ResponsePrintFormat, Status.InnerText));

                foreach (var header in MsgHeaders.InnerText.Split(this.LineSeparator, StringSplitOptions.RemoveEmptyEntries))
                {
                    writer.WriteLine("\t" + header);
                }

                foreach (var bodyLine in MsgBody.InnerText.Split(this.LineSeparator, StringSplitOptions.RemoveEmptyEntries))
                {
                    writer.WriteLine("\t" + bodyLine);
                }

                writer.WriteLine("</result>");
            }
        }

		override protected void OnInit(EventArgs e)
		{
            //
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);

            // Since we can't "OR" permissions, we will initialize the base 
            // SecuredAdminPage first to figure out which permissions the user 
            // currently has and then redirect them if necessary from there.
            if (!base.HasPermissionToDo(E_InternalUserPermissions.IsDevelopment) &&
                !base.HasPermissionToDo(E_InternalUserPermissions.EditIntegrationSetup))
            {
                base.RedirectRequestor("The IsDevelopment or EditIntegrationSetup permissions are required to view this page.");
            }
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
    }
}
