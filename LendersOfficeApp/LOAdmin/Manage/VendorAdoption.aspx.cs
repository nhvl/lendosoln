﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web.Services;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.GenericFramework;
    using LendersOffice.Integration.Irs4506T;
    using LendersOffice.Integration.MortgageInsurance;
    using LendersOffice.Integration.OCR;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Security;

    /// <summary>
    /// Vendor Adoption.
    /// </summary>
    /// <seealso cref="LendersOffice.Admin.SecuredAdminPage" />
    public partial class VendorAdoption : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Integration Type.
        /// </summary>
        internal enum E_IntergrationType
        {
            [OrderedDescription("4506-T")]
            _4506T = 1,
            Appraisal = 2,
            AUS = 3,
            Compliance = 4,
            Credit = 5,
            Document = 6,
            [OrderedDescription("FHA Connection")]
            FHAConnection = 7,
            Flood = 8,
            [OrderedDescription("Fraud (DRIVE)")]
            Fraud = 9,
            [OrderedDescription("Generic Framework")]
            GenericFramework = 10,
            KIVA = 11,
            [OrderedDescription("Mortgage Insurance")]
            MortgageInsurance = 12,
            OCR = 13,
            [OrderedDescription("Quality Control (EarlyCheck)")]
            QualityControl = 14,
            Symitar = 15,
            Title = 16,
            VOX = 17
        }

        /// <summary>
        /// Gets the style sheet.
        /// </summary>
        /// <value>
        /// The style sheet.
        /// </value>
        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Gets the integration types.
        /// </summary>
        /// <returns>The list of Integration Types.</returns>
        [WebMethod]
        public static List<KeyValuePair<string, string>> GetIntegrationTypes()
        {
            return Enum.GetValues(typeof(E_IntergrationType))
                        .Cast<E_IntergrationType>()
                        .Select(en => new KeyValuePair<string, string>(Convert.ToInt32(en).ToString(), en.GetDescription()))
                        .ToList();
        }

        /// <summary>
        /// Gets the vendors.
        /// </summary>
        /// <param name="integrationId">The integration identifier.</param>
        /// <returns>The list of vendors.</returns>
        [WebMethod]
        public static List<KeyValuePair<string, string>> GetVendors(int integrationId)
        {
            E_IntergrationType en = (E_IntergrationType)integrationId;
            var activeVendors = new List<KeyValuePair<string, string>>();
            switch (en)
            {
                case E_IntergrationType._4506T:
                    activeVendors = Irs4506TVendorConfiguration.ListActiveVendor().Select(t => new KeyValuePair<string, string>(t.VendorId.ToString(), t.VendorName)).ToList();
                    break;
                case E_IntergrationType.Appraisal:
                    activeVendors = AppraisalVendorConfig.ListAll().Select(t => new KeyValuePair<string, string>(t.VendorId.ToString(), t.VendorName)).ToList();
                    break;
                case E_IntergrationType.AUS:
                    break;
                case E_IntergrationType.Compliance:
                    break;
                case E_IntergrationType.Credit:
                    activeVendors = new List<KeyValuePair<string, string>>();
                    using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "ListAllCras"))
                    {
                        while (reader.Read())
                        {
                            Guid comID = (Guid)reader["ComId"];
                            string comNm = (string)reader["ComNm"];
                            activeVendors.Add(new KeyValuePair<string, string>(comID.ToString(), comNm));
                        }
                    }

                    break;
                case E_IntergrationType.Document:
                    activeVendors = DocumentVendorFactory.AvailableVendors().Select(t => new KeyValuePair<string, string>(t.VendorId.ToString(), t.VendorName)).ToList();
                    break;
                case E_IntergrationType.FHAConnection:
                    break;
                case E_IntergrationType.Flood:
                    break;
                case E_IntergrationType.Fraud:
                    break;
                case E_IntergrationType.KIVA:
                    break;
                case E_IntergrationType.GenericFramework:
                    activeVendors = GenericFrameworkVendor.LoadAllVendors().Select(t => new KeyValuePair<string, string>(t.ProviderID.ToString(), t.Name)).ToList();
                    break;
                case E_IntergrationType.MortgageInsurance:
                    activeVendors = MortgageInsuranceVendorConfig.ListActiveVendors().Select(t => new KeyValuePair<string, string>(t.VendorId.ToString(), t.VendorTypeFriendlyDisplay)).ToList();
                    break;
                case E_IntergrationType.OCR:
                    var ocrService = new OCRService(PrincipalFactory.CurrentPrincipal);
                    activeVendors = ocrService.GetVendors().Select(t => new KeyValuePair<string, string>(t.Id.ToString(), t.DisplayName)).ToList();
                    break;
                case E_IntergrationType.QualityControl:
                    break;
                case E_IntergrationType.Symitar:
                    break;
                case E_IntergrationType.Title:
                    activeVendors = TitleVendor.LoadTitleVendors_Light().Select(t => new KeyValuePair<string, string>(t.VendorId.ToString(), t.CompanyName)).ToList();
                    break;
                case E_IntergrationType.VOX:
                    activeVendors = VOXVendor.LoadLightVendors().Select(t => new KeyValuePair<string, string>(t.VendorId.ToString(), t.CompanyName)).ToList();
                    break;
                default:
                    throw new InvalidEnumArgumentException();
            }

            return activeVendors;
        }

        /// <summary>
        /// Searches the specified integration identifier.
        /// </summary>
        /// <param name="integrationId">The integration identifier.</param>
        /// <param name="vendorId">The vendor identifier.</param>
        /// <param name="genericFrameworkProviderId">The generic framework provider identifier.</param>
        /// <param name="serviceVendorId">The service vendor identifier.</param>
        /// <returns>The list of vendor clients.</returns>
        [WebMethod]
        public static List<VendorClient> Search(int integrationId, string vendorId, string genericFrameworkProviderId, string serviceVendorId)
        {
            var clients = new List<VendorClient>();
            E_IntergrationType en = (E_IntergrationType)integrationId;
            var parameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@IntegrationType", System.Data.SqlDbType.VarChar, 50) { Value = en.ToString() },
                        new SqlParameter("@VendorId", System.Data.SqlDbType.UniqueIdentifier) { Value = string.IsNullOrWhiteSpace(vendorId) ? DBNull.Value : (object)new System.Data.SqlTypes.SqlGuid(vendorId) },
                        new SqlParameter("@GenericFrameworkProviderId", System.Data.SqlDbType.VarChar, 7) { Value = string.IsNullOrWhiteSpace(genericFrameworkProviderId) ? DBNull.Value : (object)genericFrameworkProviderId },
                        new SqlParameter("@ServiceVendorId", System.Data.SqlDbType.Int) { Value = string.IsNullOrWhiteSpace(serviceVendorId) ? DBNull.Value : (object)serviceVendorId }
                    };
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShare, "Vendor_Adoption_Get", 3, parameters))
            {
                while (reader.Read())
                {
                    var brokerId = (Guid)reader["BrokerId"];
                    if (en == E_IntergrationType.Symitar)
                    {
                        var brokerDb = BrokerDB.RetrieveById(brokerId);
                        if (!brokerDb.IsSymitarIntegrationEnabled(E_sLoanFileT.Loan) && !brokerDb.IsSymitarIntegrationEnabled(E_sLoanFileT.Test))
                        {
                            continue;
                        }
                    }

                    var vendorClient = new VendorClient();
                    vendorClient.BrokerId = brokerId;
                    vendorClient.Client = (string)reader["Client"];
                    vendorClient.PMLCode = (string)reader["CustomerCode"];
                    clients.Add(vendorClient);
                }
            }

            for (var i = 0; i < clients.Count; i++)
            {
                parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@BrokerId", System.Data.SqlDbType.UniqueIdentifier) { Value = clients[i].BrokerId }
                };
                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShare, "ListAllAdministratorsPerBrokerId", 3, parameters))
                {
                    while (reader.Read())
                    {
                        var adminInfo = new AdminInformation();
                        adminInfo.UserFirstNm = (string)reader["UserFirstNm"];
                        adminInfo.UserLastNm = (string)reader["UserLastNm"];
                        adminInfo.Email = (string)reader["Email"];
                        clients[i].AdminInfo.Add(adminInfo);
                    }
                }

                clients[i].UpdateAdminInfo();
            }

            return clients;
        }

        /// <summary>
        /// Every admin page that inherits from this base page
        /// should implement this query method to specify the
        /// permission set required to access this page.
        /// </summary>
        /// <returns>
        /// List of required permissions.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditIntegrationSetup
            };
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsScript("jquery.tablesorter.min.js");
        }

        /// <summary>
        /// Admin Information.
        /// </summary>
        public class AdminInformation
        {
            /// <summary>
            /// Gets or sets the user first name.
            /// </summary>
            /// <value>
            /// The user first name.
            /// </value>
            public string UserFirstNm { get; set; }

            /// <summary>
            /// Gets or sets the user last name.
            /// </summary>
            /// <value>
            /// The user last name.
            /// </value>
            public string UserLastNm { get; set; }

            /// <summary>
            /// Gets or sets the email.
            /// </summary>
            /// <value>
            /// The email.
            /// </value>
            public string Email { get; set; }
        }

        /// <summary>
        /// Vendor Client.
        /// </summary>
        public class VendorClient
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="VendorClient" /> class.
            /// </summary>
            public VendorClient()
            {
                this.AdminInfo = new List<AdminInformation>();
            }

            /// <summary>
            /// Gets or sets the client.
            /// </summary>
            /// <value>
            /// The client.
            /// </value>
            public string Client { get; set; }

            /// <summary>
            /// Gets or sets the broker identifier.
            /// </summary>
            /// <value>
            /// The broker identifier.
            /// </value>
            public Guid BrokerId { get; set; }

            /// <summary>Gets or sets the PML code.</summary>
            /// <value>The PML code.</value>
            public string PMLCode { get; set; }

            /// <summary>
            /// Gets or sets the admin user.
            /// </summary>
            /// <value>
            /// The admin user.
            /// </value>
            public string AdminUser { get; set; }

            /// <summary>
            /// Gets or sets the admin email.
            /// </summary>
            /// <value>
            /// The admin email.
            /// </value>
            public string AdminEmail { get; set; }

            /// <summary>
            /// Gets or sets the admin information.
            /// </summary>
            /// <value>
            /// The admin information.
            /// </value>
            public List<AdminInformation> AdminInfo { get; set; }

            /// <summary>
            /// Updates the admin information.
            /// </summary>
            public void UpdateAdminInfo()
            {
                this.AdminUser = string.Empty;
                this.AdminEmail = string.Empty;
                for (var i = 0; i < this.AdminInfo.Count; i++)
                {
                    this.AdminUser += this.AdminInfo[i].UserFirstNm + " " + this.AdminInfo[i].UserLastNm;
                    this.AdminEmail += this.AdminInfo[i].Email;
                    if (i < this.AdminInfo.Count - 1)
                    {
                        this.AdminUser += "; ";
                        this.AdminEmail += "; ";
                    }
                }
            }
        }
    }
}