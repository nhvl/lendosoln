﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Integration.Irs4506T;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class ManageIrs4506TVendorsService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                 case "Delete":
                    DeleteVendor();
                    break;
                case "Update":
                    UpdateVendor();
                    break;
                case "Retrieve":
                    RetrieveVendor();
                    break;
            }
        }
        private void RetrieveVendor()
        {
            Guid vendorId = GetGuid("VendorId");

            var vendor = Irs4506TVendorConfiguration.RetrieveById(vendorId);

            SetResult("VendorName", vendor.VendorName);
            SetResult("ExportPath", vendor.ExportPath);
            SetResult("RequiredAccountId", vendor.IsUseAccountId);
            SetResult("EnableConnectionTest", vendor.EnableConnectionTest);
            SetResult("CommunicationModel", vendor.CommunicationModel);
        }
        private void UpdateVendor()
        {
            Guid vendorId = GetGuid("VendorId", Guid.Empty);

            string vendorName = GetString("VendorName");
            string exportPath = GetString("ExportPath");
            bool requiredAccountId = GetBool("RequiredAccountId");
            bool enableConnectionTest = GetBool("EnableConnectionTest");
            Irs4506TCommunicationModel communicationModel = GetEnum<Irs4506TCommunicationModel>("CommunicationModel");


            if (vendorId == Guid.Empty)
            {
                Irs4506TVendorConfiguration.CreateNew(vendorName, exportPath, requiredAccountId, enableConnectionTest, communicationModel);
            }
            else
            {
                var vendor = Irs4506TVendorConfiguration.RetrieveById(vendorId);
                vendor.VendorName = vendorName;
                vendor.ExportPath = exportPath;
                vendor.IsUseAccountId = requiredAccountId;
                vendor.EnableConnectionTest = enableConnectionTest;
                vendor.CommunicationModel = communicationModel;
                vendor.Save();
            }

        }
        private void DeleteVendor()
        {
            Guid vendorId = GetGuid("VendorId");

            // 8/4/2013 dd - Only mark vendor as invalid instead of delete.
            var vendor = Irs4506TVendorConfiguration.RetrieveById(vendorId);
            vendor.IsValid = false;
            vendor.Save();
        }

    }
}
