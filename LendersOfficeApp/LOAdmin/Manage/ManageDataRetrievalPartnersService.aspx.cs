﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Integration.DataRetrievalFramework;
using LendersOffice.XsltExportReport;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class ManageDataRetrievalPartnersService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                 case "Delete":
                    DeletePartner();
                    break;
                case "Update":
                    UpdatePartner();
                    break;
                case "Retrieve":
                    RetrievePartner();
                    break;
            }
        }

        private void DeletePartner()
        {
            Guid partnerID = GetGuid("PartnerId");

            DataRetrievalPartner partner = DataRetrievalPartner.RetrieveById(partnerID);
            partner.Delete();
        }

        private void RetrievePartner()
        {
            Guid partnerID = GetGuid("PartnerId");

            DataRetrievalPartner partner = DataRetrievalPartner.RetrieveById(partnerID);

            SetResult("Name", partner.Name);
            SetResult("ExportPath", partner.ExportPath);
        }

        private void UpdatePartner()
        {
            Guid partnerID = GetGuid("PartnerId", Guid.Empty);

            string sPartnerName = GetString("Name");
            string sExportPath = GetString("ExportPath");

            if (IsInputValid(sPartnerName, sExportPath))
            {
                if (partnerID == Guid.Empty)
                {
                    DataRetrievalPartner.CreatePartner(sPartnerName, sExportPath);
                }
                else
                {
                    DataRetrievalPartner partner = DataRetrievalPartner.RetrieveById(partnerID);
                    partner.Name = sPartnerName;
                    partner.ExportPath = sExportPath;
                    partner.Save();
                }
            }
        }

        private bool IsInputValid(string sPartnerName, string sExportPath)
        {
            bool bIsValid = false;

            if (string.IsNullOrEmpty(sPartnerName))
            {
                throw new CBaseException("Partner Name is required.", "LOAdmin user failed to enter a Data Retrieval Framework Partner Name.");
            }
            else
            {
                if (sExportPath.StartsWith("XSLT_EXPORT:"))
                {
                    // 1/21/2014 dd - Check to see if map name is valid.
                    try
                    {
                        XsltMap.Get(sExportPath.Substring("XSLT_EXPORT:".Length));
                    }
                    catch (NotFoundException)
                    {
                        throw new CBaseException("Xslt Map Name=[" + sExportPath.Substring("XSLT_EXPORT:".Length) + "] could not found.", "Xslt Map Name=[" + sExportPath.Substring("XSLT_EXPORT:".Length) + "] could not found.");
                    }
                }
                else
                {
                    try
                    {
                        Uri exportURI = new Uri(sExportPath);
                    }
                    catch (UriFormatException)
                    {
                        throw new CBaseException("Export Path must be a valid URL.", "LOAdmin user failed to enter a valid Data Retrieval Framework Partner URL.");
                    }
                }
            }

            bIsValid = true;
            return bIsValid;
        }
    }
}
