﻿using System;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Integration.DocumentVendor;

namespace LendersOfficeApp.LOAdmin.Manage
{
    [Obsolete("Please update on LendersOfficeApp.LOAdmin.Manage.ManageDocumentVendors instead")]
    public partial class EditDocumentVendor : LendersOffice.Admin.SecuredAdminPage
    {
        protected bool EditMode
        {
            get { return this.VendorId != Guid.Empty; }
        }

        protected Guid VendorId
        {
            get { return RequestHelper.GetGuid("vendorid", Guid.Empty); }
        }

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return ManageDocumentVendors.ManageDocumentVendorsPermissions;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            if (!this.Page.IsPostBack)
            {
                if (this.EditMode) // Load existing vendor
                {
                    VendorConfig vendor = VendorConfig.Retrieve(VendorId);
                    this.VendorName.Text = vendor.VendorName;
                    this.TestAccountId.Text = vendor.TestAccountId;
                    this.LiveAccountId.Text = vendor.LiveAccountId;

                    this.ExportPath.Text = vendor.PrimaryExportPath.AbsoluteUri;
                    this.BackupExportPath.Text = vendor.BackupExportPath.ToStringOrEmpty();
                    this.TestingExportPath.Text = vendor.TestingExportPath.ToStringOrEmpty();

                    this.PackageDataListJSON.Text = vendor.PackageDataListJSON;
                    this.Parameter.Text = vendor.ParameterName;
                    this.ExtraParams.Text = vendor.ExtraParameters;

                    this.UsesAccountId.Checked = vendor.UsesAccountId;
                    this.UsesPassword.Checked = vendor.UsesPassword;
                    this.UsesUsername.Checked = vendor.UsesUsername;

                    this.SupportsOnlineInterface.Checked = vendor.SupportsOnlineInterface;
                    this.EnableMismo34.Checked = vendor.EnableMismo34;
                    this.SupportsBarcodes.Checked = vendor.SupportsBarcodes;
                    this.DisableManualFulfilmentAddr.Checked = vendor.DisableManualFulfilmentAddr;
                }
            }
        }

        protected void OnSaveClicked(object sender, System.EventArgs e)
        {
            this.RequirePermission(E_InternalUserPermissions.EditIntegrationSetup);

            VendorConfig vendor = this.EditMode ? VendorConfig.Retrieve(this.VendorId) : new VendorConfig();
            vendor.VendorName = this.VendorName.Text;
            vendor.TestAccountId = this.TestAccountId.Text;
            vendor.LiveAccountId = this.LiveAccountId.Text;
            try
            {
                vendor.PrimaryExportPath = new Uri(this.ExportPath.Text);
            }
            catch (UriFormatException)
            {
                m_errorMsg.Text = "Export path must be a valid url";
                return;
            }

            vendor.BackupExportPath = this.BackupExportPath.Text.ToUriOrNull();
            vendor.TestingExportPath = this.TestingExportPath.Text.ToUriOrNull();

            string errorMessage;
            if (!vendor.TrySetPackageDataListJSON(this.PackageDataListJSON.Text, out errorMessage))
            {
                m_errorMsg.Text = errorMessage;
                return;
            }

            vendor.ParameterName = this.Parameter.Text;
            vendor.ExtraParameters = this.ExtraParams.Text;

            vendor.UsesAccountId = this.UsesAccountId.Checked;
            vendor.UsesPassword = this.UsesPassword.Checked;
            vendor.UsesUsername = this.UsesUsername.Checked;

            vendor.SupportsOnlineInterface = this.SupportsOnlineInterface.Checked;
            vendor.EnableMismo34 = this.EnableMismo34.Checked;
            vendor.SupportsBarcodes = this.SupportsBarcodes.Checked;
            vendor.DisableManualFulfilmentAddr = this.DisableManualFulfilmentAddr.Checked;

            VendorConfig.Save(vendor);

            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "OK", "true");
        }
    }
}
