﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditDocumentVendor.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.EditDocumentVendor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
    div
    {
	    padding : 2px;
    }
    .required-error
    {
        color: Red;
    }
    .required-asterix
    {
        color: Red;
    }
    label.header
    {
        font-weight: bold;
        display: block;
    }
    label.option
    {
        display: block;
    }
    </style>
</head>
<body onload="resize(515, 630);">
    <script type="text/javascript">

        jQuery(function($) {
            var $requiredFields = $('.required');

            $requiredFields.after(' <span class="required-asterix">*</span>');

            function validatePage() {
                var len = $requiredFields.length;
                for (var i = 0; i < $requiredFields.length; i++) {
                    var $field = $($requiredFields[i]);
                    if ($field.val() == '') {
                        $j('#m_errorMsg').text('Fill out all required fields.');

                        if (!$field.siblings('.required-error').length > 0) {
                            $field.after("<span class='required-error'>Required</span>");
                        }
                    }
                    else {
                        $field.siblings('.required-error').remove();
                    }
                }

                if ($j('.required-error').length > 0) return false;

                return true;
            }

            window.validatePage = validatePage;
        });
    </script>
    <form id="form1" runat="server">
    <div>
        <ml:EncodedLabel ID="m_errorMsg" runat="server" ForeColor="Red"></ml:EncodedLabel>
    </div>
    <div>
        Integration Partner: <asp:TextBox ID="VendorName" class="required" runat="server" Width="300px"></asp:TextBox>
    </div>
    <div>
        Test Account ID: <asp:TextBox ID="TestAccountId" class="required" runat="server"></asp:TextBox>
    </div>
    <div>
        Live Billing ID: <asp:TextBox ID="LiveAccountId" class="required" runat="server"></asp:TextBox>
    </div>
    <div>
        Primary Export Path: <asp:TextBox ID="ExportPath" class="required" runat="server" Width="360px"></asp:TextBox>
    </div>
    <div>
        Backup Export Path: <asp:TextBox ID="BackupExportPath" runat="server" Width="360px"></asp:TextBox>
    </div>
    <div>
        Testing Environment: <asp:TextBox ID="TestingExportPath" runat="server" Width="360px"></asp:TextBox>
    </div>
    <div>
        Package Information:<br />
        <asp:TextBox ID="PackageDataListJSON" TextMode="MultiLine" style="height:150px; width:300px;" runat="server" />
    </div>
    <div>
        Parameter Name: <asp:TextBox ID="Parameter" runat="server"></asp:TextBox>
    </div>
    <div>
        Extra Parameters: <asp:TextBox ID="ExtraParams" runat="server" Width="360px"></asp:TextBox>
    </div>
    <div>
        <label class="header">Credential Format:</label>
        <label class="option">Account ID <asp:CheckBox ID="UsesAccountId" runat="server" /> </label>
        <label class="option">Username <asp:CheckBox ID="UsesUsername" runat="server" /> </label>
        <label class="option">Password <asp:CheckBox ID="UsesPassword" runat="server" /> </label>
    </div>
    <div>
        <label class="header">Supported Features:</label>
        <label class="option">Online Interface <asp:CheckBox ID="SupportsOnlineInterface" runat="server" /> </label>
        <label class="option">Document Barcoding <asp:CheckBox ID="SupportsBarcodes" runat="server" /> </label>
        <label class="option">Enable MISMO 3.4 <asp:CheckBox ID="EnableMismo34" runat="server" /></label>
    </div>
    <div>
        <label class="header">Custom Configuration:</label>
        <label class="option">Disable manual fulfillment address entry <asp:CheckBox ID="DisableManualFulfilmentAddr" runat="server" /> </label>
    </div>
    <div>
        <asp:Button ID="m_SaveBtn" runat="server" Text="Save" OnClientClick="return validatePage();" OnClick="OnSaveClicked" />
        <input type="button" value="Cancel" onclick="window.close();" />
        <ml:EncodedLabel ID="m_reqFieldLabel" runat="server" ForeColor="Red">* = Required field</ml:EncodedLabel>
    </div>
    </form>
</body>
</html>
