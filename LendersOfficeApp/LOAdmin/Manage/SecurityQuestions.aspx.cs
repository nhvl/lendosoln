﻿using System;
using System.Linq;
using System.Web.Services;
using LendersOffice.Admin;
using System.Collections;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class SecurityQuestions : LendersOffice.Admin.SecuredAdminPage
    {
        [WebMethod]
        public static IEnumerable GetSecureQuestions()
        {
            return SecurityQuestion.ListAllSecurityQuestions(true)
                .Select(q => new { q.IsObsolete, q.Question, q.QuestionId });
        }

        [WebMethod]
        public static int CreateQuestion(string question)
        {
            if (string.IsNullOrEmpty(question)) { throw new ArgumentNullException(nameof(question)); }
            question = question.TrimWhitespaceAndBOM();
            if (string.IsNullOrEmpty(question)) { throw new ArgumentNullException(nameof(question)); }

            return SecurityQuestion.CreateSecurityQuestion(question);
        }

        [WebMethod]
        public static void UpdateQuestion(int questionId, bool isObsolete)
        {
            SecurityQuestion.MarkQuestionObsolete(questionId, isObsolete);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterResources();
        }

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.Manage.SecurityQuestions.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
    }
}
