﻿<%@ Page Language="C#" CodeBehind="EDMSFaxPanel.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Manage.EDMSFaxPanel"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="EDMS Fax Tools" ValidateRequest="false" %>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style>
         .center-div
        {
            margin: 0 auto;
            width: 70%;
        }
         .expand-span{
             width:100%;
         }
         .notif{
             resize: none;
         }
         textarea[readonly]{
             background-color:white !important;
         }
    </style>
    <form id="form1" runat="server" class="form-horizontal form-horizontal-left">
    <div class="container-fluid"><div class="row"><div class="center-div"><div class="panel panel-default"><div class="panel-body">
        <div class="form-group">
            <div class="col-xs-12">
    <asp:TextBox AutoComplete="off" CssClass="form-control input notif" runat="server" id="m_response" Rows="10" TextMode="MultiLine" ReadOnly="true" Height="150px" Visible="false" EnableViewState="false" />
                </div>
            </div>
        <div class="form-group">
            <label class="col-xs-4">PDF (leave blank for generic test file)</label>
                <asp:FileUpload runat="server" ID="FileUpload" accept="application/pdf"/>
        </div>    
        <legend>Barcode Data</legend>
        <p class="help-block">(It will be processed as CP barcode if CP request id exists)</p>
        <div class="form-group">
            <label class="control-label col-xs-3">DocType</label>
            <div class="col-xs-2">
                <asp:TextBox AutoComplete="off" ID="m_docTypeTb" runat="server" MaxLength="4" CssClass="form-control input" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3">Consumer Portal Request Id</label>
            <div class="col-xs-2">
                <asp:TextBox AutoComplete="off" ID="m_cPid" runat="server" MaxLength="38" CssClass="form-control input" />
            </div>
            <label class="control-label col-xs-3">Broker ID (for CP Request ID DB)</label>
            <div class="col-xs-3">
                <asp:TextBox AutoComplete="off" ID="brokerIdForCP" runat="server" MaxLength="36" CssClass="form-control input" />
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-3">LoanId</label>
            <div class="col-xs-7">
                <asp:TextBox AutoComplete="off" ID="m_loanId" runat="server" MaxLength="38" CssClass="form-control input" /> (not required if line above is filled out)
            </div>
        </div>
         <div class="form-group">
            <label class="control-label col-xs-3">App Id</label>
            <div class="col-xs-7">
                <asp:TextBox AutoComplete="off" ID="m_appId" runat="server" MaxLength="38" CssClass="form-control input" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3">Description</label>
            <div class="col-xs-9">
                <asp:TextBox AutoComplete="off" ID="m_descriptionTB" runat="server" MaxLength="250" CssClass="form-control input" />
            </div>
        </div>
        <legend>Post Data</legend>
        <div class="form-group">
            <label class="control-label col-xs-2">Login</label>
            <div class="col-xs-3">
                <asp:TextBox AutoComplete="off" ID="m_loginTb" runat="server" MaxLength="50" CssClass="form-control input" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-2">Password</label>
            <div class="col-xs-3">
                <asp:TextBox AutoComplete="off" ID="m_passwordTb" runat="server" MaxLength="50" CssClass="form-control input" />
            </div>
        </div>
        <legend> Action</legend>
        <div class="form-group">
            <div class="col-xs-2">
                <span class="clipper">
                    <asp:Button id="postBtn" runat="server" Text="Post to address:" CssClass="btn btn-default" OnClick="PostClick" />
                </span>
            </div>
            <div class="col-xs-4">
                <asp:TextBox AutoComplete="off" ID="m_urlToPostTb" runat="server" CssClass="form-control input" />
            </div>
            <div class="col-xs-2">
                <label class="control-label pull-right">Timeout (ms)</label>
            </div>
            <div class="col-xs-2">
                <asp:TextBox AutoComplete="off" ID="m_timeOutTb" runat="server" MaxLength="10" CssClass="form-control input" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
            <span class="clipper">
                <asp:Button id="postObsoleteBtn" runat="server" Text="Post in old eDocs format" CssClass="btn btn-default" OnClick="PostObsoleteClick" />
            </span>
            <span class="clipper">
                <asp:Button id="downloadPdf" runat="server" Text="Download coversheet with doc" CssClass="btn btn-default" OnClick="DownloadPdfClick" />
            </span>
            <span class="clipper">
                <asp:Button id="Button1" runat="server" Text="Download coversheet only" CssClass="btn btn-default" OnClick="DownloadPdfCoverClick" />
            </span>
            </div>
        </div>
        <legend>Testing Actions</legend>
        <div class="form-group">
             <label class="control-label col-xs-1">Id</label>
            <div class="col-xs-7">
                <asp:TextBox AutoComplete="off" ID="m_lookupId" runat="server" Width="200px" CssClass="form-control input" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-3">
                <span class="clipper expand-span">
                    <asp:Button ID="pullusingFileDB" runat="server" Text="Get fileDB PDF" CssClass="btn btn-default" OnClick="PullFileDbClick" Width="100%"/>
                </span>
            </div>
                <label class="control-label">Download a PDF directly from filedb by this filedb Id.</label>
        </div>
        <div class="form-group">
            <div class="col-xs-3">
                <span class="clipper expand-span">
                    <asp:Button ID="pullusingRequest" runat="server" Text="Get CP PDF" CssClass="btn btn-default" OnClick="PullRequestClick" Width="100%"/>
                </span>
                </div>
                <label class="control-label">Download a PDF directly from filedb by this Consumer Portal request Id.</label>
        </div>
        <div class="form-group">
            <div class="col-xs-3">
                <span class="clipper expand-span">
                    <asp:Button ID="pullFaxCover" runat="server" Text="Get CP Fax Cover" CssClass="btn btn-default" OnClick="FaxCoverClick" Width="100%"/>
                </span>
                </div>
                <label class="control-label">Download the PDF fax coversheet or fax package by this Consumer Portal request Id.</label>
        </div>
        <div class="form-group">
            <div class="col-xs-3">
                <span class="clipper expand-span">
                    <asp:Button ID="simulateFaxToId" runat="server" Text="Simulate CP Fax" CssClass="btn btn-default" OnClick="SimulateFaxClick" Width="100%"/>
                </span>
                </div>
                <label class="control-label">Attach the PDF file to Consumer Portal request by this CP request Id as if it were an incoming fax.</label>
        </div>
        </div></div></div></div></div>
    </form>
</asp:Content>