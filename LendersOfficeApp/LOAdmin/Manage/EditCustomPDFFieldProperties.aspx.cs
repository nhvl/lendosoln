﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.CustomFormField;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class EditCustomPDFFieldProperties : LendersOffice.Admin.SecuredAdminPage
    {
        private string FieldID
        {
            get { return RequestHelper.GetSafeQueryString("id"); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SaveButton.Style.Add("display", "none");
                CancelButton.Style.Add("display", "none");

                FormField item = FormField.RetrieveById(FieldID);
                Id.Text = item.Id;
                Description.Text = item.Description;
                FriendlyName.Text = item.FriendlyName;
                IsReviewNeeded.Checked = !item.IsReviewNeeded;
                IsHidden.Checked = item.IsHidden;
                CategoryName.DataSource = FormFieldCategory.ListAll();
                CategoryName.DataTextField = "Name";
                CategoryName.DataValueField = "Id";
                CategoryName.DataBind();
                CategoryName.SelectedValue = item.Category.Id.ToString();
            }
        }

        protected void OnSaveButtonClicked(object sender, EventArgs e)
        {
            FormField item = FormField.RetrieveById(FieldID);
            item.Description = Description.Text;
            item.FriendlyName = FriendlyName.Text;
            item.IsReviewNeeded = !IsReviewNeeded.Checked;
            item.IsHidden = IsHidden.Checked;
            foreach (FormFieldCategory ffc in FormFieldCategory.ListAll())
            {
                if (ffc.Id == Convert.ToInt32(CategoryName.SelectedValue))
                {
                    item.Category = ffc;
                    break;
                }
            }

            item.Save();
            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this);
        }
    }
}
