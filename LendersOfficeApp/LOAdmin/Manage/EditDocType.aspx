﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditDocType.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.EditDocType" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Edit Doc Type</title>
	<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
    <script language="javascript">
        function _init() {
            resize(430, 170);
            document.getElementById('DocTypeName').focus();
        }

        function clearError() {
            document.getElementById('m_lblError').innerText = '';
        }
    </script>
</head>
<body>
    <h4 class="page-header">Add Doc Type</h4>
    <form id="form1" runat="server">
        <table width="100%" border="0" cellspacing="2" cellpadding="3">
			<tr>
			    <td noWrap>
			        <br />
			        <strong>Doc Type Name:</strong>
			        <input type="text" runat="server" ID="DocTypeName" onkeyup="clearError();" MaxLength="50" style="width:70%" />
			        <asp:RequiredFieldValidator ID="m_DocTypeNameValidator" runat="server" ControlToValidate="DocTypeName">
                        <img runat="server" src="../../images/error_icon.gif">
			        </asp:RequiredFieldValidator>
			    </td>
			</tr>
			<tr>
			    <td>
			        <ml:EncodedLabel ID="m_lblError" runat="server" ForeColor="Red"></ml:EncodedLabel>&nbsp;
			    </td>
			</tr>
			<tr>
			    <td align="left">
			        <asp:Button ID="m_btnSave" runat="server" Text="Save" OnClick="OnSaveBtnClicked" />
			        <input type="button" value="Cancel" onclick="onClosePopup();" />
			    </td>
			</tr>
		</table>	
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
</body>
</html>
