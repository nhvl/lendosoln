﻿<%@ Page Language="C#" CodeBehind="~/LOAdmin/Manage/LostEDocs.aspx.cs" MasterPageFile="~/LOAdmin/loadmin.Master" Inherits="LendersOfficeApp.LOAdmin.Manage.LostEDocs" Title="Browse Lost Electronic Documents"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="EDocs" %>
<%@ Import Namespace="DataAccess" %>
<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
    <script>

    $('#PMLContain').ready(function(){

        $('#pagiItem.2').find("a:first").text("...");
        $('#pagiItem.1').addClass("active");

        $('#prev').bind('click',function(){
            eval($("#pagiItem.active").prev("li").find("a:first").attr('href'))
        });
        $('#next').bind('click',function(){
            eval($("#pagiItem.active").next("li").find("a:first").attr('href'))
        });
       
       if ( $('#registerTable tbody').children().length==0)
        {
            $('#registerTable').hide();
        }
        else{
            $('#emptyTableNote').hide();
        }
    });
    function view(docid) {
        window.open('ViewEDocPdf.aspx?a=1&id=' + docid, '_parent');
        window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/LOAdmin/Manage/ViewEdocPdf.aspx"))) %> + '?a=1&id=' + docid, '_parent');
     }
    
</script>
  <div class="container">
    <div class="row">  
        <div class="col-xs-6 col-xs-offset-3 ">
            <div class="panel panel-default col-xs-10 col-xs-offset-1">
              <div class="panel-body">
                <form id="Form1" runat="server" >
                    <div id="pagiGroup" class="text-center">
                      <asp:Repeater id="pagingRepeater" runat="server" >
                        <HeaderTemplate > 
                          <ul id="pagi" class="pagination  pagination-sm" >
                            <li ><a id="prev" >Prev</a></li>
                        </HeaderTemplate > 
                        <ItemTemplate>
                            <li id="pagiItem" class = '<%# AspxTools.HtmlString( ((System.Tuple<int, int>)Container.DataItem).Item2 )%>'>
                                <asp:LinkButton  runat="server"  OnClick="PageNumUpdating"  CommandArgument='<%# AspxTools.HtmlString (((System.Tuple<int, int>)Container.DataItem).Item1 )%>' Text='<%# AspxTools.HtmlString (((CommonProjectLib.Common.Lib.Tuple<int, int>)Container.DataItem).Item1 )%>'></asp:LinkButton>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            <li  ><a id="next">Next</a></li>
                          </ul>
                        </FooterTemplate>
                      </asp:Repeater>
                    </div>
                     <div class=" col-xs-10 col-xs-offset-1">
                        <asp:Repeater id="edocRepeater" runat="server" >
                          <HeaderTemplate > 
                            <table id="registerTable" class=" table table-striped table-condensed">
                                <thead>
                                  <tr>
                                    <th class ="text-right col-md-7">Creation Date</th>
                                    <th></th>
                                  </tr>
                                </thead>
                                 <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                  <tr>
                                    <td class ="text-right col-md-7"><%# AspxTools.HtmlString( ((System.Tuple<Guid, string>)Container.DataItem).Item2 )%></td>
                                     <td class ="text-center col-md-5"><a  href="javascript:void(0);" onclick="view(<%# AspxTools.JsString( ((System.Tuple<Guid, string>)Container.DataItem).Item1 ) %>);" >view</a></td>
                                  </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                              </table>
                              <p id="emptyTableNote" class ="text-center" ><em>0 records found</em></p>
                             </FooterTemplate>  
                        </asp:Repeater>
                      </div>
                      
                </form>   
              </div>
            </div>
        </div> 
    </div>
  </div>
</asp:Content>
