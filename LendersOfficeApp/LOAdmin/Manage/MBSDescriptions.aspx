﻿<%@ Page Language="C#" CodeBehind="~/LOAdmin/Manage/MBSDescriptions.aspx.cs" MasterPageFile="~/LOAdmin/loadmin.Master" Inherits="LendersOfficeApp.LOAdmin.Manage.MBSDescriptions" Title="MBS Descriptions"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
  <div class="container">
    <div class="row"> 
        <!--[if IE 9]>  
            <script>
            function validateInput (){
                var xs = $("input,textarea,select")
                    .filter(":visible")
                    .filter(function () {
                    var $input = $(this);
                    return (($input.prop("required") === true) ||
                        ($input.data("required") == true));
                });
                xs.tooltip("destroy");
                var ys = xs.filter(function (i, x) { return !(x.value); });
                if (ys.length > 0) {
                    var y = ys.first();
                    y.tooltip({ title: "Required." });
                    y.focus();
                    y.one("change blur input", function () {
                        $(this).tooltip("destroy");
                    });
                    return false;
                }
                        var y = $("div[id*='inputGroup']");
                        y.addClass("has-error");
                         y.tooltip({ title: "Required." });
                            y.focus();
                            y.one("change blur input", function () {
                                $(this).tooltip("destroy");
                            });
            }
            </script> 
            
        <![endif]--> 
        <!--[if !IE]> -->
             <script>
                 function validateInput (){
                    return true;
                 }
            </script> 
        <!-- <![endif]-->
        <div class="col-xs-4 col-xs-offset-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <form id="Form1" runat="server">
                  <div class="col-xs-10 col-xs-offset-1">
                      <div class="form-group">
                        <div>
                          <label class=" control-label ">MBS Description:</label> <span id="error" class="text-danger">*</span>
                        </div>
                        <div >
                          <div id="inputGroup" class="input-group">
                            <asp:TextBox  required AutoComplete="off" class="form-control input-sm" ID="NewDescription" runat="server" MaxLength="50" data-required="true"></asp:TextBox>
                            <script>
                                    $(function() {$("input[id*=NewDescription").focus();});
                            </script>
                            <span class="input-group-btn">
                              <span class="clipper">
                                <asp:Button  class="btn btn-sm btn-primary" OnClientClick="return preAddDesc();" OnClick="OnAddClicked" ID="AddDescription" Text="Add" runat="server" ></asp:Button>
                              </span>
                            </span> 
                          </div>
                        </div>
                      </div>
                      <div >
                        <asp:Repeater  id="descRepeater" runat="server" >
                          <HeaderTemplate > 
                            <table  class="table table-striped table-condensed   ">
                                <thead>
                                  <tr>
                                    <th>Description</th>
                                    <th></th>
                                  </tr>
                                </thead>
                                 <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                  <tr>
                                    <td><%# AspxTools.HtmlString( ((System.Tuple<Guid, string>)Container.DataItem).Item2 )%></td>
                                    <td><input type="button" ID="Delete" class="btn btn-link" onclick=<%# AspxTools.HtmlAttribute("openDeleteDialog("+ AspxTools.JsString(((System.Tuple<Guid, string>)Container.DataItem).Item1) + ")")%>  value="delete"/></td>
                                  </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                               </table>
                             </FooterTemplate>
                        </asp:Repeater>
                      </div>
                      <script>
                       function preAddDesc ()
                        {
                            var inputItem = $("input[id*=NewDescription]");
                            if($.trim(inputItem.val()).length == 0){
                                inputItem.tooltip("destroy");
                                inputItem.tooltip({ title: "Need at least one character." });
                                inputItem.focus();
                                inputItem.one("change blur input", function () {
                                    $(this).tooltip("destroy");
                                });
                                return false;
                            }
                            return validateInput();
                       }
                       function openDeleteDialog(key) {
                           $("input[id*='hiddenKey']").val(key);
                           $("#deleteModal").modal('show');
                       };
                      </script>
                  </div>
                    <asp:HiddenField id="hiddenKey" runat="server" value=""/>
                    <div id="deleteModal" class="modal v-middle fade " >
                        <div class="modal-dialog">
                            <div class="modal-content  col-xs-6 col-xs-offset-3">
                                <div class="modal-body">Delete ?</div>
                                <div class="modal-footer">
                                    <span class="clipper"><input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel"/></span>
                                    <span class="clipper"><asp:LinkButton OnClick="OnDeleteClicked" class="btn btn-primary" Text="OK"  runat="server" /></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>   
              </div>
            </div>
        </div> 
    </div>
  </div>
</asp:Content>
