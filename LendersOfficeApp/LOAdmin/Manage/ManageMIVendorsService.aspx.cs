﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.MortgageInsurance;

    public partial class ManageMIVendorsService : BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Delete":
                    DeleteVendor();
                    break;
                case "Update":
                    UpdateVendor();
                    break;
                case "Retrieve":
                    RetrieveVendor();
                    break;
            }
        }

        private void RetrieveVendor()
        {
            Guid vendorId = GetGuid("VendorId");

            MortgageInsuranceVendorConfig vendor = MortgageInsuranceVendorConfig.RetrieveById(vendorId);

            SetResult("VendorType", vendor.VendorType);
            SetResult("ExportPath_Test", vendor.ExportPath_Test);
            SetResult("ExportPath_Production", vendor.ExportPath_Production);
            SetResult("RequiredAccountId", vendor.IsUseAccountId);
            SetResult("SendAuthTicketWithOrders", vendor.SendAuthTicketWithOrders);
            SetResult("EnableConnectionTest", vendor.EnableConnectionTest);
            SetResult("UsesSplitPremiumPlans", vendor.UsesSplitPremiumPlans);
            SetResult("SplitPremiumOptions", SerializationHelper.JsonNetAnonymousSerialize(vendor.SplitPremiumOptions.Select(o => o.ToView()).ToArray()));
        }

        private void UpdateVendor()
        {
            Guid vendorId = GetGuid("VendorId", Guid.Empty);

            E_sMiCompanyNmT vendorType = (E_sMiCompanyNmT)GetInt("VendorType");
            string exportPath_Test = GetString("ExportPath_Test");
            string exportPath_Production = GetString("ExportPath_Production");
            bool requiredAccountId = GetBool("RequiredAccountId");
            bool sendAuthTicketWithOrders = GetBool("SendAuthTicketWithOrders");
            bool enableConnectionTest = GetBool("EnableConnectionTest");
            bool usesSplitPremiumPlans = GetBool("UsesSplitPremiumPlans");

            List<MISplitPremiumOption> splitPremiumOptions = null;
            if (usesSplitPremiumPlans)
            {
                string splitPremiumOptionsRaw = GetString("SplitPremiumOptions");
                var splitPremiumViewList = SerializationHelper.JsonNetDeserialize<List<MISplitPremiumOptionView>>(splitPremiumOptionsRaw);

                splitPremiumOptions = new List<MISplitPremiumOption>();
                foreach (var view in splitPremiumViewList)
                {
                    decimal upfrontPercentage;
                    if (!decimal.TryParse(view.UpfrontPercentage, out upfrontPercentage))
                    {
                        this.SetResult("Success", false);
                        this.SetResult("Error", "All upfront percentage values must be valid decimals.");
                        return;
                    }

                    splitPremiumOptions.Add(new MISplitPremiumOption(upfrontPercentage, view.MismoValue));
                }

                if (!splitPremiumOptions.Any())
                {
                    this.SetResult("Success", false);
                    this.SetResult("Error", "If the vendor offers split premium plans, at least one option must be defined.");
                    return;
                }

                foreach (var option in splitPremiumOptions)
                {
                    if (option.UpfrontPercentage == 0.00m)
                    {
                        this.SetResult("Success", false);
                        this.SetResult("Error", "An Upfront Percentage option cannot be blank or have a value of 0%");
                        return;
                    }
                    else if (splitPremiumOptions.Where(o => o.UpfrontPercentage == option.UpfrontPercentage).Count() > 1)
                    {
                        this.SetResult("Success", false);
                        this.SetResult("Error", "Upfront Percentage values cannot appear twice. Please input unique values.");
                        return;
                    }
                    else if (!Regex.IsMatch(option.MismoValue, @"^SplitPremium[0-9]{0,2}$"))
                    {
                        this.SetResult("Success", false);
                        this.SetResult("Error", "MISMO values must be of the form \"SplitPremiumXX\"");
                        return;
                    }
                    else if (splitPremiumOptions.Where(o => o.MismoValue == option.MismoValue).Count() > 1)
                    {
                        this.SetResult("Success", false);
                        this.SetResult("Error", "MISMO key values cannot appear twice. Please input unique values.");
                        return;
                    }
                }
            }

            if (vendorId == Guid.Empty)
            {
                MortgageInsuranceVendorConfig vendor = new MortgageInsuranceVendorConfig(
                    vendorType,
                    exportPath_Test,
                    exportPath_Production,
                    requiredAccountId,
                    sendAuthTicketWithOrders,
                    enableConnectionTest,
                    usesSplitPremiumPlans,
                    splitPremiumOptions);

                vendor.Save();
            }
            else
            {
                MortgageInsuranceVendorConfig vendor = MortgageInsuranceVendorConfig.RetrieveById(vendorId);
                vendor.VendorType = vendorType;
                vendor.ExportPath_Test = exportPath_Test;
                vendor.ExportPath_Production = exportPath_Production;
                vendor.IsUseAccountId = requiredAccountId;
                vendor.SendAuthTicketWithOrders = sendAuthTicketWithOrders;
                vendor.EnableConnectionTest = enableConnectionTest;
                vendor.UsesSplitPremiumPlans = usesSplitPremiumPlans;
                vendor.SplitPremiumOptions = splitPremiumOptions;
                vendor.Save();
            }

            this.SetResult("Success", true);
        }

        private void DeleteVendor()
        {
            Guid vendorId = GetGuid("VendorId");
            var vendor = MortgageInsuranceVendorConfig.RetrieveById(vendorId);
            vendor.IsValid = false;
            vendor.Save();
            this.SetResult("Success", true);
        }
    }
}