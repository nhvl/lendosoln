namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Web.Security;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public partial class SiteConfigEditService : BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "GetHashPassword":
                    GetHashPassword();
                    break;
                case "CreateFriendlyId":
                    CreateFriendlyId();
                    break;
                case "MoveAuditTrail":
                    MoveAuditTrail();
                    break;
                case "GenerateRandomKey":
                    GenerateRandomKey();
                    break;
                case "Decrypt":
                    Decrypt();
                    break;
                case "DecryptCookie":
                    DecryptCookie();
                    break;
                case "EncryptSqlConnection":
                    EncryptSqlConnection();
                    break;
                case "Encrypt":
                    Encrypt();
                    break;
                case "RestartPdfConversionProcess":
                    RestartPdfConversionProcess();
                    break;
                case "GetSaltedHash":
                    GetSaltedHash();
                    break;
                case nameof(GenerateVendorClientCert):
                    this.GenerateVendorClientCert();
                    break;
            }
        }

        private void EncryptSqlConnection()
        {
            string conn = GetString("ConnectionString");
            if (string.IsNullOrEmpty(conn))
            {
                SetResult("Value", "");
                return;
            }
            try
            {
                SetResult("ConnectionString", DbAccessUtils.EncryptSqlConnection(conn));
            }
            catch (System.ArgumentException)
            {
                SetResult("ConnectionString", "");
            }
        }
        private void DecryptCookie()
        {
            string data = GetString("Cookie");
            if (string.IsNullOrEmpty(data))
            {
                SetResult("DecryptedCookie", "");
                return;
            }
            try
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(data);

                 SetResult("DecryptedCookie", ticket.UserData);
            }
            catch (ArgumentException)
            {
                SetResult("DecryptedCookie", "");
            }
        }

        private void Encrypt()
        {
            string data = GetString("EncValue");
            if (string.IsNullOrEmpty(data))
            {
                SetResult("Value", "");
            }

            SetResult("Value", EncryptionHelper.Encrypt(data));
        }
        private void Decrypt()
        {
            string data = GetString("value");
            if( string.IsNullOrEmpty(data)  )
            {
                SetResult("Value", "");
                return;
            }
            try{
                SetResult("Value",EncryptionHelper.Decrypt(data));
            }
            catch (FormatException)
            {
                SetResult("Value", "");
            }
        }
        private void MoveAuditTrail() 
        {
            // 10/7/2005 dd - Temporary place to move daily audit to FileDB
            List<Guid> list = new List<Guid>();
            
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListLoanIdInAuditTrail", null))
                {
                    while (reader.Read())
                    {
                        list.Add((Guid)reader["LoanId"]);
                    }
                }
               
            }

            foreach (Guid id in list) 
            {
                try
                { 
                    LendersOffice.Audit.AuditManager.MoveToPermanentStorage(id);
                }
                catch(LoanNotFoundException)
                {
                    SetResult("Status", "Error: Loan not found");
                    return;
                }
                SetResult("Status", "Done");
            }
        }
        private void GetSaltedHash()
        {
            string password = GetString("Password");
            string salt;
            string hash = EncryptionHelper.GeneratePBKDF2Hash(password, out salt);

            SetResult("Hash", hash);
            SetResult("Salt", salt);
        }
        private void GetHashPassword() 
        {
            string password = GetString("Password");
            SetResult("HashPassword", Tools.HashOfPassword(password));
        }
        private void CreateFriendlyId() 
        {
            CPmlFIdGenerator idGen = new CPmlFIdGenerator();
            SetResult("FriendlyId", idGen.GenerateNewFriendlyId());
        }
        private void GenerateRandomKey() 
        {
            int len = GetInt("KeySizeBytes");
            byte[] buffer = new byte[len];

            System.Security.Cryptography.RNGCryptoServiceProvider rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            rng.GetBytes(buffer);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < buffer.Length; i++) 
            {
                sb.Append(string.Format("{0:X2}", buffer[i]));
            }
            SetResult("Keys", sb.ToString());
        }
        private void RestartPdfConversionProcess()
        {
            Tools.RestartPdfConversionProcess();
        }

        private void GenerateVendorClientCert()
        {
            var vendorName = this.GetString("VendorName");
            var certificatePassword = MultiFactorAuthCodeUtilities.GenerateRandom(6);
            var certificateId = Guid.NewGuid();

            var certificateBytes = ClientCertificateUtilities.GenerateClientCertificate(certificateId, certificatePassword, vendorName, ClientCertificateType.Vendor);

            var fileDbKey = Guid.NewGuid() + "_vendorcert";
            FileDBTools.WriteData(E_FileDB.Temp, fileDbKey, certificateBytes);

            this.SetResult("CertificateKey", fileDbKey);
            this.SetResult("CertificateId", certificateId);
            this.SetResult("CertificatePassword", certificatePassword);
        }
    }
}
