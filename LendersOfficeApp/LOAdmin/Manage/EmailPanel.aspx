<%@ Register TagPrefix="UC" TagName="Header" Src="../Common/Header.ascx" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/Common/HeaderNav.ascx" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="EmailPanel.aspx.cs" AutoEventWireup="false" Inherits="LendersOffice.Admin.EmailPanel" ValidateRequest="False" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Email Panel</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </HEAD>
	<body MS_POSITIONING="FlowLayout" onload="init();">
		<script type="text/javascript">
			function init()
			{
				if( document.getElementById("m_errorMessage") != null )
				{
					document.getElementById("m_Error").innerText = document.getElementById("m_errorMessage").value;
				}
			}		
		</script>
		<form id="EmailPanel" method="post" runat="server">
			<uc:Header runat="server" id=Header1>
			</uc:Header>
			<uc:HeaderNav runat="server" id=HeaderNav1>
				<MenuItem URL="~/LOAdmin/Main.aspx" Label="Main">
				</MenuItem>
				<MenuItem URL="~/LOAdmin/Manage/EmailPanel.aspx" Label="Email Control Panel">
				</MenuItem>
			</uc:HeaderNav>
			<table style="WIDTH: 10in" cellspacing="0" cellpadding="0" border="0">
			<tr>
			<td class="FormTable">
				<br>
				<table style="WIDTH: 100%" cellspacing="0" cellpadding="0">
				<tr>
				<td class="FieldLabel" align="left">
					<input type="radio" name="Panel" onclick="location.href = 'TrackPanel.aspx';">
					Track
					<input type="radio" name="Panel" checked onclick="location.href = 'EmailPanel.aspx';">
					Email
					<input type="radio" name="Panel" onclick="location.href = 'AdminPanel.aspx';">
					Admin
					<input type="radio" name="Panel"  onclick="location.href = 'DBMQManager.aspx';">
					Message Queue
				</td>
				<td align="right">
					<input type="button" value="Back" onclick="location.href = '../Main.aspx';">
				</td>
				</tr>
				</table>
				<hr>
				<div style="BORDER-RIGHT: lightgrey 2px solid; PADDING-RIGHT: 0px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 100%; PADDING-TOP: 0px; BORDER-BOTTOM: lightgrey 2px solid">
					<table cellpadding="4" cellspacing="0" border="0" width="100%">
					<tr class="FormTableHeader">
					<td>
						Email control panel
					</td>
					</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0">
					<tr>
					<td>
						<table cellpadding="0" cellspacing="8" border="0">
                            <tr>
                                <td class="FieldLabel" nowrap>QWait (ms)</td>
                                <td>
                                    <asp:TextBox ID="m_QWait" runat="server" Style="padding-left: 4px" ReadOnly="True" /></td>
                            </tr>
                            <tr>
						<td class="FieldLabel" nowrap>
							Server Clock
						</td>
						<td>
							<asp:TextBox id="m_ServerClock" runat="server" style="PADDING-LEFT: 4px" ReadOnly="True" />
						</td>
						</tr>
						<tr> 
							<td class="FieldLabel" nowrap> 
								Is Archiving
							</td>
							
							<td>
								<asp:TextBox ID="m_IsArchive" Runat="server" style="PADDING-LEFT: 4px" ReadOnly="True"/>
							</td>
						</tr>
						</table>
					</td>
					<td>
						<table cellpadding="0" cellspacing="8" border="0">
						<tr>
						<td class="FieldLabel" nowrap>
							<asp:Button id="m_Clear" runat="server" Text="Clear" OnClick="ClearClick">
							</asp:Button>
							<asp:TextBox id="m_Address" runat="server" style="PADDING-LEFT: 4px" Width="60">
							</asp:TextBox>
							<asp:Button id="m_Test" runat="server" Text="Test" OnClick="TestClick"/>
							<asp:Button id="m_ToggleArchive" runat="server" Text="Toggle Archive" OnClick="ArchiveClick"/>
							<asp:Button ID="m_ClearArchive" Runat="server" Text="Clear Archive" OnClick="ClearArchiveClick"/>
						</td>
						<td nowrap>
							<asp:Button id="m_Refresh" runat="server" Text="Refresh">
							</asp:Button>
						</td>
						<td>
							<div id="m_Error" style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; FONT: 12px arial; COLOR: red; PADDING-TOP: 8px">
							</div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</div>
				<div style="BORDER-RIGHT: lightgrey 2px solid; PADDING-RIGHT: 4px; BORDER-TOP: lightgrey 2px solid; MARGIN-TOP: 4px; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 100%; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; HEIGHT: 334px">
					<ml:CommonDataGrid id="m_Grid" runat="server" CellSpacing="0" CellPadding="2" OnItemCommand="GridItemClick" EnableViewState="True">
					<Columns>
						<asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<asp:LinkButton runat="server" CommandName="Drop" CommandArgument="<%#AspxTools.HtmlString(Container.DataItem)%>">
									drop
								</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<asp:LinkButton runat="server" CommandName="Send" CommandArgument="<%#AspxTools.HtmlString(Container.DataItem)%>">
									send
								</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="Label" HeaderText="Label">
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Retry" HeaderText="Retry">
						</asp:BoundColumn>
						<asp:TemplateColumn HeaderText="From">
							<ItemTemplate>
								<%#AspxTools.HtmlString( DataBinder.Eval( Container.DataItem , "From" ).ToString() ) %>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="To">
							<ItemTemplate>
                                <%# AspxTools.HtmlString(((LendersOffice.Email.EmailEntry)Container.DataItem).To) %>
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
					</ml:CommonDataGrid>
				</div>
				<br>	
				<div style="BORDER-RIGHT: lightgrey 2px solid; PADDING-RIGHT: 4px; BORDER-TOP: lightgrey 2px solid; MARGIN-TOP: 4px; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 100%; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; HEIGHT: 364px">
					<table cellpadding="4" cellspacing="0" border="0" width="100%">
					<tr class="FormTableHeader">
						<td>
							Archived Messages 
						</td>
						<td align="right">
							Size <Asp:TextBox id="m_TopSize" runat="server"/>  
							<asp:Button runat="server" Text="Update" id=Button1 />
							
						</td>
					</tr>
				</table>
					<ml:CommonDataGrid id="m_ArchiveGrid" runat="server" CellSpacing="0" CellPadding="2" OnItemCommand="GridItemClick" EnableViewState="True">
						<Columns>
							<asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
								<ItemTemplate>
									<asp:LinkButton runat="server" CommandName="DropArchive" CommandArgument='<%#AspxTools.HtmlString(((System.Data.DataRowView)Container.DataItem)["MessageId"])%>' ID="Linkbutton1">
																		drop
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
								<ItemTemplate>
									<asp:LinkButton Runat="server" CommandName="CopyToM" CommandArgument='<%#AspxTools.HtmlString(((System.Data.DataRowView)Container.DataItem)["MessageId"])%>' ID="Linkbutton2">Move to Queue</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:BoundColumn DataField="MessageId" HeaderText="Id"></asp:BoundColumn>
							<asp:BoundColumn DataField="Priority" HeaderText="Priority"></asp:BoundColumn>
							<asp:BoundColumn DataField="InsertionTime" HeaderText="Time Added"></asp:BoundColumn>
							<asp:BoundColumn DataField="RemovalTime" HeaderText="Time Removed"></asp:BoundColumn>
							<asp:BoundColumn DataField="Subject1" HeaderText="Subject1"></asp:BoundColumn>
							<asp:BoundColumn DataField="Subject2" HeaderText="Subject2"></asp:BoundColumn>
							<asp:BoundColumn DataField="DataLoc" HeaderText="Data Location"></asp:BoundColumn>
						</Columns>
					</ml:CommonDataGrid>
				</div>
			</td>
			</tr>
			</table>
		</form>
	</body>
</HTML>
