﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data.SqlClient;
using LendersOffice.Integration.Appraisals;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class ManageAppraisalVendors : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditIntegrationSetup
            };
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnInit(EventArgs e)
        {
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(DeleteVendor.Value))
            {
                Guid id = new Guid(DeleteVendor.Value);
                AppraisalVendorConfig.DeleteVendor(id);
                DeleteVendor.Value = "";
            }
            try
            {
                Vendors.DataSource = AppraisalVendorConfig.ListAll();
                Vendors.DataBind();
                if (Vendors.Rows.Count != 0)
                {
                    Vendors.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
            catch(SqlException exc)
            {
                Tools.LogError(exc);
                Vendors.Visible = false;
                ErrorMessage.Visible = true;
            }
            if (IsPostBack)
            {
                Response.Redirect(Request.Url.ToString(), false);
            }
        }
    }
}
