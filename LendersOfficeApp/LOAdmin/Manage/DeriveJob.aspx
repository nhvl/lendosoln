<%@ Page language="C#" Codebehind="DeriveJob.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Manage.DeriveJob"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Derivation Processor Control Panel" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <style>
        .table > tbody > tr > td {
             vertical-align: top;
        }
    </style>
    <div class="container-fluid"><div class="row"><div class="panel panel-default"><div class="panel-body">
         <div class="ng-cloak" ng-app="app" ng-controller="DJobManager as dj">
            <div ng-hide="!isLoadding">loading&hellip;</div>
            <div ng-show="!isLoading">
                <table class="table table-striped table-condensed table-layout-fix">
                    <thead>
                        <tr>
                            <th class="col-xs-1" ng-show="dj.hasCancel">&nbsp;</th>
                            <th class="col-xs-1" ng-show="dj.hasEnqueue">&nbsp;</th>
                            <th class="col-xs-3">Status</th>
                            <th class="col-xs-1">Priority</th>
                            <th>Date Submitted</th>
                            <th>Notify Email</th>
                            <th>Label</th>
                            <th>Login Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="e in dj.DJobs | orderBy:dj.entrySort.by:dj.entrySort.desc track by e.JobID">
                            <td ng-show="dj.hasCancel">
                                <a ng-hide="e.JobStatus == 'InProgress'" ng-click="dj.SetJobCommand(e,'cancel',$event)"
                                    data-toggle="modal" data-target="#confirmModal">cancel</a>
                            </td>

                            <td ng-show="dj.hasEnqueue">
                                <a ng-show="e.JobStatus == 'Error'" ng-click="dj.SetJobCommand(e,'enqueue',$event)"
                                    data-toggle="modal" data-target="#confirmModal">enqueue</a>
                            </td>
                            
                            <td ng-bind="e.JobStatus" ng-show="e.JobStatus == 'Pending'">
                            </td>
                            <td ng-show="e.JobStatus == 'InProgress'">
                                <b>{{e.JobStatus}} (started {{e.JobStartD}})</b>
                            </td>
                            <td ng-show="e.JobStatus == 'Error'">
                                <a ng-bind="e.JobStatus" ng-click="dj.toogleError(e,$event)"></a>
                                <div class="well" ng-bind="e.ErrorText" ng-show="e.IsShowError"></div>
                            </td>
                            <td ng-bind="e.JobPriority"></td>
                            <td ng-bind="e.JobSubmitD"></td>
                            <td ng-bind="e.JobNotifyEmailAddr"></td>
                            <td ng-bind="e.JobUserLabel"></td>
                            <td ng-bind="e.UserLoginNm"></td>
                        </tr>
                    </tbody>
                    </table>
                </div>


            <!--Confirm Cancel or Enqueue Dialog-->
             <div id="confirmModal" class="modal v-middle fade"><div class="modal-dialog modal-lg"><div class="modal-content">
                <div class="modal-header">
                    <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
                    <h4 class="modal-title">
                        <div ng-hide="!(dj.command == 'cancel')">Cancel Derivation Processor Control Panel</div>
                        <div ng-show="!(dj.command == 'cancel')">Enqueue Derivation Processor Control Panel</div>
                    </h4>
                </div>
                <form name="defaultForm" class="form-group-sm form-adjust-mb" ng-submit="dj.ExecuteJobCommand($event)">
                    <div class="modal-body">
                        Do you want to <span ng-bind="dj.command"></span> the Derivation Job "<span ng-bind="dj.confirmDJ.JobUserLabel"></span>" ?
                    </div>
                    <div class="modal-footer">
                        <span class="clipper"><a class="btn btn-default" data-dismiss="modal">Cancel</a></span>
                        <span class="clipper"><button type="submit" class="btn btn-primary">OK</button></span>
                    </div>
                </form>
            </div></div></div>

    </div></div></div></div></div>
</asp:Content>