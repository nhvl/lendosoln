﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Security.ThirdParty;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Allows users to register new client certificates.
    /// </summary>
    public partial class ManageThirdPartyClientCertificates : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Gets the style sheet.
        /// </summary>
        /// <value>The style sheet.</value>
        public override string StyleSheet
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the required permissions for the page.
        /// </summary>
        /// <returns>The set of permissions required for the page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.IsDevelopment
            };
        }

        /// <summary>
        /// Handles an on click for the associate url button.
        /// </summary>
        /// <param name="sender">The button that triggered the event.</param>
        /// <param name="args">The arguments passed to the button.</param>
        protected void AssociateUrl_OnClick(object sender, EventArgs args)
        {
            ThirdPartyClientCertificateManager manager = new ThirdPartyClientCertificateManager();
            manager.AssociateUrl(this.associationUrl.Value, int.Parse(this.certificateId.Value));
            Response.Redirect(this.Request.RawUrl);
        }

        /// <summary>
        /// Handles a add new cert button click.
        /// </summary>
        /// <param name="sender">The sender that triggered the action.</param>
        /// <param name="args">The arguments for the event.</param>
        protected void AddNewThirdPartyCert_OnClick(object sender, EventArgs args)
        {
            if (this.certificateFile.PostedFile == null)
            {
                return;
            }

            LocalFilePath file = LqbGrammar.Utils.TempFileUtils.NewTempFilePath();
            this.certificateFile.PostedFile.SaveAs(file.Value);

            NewClientCertificateDetails cert = new NewClientCertificateDetails()
            {
                Path = file,
                Description = this.Description.Value,
                Password = this.Password.Value
            };

            ThirdPartyClientCertificateManager manager = new ThirdPartyClientCertificateManager();
            var result = manager.AddNewCertificate(cert);
            
            Response.Redirect($"{Request.Path}?status={result.ToString("d")}");
        }

        /// <summary>
        /// Handles a delete button press.
        /// </summary>
        /// <param name="sender">The sender that triggered the event.</param>
        /// <param name="args">The arguments passed in the event.</param>
        protected void DeleteAssociation_onClick(object sender, EventArgs args)
        {
            LinkButton btn = sender as LinkButton;
            int associationId = int.Parse(btn.CommandArgument);
            ThirdPartyClientCertificateManager manager = new ThirdPartyClientCertificateManager();
            manager.DeleteAssociatedUrl(associationId);
            Response.Redirect(Request.RawUrl);
        }

        /// <summary>
        /// Called on page load. Loads the data needed to generate the page.
        /// </summary>
        /// <param name="sender">The caller of the method.</param>
        /// <param name="e">The arguments for the event.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsScript("simpleservices.js");

            int status = RequestHelper.GetInt("status", -1);

            if (status > -1)
            {
                string errorMessage = null;
                var result = (CertificateAdditionResult)status;

                switch (result)
                {
                    case CertificateAdditionResult.OK:
                        break;
                    case CertificateAdditionResult.InvalidPassword:
                        errorMessage = "Invalid password.";
                        break;
                    case CertificateAdditionResult.DescriptionCannotBeEmpty:
                        errorMessage = "Description missing.";
                        break;
                    case CertificateAdditionResult.InvalidCertificate:
                        errorMessage = "Invalid certificate.";
                        break;
                    case CertificateAdditionResult.DescriptionTooLong:
                        errorMessage = "Description too long.";
                        break;
                    case CertificateAdditionResult.DoesNotContainPrivateKey:
                        errorMessage = "Certificate does not contain private key.";
                        break;
                    case CertificateAdditionResult.Error:
                        errorMessage = "Unknown error.";
                        break;
                    default:
                        throw new UnhandledEnumException(result);
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    this.ErrorMessagePlaceHolder.Visible = true;
                    this.ErrorMessage.Text = errorMessage;
                }
            }

            if (!Page.IsPostBack)
            {
                ThirdPartyClientCertificateManager manager = new ThirdPartyClientCertificateManager();
                this.Configurations.DataSource = manager.GetCertificateView();
                this.Configurations.DataBind();
            }
        }

        /// <summary>
        /// Binds a client certificate to the configuration repeater row.
        /// </summary>
        /// <param name="sender">The object the binding is being performed on.</param>
        /// <param name="args">The arguments of the event.</param>
        protected void Configurations_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem  && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            ClientCertificateView view = (ClientCertificateView)args.Item.DataItem;
            EncodedLiteral description = (EncodedLiteral)args.Item.FindControl("Description");
            EncodedLiteral createdDate = (EncodedLiteral)args.Item.FindControl("CreatedDate");
            EncodedLiteral issuer = (EncodedLiteral)args.Item.FindControl("Issuer");
            EncodedLiteral subject = (EncodedLiteral)args.Item.FindControl("Subject");
            EncodedLiteral validRange = (EncodedLiteral)args.Item.FindControl("ValidRange");
            Repeater repeater = (Repeater)args.Item.FindControl("CertUrlsRepeater");
            repeater.ItemDataBound += this.CertUrlsRepeater_ItemDataBound;
            repeater.DataSource = view.Url;
            repeater.DataBind();
            HtmlTableRow row = (HtmlTableRow)args.Item.FindControl("CertificateRow");
            row.Attributes.Add("data-id", view.Certificate.Id.ToString());
            description.Text = view.Certificate.Description;
            createdDate.Text = view.Certificate.CreationDate.ToString();
            issuer.Text = view.Certificate.Issuer;
            subject.Text = view.Certificate.Subject;
            validRange.Text = $"{view.Certificate.ValidOn} - {view.Certificate.ExpiresOn}";
        }

        /// <summary>
        /// Binds a URL entry to the list of URLs.
        /// </summary>
        /// <param name="sender">The row being bound to.</param>
        /// <param name="args">The arguments passed to the event.</param>
        protected void CertUrlsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            var url = (ClientCertificateUrlAssociation)args.Item.DataItem;
            EncodedLiteral description = (EncodedLiteral)args.Item.FindControl("CertURL");
            LinkButton anchor = (LinkButton)args.Item.FindControl("deleteAssociation");
            anchor.CommandArgument = url.Id.ToString();
            anchor.OnClientClick = $"return Confirm('Delete {AspxTools.JsStringUnquoted(url.Url)}?');";
            description.Text = url.Url;
        }
    }
}