using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CommonProjectLib.Common.Lib;
using ComplianceEase;
using DataAccess;
using EDocs;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Conversions.ComplianceEase;
using LendersOffice.Drivers.SqlServerDB;
using LendersOffice.Integration.OCR;
using LendersOffice.Migrators;
using LendersOffice.ObjLib.UI.Themes;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LqbGrammar.DataTypes;
using PdfRasterizerLib;  //! move this into library - for 105723
using Toolbox.Distributed;
using System.Data.Common;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class UpdateForm : LendersOffice.Admin.SecuredAdminPage
    {

        #region Consumer Portal EDocs migration

        public struct ShippingTemplateDesc
        {
            public Guid BrokerId;
            public int ShippingTemplateId;
            public string ShippingTemplateName;
        }


        public struct StandardDocTypeDesc
        {
            public int OldDocTypeId;
            public int NewDocTypeId;
            public string DocTypeName;
        }

        #endregion

        /// <summary>
        /// Every admin page that inherits from this base page
        /// should implement this query method to specify the
        /// permission set required to access this page.
        /// </summary>
        /// <returns>
        /// List of required permissions.
        /// </returns>

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return an empty set.  Overload to provide
            // a page specific permission list.

            return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsDevelopment ,
				E_InternalUserPermissions.ViewBrokers
			};
        }

        protected void RequeueOCRRequest_OnClick(object sender, EventArgs args)
        {
            Guid brokerId;
            Guid transactionId;

            if (!Guid.TryParse(this.OCR_BrokerId.Text, out brokerId))
            {
                ErrorMessage.Text = "Could not parse brokerid.";
                return;
            }

            if (!Guid.TryParse(this.OCR_TransactionId.Text, out transactionId))
            {
                ErrorMessage.Text = "Could not parse transaction id.";
                return;
            }

            OCRService.RequeueDoc(brokerId, transactionId);
            ErrorMessage.Text = "Request sent.";
        }

        protected void LicneseUpdateByLoanOfficer_OnClick(object sender, EventArgs args)
        {
            Context.Server.ScriptTimeout =   3600;
            string[] ids = LoanOfficerIds.Text.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            if (ids.Length == 0)
            {
                return;
            }

            var loansAndConnections = new List<Tuple<DbConnectionInfo, Guid, Guid>>();
            GatherLoansAndConnections(ids, loansAndConnections, true, ErrorMessage);
            Tools.LogInfo("Selected " + loansAndConnections.Count + " loans.");

            int count = 0;
            bool showError = false;
            foreach (var entry in loansAndConnections.OrderBy(p=>p.Item3))
            {
                try
                {
                    CPageData pageData = new NotEnforceAccessControlPageData(entry.Item2, new string[] { "sf_UpdateLicenseInfoForLoanOfficer" });
                    pageData.DisableLoanModificationTracking = true;
                    pageData.InitSave(ConstAppDavid.SkipVersionCheck);

                    if (pageData.UpdateLicenseInfoForLoanOfficer())
                    {
                        pageData.Save();
                        count ++;
                    }
                }
                catch (Exception e)
                {
                    Tools.LogError("Failed to save " + entry.Item2 + " for loan officer " +  entry.Item3 + " due to error.", e);
                    showError = true;
                }
            }

            ErrorMessage.Text = "Loans updated : " + count;
            if (showError)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var entry in loansAndConnections.OrderBy(p => p.Item3))
                {
                    sb.AppendLine(entry.Item3.ToString());
                }

                Tools.LogError(sb.ToString());
            }
        }

        public static void GatherLoansAndConnections(string[] ids, List<Tuple<DbConnectionInfo, Guid, Guid>> loansAndConnections, bool enableLogging, System.Web.UI.WebControls.Label ErrorMessage)
        {
            int index = 0;
            StringBuilder sb = new StringBuilder();
            var listParams = new List<SqlParameter>();
            foreach (string stringId in ids)
            {
                Guid loanId;
                if (Guid.TryParse(stringId.TrimWhitespaceAndBOM(), out loanId) == false)
                {
                    if (enableLogging)
                    {
                        ErrorMessage.Text = "Could not parse id " + stringId;
                    }
                    return;
                }

                var paramName = "@p" + index.ToString();
                listParams.Add(new SqlParameter(paramName, loanId));
                index++;

                if (sb.Length > 0) sb.Append(",");
                sb.Append(paramName);
            }


            string sql = string.Format("select slid, sEmployeeLoanRepId from loan_file_cache c join broker b on c.sbrokerid = b.brokerid where sEmployeeLoanRepId in ({0}) and isvalid = 1 and sloanfilet = 0 and istemplate = 0 and status = 1", sb);
            if (enableLogging)
            {
                Tools.LogInfo(sql);
            }

            // NOTE: Since the connection is being used within the code that
            //       is processing the returned data, we cannot take advantage
            //       of the DBSelectUtility methods.
            var sqlQuery = SQLQueryString.Create(sql); // don't bother checking for null

            var sqlDriver = SqlServerHelper.CreateSpecificDriver(TimeoutInSeconds.Thirty, LqbGrammar.Drivers.SqlDecoratorType.Default);
            foreach (var con in DbConnectionInfo.ListAll())
            {
                using (var sqlConnection = con.GetReadOnlyConnection())
                {
                    // Need new parameter instance for each statement to avoid error due to parameter reuse.
                    List<SqlParameter> listParamsCopy = new List<SqlParameter>();
                    listParams.ForEach(p => listParamsCopy.Add(new SqlParameter(p.ParameterName, p.Value)));

                    using (var reader = sqlDriver.Select(sqlConnection, null, sqlQuery.Value, listParamsCopy))
                    {
                        while (reader.Read())
                        {
                            loansAndConnections.Add(Tuple.Create(con, (Guid)reader["slid"], (Guid)reader["sEmployeeLoanRepId"]));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Initialize page.
        /// </summary>

        protected void PageLoad(object sender, System.EventArgs a)
        {
            // Load all brokers so we can choose one.

            BrokerList.DataSource = BrokerDbSearch.Search(string.Empty, string.Empty, null, string.Empty, Guid.Empty, suiteType: null);
            BrokerList.ItemDataBound += ChooseBrokerOnItemDataBound;
            BrokerList.DataBind();

            // Clear out error message.

            ErrorMessage.Text = "";
            Feedback.Text = "";
        }

        protected void RestartRasterizer_Click(object sender, EventArgs args)
        {
            if (string.IsNullOrEmpty(ConstSite.PdfRasterizerHost))
            {
                ErrorMessage.Text = "Pdf Rasterizer Host Is Empty.";
                return;
            }

            try
            {
                PdfRasterizerFactory.Restart(ConstSite.PdfRasterizerHost);
                ErrorMessage.Text = "Service Restarted successfully on " + ConstSite.PdfRasterizerHost; 
            }
            catch (Exception e)
            {
                //Look at http://serverfault.com/questions/187302/how-do-i-grant-start-stop-restart-permissions-on-a-service-to-an-arbitrary-user 
                ErrorMessage.Text = e.ToString();
            }
        }

        protected void PagePreRender(object sender, System.EventArgs a)
        {
            // Display post-event changes.
            string rawVersion = System.Environment.Version.ToString();
            string labelVersion = "Unknown or New";
            switch (rawVersion)
            {
                case "1.0.3705.000":
                    labelVersion = "(.NET 1.0)";
                    break;
                case "1.0.3705.209":
                    labelVersion = " (.NET 1.0 SP1)";
                    break;
                case "1.0.3705.288":
                    labelVersion = (" (.NET 1.0 SP2)");
                    break;
                case "1.0.3705.6018":
                    labelVersion = (" (.NET 1.0 SP3)");
                    break;
                case "1.1.4322.573":
                    labelVersion = (" (.NET 1.1)");
                    break;
                case "1.1.4322.2032":
                    labelVersion = (" (.NET 1.1 SP1)");
                    break;
                case "2.0.50727.3053":
                case "2.0.50727.3603":
                case "2.0.50727.3623":
                    labelVersion = " (.NET 3.5 SP1)";
                    break;
            }

            DotNetVersionEd.Text = rawVersion + ":" + labelVersion;

            try
            {
                // Get the broker's id and its loans.

                Guid brokerid = Guid.Empty;

                if (CurrentBrokerId.Text.TrimWhitespaceAndBOM() != "No id specified")
                {
                    brokerid = new Guid(CurrentBrokerId.Text.TrimWhitespaceAndBOM());
                }
                DataSet ds = new DataSet();
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerid)
                                            };
                DataSetHelper.Fill(ds, brokerid, "ListLoansByBrokerId", parameters);
                LoanList.DataSource = ds;
                LoanList.DataBind();
            }
            catch (Exception e)
            {
                ErrorMessage.Text = e.Message;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.PreRender += new System.EventHandler(this.PagePreRender);
            

        }

        #endregion

        #region Async CE Export for loan
        protected void TryAsyncCEExportForLoan(object sender, EventArgs e)
        {
            Guid loanID = Guid.Empty;
            try
            {
                loanID = new Guid(CEExportLoanID.Text.TrimWhitespaceAndBOM());
            }
            catch(FormatException)
            {
                TryAsyncCEExportForLoanStatus.Text = "bad loanid.";
                return;
            }
            TryAsyncCEExportForLoanStatus.Text = CEForceAsyncExporter.TryExport(loanID);
        }

        private class CEForceAsyncExporter
        {
            public static string TryExport(Guid loanID)
            {
                var dataLoan = new CFullAccessPageData(loanID,new string[]{"sIsLineOfCredit", "sComplianceEaseStatus" });
                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.InitLoad();
                var broker = dataLoan.BrokerDB;
                string userName = "";
                string password = "";
                if (E_BrokerBillingVersion.PerTransaction == broker.BillingVersion && broker.IsEnablePTMComplianceEaseIndicator)
                {
                    // 10/30/2013 gf - opm 142462 Don't enqueue HELOCs. This dependency is
                    // added in InitSavePrivate in LFF_Core.
                    if (dataLoan.sIsLineOfCredit)
                    {
                        return "loan is line of credit, won't run.";
                    }

                    bool isPaidAccount = !string.IsNullOrEmpty(broker.ComplianceEaseUserName);

                    if (false == isPaidAccount)
                    {
                        userName = ConstStage.ComplianceEasePTMTestLogin;
                        password = ConstStage.ComplianceEasePTMTestPassword;
                    }
                    else
                    {
                        userName = broker.ComplianceEaseUserName;
                        password = broker.ComplianceEasePassword.Value;
                    }
                }
                else
                {
                    return "broker doesn't have the feature enabled.";
                }

                ServiceFault serviceFault = null;
                try
                {
                    bool didUpdate = false;
                    var hasError = ComplianceEaseExporter.Export(loanID, userName, password,
                        ComplianceEase.E_TransmittalDataComplianceAuditType.PreClose,
                        out serviceFault, true, dataLoan.sFileVersion,
                        true, // setting this to true will ignore the previous checksum.
                        out didUpdate);

                    if (!hasError)
                    {
                        if (didUpdate)
                        {
                            return "Success! " + dataLoan.sComplianceEaseStatus;
                        }
                        else
                        {
                            return "didn't do the update.";
                        }
                    }
                    else
                    {
                        return "error occurred";
                    }
                }
                catch (Exception ex)
                {
                    Tools.LogError(ex);
                    return "exception occurred.";
                }
            }
        }
        #endregion

        #region ( 105723 Migration )

        
        protected void MigrateLockPolicyFor105723(object sender, EventArgs e)
        {
            try
            {
                var brokerID = new Guid(MigrateLockPolicyFor105723Broker.Text.TrimWhitespaceAndBOM());
                LockPolicyAndPriceGroupMigratorFor105723.MigrateBroker(brokerID);
                MigrateLockPolicyFor105723Result.Text = "Success.";
            }
            catch (Exception ex)
            {
                Tools.LogError(ex);
                MigrateLockPolicyFor105723Result.Text = "Failed.";
            }
        }

        #endregion

        private void AlertUser(string sMessage)
        {
            string sScript = @"<script type='text/javascript'>
/*[CDATA[*/
    alert('" + sMessage + @"');
/*]]*/
</script>";
            ClientScript.RegisterStartupScript(this.GetType(), "Updated", sScript);
        }

         protected void RequeueBtn_OnClick(object sender, EventArgs e)
         {
             if (!EDocRequeuCSVFile.HasFile)
             {
                 return;
             }

             Dictionary<Guid, HashSet<Guid>> docsByBroker = new Dictionary<Guid, HashSet<Guid>>();
             int count = 0;
             using (StreamReader sr = new StreamReader(EDocRequeuCSVFile.FileContent))
             {
                 while (!sr.EndOfStream)
                 {
                     string line = sr.ReadLine();
                     string[] parts = line.Split(',');

                     if (parts.Length > 1)
                     {
                         Guid brokerId; 
                         Guid docId; 

                         try
                         {
                             brokerId = new Guid(parts[0]);
                             docId = new Guid(parts[1]);
                         }
                         catch ( FormatException)
                         {
                             continue;
                         }

                         HashSet<Guid> docs;
                         if (!docsByBroker.TryGetValue(brokerId, out docs))
                         {
                             docs = new HashSet<Guid>();
                             docsByBroker.Add(brokerId, docs);
                         }

                         docs.Add(docId);
                         count++;
                     }
                 }
             }

             foreach (var entry in docsByBroker)
             {
                 EDocumentRepository repo = EDocumentRepository.GetSystemRepository(entry.Key);

                 foreach (Guid docId in entry.Value)
                 {
                     EDocument doc = repo.GetDocumentById(docId);
                     doc.EnqueuePDFConversion(E_EDocOrigin.LO, VerifyPages.Checked);
                 }
             }
             ErrorMessage.Text = String.Format("Enqueued {0} docs.", count);
         }

        protected void UpdateCachedLoanClick(object sender, System.EventArgs a)
        {
            // Transfer the contents for the given loan.

            Guid id = Guid.Empty;

            try
            {
                id = new Guid(CurrentLoanId.Text);
            }
            catch
            {
                ErrorMessage.Text = "Invalid loan id.  Please specify a valid identifier.";
            }

            if (id != Guid.Empty)
            {
                // We have an id, so let's load it and save it.

                try
                {
                    Tools.LogMadness(Tools.LogMadnessType.CachedDataMigrationInfo, "Data Migration: updating cache of loan with id = " + id + ".");

                    Tools.UpdateCacheTable(id, null);

                    Feedback.Text = "Processed loan and refreshed cache.";
                }
                catch (Exception e)
                {
                    Tools.LogError("Need to update cache data for loan with id = " + id + " -- " + e.Message + ".");

                    ErrorMessage.Text = e.Message;
                }
            }
        }

        protected void MaskBrokersLoansClick(object sender, System.EventArgs a)
        {
            // Get all the loan ids again and mask each one.

            try
            {
                // Get current selection and go to town.

                Guid brokerid = Guid.Empty;

                try
                {
                    brokerid = new Guid(CurrentBrokerId.Text);
                }
                catch
                {
                    ErrorMessage.Text = "No broker selected.  Specify a broker before masking its loans.";
                }

                // Gather broker's loans and mask out all
                // the sensitive fields.  Add to this set
                // by extending the manipulation code and
                // the mask data class' field list.

                int total = 0;

                if (brokerid != Guid.Empty)
                {
                    // We first load all the loan ids, then we
                    // process each one.
                    DataSet ds = new DataSet();
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerId", brokerid)
                                                };

                    DataSetHelper.Fill(ds, brokerid, "ListLoansByBrokerId", parameters);

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        // Get next loan identifier.

                        Guid id = new Guid(row["LoanId"].ToString());

                        if (id == Guid.Empty)
                        {
                            continue;
                        }

                        // For each loan, we get 3 separate views of the
                        // data -- each is needed to save masked updates
                        // to the loan.

                        CPageData loan = new CMaskData(id);

                        loan.InitSave(ConstAppDavid.SkipVersionCheck);

                        try
                        {
                            // Manipulate the masked fields so that sensitive info
                            // is no longer accessible.

                            for (int i = 0, n = loan.nApps; i < n; ++i)
                            {
                                // Get next application.

                                CAppData data = loan.GetAppData(i);

                                if (data == null)
                                {
                                    continue;
                                }

                                // Update social security numbers.

                                if (data.aBSsn != null && data.aBSsn.Length > 0)
                                {
                                    data.aBSsn = "123-45-6789";
                                }

                                if (data.aCSsn != null && data.aCSsn.Length > 0)
                                {
                                    data.aCSsn = "987-65-4321";
                                }

                                // Update borrowers' personal info.

                                if (data.aBAddr != null && data.aBAddr.Length > 0)
                                {
                                    data.aBAddr = "1234 Any Street";
                                }

                                if (data.aBAddrMail != null && data.aBAddrMail.Length > 0)
                                {
                                    data.aBAddrMail = "1234 Any Street";
                                }

                                if (data.aBBusPhone != null && data.aBBusPhone.Length > 0)
                                {
                                    data.aBBusPhone = "(714) 999-9999";
                                }

                                if (data.aBHPhone != null && data.aBHPhone.Length > 0)
                                {
                                    data.aBHPhone = "(714) 999-9999";
                                }

                                if (data.aBCellPhone != null && data.aBCellPhone.Length > 0)
                                {
                                    data.aBCellPhone = "(714) 999-9999";
                                }

                                if (data.aBEmail != null && data.aBEmail.Length > 0)
                                {
                                    data.aBEmail = "any@anywhere.com";
                                }

                                if (data.aCAddr != null && data.aCAddr.Length > 0)
                                {
                                    data.aCAddr = "1234 Any Street";
                                }

                                if (data.aCAddrMail != null && data.aCAddrMail.Length > 0)
                                {
                                    data.aCAddrMail = "1234 Any Street";
                                }

                                if (data.aCBusPhone != null && data.aCBusPhone.Length > 0)
                                {
                                    data.aCBusPhone = "(714) 000-0000";
                                }

                                if (data.aCHPhone != null && data.aCHPhone.Length > 0)
                                {
                                    data.aCHPhone = "(714) 000-0000";
                                }

                                if (data.aCCellPhone != null && data.aCCellPhone.Length > 0)
                                {
                                    data.aCCellPhone = "(714) 000-0000";
                                }

                                if (data.aCEmail != null && data.aCEmail.Length > 0)
                                {
                                    data.aCEmail = "any@anywhere.com";
                                }
                            }

                            // Manipulate the borrower's employment records to hide
                            // sensitive information.

                            for (int i = 0, n = loan.nApps; i < n; ++i)
                            {
                                // Get next application.

                                CAppData data = loan.GetAppData(i);

                                if (data == null)
                                {
                                    continue;
                                }

                                // Update individual employment records.

                                IEmpCollection bemps = data.aBEmpCollection;

                                foreach (IEmploymentRecord emp in bemps.GetSubcollection(true, E_EmpGroupT.All))
                                {
                                    if (emp.EmplrAddr != null && emp.EmplrAddr.Length > 0)
                                    {
                                        emp.EmplrAddr = "1234 Any Street";
                                    }

                                    if (emp.EmplrNm != null && emp.EmplrNm.Length > 0)
                                    {
                                        emp.EmplrNm = "Company Name";
                                    }
                                }

                                // Update individual employment records.

                                IEmpCollection cemps = data.aCEmpCollection;

                                foreach (IEmploymentRecord emp in cemps.GetSubcollection(true, E_EmpGroupT.All))
                                {
                                    if (emp.EmplrAddr != null && emp.EmplrAddr.Length > 0)
                                    {
                                        emp.EmplrAddr = "1234 Any Street";
                                    }

                                    if (emp.EmplrNm != null && emp.EmplrNm.Length > 0)
                                    {
                                        emp.EmplrNm = "Company Name";
                                    }
                                }
                            }

                            // Manipulate the borrower's asset records to hide
                            // sensitive information.

                            for (int i = 0, n = loan.nApps; i < n; ++i)
                            {
                                // Get next application.

                                CAppData data = loan.GetAppData(i);

                                if (data == null)
                                {
                                    continue;
                                }

                                // Update individual asset records.

                                IAssetCollection assets = data.aAssetCollection;

                                foreach (var item in assets.GetSubcollection(true, E_AssetGroupT.Regular))
                                {
                                    var asset = (IAssetRegular)item;
                                    asset.AccNm = "Account Name";
                                    asset.AccNum = "Account ####";

                                    if (asset.StAddr != null && asset.StAddr.Length > 0)
                                    {
                                        asset.StAddr = "1234 Any Street";
                                    }

                                    if (asset.ComNm != null && asset.ComNm.Length > 0)
                                    {
                                        asset.ComNm = "Any Company";
                                    }
                                }
                            }

                            // Manipulate the borrower's liability records to hide
                            // sensitive information.

                            for (int i = 0, n = loan.nApps; i < n; ++i)
                            {
                                // Get next application.

                                CAppData data = loan.GetAppData(i);

                                if (data == null)
                                {
                                    continue;
                                }

                                // Update individual debt records.

                                ILiaCollection debts = data.aLiaCollection;

                                foreach (ILiabilityRegular debt in debts.GetSubcollection(true, E_DebtGroupT.Regular))
                                {
                                    debt.AccNm = "Account Name";
                                    debt.AccNum = "Account ####";

                                    if (debt.ComAddr != null && debt.ComAddr.Length > 0)
                                    {
                                        debt.ComAddr = "1234 Any Street";
                                    }

                                    if (debt.ComNm != null && debt.ComNm.Length > 0)
                                    {
                                        debt.ComNm = "Any Company";
                                    }
                                }
                            }

                            // Manipulate the borrower's reo records to hide
                            // sensitive information.

                            for (int i = 0, n = loan.nApps; i < n; ++i)
                            {
                                // Get next application.

                                CAppData data = loan.GetAppData(i);

                                if (data == null)
                                {
                                    continue;
                                }

                                // Update individual reo records.

                                var reos = data.aReCollection;

                                foreach (var item in reos.GetSubcollection(true, E_ReoGroupT.All))
                                {
                                    var reo = (IRealEstateOwned)item;
                                    if (reo.Addr != null && reo.Addr.Length > 0)
                                    {
                                        reo.Addr = "1234 Any Street";
                                    }
                                }
                            }

                            // Update the property addresses to hide the
                            // location details.

                            if (loan.sSpAddr != null && loan.sSpAddr.Length > 0)
                            {
                                loan.sSpAddr = "1234 Any Street";
                            }

                            // Bump up the successful masking count.

                            ++total;
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }

                        // Commit this loan to the database.

                        loan.Save();
                    }
                }

                Feedback.Text = "Processed " + total + " loans by masking their contents.";
            }
            catch (Exception e)
            {
                ErrorMessage.Text = e.Message;
            }
        }

        protected void ChooseBrokerOnItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs a)
        {
            if (a.Item.DataItem == null)
            {
                return;
            }

            var lender = (BrokerDbSearchItem)a.Item.DataItem;
            var linkButton = (System.Web.UI.WebControls.LinkButton)a.Item.FindControl("ChooseBroker");
            linkButton.CommandArgument = lender.BrokerId + ":" + lender.BrokerNm;
            linkButton.CommandName = "Choose";
        }

        /// <summary>
        /// Handle broker list selection.
        /// </summary>

        protected void ChooseBrokerForOp(object sender, System.Web.UI.WebControls.RepeaterCommandEventArgs a)
        {
            // Load up a broker's loans into the loan list.
            // We process what is listed.

            try
            {
                // Set the new selection parameters.
                string[] args = a.CommandArgument.ToString().Split(new[] { ':' }, 2);

                CurrentBrokerId.Text = args[0];
                CurrentBrokerNm.Text = args[1];

            }
            catch (Exception e)
            {
                ErrorMessage.Text = e.Message;
            }
        }

        /// <summary>
        /// Filter invalid cell strings.
        /// </summary>

        protected string Filter(object oItem)
        {
            if (oItem != null && oItem is DBNull == false)
            {
                return oItem.ToString();
            }

            return "Not specified";
        }

        /// <summary>
        /// Filter status cell strings.
        /// </summary>

        protected string Status(object oItem)
        {
            if (oItem != null && oItem is Int32 == true)
            {
                E_sStatusT sItem = (E_sStatusT)oItem;

                return sItem.ToString();
            }

            return "Not specified";
        }

        protected void GarbageCollectBtn_Click(object sender, System.EventArgs e)
        {
            GC.Collect();
        }

        protected void InheritanceUpdateBtn_Click(object sender, System.EventArgs e)
        {
            try
            {
                using (CSingletonDistributed distributedLock = Toolbox.Distributed.CSingletonDistributed.RequestSingleton(CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket,
                           "LoAdmin Migration Page", 120))
                {
                    Toolbox.LoanProgram.EventHandlers.AfterLoadminRequestFixupAllLoanProgramsInSystem();
                }
                ErrorMessage.Text = "";
            }
            catch (CBaseException ex)
            {
                ErrorMessage.Text = ex.DeveloperMessage + "\r\n" + ex.StackTrace;
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = ex.Message;
            }

        }

        protected void AfterRateOptionBatchUpdateBtn_Click(object sender, System.EventArgs e)
        {
            try
            {
                using (CSingletonDistributed tix = CSingletonDistributed.RequestSingleton
                           (CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket,
                           "LoAdminMigration", 120))
                {
                    Toolbox.LoanProgram.EventHandlers.AfterRateOptionsBatchUpdate();
                } // using

                ErrorMessage.Text = "";
            }
            catch (CBaseException ex)
            {
                ErrorMessage.Text = ex.DeveloperMessage + "\r\n" + ex.StackTrace;
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = ex.Message + "\r\n" + ex.StackTrace;
            }
        }

        protected void AfterLpeUpdateProjectIsRunBtn_Click(object sender, System.EventArgs e)
        {
            try
            {
                using (CSingletonDistributed tix = CSingletonDistributed.RequestSingleton
                           (CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket,
                           "LoAdminMigration", 120))
                {
                    Toolbox.LoanProgram.EventHandlers.AfterLpeUpdateProjectIsRun();
                } // using

                ErrorMessage.Text = "";
            }
            catch (CBaseException ex)
            {
                ErrorMessage.Text = ex.DeveloperMessage + "\r\n" + ex.StackTrace;
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = ex.Message + "\r\n" + ex.StackTrace;
            }
        }

        protected void RemoveBlankAssocBtn_Click(object sender, System.EventArgs e)
        {
            try
            {
                int nRows = DataAccess.StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "RemoveBlankPricePolicyAssociationRows", 0);
                AssocRowsRemovedEd.Text = nRows.ToString();
            }
            catch (Exception ex)
            {
                AssocRowsRemovedEd.Text = ex.Message;
            }
        }

        protected void AfterModifOfThisMasterBtn_Click(object sender, System.EventArgs e)
        {
            try
            {
                using (CSingletonDistributed tix = CSingletonDistributed.RequestSingleton
                           (CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket,
                           "LoAdminMigration", 120))
                {
                    Toolbox.LoanProgram.EventHandlers.AfterModifOfThisMaster(
                        new Guid(EventHandlerProdId_ed.Text), new Guid(EventHandlerBrokerId_ed.Text));
                } // using
                EventHandlerMsg_ed.Text = "done";
            }
            catch (CBaseException ex)
            {
                EventHandlerMsg_ed.Text = ex.DeveloperMessage + "\r\n" + ex.StackTrace;
            }
            catch (Exception ex)
            {
                EventHandlerMsg_ed.Text = ex.Message + "\r\n" + ex.StackTrace;
            }
        }

        protected void AfterUpdateAllNonmasterProdsBtn_Click(object sender, System.EventArgs e)
        {


            try
            {
                using (CSingletonDistributed tix = CSingletonDistributed.RequestSingleton
                           (CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket,
                           "LoAdminMigration", 120))
                {
                    Toolbox.LoanProgram.EventHandlers.AfterUpdateAllNonmasterProds();
                } // using

                ErrorMessage.Text = "";
            }
            catch (CBaseException ex)
            {
                ErrorMessage.Text = ex.DeveloperMessage + "\r\n" + ex.StackTrace;
            }

        }

        protected void CollectMemBtn_Click(object sender, System.EventArgs e)
        {
            long memUsed = GC.GetTotalMemory(true) / 1000; // converting bytes to kbytes
            memUsedEd.Text = string.Format(" {0} KB", memUsed.ToString("N0"));
        }

        protected void gseUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                LendersOfficeApp.ObjLib.LoanLimitsData.LoanLimit.UpdateDB(LendersOfficeApp.ObjLib.LoanLimitsData.E_LoanLimitType.GSE, this.gseUrlBox.Text, DateTime.Parse(m_gseEffectiveDate.Text));
                errorPannel.Visible = false;
            }
            catch (Exception ex)
            {
                pageError.Text = ex.Message;
                errorPannel.Visible = true;
            }
        }

        protected void m_btnUpdateFhaLimit_Click(object sender, System.EventArgs e)
        {
            try
            {
                LendersOfficeApp.ObjLib.LoanLimitsData.LoanLimit.UpdateDB(LendersOfficeApp.ObjLib.LoanLimitsData.E_LoanLimitType.FHA, this.fhaUrlBx.Text, DateTime.Parse(m_FhaEffectiveDate.Text)); 
                errorPannel.Visible = false;
            }
            catch (Exception ex)
            {
                pageError.Text = ex.Message;
                errorPannel.Visible = true; 
            }
        }

        protected void UpdateConditionXmlCache_Click(object sender, System.EventArgs e)
        {
            List<Guid> loanIdList = new List<Guid>();

            Guid brokerId = new Guid (m_ConditionXmlUpdateTb.Text );
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                            , new SqlParameter("@IsValid", 1)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoansByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    loanIdList.Add((Guid)reader["LoanId"]);
                }
            }

            // Use background process. Attempting to update all of a brokers loans at the same time will
            // just cause a timeout in most cases.
            foreach (Guid loanId in loanIdList)
            {
                LendersOffice.ObjLib.Task.TaskUtilities.AddToWorkflowQueue(brokerId, loanId);
            }
        }

        protected void DumpSassDefaults_Click(object sender, EventArgs e)
        {
            var tempFile = TempFileUtils.NewTempFile().Value;

            File.AppendAllText(tempFile, SassDefaults.LqbDefaultColorTheme.Id.ToString() + Environment.NewLine);
            File.AppendAllText(tempFile, SassDefaults.LqbDefaultColorTheme.Name + Environment.NewLine);
            File.AppendAllText(tempFile, string.Join(Environment.NewLine, SassDefaults.LqbDefaultColorTheme.Variables.Select(kvp => kvp.Key + " = " + kvp.Value)));

            File.AppendAllText(tempFile, Environment.NewLine + Environment.NewLine);

            File.AppendAllText(tempFile, string.Join(Environment.NewLine, SassDefaults.UiEditorNameToSassVariableNameMappings.Select(kvp => kvp.Key + " = " + kvp.Value)));
            File.AppendAllText(tempFile, string.Join(Environment.NewLine, SassDefaults.SassVariableNameToUiEditorNameMappings.Select(kvp => kvp.Key + " = " + kvp.Value)));

            File.AppendAllText(tempFile, Environment.NewLine + Environment.NewLine);

            File.AppendAllText(tempFile, SassDefaults.CompiledDefaultPricingCss);

            File.AppendAllText(tempFile, Environment.NewLine + Environment.NewLine);

            var assembly = typeof(SassDefaults).Assembly;

            const string LqbDefaultMasterSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterSCSS.scss";
            const string LqbDefaultMasterVariableSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterVariables.scss";
            const string LqbDefaultMasterFunctionSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterFunc.scss";
            const string LqbDefaultMasterNavigationSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterNavigation.scss";
            const string LqbDefaultMasterPricingSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterPricing.scss";
            const string LqbDefaultMasterPreviewSassFileName = "LendersOffice.ObjLib.UI.Themes.MasterFiles.MasterPreview.scss";

            var LqbMasterSassContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterSassFileName);

            File.AppendAllText(tempFile, LqbMasterSassContent);

            File.AppendAllText(tempFile, Environment.NewLine + Environment.NewLine);

            var LqbMasterVariablesContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterVariableSassFileName);

            File.AppendAllText(tempFile, LqbMasterVariablesContent);

            File.AppendAllText(tempFile, Environment.NewLine + Environment.NewLine);

            var LqbMasterFunctionContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterFunctionSassFileName);

            File.AppendAllText(tempFile, LqbMasterFunctionContent);

            File.AppendAllText(tempFile, Environment.NewLine + Environment.NewLine);

            var LqbMasterNavigationContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterNavigationSassFileName);

            File.AppendAllText(tempFile, LqbMasterNavigationContent);

            File.AppendAllText(tempFile, Environment.NewLine + Environment.NewLine);

            var LqbMasterPricingContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterPricingSassFileName);

            File.AppendAllText(tempFile, LqbMasterPricingContent);

            File.AppendAllText(tempFile, Environment.NewLine + Environment.NewLine);

            var LqbMasterPreviewContent = GetEmbeddedSassContent(assembly, LqbDefaultMasterPreviewSassFileName);

            File.AppendAllText(tempFile, LqbMasterPreviewContent);

            File.AppendAllText(tempFile, Environment.NewLine + Environment.NewLine);

            RequestHelper.SendFileToClient(System.Web.HttpContext.Current, tempFile, "text/plain", "Sass_File_Dump.txt");
        }

        private static string GetEmbeddedSassContent(System.Reflection.Assembly embeddedResourceAssembly, string name)
        {
            using (var stream = new StreamReader(embeddedResourceAssembly.GetManifestResourceStream(name)))
            {
                return stream.ReadToEnd();
            }
        }
    }

    internal class CFileCachedInfo
    {
        internal CFileCachedInfo(Guid _sLId, string _sLNm)
        {
            sLId = _sLId;
            sLNm = _sLNm;
        }

        internal Guid sLId { get; private set; }
        internal string sLNm { get; private set; }
    }

    /// <summary>
    /// Fetch all variables and their dependencies for
    /// masking loans.
    /// </summary>

    public class CMaskData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        protected override bool m_enforceAccessControl
        {
            // 2/23/2005 kb - We don't need to block access because this
            // is a privelaged command.

            get
            {
                return false;
            }
        }

        static CMaskData()
        {
            StringList list = new StringList();

            list.Add("aBNm");
            list.Add("aCNm");
            list.Add("aBSsn");
            list.Add("aCSsn");
            list.Add("aBAddr");
            list.Add("aBAddrMail");
            list.Add("aBBusPhone");
            list.Add("aBHPhone");
            list.Add("aBCellPhone");
            list.Add("aBEmail");
            list.Add("aCAddr");
            list.Add("aCAddrMail");
            list.Add("aCBusPhone");
            list.Add("aCHPhone");
            list.Add("aCCellPhone");
            list.Add("aCEmail");
            list.Add("aBEmpCollection");
            list.Add("aCEmpCollection");
            list.Add("aAssetCollection");
            list.Add("aLiaCollection");
            list.Add("aReCollection");
            list.Add("sSpAddr");

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);
        }

        public CMaskData(Guid fileId)
            : base(fileId, "MaskData", s_selectProvider)
        {
        }

    }

}
