namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.AntiXss;

    /// <summary>
    ///		Manages the DBMessageQueues. 
    /// </summary>
    public partial  class Manager : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.TextBox Textbox1;

		/// <summary>
		/// If the viewstate has the correct information then it returns  a new queue with the given name and id.
		/// </summary>
		private DBQueue QueueType
		{
			get 
			{ 
				if ( ViewState["CurrentQId"] != null && ViewState["CurrentQId"] is int ) 
				{
					return new DBQueue( (int) ViewState["CurrentQId"], ViewState["CurrentQName"] == null ?  String.Empty : (string) ViewState["CurrentQName"] ); 
				}
				else 
				{
                    throw new CBaseException(ErrorMessages.QueueNotSelected, ErrorMessages.QueueNotSelected);
				}
			}
		}

		/// <summary>
		/// Returns a new dbmessage queue based on the queuetype defined by the viewstate.
		/// </summary>
		private DBMessageQueue Queue 
		{
			get 
			{
				return new DBMessageQueue( QueueType ); 
			}
		}

		protected string CurrentQueueName 
		{
			get { return ViewState["CurrentQName"] != null ? ViewState["CurrentQName"] as string : null; }
		}



		protected void PageLoad(object sender, System.EventArgs e)
		{
			m_ErrorMessage.Attributes.Add("Style", "color:Red; text-align: center; font-size: 1.3em;");
			//confirmation messages.
			DeleteMessagesButton.Attributes.Add("onclick", "return confirm('Are you sure you want to delete the messages?')");
			ClearArchiveButton.Attributes.Add("onclick","return confirm('Are you sure you want to delete the messages?');");

			if ( ! IsPostBack ) 
				BindData();
		}

		/// <summary>
		/// Updates the datagrids.
		/// </summary>
		private void BindData() 
		{
		
			m_QGrid.DataSource = DBQueue.GetQueues().Tables[0];
			m_QGrid.DataBind();


			if ( ViewState["CurrentQId"] != null ) 
			{	
				int size = 25;
				if ( ArchiveSizeBox.Text != String.Empty ) 
				{
                    size = int.Parse(ArchiveSizeBox.Text);
                    if (size < 1)
                    {
                        size = 25;
                        ArchiveSizeBox.Text = "25";
                    }
				}
                m_ArchiveGrid.DataSource = Queue.GetArchive(size);
                m_ArchiveGrid.DataBind();

                m_QMGrid.DataSource = Queue.GetMessages();
                m_QMGrid.DataBind();

                m_qArchive.Visible = true;
                q_MessagePanel.Visible = true;
			} 
			else 
			{
				m_qArchive.Visible = false;
				q_MessagePanel.Visible = false;
			}
		}

		/// <summary>
		/// Called by the grids on buttoncolumns.
		/// </summary>
		protected void Grid_OnClick( object sender, DataGridCommandEventArgs e) 
		{
			switch ( e.CommandName ) 
			{
				case "DropMessage" :
                    Queue.ReceiveById(long.Parse(e.Item.Cells[1].Text));
					break;

				case "DropArchiveMessage" :
                    Queue.DeleteFromArchive(long.Parse(e.Item.Cells[2].Text));
					break;

				case "MoveToMessage" :

                    Queue.MoveArchiveMsgToQueue(long.Parse(e.Item.Cells[2].Text));
					break;
			}

			BindData();

		
		}

		/// <summary>
		/// adds the confirmation dialog
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void QGrid_ItemDataBound(object sender, DataGridItemEventArgs e) 
		{
			if ( e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer ) 
			{
				LinkButton butn = e.Item.Cells[0].Controls[0] as LinkButton; 
				butn.Attributes["onclick"] = "javascript:return confirm(" + AspxTools.JsString("Are you sure you want to delete Queue " + e.Item.Cells[1].Text + "?") + ");";
				if ( ViewState["CurrentQId"] != null && e.Item.Cells[1].Text == ViewState["CurrentQId"].ToString() ) 
				{
					butn = e.Item.Cells[5].Controls[0] as LinkButton; 
					butn.Enabled = false; 
				}
			}
		}

		/// <summary>
		/// Handles delete on grid click.
		/// </summary>
		protected void QGrid_Delete(object sender, DataGridCommandEventArgs e ) 
		{
            int id = int.Parse(e.Item.Cells[1].Text);



            if (ViewState["CurrentQId"] != null && (int)ViewState["CurrentQId"] == id)
            {
                ViewState["CurrentQId"] = null;
                ViewState["CurrentQName"] = null;
            }
            DBQueue.DropQueue(id);

            BindData();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion


		/// <summary>
		/// Handles delete and toggle columns from the  queue type datagrid.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void QGrid_ItemCommand ( object sender, DataGridCommandEventArgs e )
		{
			if ( e.CommandName == "Toggle Archive" )  
			{
                (new DBQueue(int.Parse(e.Item.Cells[1].Text), String.Empty)).ToggleArchiving();
			}  
			else if ( e.CommandName == "SetMessageId" ) 
			{
				ViewState["CurrentQId"] = Int32.Parse(e.Item.Cells[1].Text );
				ViewState["CurrentQName"] = e.Item.Cells[2].Text;
			}
			BindData();
		}

		/// <summary>
		/// Handles Delete Button Click on Q Datagrid.
		/// </summary>
		protected void DeleteMessageBtn_Click( object sender, System.EventArgs e) 
		{
			if ( ViewState["CurrentQId"] != null ) 
			{
                Queue.ClearQueue();
                Queue.ClearArchive();
                BindData();
			}
			else 
			{
				m_ErrorMessage.Text = "Error: There is no queue selected!"; //should never happen
			}
		}

		/// <summary>
		/// Handles Send message click.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void m_SndMsg_Click( object sender, System.EventArgs e ) 
		{
            string body = DatalocList.SelectedItem.Value == "db" ? "Jlas" : new String(new char[6000]);
            Queue.Send(m_Subject1.Text, body, m_Subject2.Text);
            BindData();

            m_Subject1.Text = String.Empty;
            m_Priority.SelectedIndex = 2;
            m_Subject2.Text = String.Empty;
            DatalocList.SelectedIndex = 0; 
		}

		/// <summary>
		/// Handles UI Clear button click. 
		/// </summary>
		protected void ClrArcBtn_Click( object sender, System.EventArgs e) 
		{
            Queue.ClearArchive();
            BindData();
		}
		/// <summary>
		/// Handle Ui Add Queue Click;
		/// </summary>
		protected void SbmtBtn_AddQueue( object sender, System.EventArgs e) 
		{
			if ( m_QName.Text.Length == 0 )  
			{
				m_ErrorMessage.Text = "Error: Name is required in order to add a queue..";
				return; 
			}
			else  
			{
				DBQueue.CreateQueue(m_QName.Text, m_QArchiveRL.SelectedItem.Text == "On" );
				m_QName.Text = String.Empty;
				BindData();
			}
		}

		/// <summary>
		/// Handle Ui Click Submit Query;
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void UpdateSize_Click( object sender, System.EventArgs e ) 
		{
			BindData();
		}
	}
}
