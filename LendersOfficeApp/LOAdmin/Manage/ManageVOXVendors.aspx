﻿<%@ Page language="C#" Codebehind="ManageVOXVendors.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageVOXVendors" Title="Manage VOX Vendors"
    MasterPageFile="~/LOAdmin/loadmin.Master" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ContentPlaceHolderID="Main" runat="server">
    <style>
        .center-div
        {
            margin: 0 auto;
            min-width: 200px;
            width: 80%;
        }
        .NameRow
        {
            width: 60%;
        }
        .modal-dialog
        {
            width: 800px;
        }
        .modal-dialog .dialog-input
        {
            padding-left: 5px;
            width: 400px;
        }
        .modal-dialog .form-inline
        {
            margin: 3px;
        }
        .input-label
        {
            width: 20%;
        }
        .indent
        {
            margin-left: 15px;
        }
        .Bordered
        {
            padding: 5px;
            border: 1px solid #e5e5e5;
        }
        .BaseCopy
        {
            display: none;
        }
        .UndecoratedList
        {
            list-style-type: none;
            padding-left: 0;
        }
        .form-inline .MiniInput
        {
            width: 60px;
        }
        .OptionDiv
        {
            display: inline-block; 
            padding-left: 10px;
        }
        .form-inline .TopAlign
        {
            vertical-align: top;
        }
        .Error
        {
            color: red;
        }
        .Hidden
        {
            display: none;
        }
    </style>
    <script>
        jQuery(function ($) {
            $('.To3rdPartyCertPage').click(function () {
                window.location = gVirtualRoot + '/LOAdmin/Manage/ManageThirdPartyClientCertificates.aspx';
            });

            $('a[data-sort="true"]').click(function () {
                var link = $(this);
                var th = $(this).parents('th').eq(0);
                var table = th.parents('table').eq(0);
                var rows = table.find('tr:gt(0):not(.BaseCopy)').toArray().sort(RowComparer(link.data('sorttarget')));

                link.data('asc', !link.data('asc'));
                if (!link.data('asc')) {
                    rows = rows.reverse();
                }

                for (var i = 0; i < rows.length; i++) {
                    table.append(rows[i]);
                }
            });

            function RowComparer(sortTargetClass) {
                return function (a, b) {
                    var sortTargetA = $(a).find('.' + sortTargetClass);
                    var sortTargetB = $(b).find('.' + sortTargetClass);
                    if (sortTargetA.is(':checkbox'))
                    {
                        var boolA = sortTargetA.is(':checked');
                        var boolB = sortTargetB.is(':checked');

                        if (boolA === true && boolB === false) {
                            return 1;
                        }
                        else if (boolA === false && boolB === true) {
                            return -1;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                    else
                    {
                        var nameA = sortTargetA.text();
                        var nameB = sortTargetB.text();
                        if (nameA < nameB) {
                            return -1;
                        }
                        else if (nameA > nameB) {
                            return 1;
                        }
                        else {
                            return 0;
                        }
                    }
                }
            }

            function ClearInputs(targetSelector) {
                $(targetSelector).find(':input')
                    .not(':button, :submit, :reset').prop('checked', false)
                    .not(':checkbox, :radio, select').val('');
                $(targetSelector).find('option:selected').prop('selected', false);
                $(targetSelector).find('.ClearableList').empty();
            }

            $('.CancelBtn').click(function () {
                ClearInputs($(this).closest('.Configuration'));
            });

            $('.TabLink').click(function () {
                $('.TabLink').each(function (index, value) {
                    $('#' + $(value).data('panel')).hide();
                    $(this).parent('li').removeClass('active');
                });

                var panelToShow = $(this).data('panel');
                $(this).parent('li').addClass('active');
                $('#' + panelToShow).show();
            });

            /**** TRANSMISSION SECTION ****/
            $('[id$="TransmissionAuthenticationT"]').change(function () {
                ToggleByAuthenticationMethod($(this));
            });

            function ToggleByAuthenticationMethod(toggledDropdown) {
                var value = toggledDropdown.val();
                var popup = toggledDropdown.closest('.Configuration');

                // TransmissionAuthenticationT.BasicAuthentication
                popup.find('.BasicAuthField').toggle(value === '0');

                // TransmissionAuthenticationT.DigitalCert
                popup.find('.DigitalCertField').toggle(value === '1');
            }

            $('[id$="UsesAccountId"]').change(function () {
                ToggleAccountCheckbox($(this));
            });

            function ToggleAccountCheckbox(usesAccountCheckbox) {
                var checked = usesAccountCheckbox.is(':checked');
                var popup = usesAccountCheckbox.closest('.Configuration');
                var target = popup.find('[id$="RequiresAccountId"]');
                target.prop('disabled', !checked);
                if(!checked)
                {
                    target.prop('checked', false);
                }
            }

            function PopulateTransmission(prefix, viewModel) {
                $('#' + prefix + 'TargetUrl').val(viewModel.TargetUrl);
                $('[id$="' + prefix + 'TransmissionAuthenticationT"]').val(viewModel.TransmissionAuthenticationT);
                $('#' + prefix + 'ResponseCertificateId').val(viewModel.ResponseCertificateId);
                $('#' + prefix + 'RequestCredentialName').val(viewModel.RequestCredentialName);
                $('#' + prefix + 'RequestCredentialPassword').val(viewModel.RequestCredentialPassword);
                $('#' + prefix + 'ResponseCredentialName').val(viewModel.ResponseCredentialName);
                $('#' + prefix + 'ResponseCredentialPassword').val(viewModel.ResponseCredentialPassword);
            }

            function GetTransmission(prefix)
            {
                var transmission =
                {
                    TargetUrl: $('#' + prefix + 'TargetUrl').val(),
                    TransmissionAuthenticationT: $('[id$="' + prefix + 'TransmissionAuthenticationT"]').val(),
                    ResponseCertificateId: $('#' + prefix + 'ResponseCertificateId').val(),
                    RequestCredentialName: $('#' + prefix + 'RequestCredentialName').val(),
                    RequestCredentialPassword: $('#' + prefix + 'RequestCredentialPassword').val(),
                    ResponseCredentialName: $('#' + prefix + 'ResponseCredentialName').val(),
                    ResponseCredentialPassword: $('#' + prefix + 'ResponseCredentialPassword').val(),
                };

                return transmission;
            }

            /**** PLATFORM POPUP ****/
            $('#PlatformSaveBtn').click(function () {
                var transmission = GetTransmission('P');

                var platform =
                {
                    PlatformId: $('#PlatformId').val(),
                    PlatformName: $('#PlatformName').val(),
                    TransmissionModelT: $('[id$="TransmissionModelT"]').val(),
                    PayloadFormatT: $('[id$="PayloadFormatT"]').val(),
                    UsesAccountId: $('#UsesAccountId').is(':checked'),
                    RequiresAccountId: $('#RequiresAccountId').is(':checked'),
                    TransmissionInfo: transmission
                };

                var data =
                {
                    PlatformViewModel: JSON.stringify(platform)
                };

                var result = gService.main.call("SavePlatform", data);
                if (!result.error) {
                    if (result.value['Success'] === 'True') {
                        $('#PlatformConfiguration').modal('hide');
                        ClearInputs('#PlatformConfiguration');
                        if (result.value["IsNew"] === 'True') {
                            var cloneRow = $('#PlatformPanel').find('.BaseCopy').clone().removeClass('BaseCopy');
                            cloneRow.find('.PlatformName').text(result.value["PlatformName"]);
                            cloneRow.data('platformid', result.value["PlatformId"]);
                            $('#PlatformPanel').find('table').append(cloneRow);
                            $('[id$="AssociatedPlatformId"]').append($('<option>', { value: result.value["PlatformId"], text: result.value["PlatformName"] }));
                        }
                        else
                        {
                            var row = $('#PlatformPanel').find('tr').filter(function () { return $(this).data('platformid') == result.value["PlatformId"] });
                            row.find('.PlatformName').text(result.value["PlatformName"]);
                            $('[id$="AssociatedPlatformId"]').find('option[value="' + result.value["PlatformId"] + '"]').text(result.value["PlatformName"]);
                        }
                    }
                    else {
                        alert(result.value['Errors']);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            });

            /**** PLATFORM TAB ****/
            $('#AddPlatformBtn').click(function () {
                $('#PlatformConfiguration').modal('show');
                ClearInputs('#PlatformConfiguration')
                $('#PTransmissionId').val('');

                ToggleAccountCheckbox($('#UsesAccountId'));
                ToggleByAuthenticationMethod($('[id$="PTransmissionAuthenticationT"]'));
            });

            $('#PlatformPanel').on('click', '.PlatformDelete', function () {
                var parentTr = $(this).closest('tr');
                var platformId = parentTr.data('platformid');
                var platformName = parentTr.find('.PlatformName').text();

                if(confirm("Really delete Platform <" + platformName + ">"))
                {
                    var data = {
                        PlatformId: platformId
                    };

                    var result = gService.main.call("DeletePlatform", data);
                    if(!result.error)
                    {
                        if (result.value["Success"] === 'True')
                        {
                            parentTr.remove();
                            $('[id$="AssociatedPlatformId"]').find('option[value="'+ platformId + '"]').remove();
                        }
                        else
                        {
                            alert(result.value['Errors']);
                        }
                    }
                    else
                    {
                        alert(result.UserMessage);
                    }
                }
            });

            $('#PlatformPanel').on('click', '.PlatformEdit', function () {
                var platformId = $(this).closest('tr').data('platformid');
                var data = {
                    PlatformId: platformId
                };

                var result = gService.main.call("RetrievePlatform", data);
                if (!result.error) {
                    if (result.value['Success'] === 'True') {
                        ClearInputs('#PlatformConfiguration');

                        var viewModel = JSON.parse(result.value["PlatformViewModel"]);
                        $('#PlatformId').val(viewModel.PlatformId);
                        $('#PlatformName').val(viewModel.PlatformName);
                        $('[id$="TransmissionModelT"]').val(viewModel.TransmissionModelT);
                        $('[id$="PayloadFormatT"]').val(viewModel.PayloadFormatT);
                        $('#UsesAccountId').prop('checked', viewModel.UsesAccountId);
                        $('#RequiresAccountId').prop('checked', viewModel.RequiresAccountId);
                        PopulateTransmission('P', viewModel.TransmissionInfo);

                        $('#PlatformConfiguration').modal('show');
                        ToggleAccountCheckbox($('#UsesAccountId'));
                        ToggleByAuthenticationMethod($('[id$="PTransmissionAuthenticationT"]'));
                    }
                    else {
                        alert(result.value["Errors"]);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            });

            /**** VENDOR TAB ****/
            $('[id$="AssociatedPlatformId"]').change(function () {
                if ($(this).val() == '0')
                {
                    return;
                }

                var data = {
                    PlatformId: $(this).val()
                };

                var result = gService.main.call("RetrievePlatform", data);
                if (result.error) {
                    alert(result.UserMessage);
                } else if (result.value['Success'] !== 'True') {
                    alert(result.value["Errors"]);
                } else {
                    var viewModel = JSON.parse(result.value["PlatformViewModel"]);
                    $('#IsOverridingPlatform').prop('checked', false);
                    TogglePlatformOverridable();
                    PopulateTransmission('V', viewModel.TransmissionInfo);
                    ToggleByAuthenticationMethod($('[id$="VTransmissionAuthenticationT"]'));
                }
            });

            $('#IsOverridingPlatform').change(function () {
                var checked = this.checked;
                TogglePlatformOverridable();
                if(!checked)
                {
                    if ($('[id$="AssociatedPlatformId"]').val() == '0')
                    {
                        return;
                    }

                    var data = {
                        PlatformId: $('[id$="AssociatedPlatformId"]').val()
                    };

                    var result = gService.main.call("RetrievePlatform", data);
                    if (result.error) {
                        alert(result.UserMessage);
                    } else if (result.value['Success'] !== 'True') {
                        alert(result.value["Errors"]);
                    } else {
                        var viewModel = JSON.parse(result.value["PlatformViewModel"]);
                        PopulateTransmission('V', viewModel.TransmissionInfo);
                        ToggleByAuthenticationMethod($('[id$="VTransmissionAuthenticationT"]'));
                    }
                }
            });

            $('#AddVendorBtn').click(function () {
                ClearInputs('#VendorConfiguration');
                $('#VendorConfiguration').modal('show');
                InitializeNewVoaService();
                ToggleByAuthenticationMethod($('[id$="VTransmissionAuthenticationT"]'));
                $('.ServiceUsed').trigger('change');
                TogglePlatformOverridable();
                CheckServices();
            });

            $('#VendorSaveBtn').click(function () {
                var transmission = GetTransmission('V');
                var voaService = GenerateVOAService();
                var voeService = GenerateVOEService();
                var ssa89Service = GenerateSSA89Service();
                var vendor = {
                    VendorId: $('#VendorId').val(),
                    CompanyName: $('#CompanyName').val(),
                    IsEnabled: $('#IsEnabled').is(':checked'),
                    IsTestVendor: $('#IsTestVendor').is(':checked'),
                    CanPayWithCreditCard: $('#CanPayWithCreditCard').prop('checked'),
                    DuValidationServiceProviderId: $('[id$="DuValidationServiceProviderId"]').val(),
                    AssociatedPlatformId: $('[id$="AssociatedPlatformId"]').val(),
                    IsOverridingPlatform: $('#IsOverridingPlatform').is(':checked'),
                    MclCraId: $('#MclCraId').val(),
                    TransmissionInfo: transmission,
                    VOAService: voaService,
                    VOEService: voeService,
                    SSA89Service: ssa89Service
                };

                var data = {
                    VendorInfoViewModel: JSON.stringify(vendor)
                };

                var result = gService.main.call("SaveVendor", data);
                if(!result.error){
                    if(result.value['Success'] === 'True') {
                        $('#VendorConfiguration').modal('hide');
                        ClearInputs('#VendorConfiguration');
                        if (result.value["IsNew"] === 'True') {
                            var cloneRow = $('#VendorPanel').find('.BaseCopy').clone().removeClass('BaseCopy');
                            cloneRow.find('.CompanyName').text(result.value["CompanyName"]);
                            cloneRow.data('vendorid', result.value["VendorId"]);
                            cloneRow.find('.IsEnabled').prop('checked', result.value["IsEnabled"] === 'True');
                            cloneRow.find('.IsTestVendor').prop('checked', result.value["IsTestVendor"] === 'True');
                            $('#VendorPanel').find('table').append(cloneRow);
                        }
                        else {
                            var row = $('#VendorPanel').find('tr').filter(function () { return $(this).data('vendorid') == result.value["VendorId"]; });
                            row.find('.CompanyName').text(result.value["CompanyName"]);
                            row.find('.IsEnabled').prop('checked', result.value["IsEnabled"] === 'True');
                            row.find('.IsTestVendor').prop('checked', result.value["IsTestVendor"] === 'True');
                        }
                    }
                    else
                    {
                        alert(result.value["Errors"]);
                    }                        
                }
                else
                {
                    alert(result.UserMessage);
                }
            });

            $('#VendorPanel').on('click', '.VendorEdit', function () {
                var vendorId = $(this).closest('tr').data('vendorid');
                var data = {
                    VendorId: vendorId
                };

                var result = gService.main.call("RetrieveVendor", data);
                if (!result.error) {
                    if (result.value['Success'] === 'True') {
                        ClearInputs('#VendorConfiguration');

                        var viewModel = JSON.parse(result.value["VendorViewModel"]);
                        $('#VendorId').val(viewModel.VendorId);
                        $('#CompanyName').val(viewModel.CompanyName);
                        $('#IsEnabled').prop('checked', viewModel.IsEnabled);
                        $('#IsTestVendor').prop('checked', viewModel.IsTestVendor);
                        $('#CanPayWithCreditCard').prop('checked', viewModel.CanPayWithCreditCard);
                        $('[id$="DuValidationServiceProviderId"]').val(viewModel.DuValidationServiceProviderId);
                        $('[id$="AssociatedPlatformId"]').val(viewModel.AssociatedPlatformId);
                        $('#IsOverridingPlatform').prop('checked', viewModel.IsOverridingPlatform);
                        $('#MclCraId').val(viewModel.MclCraId);
                        PopulateTransmission('V', viewModel.TransmissionInfo);

                        PopulateVOAService(viewModel.VOAService);
                        PopulateVOEService(viewModel.VOEService);
                        PopulateSSA89Service(viewModel.SSA89Service);

                        $('#VendorConfiguration').modal('show');
                        ToggleByAuthenticationMethod($('[id$="VTransmissionAuthenticationT"]'));
                        $('.ServiceUsed').trigger('change');
                        TogglePlatformOverridable();
                        CheckServices();
                    }
                    else {
                        alert(result.value["Errors"]);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            });
            
            $('#VendorPanel').on('click', '.VendorDelete', function () {
                var parentTr = $(this).closest('tr');
                var vendorId = parentTr.data('vendorid');
                var vendorName = parentTr.find('.CompanyName').text();

                if (confirm("Really delete Vendor <" + vendorName + ">")) {
                    var data = {
                        VendorId: vendorId
                    };

                    var result = gService.main.call("DeleteVendor", data);
                    if (!result.error) {
                        if (result.value["Success"] === 'True') {
                            parentTr.remove();
                        }
                        else {
                            alert(result.value['Errors']);
                        }
                    }
                    else {
                        alert(result.UserMessage);
                    }
                }
            });

            $('.IsADirectVendor, .IsAReseller').change(function () {
                CheckServices();
            });

            function CheckServices()
            {
                var checkedServices = $('.IsADirectVendor:checked').length + $('.IsAReseller:checked').length;

                if (checkedServices === 0) {
                    $('[id$="AssociatedPlatformId"]').val(0).prop('disabled', true);
                    $('#IsOverridingPlatform').prop('checked', false).prop('disabled', true);
                    TogglePlatformOverridable();
                    $('#PlatformError').show();
                    return true;
                }
                else {
                    $('[id$="AssociatedPlatformId"]').prop('disabled', false);
                    $('#IsOverridingPlatform').prop('disabled', false);
                    TogglePlatformOverridable();
                    $('#PlatformError').hide();
                    return false;
                }
            }

            function TogglePlatformOverridable(shouldDisable)
            {
                if (typeof (shouldDisable) === 'undefined') {
                    shouldDisable = $('#IsOverridingPlatform').is(':not(:checked)');
                }

                $('#VTransmission').find(':input').not('.AlwaysEditable').prop('disabled', shouldDisable);
            }

            $('.ServiceUsed').on('change', function (e) {
                $('#' + $(this).data('service') + 'ServiceSection').find(':input').prop('disabled', !this.checked);
            });

            /**** VOA services ****/
            $('.AddOption').click(function () {
                var id = $(this).data('target');
                $('#' + id).append(CreateOption(""));
            });

            $('#VOAServiceSection').on('click', '.DeleteOption', function () {
                $(this).closest('li').remove();
            });

            function PopulateVOAService(viewModel)
            {
                if (viewModel === null) {
                    return;
                }

                $('#VoaServiceId').val(viewModel.VendorServiceId);
                $('#IsVoaEnabled').prop('checked', viewModel.IsEnabled);
                $('#VOA_IsADirectVendor').prop('checked', viewModel.IsADirectVendor);
                $('#VOA_SellsViaResellers').prop('checked', viewModel.SellsViaResellers);
                $('#VOA_IsAReseller').prop('checked', viewModel.IsAReseller);
                $('#AskForNotificationEmail').prop('checked', viewModel.AskForNotificationEmail);
                $('[name="VoaVerificationT"][value="' + viewModel.VoaVerificationT + '"]').prop('checked', true);
                var refreshList = $('#RefreshPeriodOptions');
                $.each(viewModel.RefreshPeriodOptions, function (index, value) {
                    refreshList.append(CreateOption(value));
                });

                var acountHistory = $('#AccountHistoryOptions');
                $.each(viewModel.AccountHistoryOptions, function (index, value) {
                    acountHistory.append(CreateOption(value));
                });
            }

            function GenerateVOAService()
            {
                var service = {
                    VendorServiceId: $('#VoaServiceId').val(),
                    IsEnabled: $('#IsVoaEnabled').is(':checked'),
                    IsADirectVendor: $('#VOA_IsADirectVendor').is(':checked'),
                    SellsViaResellers: $('#VOA_SellsViaResellers').is(':checked'),
                    IsAReseller: $('#VOA_IsAReseller').is(':checked'),
                    AskForNotificationEmail: $('#AskForNotificationEmail').is(':checked'),
                    VoaVerificationT: $('[name="VoaVerificationT"]:checked').val(),
                    RefreshPeriodOptions: $('#RefreshPeriodOptions> li').map(function(i, el) { return $(el).find('.MiniInput').val(); }).get(),
                    AccountHistoryOptions: $('#AccountHistoryOptions > li').map(function(i, el) { return $(el).find('.MiniInput').val(); }).get(),
                };

                return service;
            }

            function CreateOption(value)
            {
                var input = $('<input>', { type: 'text', class: "form-control MiniInput", value: value });
                var button = $('<input>', { type: 'button', class: "btn btn-default DeleteOption", value: '-' });
                var li = $('<li>').append(input).append(button);
                return li;
            }

            function InitializeNewVoaService()
            {
                $('#AccountHistoryOptions').append(['30', '60', '90', '999'].map(CreateOption));
                $('#RefreshPeriodOptions').append(['0', '30', '60', '90'].map(CreateOption));
            }


            /**** VOE SERVICE ****/
            function PopulateVOEService(viewModel) {
                if (viewModel === null) {
                    return;
                }

                $('#VoeServiceId').val(viewModel.VendorServiceId);
                $('#IsVoeEnabled').prop('checked', viewModel.IsEnabled);
                $('#VOE_IsADirectVendor').prop('checked', viewModel.IsADirectVendor);
                $('#VOE_SellsViaResellers').prop('checked', viewModel.SellsViaResellers);
                $('#VOE_IsAReseller').prop('checked', viewModel.IsAReseller);
                $('[name="VoeVerificationT"][value="' + viewModel.VerificationType + '"]').prop('checked', true);
                $('#AskForSalaryKey').prop('checked', viewModel.AskForSalaryKey);
            }

            function GenerateVOEService() {
                var service = {
                    VendorServiceId: $('#VoeServiceId').val(),
                    IsEnabled: $('#IsVoeEnabled').is(':checked'),
                    IsADirectVendor: $('#VOE_IsADirectVendor').is(':checked'),
                    SellsViaResellers: $('#VOE_SellsViaResellers').is(':checked'),
                    IsAReseller: $('#VOE_IsAReseller').is(':checked'),
                    VerificationType: $('[name="VoeVerificationT"]:checked').val(),
                    AskForSalaryKey: $('#AskForSalaryKey').is(':checked')
                };

                return service;
            }

            /**** SSA-89 Service ****/
            function PopulateSSA89Service(viewModel) {
                if (viewModel == null) {
                    return;
                }

                $('#Ssa89ServiceId').val(viewModel.VendorServiceId);
                $('#IsSsa89Enabled').prop('checked', viewModel.IsEnabled);
                $('#SSA89_IsADirectVendor').prop('checked', viewModel.IsADirectVendor);
                $('#SSA89_SellsViaResellers').prop('checked', viewModel.SellsViaResellers);
                $('#SSA89_IsAReseller').prop('checked', viewModel.IsAReseller);
            }

            function GenerateSSA89Service() {
                return {
                    VendorServiceId: $('#Ssa89ServiceId').val(),
                    IsEnabled: $('#IsSsa89Enabled').is(':checked'),
                    IsADirectVendor: $('#SSA89_IsADirectVendor').is(':checked'),
                    SellsViaResellers: $('#SSA89_SellsViaResellers').is(':checked'),
                    IsAReseller: $('#SSA89_IsAReseller').is(':checked')
                };
            }
        });
    </script>

    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="center-div">
                    <div>
                        <ul class="nav nav-tabs">
                            <li class="active" id="VendorTab"><a class="TabLink" data-panel="VendorPanel">Vendors</a></li>
                            <li id="PlatformTab"><a class="TabLink" data-panel="PlatformPanel">Platforms</a></li>
                        </ul>
                    </div>
                    <div class="panel panel-default Hidden" id="PlatformPanel">
                        <div class="panel-body div-wrap-tb">
                            <table class="table table-striped table-condensed table-layout-fix">
                                <thead>
                                    <tr>
                                        <th class="NameRow">
                                            <a data-sort="true" data-sorttarget="PlatformName" data-asc="false">Platform</a>
                                        </th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="BaseCopy" data-platformid="">
                                        <td>
                                            <span class="PlatformName"></span>
                                        </td>
                                        <td>
                                            <a class="PlatformEdit">edit</a>
                                        </td>
                                        <td>
                                            <a class="PlatformDelete">delete</a>
                                        </td>
                                    </tr>
                                    <asp:Repeater runat="server" ID="Platforms">
                                        <ItemTemplate>
                                            <tr data-platformid="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PlatformId").ToString()) %>">
                                                <td>
                                                    <span class="PlatformName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PlatformName").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <a class="PlatformEdit">edit</a>
                                                </td>
                                                <td>
                                                    <a class="PlatformDelete">delete</a>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>        
                                </tbody>
                            </table>
                            <span class="clipper">
                                <button type="button" class="btn btn-primary" id="AddPlatformBtn">Add New</button>
                            </span>
                        </div>
                    </div>
                    <div class="panel panel-default" id="VendorPanel">
                        <div class="panel-body div-wrap-tb">
                            <table class="table table-striped table-condensed table-layout-fix">
                                <thead>
				                    <tr>
					                    <th class="NameRow">
						                    <a data-sort="true" data-sorttarget="CompanyName" data-asc="false">Vendor</a>
					                    </th>
					                    <th>
                                            <a data-sort="true" data-sorttarget="IsEnabled" data-asc="false">Enabled?</a>
					                    </th>
					                    <th>
                                            <a data-sort="true" data-sorttarget="IsTestVendor" data-asc="false">Test Vendor?</a>
					                    </th>
                                        <th></th>
                                        <th></th>
				                    </tr>
			                    </thead>
                                <tbody>
                                    <tr class="BaseCopy" data-vendorid="">
                                        <td>
                                            <span class="CompanyName"></span>
                                        </td>
                                        <td>
                                            <input type="checkbox" disabled="disabled" class="IsEnabled" />
                                        </td>
                                        <td>
                                            <input type="checkbox" disabled="disabled" class="IsTestVendor" />
                                        </td>
                                        <td>
                                            <a class="VendorEdit">edit</a>
                                        </td>
                                        <td>
                                            <a class="VendorDelete">delete</a>
                                        </td>
                                    </tr>
                                    <asp:Repeater runat="server" ID="Vendors" OnItemDataBound="Vendors_ItemDataBound">
					                    <ItemTemplate>
						                    <tr data-vendorid="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "VendorId").ToString()) %>">
							                    <td>
                                                    <span class="CompanyName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CompanyName").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <input type="checkbox" disabled="disabled" class="IsEnabled" runat="server" id="IsEnabled"/>
                                                </td>
                                                <td>
                                                    <input type="checkbox" disabled="disabled" class="IsTestVendor" runat="server" id="IsTestVendor"/>
                                                </td>
                                                <td>
                                                    <a class="VendorEdit">edit</a>
                                                </td>
                                                <td>
                                                    <a class="VendorDelete">delete</a>
                                                </td>
						                    </tr>
					                    </ItemTemplate>
				                    </asp:Repeater>  
                                </tbody>
                            </table>
                            <span class="clipper">
			                    <button type="button" class="btn btn-primary" id="AddVendorBtn">Add New</button>
		                    </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="PlatformConfiguration" class="modal v-middle fade Configuration" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 claSS="modal-title">Platform Configuration</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="PlatformId" />
                        <div class="form-inline">
                            <label class="input-label">Platform</label>
                            <input type="text" id="PlatformName" class="dialog-input form-control" />
                        </div>
                        <br />
                        <div>
                            Transmission Details
                        </div>
                        <div class="Bordered">
                            <div class="form-inline">
                                <label class="input-label">Transmission model</label>
                                <asp:DropDownList runat="server" ID="TransmissionModelT" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="form-inline">
                                <label class="input-label">Payload Format</label>
                                <asp:DropDownList runat="server" ID="PayloadFormatT" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="form-inline">
                                <label class="input-label">Target URL</label>
                                <input type="text" id="PTargetUrl" class="dialog-input form-control" />
                            </div>
                            <div class="form-inline">
                                <label class="input-label">Authentication</label>
                                <asp:DropDownList runat="server" ID="PTransmissionAuthenticationT" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="form-inline DigitalCertField">
                                <div>
                                    <a class="To3rdPartyCertPage">Add Request Certificate to the Third Party Certificate Manager</a>
                                </div>
                                <div>
                                    <span>Note: Certificate's associated urls must contain this Transmission Detail's Target URL.</span>
                                </div>
                            </div>
                            <div class="form-inline DigitalCertField">
                                <label class="input-label">Response Cert ID</label>
                                <input type="text" id="PResponseCertificateId" class="dialog-input form-control" />
                            </div>
                            <div class="form-inline BasicAuthField">
                                <label class="input-label">Request Credential</label>
                                <label>Name</label>
                                <input type="text" id="PRequestCredentialName" class="form-control" />
                                <label>Password</label>
                                <input type="password" id="PRequestCredentialPassword" class="form-control" />
                            </div>
                            <div class="form-inline BasicAuthField">
                                <label class="input-label">Response Credential</label>
                                <label>Name</label>
                                <input type="text" id="PResponseCredentialName" class="form-control" />
                                <label>Password</label>
                                <input type="password" id="PResponseCredentialPassword" class="form-control" />
                            </div>
                            <div class="form-group checkbox">
                                <label class="control-label">
                                    <input type="checkbox" id="UsesAccountId" />
                                    This Platform uses an Account Id
                                </label>
                                <div class="indent">
                                    <label class="control-label">
                                        <input type="checkbox" id="RequiresAccountId" />
                                        This Platform requires an Account Id
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span class="clipper">
                            <input type="button" class="btn btn-default CancelBtn" id="PlatformCancelBtn" data-dismiss="modal" value="Cancel"/>
                        </span>
                        <span class="clipper">
                            <input type="button" class="btn btn-primary" id="PlatformSaveBtn" value="Save" />
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div id="VendorConfiguration" class="modal v-middle fade Configuration" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Vendor Configuration</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-inline">
                            <label class="input-label">Company Name</label>
                            <input type="text" id="CompanyName" class="dialog-input form-control" />
                        </div>
                        <div class="form-inline">
                            <label class="input-label">Vendor Id</label>
                            <input type="text" id="VendorId" class="form-control" disabled="disabled" />
                        </div>
                        <div class="form-inline">
                            <label class="input-label">Enabled?</label>
                            <input type="checkbox" class="checkbox" id="IsEnabled" />
                        </div>
                        <div class="form-inline">
                            <label class="input-label">Test Vendor?</label>
                            <input type="checkbox" class="checkbox" id="IsTestVendor" />
                        </div>
                        <br />
                        <div>Services</div>
                        <div class="Bordered">
                            <div class="form-group checkbox">
                                <input type="hidden" id="VoaServiceId" />
                                <label class="control-label">
                                    <input type="checkbox" id="IsVoaEnabled" class="ServiceUsed" data-service="VOA" />VOA
                                </label>
                            </div>
                            <div id="VOAServiceSection">
                                <div class="indent">
                                    <div class="form-inline checkbox">
                                        For VOA/VOD, this vendor... (check all that apply):
                                        <label class="control-label">
                                            <input type="checkbox" class="IsADirectVendor" id="VOA_IsADirectVendor" />Is a Direct Vendor &nbsp;
                                        </label>
                                        <label class="control-label">
                                            <input type="checkbox" id="VOA_SellsViaResellers" />Sells via Provider &nbsp;
                                        </label>
                                        <label class="control-label">
                                            <input type="checkbox" class="IsAReseller" id="VOA_IsAReseller" />Is a Provider &nbsp;
                                        </label>
                                    </div>
                                    <div class="form-inline">
                                        <label class="control-label TopAlign">Refresh Period Options:</label>
                                        <div class="OptionDiv">
                                            <div>Number of days (0 = 'None', 999 = 'Max')</div>
                                            <ul class="UndecoratedList ClearableList" id="RefreshPeriodOptions">
                                                <li>
                                                </li>
                                            </ul>
                                            <input type="button" class="btn btn-default AddOption" data-target="RefreshPeriodOptions" value="Add option" />
                                        </div>
                                    </div>
                                    <div class="form-inline">
                                        <label class="control-label TopAlign">Account History Options:</label>
                                        <div class="OptionDiv">
                                            <div>Number of days (0 = 'None', 999 = 'Max')</div>
                                            <ul class="UndecoratedList ClearableList" id="AccountHistoryOptions">
                                                <li>
                                                </li>
                                            </ul>
                                            <input type="button" class="btn btn-default AddOption" data-target="AccountHistoryOptions" value="Add option" />
                                        </div>
                                    </div>
                                    <div class="form-inline checkbox">
                                        <label class="control-label">
                                            <input type="checkbox" id="AskForNotificationEmail" />Ask for notification email?
                                        </label>
                                    </div>
                                    <div class="form-inline">
                                        <input type="radio" name="VoaVerificationT" value="0"/>Assets
                                        <input type="radio" name="VoaVerificationT" value="1" />Deposits
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group checkbox">
                                <input type="hidden" id="VoeServiceId" />
                                <label class="control-label">
                                    <input type="checkbox" id="IsVoeEnabled" class="ServiceUsed" data-service="VOE" />VOE
                                </label>
                            </div>
                            <div id="VOEServiceSection">
                                <div class="indent">
                                    <div class="form-inline checkbox">
                                        For VOE, this vendor... (check all that apply):
                                        <label class="control-label">
                                            <input type="checkbox" class="IsADirectVendor" id="VOE_IsADirectVendor" />Is a Direct Vendor &nbsp;
                                        </label>
                                        <label class="control-label">
                                            <input type="checkbox" id="VOE_SellsViaResellers" />Sells via Provider &nbsp;
                                        </label>
                                        <label class="control-label">
                                            <input type="checkbox" class="IsAReseller" id="VOE_IsAReseller" />Is a Provider &nbsp;
                                        </label>
                                    </div>
                                    <div class="form-inline">
                                        <input type="radio" name="VoeVerificationT" value="0"/>Employment
                                        <input type="radio" name="VoeVerificationT" value="1" />Employment + Income
                                    </div>
                                    <div class="form-inline checkbox">
                                        <label class="control-label">
                                            <input type="checkbox" id="AskForSalaryKey" />Ask for Salary Key?
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group checkbox">
                                <input type="hidden" id="Ssa89ServiceId" />
                                <label class="control-label">
                                    <input type="checkbox" id="IsSsa89Enabled" class="ServiceUsed" data-service="SSA89" />SSA-89
                                </label>
                            </div>
                            <div id="SSA89ServiceSection">
                                <div class="indent">
                                    <div class="form-inline checkbox">
                                        This vendor:
                                        <label class="control-label">
                                            <input type="checkbox" class="IsADirectVendor" id="SSA89_IsADirectVendor" />Is a Direct Vendor&nbsp;
                                        </label>
                                        <label class="control-label">
                                            <input type="checkbox" id="SSA89_SellsViaResellers" />Sells via Provider&nbsp;
                                        </label>
                                        <label class="control-label">
                                            <input type="checkbox" class="IsAReseller" id="SSA89_IsAReseller" />Is a Provider&nbsp;
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="form-group checkbox">
                                <label class="control-label">
                                    <input type="checkbox" id="CanPayWithCreditCard" />Pay with Credit Card?
                                </label>
                            </div>
                            <div class="form-inline">
                                <label>DU Validation Service Provider Name:</label>
						        <asp:DropDownList runat="server" ID="DuValidationServiceProviderId" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                        <br />
                        <div class="form-inline">
                            <label class="input-label">Platform</label>
                            <asp:DropDownList runat="server" ID="AssociatedPlatformId" CssClass="form-control"></asp:DropDownList>
                            <label class="control-label checkbox">
                                <input type="checkbox" id="IsOverridingPlatform" />Override Defaults?
                            </label>
                            <span class="Error" id="PlatformError">Vendor must be a Direct Vendor or Provider to select a Platform</span>
                        </div>
                        <br />
                        <div>Transmission Details</div>
                        <div class="Bordered" id="VTransmission">
                            <div class="form-inline">
                                <label class="input-label">Target URL</label>
                                <input type="text" id="VTargetUrl" class="dialog-input form-control" />
                            </div>
                            <div class="form-inline">
                                <label class="input-label">Authentication</label>
                                <asp:DropDownList runat="server" ID="VTransmissionAuthenticationT" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="form-inline DigitalCertField">
                                <div>
                                    <a class="To3rdPartyCertPage">Add Request Certificate to the Third Party Certificate Manager</a>
                                </div>
                                <div>
                                    <span>Note: Certificate's associated urls must contain this Transmission Detail's Target URL.</span>
                                </div>
                            </div>
                            <div class="form-inline DigitalCertField">
                                <label class="input-label">Response Cert ID</label>
                                <input type="text" id="VResponseCertificateId" class="dialog-input form-control" />
                            </div>
                            <div class="form-inline BasicAuthField">
                                <label class="input-label">Request Credential</label>
                                <label>Name</label>
                                <input type="text" id="VRequestCredentialName" class="form-control" />
                                <label>Password</label>
                                <input type="password" id="VRequestCredentialPassword" class="form-control" />
                            </div>
                            <div class="form-inline BasicAuthField">
                                <label class="input-label">Response Credential</label>
                                <label>Name</label>
                                <input type="text" id="VResponseCredentialName" class="form-control" />
                                <label>Password</label>
                                <input type="password" id="VResponseCredentialPassword" class="form-control" />
                            </div>
                            <div class="form-inline">
                                <label class="input-label">MCL CRA ID</label>
                                <input type="text" class="form-control MiniInput AlwaysEditable" id="MclCraId"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
				        <span class="clipper">
					        <input type="button" class="btn btn-default CancelBtn" id="VendorCancelBtn" data-dismiss="modal" value="Cancel"/>
				        </span>
				        <span class="clipper">
					        <input type="button" class="btn btn-primary" id="VendorSaveBtn" value="Save" />
				        </span>
			        </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>