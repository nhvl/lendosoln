﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageOCRVendors.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageOCRVendors" EnableViewState="false" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/Common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../Common/Header.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage OCR Vendors</title>
</head>
<body>
    <script type="text/javascript" >
        $j(function () {
            $j('a.edit').click(function () {
                var anchor = $j(this);
                $j('#editVendorId').val(anchor.attr('data-id'));
                $j('#editName').val(anchor.attr('data-name'));
                $j('#editUrl').val(anchor.attr('data-portalUrl'));
                $j('#uploadUrl').val(anchor.attr('data-uploadUrl'));

                var checked = false;

                if (anchor.attr('data-valid') == 'True') {
                    checked = true;
                }

                $j('#editChecbox').prop('checked', checked);
                $j('#enableConnectionTestCheckbox').prop('checked', anchor.attr('data-enableConnectionTest') == 'True');
                $j('#editHeader').text('Edit Vendor ' + anchor.attr('data-name'));
                $j('#CancelEdit').show();
                $j('#DeleteVendor').show();
            }); 

            $j('#CancelEdit').click(function () {
                $j('#editVendorId').val('00000000-0000-0000-0000-000000000000');
                $j('#editName').val('');
                $j('#editUrl').val('');
                $j('#uploadUrl').val('');
                $j('#editChecbox').prop('checked', false);
                $j('#enableConnectionTestCheckbox').prop('checked', false);
                $j('#editHeader').html('Add New Vendor');
                $j(this).hide();
                $j('#DeleteVendor').hide();
            });
        });
    </script>
    <form id="form1" runat="server">
        	<uc:header id="m_Header" runat="server" />
			<uc:headernav id="m_Navigate" runat="server">
				<MenuItem URL="~/LOAdmin/Main.aspx" Label="Main" />
				<MenuItem URL="~/LOAdmin/Manage/ManageOCRVendors.aspx" Label="Manage OCR Vendors" />
			</uc:headernav>
    <div>
        <asp:Repeater runat="server" ID="OCRVendorList" OnItemDataBound="OCRVendorList_OnItemBound">
            <HeaderTemplate>
                <table>
                    <thead>
                        <tr class="GridHeader">
                            <td>&nbsp;</td>
                            <td>Name</td>
                            <td>Portal Url</td>
                            <td>Valid</td>
                            <td>Connection Test</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
            </HeaderTemplate>
            <FooterTemplate>
                    </tr>
                </tbody>
                </table>
            </FooterTemplate>
            <ItemTemplate>
                <tr runat="server" id="row">
                    <td><a runat="server" id="edit" class="edit">edit</a></td>
                    <td style="width:200px;"><ml:EncodedLiteral runat="server" ID="name"></ml:EncodedLiteral></td>
                    <td style="width:500px;"><ml:EncodedLiteral runat="server" ID="portalUrl"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="valid"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="enableConnectionTest"></ml:EncodedLiteral></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </div>
        <div>
            <asp:HiddenField runat="server" ID="editVendorId" />
            <ul style="list-style: none;">
                <li id="editHeader" style="font-weight: bold;">
                    Add New Vendor
                </li>
                <li>
                    <span style="width:200px; display: inline-block;"> Display Name  </span> <asp:TextBox runat="server" ID="editName"></asp:TextBox>
                </li>
                <li>
                    <span style="width:200px; display: inline-block;">Portal Url </span><asp:TextBox runat="server" ID="editUrl"></asp:TextBox>
                </li>
                 <li>
                    <span style="width:200px; display: inline-block;">Upload Url </span><asp:TextBox runat="server" ID="uploadUrl"></asp:TextBox>
                </li>
                <li>
                    Vendor Enabled <asp:CheckBox runat="server" ID="editChecbox" />
                </li>
                <li>
                    Enable Connection Tests <asp:CheckBox runat="server" ID="enableConnectionTestCheckbox" />
                </li>
                <li>    </li>
                <li>
                    <asp:Button runat="server" ID="SaveVendor" Text="Save" OnClick="Save_OnClick" />                                                                                                                                                                                        
                    <input type="button" id="CancelEdit" style="display:none;" value="Cancel Edit" />
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <asp:Button runat="server" ID="DeleteVendor" UseSubmitBehavior="false" style="display:none;" OnClick="DeleteVendor_OnClick" Text="Delete" OnClientClick="if (!confirm('Are save lender credentials for this vendor will be deleted effectively disabling OCR. Are you sure?')) { return false; }" />

                </li>
            </ul>
        </div>
    </form>
</body>
</html>
