﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a means for managing OCR e-doc doc types.
    /// </summary>
    public partial class ManageOcrDocTypes : SecuredAdminPage
    {
        /// <summary>
        /// Gets the style sheet for the page.
        /// </summary>
        /// <value>
        /// The style sheet for the page.
        /// </value>
        public override string StyleSheet => string.Empty;

        /// <summary>
        /// Gets the forced compatibility mode.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.Edge;

        /// <summary>
        /// Gets the required permissions for the page.
        /// </summary>
        /// <returns>
        /// The required permissions for the page.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new[] { E_InternalUserPermissions.EditIntegrationSetup };
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;

            this.RegisterService("ocr", "/LOAdmin/Manage/ManageOcrDocTypesService.aspx");

            this.RegisterJsScript("angular-1.4.8.min.js");
            this.RegisterJsScript("ManageOcrDocTypes.js");
        }
    }
}