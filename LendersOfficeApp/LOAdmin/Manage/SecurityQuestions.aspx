﻿<%@ Page language="C#" Codebehind="SecurityQuestions.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Manage.SecurityQuestions"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Password Security Questions - LendingQB" %>

<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <div id="container">
        <div class="container"><div class="alert alert-info" role="alert">Loading&hellip;</div></div>
    </div>

    <form runat="server"></form>
</asp:Content>
