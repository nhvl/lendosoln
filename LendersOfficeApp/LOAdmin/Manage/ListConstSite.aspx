<%@ Page Language="C#" CodeBehind="ListConstSite.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Manage.ListConstSite"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="List ConstSite" %>

<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style>
        .center-div
        {
            margin: 0 auto;
            /*min-width: 200px;
            max-width: 500px;*/
            width: 80%;
        }
        .breakword li {
            word-wrap: break-word;
        }
    </style>
    <div class="container-fluid"><div class="row"><div class="center-div"><div class="panel panel-default"><div class="panel-body">
    <form id="ListConstSite" method="post" runat="server">
        <ul>
    <li><a href="ListConstSite.aspx?type=constsite">LendersOffice.Constants.ConstSite (Per site web.config)</a>
    <li><a href="ListConstSite.aspx?type=conststage">LendersOffice.Constants.ConstStage (Per stage Stage_Config)</a></li>
    </ul>
    <hr>
        <ml:PassthroughLiteral id=WebConfigList runat="server" EnableViewState="False"></ml:PassthroughLiteral>    
     </form>
	</div></div></div></div></div>
</asp:Content>
