using System;
using System.Web.UI.WebControls;
using LendersOffice.Constants;
using LendersOffice.Email;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOffice.Admin
{
	/// <summary>
	/// Summary description for EmailPanel.
	/// </summary>
	public partial class EmailPanel : LendersOffice.Admin.SecuredAdminPage
	{

		private EmailProcessor m_Emp = new EmailProcessor();

		private String ErrorMessage
		{
			// Write out message.

			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		/// <summary>
		/// Every admin page that inherits from this base page
		/// should implement this query method to specify the
		/// permission set required to access this page.
		/// </summary>
		/// <returns>
		/// List of required permissions.
		/// </returns>

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return an empty set.  Overload to provide
			// a page specific permission list.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsDevelopment
			};
		}

		/// <summary>
		/// Render this page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Show control panel latest.  Each parameter is
			// what we currently have for the processor.

            m_ServerClock.Text = DateTime.Now.ToString();

            m_QWait.Text = ConstApp.MessageQueueTimeout.ToString();
            // Now load up the latest events.

            m_Grid.DefaultSortExpression = "Id";

            string data = ConstMsg.EmailQueue.RequiresArchive.ToString();
            m_IsArchive.Text = char.ToUpper(data[0]) + data.Substring(1);

            m_Grid.DataSource = m_Emp.Entries;
            m_Grid.DataBind();

            int ArchiveSize = 25;

            if (m_TopSize.Text.Length != 0)
            {
                ArchiveSize = Int32.Parse(m_TopSize.Text);
            }

            DBMessageQueue mQ = new DBMessageQueue(ConstMsg.EmailQueue);

            m_ArchiveGrid.DefaultSortExpression = "MessageId DESC";
            m_ArchiveGrid.DataSource = mQ.GetArchive(ArchiveSize);
            m_ArchiveGrid.DataBind(); 
		}

		/// <summary>
		/// Initialize page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.PreRender += new System.EventHandler(this.PagePreRender);

        }
		#endregion

		/// <summary>
		/// Handle Grid event.
		/// </summary>

		protected void GridItemClick( object sender , System.Web.UI.WebControls.DataGridCommandEventArgs a )
		{
			// Fire off event using message id as argument.
            String sArg = a.CommandArgument.ToString();
            DBMessageQueue mQ = new DBMessageQueue(ConstMsg.EmailQueue);
            switch (a.CommandName.ToLower())
            {
                case "drop":
                    {
                        // Remove the specified message from our processor.

                        m_Emp.Drop(sArg);
                    }
                    break;

                case "send":
                    {
                        // Resend the message with the latest to address.

                        TextBox tBox = m_Grid.Items[a.Item.ItemIndex].FindControl("To") as TextBox;

                        if (tBox != null)
                        {
                            m_Emp.Send(sArg, tBox.Text.TrimWhitespaceAndBOM());
                        }
                    }
                    break;
                case "droparchive":
                    {
                        mQ.DeleteFromArchive(Convert.ToInt64(sArg));
                    }
                    break;

                case "copytom":
                    {
                        mQ.MoveArchiveMsgToQueue(Convert.ToInt64(sArg));
                    }
                    break;
            }
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void ClearClick( object sender , System.EventArgs a )
		{
			// Delegate to service interface to clear service.

            m_Emp.Clear();
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void TestClick( object sender , System.EventArgs a )
		{
			// Delegate to service interface to test service.

            if (string.IsNullOrEmpty(m_Address.Text.TrimWhitespaceAndBOM()))
            {
                ErrorMessage = "Please enter a recipient email address in the textbox to queue a test email.";
                return;
            }
            

            CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                Subject = "Test email",
                From = "lo@lendingqb.com",
                Bcc = "",
                To = m_Address.Text,
                Message = "This is a test of the application server's email system sent on " + DateTime.Now + ".\n\nLendingQB"
            };
            m_Emp.Send(cbe);  //! should I change this one?
		}
		protected void ArchiveClick( object sender, System.EventArgs a ) 
		{
            ConstMsg.EmailQueue.ToggleArchiving();
            string data = ConstMsg.EmailQueue.RequiresArchive.ToString();
            m_IsArchive.Text = char.ToUpper(data[0]) + data.Substring(1);

		}

		protected void ClearArchiveClick( object sender, System.EventArgs a ) 
		{
            DBMessageQueue mQ = new DBMessageQueue(ConstMsg.EmailQueue);
            mQ.ClearArchive();
		}

	}

}
