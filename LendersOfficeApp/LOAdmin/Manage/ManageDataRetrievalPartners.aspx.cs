﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Integration.DataRetrievalFramework;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class ManageDataRetrievalPartners : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditIntegrationSetup
            };
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreRender(EventArgs e)
        {
            //m_loadLOdefaultScripts is used to register gService
            m_loadLOdefaultScripts = true;
            base.OnPreRender(e);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            RegisterJsScript("simpleservices.js");//Load for using gService

            RegisterService("main", "/loadmin/manage/ManageDataRetrievalPartnersService.aspx");
            //this.EnableJquery = true;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Partners.DataSource = DataRetrievalPartner.ListPartners();
            Partners.DataBind();
            if (Partners.Rows.Count != 0)
            {
                Partners.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            if (IsPostBack)
            {
                Response.Redirect(Request.Url.ToString(), false);
            }
        }
    }
}
