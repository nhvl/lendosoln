﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Services;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.FeeService;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.ObjLib.TitleProvider;
    using LendersOffice.Security;

    public partial class ManageTitleVendors : SecuredAdminPage
    {
        /// <summary>
        /// Initial platforms on load.
        /// </summary>
        private Lazy<IEnumerable<TitlePlatform>> lazyPlatforms = new Lazy<IEnumerable<TitlePlatform>>(TitlePlatform.LoadPlatforms);

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditIntegrationSetup
            };
        }

        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            
            RegisterJsScript("angular-1.4.8.min.js");

            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");

            this.RegisterJsScript("simpleservices.js");
            this.RegisterService("main", "/loadmin/manage/ManageTitleVendorService.aspx");
            Tools.Bind_TitleTransmissionModelT(this.TransmissionModelT);
            Tools.Bind_TitlePayloadFormatT(this.PayloadFormatT);
            Tools.Bind_TransmissionAuthenticationT(this.PTransmissionAuthenticationT);
            Tools.Bind_TransmissionAuthenticationT(this.VQTransmissionAuthenticationT);
            Tools.Bind_TransmissionAuthenticationT(this.VPTransmissionAuthenticationT);
            this.BindPlatformDDL(this.QuotingPlatformId);
            this.BindPlatformDDL(this.PolicyOrderingPlatformId);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            m_loadLOdefaultScripts = true;
            if (!IsPostBack)
            {
                BindTitleFrameworkTables();
                UpdateList();
            }
            TitleVendorType.Items.Clear();
            TitleVendorType.Items.Add(new ListItem("FirstAmerican", "1"));
        }

        #region Public Methods
        
        protected void DeleteLink_OnCommand(object sender, CommandEventArgs e)
        {
            int vendorId = GetHiddenVendorId();
            if (vendorId > 0)
            {
                TitleProvider.Delete(vendorId);
                ReloadPage();
            }
        }

        protected void EditLink_OnCommand(object sender, CommandEventArgs e)
        {
            int vendorId = GetHiddenVendorId();
            if (vendorId > 0)
            {
                Vendor vendor = GetVendor(vendorId);
                if (vendor != null)
                {
                    TitleProvider.Update(vendor);
                    ReloadPage();
                }
                
            }
        }

        protected void AddLink_OnCommand(object sender, CommandEventArgs e)
        {
                Vendor vendor = GetVendor(-1);
                if (vendor != null)
                {
                    TitleProvider.Update(vendor);
                    ReloadPage();
                }
        }

        [WebMethod]
        public static object CheckCanDeleteVendor(int vendorId)
        {
            return Tools.LogAndThrowIfErrors(() => CheckCanDeleteVendorImpl(vendorId));
        }

        private static object CheckCanDeleteVendorImpl(int vendorId)
        {
            // Make sure vendor is not part of any vendors fee service.
            ArrayList badBrokers = new ArrayList();
            foreach (Guid brokerId in Tools.GetAllActiveBrokers())
            {
                // Only need to check brokers with enhanced title quotes enabled.
                BrokerDB broker = BrokerDB.RetrieveById(brokerId);
                if (!broker.EnableEnhancedTitleQuotes)
                {
                    continue;
                }

                // Check Fee Service.
                FeeServiceRevision currentFeeRevision = FeeServiceApplication.GetCurrentRevision(brokerId, false);
                if (currentFeeRevision == null)
                {
                    continue;
                }

                bool foundMatch = false;
                foreach (FeeServiceTemplate template in currentFeeRevision.FeeTemplates)
                {
                    foreach (FeeServiceTemplate.TemplateField field in template.GetTemplateFields())
                    {
                        if (field.RuleType == FeeServiceRuleType.TitleSource && int.Parse(field.Value) == vendorId)
                        {
                            badBrokers.Add(new { BrokerId = brokerId, CustomerCode = broker.CustomerCode });
                            foundMatch = true;
                            break;
                        }
                    }

                    // Don't need to keep checking this broker's config once we found a match.
                    if (foundMatch)
                    {
                        break;
                    }
                }
            }

            return new {
                CanDeleteVendor = badBrokers.Count == 0,
                BadBrokers = badBrokers
            };
        }

        #endregion

        /// <summary>
        /// Binds the checkboxes for the vendor list.
        /// </summary>
        /// <param name="sender">The caller of this method.</param>
        /// <param name="e">The event arguments.</param>
        protected void Vendors_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                LightTitleVendor light = e.Item.DataItem as LightTitleVendor;
                HtmlInputCheckBox isEnabled = e.Item.FindControl("IsEnabled") as HtmlInputCheckBox;
                if (isEnabled != null)
                {
                    isEnabled.Checked = light.IsEnabled;
                }

                HtmlInputCheckBox isTestVendor = e.Item.FindControl("IsTestVendor") as HtmlInputCheckBox;
                if (isTestVendor != null)
                {
                    isTestVendor.Checked = light.IsTestVendor;
                }
            }
        }

        /// <summary>
        /// Populates the platform DDL with an initial list of platforms.
        /// </summary>
        private void BindPlatformDDL(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem(string.Empty, "0"));
            foreach (var platform in this.lazyPlatforms.Value)
            {
                ddl.Items.Add(new ListItem(platform.PlatformName, platform.PlatformId.ToString()));
            }
        }

        #region Private methods
        private void BindTitleFrameworkTables()
        {
            this.Platforms.DataSource = this.lazyPlatforms.Value;
            this.Platforms.DataBind();

            this.Vendors.DataSource = TitleVendor.LoadTitleVendors_Light();
            this.Vendors.DataBind();
        }

        /// <summary>
        /// Pull the vendor list from database and save to vendorDataKey hidden field
        /// </summary>
        private void UpdateList()
        {
            List<Vendor> vendorList = TitleProvider.GetProviders().OrderBy(x => x.Id).ToList();
            string returnData = ObsoleteSerializationHelper.JavascriptJsonSerialize(vendorList);
            if (string.IsNullOrEmpty(returnData))
            {
                returnData = "{}";
            }
            vendorDataKey.Value = returnData;
        }

        /// <summary>
        /// Get Vendor info from an id.
        /// </summary>
        /// <param name="vendorId">ID number of a vendor.</param>
        /// <returns>The Vendor object.</returns>
        private Vendor GetVendor(int vendorId)
        {
            if (string.IsNullOrEmpty(ExportPath.Text) || string.IsNullOrEmpty(VendorName.Text))
            {
                return null;
            }
            Vendor vendor = (vendorId > 0 ? TitleProvider.GetProvider(vendorId) : null) ?? new Vendor();
            vendor.Name = VendorName.Text;
            vendor.RequiresAccountId = RequireAccountId.Checked;
            vendor.ExportPath = ExportPath.Text;
            vendor.LQBServicePassword = LQBServicePassword.Text;
            vendor.LQBServiceSourceName = LQBServiceSourceName.Text;
            vendor.LQBServiceUserName = LQBServiceUserName.Text;
            vendor.TitleVendorIntegrationType = (E_TitleVendorIntegrationType)int.Parse(TitleVendorType.SelectedValue);
            vendor.ExportPathTitle = ExportTitle.Text;
            vendor.EnableConnectionTest = EnableConnectionTest.Checked;
            return vendor;
        }

        /// <summary>
        /// Get selected vendor in front-end.
        /// The selected vendor id will be included in hidden field in postback from.
        /// </summary>
        /// <returns></returns>
        private int GetHiddenVendorId()
        {
            int id = -1;

            if (string.IsNullOrEmpty(hiddenKey.Value) || int.TryParse(hiddenKey.Value, out id))
            {
                hiddenKey.Value = "";
                return id;
            }
            return id;
        }

        private void ReloadPage()
        {
            Response.Redirect("ManageTitleVendors.aspx");

        }

        #endregion
    }
}
