<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DefaultPermissions.ascx.cs" Inherits="LendersOfficeApp.LOAdmin.Broker.DefaultPermissions" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript" > 
	var Modal = {
		Hide : function(  ) 
		{
			document.getElementById("DefaultRolePermissions").style.display="none"; 
			document.getElementById("mask").style.display="none"; 
	
			
		}, 
		
		Show : function( ) 
		{
			document.getElementById( "DefaultRolePermissions" ).style.display="block"; 
			document.getElementById( "mask" ).style.display="block";
			window.onscroll = this.ScrollFunction;  
		}, 
		 
		
		ScrollFunction : function () 
		{
			document.getElementById("mask").style.top = document.body.scrollTop; 
			document.getElementById("DefaultRolePermissions").style.top = document.body.scrollTop  + (document.body.clientHeight/2) ;   
		//	alert( document.body.scrollTop );
		}
	}; 
	
	var DefaultPermissions = {
		Update :  function( ) 
		{
			var c = confirm("Are you sure you want to change the default permission for every broker?"); 
			if ( !c ) return; 
			var roleList =  <%= AspxTools.JsGetElementById(m_Roles) %>; 
			var permission = <%= AspxTools.JsGetElementById(m_Permissions) %>; 
			var on = document.getElementById('<%=AspxTools.JsStringUnquoted(ClientID)%>_On');
					
			var arg = { 
				role : roleList.options[roleList.selectedIndex].value,  
				permission : permission.options[permission.selectedIndex].value,
				status : on.checked
			}; 
			
			var results = gService.loadmin_utilities.call("UpdatePermissions", arg); 
			if ( results.value.error ) 
			{
				alert( "Error: "+  results.value["error"] ) ;
			 }
			 else 
			 {
				alert( "Update completed.") ;
			 }
		}, 
		
		EnforceStatusSelection : function( val ) 
		{
			
			if ( val == 0 ) 
			{
				 document.getElementById( '<%=AspxTools.JsStringUnquoted(ClientID)%>_On' ).checked = false; 
			}
			else 
			{
				 document.getElementById( '<%=AspxTools.JsStringUnquoted(ClientID)%>_Off' ).checked = false; 		
			}
		}
	};
</script>



<div class="modalCotainer" id="DefaultRolePermissions" style="width: 400px">  
	<div class="modal" style="width: 400px; height: 100px">
		<div class="modalTop" style="width: 398px"> 
			<span> Default Role Permission Updater </span>
			<a href="javascript:Modal.Hide();" > [X] </a>
		</div>
		<div class="modalBody" >
			<div >
				<label style="width:80px; " for="<%= AspxTools.ClientId(m_Roles) %>" > Roles : </label> 
				<asp:DropDownList Runat="server" ID="m_Roles" > </asp:DropDownList> 
			</div>
			<div> 
				<label style="width:80px;"  for="<%= AspxTools.ClientId(m_Permissions) %>" > Permission : </label> 
				<asp:DropDownList Runat="server" ID="m_Permissions" > </asp:DropDownList>
			</div>
			
			<div>
				<span style="width:80px; " > Default Value : </span>	
				<label for="<%=AspxTools.HtmlString(ClientID)%>_On" > On </label> 
				<input type="radio" name="<%=AspxTools.HtmlString(ClientID)%>_On" onclick="DefaultPermissions.EnforceStatusSelection(1)" Value="1" /> 
				
				<label for="<%=AspxTools.HtmlString(ClientID)%>_Off" > Off </label> 
				<input type="radio" checked="true"  name="<%=AspxTools.HtmlString(ClientID)%>_Off" onclick="DefaultPermissions.EnforceStatusSelection(0)" Value="0" />
			</div>			
			<div style="text-align:Center">
				<input type="button" onclick="javascript:DefaultPermissions.Update()"  value="Update"/>		
			</div>
		</div>
	</div>
</div>