﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ManageDataRetrievalPartners.aspx.cs"
    Inherits="LendersOfficeApp.LOAdmin.Manage.ManageDataRetrievalPartners" MasterPageFile="~/LOAdmin/loadmin.Master" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style type="text/css">
        /*Show loading animate*/
        .glyphicon-refresh-animate{
            -animation: spin .7s infinite linear;
            -ms-animation: spin .7s infinite linear;
            -webkit-animation: spinw .7s infinite linear;
            -moz-animation: spinm .7s infinite linear;
        }
        @keyframes spin{from{transform:scale(1)rotate(0deg);}to{transform: scale(1) rotate(360deg);}}
        @-webkit-keyframes spinw{from{-webkit-transform:rotate(0deg);}to{-webkit-transform: rotate(360deg);}
        }@-moz-keyframes spinm{from{-moz-transform:rotate(0deg);}to{-moz-transform: rotate(360deg);}}
        /******/
        .btn:focus, .btn:active
        {
            outline: none !important;
        }
        .center-div
        {
            margin: 0 auto;
            min-width: 200px;
            max-width: 500px;
            width: 30%;
        }
    </style>

    <script type="text/javascript">
    function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
    }
    function f_clearDialogInput() {
        $('#PartnerId').val('');
        $('#Name').val('');
        $('#ExportPath').val('');
    }

    function f_launchDialog(sTitle) {
        $('#myModal .modal-title').text(sTitle);
        $('#loadingModal').hide();
        $('#myModal').modal('show');
    }

    function f_newPartner() {
        f_clearDialogInput();
        f_launchDialog('Add New Partner');
    }
     function setTooltip(selector,title)
        {
            selector.prop("title",title);
            selector.focus();
            selector.tooltip({ placement: 'bottom'});
            selector.tooltip('show');
            selector.one("blur change", function () { selector.tooltip("destroy"); });
            //selector.one("blur change", function(){ selector.tooltip("destroy");$("div[role='tooltip']").tooltip('destroy');});
        }
    function f_savePartner(e) {
        e.preventDefault();
        var args = {
            PartnerId:$('#PartnerId').val(),
            Name:$('#Name').val().trim(),
            ExportPath:$('#ExportPath').val().trim()
        };
         if (!args.Name) {
                    $('#Name').val('');
                    setTooltip($('#Name'),"Please fill out the Partner Name");
                    return false;
                }
        if (!args.ExportPath) {
                    $('#ExportPath').val('');
                    setTooltip($('#ExportPath'),"Please fill out the Export Path");
                    return false;
                }
        
        if(!args.ExportPath.toLowerCase().match('^xslt_export:')&&!isUrlValid(args.ExportPath)){
            setTooltip($('#ExportPath'),"Export Path must be a valid URL.");
            return false;
        }
        $('#loadingModal').show();
        //setTimeout(function(){
            var result = gService.main.call("Update", args, true, false, false, true, successCallback, errorCallback);
        //},1000);
    }

    function f_edit(partnerId) {
        var args = {
            PartnerId:partnerId
        }
        $('#PartnerId').val(partnerId);
        var result = gService.main.call("Retrieve", args, true, false, false, true, loadSuccessCallback, errorCallback);
        return false;
    }

    function f_delete(partnerId) {
     var args = {
            PartnerId:partnerId
        }
        confirmDialog('Do you really want to delete this partner?',args);
        return false;
    }
     function confirmDialog(message,data){
            $('#myConfirm .modal-body').html(message);
            $('#myConfirm #btnOK').click(function(){
                var result = gService.main.call("Delete", data, true, false, false, true, successCallback, errorCallback);
                return false;
            });
            $('#myConfirm').modal('show');
     }
     function successCallback(result) {
         if (!result.error) {
             $('#myModal').modal('hide');
             location.reload();
         }
         else {
             alert(result.UserMessage);
         }
     }
     function loadSuccessCallback(result) {
         if (result.error) {
             alert(result.UserMessage);
             return false;
         }
         $('#Name').val(result.value['Name']);
         $('#ExportPath').val(result.value['ExportPath']);
         f_launchDialog('Edit Partner');
     }
     function errorCallback(result) {
         alert(result.UserMessage);
     }
    </script>

    <div class="container-fluid">
        <div class="row">
            <div class="center-div">
                <div class="panel panel-default">
                    <div class="panel-body div-wrap-tb">
                        <form id="form" runat="server">
                        <div>
                            <span class="clipper"><button type="button" class="btn btn-primary" onclick="f_newPartner();">
                                                    Add New</button></span>
                            <asp:GridView runat="server" ID="Partners" CssClass="table table-striped table-condensed table-layout-fix"
                                CellSpacing="0" CellPadding="0" BorderWidth="0" GridLines="None" UseAccessibleHeader="true"
                                AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:void(0)" onclick="return f_edit('<%# AspxTools.HtmlString(Eval("PartnerId").ToString()) %>');">
                                                    edit</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:void(0)" onclick="return f_delete('<%# AspxTools.HtmlString(Eval("PartnerId").ToString()) %>');">
                                                    delete</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField></asp:TemplateField>
                                    <asp:BoundField HeaderText="Partner Name" DataField="Name" HeaderStyle-Width="50%" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        </form>
                        <!-- Modal -->
                        <div id="myModal" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <form action="javascript:void(0);" onsubmit="return f_savePartner(event);">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">
                                            &times;</button>
                                        <h4 class="modal-title">
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" id="PartnerId" name="PartnerId" />
                                        <div class="form-group">
                                            <label>
                                                Partner Name:<span class="text-danger">*</span></label>
                                            <input type="text" id="Name" class="form-control" required title="Please fill out the Partner Name"
                                                autocomplete="off" />
                                        </div>
                                        <div class="form-group">
                                            NOTE: Adding new url REQUIRES making a request to IT to allow our production servers
                                            to connect to it.
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Export Path:<span class="text-danger">*</span></label>
                                            <input type="text" id="ExportPath" class="form-control" required title="Please fill out the Export Path"
                                                autocomplete="off" />
                                        </div>
                                        <div id="loadingModal">
                                            <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>Loading...</div>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel</button></span> <span class="clipper">
                                                    <button type="submit" class="btn btn-primary">
                                                        Save</button></span>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <!--Confirm Dialog-->
                        <div id="myConfirm" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog modal-width">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">
                                            Confirm Delete Partner
                                        </h4>
                                    </div>
                                    <div class="modal-body  text-center">
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel</button></span> <span class="clipper">
                                                    <button type="button" class="btn btn-primary" id="btnOK">
                                                        Delete</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
