﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web.Services;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Edocs;
    using LendersOffice.Security;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Add/delete a DocuTech/DocMagic/IDS DocType record.
    /// </summary>
    public partial class ManageDocBarcode : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Manage Docutech Type.
        /// </summary>
        private const string DocuTech = "DocuTech";

        /// <summary>
        /// Manage IDS Type.
        /// </summary>
        private const string IDS = "IDS";

        /// <summary>
        /// Manage DocMagic Type.
        /// </summary>
        private const string DocMagic = "DocMagic";

        /// <summary>
        /// Permission used for this page.
        /// </summary>
        private static readonly E_InternalUserPermissions[] RequiredPermissions = { E_InternalUserPermissions.EditIntegrationSetup };

        /// <summary>
        /// Barcode record manager.
        /// </summary>
        private IDocVendorDocTypeRecordManager recordManager;

        /// <summary>
        /// Gets the style sheet.
        /// </summary>
        /// <value>The style sheet.</value>
        public override string StyleSheet
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets/set barcode format.
        /// </summary>
        protected string DocType { get; private set; }

        /// <summary>
        /// Gets/sets column index that used for sort.
        /// </summary>
        protected int SortType { get; private set; } = 0;

        /// <summary>
        /// Sets error message that will show by js alert.
        /// </summary>
        private string ErrorMessage
        {
            set { ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + "."); }
        }

        /// <summary>
        /// Get description and barcode from barcodeFmt and recordId.
        /// </summary>
        /// <param name="barcodeFmt">The valid data are DocuTech, DocMagic and IDS.</param>
        /// <param name="recordId">The id of record to look up.</param>
        /// <returns>Return description and barcode.</returns>
        [WebMethod]
        public static object GetBarcodeInfo(string barcodeFmt, int recordId)
        {
            IDocVendorDocTypeRecordManager recordManager = GetBarcodeRecordManager(barcodeFmt);
            if (recordManager == null)
            {
                throw new ArgumentException($"Unknow barcode format '{barcodeFmt}'");
            }

            var record = recordManager.Get(recordId);
            return new { InternalDescription = record.InternalDescription, BarcodeClassification = record.BarcodeClassification };
        }

        /// <summary>
        /// Return this page's required permissions set. This test occurs with each page initialization.
        /// </summary>
        /// <returns>
        /// Array of required permissions.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return RequiredPermissions;
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions))
            {
                this.ErrorMessage = "Error: No permission.";
                return;
            }

            this.DocType = RequestHelper.GetSafeQueryString("docType");
            if (string.IsNullOrEmpty(this.DocType))
            {
                this.DocType = DocuTech;
            }
            
            this.recordManager = GetBarcodeRecordManager(this.DocType);
            if (this.recordManager == null)
            {
                this.ErrorMessage = "Invalid document type. The expected doc type is DocuTech or IDS or DocMagic.";
                return;
            }

            ClientScript.GetPostBackEventReference(this, string.Empty);
            if (this.IsPostBack)
            {
                if (this.hiddenCmd.Value == "sortByColumn")
                {
                    int colIdx = 0;
                    if (!int.TryParse(this.hiddenId.Value, out colIdx))
                    {
                        colIdx = 0;
                    }

                    this.SortType = colIdx;
                }
                else if (this.hiddenCmd.Value == "addDocType")
                {
                    this.AddNewBarcode(this.hiddenDesc.Value, this.hiddenBarcode.Value);
                }
                else if (this.hiddenCmd.Value == "deleteDocType")
                {
                    int recordId = -1;
                    if (!int.TryParse(this.hiddenId.Value, out recordId))
                    {
                        //// expect never happend
                        this.ErrorMessage = "Invalid document type ID '{this.hiddenId.Value}'";
                        return;
                    }

                    this.DeleteBarcode(recordId);
                }
                else if (this.hiddenCmd.Value == "updateDocType")
                {
                    int recordId = -1;
                    if (!int.TryParse(this.hiddenId.Value, out recordId))
                    {
                        //// expect never happend
                        this.ErrorMessage = "Invalid document type ID '{this.hiddenId.Value}'";
                        return;
                    }

                    this.UpdateBarcode(recordId, this.hiddenDesc.Value, this.hiddenBarcode.Value);
                }
            }

            var items = this.recordManager.ListRecords();
            this.DocTypes.DataSource = items.OrderBy(i => this.SortType == 0 ? i.InternalDescription : i.BarcodeClassification);
            this.DocTypes.DataBind();

            this.hiddenCmd.Value = string.Empty;
            this.hiddenBarcode.Value = string.Empty;
            this.hiddenDesc.Value = string.Empty;
            this.hiddenId.Value = string.Empty;
        }

        /// <summary>
        /// Get IBarcodeRecordManage from barcode format.
        /// </summary>
        /// <param name="barcodeFmtStr">Its values are DocuTech, DocMagic and IDS.</param>
        /// <returns>IDocVendorDocTypeRecordManager object if barcodeFmt is valid. Otherwise return null.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("LendingQBStyleCop.LendingQBCustomRules", "LB1001:DefaultSwitchCaseMustThrowException", Justification = "Allow to return null.")]
        private static IDocVendorDocTypeRecordManager GetBarcodeRecordManager(string barcodeFmtStr)
        {
            E_DocBarcodeFormatT barcodeFmt = E_DocBarcodeFormatT.IDS;
            switch (barcodeFmtStr)
            {
            case DocuTech:
                barcodeFmt = E_DocBarcodeFormatT.DocuTech;
                break;

            case DocMagic:
                barcodeFmt = E_DocBarcodeFormatT.DocMagic;
                break;

            case IDS:
                barcodeFmt = E_DocBarcodeFormatT.IDS;
                break;

            default:
                return null;
            }

            return DocVendorDocTypeRecordManagerFactory.Create(barcodeFmt);
        }

        /// <summary>
        /// Add new barcode record.
        /// </summary>
        /// <param name="desc">Barcode's decription. </param>
        /// <param name="barcode">Barcode Classification.</param>
        private void AddNewBarcode(string desc, string barcode)
        {
            string errMsg = $"Cannot add the {this.DocType} {barcode}. ";

            try
            {
                this.recordManager.Create(desc, barcode);
            }
            catch (SqlException exc)
            {
                this.ErrorMessage = errMsg + exc.Message;
                Tools.LogError(errMsg, exc);
            }
            catch (LqbException exc)
            {
                this.ErrorMessage = errMsg + exc.Message;
                Tools.LogError(errMsg, exc);
            }
            catch (Exception exc)
            {
                Tools.LogError(errMsg, exc);
                throw;
            }
        }

        /// <summary>
        /// Update barcode record.
        /// </summary>
        /// <param name="recordId">The id of record to update.</param>
        /// <param name="desc">The barcode description.</param>
        /// <param name="barcode">The barcode classification.</param>
        private void UpdateBarcode(int recordId, string desc, string barcode)
        {
            string errMsg = $"Cannot update bardcode with id = {recordId}. ";

            try
            {
                this.recordManager.Update(recordId, desc, barcode);
            }
            catch (SqlException exc)
            {
                this.ErrorMessage = errMsg + exc.Message;
                Tools.LogError(errMsg, exc);
            }
            catch (LqbException exc)
            {
                this.ErrorMessage = errMsg + exc.Message;
                Tools.LogError(errMsg, exc);
            }
            catch (Exception exc)
            {
                Tools.LogError(errMsg, exc);
                throw;
            }
        }

        /// <summary>
        /// Delete a barcode record with its Id = recordId.
        /// </summary>
        /// <param name="recordId">The record id to delete.</param>
        private void DeleteBarcode(int recordId)
        {
            string errMsg = $"Cannot delete {this.DocType} with id = {recordId}. ";
            try
            {
                this.recordManager.Delete(recordId);
            }
            catch (SqlException exc)
            {
                this.ErrorMessage = errMsg + exc.Message;
                Tools.LogError(errMsg, exc);
            }
            catch (LqbException exc)
            {
                this.ErrorMessage = errMsg + exc.Message;
                Tools.LogError(errMsg, exc);
            }
            catch (Exception exc)
            {
                Tools.LogError(errMsg, exc);
                throw;
            }
        }
    }
}