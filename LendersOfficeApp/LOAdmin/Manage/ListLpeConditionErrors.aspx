<%@ Page Language="c#" CodeBehind="ListLpeConditionErrors.aspx.cs" AutoEventWireup="false"
    Inherits="LendersOfficeApp.LOAdmin.Manage.ListLpeConditionErrors" MasterPageFile="~/LOAdmin/loadmin.Master"
    Title="ListLpeConditionErrors" %>

<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style type="text/css">
        .center-div
        {
            margin: 0 auto;
            min-width: 989px;
        }
    </style>
    <div class="container-fluid">
        <div class="row-masonry">
            <div class="grid-sizer">
            </div>
            <div class="masonry-item center-div">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="DynamicCall" onsubmit="document.getElementsByTagName('textarea')[0].value = 'Please wait one moment...';"
                        method="post" runat="server" class="form-horizontal">
                        <p align="left">
                            <a href="#" class="clipper">
                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn btn-default"
                                    OnClick="btnClear_Click"></asp:Button></a> <a href="#" class="clipper">
                                        <asp:Button ID="btnRemoveEmptyAdjustTargets" runat="server" Text="Remove Empty AdjustTargets"
                                            CssClass="btn btn-default" OnClick="btnRemoveEmptyAdjustTargets_Click"></asp:Button></a>
                            <a href="#" class="clipper">
                                <asp:Button ID="bttnVerifySrcPtr" runat="server" Text="Verify SrcRateOptionsProgId"
                                    CssClass="btn btn-default" OnClick="bttnVerifySrcPtr_Click"></asp:Button></a>
                            <a href="#" class="clipper">
                                <asp:Button ID="btnRun" runat="server" Text="Validate LPE Conditions" CssClass="btn btn-default"
                                    OnClick="btnRun_Click"></asp:Button></a> <a href="#" class="clipper">
                                        <asp:Button ID="btnPolicyValidate" runat="server" Text="Validate LPE Policies" CssClass="btn btn-default"
                                            OnClick="btnPolicyValidate_Click"></asp:Button></a>
                        </p>
                        <asp:TextBox class="form-control" ID="m_txtResult" runat="server" Height="446px"
                            TextMode="MultiLine" EnableViewState="False" Wrap="True"></asp:TextBox>
                        <% SpecialProcess(); %>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
