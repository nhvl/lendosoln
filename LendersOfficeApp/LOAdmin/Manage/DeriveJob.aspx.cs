using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using System.Xml;
using DataAccess;
using LendersOfficeApp.los.Template;
using LendersOffice.Constants;
using LendersOffice.RatePrice;
using System.Web.Services;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;

namespace LendersOfficeApp.LOAdmin.Manage
{
	// 4/23/07 mf OPM 11856.  List the derivation job queue contents here
	// Users can also cancel pending jobs or re-queue failed ones.
	// Admins can also start/stop the processor here.
	public partial class DeriveJob : LendersOffice.Admin.SecuredAdminPage
	{
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        void RegisterResources()
        {
            RegisterJsScript("LOAdmin.Manage.DeriveJob.js");
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
            RegisterResources();
		}
        [WebMethod]
        public static IEnumerable<DJConfig> Get()
        {
            var rs = new List<DJConfig>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "ListDeriveJobs"))
            {
                while (reader.Read())
                {
                    rs.Add(new DJConfig(reader));
                }
            }
            return rs;
        }
        [WebMethod]
        public static void JobCommand(int jobId, string command)
        {
            switch(command){
                case "cancel":
                    DerivationJob.DeleteJob(jobId, false);
                    break;
                case "enqueue":
                    DerivationJob.EnqueueJob(jobId);
                    break;
            }
        }
        public class DJConfig
        {
            public int? JobID{get;set;}
	        public string JobXmlContent{get;set;}
	        public string JobStatus{get;set;}
	        public string JobPriority{get;set;}
	        public string JobSubmitD{get;set;}
	        public string JobStartD{get;set;}
	        public string JobNotifyEmailAddr{get;set;}
	        public string JobUserLabel{get;set;}
	        public string JobUserNotes{get;set;}
            public string UserLoginNm { get; set;}

            public string ErrorText { get; set; }
            public Boolean IsShowError { get; set; }

            public DJConfig(DbDataReader reader)
            {
                if (reader["JobID"] == DBNull.Value)
                    JobID = null;
                else
                    JobID = int.Parse(reader["JobID"].ToString());
                JobXmlContent = reader["JobXmlContent"].ToString();
                JobStatus = reader["JobStatus"].ToString();
                JobPriority = reader["JobPriority"].ToString();

                JobSubmitD = reader["JobSubmitD"].ToString();
                JobStartD = reader["JobStartD"].ToString();

                JobNotifyEmailAddr = reader["JobNotifyEmailAddr"].ToString();
                JobUserLabel = reader["JobUserLabel"].ToString();
                JobUserNotes = reader["JobUserNotes"].ToString();
                UserLoginNm = reader["UserLoginNm"].ToString();

                if (JobStatus == "Error")
                {
                    ErrorText = GetErrorText(JobXmlContent);
                }
                IsShowError = false;
            }

            private string GetErrorText(string xmlContent)
            {
                // We just want to pull out the errors for listing
                System.Text.StringBuilder errors = new System.Text.StringBuilder();
                XmlDocument doc = Tools.CreateXmlDoc(xmlContent);

                XmlNodeList errorList = doc.SelectNodes("//DeriveJob/ErrorList/Error");

                foreach (XmlElement errorNode in errorList)
                {
                    errors.AppendFormat("{0}:{2}{1}" + (errors.Length > 0 ? "" : "{2}"), errorNode.GetAttribute("Time"), errorNode.GetAttribute("Message"), Environment.NewLine);
                }

                return errors.ToString();
            }
        }
		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsDevelopment
			};
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
