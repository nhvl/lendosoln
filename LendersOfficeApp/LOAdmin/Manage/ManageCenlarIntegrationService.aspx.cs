﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Threading;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.Cenlar;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// The background service for the Manage Cenlar Integration page.
    /// </summary>
    public partial class ManageCenlarIntegrationService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Determines which process should be called.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.SendCenlarTransmission):
                    this.SendCenlarTransmission();
                    break;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Method not implemented {methodName}"));
            }
        }
        
        /// <summary>
        /// Sends a manual transmission to Cenlar.
        /// </summary>
        private void SendCenlarTransmission()
        {
            var brokerId = this.GetGuid("BrokerId");
            var environment = this.GetEnum<CenlarEnvironmentT>("EnvironmentT");
            var cachedPrincipal = PrincipalFactory.CurrentPrincipal;
            InitialPrincipalTypeT initialPrincipalType = cachedPrincipal.InitialPrincipalType;
            Guid initialuserId = cachedPrincipal.InitialUserId;

            if (cachedPrincipal is InternalUserPrincipal)
            {
                initialPrincipalType = InitialPrincipalTypeT.Internal;
                initialuserId = cachedPrincipal.UserId;
            }

            try
            {
                var requestData = new CenlarRequestData(brokerId, CenlarActionT.SendLoanFiles, environment, automatedTransmission: false, initialPrincipalType: initialPrincipalType, initialUserId: initialuserId);
                var requestHandler = new CenlarRequestHandler(requestData);
                requestHandler.SubmitRequest();
            }
            catch (DeveloperException exc)
            {
                this.SetResult("Success", false);
                this.SetResult("Message", exc.Context.Serialize());
                Tools.LogWarning(exc);
                return;
            }
            catch (CBaseException exc)
            {
                this.SetResult("Success", false);
                this.SetResult("Message", exc.DeveloperMessage);
                Tools.LogWarning(exc);
                return;
            }
            catch (ArgumentException exc)
            {
                this.SetResult("Success", false);
                this.SetResult("Message", exc.Message);
                Tools.LogWarning(exc);
                return;
            }
            finally
            {
                this.ResetPrincipal(cachedPrincipal);
            }

            this.SetResult("Success", true);
        }

        /// <summary>
        /// Resets the principal to a cached principal in the thread and cookie.
        /// </summary>
        /// <param name="cachedPrincipal">The cached principal.</param>
        /// <remarks>
        /// This must be run after a Cenlar call from LOAdmin as the batch
        /// export internals will override the current LOAdmin user.
        /// </remarks>
        private void ResetPrincipal(AbstractUserPrincipal cachedPrincipal)
        {
            if (HttpContext.Current?.Response != null)
            {
                RequestHelper.StoreToCookie(cachedPrincipal, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
            }

            Thread.CurrentPrincipal = cachedPrincipal;
        }
    }
}