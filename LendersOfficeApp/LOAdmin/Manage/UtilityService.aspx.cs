using System;
using System.Collections.Generic;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin 
{
	
	public partial class UtilityService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
		protected override void Process(string methodName) 
		{
			switch ( methodName ) 
			{
				case "UpdatePermissions" : 
					UpdatePermissions(); 
					break;
				case "ClearPmlLogo" : 
					ClearPmlLogo();
					break;
			}
		}

		private void ClearPmlLogo() 
		{
			InternalUserPrincipal principal = System.Threading.Thread.CurrentPrincipal as InternalUserPrincipal;
			if ( principal == null ) 
			{
				SetResult("error", "Insufficient permissions to do this operation.");
				return; 
			}

			InternalUserPermissions p = new InternalUserPermissions(principal.Permissions);
			if ( !p.Can(E_InternalUserPermissions.IsSAE  ) ) 
			{
				SetResult("error", "Insufficient permissions to do this operation."); 
				return; 
			}
			
			Guid pmlSiteId = GetGuid("pmlsiteid", Guid.Empty);

            FileDBTools.Delete(E_FileDB.Normal, pmlSiteId.ToString() + ".logo.gif");
			this.SetResult("okay","okay");
		}

		private void UpdatePermissions() 
		{
			Guid role = GetGuid("role", Guid.Empty);
			string permission = GetString( "permission", string.Empty); 
			string status = GetString("status", string.Empty ); 
			
			if ( role == Guid.Empty || permission == string.Empty || status == string.Empty ) 
			{
				SetResult("error", "Incomplete arguments");
				return; 
			}

			bool IsOn = GetBool("status"); 
			
			Permission perm; 
			
			try 
			{
				perm = (Permission) Enum.Parse( typeof( Permission ), permission, true ) ; 
			}
			catch 
			{
				SetResult("error", "Could not deciper permission : " + permission );
				return; 
			}

            UtilityService.UpdatePermission(role, IsOn, perm);
			
		}

		public static void UpdatePermission(Guid roleId , bool isOn, params Permission[] permissions ) 
		{
			List<Guid> list = new List<Guid>();
            foreach (BrokerDbSearchItem item in BrokerDbSearch.SearchAll())
            {
                list.Add(item.BrokerId);
            }
			
			Tools.LogInfo( "Begining to set default permissions for role id : " + roleId.ToString() );

            foreach (Guid brokerId in list)
            {
                using (CStoredProcedureExec spEx = new CStoredProcedureExec(DbConnectionInfo.GetConnectionInfo(brokerId)))
                {
                    try
                    {
                        spEx.BeginTransactionForWrite();

                        BrokerUserPermissions indPerm = new BrokerUserPermissions();
                        indPerm.RetrieveRolePermissions(brokerId, roleId);
                        foreach (Permission perm in permissions)
                        {
                            indPerm.SetPermission(perm, isOn);
                        }
                        indPerm.UpdateRolePermissions(spEx, brokerId, roleId);

                        spEx.CommitTransaction();
                    }
                    catch (Exception)
                    {
                        spEx.RollbackTransaction();
                        throw;
                    }
                }
            }

			Tools.LogInfo( "Default Role permissions have been set for role id : " + roleId.ToString() + " to " + (isOn ? "on." : "off.") );
		}
	}
}