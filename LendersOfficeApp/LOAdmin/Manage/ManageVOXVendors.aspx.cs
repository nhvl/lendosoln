﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.FannieMae;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Security;

    /// <summary>
    /// LOAdmin manage VOX vendors class.
    /// </summary>
    public partial class ManageVOXVendors : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Initial platforms on load.
        /// </summary>
        private Lazy<IEnumerable<VOXPlatform>> lazyPlatforms = new Lazy<IEnumerable<VOXPlatform>>(VOXPlatform.LoadPlatforms);

        /// <summary>
        /// Gets the style sheet.
        /// </summary>
        /// <value>The style sheet.</value>
        public override string StyleSheet
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Required permission to access this page.
        /// </summary>
        /// <returns>The required permissions to access this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditIntegrationSetup
            };
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Platforms.DataSource = this.lazyPlatforms.Value;
            this.Platforms.DataBind();

            this.Vendors.DataSource = VOXVendor.LoadLightVendors();
            this.Vendors.DataBind();
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            // for stop loading `common.js` from `BasePage`
            // this.m_loadLOdefaultScripts = false; 
            
            // Load for using gService
            this.RegisterJsScript("simpleservices.js");
            this.RegisterService("main", "/loadmin/manage/ManageVOXVendorsService.aspx");
            Tools.Bind_VOXTransmissionModelT(this.TransmissionModelT);
            Tools.Bind_VOXPayloadFormatT(this.PayloadFormatT);
            Tools.Bind_TransmissionAuthenticationT(this.PTransmissionAuthenticationT);
            Tools.Bind_TransmissionAuthenticationT(this.VTransmissionAuthenticationT);
            this.BindDuValidationServiceProviders();
            this.BindPlatformDDL();
        }

        /// <summary>
        /// Pre render function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnPreRender(EventArgs e)
        {
            // m_loadLOdefaultScripts is used to register gService
            // this.m_loadLOdefaultScripts = true;
            base.OnPreRender(e);
        }

        /// <summary>
        /// Binds the checkboxes for the vendor list.
        /// </summary>
        /// <param name="sender">The caller of this method.</param>
        /// <param name="e">The event arguments.</param>
        protected void Vendors_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                LightVOXVendor light = e.Item.DataItem as LightVOXVendor;
                HtmlInputCheckBox isEnabled = e.Item.FindControl("IsEnabled") as HtmlInputCheckBox;
                if (isEnabled != null)
                {
                    isEnabled.Checked = light.IsEnabled;
                }

                HtmlInputCheckBox isTestVendor = e.Item.FindControl("IsTestVendor") as HtmlInputCheckBox;
                if (isTestVendor != null)
                {
                    isTestVendor.Checked = light.IsTestVendor;
                }
            }
        }

        /// <summary>
        /// Populates the DU Validation Service Providers dropdown.
        /// </summary>
        private void BindDuValidationServiceProviders()
        {
            this.DuValidationServiceProviderId.Items.Add(new ListItem(string.Empty, string.Empty));
            foreach (var provider in DuServiceProviders.Instance.Providers)
            {
                this.DuValidationServiceProviderId.Items.Add(new ListItem(provider.DisplayName, provider.ProviderId.ToString()));
            }
        }

        /// <summary>
        /// Populates the platform DDL with an initial list of platforms.
        /// </summary>
        private void BindPlatformDDL()
        {
            this.AssociatedPlatformId.Items.Add(new ListItem(string.Empty, "0"));
            foreach (var platform in this.lazyPlatforms.Value)
            {
                this.AssociatedPlatformId.Items.Add(new ListItem(platform.PlatformName, platform.PlatformId.ToString()));
            }
        }
    }
}