<%@ Register TagPrefix="UC" TagName="DPU" Src="DefaultPermissions.ascx" %>
<%@ Page language="c#" Codebehind="Update.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Manage.UpdateForm" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/Common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../Common/Header.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<TITLE>Update Database</TITLE>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<link href="../../css/modal.css" type="text/css" rel="stylesheet">
		
		<script type="text/javascript" > 

		function Set105723PendingMessage() {
		    <%=AspxTools.JsGetElementById(MigrateLockPolicyFor105723Result)%>.value='Pending...';
		}
		function SetCEExportPendingMessage() {
		    <%=AspxTools.JsGetElementById(TryAsyncCEExportForLoanStatus)%>.value='Pending...';
		}
		</script>
  </HEAD>
  
	<BODY MS_POSITIONING="FlowLayout">
		<div class='mask' id='mask' onclick='javascript:Modal.Hide()'> </div>
		<FORM id="Update" method="post" runat="server">
			<uc:header id="m_Header" runat="server" />
			<uc:headernav id="m_Navigate" runat="server">
				<MenuItem URL="~/LOAdmin/Main.aspx" Label="Main" />
				<MenuItem URL="~/LOAdmin/Manage/Update.aspx" Label="Update Database" />
			</uc:headernav>

			
			
			<TABLE id="Table1" style="WIDTH: 8in" cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<TD class="FormTable"><BR>
						<TABLE id="Table3" cellSpacing="0" cellPadding="8" width="100%" border="0">
										<TR>
								<TD align="right" colSpan="2">
									<DIV><ml:EncodedLabel id="ErrorMessage" runat="server" ForeColor="Red"></ml:EncodedLabel></DIV>
									<DIV><ml:EncodedLabel id="Feedback" runat="server"></ml:EncodedLabel></DIV>
								</TD>
							</TR>
                                  <tr>
                                <td style="WIDTH: 297px"  align="right">
                                    Requeue OCR Doc Request
                                    </td>
                                <td>
                                    <ml:EncodedLabel AssociatedControlID="OCR_TransactionId" runat="server">Transaction Id</ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="OCR_TransactionId"></asp:TextBox>
                                    <br />
                                    <ml:EncodedLabel AssociatedControlID="OCR_BrokerId" runat="server">Broker Id</ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="OCR_BrokerId"></asp:TextBox>
                                    <asp:Button OnClick="RequeueOCRRequest_OnClick"  Text="Requeue" runat="server"/>
                                   
                                </td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 297px"  align="right">
                                    Update Expiration By Loan Officer
                                    </td>
                                <td>
                                    A list of loan officer employee ids separated by new lines.
                                     <asp:TextBox runat="server" ID="LoanOfficerIds"  TextMode="MultiLine" Columns="100" Rows="5" ></asp:TextBox>
                                    <br />
                                    <asp:Button runat="server" ID="LicneseUpdateByLoanOfficer" UseSubmitBehavior="false"  OnClick="LicneseUpdateByLoanOfficer_OnClick" Text="Update Loan Officer License" />

                                </td>
                            </tr>
                            <tr>
                                <td style="WIDTH: 297px">
							        <p align="right">Restart PDF Rasterizer</p>
							    </td>
                                <td>
                                    <asp:Button runat="server" ID="RestartRasterizer" UseSubmitBehavior="false" OnClick="RestartRasterizer_Click" Text="Restart PDF Rasterizer" />
                                </td>
                            </tr>
							<tr>
							    <td style="WIDTH: 297px">
							        <p align="right">Try ComplianceEase async export.</p>
							    </td>
							    <td style="BORDER-BOTTOM: gray 2px dotted" align="center">
							        <p align="left">
							            LoanID: <asp:TextBox ID="CEExportLoanID" runat="server" width="300px"></asp:TextBox>
							            <asp:Button ID="CEExportLoanButton" runat="server" Text="Try Exporting" OnClick="TryAsyncCEExportForLoan" 
							            OnClientClick="SetCEExportPendingMessage();" /> 
							            <br />
							            Status: <asp:TextBox runat="server" ID="TryAsyncCEExportForLoanStatus" ReadOnly="true">Not Performed</asp:TextBox>
							        </p>
							    </td>
							</tr>
							<tr>
							    <td style="WIDTH: 297px">
							        <p align="right">Case 105723 - Migrate lock policies / programs for broker (4/23/2013)&nbsp;</p>
							    </td>
							    <td style="BORDER-BOTTOM: gray 2px dotted" align="center">
							        <p align="left">
							            Broker ID: <asp:TextBox ID="MigrateLockPolicyFor105723Broker" runat="server" width="300px"></asp:TextBox>
							            <asp:Button ID="MigrateLockPolicyFor105723Btn" runat="server" Text="Migrate Lock Policies" OnClick="MigrateLockPolicyFor105723" 
							            OnClientClick="Set105723PendingMessage();" /> 
							            <br />
							            Status: <asp:TextBox runat="server" ID="MigrateLockPolicyFor105723Result" ReadOnly="true">Not Performed</asp:TextBox>
							        </p>
							    </td>
							</tr>
							<TR>
								<TD style="WIDTH: 297px">
									<P align="right">GetTotalMemory(true) including gc collect &nbsp; (10/6/06)</P>
								</TD>
								<TD style="BORDER-BOTTOM: gray 2px dotted" align="center">
									<P align="left">
										<asp:Button id="CollectMemBtn" runat="server" Width="97px" Font-Bold="True" Text="Call Now!" onclick="CollectMemBtn_Click"></asp:Button>&nbsp;
										<asp:textbox id="memUsedEd" runat="server" Width="131px" ReadOnly="True" Wrap="False"></asp:textbox></P>
								</TD>
							</TR>


								<tr>
						            <td>
						                <p align="right">Update Cached ConditionXML &nbsp;(10/07/16)</p>
						            </td>
						            <td style="border-bottom: gray 2px dotted" align="center">		
						                <p align="left">
									        <asp:Button id="Button1" onclick="UpdateConditionXmlCache_Click" runat="server" Text="Update Condition Cache"></asp:Button>&nbsp;&nbsp;
									        BrokerId: <asp:TextBox id="m_ConditionXmlUpdateTb" width="300px" runat="server"></asp:TextBox>
								        </p>
                                        <p style="text-align: left;">
                                            Note: This adds all of a brokers active loans to the Workflow Cache Updater queue, so the cached ConditionXML update will not be instantaneous.
                                        </p>
						            </td>
						        </tr>



							<asp:Panel runat="server" ID="errorPannel" Visible="false">
						        <tr>
						            <td colspan="2" style="color:Red; text-align: center;" >  
						                <ml:EncodedLiteral runat="server" ID="pageError" />
						            </td>
						        </tr>
						    </asp:Panel>
                            <TR>
                                <TD style="WIDTH: 297px" align="right">Update GSE Mortgage Limits  (1/5/09 hardcoded)</TD> 
                                <TD style="BORDER-BOTTOM: gray 2px dotted" >
                                    <ml:EncodedLabel AssociatedControlID="gseUrlBox" runat="server" ID="Label1" Text="URL:" ></ml:EncodedLabel> 
                                    <asp:TextBox runat="server" ID="gseUrlBox" Width="360" Text="http://www.hud.gov/pub/chums/cy2017-gse-limits.txt" > </asp:TextBox>
                                     <asp:Button id=gseUpdate runat="server" Text="Update" onclick="gseUpdate_Click"></asp:Button>
                                     Effective&nbsp;Date:<ml:DateTextBox runat="server" id="m_gseEffectiveDate" >1/1/2017</ml:DateTextBox>
                                 </TD>
                            </TR>
							<tr> 
								<td colspan=2 style="BORDER-BOTTOM: gray 2px dotted"> 
									<uc:DPU runat="Server" id="dpf" ></uc:DPU>
									<a  href="javascript:Modal.Show();" > Update Default Role Permissions </a>
								</td>															
							</tr>

						
        <TR>
          <TD style="WIDTH: 297px" align="right">Update FHA Mortgage Limits  (12/31/08 hardcoded)</TD> 
          <TD style="BORDER-BOTTOM: gray 2px dotted" >
              <ml:EncodedLabel AssociatedControlID="fhaUrlBx" runat="server" ID="lblFha" Text="URL:" ></ml:EncodedLabel> 
              <asp:TextBox runat="server" ID="fhaUrlBx" Width="360" Text="http://www.hud.gov/pub/chums/cy2017-forward-limits.txt" > </asp:TextBox>
              <asp:Button id=m_btnUpdateFhaLimit runat="server" Text="Update" onclick="m_btnUpdateFhaLimit_Click"></asp:Button>
              Effective&nbsp;Date:<ml:DateTextBox runat="server" id="m_FhaEffectiveDate" >1/1/2017</ml:DateTextBox>
           </TD>
        </TR>

							<TR>
								<TD style="WIDTH: 297px">
									<P align="right">Dot Net Version Being Used</P>
								</TD>
								<TD style="BORDER-BOTTOM: gray 2px dotted" align="center">
									<P align="left">
										<asp:TextBox id="DotNetVersionEd" runat="server" Width="380px" ReadOnly="True"></asp:TextBox></P>
								</TD>
							</TR>
							
							<TR>
								<TD style="WIDTH: 297px">
									<P align="right">Remove blank rows of price policy association</P>
								</TD>
								<TD style="BORDER-BOTTOM: gray 2px dotted" align="center">
									<P align="left"><asp:button id="RemoveBlankAssocBtn" runat="server" Width="95px" Text="Remove Now!" Font-Bold="True" onclick="RemoveBlankAssocBtn_Click"></asp:button>&nbsp;&nbsp;&nbsp;
										<asp:textbox id="AssocRowsRemovedEd" runat="server" Width="221px" Wrap="False" ReadOnly="True"></asp:textbox>&nbsp;rows 
										affected</P>
								</TD>
							</TR>
							<TR>
								<TD style="WIDTH: 297px"></TD>
								<TD style="BORDER-BOTTOM: gray 2px dotted" align="center">
									<P align="left">ProductId:
										<asp:textbox id="EventHandlerProdId_ed" runat="server" Width="221px" Wrap="False"></asp:textbox></P>
									<P align="left">BrokerId:
										<asp:textbox id="EventHandlerBrokerId_ed" runat="server" Width="221px" Wrap="False"></asp:textbox></P>
									<P align="left"><asp:button id="AfterModifOfThisMasterBtn" runat="server" Width="174px" Text="AfterModifOfThisMaster" Font-Bold="True" onclick="AfterModifOfThisMasterBtn_Click"></asp:button></P>
									<P align="left">Msg:
										<asp:textbox id="EventHandlerMsg_ed" runat="server" Width="424px" Wrap="False" ReadOnly="True"></asp:textbox></P>
								</TD>
							</TR>
							<TR>
								<TD style="WIDTH: 297px">
									<P align="right">Force&nbsp;Update Cached Value of Product Inheritance</P>
									<P align="right">&nbsp;</P>
									<P align="right">&nbsp;</P>
								</TD>
								<TD style="BORDER-BOTTOM: gray 2px dotted" align="center">
									<P align="left">AfterRateOptionsBatchUpdate:&nbsp;
										<asp:button id="AfterRateOptionBatchUpdateBtn" runat="server" Width="82px" Text="Update Now!" Font-Bold="True" onclick="AfterRateOptionBatchUpdateBtn_Click"></asp:button></P>
									<P align="left">AfterLpeUpdateProjectIsRun:&nbsp;
										<asp:button id="AfterLpeUpdateProjectIsRunBtn" runat="server" Width="83px" Text="Update Now!" Font-Bold="True" onclick="AfterLpeUpdateProjectIsRunBtn_Click"></asp:button></P>
									<P align="left">AfterLoadminRequestFixupAllLoanProgramsInSystem:&nbsp;
										<asp:button id="InheritanceUpdateBtn" runat="server" Width="82px" Text="Update Now!" Font-Bold="True" onclick="InheritanceUpdateBtn_Click"></asp:button></P>
									<P align="left">AfterUpdateAllNonmasterProds
										<asp:button id="AfterUpdateAllNonmasterProdsBtn" runat="server" Width="82px" Text="Update Now!" Font-Bold="True" onclick="AfterUpdateAllNonmasterProdsBtn_Click"></asp:button></P>
									<P align="left">&nbsp;</P>
								</TD>
							</TR>
							<TR>
								<TD style="WIDTH: 297px">
									<P align="right">Force Garbage Collection</P>
								</TD>
								<TD style="BORDER-BOTTOM: gray 2px dotted" align="center">
									<P align="left"><asp:button id="GarbageCollectBtn" runat="server" Width="139px" Text="Garbage Collect Now!" Font-Bold="True" onclick="GarbageCollectBtn_Click"></asp:button></P>
								</TD>
							</TR>
							<TR>
								<TD style="WIDTH: 297px">
									<P align="right">Operate on a broker's loan</P>
								</TD>
								<TD style="BORDER-BOTTOM: gray 2px dotted" align="center">
									<DIV style="BORDER-RIGHT: gray 2px solid; PADDING-RIGHT: 6px; BORDER-TOP: gray 2px solid; PADDING-LEFT: 6px; BACKGROUND: ivory; MARGIN-BOTTOM: 8px; PADDING-BOTTOM: 6px; FONT: 11px arial; BORDER-LEFT: gray 2px solid; WIDTH: 4in; PADDING-TOP: 6px; BORDER-BOTTOM: gray 2px solid; TEXT-ALIGN: center"><SPAN style="MARGIN-RIGHT: 0px">Loan 
											id = </SPAN>
										<ASP:TEXTBOX id="CurrentLoanId" style="BORDER-RIGHT: gray 2px solid; BORDER-TOP: gray 2px solid; PADDING-LEFT: 4px; BORDER-LEFT: gray 2px solid; BORDER-BOTTOM: gray 2px solid" runat="server" Width="3in"></ASP:TEXTBOX></DIV>
									<ASP:BUTTON id="UpdateCachedLoan" runat="server" Width="132px" Text="Update cached loan" Font-Bold="True" onclick="UpdateCachedLoanClick"></ASP:BUTTON></TD>
							<TR>
								<TD style="WIDTH: 297px">
									<P align="right">Operate on broker's loans</P>
								</TD>
								<TD style="BORDER-BOTTOM: gray 2px dotted" align="center">
									<DIV style="BORDER-RIGHT: gray 2px solid; BORDER-TOP: gray 2px solid; OVERFLOW-Y: scroll; BACKGROUND: ivory; MARGIN-BOTTOM: 8px; FONT: 11px arial; BORDER-LEFT: gray 2px solid; WIDTH: 4in; BORDER-BOTTOM: gray 2px solid; HEIGHT: 1in">
										<TABLE id="Table5" style="FONT: 11px arial" cellSpacing="2" cellPadding="4" width="100%" border="0">
											<THEAD style="BACKGROUND-COLOR: khaki">
												<TR>
													<TD align="left">Name
													</TD>
													<TD align="center">Active?
													</TD>
													<TD align="center"></TD>
												</TR>
											</THEAD>
											<ASP:REPEATER id="BrokerList" runat="server" EnableViewState="False" OnItemCommand="ChooseBrokerForOp">
												<ItemTemplate>
													<TR style="BACKGROUND: transparent;" onmouseover="style.backgroundColor = 'beige';" onmouseout="style.backgroundColor = 'transparent';">
														<TD align="left">
															<%#AspxTools.HtmlString(Filter( DataBinder.Eval( Container.DataItem , "BrokerNm" ) ))%>
														</TD>
														<TD align="center">
															<%#AspxTools.HtmlString(Filter( DataBinder.Eval( Container.DataItem , "IsActive" ) ))%>
														</TD>
														<TD align="center">
															<ASP:LinkButton ID="ChooseBroker" runat="server">choose</ASP:LinkButton>
														</TD>
													</TR>
												</ItemTemplate>
											</ASP:REPEATER></TABLE>
									</DIV>
									<DIV style="BORDER-RIGHT: gray 2px solid; PADDING-RIGHT: 6px; BORDER-TOP: gray 2px solid; PADDING-LEFT: 6px; BACKGROUND: ivory; MARGIN-BOTTOM: 8px; PADDING-BOTTOM: 6px; FONT: 11px arial; BORDER-LEFT: gray 2px solid; WIDTH: 4in; PADDING-TOP: 6px; BORDER-BOTTOM: gray 2px solid; TEXT-ALIGN: center">
                                        <ml:EncodedLabel id="CurrentBrokerNm" runat="server">Broker not selected</ml:EncodedLabel>:
										<ml:EncodedLabel id="CurrentBrokerId" runat="server">No id specified</ml:EncodedLabel>
									</DIV>
									<DIV style="BORDER-RIGHT: gray 2px solid; BORDER-TOP: gray 2px solid; OVERFLOW-Y: scroll; BACKGROUND: ivory; MARGIN-BOTTOM: 8px; FONT: 11px arial; BORDER-LEFT: gray 2px solid; WIDTH: 4in; BORDER-BOTTOM: gray 2px solid; HEIGHT: 1in">
										<TABLE id="Table6" style="FONT: 11px arial" cellSpacing="2" cellPadding="4" width="100%" border="0">
											<THEAD style="BACKGROUND-COLOR: tan">
												<TR>
													<TD align="left">Loan
													</TD>
													<TD align="center">Status
													</TD>
												</TR>
											</THEAD>
											<ASP:REPEATER id="LoanList" runat="server" EnableViewState="False">
												<ItemTemplate>
													<TR style="BACKGROUND: transparent;" onmouseover="style.backgroundColor = 'beige';" onmouseout="style.backgroundColor = 'transparent';">
														<TD align="left">
															<%#AspxTools.HtmlString(Filter( DataBinder.Eval( Container.DataItem , "LoanNm" ) ))%>
														</TD>
														<TD align="center">
															<%#AspxTools.HtmlString(Status( DataBinder.Eval( Container.DataItem , "Status" ) ))%>
														</TD>
													</TR>
												</ItemTemplate>
											</ASP:REPEATER></TABLE>
										<DIV style="PADDING-RIGHT: 4px; PADDING-LEFT: 8px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px; TEXT-ALIGN: left">(
											<%=AspxTools.HtmlString(LoanList.Items.Count)%>
											loans )
										</DIV>
									</DIV>
									<ASP:BUTTON id="MaskBrokersLoans" runat="server" Width="125px" Text="Mask broker's loans" Font-Bold="True" onclick="MaskBrokersLoansClick"></ASP:BUTTON></TD>
							</TR>
			<tr>
			    <td align=right class="FieldLabel">
			        Requeue EDoc 
			    </td>
			    <td>
			        <asp:FileUpload runat="server" ID="EDocRequeuCSVFile" /> CSV File (BrokerId,DocumentId) - Limit Size, Bad on DB 
			        <br />
			        <asp:CheckBox runat="server" id="VerifyPages" Text="Ensure all images exist (VERY SLOW)" onclick="if(this.checked && !confirm('Are you sure? This will make conversion take much longer.')){this.checked = false;}" />
			        <br />
			        <asp:Button runat="server" ID="RequeueBtn" Font-Bold="true" Text="Requeue EDocs" OnClick="RequeueBtn_OnClick" />
			    </td>
			</tr>
            <tr>
                <td align="right">
                    Dump Sass Defaults
                </td>
                <td>
                    <asp:Button runat="server" ID="DumpSassDefaults" text="Dump Sass Defaults" OnClick="DumpSassDefaults_Click" />
                </td>
            </tr>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</FORM>
	</BODY>
</HTML>
