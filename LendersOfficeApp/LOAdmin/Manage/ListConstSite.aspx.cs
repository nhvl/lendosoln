using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text;
using System.Reflection;

using LendersOffice.Constants;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public class NameValue 
    {
        private string m_name;
        private string m_value;

        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string Value 
        {
            get { return m_value; }
            set { m_value = value; }
        }

        public NameValue(string name, string value) 
        {
            m_name = name;
            m_value = value;
        }
    }

    public abstract class AbstractConfigValidator 
    {
        private SortedList m_mainList;
        private SortedList m_actualList;
        private SortedList m_normalList;
        private SortedList m_missingInMainList;
        private SortedList m_missingInActualList;

        public AbstractConfigValidator(object constantClass) 
        {
            m_mainList = new SortedList();
            GetPropertiesInClass(m_mainList, constantClass);
        }

        protected void SetActualList(SortedList actualList) 
        {
            m_actualList = actualList;
        }

        public SortedList NormalList 
        {
            get { return m_normalList; }
        }

        public SortedList MissingInMainList 
        {
            get { return m_missingInMainList; }
        }

        public SortedList MissingInActualList 
        {
            get { return m_missingInActualList; }
        }
        public void Process() 
        {
            if (null == m_mainList)
                throw new ArgumentNullException("mainList");
            if (null == m_actualList)
                throw new ArgumentNullException("actualList");

            m_normalList = new SortedList();
            m_missingInActualList = new SortedList();
            m_missingInMainList = new SortedList();
            int i = 0;
            int j = 0;

            while (true) 
            {
                if (i >= m_mainList.Count && j >= m_actualList.Count) 
                {
                    break; // Complete.
                } 
                else if (i >= m_mainList.Count) 
                {
                    while (j < m_actualList.Count) 
                    {
                        AddToList(m_missingInMainList, (NameValue) m_actualList.GetByIndex(j++));
                    }
                    break;
                } 
                else if (j >= m_actualList.Count) 
                {
                    while (i < m_mainList.Count) 
                    {
                        AddToList(m_missingInActualList, (NameValue) m_mainList.GetByIndex(i++));
                    }
                    break;
                }

                string lKey = (string) m_mainList.GetKey(i);
                string rKey = (string) m_actualList.GetKey(j);

                int eq = lKey.CompareTo(rKey);

                if (eq == 0) 
                {
                    AddToList(m_normalList, (NameValue) m_mainList[lKey]);
                    i++;
                    j++;
                } 
                else if (eq < 0) 
                {
                    AddToList(m_missingInActualList, (NameValue) m_mainList[lKey]);
                    i++;
                }
                else if(eq > 0) 
                {
                    AddToList(m_missingInMainList, (NameValue) m_actualList[rKey]);
                    j++;
                }
            }
        }

        protected void AddToList(SortedList list, NameValue nv) 
        {
            list.Add(nv.Name.ToLower(), nv);
        }
        private void AddToList(SortedList list, string key, NameValue nv) 
        {
            list.Add(key.ToLower(), nv);
        }

        private void GetPropertiesInClass(SortedList list, object obj) 
        {
            try 
            {
                Type t = obj.GetType();

                PropertyInfo[] props = t.GetProperties(BindingFlags.Static | BindingFlags.Public);

                foreach (PropertyInfo p in props) 
                {

                    object[] attrs = p.GetCustomAttributes(typeof(ConstantAttribute), false);
                
                    string key = p.Name;
                    string name = p.Name;
                    string value = "UNDEFINED!!!";
                    try 
                    {
                        value = p.GetValue(obj, new object[0]).ToString();
                    } 
                    catch {}

                    if (attrs.Length > 0) 
                    {
                        ConstantAttribute attr = (ConstantAttribute) attrs[0];
                        if (attr.IsSensitiveData)
                            value = "** INFORMATION TOO SENSITIVE TO DISPLAY **";
                        if (attr.ActualKey != "")
                            key = attr.ActualKey;
                    }

                    AddToList(list, key, new NameValue(name, value));
                }
            } 
            catch {}

        }
    }

    public class WebConfigValidator : AbstractConfigValidator
    {
        public WebConfigValidator() : base(new ConstSite()) 
        {
            SortedList list = new SortedList();
            foreach (string key in ConfigurationManager.AppSettings.Keys) 
            {
                AddToList(list, new NameValue(key, ConfigurationManager.AppSettings[key]));
            }

            SetActualList(list);
        }
    }
    public class StageConfigValidator : AbstractConfigValidator 
    {
        public StageConfigValidator() : base(new ConstStage()) 
        {
            SortedList list = new SortedList();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "ListStageConfigKeys")) 
            {
                while (reader.Read()) 
                {
                    string key = (string) reader["KeyIdStr"];
                    string value = string.Format("int:{0}, str:{1}", reader["OptionContentInt"], reader["OptionContentStr"]);
                    AddToList(list, new NameValue(key, value));
                }
            }
            SetActualList(list);
        }
    }

	public partial class ListConstSite : LendersOffice.Admin.SecuredAdminPage
	{
    
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.IsDevelopment
            };
        }

        private void DisplayConstSiteList() 
        {
            WebConfigValidator validator = new WebConfigValidator();
            validator.Process();

            SortedList normalList = validator.NormalList;
            SortedList missingInMainList = validator.MissingInMainList;
            SortedList missingInActualList = validator.MissingInActualList;

            StringBuilder sb = new StringBuilder();

            if (missingInMainList.Count > 0) 
            {
                sb.Append("<h3 class='text-danger'>NOT DEFINE in LendersOffice.Constant.ConstSite class. Possible cause is the value is obsolete.</h3>");
                ExportList(sb, missingInMainList);
            }
            if (missingInActualList.Count > 0) 
            {
                sb.Append("<h3 class='text-danger'>NOT DEFINE in web.config. Possible cause new constant is added but not define in web.config.</h3>");
                ExportList(sb, missingInActualList);
            }
            sb.Append("<h3>LendersOffice.Constant.ConstSite</h3>");
            ExportList(sb, normalList);


            WebConfigList.Text = sb.ToString();

        }

        private void DisplayConstStageList() 
        {
            StageConfigValidator validator = new StageConfigValidator();
            validator.Process();

            SortedList normalList = validator.NormalList;
            SortedList missingInMainList = validator.MissingInMainList;
            SortedList missingInActualList = validator.MissingInActualList;

            StringBuilder sb = new StringBuilder();

            if (missingInMainList.Count > 0) 
            {
                sb.Append("<h3 class='text-danger'>NOT DEFINE in LendersOffice.Constant.ConstStage class. Possible cause is the value is obsolete.</h3>");
                ExportList(sb, missingInMainList);
            }
            if (missingInActualList.Count > 0) 
            {
                sb.Append("<h3 class='text-danger'>NOT DEFINE in Stage_Config table. Possible cause new constant is added but not define in Stage_Config.</h3>");
                ExportList(sb, missingInActualList);
            }
            sb.Append("<h3>LendersOffice.Constant.ConstStage</h3>");
            ExportList(sb, normalList);


            WebConfigList.Text = sb.ToString();

        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
            string type = RequestHelper.GetSafeQueryString("type");

            if (type == "conststage") 
            {
                DisplayConstStageList();

            } 
            else 
            {
                DisplayConstSiteList();
            }

		}

        private void ExportList(StringBuilder sb, SortedList list) 
        {
            sb.Append("<ul class='breakword'>");
            foreach (NameValue nv in list.Values) 
            {
                sb.AppendFormat("<li><b>{0}</b> - {1}", AspxTools.HtmlString(nv.Name), AspxTools.HtmlString(nv.Value));
            }
            sb.Append("</ul>");
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
