﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class EditShippingTemplateService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Save":
                    Save();
                    break;
                default:
                    break;
            }
        }

        private void Save()
        {
            bool invalidOp = false;
            string shippingTemplateName = GetString("name");
            string id = GetString("id");
            string order = GetString("order");

            EDocumentShippingTemplate template = new EDocumentShippingTemplate();

            try
            {
                if (!String.IsNullOrEmpty(id))
                    template.EditShippingTemplateType(shippingTemplateName, order, Convert.ToInt32(id));
                else
                    template.AddShippingTemplateType(shippingTemplateName, order);
            }
            catch (DuplicateNameSelectedException exc)
            {
                invalidOp = true;
                SetResult("userMsg", exc.UserMessage);
            }

            SetResult("invalidOp", invalidOp);
        }
    }
}
