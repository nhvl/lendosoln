<%@ Page Language="C#" CodeBehind="~/LOAdmin/Manage/SiteConfigEdit.aspx" MasterPageFile="~/LOAdmin/loadmin.Master" Inherits="LendersOfficeApp.LOAdmin.Manage.SiteConfigEdit" Title="Random Utilities"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
  <div class="row">
    <div class="col-xs-10 col-xs-offset-1"><div class="col-xs-12"><div class="col-xs-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <form id="stageForm" runat="server">
            <div class="form-horizontal">
            <h4 class="col-xs-offset-1">Decrypt String</h4>
              <div class="form-group">
                  <label class="control-label col-xs-3">Value:</label>
                  <div class=" col-xs-4 "><input type="text" id="EncValue" class="form-control input-sm"/></div>
                  <span class="clipper" ><input type="button" value="Decrypt" onclick="f_decrypt()" class="btn btn-sm btn-default"/> </span>
                  <span class="clipper"><input type="button" value="Encrypt" onclick="f_encrypt()" class="btn btn-sm btn-primary" /></span>
              </div>
                <div class="form-group">
                  <div class=" col-xs-4  col-xs-offset-3"><input type="text" id="DecValue" class="form-control input-sm"/></div>
                </div>
              <br />
              <h4 class="col-xs-offset-1">Quick Utilities for generating password hash and friendly ID</h4>
              <div class="form-group">
                <label class="control-label col-xs-3">Password:</label>
                <div class=" col-xs-4">
                    <input type="text" id="Password" class="form-control input-sm"/>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-xs-3">Hash Password:</label>
                <div class=" col-xs-4"><input type="text" id="HashPassword" class="form-control input-sm"/></div>
                <span class="clipper"><input type="button"  value="Get Hash" onclick="f_hash();" class="btn btn-sm btn-primary" /></span>
                <span class="clipper"><input type="button"  value="Get Salted Hash" onclick="f_saltHash();" class="btn btn-sm btn-primary" /></span>
              </div>
                <div class="form-group">
                <label class="control-label col-xs-3">Salt</label>
                <div class=" col-xs-4"><input type="text" id="Salt" class="form-control input-sm"/></div>
              </div>
              <div class="form-group">
                <label class="control-label col-xs-3">Friendly ID:</label>
                <div class=" col-xs-4"><input type="text" id="FriendlyId" class="form-control input-sm"/></div>
                <span class="clipper"><input type="button" value="Generate Friendly ID" onclick="f_generateFriendlyId();" class="btn btn-sm btn-primary" /></span>
              </div>
              <br />
              <h4 class="col-xs-offset-1">Generate Random Keys (SHA1 - 64 bytes, AES - 32 bytes, 3DES - 24 bytes)</h4>
              <div class="form-group">
                <label class="control-label col-xs-3">Key Size:</label>
                <div class=" col-xs-1"><input type="text" name="EncryptedCookie" id="KeySizeBytes" class="form-control input-sm"/></div>
                <label class="control-label">bytes:</label>
                <span class="clipper"><input type="button" value="Generate Random Keys" onclick="f_generateRandomKey();" class="btn btn-sm btn-primary" /></span>
              </div>
              <div class="form-group">
                <div class=" col-xs-6  col-xs-offset-3"><input ID="Keys"  type="text" class="form-control input-sm" /></div>
              </div>
              <div class="form-group">
                <label class="control-label col-xs-3">Decrypt Cookie:</label>
                <div class=" col-xs-2"><input type="text" name="EncryptedCookie" id="EncryptedCookie" class="form-control input-sm"/></div>
                <div class=" col-xs-2"><input type="text" name="ReadableCookieValue" id="ReadableCookieValue" class="form-control input-sm"/></div>
                <span class="clipper"><input type="button" onclick="decryptCookie()" value="Get Cookie Data" class="btn btn-sm btn-primary" /></span>
              </div>
              <div class="form-group">
                <label class="control-label col-xs-3">Unencrypted Connection String:</label>
                <div class=" col-xs-5"><asp:TextBox ID="UnencryptedConnectionString" runat="server" class="form-control input-sm"/></div>
                <span class="clipper"><input type="button" onclick="encryptConnectionString()" value="Encrypt" class="btn btn-sm btn-primary" /></span>
              </div>
              <div class="form-group">
                <label class="control-label col-xs-3">Encrypted Connection String:</label>
                <div class=" col-xs-6"><asp:TextBox ID="EncryptedConnectionString" runat="server" class="form-control input-sm"/></div>
              </div>
              <br />
              <h4 class="col-xs-offset-1">Generate Vendor Client Certificate</h4>
              <div class="form-group">
                <label for="VendorClientCertName" class="control-label col-xs-3">Vendor Name:</label>
                <div class=" col-xs-4"><input type="text" id="VendorClientCertName" class="form-control input-sm"/></div>
                <span class="clipper"><input type="button" onclick="generateVendorClientCert()" value="Generate Cert" class="btn btn-sm btn-primary" /></span>
              </div>
              <div class="form-group">
                <label for="VendorClientCertId" class="control-label col-xs-3">Certificate Thumbprint:</label>
                <div class=" col-xs-4"><input type="text" id="VendorClientCertThumbprint" class="form-control input-sm" readonly="readonly"/></div>
              </div>
              <div class="form-group">
                <label for="VendorClientCertId" class="control-label col-xs-3">Certificate ID:</label>
                <div class=" col-xs-4"><input type="text" id="VendorClientCertId" class="form-control input-sm" readonly="readonly"/></div>
              </div>
              <div class="form-group">
                <label for="VendorClientCertPassword" class="control-label col-xs-3">Certificate Password:</label>
                <div class=" col-xs-4"><input type="text" id="VendorClientCertPassword" class="form-control input-sm" readonly="readonly"/></div>
              </div>
              <br />
              <div class="form-group text-center">
                <input type="button" value="Move Audit Trail" onclick="f_moveAuditTrail(); " class="btn btn-sm btn-default">
                 <input type="button" value="Restart PDF Conversion Process" onclick="f_restartPdfConversionProcess()" class="btn btn-sm btn-primary">
              </div>
            </div>
          </form>
          <script type="text/javascript">
              $(document).ready(function() {
                  $('#VendorClientCertThumbprint').val(ML.CertificateThumbprint);
              });

              function alertFailedService(result){
                  alert(result.UserMessage);
              }
              function f_saltHash()
              {
                if (!inputValidated(input = $("#Password") ))
                { 
                    return;
                }

                var args = 
                { 
                    Password: input.val() 
                };

                var result = gService.siteconfigedit.call("GetSaltedHash", args, true, false, false, true,
                                function(result) 
                                {
                                    $("#HashPassword").val(result.value["Hash"]);
                                    $("#Salt").val(result.value["Salt"]);
                                },
                                alertFailedService);
              }
              function f_hash() {

                  if (!inputValidated (input = $("#Password") )){ return;}
                  var args = { Password: input.val() };

                  var result=gService.siteconfigedit.call("GetHashPassword",args,true, false, false, true,
                      function(result) {document.getElementById("HashPassword").value=result.value["HashPassword"];},
                     alertFailedService);
              }

              function f_generateFriendlyId() {
                  var result=gService.siteconfigedit.call("CreateFriendlyId",null,true, false, false, true,
                      function(result) { document.getElementById("FriendlyId").value=result.value["FriendlyId"];},
                      function(result){alert(result.UserMessage);});
              }

              function f_moveAuditTrail() {

                  var result=gService.siteconfigedit.call("MoveAuditTrail",null,true, false, false, true,
                      function(result) {alert(result.value["Status"]);},
                     alertFailedService);
              }

              function f_restartPdfConversionProcess() {

                  var result=gService.siteconfigedit.call("RestartPdfConversionProcess",null,true, false, false, true,
                  function(result) {alert('Done');},
                     alertFailedService);
              }

              function f_generateRandomKey() {

                  if (!inputValidated (input = $("#KeySizeBytes") )){ return;}
                  var args = { KeySizeBytes: input.val() };

                  var result=gService.siteconfigedit.call("GenerateRandomKey",args,true, false, false, true,
                  function(result) {document.getElementById("Keys").value=result.value["Keys"];},
                     alertFailedService);
              }

              function f_decrypt() {

                  if (!inputValidated (input = $("#EncValue") )){ return;}
                  var args = { value: input.val() };
                    
                  var result=gService.siteconfigedit.call('Decrypt',args,true, false, false, true,
                  function (result)  {document.getElementById('DecValue').value=result.value["Value"]; },
                  function (result)  { alert ("Can not decrypt value");} );
              }
        
              function f_encrypt() {
                  if (!inputValidated (input = $("#EncValue") )){ return;}
                  var args = { EncValue: input.val() };

                  var result=gService.siteconfigedit.call('Encrypt',args,true, false, false, true,
                  function(result) { document.getElementById('DecValue').value=result.value["Value"];},
                     alertFailedService);
              }
        
              function decryptCookie() {

                  if (!inputValidated (input = $("#EncryptedCookie") )){ return;}
                  var args = { Cookie: input.val() };

                  var decryptedCookie =  document.getElementById('ReadableCookieValue');

                  var result = gService.siteconfigedit.call('DecryptCookie',args,true, false, false, true,
                  function(result) {decryptedCookie.value = result.value.DecryptedCookie;;},
                     alertFailedService);
              }

              function encryptConnectionString(){
                
                  var inputId = <%=AspxTools.JQuery(UnencryptedConnectionString)%>;
                    if (!inputValidated (input = inputId )){ return;}
                    var args = { ConnectionString: input.val() };

                    var connStr = <%= AspxTools.JQuery(UnencryptedConnectionString) %>.val();
                    var args = { ConnectionString : connStr};
            
                    var result = gService.siteconfigedit.call('EncryptSqlConnection', args,true, false, false, true,
                    function(result) {<%= AspxTools.JQuery(EncryptedConnectionString) %>.val(result.value.ConnectionString);},
                       alertFailedService);
              }

              function generateVendorClientCert()
              {
                  var vendorNameInput = $('#VendorClientCertName');
                  if (!inputValidated(vendorNameInput)) {
                      return;
                  }

                  var args = {
                      VendorName: vendorNameInput.val()
                  };
                  
                  gService.siteconfigedit.callAsyncSimple('GenerateVendorClientCert', args, vendorCertSuccess, alertFailedService);
              }

              function vendorCertSuccess(result) {
                  $('#VendorClientCertId').val(result.value.CertificateId);
                  $('#VendorClientCertPassword').val(result.value.CertificatePassword);

                  var url = <%=AspxTools.SafeUrl(VirtualRoot + "/LOAdmin/Manage/DownloadVendorCert.aspx?k=")%> + result.value.CertificateKey;
                  window.open(url);

                  alert('To register the vendor cert, add an entry to the "VENDOR_CLIENT_CERTIFICATE" table.');
              }

                    function inputValidated(requiredInput){
                        refreshAlert();
                        if (requiredInput){
                            if (requiredInput.val() == ''){
                                showInputError (requiredInput,"Required.");
                            }
                            else if (requiredInput.val() == ''){
                                showInputError (requiredInput,"Required at least one non-space character.");
                            }
                            else{
                                return true;
                            }
                        }
                        return false;
                    }

                    function refreshAlert() {

                        $("div.has-error").removeClass("has-error has-feedback");
                        $("input").tooltip("destroy");
                        $('.tooltip').remove();
                    }

                    function showInputError(input,errorString){

                        input.parent().closest('div').addClass( "has-error");
                        input.tooltip({ title: errorString });
                        input.focus();
                    }

                    $("input[type=text]").focus(function () {

                        if (!($(this).parent().closest('div').hasClass("has-error"))) {
                            refreshAlert();
                        }
                    });
          </script>
        </div>
      </div>
    </div></div></div>
  </div>
</asp:Content>
