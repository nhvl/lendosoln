﻿/// <copyright file="ManageModifiedLoans.aspx.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   3/18/2016
/// </summary>
namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Services;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.LOAdmin.Manage;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a means of viewing loans and adding loans
    /// to the modified loan file list.
    /// </summary>
    public partial class ManageModifiedLoans : SecuredAdminPage
    {
        /// <summary>
        /// Represents the blank option displayed in the broker selection dropdown.
        /// </summary>
        private const string BlankBrokerCode = "<-- Select a Broker -->";

        /// <summary>
        /// Represents the required permissions to view the page.
        /// </summary>
        private static readonly E_InternalUserPermissions[] PagePermissions = new E_InternalUserPermissions[]
        {
            E_InternalUserPermissions.ViewBrokers
        };

        /// <summary>
        /// Gets the value for the page's style sheet.
        /// </summary>
        /// <value>
        /// <see cref="string.Empty"/>, as the page will
        /// use custom style sheets.
        /// </value>
        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Retrieves the app codes for the broker with the
        /// specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> of the broker.
        /// </param>
        /// <returns>
        /// A list of app code names and their associated ids.
        /// </returns>
        [WebMethod]
        public static string RetrieveAppCodes(Guid brokerId)
        {
            return Tools.LogAndThrowIfErrors(() =>
            {
                return ObsoleteSerializationHelper.JsonSerialize(AppCode.Retrieve(brokerId));
            });
        }

        /// <summary>
        /// Searches for modified loans within the last 90 days using 
        /// the specified <paramref name="appCodeId"/> for the broker
        /// with the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> of the broker.
        /// </param>
        /// <param name="appCodeId">
        /// The <see cref="Guid"/> app code to search.
        /// </param>
        /// <returns>
        /// The list of modified loans for the broker.
        /// </returns>
        [WebMethod]
        public static string Search(Guid brokerId, Guid appCodeId)
        {
            return Tools.LogAndThrowIfErrors(() =>
            {
                return ObsoleteSerializationHelper.JsonSerialize(ModifiedLoan.Search(brokerId, appCodeId));
            });
        }

        /// <summary>
        /// Marks loans in the specified <paramref name="loanList"/> as modified
        /// using the specified <paramref name="appCodeId"/> for the broker
        /// with the specified <paramref name="brokerId"/>. A boolean indicates
        /// if a loan's name or id is specified.
        /// </summary>
        /// <param name="brokerId">
        /// The broker for the modified loans.
        /// </param>
        /// <param name="appCodeId">
        /// The <see cref="AppCode"/> that will mark the loan as modified.
        /// </param>
        /// <param name="loanList">
        /// The list of loan separated by newlines.
        /// </param>
        /// <param name="useLoanName">
        /// True if <paramref name="loanList"/> is comprised of loan names, false
        /// if <paramref name="loanList"/> is comprised directly of loan ids.
        /// </param>
        /// <returns>
        /// A list representing the results for the update.
        /// </returns>
        [WebMethod]
        public static string Update(Guid brokerId, Guid appCodeId, string loanList, bool useLoanName)
        {
            return Tools.LogAndThrowIfErrors(() =>
            {
                return ObsoleteSerializationHelper.JsonSerialize(ModifiedLoan.Update(brokerId, appCodeId, loanList, useLoanName));
            });
        }

        /// <summary>
        /// Adds the given list of loans to the modified list for the given brokerId and app code.
        /// </summary>
        /// <param name="brokerId">The Broker ID for the lender that the loans belong to.</param>
        /// <param name="appCodeId">The app code for the application that is tracking modified files.</param>
        /// <param name="loanList">A delimited list of loan IDs to add to the modified list.</param>
        /// <param name="useLoanName">Whether or not to use the loan name as the ID in <paramref name="loanList"/>. If false, uses Loan IDs.</param>
        /// <returns>A object that contains a message and a invaid loan list.</returns>
        [WebMethod]
        public static object UpdateInBatch(Guid brokerId, Guid appCodeId, string loanList, bool useLoanName)
        {
            return Tools.LogAndThrowIfErrors(() => ModifiedLoan.UpdateInBatch(brokerId, appCodeId, loanList, splitString: "\n", useLoanName: useLoanName));
        }

        /// <summary>
        /// Adds all of the lender's valid, non-template, non-sandbox, non-test loans to the modified loan list with the specified app code.
        /// </summary>
        /// <param name="brokerId">The brokerId of the lender to add loans for.</param>
        /// <param name="appCodeId">The app code for the application that is tracking modified files.</param>
        /// <param name="markValidLoans">True if valid loans should be marked, false if invalid loans should be marked.</param>
        /// <returns>A status message describing whether or not the operation to add all loans completed successfully.</returns>
        [WebMethod]
        public static string AddAllFiles(Guid brokerId, Guid appCodeId, bool markValidLoans)
        {
            return Tools.LogAndThrowIfErrors(() => ModifiedLoan.AddAllLoansToModifiedList(brokerId, appCodeId, markValidLoans));
        }

        /// <summary>
        /// Removes loans from the modified list for the given broker older than the given Last Modified date.
        /// </summary>
        /// <param name="brokerId">The brokerId of the lender to clear loans for.</param>
        /// <param name="appCodeId">The app code for the application that is tracking modified files.</param>
        /// <param name="lastModified">The modified list will be cleared of loans with a Last Modified Date prior to this date.</param>
        /// <returns>A status message describing whether or not the operation to clear loans completed successfully.</returns>
        [WebMethod]
        public static string ClearModifiedList(Guid brokerId, Guid appCodeId, DateTime lastModified)
        {
            return Tools.LogAndThrowIfErrors(() => ModifiedLoan.ClearModifiedList(brokerId, appCodeId, lastModified));
        }

        /// <summary>
        /// Obtains an array of the permissions required to access the page.
        /// </summary>
        /// <returns>
        /// An array containing the required permissions to access the page.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return ManageModifiedLoans.PagePermissions;
        }

        /// <summary>
        /// Handles the page pre-initialization event.
        /// </summary>
        /// <param name="e">
        /// The event arguments for the page pre-initialization.
        /// </param>
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            // don't load common.js from BasePage
            this.m_loadLOdefaultScripts = false;
            this.m_loadDefaultStylesheet = false;
        }

        /// <summary>
        /// Initializes the page by binding the dropdown list containing
        /// the customer codes of brokers for import.
        /// </summary>
        /// <param name="sender">
        /// The sender that triggered the page load.
        /// </param>
        /// <param name="e">
        /// The event arguments for the page load.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindBrokersToDDL();
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(this.Page_Load);
        }

        /// <summary>
        /// Binds the broker dropdown on the page to the list
        /// of possible brokers by customer code.
        /// </summary>
        private void BindBrokersToDDL()
        {
            var brokers = new List<ListItem>();

            var searchResult = BrokerDbSearch.Search(
                name: string.Empty,
                fannieUserId: string.Empty,
                isActive: null,
                customerCode: string.Empty,
                brokerId: Guid.Empty, 
                suiteType: null);

            foreach (var item in searchResult)
            {
                brokers.Add(new ListItem()
                {
                    Text = item.CustomerCode,
                    Value = item.BrokerId.ToString()
                });
            }

            this.BrokerDropdown.Items.Add(new ListItem()
            {
                Text = ManageModifiedLoans.BlankBrokerCode,
                Value = Guid.Empty.ToString()
            });

            foreach (var broker in brokers.OrderBy(li => li.Text))
            {
                this.BrokerDropdown.Items.Add(broker);
            }
        }
    }
}