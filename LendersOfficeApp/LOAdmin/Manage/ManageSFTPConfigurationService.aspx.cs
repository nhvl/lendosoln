﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using CommonProjectLib.Common;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.UI;
    using LendersOffice.XsltExportReport;

    /// <summary>
    /// Service page for the manage SFTP configuration page.
    /// </summary>
    public partial class ManageSFTPConfigurationService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Runs the service method called from the page.
        /// </summary>
        /// <param name="methodName">The method to run.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.LoadConfigurationById):
                    this.LoadConfigurationById();
                    break;
                case nameof(this.SaveConfiguration):
                    this.SaveConfiguration();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, $"Method not implemented {methodName}");
            }
        }

        /// <summary>
        /// Loads up the configuration by id.
        /// </summary>
        private void LoadConfigurationById()
        {
            int exportId = this.GetInt("ExportId");
            Guid brokerId = this.GetGuid("BrokerId");

            XsltExportTransferSettings config = XsltExportTransferSettings.Retrieve(brokerId, exportId);
            if (config != null)
            {
                SFTPConfigViewModel viewModel = config.ToViewModel();

                this.SetResult("Success", true);
                this.SetResult("SFTPConfigViewModel", SerializationHelper.JsonNetSerialize(viewModel));
            }
            else
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", "Unable to load SFTP configuration.");
            }
        }

        /// <summary>
        /// Saves the configuration.
        /// </summary>
        private void SaveConfiguration()
        {
            SFTPConfigViewModel viewModel = SerializationHelper.JsonNetDeserialize<SFTPConfigViewModel>(this.GetString("SFTPConfigViewModel"));

            List<string> errors;
            XsltExportTransferSettings config = XsltExportTransferSettings.CreateFromViewModel(viewModel, out errors);
            if (config == null)
            {
                this.SetResult("Errors", string.Join(Environment.NewLine, errors));
                this.SetResult("Success", false);
                return;
            }

            bool isNew = config.IsNewObject;
            config.Save();

            this.SetResult("IsNew", isNew);
            this.SetResult("Success", true);
            this.SetResult("ExportName", config.ExportName);
            this.SetResult("MapName", config.MapName);
            this.SetResult("UserId", config.UserId);
            this.SetResult("ReportId", config.ReportId);
            this.SetResult("Protocol", config.TransferOptions.Protocol.ToString());
            this.SetResult("ExportId", config.ExportId);
        }
    }
}