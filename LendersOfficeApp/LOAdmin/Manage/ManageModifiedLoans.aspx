﻿<%@ Page Title="Manage Modified Loan Files" Language="C#" MasterPageFile="~/LOAdmin/loadmin.Master" AutoEventWireup="true" CodeBehind="ManageModifiedLoans.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageModifiedLoans" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" runat="Server">
    <style type="text/css">
        .modal-dialog {
            width: 350px;
        }
    </style>
    <script type="text/javascript">
        function noBrokerSelected() {
            return $(<%=AspxTools.JsGetElementById(this.BrokerDropdown)%>).val() === 
                <%=AspxTools.JsString(Guid.Empty)%>;
        }

        function validateOptions(includeLoanIds) {
            var errorMsg = "";

            if ($("#AppCodeDropdown").val() === "") {
                errorMsg += "You must specify an app code.\n";
            }

            if (includeLoanIds && $.trim($("#LoanIds").val()) === "")
            {
                errorMsg += "You must specify at least one loan to mark as modified.\n";
            }

            if (errorMsg === "")
            {
                return true;
            }
            else
            {
                alert(errorMsg);

                return false;
            }
        }

        function callWebMethod(methodName, parameters, successCallback, errorCallback) {
            $("input,select,textarea").prop("disabled", true);

            var settings = {
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'ManageModifiedLoans.aspx/' + methodName,
                data: JSON.stringify(parameters),
                dataType: 'json',
                async: true,
                error: function(XMLHttpRequest, textStatus, errorThrown) { errorCallback(XMLHttpRequest, textStatus, errorThrown); },
                success: function(result) { successCallback(result); },
                complete: function() { $("input,select,textarea").prop("disabled", false); }
            };

            callWebMethodAsync(settings);
        }

        function updateAppCodesCallback(result) {
            var appCodes = JSON.parse(result.d);

            $("#NoAppCodesWarningRow,#AppCodesDropdownRow,#ViewDiv,#UpdateDiv,#ClearDiv,#SearchResultsRow,#UpdateResultsRow").hide();

            if (appCodes.length === 0) {
                $("#NoAppCodesWarningRow").show();
            }
            else {
                var appCodeDropdown = $("#AppCodeDropdown");

                for (var i = 0; i < appCodes.length; i++) {
                    appCodeDropdown.append(
                        $("<option></option>")
                        .attr("class", "app-code-option")
                        .attr("value", appCodes[i].Id)
                        .text(appCodes[i].Name));
                }

                $("#AppCodesDropdownRow").show();

                var checkedManageType = $('input[type="radio"][name="ManageType"]:checked').val();
                

                $("#ViewDiv").toggle(checkedManageType === 'View');

                $("#UpdateDiv").toggle(checkedManageType === 'Update');

                $("#ClearDiv").toggle(checkedManageType === 'Clear');
            }
        }

        function updateAppCodes(brokerId) {
            var parameters = { brokerId: brokerId };

            var errorCallback = function(XMLHttpRequest, textStatus, errorThrown) { 
                $("#AppCodesDropdownRow,#NoAppCodesWarningRow,#ViewDiv,#UpdateDiv,#ClearDiv").hide();

                alert(JSON.parse(XMLHttpRequest.responseText)["Message"]);
            }

            callWebMethod('RetrieveAppCodes', parameters, updateAppCodesCallback, errorCallback)
        }

        function searchCallback(result) {
            var modifiedLoans = JSON.parse(result.d);

            $("#SearchResultsCount").text(modifiedLoans.length);
                    
            var resultsTable = $("#SearchResultsTable tbody");

            for (var i = 0; i < modifiedLoans.length; i++) {
                resultsTable.append($("<tr></tr>").prop("class", "search-result-row")
                    .append($("<td></td>").text(modifiedLoans[i].LoanId))
                    .append($("<td></td>").text(modifiedLoans[i].LoanName))
                    .append($("<td></td>").text(modifiedLoans[i].LastModifiedDate))
                    .append($("<td></td>").text(modifiedLoans[i].OldLoanName))
                    .append($("<td></td>").text(modifiedLoans[i].BorrowerName))
                    .append($("<td></td>").text(modifiedLoans[i].LoanStatus))
                    .append($("<td></td>").text(modifiedLoans[i].IsValid)));
            }

            $("#SearchResultsRow").show();
        }

        function onSearchClick() {
            if (!validateOptions(false)) {
                return;
            }

            $("#SearchResultsCount").text("Searching...");

            $(".search-result-row").remove();

            var parameters = {
                brokerId: $(<%=AspxTools.JsGetElementById(this.BrokerDropdown)%>).val(),
                appCodeId: $("#AppCodeDropdown").val()
            };

            var errorCallback = function(XMLHttpRequest, textStatus, errorThrown) { 
                $("#ViewDiv,#UpdateDiv").hide();

                alert(JSON.parse(XMLHttpRequest.responseText)["Message"]);
            }

            callWebMethod('Search', parameters, searchCallback, errorCallback);
        }

        function updateCallback(result) {
            $("#UpdateResultsDiv").hide();

            var modifiedLoans = JSON.parse(result.d);
            displayLoanResult(modifiedLoans);
        }

        function displayLoanResult(modifiedLoans) {                   
            var resultsTable = $("#UpdateResultsTable tbody");

            for (var i = 0; i < modifiedLoans.length; i++) {
                resultsTable.append($("<tr></tr>").prop("class", "update-result-row")
                    .append($("<td></td>").text(modifiedLoans[i].LoanId))
                    .append($("<td></td>").text(modifiedLoans[i].LoanName))
                    .append($("<td></td>").text(modifiedLoans[i].UpdateStatus))
                    .append($("<td></td>").text(modifiedLoans[i].UpdateStatusLog)));
            }

            $("#UpdateResultsTable").show()
            $("#UpdateResultsRow").show();
        }

        function onUpdateClick() {
            if (!validateOptions(true)) {
                return;
            }

            $(".update-result-row").remove();

            var parameters = {
                brokerId: $(<%=AspxTools.JsGetElementById(this.BrokerDropdown)%>).val(),
                appCodeId: $("#AppCodeDropdown").val(),
                loanList: $("#LoanIds").val(),
                useLoanName: $("#LoanIdType").val() === 'Name'
            };

            var errorCallback = function(XMLHttpRequest, textStatus, errorThrown) { 
                $("#ViewDiv,#UpdateDiv").hide();

                alert(JSON.parse(XMLHttpRequest.responseText)["Message"]);
            }

            var successCallback = function(result) { updateCallback(result); };

            callWebMethod('Update', parameters, successCallback, errorCallback);
        }

        function onUpdateInBatchClick() {
            if (!validateOptions(true)) {
                return;
            }

            $(".update-result-row").remove();
            $("#UpdateResultsRow").hide();

            var parameters = {
                brokerId: $(<%=AspxTools.JsGetElementById(this.BrokerDropdown)%>).val(),
                appCodeId: $("#AppCodeDropdown").val(),
                loanList: $("#LoanIds").val(),
                useLoanName: $("#LoanIdType").val() === 'Name'
            }

            var errorCallback = function(XMLHttpRequest, textStatus, errorThrown) { 
                $("#ViewDiv,#UpdateDiv").hide();

                alert(JSON.parse(XMLHttpRequest.responseText)["Message"]);
            }

            var successCallback = function(result) { updateAtOnceCallback(result) };

            callWebMethod('UpdateInBatch', parameters, successCallback, errorCallback);
        }

        function onClearClick() {
            if (!validateOptions(false)) {
                return;
            }

            $(".update-result-row").remove();
            $("#UpdateResultsRow").hide();

            var parameters = {
                brokerId: $(<%=AspxTools.JsGetElementById(this.BrokerDropdown)%>).val(),
                appCodeId: $("#AppCodeDropdown").val(),
                lastModified: new Date($("#LastModifiedDate").val()).toISOString()
            }

            var errorCallback = function(XMLHttpRequest, textStatus, errorThrown) { 
                $("#ViewDiv,#UpdateDiv").hide();

                alert(JSON.parse(XMLHttpRequest.responseText)["Message"]);
            }

            var successCallback = function(result) { 
                $("#ClearResult div").html(result.d);

                $("#ClearResultsRow").show();
            };

            callWebMethod('ClearModifiedList', parameters, successCallback, errorCallback);
        }

        function updateAtOnceCallback(result) {
            var msg = result.d.Message;
            var invalidLoans = JSON.parse(result.d.InvalidLoans);
            if( invalidLoans.length > 0) {
                msg += "<br/><b>" + invalidLoans.length + " non-existent loan(s)</b><br/>"
            }

            var resultsDiv = $("#UpdateResultsDiv");

            resultsDiv.html("NOTE: If this update was made while a process (like SendDataToClientDB) was running, it may not have worked completely. <br/>" + msg);

            $("#UpdateResultsTable").hide();
            $("#UpdateResultsRow").show();
            resultsDiv.show();

            if( invalidLoans.length > 0) {
                displayLoanResult(invalidLoans);
            }
        }

        function lastModifiedNowClick() {
            $("#LastModifiedDate").val(new Date());
        }

        $(document).ready(function() {
            $("#AppCodesDropdownRow,#NoAppCodesWarningRow,#ViewDiv,#UpdateDiv,#ClearDiv").hide();

            $('input[type="radio"][name="ManageType"]').change(function() {
                if (noBrokerSelected() || $("#NoAppCodesWarningRow").is(":visible")) {
                    return;
                }

                if (this.value === 'View') {
                    $("#ViewDiv").show();
                    $("#UpdateDiv,#ClearDiv,#SearchResultsRow,#ClearResultsRow").hide();
                }
                else if (this.value === 'Clear') {
                    $("#ClearDiv").show();
                    $("#ViewDiv,#UpdateDiv,#SearchResultsRow,#UpdateResultsRow").hide();
                }
                else if (this.value === 'Update') {
                    $("#UpdateDiv").show();
                    $("#ViewDiv,#ClearDiv,#UpdateResultsRow,#ClearResultsRow").hide();
                }
            });

            $(<%=AspxTools.JsGetElementById(this.BrokerDropdown)%>).change(function() {
                $(".app-code-option").remove();

                if (noBrokerSelected()) {
                    $("#AppCodesDropdownRow,#NoAppCodesWarningRow,#ViewDiv,#UpdateDiv,#ClearDiv").hide();
                }
                else {
                    updateAppCodes($(this).val());
                }
            });

            $("#LastModifiedDate").val(new Date().toString());

            $('#MarkAllLoansBtn').click(function() {
                if (validateOptions(false)) {
                    $('#MarkAllLoans_ValidityType').val('Valid');
                    $('#MarkAllLoansModal').modal('show');
                }
            });

            $('#MarkAllLoans_StartBtn').click(function() {
                $('#MarkAllLoansModal').modal('hide');
                $(".update-result-row").remove();
                $("#UpdateResultsRow").hide();

                var parameters = {
                    brokerId: $(<%=AspxTools.JsGetElementById(this.BrokerDropdown)%>).val(),
                    appCodeId: $("#AppCodeDropdown").val(),
                    markValidLoans: $('#MarkAllLoans_ValidityType').val() === 'Valid'
                }

                var errorCallback = function(XMLHttpRequest, textStatus, errorThrown) { 
                    $("#ViewDiv,#UpdateDiv").hide();

                    alert(JSON.parse(XMLHttpRequest.responseText)["Message"]);
                }

                var successCallback = function(result) { updateAtOnceCallback(result) };

                callWebMethod('AddAllFiles', parameters, successCallback, errorCallback);
            });
        });
    </script>
    <div class="panel panel-default">
        <form runat="server">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-9">
                        <div class="form-group text-center">
                            <input type="radio" name="ManageType" value="View" checked="checked" />&nbsp; View
                            &nbsp;
                            <input type="radio" name="ManageType" value="Update" />&nbsp; Update
                            &nbsp;
                            <input type="radio" name="ManageType" value="Clear" />&nbsp; Clear
                        </div>
                    </div>

                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            Select broker by customer code:
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="form-group text-center">
                            <asp:DropDownList runat="server" ID="BrokerDropdown" AutoPostBack="false"
                                CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>
                </div>

                <div class="row" id="NoAppCodesWarningRow">
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-10">
                        <div class="form-group form-group-sm text-center">
                            <p class="text-danger">No app codes were found for the specified broker.</p>
                        </div>
                    </div>

                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>
                </div>

                <div class="row" id="AppCodesDropdownRow">
                        <div class="col-sm-1">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                Select an app code:
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-group text-center">
                                <select id="AppCodeDropdown" class="form-control">
                                    <option value="">&lt;-- Select an App Code --&gt;</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                <div id="ViewDiv">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-group text-center">
                                <input type="button" class="btn btn-primary" onclick="onSearchClick();" value="Search Modified Loans"/> 
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="row" id="SearchResultsRow">
                        <div class="col-sm-1">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>

                        <div class="col-sm-10">
                            <p class="text-center">Total number of modified loans: <span id="SearchResultsCount">&nbsp;</span></p>
                            <br />
                            <table id="SearchResultsTable" class="table table-condensed table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-left">Loan ID</th>
                                        <th class="text-left">Loan Name</th>
                                        <th class="text-left">Last Modified Date</th>
                                        <th class="text-left">Old Loan Name</th>
                                        <th class="text-left">Borrower Name</th>
                                        <th class="text-left">Loan Status</th>
                                        <th class="text-left">Valid?</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>

                <div id="UpdateDiv">
                    <div class="row">
                        <div class="col-sm-1">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                Select type of loan identifier:
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-group text-center">
                                <select id="LoanIdType" class="form-control">
                                    <option value="Id">Loan ID</option>
                                    <option value="Name">Loan Name</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                Enter loan identifiers on separate lines:
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-group text-center">
                                <textarea id="LoanIds" class="form-control" rows="25" cols="50"></textarea>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-group text-center">
                                <input type="button" class="btn btn-primary" onclick="onUpdateClick();" value="Mark Selected Loans As Modified"/> 
                                <input type="button" class="btn btn-primary" onclick="onUpdateInBatchClick();" value="Mark Loans As Modified (Batch)" title="Use this if the other &quot;Mark Selected Loans As Modified&quot; button doesn't work. May not work well if the app is currently running."/> 
                            </div>
                            <div class="form-group text-center">
                                <input type="button" class="btn btn-danger" id="MarkAllLoansBtn" value="Mark ALL of Lender's Loans As Modified" title="May not work well if the associated app is currently running."/>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="row" id="UpdateResultsRow">
                        <div class="col-sm-1">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>

                        <div class="col-sm-10">
                            <div id="UpdateResultsDiv" class="form-group">
                            </div>

                            <table id="UpdateResultsTable" class="table table-condensed table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-left">Loan ID</th>
                                        <th class="text-left">Loan Name</th>
                                        <th class="text-left">Status</th>
                                        <th class="text-left">Additional Log Data</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ClearDiv">
                    <div class="row">
                        <div class="col-sm-1">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                Clear modified files older than this date:
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group text-center">
                                <input type="date" id="LastModifiedDate" class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="form-group text-center">
                                <input type="button" id="LastModifiedDateNow" onclick="lastModifiedNowClick()" value="Now" class="btn btn-primary"/>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class ="row">
                        <div class="col-sm-5">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group text-center">
                                <input type="button" class="btn btn-primary" onclick="onClearClick();" value="Clear Loans From Modified List"/> 
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="row" id="ClearResultsRow">
                        <div class="col-sm-5">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                        <div class="col-sm-5" id="ClearResult">
                            <div class="form-group text-center">
                                &nbsp;
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group form-group-sm">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="MarkAllLoansModal" class="modal v-middle fade Configuration" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Mark All Loans</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-inline">
                                <label class="input-label">Mark ALL&nbsp;</label>
                                <select ID="MarkAllLoans_ValidityType" class="form-control">
                                    <option value="Valid">Valid</option>
                                    <option value="Invalid">Invalid</option>
                                </select>
                                <label class="input-label">&nbsp;loans as modified.</label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <span class="clipper">
                                <input type="button" class="btn btn-danger" id="MarkAllLoans_StartBtn" value="Start" />
                                <input type="button" class="btn btn-default CancelBtn" id="MarkAllLoans_CancelBtn" data-dismiss="modal" value="Cancel"/>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </form>
     </div>
</asp:Content>
