﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Linq;
    using EDocs;
    using LendersOffice.Common;

    /// <summary>
    /// Provides methods for the "Manage OCR Document Types" page.
    /// </summary>
    public partial class ManageOcrDocTypesService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Processes the requested method.
        /// </summary>
        /// <param name="methodName">
        /// The name of the method to process.
        /// </param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.GetViewModel):
                    this.GetViewModel();
                    break;
                case nameof(this.AddDocType):
                    this.AddDocType();
                    break;
                case nameof(this.EditDocType):
                    this.EditDocType();
                    break;
                case nameof(this.DeleteDocType):
                    this.DeleteDocType();
                    break;
                default:
                    throw new InvalidOperationException(methodName);
            }
        }

        /// <summary>
        /// Gets the view model for the page.
        /// </summary>
        private void GetViewModel()
        {
            var classifications = EdocClassification.GetSystemClassifications().Values;
            this.SetResult("ViewModel", SerializationHelper.JsonNetAnonymousSerialize(classifications));
        }

        /// <summary>
        /// Adds a new classification.
        /// </summary>
        private void AddDocType()
        {
            var name = this.GetString("Name");
            EdocClassification.AddClassification(name);
            this.GetViewModel();
        }

        /// <summary>
        /// Edits an existing classification.
        /// </summary>
        private void EditDocType()
        {
            var id = this.GetInt("Id");
            var name = this.GetString("NewName");
            var wasEdited = EdocClassification.EditClassification(id, name);

            this.SetResult("Result", wasEdited);
            if (wasEdited)
            {
                this.GetViewModel();
            }
        }

        /// <summary>
        /// Deletes a classification.
        /// </summary>
        private void DeleteDocType()
        {
            var id = this.GetInt("Id");
            var wasDeleted = EdocClassification.DeleteClassification(id);

            this.SetResult("WasDeleted", wasDeleted);
            if (wasDeleted)
            {
                this.GetViewModel();
            }
        }
    }
}