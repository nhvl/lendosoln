﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageThirdPartyClientCertificates.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageThirdPartyClientCertificates"
   MasterPageFile="~/LOAdmin/loadmin.Master"
      %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content  ContentPlaceHolderID="Main" runat="server">
        <style type="text/css">
            .modal-dialog {
                width: 800px;
            }

                .modal-dialog .dialog-input {
                    width: 250px;
                }

            .form-inline .MiniInput {
                width: 60px;
            }

            .form-inline .LargeInput {
                width: 400px;
            }

            .form-inline .SuperLargeInput {
                width: 500px;
            }

            .modal-dialog .form-inline {
                margin: 3px;
                min-height: 34px;
            }

            .input-label {
                width: 20%;
            }

            .BaseCopy {
                display: none;
            }

            table.table-striped tr.invalid:nth-of-type(2n+1) {
                background-color: #f9dada;
            }

            table.table-striped tr.invalid {
                background-color: #ffe0e0;
            }

            .hidden {
                display: none;
            }
        </style>
    <script type="text/javascript">
        jQuery(function ($) {
            var uploadPopup = $('#AddNewThirdPartyCertificatePopup');

            if ($('#UploadError').length > 0) {
                $('#UploadErrorMessage').removeClass('hidden').text($('#UploadError'));
                uploadPopup.modal('show');
            }

            function ClearInputs(targetSelector) {
                $(targetSelector).find(':input').not('.btn').val('');
            }

            $('#UploadThirdPartyCertificate').click(function () {
                $('#AddNewThirdPartyCertificatePopup').modal('show');
                ClearInputs('#AddNewThirdPartyCertificatePopup');
            });

            $('a.associate').click(function () {
                ClearInputs('#AssociateURLModal');

                var id = $(this).closest('tr').attr('data-id');
                $('#AssociateURLModal').modal('show');
                $('input.association-cert-id').val(id);
            });

            $('a.url-delete').click(function () {
                var associationId = $(this).attr('data-id');

            });
        });
    </script>
    <form id="form1" runat="server">
    <div>
        <div class="container-fluid">
            <div class="row">
                <div class="center-div">
            

                    <div class="panel panel-default" id="ConfigPanel">
                        <div class="panel-body div-wrap-tb">
                            <table id="ConfigTable" class="table table-striped table-condensed table-layout-fix">
                                <thead>
                                    <tr>
                                        <th>Description</th>
                                        <th>Subject</th>
                                        <th>Issuer</th>
                                        <th>Uploaded Date</th>
                                        <th>Valid Range</th>
                                        <th>Applicable URLs</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater runat="server" ID="Configurations" OnItemDataBound="Configurations_ItemDataBound">
                                        <ItemTemplate>
                                            <tr runat="server" id="CertificateRow" >
                                                <td>
                                                    <ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                                                    <ml:EncodedLiteral runat="server" ID="Subject"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                                                    <ml:EncodedLiteral runat="server" ID="Issuer"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                                                    <ml:EncodedLiteral runat="server" ID="CreatedDate"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                                                    <ml:EncodedLiteral runat="server" ID="ValidRange"></ml:EncodedLiteral>
                                                </td>
                                                <td>
                                                    <ul>
                                                    <asp:Repeater runat="server" ID="CertUrlsRepeater" OnItemDataBound="CertUrlsRepeater_ItemDataBound">
                                                        <ItemTemplate>
                                                            <li>
                                                                <ml:EncodedLiteral runat="server" id="CertURL"></ml:EncodedLiteral> (<asp:LinkButton runat="server" ID="deleteAssociation" onclick="DeleteAssociation_onClick">delete</asp:LinkButton>)
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                        </ul>
                                                </td>
                                                <td>
                                                    <a class="associate">associate url</a>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                                    <asp:PlaceHolder runat="server" ID="ErrorMessagePlaceHolder" Visible="false">
                    <div class="alert alert-danger">
                        <strong>Error</strong>&nbsp; <ml:EncodedLiteral runat="server" id="ErrorMessage"></ml:EncodedLiteral>
                    </div>
                    </asp:PlaceHolder>
                            <span class="clipper">
                                <button type="button" class="btn btn-primary" id="UploadThirdPartyCertificate">Upload new Third Party Certificate</button>
                            </span>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="AddNewThirdPartyCertificatePopup" class="modal v-middle fade Configuration" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Third Party Certificate</h4>
                </div>
                <div class="modal-body">
                    <div class="form-inline">
                        <label class="input-label">Description</label>
                        <input type="text" id="Description" runat="server" class="dialog-input form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Password</label>
                        <input type="password" id="Password"  runat="server" class="LargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">PFX Certificate</label>
                        <input type="file" id="certificateFile"  runat="server" class="LargeInput form-control" />
                    </div>
             
                </div>
                <div class="modal-footer">
                    <span class="clipper">
                        <input type="button" class="btn btn-default" id="CancelBtn" data-dismiss="modal" value="Cancel"/>
                    </span>
                    <span class="clipper">
                        <asp:Button runat="server" ID="AddNewThirdPartyCert" OnClick="AddNewThirdPartyCert_OnClick"  Text="Create" CssClass="btn btn-primary"/>
                    </span>
                </div>
            </div>
        </div>
    </div>

        <div id="AssociateURLModal" class="modal v-middle fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Associate URL </h4>
                </div>
                <div class="modal-body">
                    <div class="form-inline">
                        <label class="input-label">URL</label>
                        <input type="text" id="associationUrl" runat="server" class="dialog-input form-control" />
                        <input type="hidden" id="certificateId" class="association-cert-id" runat="server"  />
                    </div>
                </div>
                <div class="modal-footer">
                    <span class="clipper">
                        <input type="button" class="btn btn-default"  data-dismiss="modal" value="Cancel"/>
                    </span>
                    <span class="clipper">
                        <asp:Button runat="server" ID="AssociateUrl" OnClick="AssociateUrl_OnClick"  Text="Associate" CssClass="btn btn-primary"/>
                    </span>
                </div>
            </div>
        </div>
    </div>
    </form>
</asp:Content>