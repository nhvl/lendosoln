﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using LendersOffice.ObjLib.Conversions.Flood;
    using LendersOffice.Security;

    /// <summary>
    /// Manage Flood Re-sellers.
    /// </summary>
    public partial class ManageFloodResellers : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Permission used for this page.
        /// </summary>
        private static readonly E_InternalUserPermissions[] RequiredPermissions = { E_InternalUserPermissions.EditIntegrationSetup };

        /// <summary>
        /// Gets the style sheet.
        /// </summary>
        /// <value>The style sheet.</value>
        public override string StyleSheet
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get all flood resellers.
        /// </summary>
        /// <returns>Return a list of flood resellers.</returns>
        [System.Web.Services.WebMethod]
        public static IEnumerable<FloodReseller> GetFloodResellers()
        {
            return FloodReseller.ListAll().Values.OrderBy(item => item.Name, StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Create new flood reseller.
        /// </summary>
        /// <param name="id">Flood reseller id.</param>
        /// <param name="name">Flood reseller name.</param>
        /// <param name="url">Flood reseller service url.</param>
        [System.Web.Services.WebMethod]
        public static void AddNewFloodReseller(string id, string name, string url)
        {
            Guid floodResellerId = string.IsNullOrEmpty(id) ? Guid.NewGuid() : new Guid(id);
            FloodReseller.Create(floodResellerId, name, url);
        }

        /// <summary>
        /// Update Flood Reseller information to database.
        /// </summary>
        /// <param name="id">Flood reseller id.</param>
        /// <param name="name">Flood reseller name.</param>
        /// <param name="url">Flood reseller service url.</param>
        [System.Web.Services.WebMethod]
        public static void UpdateFloodReseller(Guid id, string name, string url)
        {
            FloodReseller.Update(id, name, url);
        }

        /// <summary>
        /// Return this page's required permissions set. This test occurs with each page initialization.
        /// </summary>
        /// <returns>
        /// Array of required permissions.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return RequiredPermissions;
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// The Page_Init function.
        /// </summary>
        /// <param name="sender">Control that calls the function.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterJsScript("angular-1.5.5.min.js");
        }
    }
}