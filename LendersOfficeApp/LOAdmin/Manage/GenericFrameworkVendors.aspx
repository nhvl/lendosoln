﻿<%@ Page language="C#" Codebehind="GenericFrameworkVendors.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Manage.GenericFrameworkVendors"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Generic Framework Vendors - LendingQB" %>

<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <div class="ng-cloak" ng-app="app">
        <vendor-manager></vendor-manager>
    </div>

    <form runat="server"></form>
</asp:Content>
