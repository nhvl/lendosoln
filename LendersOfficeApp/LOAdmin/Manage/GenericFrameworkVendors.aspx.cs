﻿using System;
using System.Linq;
using LendersOffice.Common;
using LendersOffice.Integration.GenericFramework;
using System.Web.Services;
using System.Xml;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class GenericFrameworkVendors : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Saves the data associated with the vendor to the Generic_Framework_Vendor_Config table. Creates a new vendor if providerID is unset.
        /// </summary>
        /// <param name="providerID">The provider ID for the vendor. Null or empty will result in the creation of a new vendor.</param>
        /// <param name="name">The name of the vendor.</param>
        /// <param name="launchURL">The launch url of the vendor.</param>
        /// <param name="lenderCredential">The credential xml format for the vendor.</param>
        /// <param name="includeUsername">Indicates whether user-level authentication is allowed for this vendor.</param>
        /// <param name="serviceType">Indicates what service this vendor provides.</param>
        /// <param name="loanIdentifierTypeToSend">Indicates the loan identifier the vendor will consume.</param>
        /// <returns>The provider ID of the saved vendor.</returns>
        [WebMethod]
        public static object SaveVendor(string providerID, string name, string launchURL, string lenderCredential, bool includeUsername, TypeOfService serviceType, TypeOfLoanIdentifier loanIdentifierTypeToSend)
        {
            string errorMessage = null;
            try
            {
                var vendor = GenericFrameworkVendor.SaveSystemLevelVendor(providerID, name, launchURL, lenderCredential, includeUsername, serviceType, loanIdentifierTypeToSend);
                providerID = vendor.ProviderID;
            }
            catch (XmlException exc)
            {
                errorMessage = exc.Data.Contains("ErrorMessage") ? exc.Data["ErrorMessage"].ToString() : exc.Message;
            }

            return new { ProviderID = providerID, Error = errorMessage };
        }

        [WebMethod]
        public static object GetVendors()
        {
            try
            {
                return GenericFrameworkVendor.LoadAllVendors()
                    .Select(v => new GenericFrameworkVendorData(v))
                    //.ToArray()
                    ;
            }
            catch (XmlException exc)
            {
                return exc.Data.Contains("ErrorMessage") ? exc.Data["ErrorMessage"].ToString() : exc.Message;
            }
        }

        [WebMethod]
        public static EnumListItem<TypeOfService>[] GetServiceTypeList()
        {
            return ServiceTypeListItems;
        }

        [WebMethod]
        public static EnumListItem<TypeOfLoanIdentifier>[] GetLoanIdentifierTypeList()
        {
            return LoanIdentifierTypeListItems;
        }

        /// <summary>
        /// Used to lazy load <see cref="ServiceTypeListItems"/>.<para/>
        /// Do not access directly.
        /// </summary>
        private static EnumListItem<TypeOfService>[] serviceTypeListItems;


        /// <summary>
        /// The options for the drop down lists to select <see cref="GenericFrameworkVendor.ServiceType"/> for each vendor.
        /// </summary>
        protected static EnumListItem<TypeOfService>[] ServiceTypeListItems => serviceTypeListItems ?? (serviceTypeListItems = EnumListItem<TypeOfService>.GetListOfEnum());


        /// <summary>
        /// Used to lazy load <see cref="ServiceTypeListItems"/>.<para/>
        /// Do not access directly.
        /// </summary>
        private static EnumListItem<TypeOfLoanIdentifier>[] loanIdentifierTypeListItems;


        /// <summary>
        /// The options for the drop down lists to select <see cref="GenericFrameworkVendor.ServiceType"/> for each vendor.
        /// </summary>
        protected static EnumListItem<TypeOfLoanIdentifier>[] LoanIdentifierTypeListItems => loanIdentifierTypeListItems ?? (loanIdentifierTypeListItems = EnumListItem<TypeOfLoanIdentifier>.GetListOfEnum());


        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterResources();
        }

        protected void Page_Init(object sender, EventArgs e)
        {

        }

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.Manage.GenericFrameworkVendors.js");
        }
        public override string StyleSheet => string.Empty;

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
    }

    public class EnumListItem<T> where T : struct, IConvertible
    {
        public T      Value { get; set; }
        public string Text  { get; set; }

        public static EnumListItem<T>[] GetListOfEnum()
        {
            return Enum.GetValues(typeof(T)).Cast<T>()
                    .Select(x => new EnumListItem<T>
                    {
                        Text = Utilities.ConvertEnumToString(x as Enum),
                        Value = x,
                    })
                    .ToArray();
        }
    }
    public class GenericFrameworkVendorData
    {
        public string CredentialXML  { get; set; }
        public bool IncludeUsername  { get; set; }
        public string LaunchURL  { get; set; }
        public string Name  { get; set; }
        public string ProviderID  { get; set; }
        public TypeOfService ServiceType  { get; set; }
        public TypeOfLoanIdentifier LoanIdentifierTypeToSend { get; set; }

        public GenericFrameworkVendorData()
        {

        }

        public GenericFrameworkVendorData(GenericFrameworkVendor v)
        {
            this.CredentialXML = v.CredentialXML?.ToString() ?? string.Empty;
            this.IncludeUsername = v.IncludeUsername;
            this.LaunchURL = v.LaunchURL;
            this.Name = v.Name;
            this.ProviderID = v.ProviderID;
            this.ServiceType = v.ServiceType;
            this.LoanIdentifierTypeToSend = v.LoanIdentifierTypeToSend;
        }
    }
}
