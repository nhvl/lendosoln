using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Monitor;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Admin
{
	/// <summary>
	/// Summary description for TrackPanel.
	/// </summary>

	public partial class TrackPanel : LendersOffice.Admin.SecuredAdminPage
	{

		private PostbackTracker m_Pbt = new PostbackTracker();

		private String ErrorMessage
		{
			// Write out message.

			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		/// <summary>
		/// Every admin page that inherits from this base page
		/// should implement this query method to specify the
		/// permission set required to access this page.
		/// </summary>
		/// <returns>
		/// List of required permissions.
		/// </returns>

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return an empty set.  Overload to provide
			// a page specific permission list.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsDevelopment
			};
		}

		/// <summary>
		/// Render this page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			try
			{
				// Show control panel latest.

				m_HistoryCount.Text = m_Pbt.HistoryCount.ToString();
				m_MaxEntries.Text   = m_Pbt.MaxEntries.ToString();
				m_StartedOn.Text    = m_Pbt.StartedOn.ToString();

				m_MemoryUsed.Text = System.GC.GetTotalMemory( false ).ToString( "#,#" ) + " bytes";

				m_ServerClock.Text = DateTime.Now.ToString();

				// Fill out contents.

				BindGrid();

				// Turn off history if empty.

				if( m_History.Items.Count == 0 )
				{
					m_History.Visible   = false;
					m_EmptyNote.Visible = true;
				}
				else
				{
					m_EmptyNote.Visible = false;
					m_History.Visible   = true;
				}
			}
			catch( Exception e )
			{
				ErrorMessage = "Failed to prerender web form: " + e.Message + ".";

				Tools.LogError( e );
			}
		}

		/// <summary>
		/// Initialize page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			try
			{
				// Fill out contents.

				if( IsPostBack == false )
				{
					m_Limit.Text = "360";
				}
			}
			catch( Exception e )
			{
				ErrorMessage = "Failed to load web form: " + e.Message + ".";

				Tools.LogError( e );
			}
		}

		/// <summary>
		/// Fill out contents.
		/// </summary>

		private void BindGrid()
		{
			// Bind our events to a dynamic data view.
			// If we have any filters, we can insert
			// them here.

			DataView dView;
			
			if( m_Limit.Text != "" )
			{
				// Get argument and generate filter that depends on whether
				// or not the argument is a count.

				String sFilter = "LoginName LIKE '" + m_Limit.Text.TrimWhitespaceAndBOM() + "%'";

				try
				{
					sFilter = "Age <= " + Convert.ToInt32( m_Limit.Text );
				}
				catch
				{
				}

				dView = ViewGenerator.Generate( m_Pbt.Entries , sFilter );
			}
			else
			{
				dView = ViewGenerator.Generate( m_Pbt.Entries );
			}

			if( m_Current.Text != "" )
			{
				// 4/1/2005 kb - Show the history of the user in its own
				// grid: case #1513.

				m_History.DataSource = m_Pbt[ new Guid( m_Current.Text ) ];
				m_History.DataBind();
			}

			m_Count.Text = "Count = " + dView.Count;

			m_Grid.DataSource = dView;
			m_Grid.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.PreRender += new System.EventHandler(this.PagePreRender);
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

		/// <summary>
		/// Handle Grid bind.
		/// </summary>

		protected void GridItemBound( object sender , System.Web.UI.WebControls.DataGridItemEventArgs a )
		{
			// Hook up link button commands to their ids.

			try
			{
				if( DataBinder.Eval( a.Item.DataItem , "UserId" ) != null )
				{
					LinkButton lCmd; String uArg = DataBinder.Eval( a.Item.DataItem , "UserId" ).ToString();

					lCmd = a.Item.FindControl( "Drop" ) as LinkButton;

					if( lCmd != null )
					{
						lCmd.CommandArgument = uArg;
					}

					lCmd = a.Item.FindControl( "View" ) as LinkButton;

					if( lCmd != null )
					{
						lCmd.CommandArgument = uArg;
					}

					if( uArg == m_Current.Text )
					{
						a.Item.BackColor = Color.LightGoldenrodYellow;
					}
				}
			}
			catch( Exception e )
			{
				// Oops!

				ErrorMessage = "Failed to handle item command: " + e.Message + ".";

				Tools.LogError( e );
			}
		}

		/// <summary>
		/// Handle Grid event.
		/// </summary>

		protected void GridItemClick( object sender , System.Web.UI.WebControls.DataGridCommandEventArgs a )
		{
			// Fire off event using message id as argument.
			//
			// 4/1/2005 kb - We support dropping and viewing.  The view
			// command will select the user for history dumping: case #1513.

			try
			{
				Guid uArg = new Guid( a.CommandArgument.ToString() );

				switch( a.CommandName.ToLower() )
				{
					case "view":
					{
						m_Current.Text = uArg.ToString();
					}
					break;

					case "drop":
					{
						m_Pbt.Drop( uArg );
					}
					break;
				}
			}
			catch( Exception e )
			{
				// Oops!

				ErrorMessage = "Failed to handle item command: " + e.Message + ".";

				Tools.LogError( e );
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void UpdateClick( object sender , System.EventArgs a )
		{
			// Set the values for delays and other settings
			// via the processor's proxy.

			try
			{
				m_Pbt.HistoryCount = Int32.Parse( m_HistoryCount.Text );
				m_Pbt.MaxEntries   = Int32.Parse( m_MaxEntries.Text );
			}
			catch( Exception e )
			{
				// Oops!

				ErrorMessage = "Failed to update: " + e.Message + ".";

				Tools.LogError( e );
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void ClearClick( object sender , System.EventArgs a )
		{
			// Use proxy to clear all records from the tables.

			try
			{
				m_Pbt.Clear();
			}
			catch( Exception e )
			{
				// Oops!

				ErrorMessage = "Failed to clear: " + e.Message + ".";

				Tools.LogError( e );
			}
		}

	}

}
