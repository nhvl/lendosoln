﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="CustomPDFFieldProperties.aspx.cs"
    Inherits="LendersOfficeApp.LOAdmin.Manage.CustomPDFFieldProperties" MasterPageFile="~/LOAdmin/loadmin.Master" %>

<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style type="text/css">
        /*Show loading animate*/
        .glyphicon-refresh-animate {
            -animation: spin .7s infinite linear;
            -ms-animation: spin .7s infinite linear;
            -webkit-animation: spinw .7s infinite linear;
            -moz-animation: spinm .7s infinite linear;
        }
 
        @keyframes spin {
            from { transform: scale(1) rotate(0deg);}
            to { transform: scale(1) rotate(360deg);}
        }
          
        @-webkit-keyframes spinw {
            from { -webkit-transform: rotate(0deg);}
            to { -webkit-transform: rotate(360deg);}
        }
         
        @-moz-keyframes spinm {
            from { -moz-transform: rotate(0deg);}
            to { -moz-transform: rotate(360deg);}
        }
        /******/
        .btn:focus,.btn:active  {
            outline: none !important;
        }
        .center-div
        {
            margin: 0 auto;
            min-width: 1200px;
        }
        .modal-width
        {
        	width:500px;
        }
        .padding-div
        {
            padding-top: 20px;
            padding-bottom: 10px;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            $("[id$='m_searchBox']").focus();
        });
        
        function onEditField(id) {
            $('#loadingModal').show();
            $('#popup_content').css('visibility','hidden');
			$('#popup_content').attr('src', './EditCustomPDFFieldProperties.aspx?id=' + id);
			$('#myModal').modal('show');
            return false;
        }
        function onDeleteField(id){
            var searchText=$("[id$='m_searchBox']").val();
            var category=$("[id$='m_category']").val();
            var data={searchText:searchText,category:category,fieldId:id};
            confirmDialog('Delete Field ID "' + id + '"?',data);
            return false;
        }
        function transformResponse(text) {
            try {
                return JSON.parse(text, function (key, value) {
                    if (typeof value == "string") {
                        var result, d;
                        // parse ASP.NET DateTime type
                        {
                            result = /\/Date\((-?[0-9]+)(?:[a-zA-Z]|(?:\+|-)[0-9]{4})?\)\//g.exec(value);
                            if (!!result) {
                                return new Date(parseFloat(result[1]));
                            }
                        }
                    }
                    return value;
                });
            } catch (e) {
                return text;
            }
        }
        function loadData(method,data){
            $('#loadingMain').show();
            $.ajax({
                type: "POST",
                url:window.location.pathname+"/"+method,
                data:JSON.stringify(data),
                contentType: "application/json; charset:utf-8",
                dataType: "json",
                converters: { "text json": transformResponse }
                    }).done(function (data, textStatus, jqXHR) {
                        var template=document.getElementById('template').innerHTML;
                         var headers = [
                            { key: "Id", value: "Field ID",css: "col-tb-5" },
                            { key: "FriendlyName", value: "Friendly Name",css: "col-tb-5" },
                            { key: "Category.Name", value: "Category",css: "col-tb-5" },
                            { key: "Description", value: "Description",css: "col-tb-5" },
                            { key: "IsReviewNeeded", value: "Review Completed", css: "col-xs-1" },
                            { key: "IsHidden", value: "Is Hidden", css: "col-xs-1" }
                        ];
                        setUp("#container",headers,data.d,template);
                        $('#loadingMain').hide();
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        console.error(arguments);
                        $('#loadingMain').hide();
                    });
        }
        function setUp(id, headers, loanData, template) {
            var ractive = new Ractive({
                el: id,
                template: template,
                data: {
                    sortByKey: '',
                    sortAsc: true,
                    headers: headers,
                    data: loanData
                }
            });
            ractive.on('sortBy', function (event, key) {
                var lastKey = ractive.get("sortByKey");
                var isAsc = ractive.get("sortAsc");
                if (key === lastKey) {
                    isAsc = !isAsc;
                } else {
                    isAsc = true;
                    lastKey = key;
                }
                var ls = loanData.slice().sort(function (x, y) {
                var u,v;
                if(key==='Category.Name'){
                    u = parseData(x['Category'].Id);
                    v = parseData(y['Category'].Id);
                }
                else{
                    u = parseData(x[key]);
                    v = parseData(y[key]);
                }
                    var x_Minus_y = ((u > v) ? 1 : ((u < v) ? -1 : 0));
                    return (isAsc ? x_Minus_y : (-x_Minus_y));
                });

                ractive.set({ sortByKey: lastKey, sortAsc: isAsc, data: ls });
            });
        }
        function search(){
            var searchText=$("[id$='m_searchBox']").val();
            var category=$("[id$='m_category']").val();
            var data={searchText:searchText,category:category};
            loadData("Search",data);
        }
        function resetForm(){
            $("[id$='m_searchBox']").val('');
            $("[id$='m_category']").val(-1);
        }
        function parseData(value) {
            if ($.isNumeric(value)) return parseFloat(value);
            return value;
        }
        //Call this function when get back from modal popup
        function callback(data, textStatus, jqXHR){
            if(data.d)
            {
                $('#myModal').modal('hide');
                search();
            }
            else
                alert(textStatus);
        }
        function OnLoadedModal(){
            $('#loadingModal').hide();
            $('#popup_content').css('visibility','visible');
        }
        function confirmDialog(message,data){
            $('#myConfirm .modal-body').html(message);
            $('#myConfirm #btnOK').click(function(){
                $('#myConfirm').modal('hide');
                loadData("Delete",data);
                return false;
            });
            $('#myConfirm').modal('show');
        }
        function saveField() {
            var fieldFrame = document.getElementById('popup_content');

            var fieldDocument = fieldFrame.contentDocument || fieldFrame.contentWindow.document;

            var saveButton = fieldDocument.getElementById('SaveButton');

            saveButton.click();

            $('#myModal').modal('hide');
        }
    </script>

    <div class="container-fluid">
        <div class="row-masonry">
            <div class="grid-sizer">
            </div>
            <div class="masonry-item center-div">
                <div class="panel panel-default">
                    <div class="panel-body div-wrap-tb">
                        <form id="form1" runat="server" class="form-label-noerror form-group-sm">
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-5">
                                <label>
                                    Search by friendly name or field ID:</label>
                                <asp:TextBox ID="m_searchBox" class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                            </div>
                            <div class="col-sm-3">
                                <label>
                                    Field Category:</label>
                                <asp:DropDownList ID="m_category" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                            <div class="col-sm-2"></div>
                        </div>
                        <div class="form-group text-right padding-div">
                            <span class="clipper">
                                        <button type="button" class="btn btn-default" onclick="resetForm();">
                                            Reset</button></span> <span class="clipper">
                                                <button id="btnSearch" type="button" class="btn btn-primary" onclick="search();">
                                                    Search</button></span>
                        </div>
                        <!-- Modal -->
                        <div id="myModal" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog modal-width">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">
                                            &times;</button>
                                        <h4 class="modal-title">
                                            Edit Field Properties</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div id="loadingModal"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...</div>
                                        <iframe id="popup_content" width="100%" frameborder="0" height="290px" onload="OnLoadedModal();" src=''></iframe>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Close</button></span> <span class="clipper">
                                                    <button type="button" class="btn btn-primary" onclick="saveField();">
                                                        Save</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Confirm Dialog-->
                        <div id="myConfirm" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog modal-width">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">
                                            Confirm Delete Field ID
                                        </h4>
                                    </div>
                                    <div class="modal-body text-center">
                                        
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel</button></span> <span class="clipper">
                                                    <button type="button" class="btn btn-primary" id="btnOK">
                                                        Delete</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="loadingMain" style="display:none"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...</div>
                        <div id='container'>
                        </div>

                        <script id='template' type='text/ractive'>
                            <table class="table table-striped table-condensed table-layout-fix">
                                <thead>
                                    <tr>
                                        <th class="col-tb-1">
                                        </th>
                                        <th class="col-tb-2">
                                        </th>
                                        {{#headers}}
                                        <th class="{{css}}">
                                            <a on-click="sortBy:{{key}}">{{value}} {{#if sortByKey == key}} {{#if sortAsc}} <span
                                                class="glyphicon glyphicon-sort-by-attributes"></span>{{else}} <span class="glyphicon glyphicon glyphicon-sort-by-attributes-alt">
                                                </span>{{/if}} {{/if}} </a>
                                        </th>
                                        {{/headers}}
                                    </tr>
                                </thead>
                                <tbody>
                                    {{#data}}
                                    <tr>
                                        <td class="text-center">
                                                <button type="button" class="btn btn-link" onclick="return onEditField('{{Id}}');">
                                                    edit</button>
                                        </td>
                                        <td class="text-center">
                                                <button type="button" class="btn btn-link" onclick="return onDeleteField('{{Id}}');">
                                                    delete</button>
                                        </td>
                                        <td>
                                            {{Id}}
                                        </td>
                                        <td>
                                            {{FriendlyName}}
                                        </td>
                                        <td>
                                            {{Category.Name}}
                                        </td>
                                        <td>
                                            {{Description}}
                                        </td>
                                        <td>
                                            {{#if IsReviewNeeded}}Yes{{else}}No{{/if}}
                                        </td>
                                        <td>
                                            {{#if IsHidden}}Yes{{else}}No{{/if}}
                                        </td>
                                    </tr>
                                    {{/data}}
                                </tbody>
                            </table>
                        </script>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
