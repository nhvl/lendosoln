using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;

namespace LendersOffice.Admin
{
	/// <summary>
	/// Summary description for AdminPanel.
	/// </summary>

	public partial class AdminPanel : LendersOffice.Admin.SecuredAdminPage
	{
        //Rebuild by Long Nguyen
        //Email: longn@meridianlink.net
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        private void InitializeComponent()
        {
        
        }
		/// <summary>
		/// Every admin page that inherits from this base page
		/// should implement this query method to specify the
		/// permission set required to access this page.
		/// </summary>
		/// <returns>
		/// List of required permissions.
		/// </returns>

		protected override E_InternalUserPermissions[] GetRequiredPermissions()
		{
			// Return an empty set.  Overload to provide
			// a page specific permission list.

			return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.IsDevelopment
			};
		}

	}

}
