﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageAppraisalVendors.aspx.cs"
    Inherits="LendersOfficeApp.LOAdmin.Manage.ManageAppraisalVendors" MasterPageFile="~/LOAdmin/loadmin.Master" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style type="text/css">
        /*Show loading animate*/.glyphicon-refresh-animate
        {
            -animation: spin .7s infinite linear;
            -ms-animation: spin .7s infinite linear;
            -webkit-animation: spinw .7s infinite linear;
            -moz-animation: spinm .7s infinite linear;
        }
        @keyframes spin{from{transform:scale(1)rotate(0deg);}
        to
        {
            transform: scale(1) rotate(360deg);
        }
        }@-webkit-keyframes spinw{from{-webkit-transform:rotate(0deg);}
        to
        {
            -webkit-transform: rotate(360deg);
        }
        }@-moz-keyframes spinm{from{-moz-transform:rotate(0deg);}
        to
        {
            -moz-transform: rotate(360deg);
        }
        }/******/
        .btn:focus, .btn:active
        {
            outline: none !important;
        }
        .center-div
        {
            margin: 0 auto;
            min-width: 200px;
            max-width: 500px;
            width: 30%;
        }
    </style>

    <script type="text/javascript">
        function f_edit(vendorId) {
			$('#loadingModal').show();
			$('#popup_content').css('visibility','hidden');
			$('#popup_content').attr('src','./EditAppraisalVendor.aspx?vendorid=' + vendorId);
			$('#myModal .modal-title').html("Edit Appraisal Vendor");
			$('#myModal').modal('show');
			return false;
		}
        function f_delete(vendorId, vendorName) {
            confirmDialog('Delete vendor "' + vendorName + '"?',vendorId);
            return false;
        }
        function f_newVendor() {
			$('#popup_content').attr('src','./EditAppraisalVendor.aspx');
			$('#myModal .modal-title').html("Add New Appraisal Vendor");
			$('#myModal').modal('show');
		}
		//Call this function when get back from modal popup
        function callback(data, textStatus, jqXHR){
            if(data.d)
            {
                $('#myModal').modal('hide');
                location.reload();
            }
            else
                alert(textStatus);
        }
        function OnLoadedModal(){
            $('#loadingModal').hide();
            $('#popup_content').css('visibility','visible');
        }
        function confirmDialog(message,data){
            $('#myConfirm .modal-body').html(message);
            $('#myConfirm #btnOK').click(function(){
                $('#myConfirm').modal('hide');
                $("[id$='DeleteVendor']").val(data);
                $( "form:first" ).submit();
                return false;
            });
            $('#myConfirm').modal('show');
        }
    </script>

    <div class="container-fluid">
        <div class="row">
            <div class="center-div">
                <div class="panel panel-default">
                    <div class="panel-body div-wrap-tb">
                        <form id="form1" runat="server">
                        <asp:HiddenField runat="server" ID="DeleteVendor" />
                        <span class="clipper"><button type="button" class="btn btn-primary" onclick="f_newVendor();">
                                                Add New</button></span>
                        <asp:GridView runat="server" ID="Vendors" CssClass="table table-striped table-condensed table-layout-fix"
                            CellSpacing="0" CellPadding="0" BorderWidth="0" GridLines="None" UseAccessibleHeader="true"
                            AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField >
                                    <ItemTemplate>
                                        <a href="javascript:void(0)" onclick="return f_edit('<%# AspxTools.HtmlString(Eval("VendorId").ToString()) %>');">
                                                edit</a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField >
                                    <ItemTemplate>
                                        <a href="javascript:void(0)" onclick="return f_delete('<%# AspxTools.HtmlString(Eval("VendorId").ToString()) %>','<%# AspxTools.HtmlString(Eval("VendorName").ToString()) %>');">
                                                delete</a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ></asp:TemplateField>
                                <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" HeaderStyle-Width="50%"/>
                            </Columns>
                        </asp:GridView>
                        <!-- Error -->
                        <div class="alert alert-error alert-dismissible hidden" role="alert" id="ErrorMessage"
                            runat="server">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <strong>Error Message!</strong> Error loading Appraisal Vendors.
                        </div>
                       
                        <!--Confirm Dialog-->
                        <div id="myConfirm" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog modal-width">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">
                                             Confirm Delete Appraisal Vendor
                                        </h4>
                                    </div>
                                    <div class="modal-body text-center">
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel</button></span> <span class="clipper">
                                                    <button type="button" class="btn btn-primary" id="btnOK">
                                                        Delete</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                         <!-- Modal -->
                        <div id="myModal" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">
                                            &times;</button>
                                        <h4 class="modal-title">
                                            Appraisal Vendor</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div id="loadingModal">
                                            <span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>Loading...</div>
                                        <iframe id="popup_content" width="100%" frameborder="0" height="400px" onload="OnLoadedModal();"
                                            src='./EditAppraisalVendor.aspx'></iframe>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel</button></span> <span class="clipper">
                                                    <button type="button" class="btn btn-primary" onclick="document.getElementById('popup_content').contentWindow.sendSubmit(callback);">
                                                        Save</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
