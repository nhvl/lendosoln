﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Integration.Cenlar;
    using LendersOffice.Security;

    /// <summary>
    /// This page allows management of the lenders using the Cenlar integration, as
    /// well as triggering manual transmissions to Cenlar's system.
    /// </summary>
    public partial class ManageCenlarIntegration : SecuredAdminPage
    {
        /// <summary>
        /// Gets the required permissions to access this page.
        /// </summary>
        /// <returns>The permissions associated with this page.</returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditIntegrationSetup
            };
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sending element.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Lenders.DataSource = LightCenlarLender.LoadAll();
            this.Lenders.DataBind();
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The sending element.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterJsScript("simpleservices.js");
            this.RegisterService("main", "/loadmin/manage/ManageCenlarIntegrationService.aspx");
            this.EnableJquery = true;

            Tools.Bind_CenlarEnvironmentT(this.Transmission_EnvironmentT);
        }

        /// <summary>
        /// Binds the data for the Lender list.
        /// </summary>
        /// <param name="sender">The caller of this method.</param>
        /// <param name="e">The event arguments.</param>
        protected void Lenders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.AlternatingItem) || e.Item.ItemType.Equals(ListItemType.Item))
            {
                var lender = (LightCenlarLender)e.Item.DataItem;

                HtmlInputCheckBox autoTransmissionsEnables = e.Item.FindControl("AreAutomaticTransmissionsEnabled") as HtmlInputCheckBox;
                if (autoTransmissionsEnables != null)
                {
                    autoTransmissionsEnables.Checked = lender.EnableAutomaticTransmissions;
                }

                HtmlAnchor editConfigurationLink = e.Item.FindControl("EditConfigurationLink") as HtmlAnchor;
                if (editConfigurationLink != null)
                {
                    editConfigurationLink.HRef = $"../Broker/BrokerEdit.aspx?cmd=edit&brokerid={lender.BrokerId.ToString()}#CenlarConfigurationTable";
                    editConfigurationLink.Target = "_blank";
                }
            }
        }
    }
}