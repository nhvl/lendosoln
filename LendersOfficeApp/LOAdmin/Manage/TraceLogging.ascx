<%@ Control Language="c#" AutoEventWireup="false" Codebehind="TraceLogging.ascx.cs" Inherits="LendersOffice.Admin.TraceLogging"%>
<div class="row">
    <label class="control-label col-xs-2">Entries</label>
    <div class="col-xs-5">
    <asp:TextBox CssClass="form-control text-vertical" id="m_Entries" runat="server" Width="300px" TextMode="MultiLine" Rows="8" autocomplete="off">
    </asp:TextBox>
        </div>
    <div class="col-xs-5">
        <span class="clipper">
    <asp:Button id="m_Update" runat="server" Text="Update" OnClick="UpdateClick" CssClass="btn btn-default" UseSubmitBehavior="false"></asp:Button>
            </span>
        <span class="clipper">
		<asp:Button id="m_Clear" runat="server" Text="Clear" OnClick="ClearClick" CssClass="btn btn-default"  UseSubmitBehavior="false"></asp:Button>
            </span>
        <span class="clipper">
		<asp:Button id="m_Start" runat="server" Text="Start" OnClick="StartClick" CssClass="btn btn-default"  UseSubmitBehavior="false"></asp:Button>
            </span>
        <span class="clipper">
		<asp:Button id="m_Stop" runat="server" Text="Stop" OnClick="StopClick" CssClass="btn btn-default"  UseSubmitBehavior="false"></asp:Button>
            </span>
        </div>
</div>
<br />
<div class="row">
    <label class="control-label col-xs-2">Max Entries</label>
        <div class="col-xs-3">
    <asp:TextBox id="m_MaxEntries" runat="server" CssClass="form-control" type="number" autocomplete="off"></asp:TextBox>
            </div>
    <div class="col-xs-7">
        </div>
</div>
<br />
<div class="row">
    <label class="control-label col-xs-2">Is On?</label>
        <div class="col-xs-3">
    <asp:TextBox id="m_On" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
        </div>
    <div class="col-xs-7">
        </div>
    </div>
<div style="PADDING: 8px; COLOR: red; FONT: 12px arial;">
			<ml:EncodedLabel id="m_Error" runat="server" EnableViewState="False"></ml:EncodedLabel>
</div>
