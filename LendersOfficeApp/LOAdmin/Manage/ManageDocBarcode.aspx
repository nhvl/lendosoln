﻿<%@ Page Language="C#" MasterPageFile="~/LOAdmin/loadmin.Master" AutoEventWireup="true" CodeBehind="ManageDocBarcode.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageDocBarcode" Title="Manage Doc Barcode" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="server">
    <style type="text/css">
        .modal-dialog
        {
            width: 800px;
        }
        .modal-dialog .dialog-input
        {
            width: 250px;
        }
        .form-inline .MiniInput
        {
            width: 60px;
        }
        .form-inline .LargeInput 
        {
            width: 400px;
        }
        .form-inline .SuperLargeInput 
        {
            width: 500px;
        }
        .modal-dialog .form-inline
        {
            margin: 3px;
            min-height: 34px;
        }
        .input-label
        {
            width: 20%;
        }
        .BaseCopy
        {
            display: none;
        }
        table.table-striped tr.invalid:nth-of-type(2n+1)
        {
            background-color: #f9dada;
        }

        table.table-striped tr.invalid
        {
            background-color: #ffe0e0;
        }
    </style>
    <script type="text/javascript">
        var barcodeFmt = <%= AspxTools.JsString(this.DocType) %>;
        var updateRecoredId;
        function _init() {
            if (document.getElementById("m_errorMessage") != null && document.getElementById("m_errorMessage").value != "") {
                alert(document.getElementById("m_errorMessage").value);
            }

            $('#AddTypeBtn').text('Add New ' + barcodeFmt + ' Barcode');
        }

        function clearInput() {
            $('#DescriptionId').val('');
            $('#BarcodeId').val('');
            $('.hiddenCmd').val('');
            $('.hiddenDesc').val('');
            $('.hiddenBarcode').val('');
            $('.hiddenId').val('');
            updateRecoredId = -1;
        }

        function sortByColumn( colIx){
            $('.hiddenCmd').val('sortByColumn');
            $('.hiddenId').val(colIx);
            __doPostBack('', '');
        }


        jQuery(function ($) {

            $('#AddTypeBtn').click(function () {
                $('#BarcodePopupDlg').modal('show');
                $('#dlgTitle').text('Add New ' + barcodeFmt + ' Barcode');
                $('#DescriptionId').val('');
                $('#BarcodeId').val('');
                updateRecoredId = -1;
            });

            $('#SaveBtn').click(function () {
                var desc = $('#DescriptionId').val().trim();
                var barcode = $('#BarcodeId').val().trim();

                if (!desc || !barcode) {
                    alert("Cannot create/update a doc type with Description or Barcode is empty!");
                    return;
                }

                if( updateRecoredId !== -1) {
                    if (confirm("Do you want to update doc type to '" + desc + "', barcode " + barcode + "?") == false)
                        return;
                }

                $('.hiddenCmd').val(updateRecoredId == -1 ? 'addDocType' : 'updateDocType');
                $('.hiddenDesc').val(desc);
                $('.hiddenBarcode').val(barcode);
                $('.hiddenId').val(updateRecoredId);
                __doPostBack('', '');
            });

            $('#CancelBtn').click(function () {
                clearInput();
            });

            $('#DocTypeTable').on('click', '.Delete', function () {
                var row = $(this).closest('tr');
                var id = row.data('id');
                var desc = row.find('.Description').text();
                var barcode = row.find('.BarcodeClassification').text();

                if (confirm("Do you want to delete doc type: '" + desc + "', barcode " + barcode + "?") == false)
                    return;

                $('.hiddenCmd').val('deleteDocType');
                $('.hiddenId').val(id);
                __doPostBack('', '');
            });

            $('#DocTypeTable').on('click', '.Edit', function () {
                var row = $(this).closest('tr');
                var id = row.data('id');
                updateRecoredId = id;

                jQuery.ajax({
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    url: 'ManageDocBarcode.aspx/GetBarcodeInfo',
                    data: JSON.stringify({ 'barcodeFmt': barcodeFmt, 'recordId':id }),
                    dataType: 'json',
                    async: false,
                    success: function (d) {
                        $('#BarcodePopupDlg').modal('show');
                        $('#dlgTitle').text('Update ' + barcodeFmt + ' Barcode');
                        $('#DescriptionId').val(d.d['InternalDescription']);
                        $('#BarcodeId').val(d.d['BarcodeClassification']);
                    },
                    error: function (xhr, status, error) {
                        var exception = JSON.parse(xhr.responseText);
                        alert("Cannot retrieve the barcode. The error message: '" + exception.Message + "'");
                    }
                });


            });


        });
    </script>

    <form id="form1" runat="server">
    <div>
        <div class="container-fluid">
            <div class="row">
                <div class="center-div">
                    <div class="panel panel-default">
                        <div class="panel-body div-wrap-tb">
                            <span class="clipper">
                                <button type="button" class="btn btn-primary" id="AddTypeBtn">Add New</button>
                            </span>
                            <table id="DocTypeTable" class="table table-striped table-condensed table-layout-fix">
                                <thead>
                                    <tr>
                                        <th width="60"></th>

                                    <% if (this.SortType == 0) {%>
                                        <th>Description &#x25b2;</th>
                                        <th><a onclick='sortByColumn(1)'>Barcode Classification</a></th>
                                    <%} else {%>
                                        <th><a onclick='sortByColumn(0)'>Description</a></th>
                                        <th>Barcode Classification &#x25b2;</th>
                                    <%}%>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater runat="server" ID="DocTypes">
                                        <ItemTemplate>
                                            <tr data-id="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id").ToString()) %>" >
                                               <td>
                                                    <a class="Edit">edit</a>
                                               </td>
                                               <td>
                                                    <span class="Description"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "InternalDescription").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <span class="BarcodeClassification"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BarcodeClassification").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <a class="Delete">delete</a>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="BarcodePopupDlg" class="modal v-middle fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dlgTitle">Add New Barcode</h4>
                </div>

                <div id="Inputs" class="modal-body">
                    <div class="form-inline">
                        <label class="input-label">Description</label>
                        <input type="text" id="DescriptionId" class="LargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Barcode Classification</label>
                        <input type="text" id="BarcodeId" class="LargeInput form-control" />
                    </div>
                </div>

                <div class="modal-footer">
                    <span class="clipper">
                        <input type="button" class="btn btn-default" id="CancelBtn" data-dismiss="modal" value="Cancel"/>
                    </span>
                    <span class="clipper">
                        <input type="button" class="btn btn-primary" id="SaveBtn" value="Save" />
                    </span>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="hiddenCmd" value="" runat="server" name="hiddenCmd" class="hiddenCmd" /> 
	<input type="hidden" id="hiddenId" value="" runat="server" name="hiddenId" class="hiddenId"/> 
	<input type="hidden" id="hiddenDesc" value="" runat="server" name="hiddenDesc" class="hiddenDesc"/> 
	<input type="hidden" id="hiddenBarcode" value="" runat="server" name="hiddenBarcode" class="hiddenBarcode"/> 

    </form>
</asp:Content>
