﻿/// <copyright file="ImportEdocs.aspx.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <summary>
/// Author: Michael Leinweaver
/// Date:   12/31/2015
/// </summary>
namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.BrokerAdmin.Importers.Edocs;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Security;

    /// <summary>
    /// Represents an importer page for document folders, document types, and
    /// Lender's Office to document vendor document type mappings.
    /// </summary>
    public partial class ImportEdocs : SecuredAdminPage
    {
        /// <summary>
        /// Represents the blank option displayed in the broker selection dropdown.
        /// </summary>
        private const string BlankBrokerCode = "<-- Select a Broker -->";

        /// <summary>
        /// Represents the extension for CSV files saved to the temporary directory.
        /// </summary>
        private const string CsvExtension = ".csv";

        /// <summary>
        /// Represents the required permissions to view the importer page.
        /// </summary>
        private static readonly E_InternalUserPermissions[] PagePermissions = new E_InternalUserPermissions[]
        {
            E_InternalUserPermissions.ViewBrokers,
            E_InternalUserPermissions.EditBroker,
        };

        /// <summary>
        /// Represents the type of import selected by the user.
        /// </summary>
        private enum ImportType
        {
            /// <summary>
            /// Folders will be imported.
            /// </summary>
            Folders,

            /// <summary>
            /// Document types will be imported.
            /// </summary>
            DocTypes,

            /// <summary>
            /// Document type mappings will be imported.
            /// </summary>
            Mappings
        }

        /// <summary>
        /// Gets the value of the style sheet for the page.
        /// </summary>
        /// <value>The empty string, as the style sheet will be handled automatically.</value>
        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Obtains an array of the permissions required to access the page.
        /// </summary>
        /// <returns>
        /// An array containing the required permissions to access the importer.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return ImportEdocs.PagePermissions;
        }

        /// <summary>
        /// Handles logic on page pre-initialization.
        /// </summary>
        /// <param name="e">
        /// The event arguments for the pre-initialization.
        /// </param>
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            // don't load common.js from BasePage
            this.m_loadLOdefaultScripts = false; 
            this.m_loadDefaultStylesheet = false;
        }

        /// <summary>
        /// Handles logic on page initialization.
        /// </summary>
        /// <param name="e">
        /// The event arguments for the initialization.
        /// </param>
        protected override void OnInit(EventArgs e)
        {
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            this.InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Initializes the page by binding the dropdown list containing
        /// the customer codes of brokers for import.
        /// </summary>
        /// <param name="sender">
        /// The sender that triggered the page load.
        /// </param>
        /// <param name="e">
        /// The event arguments for the page load.
        /// </param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.BindBrokersToDDL();
        }

        /// <summary>
        /// Imports a set of folders.
        /// </summary>
        /// <param name="sender">
        /// The activator of the button click event.
        /// </param>
        /// <param name="e">
        /// The event arguments for the button click.
        /// </param>
        protected void FoldersFileInputButton_Click(object sender, EventArgs e)
        {
            this.ImportFile(this.FoldersFileInput, ImportType.Folders);
        }

        /// <summary>
        /// Imports a set of document types.
        /// </summary>
        /// <param name="sender">
        /// The activator of the button click event.
        /// </param>
        /// <param name="e">
        /// The event arguments for the button click.
        /// </param>
        protected void DocTypesFileInputButton_Click(object sender, EventArgs e)
        {
            this.ImportFile(this.DocTypesFileInput, ImportType.DocTypes);
        }

        /// <summary>
        /// Imports a set of document types.
        /// </summary>
        /// <param name="sender">
        /// The activator of the button click event.
        /// </param>
        /// <param name="e">
        /// The event arguments for the button click.
        /// </param>
        protected void MappingsFileInputButton_Click(object sender, EventArgs e)
        {
            this.ImportFile(this.MappingsFileInput, ImportType.Mappings);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }

        /// <summary>
        /// Binds the broker dropdown on the page to the list
        /// of possible brokers by customer code.
        /// </summary>
        private void BindBrokersToDDL()
        {
            var brokers = new List<ListItem>();

            var searchResult = BrokerDbSearch.Search(
                name: string.Empty,
                fannieUserId: string.Empty,
                isActive: null,
                customerCode: string.Empty,
                brokerId: Guid.Empty, 
                suiteType: null);

            foreach (var item in searchResult)
            {
                brokers.Add(new ListItem()
                {
                    Text = item.CustomerCode,
                    Value = item.BrokerId.ToString()
                });
            }

            this.BrokerDropdown.Items.Add(new ListItem()
            {
                Text = ImportEdocs.BlankBrokerCode,
                Value = Guid.Empty.ToString()
            });

            foreach (var broker in brokers.OrderBy(li => li.Text))
            {
                this.BrokerDropdown.Items.Add(broker);
            }
        }

        /// <summary>
        /// Imports the file from the specified <paramref name="fileUpload"/>.
        /// </summary>
        /// <param name="fileUpload">
        /// The <see cref="FileUpload"/> containing the file to import.
        /// </param>
        /// <param name="importType">
        /// The type of import being performed.
        /// </param>
        private void ImportFile(FileUpload fileUpload, ImportType importType)
        {
            var filename = TempFileUtils.NewTempFilePath() + ImportEdocs.CsvExtension;

            fileUpload.PostedFile.SaveAs(filename);

            switch (importType)
            {
                case ImportType.Folders:
                    EdocsImporter.ImportFolders(
                        brokerId: Guid.Parse(this.BrokerDropdown.SelectedValue), 
                        path: filename);
                    break;
                case ImportType.DocTypes:
                    EdocsImporter.ImportDocTypes(
                        brokerId: Guid.Parse(this.BrokerDropdown.SelectedValue),
                        path: filename);
                    break;
                case ImportType.Mappings:
                    EdocsImporter.ImportMappings(
                        brokerId: Guid.Parse(this.BrokerDropdown.SelectedValue),
                        path: filename);
                    break;
                default:
                    throw new InvalidOperationException(
                        ErrorMessages.EdocsImporter.UnknownImportTypeEncounteredMessage);
            }

            FileOperationHelper.Delete(filename);

            ClientScript.RegisterStartupScript(
                type: typeof(ImportEdocs),
                key: Guid.NewGuid().ToString(),
                script: "onImportSuccess('" + Enum.GetName(typeof(ImportType), importType) + "');",
                addScriptTags: true);
        }
    }
}