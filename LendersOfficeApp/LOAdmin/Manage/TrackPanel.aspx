<%@ Page language="c#" Codebehind="TrackPanel.aspx.cs" AutoEventWireup="false" Inherits="LendersOffice.Admin.TrackPanel" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/Common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../Common/Header.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Track Panel</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</head>
	<body MS_POSITIONING="FlowLayout">
		<script type="text/javascript">
			function init()
			{
				if( document.getElementById("m_errorMessage") != null )
				{
					document.getElementById("m_Error").innerText = document.getElementById("m_errorMessage").value;
				}
			}		
		</script>
		<form id="TrackPanel" method="post" runat="server">
			<uc:Header runat="server">
			</uc:Header>
			<uc:HeaderNav runat="server">
				<MenuItem URL="~/LOAdmin/Main.aspx" Label="Main">
				</MenuItem>
				<MenuItem URL="~/LOAdmin/Manage/TrackPanel.aspx" Label="Track Control Panel">
				</MenuItem>
			</uc:HeaderNav>
			<table style="WIDTH: 8in;" cellspacing="0" cellpadding="0" border="0">
			<tr>
			<td class="FormTable">
				<br>
				<table style="WIDTH: 100%;" cellspacing="0" cellpadding="0">
				<tr>
				<td class="FieldLabel" align="left">
					<input type="radio" name="Panel" checked onclick="location.href = 'TrackPanel.aspx';">
					Track
					<input type="radio" name="Panel" onclick="location.href = 'EmailPanel.aspx';">
					Email
					<input type="radio" name="Panel" onclick="location.href = 'AdminPanel.aspx';">
					Admin
					<input type="radio" name="Panel"  onclick="location.href = 'DBMQManager.aspx';">
					Message Queue
				</td>
				<td align="right">
					<input type="button" value="Back" onclick="location.href = '../Main.aspx';">
				</td>
				</tr>
				</table>
				<hr>
				<div style="MARGIN: 0px; PADDING: 0px; BORDER: 2px solid lightgrey; WIDTH: 100%;">
					<table cellpadding="4" cellspacing="0" border="0" width="100%">
					<tr class="FormTableHeader">
					<td>
						Track control panel
					</td>
					</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0">
					<tr>
					<td>
						<table cellpadding="0" cellspacing="8" border="0">
						<tr>
						<td class="FieldLabel">
							Started On
						</td>
						<td>
							<asp:TextBox id="m_StartedOn" runat="server" style="PADDING-LEFT: 4px;" ReadOnly="True">
							</asp:TextBox>
						</td>
						</tr>
						<tr>
						<td class="FieldLabel">
							History Count
						</td>
						<td>
							<asp:TextBox id="m_HistoryCount" runat="server" style="PADDING-LEFT: 4px;">
							</asp:TextBox>
						</td>
						</tr>
						<tr>
						<td class="FieldLabel">
							Max Entries
						</td>
						<td>
							<asp:TextBox id="m_MaxEntries" runat="server" style="PADDING-LEFT: 4px;">
							</asp:TextBox>
						</td>
						</tr>
						<tr>
						<td class="FieldLabel">
							Memory Used
						</td>
						<td>
							<asp:TextBox id="m_MemoryUsed" runat="server" style="PADDING-LEFT: 4px;" ReadOnly="True">
							</asp:TextBox>
						</td>
						</tr>
						<tr>
						<td class="FieldLabel" nowrap>
							Server Clock
						</td>
						<td>
							<asp:TextBox id="m_ServerClock" runat="server" style="PADDING-LEFT: 4px;" ReadOnly="True">
							</asp:TextBox>
						</td>
						</tr>
						</table>
					</td>
					<td>
						<table cellpadding="0" cellspacing="8" border="0">
						<tr>
						<td>
							[
							<ML:EncodedLabel id="m_Count" runat="server">Count = #</ML:EncodedLabel>
							]
						</td>
						<td>
							<asp:TextBox id="m_Limit" runat="server" style="PADDING-LEFT: 4px;" Width="40px">
								360
							</asp:TextBox>
						</td>
						<td class="FieldLabel">
							<asp:Button id="m_Update" runat="server" Text="Update" OnClick="UpdateClick">
							</asp:Button>
							<asp:Button id="m_Clear" runat="server" Text="Clear" OnClick="ClearClick">
							</asp:Button>
						</td>
						<td>
							<asp:Button id="m_Refresh" runat="server" Text="Refresh">
							</asp:Button>
						</td>
						<td>
							<div id="m_Error" style="PADDING: 8px; COLOR: red; FONT: 12px arial;">
							</div>
						</td>
						</tr>
						</table>
					</td>
					</tr>
					</table>
				</div>
				<div style="MARGIN-TOP: 4px; PADDING: 4px; WIDTH: 100%; HEIGHT: 270px; BORDER: 2px solid lightgrey; OVERFLOW-Y: scroll;">
					<ml:CommonDataGrid id="m_Grid" runat="server" OnItemCommand="GridItemClick" OnItemDataBound="GridItemBound" EnableViewState="True">
					<Columns>
						<asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<asp:LinkButton id="Drop" runat="server" CommandName="Drop" CommandArgument="<%#AspxTools.HtmlString(Container.DataItem)%>">
									drop
								</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<asp:LinkButton id="View" runat="server" CommandName="View" CommandArgument="<%#AspxTools.HtmlString(Container.DataItem)%>">
									view
								</asp:LinkButton>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="LoginName" SortExpression="LoginName" HeaderText="User">
						</asp:BoundColumn>
						<asp:BoundColumn DataField="RecordCount" HeaderText="Record Count (#)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
						</asp:BoundColumn>
						<asp:BoundColumn DataField="AverageDelay" SortExpression="AverageDelay" DataFormatString="{0:N}" HeaderText="Average Delay (s)" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Age" SortExpression="Age" DataFormatString="{0:N}" HeaderText="Age (m)" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						</asp:BoundColumn>
					</Columns>
					</ml:CommonDataGrid>
				</div>
				<div style="MARGIN-TOP: 4px; PADDING: 4px; WIDTH: 100%; HEIGHT: 90px; BORDER: 2px solid lightgrey; OVERFLOW-Y: scroll;">
					<ml:CommonDataGrid id="m_History" runat="server">
					<Columns>
						<asp:BoundColumn DataField="Operation" HeaderText="Operation">
						</asp:BoundColumn>
						<asp:BoundColumn DataField="What" HeaderText="What">
						</asp:BoundColumn>
						<asp:BoundColumn DataField="From" HeaderText="From">
						</asp:BoundColumn>
						<asp:BoundColumn DataField="RequestedOn" HeaderText="Requested On" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						</asp:BoundColumn>
					</Columns>
					</ml:CommonDataGrid>
					<asp:TextBox id="m_Current" runat="server" style="DISPLAY: none;" ReadOnly="True">
					</asp:TextBox>
					<asp:Panel id="m_EmptyNote" runat="server" style="WIDTH: 100%; PADDING: 30px; COLOR: dimgray; FONT: 11px arial; TEXT-ALIGN: center;">
						No history to show.  Nothing selected.
					</asp:Panel>
				</div>
			</td>
			</tr>
			</table>
		</form>
	</body>
</html>
