﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.ObjLib.UI;
    using LendersOffice.XsltExportReport;

    /// <summary>
    /// Manage SFTP configurations page.
    /// </summary>
    public partial class ManageSFTPConfiguration : LendersOffice.Admin.SecuredAdminPage
    {
        /// <summary>
        /// Gets the style sheet.
        /// </summary>
        /// <value>The style sheet.</value>
        public override string StyleSheet
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            // for stop loading `common.js` from `BasePage`
            // this.m_loadLOdefaultScripts = false; 
            // Load for using gService
            this.RegisterJsScript("simpleservices.js");
            this.RegisterService("main", "/loadmin/manage/ManageSFTPConfigurationService.aspx");
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            IEnumerable<SFTPConfigViewModel> configs = XsltExportTransferSettings.RetrieveAll().Select(config => config.ToViewModel());
            this.Configurations.DataSource = configs;
            this.Configurations.DataBind();
        }

        /// <summary>
        /// Pre render function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnPreRender(EventArgs e)
        {
            // m_loadLOdefaultScripts is used to register gService
            // this.m_loadLOdefaultScripts = true;
            base.OnPreRender(e);
        }
    }
}