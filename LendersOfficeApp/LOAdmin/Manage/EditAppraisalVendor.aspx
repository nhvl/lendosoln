﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditAppraisalVendor.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.EditAppraisalVendor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <script type="text/javascript">
    var callback;
        function transformResponse(text) {
            try {
                return JSON.parse(text, function (key, value) {
                    if (typeof value == "string") {
                        var result, d;
                        // parse ASP.NET DateTime type
                        {
                            result = /\/Date\((-?[0-9]+)(?:[a-zA-Z]|(?:\+|-)[0-9]{4})?\)\//g.exec(value);
                            if (!!result) {
                                return new Date(parseFloat(result[1]));
                            }
                        }
                    }
                    return value;
                });
            } catch (e) {
                return text;
            }
        }
        function setTooltip(selector,title)
        {
            selector.prop("title",title);
            selector.focus();
            selector.tooltip({ placement: 'bottom'});
            selector.tooltip('show');
            selector.one("blur change", function(){ selector.tooltip("destroy");});
            //selector.one("blur change", function(){ selector.tooltip("destroy");$("div[role='tooltip']").tooltip('destroy');});
        }
    function save(e){
        e.preventDefault();
         var data={VId:$("[id$='VId']").val(),
                VendorName:$("[id$='VendorName']").val().trim(),
                ExportPath:$("[id$='ExportPath']").val().trim(),
                UsesGlobalDMS: $("[id$='UsesGlobalDMS']").prop('checked'),
                UsesAccountId: $("[id$='UsesAccountId']").prop('checked'),
                AppraisalNeededDateOption: $("[id$='AppraisalNeededDateOptions']").val().trim(),
                HideNotes: $("[id$='HideNotes']").prop('checked'),
                UseStageExportPath: $("[id$='UseStageExportPathCkd']").prop('checked'),
                AllowOnlyOnePrimaryProduct: $("[id$='AllowOnlyOnePrimaryProduct']").prop('checked'),
                ShowAdditionalEmails: $("[id$='ShowAdditionalEmails']").prop('checked'),
                SendAuthTicketWithOrders: $("[id$='SendAuthTicketWithOrders']").prop('checked'),
                EnableConnectionTest: $('#EnableConnectionTest').prop('checked'),
                AllowBiDirectionalCommunication: $('#AllowBiDirectionalCommunication').prop('checked')
                };
                if (!data.VendorName) {
                $("[id$='VendorName']").val('');
                setTooltip($("[id$='VendorName']"),"Please fill out the Vendor Name");
                return false;
            }
            if (!data.ExportPath&&!data.UsesGlobalDMS) {
                $("[id$='ExportPath']").val('');
                setTooltip($("[id$='ExportPath']"),"Please fill out the Export Path");
                return false;
            }
         $.ajax({
                type: "POST",
                url:window.location.pathname+"/Save",
                data:JSON.stringify(data),
                contentType: "application/json; charset:utf-8",
                dataType: "json",
                converters: { "text json": transformResponse }
                    }).done(function (data, textStatus, jqXHR) {
                        if(data.d){
                            callback(data, textStatus, jqXHR);
                        }
                        else{
                            setTooltip($("[id$='ExportPath']"),"Export path must be a valid url");
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        setTooltip($("[id$='ExportPath']"),JSON.parse(jqXHR.responseText).Message);
                    });
          return false;
                    
    }
        function f_usesGDMSClick() {
            var usesGDMS = $("[id$='UsesGlobalDMS']").prop('checked');
            if(usesGDMS){
                $("[id$='ExportPath']").attr("disabled", "disabled");
                $("[id$='ExportPath']").val('');
                $("[id$='ExportPath']").prop('required',false);
                
                $("[id$='SendAuthTicketWithOrders']").parent().parent().hide();
                $("[id$='UseStageExportPathCkd']").parent().parent().show();
                $("[id$='UseStageExportPathCkd']").prop('disabled', false);
            }
            else{
                $("[id$='ExportPath']").removeAttr("disabled");
                $("[id$='ExportPath']").prop('required',true);
                
                $("[id$='SendAuthTicketWithOrders']").prop("checked", false);
                $("[id$='SendAuthTicketWithOrders']").parent().parent().show();
                $("[id$='UseStageExportPathCkd']").parent().parent().hide();
            }
        }
        function sendSubmit(f){
            callback=f;
            $("[id$='form1']").prop("action", "");
            $('#btnSubmit').click();
        }
        f_usesGDMSClick();
    </script>
    <form id="form1" runat="server" onsubmit="return save(event);">
    <input type="submit" id="btnSubmit" class="hidden" />
    <asp:HiddenField runat="server" ID="VId"/>
    <div class="form-group">
        <label>Friendly Name:<span class="text-danger">*</span></label>
        <asp:TextBox runat="server" ID="VendorName" CssClass="form-control input" AutoComplete="off" required title="Please fill out the Vendor Name"></asp:TextBox>
    </div>
    <div class="form-group">
        <label>Export Path:<span class="text-danger">*</span></label>
        <asp:TextBox runat="server" ID="ExportPath" CssClass="form-control input" AutoComplete="off" required title="Please fill out the Export Path"></asp:TextBox>
    </div>
    <div class="checkbox">
        <label><asp:CheckBox ID="UsesGlobalDMS" runat="server" onchange="f_usesGDMSClick();"/>Use GlobalDMS Integration</label>
    </div>
    <div class="checkbox">
        <label><asp:CheckBox ID="UseStageExportPathCkd" runat="server"/>Export to staging path</label>
    </div>
    <div class="checkbox">
        <label><asp:CheckBox ID="UsesAccountId" runat="server" />Requires account id in credentials</label>
    </div>
    <div class="form-group">
        <label>'Appraisal Needed' date Options:</label>
        <asp:DropDownList runat="server" ID="AppraisalNeededDateOptions"/>
    </div>
    <div class="checkbox">
        <label><asp:CheckBox ID="HideNotes" runat="server" />Exclude 'Notes' field</label>
    </div>
    <div class="checkbox">
        <label><asp:CheckBox ID="AllowOnlyOnePrimaryProduct" runat="server" />Allow selecting only one primary product</label>
    </div>
    <div class="checkbox">
        <label><asp:CheckBox ID="ShowAdditionalEmails" runat="server" />Allow entering additional contact emails</label>
    </div>
    <div class="checkbox">
        <label><asp:CheckBox ID="SendAuthTicketWithOrders" runat="server" />Add generic auth ticket to appraisal orders</label>        
    </div>
    <div class="checkbox">
        <label><asp:CheckBox ID="EnableConnectionTest" runat="server" />Enable associated connection tests</label>        
    </div>
    <div class="checkbox">
        <label><asp:CheckBox ID="AllowBiDirectionalCommunication" runat="server" />Allow bi-directional communication</label>        
    </div>
    </form>
</body>
</html>
