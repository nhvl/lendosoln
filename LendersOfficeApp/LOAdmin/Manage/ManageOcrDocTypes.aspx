﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LOAdmin/loadmin.Master" AutoEventWireup="true" CodeBehind="ManageOcrDocTypes.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageOcrDocTypes" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
    <style type="text/css">
        .add-button {
            margin-bottom: 20px;
        }
    </style>

    <div ng-app="ManageOcrDocTypes" class="container-fluid">
        <div id="ManageOcrDocTypesDiv" ng-controller="ManageOcrDocTypesController" class="row">
            <div class="col-xs-6 col-xs-offset-3">
                <div class="panel panel-default">
                    <div class="panel-body div-wrap-tb">
                        <form id="form1" runat="server">
                            <div>
                                <span class="clipper add-button">
                                    <button type="button" class="btn btn-primary" ng-click="addNewDocType();">Add New</button>
                                </span>
                                <table class="table table-striped table-condensed">
                                    <tr>
                                        <th colspan="2">&nbsp;</th>
                                        <td>
                                            Name
                                        </td>
                                    </tr>
                                    <tr ng-repeat="classification in vm.DocTypes | orderBy:'Name' track by classification.Id">
                                        <td>
                                            <a ng-click="edit(classification.Id, classification.Name);">edit</a>
                                        </td>
                                        <td>
                                            <a ng-click="delete(classification.Id, classification.Name, $index);">delete</a>
                                        </td>
                                        <td>
                                            <span ng-bind="classification.Name">&nbsp;</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
