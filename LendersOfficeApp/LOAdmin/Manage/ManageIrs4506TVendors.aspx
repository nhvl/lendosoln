﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageIrs4506TVendors.aspx.cs"
    Inherits="LendersOfficeApp.LOAdmin.Manage.ManageIrs4506TVendors" MasterPageFile="~/LOAdmin/loadmin.Master" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style type="text/css">
        .btn:focus, .btn:active
        {
            outline: none !important;
        }
        .center-div
        {
            margin: 0 auto;
            min-width: 200px;
            max-width: 500px;
            width: 30%;
        }
    </style>

    <script type="text/javascript">
        function f_clearDialogInput() {
            $('#VendorId').val('');
            $('#VendorName').val('');
            $('#ExportPath').val('');
            $('#RequiredAccountId').prop('checked', false);
        }
        function f_newVendor() {
            f_clearDialogInput();
            $('#myModal .modal-title').html("Add New 4506-T Vendor");
            $('#myModal').modal('show');
        }
        
        function f_saveVendor() {
            var args = {
                VendorId:$('#VendorId').val(),
                VendorName:$('#VendorName').val().trim(),
                ExportPath:$('#ExportPath').val().trim(),
                RequiredAccountId: $('#RequiredAccountId').prop('checked') ? 'True' : 'False',
                EnableConnectionTest: $('#EnableConnectionTest').prop('checked') ? 'True' : 'False',
                CommunicationModel: $('#CommunicationModel').val()
            };
            if (!args.VendorName) {
                var $VendorName = $('#VendorName');
                $VendorName.val('');
                $VendorName.focus();
                $VendorName.tooltip({ placement: 'bottom'});
                $VendorName.tooltip('show');
                $VendorName.one("blur change", function(){ $VendorName.tooltip("destroy");});
                return false;
            }
            var result = gService.main.call("Update", args, true, false, false, true, successCallback, errorCallback);
        }
        
        function f_edit(vendorId) {
            var args = {
                VendorId: vendorId
            }
            $('#VendorId').val(vendorId);
            $('#myModal .modal-title').html("Edit 4506-T Vendor");
            var result = gService.main.call("Retrieve", args, true, false, false, true, loadSuccessCallback, errorCallback);
            return false;
        }

        function f_delete(vendorId) {
            var args = {
                VendorId:vendorId
            }
            confirmDialog('Do you want to delete this 4506-T vendor?',args);
            return false;
        }
        function confirmDialog(message,data){
            $('#myConfirm .modal-body').html(message);
            $('#myConfirm #btnOK').click(function(){
                var result = gService.main.call("Delete", data, true, false, false, true, successCallback, errorCallback);
                return false;
            });
            $('#myConfirm').modal('show');
        }
        function successCallback(result) {
            if (!result.error) {
                $('#myModal').modal('hide');
                location.reload();
            }
            else {
                alert(result.UserMessage);
            }
        }
        function loadSuccessCallback(result) {
            if (result.error) {
                alert(result.UserMessage);
                return false;
            }
            $('#VendorName').val(result.value['VendorName']);
            $('#ExportPath').val(result.value['ExportPath']);
            $('#RequiredAccountId').prop('checked', result.value['RequiredAccountId'] == 'True');
            $('#EnableConnectionTest').prop('checked', result.value['EnableConnectionTest'] == 'True');
            $('#CommunicationModel').val(result.value['CommunicationModel']);
            $('#myModal').modal('show');
        }
        function errorCallback(result) {
            alert(result.UserMessage);
        }
    </script>

    <div class="container-fluid">
        <div class="row">
            <div class="center-div">
                <div class="panel panel-default">
                    <div class="panel-body div-wrap-tb">
                        <form id="form1" runat="server">
                        <div>
                        <span class="clipper"><button type="button" class="btn btn-primary" onclick="f_newVendor();">
                                                    Add New</button></span>
                            <asp:GridView runat="server" ID="Vendors" CssClass="table table-striped table-condensed table-layout-fix"
                                CellSpacing="0" CellPadding="0" BorderWidth="0" GridLines="None" UseAccessibleHeader="true"
                                AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:void(0)" onclick="return f_edit('<%# AspxTools.HtmlString(Eval("VendorId").ToString()) %>');">
                                                    edit</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="javascript:void(0)" onclick="return f_delete('<%# AspxTools.HtmlString(Eval("VendorId").ToString()) %>');">
                                                    delete</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ></asp:TemplateField>
                                    <asp:BoundField HeaderText="Vendor Name" DataField="VendorName" HeaderStyle-Width="50%"/>
                                </Columns>
                            </asp:GridView>
                        </div>
                            <div>
                                <b>Live testing for IRS 4506t order requests:</b><br />
                                LoXml File: <asp:DropDownList runat="server" ID="LoXmlFileDropDown"></asp:DropDownList><asp:FileUpload runat="server" ID="LoXmlFileUpload"/><br />
                                Xsl File: <asp:DropDownList runat="server" ID="XslFileDropDown"></asp:DropDownList><ml:EncodedLiteral runat="server" ID="XslFilNameElement"></ml:EncodedLiteral><asp:FileUpload runat="server" ID="XslFileUpload" /><br />
                                Vendor using Xslt: <asp:DropDownList runat="server" ID="VendorDropdownForUsingXslt"></asp:DropDownList><br />
                                <asp:Button runat="server" OnClick="Click_SetTestVendorAndFiles" Text="Update vendor and files for xsl transform" />
                            </div>
                        <!--Confirm Dialog-->
                        <div id="myConfirm" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog modal-width">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">
                                            Confirm Delete 4506-T Vendor
                                        </h4>
                                    </div>
                                    <div class="modal-body  text-center">
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel</button></span> <span class="clipper">
                                                    <button type="button" class="btn btn-primary" id="btnOK">
                                                        Delete</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <!-- Modal -->
                        <div id="myModal" class="modal v-middle fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <form action="javascript:void(0);" onsubmit="return f_saveVendor();">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">
                                            &times;</button>
                                        <h4 class="modal-title">
                                            4506-T Vendor</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" id="VendorId" name="VendorId" />
                                        <div class="form-group">
                                            <label>
                                                Vendor Name:<span class="text-danger">*</span></label>
                                            <input type="text" id="VendorName" class="form-control" required title="Please fill out the Vendor Name"
                                                autocomplete="off" />
                                        </div>
                                        <div class="form-group">
                                            NOTE: Adding new url REQUIRES making a request to IT to allow our production servers
                                            to connect to it.
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                Export Path:</label>
                                            <input type="text" id="ExportPath" class="form-control" autocomplete="off" />
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="RequiredAccountId" />Require Account ID</label>
                                        </div>
                                        <div class="dropdown">
                                            <label>
                                                <select id="CommunicationModel">
                                                    <option selected="selected" value="0">LendingQB Async</option>
                                                    <option value="1">Equifax</option>
                                                </select> Communication Model</label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="EnableConnectionTest" />Enable associated connection tests</label>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="clipper">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancel</button></span> <span class="clipper">
                                                    <button type="submit" class="btn btn-primary">
                                                        Save</button></span>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
