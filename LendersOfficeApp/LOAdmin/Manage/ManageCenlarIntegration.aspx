﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LOAdmin/loadmin.Master" AutoEventWireup="true" CodeBehind="ManageCenlarIntegration.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageCenlarIntegration" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Integration.Cenlar" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Main" runat="server">
    <style type="text/css">
        .BaseCopy {
            display: none;
        }

        .center-div {
            margin: 0 auto;
            min-width: 200px;
            width: 70%;
        }

        .modal-dialog
        {
            width: 800px;
        }
        .modal-dialog .dialog-input
        {
            padding-left: 5px;
            width: 400px;
        }
        .modal-dialog .form-inline
        {
            margin: 3px;
        }

        .NameRow, .AutoTransmissionsRow {
            width: 30%;
        }

        .input-label
        {
            width: 20%;
            font-size: 13px;
        }

        .CustomerCodeRow {
            width: 12%;
        }

        .EditRow, .TransmitRow {
            width: 14%;
        }

        .align-center {
            text-align: center;
        }

        #Transmission_ResultSection {
            padding: 5px;
            border: 1px solid #e5e5e5;
            background-color: #eeeeee;
            width: 100%;
            height: 80px;
            overflow-y: auto;
            white-space: pre-line;
        }
    </style>
    <script type="text/javascript">
        jQuery(function ($) {
            $('#LenderPanel').on('click', '.SendTransmission', function () {
                clearTransmissionModal();
                var $parentRow = $(this).closest('tr');

                var brokerId = $parentRow.data('brokerid');
                $('#Transmission_BrokerId').val(brokerId);

                var companyName = $parentRow.find('.CompanyName:first');
                $('#Transmission_CompanyName').val(companyName.text());

                var customerCode = $parentRow.find('.CustomerCode:first');
                $('#Transmission_CustomerCode').val(customerCode.text());

                $('#LoanTransmissionModal').modal('show');
            });

            $('#Transmission_TransmitBtn').click(function () {
                var $results = $('#Transmission_ResultSection')
                var waitMessage = 'Please wait.';
                $results.text(waitMessage);

                var data =
                {
                    BrokerId: $('#Transmission_BrokerId').val(),
                    EnvironmentT: $('[id$="Transmission_EnvironmentT"]').val()
                };

                var fourDotRegex = /\.\.\.\./;
                var waitingInterval = window.setInterval(function () {
                    if ($results.text().match(fourDotRegex)) {
                        $results.text(waitMessage);
                    }
                    else {
                        $results.append('.');
                    }
                }, 500);

                gService.main.callAsyncSimple('SendCenlarTransmission', data, function (result) {
                    sendCenlarTransmissionCallback(result, waitingInterval);
                });
            });

            function sendCenlarTransmissionCallback(result, interval) {
                window.clearInterval(interval);

                if (result.error) {
                    $('#Transmission_ResultSection').text('The transmission could not be completed. Please review PB logs for any supplementary information.');
                }
                else {
                    if (result.value['Success'] === 'True') {
                        $('#Transmission_ResultSection').text('The transmission completed successfully.');
                    }
                    else {
                        var errorMessage = result.value['Message'];
                        $('#Transmission_ResultSection').text(errorMessage.trim());
                    }
                }
            }

            function clearTransmissionModal() {
                $('#Transmission_BrokerId').val('');
                $('#Transmission_CompanyName').val('');
                $('#Transmission_CustomerCode').val('');
                $('#Transmission_ResultSection').text('');

                var cenlarBetaEnvironment = 1; // CenlarEnvironmentT
                $('[id$="Transmission_EnvironmentT"]').val(cenlarBetaEnvironment);
            }
        });
    </script>
    <form id="form1" runat="server">
         <div class="container-fluid">
            <div class="row">
                <div class="center-div">
                    <div class="panel panel-default" id="LenderPanel">
                        <div class="panel-body div-wrap-tb">
                            <table class="table table-striped table-condensed table-layout-fix">
                                <thead>
                                    <tr>
                                        <th class="NameRow">
                                            <a data-sort="true" data-sorttarget="CompanyName" data-asc="false">Company Name</a>
                                        </th>
                                        <th class="CustomerCodeRow">
                                            <a data-sort="true" data-sorttarget="CustomerCode" data-asc="false">Customer Code</a>
                                        </th>
                                        <th class="AutoTransmissionsRow align-center">
                                            <a data-sort="true" data-sorttarget="AreAutomaticTransmissionsEnabled" data-asc="false">Automatic Transmissions Enabled?</a>
                                        </th>
                                        <th class="EditRow">&nbsp;</th>
                                        <th class="TransmitRow">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater runat="server" ID="Lenders" OnItemDataBound="Lenders_ItemDataBound">
                                        <ItemTemplate>
                                            <tr data-brokerid="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BrokerId").ToString()) %>">
                                                <td>
                                                    <span class="CompanyName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CompanyName").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <span class="CustomerCode"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CustomerCode").ToString()) %></span>
                                                </td>
                                                <td class="align-center">
                                                    <input type="checkbox" disabled="disabled" class="AreAutomaticTransmissionsEnabled" runat="server" id="AreAutomaticTransmissionsEnabled"/>
                                                </td>
                                                <td>
                                                    <a class="LenderEdit" id="EditConfigurationLink" runat="server">
                                                        edit configuration
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="SendTransmission" id="TransmitLoansLink" runat="server">
                                                        transmit loans
                                                    </a>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="LoanTransmissionModal" class="modal v-middle fade Configuration" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Transmit Loans</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-inline">
                            <input type="hidden" id="Transmission_BrokerId" />
                            <label class="input-label">Company Name</label>
                            <input type="text" id="Transmission_CompanyName" class="dialog-input form-control" disabled="disabled" />
                        </div>
                        <div class="form-inline">
                            <label class="input-label">Customer Code</label>
                            <input type="text" id="Transmission_CustomerCode" class="dialog-input form-control" disabled="disabled" />
                        </div>
                        <div class="form-inline">
                            <label class="input-label">Destination</label>
                            <asp:DropDownList runat="server" ID="Transmission_EnvironmentT" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <br />
                        <div class="form-inline">
                            <label class="input-label">Transmission Result</label>
                        </div>
                        <div id="Transmission_ResultSection"></div>
                        <br />
                        <div class="form-inline">
                            <input type="button" class="btn btn-primary" id="Transmission_TransmitBtn" value="Transmit" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span class="clipper">
                            <input type="button" class="btn btn-default CloseBtn" id="Transmission_CloseBtn" data-dismiss="modal" value="Close"/>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>
