﻿
<%@ Page Language="C#" CodeBehind="~/LOAdmin/Manage/ManageTitleVendors.aspx.cs" MasterPageFile="~/LOAdmin/loadmin.Master" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageTitleVendors" Title=" Manage Title Vendors"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
<style type="text/css">
    #dialog-cannotDelete {
        display:none;
    }

    #brokerList {
        width: 100%;
    }

    #dialog-wait {
        display: none;
        font-weight: bold;
    }

    #dialog-wait .ui-dialog-content {
        visibility: hidden;
    }

    .center-div
    {
        margin: 0 auto;
        min-width: 200px;
        width: 80%;
    }
    .NameRow
    {
        width: 60%;
    }
    .modal-dialog
    {
        width: 800px;
    }
    .modal-dialog .dialog-input
    {
        padding-left: 5px;
        width: 400px;
    }
    .modal-dialog .form-inline
    {
        margin: 3px;
    }
    .input-label
    {
        width: 20%;
    }
    .indent
    {
        margin-left: 15px;
    }
    .Bordered
    {
        padding: 5px;
        border: 1px solid #e5e5e5;
    }
    .Hidden
    {
        display: none;
    }
</style>
<script type="text/javascript">
    jQuery(function ($) {
        $('.To3rdPartyCertPage').click(function () {
            window.location = gVirtualRoot + '/LOAdmin/Manage/ManageThirdPartyClientCertificates.aspx';
        });

        $('a[data-sort="true"]').click(function () {
            var link = $(this);
            var th = $(this).parents('th').eq(0);
            var table = th.parents('table').eq(0);
            var rows = table.find('tr:gt(0):not(.BaseCopy)').toArray().sort(RowComparer(link.data('sorttarget')));

            link.data('asc', !link.data('asc'));
            if (!link.data('asc')) {
                rows = rows.reverse();
            }

            for (var i = 0; i < rows.length; i++) {
                table.append(rows[i]);
            }
        });

        $('.TabLink').click(function () {
            $('.TabLink').each(function (index, value) {
                $('#' + $(value).data('panel')).hide();
                $(this).parent('li').removeClass('active');
            });

            var panelToShow = $(this).data('panel');
            $(this).parent('li').addClass('active');
            $('#' + panelToShow).show();
        });

        $('.CancelBtn').click(function () {
            ClearInputs($(this).closest('.Configuration'));
        });

        function RowComparer(sortTargetClass) {
            return function (a, b) {
                var sortTargetA = $(a).find('.' + sortTargetClass);
                var sortTargetB = $(b).find('.' + sortTargetClass);
                if (sortTargetA.is(':checkbox')) {
                    var boolA = sortTargetA.is(':checked');
                    var boolB = sortTargetB.is(':checked');

                    if (boolA === true && boolB === false) {
                        return 1;
                    }
                    else if (boolA === false && boolB === true) {
                        return -1;
                    }
                    else {
                        return -1;
                    }
                }
                else {
                    var nameA = sortTargetA.text();
                    var nameB = sortTargetB.text();
                    if (nameA < nameB) {
                        return -1;
                    }
                    else if (nameA > nameB) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            }
        }

        function ClearInputs(targetSelector) {
            $(targetSelector).find(':input')
                .not(':button, :submit, :reset').prop('checked', false)
                .not(':checkbox, :radio, select').val('');
            $(targetSelector).find('option:selected').prop('selected', false);
            $(targetSelector).find('.ClearableList').empty();
        }

        /**** PLATFORM TAB ****/
        $('#AddPlatformBtn').click(function () {
            $('#PlatformConfiguration').modal('show');
            ClearInputs('#PlatformConfiguration')
            $('#PTransmissionId').val('');

            ToggleAccountCheckbox($('#UsesAccountId'));
            ToggleByAuthenticationMethod($('[id$="PTransmissionAuthenticationT"]'));
        });

        $('#PlatformPanel').on('click', '.PlatformDelete', function () {
            var parentTr = $(this).closest('tr');
            var platformId = parentTr.data('platformid');
            var platformName = parentTr.find('.PlatformName').text();

            if (confirm("Really delete Platform <" + platformName + ">")) {
                var data = {
                    PlatformId: platformId
                };

                var result = gService.main.call("DeletePlatform", data);
                if (!result.error) {
                    if (result.value["Success"] === 'True') {
                        parentTr.remove();
                        $('.VendorPlatform').find('option[value="' + platformId + '"]').remove();
                    }
                    else {
                        alert(result.value['Errors']);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            }
        });

        $('#PlatformPanel').on('click', '.PlatformEdit', function () {
            var platformId = $(this).closest('tr').data('platformid');
            var data = {
                PlatformId: platformId
            };

            var result = gService.main.call("RetrievePlatform", data);
            if (!result.error) {
                if (result.value['Success'] === 'True') {
                    ClearInputs('#PlatformConfiguration');

                    var viewModel = JSON.parse(result.value["PlatformViewModel"]);
                    $('#PlatformId').val(viewModel.PlatformId);
                    $('#PlatformName').val(viewModel.PlatformName);
                    $('[id$="TransmissionModelT"]').val(viewModel.TransmissionModelT);
                    $('[id$="PayloadFormatT"]').val(viewModel.PayloadFormatT);
                    $('#UsesAccountId').prop('checked', viewModel.UsesAccountId);
                    $('#RequiresAccountId').prop('checked', viewModel.RequiresAccountId);
                    PopulateTransmission('P', viewModel.TransmissionInfo);

                    $('#PlatformConfiguration').modal('show');
                    ToggleAccountCheckbox($('#UsesAccountId'));
                    ToggleByAuthenticationMethod($('[id$="PTransmissionAuthenticationT"]'));
                }
                else {
                    alert(result.value["Errors"]);
                }
            }
            else {
                alert(result.UserMessage);
            }
        });

        /**** PLATFORM POPUP ****/
        $('#PlatformSaveBtn').click(function () {
            var transmission = GetTransmission('P');

            var platform = {
                PlatformId: $('#PlatformId').val(),
                PlatformName: $('#PlatformName').val(),
                TransmissionModelT: $('[id$="TransmissionModelT"]').val(),
                PayloadFormatT: $('[id$="PayloadFormatT"]').val(),
                UsesAccountId: $('#UsesAccountId').is(':checked'),
                RequiresAccountId: $('#RequiresAccountId').is(':checked'),
                TransmissionInfo: transmission
            };

            var data = {
                PlatformViewModel: JSON.stringify(platform)
            };

            var result = gService.main.call("SavePlatform", data);
            if (!result.error) {
                if (result.value['Success'] === 'True') {
                    $('#PlatformConfiguration').modal('hide');
                    ClearInputs('#PlatformConfiguration');
                    if (result.value["IsNew"] === 'True') {
                        var cloneRow = $('#PlatformPanel').find('.BaseCopy').clone().removeClass('BaseCopy Hidden');
                        cloneRow.find('.PlatformName').text(result.value["PlatformName"]);
                        cloneRow.data('platformid', result.value["PlatformId"]);
                        $('#PlatformPanel').find('table').append(cloneRow);
                        $('.VendorPlatform').append($('<option>', { value: result.value["PlatformId"], text: result.value["PlatformName"] }));
                    }
                    else {
                        var row = $('#PlatformPanel').find('tr').filter(function () { return $(this).data('platformid') == result.value["PlatformId"] });
                        row.find('.PlatformName').text(result.value["PlatformName"]);
                        $('.VendorPlatform').find('option[value="' + result.value["PlatformId"] + '"]').text(result.value["PlatformName"]);
                    }
                }
                else {
                    alert(result.value['Errors']);
                }
            }
            else {
                alert(result.UserMessage);
            }
        });

        /**** VENDOR TAB ****/
        $('#AddVendorBtn').click(function () {
            ClearInputs('#VendorConfiguration');
            $('#VendorConfiguration').modal('show');
            ToggleByAuthenticationMethod($('[id$="VQTransmissionAuthenticationT"]'));
            ToggleByAuthenticationMethod($('[id$="VPTransmissionAuthenticationT"]'));
            TogglePlatformEnabled('QuotingSection');
            TogglePlatformEnabled('PolicyOrderingSection');
        });

        $('#VendorPanel').on('click', '.VendorEdit', function () {
            var vendorId = $(this).closest('tr').data('vendorid');
            var data = {
                VendorId: vendorId
            };

            var result = gService.main.call("RetrieveVendor", data);
            if (!result.error) {
                if (result.value['Success'] === 'True') {
                    ClearInputs('#VendorConfiguration');

                    var viewModel = JSON.parse(result.value["VendorViewModel"]);
                    $('#VendorId').val(viewModel.VendorId);
                    $('#CompanyName').val(viewModel.CompanyName);
                    $('#IsEnabled').prop('checked', viewModel.IsEnabled);
                    $('#IsTestVendor').prop('checked', viewModel.IsTestVendor);
                    $('#UsesProviderCodes').prop('checked', viewModel.UsesProviderCodes);
                    $('#IsQuickQuotingEnabled').prop('checked', viewModel.IsQuickQuotingEnabled);
                    $('#IsDetailedQuotingEnabled').prop('checked', viewModel.IsDetailedQuotingEnabled);
                    $('#IsPolicyOrderingEnabled').prop('checked', viewModel.IsPolicyOrderingEnabled);

                    $('[id$="PolicyOrderingPlatformId"').val(viewModel.PolicyOrderingPlatformId == null ? 0 : viewModel.PolicyOrderingPlatformId);
                    $('#IsOverridingPolicyOrderingPlatform').prop('checked', viewModel.IsOverridingPolicyOrderingPlatform);
                    PopulateTransmission('VP', viewModel.PolicyOrderingTransmission);

                    $('[id$="QuotingPlatformId"').val(viewModel.QuotingPlatformId == null ? 0 : viewModel.QuotingPlatformId);
                    $('#IsOverridingQuotingPlatform').prop('checked', viewModel.IsOverridingQuotingPlatform);
                    PopulateTransmission('VQ', viewModel.QuotingTransmission);


                    $('#VendorConfiguration').modal('show');
                    ToggleByAuthenticationMethod($('[id$="VQTransmissionAuthenticationT"]'));
                    ToggleByAuthenticationMethod($('[id$="VPTransmissionAuthenticationT"]'));
                    TogglePlatformEnabled('QuotingSection');
                    TogglePlatformEnabled('PolicyOrderingSection');
                }
                else {
                    alert(result.value["Errors"]);
                }
            }
            else {
                alert(result.UserMessage);
            }
        });

        $('#VendorPanel').on('click', '.VendorDelete', function () {
            var parentTr = $(this).closest('tr');
            var vendorId = parentTr.data('vendorid');
            var vendorName = parentTr.find('.CompanyName').text();

            if (confirm("Really delete Vendor <" + vendorName + ">")) {
                var data = {
                    VendorId: vendorId
                };

                var result = gService.main.call("DeleteVendor", data);
                if (!result.error) {
                    if (result.value["Success"] === 'True') {
                        parentTr.remove();
                    }
                    else {
                        alert(result.value['Errors']);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            }
        });

        /**** VENDOR POPUP ****/
        $('.EnableCb').change(function () {
            TogglePlatformEnabled($(this).closest('.TransmissionDetails').prop('id'));
        });

        $('.VendorPlatform').change(function () {
            if ($(this).val() == '0') {
                return;
            }

            var data = {
                PlatformId: $(this).val()
            };

            var transmissionDetails = $(this).closest('.TransmissionDetails');
            var result = gService.main.call("RetrievePlatform", data);
            if (result.error) {
                alert(result.UserMessage);
            } else if (result.value['Success'] !== 'True') {
                alert(result.value["Errors"]);
            } else {
                var viewModel = JSON.parse(result.value["PlatformViewModel"]);
                transmissionDetails.find('.IsOverridingPlatform').prop('checked', false);
                PopulateTransmission($(this).data('prefix'), viewModel.TransmissionInfo);
                ToggleByAuthenticationMethod(transmissionDetails.find('[id$="TransmissionAuthenticationT"]'));
                TogglePlatformEnabled(transmissionDetails.attr('id'));
            }
        });

        $('.IsOverridingPlatform').change(function () {
            var checked = this.checked;
            var transmissionDetails = $(this).closest('.TransmissionDetails');
            TogglePlatformEnabled(transmissionDetails.attr('id'));
            if (!checked) {
                if (transmissionDetails.find('.VendorPlatform').val() == '0') {
                    return;
                }

                var data = {
                    PlatformId: $(transmissionDetails.find('.VendorPlatform')).val()
                };

                var result = gService.main.call("RetrievePlatform", data);
                if (result.error) {
                    alert(result.UserMessage);
                } else if (result.value['Success'] !== 'True') {
                    alert(result.value["Errors"]);
                } else {
                    var viewModel = JSON.parse(result.value["PlatformViewModel"]);
                    PopulateTransmission($(this).data('prefix'), viewModel.TransmissionInfo);
                    ToggleByAuthenticationMethod(transmissionDetails.find('[id$="TransmissionAuthenticationT"]'));
                }
            }
        });

        $('#VendorSaveBtn').click(function () {
            var isQuickQuotingEnabled = $('#IsQuickQuotingEnabled').is(':checked');
            var isDetailedQuotingEnabled = $('#IsDetailedQuotingEnabled').is(':checked');
            var quotingTransmission = isQuickQuotingEnabled || isDetailedQuotingEnabled ? GetTransmission('VQ') : null;
            var isPolicyOrderingEnabled = $('#IsPolicyOrderingEnabled').is(':checked');
            var policyOrderingTransmission = isPolicyOrderingEnabled ? GetTransmission('VP') : null;
            var vendor = {
                VendorId: $('#VendorId').val(),
                CompanyName: $('#CompanyName').val(),
                IsEnabled: $('#IsEnabled').is(':checked'),
                IsTestVendor: $('#IsTestVendor').is(':checked'),
                UsesProviderCodes: $('#UsesProviderCodes').is(':checked'),
                IsQuickQuotingEnabled: isQuickQuotingEnabled,
                IsDetailedQuotingEnabled: isDetailedQuotingEnabled,
                IsPolicyOrderingEnabled: isPolicyOrderingEnabled,
                QuotingPlatformId: $('[id$="QuotingPlatformId"]').val(),
                IsOverridingQuotingPlatform: $('#IsOverridingQuotingPlatform').is(':checked'),
                QuotingTransmission: quotingTransmission,
                PolicyOrderingPlatformId: $('[id$="PolicyOrderingPlatformId"]').val(),
                IsOverridingPolicyOrderingPlatform: $('#IsOverridingPolicyOrderingPlatform').is(':checked'),
                PolicyOrderingTransmission: policyOrderingTransmission
            };

            var data = {
                VendorInfoViewModel: JSON.stringify(vendor)
            };

            var result = gService.main.call("SaveVendor", data);
            if (!result.error) {
                if (result.value['Success'] === 'True') {
                    $('#VendorConfiguration').modal('hide');
                    ClearInputs('#VendorConfiguration');
                    if (result.value["IsNew"] === 'True') {
                        var cloneRow = $('#VendorPanel').find('.BaseCopy').clone().removeClass('BaseCopy Hidden');
                        cloneRow.find('.CompanyName').text(result.value["CompanyName"]);
                        cloneRow.data('vendorid', result.value["VendorId"]);
                        cloneRow.find('.IsEnabled').prop('checked', result.value["IsEnabled"] === 'True');
                        cloneRow.find('.IsTestVendor').prop('checked', result.value["IsTestVendor"] === 'True');
                        $('#VendorPanel').find('table').append(cloneRow);
                    }
                    else {
                        var row = $('#VendorPanel').find('tr').filter(function () { return $(this).data('vendorid') == result.value["VendorId"]; });
                        row.find('.CompanyName').text(result.value["CompanyName"]);
                        row.find('.IsEnabled').prop('checked', result.value["IsEnabled"] === 'True');
                        row.find('.IsTestVendor').prop('checked', result.value["IsTestVendor"] === 'True');
                    }
                }
                else {
                    alert(result.value["Errors"]);
                }
            }
            else {
                alert(result.UserMessage);
            }
        });

        function TogglePlatformEnabled(section) {
            var transmissionSection = $('#' + section);
            var enabled = transmissionSection.find('.EnableCb:checked').length > 0;
            var overriden = transmissionSection.find('.IsOverridingPlatform').is(':checked');

            transmissionSection.find(':input').not('.AlwaysEditable').prop('disabled', !enabled);
            transmissionSection.find(':input').not('.AlwaysEditable, .IsOverridingPlatform, .VendorPlatform').prop('disabled', !enabled || !overriden);
        }

        /**** TRANSMISSION SECTION ****/
        $('[id$="TransmissionAuthenticationT"]').change(function () {
            ToggleByAuthenticationMethod($(this));
        });

        function ToggleByAuthenticationMethod(toggledDropdown) {
            var value = toggledDropdown.val();
            var popup = toggledDropdown.closest('.TransmissionDetails');

            // TransmissionAuthenticationT.BasicAuthentication
            popup.find('.BasicAuthField').toggle(value === '0');

            // TransmissionAuthenticationT.DigitalCert
            popup.find('.DigitalCertField').toggle(value === '1');
        }

        $('[id$="UsesAccountId"]').change(function () {
            ToggleAccountCheckbox($(this));
        });

        function ToggleAccountCheckbox(usesAccountCheckbox) {
            var checked = usesAccountCheckbox.is(':checked');
            var popup = usesAccountCheckbox.closest('.TransmissionDetails');
            var target = popup.find('[id$="RequiresAccountId"]');
            target.prop('disabled', !checked);
            if (!checked) {
                target.prop('checked', false);
            }
        }

        function PopulateTransmission(prefix, viewModel) {
            if(!viewModel) {
                return;
            }

            $('#' + prefix + 'TargetUrl').val(viewModel.TargetUrl);
            $('[id$="' + prefix + 'TransmissionAuthenticationT"]').val(viewModel.TransmissionAuthenticationT);
            $('#' + prefix + 'ResponseCertificateId').val(viewModel.ResponseCertificateId);
            $('#' + prefix + 'RequestCredentialName').val(viewModel.RequestCredentialName);
            $('#' + prefix + 'RequestCredentialPassword').val(viewModel.RequestCredentialPassword);
            $('#' + prefix + 'ResponseCredentialName').val(viewModel.ResponseCredentialName);
            $('#' + prefix + 'ResponseCredentialPassword').val(viewModel.ResponseCredentialPassword);
        }

        function GetTransmission(prefix) {
            var transmission =
            {
                TargetUrl: $('#' + prefix + 'TargetUrl').val(),
                TransmissionAuthenticationT: $('[id$="' + prefix + 'TransmissionAuthenticationT"]').val(),
                ResponseCertificateId: $('#' + prefix + 'ResponseCertificateId').val(),
                RequestCredentialName: $('#' + prefix + 'RequestCredentialName').val(),
                RequestCredentialPassword: $('#' + prefix + 'RequestCredentialPassword').val(),
                ResponseCredentialName: $('#' + prefix + 'ResponseCredentialName').val(),
                ResponseCredentialPassword: $('#' + prefix + 'ResponseCredentialPassword').val(),
            };

            return transmission;
        }
    });
    
</script>
<form runat="server" onsubmit="return customOnSubmit();">
  <div class="container">
    <div class="row">
        <div class="col-xs-4 col-xs-offset-4">
             <div class="panel panel-default">
                <div class="panel-body div-wrap-tb">
                    <div ng-app="myApp" ng-controller="vendorCtrl">
                        <div id="dialog-wait" title="Please wait"> 
                            Checking if vendor is safe to delete.
                            <br />
                            Please wait...
                            <br />
                            <img src="../../images/status.gif" alt="Loading" />
                        </div>
                        <div id="dialog-cannotDelete" title="Cannot delete Vendor!">
                            <p>This vendor is set in fee service file for the following brokers:</p>
                            <p id="brokerList"></p>
                            <p>Please remove this vendor from any active fee service file before deleting it.</p>
                        </div>
                        <div class="text-left">
                          <span class="clipper">
                            <input  type="button"  ID="CreateBtn" class="btn btn-primary" value="Add New"  ng-click="addVendor()" />
                          </span>
                        </div>

                        <table  class="table table-striped table-condensed table-layout-fix " ng-show="vendorList.length">
                                <thead>
                                  <tr>
                                    <th scope="col"></th>
                                     <th  scope="col""></th>
                                    <th  scope="col" class="col-md-8">Vendor Name</th>
                                  </tr>
                                </thead>
                                 <tbody>
                                     <tr ng-repeat="vendor in vendorList">
                                        <td class="text-center owner" ><a  ng-click="editVendor(vendor.Id)">edit</a></td>
                                        <td class="text-center owner"><a ng-click="deleteVendor(vendor.Id)">delete</a></td>
                                         <td>{{vendor.Name}}</td>
                                    </tr>
                                 </tbody>
                            </table>                                    
                        <div id="addModal" class="modal v-middle fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">{{formTitle}} Vendor</h4>
                                    </div>
                                    <div class="modal-body  form-label-noerror">
                                        <div class="form-group ">
                                        <label>Vendor Name</label><span class="text-danger"  id="asterisk">&nbsp;*</span>
                                        <div class="text-right-noerror">
                                            <span id="error"  class="text-danger"/>
                                        </div>
                                        <asp:TextBox   AutoComplete="off" class="form-control input-sm" ID="VendorName" runat="server" >{{selectedVendor.Name}}</asp:TextBox>
                                        </div>
                                        <div class="form-group ">
                                            <label>Export Path for Quote</label><span class="text-danger"  id="asterisk">&nbsp;*</span>
                                            <div class="text-right-noerror">
                                                <span id="error"  class="text-danger"/>
                                            </div>
                                            <asp:TextBox  AutoComplete="off"  class="form-control input-sm" ID="ExportPath" runat="server" >{{selectedVendor.ExportPath}}</asp:TextBox>
                                        </div>
                                        <div class="form-group ">
                                            <label>Export Path for Title</label>
                                            
                                            <asp:TextBox  AutoComplete="off"  class="form-control input-sm" ID="ExportTitle" runat="server" >{{selectedVendor.ExportPathTitle}}</asp:TextBox>
                                        </div>
                                        <div class="form-group ">
                                            <label>LQB Service Username</label>
                                            <div class="text-right-noerror">
                                                <span id="error"  class="text-danger"/>
                                            </div>
                                            <asp:TextBox  AutoComplete="off"  class="form-control input-sm" ID="LQBServiceUserName" runat="server" >{{selectedVendor.LQBServiceUserName}}</asp:TextBox>
                                        </div>
                                        <div class="form-group ">
                                            <label>LQB Service Password</label>
                                            <div class="text-right-noerror">
                                                <span id="error"  class="text-danger"/>
                                            </div>
                                            <asp:TextBox  AutoComplete="off"  class="form-control input-sm" ID="LQBServicePassword" runat="server" >{{selectedVendor.LQBServicePassword}}</asp:TextBox>
                                        </div>
                                        <div class="form-group ">
                                            <label>LQB Service Source Name</label>
                                            <div class="text-right-noerror">
                                                <span id="error"  class="text-danger"/>
                                            </div>
                                            <asp:TextBox  AutoComplete="off"  class="form-control input-sm" ID="LQBServiceSourceName" runat="server" >{{selectedVendor.LQBServiceSourceName}}</asp:TextBox>
                                        </div>
                                        <div class="form-inline ">
                                            <label>Title Vendor Type</label>
                                            <asp:DropDownList runat="server" ID="TitleVendorType" class="form-control">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group checkbox">
                                            <label class="control-label">
                                            <asp:CheckBox runat="server" ID="RequireAccountId" > </asp:CheckBox > Requires account id in credentials
                                            </label>
                                        </div>
                                        <div class="form-group checkbox">
                                            <label class="control-label">
                                            <asp:CheckBox runat="server" ID="EnableConnectionTest" > </asp:CheckBox > Enable associated connection tests
                                            </label>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    <span>
                                        <span class="clipper"><input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel"/></span>
                                        <span class="clipper">
                                            <asp:Button ID="Button1" class="btn btn-primary" Text="Save" OnCommand="AddLink_OnCommand" ng-if="formTitle !='Edit'" runat="server" />
                                            <asp:Button ID="Button2" class="btn btn-primary" Text="Save" OnCommand="EditLink_OnCommand" ng-if="formTitle =='Edit'" runat="server" />
                                        </span>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="deleteModal" class="modal v-middle fade " >
                        <div class="modal-dialog">
                            <div class="modal-content">
                                 <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title">Confirm Delete Vendor</h4>
                                </div>
                                <div class="modal-body text-center">Do you want to delete vendor "{{selectedVendor.Name}}" ?<label id="delLabel"></label></div>
                                <div class="modal-footer">
                                    <span class="clipper"><input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel"/></span>
                                    <span class="clipper"><asp:Button class="btn btn-primary" Text="OK" OnCommand="DeleteLink_OnCommand" runat="server" /></span>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField id="hiddenKey" runat="server" value="{{selectedVendor.Id}}"/>
                        <asp:HiddenField id="vendorDataKey" runat="server"/>
                    </div>
                  </div>
                <script type="text/javascript">
                    function openAddDialog() {
                    }
                    var app = angular.module('myApp', []);

                    app.controller('vendorCtrl', function($scope) {
                        $scope.vendorList = JSON.parse($("[id*=vendorDataKey").val());
                        $scope.formTitle = "Add New";

                        $scope.setVendorModal = true;

                        $scope.isRequired = function () {
                            if ($scope.setVendorModa){
                                return "ng-Required='isRequired'";
                            }
                            else{
                                return "";
                            }
                        }
                        $scope.addVendor = function () {

                            $("#addModal").modal('show');
                            $scope.formTitle = "Add New";
                            $scope.selectedVendor = "";

                            setRequiredFields(true);
                            
                            $("[id*=RequireAccountId").prop('checked', false);
                            $("[id*='EnableConnectionTest']").prop('checked', false);
                        }
                        $scope.editVendor = function ( vendorId) {
                            $("#addModal").modal('show');
                            $scope.formTitle = "Edit";
                            $scope.selectedVendor = "";

                            setRequiredFields(true);

                            var i=0, len=  $scope.vendorList.length;
                            for (; i<len; i++) {
                                if ($scope.vendorList[i].Id == vendorId) {
                                    $scope.selectedVendor = $scope.vendorList[i];
                                    $("[id*=RequireAccountId").prop('checked', $scope.selectedVendor.RequiresAccountId);
                                    $("[id*='EnableConnectionTest']").prop('checked', $scope.selectedVendor.EnableConnectionTest);
                                    break;
                                }
                            }
                        }
                        $scope.deleteVendor = function (vendorId) {
                            $scope.selectedVendor = "";

                            // Open wait dialog.
                            $('#dialog-wait').dialog({
                                modal: true,
                                closeOnEscape: false,
                                width: "300",
                                height: "125",
                                draggable: false,
                                dialogClass: "LQBDialogBox",
                                resizable: false
                            });

                            // Remove dialog title bar.
                            $('#dialog-wait').prev(".ui-dialog-titlebar").hide();

                            // Try for async.
                            $.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: 'ManageTitleVendors.aspx/CheckCanDeleteVendor',
                                data: JSON.stringify({ vendorId: vendorId }),
                                dataType: 'json',
                                success: function (result) {
                                    // Close wait dialog.
                                    $('#dialog-wait').dialog("close");

                                    // Check if can delete
                                    if (!result.d.CanDeleteVendor) {

                                        // Generate broker list.
                                        var s = "";
                                        //var badBrokers = $.parseJSON(result.value["BadBrokers"]);
                                        for (var i = 0; i < result.d.BadBrokers.length; i++) {
                                            var broker = result.d.BadBrokers[i];
                                            s += broker.CustomerCode + " (" + broker.BrokerId + ")\n"
                                        }

                                        $('#brokerList').text(s);

                                        $('#dialog-cannotDelete').dialog({
                                            modal: true,
                                            buttons: {
                                                "Ok": function () {
                                                    $(this).dialog("close");
                                                }
                                            },
                                            closeOnEscape: false,
                                            width: "400",
                                            draggable: false,
                                            dialogClass: "LQBDialogBox",
                                            resizable: false
                                        });

                                        return;
                                    }

                                    // Show delete confimation dialog.
                                    $("#deleteModal").modal('show');
                                    var i=0, len=  $scope.vendorList.length;
                                    setRequiredFields(false);
                                    for (; i<len; i++) {

                                        if ($scope.vendorList[i].Id == vendorId) {
                                            $scope.selectedVendor = $scope.vendorList[i];
                                            break;
                                        }
                                    }
                                    $scope.$apply();
                                },
                                failure: function (response) { alert("error - " + response); },
                                error: function (XMLHttpRequest, textStatus, errorThrown) {
                                    alert(textStatus + errorThrown);
                                }
                            });
                        }
                    });
                     
                    function setRequiredFields(x){
                        $("input[id*='VendorName']").prop('required',x);
                        $("input[id*='ExportPath']").prop('required',x);
                        $("input[id*='VendorName']").data("required",true);
                        $("input[id*='ExportPath']").data("required",true);
                    }

                    function customOnSubmit(){
                        var xs = $("input,textarea,select")
                       .filter(":visible")
                       .filter(function () {
                           var $input = $(this);
                           return (($input.prop("required") === true) ||
                               ($input.data("required") == true));
                       });
                        xs.tooltip("destroy");
                        var ys = xs.filter(function (i, x) { return !(x.value); });
                        if (ys.length > 0) {
                            var y = ys.first();
                            y.tooltip({ title: "Required." });
                            y.focus();
                            y.one("change blur input", function () {
                                $(this).tooltip("destroy");
                            });
                            return false;
                        }
                    }

                </script>
                </div>
             </div>
        </div>
    </div>
  </div>
    <div class="container-fluid">
        <div class="row">
            <div class="center-div">
                <div>
                    <ul class="nav nav-tabs">
                        <li class="active" id="VendorTab"><a class="TabLink" data-panel="VendorPanel">Vendors</a></li>
                        <li id="PlatformTab"><a class="TabLink" data-panel="PlatformPanel">Platforms</a></li>
                    </ul>
                </div>
                <div class="panel panel-default Hidden" id="PlatformPanel">
                    <div class="panel-body div-wrap-tb">
                        <table class="table table-striped table-condensed table-layout-fix">
                            <thead>
                                <tr>
                                    <th class="NameRow">
                                        <a data-sort="true" data-sorttarget="PlatformName" data-asc="false">Platform</a>
                                    </th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="BaseCopy Hidden" data-platformid="">
                                    <td>
                                        <span class="PlatformName"></span>
                                    </td>
                                    <td>
                                        <a class="PlatformEdit">edit</a>
                                    </td>
                                    <td>
                                        <a class="PlatformDelete">delete</a>
                                    </td>
                                </tr>
                                <asp:Repeater runat="server" ID="Platforms">
                                    <ItemTemplate>
                                        <tr data-platformid="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PlatformId").ToString()) %>">
                                            <td>
                                                <span class="PlatformName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PlatformName").ToString()) %></span>
                                            </td>
                                            <td>
                                                <a class="PlatformEdit">edit</a>
                                            </td>
                                            <td>
                                                <a class="PlatformDelete">delete</a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>   
                            </tbody>
                        </table>
                        <span class="clipper">
                            <button type="button" class="btn btn-primary" id="AddPlatformBtn">Add New</button>
                        </span>
                    </div>
                </div>
                <div class="panel panel-default" id="VendorPanel">
                    <div class="panel-body div-wrap-tb">
                        <table class="table table-striped table-condensed table-layout-fix">
                            <thead>
                                <tr>
                                    <th class="NameRow">
                                        <a data-sort="true" data-sorttarget="CompanyName" data-asc="false">Vendor</a>
                                    </th>
                                    <th>
                                        <a data-sort="true" data-sorttarget="IsEnabled" data-asc="false">Enabled?</a>
                                    </th>
                                    <th>
                                        <a data-sort="true" data-sorttarget="IsTestVendor" data-asc="false">Test Vendor?</a>
                                    </th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="BaseCopy Hidden" data-vendorid="">
                                    <td>
                                        <span class="CompanyName"></span>
                                    </td>
                                    <td>
                                        <input type="checkbox" disabled="disabled" class="IsEnabled" />
                                    </td>
                                    <td>
                                        <input type="checkbox" disabled="disabled" class="IsTestVendor" />
                                    </td>
                                    <td>
                                        <a class="VendorEdit">edit</a>
                                    </td>
                                    <td>
                                        <a class="VendorDelete">delete</a>
                                    </td>
                                </tr>
                                <asp:Repeater runat="server" ID="Vendors" OnItemDataBound="Vendors_ItemDataBound">
                                    <ItemTemplate>
                                        <tr data-vendorid="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "VendorId").ToString()) %>">
                                            <td>
                                                <span class="CompanyName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CompanyName").ToString()) %></span>
                                            </td>
                                            <td>
                                                <input type="checkbox" disabled="disabled" class="IsEnabled" runat="server" id="IsEnabled"/>
                                            </td>
                                            <td>
                                                <input type="checkbox" disabled="disabled" class="IsTestVendor" runat="server" id="IsTestVendor"/>
                                            </td>
                                            <td>
                                                <a class="VendorEdit">edit</a>
                                            </td>
                                            <td>
                                                <a class="VendorDelete">delete</a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <span class="clipper">
                            <button type="button" class="btn btn-primary" id="AddVendorBtn">Add New</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PlatformConfiguration" class="modal v-middle fade Configuration" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 claSS="modal-title">Platform Configuration</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="PlatformId" />
                        <div class="form-inline">
                            <label class="input-label">Platform</label>
                            <input type="text" id="PlatformName" class="dialog-input form-control" />
                        </div>
                        <br />
                        <div>
                            Transmission Details
                        </div>
                        <div class="Bordered TransmissionDetails">
                            <div class="form-inline">
                                <label class="input-label">Transmission model</label>
                                <asp:DropDownList runat="server" ID="TransmissionModelT" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="form-inline">
                                <label class="input-label">Payload Format</label>
                                <asp:DropDownList runat="server" ID="PayloadFormatT" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="form-inline">
                                <label class="input-label">Target URL</label>
                                <input type="text" id="PTargetUrl" class="dialog-input form-control" />
                            </div>
                            <div class="form-inline">
                                <label class="input-label">Authentication</label>
                                <asp:DropDownList runat="server" ID="PTransmissionAuthenticationT" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <div class="form-inline DigitalCertField">
                                <div>
                                    <a class="To3rdPartyCertPage">Add Request Certificate to the Third Party Certificate Manager</a>
                                </div>
                                <div>
                                    <span>Note: Certificate's associated urls must contain this Transmission Detail's Target URL.</span>
                                </div>
                            </div>
                            <div class="form-inline DigitalCertField">
                                <label class="input-label">Response Cert ID</label>
                                <input type="text" id="PResponseCertificateId" class="dialog-input form-control" />
                            </div>
                            <div class="form-inline BasicAuthField">
                                <label class="input-label">Request Credential</label>
                                <label>Name</label>
                                <input type="text" id="PRequestCredentialName" class="form-control" />
                                <label>Password</label>
                                <input type="password" id="PRequestCredentialPassword" class="form-control" />
                            </div>
                            <div class="form-inline BasicAuthField">
                                <label class="input-label">Response Credential</label>
                                <label>Name</label>
                                <input type="text" id="PResponseCredentialName" class="form-control" />
                                <label>Password</label>
                                <input type="password" id="PResponseCredentialPassword" class="form-control" />
                            </div>
                            <div class="form-group checkbox">
                                <label class="control-label">
                                    <input type="checkbox" id="UsesAccountId" />
                                    This Platform uses an Account Id
                                </label>
                                <div class="indent">
                                    <label class="control-label">
                                        <input type="checkbox" id="RequiresAccountId" />
                                        This Platform requires an Account Id
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span class="clipper">
                            <input type="button" class="btn btn-default CancelBtn" id="PlatformCancelBtn" data-dismiss="modal" value="Cancel"/>
                        </span>
                        <span class="clipper">
                            <input type="button" class="btn btn-primary" id="PlatformSaveBtn" value="Save" />
                        </span>
                    </div>
                </div>
            </div>
        </div>
    <div id="VendorConfiguration" class="modal v-middle fade Configuration" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Vendor Configuration</h4>
                </div>
                <div class="modal-body">
                    <div class="form-inline">
                        <label class="input-label">Company Name</label>
                        <input type="text" id="CompanyName" class="dialog-input form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Vendor Id</label>
                        <input type="text" id="VendorId" class="form-control" disabled="disabled" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Enabled?</label>
                        <input type="checkbox" class="checkbox" id="IsEnabled" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Test Vendor?</label>
                        <input type="checkbox" class="checkbox" id="IsTestVendor" />
                    </div>
                    <br />
                    <div>Services</div>
                    <div class="Bordered">
                        <div id="QuotingSection" class="TransmissionDetails">
                            <div class="form-group checkbox">
                                <label class="control-label">
                                    <input type="checkbox" id="IsQuickQuotingEnabled" class="AlwaysEditable EnableCb" />
                                    Quick Quoting
                                </label>
                            </div>
                            <div class="form-group checkbox">
                                <label class="control-label">
                                    <input type="checkbox" id="IsDetailedQuotingEnabled" class="AlwaysEditable EnableCb" />
                                    Detailed Quoting
                                </label>
                            </div>
                            <div class="indent">
                                <div class="form-inline">
                                    <label class="input-label">Quoting Platform</label>
                                    <asp:DropDownList runat="server" ID="QuotingPlatformId" CssClass="form-control VendorPlatform" data-prefix="VQ"></asp:DropDownList>
                                    <label class="control-label checkbox">
                                        <input type="checkbox" id="IsOverridingQuotingPlatform" class="IsOverridingPlatform" data-prefix="VQ" />Override Defaults?
                                    </label>
                                </div>
                                <div class="form-inline">
                                <label class="input-label">Target URL</label>
                                    <input type="text" id="VQTargetUrl" class="dialog-input form-control" />
                                </div>
                                <div class="form-inline">
                                    <label class="input-label">Authentication</label>
                                    <asp:DropDownList runat="server" ID="VQTransmissionAuthenticationT" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="form-inline DigitalCertField">
                                    <div>
                                        <a class="To3rdPartyCertPage">Add Request Certificate to the Third Party Certificate Manager</a>
                                    </div>
                                    <div>
                                        <span>Note: Certificate's associated urls must contain this Transmission Detail's Target URL.</span>
                                    </div>
                                </div>
                                <div class="form-inline DigitalCertField">
                                    <label class="input-label">Response Cert ID</label>
                                    <input type="text" id="VQResponseCertificateId" class="dialog-input form-control" />
                                </div>
                                <div class="form-inline BasicAuthField">
                                    <label class="input-label">Request Credential</label>
                                    <label>Name</label>
                                    <input type="text" id="VQRequestCredentialName" class="form-control" />
                                    <label>Password</label>
                                    <input type="password" id="VQRequestCredentialPassword" class="form-control" />
                                </div>
                                <div class="form-inline BasicAuthField">
                                    <label class="input-label">Response Credential</label>
                                    <label>Name</label>
                                    <input type="text" id="VQResponseCredentialName" class="form-control" />
                                    <label>Password</label>
                                    <input type="password" id="VQResponseCredentialPassword" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div id="PolicyOrderingSection" class="TransmissionDetails">
                            <div class="form-group checkbox">
                                <label class="control-label">
                                    <input type="checkbox" id="IsPolicyOrderingEnabled" class="AlwaysEditable EnableCb" />
                                    Policy Ordering
                                </label>
                            </div>
                            <div class="indent">
                                <div class="form-inline">
                                    <label class="input-label">Order Platform</label>
                                    <asp:DropDownList runat="server" ID="PolicyOrderingPlatformId" CssClass="form-control VendorPlatform" data-prefix="VP"></asp:DropDownList>
                                    <label class="control-label checkbox">
                                        <input type="checkbox" id="IsOverridingPolicyOrderingPlatform" class="IsOverridingPlatform" data-prefix="VP" />Override Defaults?
                                    </label>
                                </div>
                                <div class="form-inline">
                                <label class="input-label">Target URL</label>
                                    <input type="text" id="VPTargetUrl" class="dialog-input form-control" />
                                </div>
                                <div class="form-inline">
                                    <label class="input-label">Authentication</label>
                                    <asp:DropDownList runat="server" ID="VPTransmissionAuthenticationT" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="form-inline DigitalCertField">
                                    <div>
                                        <a class="To3rdPartyCertPage">Add Request Certificate to the Third Party Certificate Manager</a>
                                    </div>
                                    <div>
                                        <span>Note: Certificate's associated urls must contain this Transmission Detail's Target URL.</span>
                                    </div>
                                </div>
                                <div class="form-inline DigitalCertField">
                                    <label class="input-label">Response Cert ID</label>
                                    <input type="text" id="VPResponseCertificateId" class="dialog-input form-control" />
                                </div>
                                <div class="form-inline BasicAuthField">
                                    <label class="input-label">Request Credential</label>
                                    <label>Name</label>
                                    <input type="text" id="VPRequestCredentialName" class="form-control" />
                                    <label>Password</label>
                                    <input type="password" id="VPRequestCredentialPassword" class="form-control" />
                                </div>
                                <div class="form-inline BasicAuthField">
                                    <label class="input-label">Response Credential</label>
                                    <label>Name</label>
                                    <input type="text" id="VPResponseCredentialName" class="form-control" />
                                    <label>Password</label>
                                    <input type="password" id="VPResponseCredentialPassword" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div>
                            <div class="form-group checkbox">
                                <label class="control-label">
                                    <input type="checkbox" id="UsesProviderCodes" class="AlwaysEditable" />
                                    Uses Product Codes?
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <span class="clipper">
                        <input type="button" class="btn btn-default CancelBtn" id="VendorCancelBtn" data-dismiss="modal" value="Cancel"/>
                    </span>
                    <span class="clipper">
                        <input type="button" class="btn btn-primary" id="VendorSaveBtn" value="Save" />
                    </span>
                </div>
            </div>
        </div>
    </div>
</form>

</asp:Content>