using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.LOAdmin.Manage
{
	public partial class SiteConfigEdit : LendersOffice.Admin.SecuredAdminPage
	{
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return an empty set.  Overload to provide
            // a page specific permission list.

            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.IsDevelopment
            };
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        protected int m_currentIndex = 0;
        
	     protected void PageLoad(object sender, System.EventArgs e)
		{
		}
        protected void PageInit(object sender, System.EventArgs e) 
        {
            m_loadLOdefaultScripts = false;
            RegisterJsScript("simpleservices.js");
            this.RegisterService("siteconfigedit", "/loadmin/Manage/SiteConfigEditService.aspx");
            this.RegisterJsGlobalVariables("CertificateThumbprint", ConstStage.IntermediateCertificateAuhorityThumbPrint);
        }

        protected override void OnPreRender(EventArgs e)
        {
            m_loadLOdefaultScripts = true;
            base.OnPreRender(e);
        }

        private string SafeJsString(string s) 
        {
            return s.Replace("'", "&#39;");

        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
		#endregion
	}
}
