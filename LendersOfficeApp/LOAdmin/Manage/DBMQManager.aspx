<%@ Page language="c#" Codebehind="DBMQManager.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Manage.DBMQManager" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/Common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../Common/Header.ascx" %>
<%@ Register TagPrefix="MQ" TagName="Manager" Src="Manager.ascx"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>DBMQManager</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout">
	
    <form id="DBMQManager" method="post" runat="server">
	<uc:Header runat="server" id=Header1>
			</uc:Header>
			<uc:HeaderNav runat="server" id=HeaderNav1>
				<MenuItem URL="~/LOAdmin/Main.aspx" Label="Main">
				</MenuItem>
				<MenuItem URL="~/LOAdmin/Manage/DBMQManager.aspx" Label="Message Queue Manager">
				</MenuItem>
			</uc:HeaderNav>
			<table style="WIDTH: 10in" cellspacing="0" cellpadding="0" border="0">
  <TBODY>
			<tr>
			<td class="FormTable">
				<br>
				<table style="WIDTH: 100%" cellspacing="0" cellpadding="0">
				<tr>
				<td class="FieldLabel" align="left">
					<input type="radio" name="Panel" onclick="location.href = 'TrackPanel.aspx';">
					Track
					<input type="radio" name="Panel" onclick="location.href = 'EmailPanel.aspx';">
					Email
					<input type="radio" name="Panel" onclick="location.href = 'AdminPanel.aspx';">
					Admin
					<input type="radio" name="Panel"  checked onclick="location.href = 'DBMQManager.aspx';">
					Message Queue
				</td>
				<td align="right">
					<input type="button" value="Back" onclick="location.href = '../Main.aspx';">
				</td>
				</tr>
				</table>
				<hr>
				
					
			<MQ:Manager runat=server ID="Manager1" NAME="Manager1"></MQ:Manager>
     </form></TR></TBODY></TABLE>
	
  </body>
</HTML>
