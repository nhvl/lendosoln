using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Admin
{
	/// <summary>
	///	Summary description for TraceLogging.
	/// </summary>

	public partial  class TraceLogging : System.Web.UI.UserControl
	{
		/// <summary>
		/// Initialize this control.
		/// </summary>

		protected System.Web.UI.WebControls.TextBox    m_Entries;

		private TraceProcessor m_tPr = new TraceProcessor();

		private String ErrorMessage
		{
			// Write out message.

			set
			{
				m_Error.Text = value.TrimEnd( '.' ) + ".";
			}
		}

		/// <summary>
		/// Render the current state.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Render the current state for display.

			try
			{
				m_MaxEntries.Text = m_tPr.MaxEntries.ToString();
				m_On.Text         = m_tPr.On.ToString();

				m_Entries.Text = "";

				foreach( String sMsg in m_tPr.Entries )
				{
					m_Entries.Text += sMsg + "\r\n";
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to render web form." , e );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PreRender += new System.EventHandler(this.PagePreRender);
		}
		#endregion

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void UpdateClick( object sender , System.EventArgs a )
		{
			// Update the tracing state.

			try
			{
				m_tPr.MaxEntries = Int32.Parse( m_MaxEntries.Text );
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to update processor." , e );
			}
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void ClearClick( object sender , System.EventArgs a )
		{
			// Update the tracing state.

			try
			{
				m_tPr.Clear();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to clear processor." , e );
			}
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void StartClick( object sender , System.EventArgs a )
		{
			// Update the tracing state.

			try
			{
				m_tPr.Start();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to start processor." , e );
			}
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void StopClick( object sender , System.EventArgs a )
		{
			// Update the tracing state.

			try
			{
				m_tPr.Stop();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to stop processor." , e );
			}
		}

	}

}
