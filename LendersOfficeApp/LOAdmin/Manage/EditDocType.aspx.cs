﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOfficeApp.common.ModalDlg;
using LendersOffice.Common;
using EDocs;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class EditDocType : LendersOffice.Admin.SecuredAdminPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void OnSaveBtnClicked(object sender, EventArgs e)
        {
            m_lblError.Text = "";
            EDocumentDocType edocs = new EDocumentDocType();

            try
            {
                //edocs.AddDocType(DocTypeName.Value, 13); // TODO - REMOVE THIS HARDCODED VALUE OF 13 and put the real value from the UI when it's ready
            }
            catch (DuplicateNameSelectedException exc)
            {
                m_lblError.Text = exc.UserMessage;
            }

            if (String.IsNullOrEmpty(m_lblError.Text))
                cModalDlg.CloseDialog(this);
        }
    }
}
