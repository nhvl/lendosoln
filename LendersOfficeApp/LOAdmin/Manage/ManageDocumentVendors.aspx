<%@ Page language="C#" Codebehind="ManageDocumentVendors.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageDocumentVendors"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Manage Document Vendors - LendingQB" %>

<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <div class="container"><div class="row"><div class="col-sm-8 col-sm-offset-2"><div class="panel">
        <form runat="server" class="form-group-sm">
            <div class="panel-body">
                <div class="form-group">
                    <div class="pull-right text-danger">
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="m_XmlFileUpload"
                            Display="Dynamic" ErrorMessage="Please select a file"/>
                    </div>
                    <label>LOXml Supplemental Data</label>
                    <asp:FileUpload ID="m_XmlFileUpload" runat="server" required/>
                </div>

                <div>
                    <asp:Button Text="Upload XML" runat="server" OnClick="OnUploadXmlClicked" CssClass="btn btn-default"/>
                    <asp:Button Text="Download XML" runat="server" OnClick="OnDownloadXmlClicked" CssClass="btn btn-default" formnovalidate CausesValidation="False"/>
                </div>
            </div>
            <hr/>
            <div class="panel-body">
                <div class="form-horizontal"><div class="form-group">
                    <label class="control-label col-sm-4">Download Log (8 hours from log)</label>
                    <div class="col-sm-4"><input type="text" id="CacheKey" class="form-control"/></div>
                    <a id="btnDownloadLog" class="btn btn-default">Download</a>
                    <ml:EncodedLabel runat="server" ID="DLLogError" EnableViewState="false" Visible="false" CssClass="text-danger">File not found.</ml:EncodedLabel>

                    <script>
                        $("#btnDownloadLog").on("click", function() {
                            var error = document.getElementById('DLLogError');

                            var key = document.getElementById('CacheKey').value;

                            if (error) {
                                error.style.display = 'none';
                            }

                            if (key.length < 1) {
                                alert('invalid key');
                                return;
                            }

                            window.location = window.location.pathname + "?cacheKey=" + encodeURIComponent(key);
                        });
                    </script>
                </div></div>
            </div>
        </form>
        <hr/>
        <div class="ng-cloak" ng-app="app">
            <vendor-manager></vendor-manager>
        </div>
    </div></div></div></div>
</asp:Content>

