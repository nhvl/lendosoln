using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Services;
using DataAccess;
using LendersOffice.Common;
using SecuredAdminPage = LendersOffice.Admin.SecuredAdminPage;
using LendersOffice.Integration.DocumentVendor;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class ManageDocumentVendors : SecuredAdminPage
    {
        [WebMethod]
        public static IEnumerable<VendorConfigData> Get()
        {
            if (SecuredAdminPageEx.IsMissingPermission(ManageDocumentVendorsPermissions)) { return null; }

            return Tools.LogAndThrowIfErrors(() =>
            {
                return DocumentVendorFactory.AvailableVendors()
                    .Where(p => p.VendorId != Guid.Empty)
                    .Select(x => new VendorConfigData(x));
            });
        }

        //[WebMethod] public
        static VendorConfigData GetDetail(Guid vendorId)
        {
            if (SecuredAdminPageEx.IsMissingPermission(ManageDocumentVendorsPermissions)) { return null; }

            var vendor = VendorConfig.Retrieve(vendorId);
            return new VendorConfigData(vendor);
        }

        [WebMethod]
        public static List<string> Update(VendorConfigData vendor)
        {
            if (SecuredAdminPageEx.IsMissingPermission(ManageDocumentVendorsPermissions)) { return null; }

            Uri primaryExportPath, backupExportPath, previewExportPath, testingExportPath;
            List<string> errorMessages = new List<string>();
            #region Validate
            primaryExportPath = vendor.PrimaryExportPath.ToUriOrNull();
            backupExportPath = vendor.BackupExportPath.ToUriOrNull();
            previewExportPath = vendor.PreviewExportPath.ToUriOrNull();
            testingExportPath = vendor.TestingExportPath.ToUriOrNull();
            if (string.IsNullOrWhiteSpace(vendor.VendorName)) errorMessages.Add("VendorName is required");
            if (string.IsNullOrWhiteSpace(vendor.TestAccountId)) errorMessages.Add("TestAccountId is required");
            if (string.IsNullOrWhiteSpace(vendor.LiveAccountId)) errorMessages.Add("LiveAccountId is required");
            if (string.IsNullOrWhiteSpace(vendor.PrimaryExportPath)) errorMessages.Add("PrimaryExportPath is required");
            else if (primaryExportPath == null) errorMessages.Add("PrimaryExportPath must be a valid url");
            if (!string.IsNullOrWhiteSpace(vendor.BackupExportPath) && backupExportPath == null) errorMessages.Add("BackupExportPath must be a valid url");
            if (!string.IsNullOrWhiteSpace(vendor.PreviewExportPath) && previewExportPath == null) errorMessages.Add("PreviewExportPath must be a valid url");
            if (!string.IsNullOrWhiteSpace(vendor.TestingExportPath) && testingExportPath == null) errorMessages.Add("TestingExportPath must be a valid url");
            #endregion

            if (errorMessages.Any())
            {
                return errorMessages;
            }

            var v = vendor.VendorId.HasValue ? VendorConfig.Retrieve(vendor.VendorId.Value) : new VendorConfig();
            v.VendorName = vendor.VendorName.Trim();
            v.TestAccountId = vendor.TestAccountId.Trim();
            v.LiveAccountId = vendor.LiveAccountId.Trim();
            v.PrimaryExportPath = primaryExportPath;
            v.BackupExportPath = backupExportPath;
            v.PreviewExportPath = previewExportPath;
            v.TestingExportPath = testingExportPath;

            string errorMessage;
            if (!v.TrySetPackageDataListJSON(vendor.PackageDataListJSON, out errorMessage))
            {
                return new List<string> { "Unable to set PackageDataListJSON: " + errorMessage };
            }

            v.ParameterName = vendor.ParameterName;
            v.ExtraParameters = vendor.ExtraParameters;

            v.UsesAccountId = vendor.UsesAccountId;
            v.UsesPassword = vendor.UsesPassword;
            v.UsesUsername = vendor.UsesUsername;

            v.SupportsOnlineInterface = vendor.SupportsOnlineInterface;
            v.EnableMismo34 = vendor.EnableMismo34;
            v.SupportsBarcodes = vendor.SupportsBarcodes;
            v.DisableManualFulfilmentAddr = vendor.DisableManualFulfilmentAddr;

            v.PlatformType = vendor.Platform;
            v.IsTestVendor = vendor.IsTestVendor;

            VendorConfig.Save(v);

            return null;
        }

        protected void OnUploadXmlClicked(object sender, EventArgs e)
        {
            if (!m_XmlFileUpload.HasFile) { return; }

            var postedFile = m_XmlFileUpload.PostedFile;
            byte[] file = new byte[postedFile.ContentLength];
            postedFile.InputStream.Read(file, 0, postedFile.ContentLength);
            VendorConfig.LOXmlExportFields = Encoding.ASCII.GetString(file);
            Response.Redirect(Request.RawUrl);
        }

        protected void OnDownloadXmlClicked(object sender, EventArgs e)
        {
            string xml = VendorConfig.LOXmlExportFields;
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition", "attachment; filename=\"tasks.xml\"");
            var bytes = new ASCIIEncoding().GetBytes(xml);
            Response.OutputStream.Write(bytes, 0, bytes.Length);
            Response.Flush();
            Response.End();
        }

        internal static readonly E_InternalUserPermissions[] ManageDocumentVendorsPermissions =
        {
            E_InternalUserPermissions.EditIntegrationSetup
        };

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.Manage.ManageDocumentVendors.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            OnDownloadLog();

            RegisterResources();
        }

        protected void OnDownloadLog()
        {
            string key = RequestHelper.GetSafeQueryString("cacheKey");
            if (string.IsNullOrEmpty(key)) return;


            if (SecuredAdminPageEx.IsMissingPermission(ManageDocumentVendorsPermissions))
            {
                return;
            }

            if (string.IsNullOrEmpty(key) || (key.StartsWith("DocVendorRequest_") == false && key.StartsWith("DocVendorResponse_") == false))
            {
                DLLogError.Visible = true;
                return;
            }

            try
            {
                bool inlineFile = RequestHelper.GetBool("inline");
                string file = AutoExpiredTextCache.GetFileFromCache(key);
                RequestHelper.SendFileToClient(Context, file, "text/xml", key + ".xml", inlineFile: inlineFile);
                Response.End();
            }
            catch (FileNotFoundException)
            {
                DLLogError.Visible = true;
            }
        }

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return ManageDocumentVendorsPermissions;
        }

        /// <summary>
        /// Export-able of VendorConfig
        /// </summary>
        public class VendorConfigData
        {
            public Guid? VendorId { get; set; }
            public string VendorName { get; set; }
            public string TestAccountId { get; set; }
            public string LiveAccountId { get; set; }

            public string PrimaryExportPath { get; set; }
            public string BackupExportPath { get; set; }
            public string PreviewExportPath { get; set; }
            public string TestingExportPath { get; set; }

            public string PackageDataListJSON { get; set; }
            public string ParameterName { get; set; }
            public string ExtraParameters { get; set; }

            public bool UsesAccountId { get; set; }
            public bool UsesPassword { get; set; }
            public bool UsesUsername { get; set; }

            public bool SupportsOnlineInterface { get; set; }
            public bool EnableMismo34 { get; set; }
            public bool SupportsBarcodes { get; set; }
            public bool DisableManualFulfilmentAddr { get; set; }

            public string errorMessage { get; set; }
            public bool EnableConnectionTest { get; set; }

            public E_DocumentVendor Platform { get; set; }
            public bool IsTestVendor { get; set; }

            public VendorConfigData() { }
            public VendorConfigData(VendorConfig v)
            {
                this.VendorId = v.VendorId;
                this.VendorName = v.VendorName;

                this.TestAccountId = v.TestAccountId;
                this.LiveAccountId = v.LiveAccountId;
                this.PrimaryExportPath = v.PrimaryExportPath?.AbsoluteUri;
                this.BackupExportPath = v.BackupExportPath?.AbsoluteUri;
                this.PreviewExportPath = v.PreviewExportPath?.AbsoluteUri;
                this.TestingExportPath = v.TestingExportPath?.AbsoluteUri;
                this.PackageDataListJSON = v.PackageDataListJSON;
                this.ParameterName = v.ParameterName;
                this.ExtraParameters = v.ExtraParameters;
                this.UsesAccountId = v.UsesAccountId;
                this.UsesPassword = v.UsesPassword;
                this.UsesUsername = v.UsesUsername;
                this.SupportsOnlineInterface = v.SupportsOnlineInterface;
                this.EnableMismo34 = v.EnableMismo34;
                this.SupportsBarcodes = v.SupportsBarcodes;
                this.DisableManualFulfilmentAddr = v.DisableManualFulfilmentAddr;
                this.EnableConnectionTest = v.EnableConnectionTest;
                this.Platform = v.PlatformType;
                this.IsTestVendor = v.IsTestVendor;
            }
        }
    }
}
