﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using LendersOffice.Integration.MortgageInsurance;
    using LendersOffice.Security;

    public partial class ManageMIVendors : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditIntegrationSetup
            };
        }

        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            RegisterJsScript("simpleservices.js"); //Load for using gService
            
            RegisterService("main", "/loadmin/manage/ManageMIVendorsService.aspx");

            if (!Page.IsPostBack)
            {
                DataAccess.Tools.Bind_sMiCompanyNmT(VendorType, DataAccess.E_sLT.Conventional);
                VendorType.Items.RemoveAt(0); //// Remove Leave Blank enum option.
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            //m_loadLOdefaultScripts is used to register gService
            m_loadLOdefaultScripts = true;
            base.OnPreRender(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Vendors.DataSource = MortgageInsuranceVendorConfig.ListActiveVendors();
            Vendors.DataBind();

            if (Vendors.Rows.Count != 0)
            {
                Vendors.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            if (IsPostBack)
            {
                Response.Redirect(Request.Url.ToString(), false);
            }
        }
    }
}