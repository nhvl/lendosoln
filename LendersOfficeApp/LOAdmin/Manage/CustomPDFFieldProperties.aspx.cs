﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.CustomFormField;
using LendersOffice.Security;
using System.Web.Services;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class CustomPDFFieldProperties : LendersOffice.Admin.SecuredAdminPage
    {
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {            
            if (!Page.IsPostBack)
            {
                var categoryItems=new List<FormFieldCategory>();
                categoryItems.Add(FormFieldCategory.Create("All"));
                categoryItems.AddRange(FormFieldCategory.ListAll());
                m_category.DataSource = categoryItems;
                m_category.DataTextField = "Name";
                m_category.DataValueField = "Id";
                m_category.DataBind();
                m_category.SelectedIndex = 0;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            RegisterJsScript("ractive-0.7.1.min.js");
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`

            this.Load += new System.EventHandler(this.PageLoad);
            base.OnInit(e);
        }
        [WebMethod]
        public static List<FormField> Search(string searchText, string category)
        {
            string search = searchText.Trim().ToLower();
            var searchResults = new List<FormField>();
            int catID;
            if (!int.TryParse(category.Trim(), out catID))
            {
                return searchResults;
            }
            var list = FormField.ListAll();
            foreach (FormField ff in list)
            {
                if (!string.IsNullOrEmpty(search) &&
                   (ff.Id.ToLower().IndexOf(search) != -1 ||
                   ff.FriendlyName.ToLower().IndexOf(search) != -1))
                {
                    if (catID == -1)
                    {
                        searchResults.Add(ff);
                        continue;
                    }
                    else
                    {
                        if (catID == ff.Category.Id)
                        {
                            searchResults.Add(ff);
                            continue;
                        }
                    }
                    continue;
                }
                if (string.IsNullOrEmpty(search))
                {
                    if (catID == -1)
                    {
                        searchResults.Add(ff);
                    }
                    else
                    {
                        if (catID == ff.Category.Id)
                        {
                            searchResults.Add(ff);
                        }
                    }
                }
            }
            return searchResults;
        }
        [WebMethod]
        public static List<FormField> Delete(string searchText, string category,string fieldId)
        {
            FormField.Delete(fieldId);
            return Search(searchText, category);
        }
    }
}
