﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Integration.Appraisals;
using DataAccess;
using System.Data.SqlClient;
using LendersOffice.Common;
using System.Web.Services;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class EditAppraisalVendor : LendersOffice.Admin.SecuredAdminPage
    {
        protected Guid VendorId
        {
            get
            {
                return RequestHelper.GetGuid("vendorid", Guid.Empty);
            }
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnInit(EventArgs e)
        {
            RegisterCSS("newdesign.main.css");
            RegisterCSS("newdesign.lendingqb.css");
            RegisterJsScript("jquery-1.11.2.min.js");
            RegisterJsScript("bootstrap-3.3.4.min.js");
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            base.OnInit(e);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
                DataAccess.Tools.Bind_sAppraisalNeededDate(AppraisalNeededDateOptions);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJquery = false;
            if (VendorId != Guid.Empty && !IsPostBack)
            {
                AppraisalVendorConfig vendor = AppraisalVendorConfig.Retrieve(VendorId);
                VId.Value = vendor.VendorId.ToString();
                VendorName.Text = vendor.VendorName;
                ExportPath.Text = vendor.PrimaryExportPath;
                UsesGlobalDMS.Checked = vendor.UsesGlobalDMS;
                UsesAccountId.Checked = vendor.UsesAccountId;
                //HideAppraisalNeededDate.Checked = vendor.HideAppraisalNeededDate;
                AppraisalNeededDateOptions.ClearSelection();
                AppraisalNeededDateOptions.SelectedValue = vendor.AppraisalNeededDateOption.ToString("D");
                HideNotes.Checked = vendor.HideNotes;
                UseStageExportPathCkd.Checked = vendor.ExportToStagePath;
                ShowAdditionalEmails.Checked = vendor.ShowAdditionalEmails;
                AllowOnlyOnePrimaryProduct.Checked = vendor.AllowOnlyOnePrimaryProduct;
                SendAuthTicketWithOrders.Checked = vendor.SendAuthTicketWithOrders;
                EnableConnectionTest.Checked = vendor.EnableConnectionTest;
                AllowBiDirectionalCommunication.Checked = vendor.EnableBiDirectionalCommunication;
            }
            ClientScriptManager cs = Page.ClientScript;
            cs.RegisterStartupScript(this.GetType(), "AKey", "f_usesGDMSClick();", true);
        }
        [WebMethod]
        public static bool Save(String VId, String VendorName, String ExportPath, bool UsesGlobalDMS, bool UsesAccountId, int AppraisalNeededDateOption, bool HideNotes, bool UseStageExportPath, bool AllowOnlyOnePrimaryProduct, bool ShowAdditionalEmails, bool SendAuthTicketWithOrders, bool EnableConnectionTest, bool AllowBiDirectionalCommunication)
        {
            AppraisalVendorConfig vendor;
            Guid VendorId = Guid.Empty;
            if (VId != "" && VId != null)
            {
                VendorId = new Guid(VId);
            }
            if (VendorId == Guid.Empty)
                vendor = new AppraisalVendorConfig();
            else
                vendor = AppraisalVendorConfig.Retrieve(VendorId);
            try
            {
                if (UsesGlobalDMS)
                    vendor.PrimaryExportPath = string.Empty;
                else
                {
                    var url = new Uri(ExportPath);
                    vendor.PrimaryExportPath = url.AbsoluteUri;
                }
            }
            catch (UriFormatException)
            {
                //throw new Exception("Export path must be a valid url");
                return false;
            }
            vendor.VendorName = VendorName;
            vendor.UsesAccountId = UsesAccountId;
            vendor.UsesGlobalDMS = UsesGlobalDMS;
            vendor.AppraisalNeededDateOption = (E_AppraisalNeededDateOptions)AppraisalNeededDateOption;
            vendor.HideNotes = HideNotes;
            vendor.ExportToStagePath = UseStageExportPath;
            vendor.AllowOnlyOnePrimaryProduct = AllowOnlyOnePrimaryProduct;
            vendor.ShowAdditionalEmails = ShowAdditionalEmails;
            vendor.SendAuthTicketWithOrders = SendAuthTicketWithOrders;
            vendor.EnableConnectionTest = EnableConnectionTest;
            vendor.EnableBiDirectionalCommunication = AllowBiDirectionalCommunication;
            AppraisalVendorConfig.Save(vendor);
            return true;
        }
    }
}
