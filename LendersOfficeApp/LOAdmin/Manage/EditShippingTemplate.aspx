﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditShippingTemplate.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.EditShippingTemplate" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="so" TagName="StackingOrderUserControl" Src="../../los/ElectronicDocs/StackingOrder.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Edit Shipping Template</title>
	<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body>
    <script language="javascript">
        function _init() {
            resize(500, 350);
            document.getElementById('m_ShippingTemplateName').focus();
        }

        function addDocType() {
            <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = '';    
            var ddl = <%= AspxTools.JsGetElementById(m_ddlDocTypes) %>;
            var docTypeName = ddl.options[ddl.selectedIndex].text;
            var docTypeId = ddl.options[ddl.selectedIndex].value;
            
            if(checkListForId(docTypeId))
                addToList(docTypeName, docTypeId);
            else
                <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = 'Doc type has already been added to the order';    
        }
        
        function validatePage() {
            var valid = true;
            var name = <%= AspxTools.JsGetElementById(m_ShippingTemplateName) %>.value.replace(new RegExp("[\\s]+$"), "");
            var nameR = document.getElementById('m_ShippingTemplateNameR');
            
            if(name == '') {
                valid = false;
                nameR.style.display = '';
            }
            else
                nameR.style.display = 'none';  
            
            if(getStackingOrderSize() == 0) {
                valid = false;
                <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = 'Please add a doc type to the template before saving';                
            }
            else
                <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = '';
                
            return valid;
        }
        
        function onSaveBtnClicked() {
            if(!validatePage())
                return;
        
            <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = '';    
            var args = new Object();
            args["order"] = getOrder();
            args["id"] = <%= AspxTools.JsGetElementById(m_ShippingTemplateId) %>.value;
            args["name"] = <%= AspxTools.JsGetElementById(m_ShippingTemplateName) %>.value;
            
            var res = gService.main.call("Save", args );
            if (!res.error) {
                if ( res.value.invalidOp == 'True' )
                    <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = res.value.userMsg;
                else
                    onClosePopup();
            }
            else
                alert(res.UserMessage);
        }
    </script>
    <h4 class="page-header"><ml:EncodedLabel ID="m_PageHeader" runat="server"></ml:EncodedLabel></h4>
    <form id="form1" runat="server">
        <asp:HiddenField ID="m_ShippingTemplateId" runat="server" />
		<table style="padding:10px" cellspacing="8">
			<tr>
			    <td class="FieldLabel" noWrap>
			        Template Name: <asp:TextBox runat="server" ID="m_ShippingTemplateName" Width="160px" MaxLength="50"></asp:TextBox>
					<img id="m_ShippingTemplateNameR" src="../../images/error_icon.gif" style="display:none" />
                </td>
			</tr>
			<tr>
			    <td>
			        <so:StackingOrderUserControl id="m_StackingOrder" runat="server" Width="100%">
					</so:StackingOrderUserControl>
			    </td>
			</tr>
			<tr>
			    <td>
			        <asp:DropDownList ID="m_ddlDocTypes" runat="server"></asp:DropDownList>
		            <input type="button" value="Add" onclick="addDocType();" style="width:40px" />
	            </td>
			</tr>
			<tr>
			    <td>
			        <ml:EncodedLabel ID="m_lblError" runat="server" ForeColor="Red"></ml:EncodedLabel>&nbsp;
			    </td>
			</tr>
			<tr>
			    <td align="left">
			        <input type="button" value="Save" onclick="onSaveBtnClicked();" />
			        <input type="button" value="Cancel" onclick="onClosePopup();" />
			    </td>
			</tr>
		</table>	
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
</body>
</html>
