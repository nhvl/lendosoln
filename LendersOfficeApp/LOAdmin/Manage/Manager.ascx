<%@ Control Language="c#" AutoEventWireup="false" Codebehind="Manager.ascx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.Manager" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<div>
	<ML:EncodedLabel ID="m_ErrorMessage" EnableViewState="False" Runat="server"/>
	<div style="BORDER-RIGHT: lightgrey 2px solid; PADDING-RIGHT: 4px; BORDER-TOP: lightgrey 2px solid; MARGIN-TOP: 4px; OVERFLOW-Y: auto; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: lightgrey 2px solid;  PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; MAX-HEIGHT: 364px">
		<div style="font-weight: bold;font-size: 12px;color: white;font-family: Arial, Helvetica, sans-serif;	background-color: #003366; Padding: .5em" >
			Message Queues
		</div>
		<div style="float: left; width:70%" >

			<ml:CommonDataGrid id="m_QGrid" runat="server"  AutoGenerateColumns="false"   OnItemDataBound="QGrid_ItemDataBound" CellSpacing="0" CellPadding="2" EnableViewState="True"  OnItemCommand="QGrid_ItemCommand" OnDeleteCommand="QGrid_Delete">
				<Columns>
					<asp:ButtonColumn Text="Delete" CommandName="Delete" />
					<asp:BoundColumn DataField="QueueId" HeaderText="Id"/> 
					<asp:BoundColumn DataField="QueueName" HeaderText="Name"/> 
					<asp:BoundColumn DataField="RequiresArchive" HeaderText="Archiving?"/> 
					<asp:ButtonColumn Text="Toggle Archive" CommandName="Toggle Archive"/>
					<asp:ButtonColumn Text="Show Messages" CommandName="SetMessageId"/>
				</Columns>
			</ml:CommonDataGrid>
		</div>
		<div style="float: right;">
			<table style="border: 2px solid lightgray; margin: 5px;"> 
				<tr>
					<th colspan="2"> Add Queue</th>
				</tr>
				<tbody>
					<tr>
						<td>Name</td>
						<td><asp:TextBox Runat="server" ID="m_QName"> </asp:TextBox></td>
					</tr>
					<tr>
						<td>Requires Archive</td>
						<td> 
							<asp:RadioButtonList ID="m_QArchiveRL" Runat="server" > 
								<asp:ListItem id="ArchiveOn" runat="server" Value="On" Selected/>
								<asp:ListItem id="ArchiveOff" runat="Server" Value="Off" />
							 </asp:RadioButtonList>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<asp:Button Text="Submit" Runat="server" OnClick="SbmtBtn_AddQueue"/> 
						</td>
					</tr>
				</tbody>
		
			</table>
		</div>
	</div>
	<br/>
	<br/>

	<asp:Panel Runat=server ID="q_MessagePanel">
		<div style="clear: both">
			<table style="width: 100%">
				<tr  style="font-weight: bold;font-size: 12px;color: white;font-family: Arial, Helvetica, sans-serif;	background-color: #003366; Padding: .5em">
					<td colspan="3" style="PADDING: .5em">
						Messages <%=AspxTools.HtmlString(CurrentQueueName == null ? "" : " For Queue : " + CurrentQueueName)%> 
					</td>	
					<td align="right" colspan="3"> 
						<asp:Button Runat=server ID="DeleteMessagesButton" Text="Clear Messages" OnClick="DeleteMessageBtn_Click"> </asp:Button>

					</td>
			</tr>		
				</tr>
				<tr>
					<td colspan="6" >
						<ml:CommonDataGrid id="m_QMGrid" runat="server" CellSpacing="0" CellPadding="2" EnableViewState="True" OnItemCommand="Grid_OnClick">
							<Columns>
								<asp:ButtonColumn Text="Drop" CommandName="DropMessage"/> 
								<asp:BoundColumn DataField="Id" HeaderText="Id"></asp:BoundColumn>
								<asp:BoundColumn DataField="Priority" HeaderText="Priority"></asp:BoundColumn>
								<asp:BoundColumn DataField="InsertionTime" HeaderText="Time Added"></asp:BoundColumn>
								<asp:BoundColumn DataField="Subject1" HeaderText="Subject1"></asp:BoundColumn>
								<asp:BoundColumn DataField="Subject2" HeaderText="Subject2"></asp:BoundColumn>
								<asp:BoundColumn DataField="DataLocation" HeaderText="Data Location"></asp:BoundColumn>
							</Columns>
						</ml:CommonDataGrid>
					</td>
				</tr>
				<tr style="border: 2px solid black ">
					<td>
						Send Message 
					</td>
					<td> 
						Priority: 
						<asp:DropDownList Runat="server" ID="m_Priority"> 
							<asp:ListItem Value="1"/> 
							<asp:ListItem Value="2"/> 
							<asp:ListItem Value="3" Selected/> 
							<asp:ListItem Value="4"/> 
							<asp:ListItem Value="5"/> 
						</asp:DropDownList>
					</td>
					<td>
						Subject1: <asp:TextBox id="m_Subject1" runat="server" MaxLength="300"/>
					</td>
					<td>
						Subject2: <Asp:TextBox id="m_Subject2" runat="Server" MaxLength="300"/>
					</td>
					<td>
						Data Location:
						<asp:DropDownList Runat="server" ID="DatalocList">
							<asp:ListItem Value="db" Selected/>
							<asp:ListItem Value="filedb"/>
						 </asp:DropDownList>
					</td>
					<td> <asp:Button ID="m_SndMsg" Runat="server" Text="Send" OnClick="m_SndMsg_Click"/> </td>
				</tr>
			</table>
		</div>
	</asp:Panel>
	<br/>
	<br/>
	<asp:Panel Runat=server ID="m_qArchive">
	
		<div>
			<table style="width: 100%;">
					<tr  style="font-weight: bold;font-size: 12px;color: white;font-family: Arial, Helvetica, sans-serif;	background-color: #003366; ">
						<td style="PADDING: .5em">
							Archived Messages <%=AspxTools.HtmlString(CurrentQueueName == null ? "" : " For Queue : " + CurrentQueueName) %> 
						</td>
						<td align="right"> 
						
									
						Size 
								<asp:TextBox ID="ArchiveSizeBox" Runat="server" TextMode=SingleLine> </asp:TextBox>
								<asp:Button ID="m_UpdateSize" Runat="server" Text="Update Size" OnClick="UpdateSize_Click"/>
								<asp:Button Runat=server ID="ClearArchiveButton" Text="Clear Messages" OnClick="ClrArcBtn_Click"> </asp:Button>
						 </td>
					<tr>
					<td colspan="2">
					
						<ml:CommonDataGrid id="m_ArchiveGrid" runat="server" CellSpacing="0" CellPadding="2" EnableViewState="True" OnItemCommand="Grid_OnClick">
							<Columns>
								<asp:ButtonColumn Text="Delete" CommandName="DropArchiveMessage"/> 
								<asp:ButtonColumn Text="Move To Queue" CommandName="MoveToMessage"/> 
								<asp:BoundColumn DataField="MessageId" HeaderText="Id"></asp:BoundColumn>
								<asp:BoundColumn DataField="Priority" HeaderText="Priority"></asp:BoundColumn>
								<asp:BoundColumn DataField="InsertionTime" HeaderText="Time Added"></asp:BoundColumn>
								<asp:BoundColumn DataField="RemovalTime" HeaderText="Time Removed"></asp:BoundColumn>
								<asp:BoundColumn DataField="Subject1" HeaderText="Subject1"></asp:BoundColumn>
								<asp:BoundColumn DataField="Subject2" HeaderText="Subject2"></asp:BoundColumn>
								<asp:BoundColumn DataField="DataLoc" HeaderText="Data Location"></asp:BoundColumn>
							</Columns>
						</ml:CommonDataGrid>
					</td>
				</tr>
			</table>
		</div>
		
	</asp:Panel>