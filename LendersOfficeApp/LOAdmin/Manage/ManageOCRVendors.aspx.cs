﻿// <copyright file="ManageOCRVendors.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.OCR;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Management page for the OCR vendors.
    /// </summary>
    public partial class ManageOCRVendors : SecuredAdminPage
    {
        /// <summary>
        /// The service to manage vendors.
        /// </summary>
        private OCRService service; 

        /// <summary>
        /// The Page Load Event.
        /// </summary>
        /// <param name="sender">The object that triggered it.</param>
        /// <param name="e">The arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.service = new OCRService(PrincipalFactory.CurrentPrincipal);
            this.EnableJquery = true;

            if (!this.IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        /// Gets the jquery version to use on the page.
        /// </summary>
        /// <returns>The version of jquery in use.</returns>
        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_11_2; 
        }

        /// <summary>
        /// The Save handler for the vendor save button.
        /// </summary>
        /// <param name="sender">The save button.</param>
        /// <param name="e">The event arguments.</param>
        protected void Save_OnClick(object sender, EventArgs e)
        {
            Vendor vendor = new Vendor();
            vendor.IsValid = this.editChecbox.Checked;
            vendor.EnableConnectionTest = this.enableConnectionTestCheckbox.Checked;
            vendor.PortalURL = this.editUrl.Text;
            vendor.DisplayName = this.editName.Text;
            vendor.Id = new Guid(this.editVendorId.Value);
            vendor.UploadURL = this.uploadUrl.Text;
            this.service.SaveVendor(vendor);
            Response.Redirect(Request.RawUrl);
        }

        /// <summary>
        /// The on click for the delete vendor button.
        /// </summary>
        /// <param name="sender">The object that triggered the event.</param>
        /// <param name="e">The event arguments.</param>
        protected void DeleteVendor_OnClick(object sender, EventArgs e)
        {
            Guid id = new Guid(this.editVendorId.Value);
            this.service.DeleteVendor(id);
            Response.Redirect(Request.RawUrl);
        }

        /// <summary>
        /// Binds the vendors to the vendor list.
        /// </summary>
        protected void BindData()
        {
            this.OCRVendorList.DataSource = this.service.GetVendors();
            this.OCRVendorList.DataBind();
            this.editVendorId.Value = Guid.Empty.ToString();
        }

        /// <summary>
        /// Maps the vendor to the current row in the vendor list. 
        /// </summary>
        /// <param name="sender">The repeater itself.</param>
        /// <param name="args">The repeater item arguments.</param>
        protected void OCRVendorList_OnItemBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            Vendor vendor = (Vendor)args.Item.DataItem;
            EncodedLiteral name = (EncodedLiteral)args.Item.FindControl("name");
            EncodedLiteral url = (EncodedLiteral)args.Item.FindControl("portalUrl");
            EncodedLiteral enabled = (EncodedLiteral)args.Item.FindControl("valid");
            EncodedLiteral enableConnectionTest = (EncodedLiteral)args.Item.FindControl("enableConnectionTest");
            HtmlTableRow row = (HtmlTableRow)args.Item.FindControl("row");
            HtmlAnchor editAnchor = (HtmlAnchor)args.Item.FindControl("edit");

            editAnchor.Attributes.Add("data-id", vendor.Id.ToString());
            editAnchor.Attributes.Add("data-portalUrl", vendor.PortalURL);
            editAnchor.Attributes.Add("data-valid", vendor.IsValid.ToString());
            editAnchor.Attributes.Add("data-enableConnectionTest", vendor.EnableConnectionTest.ToString());
            editAnchor.Attributes.Add("data-name", vendor.DisplayName);
            editAnchor.Attributes.Add("data-uploadUrl", vendor.UploadURL);

            name.Text = vendor.DisplayName;
            url.Text = vendor.PortalURL;
            enabled.Text = vendor.IsValid ? "Enabled" : "Disabled";
            enableConnectionTest.Text = vendor.EnableConnectionTest ? "Enabled" : "Disabled";

            string cssClass = "GridItem";

            if (args.Item.ItemType == ListItemType.AlternatingItem)
            {
                cssClass = "GridAlternatingItem";
            }

            row.Attributes.Add("class", cssClass);
        }
    }
}