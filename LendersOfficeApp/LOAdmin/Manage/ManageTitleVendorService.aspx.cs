﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using CommonProjectLib.Common;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;

    /// <summary>
    /// Service class for the ManageTitleVendor page.
    /// </summary>
    public partial class ManageTitleVendorService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Runs the service method called from the page.
        /// </summary>
        /// <param name="methodName">The method to run.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.RetrievePlatform):
                    this.RetrievePlatform();
                    break;
                case nameof(this.DeletePlatform):
                    this.DeletePlatform();
                    break;
                case nameof(this.SavePlatform):
                    this.SavePlatform();
                    break;
                case nameof(this.RetrieveVendor):
                    this.RetrieveVendor();
                    break;
                case nameof(this.DeleteVendor):
                    this.DeleteVendor();
                    break;
                case nameof(this.SaveVendor):
                    this.SaveVendor();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, $"Method not implemented {methodName}");
            }
        }

        /// <summary>
        /// Retrieves the platform using a platform id.
        /// </summary>
        private void RetrievePlatform()
        {
            int platformId = this.GetInt("PlatformId");
            TitlePlatform platform = TitlePlatform.LoadPlatform(platformId);
            if (platform == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", $"Platform with platform id {platform} not found.");
            }
            else
            {
                TitlePlatformViewModel viewModel = platform.ToViewModel();
                this.SetResult("PlatformViewModel", SerializationHelper.JsonNetSerialize(viewModel));
                this.SetResult("Success", true);
            }
        }

        /// <summary>
        /// Deletes a platform.
        /// </summary>
        private void DeletePlatform()
        {
            int platformId = this.GetInt("PlatformId");
            string errors;
            if (!TitlePlatform.DeletePlatform(platformId, out errors))
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", errors);
            }
            else
            {
                this.SetResult("Success", true);
            }
        }

        /// <summary>
        /// Saves the platform.
        /// </summary>
        private void SavePlatform()
        {
            TitlePlatformViewModel viewModel = SerializationHelper.JsonNetDeserialize<TitlePlatformViewModel>(GetString("PlatformViewModel"));
            string errors;
            TitlePlatform platform = TitlePlatform.CreateFromViewModel(viewModel, out errors);
            if (platform == null)
            {
                this.SetResult("Errors", errors);
                this.SetResult("Success", false);
                return;
            }

            bool isNew = platform.IsNew;
            platform.SavePlatform();

            this.SetResult("Success", true);
            this.SetResult("PlatformName", platform.PlatformName);
            this.SetResult("PlatformId", platform.PlatformId);
            this.SetResult("IsNew", isNew);
        }

        /// <summary>
        /// Loads a vendor with a vendor id.
        /// </summary>
        private void RetrieveVendor()
        {
            int vendorId = this.GetInt("VendorId");
            TitleVendor vendor = TitleVendor.LoadTitleVendor(vendorId);
            if (vendor == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", $"Unable to load vendor with id {vendorId}");
            }
            else
            {
                TitleVendorViewModel viewModel = vendor.ToViewModel();
                this.SetResult("VendorViewModel", SerializationHelper.JsonNetSerialize(viewModel));
                this.SetResult("Success", true);
            }
        }

        /// <summary>
        /// Deletes the vendor.
        /// </summary>
        private void DeleteVendor()
        {
            int vendorId = this.GetInt("VendorId");
            string errors;
            if (!TitleVendor.DeleteVendor(vendorId, out errors))
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", errors);
            }
            else
            {
                this.SetResult("Success", true);
            }
        }

        /// <summary>
        /// Saves the vendor.
        /// </summary>
        private void SaveVendor()
        {
            TitleVendorViewModel viewModel = SerializationHelper.JsonNetDeserialize<TitleVendorViewModel>(GetString("VendorInfoViewModel"));
            string errors;
            TitleVendor vendor = TitleVendor.CreateFromViewModel(viewModel, out errors);
            if (vendor == null)
            {
                this.SetResult("Errors", errors);
                this.SetResult("Success", false);
                return;
            }

            bool isNew = vendor.IsNew;
            vendor.Save();
            this.SetResult("Success", true);
            this.SetResult("CompanyName", vendor.CompanyName);
            this.SetResult("VendorId", vendor.VendorId);
            this.SetResult("IsEnabled", vendor.IsEnabled);
            this.SetResult("IsTestVendor", vendor.IsTestVendor);
            this.SetResult("IsNew", isNew);
        }
    }
}