﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorAdoption.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.VendorAdoption" MasterPageFile="~/LOAdmin/loadmin.Master" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <style>
        .adoption-width{
            max-width: 1250px;
        }
    </style>
    <script>
        var emailList;
        function retrieveVendorName() {
            var inputData = { integrationId: $('#integrationType').children("option:selected").val().trim() }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'VendorAdoption.aspx/GetVendors',
                data: JSON.stringify(inputData),
                dataType: 'json',
                async: false,
                success: function (data) {
                    $('#vendorName')
                    .find('option')
                    .remove()
                    .end()
                    .append($('<option/>', {
                        value: "",
                        text: "Select...",
                        disabled: true,
                        hidden: true,
                        selected: true
                    }));
                    $.each(data.d, function (i, v) {
                        $('#vendorName').append($('<option/>', {
                            value: v.Key,
                            text: v.Value
                        }));
                    });
                    enableButton('btnSearch', $('#vendorName option').length == 1);
                },
                error: function () {
                    alert("System error.");
                }
            });
        }
        function enableButton(btnId, enabled) {
            if (enabled) {
                $('#' + btnId).removeAttr('disabled');
            }
            else {
                $('#' + btnId).attr('disabled', 'disabled');
            }
        }
        function search() {
            var integrationId = $('#integrationType').children("option:selected").val().trim();
            var vendorId = "";
            var genericFrameworkProviderId = "";
            var serviceVendorId = "";
            switch (integrationId) {
                case "10":
                    //Generic Framework
                    genericFrameworkProviderId = $('#vendorName').children("option:selected").val().trim();
                    break;
                case "16": //Title
                case "17": //VOX
                    serviceVendorId = $('#vendorName').children("option:selected").val().trim();
                    break;
                default:
                    vendorId = $('#vendorName').children("option:selected").val().trim();
            }
            var inputData = {
                integrationId: integrationId,
                vendorId: vendorId,
                genericFrameworkProviderId: genericFrameworkProviderId,
                serviceVendorId: serviceVendorId
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'VendorAdoption.aspx/Search',
                data: JSON.stringify(inputData),
                dataType: 'json',
                async: false,
                success: function (data) {
                    emailList = "";
                    $('#clientList tbody')
                    .find('tr')
                    .remove()
                    .end();
                    $.each(data.d, function (i, v) {
                        $('#clientList tbody').append('<tr><td>'
                            + HtmlEncode(v.Client) + '</td><td>'
                            + HtmlEncode(v.PMLCode) + '</td><td>'
                            + HtmlEncode(v.AdminUser) + '</td><td>'
                            + HtmlEncode(v.AdminEmail) + '</td></tr>');
                        emailList += v.AdminEmail + "; ";
                    });
                    enableButton('btnCopy', $('#clientList tbody tr').length != 0);
                    $("#clientList").trigger("update");
                },
                error: function () {
                    alert("System error.");
                }
            });
        }
        function copy() {
            if (copyToClipboard(emailList)) {
                alert("Emails are copied to your windows clipboard.");
            }
        }
        $(function () {
            $("#clientList").tablesorter();
            $("#clientList").on("sortEnd", function () {
                $(this).find('th span[class^=glyphicon]').remove();
                $(this).find('th[class*=headerSortDown] a').append('<span class="glyphicon glyphicon-sort-by-attributes"></span>');
                $(this).find('th[class*=headerSortUp] a').append('<span class="glyphicon glyphicon glyphicon-sort-by-attributes-alt"></span>');
            });
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'VendorAdoption.aspx/GetIntegrationTypes',
                data: null,
                dataType: 'json',
                async: false,
                success: function (data) {
                    $('#integrationType')
                    .find('option')
                    .remove()
                    .end()
                    .append($('<option/>', {
                        value: "",
                        text: "Select...",
                        disabled: true,
                        hidden: true,
                        selected: true
                    }));
                    $.each(data.d, function (i, v) {
                        $('#integrationType').append($('<option/>', {
                            value: v.Key,
                            text: v.Value
                        }));
                    });
                },
                error: function () {
                    alert("System error.");
                }
            });
        });
    </script>
    <div class="container-fluid adoption-width">
        <div class="row">
            <div class="center-div">
                <div class="panel panel-default">
                    <div class="panel-body div-wrap-tb">
                        <h4 class="form-group col-xs-offset-2">Vendor Adoption</h4>
                        <div class="row form-group">    
                            <div class="col-xs-4 text-right">
                                <label>Integration Type</label>
                            </div>
                            <div class="col-xs-3">
                                <select id="integrationType" class="form-control input-sm" onchange="retrieveVendorName()"></select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-4 text-right">
                                <label>Vendor Name</label>
                            </div>
                            <div class="col-xs-3">    
                                <select id="vendorName" class="form-control input-sm" onchange="enableButton('btnSearch',this.value!='')">
                                    <option value="" selected disabled hidden>Select...</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-4"></div>
                            <div class="col-xs-6">    
                                <span class="clipper"><button id="btnSearch" type="button" class="btn btn-primary" onclick="search()" disabled>Search</button></span>
                            </div>
                        </div>
                        
                        <table id="clientList" class="table table-striped table-condensed table-layout-fix">
                            <thead>
                                <tr>
                                    <th class="col-xs-1"><a>Client</a></th>
                                    <th class="col-xs-1"><a>PML Code</a></th>
                                    <th class="col-xs-4"><a>Admin User</a></th>
                                    <th class="col-xs-6"><a>Admin Email</a></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <span class="clipper pull-right"><button id="btnCopy" type="button" class="btn btn-default" onclick="copy()" disabled>Copy list of emails to clipboard</button></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
