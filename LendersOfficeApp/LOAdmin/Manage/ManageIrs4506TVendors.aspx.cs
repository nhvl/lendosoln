﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Integration;
    using LendersOffice.Integration.Irs4506T;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    public partial class ManageIrs4506TVendors : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[]
            {
                E_InternalUserPermissions.EditIntegrationSetup
            };
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            RegisterJsScript("simpleservices.js");//Load for using gService
            
            RegisterService("main", "/loadmin/manage/ManageIrs4506TVendorsService.aspx");
        }
        protected override void OnPreRender(EventArgs e)
        {
            //m_loadLOdefaultScripts is used to register gService
            m_loadLOdefaultScripts = true;
            base.OnPreRender(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var activeVendors = Irs4506TVendorConfiguration.ListActiveVendor();
            Vendors.DataSource = activeVendors;
            Vendors.DataBind();

            if (Vendors.Rows.Count != 0)
            {
                Vendors.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            if (IsPostBack)
            {
                return;
            }
            else
            {
                var lqbModelActiveVendors = activeVendors.Where(v => v.CommunicationModel == Irs4506TCommunicationModel.LendingQB);
                this.VendorDropdownForUsingXslt.Items.Add(new ListItem("--None--", string.Empty));
                this.VendorDropdownForUsingXslt.Items.AddRange(lqbModelActiveVendors.Select(v => new ListItem(v.VendorName, v.VendorId.ToString())).ToArray());

                this.LoXmlFileDropDown.Items.AddRange(SharedDbBackedFilesManager.GetAllLoXmlFileInfos().Select(t => new ListItem(t.FileName, t.Id.ToString())).ToArray());
                this.XslFileDropDown.Items.AddRange(SharedDbBackedFilesManager.GetAllXslFiles().Select(t => new ListItem(t.FileName, t.Id.ToString())).ToArray());

                var requestFormatDetails = Irs4506TVendorConfiguration.Temp_GetOnlyVendorIntegrationRequestFormat(PrincipalFactory.CurrentPrincipal);
                if (requestFormatDetails != null)
                {
                    this.VendorDropdownForUsingXslt.SelectedValue = requestFormatDetails.VendorId.ToString();
                    this.LoXmlFileDropDown.SelectedValue = requestFormatDetails.LoXmlFileId.ToString();
                    this.XslFileDropDown.SelectedValue = requestFormatDetails.XslFileId.ToString();
                }
                else
                {
                    // just let the loxml and xslfile dropdown be the first one for now.
                    this.VendorDropdownForUsingXslt.SelectedValue = string.Empty;                    
                }
            }
        }

        protected void Click_SetTestVendorAndFiles(object sender, EventArgs e)
        {
            int? loXmlFileId = null;
            if (this.LoXmlFileUpload.HasFile)
            {
                var tempFilePath = TempFileUtils.NewTempFilePath();
                this.LoXmlFileUpload.SaveAs(tempFilePath);
                var localFilePath = LocalFilePath.Create(tempFilePath).Value;
                var uniqueName = this.LoXmlFileUpload.FileName.Trim();
                loXmlFileId = SharedDbBackedFilesManager.AddFileToDbAndFileDb(PrincipalFactory.CurrentPrincipal, localFilePath, uniqueName, SharedDbBackedFileType.LoXml);
                this.LoXmlFileDropDown.Items.Add(new ListItem(uniqueName, loXmlFileId.Value.ToString()));
                this.LoXmlFileDropDown.SelectedValue = loXmlFileId.Value.ToString();
            }

            int? xslFileId = null;
            if (this.XslFileUpload.HasFile)
            {
                var tempFilePath = TempFileUtils.NewTempFilePath();
                this.XslFileUpload.SaveAs(tempFilePath);
                var localFilePath = LocalFilePath.Create(tempFilePath).Value;
                var uniqueName = this.XslFileUpload.FileName.Trim();
                xslFileId = SharedDbBackedFilesManager.AddFileToDbAndFileDb(PrincipalFactory.CurrentPrincipal, localFilePath, uniqueName, SharedDbBackedFileType.Xsl);
                this.XslFileDropDown.Items.Add(new ListItem(uniqueName, xslFileId.Value.ToString()));
                this.XslFileDropDown.SelectedValue = xslFileId.Value.ToString();
            }

            Guid? selectedVendorId = null;
            string selectedVendorIdValue = VendorDropdownForUsingXslt.SelectedValue;
            if(!string.IsNullOrEmpty(selectedVendorIdValue))
            {
                selectedVendorId = Guid.Parse(selectedVendorIdValue);
            }

            if(selectedVendorId == null)
            {
                Irs4506TVendorConfiguration.Temp_RemoveOnlyVendorIntegrationRequestFormat(PrincipalFactory.CurrentPrincipal);
            }
            else
            {
                var details = new VendorSpecificIntegrationRequestFormat()
                {
                    VendorId = selectedVendorId.Value,
                    IntegrationRequestType = IntegrationRequestType.Irs4506T_Order,
                    LoXmlFileId = Int32.Parse(this.LoXmlFileDropDown.SelectedValue),
                    XslFileId = Int32.Parse(this.XslFileDropDown.SelectedValue)
                };

                Irs4506TVendorConfiguration.Temp_SetOnlyVendorIntegrationRequestFormat(PrincipalFactory.CurrentPrincipal, details);
            }
        }
    }
}
