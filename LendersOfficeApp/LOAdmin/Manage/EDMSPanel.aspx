﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EDMSPanel.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.EDMSPanel" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../Common/Header.ascx" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/Common/HeaderNav.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Configure Electronic Document Management System</title>
    <script language="javascript">
        function onAddDocType(id) {
            showModal("/LOAdmin/Manage/EditDocType.aspx", null, null, null, function(){
				__doPostBack('', '');
			});
			//window.open(VRoot + '/LOAdmin/Manage/EditDocType.aspx');
        }

        function onDeleteDocType(id) {
            if (confirm("Delete Doc Type?") == false)
                return;

            document.getElementById("m_hiddenId").value = id;
            document.getElementById("m_hiddenCmd").value = "deleteDocType";
            __doPostBack('', '');
        }

        function onAddShippingTemplate() {
            showModal('/LOAdmin/Manage/EditShippingTemplate.aspx', null, null, null, function(){
				__doPostBack('', '');
			});
			//window.open(VRoot + '/LOAdmin/Manage/EditShippingTemplate.aspx');
        }

        function onEditShippingTemplate(id) {
            showModal('/LOAdmin/Manage/EditShippingTemplate.aspx?id=' + id, null, null, null, function(){
				__doPostBack('', '');
			});
			//window.open(VRoot + '/LOAdmin/Manage/EditShippingTemplate.aspx?id=' + id);
        }

        function onDeleteShippingTemplate(id) {
            if (confirm("Delete Shipping Template?") == false)
                return;

            document.getElementById("m_hiddenId").value = id;
            document.getElementById("m_hiddenCmd").value = "deleteShippingTemplate";
            __doPostBack('', '');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="m_hiddenCmd" value="" runat="server" name="m_hiddenCmd"/> 
		<input type="hidden" id="m_hiddenId" value="" runat="server" name="m_hiddenId"/> 
		<uc:Header runat="server" id=Header1>
        </uc:Header>
		<uc:HeaderNav runat="server" id=HeaderNav1>
			<MenuItem URL="~/LOAdmin/Main.aspx" Label="Main">
			</MenuItem>
			<MenuItem URL="~/LOAdmin/Manage/EDMSPanel.aspx" Label="Configure Electronic Document Management System">
			</MenuItem>
		</uc:HeaderNav>
		<br />
		<div style="padding:10px" class="FieldLabel">
		                        <table class="InsetBorder" cellspacing="0" cellpadding="5" border="0">
                            <tr>
                                <td colspan="2" class="FieldLabel">
                                    Configure Shared Fax Number
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Fax Number
                                </td>
                                <td>
                                    <asp:TextBox ID="m_FaxNumber" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Doc Router Login
                                </td>
                                <td>
                                    <asp:TextBox ID="m_DocRouterLogin" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Doc Router Password
                                </td>
                                <td>
                                    <asp:TextBox ID="m_DocRouterPassword" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                            <td align="right" colspan="2"><asp:Button id="m_ApplyBtn" Text=" Apply " OnClick="ApplyFaxNumber" runat="server" class="ButtonStyle" /></td>
                            </tr>
                        </table>
		
		    <table width="50%">
		        <tr>
		            <td>
            		    Configure Doc Types
		            </td>
		        </tr>
        		<tr>
				    <td>
				        <input type="button" value="Add" onclick="return onAddDocType();" style="width:40px" />
				    </td>
				</tr>						
        		<tr>
				    <td>
				        <ml:CommonDataGrid id="m_dgDocTypes" runat="server">
							<alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
							<itemstyle cssclass="GridItem"></itemstyle>
							<headerstyle cssclass="GridHeader"></headerstyle>
							<columns>
                                <asp:BoundColumn DataField="DocTypeName" HeaderText="Doc Type" ItemStyle-Width="90%" />
								<asp:TemplateColumn ItemStyle-Width="10%">
									<itemtemplate>
										<a href="#" onclick="onDeleteDocType(<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocTypeId").ToString())%>);">delete</a>
									</itemtemplate>
								</asp:TemplateColumn>
							</columns>
						</ml:CommonDataGrid>
				    </td>
				</tr>
				<tr>
				    <td>
				        <hr />
				    </td>
				</tr>
				<tr>
				    <td>
		                Configure Shipping Templates
				    </td>
				</tr>
				<tr>
				    <td>
				        <input type="button" value="Add" onclick="onAddShippingTemplate();" style="width:40px" />
				    </td>
				</tr>
				<tr>
				    <td>
	    			    <ml:CommonDataGrid id="m_dgShippingTemplates" runat="server" Width="100%">
						    <alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
						    <itemstyle cssclass="GridItem"></itemstyle>
						    <headerstyle cssclass="GridHeader"></headerstyle>
					        <columns>
                                <asp:BoundColumn DataField="ShippingTemplateName" HeaderText="Template Name" ItemStyle-Width="80%" />
							    <asp:TemplateColumn ItemStyle-Width="10%">
								    <itemtemplate>
									    <a href="#" onclick="onEditShippingTemplate(<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ShippingTemplateId").ToString())%>);">edit</a>
								    </itemtemplate>
							    </asp:TemplateColumn>
						        <asp:TemplateColumn ItemStyle-Width="10%">
								    <itemtemplate>
									    <a href="#" onclick="onDeleteShippingTemplate(<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ShippingTemplateId").ToString())%>);">delete</a>
								    </itemtemplate>
							    </asp:TemplateColumn>
						    </columns>
					    </ml:CommonDataGrid>
				    </td>
				</tr>
		    </table>
		</div>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
</body>
</html>
