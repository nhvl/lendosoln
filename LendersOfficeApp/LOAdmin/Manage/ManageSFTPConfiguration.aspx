﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageSFTPConfiguration.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.ManageSFTPConfiguration"  Title="Manage SFTP Configurations"
    MasterPageFile="~/LOAdmin/loadmin.Master" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ContentPlaceHolderID="Main" runat="server">
    <style type="text/css">
        .modal-dialog
        {
            width: 800px;
        }
        .modal-dialog .dialog-input
        {
            width: 250px;
        }
        .form-inline .MiniInput
        {
            width: 60px;
        }
        .form-inline .LargeInput 
        {
            width: 400px;
        }
        .form-inline .SuperLargeInput 
        {
            width: 500px;
        }
        .modal-dialog .form-inline
        {
            margin: 3px;
            min-height: 34px;
        }
        .input-label
        {
            width: 20%;
        }
        .BaseCopy
        {
            display: none;
        }
        table.table-striped tr.invalid:nth-of-type(2n+1)
        {
            background-color: #f9dada;
        }

        table.table-striped tr.invalid
        {
            background-color: #ffe0e0;
        }
    </style>
    <script type="text/javascript">
        jQuery(function ($) {
            function ClearInputs(targetSelector) {
                $(targetSelector).find(':input')
                    .not(':button, :submit, :reset').prop('checked', false)
                    .not(':checkbox, :radio, select').val('');
                $(targetSelector).find('option:selected').prop('selected', false);
                $(targetSelector).find('.ClearableList').empty();
            }

            $('#AddConfigBtn').click(function () {
                $('#ConfigurationPopup').modal('show');
                ClearInputs('#ConfigurationPopup');
                $('.BrokerIdRow').show();
                $('.BrokerDisplayNameRow').hide();
                $('#BrokerDisplayName').text('');
                $('#ExportId').val('-1');
                $('#Use_DotNet').prop('checked', true);

                $('#FtpBtn').show();
                $('#EmailBtn').show();
                $('#BackBtn').hide();

                $('#SharedInputs').show();
                $('#FTPDeliveryInputs').hide();
                $('#EmailDeliveryInputs').hide();
            });

            $('#CancelBtn').click(function () {
                ClearInputs('#ConfigurationPopup');
                $('#ConfigurationPopup').modal('hide');

                $('#SharedInputs').show();
                $('#FTPDeliveryInputs').hide();
                $('#EmailDeliveryInputs').hide();

                $('#FtpBtn').show();
                $('#EmailBtn').show();
                $('#BackBtn').hide();
            });

            $('#FtpBtn').click(function () {
                $('#SharedInputs').hide();
                $('#FTPDeliveryInputs').show();
                $('#EmailDeliveryInputs').hide();

                $('#FtpBtn').hide();
                $('#EmailBtn').hide();
                $('#BackBtn').show();
            });

            $('#EmailBtn').click(function () {
                $('#SharedInputs').hide();
                $('#FTPDeliveryInputs').hide();
                $('#EmailDeliveryInputs').show();

                $('#FtpBtn').hide();
                $('#EmailBtn').hide();
                $('#BackBtn').show();
            });

            $('#BackBtn').click(function () {
                $('#SharedInputs').show();
                $('#FTPDeliveryInputs').hide();
                $('#EmailDeliveryInputs').hide();

                $('#FtpBtn').show();
                $('#EmailBtn').show();
                $('#BackBtn').hide();
            });

            $('#SaveBtn').click(function () {
                var configuration = {
                    BrokerId: $('#BrokerId').val(),
                    ExportId: $('#ExportId').val(),
                    ExportName: $('#ExportName').val(),
                    MapName: $('#MapName').val(),
                    UserId: $('#UserId').val(),
                    ReportId: $('#ReportId').val(),
                    Notes: $('#Notes').val(),
                    Protocol: $('#Protocol').val(),
                    HostName: $('#HostName').val(),
                    Login: $('#Login').val(),
                    EncryptedPassword: $('#EncryptedPassword').val(),
                    Port: $('#Port').val(),
                    SSHFingerprint: $('#SSHFingerprint').val(),
                    DestPath: $('#DestPath').val(),
                    Use_DotNet: $('#Use_DotNet').is(':checked'),
                    FtpSecure: $('#SecurityMode').val(),
                    SSLHostFingerprint: $('#SSLHostFingerprint').val(),
                    UsePassive: $('#UsePassive').is(':checked'),
                    AdditionalPostUrl: $('#AdditionalPostUrl').val(),
                    Comments: $('#Comments').val(),
                    From: $('#From').val(),
                    To: $('#To').val(),
                    FileName: $('#FileName').val(),
                    Subject: $('#Subject').val(),
                    Message: $('#Message').val()
                }

                var data = {
                    SFTPConfigViewModel: JSON.stringify(configuration)
                };

                var result = gService.main.call("SaveConfiguration", data);
                if (!result.error) {
                    if (result.value['Success'] === 'True') {
                        ClearInputs('#ConfigurationPopup');
                        $('#ConfigurationPopup').modal('hide');
                        if (result.value['IsNew'].toLowerCase() === 'true') {
                            var cloneRow = $('#ConfigTable').find('.BaseCopy').clone().removeClass('BaseCopy');
                            cloneRow.find('.ExportName').text(result.value['ExportName']);
                            cloneRow.find('.MapName').text(result.value['MapName']);
                            cloneRow.find('.UserId').text(result.value['UserId']);
                            cloneRow.find('.ReportId').text(result.value['ReportId']);
                            cloneRow.find('.Protocol').text(result.value['Protocol']);
                            cloneRow.data('exportid', result.value['ExportId']);
                            $('#ConfigTable').append(cloneRow);
                        }
                        else {
                            var row = $('#ConfigTable').find('tr').filter(function () { return $(this).data('exportid') == result.value['ExportId'] });
                            row.find('.ExportName').text(result.value['ExportName']);
                            row.find('.MapName').text(result.value['MapName']);
                            row.find('.UserId').text(result.value['UserId']);
                            row.find('.ReportId').text(result.value['ReportId']);
                            row.find('.Protocol').text(result.value['Protocol']);
                            row.data('exportid', result.value['ExportId']);
                        }
                    }
                    else {
                        alert(result.value['Errors']);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            });

            $('#ConfigPanel').on('click', '.Edit', function () {
                var exportId = $(this).closest('tr').data('exportid');
                var brokerId = $(this).closest('tr').data('brokerid');
                var data = {
                    ExportId: exportId,
                    BrokerId: brokerId
                };

                var result = gService.main.call('LoadConfigurationById', data);
                if (!result.error) {
                    if (result.value['Success'].toLowerCase() === 'true') {
                        ClearInputs('#ConfigurationPopup');
                        var viewModel = JSON.parse(result.value['SFTPConfigViewModel']);
                        $('.BrokerIdRow').hide();
                        $('.BrokerDisplayNameRow').show();
                        $('#BrokerDisplayName').text(viewModel.BrokerDisplayName);
                        $('#BrokerId').val(viewModel.BrokerId);
                        $('#ExportId').val(viewModel.ExportId);
                        $('#ExportName').val(viewModel.ExportName);
                        $('#MapName').val(viewModel.MapName);
                        $('#UserId').val(viewModel.UserId);
                        $('#ReportId').val(viewModel.ReportId);
                        $('#Notes').val(viewModel.Notes);
                        $('#Protocol').val(viewModel.Protocol);
                        $('#HostName').val(viewModel.HostName);
                        $('#Login').val(viewModel.Login);
                        $('#EncryptedPassword').val(viewModel.EncryptedPassword);
                        $('#Port').val(viewModel.Port);
                        $('#SSHFingerprint').val(viewModel.SSHFingerprint);
                        $('#DestPath').val(viewModel.DestPath);
                        $('#Use_DotNet').prop('checked', viewModel.Use_DotNet);
                        $('#Use_WinSCP').prop('checked', !viewModel.Use_DotNet);
                        $('#SecurityMode').val(viewModel.FtpSecure);
                        $('#SSLHostFingerprint').val(viewModel.SSLHostFingerprint);
                        $('#UsePassive').prop('checked', viewModel.UsePassive);
                        $('#AdditionalPostUrl').val(viewModel.AdditionalPostUrl);
                        $('#Comments').val(viewModel.Comments);
                        $('#To').val(viewModel.To);
                        $('#From').val(viewModel.From);
                        $('#FileName').val(viewModel.FileName);
                        $('#Subject').val(viewModel.Subject);
                        $('#Message').val(viewModel.Message);

                        $('#SharedInputs').show();
                        $('#FTPDeliveryInputs').hide();
                        $('#EmailDeliveryInputs').hide();

                        $('#FtpBtn').show();
                        $('#EmailBtn').show();
                        $('#BackBtn').hide();

                        $('#ConfigurationPopup').modal('show');
                    }
                    else {
                        alert(result.value["Errors"]);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            });
        });
    </script>
    <form id="form1" runat="server">
    <div>
        <div class="container-fluid">
            <div class="row">
                <div class="center-div">
                    <div class="panel panel-default" id="ConfigPanel">
                        <div class="panel-body div-wrap-tb">
                            <table id="ConfigTable" class="table table-striped table-condensed table-layout-fix">
                                <thead>
                                    <tr>
                                        <th>Broker Name</th>
                                        <th>Export Name</th>
                                        <th>Map Name</th>
                                        <th>User Login</th>
                                        <th>Report Name</th>
                                        <th>Protocol</th>
                                        <th>Notes</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="BaseCopy" data-id="-1">
                                        <td>
                                            <span class="BrokerDisplayName"></span>
                                        </td>
                                        <td>
                                            <span class="ExportName"></span>
                                        </td>
                                        <td>
                                            <span class="MapName"></span>
                                        </td>
                                        <td>
                                            <span class="UserLoginName"></span>
                                        </td>
                                        <td>
                                            <span class="ReportName"></span>
                                        </td>
                                        <td>
                                            <span class="Notes"></span>
                                        </td>
                                        <td>
                                            <span class="Protocol"></span>
                                        </td>
                                        <td>
                                            <a class="Edit">edit</a>
                                        </td>
                                    </tr>
                                    <asp:Repeater runat="server" ID="Configurations">
                                        <ItemTemplate>
                                            <tr data-brokerid="<%# AspxTools.HtmlString(Eval("BrokerId")) %>" data-exportid="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ExportId").ToString()) %>" class="<%# AspxTools.HtmlString((bool)(DataBinder.Eval(Container.DataItem, "IsValid")) ? "" : "invalid") %>">
                                                <td>
                                                    <span class="BrokerDisplayName"><%# AspxTools.HtmlString(Eval("BrokerDisplayName").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <span class="ExportName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ExportName").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <span class="MapName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "MapName").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <span class="UserLoginName"><%# AspxTools.HtmlString(Eval("UserLoginName").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <span class="ReportName"><%# AspxTools.HtmlString(Eval("ReportName").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <span class="Protocol"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Protocol").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <span class="Notes"><%# AspxTools.HtmlString(Eval("Notes").ToString()) %></span>
                                                </td>
                                                <td>
                                                    <a class="Edit">edit</a>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                            <span class="clipper">
                                <button type="button" class="btn btn-primary" id="AddConfigBtn">Add New</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="ConfigurationPopup" class="modal v-middle fade Configuration" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">SFTP Configuration</h4>
                </div>

                <div id="SharedInputs" class="modal-body">
                    <input type="hidden" id="ExportId" />
                    <div class="form-inline BrokerDisplayNameRow">
                        <label class="input-label">Broker Name</label>
                        <label><span id="BrokerDisplayName"></span></label>
                    </div>
                    <div class="form-inline BrokerIdRow">
                        <label class="input-label">Broker Id</label>
                        <input type="text" id="BrokerId" class="LargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Export Name (Note: Avoid space in name)</label>
                        <input type="text" id="ExportName" class="dialog-input form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Map Name</label>
                        <input type="text" id="MapName" class="LargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">User Id</label>
                        <input type="text" id="UserId" class="LargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Report Id</label>
                        <input type="text" id="ReportId" class="LargeInput form-control" />
                    </div>      
                    <div class="form-inline">
                        <label class="input-label">Notes</label>
                        <textarea id="Notes" class="dialog-input form-control"></textarea>
                    </div>
                </div>

                <div id="EmailDeliveryInputs" class="modal-body">
                    <div class="form-inline">
                        <label class="input-label">From</label>
                        <input type="text" id="From" class="dialog-input form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">To</label>
                        <input type="text" id="To" class="LargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">File Name</label>
                        <input type="text" id="FileName" class="LargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Email Subject</label>
                        <input type="text" id="Subject" class="LargeInput form-control" />
                    </div>      
                    <div class="form-inline">
                        <label class="input-label">Email Message</label>
                        <textarea id="Message" cols="80" rows="10" class="LargeInput form-control"></textarea>
                    </div>
                </div>

                <div id="FTPDeliveryInputs" class="modal-body">
                    <div class="form-inline">
                        <label class="input-label">Protocol</label>
                        <select id="Protocol" class="form-control">
                            <option value="SFTP">SFTP</option> 
                            <option value="FTP">FTP</option> 
                            <option value="SCP">SCP</option> 
                        </select>
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Host Name</label>
                        <input type="text" id="HostName" class="LargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Login</label>
                        <input type="text" id="Login" class="dialog-input form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Encrypted Password</label>
                        <input type="text" id="EncryptedPassword" class="dialog-input form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Port</label>
                        <input type="text" id="Port" class="MiniInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">SSH Fingerprint</label>
                        <input type="text" id="SSHFingerprint" class="SuperLargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Destination Path</label>
                        <input type="text" id="DestPath" class="SuperLargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <input type="radio" name="transmissionType" id="Use_DotNet" value="true" checked="checked" /> <label class="control-label" for="Use_DotNet">.NET</label> 
                        <input type="radio" name="transmissionType" id="Use_WinSCP" value="false" /> <label class="control-label" for="Use_WinSCP">WinSCP</label> 
                    </div>
                    <div class="form-inline">
                        <label class="input-label">FTP Security Mode</label>
                        <select id="SecurityMode" class="form-control">
                            <option value="None">None</option> 
                            <option value="Implicit">Implicit</option> 
                            <option value="ExplicitSsl">Explicit SSL</option> 
                            <option value="ExplicitTls">Explicit TLS</option> 
                        </select>
                    </div>
                    <div class="form-inline">
                        <label class="input-label">SSL Host Fingerprint</label>
                        <input type="text" id="SSLHostFingerprint" class="SuperLargeInput form-control" />
                    </div>
                    <div class="form-inline checkbox">
                        <label class="control-label">
                            <input type="checkbox" id="UsePassive" />Use Passive?
                        </label>
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Additional Post Url</label>
                        <input type="text" id="AdditionalPostUrl" class="SuperLargeInput form-control" />
                    </div>
                    <div class="form-inline">
                        <label class="input-label">Comments</label>
                        <textarea id="Comments" cols="80" rows="10" class="LargeInput form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <span class="clipper">
                        <input type="button" class="btn btn-default" id="CancelBtn" data-dismiss="modal" value="Cancel"/>
                    </span>
                    <span class="clipper">
                        <input type="button" class="btn btn-default" id="FtpBtn" value="Choose FTP Delivery Method"/>
                    </span>
                    <span class="clipper">
                        <input type="button" class="btn btn-default" id="EmailBtn" value="Choose Email Delivery Method"/>
                    </span>
                    <span class="clipper">
                        <input type="button" class="btn btn-default" id="BackBtn" value="Back"/>
                    </span>
                    <span class="clipper">
                        <input type="button" class="btn btn-primary" id="SaveBtn" value="Save" />
                    </span>
                </div>
            </div>
        </div>
    </div>
    </form>
</asp:Content>