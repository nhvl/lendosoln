using System;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.los.RatePrice;
using CCondition = LendersOfficeApp.los.RatePrice.CCondition;
using PricePolicyTools = LendersOfficeApp.los.RatePrice.PricePolicyTools;

namespace LendersOfficeApp.LOAdmin.Manage
{
    /// <summary>
    /// Summary description for ListLpeConditionErrors.
    /// </summary>
    public partial class ListLpeConditionErrors : LendersOffice.Admin.SecuredAdminPage
	{

        private   string m_action = "";
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

        protected void btnRun_Click(object sender, System.EventArgs e)
        {
            m_action = "TestAllRuleConditions";
        }

        void TestAllRule_Process(bool testConditionOnly)
        {
            string result = "";
            try
            {
                if( testConditionOnly )
                    result = CConditionTest.TestAllRuleConditions( new CConditionTest.NotifyCallback(NotifyByJavaScript) );
                else
                    result = CRuleTest.TestAllRule( new CRuleTest.NotifyCallback(NotifyByJavaScript) );

                NotifyByJavaScript( result );
            }
            catch(Exception ex)
            {
                result = ex.ToString();
                AppendNotifyByJavaScript( result );
            }        
            
        }

        protected void btnClear_Click(object sender, System.EventArgs e)
        {
            m_txtResult.Text = "";        
        }


        protected void bttnVerifySrcPtr_Click(object sender, System.EventArgs e)
        {
            try
            {
                m_txtResult.Text = LendersOfficeApp.los.RatePrice.LpeDataVerify.SrcPtrVerify();
            }
            catch(Exception ex)
            {
                m_txtResult.Text = ex.ToString();
            }        

        
        }

        public static string SafeJsLiteralString(string str) 
        {
            if (null != str) 
            {
                StringBuilder sb = new StringBuilder(str);
                sb.Replace("\\", "\\\\").Replace("\n", "\\n").Replace("\r", "\\r").Replace("'", "\\'");
                return sb.ToString();
            }
            else 
                return null;
        }

        void NotifyByJavaScript( string msg )
        {
            Response.Write("\n<script language='javascript' type='text/javascript'>"
                           + " $(\"[id$='m_txtResult']\").val('"
                           + SafeJsLiteralString(msg) + "'); " + TextAreaheightAdjust() + "</script>");
            Response.Flush();
        }
        private string TextAreaheightAdjust() {
            return "$(\"[id$='m_txtResult']\").css('height',$(\"[id$='m_txtResult']\")[0].scrollHeight+5);";
        }
        void AppendNotifyByJavaScript( string msg )
        {
            Response.Write("\n<script language='javascript' type='text/javascript'>"
                + " $(\"[id$='m_txtResult']\").val($(\"[id$='m_txtResult']\").val() + '"
                + SafeJsLiteralString(msg) + "'); " + TextAreaheightAdjust() + "</script>");
            Response.Flush();
        }


        private void RemoveEmptyAdjustTargets_Progress(  int processedPolicies, int totalPolicies )
        {
            NotifyByJavaScript( string.Format("RemoveEmptyAdjustTargets :  {0:N0} / {1:N0} records.", processedPolicies, totalPolicies) );            
        }


        private string RemoveEmptyAdjustTargets_Process()
        {
            string str = "";
            try
            {               

                int expectRecords = 0;

                int modifiedRecords = PricePolicyTools.ReducePolicySizeFromDb(out expectRecords, 
                                                    new PricePolicyTools.ProgressCallback(RemoveEmptyAdjustTargets_Progress) );

                if( expectRecords == modifiedRecords )
                {
                    if( modifiedRecords == 0 )
                        str = "\nRemoveEmptyAdjustTargetsFromDb : no record need to be changed.";
                    else
                        str = String.Format("\nRemoveEmptyAdjustTargetsFromDb : successfully change {0:N0} records.", modifiedRecords);
                }
                else
                    str = String.Format("\nRemoveEmptyAdjustTargetsFromDb : need modify {0:N0} record(s), but successfully change {1:N0} record(s)", expectRecords, modifiedRecords);                
            }
            catch( Exception exc )
            {
                str = "\nRemoveEmptyAdjustTargetsFromDb  error : \n" + exc.ToString();
            }

            AppendNotifyByJavaScript( str );

            return str;
        }


        protected void btnRemoveEmptyAdjustTargets_Click(object sender, System.EventArgs e)
        {
            m_action = "RemoveEmptyAdjustTargets";
            //m_txtResult.Text = RemoveEmptyAdjustTargets();
        }

        protected void SpecialProcess()
        {
            switch( m_action )
            {
                case "TestAllRuleConditions" :
                    TestAllRule_Process( true /* test condition only */);
                    break;

                case "PolicyValidate" :
                    TestAllRule_Process( false );
                    break;

                case "RemoveEmptyAdjustTargets" :
                    RemoveEmptyAdjustTargets_Process();
                    break;
            }

        }

        protected void btnPolicyValidate_Click(object sender, System.EventArgs e)
        {
            m_action = "PolicyValidate";
        
        }



	}


    class CConditionTest
    {
        private static bool ConditionValidate( string condStr)
        {
            CCondition condition = CCondition.CreateTestCondition( condStr );

            XmlDocument xmlDocEval = new XmlDocument();
            return condition.Evaluate(xmlDocEval, new LendersOfficeApp.los.RatePrice.CSymbolTable(null) ); 
        }

        public delegate void NotifyCallback( string msg );

        static public string TestAllRuleConditions( NotifyCallback callback )
        {
            StringBuilder sb = new StringBuilder();

            int counterPolicies = 0;
            int countRules      = 0;
            int countErrors    = 0;

            const int blockSize = 200;

            using( DbDataReader r = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "RetrieveAllPricePolicies" ) )
            {

                Hashtable   ruleIds = new Hashtable();
                XmlDocument doc     = new XmlDocument();
                while( r.Read() )
                {
                    counterPolicies++;

                    // PricePolicyId, PricePolicyDescription, PricePolicyXmlText, MutualExclusiveExecSortedId
                    string xmlPolicy = "";
                    try
                    {
                        xmlPolicy = (string)r["PricePolicyXmlText"];
                        if( xmlPolicy == null || xmlPolicy.TrimWhitespaceAndBOM() == "")
                            continue;

                        doc = new XmlDocument();
                        doc.LoadXml(xmlPolicy); 

                        bool bMutualExclusiveExec = doc.SelectSingleNode(".//MutualExclusiveExecSortedId") != null;

                        ruleIds.Clear();
                        foreach( XmlNode rule in doc.SelectNodes("//Rule") )
                        {
                            countRules++;
                            string condStr = "";
                            string ruleId  = "";
                            try
                            {
                                XmlAttribute attribute = rule.Attributes["RuleId"];
                                if( attribute != null )
                                {
                                    ruleId = attribute.Value;
                                    if( ruleIds.Contains(ruleId) )
                                    {
                                        countErrors++;
                                        sb.AppendFormat("Policy {0} contains duplicate rule {1}\n\n", 
                                            r["PricePolicyId"], ruleId);
                                    }
                                    else
                                        ruleIds[ruleId] = "dummy";
                                }
                                else
                                {
                                    countErrors++;
                                    sb.AppendFormat("PolicyId {0} contains the rule that doesn't have ruleId\n\n", 
                                        r["PricePolicyId"]);
                                    continue;
                                }

                                if( bMutualExclusiveExec )
                                {
                                    bool bSkip = rule.SelectSingleNode("Skip") != null;
                                    countErrors++;
                                    sb.AppendFormat("MutualExclusiveExec Policy {0} contains 'Skip' rule {1}\n\n", 
                                        r["PricePolicyId"], ruleId);
                                }


                                XmlNode condNode = rule.SelectSingleNode("Condition");
                                condStr = condNode.InnerText;
                                if( condStr == null || condStr == "" )
                                    continue;
                                ConditionValidate( condNode.InnerText ); 
                            }
                            catch( Exception ex)
                            {
                                countErrors++;
                                sb.AppendFormat("PolicyId {0} ruleId {1} : \"{2}\".\nException : {3}\n\n", 
                                    r["PricePolicyId"], ruleId, condStr, CBaseException.GetVerboseMessage( ex ) );
                            }

                        }

                    }
                    catch( Exception ex )
                    {
                        countErrors++;
                        sb.AppendFormat(" xmlPolicy = '" + xmlPolicy + "'\nException : " + ex.Message + "\n");
                    }
                    if( callback != null && ( (counterPolicies % blockSize) == 0 || counterPolicies == 1) )
                        callback( string.Format("Verifying : {0:N0} policies  {1:N0} rules  [ {2} errors ] ", counterPolicies, countRules, countErrors) );

                }

            }

            if( callback != null  )
                callback( string.Format("Verifying : {0:N0} policies  {1:N0} rules  [ {2} errors ] ", counterPolicies, countRules, countErrors) );

            
            sb.Insert(0, string.Format("#policies : {0:N0}\n" +
                                       "#rules    : {1:N0}\n" +
                                       "#errors   : {2:N0}\n\n" ,
                                       counterPolicies,  countRules, countErrors) );
 
            return sb.ToString();
        }

    }


    class CRuleTest
    {
        static public CAdjustTarget GetAdjustTarget( CConsequence consequence, string fldName )
        {
            return (CAdjustTarget)typeof(CConsequence).InvokeMember( fldName,  BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic 
                                                         | BindingFlags.GetProperty | BindingFlags.GetField, null, consequence, null);
        }


        static string []s_adjustTargetNames = {
                                                      "QltvAdjustTarget", 
                                                      "QcltvAdjustTarget", 
                                                      "QLAmtAdjustTarget", 
                                                      "MaxYspAdjustTarget", 
                                                      "QscoreTarget", 
                                                      "QrateTarget", 
                                                      "QrateAdjustTarget", 
                                                      "TeaserRateAdjustTarget", 
                                                      "MaxDtiTarget", 
                                                      "MarginAdjustTarget", 
                                                      "RateAdjustTarget", 
                                                      "MarginBaseTarget", 
                                        };

        static private int RuleValidate( string policyId, XmlElement ruleElement, out string errMsg)
        {
            int countErrors = 0;
            StringBuilder sb = new StringBuilder();
            XmlDocument xmlDocEval = new XmlDocument();
            CSymbolTable symTable = new LendersOfficeApp.los.RatePrice.CSymbolTable(null);

            CRule rule = new CRule( ruleElement );

            try
            {
                rule.Condition.Evaluate(xmlDocEval, symTable ); 
            }
            catch(Exception exc)
            {
                countErrors++;
                sb.AppendFormat("PolicyId {0} ruleId {1}, {4} \"{2}\".\nException : {3}\n\n", 
                               policyId, rule.RuleId, rule.Condition.UserExpression, 
                               CBaseException.GetVerboseMessage( exc ), "condition" );
            }

            foreach( string propertyName in s_adjustTargetNames )
            {
                CAdjustTarget adjustTarget = GetAdjustTarget( rule.Consequence, propertyName);
                try
                {
                    if( adjustTarget.UserExpression == "" || adjustTarget.UserExpression == null )
                        continue;
                    adjustTarget.AllowExceptionForEvaluate = true;
                    adjustTarget.Evaluate(xmlDocEval, symTable ); 
                }
                catch(Exception exc)
                {
                    countErrors++;
                    sb.AppendFormat("PolicyId {0} ruleId {1} {4} : \"{2}\".\nException : {3}\n\n", 
                        policyId, rule.RuleId, adjustTarget.UserExpression, 
                        CBaseException.GetVerboseMessage( exc ), propertyName );
                }

            }

            errMsg = sb.ToString(); 
            return countErrors;
        }

        public delegate void NotifyCallback( string msg );

        static public string TestAllRule( NotifyCallback callback )
        {
            StringBuilder sb = new StringBuilder();

            int counterPolicies = 0;
            int countRules      = 0;
            int countErrors    = 0;

            const int blockSize = 200;

            using( DbDataReader r = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "RetrieveAllPricePolicies" ) )
            {

                Hashtable   ruleIds = new Hashtable();
                XmlDocument doc     = new XmlDocument();
                while( r.Read() )
                {
                    counterPolicies++;

                    // PricePolicyId, PricePolicyDescription, PricePolicyXmlText, MutualExclusiveExecSortedId
                    string xmlPolicy = "";
                    try
                    {
                        xmlPolicy = (string)r["PricePolicyXmlText"];
                        if( xmlPolicy == null || xmlPolicy.TrimWhitespaceAndBOM() == "")
                            continue;

                        doc = new XmlDocument();
                        doc.LoadXml(xmlPolicy); 

                        bool bMutualExclusiveExec = doc.SelectSingleNode(".//MutualExclusiveExecSortedId") != null;

                        ruleIds.Clear();
                        foreach( XmlNode rule in doc.SelectNodes("//Rule") )
                        {
                            countRules++;


                            string condStr = "";
                            string ruleId  = "";
                            try
                            {
                                XmlAttribute attribute = rule.Attributes["RuleId"];
                                if( attribute != null )
                                {
                                    ruleId = attribute.Value;
                                    if( ruleIds.Contains(ruleId) )
                                    {
                                        countErrors++;
                                        sb.AppendFormat("Policy {0} contains duplicate rule {1}\n\n", 
                                            r["PricePolicyId"], ruleId);
                                    }
                                    else
                                        ruleIds[ruleId] = "dummy";
                                }
                                else
                                {
                                    countErrors++;
                                    sb.AppendFormat("PolicyId {0} contains the rule that doesn't have ruleId\n\n", 
                                        r["PricePolicyId"]);
                                    continue;
                                }

                                if( bMutualExclusiveExec )
                                {
                                    bool bSkip = rule.SelectSingleNode("Skip") != null;
                                    countErrors++;
                                    sb.AppendFormat("MutualExclusiveExec Policy {0} contains 'Skip' rule {1}\n\n", 
                                        r["PricePolicyId"], ruleId);
                                }

                                string errMsg;
                                countErrors += RuleValidate( r["PricePolicyId"].ToString(), (XmlElement) rule, out errMsg);
                                if( errMsg != "" )
                                    sb.Append( errMsg );
                            }
                            catch( Exception ex)
                            {
                                countErrors++;
                                sb.AppendFormat("PolicyId {0} ruleId {1} : \"{2}\".\nException : {3}\n\n", 
                                    r["PricePolicyId"], ruleId, condStr, CBaseException.GetVerboseMessage( ex ) );
                            }

                        }

                    }
                    catch( Exception ex )
                    {
                        countErrors++;
                        sb.AppendFormat(" xmlPolicy = '" + xmlPolicy + "'\nException : " + ex.Message + "\n");
                    }
                    if( callback != null && ( (counterPolicies % blockSize) == 0 || counterPolicies == 1) )
                        callback( string.Format("Verifying : {0:N0} policies  {1:N0} rules  [ {2} errors ] ", counterPolicies, countRules, countErrors) );

                }

            }

            if( callback != null  )
                callback( string.Format("Verifying : {0:N0} policies  {1:N0} rules  [ {2} errors ] ", counterPolicies, countRules, countErrors) );

            
            sb.Insert(0, string.Format("#policies : {0:N0}\n" +
                "#rules    : {1:N0}\n" +
                "#errors   : {2:N0}\n\n" ,
                counterPolicies,  countRules, countErrors) );
 
            return sb.ToString();
        }

    }



}
