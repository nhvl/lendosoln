﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonProjectLib.Common.Lib;
using LendersOffice.ObjLib.CapitalMarkets;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class MBSDescriptions : LendersOffice.Admin.SecuredAdminPage
    {
        protected void PageLoad(object sender, System.EventArgs e)
        {
            m_loadLOdefaultScripts = false;
            //update desc table
            if (!IsPostBack)
            {
                ReloadDescData();
            }
            else
            {
                Response.Redirect(Request.Url.ToString(), false);
                Response.Redirect(Request.Url.ToString(), false);
            }
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        private void ReloadDescData()
        {
            descRepeater.DataSource = Description.GetDescriptions().OrderBy(x=>x.DescriptionName).ToList().ConvertAll(x => new Tuple<Guid, string> (x.DescriptionId,x.DescriptionName));
            descRepeater.DataBind();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hiddenKey.Value))
            {
                return;
            }

            var descGuid = new Guid(hiddenKey.Value);
            Description.DeleteDescription(descGuid);
            ReloadDescData();
        }

        protected void OnAddClicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(NewDescription.Text.TrimWhitespaceAndBOM()))
            {
                Description.CreateDescription(NewDescription.Text.TrimWhitespaceAndBOM());
                NewDescription.Text = "";
                ReloadDescData();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
        
    }
}
