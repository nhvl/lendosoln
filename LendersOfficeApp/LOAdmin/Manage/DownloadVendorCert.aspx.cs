﻿namespace LendersOfficeApp.LOAdmin.Manage
{
    using System;
    using System.Web.UI;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Provides a means for downloading vendor certificates.
    /// </summary>
    public partial class DownloadVendorCert : Page
    {
        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var fileDbKey = RequestHelper.GetSafeQueryString("k");
            var tempFilePath = FileDBTools.CreateCopy(E_FileDB.Temp, fileDbKey);

            RequestHelper.SendFileToClient(this.Context, tempFilePath, "application/octet-stream", "certificate.p12");
            this.Context.ApplicationInstance.CompleteRequest();
        }
    }
}