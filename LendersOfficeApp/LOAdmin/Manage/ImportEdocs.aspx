﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportEdocs.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.ImportEdocs"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Edocs Batch Importer" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
    <script type="text/javascript">
        function onImportSuccess(importType)
        {
            $('#AlertDiv').hide();

            $('#LoadingDiv').hide();

            alert(importType + " import successful!");
        }

        function validateInputs(importType, fileUpload, errorMessage)
        {
            var ret = true;

            $('#AlertDivMessages').empty();

            if ($(<%=AspxTools.JsGetElementById(BrokerDropdown)%>).val() === <%=AspxTools.JsString(Guid.Empty)%>) 
            {
                $("#AlertDivMessages").append("<p>You must specify the broker to use when importing.</p>");

                ret = false;
            }

            if ($(fileUpload).val() === "") 
            {
                $("#AlertDivMessages").append("<p>You must specify the file containing " + errorMessage + ".</p>");

                ret = false;
            }

            return ret;
        }

        function setLoadingDisplay()
        {
            $('#AlertDiv').hide();

            $('#LoadingDiv').show();
        }

        function onFoldersImportClick() 
        {
            if (validateInputs('Folders', <%=AspxTools.JsGetElementById(FoldersFileInput)%>, 'folders with roles') === false) 
            {
                $('#AlertDiv').show();

                return false;
            }
            else 
            {
                setLoadingDisplay();

                return true;
            }
        }

        function onDocTypeImportClick() 
        {
            if (validateInputs("DocTypes", <%=AspxTools.JsGetElementById(DocTypesFileInput)%>, 'doc types to import') === false) 
            {
                $('#AlertDiv').show();

                return false;
            }
            else 
            {
                setLoadingDisplay();

                return true;
            }
        }

        function onMappingsImportClick() 
        {
            if (validateInputs('Mappings', <%=AspxTools.JsGetElementById(MappingsFileInput)%>, 'doc type mappings for import') === false) 
            {
                $('#AlertDiv').show();

                return false;
            }
            else 
            {
                setLoadingDisplay();

                return true;
            }
        }

        $('#PMLContain').ready(function()
        {
            $('#LoadingDiv,#AlertDiv').hide();

            $('#AlertCloseBtn').click(function() 
            {
                $('#AlertDiv').hide();
            });
        });
    </script>

    <div id="AlertDiv" class="alert alert-error alert-dismissible text-center" role="alert">
        <button id="AlertCloseBtn" type="button" class="close">&times;</button>
        <div id="AlertDivMessages"></div>
        <p>No imports were saved for this operation.</p>
    </div>

    <div id="LoadingDiv" class="alert alert-info text-center" role="alert"><p>Importing, please wait&hellip;</p></div>

    <div class="panel panel-default">
        <form id="Form1" runat="server">
            <div class="panel-body">
                <div class="row">
                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            Select broker by customer code:
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="form-group text-center">
                            <asp:DropDownList runat="server" ID="BrokerDropdown" AutoPostBack="false"></asp:DropDownList>
                        </div>
                    </div>

                     <!-- Spacer -->
                    <div class="col-sm-2">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <h3>Folder / DocType Import</h3>
                        <hr />
                    </div>

                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            Select CSV file containing folders with roles:
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="form-group">
                            <asp:FileUpload runat="server" ID="FoldersFileInput" accept=".csv" CssClass="form-control text-center input-sm" />
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <asp:Button runat="server" ID="FoldersFileInputButton" CssClass="btn btn-primary" Text="Import" 
                                OnClientClick="return onFoldersImportClick();" OnClick="FoldersFileInputButton_Click" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            Select CSV file containing document types:
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="form-group">
                            <asp:FileUpload runat="server" ID="DocTypesFileInput" accept=".csv" CssClass="form-control text-center input-sm" />
                        </div>
                    </div>

                     <div class="col-sm-2">
                        <div class="form-group">
                            <asp:Button runat="server" ID="DocTypesFileInputButton" CssClass="btn btn-primary" Text="Import"
                                OnClientClick="return onDocTypeImportClick();" OnClick="DocTypesFileInputButton_Click" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <h3>Vendor Mapping Import</h3>
                        <hr />
                    </div>
                </div>

                <div class="row">
                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            Select CSV file containing document types to vendor mappings:
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="form-group">
                            <asp:FileUpload runat="server" ID="MappingsFileInput" accept=".csv" CssClass="form-control text-center input-sm" />
                        </div>
                    </div>

                     <div class="col-sm-2">
                        <div class="form-group">
                            <asp:Button runat="server" ID="MappingsFileInputButton" CssClass="btn btn-primary" Text="Import"
                                OnClientClick="return onMappingsImportClick();" OnClick="MappingsFileInputButton_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </form>
     </div>
</asp:Content>
