﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using EDocs;
using CommonProjectLib.Common.Lib;
using LendersOffice.Common;
using LendersOffice.AntiXss;
namespace LendersOfficeApp.LOAdmin.Manage
{
    public partial class LostEDocs : LendersOffice.Admin.SecuredAdminPage
    {

        private const int PAGE_SIZE = 50;//HACK: 50;
        public int pageCount { get; private set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //stop loading `common.js` from `BasePage`
            RegisterJsScript("ractive-0.7.1.min.js");
            m_loadLOdefaultScripts = false;
            if (!IsPostBack)
            {
                BindDatagrid(1);
            }

        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        private void BindDatagrid(int page)
        {
            //load edoc
            int count = 0;
            List<LostEDocument> lostEDoclist = null; ;
            while (count == 0 && page > 0)
            {
                lostEDoclist = LostEDocument.GetDocuments(--page, PAGE_SIZE, out count).ToList();
                
            }
            //assign number of page
            if (page == 0 && count == 0)
            {
                pagingRepeater.DataSource = null;
                edocRepeater.DataBind();
                return;
            }
            int pageCount = count > 0?  (count-1) / PAGE_SIZE + 1 : 1;
            updatePagingRepeater(pageCount, page+1);
            // convert to edocRepeater.
            edocRepeater.DataSource = lostEDoclist.ConvertAll(x => new Tuple<Guid, string>(x.DocumentId, x.CreatedDate.ToString("dd/MM/yyyy hh:mm:ss tt", new System.Globalization.CultureInfo("en-US"))));
            
            //binding repeater
            edocRepeater.DataBind();   
        }
        enum PagiItemType { Normal = 0, Chosen = 1, Extend = 2 };

        private const int maxDisplayedItem = 7;
        private void updatePagingRepeater(int pageCount,int chosenIndex)
        {
            //if there is one or new record. Don't even show the pagination
            if (pageCount <=1) { pagingRepeater.DataSource = null; }
            //else load pagination repeater
            else
            {
                var testList = new List<Tuple<int, int>>();
                //load list page count
                int defaultSideDisplaySpace = (maxDisplayedItem -1) / 2;
                //if need show ... button
                if (pageCount > maxDisplayedItem)
                {
                    //caculate space to each side
                    var leftSideLength = chosenIndex - 1;
                    var rightSideLength = pageCount - chosenIndex;

                    var leftSideDisplaySpace = defaultSideDisplaySpace;
                    var rightSideDisplaySpace = defaultSideDisplaySpace;

                    if (leftSideLength < leftSideDisplaySpace)
                    {
                        leftSideDisplaySpace = leftSideLength;
                        rightSideDisplaySpace = maxDisplayedItem - leftSideDisplaySpace - 1;
                    }
                    else if (rightSideLength < rightSideDisplaySpace)
                    {
                        rightSideDisplaySpace = rightSideLength;
                        leftSideDisplaySpace = maxDisplayedItem - rightSideDisplaySpace - 1;
                    }
                    //work with left side first
                    if (leftSideDisplaySpace > 0)
                    {
                        //if left side can show all item, show the extend
                        if (leftSideLength <= leftSideDisplaySpace)
                        {
                            testList.AddRange(Enumerable.Range(1, chosenIndex-1).ToList().ConvertAll(x => new Tuple<int, int>(x, (int)PagiItemType.Normal)));
                        }
                        //if not only show the first and the items  nearest to chosen one 
                        else
                        {
                            testList.Add(new Tuple<int, int>(1, (int)PagiItemType.Normal));
                            var iCounter = leftSideDisplaySpace - 1;
                            testList.Add(new Tuple<int, int>(chosenIndex - iCounter--, (int)PagiItemType.Extend));
                            while (iCounter > 0)
                            {
                                testList.Add(new Tuple<int, int>(chosenIndex - iCounter--, (int)PagiItemType.Normal));
                            }
                        }
                    }
                    //add choosen index
                    testList.Add(new Tuple<int, int>(chosenIndex, (int)PagiItemType.Chosen));
                    //work with right side
                    if (rightSideDisplaySpace > 0)
                    {
                        //if that side can show all item, show them
                        if (rightSideLength <= rightSideDisplaySpace)
                        {
                            testList.AddRange(Enumerable.Range(chosenIndex + 1, pageCount - chosenIndex).ToList().ConvertAll(x => new Tuple<int, int>(x, (int)PagiItemType.Normal)));
                        }
                        //if not only show the latest and the items  nearest to chosen one 
                        else
                        {
                            var iCounter = 1;
                            while (iCounter < rightSideDisplaySpace - 1)
                            {
                                testList.Add(new Tuple<int, int>(chosenIndex + iCounter++, (int)PagiItemType.Normal));
                            }
                            testList.Add(new Tuple<int, int>(chosenIndex + iCounter, (int)PagiItemType.Extend));
                            testList.Add(new Tuple<int, int>(pageCount, (int)PagiItemType.Normal));
                        }
                    }
                }
                else{
                    testList = Enumerable.Range(1, pageCount).ToList().ConvertAll(x => new Tuple<int, int>(x, (int)(x == chosenIndex ? PagiItemType.Chosen : PagiItemType.Normal)));
                }
                //parse datat to repeter
                pagingRepeater.DataSource = testList;
            }
            pagingRepeater.DataBind();
        }
 
        protected void PageNumUpdating(object sender, EventArgs args)
        {
            LinkButton btn = sender as LinkButton;
            BindDatagrid(int.Parse( btn.Text) );
        }

        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return an empty set.  Overload to provide
            // a page specific permission list.

            return new E_InternalUserPermissions[]
			{
				E_InternalUserPermissions.EditBroker
			};
        }
        #region testcode


        #endregion
    }
}
