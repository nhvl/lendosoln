﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditCustomPDFFieldProperties.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Manage.EditCustomPDFFieldProperties" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Edit Custom PDF Field Properties</title>
</head>
<body onload="resize(400, 160);">
    <form id="form1" runat="server">
        <table class="FieldLabel">
            <tr>
                <td>
                    Field ID
                </td>
                <td>
                    <ml:EncodedLiteral ID="Id" runat="server"></ml:EncodedLiteral> 
                </td>
            </tr>
            <tr>
                <td>
                    Friendly Name
                </td>
                <td>
                    <asp:TextBox ID="FriendlyName" Width="250px" MaxLength="200" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Description
                </td>
                <td>
                    <asp:TextBox ID="Description" Width="250px" MaxLength="200" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Category
                </td>
                <td>
                    <asp:DropDownList ID="CategoryName" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Is Review Completed?
                </td>
                <td>
                    <asp:CheckBox ID="IsReviewNeeded" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Is Hidden?
                </td>
                <td>
                    <asp:CheckBox ID="IsHidden" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="SaveButton" OnClick="OnSaveButtonClicked" Text="OK" Width="40px" runat="server" />
                    <input ID="CancelButton" type="button" onclick="window.close();" value="Cancel" runat="server" />
                </td>
            </tr>
        </table>
		<uc1:cmodaldlg id="Cmodaldlg1" runat="server"></uc1:cmodaldlg>
    </form>
</body>
</html>
