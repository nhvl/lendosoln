<%@ Page language="C#" Codebehind="CheckUrl.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Manage.CheckUrl"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Check URL Connectivity - LendingQB" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
    <script type="text/javascript">
        function setClickHandlers(urlTextbox)
        {
            $('#AlertCloseBtn').click(function() 
            {
                $('#AlertDiv').hide();
                urlTextbox.focus();
            });

            $('#RunBtn').click(function()
            {
                $('#AlertDiv').hide()
                $('#LoadingDiv').show();
                this.disabled = 'disabled';

                // In addition to disabling the download button on page load and when
                // an error occurs in the code-behind, we also disable the download button
                // here to prevent a file download when another connectivity check is about
                // to be performed.
                <%=AspxTools.JsGetElementById(DownloadBtn)%>.disabled = 'disabled';

                <%=AspxTools.JsGetElementById(RunCheckHidden)%>.click();
            });
        }

        function setInitialDisplay()
        {
            $('#LoadingDiv').hide();

            $('#RunBtn').disabled = '';

            if(<%=AspxTools.JsGetElementById(HasError)%>.value === 'True')
            {
                $('#AlertDiv').show();
            }
            else
            {
                $('#AlertDiv').hide();
            }
        }

        $('#PMLContain').ready(function()
        {
            var urlTextbox = <%=AspxTools.JsGetElementById(UrlTextbox)%>;

            urlTextbox.focus();

            setClickHandlers(urlTextbox);

            setInitialDisplay();
        });
    </script>

    <div id="AlertDiv" class="alert alert-error alert-dismissible text-center" role="alert">
        <button id="AlertCloseBtn" type="button" class="close">&times;</button>
        <p>
            You must enter a valid URL. Please ensure that your URL is in the same format as one of the following:<br />
            <br /> 
            example.com <br />
            www.example.com <br /> 
            http://www.example.com <br /> 
            https://www.example.com
        </p>
    </div>

    <div id="LoadingDiv" class="alert alert-info text-center" role="alert">
        <p>
            Validating URL and running connectivity test&hellip;
        </p>
    </div>

    <div class="panel panel-default">
        <form runat="server">
            <asp:HiddenField runat="server" ID="HasError" Value="False" />

            <div class="panel-body">
                <div class="row">
                    <!-- Spacer -->
                    <div class="col-sm-3">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <div>
                                <asp:TextBox runat="server" ID="UrlTextbox" CssClass="form-control text-center"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2 text-center">
                        <div class="form-group form-group-sm">
                            <!-- We need two separate buttons here so that the visible button, RunBtn, can
                                be disabled when clicked. If we only have the ASP button, disabling that button
                                through JavaScript when clicked will prevent the RunCheck_Click event from firing. -->
                            <span class="clipper">
                                <input type="button" id="RunBtn" class="btn btn-primary" value="Test URL Connectivity" />
                            </span>
                            <span style="display: none">
                                <asp:Button runat="server" ID="RunCheckHidden" OnClick="RunCheck_Click" />
                            </span>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group form-group-sm">
                            <span class="clipper">
                                <asp:Button runat="server" ID="DownloadBtn" CssClass="btn btn-primary" Text="Download Result As XML File" 
                                    OnClick="DownloadBtn_Click" />
                            </span>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group form-group-sm">
                            <p>Current Server:</p>
                        </div>
                    </div>

                    <div class="col-sm-8">
                        <div class="form-group form-group-sm">
                            <p id="RespServer" runat="server">&nbsp;</p>
                        </div>
                    </div>

                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group form-group-sm">
                            <p>Status:</p>
                        </div>
                    </div>

                    <div class="col-sm-8">
                        <div class="form-group form-group-sm">
                            <p id="Status" runat="server">&nbsp;</p>
                        </div>
                    </div>

                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group form-group-sm">
                            <p>Response Headers:</p>
                        </div>
                    </div>

                    <div class="col-sm-8">
                        <div class="form-group form-group-sm">
                            <pre><span id="MsgHeaders" runat="server">&nbsp;</span></pre>
                        </div>
                    </div>

                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>
                </div>

                <div class="row">
                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group form-group-sm">
                            <p>Response Body:</p>
                        </div>
                    </div>

                    <div class="col-sm-8">
                        <div class="form-group form-group-sm">
                            <pre><span id="MsgBody" runat="server">&nbsp;</span></pre>
                        </div>
                    </div>

                    <!-- Spacer -->
                    <div class="col-sm-1">
                        <div class="form-group form-group-sm">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
        </form>
     </div>
</asp:Content>
