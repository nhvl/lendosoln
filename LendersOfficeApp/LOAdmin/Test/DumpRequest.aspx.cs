﻿namespace LendersOfficeApp.LOAdmin.Test
{
    using System;
    using System.IO;
    using LendersOffice.Common;

    /// <summary>
    /// Page will dump the request content to a temporary file and return the path to that file in the response.
    /// </summary>
    public partial class DumpRequest : MinimalPage
    {
        /// <summary>
        /// Gets or sets the path to the file where the request data is dumped.
        /// </summary>
        /// <value>The path to the file where the request data is dumped.</value>
        protected string DumpPath { get; set; }

        /// <summary>
        /// Read the request data and dump it into a temp file.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.DumpPath = Path.GetTempFileName();

            using (StreamWriter writer = File.CreateText(this.DumpPath))
            {
                var req = this.Request;

                writer.WriteLine(this.GetNameAndValue("HttpMethod", req.HttpMethod));
                writer.WriteLine(this.GetNameAndValue("ContentType", req.ContentType));
                writer.WriteLine(this.GetNameAndValue("ClientCertificate", req.ClientCertificate != null ? "YES" : "NO"));
                foreach (var name in req.QueryString.AllKeys)
                {
                    writer.WriteLine(this.GetNameAndValue("QueryString." + name, req.QueryString[name]));
                }

                foreach (var name in req.Headers.AllKeys)
                {
                    writer.WriteLine(this.GetNameAndValue("Header." + name, req.Headers[name]));
                }

                foreach (var name in req.Cookies.AllKeys)
                {
                    writer.WriteLine(this.GetNameAndValue("Cookie." + name, req.Cookies[name].Value));
                }

                if (req.HttpMethod == "POST")
                {
                    if (req.ContentType == "application/x-www-form-urlencoded")
                    {
                        foreach (var name in req.Form.AllKeys)
                        {
                            writer.WriteLine(this.GetNameAndValue("Form." + name, req.Form[name]));
                        }
                    }
                    else
                    {
                        using (StreamReader reader = new StreamReader(req.InputStream))
                        {
                            writer.WriteLine(this.GetNameAndValue("Body", reader.ReadToEnd()));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Formulate a name=value string.
        /// </summary>
        /// <param name="name">The name for the value.</param>
        /// <param name="value">The value associated with the name.</param>
        /// <returns>A name=value string.</returns>
        private string GetNameAndValue(string name, string value)
        {
            return string.Concat(name, "=", value);
        }
    }
}
