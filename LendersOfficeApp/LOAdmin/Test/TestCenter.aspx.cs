using System;
using System.Web.Services;
using LUnit;

namespace LendersOfficeApp.LOAdmin.Test
{
	/// <summary>
	/// Summary description for TestCenter.
	/// </summary>
    public partial class TestCenter : LendersOffice.Admin.SecuredAdminPage
	{
        [WebMethod]
        public static LUnit.WebShim.TestClassData[] GetTestManager()
        {
            var tester = CreateRunner();
            var response = tester.GetTestFixtures();
            return response;
        }

        [WebMethod]
        public static LUnit.WebShim.TestClassResult[] RunTestMethod(int fixtureId, int methodId)
        {
            var tester = CreateRunner();
            var result = tester.RunSingleTest(fixtureId, methodId);
            return new LUnit.WebShim.TestClassResult[] { result };
        }

        [WebMethod]
        public static LUnit.WebShim.TestClassResult[] RunTestFixture(int fixtureId)
        {
            var tester = CreateRunner();
            var response = tester.RunFixture(fixtureId);
            return response;
        }

        private static LUnit.WebShim.IWebRunner CreateRunner()
        {
            return WebRunnerFactory.Create(LUnitType.Current);
        }

        void RegisterResources()
        {
            RegisterJsScript("LOAdmin.Test.TestCenter.js");
        }

        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            RegisterResources();
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
