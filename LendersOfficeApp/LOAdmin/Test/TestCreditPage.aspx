<%@ Page language="c#" Codebehind="TestCreditPage.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Test.TestCreditPage" enableViewState="False"%>
<%@ Register TagPrefix="uc1" TagName="header" Src="../common/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="HeaderNav" Src="../../common/HeaderNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>TestCreditPage</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="../css/stylesheet.css" type=text/css rel=stylesheet>
    <style>
    .Success { COLOR: green }
    .Fail { COLOR: red }
    table.td { line-height:normal; }
    </style>
    <LINK href="../../css/stylesheet.css" type=text/css rel=stylesheet >
</HEAD>
  <body MS_POSITIONING="FlowLayout">
	<script language=javascript>
<!--
var gTestCount = 0;
var gCurrentIndex = 0;
var aTestCases = null;
function _init() {

}
function f_runAll() {
  gCurrentIndex = 0;
  gTestCount = aTestCases.length;
  f_run(aTestCases[0], true);
}
function f_runIncludeMCL() {
  aTestCases = mclTestCases;
  f_runAll();
}
function f_runExcludeMCL() {
  aTestCases = nonMclTestCases;
  f_runAll();
}
function f_runNextTest() {

  if (++gCurrentIndex < gTestCount) {
    f_run(aTestCases[gCurrentIndex], true);
  }

}

function f_run(id, bRunNextTest) {
  var label = document.getElementById(id);
  label.innerText = "Running...";

  window.setTimeout(f_runImpl, 300, id, bRunNextTest);
}
function f_runImpl(id, bRunNextTest) {
  var label = document.getElementById(id);
  var args = new Object();
  var result = gService.test.call(id, args);
  if (!result.error) {
    label.innerHTML = result.value.Message;
  } else {
    label.innerHTML = "<font color=red>FAILED</font>";

  }
  if (bRunNextTest)
    f_runNextTest();

}
//-->
</script>

    <form id="TestCreditPage" method="post" runat="server">
    <uc1:header id=Header1 runat="server"></uc1:header>
						<uc1:headernav id="HeaderNav1" runat="server">
							<menuitem label="Main" url="~/LOAdmin/main.aspx"></menuitem>
							<menuitem label="Credit Report Test Pages"></menuitem>
						</uc1:headernav>
    <hr>
    <table cellpadding=1>
    <tr bgcolor=gainsboro>
    <td colspan=3><b>Notes</b></td>
    </tr>
    <tr valign="top">
    <td>01/10/2005</td>
    <td>dd</td>
    <td>Connection to LandSafe servers (stage &amp; production) are slow and may result in timeouts (especially on weekends).&nbsp; Therefore,
    if errors still occur on LandSafe servers after multiple tries, just ignore it.</td>
    </tr>
    <tr valign="top">
    <td style="width:75px;">10/26/2005</td>
    <td style="width:30px;">dd</td>
    <td>Removed test connections to MCL.&nbsp; Reason:&nbsp; all credit requests using test instant view id will route to http://credit.meridianlink.com.&nbsp;
    We don't have instant view id of all MCL CRAs to perform the correct tests.</td>
    </tr>
    <tr valign="top">
    <td>11/10/2006</td>
    <td>db</td>
    <td>Hide extra button.&nbsp; We only need one button now to run all tests in gray area, since individual MCL CRAs are no longer tested (see David's note above).</td>
    </tr>
    <tr valign="top">
    <td>12/08/2009</td>
    <td>fs</td>
    <td>Updated <i>MCL Instant View ID</i> to use Conststage values instead of hardcoded ones.</td>
    </tr>
    </table>
    <hr>
    <!--input type="button" onclick="f_runIncludeMCL();" value="Run All"-->
    <input type="button" onclick="f_runExcludeMCL();" value="Run All">
    <br><br><ml:PassthroughLiteral id=MainHtml runat="server"></ml:PassthroughLiteral>
     </form>

  </body>
</HTML>
