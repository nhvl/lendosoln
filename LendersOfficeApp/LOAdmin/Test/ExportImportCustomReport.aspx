﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportImportCustomReport.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Test.ExportImportCustomReport" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="../../common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../Common/Header.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Export/Import Custom Report</title>
</head>
<body>
    <UC:HEADER id="m_Header" runat="server"></UC:HEADER>
    <UC:HEADERNAV id="m_Navigate" runat="server">
	    <MenuItem URL="~/LOAdmin/Main.aspx" Label="Main"></MenuItem>
	    <MenuItem URL="." Label="Disclosure Automation"></MenuItem>
	</UC:HEADERNAV>
	
    <form id="form1" runat="server">
    <ul>
        <li>
            <div class=FieldLabel>Enter Broker AND User Name to Export Custom Report</div>
            <asp:DropDownList ID="ExportCustomReportBroker" runat="server" />
            <asp:TextBox ID="ExportCustomReportUserName" runat="server" />
            <span onclick="return f_checkExportUserName();"><asp:Button ID="Button1" runat="server" Text="Export" onclick="OnClickExportCustomReport" /></span>
        </li>
        
        <li>
            
            
            <div class=FieldLabel>Select one xml file to upload:</div>

            <asp:FileUpload id="FileUpload1" runat="server">
            </asp:FileUpload>

            <br/><br/>
            <div class=FieldLabel>Enter Broker AND User Name to Import Custom Report into his/her account</div>
            <asp:DropDownList ID="ImportCustomReportBroker" runat="server" />
            <asp:TextBox ID="ImportCustomReportUserName" runat="server" />
            
            <span onclick="return f_checkImportUserName() && f_LoanUpdateWarning();">
                <asp:Button id="UploadButton" Text="Import Loan File" onclick="UploadButton_Click" runat="server">
                </asp:Button>
            </span>
           
            <ml:EncodedLabel id="UploadStatusLabel" runat="server"></ml:EncodedLabel>
            <ml:EncodedLabel id="DatabaseStatusLabel" runat="server"></ml:EncodedLabel>
           
            <hr />
            
            <br/><br/><br/>
            <h3>Instruction</h3>
            <div>
                Please try to save the export xml directly.
                <br/>If you open it, try to use the software(e.g. browser) which you use to open the xml file to save the file.
                <br/>For example, in IE, use menu Page/Save As to save the file.
                <br/>If you want to copy the xml, please ensure the xml format is correct.
                <br/>Some browser will show the empty space in the front of each line as well as some other characters like '-', '+'.
                <br/>Please make sure to erase them before you do the import.
                <br/>Overall, in order to use this tool conveniently and avoid any errors, please try your best to save it directly.
                <br/>
                <br/>Thank you!
            </div>
            
        </li>
    </ul>
    </form>
    
    <script type="text/javascript">
    <!--
    function f_checkExportUserName() {
		var textbox = document.getElementById('ExportCustomReportUserName');
        var val = textbox.value;
        if (!String.prototype.trim) {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/gm, '');
            };
        }
        if (val == "") {
            alert("Please enter user name for export.");
            return false;
        } else if (val.trim() == "") {
            alert("User Name must not be blank.");
            return false;
        }
        return true;
    }
    function f_checkImportUserName() {
        var textbox = document.getElementById('ImportCustomReportUserName');
        var val = textbox.value;
        if (!String.prototype.trim) {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/gm, '');
            };
        }
        if (val == "") {
            alert("Please enter user name for import.");
            return false;
        } else if (val.trim() == "") {
            alert("User Name must not be blank.");
            return false;
        }
        return true;
    }

    function f_LoanUpdateWarning() {
        var code = Math.floor(Math.random() * 1000) + '';
        var ans = prompt("WARNING: Data will only be imported. The original data in this user account will remain the same." +
	  "Please type '" + code + "' below to confirm it:", "");
        var doIt = (ans != null && ans == code);

        if (doIt == false && ans != null)
            alert("Code does not match.  Deletions will not be performed.");

        return doIt;
    }
    //-->
    </script>
    
</body>
</html>
