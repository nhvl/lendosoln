﻿//-----------------------------------------------------------------------
// <summary>
//      Debug TeamUserAssignmentInfo
// </summary>
// <copyright file="TeamUserAssignmentInfo.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LendersOffice.Debugging
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using LendersOffice.Db.TeamUserAssignments;
    using LendersOffice.Security;

    /// <summary>
    /// A class intended for use by the DynamicCall page to return bad team user assignments.
    /// </summary>
    public class TeamUserAssignmentInfo
    {
        /// <summary>
        /// Gets a string representation of the team user assignments where there's more than one primary team assigned for a user and role.
        /// </summary>
        /// <param name="dataInput">Passed from the dynamic call page.</param>
        /// <returns>A string representing the employee role combos where there's more than one primary team.</returns>
        [DynamicCall]
        public static string GetTeamAssignmentsTooManyPrimaries(DataInput dataInput)
        {
            return GetTeamAssignmentsString(tq => tq.FindEmployeesWithTooManyPrimaryTeamsForARole());
        }

        /// <summary>
        /// Gets a string representation of the team user assignments where there's teams assigned for a user and role but no primary.
        /// </summary>
        /// <param name="dataInput">Passed from the dynamic call page.</param>
        /// <returns>A string representing the employee role combos where there's teams assigned for a user and role but no primary..</returns>
        [DynamicCall]
        public static string GetTeamAssignmentsNoPrimaries(DataInput dataInput)
        {
            return GetTeamAssignmentsString(tq => tq.FindEmployeesWithTeamsButNoPrimaryTeamsForARole());
        }

        /// <summary>
        /// Gets a string representation of the bad team assignments given the resultGetter method.
        /// </summary>
        /// <param name="resultGetter">A method of the Querier to get the result set.</param>
        /// <returns>A string represention of the result set.</returns>
        private static string GetTeamAssignmentsString(Func<Querier, IEnumerable<QueryResult>> resultGetter)
        {
            var querier = new Querier(PrincipalFactory.CurrentPrincipal);
            var sb = new StringBuilder();

            sb.AppendLine(QueryResult.GetTsvHeaderString());

            foreach (var result in resultGetter(querier))
            {
                sb.AppendLine(result.AsTsvString());
            }

            return sb.ToString();
        }
    }
}