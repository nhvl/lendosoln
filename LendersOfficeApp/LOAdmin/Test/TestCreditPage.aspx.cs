using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

namespace LendersOfficeApp.LOAdmin.Test
{
    public partial class TestCreditPage : LendersOffice.Admin.SecuredAdminPage
    {

        private string[][] m_testCases = {
                                             new string[] {"MCL New Credit Report <a href='mailto:helen@meridianlink.com'>Report Error</a>", "_test001" },
                                             new string[] {"MCL Instant View ID <a href='mailto:helen@meridianlink.com'>Report Error</a>", "_test002" },
                                             /* vm - 4/19/10, OPM 49481 - We are no longer support direct connection to LandSafe and Kroll Factual Data.
                                             new string[] {"Kroll Factual Data", "_test003" },
                                             new string[] {"LandSafe Production Server", "_test004" },*/
                                             /* LandSafe Staging server failed too many times during smoke test. Disable it.
                                             new string[] {"LandSafe Staged Server", "_test004a" },
                                             */
                                             new string[] {"CREDCO", "_test005" },
											 /* dl - 10/31/06.  David advised that no one is using Advantage FL.  I think we
											  * also have a Fannie connection for them.
                                             new string[] {"Advantage Credit (FL)", "_test006"},
											 
											  * We are no longer support direct integration to StandFacts. Currently no one
                                              * using it anyway. If in future they want to use it then force them to go through
                                              * FannieMae connection. dd 8/26/05
                                             new string[] {"StandFacts Credit Services", "_test007"},
                                             */
                                             /* vm - 4/19/10, OPM 49481 - We are no longer support direct connection to Universal Credit.
                                             new string[] {"Universal Credit", "_test008"},*/
                                             new string[] {"Fiserv / Chase", "_test009" },
                                             new string[] {"FannieMae", "_test010" },
//                                             new string[] {"Info Network", "_test011"}, // dd 05/14/2007 - Test account no longer work. Info Netword does not response regarding update on test account.
                                             //new string[] {"SharperLending", "_test012"}, // db 6/23/08 - OPM 22965 Remove Sharper Lending protocol from test since we use fannie mae connections to them now
											 new string[] {"Credit Quick Services", "_test013"},
											 new string[] {"Old Republic Credit Services", "_test014"},
											 new string[] {"CSC", "_test015"},
											 new string[] {"CSD (Joint Borrowers)", "_test016"},
											 new string[] {"CSD (Single Borrower)", "_test016a"},

											 new string[] {"CBC", "_test017"}
                                         };
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            ArrayList testFunctionList = new ArrayList();
            ArrayList testFunctionListExcludeMCL = new ArrayList();

            StringBuilder sb = new StringBuilder();

            sb.Append("<table border=1><tbody style='background-color:gainsboro'>");
            foreach (string[] testCase in m_testCases) 
            {
                testFunctionList.Add("'" + testCase[1] + "'");
                testFunctionListExcludeMCL.Add("'" + testCase[1] + "'");
                sb.AppendFormat(@"<tr><td>{0}</td><td><div id={1}></td><td><a href=""#"" onclick=""f_run('{1}', false);"">re-run</a></td></tr>", testCase[0], testCase[1]);
            }
            
//            sb.AppendFormat("</tbody><tr><td colspan=3>--- Individial MCL CRA -- </td></tr>");
//            for (int i = 0; i < TestCreditPageService.MCL_CRAS.Length; i++) 
//            {
//                string cra = TestCreditPageService.MCL_CRAS[i];
//                string jsFunction = "_test002_" + i;
//                testFunctionList.Add("'" + jsFunction + "'");
//                sb.AppendFormat(@"<tr><td>MCL Instant View ID on {0}</td><td><div id={1}></td><td><a href=""#"" onclick=""f_run('{1}', false); return false;"">re-run</a></td></tr>", cra, jsFunction);
//            }

            sb.Append("</table>");

            MainHtml.Text = sb.ToString();
            ClientScript.RegisterArrayDeclaration("mclTestCases", string.Join(",", (string[]) testFunctionList.ToArray(typeof(string))));
            ClientScript.RegisterArrayDeclaration("nonMclTestCases", string.Join(",", (string[]) testFunctionListExcludeMCL.ToArray(typeof(string))));

		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterService("test", "/loadmin/test/testcreditpageservice.aspx");
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
