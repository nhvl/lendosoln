﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Xml;
using LendersOffice.Admin;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Test
{
    public partial class ExportImportCustomReport : LendersOffice.Admin.SecuredAdminPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var dictionary = BrokerDbLite.ListAllSlow();

            List<KeyValuePair<Guid, string>> list = new List<KeyValuePair<Guid, string>>();

            foreach (var o in dictionary)
            {
                if (o.Value.Status == 0)
                {
                    continue;
                }
                list.Add(new KeyValuePair<Guid, string>(o.Value.BrokerId, o.Value.BrokerNm + " (" + o.Value.CustomerCode + ")"));
            }
            var orderList = list.OrderBy(o => o.Value);

            ExportCustomReportBroker.DataSource = orderList;
            ExportCustomReportBroker.DataTextField = "Value";
            ExportCustomReportBroker.DataValueField = "Key";
            ExportCustomReportBroker.DataBind();

            ImportCustomReportBroker.DataSource = orderList;
            ImportCustomReportBroker.DataTextField = "Value";
            ImportCustomReportBroker.DataValueField = "Key";
            ImportCustomReportBroker.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void OnClickExportCustomReport(object sender, EventArgs e)
        {
            string userName = ExportCustomReportUserName.Text;
            Guid brokerId = new Guid(ExportCustomReportBroker.SelectedValue);
            string fileName = "", msg = "";
            List<string> errorMsgList = new List<string>();
            bool isError = ExportAllCustomReportByUser(ref fileName, ref errorMsgList, brokerId, userName);
            if (errorMsgList.Count > 0)
            {
                msg = "";
                foreach (string m in errorMsgList)
                {
                    msg += m + @".\n";
                }
                string cmd = @"<script language='javascript'>alert('The following errors or warnings have occurred: \n" + msg + "');</script>";
                Response.Write(cmd);
            }
            else
            {
                Response.AddHeader("Content-Disposition", $"attachment; filename=\"{fileName}\"");
                Response.ContentType = "text/xml";
                Response.TransmitFile(fileName);
                Response.End();
            }
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            string savePath = "", userName = ImportCustomReportUserName.Text;

            Guid brokerId = new Guid(ImportCustomReportBroker.SelectedValue);

            if (FileUpload1.HasFile)
            {
                int fileSize = FileUpload1.PostedFile.ContentLength;
                savePath = TempFileUtils.NewTempFilePath() + "_upload.xml";
                FileUpload1.SaveAs(savePath);
                UploadStatusLabel.Text = "Your file was uploaded successfully.";
                List<string> errorMsgList = new List<string>();
                importCustomReport(ref errorMsgList, brokerId, userName, savePath);
                string msg = "";
                if (errorMsgList.Count > 0)
                {
                    msg = "";
                    foreach (string m in errorMsgList)
                    {
                        msg += m + @".\n";
                    }
                    string cmd = @"<script language='javascript'>alert('The following errors or warnings have occurred: \n" + msg + "');</script>";
                    Response.Write(cmd);
                    UploadStatusLabel.Text = "File is not updated. Please correct the error and try again.";
                }
                else UploadStatusLabel.Text = "File is successfully updated.";
            }
            else
            {
                UploadStatusLabel.Text = "You did not specify a file to upload.";
            }
        }

        protected bool ExportAllCustomReportByUser(ref string uniqueFileName, ref List<string> errorMsgList, Guid brokerId, string userName)
        {
            bool isSafe = true;
            string mEmpId = "", mQueryNm = "", mXmlContent = "", mIsPublished = "", mNamePublishedAs = "", mShowPosition = "";
            Guid mBrokerId;

            uniqueFileName = TempFileUtils.NewTempFilePath() + ".xml";
            XElement allReportsOfCurrUser = new XElement("AllReportsOfCurrUser");
            XDocument xdoc = new XDocument(allReportsOfCurrUser);

            SqlParameter[] parameters = { new SqlParameter("LoginNm", userName) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetBrokerIdAndEmpIdByLoginNm", parameters))
            {
                while (reader.Read())
                {
                    mBrokerId = (Guid)reader["BrId"];
                    if (mBrokerId != brokerId)
                    {
                        continue;
                    }

                    mQueryNm = (string)reader["QueryName"];
                    if (mQueryNm == null || mQueryNm.TrimWhitespaceAndBOM() == "")
                    {
                        errorMsgList.Add(@"Error: QueryName must not be blank.");
                        return true;
                    }

                    
                    mEmpId = reader["EmpId"].ToString();
                    
                    mXmlContent = reader["XmlContent"].ToString().TrimWhitespaceAndBOM();
                    mIsPublished = reader["IsPublished"].ToString(); 
                    mNamePublishedAs = (string)reader["NamePublishedAs"];
                    mShowPosition = reader["ShowPosition"].ToString();

                    

                    XElement report = new XElement("Report",
                        new XElement("t", new XAttribute("n", "BrokerId"), mBrokerId.ToString()),
                        new XElement("t", new XAttribute("n", "EmployeeId"), mEmpId),
                        new XElement("t", new XAttribute("n", "QueryName"), mQueryNm),
                        new XElement("t", new XAttribute("n", "IsPublished"), mIsPublished),
                        new XElement("t", new XAttribute("n", "NamePublishedAs"), mNamePublishedAs),
                        new XElement("t", new XAttribute("n", "ShowPosition"), mShowPosition),
                        new XElement("t", new XAttribute("n", "XmlContent"), new XCData(mXmlContent))
                    );
                    allReportsOfCurrUser.Add(report);
                }
            }

            xdoc.Save(uniqueFileName);
            return isSafe;
        }

        
        //import custom report by login name
        protected void importCustomReport(ref List<string> errorMsgList, Guid brokerId, string userName, string savePath)
        {
            //need to check if any query name already exists: scan the xml and locate all the query name. 
            //Then do sql query of related query name. Prompt warning before doing any update
            SqlParameter[] parameters = { new SqlParameter("LoginNm", userName) };
            List<string> sqlQueryNmList = new List<string>();
            using (DbDataReader sr = StoredProcedureHelper.ExecuteReader(brokerId, "GetBrokerIdAndEmpIdByLoginNm", parameters))
            {
                while (sr.Read())
                {
                    sqlQueryNmList.Add((string)sr["QueryName"]);
                }
            }
            XDocument xdoc = XDocument.Load(savePath);
            IEnumerable<string> xmlQueryNmList = from r in xdoc.Descendants("Report")
                        from t in r.Elements()
                        where t.Attribute("n").Value == "QueryName"
                        select t.Value;
            List<string> dupQueryNmList = new List<string>();
            foreach (string xmlQueryNm in xmlQueryNmList)
            {
                if (sqlQueryNmList.Contains(xmlQueryNm))
                {
                    dupQueryNmList.Add(xmlQueryNm);
                }
            }
            if (dupQueryNmList.Count > 0)
            {
                string msg = "The following Query Name already exists: ";
                foreach (string dupQueryNm in dupQueryNmList)
                    msg += "\"" + dupQueryNm + "\", ";
                msg.TrimEnd();
                msg.TrimEnd(',');
                msg += ". Please rename the report query name.";
                errorMsgList.Add(msg);
                return;
            }

            //Since no duplicate query name, start importing...
            string mQueryNm = "", mXmlContent = "", mIsPublished = "", mNamePublishedAs = "", mShowPosition = ""; ;
            using (XmlTextReader reader = new XmlTextReader(savePath))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name.ToLower() == "report")
                    {
                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.EndElement && reader.Name.ToLower() == "report")
                            {
                                SqlParameter[] paras = { new SqlParameter("LoginNm", userName), new SqlParameter("QueryId", Guid.NewGuid()), 
                                       new SqlParameter("QueryNm", mQueryNm), new SqlParameter("XmlContent", mXmlContent), 
                                       new SqlParameter("IsPublished", mIsPublished), new SqlParameter("NamePublishedAs", mNamePublishedAs), 
                                       new SqlParameter("ShowPosition", mShowPosition) };
                                StoredProcedureHelper.ExecuteNonQuery(brokerId, "ImportCustomReportByLoginNm", 1, paras);
                                break;
                            }
                            if (reader.HasAttributes)
                            {
                                reader.MoveToNextAttribute();
                                switch(reader.Value)
                                {
                                    case "QueryName":
                                        reader.Read();
                                        if (reader.Value == null || reader.Value.TrimWhitespaceAndBOM() == "") { errorMsgList.Add("Error: QueryName is blank. The previous Query name(if exists) is " + mQueryNm); } 
                                        else mQueryNm = reader.Value;
                                        break;
                                    case "IsPublished":
                                        reader.Read();
                                        mIsPublished = (reader.Value == null) ? "" : reader.Value;
                                        break;
                                    case "NamePublishedAs":
                                        reader.Read();
                                        mNamePublishedAs = (reader.Value == null) ? "" : reader.Value;
                                        break;
                                    case "ShowPosition":
                                        reader.Read();
                                        mShowPosition = (reader.Value == null) ? "" : reader.Value;
                                        break;
                                    case "XmlContent":
                                        reader.Read();
                                        mXmlContent = (reader.Value == null) ? "" : reader.Value.Trim(' ', '\n');
                                        break;
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}
