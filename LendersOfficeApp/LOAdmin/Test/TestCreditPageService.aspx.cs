using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

using LendersOffice.CreditReport;
using LendersOffice.CreditReport.Mcl;

using LendersOfficeApp.los.admin;
using CommonLib;
using LendersOffice.Constants;

using NUnit.Framework;
using LUnit;

namespace LendersOfficeApp.LOAdmin.Test
{
    public partial class TestCreditPageService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName) 
        {
            TestCreditPageModel obj = new TestCreditPageModel();
            obj.Process( methodName ); 

            string key, val;
            obj.GetResult( out key, out val);
            SetResult( key, val );

        }
    }

    [LoTestFixture]
    internal class TestCreditPageModel
    {
        string m_key = null, m_value=null;
        void SetResult(string key, string val) 
        {
            m_key   = key;
            m_value = val;
        }

        public void GetResult( out string key, out string val )
        {
            key = m_key;
            val = m_value;
        }


        [TearDown]
        public void UnitTestProcessResult()
        {
            string msg = m_value;

            if( msg.IndexOf("<span class=Success>") >= 0 )
                return;

            if( msg.IndexOf("<span class=Fail>") >= 0 )
            {
                msg = msg.Substring( "<span class=Fail>".Length ).Replace("</span>","")
                         .Replace("<br>", System.Environment.NewLine);
                throw new ShowMessageOnlyExc( msg  );
            }
                

             throw new ShowMessageOnlyExc("Internal error : please review test code because can not recognize the result\r\n" + m_value );
            


        }

        #region ADD/REMOVE MCL CRA HERE
        public static string [] MCL_CRAS = new string[] {
                                                            "acr.meridianlink.com",
                                                            "acs.meridianlink.com",
                                                            "advantageplus.meridianlink.com",
                                                            "alliance.meridianlink.com",
                                                            "amcr.meridianlink.com",
                                                            "aminfcb.meridianlink.com",
                                                            "apluscreditco.meridianlink.com",
                                                            "arc.meridianlink.com",
                                                            "birchwood.meridianlink.com",
                                                            "calcoast.meridianlink.com",
                                                            "calmayacredit.meridianlink.com",
                                                            "cbetinc.meridianlink.com",
                                                            "cbfbusinesssolutions.meridianlink.com",
                                                            "cbr.meridianlink.com",
                                                            "cbs.meridianlink.com",
                                                            "cby.meridianlink.com",
                                                            "cci.meridianlink.com",
                                                            "ccismortgage.meridianlink.com",
                                                            "cdi.meridianlink.com",
                                                            "cds.meridianlink.com",
                                                            "cdsfl.meridianlink.com",
                                                            "certifiedcredit.meridianlink.com",
                                                            "cic.meridianlink.com",
                                                            "cis.meridianlink.com",
                                                            "cm.meridianlink.com",
                                                            "cmr.meridianlink.com",
                                                            "cpsg.meridianlink.com",
                                                            "credit.advcredit.com",
                                                            "credit.ciscocredit.com",
                                                            "credit.coastalcreditcorp.com",
                                                            "credit.credamerica.net",
                                                            "credit.creditplus.com",
                                                            "credit.creditsearchinc.com",
                                                            "credit.credittechnologies.com",
                                                            "credit.csc4credit.com",
                                                            "credit.datafaxcredit.com",
                                                            "credit.dcicredit.com",
                                                            "credit.financialresearch.com",
                                                            "credit.firstcheck.com",
                                                            "credit.kewaneecreditbureau.com",
                                                            "credit.mcs-mclhost.com",
                                                            "credit.nccredit.com",
                                                            "credit.pacificcreditlineservices.com",
                                                            "credit.westerndataresearch.com",
                                                            "creditlink.meridianlink.com",
                                                            "creditonline.meridianlink.com",
                                                            "creditresources.meridianlink.com",
                                                            "cri.meridianlink.com",
                                                            "crs.meridianlink.com",
                                                            "ctinetwork.meridianlink.com",
                                                            "cvi.meridianlink.com",
                                                            "dpcredit.meridianlink.com",
                                                            "equidata.meridianlink.com",
                                                            "fdr.meridianlink.com",
                                                            "fedcomp.meridianlink.com",
                                                            "firstsource.meridianlink.com",
                                                            "fiserv.chasecredit.com",
                                                            "innovativecredit.meridianlink.com",
                                                            "iscsite.meridianlink.com",
                                                            "login.ncacredit.com",
                                                            "mcb.meridianlink.com",
                                                            "mcbsav.meridianlink.com",
                                                            "mcd.meridianlink.com",
                                                            "midwestcredit.meridianlink.com",
                                                            "ncd.meridianlink.com",
                                                            "ncs.meridianlink.com",
                                                            "order.creditdata.net",
                                                            "order.professionalcreditservices.com",
                                                            "premium.meridianlink.com",
                                                            "qmr.meridianlink.com",
                                                            "reports.qccredit.com",
                                                            "royal.meridianlink.com",
                                                            "sos.meridianlink.com",
                                                            "svc1stinfo.meridianlink.com",
                                                            "swcsi.meridianlink.com",
                                                            "taz.meridianlink.com",
                                                            "unicred.meridianlink.com",
                                                            "unitedoneresources.meridianlink.com",
                                                            "vip.meridianlink.com",
                                                            "wmr.meridianlink.com"
                                                        };
            #endregion



        public void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "_test001": Test001(); break;
                case "_test002": Test002(); break;
                case "_test003": Test003(); break;
                case "_test004": Test004(); break;
                case "_test004a": Test004a(); break;
                case "_test005": Test005(); break;
                case "_test006": Test006(); break;
                case "_test007": Test007(); break;
                case "_test008": Test008(); break;
                case "_test009": Test009(); break;
                case "_test010": Test010(); break;
                case "_test011": Test011(); break;
                case "_test012": Test012(); break;
				case "_test013": Test013(); break;
				case "_test014": Test014(); break;
				case "_test015": Test015(); break;
				case "_test016": Test016(); break;
                case "_test016a": Test016a(); break;
				case "_test017": Test017(); break; // OPM 19664 - CBC
            }
            if (methodName.StartsWith("_test002_")) 
            {
                try 
                {
                    int index = int.Parse(methodName.Substring(9));
                    TestMclCra(index);
                } 
                catch {}
            }
        }

        /// <summary>
        /// Pull new MCL Credit Report.
        /// </summary>
        [LoTest(Description="MCL New Credit Report", Timeout=60 /*seconds*/ )]
        private void Test001()
        {
			 CreditRequestData data; 
		/*	if (! ConstStage.AppendMismoToMclCreditReportAndCompare ) 
			{
				// zyxzyx12, x123y123
				data  = CreateRequestData("https://credit.meridianlink.com", CreditReportProtocol.Mcl,
					"", "xyz", "zyxzyx13",
					"BILL", "STEINER", "216-96-5192",
					"", "", "",
					"563 68th B", "ARVERNE", "NY", "11692");
				data.ProviderId = "A4"; 
			}
			else 
			{*/
				data  = CreateRequestData("http://beta.mortgagecreditlink.com", CreditReportProtocol.Mcl,
					"", "pml_test", ConstStage.CreditTestPasswordForMcl,
					"BILL", "STEINER", "216-96-5192",
					"", "", "",
					"563 68th B", "ARVERNE", "NY", "11692");
				data.ProviderId = "CC";
                data.MclRequestType = MclRequestType.New; 
			data.ReportID="";
            OrderCreditReport(data);

        }

		/// <summary>
		/// Pull new MCL Mismo Credit Report.
		/// </summary>
		private void Test001_mismo()   
		{
			CreditRequestData data = CreateRequestData("http://beta.mortgagecreditlink.com/inetapi/au/get_credit_report.aspx ", CreditReportProtocol.Mcl,
				"", "pml_test", "zyxzyx13",
				"BILL", "STEINER", "216-96-5192",
				"", "", "",
				"563 68th B", "ARVERNE", "NY", "11692");

			OrderCreditReport(data);

		}

        /// <summary>
        /// Pull MCL InstantView ID Report
        /// </summary>
        [LoTest(Description="MCL Instant View ID", Timeout=60 /*seconds*/ )]
        private void Test002() 
        {
			CreditRequestData data;
			data = CreditRequestData.CreateForTesting();
            string ReportID = "35888";
            string InstantViewID = "CC-30F1D463";
		
			data.Url=  "http://beta.mortgagecreditlink.com/inetapi/AU/get_credit_report_mclxml.aspx";
			data.CreditProtocol =  CreditReportProtocol.Mcl;
            try
            {
                string co = ConstStage.CreditTestPasswordForMCLInstantView;
                string[] split = co.Split(';');
                ReportID = split[0];
                InstantViewID = split[1];
            }
            catch { }

            data.ReportID = ReportID;
            data.InstantViewID = InstantViewID;   //URL is set from the instant view pw
		
            data.MclRequestType = MclRequestType.Get; 
            OrderCreditReport(data);
        }

		private void Test002_mismo() 
		{
			CreditRequestData data = CreditRequestData.CreateForTesting();
			data.Url = "http://beta.mortgagecreditlink.com/inetapi/au/get_credit_report.aspx ";
			data.CreditProtocol = CreditReportProtocol.Mcl;
			data.ReportID = "35888";
			data.InstantViewID = "CC-30F1D463";
			data.LoginInfo.AccountIdentifier = "INSTANTVIEW";
			data.LoginInfo.UserName = "INSTANTVIEW";
			data.LoginInfo.Password = "CC-30F1D463";
            data.MclRequestType = MclRequestType.Get; 
			OrderCreditReport(data);
		}

        private void TestMclCra(int index) 
        {
            string url = "https://" + MCL_CRAS[index];

            CreditRequestData data = CreditRequestData.CreateForTesting();
            data.Url = url;
            data.CreditProtocol = CreditReportProtocol.Mcl;
            string ReportID = "2705";
            string InstantViewID = "A4-4F39361";
            try
            {
                string co = ConstStage.CreditTestPasswordForMCLInstantView;
                string[] split = co.Split(';');
                ReportID = split[0];
                InstantViewID = split[1];
            } catch { }

            data.ReportID = ReportID;
            data.InstantViewID = InstantViewID;
            data.MclRequestType = MclRequestType.Get; 

            OrderCreditReport(data);

        }

        private CreditRequestData CreateRequestData(string url, CreditReportProtocol creditProtocol, 
            string accountIdentifier, string userName, string password,
            string borrowerFirstName, string borrowerLastName, string borrowerSsn,
            string coborrowerFirstName, string coborrowerLastName, string coborrowerSsn,
            string presentStreetAddress, string presentCity, string presentState, string presentZipcode) 

        {
            CreditRequestData data = CreditRequestData.CreateForTesting();

            data.RequestingPartyRequestedByName = "David Dao";
            data.Url = url;
            data.CreditProtocol = creditProtocol;
            data.LoginInfo.AccountIdentifier = accountIdentifier;
            data.LoginInfo.UserName = userName;
            data.LoginInfo.Password = password;

            data.IncludeEquifax = true;
            if (creditProtocol == CreditReportProtocol.InfoNetwork) 
            {
                // 2/6/2006 dd - Kassa tell me not to test with Experian server for now.
                data.IncludeExperian = false;
            } 
            else 
            {
                data.IncludeExperian = true;
            }
            data.IncludeTransUnion = true;

            data.Borrower.FirstName = borrowerFirstName;
            data.Borrower.LastName = borrowerLastName;
            data.Borrower.Ssn = borrowerSsn;
            data.Coborrower.FirstName = coborrowerFirstName;
            data.Coborrower.LastName = coborrowerLastName;
            data.Coborrower.Ssn = coborrowerSsn;

            Address address = new Address();
            address.ParseStreetAddress(presentStreetAddress);
            address.City = presentCity;
            address.State = presentState;
            address.Zipcode = presentZipcode;

            data.Borrower.CurrentAddress = address;

            return data;

        }

        /// <summary>
        /// Pull Kroll Factual Data
        /// </summary>
        [LoTest(Description="Kroll Factual Data", Timeout=60 /*seconds*/ )]
        private void Test003() 
        {
            CreditRequestData data = CreateRequestData("", CreditReportProtocol.KrollFactualData,
                "0603INLEND", "foo", ConstStage.CreditTestPasswordForKroll,
                "Sara", "Pratt", "000-00-0029",
                "George", "Pratt", "000-00-0030",
                "1496 WILLOW PARK", "TOONTOWN", "IL", "60144");

            OrderCreditReport(data);

        }
        /// <summary>
        /// Pull LandSafe Credit Report Production Server
        /// </summary>
        [LoTest(Description="LandSafe Credit Report Production Server (maybe timeout)", Timeout=90 /*seconds*/ )]
        private void Test004() 
        {
            CreditRequestData data = CreateRequestData("", CreditReportProtocol.Landsafe,
                "", "LINNCOOK", ConstStage.CreditTestPasswordForLandSafe,
                "Carmen", "ACACOMMON", "066-62-2527",
                "", "", "",
                "2 CENTURY PLZ", "TOMMORROW", "IL", "60750");

            OrderCreditReport(data);

        }
        /// <summary>
        /// Pull LandSafe Credit Report Beta Server
        /// </summary>
        private void Test004a() 
        {
            CreditRequestData data = CreateRequestData("", CreditReportProtocol.Landsafe,
                "", "linncook", "12345guess",
                "Michael", "ABACOMMON", "076-84-9627",
                "", "", "",
                "1 COMPLIANT DRIVE", "TOMMORROW", "IL", "60750");

            OrderCreditReport(data);

        }

        /// <summary>
        /// Pull Credco Credit Report
        /// </summary>
        [LoTest(Description="Pull Credco Credit Report", Timeout=60 /*seconds*/ )]
        private void Test005() 
        {
            //8-24-07 db - Use beta url instead of production because the login account on production is disabled
			/*CreditRequestData data = CreateRequestData("", CreditReportProtocol.Credco,
                "", "3232757_P", "MH7G9Z1P",
                "Ken", "Customer", "500-50-7000",
                "", "", "",
                "10655 BIRCH STREET", "BURBANK", "CA", "91502");

            OrderCreditReport(data);*/

			CreditRequestData data = CreateRequestData("", CreditReportProtocol.Credco,
				"", "3232757", ConstStage.CreditTestPasswordForCredco,
				"Ken", "Customer", "500-50-7000",
				"", "", "",
				"10655 BIRCH STREET", "BURBANK", "CA", "91502");

			OrderCreditReport(data);
           
        }

        /// <summary>
        /// Pull Advantage Credit (FL) Credit Report
        /// </summary>
        private void Test006() 
        {
            string url = "https://axis.adin.net/InsightLendingSolutions/ProcessRequestMISMO21BinaryPOST.asp";

            CreditRequestData data = CreateRequestData(url, CreditReportProtocol.MISMO_2_1,
                "16147", "insightlending", "1n51ght",
                "Jonathon", "Consumer", "123-45-6789",
                "", "", "",
                "10655 BIRCH STREET", "BURBANK", "CA", "91502");

            OrderCreditReport(data);

        }

        /// <summary>
        /// StandFacts Credit Services
        /// </summary>
        private void Test007() 
        {
            CreditRequestData data = CreateRequestData("", CreditReportProtocol.StandFacts,
                "F800ILS", "", "546794!ILS",
                "Guadalupe", "Testcase", "111-11-1111",
                "", "", "",
                "1561 N Fir", "ONTARIO", "CA", "91764");

            OrderCreditReport(data);

        }

        /// <summary>
        /// Universal Credits
        /// </summary>
        [LoTest(Description="Universal Credits", Timeout=60 /*seconds*/ )]
        private void Test008() 
        {
            CreditRequestData data = CreateRequestData("", CreditReportProtocol.UniversalCredit,
                "", "TestPriceMyLoan", ConstStage.CreditTestPasswordForUniversal, 
                "Michael", "Abacommon", "076-84-9627",
                "", "", "",
                "1 Compliant Dr. Apt 14", "Tommorrow", "IL", "60750");

            OrderCreditReport(data);
        }

        /// <summary>
        /// Fiserv / Chase Credit
        /// </summary>
        [LoTest(Description="Fiserv / Chase Credit", Timeout=60 /*seconds*/ )]
        private void Test009() 
        {
            CreditRequestData data = CreateRequestData("", CreditReportProtocol.Fiserv,
                "", "meridian_test", ConstStage.CreditTestPasswordForFiserv, 
                "Wil", "TestCase", "101-01-0001",
                "", "", "",
                "9000 Pillow Bridge Rd", "Glen Arrow", "VA", "23000");

            OrderCreditReport(data);
        }

        /// <summary>
        /// FannieMae credit
        /// </summary>
        [LoTest(Description="FannieMae", Timeout=60 /*seconds*/ )]
        private void Test010() 
        {
            CreditRequestData data = CreateRequestData("", CreditReportProtocol.FannieMae, "", "1234567", ConstStage.CreditTestPasswordForFannieMae,
                "Patrick", "Purchaser", "999-12-1234",
                "Lorraine", "Purchaser", "888-56-5678",
                "1234 Main Street", "Baltimore", "MD", "20600");

            data.LoginInfo.FannieMaeCreditProvider = "200";
            data.LoginInfo.FannieMaeMORNETUserID = ConstAppDavid.FannieMae_TestDoUserId;
            data.LoginInfo.FannieMaeMORNETPassword = ConstAppDavid.FannieMae_TestDoPassword;

            OrderCreditReport(data);

        }

        private void Test011() 
        {
            CreditRequestData data = CreateRequestData("", CreditReportProtocol.InfoNetwork, "99993-0000", "Te8s7t", "Se5rv7",
                "Michelle", "Testfile", "897-01-5432",
                "", "", "",
                "133 Bufford Dr", "Bufford", "GA", "30221");

            OrderCreditReport(data);

        }

        [LoTest(Description="SharperLending", Timeout=60 /*seconds*/ )]
        private void Test012() 
        {
            CreditRequestData data = CreateRequestData("", CreditReportProtocol.SharperLending, "ILS", "test", ConstStage.CreditTestPasswordForSharperLending, 
                "Charles", "DTestfile", "777-77-7777",
                "Annette", "DTEstfile", "677-77-7777",
                "4444 W Main", "LTestcity", "CA", "99000");

            OrderCreditReport(data);

        }

		// Credit Quick Services
        [LoTest(Description="Credit Quick Services", Timeout=60 /*seconds*/ )]
		private void Test013()
		{
			string url = "https://www.creditquickservices.com/losrequest/weblosrequest.aspx?Client=PriceMyLoan";

			CreditRequestData data = CreateRequestData(url, CreditReportProtocol.CreditInterlink, "", "pmlcqs", ConstStage.CreditTestPasswordForCQS, 
				"Betty", "Oldcredit", "707-07-0707", 
				"", "", "", 
				"425 E. McFetridge Drive", "Chicago", "IL", "60605");

			OrderCreditReport(data);
		}

		// Old Republic Credit Services
        [LoTest(Description="Old Republic Credit Services", Timeout=60 /*seconds*/ )]
		private void Test014()
		{
			string url = "https://www.orcredit.com/losrequest/weblosrequest.aspx?Client=PriceMyLoan";

			CreditRequestData data = CreateRequestData(url, CreditReportProtocol.CreditInterlink, "", "pmlorcs", ConstStage.CreditTestPasswordForORCS, 
				"Joe", "Hicredit", "767-67-6767", 
				"", "", "", 
				"777 Red Dog Road", "Green Bay", "WI", "54303");

			OrderCreditReport(data);
		}

		//CSC
        [LoTest(Description="CSC", Timeout=60 /*seconds*/ )]
		private void Test015()
		{
			string url = "https://emsws.equifax.com/emsws/services/post/MergeCreditWWW";

			CreditRequestData data = CreateRequestData(url, CreditReportProtocol.CSC, "999PM08167", "999PM08167", ConstStage.CreditTestPasswordForCSC, 
				"Kitty", "Cat", "400-41-4001", 
				"", "", "", 
				"2525 Litter Lane", "Atlanta", "GA", "30022");

			OrderCreditReport(data);
		}

		//OPM 18327 CSD
        [LoTest(Description="CSD", Timeout=60 /*seconds*/ )]
		private void Test016()
		{
			string url = "https://www.ultraamps.com/uaweb/mismo";

			CreditRequestData data = CreateRequestData(url, CreditReportProtocol.CSD, "", "ils", ConstStage.CreditTestPasswordForCSD, 
				"ANDREW", "DVACOMMON", "181-50-1952", 
				"DJAUDAT", "DVACOMMON", "563-53-9899", 
				"225 COVE AV", "Fantasy Island", "IL", "60750");

			OrderCreditReport(data);
		}

        [LoTest(Description = "CSD", Timeout = 60 /*seconds*/ )]
        private void Test016a()
        {
            string url = "https://www.ultraamps.com/uaweb/mismo";

            CreditRequestData data = CreateRequestData(url, CreditReportProtocol.CSD, "", "ils", ConstStage.CreditTestPasswordForCSD,
                "MICHAEL", "ABACOMMON", "076-84-9627",
                "", "", "",
                "1 COMPLIANT", "Fantasy Island", "IL", "60750");

            OrderCreditReport(data);
        }
		//OPM 19664 CBC
		[LoTest(Description="CBC", Timeout=60 /*seconds*/ )]
		private void Test017()
		{
			string url = "https://www.creditbureaureports.com/servlet/gnbank?logid=lendoff&command=apiordretpost&options=ORD%3DIN+PA%3DXM+TEXT%3DN+PS%3DH+REVL%3DY+REVF%3DX4";

			CreditRequestData data = CreateRequestData(url, CreditReportProtocol.CBC, "", "pr99903b",ConstStage.CreditTestPasswordForCBC, 
				"RONAL", "BOGUS", "005-16-0001", 
				"", "", "", 
				"9806 CAPITALISTIC DR #306", "ALLISON PARK", "PA", "15101");

			OrderCreditReport(data);
		}

        private void OrderCreditReport(CreditRequestData requestData) 
        {
            bool isReady = false;
            int MAX_TRIES = 20;
            int currentCount = 0;
            try 
            {
                while (!isReady) 
                {
                    var requestHandler = new CreditReportRequestHandler(requestData, principal: null);

                    // Tests don't need to go through the asynchronous processor.
                    var result = requestHandler.SubmitSynchronously();
                    if (result.Status == CreditReportRequestResultStatus.Failure)
                    {
                        SetResult("Message", $"<span class=Fail>{string.Join(", ", result.ErrorMessages)}</span>");
                        return;
                    }

                    ICreditReportResponse creditResponse = result.CreditReportResponse;
                    if (null == creditResponse) 
                    {
                        SetResult("Message", "<span class=Fail>CreditResponse is null</span>");
                        return;
                    } 
                    if (creditResponse.HasError) 
                    {
                        SetResult("Message", "<span class=Fail>" +creditResponse.ErrorMessage + "</span>");
                        return;
                    }
                    if (creditResponse.IsReady) 
                    {
                        SetResult("Message", "<span class=Success>OK</span>");
                        return;
                    } 
                    else 
                    {
                        if (requestData.CreditProtocol == CreditReportProtocol.Mcl) 
                        {
                            requestData.ReportID = creditResponse.ReportID;
                            requestData.MclRequestType = MclRequestType.Get; 
                        } 
                        else if (requestData.CreditProtocol == CreditReportProtocol.InfoNetwork) 
                        {
                            requestData.ReportID = creditResponse.ReportID;
                            requestData.RequestActionType = LendersOffice.CreditReport.Mismo.E_CreditReportRequestActionType.StatusQuery;
                        }
                        // Sleep for about 10 seconds before issue another get request.
                        if (currentCount < MAX_TRIES) 
                        {
                            LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10000);
                            currentCount++;
                        } 
                        else 
                        {
                            SetResult("Message", "<span class=Fail>This request has the tendency to become infinite loop.</span>");
                            return;
                        }

                    }
                }
            } 
            catch (Exception exc) 
            {
                SetResult("Message", "<span class=Fail>" + exc.ToString() + "</span>");
                return;
            }

        }

    }
}
