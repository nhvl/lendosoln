<%@ Page language="C#" Codebehind="TestCenter.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Test.TestCenter"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Test Center - LendingQB" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <div class="container"><div class="panel"><div class="panel-body">
        <div ng-app="app">
            <test-center></test-center>
        </div>
    </div></div></div>

    <form runat="server"></form>
    <style>
        .popover {
            max-width: 600px;
        }
    </style>
</asp:Content>
