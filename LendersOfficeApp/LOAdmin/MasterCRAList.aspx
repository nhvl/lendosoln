<%@ Page language="c#" Codebehind="MasterCRAList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.MasterCRAListPage" EnableViewState="false" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="common/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="HeaderNav" Src="../common/HeaderNav.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>MasterCRAList</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <style type="text/css">
        TD, Input {
            font-size: 11px;
            line-height: 16px;
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <LINK href="../css/stylesheet.css" type=text/css rel=stylesheet >
  </HEAD>
  <body MS_POSITIONING="FlowLayout">
    <script type="text/javascript" language="javascript">
        function f_update(i) {
            if (i < 10) {
                i = '0' + i;
            }
            var ComId = document.getElementById("CraList_ctl" + i +"_ComId").value;
            var ComNm = document.getElementById("CraList_ctl" + i + "_ComNm").value;
            var ComUrl = document.getElementById("CraList_ctl" + i + "_ComUrl").value;
            var ProtocolType = document.getElementById("CraList_ctl" + i + "_ProtocolType").value;
            var IsValid = document.getElementById("CraList_ctl" + i + "_IsValid").checked ? "True" : "False";
            var IsInternalOnly = document.getElementById("CraList_ctl" + i + "_IsInternalOnly").checked ? "True" : "False";
            var MclCraCode = document.getElementById("CraList_ctl" + i + "_MclCraCode").value;
            var IsWarningOn = document.getElementById("CraList_ctl" + i + "_IsWarningOn").checked ? "True" : "False";
            var WarningMsg = document.getElementById("CraList_ctl" + i + "_WarningMsg").value;
            var vendorIdDDL = document.getElementById("CraList_ctl" + i + "_VoxVendorId");
            var VoxVendorId = vendorIdDDL.options[vendorIdDDL.selectedIndex].value;

            var args = new Object();
            args["ComId"] = ComId;
            args["ComNm"] = ComNm;
            args["ComUrl"] = ComUrl;
            args["ProtocolType"] = ProtocolType;
            args["IsValid"] = IsValid;
            args["IsInternalOnly"] = IsInternalOnly;
            args["MclCraCode"] = MclCraCode;
            args["IsWarningOn"] = IsWarningOn;
            args["WarningMsg"] = WarningMsg;
            args["VoxVendorId"] = VoxVendorId;
            var result = gService.test.call("Update", args);
        }
        function f_insert() {
            var ComId = document.getElementById("ComId").value;
            var ComNm = document.getElementById("ComNm").value;
            var ComUrl = document.getElementById("ComUrl").value;
            var ProtocolType = document.getElementById("ProtocolType").value;
            var IsValid = document.getElementById("IsValid").checked ? "True" : "False";
            var IsInternalOnly = document.getElementById("IsInternalOnly").checked ? "True" : "False";
            var MclCraCode = document.getElementById("MclCraCode").value;
            var IsWarningOn = document.getElementById("IsWarningOn").checked ? "True" : "False";
            var WarningMsg = document.getElementById("WarningMsg").value;
            var vendorIdDDL = document.getElementById("VoxVendorId");
            var VoxVendorId = vendorIdDDL.options[vendorIdDDL.selectedIndex].value;

            var args = new Object();
            args["ComId"] = ComId;
            args["ComNm"] = ComNm;
            args["ComUrl"] = ComUrl;
            args["ProtocolType"] = ProtocolType;
            args["IsValid"] = IsValid;
            args["IsInternalOnly"] = IsInternalOnly;
            args["MclCraCode"] = MclCraCode;
            args["IsWarningOn"] = IsWarningOn;
            args["WarningMsg"] = WarningMsg;
            args["VoxVendorId"] = VoxVendorId;
            var result = gService.test.call("Insert", args);
            if (!result.error)
                window.location.reload();
        }
        function f_update_ProtocolType() {
            var ProtocolType = document.getElementById("ProtocolType_ProtocolType").value;
            var IsWarningOn = document.getElementById("IsWarningOn_ProtocolType").checked ? "True" : "False";
            var WarningMsg = document.getElementById("WarningMsg_ProtocolType").value;
    
            if (ProtocolType.Length == 0)
                return;
    
            var args = new Object();
            args["ProtocolType"] = ProtocolType;
            args["IsWarningOn"] = IsWarningOn;
            args["WarningMsg"] = WarningMsg;
            var result = gService.test.call("UpdateEntireProtocolType", args);
        }
    </script>

    <form id="MasterCRAList" method="post" runat="server"><uc1:header id=Header1 runat="server"></uc1:header>
                        <uc1:headernav id="HeaderNav1" runat="server">
                            <menuitem label="Main" url="~/LOAdmin/main.aspx"></menuitem>
                            <menuitem label="List of all CRA"></menuitem>
                        </uc1:headernav>      
        <hr />
        Note 1: You have to update each record individually unless you are using the mass warning message feature.
        <br />
        Note 2: You have to refresh this page to see the effect of updates.
        <br />
        Note 3: To enable the Warning checkbox, a Warning Message must be provided as well.
        <br />
        Protocol: Mcl = 0, Mismo_2_1 = 1, Landsafe = 2, Mismo_2_3 = 3, Kroll Factual Data = 4, Credco = 5, StandFacts = 6, UniversalCredit = 7, Fiserv = 8, FannieMae = 9, InfoNetwork = 10, SharperLending = 11, CreditInterlink = 12, CSC = 13, CSD = 14, CBC = 15, FundingSuite = 16, Informative Research = 17, Equifax = 18
        <hr />
        <b>This section will update the warning message information for an entire protocol type, such as FannieMae or MCL.</b>
        <table width="100%" cellpadding="2" border="1">
            <tr>
                <td>Warning</td>
                <td>Warning Message</td>
                <td>Protocol</td>
                <td>&nbsp;</td></tr>
            <tr>
                <td><input type="checkbox" id="IsWarningOn_ProtocolType" />On</td>
                <td><input type="text" style="width:500px" id="WarningMsg_ProtocolType" /></td>
                <td><input style="width:30px" type="text" id="ProtocolType_ProtocolType" /></td>
                <td><input type="button" value="Update Entire Protocol Type" onclick="f_update_ProtocolType();" /></td></tr>
        </table>
        <hr />
        <table width="100%" cellpadding="2" border="1">
            <tr>
                <td>ComId</td>
                <td>Name</td>
                <td>Warning</td>
                <td>Warning Message (Activation Date)</td>
                <td>MclCode</td>
                <td>Url</td>
                <td>Protocol</td>
                <td>Is Valid?</td>
                <td>Is Internal?</td>
                <td>VOX Vendor</td>
                <td>&nbsp;</td>
            </tr>
            <asp:Repeater ID="CraList" runat="server" OnItemDataBound="CraList_ItemDataBound">
                <ItemTemplate>
                    <tr>
                        <td><ml:EncodedLiteral ID="CraComId" runat="server"></ml:EncodedLiteral><asp:HiddenField ID="ComId" runat="server" /></td>
                        <td><asp:TextBox Width="200px" ID="ComNm" runat="server"></asp:TextBox></td>
                        <td><asp:CheckBox ID="IsWarningOn" runat="server" Text="On" /></td>
                        <td><asp:TextBox Width="200px" ID="WarningMsg" runat="server"></asp:TextBox></td>
                        <td><asp:TextBox Width="30px" MaxLength="2" ID="MclCraCode" runat="server"></asp:TextBox></td>
                        <td><asp:TextBox Width="200px" ID="ComUrl" runat="server"></asp:TextBox></td>
                        <td><asp:TextBox Width="30px" ID="ProtocolType" runat="server"></asp:TextBox></td>
                        <td><asp:CheckBox ID="IsValid" runat="server" Text="Valid"></asp:CheckBox></td>
                        <td><asp:CheckBox ID="IsInternalOnly" runat="server" Text="Yes"></asp:CheckBox></td>
                        <td><asp:DropDownList id="VoxVendorId" runat="server"></asp:DropDownList></td>
                        <td><button onclick=<%# AspxTools.HtmlAttribute("f_update(" + Container.ItemIndex + ")") %> >Update</button></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td colspan="10" style="padding-left:5px;">ADD NEW CRA - DON'T FORGET TO REFRESH AFTER INSERT</td></tr>
            <tr>
                <td><asp:TextBox type="text" style="width:215px" id="ComId" runat="server"/></td>
                <td><input style="width:200px" type="text" id="ComNm" /></td>
                <td><input type="checkbox" id="IsWarningOn" />On</td>
                <td><input style="width:200px" type="text" id="WarningMsg" /></td>
                <td><input style="width:30px" type="text" id="MclCraCode" /></td>
                <td><input style="width:200px" type="text" id="ComUrl" /></td>
                <td><input style="width:30px" type="text" id="ProtocolType"></td>
                <td><input type="checkbox" id="IsValid">Valid</td>
                <td><input type="checkbox" id="IsInternalOnly">Yes</td>
                <td><asp:DropDownList ID="VoxVendorId" runat="server"></asp:DropDownList></td>
                <td><input type="button" value="Update" onclick="f_insert();"></td>
            </tr>
        </table>
     </form>
  </body>
</HTML>
