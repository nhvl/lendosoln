﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExecutingEngineTestPage.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.ExecutingEngineTestPage" EnableViewState="false" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="~/LOAdmin/common/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="HeaderNav" Src="~/common/HeaderNav.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Executing Engine Test Page</title>
 <style>
 
 .container { width:960px; margin-left:auto;margin-right:auto;}
 .clear{clear:both;display:block;overflow:hidden;visibility:hidden;width:0;height:0}
 .clearfix:after{clear:both;content:' ';display:block;font-size:0;line-height:0;visibility:hidden;width:0;height:0}
 * html .clearfix,*:first-child+html .clearfix{zoom:1}
 .col_0,.col_1 { display:inline;float:left;}
 .col_0 { width:410px;}
 .col_1 { width:550px;}
 
 .focus_label { font-weight:bold; color:blue}
 span { padding-left:5px}
 .span_value { font-weight:bold}
 .result_allow { color:Green;}
 .result_notallow { color:Red;}
 
 #load_user_panel {margin-left:auto;margin-right:auto; width:680px;padding:3px;}
 pre { margin-left:20px;}
 </style>    
</head>
<body>
 <script type="text/javascript">
 
 var g_boolOptions = [{val:'True',desc:'True'},{val:'False',desc:'False'}];
 
 $(function(){
  $('#ddlBrokerList').on("change", ddlBrokerList_onchange);
  $('#btnCanPerform').on("click", btnCanPerform_onclick);
  $('#btnLoadUserLoan').on("click", btnLoadUserLoan_onclick);
  $('#btnClearUser').on("click", btnClearUser_onclick);
  $('#ddlOperationList').on("change", ddlOperationList_onchange);
  
  $(document).on('focusin', '#parameter_panel input, #parameter_panel select', input_focusin);
  $(document).on('focusout', '#parameter_panel input, #parameter_panel select', input_focusout);  

  
  populateOperationDropDownList(null);
  $('#btnCanPerform').prop('disabled', true);
 });
 
 function input_focusin(event)
 {
	$(this).parent().prev().addClass('focus_label');
 }
 function input_focusout(event)
 {
	$(this).parent().prev().removeClass('focus_label');
 
 }
 function btnLoadUserLoan_onclick(event) 
 {
    var operation = $('#ddlOperationList').val();
    resetResults();
    if (operation != '') 
    {
        var brokerId = $('#ddlBrokerList').val();
        var userName = $('#txtUserName').val();
        var userType = $('#ddlUserType').val();
        var sLNm = $('#sLNm').val();
        var args = { 
            operation: operation, 
            userName: userName, 
            brokerId : brokerId, 
            userType : userType,
            sLNm : sLNm
            };
        
        var result = gService.main.call("LoadUserAndLoan", args);
        if (!result.error)
        {
          var debugResult = $j.parseJSON(result.value.json);
          $('#btnCanPerform').prop('disabled', true);
          $('#tmplDebugResult').tmpl(debugResult).appendTo('#result_debug_list');     
          $('#tmplDebugResultDescription').tmpl({Operation:result.value.operation, Description:result.value.Description, Result:result.value.Result}).appendTo('#main_result_label');   
          $('#user_message_label').text(result.value.UserFriendlyMessage);
        } 
        else
        {
          alert(result.UserMessage);
        }
    }
 }
 function btnClearUser_onclick(event)
 {
    $('#txtUserName,#sLNm').val('');
    ddlOperationList_onchange(null);
 }
 function ddlOperationList_onchange(event)
 {
  var operation = $('#ddlOperationList').val();
    resetResults();
  
  if (operation != '')
  {
  var userName = $('#txtUserName').val();
  
    if (userName == '') {
        var brokerId = $('#ddlBrokerList').val();
        
        var args = { operation:operation, BrokerId:brokerId};
        
        var result = gService.main.call("ListInputByOperation", args);
        if (!result.error)
        {
          var list = $j.parseJSON(result.value.json);
          renderParameterList(list);
          $('#btnCanPerform').prop('disabled', false);
        }
        else
        {
          alert(result.UserMessage);
        }
    } else {
      btnLoadUserLoan_onclick();
    }
  }
 }
 function ddlBrokerList_onchange(event)
 {
    resetResults();
    $('#EngineReleasedDate').text('N/A');
    $('#EngineSize').text('N/A');    
    var brokerId = $(this).val();
    if (brokerId == <%= AspxTools.JsString(Guid.Empty) %>)
    {
      populateOperationDropDownList(null);
    }
    else 
    {
      // Get Operation List.
      var args = { BrokerId : brokerId};
      
      var result = gService.main.call("ListOperationByBroker", args);
      
      if (!result.error)
      {
        var operationList = $j.parseJSON(result.value.json);
        
        var options = [];
        var length = operationList.length;
        for (var i = 0; i < length; i++)
        {
          options.push({desc:operationList[i].value, val:operationList[i].key});
        }
        populateOperationDropDownList(options);
        
        $('#EngineReleasedDate').text(result.value.EngineReleasedDate);
        $('#EngineSize').text(result.value.EngineSize);
      } 
      else 
      {
        alert('Error');
      }
      
    }
 }
 function resetResults()
 {
	  $('#parameter_panel').empty(); 
  	$('#result_debug_list').empty();
  	$('#main_result_label').text('').removeClass();
  	$('#btnCanPerform').prop('disabled', true);
    $('#user_message_label').empty();
 }

  function populateDebugResult(debugResult, resultText)
  {
    var cssResult = resultText == "ALLOW" ? "result_allow" : "result_notallow";
	  $('#main_result_label').text(resultText).removeClass().addClass(cssResult);
	
	  $('#tmplDebugResult').tmpl(debugResult).appendTo('#result_debug_list');
  }

 
 function btnCanPerform_onclick()
 {
  	$('#result_debug_list').empty();
  	$('#main_result_label').text('').removeClass(); 
 
  var brokerId = $('#ddlBrokerList').val();
  var operation = $('#ddlOperationList').val();
  
  var args = { operation : operation, BrokerId : brokerId};
  
  $('#parameter_panel :input').each(function() 
      { 
        var id = $(this).attr('id'); 
        var val =$(this).val();
        args[id] = val;
      });
      
  var result = gService.main.call('CanPerform', args);
  
  if (!result.error)
  {
      var debugResult = $j.parseJSON(result.value.json);
      populateDebugResult(debugResult, result.value.ResultString);  
  } 
  else { alert('Error');}
 }
 function renderParameterList(parameterList) {
	$('#parameter_panel').empty();
	$('#tmplInputParameter').tmpl(parameterList, 
	{
	  indexOf: function() { return $j.inArray(this.data, parameterList);}
	}).appendTo('#parameter_panel');

}

 function populateOperationDropDownList(options)
 {
    var ddl = $('#ddlOperationList');
    ddl.empty();
    options = [{desc:'-- Choose Option --', val:''}].concat(options);
  	$('#tmplSelectOption').tmpl(options).appendTo(ddl);
 }

 function getCssResult(data) {
     if (data.Result == 'ALLOW' || data.Result == 'MATCH' || data.Result == 'True') {
        return 'result_allow';
    } else {
        return 'result_notallow';
    }
 }

 function getCssResultTrigger(result) {
     if (result == 'True') {
         return 'result_allow';
     } else {
         return 'result_notallow';
     }
 }

 function getCssResultRestraints(data) {
     if (data.Result == 'ALLOW' || data.Result == 'FAIL') {
         return 'result_allow';
     } else {
         return 'result_notallow';
     }
 }

 function f_isType(data, t) {
  return data.type === t;
 }

 </script>
 <script id="tmplInputParameter" type="text/x-jquery-tmpl">
 <div class='col_0'>${$item.indexOf()} - ${desc} (${id})</div>
 <div class='col_1'>
 {{if $data.type == 'text'}}
    <input type='text' id='${id}' value='${val}' ${disabled}/>
 {{else $data.type == 'enum'}}
    <select id='${id}' ${disabled}>
      {{tmpl(opts) "#tmplSelectOption"}}
    </select>
 {{else $data.type === 'bool'}}
    <select id='${id}' ${disabled}>
      {{tmpl(g_boolOptions) "#tmplSelectOption"}}    
    </select> 
 {{/if}}
 </div>
 <hr class='clear'/>
 </script>
 <script id="tmplDebugResultDescription" type="text/x-jquery-tmpl">
  <span class='span_value'>${Operation}</span> - ${Description} - <span class='${getCssResult($data)}'>${Result}</span>
 </script>
<script id="tmplSelectOption" type="text/x-jquery-tmpl">
  <option value='${val}' ${selected}>${desc}</option>
</script>
<script id="tmplDebugResult" type="text/x-jquery-tmpl">
<li>Operation: <span class='span_value'>${Operation}</span> <span class='${getCssResult($data)}'>${Result}</span>
  <ul>
  {{if RestraintList}}
  <li> Restraints:
  <ul>
    {{each(i, condition) RestraintList}}  
    <li >Condition #${i}: Id:${ConditionId} <span class='${getCssResultRestraints(condition)}'>${Result}</span>
    {{if ParameterList}}
    <ul>
      {{each ParameterList}}    
      <li><span class='${getCssResultRestraints($value)}'>${Result}</span>
          <span>VarId: </span><span class='span_value'>${VarName}</span>
          <span>Function: </span><span class='span_value'>${Func}</span>
          <span>Expected Value: </span><span class='span_value'>"${ExpectedValue}"</span>
          <span>Actual Value: </span><span class='span_value'>"${ActualValue}"</span>
      </li>
      {{/each}}
    </ul>

    {{/if}}
    </li>
    {{/each}}

    {{each RestraintTriggerList}}  
    <li >Trigger <span class='span_value'>'${TriggerName}'</span> <span class='${getCssResultTrigger(Result)}'>${Result}</span>
  
        {{if ConditionList}}
        <ul>
          {{each(i, condition) ConditionList}}  
          <li >Condition #${i}: Id:${ConditionId} <span class='${getCssResult(condition)}'>${Result}</span>
          {{if ParameterList}}
          <ul>
            {{each ParameterList}}    
            <li><span class='${getCssResult($value)}'>${Result}</span>
                <span>VarId: </span><span class='span_value'>${VarName}</span>
                <span>Function: </span><span class='span_value'>${Func}</span>
                <span>Expected Value: </span><span class='span_value'>"${ExpectedValue}"</span>
                <span>Actual Value: </span><span class='span_value'>"${ActualValue}"</span>
            </li>
            {{/each}}
          </ul>
  
          {{/if}}
          </li>
          {{/each}}
        </ul>
        {{/if}}
    
    </li>
    {{/each}}



  </ul>
  </li>
  {{/if}}

  {{if ConditionList}}
  <li> Privileges:
  <ul>
    {{each(i, condition) ConditionList}}  
    <li >Condition #${i}: Id:${ConditionId} <span class='${getCssResult(condition)}'>${Result}</span>
    {{if ParameterList}}
    <ul>
      {{each ParameterList}}    
      <li><span class='${getCssResult($value)}'>${Result}</span>
          <span>VarId: </span><span class='span_value'>${VarName}</span>
          <span>Function: </span><span class='span_value'>${Func}</span>
          <span>Expected Value: </span><span class='span_value'>"${ExpectedValue}"</span>
          <span>Actual Value: </span><span class='span_value'>"${ActualValue}"</span>
      </li>
      {{/each}}
    </ul>

    {{/if}}
    </li>
    {{/each}}

    {{each ConditionTriggerList}}  
    <li >Trigger <span class='span_value'>'${TriggerName}'</span> <span class='${getCssResultTrigger(Result)}'>${Result}</span>
  
        {{if ConditionList}}
        <ul>
          {{each(i, condition) ConditionList}}  
          <li >Condition #${i}: Id:${ConditionId} <span class='${getCssResult(condition)}'>${Result}</span>
          {{if ParameterList}}
          <ul>
            {{each ParameterList}}    
            <li><span class='${getCssResult($value)}'>${Result}</span>
                <span>VarId: </span><span class='span_value'>${VarName}</span>
                <span>Function: </span><span class='span_value'>${Func}</span>
                <span>Expected Value: </span><span class='span_value'>"${ExpectedValue}"</span>
                <span>Actual Value: </span><span class='span_value'>"${ActualValue}"</span>
            </li>
            {{/each}}
          </ul>
  
          {{/if}}
          </li>
          {{/each}}
        </ul>
        {{/if}}
    
    </li>
    {{/each}}



  </ul>
  </li>
  {{/if}}

  </ul>
</li>
</script>
  <form id="form1" runat="server">
  <uc1:header ID="HeaderControl" runat="server" />
  <uc1:HeaderNav ID="HeaderNavigation" runat="server">
      <menuitem label="Main" url="~/LOAdmin/main.aspx"></menuitem>
      <menuitem label="Executing Engine Test Page"></menuitem>
  </uc1:HeaderNav>
  <div class="container">
    <div class="col_0">Broker</div>
    <div class="col_1"><asp:DropDownList ID="ddlBrokerList" runat="server" /></div>
    <hr class="clear" />
    <div class="col_0">Operation:</div>
    <div class="col_1"><select id="ddlOperationList"></select></div>
    <hr class="clear" />
  </div>
  <div class="container">
    <div id="load_user_panel">
    User Name: <input type="text" id="txtUserName" />
    <select id="ddlUserType">
      <option value="B">B User</option>
      <option value="P">P User</option>
    </select>
    Loan Number: <input type="text" id="sLNm" />
    <input type="button" value="Load User & Loan" id="btnLoadUserLoan" />
    <input type="button" value="Clear User" id="btnClearUser" />
    </div>
  </div>
  <div id="parameter_panel" class="container"></div>
  <div id="button_panel" class="container">
    <input id="btnCanPerform" type="button" value="Can Perform ?">
  </div>
  <div id="engine_info_panel">
    <span>Engine Released Date</span><span id="EngineReleasedDate"></span><span>Engine Size (Bytes):</span><span id="EngineSize"></span>
  </div>
  <div id="result_panel">
    Results: <span id="main_result_label"></span>
    <br/>
    User Friendly Message
    <pre id="user_message_label"></pre>
    <ul id="result_debug_list"></ul>
  </div>
  </form>
</body>
</html>
