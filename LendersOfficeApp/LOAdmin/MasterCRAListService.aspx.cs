using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.LOAdmin
{

	public partial class MasterCRAListService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "Update":
                    Execute("UpdateServiceCompany");
                    break;
                case "Insert":
                    Execute("InsertServiceCompany");
                    break;
				case "UpdateEntireProtocolType":
					ExecuteEntireProtocolType("UpdateEntireProtocolType");
					break;
            }
        }
        private void Execute(string spName) 
        {
            Guid ComId = GetGuid("ComId");
            string ComNm = GetString("ComNm");
            string ComUrl = GetString("ComUrl");
            int ProtocolType = GetInt("ProtocolType");
            bool IsValid = GetBool("IsValid");
            bool IsInternalOnly = GetBool("IsInternalOnly");
            string MclCraCode = GetString("MclCraCode");
			bool IsWarningOn = GetBool("IsWarningOn");
			string WarningMsg = GetString("WarningMsg");

            int? VoxVendorId = GetInt("VoxVendorId", -1);
            VoxVendorId = VoxVendorId == -1 ? null : VoxVendorId;

            SqlParameter[] parameters =
            {
                new SqlParameter("@ComId", ComId),
                new SqlParameter("@ComNm", ComNm),
                new SqlParameter("@ComUrl", ComUrl),
                new SqlParameter("@ProtocolType", ProtocolType),
                new SqlParameter("@IsValid", IsValid),
                new SqlParameter("@IsInternalOnly", IsInternalOnly),
                new SqlParameter("@MclCraCode", MclCraCode),
                new SqlParameter("@IsWarningOn", IsWarningOn),
                new SqlParameter("@WarningMsg", WarningMsg),
                new SqlParameter("@WarningStartD", IsWarningOn ? DateTime.Now : (object) DBNull.Value),
                new SqlParameter("@VoxVendorId", VoxVendorId)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, spName, 0, parameters);
        }

		private void ExecuteEntireProtocolType(string spName)
		{
			int ProtocolType = GetInt("ProtocolType");
			bool IsWarningOn = GetBool("IsWarningOn");
			string WarningMsg = GetString("WarningMsg");

			SqlParameter[] parameters = {
											new SqlParameter("@ProtocolType", ProtocolType),
											new SqlParameter("@IsWarningOn", IsWarningOn),
											new SqlParameter("@WarningMsg", WarningMsg),
											new SqlParameter("@WarningStartD", IsWarningOn ? DateTime.Now : (object) DBNull.Value)
										};

			StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, spName, 0, parameters);
		}
	}
}
