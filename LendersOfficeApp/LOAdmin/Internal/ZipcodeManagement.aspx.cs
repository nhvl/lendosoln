﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Zipcode;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin.Internal
{
    public partial class ZipcodeManagement : LendersOffice.Admin.SecuredAdminPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                if (ZipcodeFile.FileName.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
                {
                    ZipcodeAuditor manager = new ZipcodeAuditor();
                    string path = TempFileUtils.NewTempFilePath() + ".csv";
                    ZipcodeFile.SaveAs(path);

                    ILookup<int, string> errors;
                    manager.ProcessUpdateFile(path, out errors);

                    string reportPath = manager.CompareAndGenerateReport();

                    RequestHelper.SendFileToClient(this.Context, reportPath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ZipcodeReport.xlsx");
                }
            }
        }
    }
}
