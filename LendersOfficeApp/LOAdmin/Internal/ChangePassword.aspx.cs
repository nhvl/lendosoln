namespace LendersOfficeApp.LOAdmin.Internal
{
    using System;
    using System.Threading;
    using System.Web.Services;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOffice.LOAdmin.Internal;

    /// <summary>
    /// OPM 23946 - Ethan
    ///
    /// This will add functionality to LoAdmin for an Internal user to change his/her password.
    /// A user types in the existing password, and the new password twice for validation and if all goes well,
    /// then a new password is saved.
    /// </summary>
    public partial class ChangePassword : LendersOffice.Admin.SecuredAdminPage
    {
        #region Web service: GetUserInfo TryChangePassword
        [WebMethod]
        public static object GetUserInfo()
        {
            var principal = Thread.CurrentPrincipal as InternalUserPrincipal;
            string loginName = string.Empty;

            var user = new InternalUserDB(principal.UserId);
            if (user.Retrieve()) { loginName = user.LoginName; }

            return new {principal.DisplayName, LoginName = loginName, LendersOffice.Constants.ConstApp.PasswordGuidelines};
        }

        [WebMethod]
        public static object TryChangePassword(string currentPassword, string newPassword)
        {
            var principal = Thread.CurrentPrincipal as InternalUserPrincipal;
            var user = new InternalUserDB(principal.UserId);

            if (string.IsNullOrEmpty(currentPassword)) { return new { error = "Current Password is required" }; }
            if (string.IsNullOrEmpty(newPassword)) { return new { error = "Current Password is required" }; }

            // retrieve the user's information
            if (!user.Retrieve()) { return new { error = "Unexpected error" }; }

            // Validate
            {
                if (newPassword.TrimWhitespaceAndBOM().Length < 6) { return new { error = "New password must be at least 6 characters in length." }; }
                var status = user.IsStrongPassword(newPassword);
                switch (status)
                {
                    case InternalStrongPasswordStatus.PasswordMustBeAlphaNumeric:
                        return new { error = "At least one number and one character is required." };
                    case InternalStrongPasswordStatus.PasswordContainsProhibitedWord:
                        return new { error = "New password can not contain login name, first name, or last name of the account." };
                }
                if (status != InternalStrongPasswordStatus.OK) { return new { error = "New password is not strong enough." }; }

                // check if the current password matches the one typed in
                if (!user.IsCurrentPassword(currentPassword)) { return new { error = "Incorrect Password" }; }
            }

            // save the new password into the database
            user.updatePassword(newPassword.TrimWhitespaceAndBOM());
            user.Save();
            return true;
        }
        #endregion

        protected void PageLoad(object sender, System.EventArgs e)
		{
			RegisterResources();
		}

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.Internal.ChangePassword.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

        static bool Prohibited(InternalUserDB user, string password)
        {
            var status = user.IsStrongPassword(password);
            return (status == InternalStrongPasswordStatus.PasswordContainsProhibitedWord);
        }

        static bool NotAlphanumeric(InternalUserDB user, string password)
        {
            var status = user.IsStrongPassword(password);
            return (status == InternalStrongPasswordStatus.PasswordMustBeAlphaNumeric);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
