namespace LendersOfficeApp.LOAdmin.Internal
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Data.Common;
    using System.Web.Services;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOffice.LOAdmin.Internal;

    /// <summary>
    /// Summary description for AdminList.
    /// </summary>
    public partial class AdminList : LendersOffice.Admin.SecuredAdminPage
    {
        #region Service
        [WebMethod]
        public static IEnumerable<AdminData> GetAdmins()
        {
            if (SecuredAdminPageEx.IsMissingPermission(_addminListPermissions)) { return null; }

            var admins = new List<AdminData>();
            {
                var parameters = new[] { new SqlParameter("@ShowInactive", 1) };
                var connectionInfo = DbConnectionInfo.DefaultConnectionInfo;
                using (var reader = StoredProcedureHelper.ExecuteReader(connectionInfo, "List_InternalAdminAccounts", parameters))
                {
                    while (reader.Read())
                    {
                        var admin = new AdminData(reader);
                        admins.Add(admin);
                    }
                }
            }

            return admins;
        }

        // OPM 209735 - Vien Luong
        // OPM 24342 - Ethan
        /// <summary>
        /// Handles the batch editing to force internal users to change their password
        /// </summary>
        /// <param name="userIds">A list of GUID format User ID</param>
        /// <returns>({invalidUserIds: Guid[], failedUserIds:Guid[]})</returns>
        [WebMethod]
        public static object ForcePasswordChange(string[] userIds)
        {
            if (SecuredAdminPageEx.IsMissingPermission(_addminListPermissions)) { return null; }

            var invalidUserIds = new List<string>(userIds.Length);
            var failedUserIds = new List<string>(userIds.Length);

            foreach (var sUserId in userIds)
            {
                Guid userId;
                try
                {
                    userId = new Guid(sUserId);
                }
                catch (FormatException)
                {
                    invalidUserIds.Add(sUserId);
                    continue;
                }

                var user = new InternalUserDB(userId);
                if (user.Retrieve())
                {
                    user.forcePasswordChangeNextLogin();
                    user.Save();
                }
                else
                {
                    failedUserIds.Add(sUserId);
                }
            }

            return new { invalidUserIds, failedUserIds };
        }

        [WebMethod]
        public static AdminData GetAdminDetail(string userId)
        {
            if (SecuredAdminPageEx.IsMissingPermission(_addminListPermissions)) { return null; }

            Guid gUserId;
            try { gUserId = new Guid(userId); }
            catch (FormatException)
            {
                SecuredAdminPageEx.RepondWith400BadRequest("~/common/AppError.aspx?errmsg=Invalid userid");
                return null;
            }

            return AdminData.LoadFromDb(gUserId);
        }

        [WebMethod]
        public static object EditAdmin(AdminData data, string password, bool changePasswordNextLogin)
        {
            // Check against the user's permission set.
            if (SecuredAdminPageEx.IsMissingPermission(_editAdminPermissions)) { return false; }

            #region Validate
            //string userId = data.UserID.ToString(); // TODO:
            //Guid? gUserId = null;
            //{
            //    if (!string.IsNullOrEmpty(userId))
            //    {
            //        try { gUserId = new Guid(userId); }
            //        catch (FormatException)
            //        {
            //            SecuredAdminPageEx.Respone4Error("~/common/AppError.aspx?errmsg=Invalid userid");
            //            return false;
            //        }
            //    }
            //}

            var gUserId = data.UserID;

            var isEditNotCreate = (gUserId.HasValue);
            var user = (isEditNotCreate) ? new InternalUserDB(gUserId.Value) : new InternalUserDB();
            var userPermissions = new InternalUserPermissions();
            if (isEditNotCreate && user.Retrieve())
            {
                userPermissions.Opts = user.Permissions;
            }

            password = password.TrimWhitespaceAndBOM();

            if (!isEditNotCreate && string.IsNullOrEmpty(password)) { return new { password = "This field is required." }; }
            if (!string.IsNullOrEmpty(password))
            {
                if (user.IsStrongPassword(password) != InternalStrongPasswordStatus.OK) { return new {password="Password is not strong enough."}; }
            }

            if (string.IsNullOrEmpty(data.LoginNm)) { return new { LoginNm = "This field is required." }; }
            if (!Validator_LoginNm_Unique(data)) { return new { LoginNm = "Login name already exists." }; }

            if (string.IsNullOrEmpty(data.FirstName)) { return new { FirstName = "This field is required." }; }
            if (string.IsNullOrEmpty(data.LastName)) { return new { LastName = "This field is required." }; }

            if (string.IsNullOrEmpty(data.FirstName)) { return new { FirstName = "This field is required." }; }
            #endregion

            #region Bind
            data.BindTo(user);

            if (!string.IsNullOrEmpty(password))
            {
                user.updatePassword(password);
            }

            // OPM 24342 - Ethan
            if (changePasswordNextLogin)
            {
                user.forcePasswordChangeNextLogin();
            }
            #endregion

            return user.Save();
        }
        #endregion

        #region Permission
        static readonly E_InternalUserPermissions[] _addminListPermissions = { E_InternalUserPermissions.IsSystemAdmin };
        internal static readonly E_InternalUserPermissions[] _editAdminPermissions = _addminListPermissions; // also used in EditAdmin.aspx

        /// <summary>
        /// Return this page's required permissions set.
        /// This test occurs with each page initialization.
        /// </summary>
        /// <returns>
        /// Array of required permissions.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.
            return _editAdminPermissions;
        }
        #endregion

        #region Validation
        // OPM 209735 - Vien Luong
        // OPM 24226 - Ethan
        /// <summary>
        /// server validation for password strength
        /// in this case, containing no prohibited words: no login name, first name, or last name allowed
        /// </summary>
        // <param name="user">Retrieved User (user.Retrieve())</param>
        public static bool validatePassword(string password, AdminData d, InternalUserDB user, bool isEditNotCreate)
        {
            password = password.TrimWhitespaceAndBOM();

            if ((isEditNotCreate) ||
                (!string.IsNullOrEmpty(d.LoginNm) && !string.IsNullOrEmpty(d.FirstName) && !string.IsNullOrEmpty(d.LastName))
                )
            {
                var status = user.IsStrongPassword(password);

                // m_PasswordContainsProhibitedWord_ServerValidate
                if (status == InternalStrongPasswordStatus.PasswordContainsProhibitedWord)
                {
                    return false;
                }

                // m_NumericAndCharacterRequired_ServerValidate
                if (status == InternalStrongPasswordStatus.PasswordMustBeAlphaNumeric)
                {
                    return false;
                }

                // m_PasswordLengthRequired_ServerValidate
                if (password.Length < 6)
                {
                    return false;
                }

                return status == InternalStrongPasswordStatus.OK;
            }

            return true;
        }

        public static bool Validator_LoginNm_Unique(AdminData d)
        {
            return DataAccess.Tools.IsLoginUnique(d.LoginNm, d.UserID ?? Guid.Empty);
        }
        #endregion

        /// <summary>
        /// Initialize this page.
        /// </summary>
		protected void PageLoad(object sender, System.EventArgs e)
		{
            RegisterResources();
		}

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.Internal.AdminList.js");
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
    }

    public class AdminData
    {
        #region Properties
        public string LoginNm     { get; set; }
        public Guid?  UserID      { get; set; }
        public string AdminNm     { get; set; } // FirstNm + ' ' + LastNm
        public bool   IsActive    { get; set; }

        public string FirstName   { get; set; }
        public string LastName    { get; set; }
        public string Permissions { get; set; } // TODO: hide

        public bool IsSystemAdmin        { get; set; }
        public bool IsDevelopment        { get; set; }
        public bool ViewBrokers          { get; set; }
        public bool EditBroker           { get; set; }
        public bool CreateBroker         { get; set; }
        public bool ViewEmployees        { get; set; }
        public bool EditEmployee         { get; set; }
        public bool BecomeUser           { get; set; }
        public bool ViewActivity         { get; set; }
        public bool ViewInvoices         { get; set; }
        public bool GenerateInvoices     { get; set; }
        public bool ViewLenders          { get; set; }
        public bool IsSAE                { get; set; }
        public bool UpdateStageConstants { get; set; }
        public bool HasBillingAccess     { get; set; }
        public bool EditIntegrationSetup { get; set; }
        public bool FeatureAdoption  { get; set; }
        public bool DownloadReplicationWhitelist { get; set; }
        public bool UploadReplicationWhitelist { get; set; }
        public bool ExportBroker { get; set; }
        public bool ImportBroker { get; set; }
        #endregion

        public AdminData()
        {
            // For serialization
        }

        public AdminData(DbDataReader reader)
        {
            this.LoginNm = reader["LoginNm"] as string;
            this.UserID = (Guid) reader["UserID"];
            this.AdminNm = reader["AdminNm"] as string;
            this.IsActive = (bool) reader["IsActive"];
        }

        protected AdminData(InternalUserDB user)
        {
            this.UserID      = user.UserId;

            this.FirstName   = user.FirstName;
            this.LastName    = user.LastName;
            this.LoginNm     = user.LoginName;
            this.Permissions = user.Permissions;
            this.IsActive    = user.IsActive;

            var userPermissions = new InternalUserPermissions {Opts = user.Permissions};

            this.IsSystemAdmin        = userPermissions.Can( E_InternalUserPermissions.IsSystemAdmin        );
            this.IsDevelopment        = userPermissions.Can( E_InternalUserPermissions.IsDevelopment        );
            this.ViewBrokers          = userPermissions.Can( E_InternalUserPermissions.ViewBrokers          );
            this.EditBroker           = userPermissions.Can( E_InternalUserPermissions.EditBroker           );
            this.CreateBroker         = userPermissions.Can( E_InternalUserPermissions.CreateBroker         );
            this.ViewEmployees        = userPermissions.Can( E_InternalUserPermissions.ViewEmployees        );
            this.EditEmployee         = userPermissions.Can( E_InternalUserPermissions.EditEmployee         );
            this.BecomeUser           = userPermissions.Can( E_InternalUserPermissions.BecomeUser           );
            this.ViewActivity         = userPermissions.Can( E_InternalUserPermissions.ViewActivity         );
            this.ViewInvoices         = userPermissions.Can( E_InternalUserPermissions.ViewInvoices         );
            this.GenerateInvoices     = userPermissions.Can( E_InternalUserPermissions.GenerateInvoices     );
            this.ViewLenders          = userPermissions.Can( E_InternalUserPermissions.ViewLenders          );
			this.IsSAE                = userPermissions.Can( E_InternalUserPermissions.IsSAE                );
			this.UpdateStageConstants = userPermissions.Can( E_InternalUserPermissions.UpdateStageConstants );
            this.HasBillingAccess     = userPermissions.Can( E_InternalUserPermissions.HasBillingAccess     );
            this.EditIntegrationSetup = userPermissions.Can( E_InternalUserPermissions.EditIntegrationSetup );
            this.FeatureAdoption  = userPermissions.Can( E_InternalUserPermissions.FeatureAdoption  );
            this.DownloadReplicationWhitelist = userPermissions.Can(E_InternalUserPermissions.DownloadReplicationWhitelist);
            this.UploadReplicationWhitelist = userPermissions.Can(E_InternalUserPermissions.UploadReplicationWhitelist);
            this.ExportBroker = userPermissions.Can(E_InternalUserPermissions.ExportBroker);
            this.ImportBroker = userPermissions.Can(E_InternalUserPermissions.ImportBroker);
        }

        public static AdminData LoadFromDb(Guid m_userID)
        {
            var user = new InternalUserDB(m_userID);
            return (user.Retrieve() ? new AdminData(user) : null);
        }

        public void BindTo(InternalUserDB user)
        {
            this.FirstName = this.FirstName.TrimWhitespaceAndBOM();
            this.LastName  = this.LastName .TrimWhitespaceAndBOM();
            this.LoginNm   = this.LoginNm  .TrimWhitespaceAndBOM();

            user.FirstName = this.FirstName;
            user.LastName  = this.LastName ;
            user.LoginName = this.LoginNm  ;
            user.IsActive  = this.IsActive ;

            var userPermissions = new InternalUserPermissions();
            {
                userPermissions.Set(E_InternalUserPermissions.IsSystemAdmin       , this.IsSystemAdmin       );
                userPermissions.Set(E_InternalUserPermissions.IsDevelopment       , this.IsDevelopment       );
                userPermissions.Set(E_InternalUserPermissions.ViewBrokers         , this.ViewBrokers         );
                userPermissions.Set(E_InternalUserPermissions.EditBroker          , this.EditBroker          );
                userPermissions.Set(E_InternalUserPermissions.CreateBroker        , this.CreateBroker        );
                userPermissions.Set(E_InternalUserPermissions.ViewEmployees       , this.ViewEmployees       );
                userPermissions.Set(E_InternalUserPermissions.EditEmployee        , this.EditEmployee        );
                userPermissions.Set(E_InternalUserPermissions.BecomeUser          , this.BecomeUser          );
                userPermissions.Set(E_InternalUserPermissions.ViewActivity        , this.ViewActivity        );
                userPermissions.Set(E_InternalUserPermissions.ViewInvoices        , this.ViewInvoices        );
                userPermissions.Set(E_InternalUserPermissions.GenerateInvoices    , this.GenerateInvoices    );
                userPermissions.Set(E_InternalUserPermissions.ViewLenders         , this.ViewLenders         );
			    userPermissions.Set(E_InternalUserPermissions.IsSAE               , this.IsSAE               );
			    userPermissions.Set(E_InternalUserPermissions.UpdateStageConstants, this.UpdateStageConstants);
                userPermissions.Set(E_InternalUserPermissions.HasBillingAccess    , this.HasBillingAccess    );
                userPermissions.Set(E_InternalUserPermissions.EditIntegrationSetup, this.EditIntegrationSetup);
                userPermissions.Set(E_InternalUserPermissions.FeatureAdoption , this.FeatureAdoption );
                userPermissions.Set(E_InternalUserPermissions.DownloadReplicationWhitelist, this.DownloadReplicationWhitelist);
                userPermissions.Set(E_InternalUserPermissions.UploadReplicationWhitelist, this.UploadReplicationWhitelist);
                userPermissions.Set(E_InternalUserPermissions.ExportBroker, this.ExportBroker);
                userPermissions.Set(E_InternalUserPermissions.ImportBroker, this.ImportBroker);
            }
            user.Permissions = userPermissions.Opts.TrimWhitespaceAndBOM();
        }
    }
}
