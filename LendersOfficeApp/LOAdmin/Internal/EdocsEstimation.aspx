﻿<%@ Page language="C#" Codebehind="EdocsEstimation.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Internal.EdocsEstimation"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Recently Underestimated EDocs" %>
<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <style>
        .center-checkbox{
            text-align: center; /* center checkbox horizontally */
            vertical-align: middle; /* center checkbox vertically */
        }
    </style>
    <div class="container-fluid"><div class="row"><div class="panel panel-default"><div class="panel-body">
         <div class="ng-cloak" ng-app="app" ng-controller="EdocsManager as em">
            <div ng-hide="!isLoading">loading&hellip;</div>
            <div ng-show="!isLoading">
                <table class="table table-striped table-condensed table-layout-fix">
                    <thead>
                        <th><a sort-handle="em.entrySort" key="brokerNm">Broker Name</a></th>
                        <th><a sort-handle="em.entrySort" key="DocumentId">Document ID</a></th>
                        <th><a sort-handle="em.entrySort" key="description">Description</a></th>
                        <th><a sort-handle="em.entrySort" key="sLid">sLid</a></th>
                        <th><a sort-handle="em.entrySort" key="CreatedDateSort">Created Date</a></th>
                        <th><a sort-handle="em.entrySort" key="TimeImageCreationEnqueuedSort">Time Image Creation Enqueued</a></th>
                        <th><a sort-handle="em.entrySort" key="TimeImageCreationStartedSort">Time Image Creation Started</a></th>
                        <th><a sort-handle="em.entrySort" key="TimeImageCreationCompletedSort">Time Image Creation Completed</a></th>
                        <th><a sort-handle="em.entrySort" key="IsUploadedByPmlUser">Is Uploaded By Pml User</a></th>
                        <th><a sort-handle="em.entrySort" key="NumPages">Num Pages</a></th>
                        <th><a sort-handle="em.entrySort" key="DisplayedEstimatedWaitTimeInSeconds">Displayed Estimated Wait Time In Seconds</a></th>
                        <th><a sort-handle="em.entrySort" key="ActualProcessingTimeFromEnqueuingInSeconds">Actual Processing Time From Enqueuing In Seconds</a></th>
                    </thead>
                    <tbody>
                         <tr ng-repeat="e in em.Edocss | orderBy:em.entrySort.by:em.entrySort.desc track by $index">
                             <td ng-bind="e.brokerNm"></td>
                             <td ng-bind="e.DocumentId"></td>
                             <td ng-bind="e.description"></td>
                             <td ng-bind="e.sLid"></td>
                             <td ng-bind="e.CreatedDate"></td>
                             <td ng-bind="e.TimeImageCreationEnqueued"></td>
                             <td ng-bind="e.TimeImageCreationStarted"></td>
                             <td ng-bind="e.TimeImageCreationCompleted"></td>
                             <td class="center-checkbox">
                                <input type="checkbox" ng-checked="e.IsUploadedByPmlUser" disabled>
                             </td>
                             <td ng-bind="e.NumPages"></td>
                             <td ng-bind="e.DisplayedEstimatedWaitTimeInSeconds"></td>
                             <td ng-bind="e.ActualProcessingTimeFromEnqueuingInSeconds"></td>
                        </tr>
                    </tbody>
                    </table>
                </div>

    </div></div></div></div></div>
</asp:Content>