using System;
using System.Collections.Generic;
using System.Web.Services;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;

namespace LendersOfficeApp.LOAdmin.Internal
{
	/// <summary>
    /// Provides the UI for managing the auto-escalation of backlogged queues.
	/// </summary>
    public partial class QueueBacklogAutoEscalation : SecuredAdminPage
    {
        #region Service
        /// <summary>
        /// Called by QueueBacklogAutoEscalation.js to populate the table for
        /// QueueBacklogAutoEscalation.html.
        /// </summary>
        /// <returns>A list of all of the queues that have threshold monitoring.</returns>
        [WebMethod]
        public static IEnumerable<QueueMonitoringData> RetrieveAll()
        {
            // Check against the user's permission set.
            if (SecuredAdminPageEx.IsMissingPermission(QueueBacklogPermissions)) { return null; }

            return Tools.LogAndThrowIfErrors<IEnumerable<QueueMonitoringData>>(() =>
                QueueMonitoringData.LoadAll());
        }

        /// <summary>
        /// Called by QueueBacklogAutoEscalation.js when the user clicks on the "edit" link for
        /// a specific queue monitoring record. The <see cref="QueueMonitoringData"/> object returned
        /// will be used to populate the modal popup.
        /// </summary>
        /// <param name="id">The id of the queue to return.</param>
        /// <returns>The <see cref="QueueMonitoringData"/> with the specified <paramref name="id"/>.</returns>
        [WebMethod]
        public static QueueMonitoringData GetQueueMonitoringData(int id)
        {
            // Check against the user's permission set.
            if (SecuredAdminPageEx.IsMissingPermission(QueueBacklogPermissions)) { return null; }

            return Tools.LogAndThrowIfErrors<QueueMonitoringData>(() =>
                QueueMonitoringData.GetQueue(id));
        }

        /// <summary>
        /// Called by QueueBacklogAutoEscalation.js when the user clicks the  "Save" button on the
        /// "Add New" modal popup. Once the new information has been verified (all of the required
        /// fields for the new monitoring have been specified), the data is saved  in the
        /// QUEUE_MONITOR_THRESHOLD table and the queue specified will now have monitoring.
        /// </summary>
        /// <param name="data">The <see cref="QueueMonitoringData"/> object to add.</param>
        /// <returns>True if the addition was successful, false otherwise.</returns>
        [WebMethod]
        public static bool AddNewQueueMonitoringData(QueueMonitoringData data)
        {
            // Check against the user's permission set.
            if (SecuredAdminPageEx.IsMissingPermission(QueueBacklogPermissions)) { return false; }

            if (data.IsMissingFields()) { return false; }

            return Tools.LogAndThrowIfErrors<bool>(() =>
                QueueMonitoringData.AddNewMonitoring(data));
        }

        /// <summary>
        /// Called by QueueBacklogAutoEscalation.js when the user clicks the "Save" button on the "Edit"
        /// modal popup. Once the modified information has been verified (all of the required fields for
        /// the new monitoring have been specified), the data is saved in the QUEUE_MONITOR_THRESHOLD
        /// table.
        /// </summary>
        /// <param name="data">The <see cref="QueueMonitoringData"/> object to edit.</param>
        /// <returns>True if the modification was successful, false otherwise.</returns>
        [WebMethod]
        public static bool ModifyExistingQueueMonitoringData(QueueMonitoringData data)
        {
            // Check against the user's permission set.
            if (SecuredAdminPageEx.IsMissingPermission(QueueBacklogPermissions)) { return false; }

            if (data.IsMissingFields()) { return false; }

            return Tools.LogAndThrowIfErrors<bool>(() =>
                QueueMonitoringData.ModifyExistingMonitoring(data));
        }

        /// <summary>
        /// Called by QueueBacklogAutoEscalation.js when the user clicks the "delete" link on
        /// QueueBacklogAutoEscalation.html for the monitoring of a specific queue.
        /// </summary>
        /// <param name="id">The ID of the <see cref="QueueMonitoringData"/> object to delete.</param>
        /// <returns>True if the deletion was successful, false otherwise.</returns>
        [WebMethod]
        public static bool RemoveQueueFromMonitoring(int id)
        {
            // Check against the user's permission set.
            if (SecuredAdminPageEx.IsMissingPermission(QueueBacklogPermissions)) { return false; }

            return Tools.LogAndThrowIfErrors<bool>(() =>
                QueueMonitoringData.RemoveMonitoring(id));
        }
        #endregion

        #region Permission
        private static readonly E_InternalUserPermissions[] QueueBacklogPermissions = { E_InternalUserPermissions.IsSystemAdmin };

        /// <summary>
        /// Return this page's required permissions set.
        /// This test occurs with each page initialization.
        /// </summary>
        /// <returns>
        /// Array of required permissions for this page.
        /// </returns>
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            // Return this page's required permissions set.
            return QueueBacklogPermissions;
        }
        #endregion

        /// <summary>
        /// Initialize this page.
        /// </summary>
		protected void PageLoad(object sender, System.EventArgs e)
		{
            RegisterResources();
		}

        void RegisterResources()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.Internal.QueueBacklogAutoEscalation.js");
        }

        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            // Prevent loading 'common.js' from 'BasePage'
            m_loadLOdefaultScripts = false;
            m_loadDefaultStylesheet = false;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
    }
}
