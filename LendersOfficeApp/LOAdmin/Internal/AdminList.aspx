<%@ Page language="C#" Codebehind="AdminList.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.LOAdmin.Internal.AdminList"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Admin List - LendingQB" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <div id="container" class="container clearfix">loading&hellip;</div>

    <div id="guidelineModal" class="modal v-middle fade"><div class="modal-dialog"><div class="modal-content">
        <div class="modal-body"><%=AspxTools.HtmlString(LendersOffice.Constants.ConstApp.PasswordGuidelines)%></div>
        <div class="modal-footer">
            <span class="clipper"><a class="btn btn-default" data-dismiss="modal">Close</a></span>
        </div>
    </div></div></div>
</asp:Content>
