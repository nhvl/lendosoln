using DataAccess;
using LendersOffice.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.Services;

namespace LendersOfficeApp.LOAdmin.Internal
{
	/// <summary>
	/// Constant object of stage config admin.
	/// </summary>
    public class StageItem
    {
        /// <summary>
        /// Gets of sets interger value of constant.
        /// </summary>
        public int Integer { get; set; }

        /// <summary>
        /// Gets of sets Key value of constant.
        /// </summary>
        public string Key{ get; set; }

        /// <summary>
        /// Gets of sets Desc of constant.
        /// </summary>
        public string Desc{ get; set; }

        /// <summary>
        /// Gets of sets Owner of constant.
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// Gets of sets interger CreatedDate of constant.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Initializes a new instance of the StageItem class.
        /// </summary>
        /// <param name="inKey">Key value.</param>
        /// <param name="inInteger">Integer value.</param>
        /// <param name="indesc">Description  value.</param>
        /// <param name="inowner">Owner  value.</param>
        /// <param name="inDate">Created date  value.</param>
        public StageItem(string inKey, int inInteger, string indesc, string inowner, DateTime inDate)
        {
            Integer = inInteger;
            Key = inKey;
            Desc = indesc;
            Owner = inowner;
            CreatedDate = inDate;
        }
    }
   
	public partial class StageConfigAdmin : LendersOffice.Admin.SecuredAdminPage
	{
        private static readonly E_InternalUserPermissions[] RequiredPermissions = { E_InternalUserPermissions.ViewBrokers };

		protected void PageLoad(object sender, System.EventArgs e)
		{
			RequirePermission( E_InternalUserPermissions.UpdateStageConstants );
		    RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("LOAdmin.Internal.StageConfigAdmin.js");
            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
		}

        public override string StyleSheet
        {
            get { return string.Empty; }
        }

        #region WebMethod 

        /// <summary>
        /// Return all currents constants in database.
        /// </summary>
        /// <returns>Return array of stage constants is success. Otherwise return error string.</returns>
        [WebMethod]
        public static object GetConstants()
        {
            try
            {
                DataSet dS = new DataSet();
                var stageConstants = new List<StageItem>();

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShare, "ListStageConstants"))
                {
                    while (reader.Read())
                    {
                        int integer;
                        string key, owner, desc;
                        DateTime createdDate;

                        key = Convert.ToString(reader["KeyIdStr"]);
                        if (string.IsNullOrEmpty(key))
                        {
                            continue;
                        }

                        try
                        {
                            integer = Convert.ToInt32(reader["OptionContentInt"]);
                        }
                        catch (InvalidCastException)
                        {
                            integer = 0;
                        }

                        desc = Convert.ToString(reader["OptionContentStr"]);
                        owner = Convert.ToString(reader["Owner"]);
                        try
                        {
                            createdDate = Convert.ToDateTime(reader["CreatedDate"]);
                        }
                        catch (FormatException)
                        {
                            createdDate = DateTime.MinValue;
                        }

                        stageConstants.Add(new StageItem(key, integer, desc, owner, createdDate));
                    }
                }

                return stageConstants.ToArray();
            }
            catch (SqlException exc)
            {
                Tools.LogError(exc);
                return exc.Message;
            }
        }

        /// <summary>
        /// Add constant to database.
        /// </summary>
        /// <param name="constantKey">Key of constant. This value must be unique on database.</param>
        /// <param name="newString">Desc of constant.</param>
        /// <param name="newInt">Integer of constant.</param>
        /// <param name="newOwner">Owner of constant.</param>
        /// <returns>Null if success, otherwist return error string.</returns>
        [WebMethod]
        public static string AddConstant(string constantKey, string newString, int newInt, string newOwner)
        {
            if (SecuredAdminPageEx.IsMissingPermission(RequiredPermissions)) 
            { 
                return "You don't have permissoin to add new constant."; 
            }
            
            SqlParameter[] parameters = {
                                                    new SqlParameter( "@KeyIdStr", constantKey),
                                                    new SqlParameter( "@OptionContentStr",newString ),
                                                    new SqlParameter( "@OptionContentInt", newInt ),
                                                    new SqlParameter( "@Owner", newOwner  ),
                                                    new SqlParameter( "@CreatedDate", DateTime.Now )
                                                };
            
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "AddNewStageConstant", 3, parameters);
            return null;
        }

        /// <summary>
        /// Edit an existed stage constant.
        /// </summary>
        /// <param name="constantKey">Key of constant. This value must be unique on database.</param>
        /// <param name="newString">Desc of constant.</param>
        /// <param name="newInt">Integer of constant.</param>
        /// <param name="newOwner">Owner of constant.</param>
        /// <returns>Null if success, otherwist return error string.</returns>
        [WebMethod]
        public static object EditConstant(string constantKey, string newString, int newInt, string newOwner )
        {
            if (newString.Length > 1500)
            {
                return ("max length of 1500 for key string of \"" + constantKey + "\"");
            }

            if (newOwner.Length > 100)
            {
                //m_responsetext.text = "<span style='color: red; font-weight: bold;'>max length of 100 exceeded for owner of \"" + constantname + "\"</span>";
                return  ("max length of 100 for owner of \"" + constantKey + "\"");
            }

            SqlParameter[] parameters = {
                                                new SqlParameter( "@keyidstr", constantKey ),
                                                new SqlParameter( "@optioncontentstr", newString ),
                                                new SqlParameter( "@optioncontentint", newInt ),
                                                new SqlParameter( "@owner", newOwner )
                                            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "updatestageconstant", 0, parameters);
            return null;
        }

        /// <summary>
        /// Delete an existed constant.
        /// </summary>
        /// <param name="constantKey">Key of constant.</param>
        /// <returns>Null if success, otherwist return error string.</returns>
        [WebMethod]
        public static bool DeleteConstant(string constantKey)
        {
            if (string.IsNullOrWhiteSpace(constantKey))
            {
                return false;
            }

            SqlParameter[] parameters = { new SqlParameter("@KeyIdStr", constantKey) };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DeleteStageConstant", 0, parameters);
            return true;
        }

        #endregion

        
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
   
}
