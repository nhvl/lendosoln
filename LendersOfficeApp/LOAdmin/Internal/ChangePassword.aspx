<%@ Page language="C#" Codebehind="ChangePassword.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.LOAdmin.Internal.ChangePassword"
    MasterPageFile="~/LOAdmin/loadmin.Master" Title="Change Password - LendingQB" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ContentPlaceHolderID="Main" Runat="Server">
    <div id="container">
        <div class="container"><div class="alert alert-info" role="alert">Loading&hellip;</div></div>
    </div>

    <div id="passwordRulesModal" class="modal v-middle fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <form method="post" runat="server"></form>
</asp:Content>
