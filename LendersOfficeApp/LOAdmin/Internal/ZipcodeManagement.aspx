﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ZipcodeManagement.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.Internal.ZipcodeManagement" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/Common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="../Common/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Zipcode Report</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
      <UC:Header id="m_Header" runat="server">
    </UC:Header>
    <UC:HeaderNav id="m_Navigate" runat="server">
        <MenuItem URL="~/LOAdmin/Main.aspx" Label="Main">
        </MenuItem>
        <MenuItem URL="~/LOAdmin/Internal/ZipcodeManagement.aspx" Label="Zipcode Report">
        </MenuItem>
    </UC:HeaderNav>
    
        Please select the new csv zipcode database file. The file should contain 6 colums (first row is ignored), Zipcode, CityMixedCase, County, State, StateFIPS, and CountyFIPS.
        <br />
        <asp:FileUpload runat="server" ID="ZipcodeFile" />
        <br />
        <asp:Button runat="server" Text="Submit CSV and Generate Diff" />
    </div>
    </form>
</body>
</html>
