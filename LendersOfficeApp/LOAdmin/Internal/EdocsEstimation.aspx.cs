﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using System.Web.Services;
using System.Collections.Generic;

namespace LendersOfficeApp.LOAdmin.Internal
{
    public partial class EdocsEstimation : LendersOffice.Admin.SecuredAdminPage
    {
        //Rebuild by Long Nguyen
        //Email: longn@meridianlink.net
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            m_loadLOdefaultScripts = false; // for stop loading `common.js` from `BasePage`
            m_loadDefaultStylesheet = false;
        }
        void RegisterResources()
        {
            RegisterJsScript("LOAdmin.Internal.EdocsEstimation.js");
        }
        [WebMethod]
        public static IEnumerable<EdocsConfig> Get()
        {
            var rs = new List<EdocsConfig>();
             int secondPerPage = ConstStage.EDocImageGeneratingMillisecondPerPageEstimate / 1000;
            int secondWait = ConstStage.EDocImageGeneratingQueueWaitingMillisecondAvgEstimate / 1000;

            SqlParameter[] parameters = {
                                            new SqlParameter("@SecondPerPage", secondPerPage),
                                            new SqlParameter("@SecondWait", secondWait)
                                        };

            // 10/16/2014 dd - TODO: Right now it is only display the edocs underestimate for broker on the default DB.
            // May want to add information about other db.
            DbConnectionInfo connInfo = DbConnectionInfo.DefaultConnectionInfo;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "EDOCS_GetDocsWithIncorrectWaitingTimeForImageConversion", parameters))
            {
                while (reader.Read())
                {
                    rs.Add(new EdocsConfig(reader));
                }
            }
            return rs;
        }
        public class EdocsConfig
        {
            public string brokerNm { get; set; }
            public string DocumentId { get; set; }
            public string description{get;set;}
            public string sLid{get;set;}
            public long? CreatedDateSort { get; set; }//Use long to sort
            public long? TimeImageCreationEnqueuedSort { get; set; }
            public long? TimeImageCreationStartedSort { get; set; }
            public long? TimeImageCreationCompletedSort { get; set; }
            
            public string CreatedDate { get; set; }//Use long to sort
            public string TimeImageCreationEnqueued { get; set; }
            public string TimeImageCreationStarted { get; set; }
            public string TimeImageCreationCompleted { get; set; }

            public bool IsUploadedByPmlUser{get;set;}
            public int? NumPages { get; set; }
            public int? DisplayedEstimatedWaitTimeInSeconds { get; set; }
            public int? ActualProcessingTimeFromEnqueuingInSeconds { get; set; }
            public EdocsConfig(DbDataReader reader)
            {
                brokerNm = reader["brokerNm"].ToString();
                DocumentId = reader["DocumentId"].ToString();
                description = reader["description"].ToString();
                sLid = reader["sLid"].ToString();
                CreatedDate = reader["CreatedDate"].ToString();
                if (reader["CreatedDate"] == DBNull.Value)
                {
                    CreatedDateSort = null;
                }
                else
                {
                    CreatedDateSort = DateTime.Parse(reader["CreatedDate"].ToString()).Ticks;
                }

                TimeImageCreationEnqueued = reader["TimeImageCreationEnqueued"].ToString();
                if (reader["TimeImageCreationEnqueued"] == DBNull.Value)
                {
                    TimeImageCreationEnqueuedSort = null;
                }
                else
                {
                    TimeImageCreationEnqueuedSort = DateTime.Parse(reader["TimeImageCreationEnqueued"].ToString()).Ticks;
                }

                TimeImageCreationStarted = reader["TimeImageCreationStarted"].ToString();
                if (reader["TimeImageCreationStarted"] == DBNull.Value)
                {
                    TimeImageCreationStartedSort = null;
                }
                else
                {
                    TimeImageCreationStartedSort = DateTime.Parse(reader["TimeImageCreationStarted"].ToString()).Ticks;
                }

                TimeImageCreationCompleted = reader["TimeImageCreationCompleted"].ToString();
                if (reader["TimeImageCreationCompleted"] == DBNull.Value)
                {
                    TimeImageCreationCompletedSort = null;
                }
                else
                {
                    TimeImageCreationCompletedSort = DateTime.Parse(reader["TimeImageCreationCompleted"].ToString()).Ticks;
                }

                if (reader["IsUploadedByPmlUser"] == DBNull.Value)
                {
                    IsUploadedByPmlUser = false;
                }
                else
                {
                    IsUploadedByPmlUser = Boolean.Parse(reader["IsUploadedByPmlUser"].ToString());
                }

                if (reader["NumPages"] == DBNull.Value)
                {
                    NumPages = null;
                }
                else
                {
                    NumPages = int.Parse(reader["NumPages"].ToString());
                }

                if (reader["DisplayedEstimatedWaitTimeInSeconds"] == DBNull.Value)
                {
                    DisplayedEstimatedWaitTimeInSeconds = null;
                }
                else
                {
                    DisplayedEstimatedWaitTimeInSeconds = int.Parse(reader["DisplayedEstimatedWaitTimeInSeconds"].ToString());
                }

                if (reader["ActualProcessingTimeFromEnqueuingInSeconds"] == DBNull.Value)
                {
                    ActualProcessingTimeFromEnqueuingInSeconds = null;
                }
                else
                {
                    ActualProcessingTimeFromEnqueuingInSeconds = int.Parse(reader["ActualProcessingTimeFromEnqueuingInSeconds"].ToString());
                }
            }
        }
        protected void PageLoad(object sender, EventArgs e)
        {
            RegisterResources();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
