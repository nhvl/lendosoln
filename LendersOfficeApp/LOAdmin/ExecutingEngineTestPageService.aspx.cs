﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using System.Runtime.Serialization;
using ConfigSystem.Engine;
using LendersOffice.ConfigSystem;
using DataAccess;
using System.Text;
using ConfigSystem.DataAccess;
using LendersOffice.Security;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using System.Text.RegularExpressions;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOfficeApp.LOAdmin
{
    class ExecutingEngineTestEvaluator : IEngineValueEvaluator
    {
        private Dictionary<string, IList<bool>> m_boolHash = new Dictionary<string, IList<bool>>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, IList<int>> m_intHash = new Dictionary<string, IList<int>>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, IList<float>> m_floatHash = new Dictionary<string, IList<float>>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, IList<string>> m_stringHash = new Dictionary<string, IList<string>>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, IList<DateTime>> m_dateTimeHash = new Dictionary<string, IList<DateTime>>(StringComparer.OrdinalIgnoreCase);

        public void Add(string name, IList<bool> value)
        {
            m_boolHash.Add(name, value);
        }
        public void Add(string name, IList<int> value)
        {
            m_intHash.Add(name, value);
        }
        public void Add(string name, IList<float> value)
        {
            m_floatHash.Add(name, value);
        }
        public void Add(string name, IList<string> value)
        {
            m_stringHash.Add(name, value);
        }
        public void Add(string name, IList<DateTime> value)
        {
            m_dateTimeHash.Add(name, value);
        }
        #region IEngineValueEvaluator Members

        public IList<bool> GetBoolValues(string name)
        {
            return m_boolHash[name];
        }

        public IList<int> GetIntValues(string name)
        {
            try
            {
                return m_intHash[name];
            }
            catch
            {
                throw new Exception("Missing " + name);
            }
        }

        public IList<float> GetFloatValues(string name)
        {
            return m_floatHash[name];
        }

        public IList<string> GetStringValues(string name)
        {
            var results = m_stringHash[name];

            // av - 222777 Workflow Bug - Blank Subj Prop State Not Evaluating Correctly
            if (name.Equals("sSpState", StringComparison.OrdinalIgnoreCase) && results != null)
            {
                results = new List<string>(results); //copy list.
                Regex blankSpaceRegex = new Regex(@"^\s{1,2}$");

                for (int i = 0; i < results.Count; i++)
                {
                    results[i] = blankSpaceRegex.Replace(results[i], string.Empty);
                }
            }

            return results;
        }

        #endregion

        #region IEngineValueEvaluator Members


        public IList<DateTime> GetDateTimeValues(string name)
        {
            if (name.StartsWith("today+", StringComparison.OrdinalIgnoreCase))
            {
                int nDays = 0;
                if (int.TryParse(name.Substring(6), out nDays) == false)
                {
                    Tools.LogErrorWithCriticalTracking("Unhandle for [" + name + "] in LoanValueEvaluator.GetDateTimeValues");
                    throw new CBaseException(ErrorMessages.Generic, "Unhandle " + name + " in GetDateTimeValues()");
                }
                List<DateTime> list = new List<DateTime>();
                
                list.Add(DateTime.Today.AddDays(nDays));
                return list;
            }
            else if (name.StartsWith("today-", StringComparison.OrdinalIgnoreCase))
            {
                int nDays = 0;
                if (int.TryParse(name.Substring(6), out nDays) == false)
                {
                    Tools.LogErrorWithCriticalTracking("Unhandle for [" + name + "] in LoanValueEvaluator.GetDateTimeValues");
                    throw new CBaseException(ErrorMessages.Generic, "Unhandle " + name + " in GetDateTimeValues()");
                }
                List<DateTime> list = new List<DateTime>();
                list.Add(DateTime.Today.AddDays(-1 * nDays));
                return list;
            }
            return m_dateTimeHash[name];
        }

        #endregion

        public bool IsDataModified(string name)
        {
            return false;
        }
    }

    [DataContract]
    class ExecutingEngineTestInputItem
    {
        [DataContract]
        class _
        {
            public _(string desc, string value, string selected)
            {
                this.Description = desc;
                this.Value = value;
                this.Selected = selected;
            }
            [DataMember(Name="val")]
            public string Value;

            [DataMember(Name="desc")]
            public string Description;

            [DataMember(Name="selected")]
            public string Selected;
        }

        [DataMember(Name="id")]
        public string Id;

        [DataMember(Name="desc")]
        public string Description;

        [DataMember(Name="type")]
        public string Type;

        [DataMember(Name="disabled")]
        private string Disabled = string.Empty;

        public bool IsDisabled
        {
            set { Disabled = value ? "disabled" : string.Empty; }
        }

        [DataMember(Name="opts")]
        private List<_> Options = new List<_>();

        public void AddOption(string desc, string value)
        {
            Options.Add(new _(desc, value, string.Empty));
        }

        public void AddOption(string desc, string value, bool isSelected)
        {
            Options.Add(new _(desc, value, isSelected ? "selected" : string.Empty));
        }
        
    }

    [DataContract]
    class ExecutingEngineDebugResultItem
    {
        [DataContract]
        public class ExecutingEngineDebugResultConditionItem
        {
            [DataContract]
            class _
            {
                public _(string r, string v, string f, string e, string a)
                {
                    this.VarName = v;
                    this.Function = f;
                    this.ExpectedValue = e;
                    this.ActualValue = a;
                    this.Result = r;
                }
                [DataMember(Name = "VarName")]
                public string VarName;
                [DataMember(Name = "Func")]
                public string Function;
                [DataMember(Name = "ExpectedValue")]
                public string ExpectedValue;
                [DataMember(Name = "ActualValue")]
                public string ActualValue;
                [DataMember(Name = "Result")]
                public string Result;
            }
            [DataMember(Name = "Result")]
            public string Result;

            [DataMember(Name = "ConditionId")]
            public Guid ConditionId;

            [DataMember(Name = "ParameterList")]
            private List<_> ParameterList = new List<_>();

            public void Add(string result, string varName, string function, string expected, string actual)
            {
                ParameterList.Add(new _(result, varName, function, expected, actual));
            }
        }

        [DataContract]
        public class TriggerResultItem
        {
            [DataMember(Name = "TriggerName")]
            public string TriggerName;

            [DataMember(Name = "Result")]
            public string Result;

            [DataMember(Name = "ConditionList")]
            private List<ExecutingEngineDebugResultConditionItem> ConditionList = new List<ExecutingEngineDebugResultConditionItem>();

            public void AddCondition(ExecutingEngineDebugResultConditionItem condition)
            {
                ConditionList.Add(condition);
            }
        }

        [DataMember(Name = "Operation")]
        public string Operation;

        [DataMember(Name = "Result")]
        public string Result;

        [DataMember(Name = "ConditionList")]
        private List<ExecutingEngineDebugResultConditionItem> ConditionList = new List<ExecutingEngineDebugResultConditionItem>();

        [DataMember(Name = "RestraintList")]
        private List<ExecutingEngineDebugResultConditionItem> RestraintList = new List<ExecutingEngineDebugResultConditionItem>();


        [DataMember(Name = "ConditionTriggerList")]
        private List<TriggerResultItem> ConditionTriggerList = new List<TriggerResultItem>();

        [DataMember(Name = "RestraintTriggerList")]
        private List<TriggerResultItem> RestraintTriggerList = new List<TriggerResultItem>();

        public void AddCondition(ExecutingEngineDebugResultConditionItem condition)
        {
            ConditionList.Add(condition);
        }

        public void AddRestraint(ExecutingEngineDebugResultConditionItem condition)
        {
            RestraintList.Add(condition);
        }

        public void AddConditionTrigger(TriggerResultItem trigger)
        {
            ConditionTriggerList.Add(trigger);
        }

        public void AddRestraintTrigger(TriggerResultItem trigger)
        {
            RestraintTriggerList.Add(trigger);
        }
    }

    public partial class ExecutingEngineTestPageService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch(methodName)
            {
                case "ListOperationByBroker":
                    ListOperationByBroker();
                    break;
                case "ListInputByOperation":
                    ListInputByOperation();
                    break;
                case "LoadUserAndLoan":
                    LoadUserAndLoan();
                    break;
                case "CanPerform":
                    CanPerform();
                    break;
            }
        }
        private void LoadUserAndLoan()
        {
            string userName = GetString("userName");
            string userType = GetString("userType");
            string sLNm = GetString("sLNm");
            Guid sBrokerId = GetGuid("brokerId");
            string operationId = GetString("operation");

            if (string.IsNullOrEmpty(operationId))
            {
                return;
            }

            WorkflowOperation operation = WorkflowOperations.Get(operationId);

            // 10/5/2010 dd - Retrieve User Id, LoanId.
            Guid userId = Guid.Empty;
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", sBrokerId),
                                            new SqlParameter("@LoginNm", userName),
                                            new SqlParameter("@UserType", userType)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(sBrokerId, "internal_RetrieveUserIdByUserName", parameters))
            {
                if (reader.Read())
                {
                    userId = (Guid)reader["UserId"];
                }
            }
            if (userId == Guid.Empty)
            {
                throw new CBaseException("Could not find userName=" + userName, "Could not find userName=" + userName);
            }


            Guid sLId = Tools.GetLoanIdByLoanName(sBrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                throw new CBaseException("Could not find loan name=" + sLNm, "Could not find loan name=" + sLNm);
            }
            AbstractUserPrincipal principal = PrincipalFactory.Create(sBrokerId, userId, userType, true /* allowDuplicateLogin */, false /* isStoreToCookie */);

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(principal.BrokerId, sLId, operation);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

            var lendingQBDebug = LendingQBExecutingEngine.MasterDebugOperation(operation, valueEvaluator, true /*debugTriggerEnable*/);

            List<ExecutingEngineDebugResultItem> list = new List<ExecutingEngineDebugResultItem>();
            //ParameterReporting parameterReporting = ParameterReporting.RetrieveByBrokerId(sBrokerId);
            //parameterReporting.UpdateCustomList(

            WorkflowSystemConfigModel model = new WorkflowSystemConfigModel(sBrokerId, false);
            ParameterReporting parameterReporting = model.ParameterREporting;

            foreach (var o in lendingQBDebug.ExecutingEngineDebugList)
            {
                ExecutingEngineDebugResultItem item = new ExecutingEngineDebugResultItem();
                item.Operation = o.Operation;
                item.Result = o.CanPerform ? "ALLOW" : "NOT ALLOW";
                list.Add(item);

                LoanConditionsFromDebug(parameterReporting, o.RestraintList, item, true);
                LoanConditionsFromDebug(parameterReporting, o.ConditionList, item, false);
                LoanTriggersFromDebug(parameterReporting, o.RestraintTriggerList, item, true);
                LoanTriggersFromDebug(parameterReporting, o.ConditionTriggerList, item, false);
            }

            string userFriendlyMessage = string.Empty;

            if (lendingQBDebug.CanPerform == false)
                {
                userFriendlyMessage = LendingQBExecutingEngine.GetUserFriendlyMessage(operation, valueEvaluator);
            }

            SetResult("UserFriendlyMessage", userFriendlyMessage);
            SetResult("operation", lendingQBDebug.Operation.Id);
            SetResult("Result", lendingQBDebug.CanPerform ? "ALLOW" : "NOT ALLOW");
            SetResult("Description", lendingQBDebug.Description);
            SetResult("json", ObsoleteSerializationHelper.JsonSerialize(list));
        }

        ExecutingEngineDebugResultItem.ExecutingEngineDebugResultConditionItem CreateResultConditionItem(ExecutingEngineConditionDebug debugCondition, ParameterReporting parameterReporting)
        {
            ExecutingEngineDebugResultItem.ExecutingEngineDebugResultConditionItem condition = new ExecutingEngineDebugResultItem.ExecutingEngineDebugResultConditionItem();
            condition.ConditionId = debugCondition.ConditionId;
            condition.Result = debugCondition.FailedParameterCount != 0 ? "False" : "True";

            foreach (var matchParameter in debugCondition.SuccessfulParameterList)
            {
                SecurityParameter.Parameter parameter = parameterReporting[matchParameter.VarName];

                string varName;
                if (parameter != null)
                {
                    varName = parameter.Id;
                }
                else
                {
                    varName = matchParameter.VarName;
                }
                condition.Add("MATCH", varName, matchParameter.Function.ToString(), GenerateValueList(parameterReporting, matchParameter.VarName, matchParameter.ExpectedValueList),
                    GenerateValueList(parameterReporting, matchParameter.VarName, matchParameter.ActualValueList));
            }
            foreach (var matchParameter in debugCondition.FailedParameterList)
            {
                SecurityParameter.Parameter parameter = parameterReporting[matchParameter.VarName];

                string varName;
                if (parameter != null)
                {
                    varName = parameter.Id;
                }
                else
                {
                    varName = matchParameter.VarName;
                }

                condition.Add("FAIL", varName, matchParameter.Function.ToString(), GenerateValueList(parameterReporting, matchParameter.VarName, matchParameter.ExpectedValueList),
                    GenerateValueList(parameterReporting, matchParameter.VarName, matchParameter.ActualValueList));

            }

            return condition;
        }

        private void LoanTriggersFromDebug(ParameterReporting parameterReporting, IEnumerable<TriggerDebug> debugTriggers, ExecutingEngineDebugResultItem item, bool forRestraints)
        {
            foreach (var debugTrigger in debugTriggers)
            {
                ExecutingEngineDebugResultItem.TriggerResultItem trigger = new ExecutingEngineDebugResultItem.TriggerResultItem();

                SecurityParameter.Parameter parameter = parameterReporting.GetSecurityParameterWithValues(debugTrigger.TriggerName);

                trigger.TriggerName = (parameter == null) ? debugTrigger.TriggerName : parameter.FriendlyName;
                trigger.Result = debugTrigger.Value.ToString();
                foreach (var debugCondition in debugTrigger.ConditionList)
                {
                    trigger.AddCondition(CreateResultConditionItem(debugCondition, parameterReporting));
                }

                if(forRestraints)
                {
                    item.AddRestraintTrigger(trigger);
                }
                else
                {
                    item.AddConditionTrigger(trigger);
                }
            }
        }

        private void LoanConditionsFromDebug(ParameterReporting parameterReporting, IEnumerable<ExecutingEngineConditionDebug> conditionList, ExecutingEngineDebugResultItem item, bool forRestraints)
        {
            foreach (var debugCondition in conditionList)
            {
                ExecutingEngineDebugResultItem.ExecutingEngineDebugResultConditionItem condition = CreateResultConditionItem(debugCondition, parameterReporting);

                if ((forRestraints && debugCondition.FailedParameterCount == 0) ||
                    (!forRestraints && debugCondition.FailedParameterCount != 0))
                {
                    condition.Result = "NOT ALLOW";
                }
                else
                {
                    condition.Result = "ALLOW";
                }

                if (forRestraints)
                {
                    item.AddRestraint(condition);
                }
                else
                {
                    item.AddCondition(condition);
                }
            }
        }

        private void CanPerform()
        {
            Guid brokerId = GetGuid("BrokerId");
            string operationId = GetString("operation");
            var operation = WorkflowOperations.Get(operationId);

            bool bResult = true;
            List<ExecutingEngineDebugResultItem> list = new List<ExecutingEngineDebugResultItem>();

            ExecutingEngine brokerEngine = GetExecutingEngine(brokerId);
            ExecutingEngine systemEngine = LendingQBExecutingEngine.SystemExecutingEngine;

            bool includeBrokerDependencies = brokerEngine != null && brokerEngine.HasOperation(operation);
            bool includeSystemDependencies = brokerEngine == null ||
                (!brokerEngine.HasRestraint(operation) && systemEngine.HasRestraint(operation)) ||
                (!brokerEngine.HasPrivilege(operation) && systemEngine.HasPrivilege(operation));

            if (includeBrokerDependencies || includeSystemDependencies)
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                
                if (includeBrokerDependencies)
                {
                    GetCompleteVarDefinition(brokerEngine, dictionary);
                }

                if (includeSystemDependencies)
                {
                    GetCompleteVarDefinition(systemEngine, dictionary);
                }

                ExecutingEngineTestEvaluator valueEvaluator = new ExecutingEngineTestEvaluator();
                ParameterReporting parameterReporting = ParameterReporting.RetrieveByBrokerId(brokerId);

                var dependencyOperationList = new List<WorkflowOperation>(); // 10/5/2010 dd - Hard code right now.
                dependencyOperationList.Add(operation);
                if (operation == WorkflowOperations.WriteTemplate)
                {
                    dependencyOperationList.Add(WorkflowOperations.ReadTemplate);
                }
                else if (operation == WorkflowOperations.RunPmlForAllLoans ||
                    operation == WorkflowOperations.RunPmlForRegisteredLoans ||
                    operation == WorkflowOperations.RunPmlToStep3 ||
                    operation == WorkflowOperations.WriteLoan)
                {
                    dependencyOperationList.Add(WorkflowOperations.ReadLoan);
                }

                HashSet<string> dependentFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

                if (includeBrokerDependencies)
                {
                    dependentFields.UnionWith(brokerEngine.GetDependencyVariableList(dependencyOperationList));
                }

                if (includeSystemDependencies)
                {
                    dependentFields.UnionWith(systemEngine.GetDependencyVariableList(dependencyOperationList));
                }

                foreach (var o in dependentFields)
                {
                    if (o.StartsWith("today+", StringComparison.OrdinalIgnoreCase) || o.StartsWith("today-", StringComparison.OrdinalIgnoreCase))
                    {
                        continue;
                    }
                    string value = GetString(o);

                    if (string.IsNullOrEmpty(value))
                    {
                        continue;
                    }
                    string varType = dictionary[o];

                    if (varType == "Bool")
                    {
                        valueEvaluator.Add(o, new bool[] { bool.Parse(value) });
                    }
                    else if (varType == "Int")
                    {
                        valueEvaluator.Add(o, new int[] { int.Parse(value) });
                    }
                    else if (varType == "Float")
                    {
                        valueEvaluator.Add(o, new float[] { float.Parse(value) });

                    }
                    else if (varType == "String")
                    {
                        valueEvaluator.Add(o, new string[] { value });

                    }
                    else if (varType == "DateTime")
                    {
                        valueEvaluator.Add(o, new DateTime[] { DateTime.Parse(value) });

                    }
                    else
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Unsupport VarTYpe=" + varType);
                    }
                }

                foreach (var op in dependencyOperationList)
                {
                    ExecutingEngine restraintEngine = null;
                    ExecutingEngine privilegeEngine = null;

                    if (brokerEngine != null && brokerEngine.HasRestraint(op))
                    {
                        restraintEngine = brokerEngine;
                    }
                    else if (systemEngine.HasRestraint(op))
                    {
                        restraintEngine = systemEngine;
                    }

                    if (brokerEngine != null && brokerEngine.HasPrivilege(op))
                    {
                        privilegeEngine = brokerEngine;
                    }
                    else if (systemEngine.HasPrivilege(op))
                    {
                        privilegeEngine = systemEngine;
                    }

                    ExecutingEngineDebug debug = new ExecutingEngineDebug(op.Id);
                    Dictionary<string, TriggerDebug> restraintTriggerDict = new Dictionary<string, TriggerDebug>();
                    Dictionary<string, TriggerDebug> privilegeTriggerDict = new Dictionary<string, TriggerDebug>();

                    debug.CanPerform = true;
                    if (restraintEngine != null && restraintEngine.HasPassingRestraint(operation, valueEvaluator, debug, restraintTriggerDict))
                    {
                        debug.CanPerform = false;
                    }

                    if (privilegeEngine == null)
                    {
                        debug.CanPerform = false;
                    }
                    else
                    {
                        debug.CanPerform &= privilegeEngine.HasPassingPrivilege(operation, valueEvaluator, debug, privilegeTriggerDict);
                    }

                    debug.SetTriggers(restraintTriggerDict, privilegeTriggerDict);
                    bResult &= debug.CanPerform;

                    ExecutingEngineDebugResultItem item = new ExecutingEngineDebugResultItem();
                    item.Operation = op.Id;
                    item.Result = debug.CanPerform ? "ALLOW" : "NOT ALLOW";

                    list.Add(item);

                    LoanConditionsFromDebug(parameterReporting, debug.RestraintList, item, true);
                    LoanConditionsFromDebug(parameterReporting, debug.ConditionList, item, false);
                    LoanTriggersFromDebug(parameterReporting, debug.RestraintTriggerList, item, true);
                    LoanTriggersFromDebug(parameterReporting, debug.ConditionTriggerList, item, false);
                }
            }

            string json = ObsoleteSerializationHelper.JsonSerialize(list);
            SetResult("json", json);
            SetResult("ResultString", bResult ? "ALLOW" : "NOT ALLOW");
        }

        private string GenerateValueList(ParameterReporting parameterReporting, string varName, IEnumerable<string> valueList)
        {
            StringBuilder sb = new StringBuilder();

            SecurityParameter.Parameter parameter = parameterReporting.GetSecurityParameterWithValues(varName);

            List<SecurityParameter.EnumMapping> enumMapping = null;
            if (null != parameter && parameter.Type == SecurityParameter.SecurityParameterType.Enum)
            {
                enumMapping = ((SecurityParameter.EnumeratedParmeter)parameter).EnumMapping;
            }
            bool isFirst = true;
            foreach (var o in valueList)
            {
                if (isFirst == false)
                {
                    sb.Append(", ");
                }
                if (enumMapping != null)
                {
                    bool isFound = false;
                    foreach (var map in enumMapping)
                    {
                        if (map.RepValue.Equals(o, StringComparison.OrdinalIgnoreCase))
                        {
                            sb.Append(map.FriendlyValue);
                            isFound = true;
                            break;
                        }
                    }
                    if (isFound == false)
                    {
                        sb.Append(o);
                    }
                }
                else
                {
                    if (parameter != null && parameter.Type == SecurityParameter.SecurityParameterType.Boolean)
                    {
                        sb.Append(o == "1" ? "True" : "False");
                    }
                    else
                    {
                        sb.Append(o);
                    }
                }
                isFirst = false;
            }
            return sb.ToString();
        }
        private void ListInputByOperation()
        {
            Guid brokerId = GetGuid("BrokerId");
            string operationId = GetString("operation");
            var operation = WorkflowOperations.Get(operationId);

            List<ExecutingEngineTestInputItem> list = new List<ExecutingEngineTestInputItem>();


            ExecutingEngine brokerEngine = GetExecutingEngine(brokerId);
            ExecutingEngine systemEngine = LendingQBExecutingEngine.SystemExecutingEngine;

            bool includeBrokerDependencies = brokerEngine != null && brokerEngine.HasOperation(operation);
            bool includeSystemDependencies = brokerEngine == null ||
                (!brokerEngine.HasRestraint(operation) && systemEngine.HasRestraint(operation)) ||
                (!brokerEngine.HasPrivilege(operation) && systemEngine.HasPrivilege(operation));

            if (includeBrokerDependencies || includeSystemDependencies)
            {
                var dependencyOperationList = new List<WorkflowOperation>(); // 10/5/2010 dd - Hard code right now.
                dependencyOperationList.Add(operation);
                if (operation == WorkflowOperations.WriteTemplate)
                {
                    dependencyOperationList.Add(WorkflowOperations.ReadTemplate);
                }
                else if (operation == WorkflowOperations.RunPmlForAllLoans ||
                    operation == WorkflowOperations.RunPmlForRegisteredLoans ||
                    operation == WorkflowOperations.RunPmlToStep3 ||
                    operation == WorkflowOperations.WriteLoan)
                {
                    dependencyOperationList.Add(WorkflowOperations.ReadLoan);
                }

                //Dictionary<string, string> dictionary = GetCompleteVarDefinition(engine);
                ParameterReporting parameterReporting = ParameterReporting.RetrieveByBrokerId(brokerId);

                HashSet<string> dependentFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

                if (includeBrokerDependencies)
                {
                    dependentFields.UnionWith(brokerEngine.GetDependencyVariableList(dependencyOperationList));
                }

                if (includeSystemDependencies)
                {
                    dependentFields.UnionWith(systemEngine.GetDependencyVariableList(dependencyOperationList));
                }

                foreach (var o in dependentFields)
                {
                    if (o.StartsWith("today+", StringComparison.OrdinalIgnoreCase) || o.StartsWith("today-", StringComparison.OrdinalIgnoreCase))
                    {
                        // 12/20/2011 dd - Skip the special keyword.
                        continue;
                    }
                    SecurityParameter.Parameter parameter = parameterReporting[o];

                    ExecutingEngineTestInputItem item = new ExecutingEngineTestInputItem();

                    if (null != parameter)
                    {
                        item.Id = parameter.Id;
                        item.Description = parameter.FriendlyName;
                        switch (parameter.Type)
                        {
                            case SecurityParameter.SecurityParameterType.Rate:
                            case SecurityParameter.SecurityParameterType.Money:
                            case SecurityParameter.SecurityParameterType.Count:
                            case SecurityParameter.SecurityParameterType.String:
                            case SecurityParameter.SecurityParameterType.Date:
                                item.Type = "text";
                                break;
                            case SecurityParameter.SecurityParameterType.Boolean:
                            case SecurityParameter.SecurityParameterType.Custom:
                                item.Type = "bool";
                                break;
                            case SecurityParameter.SecurityParameterType.Enum:
                                item.Type = "enum";
                                SecurityParameter.EnumeratedParmeter enumParam = parameter as SecurityParameter.EnumeratedParmeter;
                                foreach (var em in enumParam.EnumMapping)
                                {
                                    item.AddOption(em.FriendlyValue, em.RepValue);
                                }
                                break;
                            default:
                                throw new UnhandledEnumException(parameter.Type);
                        }
                    }
                    else
                    {
                        item.Id = o;
                        item.Type = "text";
                        item.Description = "";
                    }


                    list.Add(item);
                }
            }

            string json = ObsoleteSerializationHelper.JsonSerialize(list);

            SetResult("json", json);
        }
        private void ListOperationByBroker()
        {
            // 10/5/2010 dd - Merge both Lender operation and System operation.
            Guid brokerId = GetGuid("BrokerId");

            Dictionary<string, KeyValuePair<string, string>> dictionary = new Dictionary<string, KeyValuePair<string, string>>(StringComparer.OrdinalIgnoreCase);

            ExecutingEngine engine = GetExecutingEngine(brokerId);

            if (null != engine)
            {
                SetResult("EngineReleasedDate", engine.ReleasedDate.ToString());
                SetResult("EngineSize", engine.Size);
                foreach (var operation in engine.GetOperationList())
                {
                    dictionary.Add(operation, new KeyValuePair<string, string>(operation, operation + " (lender)"));
                }
            }

            engine = LendingQBExecutingEngine.SystemExecutingEngine;
            foreach (var operation in engine.GetOperationList())
            {
                var item = new KeyValuePair<string, string>(operation, operation + " (SYSTEM)");

                if (dictionary.ContainsKey(operation) == false)
                {
                    dictionary.Add(operation, item);
                }
            }

            List<KeyValuePair<string, string>> operationList = new List<KeyValuePair<string, string>>(dictionary.Values);
            string json = ObsoleteSerializationHelper.JsonSerialize(operationList);

            SetResult("json", json);
        }

        private void GetCompleteVarDefinition(ExecutingEngine engine, Dictionary<string, string> varDict) 
        {
            if (engine != null)
            {
                foreach (var o in engine.GetVariableList())
                {
                    if (varDict.ContainsKey(o.Key))
                    {
                        if (varDict[o.Key] != o.Value)
                        {
                            Tools.LogError("CustomVar with diff type " + o.Value + " " + varDict[o.Key]);
                        }
                        continue;
                    }
                    varDict.Add(o.Key, o.Value);
                }
            }
        }

        private ExecutingEngine GetExecutingEngine(Guid brokerId)
        {
            if (brokerId == new Guid("11111111-1111-1111-1111-111111111111"))
            {
                return LendingQBExecutingEngine.SystemExecutingEngine;
            }
            else
            {
                IConfigRepository configRepository = ConfigHandler.GetRepository(brokerId);
                return ExecutingEngine.GetEngineByBrokerId(configRepository, brokerId);
            }
        }
    }
}
