﻿<%@ Page Language="C#" CodeBehind="~/LOAdmin/JSOnlyHotfix.aspx.cs" MasterPageFile="~/LOAdmin/loadmin.Master" Inherits="LendersOfficeApp.LOAdmin.JSOnlyHotfix" Title="JS-only hotfix"%>
<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                <form id="Form1" runat="server">
                    <div>
                        <p class="text-center">
                            I just midday hotfixed some javascript/css files and want the users to see it, but don't want to iisreset
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 col-xs-offset-8">
                            <span class="clipper">
                                <asp:Button ID="Button1" class="btn btn-primary"  runat="server" Text="Reset JS version!" onclick="ResetJSVersion"></asp:Button>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <p class="text-center">
                            Current commit SHA-1 hash: <code id="GitSha1Hash" runat="server" />
                        </p>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
