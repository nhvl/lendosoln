﻿<%@ Page Language="C#" MasterPageFile="~/LOAdmin/loadmin.Master" Inherits="LendersOfficeApp.LOAdmin.QuickPricer2FeatureInDB" Title="QuickPricer 2.0 Presence in Database" Codebehind="~/LOAdmin/QuickPricer2FeatureInDB.aspx.cs" AutoEventWireup="True" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ID="PMLContain" ContentPlaceHolderID="Main" Runat="Server">
    <div class="row container-fluid">
        <div class="col-xs-10 col-xs-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    <!-- Keeping The below only to demonstrate a tmpl pattern. -->
                    <script id="canDeleteHeaderRowTemplate" type="text/x-jquery-tmpl">
                        <tr>
                            <th>
                                Loan ID
                            </th>
                            <th>
                                Broker ID
                            </th>
                            <th>
                                Can Delete?
                            </th>
                            <th>
                                Reason
                            </th>
                            <th>
                                FileDBEntryInfos (from DB)
                            </th>
                        </tr>
                    </script>
                    <script id="canDeleteBodyRowTemplate" type="text/x-jquery-tmpl">
                        <tr id="${LoanID}">
                            <td>
                                ${LoanID}
                            </td>
                            <td>
                                ${BrokerID}
                            </td>
                            <td>
                                ${CanDeleteLoan}
                            </td>
                            <td>
                                ${ReasonCantDeleteLoan}
                            </td>
                            <td>
                                {{tmpl(FileDBEntryInfos) '#fileDBEntryInfoTemplate'}}
                            </td>
                        </tr>
                    </script>   
                    <script id="fileDBEntryInfoTemplate" type="text/x-jquery-tmpl">
                        ${FileDBType} : ${Key},
                    </script>
                    <form id="Form1" runat="server" >
                        <div class="row">
                            <div class="form-horizontal form-horizontal-left col-xs-8 col-xs-offset-2">
                                <div class="form-group row">
                                    <div class="col-xs-1">
                                        <input type="button" id="bGetUsersWithQP2ExplicitlyEnabled" data-handler="GetUsersWithQP2ExplicitlyEnabled" class="btn btn-sm btn-primary" value="Get"/>
                                    </div>
                                    <label class="control-label col-xs-11">Users with QP2.0 explicitly enabled</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-1">
                                        <input type="button" id="bGetQP2LoansByUserID" class="btn btn-sm btn-primary" data-handler="GetQP2LoansByUserID" data-argsGetter="f_getQP2LoansByUserIDArgs" value="Get"/>
                                    </div>
                                    <label class="control-label col-xs-3">Loans in pool for user:</label>
                                    <div  class="col-xs-5">
                                        <input type="text" id="tbUserID" class="form-control input-sm"/>
                                     </div>
                                     <span id="LoansInPoolError"  class="text-danger"/>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-1">
                                        <input type="button" id="bGetBrokersWithQP2HardEnabled" class="btn btn-sm btn-primary"  data-handler="GetActiveBrokersWithPml2AsQuickPricerMode" data-argsGetter="f_getBrokerArgs" value="Get"/>
                                    </div>
                                    <label class="control-label col-xs-4">Brokers with Use QP2.0? set to </label>
                                    <div  class="col-xs-4">
                                        <asp:DropDownList runat="server" ID="m_Pml2AsQuickPricerMode" class="form-control input-sm"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-1">
                                        <input type="button" id="bGetAbandonedSandboxedLoans" class="btn btn-sm btn-primary" data-handler="GetAbandonedSandboxedLoans" value="Get"/>
                                    </div>
                                    <label class="control-label col-xs-11">Abandoned QP 2.0 Sandboxed loans (Long running query.) </label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-1">
                                        <input type="button" id="bGetOldInUseLoans" class="btn btn-sm btn-primary"  data-handler="GetOldInUseLoans" value="Get"/>
                                    </div>
                                    <label class="control-label col-xs-11">Old "in use" loans (sorta slow)</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-1">
                                        <input type="button" id="bGetUnusedLoansNotMatchingQPTemplateAtBroker"  data-handler="GetUnusedLoansNotMatchingQPTemplateAtBroker"class="btn btn-sm btn-primary" value="Get"/>
                                    </div>
                                    <label class="control-label col-xs-11">Unused loans not matching qp template (sorta slow)</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-1">
                                        <input type="button" id="bGetCanDeleteLoan" data-argsGetter="f_getCanDeleteLoanArgs"  data-handler="CanDeleteLoan" class="btn btn-sm btn-primary" value="Get"/>
                                    </div>
                                    <label class="control-label col-xs-3">Can Delete Loan?</label>
                                    <div  class="col-xs-5">
                                        <input type="text" id="tbLoanID" class="form-control input-sm"/>
                                    </div>
                                    <span id="canDeleteError"  class="text-danger"/>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div id="theLoadingDiv" style="display:none">
                                Loading...
                            </div>
                            <div id="theResultsTallyDiv" style="display:none" class="text-info">
                                Num Results = <label id="theResultsTallyLabel"  class="text-info"></label>
                            </div>
                            <table id="theResultsTable" style="display:none" class="table table-striped table-condensed">
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                       </div>
                        <div id="theNoResultsDiv" style="display:none">
                             No Results.
                        </div>
                        <div id="alertModel" class="modal v-middle fade " >
                            <div class="modal-dialog">
                                <div class="modal-content">
                                     <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                                      <h4 class="modal-title">Confirm Delete Processing Priority </h4>
                                    </div>
                                    <div class="modal-body text-center"><label id="alertLabel"></label></div>
                                    <div class="modal-footer">
                                        <span class="clipper"><input type="button" class="btn btn-primary" data-dismiss="modal" value="OK"/></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <script type="text/javascript">
                        function showAlert(alertText){
                            $("#alertLabel").text(alertText);
                            $("#alertModel").modal('show');
                        }

                        $(function() {
                            var $theResultsTable = $j('#theResultsTable');
                            var $thead = $j('#theResultsTable > thead');
                            var $tbody = $j('#theResultsTable > tbody');
                            var $loadingDiv = $j('#theLoadingDiv');
                            var loading = false;
                            var $theNoResultsDiv = $j('#theNoResultsDiv');
                            var $theResultsTallyDiv = $j('#theResultsTallyDiv');
                            var $theResultsTallyLabel = $j('#theResultsTallyLabel');
                            var $tbLoanID = $j('#tbLoanID');
                            var $tbUserID = $j('#tbUserID');
                            var $ddlBrokerQP2Mode = $j(<%=AspxTools.JsGetElementById(m_Pml2AsQuickPricerMode) %>);

                             $j('input[type="button"][class="btn btn-sm btn-primary"]').on('click', f_standardTableHandler);

                            function f_standardTableHandler($eventObject) {
                                removeAlerts();
                                var $target = $j($eventObject.target);
                                var getter = eval($target.attr('data-argsGetter'));
                                var args = {};
                                if (typeof (getter) !== 'undefined') {
                                    args = getter();
                                    if (args === 'undefined'){
                                        return;
                                    }
                                }
                                f_callWebmethod($target.attr('data-handler'), args, f_updateTable);
                            }

                            function f_getQP2LoansByUserIDArgs() {
                                var userId =  $tbUserID.val();
                                if (!isGuid(userId)){
                                    f_showError($tbUserID,"Must be in GUID format.");
                                    return 'undefined';
                                }
                                return { userID:userId };
                            }
                            function f_getCanDeleteLoanArgs() {
                                var loanId = $tbLoanID.val();
                                if (!isGuid(loanId)){
                                    f_showError($tbLoanID,"Must be in GUID format.");
                                    return 'undefined';
                                }
                                return { loanID: loanId };
                            }
                            function f_getBrokerArgs() {
                                return { qp2mode: $ddlBrokerQP2Mode.val() };
                            }

                            function f_updateTable(objArr) {
                                $thead.empty();
                                $tbody.empty();
                                if (objArr.length < 1) return;
                                
                                var keys = Object.keys(objArr[0]);
                                var h = hypescriptDom;
                                
                                $thead.append(
                                    h("tr", {}, keys.map(function(key){ return h("th", {}, h("a", {attrs:{sort:"true"}}, key)) }))
                                );
                                $tbody.append(objArr.map(function(obj){
                                    return h("tr", {}, keys.map(function(key){ 
                                        var value = obj[key];
                                        return h("td", {}, typeof value === "string" ? value : JSON.stringify(value))
                                    }));
                                }));

                                $('a[sort="true"]').click(function(){
                                    var th =  $(this).parents('th').eq(0);
                                    var table = th.parents('table').eq(0);
                                    var rows = table.find('tr:gt(0)').toArray().sort(comparer(th.index()));
                                    this.asc = !this.asc;
                                    if (!this.asc){rows = rows.reverse()}
                                    for (var i = 0; i < rows.length; i++){table.append(rows[i])}
                                })
                                function comparer(index) {
                                    return function(a, b) {
                                        var valA = getCellValue(a, index), valB = getCellValue(b, index)
                                        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
                                    }
                                }
                                function getCellValue(row, index){ return $(row).children('td').eq(index).html() }
                            }

                            function f_callWebmethod(methodName, data, successHandler) {
                                if (loading) {
                                    alert('already loading another result.');
                                    return false;
                                }
                                $theResultsTallyDiv.toggle(false);
                                $theNoResultsDiv.toggle(false);
                                $theResultsTable.toggle(false);
                                $loadingDiv.toggle(true);
                                loading = true;
                                var success = false;
                                callWebMethodAsync({
                                    type: 'POST',
                                    url: 'QuickPricer2FeatureInDB.aspx/' + methodName,
                                    data: JSON.stringify(data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: true, // for UI responsiveness
                                    success: function(msg) {
                                        result = msg.d;
                                        if (result.length == 0) {
                                            $theNoResultsDiv.toggle(true);
                                        }
                                        success = true;
                                        loading = false;
                                        $loadingDiv.toggle(false);
                                        successHandler(result);
                                        $theResultsTallyLabel.text(result.length);
                                        $theResultsTable.toggle(true);
                                        $theResultsTallyDiv.toggle(true);
                                    },
                                    error: function(msg) {
                                        loading = false;
                                        $loadingDiv.toggle(false);
                                        var msgToShowUser = msg.UserMessage || 'Call to ' + methodName + ' failed.';
                                        showAlert(msgToShowUser);
                                    }
                                });
                                return success;
                            }

                            function isGuid(value) {    
                                var regex = /[a-f0-9]{8}(?:-[a-f0-9]{4}){3}-[a-f0-9]{12}/i;
                                var match = regex.exec(value);
                                return match != null;
                            }

                            function removeAlerts(){
                                $("div.alert").hide();
                                $("div.has-error").removeClass( "has-error has-feedback" );
                                $("span.text-danger").text("");
                            }

                             function f_showError(textbox,errorText) {

                                textbox.parent().closest('div').addClass( "has-error has-feedback" );
                                textbox.parent().siblings('.text-danger').text(errorText);
                                textbox.focus();
                            }

                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
