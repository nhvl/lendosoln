﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Web.Services;
using LendersOffice.Migration;
using LendersOfficeApp.los.admin;
using LendersOffice.ObjLib.QuickPricer;
using System.Threading;
using LendersOffice.Security;
using DataAccess;
using System.Data.SqlClient;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;
using System.Data;

namespace LendersOfficeApp.LOAdmin
{
    public partial class QuickPricer2FeatureInDB : SecuredAdminPage
    {
        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_7_1;
        }
        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJquery = true;
            m_loadLOdefaultScripts = false; 
            RegisterJsScript("jquery.tmpl.js");
        }
        protected void PageLoad(object sender, EventArgs e)
        {
            Bind_Pml2AsQuickPricerMode(m_Pml2AsQuickPricerMode);
        }
        public override string StyleSheet
        {
            get { return string.Empty; }
        }
        private void Bind_Pml2AsQuickPricerMode(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("No", E_Pml2AsQuickPricerMode.Disabled.ToString("D")));
            ddl.Items.Add(new ListItem("Yes - User level access", E_Pml2AsQuickPricerMode.PerUser.ToString("D")));
            ddl.Items.Add(new ListItem("Yes", E_Pml2AsQuickPricerMode.Enabled.ToString("D")));
        }

        [WebMethod]
        public static object[] GetQP2LoansByUserID(Guid userID)
        {
            try
            {
                var sqlStatement = "select * from quickpricer_2_loan_usage where userIDofUsingUser = @id";

                var loans = new List<Object>();
                bool hasAtLeastOneRecord = false;

                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    while (reader.Read())
                    {
                        loans.Add(new
                        {
                            LoanID = (Guid)reader["LoanID"],
                            BrokerID = (Guid)reader["BrokerID"],
                            QuickPricerTemplateID = (Guid)reader["QuickPricerTemplateID"],
                            QuickPricerTemplateFileVersion = (int)reader["QuickPricerTemplateFileVersion"],
                            IsInUse = (bool)reader["IsInUse"],
                            LastUsedD = (
                              reader.AsNullableStruct<DateTime>("LastUsedD").HasValue
                            ? reader.AsNullableStruct<DateTime>("LastUsedD").Value.ToLongDateString()
                            : "NULL")
                        });
                        hasAtLeastOneRecord = true;
                    }
                };

                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    var listParams = new SqlParameter[] { new SqlParameter("@id", userID) };
                    DBSelectUtility.ProcessDBData(connInfo, sqlStatement, null, listParams, readHandler);

                    if (hasAtLeastOneRecord)
                    {
                        break;
                    }
                }

                return loans.ToArray();
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        [WebMethod]
        public static object[] CanDeleteLoan(Guid loanID)
        {
            try
            {
                var dataLoan = new CFullAccessPageData(loanID, new string[] { "sBrokerID" });
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();
                var broker = dataLoan.BrokerDB;

                string reasonCantDeleteLoan = null;
                IEnumerable<FileDBEntryInfo> fileDBEntryInfos = null;
                var oldPrincipal = Thread.CurrentPrincipal;
                Thread.CurrentPrincipal = SystemUserPrincipal.QP2SystemUser;
                try
                {
                    var canDelete = QuickPricer2LoanPoolManager.CanDeleteLoan(
                        broker,
                        loanID,
                        true,
                        true,
                        true,
                        out reasonCantDeleteLoan,
                        out fileDBEntryInfos);
                    return new object[]{
                    new
                    {
                        BrokerID = broker.BrokerID,
                        CanDeleteLoan = canDelete,
                        ReasonCantDeleteLoan = reasonCantDeleteLoan,
                        FileDBEntryInfos = fileDBEntryInfos
                    }
                };
                }
                catch (Exception e)
                {
                    Thread.CurrentPrincipal = oldPrincipal;
                    Tools.LogError(e);
                    throw;
                }
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        [WebMethod]
        public static object[] GetActiveBrokersWithPml2AsQuickPricerMode(E_Pml2AsQuickPricerMode qp2mode)
        {
            try
            {
                var brokers = new List<object>();
                foreach (var brokerID in QuickPricer2FeatureHelper.GetActiveBrokersWithPml2AsQuickPricerMode(qp2mode))
                {
                    var broker = BrokerDB.RetrieveById(brokerID);
                    brokers.Add(new
                        {
                            CustomerCode = broker.CustomerCode,
                            ID = broker.BrokerID,
                            Name = broker.Name,
                        });
                }
                return brokers.ToArray();
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        [WebMethod]
        public static Object[]GetUsersWithQP2ExplicitlyEnabled()
        {
            try
            {
                var enabledUsers = new List<Object>();
                foreach (var brokerID in QuickPricer2FeatureHelper.GetActiveBrokersWithFeatureEnabledPerUser()) //! better as stored procedure...
                {
                    var broker = BrokerDB.RetrieveById(brokerID);
                    foreach (var user in QuickPricer2FeatureHelper.GetActiveQP2UsersAtBroker(broker))
                    {
                        enabledUsers.Add(new
                            {
                                UserID = user.UserID,
                                LoginName = user.LoginName,
                                UserType = user.UserType,
                                BrokerID = broker.BrokerID,
                                BrokerName = broker.Name
                            });
                    }
                }
                return enabledUsers.ToArray();
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        [WebMethod]
        public static Object[] GetAbandonedSandboxedLoans()
        {
            try
            {
                var abandonedLoans = new List<Object>();
                foreach (var loan in QuickPricer2FeatureHelper.GetAbandonedSandboxedLoans(null)) //! better as stored procedure...
                {
                    abandonedLoans.Add(loan);
                }
                return abandonedLoans.ToArray();
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        [WebMethod]
        public static object[] GetOldInUseLoans()
        {
            try
            {
                var loans = new List<Object>();
                foreach (var brokerID in Tools.GetAllActiveBrokers())
                {
                    var broker = BrokerDB.RetrieveById(brokerID);
                    foreach (var loanID in QuickPricer2LoanPoolManager.FindOldInUseLoansAtBroker(broker)) //! better as stored procedure...
                    {
                        loans.Add(new { LoanID = loanID });
                    }
                }
                return loans.ToArray();
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        [WebMethod]
        public static object[] GetUnusedLoansNotMatchingQPTemplateAtBroker()
        {
            try
            {
                var loans = new List<Object>();
                foreach (var brokerID in Tools.GetAllActiveBrokers())
                {
                    var broker = BrokerDB.RetrieveById(brokerID);
                    if (broker.QuickPricerTemplateId == Guid.Empty)
                    {
                        continue;
                    }
                    foreach (var loanID in QuickPricer2LoanPoolManager.FindUnusedLoansNotMatchingQPTemplateAtBroker(broker)) //! better as stored procedure...
                    {
                        loans.Add(new { LoanID = loanID });
                    }
                }
                return loans.ToArray();
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
