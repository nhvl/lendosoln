﻿using System.Collections.Generic;
using System.Reflection;
using LendersOfficeApp.newlos;
using System;
using DataAccess;
using System.Linq;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Web.Compilation;
using System.IO;
using System.Web;
using System.Web.UI;
using LendersOfficeApp.common;
using System.Web.Hosting;
using LendersOffice.Constants;
using System.Diagnostics;
using LendersOffice.Common;
using LendersOffice.CustomFormField;
using System.Text;
using LendersOffice.Reports;
using LendersOffice.Migration;


namespace LendersOfficeApp.LOAdmin.TaskBackendUtilities
{
    public static class FieldEnumeratorExtensions
    {
        #region Things in CAppData and CPageData that are known to not be loan fields
        private static readonly HashSet<Type> IgnoreLoanDataTypes = new HashSet<Type>
        {
            typeof(CPageBase),
            typeof(LosConvert),
            typeof(E_DataState),
        };

        private static readonly HashSet<string> IgnoredPropertyNames = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase)
        {
            "ByPassFieldSecurityCheck",
        };
        #endregion
        
        public static IEnumerable<System.Reflection.PropertyInfo> FilterTypes(this IEnumerable<System.Reflection.PropertyInfo> props)
        {
            foreach (var p in props)
            {
                if (!IgnoreLoanDataTypes.Contains(p.PropertyType) && !IgnoredPropertyNames.Contains(p.Name))
                {
                    yield return p;
                }
            }
        }
    }
    public static class FieldEnumerator
    {
        /// <summary>
        /// Used to generate the CollectionFields static member
        /// </summary>
        /// <param name="filenames">A list of full paths to the xml schema files.</param>
        private static void GenerateCollectionFields(IEnumerable<string> filenames)
        {
            Debug.WriteLine("string[] CollectionFields = {");

            foreach (string f in filenames)
            {
                Debug.WriteLine("// " + f);
                XDocument xml = XDocument.Load(f);
                //xml root - schema - base name - collection name
                string prefix = xml.Elements().Elements().First().Attribute("name").Value;

                switch (prefix)
                {
                    case ("VorXmlContent"):
                        prefix = "CVorFields";
                        break;
                    case ("AssetXmlContent"):
                        prefix = "CAssetCollection";
                        break;
                    case ("VaPastLXmlContent"):
                        prefix = "CVaPastLCollection";
                        break;
                    case ("LiaXmlContent"):
                        prefix = "CLiaCollection";
                        break;
                    case ("EmplmtXmlContent"):
                        prefix = "CEmpCollection";
                        break;
                    case ("ReXmlContent"):
                        prefix = "CReCollection";
                        break;
                    case ("AgentXmlContent"):
                        prefix = "CAgentFields";
                        break;
                }
                var fields = from d in xml.Elements().Elements().First().Descendants() where d.Name.LocalName.Equals("element", StringComparison.InvariantCultureIgnoreCase) select d;
                foreach (var thing in fields)
                {
                    Debug.WriteLine("\"coll|" + prefix + "|" + thing.Attribute("name").Value + "\",");
                }
                Debug.WriteLine("");

            }
            Debug.WriteLine("};");
        }

        /// <summary>
        /// A list of collection field names. Format is:
        /// [coll:{collection name}:{field name}]
        /// </summary>
        private static string[] StaticCollectionFields = 
        {
           #region Collection fields (created by GenerateCollectionFields)
            // C:\LendOSoln\LendersOfficeLib\DataAccess\VorXmlSchema.xml.config
            "coll|CVorFields|RecordId",
            "coll|CVorFields|LandlordCreditorName",
            "coll|CVorFields|Description",
            "coll|CVorFields|AddressFor",
            "coll|CVorFields|CityFor",
            "coll|CVorFields|StateFor",
            "coll|CVorFields|ZipFor",
            "coll|CVorFields|AddressTo",
            "coll|CVorFields|CityTo",
            "coll|CVorFields|StateTo",
            "coll|CVorFields|ZipTo",
            "coll|CVorFields|PhoneTo",
            "coll|CVorFields|FaxTo",
            "coll|CVorFields|AccountName",
            "coll|CVorFields|VerifSentD",
            "coll|CVorFields|VerifRecvD",
            "coll|CVorFields|VerifExpD",
            "coll|CVorFields|VerifReorderedD",
            "coll|CVorFields|IsForBorrower",
            "coll|CVorFields|VerifT",
            "coll|CVorFields|Attention",
            "coll|CVorFields|PrepD",
            "coll|CVorFields|IsSeeAttachment",
            "coll|CVorFields|VerifSigningEmployeeId",
            "coll|CVorFields|VerifSignatureImgId",

            // C:\LendOSoln\LendersOfficeLib\DataAccess\AssetXmlSchema.xml.config
            "coll|CAssetCollection|RecordId",
            "coll|CAssetCollection|OwnerT",
            "coll|CAssetCollection|AssetT",
            "coll|CAssetCollection|Val",
            "coll|CAssetCollection|Desc",
            "coll|CAssetCollection|FaceVal",
            "coll|CAssetCollection|AccNum",
            "coll|CAssetCollection|AccNm",
            "coll|CAssetCollection|ComNm",
            "coll|CAssetCollection|StAddr",
            "coll|CAssetCollection|City",
            "coll|CAssetCollection|State",
            "coll|CAssetCollection|Zip",
            "coll|CAssetCollection|VerifReorderedD",
            "coll|CAssetCollection|VerifSentD",
            "coll|CAssetCollection|VerifRecvD",
            "coll|CAssetCollection|VerifExpD",
            "coll|CAssetCollection|OtherTypeDesc",
            "coll|CAssetCollection|DepartmentName",
            "coll|CAssetCollection|Attention",
            "coll|CAssetCollection|PrepD",
            "coll|CAssetCollection|IsSeeAttachment",
            "coll|CAssetCollection|OrderRankValue",
            "coll|CAssetCollection|AssetCashDepositT",
            "coll|CAssetCollection|VerifSigningEmployeeId",
            "coll|CAssetCollection|VerifSignatureImgId",
            "coll|CAssetCollection|H4HIsRetirement",
            "coll|CAssetCollection|H4HNumOfJointOwners",
            "coll|CAssetCollection|GiftSource",
            "coll|CAssetCollection|IsEmptyCreated",

            // C:\LendOSoln\LendersOfficeLib\DataAccess\VaPastLSchema.xml.config
            "coll|CVaPastLCollection|RecordId",
            "coll|CVaPastLCollection|LoanTypeDesc",
            "coll|CVaPastLCollection|StAddr",
            "coll|CVaPastLCollection|City",
            "coll|CVaPastLCollection|State",
            "coll|CVaPastLCollection|Zip",
            "coll|CVaPastLCollection|LoanD",
            "coll|CVaPastLCollection|IsStillOwned",
            "coll|CVaPastLCollection|PropSoldD",
            "coll|CVaPastLCollection|VaLoanNum",
            "coll|CVaPastLCollection|OrderRankValue",
            "coll|CVaPastLCollection|DateOfLoan",
            "coll|CVaPastLCollection|CityState",

            // C:\LendOSoln\LendersOfficeLib\ObjLib\Schema\LiabilitySchema.xml.config
            "coll|CLiaCollection|RecordId",
            "coll|CLiaCollection|MatchedReRecordId",
            "coll|CLiaCollection|OwnerT",
            "coll|CLiaCollection|DebtT",
            "coll|CLiaCollection|ComNm",
            "coll|CLiaCollection|ComAddr",
            "coll|CLiaCollection|ComCity",
            "coll|CLiaCollection|ComState",
            "coll|CLiaCollection|ComZip",
            "coll|CLiaCollection|ComPhone",
            "coll|CLiaCollection|ComFax",
            "coll|CLiaCollection|AccNum",
            "coll|CLiaCollection|AccNm",
            "coll|CLiaCollection|Bal",
            "coll|CLiaCollection|Pmt",
            "coll|CLiaCollection|R",
            "coll|CLiaCollection|OrigTerm",
            "coll|CLiaCollection|Due",
            "coll|CLiaCollection|RemainMons",
            "coll|CLiaCollection|WillBePdOff",
            "coll|CLiaCollection|NotUsedInRatio",
            "coll|CLiaCollection|IsPiggyBack",
            "coll|CLiaCollection|Late30",
            "coll|CLiaCollection|Late60",
            "coll|CLiaCollection|Late90Plus",
            "coll|CLiaCollection|IncInReposession",
            "coll|CLiaCollection|IncInBankruptcy",
            "coll|CLiaCollection|IncInForeclosure",
            "coll|CLiaCollection|ExcFromUnderwriting",
            "coll|CLiaCollection|VerifSentD",
            "coll|CLiaCollection|VerifRecvD",
            "coll|CLiaCollection|VerifExpD",
            "coll|CLiaCollection|VerifReorderedD",
            "coll|CLiaCollection|Desc",
            "coll|CLiaCollection|Attention",
            "coll|CLiaCollection|PrepD",
            "coll|CLiaCollection|IsSeeAttachment",
            "coll|CLiaCollection|OrderRankValue",
            "coll|CLiaCollection|DebtJobExpenseT",
            "coll|CLiaCollection|OrigDebtAmt",
            "coll|CLiaCollection|AutoYearMake",
            "coll|CLiaCollection|IsMortFHAInsured",
            "coll|CLiaCollection|IsForAuto",
            "coll|CLiaCollection|ReconcileStatusT",
            "coll|CLiaCollection|PmlAuditTrailXmlContent",
            "coll|CLiaCollection|IsSubjectProperty1stMortgage",
            "coll|CLiaCollection|FullyIndexedPITIPayment",
            "coll|CLiaCollection|H4HIsRetirement",
            "coll|CLiaCollection|H4HNumOfJointOwners",
            "coll|CLiaCollection|VerifSigningEmployeeId",
            "coll|CLiaCollection|VerifSignatureImgId",

            // C:\LendOSoln\LendersOfficeLib\DataAccess\EmplmntXmlSchema.xml.config
            "coll|CEmpCollection|RecordId",
            "coll|CEmpCollection|EmplmtStartD",
            "coll|CEmpCollection|EmplmtEndD",
            "coll|CEmpCollection|EmplrNm",
            "coll|CEmpCollection|EmplrBusPhone",
            "coll|CEmpCollection|EmplrFax",
            "coll|CEmpCollection|EmplrAddr",
            "coll|CEmpCollection|EmplrCity",
            "coll|CEmpCollection|EmplrState",
            "coll|CEmpCollection|JobTitle",
            "coll|CEmpCollection|EmplrZip",
            "coll|CEmpCollection|VerifSentD",
            "coll|CEmpCollection|VerifReorderedD",
            "coll|CEmpCollection|VerifRecvD",
            "coll|CEmpCollection|VerifExpD",
            "coll|CEmpCollection|IsSelfEmplmt",
            "coll|CEmpCollection|MonI",
            "coll|CEmpCollection|EmplmtStat",
            "coll|CEmpCollection|EmpltStartD", // OPM 241180
            "coll|CEmpCollection|EmplmtLen",
            "coll|CEmpCollection|ProfStartD", // OPM 241180
            "coll|CEmpCollection|ProfLen",
            "coll|CEmpCollection|Attention",
            "coll|CEmpCollection|PrepD",
            "coll|CEmpCollection|IsSeeAttachment",
            "coll|CEmpCollection|OrderRankValue",
            "coll|CEmpCollection|VerifSigningEmployeeId",
            "coll|CEmpCollection|VerifSignatureImgId",
            "coll|CEmpCollection|IsCurrent",

            // C:\LendOSoln\LendersOfficeLib\DataAccess\ReXmlSchema.xml.config
            "coll|CReCollection|RecordId",
            "coll|CReCollection|Addr",
            "coll|CReCollection|City",
            "coll|CReCollection|State",
            "coll|CReCollection|Zip",
            "coll|CReCollection|Stat",
            "coll|CReCollection|Type",
            "coll|CReCollection|OccR",
            "coll|CReCollection|Val",
            "coll|CReCollection|MAmt",
            "coll|CReCollection|GrossRentI",
            "coll|CReCollection|MPmt",
            "coll|CReCollection|HExp",
            "coll|CReCollection|NetRentI",
            "coll|CReCollection|NetRentILckd",
            "coll|CReCollection|ReOwnerT",
            "coll|CReCollection|IsSubjectProp",
            "coll|CReCollection|OrderRankValue",
            "coll|CReCollection|IsForceCalcNetRentalI",
            "coll|CReCollection|VerifSigningEmployeeId",
            "coll|CReCollection|VerifSignatureImgId",
            "coll|CReCollection|H4HNumOfJointOwners",
            "coll|CReCollection|IsEmptyCreated",
            "coll|CReCollection|IsPrimaryResidence",

            // C:\LendOSoln\LendersOfficeLib\ObjLib\Schema\PreparerXmlSchema.xml.config
            "coll|CPreparerFields|RecordId",
            "coll|CPreparerFields|PreparerFormT",
            "coll|CPreparerFields|PreparerName",
            "coll|CPreparerFields|Title",
            "coll|CPreparerFields|LicenseNum",
            "coll|CPreparerFields|CaseNum",
            "coll|CPreparerFields|CompanyName",
            "coll|CPreparerFields|StreetAddr",
            "coll|CPreparerFields|City",
            "coll|CPreparerFields|State",
            "coll|CPreparerFields|Zip",
            "coll|CPreparerFields|Phone",
            "coll|CPreparerFields|CellPhone",
            "coll|CPreparerFields|PagerNum",
            "coll|CPreparerFields|FaxNum",
            "coll|CPreparerFields|EmailAddr",
            "coll|CPreparerFields|DepartmentName",
            "coll|CPreparerFields|PrepareDate",
            "coll|CPreparerFields|LicenseNumOfCompany",
            "coll|CPreparerFields|PhoneOfCompany",
            "coll|CPreparerFields|FaxOfCompany",
            "coll|CPreparerFields|IsLocked",
            "coll|CPreparerFields|AgentRoleT",
            "coll|CPreparerFields|TaxId",
            "coll|CPreparerFields|LoanOriginatorIdentifier",
            "coll|CPreparerFields|CompanyLoanOriginatorIdentifier",


            // C:\LendOSoln\LendersOfficeLib\ObjLib\Schema\AgentXmlSchema.xml.config
            "coll|CAgentFields|RecordId",
            "coll|CAgentFields|AgentRoleT",
            "coll|CAgentFields|AgentName",
            "coll|CAgentFields|LicenseNum",
            "coll|CAgentFields|CaseNum",
            "coll|CAgentFields|CompanyName",
            "coll|CAgentFields|StreetAddr",
            "coll|CAgentFields|City",
            "coll|CAgentFields|State",
            "coll|CAgentFields|Zip",
            "coll|CAgentFields|Phone",
            "coll|CAgentFields|CellPhone",
            "coll|CAgentFields|PagerNum",
            "coll|CAgentFields|FaxNum",
            "coll|CAgentFields|EmployeeIDInCompany",
            "coll|CAgentFields|EmailAddr",
            "coll|CAgentFields|Notes",
            "coll|CAgentFields|OtherAgentRoleTDesc",
            "coll|CAgentFields|DepartmentName",
            "coll|CAgentFields|CommissionPointOfLoanAmount",
            "coll|CAgentFields|CommissionPointOfGrossProfit",
            "coll|CAgentFields|CommissionMinBase",
            "coll|CAgentFields|IsCommissionPaid",
            "coll|CAgentFields|PaymentNotes",
            "coll|CAgentFields|InvestorSoldDate",
            "coll|CAgentFields|InvestorBasisPoints",
            "coll|CAgentFields|IsListedInGFEProviderForm",
            "coll|CAgentFields|IsLenderAssociation",
            "coll|CAgentFields|IsLenderAffiliate",
            "coll|CAgentFields|IsLenderRelative",
            "coll|CAgentFields|HasLenderRelationship",
            "coll|CAgentFields|HasLenderAccountLast12Months",
            "coll|CAgentFields|IsUsedRepeatlyByLenderLast12Months",
            "coll|CAgentFields|ProviderItemNumber",
            "coll|CAgentFields|LicenseNumForCompany",
            "coll|CAgentFields|PhoneOfCompanyminOccurs=0",
            "coll|CAgentFields|FaxOfCompany",
            "coll|CAgentFields|IsNotifyWhenLoanStatusChange",
            "coll|CAgentFields|LendingLicenseXmlContent",
            "coll|CAgentFields|CompanyLendingLicenseXmlContent",
            "coll|CAgentFields|TaxId",
            "coll|CAgentFields|LoanOriginatorIdentifier",
            "coll|CAgentFields|CompanyLoanOriginatorIdentifier",
            "coll|CAgentFields|EmployeeId",
            "coll|CAgentFields|BranchName",
            "coll|CAgentFields|PayToBankName",
            "coll|CAgentFields|PayToBankCityState",
            "coll|CAgentFields|PayToABANumber",
            "coll|CAgentFields|PayToAccountNumber",
            "coll|CAgentFields|PayToAccountName",
            "coll|CAgentFields|FurtherCreditToAccountNumber",
            "coll|CAgentFields|FurtherCreditToAccountName",
            "coll|CAgentFields|AgentSourceT",
            "coll|CAgentFields|BrokerLevelAgentID",
     


            #endregion
        };

        public class PageInfo
        {
            public PageInfo(string name, string desc, string id, string url, Type type, IEnumerable<LoanFieldInfo> fields)
            {
                Name = name;
                Desc = desc;
                Id = id;
                Type = type;
                Fields = fields.ToList();
                Url = url;

                //Make sure each field knows it's on this page
                foreach (var field in Fields)
                {
                    field.Pages.Add(this);
                }
            }

            /// <summary>
            /// The name of this page
            /// </summary>
            public string Name { get; private set; }
            /// <summary>
            /// This page's description (currently unused)
            /// </summary>
            public string Desc { get; private set; }
            /// <summary>
            /// This page's pageId
            /// </summary>
            public string Id { get; private set; }
            public Type Type { get; private set; }
            public List<LoanFieldInfo> Fields { get; private set; }
            public string Url { get; private set; }
        }

        public class LoanFieldInfo
        {
            public LoanFieldInfo(Type type, IEnumerable<FormField> info, string name, string friendly, bool canwrite)
            {
                Type = type;
                Name = name;
                FormFieldInfo = info.ToList();
                FriendlyName = friendly;
                Pages = new List<PageInfo>();
                CanWrite = canwrite;
            }

            /// <summary>
            /// The C# Type of the field; note that this is only programmatically defined types, not
            /// necessarily datalayer types like money or ssn.
            /// </summary>
            public Type Type { get; private set; }

            /// <summary>
            /// The FormField information associated with this field.
            /// </summary>
            public List<FormField> FormFieldInfo { get; private set; }

            /// <summary>
            /// The name of this field in the loan data.
            /// </summary>
            public string Name { get; private set; }

            /// <summary>
            /// A list of pages on which this loan field is found
            /// </summary>
            public List<PageInfo> Pages { get; set; }

            /// <summary>
            /// The friendly name of this field
            /// </summary>
            public string FriendlyName { get; private set; }

            /// <summary>
            /// True if this field can be written to; false otherwise.
            /// </summary>
            public bool CanWrite { get; private set; }
        }

        #region Things that are known to potentially be loan field types
        private static readonly HashSet<Type> PotentialLoanFieldTypes = new HashSet<Type>{
            typeof(System.Web.UI.WebControls.TextBox),
            typeof(System.Web.UI.WebControls.CheckBox),
            typeof(MeridianLink.CommonControls.DateTextBox),
            typeof(MeridianLink.CommonControls.MoneyTextBox),
            typeof(MeridianLink.CommonControls.PercentTextBox),
            typeof(MeridianLink.CommonControls.PhoneTextBox),
            typeof(MeridianLink.CommonControls.SSNTextBox),
            typeof(MeridianLink.CommonControls.StateDropDownList),
            typeof(MeridianLink.CommonControls.ZipcodeTextBox),
            typeof(System.Web.UI.HtmlControls.HtmlInputCheckBox),
            typeof(MeridianLink.CommonControls.ComboBox),
            typeof(System.Web.UI.HtmlControls.HtmlInputRadioButton),
            typeof(System.Web.UI.WebControls.RadioButton),
            typeof(System.Web.UI.WebControls.DropDownList),
            typeof(System.Web.UI.WebControls.RadioButtonList),
            typeof(System.Web.UI.WebControls.CheckBoxList),
            typeof(System.Web.UI.HtmlControls.HtmlInputButton)
        };
        #endregion

        #region Things that are known to _not_ be loan field types, so we can force people to categorize things we don't recognize
        private static readonly HashSet<Type> IgnoreLoanFieldTypes = new HashSet<Type> {
            typeof(System.Web.UI.Page),
            typeof(System.Web.UI.HtmlControls.HtmlForm),
            typeof(System.Web.UI.WebControls.Label),
            typeof(System.Web.UI.HtmlControls.HtmlInputHidden),
            typeof(System.Web.UI.HtmlControls.HtmlInputFile),
            typeof(System.Web.UI.WebControls.Button),
            typeof(System.Web.UI.WebControls.Literal),
            typeof(LendersOfficeApp.common.ModalDlg.cModalDlg),
            typeof(System.Web.UI.WebControls.HiddenField),
            typeof(System.Web.UI.HtmlControls.HtmlTableRow),
            typeof(System.Web.UI.WebControls.HyperLink),
            typeof(System.Web.UI.WebControls.PlaceHolder),
            typeof(System.Web.UI.HtmlControls.HtmlAnchor),
            typeof(MeridianLink.CommonControls.CommonDataGrid),
            typeof(System.Web.UI.WebControls.Repeater),
            typeof(System.Web.UI.HtmlControls.HtmlGenericControl),
            typeof(System.Web.UI.WebControls.CustomValidator),
            typeof(LendersOfficeApp.newlos.LoanInfoUserControl),
            typeof(LendersOfficeApp.newlos.UpfrontMIP),
            typeof(LendersOfficeApp.newlos.OtherFinancing),
            typeof(LendersOfficeApp.common.BaseTabPage),
            typeof(LendersOfficeApp.newlos.Status.ContactFieldMapper),
            typeof(System.Web.UI.WebControls.FileUpload),
            typeof(LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn),
            typeof(MeridianLink.CommonControls.NoDoubleClickButton),
            typeof(System.Web.UI.WebControls.DataGrid),
            typeof(System.Web.UI.WebControls.Panel),
            typeof(Microsoft.Web.UI.WebControls.TreeView),
            typeof(Microsoft.Web.UI.WebControls.TabStrip),
            typeof(LendersOfficeApp.newlos.Forms.Loan1003pg1),
            typeof(LendersOfficeApp.newlos.Forms.Loan1003pg2),
            typeof(LendersOfficeApp.newlos.Forms.Loan1003pg3),
            typeof(LendersOfficeApp.newlos.Forms.Loan1003pg4),
            typeof(System.Web.UI.HtmlControls.HtmlImage),
            typeof(System.Web.UI.HtmlControls.HtmlTable),
            typeof(System.Web.UI.WebControls.Image),
            typeof(LendersOfficeApp.newlos.ElectronicDocs.DocTypePickerControl),
            typeof(LendersOfficeApp.newlos.Verifications.VODTab),
            typeof(LendersOfficeApp.newlos.Verifications.VOETab),
            typeof(LendersOfficeApp.newlos.Verifications.VOMTab),
            typeof(LendersOfficeApp.newlos.Verifications.VOLTab),
            typeof(LendersOfficeApp.newlos.Verifications.VOLandTab),
            typeof(System.Web.UI.WebControls.GridView),
            typeof(System.Web.UI.WebControls.RequiredFieldValidator),
            typeof(System.Web.UI.WebControls.RegularExpressionValidator),
            typeof(LendersOfficeApp.los.BrokerAdmin.LendingLicensesPanel),
            typeof(LendersOfficeApp.newlos.PropertyInfoUserControl),
            typeof(LendersOfficeApp.newlos.SubjPropInvestment),
            typeof(LendersOfficeApp.newlos.ElectronicDocs.DocsReceived),
            typeof(System.Web.UI.HtmlControls.HtmlHead),
            typeof(System.Web.UI.WebControls.LinkButton),
            typeof(LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn),
            typeof(LendersOfficeApp.newlos.Forms.MLDSpg1),
            typeof(LendersOfficeApp.newlos.Forms.MLDSpg2),
            typeof(LendersOfficeApp.newlos.Finance.ServicingLineItem),
            typeof(LendersOfficeApp.newlos.ElectronicDocs.DocRequests),
            typeof(LendersOfficeApp.newlos.ElectronicDocs.ShipDocs),
            typeof(LendersOfficeApp.newlos.ElectronicDocs.FaxDocs),
            typeof(LendersOfficeApp.newlos.ElectronicDocs.UploadDocs),
            typeof(MeridianLink.CommonControls.TimeTextBox),
            typeof(LendersOfficeApp.newlos.BorrowerLiabilityFrame),
            typeof(LendersOfficeApp.newlos.Forms.CAMLDSRE885pg1),
            typeof(LendersOfficeApp.newlos.Forms.CAMLDSRE885pg2),
            typeof(LendersOfficeApp.newlos.Forms.CAMLDSRE885pg3),
            typeof(LendersOfficeApp.newlos.Forms.CAMLDSRE885pg4),
            typeof(LendersOfficeApp.newlos.BorrowerInfoTab),
            typeof(LendersOfficeApp.newlos.BorrowerMonthlyIncome),
            typeof(LendersOfficeApp.newlos.BorrowerEmploymentFrame),
            typeof(LendersOfficeApp.newlos.BorrowerAssetFrame),
            typeof(LendersOfficeApp.newlos.BorrowerREOFrame),
            typeof(LendersOfficeApp.newlos.BorrowerHousingExpense),
            typeof(System.Web.UI.HtmlControls.HtmlInputText),
            typeof(LendersOfficeApp.newlos.ElectronicDocs.ShipDocSelectDocument),
            typeof(LendersOfficeApp.newlos.ElectronicDocs.ShipDocReview),
            typeof(LendersOfficeApp.newlos.ElectronicDocs.ShipDocStackingOrder),
        };
        #endregion

        #region Strategies to figure out alternate representations of names
        private static Regex enumNumberRemover = new Regex(@"_\d+$", RegexOptions.Compiled);
        private static List<Func<string, string>> RenamingStrategies = new List<Func<string, string>>()
        {
            //These are in order of frequency, so ideally 
            //Try leaving it alone first
            (name) => name,
            //Sometimes enums have _<digits> at the end
            (name) => enumNumberRemover.Replace(name, ""),
            //Sometimes there's a _rep after it
            (name) => name + "_rep",
            //Sometimes it's Calc
            (name) => name + "Calc",
            //In at least one case it's Unsp instead of Borr
            (name) => name.Replace("Unsp", "Borr"),
            //Sometimes people put a T in there when there isn't one
            (name) => name.TrimEnd('T'),
            //Sometimes things start with s when they need to start with a
            (name) => "a" + name.Substring(1),
            //Sometimes it needs both the a and the _rep
            (name) => "a" + name.Substring(1) + "_rep",

        };
        #endregion

        #region Stuff to make acroynmical data layer names friendlier
        private static readonly Regex InflateCamelCase = new Regex("([A-Z])");
        private static readonly string InflateCamelCaseReplacement = " $1";
        private static readonly string Catch = " {0}( |$)";
        private static readonly string Replace = @" {0} ";

        private static readonly Regex FirstLetterLowerCase = new Regex("^[a-z] ", RegexOptions.Compiled);
        private static readonly Regex CollapseAcronyms = new Regex(@"([A-Z]) ([A-Z])(?![a-z])", RegexOptions.Compiled);
        //This is a list of KVPs because I'm just going to use it to build a list of regexes
        //based on it, so it's not worth having the computer build a dictionary just yet.
        private static List<KeyValuePair<string, string>> Replacements = new List<KeyValuePair<string, string>>() 
        {
            //Some common abbreviations
            new KeyValuePair<string, string>("Nm", "Name"),
            new KeyValuePair<string, string>("Adj", "Adjust"),
            new KeyValuePair<string, string>("Qual", "Qualified"),
            new KeyValuePair<string, string>("L", "Loan"),
            new KeyValuePair<string, string>("Pmt", "Payment"),
            new KeyValuePair<string, string>("Amt", "Amount"),
            new KeyValuePair<string, string>("Desc", "Description"),
            new KeyValuePair<string, string>("N", "Notes"),
            new KeyValuePair<string, string>("Cr", "Credit"),
            new KeyValuePair<string, string>("Src", "Source"),
            new KeyValuePair<string, string>("D", "Date"),
            new KeyValuePair<string, string>("Od", "Ordered Date"),
            new KeyValuePair<string, string>("Rd", "Received Date"),
            new KeyValuePair<string, string>("Chk", "Check"),
            new KeyValuePair<string, string>("Num", "Number"),
            new KeyValuePair<string, string>("Yr", "Year"),
            new KeyValuePair<string, string>("T", "Type"),
            new KeyValuePair<string, string>("Pc", "Percent"),
            new KeyValuePair<string, string>("B", "Borrower"),
            new KeyValuePair<string, string>("C", "Coborrower"),

            //Note that this really can't go in here, as we _must_ do it after the borrower/coborrower
            //replacement - and dictionaries don't really make any guarantees as to their ordering.
            //new KeyValuePair<string, string>("^[a-z] ", ""),
        };

        //Prefabricate all of our regexes, because they're going to be called a lot. These guys
        //are used to turn something like "sCrOd" into "Credit Ordered Date"
        private static Dictionary<string, Regex> Replacers =
        (
            from a in Replacements
            select new
            {
                Regex = new Regex(string.Format(Catch, a.Key), RegexOptions.Compiled),
                Replacement = a.Value
            }
        ).ToDictionary((a) => a.Replacement, (a) => a.Regex);
        #endregion

        //Cache the gigantor list of loan field Name->Type, by name for easy access while we're thinking
        //This is necessary because when you get a loan field name from reflection, its type 
        //is going to be some junk like TextBox or DropDownList because it came from an aspx page, 
        //when what we really want is real type in the data layer. 

        //This has a lot of entries, so iterate with caution
        private static Dictionary<string, System.Reflection.PropertyInfo> AllLoanFieldTypesByName = null;

        static FieldEnumerator()
        {
            
            AllLoanFieldTypesByName = 
            typeof(CAppData).GetProperties
            (
                BindingFlags.Instance |
                BindingFlags.NonPublic |
                BindingFlags.Public
            )
            .Union
            (
                typeof(CPageData).GetProperties
                (
                    BindingFlags.Instance |
                    BindingFlags.NonPublic |
                    BindingFlags.Public
                )
            )
            .FilterTypes()
            .ToDictionary(a => a.Name, StringComparer.InvariantCultureIgnoreCase);

            string filename = Tools.GetServerMapPath("~/newlos/LoanNavigationNew.xml.config");
            XDocument doc = XDocument.Load(filename);

            //This gets filled out when we initialize LoanPages
            LoanFieldsByName = new Dictionary<string, LoanFieldInfo>(StringComparer.InvariantCultureIgnoreCase);

            LoanPages = (from XElement node in doc.Descendants("item")
                         where node.Attribute("pageid") != null
                         //Some of the links in the nav bar don't refer to actual pages, 
                         //(like "duplicate this loan file")
                         //so we can skip them with this regex 
                         where GetPathFromOnClick.IsMatch(node.Attribute("onclick").Value)
                         where !IsExcludedFromFieldEnumerator(node)
                         select node)
                         .UniquePages()
                         .PageInfoFromXml()
                         .Where((a) => a.Fields.Count > 0)
                         .ToDictionary((a) => a.Id);

            //We don't want to carry this around in memory because it's kinda huge, 
            //so clear out this reference to it and let it be GC'd.
            AllLoanFieldTypesByName = null;

            HardcodedFieldToPageMap = InitializeHardcodedFieldToPageMap();

            PreMigrationPageFilters = InitializePreMigrationPageFilters();
        }

        private static bool IsExcludedFromFieldEnumerator(XElement node)
        {
            return string.Equals(bool.TrueString, node.Attribute("skipfieldenumerator")?.Value, StringComparison.OrdinalIgnoreCase);
        }

        private static IEnumerable<XElement> UniquePages(this IEnumerable<XElement> nodes)
        {
            HashSet<string> SeenPages = new HashSet<string>();
            foreach (var node in nodes)
            {
                //A page is unique if its onclick and pageid haven't been seen yet
                string key = node.Attribute("onclick").Value + node.Attribute("pageid").Value;
                if (!SeenPages.Contains(key))
                {
                    SeenPages.Add(key);
                    yield return node;
                }
            }
        }

        /// <summary>
        /// This list of unrecognized fields is filled out by GetLoanFields
        /// </summary>
        private static LinkedList<FieldInfo> UnknownFields = new LinkedList<FieldInfo>();

        /// <summary>
        /// This keeps track of our result, so we don't recalculate it every single time.
        /// </summary>
        private static Dictionary<string, PageInfo> LoanPages = null;
        private static Dictionary<string, LoanFieldInfo> LoanFieldsByName = null;
        private static Dictionary<string, Dictionary<string, string>> HardcodedFieldToPageMap = null;

        /// <summary>
        /// Nested dictionaries that map loan version to fieldId to page name.
        /// Used to filter out pages that field is hidden in until after the associated migration is run.
        /// </summary>
        private static List<FilterGroup> PreMigrationPageFilters = null;

        /// <summary>
        /// Gets a mapping of of loan page id => loan page info.
        /// </summary>
        /// <returns></returns>
        public static IDictionary<string, PageInfo> ListLoanPages()
        {
            return LoanPages;
        }

        /// <summary>
        /// Get a list of page Id & page name pairs
        /// </summary>
        public static IEnumerable<KeyValuePair<string, string>> PageNames
        {
            get
            {
                return from page in LoanPages.OrderBy((item) => item.Value.Name)
                       select new KeyValuePair<string, string>(page.Key, page.Value.Name);
            }
        }

        private static IDictionary<string, IEnumerable<string>> GetFieldsInPages()
        {
            Dictionary<string, IEnumerable<string>> dictionary = new Dictionary<string, IEnumerable<string>>(StringComparer.OrdinalIgnoreCase);
            foreach (var page in LoanPages.Values)
            {
                HashSet<string> fields = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                foreach (var field in page.Fields)
                {
                    fields.Add(field.Name);
                }

                dictionary.Add(page.Id, fields);
            }

            return dictionary;
        }

        public static string GetFieldPageJson()
        {
            return  ObsoleteSerializationHelper.JavascriptJsonSerialize(GetFieldsInPages());
        }

        /// <summary>
        /// Gets a list of loan fields by name.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, LoanFieldInfo> ListLoanFieldsByName()
        {
            return LoanFieldsByName;
        }

        /// <summary>
        /// Will attempt to map the field values in the loan data migration model with the associated pages.
        /// </summary>
        /// <param name="viewModel">The view model to populate.</param>
        public static void PopulateViewModelWithPages(LoanDataMigrationViewModel viewModel)
        {
            PopulateResultViewModelWithPages(viewModel.MigrationResults, viewModel.CurrentVersion);
            PopulateResultViewModelWithPages(viewModel.MigrationAuditHistory, viewModel.CurrentVersion);
        }

        /// <summary>
        /// Will attempt to map the field values in the migration results with the associated pages.
        /// </summary>
        /// <param name="resultList">The list of results to populate.</param>
        private static void PopulateResultViewModelWithPages(List<LoanDataMigrationResultViewModel> resultList, LoanVersionT loanVersion)
        {
            foreach (LoanDataMigrationResultViewModel resultModel in resultList)
            {
                IEnumerable<string> fieldIds = resultModel.GetDependentFieldIds();
                Dictionary<string, Dictionary<string, string>> idsToPages = GetPagesForFieldIds(fieldIds, loanVersion);
                resultModel.SetFieldPageUrls(idsToPages);
            }
        }

        /// <summary>
        /// For each field id in the list, will get the pages that it appears in.
        /// </summary>
        /// <param name="fieldIds">The list of field ids.</param>
        /// <returns>
        /// The inner dictionary maps a page name to the page url.
        /// The outer dictionary maps a field id to the dictionary containing all the pages the field can be found in.
        /// </returns>
        public static Dictionary<string, Dictionary<string, string>> GetPagesForFieldIds(IEnumerable<string> fieldIds, LoanVersionT loanVersion)
        {
            var idsToPages = new Dictionary<string, Dictionary<string, string>>();
            Dictionary<string, HashSet<string>> pageFiltersByFieldId = GetFilters(loanVersion);

            foreach (string fieldId in fieldIds)
            {
                HashSet<string> filterSet;
                if (!pageFiltersByFieldId.TryGetValue(fieldId, out filterSet))
                {
                    filterSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                }

                FieldEnumerator.LoanFieldInfo info = null;
                if (LoanFieldsByName.TryGetValue(fieldId, out info))
                {
                    if (!idsToPages.ContainsKey(fieldId))
                    {
                        idsToPages.Add(fieldId, new Dictionary<string, string>());
                    }

                    foreach (FieldEnumerator.PageInfo page in info.Pages)
                    {
                        if (!idsToPages[fieldId].ContainsKey(page.Name) && !filterSet.Contains(page.Name))
                        {
                            if (page.Name != "Everything Else")
                            {
                                idsToPages[fieldId].Add(page.Name, page.Url.Replace("~", string.Empty));
                            }
                        }
                    }
                }

                Dictionary<string, string> pages;
                if (HardcodedFieldToPageMap.TryGetValue(fieldId, out pages))
                {
                    if (!idsToPages.ContainsKey(fieldId))
                    {
                        idsToPages.Add(fieldId, new Dictionary<string, string>());
                    }

                    foreach (KeyValuePair<string, string> page in pages)
                    {
                        if (!idsToPages[fieldId].ContainsKey(page.Key) && !filterSet.Contains(page.Key))
                        {
                            idsToPages[fieldId].Add(page.Key, page.Value.Replace("~", string.Empty));
                        }
                    }
                }
            }

            return idsToPages;
        }

        private static Dictionary<string, HashSet<string>> GetFilters(LoanVersionT loanVersion)
        {
            Dictionary<string, HashSet<string>> pageFiltersByFieldId = new Dictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);
            foreach (FilterGroup filterGroup in PreMigrationPageFilters)
            {
                if (filterGroup.LoanVersion <= loanVersion)
                {
                    continue;
                }

                foreach (string fieldId in filterGroup.PageMap.Keys)
                {
                    HashSet<string> pages;
                    if (!pageFiltersByFieldId.TryGetValue(fieldId, out pages))
                    {
                        pages = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                        pageFiltersByFieldId.Add(fieldId, pages);
                    }

                    pages.UnionWith(filterGroup.PageMap[fieldId]);
                }
            }

            return pageFiltersByFieldId;
        }

        private static Dictionary<string, Dictionary<string, string>> InitializeHardcodedFieldToPageMap()
        {
            var expensesPages = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "Non P&I Housing Expenses", "~/newlos/Forms/PropHousingExpenses.aspx" }
            };

            var miPages = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "Upfront MIP/FF",  "~/newlos/LoanInfo.aspx?pg=1" }
            };

            var agentPages = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "Agents", "~/newlos/Status/Agents.aspx" }
            };

            var qmPages = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "QM", "~/newlos/Underwriting/QM/QM2015/QM2015.aspx" }
            };

            var hmdaPages = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "HMDA - State Call Report", "~/newlos/Status/HMDA.aspx" },
                { "HMDA LAR Data", "~/newlos/Status/HmdaLar.aspx" }
            };

            var liabilityPages = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                ["Liabilities"] = "~/newlos/BorrowerInfo.aspx?pg=4"
            };

            var constructionArmPages = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "Construction Loan Info", "~/newlos/ConstructionLoanInfo.aspx" }
            };

            return new Dictionary<string, Dictionary<string, string>>(StringComparer.OrdinalIgnoreCase)
            {
                { "sHazardExpense", expensesPages },
                { "sFloodExpense", expensesPages },
                { "sRealEstateTaxExpense", expensesPages },
                { "sSchoolTaxExpense", expensesPages },
                { "sHOADuesExpense", expensesPages },
                { "sGroundRentExpense", expensesPages },
                { "sWindstormExpense", expensesPages },
                { "sCondoHO6Expense", expensesPages },
                { "sOtherTax1Expense", expensesPages },
                { "sOtherTax2Expense", expensesPages },
                { "sOtherTax3Expense", expensesPages },
                { "sOtherTax4Expense", expensesPages },
                { "sLine1008Expense", expensesPages },
                { "sLine1009Expense", expensesPages },
                { "sLine1010Expense", expensesPages },
                { "sLine1011Expense", expensesPages },
                { "sMortgageInsurance", miPages },
                { "sAgents", agentPages },
                { "sSponsoredOriginatorEIN", agentPages },
                { "sQMMaxPointAndFeesAllowedAmt", qmPages },
                { "sQMStatusT", qmPages },
                { "sHmdaDenialReason1", hmdaPages },
                { "sHmdaDenialReason2", hmdaPages },
                { "sHmdaDenialReason3", hmdaPages },
                { "sHmdaDenialReason4", hmdaPages },
                { "sHmdaDenialReasonT1", hmdaPages },
                { "sHmdaDenialReasonT2", hmdaPages },
                { "sHmdaDenialReasonT3", hmdaPages },
                { "sHmdaDenialReasonT4", hmdaPages },
                { nameof(CAppData.aLiaCollection), liabilityPages },
                { "sConstructionRAdj1stCapR", constructionArmPages},
                { "sConstructionRAdj1stCapMon", constructionArmPages},
                { "sConstructionRAdjCapR", constructionArmPages},
                { "sConstructionRAdjCapMon", constructionArmPages},
                { "sConstructionRAdjLifeCapR", constructionArmPages},
                { "sConstructionRAdjMarginR", constructionArmPages},
                { "sConstructionArmIndexNameVstr", constructionArmPages},
                { "sConstructionRAdjIndexR", constructionArmPages},
                { "sConstructionRAdjFloorCalcT", constructionArmPages},
                { "sConstructionRAdjFloorAddR", constructionArmPages},
                { "sConstructionRAdjFloorR", constructionArmPages},
                { "sConstructionRAdjRoundToR", constructionArmPages}
            };
        }

        private class FilterGroup
        {
            public LoanVersionT LoanVersion { get; set; }
            public Dictionary<string, HashSet<string>> PageMap { get; set; }
        }

        private static List<FilterGroup> InitializePreMigrationPageFilters()
        {
            return new List<FilterGroup>()
            {
                new FilterGroup()
                {
                    LoanVersion = LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints,
                    PageMap = new Dictionary<string, HashSet<string>> (StringComparer.OrdinalIgnoreCase)
                    {
                        {
                            nameof(CPageData.sLotImprovC),
                            new HashSet<string>(StringComparer.OrdinalIgnoreCase)
                            {
                                "Construction Loan Info"
                            }
                        }
                    }
                },

                new FilterGroup()
                {
                    LoanVersion = LoanVersionT.V32_ConstructionLoans_CalcChanges,
                    PageMap = new Dictionary<string, HashSet<string>> (StringComparer.OrdinalIgnoreCase)
                    {
                        {
                            nameof(CPageData.sLotAcquiredD),
                            new HashSet<string>(StringComparer.OrdinalIgnoreCase)
                            {
                                "Refi/Construction Loan Page",
                                "Forms 1003 Page 1"
                            }
                        }
                    }
                }
            };
        }

        private static readonly Regex PageNumberOnly = new Regex(@"Page \d", RegexOptions.Compiled);

        /// <summary>
        /// Takes a list of xml &lt;item&gt; nodes from the LoanNavigationNew.xml.config file and 
        /// turns them into a list of PageInfo objects, which contain loan data field information.
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        private static IEnumerable<PageInfo> PageInfoFromXml(this IEnumerable<XElement> nodes)
        {
            List<string> collectionFields = new List<string>(StaticCollectionFields);
            collectionFields.AddRange(LoanFileFieldValidator.GetListOfCustomCollectionFields());

            //Example item:
            //<item name="Monthly Income"  onclick="return load('BorrowerInfo.aspx?pg=3');"  title="Monthly Income" pageid="BorrowerMonthlyIncome" id="Page_BorrowerMonthlyIncome" />
            foreach (var node in nodes)
            {
                //Some tabs have simple names like "Page N", so modify the name in order to make it
                //clear which page they are.
                string name = node.Attribute("name").Value;

                if (PageNumberOnly.IsMatch(name))
                {
                    if (node.Parent.Attribute("name") != null)
                    {
                        name = node.Parent.Attribute("name").Value + " " + name;
                    }
                    else
                    {
                        if (node.Parent.Name == "root")
                        {
                            if (name.StartsWith("1003 Page ", StringComparison.OrdinalIgnoreCase))
                            {
                                name = "Loan " + name;
                            }
                            // no op, it doesn't need a folder name.
                        }
                        else
                        {
                            throw new NotImplementedException("unexpected item from xml");
                        }
                    }

                }

                string pageURL = GetFullUrlFromOnclick(node.Attribute("onclick"));
                string pageId = node.Attribute("pageid").Value;
                List<LoanFieldInfo> fields = new List<LoanFieldInfo>();
                BaseLoanPage pageInstance;
                try
                {
                    pageInstance = (BaseLoanPage)BuildManager.CreateInstanceFromVirtualPath(pageURL, typeof(BaseLoanPage));
                }
                catch (HttpParseException e)
                {
                    Tools.LogError("Field enumerator couldn't enumerate " + pageURL, e);
                    continue;
                }
                catch (HttpException e)
                {
                    Tools.LogError("Field enumerator couldn't find " + pageURL, e);
                    continue;
                }
                Type pageType = pageInstance.GetType();
                //If this page has tabs, there will be a "pg=<number>" somewhere in the query string,
                //which tells the tabs control which tab to jump to.
                //We'll have to examine that tab page for loan fields. This means swapping the type
                //we're currently looking at for the tab's type.           
                int? tabPageP = ParseOnClickForTabIndex(node.Attribute("onclick"));
                if (tabPageP.HasValue)
                {

                    //Get a list of potential tabs - they'll all be member fields, and inherit from
                    //IAutoLoadUserControl.
                    //Then we want to match the names of those tabs to the distance between their names
                    //and the id we're looking at.
                    var Tabs = from FieldInfo a in pageType.GetFields(BindingFlags.Instance | BindingFlags.NonPublic)
                               where typeof(IAutoLoadUserControl).IsAssignableFrom(a.FieldType)
                               select new { Info = a, DistanceToId = CalcEditDistance(a.Name, pageId) };

                    //And then, since we really have no way of getting the tab index
                    //without somehow initializing the page (it's really hard, I tried)
                    //grab the tab whose name most closely matches the pageid
                    var min = (from a in Tabs where a.DistanceToId == Tabs.Min((b) => b.DistanceToId) select a).First();

                    pageType = min.Info.FieldType;

                    //Of course, we also need to remember the tab information in the url.
                    pageURL = pageURL + "?pg=" + tabPageP.Value;
                }

                //Do stuff to get loan file fields from the type
                var pageFields = pageType.GetFields(BindingFlags.Instance |
                                                BindingFlags.Public |
                                                BindingFlags.NonPublic)
                                        .GetLoanFields();

                yield return new PageInfo(name, null, pageId, pageURL, pageType, pageFields);
            }

            //Coda: create fake page, to catch all the loan fields we didn't see before.
            var restFields = from name in AllLoanFieldTypesByName.Keys
                             where !LoanFieldsByName.ContainsKey(name)
                             select GetLoanFieldInfoByName(name);

            yield return new PageInfo("Everything Else", null, "Everything_Else", "Everything_Else", typeof(BasePage), restFields);

            //Even more stuff to add on: create some fake collection pages
            var collections = from CollInfo in
                                  (from parts in
                                       (from delimited in collectionFields
                                        select new { Parts = delimited.Split('|'), Raw = delimited })
                                   select new { CollType = parts.Parts[1], Name = parts.Parts[2], Raw = parts.Raw })
                              group CollInfo by CollInfo.CollType;

            foreach (var collectionType in collections)
            {
                string pageName = collectionType.Key;
                List<LoanFieldInfo> lfs = new List<LoanFieldInfo>();
                foreach (var collectionField in collectionType)
                {
                    FormField field = CreateFormField(collectionField.Raw);
                    field.FriendlyName = FriendifyName(collectionField.Name);
                    field.Description = "A collection field";
                    LoanFieldInfo temp = new LoanFieldInfo(typeof(string),
                        new FormField[] { field },
                        collectionField.Raw,
                        field.FriendlyName,
                        true); //We'll just pretend that collection fields are r/w
                    lfs.Add(temp);
                }

                yield return new PageInfo(pageName, null, pageName, "Collection", typeof(BasePage), lfs);
            }

        }

        /// <summary>
        /// Takes a list of FieldInfo objects and, based on their names, converts them into 
        /// LoanFieldInfo objects by either getting a friendly name and description from the 
        /// database, or creating them based on the field's name.
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        private static IEnumerable<LoanFieldInfo> GetLoanFields(this IEnumerable<FieldInfo> fields)
        {
            foreach (FieldInfo field in fields)
            {
                //Is there a chance that this type in the UI maps to a type in the data layer?
                if (!PotentialLoanFieldTypes.Contains(field.FieldType))
                {
                    //Make sure that it's a type that we know about; make note of it if it isn't
                    if (field.FieldType.IsSubclassOf(typeof(System.Web.UI.Control)) &&
                             !IgnoreLoanFieldTypes.Contains(field.FieldType))
                    {
                        Debug.WriteLine("typeof( " + field.FieldType.FullName + "),");
                    }

                    //And then skip it, we don't care about this one
                    continue;
                }

                //Can we figure out what its name in the datalayer is?
                string realName = GetFieldId(field.Name);
                if (string.IsNullOrEmpty(realName))
                {
                    //Okay, a special case - the 4506T has both borrower and coborrower information in it,
                    //but doesn't specify it in the normal way (with aB or aC). Check for that, and if it 
                    //is the case, we need to put in one thing for aB and one thing for aC.
                    if (field.Name.Contains("4506") || field.Name.Contains("4056")) //Yes, in at least a few cases someone transposed the digits
                    {
                        //We have something that looks like a4506TThirdPartyStreetAddr 
                        //and need to make it look like     aB4506TThirdPartyStreetAddr 
                        //as well as                        aC4506TThirdPartyStreetAddr 
                        //Take off the "a" at the beginning
                        string suffix = field.Name.Substring(1);
                        string aBName = GetFieldId("aB" + suffix);
                        if (!string.IsNullOrEmpty(aBName))
                        {
                            yield return GetLoanFieldInfoByName(aBName);
                        }
                        string aCName = GetFieldId("aC" + suffix);
                        if (!string.IsNullOrEmpty(aCName))
                        {
                            yield return GetLoanFieldInfoByName(aCName);
                        }

                        continue;
                    }


                    //Can't figure out the data layer name, so remember it.
                    UnknownFields.AddLast(field);
                    continue;
                }

                yield return GetLoanFieldInfoByName(realName);
            }
        }

        private static FormField CreateFormField(string id)
        {
            //It turns out that I need to be able to use the FormField's private Id setter...
            //maybe I should just give up on them?
            //(I need to set it because FormField.Create requires that the field be findable in 
            //PageDataUtilities.ContainsField, and some of the fields I'm enumerating aren't. 
            //I could change the way ContainsField behaves, but it's used in a lot of places)

            //Anyway for now I'll just be a bad person and set the id via reflection
            Action<FormField, string> setId = (field, newId) =>
                 typeof(FormField)
                    .InvokeMember(
                    "Id",
                    BindingFlags.Public |
                        BindingFlags.NonPublic |
                        BindingFlags.SetProperty |
                        BindingFlags.Instance,
                    null, field, new object[] { newId });

            FormField temp = new FormField();
            setId(temp, id);
            return temp;
        }

        private static Dictionary<string, Dictionary<string, int>> m_orphanedEnumFields = new Dictionary<string, Dictionary<string, int>>();
        public static Dictionary<string, Dictionary<string, int>> OrphanedEnumFields
        {
            get { return m_orphanedEnumFields; }
        }

        private static ILookup<string, FormField> Forms = FormField.ListAll().ToLookup((a) => a.Id.Split(':')[0]);
        private static Regex StripEnumInfo = new Regex(@"\(.*\)", RegexOptions.Compiled);
        private static LoanFieldInfo GetLoanFieldInfoByName(string name)
        {
            // Most loan fields are represented by a single FormField object, with one exception:
            //enum fields are represented by _multiple_ FormField objects, each with a different
            //name based on what that enumeration value means.
            // Since I want to capture the concept of a single field in the loan, I can't use
            //FormFields directly, and have to wrap them in LoanFieldInfo :(

            if (!AllLoanFieldTypesByName.ContainsKey(name))
            {
                throw new ArgumentException("Field id [" + name + "]is not in the loan field dictionary", "name");
            }

            //Anyway, look up its type
            Type realType = AllLoanFieldTypesByName[name].PropertyType;

            if (Forms.Contains(name))
            {
                //There's an entry in the db for this, so use that information
                if (!LoanFieldsByName.ContainsKey(name))
                {
                    string friendlyName = StripEnumInfo.Replace(Forms[name].First().FriendlyName, "").TrimWhitespaceAndBOM();
                    LoanFieldsByName[name] = new LoanFieldInfo(realType, Forms[name], name, friendlyName, AllLoanFieldTypesByName[name].CanWrite);
                }
                return LoanFieldsByName[name];
            }

            //Otherwise, we'll have to craft the friendly name on our own.
            List<FormField> values = new List<FormField>();
            string friendly = FriendifyName(name);

            //Like I said above, enums are treated differently in FormFields. Set that up here.
            if (realType.IsEnum)
            {
                Dictionary<string, int> enumVals = new Dictionary<string, int>();

                foreach (var val in Enum.GetValues(realType))
                {
                    if (!enumVals.ContainsKey(val.ToString()))
                        enumVals.Add(val.ToString(), (int)val);
                    FormField blank = CreateFormField(name + ":" + (int)val);
                    blank.FriendlyName = friendly + " (" + Enum.GetName(realType, val) + ")";
                    blank.Description = "No description available";
                    values.Add(blank);
                }
                if (!m_orphanedEnumFields.ContainsKey(name))
                    m_orphanedEnumFields.Add(name, enumVals);
            }
            else
            {
                FormField blank = CreateFormField(name);
                blank.FriendlyName = FriendifyName(name);
                blank.Description = "No description available";
                values.Add(blank);
            }
            if (!LoanFieldsByName.ContainsKey(name))
            {
                LoanFieldsByName[name] = new LoanFieldInfo(realType, values, name, friendly, AllLoanFieldTypesByName[name].CanWrite);
            }
            return LoanFieldsByName[name];
        }

        /// <summary>
        /// Given a data layer name in CamelCase that vaguely adheres to our our naming conventions, 
        /// returns a  "friendlier" version of that name with internal spaces and abbreviations 
        /// filled out.
        /// Entries in the database table "CUSTOM_FORM_ListFieldCategory" should be considered
        /// cannonical, and override this.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static string FriendifyName(string name)
        {
            name = InflateCamelCase.Replace(name, InflateCamelCaseReplacement);

            //Remove that unsightly _rep, if it's there
            if (name.EndsWith("_rep"))
            {
                name = name.Substring(0, name.Length - 4);
            }

            //Expand our abbreviations
            foreach (var replacement in Replacers)
            {
                name = replacement.Value.Replace(name, string.Format(Replace, replacement.Key));
            }

            //Because of the way we inflated it, acronyms come out as "G F E"; remove those spaces.
            while (CollapseAcronyms.IsMatch(name)) { name = CollapseAcronyms.Replace(name, "$1$2"); }

            //and sometimes things will start with a lowercase letter, get rid of it if it's there.
            //Note that this has to happen after the borrower/coborrower replacement in Replacers
            if (FirstLetterLowerCase.IsMatch(name))
            {
                name = name.Substring(1);
            }

            return name.TrimWhitespaceAndBOM();
        }

        /// <summary>
        /// Gets the data layer id (in CAppData or CPageData) of a UI field with a given name
        /// </summary>
        /// <param name="inputName">The name of the field to convert</param>
        /// <returns>The name of the field as it is used in the datalayer, or the empty string if it was not found.</returns>
        private static string GetFieldId(string inputName)
        {
            //Go through our list of strategies for figuring this out
            foreach (var strat in RenamingStrategies)
            {
                string tryName = strat(inputName);
                if (AllLoanFieldTypesByName.ContainsKey(tryName))
                {
                    return AllLoanFieldTypesByName[tryName].Name;
                }
            }

            //Otherwise, just return empty
            return "";
        }

        private static readonly Regex GetTabIdxFromOnClick = new Regex(@"\?.*pg=(\d+)", RegexOptions.Compiled);
        private static int? ParseOnClickForTabIndex(XAttribute attr)
        {
            Match m = GetTabIdxFromOnClick.Match(attr.Value);
            if (m.Groups[1].Success)
            {
                return int.Parse(m.Groups[1].Value);
            }

            return null;
        }

        private static readonly Regex GetPathFromOnClick = new Regex(@"('.*\.aspx)(.+)?'", RegexOptions.Compiled);
        private static string GetFullUrlFromOnclick(XAttribute attr)
        {
            //Most onclicks will have something that looks like "'~/path/something.aspx'", so use the regex to extract it.
            Match m = GetPathFromOnClick.Match(attr.Value);
            if (m.Success)
            {
                string path = m.Groups[1].Value.Trim('\'').TrimStart('~');
                //LoadRaw loads it from the root, everything else loads it from ~/newlos
                if (attr.Value.Contains("loadRaw"))
                {
                    path = "~/" + path;
                }
                else
                {
                    path = "~/newlos/" + path;
                }

                return path;
            }

            throw new ArgumentException("Couldn't figure out which page this mapped to: " + attr);
        }

        /// <summary>
        /// Calculates the Levenshtein edit distance between two strings, ignoring case. Zero means that the strings are equivalent. Ignores case.
        /// Runs in N^2 space and time (where N is the length of the larger string) if the strings are not equal, so don't use long strings. 
        /// It could be optimized to run in N space, but that's not worthwhile yet.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        private static int CalcEditDistance(string a, string b)
        {
            //Early exit if either string is null or empty
            //Why did they make strings nullable in C# in the first place?
            //Anyway if one doesn't exist we would have to make n edits,
            //where n is the length of the other string.
            if (string.IsNullOrEmpty(a) || string.IsNullOrEmpty(b))
            {
                return Math.Max((a ?? "").Length, (b ?? "").Length);
            }

            //Ignore case
            a = a.ToLowerInvariant();
            b = b.ToLowerInvariant();

            //Early exit if the strings are equal
            if (a.Equals(b))
            {
                return 0;
            }

            //Make a always be the longer string (not necessary, 
            //but I prefer thinking about it this way)
            if (a.Length > b.Length)
            {
                string temp = a;
                a = b;
                b = temp;
            }

            //The way this algorithm works is by making a graph with two axes; along each axis, 
            //it plots one of the two words. Every step along an axis represents an edit to one
            //of the words (which has a cost), or no edit at that step (which has no cost).
            //Note that this is slightly larger than the strings themselves, because there's a 
            //"neutral" spot at edits[0, 0] before anything happens.
            int iLength = a.Length + 1;
            int jLength = b.Length + 1;
            int[,] edits = new int[iLength, jLength];

            //Initialize our array - this means "replace every letter in a with a letter in b"
            for (int i = 0; i < iLength; i++)
            {
                edits[i, 0] = i;
            }

            //This means "replace every letter in b with a letter in a"
            for (int j = 0; j < jLength; j++)
            {
                edits[0, j] = j;
            }

            //And then fill the edits with all the other possible permutations
            for (int i = 1; i < iLength; i++)
            {
                for (int j = 1; j < jLength; j++)
                {
                    //If the two words are equal at this letter, 
                    //the "cost" at this point comes from the diagonal.
                    //Note that we have to subtract 1 from the string indexes 
                    //because 0,0 is "before" the words start.
                    if (a[i - 1] == b[j - 1])
                    {
                        edits[i, j] = edits[i - 1, j - 1];
                    }
                    else
                    {
                        //If they're not equal, we need to figure out the shortest way to get here
                        //using only the edits available to us.

                        edits[i, j] = Math.Min(Math.Min(//nested mins, gross

                                        edits[i - 1, j] + 1, //take a step along j only - this means "delete whatever character is in b at this step"
                                        edits[i, j - 1] + 1), //take a step along i only - this means "add whatever's in a at this point"
                                        edits[i - 1, j - 1] + 2 //Take a diagonal step - this means "swap the letter in b for the letter in a here"
                            //Note that this is tweaked to make letter swaps more expensive - generally we use
                            //abbreviations (which would be covered by insertion/deletion), instead of changing names.
                            //In particular, this gives better results when presented with VOLand vs VOLandTab or VOLTab
                                      );
                    }
                }
            }

            //And then the edit distance is going to be the element at i,j
            int min = edits[iLength - 1, jLength - 1];

            return min;
        }

        /// <summary>
        /// Checks to make sure that there is full coverage of all the fields in CAppData.
        /// There isn't, but it's pretty close.
        /// </summary>
        /// <param name="toValidate"></param>
        private static void ValidateEntries(ILookup<string, PageInfo> toValidate)
        {
            HashSet<string> RequiredNames = new HashSet<string>(AllLoanFieldTypesByName.Keys);

            foreach (var pageId in toValidate)
            {
                foreach (var page in pageId)
                {
                    foreach (var field in page.Fields)
                    {
                        string temp = field.Name;
                        if (RequiredNames.Contains(temp))
                        {
                            RequiredNames.Remove(temp);
                            //Also remove the representation, if we've removed the field itself
                            RequiredNames.Remove(temp + "_rep");
                        }
                    }
                }
            }
            Debug.WriteLine("");
            Debug.WriteLine("Uncovered names: ");
            foreach (var name in RequiredNames.OrderBy((a) => a))
            {
                Debug.WriteLine(AllLoanFieldTypesByName[name].Name);
            }

            Debug.WriteLine("Guesses: ");
            GuessAtUnknownFields(RequiredNames, UnknownFields);
        }

        /// <summary>
        /// Guesses at the "real" identity of a list of field names. Very expensive with real data, 
        /// though not algorithmically infeasible (think O(n) for n = lots, not O(n^2))
        /// </summary>
        /// <param name="Unknowns"></param>
        /// <param name="LookIn"></param>
        private static void GuessAtUnknownFields(IEnumerable<string> Unknowns, IEnumerable<FieldInfo> LookIn)
        {
            Debug.WriteLine("Going through unknown fields...");

            var guesses = (
                             from name in Unknowns
                             select new
                             {
                                 GuessAt = name,
                                 Guesses =
                                 (
                                      from info in LookIn
                                      select new
                                      {
                                          Info = info,
                                          Leda = CalcEditDistance(info.Name, name)
                                      }
                                  ).GroupBy((a) => a.Leda)
                                  .OrderBy((a) => a.Key)
                                  .First()
                             }
                          )
                          .OrderBy((a) => a.Guesses.Key);

            foreach (var attempt in guesses)
            {
                Debug.WriteLine("Looking for: " + attempt.GuessAt + ", score: " + attempt.Guesses.Key);
                foreach (var guess in attempt.Guesses)
                {
                    Debug.WriteLine(guess.Info.Name + " in " + guess.Info.DeclaringType.FullName);
                }
                Debug.WriteLine("\n");
            }

        }
    }
}