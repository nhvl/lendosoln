﻿namespace LendersOfficeApp.LOAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Services;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.BrokerAdmin.Importers;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.ExcelHelpers;
    using LendersOffice.Security;

    public partial class DataImport : LendersOffice.Admin.SecuredAdminPage
    {
        protected override E_InternalUserPermissions[] GetRequiredPermissions()
        {
            return new E_InternalUserPermissions[] { 
                E_InternalUserPermissions.EditEmployee
            };
        }
        private DataTable m_importData = null;

        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_7_1;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            EnableJquery = true;
            this.EnableJqueryUiLatest = false;
            RegisterJsScript("jquery-ui-1.8.23.min.js");
            RegisterCSS("jquery-ui-1.8.12.custom.css");

            HtmlTable.RenderAction = RenderDataTable;

            Success.Visible = false;
            if (Page.IsPostBack)
            {
                InitialScript.Visible = false;
                LookupResult.Visible = true;
            }
            else
            {
                LookupResult.Visible = false;
                InitialScript.Visible = true;
                ImportManager manager = new ImportManager();
                ImportDestination.DataSource = manager.GetAvailableImporters();
                ImportDestination.DataBind();
                ImportDestination.Items.Insert(0, new ListItem() { Text = "", Value = "" });

                BrokerDDL.Items.Add(new ListItem() { });

                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "INTERNAL_USE_ONLY_ListActiveBrokers", null))
                    {
                        while (reader.Read())
                        {
                            string brokerId = reader["BrokerId"].ToString();
                            string description = reader["BrokerNm"] + " (" + reader["CustomerCode"] + ")";
                            BrokerDDL.Items.Add(new ListItem(description, brokerId));
                        }
                    }
                }
            }
        }

        private void RenderDataTable(HtmlTextWriter sw)
        {
            if (m_importData == null)
            {
                return;
            }

            bool match = m_importData.Columns.Contains("RecordsMatched");

            sw.Write("<table id=\"ExcelData\" >");
            //add header row
            sw.Write("<thead><tr class=\"GridHeader\">");
            for (int i = 0; i < m_importData.Columns.Count; i++)
            {
                sw.Write("<th>{0}</th>", AspxTools.HtmlString(m_importData.Columns[i].ColumnName));
            }
            sw.Write("</tr></thead><tbody>");
            //add rows
            for (int i = 0; i < m_importData.Rows.Count; i++)
            {
                if (match)
                {
                    string cssClass = string.Empty;
                    int recordsMatched = ((int)m_importData.Rows[i]["RecordsMatched"]);
                    if (recordsMatched == 0)
                    {
                        cssClass = "NotFound";
                    }
                    else if (recordsMatched == 1)
                    {
                        cssClass = "MatchExactly";
                    }
                    else if (recordsMatched > 1)
                    {
                        cssClass = "VariousMatches";
                    }

                    sw.Write("<tr class=\"{0} {1}\" data-rownum=\"{2}\">", cssClass, i % 2 == 0 ? "GridItem" : "GridAlternatingItem", i);
                }


                else
                {
                    sw.Write("<tr class=\"{0}\">", i % 2 == 0 ? "GridItem" : "GridAlternatingItem");
                }

                var isNewImport = false;
                for (int j = 0; j < m_importData.Columns.Count; j++)
                {
                    string data = m_importData.Rows[i][j].ToString();
                    if (match && j == 0)
                    {
                        if (data.Equals("0"))
                        {
                            data = "0 - CreateNew";
                            isNewImport = true;
                        }
                        else if (data.Equals("1"))
                        {
                            data = "1 - WillUpdate";
                        }
                        else
                        {
                            data = data + " - WillUpdateMultiple";
                        }
                    }

                    if (isNewImport && data == AvailableImportDestination.NO_CHANGE_INDICATOR)
                    {
                        // OPM 462265.  If it is going to be a new entry, "No change" is misleading.
                        // Show it as blank.
                        data = string.Empty;
                    }

                    sw.Write("<td>{0}</td>", AspxTools.HtmlString(data));

                }
                sw.Write("</tr>");
            }
            sw.Write("</tbody></table>");
        }

        private string GetKey(Guid id)
        {
            return string.Concat(id.ToString("N"), "_", PrincipalFactory.CurrentPrincipal.UserId.ToString("N"));
        }

        protected void UploadFile_OnClick(object sender, EventArgs args)
        {
            if (!ExcelFile.HasFile)
            {
                Response.Redirect(Request.RawUrl);
                return;
            }

            Guid brokerId = new Guid(BrokerDDL.SelectedValue);
            string tempFile = TempFileUtils.NewTempFilePath();
            try
            {
                ImportOverfiew.InnerText = string.Concat("Importing to ", BrokerDDL.SelectedItem.Text, " ", ImportDestination.SelectedItem);
                Guid key = Guid.NewGuid();
                ExcelFile.PostedFile.SaveAs(tempFile);
                try
                {
                    m_importData = ClosedXMLHelper.GenerateDataTable(tempFile, 1);
                }
                catch (DuplicateNameException exc)
                {
                    AddImportError(exc.Message);
                    return;
                }

                AutoExpiredTextCache.AddFileToCache(tempFile, TimeSpan.FromHours(1), GetKey(key));
                FileKey.Value = key.ToString("N");
                ImportManager manager = new ImportManager();
                IEnumerable<string> errors;
                IEnumerable<string> warnings;
                bool isValid = manager.IsValidData(ImportDestination.Text, m_importData, brokerId, out errors, out warnings);

                foreach (String error in errors)
                {
                    Errors.Controls.Add(new HtmlGenericControl("li") { InnerText = "Error: " + error });
                }

                foreach (String warning in warnings)
                {
                    Warnings.Controls.Add(new HtmlGenericControl("li") { InnerText = "Warning: " + warning });
                }
                Warnings.Visible = warnings.Count() != 0;

                if (isValid)
                {
                    manager.MatchData(ImportDestination.Text, brokerId, m_importData, out errors, CurrentPrincipal);
                    if (errors.Any())
                    {
                        foreach (String error in errors)
                        {
                            AddImportError(error);
                        }
                    }
                    else
                    {
                        Errors.Visible = false;
                    }
                }
                else
                {
                    ImportFinalizeVisible.Visible = false;
                    ImportWarning.Visible = true;
                }
            }
            finally
            {
                if (FileOperationHelper.Exists(tempFile))
                {
                    FileOperationHelper.Delete(tempFile);
                }
            }
        }

        private void AddImportError(string errorText)
        {
            Errors.Controls.Add(new HtmlGenericControl("li") { InnerText = errorText });
            ImportFinalizeVisible.Visible = false;
            ImportWarning.Visible = true;
        }



        [WebMethod]
        public static object GetAvailableFields(string id)
        {
            ImportManager manager = new ImportManager();
            return manager.GetAvailableFields(id);
        }


        protected void ImportFinalize_OnClick(object sender, EventArgs args)
        {
            string file = AutoExpiredTextCache.GetFileFromCache(GetKey(new Guid(FileKey.Value)));
            m_importData = ClosedXMLHelper.GenerateDataTable(file, 1);
            ImportManager manager = new ImportManager();
            IEnumerable<string> errors;
            IEnumerable<string> warnings;
            var brokerId = new Guid(BrokerDDL.SelectedValue);
            bool isValid = manager.IsValidData(ImportDestination.Text, m_importData, brokerId, out errors, out warnings);

            if (isValid)
            {
                manager.ImportData(ImportDestination.Text, brokerId, m_importData, out errors, PrincipalFactory.CurrentPrincipal);

                if (errors.Any())
                {
                    Errors.Visible = true;
                    foreach (String error in errors)
                    {
                        Errors.Controls.Add(new HtmlGenericControl("li") { InnerText = error });
                    }
                }
                else
                {
                    Success.Visible = true;
                }
            }

            ImportFinalizeVisible.Visible = false;
        }
    }
}
