﻿<%@ Register TagPrefix="UC" TagName="Header" Src="common/header.ascx" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/common/HeaderNav.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RateMonitorTable.aspx.cs" Inherits="LendersOfficeApp.LOAdmin.RateMonitorTable" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Rate Monitor Table Management</title>
    <script type="text/javascript">
        function toggle_createdBefore() {
            document.getElementById("m_createdBefore").disabled = !document.getElementById("m_createdBeforeEnabled").checked;
        }
        function toggle_createdAfter() {
            document.getElementById("m_createdAfter").disabled = !document.getElementById("m_createdAfterEnabled").checked;
        }
        function checkForSelected() {
            var table = document.getElementById('m_Grid');
            var EChecked = false, EUnchecked = false;
            for (var i = 1; i < table.rows.length - 1 /* Page links are on the last row */; i++) {
                if (table.rows[i].cells[0].children[0].checked) {
                    EChecked = true;
                }
                else {
                    EUnchecked = true;
                }
                if (EChecked && EUnchecked) {
                    break;
                }
            }
            document.getElementById('m_deleteSelected').disabled = !EChecked;
            table.rows[0].cells[0].children[0].checked = !EUnchecked; //header "select all" checkbox
            
        }
        function toggleSelectAll() {
            var table = document.getElementById('m_Grid');
            
            var checked = table.rows[0].cells[0].children[0].checked; //header "select all" checkbox
            for (var i = 1; i < table.rows.length; i++) {
                table.rows[i].cells[0].children[0].checked = checked;
            }
            document.getElementById('m_deleteSelected').disabled = !checked;
        }
        function onLoad() {
        
            toggle_createdAfter();
            toggle_createdBefore();
            
            var table = document.getElementById('m_Grid');
            if(table.rows.length<=1) //no loans found; only header row
            {
                table.rows[0].cells[0].children[0].style.display = "none";
            }
            else
            {
                table.rows[0].cells[0].children[0].style.display = "inline";
            }
        }
    </script>
    <style type="text/css">
        .ErrorMessage
        {
            color:Red;
            visibility:hidden;            
        }
    </style>
</head>
<body onload='onLoad();'>
    <form runat="server">
    <UC:Header runat="server"></UC:Header>
    <UC:HeaderNav runat="server">
        <MenuItem url="~/LOAdmin/main.aspx" label="Main">
        </MenuItem>
        <MenuItem url="~/LOAdmin/RateMonitorTable.aspx" label="Rate Monitor Table Management">
        </MenuItem>
    </UC:HeaderNav>
    
    <%-- 
        Field Entries/Search Controls
    --%>
    <div style="padding-top:5px;">
        <span class="FieldLabel">&nbsp;&nbsp;All active monitors have been run today? </span>
        <label id="m_completedToday" runat='server'></label>
        <asp:LinkButton runat="server" OnClick="UpdateCompletedToday">refresh</asp:LinkButton>
    </div>
    <hr />
    <table cellspacing="2" cellpadding="2">
        <%--Row 0--%>
        <tr>
            <td>
                <span class="FieldLabel">Monitor ID: </span>
            </td>
            <td>
                <asp:TextBox ID="m_monitorIdField" runat="server" Width='50px'></asp:TextBox>
            </td>
            <td>
               <label id='m_monitorIdError' runat='server' class="ErrorMessage">Not a valid Monitor ID</label>
            </td>
            <td style="width: 25%">
                &nbsp;
            </td>
            
            <td>
                <span class="FieldLabel">Monitor Status: </span>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <asp:DropDownList ID="m_monitorStatusField" runat="server">
                    <asp:ListItem Value="All" Selected="True">All</asp:ListItem>
                    <asp:ListItem Value="Enabled">Enabled</asp:ListItem>
                    <asp:ListItem Value="Disabled">Disabled</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <%--Row 1--%>
        <tr>
            <td>
                <span class="FieldLabel">Loan ID: </span> 
            </td>
            <td>
                <asp:TextBox ID="m_loanIdField" runat="server"></asp:TextBox>
            </td>
            <td>
                <label id='m_loanIdError' runat='server' class="ErrorMessage">Not a valid Loan ID</label>
            </td>
            <td style="width: 25%">
                &nbsp;
            </td>
            <td>
                <span class="FieldLabel">Created On or After: </span>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="m_createdAfterEnabled" onclick="toggle_createdAfter()" />
            </td>
            <td>
                <ml:DateTextBox runat="server" ID="m_createdAfter" preset='date' ></ml:DateTextBox>
            </td>
            <td>
            <label id='m_AfterDateError' runat='server' class="ErrorMessage">Not a valid Date</label>
            </td>
        </tr>
        <%--Row 2--%>
        <tr>
            <td>
                <span class="FieldLabel">Pricing Group: </span>
            </td>
            <td>
                <asp:TextBox runat="server" ID="m_pricingGroupField"></asp:TextBox>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="width: 25%">
                &nbsp;
            </td>
            <td>
                <span class="FieldLabel">Created On or Before: </span>
            </td>
            <td>
                <asp:CheckBox runat="server" ID="m_createdBeforeEnabled" onclick="toggle_createdBefore()" />
            </td>
            <td>
                <ml:DateTextBox runat="server" ID="m_createdBefore" preset='date'></ml:DateTextBox>
            </td>
            <td>
            <label id='m_BeforeDateError' runat='server' class="ErrorMessage">Not a valid Date</label>
            </td>
        </tr>
        <%--Row 3--%>
        <tr>
            <td colspan="4">
                <asp:Button ID="m_searchButton" runat='server' Text="Search"/>
                <asp:Button ID="m_deleteSelected" runat="server" Text="Delete Selected" OnClick="DeleteSelected" Enabled="false" />
                <asp:Button ID="m_showAllButton" runat='server' Text="Clear Filters" OnClick="ShowAllMonitors" />
            </td>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>
    </table>
    <hr/>
    
    <%--
        Search Results
    --%>
    <ml:EncodedLabel runat="server" ID="m_pageNumberInfo" style="margin-left:5px;" />
    <ml:CommonDataGrid ID="m_Grid" runat="server" CellPadding="2">
        <Columns>
            <asp:TemplateColumn ItemStyle-Width="1%">
                <HeaderTemplate>
                    <asp:CheckBox runat='server' id="m_checkAll" onclick="toggleSelectAll();"/>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox runat="server" onclick="checkForSelected()" id="Selected" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Monitor ID" SortExpression="Id" ItemStyle-Width="2%">
                <ItemTemplate>
                    <ml:EncodedLabel runat="server" ID="Key" Text='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ID").ToString()) %>'></ml:EncodedLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Is Enabled?" SortExpression="IsEnabled" ItemStyle-Width="2%">
                <ItemTemplate>
                    <ml:EncodedLabel Text='<%# AspxTools.HtmlString(((bool)DataBinder.Eval(Container.DataItem, "IsEnabled") ? "Yes":"No")) %>' runat="server" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Loan ID" SortExpression="sLId" ItemStyle-Width="15%">
                <ItemTemplate>
                    <ml:EncodedLabel Text='<%# AspxTools.HtmlString(Eval("sLId").ToString()) %>' runat="server" ID="sLId" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Rate" SortExpression="Rate" ItemStyle-Wrap="false" ItemStyle-Width="2%">
                <ItemTemplate>
                    <ml:EncodedLabel runat='server' Text='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Rate").ToString()+"%") %>'/>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Point" HeaderText="Points" ItemStyle-Wrap="false" SortExpression="Point" ItemStyle-Width="2%"></asp:BoundColumn>
            <asp:BoundColumn DataField="BPDesc" HeaderText="Pricing Group" SortExpression="BPDesc" ItemStyle-Width="12%"></asp:BoundColumn>
            <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" ItemStyle-Width="5%" />
            <asp:BoundColumn DataField="DisabledDate" HeaderText="Disabled Date" ItemStyle-Width="5%" />
            <asp:BoundColumn DataField="DisabledReason" HeaderText="Disabled Reason" SortExpression="DisabledReason" ItemStyle-Width="12%" />
            <asp:BoundColumn DataField="LastRunDate" HeaderText="Last Run Date" ItemStyle-Width="5%" />
            <asp:BoundColumn DataField="LastRunBatchId" HeaderText="Last Run Batch ID" SortExpression="LastRunBatchId" HeaderStyle-Width="5%" ItemStyle-Width="2%" />
        </Columns>
    </ml:CommonDataGrid>
    </form>

</body>
</html>
