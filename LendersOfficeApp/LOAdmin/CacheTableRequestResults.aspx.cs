using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;

namespace LendersOfficeApp.LOAdmin
{
	/// <summary>
	/// This page displays the results of the CacheTableRequestErrors page.  It is envoked when the user
	/// re-runs any of the previously failed cache table update requests.  
	/// </summary>
	public partial class CacheTableRequestResults : LendersOffice.Admin.SecuredAdminPage
	{
		private int numSuccesses = 0;
		private int numFailures = 0;

	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			ProcessResults();	
		}

		private void ProcessResults()
		{
			numSuccesses = Convert.ToInt32(RequestHelper.GetSafeQueryString("success"));
			numFailures = Convert.ToInt32(RequestHelper.GetSafeQueryString("failure"));

			successLit.Text = "Successes: " + numSuccesses;
			failureLit.Text = "Failures: " + numFailures;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}