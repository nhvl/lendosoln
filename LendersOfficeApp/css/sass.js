"use strict";

const sass = require('node-sass');
const fs = require("fs");

const file = 
    `${__dirname}/Prototype/SASS/LoanEditorTheme.scss`
    ;

const outFile =  `${__dirname}/Prototype/SASS/LoanEditorTheme.css`;

const options = {
  file,
  outFile,
  sourceMap: true,
  // outputStyle: "compressed",
};

function handle(err, result){
    if (err) {
        console.error(error.status); // used to be "code" in v2x and below 
        console.log(error.column);
        console.log(error.message);
        console.log(error.line);
        return;
    }

    fs.writeFile(outFile, result.css, function(err){
        if(!err){
          //file written on disk 
        }
    });
}

sass.
    render
    // renderSync
    (options, handle);