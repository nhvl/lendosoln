'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');


const options = {
	// sourceMap: true,
	outputStyle: "compressed",
};

const src   = './Prototype/SASS/**/*.scss';
const entry = [
	"./Prototype/SASS/LoanEditorTheme.scss",
	"./Prototype/SASS/DarkTheme.scss",
	"./Prototype/SASS/PipelineTheme.scss",
	"./Prototype/SASS/DarkPipelineTheme.scss",
	"./Prototype/SASS/TradesAndPoolsTheme.scss",
	"./Prototype/SASS/DarkTradesAndPoolsTheme.scss",
	];

gulp.task('sass', function () {
  return gulp.src(entry)
  	.pipe(sourcemaps.init())
    .pipe(sass(options).on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./Prototype/SASS/'));
});

gulp.task('sass:watch', function () {
  gulp.watch(src, ['sass']);
});
