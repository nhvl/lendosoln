using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using System.Text;
using LendersOffice.Common;

namespace LendersOfficeApp.test
{
	/// <summary>
	/// Summary description for TestFriendlyIdGenerator.
	/// </summary>
	public partial class TestFriendlyIdGenerator : BasePage
	{
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

		protected void GenerateBtn_Click(object sender, System.EventArgs e)
		{
			if( NumberEd.Text == "" )
				return;

			int num = int.Parse( NumberEd.Text );
			StringBuilder strBuilder = new StringBuilder( num );
			for( int i = 0; i < num; ++i )
			{
				CPmlFIdGenerator gen = new CPmlFIdGenerator();
				
				strBuilder.Append( string.Format( "{0}\n", gen.GenerateNewFriendlyId() ) );
			}

			ListingEd.Text = ListingEd.Text.Insert( 0, strBuilder.ToString() );
		}

		protected void ClearAllBtn_Click(object sender, System.EventArgs e)
		{
			ListingEd.Text = "";
		}

        protected void m_btnHash_Click(object sender, System.EventArgs e)
        {
            m_tbHash.Text = Tools.HashOfPassword(m_tbPlainText.Text);
            m_count.Text = m_tbHash.Text.Length.ToString();
        }
	}
}
