<%@ Page language="c#" Codebehind="TestFriendlyIdGenerator.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.test.TestFriendlyIdGenerator" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>TestFriendlyIdGenerator</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="TestFriendlyIdGenerator" method="post" runat="server">
			<P>
				<asp:TextBox id="NumberEd" runat="server" Width="93px">5</asp:TextBox>&nbsp;
				<asp:Button id="GenerateBtn" runat="server" Text="Generate" onclick="GenerateBtn_Click"></asp:Button>&nbsp;&nbsp;
				<asp:Button id="ClearAllBtn" runat="server" Text="Clear All" onclick="ClearAllBtn_Click"></asp:Button></P>
			<P>
				<asp:TextBox id="ListingEd" runat="server" TextMode="MultiLine" Width="291px" Height="237px"></asp:TextBox></P>
				<hr>
				<table><tr><td>Plain Text</td><td><asp:TextBox id=m_tbPlainText runat="server" Width="554px"></asp:TextBox><asp:Button id=m_btnHash runat="server" Text="Hash" onclick="m_btnHash_Click"></asp:Button></td></tr>
				<tr><td>Hash</td><td><asp:TextBox id=m_tbHash runat="server" Width="554px"></asp:TextBox><ml:EncodedLabel id=m_count runat="server"></ml:EncodedLabel></td></tr>
				</table>
		</form>
	</body>
</HTML>
