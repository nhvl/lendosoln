<%@ Page language="c#" Codebehind="DependencyLookup.aspx.cs" AutoEventWireup="false" Inherits="LendersOffice.Test.DependencyLookup" %>
<%@ Register TagPrefix="UC" TagName="HeaderNav" Src="~/common/HeaderNav.ascx" %>
<%@ Register TagPrefix="UC" TagName="Header" Src="~/LOAdmin/common/header.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<TITLE>DependencyLookup</TITLE>
		<META name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<META name="CODE_LANGUAGE" Content="C#">
		<META name="vs_defaultClientScript" content="JavaScript">
		<META name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../css/stylesheet.css" type=text/css rel=stylesheet >
	</HEAD>
	<BODY MS_POSITIONING="FlowLayout" onload="if( document.getElementById('m_errorMessage') != null ) alert( document.getElementById('m_errorMessage').value );">

    <UC:Header id="m_headerTag" runat="server"></UC:Header>
    <UC:HeaderNav id="HeaderNav" runat="server">
        <MenuItem URL="~/LOAdmin/main.aspx" Label="Main"></MenuItem>
        <MenuItem URL="~/test/dependencylookup.aspx" Label="Loan Field Dependency Lookup"></MenuItem>
    </UC:HeaderNav>

		<FORM id="DependencyLookup" method="post" runat="server">
			<DIV style="BORDER-RIGHT: dimgray 2px solid; PADDING-RIGHT: 16px; BORDER-TOP: dimgray 2px solid; PADDING-LEFT: 16px; BACKGROUND: whitesmoke; PADDING-BOTTOM: 16px; MARGIN: 24px; FONT: 16px arial; BORDER-LEFT: dimgray 2px solid; PADDING-TOP: 16px; BORDER-BOTTOM: dimgray 2px solid">
				DependsOn calculator: <INPUT id="m_DFieldList" type="text" style="WIDTH: 300px">
				<INPUT id="m_DSubmit" type="button" value="For LendersOffice" onclick="self.location = 'DependencyLookup.aspx?whatToDo=DependsOn&amp;fieldList=' + encodeURIComponent(document.getElementById('m_DFieldList').value);">
				<INPUT id="m_DSubmit4PML" type="button" value="For PML" onclick="self.location = 'DependencyLookup.aspx?whatToDo=PmlDependsOn&amp;fieldList=' + encodeURIComponent(document.getElementById('m_DFieldList').value);">
				<SPAN style="FONT: 12px arial; COLOR: gray">( Enter comma separated list of fields
					) </SPAN>
			</DIV>
			<DIV style="BORDER-RIGHT: dimgray 2px solid; PADDING-RIGHT: 16px; BORDER-TOP: dimgray 2px solid; PADDING-LEFT: 16px; BACKGROUND: whitesmoke; PADDING-BOTTOM: 16px; MARGIN: 24px; FONT: 16px arial; BORDER-LEFT: dimgray 2px solid; PADDING-TOP: 16px; BORDER-BOTTOM: dimgray 2px solid">
				Affecting calculator: <INPUT id="m_AFieldList" type="text" style="WIDTH: 300px">
				<INPUT id="m_ASubmit" type="button" value="For LendersOffice" onclick="self.location = 'DependencyLookup.aspx?whatToDo=Affecting&amp;fieldList=' + encodeURIComponent(document.getElementById('m_AFieldList').value);">
				<INPUT id="m_ASubmit4PML" type="button" value="For PML" onclick="self.location = 'DependencyLookup.aspx?whatToDo=PmlAffecting&amp;fieldList=' + document.getElementById('m_AFieldList').value;">
				<INPUT id="m_ASubmit4Keyword" type="button" value="For Keywords" onclick="self.location = 'DependencyLookup.aspx?whatToDo=AffectingKeywords&amp;fieldList=' + encodeURIComponent(document.getElementById('m_AFieldList').value);">
				<SPAN style="FONT: 12px arial; COLOR: gray">( Enter comma separated list of fields
					) </SPAN>
			</DIV>
		</FORM>
	</BODY>
</HTML>
