<%@ Page language="c#" Codebehind="CachedData.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.test.CachedData" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>CachedData</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="CachedData" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 696px; HEIGHT: 285px" cellSpacing="1" cellPadding="1" width="696" border="1">
				<TR>
					<TD style="WIDTH: 99px"><STRONG>Field Name</STRONG></TD>
					<TD><STRONG>Field Description</STRONG></TD>
					<TD><STRONG>Dependency?</STRONG></TD>
					<TD><STRONG>Value</STRONG></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">LOAN FIELDS</TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px" id="sStatusT">sStatusT</TD>
					<TD>Loan Status</TD>
					<TD>other event dates</TD>
					<TD>
						<asp:TextBox id="sStatusT" runat="server" DESIGNTIMEDRAGDROP="90"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">sStatusLckd</TD>
					<TD>loan status lock</TD>
					<TD></TD>
					<TD>
						<asp:CheckBox id="sStatusLckd" runat="server" Text="Locked"></asp:CheckBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">sClosedD</TD>
					<TD></TD>
					<TD></TD>
					<TD>
						<asp:TextBox id="sClosedD" runat="server" DESIGNTIMEDRAGDROP="103"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">sRejectD</TD>
					<TD></TD>
					<TD></TD>
					<TD>
						<asp:TextBox id="sRejectD" runat="server"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">sLAmt</TD>
					<TD>Loan Amount</TD>
					<TD>No</TD>
					<TD>
						<asp:TextBox id="sLAmt" runat="server" DESIGNTIMEDRAGDROP="34"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">sEmployeeLoanRepName</TD>
					<TD>Name of loan officer</TD>
					<TD>
						<P>Used to be, cached now</P>
					</TD>
					<TD>
						<asp:TextBox id="sEmployeeLoanRepName" runat="server" DESIGNTIMEDRAGDROP="36"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">sPrimBorrowerFullNm</TD>
					<TD>Primary Borrower Full Name</TD>
					<TD></TD>
					<TD>
						<asp:TextBox id="sPrimBorrowerFullNm" runat="server" DESIGNTIMEDRAGDROP="91"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">sCreditScoreType2</TD>
					<TD></TD>
					<TD></TD>
					<TD>
						<asp:TextBox id="sCreditScoreType2" runat="server"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px"></TD>
					<TD></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">APP FIELDS</TD>
					<TD colSpan="3"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">aBNm</TD>
					<TD>borrower full name</TD>
					<TD>aBFirstNm, aBMidNm, aBLastNm, aBSuffix</TD>
					<TD>
						<asp:TextBox id="aBNm" runat="server" DESIGNTIMEDRAGDROP="59"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">aBFirstNm</TD>
					<TD>borrower first name</TD>
					<TD>no</TD>
					<TD>
						<asp:TextBox id="aBFirstNm" runat="server"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">aBMidNm</TD>
					<TD>borrower mid name</TD>
					<TD>no</TD>
					<TD>
						<asp:TextBox id="aBMidNm" runat="server"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">aBLastNm
					</TD>
					<TD>borrower last name</TD>
					<TD>no</TD>
					<TD>
						<asp:TextBox id="aBLastNm" runat="server" DESIGNTIMEDRAGDROP="66"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">aBSuffix</TD>
					<TD>borrower suffix</TD>
					<TD>no</TD>
					<TD>
						<asp:TextBox id="aBSuffix" runat="server" DESIGNTIMEDRAGDROP="127"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 99px">aBExperianScore</TD>
					<TD></TD>
					<TD>no</TD>
					<TD>
						<asp:TextBox id="aBExperianScore" runat="server"></asp:TextBox></TD>
				</TR>
			</TABLE>
			<asp:Button id="Button1" runat="server" Text="Save" onclick="Button1_Click"></asp:Button><asp:Button id=LookupField runat="server" Text="Lookup" onclick="LookupField_Click"></asp:Button><asp:TextBox id=FieldId runat="server"></asp:TextBox>
			<hr>
			<asp:TextBox id="graphDump" runat="server" Rows="10" TextMode="MultiLine" Width="7in"></asp:TextBox>
			<hr>
		</form>
	</body>
</HTML>
