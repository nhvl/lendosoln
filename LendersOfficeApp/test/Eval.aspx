<%@ Page language="c#" Codebehind="Eval.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.test.EvalForm" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Eval</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="EvalForm" method="post" runat="server">
			<P>
				<asp:TextBox id="CondEd" runat="server" Width="652px" Height="102px" TextMode="MultiLine"></asp:TextBox></P>
			<P align="left">
				<asp:Button id="EvalBtn" runat="server" Text="Eval Now" onclick="EvalBtn_Click"></asp:Button></P>
			<P align="left">RESULT:
				<asp:TextBox id="ResEd" runat="server"></asp:TextBox></P>
			<P align="left">ERROR:
				<asp:TextBox id="ErrEd" runat="server"></asp:TextBox></P>
		</form>
	</body>
</HTML>
