using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.Caching;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.QueryProcessor;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.test
{
	/// <summary>
	/// Summary description for CachedData.
	/// </summary>
	public partial class CachedData : MinimalPage
	{

        private Guid m_BrokerID
        {
            // Access member.

            get
            {
                // Lookup current principal.

                BrokerUserPrincipal broker = Thread.CurrentPrincipal as BrokerUserPrincipal;

                if( broker == null )
                {
                    return Guid.Empty;
                }

                return broker.BrokerId;
            }
        }

		private void LoadLoan()
		{
			CCalcCacheData loan = new CCalcCacheData( new Guid( "6364f55a-6fe8-4b06-8644-9daf9ece8f1b" ) ); 
			loan.InitLoad();
			
			CAppBase app = loan.GetAppData(0);

			sEmployeeLoanRepName.Text = loan.sEmployeeLoanRep.FullName;
			sStatusT.Text = loan.sStatusT.ToString();
			aBNm.Text = app.aBNm;
			aBLastNm.Text = app.aBLastNm;
			aBMidNm.Text = app.aBMidNm;
			aBFirstNm.Text = app.aBFirstNm;
			aBSuffix.Text = app.aBSuffix;
			sPrimBorrowerFullNm.Text = loan.sPrimBorrowerFullNm;
			sClosedD.Text = loan.sClosedD_rep;
			sRejectD.Text = loan.sRejectD_rep;
			sCreditScoreType2.Text = loan.sCreditScoreType2_rep;
			aBExperianScore.Text = app.aBExperianScore_rep;
			sStatusLckd.Checked = loan.sStatusLckd;
			sLAmt.Text = loan.sLAmtCalc_rep;
		}
		protected void PageLoad(object sender, System.EventArgs e)
		{
			if( !IsPostBack )
            {
                // Get the loan.

				LoadLoan();

    			// Put user code to initialize the page here.

                CFieldInfoTable cache = CFieldInfoTable.GetInstance();
                String          dump  = "";

                dump += "# complete sets";

                dump += "\r\nDL:";

                foreach( var item in cache.CachingSet.DbFields.GetFieldSpecList() )
                {
                    dump += " " + item.Name;
                }

                dump += "\r\nCL:";

                foreach( var item in cache.CachingSet.UpdateFields.GetFieldSpecList() )
                {
                    dump += " " + item.Name;
                }

                dump += "\r\n\r\n";

                graphDump.Text = dump;
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

		protected void Button1_Click(object sender, System.EventArgs e)
		{

		}

        protected void LookupField_Click( object sender , System.EventArgs e )
        {
            // Get the specified field id and calculate the caching
            // result using our graph of data.

            CFieldInfoTable cache = CFieldInfoTable.GetInstance();

            if( FieldId.Text != "" )
            {
                List<string> strset = new List<string>();

                strset.Add( FieldId.Text );

                UpdateResult result = cache.GatherAffectedCacheFields( strset );

                if( result != null )
                {
                    string dump = "";

                    dump += "\r\n>> " + FieldId.Text + "...";
                    dump += "\r\nDL:";

                    foreach( var item in result.DbFields.GetFieldSpecList() )
                    {
                        dump += " " + item.Name;
                    }

                    dump += "\r\nCL:";

                    foreach( var item in result.UpdateFields.GetFieldSpecList() )
                    {
                        dump += " " + item.Name;
                    }

                    dump += "\r\n\r\n";

                    graphDump.Text += dump;
                }
            }
        }
	}
}
