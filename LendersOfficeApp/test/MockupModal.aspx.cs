﻿namespace LendersOfficeApp
{
    using System;
    using LendersOffice.Common;

    /// <summary>
    /// TestModal class.
    /// </summary>    
    public partial class MockupModal : LendersOffice.Common.BasePage
    {
        /// <summary>
        /// TestModal Page Load.
        /// </summary>
        /// <param name="sender">ModalTest Page Load sender.</param>
        /// <param name="e">ModalTest Page Load event.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            sizePanel.Width = RequestHelper.GetInt("w", 800);
            sizePanel.Height = RequestHelper.GetInt("h", 400);
        }
    }
}