<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModalTest.aspx.cs" Inherits="LendersOfficeApp.ModalTest" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Security" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Modal Test</title>
</head>
<body style="height:100%;">
<form runat="server"></form>
<script type="module" src="../inc/test.ModalTest.js"></script>
<script nomodule src="../inc/test.ModalTest.js"></script>
</script>
</body>
</html>

