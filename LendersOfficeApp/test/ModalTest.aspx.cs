﻿namespace LendersOfficeApp
{
    using System;

    /// <summary>
    /// ModalTest class.
    /// </summary>
    public partial class ModalTest : LendersOffice.Common.BaseServicePage
    {
        /// <summary>
        /// ModalTest Page Load.
        /// </summary>
        /// <param name="sender">ModalTest Page Load sender.</param>
        /// <param name="e">ModalTest Page Load event.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
        }
    }
}
