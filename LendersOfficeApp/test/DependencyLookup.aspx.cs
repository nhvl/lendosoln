using System;
using System.Collections;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using DetectLpeKeywordDependsOn = LendersOfficeApp.los.RatePrice.DetectLpeKeywordDependsOn;

namespace LendersOffice.Test
{
	public partial class DependencyLookup : LendersOffice.Admin.SecuredAdminPage
	{
		protected System.Web.UI.WebControls.TextBox m_FieldList;
		protected System.Web.UI.WebControls.Button m_Submit;
	
		public String ErrorMessage
		{
			// Access member.

			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value );
			}
		}

		/// <summary>
		/// Initialize page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			try
			{
				// Load up the field classes.  Each type should
				// have depends on attributes embedded within.

				DependencyGraph dg = new DependencyGraph();

                string whatToDo = RequestHelper.GetSafeQueryString( "whatToDo" );

				if( RequestHelper.GetSafeQueryString( "fieldList" ) == null || whatToDo == null )
					return;

                string[] fieldList = RequestHelper.GetSafeQueryString( "fieldList" ).Split( ',' );

                if( whatToDo == "DependsOn" || whatToDo == "PmlDependsOn")
                {
                    KindOfDependsOn kindOfDependsOn = whatToDo == "DependsOn" ? KindOfDependsOn.LendersOffice : KindOfDependsOn.PML;
                    dg.InitializeGraph( kindOfDependsOn, typeof( CPageBase ) , typeof( CAppData ) );

                    // Lookup the specified fields and generate an
                    // export document for the set.

                    DependsOnExport exports = new DependsOnExport();
                    exports.AppName = kindOfDependsOn.ToString();

                    foreach( String name in fieldList )
                    {
                        FieldNode node = dg[ name.TrimWhitespaceAndBOM() ];

                        if( node == null )
                        {
                            continue;
                        }

                        ArrayList names = new ArrayList();
                        Field     field = new Field();

                        field.Adopt( node , names );

                        exports.Add( field );
                    }

                    Response.Output.Write( exports );
                }
                else if( whatToDo == "Affecting" || whatToDo == "PmlAffecting")
                {
                    KindOfDependsOn kindOfDependsOn = whatToDo == "Affecting" ? KindOfDependsOn.LendersOffice : KindOfDependsOn.PML;
                    dg.InitializeGraph( kindOfDependsOn, typeof( CPageBase ) , typeof( CAppData ) );

                    // Lookup the specified fields and generate an
                    // export document for the set.

                    AffectingExport exports = new AffectingExport();
                    exports.AppName  = kindOfDependsOn.ToString();  

                    foreach( String name in fieldList )
                    {
                        FieldNode node = dg[ name.TrimWhitespaceAndBOM() ];

                        if( node == null )
                        {
                            continue;
                        }

                        ArrayList names = new ArrayList();
                        Field     field = new Field();

                        field.Reach( node , names );

                        exports.Add( field );
                    }

                    Response.Output.Write( exports );
                }
                else if( whatToDo == "AffectingKeywords" )
                {                    
                    DetectLpeKeywordDependsOn affectKeywords = new DetectLpeKeywordDependsOn( KindOfDependsOn.PML, fieldList);
                    XmlDocument doc = affectKeywords.ToXmlDocument();
                    Response.Output.Write( doc.InnerXml );
                }

				Response.AddHeader( "Content-Disposition" , "attachment; filename=\"DependsOn.xml\"" );
				Response.ContentType = "text/xml";

                Response.Flush();
                Response.End();
			}
			catch( Exception e )
			{
				if( e is System.Threading.ThreadAbortException == false )
				{
					ErrorMessage = e.Message;
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
			notOnlySAE();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

	}


}
