<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="LoginUserControl.ascx.cs" Inherits="LendersOfficeApp.LoginUserControl" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<style>
    .message-container {
        border-right: black 1px solid;
        border-top: black 1px solid;
        font-weight: bold;
        font-size: 14px;
        border-left: black 1px solid;
        width: 300px;
        color: red;
        border-bottom: black 1px solid;
        background-color: gainsboro;
    }
    .table-login {
        margin: 0 auto; 
        text-align: left; 
        width: 378px;
    }
    th {
        font-size: 11px;
        line-height: 16px;
        padding-right: 20px;
        padding-bottom: 4px;
        color: #666666;
        font-weight: bold;
    }
    .customer-code th {
        padding-right: 10px;
    }
    a.forgot-password {
        padding-left: 0;
    }
    #loginHelp {
        padding-left: 4px;
    }
    input[disabled] {
        background-color: gainsboro;
    }
    .capslock-on {
        color: red;
        display: none;
    }
    .capslock-on.pass-focus.is-capslock {
        display: table-row;
    }
    .form-control-input {
        width: 144px;
        padding-left: 4px;
        border: 1px solid #707070;
    }
    .form-control-input:focus {
        outline: 0;
    }
    .w-label-login {
        width: 82px;
    }
    .text-left {
        text-align: left;
    }
    p {
        margin-top: 0;
    }
</style>



<table border="0" cellpadding="0" cellspacing="0" class="table-login">
    <tbody>
        <tr id="loginTypeSection">
            <th class="w-label-login text-left">Login Type:</th>
            <td style="padding-right: 50px">
                <label><input type="radio" id="m_sLoginType_Normal" value="True" name="m_sLoginType" onclick="f_LoginTypeChanged(false);" runat="server"> Normal</label>
                <label><input type="radio" id="m_sLoginType_AD" value="False" name="m_sLoginType" onclick="f_LoginTypeChanged(true);" runat="server"> Active Directory</label>
            </td>
        </tr>

        <tr>
            <td><br /></td>
            <td></td>
        </tr>

        <tr>
            <th class="w-label-login text-left">Login:</th>
            <td style="padding-right: 50px">
                <asp:TextBox ID="m_UserName" runat="server" autocomplete="off" class="form-control-input" autofocus />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" Display="Dynamic" ControlToValidate="m_UserName" />
            </td>
        </tr>
        <tr>
            <th class="w-label-login text-left">Password:</th>
            <td>
                <asp:TextBox ID="m_Password" runat="server" TextMode="Password" autocomplete="off" class="form-control-input" />
                <% if (!IsInternalLogin) { %>
                   &nbsp;
                   <a href="PasswordReset/RequestPasswordReset.aspx" class="forgot-password">Forgot your password?</a>
                <% } %>
            </td>
        </tr>
        <tr class="customer-code">
            <th class="w-label-login text-left">Customer Code:</th>
            <td>
                <asp:TextBox ID="m_CustomerCode" runat="server" autocomplete="off" class="form-control-input" />
            </td>
        </tr>

        <tr class="capslock-on">
            <td></td>
            <td>Caps lock is turned ON</td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td colspan="2" align="center">
                <div style="padding-bottom: 4px; padding-top: 4px; height: 20px;">
                    <% if (IsInternalLogin) { %>
                       &nbsp;&nbsp;&nbsp;
                    <% } %>
                    <ml:EncodedLabel Style="padding-right: 15px" ID="m_ErrMsg" runat="server" ForeColor="Red" />
                    <br>
                    <ml:EncodedLabel Style="padding-right: 5px" ID="m_ErrMsg2" runat="server" ForeColor="Red" />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <div id="WarningMessageContainer" runat="server" class="message-container" Visible="false">
                    <span runat="server" id="WarningMessageSpan"></span>
                    <a id="UpgradeIeLink" runat="server"
                       href="https://support.microsoft.com/en-us/help/17621/internet-explorer-downloads">Please use Internet Explorer 11 to continue accessing LendingQB.</a>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="m_login" runat="server" Text="Login" OnClick="onLoginClick" />&nbsp;
                <a href="https://support.lendingqb.com/index.php?//Knowledgebase/Article/View/185" id="loginHelp" target="_blank">Login help</a>
            </td>
        </tr>
        <tr runat="server" id="TrustedSiteRow">
            <td colspan="2">
                <br />
                <div class="IE7Warning">
                    Please
                    <asp:HyperLink ID="TrustedSitesLink" runat="server" ToolTip="Click here for help" Target="_blank">add LendingQB to the Trusted Sites zone</asp:HyperLink>
                    for better compatibility.
                </div>
            </td>
        </tr>
    </tbody>
</table>

<script>
    var bIsPageValid = false;
    function _init() {
        <%-- This login page will remove itself from frame, if someone embed this page inside a frame. --%>
        if (top.location != null && top.location != window.location)
            top.location.href = window.location.href;

        var m_UserName = <%= AspxTools.JsGetElementById(m_UserName) %>;
        var m_Password = <%= AspxTools.JsGetElementById(m_Password) %>;
        
        if (m_UserName != null) m_UserName.focus();
        
        var bHideADFields = <%= AspxTools.JsBool(IsInternalLogin || IsThirdPartyReferrer) %>;

        bIsPageValid = true;
        if (!bIsPageValid) f_disableLogin();

        f_InitCustomerCode();

        if (bHideADFields) f_hideADFields();
        
        document.msCapsLockWarningOff = true;
        
        var capslockOnSection = $(".capslock-on");
        $(document).on("keydown click", function(event) {
            if (event.originalEvent.getModifierState == null) return;
            var capslock = event.originalEvent.getModifierState("CapsLock");
            capslockOnSection.toggleClass("is-capslock", capslock);
        });
        $(document).on("focus blur", "input[type='password']", null, function(event){
            capslockOnSection.toggleClass("pass-focus", event.type === "focusin");
        });
    }

    function _initFrameAllow() {
        var bHideADFields = false;
        bIsPageValid = true;

        if (ML && ML.isInternalLogin) {
            bHideADFields = true;
        }

        if (!bIsPageValid)
            f_disableLogin();

        f_InitCustomerCode();

        if (bHideADFields) {
            f_hideADFields();
        }

        var oUserName = <%= AspxTools.JsGetElementById(m_UserName) %>;
        if (null != oUserName)
            oUserName.focus();
    }

    function f_disableLogin() {
        <%= AspxTools.JsGetElementById(m_ErrMsg) %>.style.display = "none";
        <%= AspxTools.JsGetElementById(m_login) %>.style.display = "none";

        <%= AspxTools.JsGetElementById(m_UserName) %>.value = "";
        <%= AspxTools.JsGetElementById(m_UserName) %>.disabled = true;

        <%= AspxTools.JsGetElementById(m_Password) %>.value = "";
        <%= AspxTools.JsGetElementById(m_Password) %>.disabled = true;

        <%= AspxTools.JsGetElementById(m_CustomerCode) %>.value = "";
        <%= AspxTools.JsGetElementById(m_CustomerCode) %>.disabled = true;

        document.getElementById("loginHelp").style.display = "none";

        f_hideADFields();
    }

    function f_hideADFields() {
        f_toggleCCode(false);

        document.getElementById("loginTypeSection").style.display = "none";
    }

    function f_toggleCCode(bShouldDisplay) {
        var oCustomerCode = <%= AspxTools.JsGetElementById(m_CustomerCode) %>;
        if (null != oCustomerCode) {
            $(".customer-code").css({display: (bShouldDisplay) ? "" : "none"});
        }
    }

    function f_InitCustomerCode() {
        var oLoginType_AD = <%= AspxTools.JsGetElementById(m_sLoginType_AD) %>;
        if (null != oLoginType_AD) {
            f_LoginTypeChanged(oLoginType_AD.checked);
        }
    }

    function f_LoginTypeChanged(bIsADLogin) {
        var bShouldDisplayCCode = bIsADLogin && (<%= AspxTools.JsBool(!IsInternalLogin) %>);
        
        f_toggleCCode(bShouldDisplayCCode);
    }
</script>

