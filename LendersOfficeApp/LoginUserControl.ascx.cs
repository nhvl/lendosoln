using System;
using System.Web;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using System.Text.RegularExpressions;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOfficeApp
{
    public partial class LoginUserControl : System.Web.UI.UserControl
    {
        public bool IsInternalLogin { get; set; }

        public bool IsThirdPartyReferrer
        {
            get
            {
                var referrer = RequestHelper.GetReferrer();
                return referrer != null;
            }
        }

        /// <summary>
        /// dd 1/28/2016 - Temporary way to allow login from other browser.
        /// </summary>
        private bool IsAllowCrossBrowser
        {
            get
            {
                return RequestHelper.GetSafeQueryString("crossbrowser") == "true";
            }
        }

        public string GetLoginButtonClientID()
        {
            return m_login.ClientID;
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            (Page as BasePage).RegisterJsGlobalVariables("isInternalLogin", this.IsInternalLogin);
            if (WebsiteUtilities.IsDatabaseOffline)
            {
                m_login.Enabled = false;
                m_UserName.ReadOnly = true;
                m_Password.ReadOnly = true;
                m_CustomerCode.ReadOnly = true;
                return;
            }

            if (!string.IsNullOrEmpty(ConstStage.SupportRedirectionCookie))
            {
                RequestHelper.StoreToCookieInsecure(LoginUserControl.SupportRedirectionCookieName, ConstStage.SupportRedirectionCookie, DateTime.Now.AddYears(1), true);
            }

            TrustedSiteRow.Visible = false;

            if (!IsPostBack)
            {
                if (IsAllowCrossBrowser)
                {
                    SetDisabledIECheck();
                }
                else if (!IsThirdPartyReferrer && !IsInternalLogin)
                {
                    TrustedSitesLink.NavigateUrl = ConstApp.BrowserTrustedSitesLink;
                    TrustedSiteRow.Visible = true;
                }

                m_sLoginType_Normal.Checked = !ADLoginType;
                m_sLoginType_AD.Checked = ADLoginType;
                string customerCode = RequestHelper.GetSafeQueryString("cc");
                if (!string.IsNullOrEmpty(customerCode) &&  Regex.IsMatch(customerCode, "^[a-zA-Z0-9]+$"))
                {
                    m_sLoginType_AD.Checked = true;
                    m_sLoginType_Normal.Checked = false;
                    m_CustomerCode.Text = customerCode.Trim();
                }
            }
        }

        private bool ADLoginType
        {
            get
            {
                HttpCookie cookie = Request.Cookies["LoginType"];
                return cookie != null && cookie.Value.Equals("AD", StringComparison.OrdinalIgnoreCase);
            }
        }

        public void ClearErrorMessages()
        {
            m_ErrMsg.Text = "";
            m_ErrMsg2.Text = "";
        }

        public void SetErrorMessage(string text)
        {
            m_ErrMsg.Text = text;
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion

        internal AbstractUserPrincipal GetInternalPrincipal()
        {
            RequestHelper.ClearAuthenticationCookies();
            PrincipalFactory.E_LoginProblem loginProblem;
            AbstractUserPrincipal principal = InternalUserPrincipal.CreatePrincipalWithFailureType(m_UserName.Text, m_Password.Text, out loginProblem, LoginSource.Website);

            if (null != principal)
            {
                RequestHelper.InsertAuditRecord(principal, Guid.Empty);
            }
            else
            {
                DisplayErrorMessage(loginProblem);
            }

            return principal;
        }

        public const string SupportCookieName = "DisplaySupportConfirm";
        public const string SupportRedirectionCookieName = "SupportRedirection";

        private void DisplayErrorMessage(PrincipalFactory.E_LoginProblem loginProblem)
        {
            switch(loginProblem)
            {
                case PrincipalFactory.E_LoginProblem.IsDisabled:
                    m_ErrMsg.Text = ErrorMessages.AccountDisabled;
                    m_ErrMsg2.Text = ErrorMessages.PleaseContactYourAdministrator;
                    Response.Redirect("./LoginFailure.aspx?showError=0");
                    break;
                case PrincipalFactory.E_LoginProblem.IsLocked:
                    m_ErrMsg.Text = ErrorMessages.AccountLocked;
                    m_ErrMsg2.Text = ErrorMessages.PleaseContactYourAdministrator;
                    Response.Redirect("./LoginFailure.aspx?showError=1");
                    break;
                case PrincipalFactory.E_LoginProblem.NeedsToWait:
                    m_ErrMsg.Text = ErrorMessages.NeedsToWaitForLogin;
                    m_ErrMsg2.Text = "";
                    Response.Redirect("./LoginFailure.aspx?showError=2");
                    break;
                case PrincipalFactory.E_LoginProblem.InvalidAndWait:
                    m_ErrMsg.Text = ErrorMessages.InvalidLoginPassword;
                    string type = (this.IsInternalLogin) ? "I" : "B";
                    m_ErrMsg2.Text = ErrorMessages.WaitTimeDueToMaxFailedLogins(PrincipalFactory.GetMinutesToWait(m_UserName.Text, type, Guid.Empty));
                    Response.Redirect("./LoginFailure.aspx?showError=3&time="+PrincipalFactory.GetMinutesToWait(m_UserName.Text, type, Guid.Empty));
                    break;
                case PrincipalFactory.E_LoginProblem.InvalidLoginPassword:
                    m_ErrMsg.Text = ErrorMessages.InvalidLoginPassword;
                    m_ErrMsg2.Text = "";
                    break;
                case PrincipalFactory.E_LoginProblem.ActiveDirectorySetupIncomplete:
                    m_ErrMsg.Text = ErrorMessages.ActiveDirectoryAuthFailure.ADNotEnabled;
                    m_ErrMsg2.Text = "";
                    break;
                case PrincipalFactory.E_LoginProblem.InvalidCustomerCode:
                    m_ErrMsg.Text = ErrorMessages.ActiveDirectoryAuthFailure.InvalidCredentials;
                    m_ErrMsg2.Text = "";
                    break;
                case PrincipalFactory.E_LoginProblem.MustLoginAsActiveDirectoryUserType:
                    m_ErrMsg.Text = ErrorMessages.ActiveDirectoryAuthFailure.MustLoginAsADUser;
                    m_ErrMsg2.Text = "";
                    break;
                case PrincipalFactory.E_LoginProblem.None:
                case PrincipalFactory.E_LoginProblem.TempPasswordExpired:
                default:
                    m_ErrMsg.Text = ErrorMessages.InvalidLoginPassword;
                    m_ErrMsg2.Text = "";
                    break;
            }
        }

        private void SetDisabledIECheck()
        {
            RequestHelper.StoreToCookie("DisabledIECheck", "True", true);
        }

        protected void onLoginClick( object sender , System.EventArgs a )
        {
            RequestHelper.ClearAuthenticationCookies();
            AbstractUserPrincipal principal = null;
            PrincipalFactory.E_LoginProblem loginProblem;

            if (this.IsInternalLogin) // LoAdmin accounts
            {
                principal = InternalUserPrincipal.CreatePrincipalWithFailureType(m_UserName.Text, m_Password.Text, out loginProblem, LoginSource.Website);
                SetDisabledIECheck();
            }
            else
            {
                principal = GetLQBUserPrincipal(out loginProblem);
            }

            if (null == principal)
            {
                DisplayErrorMessage(loginProblem);
                return;
            }

            if(principal.BrokerDB != null
                && !this.IsAllowCrossBrowser
                && !this.IsThirdPartyReferrer
                && !this.IsInternalLogin)
            {
                string msg = ConstApp.GetBrowserIncompatibleNotification(Request.Browser.Browser, Request.Browser.Version, Request.UserAgent, PrincipalFactory.CurrentPrincipal);
                if (!string.IsNullOrEmpty(msg))
                {
                    WarningMessageContainer.Visible = true;
                    WarningMessageSpan.InnerText = msg;
                    UpgradeIeLink.Visible = msg == ConstApp.BrowserObsoleteNotification;
                        Response.Cookies.Clear();
                        RequestHelper.ClearAuthenticationCookies();
                    return;
                }
            }

            string confirmToken = RequestHelper.GetSafeQueryString("confirmToken");

            if (!string.IsNullOrEmpty(confirmToken))
            {
                EmployeeDB db = EmployeeDB.RetrieveById(principal.BrokerId, principal.EmployeeId);
                bool success = db.ConfirmEmailAndSave(confirmToken, principal);
                RequestHelper.StoreToCookie(LoginUserControl.SupportCookieName, success.ToString(), DateTime.Now.AddMinutes(5));
            }

            RequestHelper.EnforceMultiFactorAuthentication();

            switch (principal.Type)
            {
                case "B":
                    BrokerUserPrincipal brokerprincipal = principal as BrokerUserPrincipal;
                    //10/25/07 db - temporary, for OPM 18700
                    if (!brokerprincipal.HasLenderDefaultFeatures &&
                        brokerprincipal.HasPermission(Permission.CanViewHiddenInformation))
                    {
                        ClearPermission(brokerprincipal);
                    }

                    // OPM 68408 - Delete security questions cookie to ensure
                    // it's asked again if user logs in a second time in same day
                    if (HttpContext.Current.Request.Cookies["secquest"] != null)
                    {
                        RequestHelper.StoreToCookie("secquest", "t", DateTime.Now.AddDays(-1d));
                    }

                    RequestHelper.DoNextPostLoginTask(PostLoginTask.Login);
                    break;
                case "I":
                    RequestHelper.DoNextPostLoginTask(PostLoginTask.Login);
                    break;
                default:
                    break;
            }
        }

        private AbstractUserPrincipal GetLQBUserPrincipal(out PrincipalFactory.E_LoginProblem loginProblem)
        {
            bool bIsNormalLQBUser = !m_sLoginType_AD.Checked;

            RequestHelper.StoreToCookie("LoginType", (bIsNormalLQBUser) ? "Normal" : "AD", true);

            if (bIsNormalLQBUser)
            {
                return PrincipalFactory.CreateWithFailureType(
                    m_UserName.Text,
                    m_Password.Text,
                    out loginProblem,
                    false /* allowDuplicateLogin */,
                    true /* isStoreToCookie */,
                    LoginSource.Website);
            }

            return PrincipalFactory.CreateWithFailureTypeForActiveDirectoryUser(m_UserName.Text, m_Password.Text, m_CustomerCode.Text, out loginProblem, false /* allowDuplicateLogin */, LoginSource.Website);
        }

        private static void ClearPermission(AbstractUserPrincipal currentPrincipal)
        {
            if(currentPrincipal == null)
                return;

            using( CStoredProcedureExec spExec = new CStoredProcedureExec(currentPrincipal.BrokerId) )
            {
                try
                {
                    spExec.BeginTransactionForWrite();
                    BrokerUserPermissions bUp = new BrokerUserPermissions(currentPrincipal.BrokerId, currentPrincipal.EmployeeId, spExec);
                    bUp.ClearPermission(Permission.CanViewHiddenInformation);
                    bUp.Update(spExec);
                    spExec.CommitTransaction();
                }
                catch
                {}
            }
        }
    }
}
