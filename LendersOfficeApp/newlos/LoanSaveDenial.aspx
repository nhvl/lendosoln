﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanSaveDenial.aspx.cs" Inherits="LendersOfficeApp.newlos.LoanSaveDenial" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Save Denied</title>
    <style>
      .ErrorMessage { padding-left:10px; color:red; font-weight:bold;}
      .ButtonPanel { width:200px; margin-left:auto;margin-right:auto}
    </style>
    <script type="text/javascript">
    <!--      
        function _init() {
            resize(300, 150);
        }
        function closeWnd() {
            var arg = window.dialogArguments || {};
            arg.Action = 'Reload';
            onClosePopup(arg);
        }
        function printWnd() {
            var arg = window.dialogArguments || {};
            arg.Action = 'Print';
            onClosePopup(arg);
        }
    //-->
    </script>
</head>
<body>
    <h4 class="page-header">Save Failed</h4>
    <form id="form1" runat="server">
        <div class="ErrorMessage">
            <ml:EncodedLiteral ID="txtMessage" runat="server"></ml:EncodedLiteral>
            <br />
            <br />
        </div>
        <div class="ButtonPanel">
            <input type="button" value="Print Page" onclick="printWnd()" />
            <input type="button" value="Close" onclick="closeWnd()" />
        </div>
    </form>
</body>
</html>
