﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DetailsofTransaction.ascx.cs" Inherits="LendersOfficeApp.newlos.DetailsofTransaction1" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="DataAccess"%>

<style type="text/css">
    .dateCol { width: 100px; }
    .docCol { width: 170px; }
    .methodCol { width: 153px; }
    .trackingCol { width: 133px; }
    .commentsCol { width: 133px; }
    td.little-indent { padding-left: 10px; }

    .construction {
        display: none;
    }
</style>

<script language=javascript>
    function _initControl() {
        lockFields();
        var bIsPurchase = <%= AspxTools.JsBool(m_isPurchase) %>;
        var isRenovationLoan = <%= AspxTools.JsBool(this.IsRenovationLoan) %>;
        <%= AspxTools.JsGetElementById(sLAmtLckd) %>.disabled = !bIsPurchase || isRenovationLoan;

        if(ML.AreConstructionLoanDataPointsMigrated && ML.IsConstruction) {
            $(".construction").show();
            $(".non-construction").hide();
        }
    }
    
    function lockFields()
    {
        lockFieldByElement(<%= AspxTools.JsGetElementById(sTotCcPbsLocked) %>, <%= AspxTools.JsGetElementById(sTotCcPbs) %>);
        lockFieldByElement(<%= AspxTools.JsGetElementById(sTotEstPp1003Lckd) %>, <%= AspxTools.JsGetElementById(sTotEstPp1003) %>);
        lockFieldByElement(<%= AspxTools.JsGetElementById(sTotEstCc1003Lckd) %>, <%= AspxTools.JsGetElementById(sTotEstCcNoDiscnt1003) %>);
        lockFieldByElement(<%= AspxTools.JsGetElementById(sFfUfmip1003Lckd) %>, <%= AspxTools.JsGetElementById(sFfUfmip1003) %>);
        lockFieldByElement(<%= AspxTools.JsGetElementById(sLDiscnt1003Lckd) %>, <%= AspxTools.JsGetElementById(sLDiscnt1003) %>);
        lockFieldByElement(<%= AspxTools.JsGetElementById(sTransNetCashLckd) %>, <%= AspxTools.JsGetElementById(sTransNetCash) %>);
        lockFieldByElement(<%= AspxTools.JsGetElementById(sOCredit1Lckd) %>, <%= AspxTools.JsGetElementById(sOCredit1Amt) %>);
        lockFieldByElement(<%= AspxTools.JsGetElementById(sOCredit1Lckd) %>, <%= AspxTools.JsGetElementById(sOCredit1Desc) %>);    
        lockFieldByElement(<%= AspxTools.JsGetElementById(sLAmtLckd) %>, <%= AspxTools.JsGetElementById(sLAmtCalc) %>);

        if(!ML.AreConstructionLoanDataPointsMigrated || ML.isConstruction) {
            lockFieldByElement(<%= AspxTools.JsGetElementById(sAltCostLckd) %>, <%= AspxTools.JsGetElementById(sAltCost) %>);
        }

        if (ML.AreConstructionLoanDataPointsMigrated && ML.isConstruction) {
            lockFieldByElement(<%= AspxTools.JsGetElementById(sRefPdOffAmt1003Lckd) %>, <%= AspxTools.JsGetElementById(sRefPdOffAmt1003) %>);
        }

        var load1003cb = <%= AspxTools.JsGetElementById(sLoads1003LineLFromAdjustments) %>;
        if(load1003cb)
        {
            lockFieldByElementFlipped(load1003cb, <%= AspxTools.JsGetElementById(sOCredit2Desc) %>);
            lockFieldByElementFlipped(load1003cb, <%= AspxTools.JsGetElementById(sOCredit2Amt) %>);
            lockFieldByElementFlipped(load1003cb, <%= AspxTools.JsGetElementById(sOCredit3Desc) %>);
            lockFieldByElementFlipped(load1003cb, <%= AspxTools.JsGetElementById(sOCredit3Amt) %>);
            lockFieldByElementFlipped(load1003cb, <%= AspxTools.JsGetElementById(sOCredit4Desc) %>);
            lockFieldByElementFlipped(load1003cb, <%= AspxTools.JsGetElementById(sOCredit4Amt) %>);
        }
    }
    
    function lockFieldByElement(cb, tb) {
        tb.readOnly = !cb.checked;

        if (!cb.checked) 
            tb.style.backgroundColor = "lightgrey";
        else
            tb.style.backgroundColor = "";
    }

    function lockFieldByElementFlipped(cb, tb) {
        tb.readOnly = cb.checked;

        if (cb.checked) 
            tb.style.backgroundColor = "lightgrey";
        else
            tb.style.backgroundColor = "";
    }

    function Migrate1003ToAdjustments()
    {
        if (isDirty()) {
            alert('Please save before migrating.');
            return;
        }

        var data =
        {
            loanId: ML.sLId
        };

        var result = gService.loanedit.call("Migrate1003ToAdjustments", data);
        if(!result.error)
        {
            refreshCalculation();
            var sLoads1003LineLFromAdjustments = <%= AspxTools.JsGetElementById(sLoads1003LineLFromAdjustments) %>.checked;
            if(sLoads1003LineLFromAdjustments)
            {
                <%= AspxTools.JsGetElementById(syncAdjAndCredBtn) %>.style.display = "none";
                <%= AspxTools.JsGetElementById(linkToAdjAndCred) %>.style.display = "";
            }
            else
            {
                <%= AspxTools.JsGetElementById(syncAdjAndCredBtn) %>.style.display = "";
                <%= AspxTools.JsGetElementById(linkToAdjAndCred) %>.style.display = "none";
            }
        }
        else
        {
            alert(result.UserMessage);
        }
    }
</script>

<div>
    <table>
        <% if (!hideHeaderAndBoarder) {%>
        <tr>
            <td class="MainRightHeader" nowrap>
                Details of Transaction
            </td>
        </tr>
        <% } %>
        <tr>
            <td>
                <table <%if(!hideHeaderAndBoarder) {%> class="InsetBorder" <% } %> style="border-color:White" id="Table1" cellspacing="0" cellpadding="0" width="700" border="0">
                    <tr>
                        <td nowrap>
                            <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                              <tr>
                                <td></td>
                                <td class="align-center" colspan="2">Locked</td>
                                <td width="5"></td>
                                <td></td>
                                <td colspan="2">Locked</td>
                              </tr>
                              <tr>
                                <td>a. Purchase price</td>
                                <td style="width: 11px"></td>
                                <td style="width: 79px"><ml:MoneyTextBox CssClass="non-construction" ID="sPurchPrice" TabIndex="3" Width="86" preset="money" runat="server" onchange="refreshCalculation();" /><ml:MoneyTextBox CssClass="construction" Readonly="true" ID="sPurchasePrice1003" TabIndex="3" Width="86" preset="money" runat="server" onchange="refreshCalculation();" /></td>
                                <td></td>
                                <td>j. <a tabindex="-1" href="javascript:linkMe(gVirtualRoot + '/newlos/LoanInfo.aspx?pg=2');" >Subordinate financing</a></td>
                                <td style="width: 6px"></td>
                                <td><ml:MoneyTextBox ID="sONewFinBal" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();" ReadOnly="True" /></td>
                              </tr>
                              <tr>
                                <td>b. Alterations, improvements, repairs</td>
                                <td style="width: 11px">
                                    <input type="checkbox" id="sAltCostLckd" runat="server" onclick="refreshCalculation();" />
                                </td>
                                <td style="width: 79px"><ml:MoneyTextBox ID="sAltCost" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                                <td></td>
                                <td>k. Borrower's closing costs paid by Seller</td>
                                <td style="width: 6px">
                                  <asp:CheckBox ID="sTotCcPbsLocked" TabIndex="4" runat="server" onclick="lockFields(); refreshCalculation();"></asp:CheckBox></td>
                                <td><ml:MoneyTextBox ID="sTotCcPbs" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                              </tr>
                              <tr>
                                <td>c. Land (if acquired separately)</td>
                                <td style="width: 11px"></td>
                                <td style="width: 79px"><ml:MoneyTextBox CssClass="non-construction" ID="sLandCost" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox><ml:MoneyTextBox CssClass="construction" Readonly="true" ID="sLandIfAcquiredSeparately1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                                <td style="height: 23px"></td>
                                <td>l.
                                  <ml:ComboBox ID="sOCredit1Desc" TabIndex="4" runat="server" Width="168px"></ml:ComboBox></td>
                                <td>
                                  <asp:CheckBox ID="sOCredit1Lckd" runat="server" onclick="lockFields(); refreshCalculation();" TabIndex="4"></asp:CheckBox></td>
                                <td style="height: 23px"><ml:MoneyTextBox ID="sOCredit1Amt" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                                <td runat="server" id="adjustmentsBracket" rowspan="4" style="padding: 0px; vertical-align: middle;">
                                    <div style="left: 1px; font-size: 100px; font-weight: lighter; font-family: 'Courier New'; letter-spacing: -8px; text-align: left;">}</div>
                                </td>
                                <td runat="server" id="adjustmentsAction" rowspan="4" style="vertical-align: middle;">
                                    <div>
                                        <input type="checkbox" runat="server" id="sLoads1003LineLFromAdjustments" style="display: none;" />
                                        <a runat="server" id="linkToAdjAndCred" tabindex="-1" href="javascript:linkMe(gVirtualRoot + '/newlos/Disclosure/AdjustmentsAndOtherCredits.aspx');">Adjustments and Other Credits</a>
                                        <input runat="server" id="syncAdjAndCredBtn" type="button" value='Sync with TRID "Adjustments and Other Credits"' onclick="Migrate1003ToAdjustments();" />
                                    </div>
                                </td>
                              </tr>
                              <tr>
                                <td>d. Refi (incl. debts to be paid off)&nbsp;</td>
                                <td class="align-right">
                                  <asp:CheckBox ID="sRefPdOffAmt1003Lckd" TabIndex="3" runat="server" onclick="lockFields(); refreshCalculation();"></asp:CheckBox></td>
                                <td style="width: 79px"><ml:MoneyTextBox ID="sRefPdOffAmt1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                                <td></td>
                                <td>&nbsp;&nbsp;&nbsp;<ml:ComboBox ID="sOCredit2Desc" TabIndex="4" runat="server" Width="168px"></ml:ComboBox>
                                </td>
                                <td>
                                  </td>
                                <td><ml:MoneyTextBox ID="sOCredit2Amt" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                              </tr>
                              <tr>
                                <td>e. Estimated prepaid items&nbsp;</td>
                                <td class="align-right">
                                  <asp:CheckBox ID="sTotEstPp1003Lckd" TabIndex="3" runat="server" onclick="lockFields(); refreshCalculation();"></asp:CheckBox></td>
                                <td style="width: 79px"><ml:MoneyTextBox ID="sTotEstPp1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                                <td></td>
                                <td colspan="2">&nbsp;&nbsp;&nbsp;<ml:ComboBox ID="sOCredit3Desc" TabIndex="4" runat="server" Width="168px"></ml:ComboBox>
                                </td>
                                <td><ml:MoneyTextBox ID="sOCredit3Amt" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();" /></td>
                              </tr>
                              <tr>
                                <td class="little-indent"><ml:EncodedLiteral runat="server" ID="PrepaidsAndReservesLabel">Prepaids and reserves</ml:EncodedLiteral></td>
                                <td><ml:MoneyTextBox runat="server" ID="sTotEstPp" ReadOnly="true"></ml:MoneyTextBox></td>
                                <td style="width: 79px"></td>
                                <td></td>
                                <td colspan="2">&nbsp;&nbsp;&nbsp;<ml:ComboBox ID="sOCredit4Desc" TabIndex="4" runat="server" Width="168px"></ml:ComboBox>
                                </td>
                                <td><ml:MoneyTextBox ID="sOCredit4Amt" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();" /></td>
                              </tr>
                              <tr>
                                <td class="little-indent"><ml:EncodedLiteral runat="server" ID="ProrationsPaidByBorrowerLabel">Prorations paid by borrower</ml:EncodedLiteral></td>
                                <td style="width: 11px"><ml:MoneyTextBox runat="server" ID="sTotalBorrowerPaidProrations" ReadOnly="true"></ml:MoneyTextBox></td>
                                <td style="width: 79px"></td>
                                <td></td>
                                <td colspan="2">&nbsp;&nbsp;&nbsp;Lender credit
                                </td>
                                <td><ml:MoneyTextBox ID="sOCredit5Amt" TabIndex="4" Width="86px" preset="money" runat="server" readonly="true" /></td>
                              </tr>
                              <asp:PlaceHolder runat="server" ID="HideIfOtherFinancingCcIncludedInCc">
                              <tr>
                                <td></td>
                                <td style="width: 11px"></td>
                                <td style="width: 79px"></td>
                                <td></td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;Other financing closing costs</td>
                                <td align="right">-&nbsp;</td>
                                <td><ml:MoneyTextBox ID="sONewFinCc" TabIndex="4" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                              </tr>
                              </asp:PlaceHolder>
                              <tr>
                                <td>f. Estimated closing costs</td>
                                <td class="align-right">
                                  <asp:CheckBox ID="sTotEstCc1003Lckd" TabIndex="3" runat="server" onclick="lockFields(); refreshCalculation();"></asp:CheckBox></td>
                                <td style="width: 79px"><ml:MoneyTextBox ID="sTotEstCcNoDiscnt1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                                <td colspan="2" runat="server" id="LabelCellM">m. Loan amount (exclude PMI, MIP, FF financed)</td>
                                <td class="align-right" runat="server" id="LockedCellM">
                                  <asp:CheckBox ID="sLAmtLckd" onclick="lockFields(); refreshCalculation();" runat="server" TabIndex="202"></asp:CheckBox></td>
                                <td runat="server" id="AmountCellM"><ml:MoneyTextBox ID="sLAmtCalc" TabIndex="4" ReadOnly="True" Width="86px" preset="money" runat="server"></ml:MoneyTextBox></td>
                              </tr>
                              <asp:PlaceHolder runat="server" ID="DisplayIfOtherFinancingCcIncludedInCc">
                              <tr>
                                <td class="little-indent">This loan closing costs</td>
                                <td style="width: 11px">
                                    <ml:MoneyTextBox runat="server" ID="sTotEstCcNoDiscnt" ReadOnly="true"></ml:MoneyTextBox>
                                </td>
                                <td style="width: 79px"></td>
                                <td colspan="2"></td>
                                <td style="width: 6px"></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td class="little-indent">Other financing costs</td>
                                <td style="width: 11px">
                                    <ml:MoneyTextBox runat="server" ID="sONewFinCc2" data-field-id="sONewFinCc" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                </td>
                                <td style="width: 79px"></td>
                                <td colspan="2"></td>
                                <td style="width: 6px"></td>
                                <td></td>
                              </tr>
                              </asp:PlaceHolder>
                              <tr>
                                <td>g. PMI,&nbsp;MIP, Funding Fee</td>
                                <td class="align-right">
                                  <asp:CheckBox ID="sFfUfmip1003Lckd" TabIndex="3" runat="server" onclick="lockFields(); refreshCalculation();"></asp:CheckBox></td>
                                <td style="width: 79px"><ml:MoneyTextBox ID="sFfUfmip1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                                <td></td>
                                <td>n. PMI, MIP, Funding Fee financed</td>
                                <td style="width: 6px"></td>
                                <td><ml:MoneyTextBox ID="sFfUfmipFinanced" TabIndex="4" ReadOnly="True" Width="86px" preset="money" runat="server"></ml:MoneyTextBox></td>
                              </tr>
                              <tr>
                                <td>h. Discount (if Borrower will pay)</td>
                                <td class="align-right">
                                  <asp:CheckBox ID="sLDiscnt1003Lckd" TabIndex="3" runat="server" onclick="lockFields(); refreshCalculation();"></asp:CheckBox></td>
                                <td style="width: 79px"><ml:MoneyTextBox ID="sLDiscnt1003" TabIndex="3" Width="86px" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                                <td></td>
                                <td>o. Loan amount (add m &amp; n)</td>
                                <td style="width: 6px"></td>
                                <td><ml:MoneyTextBox ID="sFinalLAmt" TabIndex="4" ReadOnly="True" Width="86px" preset="money" runat="server"></ml:MoneyTextBox></td>
                              </tr>
                              <tr>
                                <td class="FieldLabel">i. Total costs (add items a to h)</td>
                                <td style="width: 11px"></td>
                                <td style="width: 79px"><ml:MoneyTextBox ID="sTotTransC" TabIndex="3" ReadOnly="True" Width="86px" preset="money" runat="server"></ml:MoneyTextBox></td>
                                <td></td>
                                <td>p. Cash from / to Borr (subtract j, k, l &amp; o from i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td style="width: 6px">
                                  <asp:CheckBox ID="sTransNetCashLckd" TabIndex="4" runat="server" onclick="lockFields(); refreshCalculation();"></asp:CheckBox></td>
                                <td><ml:MoneyTextBox ID="sTransNetCash" TabIndex="4" Width="86px" preset="money" runat="server"></ml:MoneyTextBox></td>
                              </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </tr>
    </table>
</div>