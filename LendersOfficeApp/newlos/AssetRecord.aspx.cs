using System;
using DataAccess;
using System.Collections.Generic;

namespace LendersOfficeApp.newlos
{
	public partial class AssetRecord : BaseSingleEditPage2
	{

		protected void PageInit(object sender, System.EventArgs e) 
		{
			Tools.Bind_AssetT(AssetT);
			Zip.SmartZipcode(City, State);

			CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(AssetRecord));
			dataLoan.InitLoad();
			CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            // Borrower name and coborrower name are needed on client side when user switch OwnerT.

			ClientScript.RegisterHiddenField("BorrowerName", dataApp.aBNm);
			ClientScript.RegisterHiddenField("CoborrowerName", dataApp.aCNm);
			this.PageTitle = "Asset record";

            var valuesThatEnableVOABtn = new List<E_AssetT> { 
                E_AssetT.BridgeLoanNotDeposited,  
                E_AssetT.CertificateOfDeposit,
                E_AssetT.Checking,
                E_AssetT.GiftEquity, 
                E_AssetT.MoneyMarketFund,
                E_AssetT.MutualFunds, 
                E_AssetT.Savings, 
                E_AssetT.SecuredBorrowedFundsNotDeposit,
                E_AssetT.TrustFunds,
                E_AssetT.PendingNetSaleProceedsFromRealEstateAssets,
                E_AssetT.OtherLiquidAsset
            };

            RegisterJsObject("VOAAssets", valuesThatEnableVOABtn);

            Tools.Bind_GiftFundSourceT(GiftSource);
			this.RegisterJsScript("LQBPopup.js");
            this.EnableJquery = true;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion


    }
}
