﻿#region Generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using DataAccess;
    using DataAccess.GFE;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.ObjLib.ToleranceCureDebug;

    /// <summary>
    /// Tolerance Cure "debug" page. Allows the user to look at the fee values that go into calculating the tolerance cures.
    /// </summary>
    public partial class ToleranceCure : BaseLoanPage
    {
        /// <summary>
        /// Shared LocConvert object.
        /// </summary>
        private LosConvert convert = null;

        /// <summary>
        /// OnInit function.
        /// </summary>
        /// <param name="e">System event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.PageTitle = "Tolerance Cure";
            this.PageID = "ToleranceCure";

            UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            base.OnInit(e);
        }

        /// <summary>
        /// Page_Loan function.
        /// </summary>
        /// <param name="sender">Control that calls Page_Load.</param>
        /// <param name="e">System event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            convert = new LosConvert();

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ToleranceCure));
            dataLoan.InitLoad();

            BindCurrentToleranceCure(dataLoan);

            if (dataLoan.sHasLoanEstimateArchiveInPendingStatus)
            {
                CurrentToleranceCureHeader.Visible = true;
                PendingToleranceCure.Visible = true;

                BindPendingToleranceCure(dataLoan);
            }
        }

        /// <summary>
        /// Fires on Item Data Bound event for ZeroToleranceRepeater.
        /// </summary>
        /// <param name="sender">Control that calls this function.</param>
        /// <param name="args">Repeater item event args.</param>
        protected void ToleranceRepeater_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            ToleranceCureDebugFee fees = (ToleranceCureDebugFee)args.Item.DataItem;

            // Controls
            HtmlGenericControl desc = (HtmlGenericControl)args.Item.FindControl("desc");
            HtmlTableCell archiveAmount = (HtmlTableCell)args.Item.FindControl("archiveAmount");
            HtmlTableCell liveAmount = (HtmlTableCell)args.Item.FindControl("liveAmount");
            HtmlTableCell diffAmount = (HtmlTableCell)args.Item.FindControl("diffAmount");
            HtmlTableCell cureAmount = (HtmlTableCell)args.Item.FindControl("cureAmount");

            HtmlGenericControl origDesc = (HtmlGenericControl)args.Item.FindControl("origDesc");

            HtmlGenericControl archiveSection = (HtmlGenericControl)args.Item.FindControl("archiveSection");
            CheckBox archiveTp = (CheckBox)args.Item.FindControl("archiveTp");
            CheckBox archiveAff = (CheckBox)args.Item.FindControl("archiveAff");
            CheckBox archiveCanShop = (CheckBox)args.Item.FindControl("archiveCanShop");
            CheckBox archiveDidShop = (CheckBox)args.Item.FindControl("archiveDidShop");

            HtmlGenericControl liveSection = (HtmlGenericControl)args.Item.FindControl("liveSection");
            CheckBox liveTp = (CheckBox)args.Item.FindControl("liveTp");
            CheckBox liveAff = (CheckBox)args.Item.FindControl("liveAff");
            CheckBox liveCanShop = (CheckBox)args.Item.FindControl("liveCanShop");
            CheckBox liveDidShop = (CheckBox)args.Item.FindControl("liveDidShop");

            desc.InnerText = fees.Description;
            diffAmount.InnerText = fees.DifferenceAmount_rep;

            if (cureAmount != null)
            {
                cureAmount.InnerText = fees.CureAmount_rep;
            }

            origDesc.InnerText = fees.OriginalDescription;

            if (fees.ArchiveFee != null && fees.LiveFee == null)
            {
                archiveAmount.InnerText = fees.ArchiveFee.TotalAmount_rep;
                liveAmount.InnerText = "Not Included";

                SetFeeDetails(fees.ArchiveFee, archiveSection, archiveTp, archiveAff, archiveCanShop, archiveDidShop);
                liveSection.InnerText = archiveSection.InnerText;
            }
            else if (fees.ArchiveFee == null && fees.LiveFee != null)
            {
                archiveAmount.InnerText = "Not Included";
                liveAmount.InnerText = fees.LiveFee.TotalAmount_rep;

                SetFeeDetails(fees.LiveFee, liveSection, liveTp, liveAff, liveCanShop, liveDidShop);
                archiveSection.InnerText = liveSection.InnerText;
            }
            else
            {
                archiveAmount.InnerText = fees.ArchiveFee.TotalAmount_rep;
                liveAmount.InnerText = fees.LiveFee.TotalAmount_rep;

                SetFeeDetails(fees.ArchiveFee, archiveSection, archiveTp, archiveAff, archiveCanShop, archiveDidShop);
                SetFeeDetails(fees.LiveFee, liveSection, liveTp, liveAff, liveCanShop, liveDidShop);
            }
        }

        /// <summary>
        /// Set UI controls based on fee values.
        /// </summary>
        /// <param name="fee">Borrower Closing Cost Fee.</param>
        /// <param name="section">Fee Section Control.</param>
        /// <param name="tp">TP CheckBox Control.</param>
        /// <param name="aff">AFF CheckBox Control.</param>
        /// <param name="canShop">Can Shop CheckBox Control.</param>
        /// <param name="didShop">Did Shop CheckBox Control.</param>
        private void SetFeeDetails(BorrowerClosingCostFee fee, HtmlGenericControl section, CheckBox tp, CheckBox aff, CheckBox canShop, CheckBox didShop)
        {
            section.InnerText = ClosingCostSetUtils.MapIntegratedDisclosureSectionToString(fee.IntegratedDisclosureSectionT);
            tp.Checked = fee.IsThirdParty;
            aff.Checked = fee.IsAffiliate;
            canShop.Checked = fee.CanShop;
            didShop.Checked = fee.DidShop;
        }

        /// <summary>
        /// Binds current tolerance cure info to UI elements.
        /// </summary>
        /// <param name="dataLoan">The Loan data.</param>
        private void BindCurrentToleranceCure(CPageData dataLoan)
        {
            this.sToleranceCure.Text = dataLoan.sToleranceCure_rep;

            this.sToleranceZeroPercentCureLckd.Checked = dataLoan.sToleranceZeroPercentCureLckd;
            this.sToleranceZeroPercentCure.Text = dataLoan.sToleranceZeroPercentCure_rep;

            this.sToleranceTenPercentCureLckd.Checked = dataLoan.sToleranceTenPercentCureLckd;
            this.sToleranceTenPercentCure.Text = dataLoan.sToleranceTenPercentCure_rep;

            // If no valid Loan Estimate Archives exists then we can't fill out the tolerance cure tables.
            // So just return here.
            if (!dataLoan.sHasLastDisclosedLoanEstimateArchive)
            {
                var tridTargetVersion = dataLoan.sTridTargetRegulationVersionT;

                // Check for archives in unknown status.
                int unknownArchivesCount = dataLoan.sClosingCostArchive.Count(p =>
                    p.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate &&
                    p.Status == ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown &&
                    ClosingCostArchive.IsToleranceDataArchive(tridTargetVersion, p));

                // Modify no archive warning if there are archives in unknow status.
                if (unknownArchivesCount > 0)
                {
                    divNoArchives.InnerText = unknownArchivesCount == 1 ? "There is an archive " : "There are archives ";
                    divNoArchives.InnerText += "with \"Unknown\" status. Please check the Loan Estimate Archive page and adjust the status appropriately.";
                }

                divNoArchives.Visible = true;
                return;
            }

            // Load debug data.
            ToleranceCureDebug debugData = dataLoan.sToleranceCureDebug;

            // Zero Tolerance Lender Credit Dummy Fee.
            if (debugData.ZeroToleranceCure.HasLenderCredit)
            {
                this.archiveAmount_lc.InnerText = debugData.ZeroToleranceCure.LenderCreditArchiveAmount_rep;
                this.liveAmount_lc.InnerText = debugData.ZeroToleranceCure.LenderCreditLiveAmount_rep;
                this.diffAmount_lc.InnerText = debugData.ZeroToleranceCure.LenderCreditDifferenceAmount_rep;
                this.cureAmount_lc.InnerText = debugData.ZeroToleranceCure.LenderCreditCureAmount_rep;

                this.trLenderCredit.Visible = true;
            }

            // Zero Tolerance Fees.
            ZeroToleranceRepeater.DataSource = debugData.ZeroToleranceCure.Fees;
            ZeroToleranceRepeater.DataBind();

            // Only count recording fee in 10% tolerance if total of all live fees is not 0.
            if (debugData.TenPercentCumulativeToleranceCure.AreRecordingFeesIncluded)
            {
                this.recordingFee_liveAmount.InnerText = debugData.TenPercentCumulativeToleranceCure.RecordingFeeLiveAmount_rep;
                this.recordingFee_archiveAmount.InnerText = debugData.TenPercentCumulativeToleranceCure.RecordingFeeArchiveAmount_rep;
                this.recordingFee_diffAmount.InnerText = debugData.TenPercentCumulativeToleranceCure.RecordingFeeDifferenceAmount_rep;

                RecordingFeeRepeater.DataSource = debugData.TenPercentCumulativeToleranceCure.RecordingFees;
                RecordingFeeRepeater.DataBind();
            }

            // 10% Tolerance Non-Recording Fees.
            TenToleranceRepeater.DataSource = debugData.TenPercentCumulativeToleranceCure.NonRecordingFees;
            TenToleranceRepeater.DataBind();

            // 10% Tolerance Table Column Sums.
            this.liveSum.InnerText = debugData.TenPercentCumulativeToleranceCure.LiveFeesSumAmount_rep;
            this.archiveSum.InnerText = debugData.TenPercentCumulativeToleranceCure.ArchiveFeesSumAmount_rep;
            this.diffSum.InnerText = debugData.TenPercentCumulativeToleranceCure.FeesSumDifferenceAmount_rep;

            // 10% Tolerance Table Calculation Row.
            this.archiveSum_2.InnerText = debugData.TenPercentCumulativeToleranceCure.ArchiveFeesSumAmount_rep;
            this.archiveSum_TenPercent.InnerText = debugData.TenPercentCumulativeToleranceCure.TenPercentArchiveFeesSumAmount_rep;
            this.liveSum_2.InnerText = debugData.TenPercentCumulativeToleranceCure.LiveFeesSumAmount_rep;

            this.archiveSum_3.InnerText = debugData.TenPercentCumulativeToleranceCure.ArchiveFeesSumAmount_rep;
            this.archiveSum_TenPercent_2.InnerText = debugData.TenPercentCumulativeToleranceCure.TenPercentArchiveFeesSumAmount_rep;
            this.liveSum_3.InnerText = debugData.TenPercentCumulativeToleranceCure.LiveFeesSumAmount_rep;

            this.tenPercentCureCalc.InnerText = debugData.TenPercentCumulativeToleranceCure.CalculatedCureAmount_rep;

            // Unlimited Tolerance Fees.
            UnlimitedToleranceRepeater.DataSource = debugData.UnlimitedToleranceFees.Fees;
            UnlimitedToleranceRepeater.DataBind();

            // Dates in headers.
            this.sLastDisclosedClosingCostArchiveD.InnerText = debugData.ZeroToleranceCure.ArchiveDate;
            this.sLastDisclosedClosingCostArchiveD_2.InnerText = debugData.UnlimitedToleranceFees.ArchiveDate;
            this.sTolerance10BasisLEArchiveD.InnerText = debugData.TenPercentCumulativeToleranceCure.ArchiveDate;
        }

        /// <summary>
        /// Binds pending tolerance cure info to UI elements.
        /// </summary>
        /// <param name="dataLoan">The Loan data.</param>
        private void BindPendingToleranceCure(CPageData dataLoan)
        {
            // Load debug data.
            ToleranceCureDebug debugData = dataLoan.sToleranceCureAfterPendingIsDisclosedDebug;

            this.sToleranceCureAfterPendingIsDisclosed.Text = debugData.ToleranceCure_rep;
            this.sToleranceZeroPercentCureAfterPendingIsDisclosed.Text = debugData.ZeroToleranceCure.CureAmount_rep;
            this.sToleranceTenPercentCureAfterPendingIsDisclosed.Text = debugData.TenPercentCumulativeToleranceCure.CureAmount_rep;

            // Zero Tolerance Lender Credit Dummy Fee.
            if (debugData.ZeroToleranceCure.HasLenderCredit)
            {
                this.pending_lc_archiveAmount.InnerText = debugData.ZeroToleranceCure.LenderCreditArchiveAmount_rep;
                this.pending_lc_liveAmount.InnerText = debugData.ZeroToleranceCure.LenderCreditLiveAmount_rep;
                this.pending_lc_diffAmount.InnerText = debugData.ZeroToleranceCure.LenderCreditDifferenceAmount_rep;
                this.pending_lc_cureAmount.InnerText = debugData.ZeroToleranceCure.LenderCreditCureAmount_rep;

                this.trPendingLenderCredit.Visible = true;
            }

            // Zero Tolerance Fees.
            PendingZeroToleranceRepeater.DataSource = debugData.ZeroToleranceCure.Fees;
            PendingZeroToleranceRepeater.DataBind();

            // Only count recording fee in 10% tolerance if total of all live fees is not 0.
            if (debugData.TenPercentCumulativeToleranceCure.AreRecordingFeesIncluded)
            {
                this.pending_recordingFee_liveAmount.InnerText = debugData.TenPercentCumulativeToleranceCure.RecordingFeeLiveAmount_rep;
                this.pending_recordingFee_archiveAmount.InnerText = debugData.TenPercentCumulativeToleranceCure.RecordingFeeArchiveAmount_rep;
                this.pending_recordingFee_diffAmount.InnerText = debugData.TenPercentCumulativeToleranceCure.RecordingFeeDifferenceAmount_rep;

                PendingRecordingFeeRepeater.DataSource = debugData.TenPercentCumulativeToleranceCure.RecordingFees;
                PendingRecordingFeeRepeater.DataBind();
            }

            // 10% Tolerance Non-Recording Fees.
            PendingTenToleranceRepeater.DataSource = debugData.TenPercentCumulativeToleranceCure.NonRecordingFees;
            PendingTenToleranceRepeater.DataBind();

            // 10% Tolerance Table Column Sums.
            this.pending_liveSum.InnerText = debugData.TenPercentCumulativeToleranceCure.LiveFeesSumAmount_rep;
            this.pending_archiveSum.InnerText = debugData.TenPercentCumulativeToleranceCure.ArchiveFeesSumAmount_rep;
            this.pending_diffSum.InnerText = debugData.TenPercentCumulativeToleranceCure.FeesSumDifferenceAmount_rep;

            // 10% Tolerance Table Calculation Row.
            this.pending_archiveSum_2.InnerText = debugData.TenPercentCumulativeToleranceCure.ArchiveFeesSumAmount_rep;
            this.pending_archiveSum_TenPercent.InnerText = debugData.TenPercentCumulativeToleranceCure.TenPercentArchiveFeesSumAmount_rep;
            this.pending_liveSum_2.InnerText = debugData.TenPercentCumulativeToleranceCure.LiveFeesSumAmount_rep;

            this.pending_archiveSum_3.InnerText = debugData.TenPercentCumulativeToleranceCure.ArchiveFeesSumAmount_rep;
            this.pending_archiveSum_TenPercent_2.InnerText = debugData.TenPercentCumulativeToleranceCure.TenPercentArchiveFeesSumAmount_rep;
            this.pending_liveSum_3.InnerText = debugData.TenPercentCumulativeToleranceCure.LiveFeesSumAmount_rep;

            this.pendingTenPercentCureCalc.InnerText = debugData.TenPercentCumulativeToleranceCure.CalculatedCureAmount_rep;

            // Unlimited Tolerance Fees.
            PendingUnlimitedToleranceRepeater.DataSource = debugData.UnlimitedToleranceFees.Fees;
            PendingUnlimitedToleranceRepeater.DataBind();

            // Dates in headers.
            this.sLoanEstimateArchiveInPendingStatusD.InnerText = debugData.ZeroToleranceCure.ArchiveDate;
            this.sLoanEstimateArchiveInPendingStatusD_2.InnerText = debugData.UnlimitedToleranceFees.ArchiveDate;
            this.pending10BasisArchiveD.InnerText = debugData.TenPercentCumulativeToleranceCure.ArchiveDate;
        }
    }
}