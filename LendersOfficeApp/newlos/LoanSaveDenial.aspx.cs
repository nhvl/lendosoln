﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using LendersOffice.Common;
using CommonLib;

namespace LendersOfficeApp.newlos
{
    public partial class LoanSaveDenial : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sMsg = SafeConvert.ToString(Request["msg"]);
            txtMessage.Text = sMsg != string.Empty ? sMsg : "Unable to save the loan file as you do not have write permission to the loan fields.";
        }
    }
}
