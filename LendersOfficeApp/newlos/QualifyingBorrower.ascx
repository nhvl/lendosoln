﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QualifyingBorrower.ascx.cs" Inherits="LendersOfficeApp.newlos.QualifyingBorrower" %>

<div class="true-container">
    <h4 class="qualifying-borrower-header page-header">
        L4.  Qualifying the Borrower - Minimum Required Funds or Cash Back
    </h4>
    <div class="qm-container">
        <div class="section">
            <div class="qm-section-header">DUE FROM BORROWERS(S)</div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">A.</div>
                    <div class="qm-row-main-text">SALES CONTRACT PRICE</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input preset="money" readonly id="sPurchasePrice1003" name="sPurchasePrice1003" type="text" />
                </div>
            </div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">B.</div>
                    <div class="qm-row-main-text">Improvements, Renovations, and Repairs</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sAltCost" name="sAltCost" readonly preset="money" type="text" />
                    <input type="checkbox" id="sAltCostLckd" name="sAltCostLckd" />
                </div>
            </div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">C.</div>
                    <div class="qm-row-main-text">Land (if acquired separately)</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sLandIfAcquiredSeparately1003" name="sLandIfAcquiredSeparately1003" readonly preset="money" type="text" />
                </div>
            </div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">D.</div>
                    <div class="qm-row-main-text">For Refinance: Balance of Mortgage Loans on the Property to be paid off in the Transaction</div>
                    <div class="qm-row-special-notes">(See Table 3a. Property You Own)</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sRefTransMortgageBalancePayoffAmt" name="sRefTransMortgageBalancePayoffAmt" readonly preset="money" type="text" />
                </div>
            </div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">E.</div>
                    <div class="qm-row-main-text">Credit Cards and Other Debts Paid Off</div>
                    <div class="qm-row-special-notes">(See Table 2c. Liabilities -- Credit Cards, Other Debts, and Leases that You Owe)</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sRefNotTransMortgageBalancePayoffAmt" name="sRefNotTransMortgageBalancePayoffAmt" readonly preset="money" type="text" />
                </div>
            </div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">F.</div>
                    <div class="qm-row-main-text">Borrower Closing Costs (including Prepaid and Initial Escrow Payments)</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sTotEstBorrCostUlad" name="sTotEstBorrCostUlad" readonly preset="money" type="text" />
                    <img class="calculator" data-calcid="sTotEstBorrCostUlad" name="sTotEstBorrCostUlad" />
                </div>
            </div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">G.</div>
                    <div class="qm-row-main-text">Discount Points</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sLDiscnt1003" name="sLDiscnt1003" readonly preset="money" type="text" />
                    <input type="checkbox" id="sLDiscnt1003Lckd" name="sLDiscnt1003Lckd" />
                </div>
            </div>
            <div class="qm-total qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">H.</div>
                    <div class="qm-row-main-text">TOTAL DUE FROM BORROWER(s) (TOTAL of A thru G)</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sTotTransCUlad" name="sTotTransCUlad" readonly preset="money" type="text" />
                </div>
            </div>
        </div>

        <div class="section">
            <div class="qm-section-header">TOTAL MORTGAGE LOANS</div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">I.</div>
                    <div class="qm-row-main-text">
                        Loan Amount
                    <div class="qm-row-text-sub-section">
                        <span>Loan Amount Excluding Financed Mortgage Insurance (or Mortgage Insurance Equivalent)</span>
                        <span class="amount-prefix"></span>
                        <input id="sLAmtCalc" name="sLAmtCalc" readonly preset="money" type="text" />
                    </div>
                        <div class="qm-row-text-sub-section">
                            <span>Financed Mortgage Insurance (or Mortgage Insurance Equivalent) Amount</span>
                            <span class="amount-prefix"></span>
                            <input id="sFfUfmipFinanced" name="sFfUfmipFinanced" readonly preset="money" type="text" />
                        </div>
                    </div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sFinalLAmt" name="sFinalLAmt" readonly preset="money" type="text" />
                </div>
            </div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">J.</div>
                    <div class="qm-row-main-text">Other New Mortgage Loans on the Property the Borrower(s) is Buying or Refinancing</div>
                    <div class="qm-row-special-notes">(See Table 4b. Other New Mortgage Loans on the Property You are Buying or Refinancing)</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sONewFinBal" name="sONewFinBal" readonly preset="money" type="text" />
                </div>
            </div>
            <div class="qm-total qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">K.</div>
                    <div class="qm-row-main-text">TOTAL MORTGAGE LOANS (Total of I and J)</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sTotMortLAmtUlad" name="sTotMortLAmtUlad" readonly preset="money" type="text" />
                </div>
            </div>
            <div class="qm-section-header">TOTAL CREDITS</div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">L.</div>
                    <div class="qm-row-main-text">Seller Credits</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sSellerCreditsUlad" name="sSellerCreditsUlad" readonly preset="money" type="text" />
                    <img class="calculator" data-calcid="sSellerCreditsUlad" name="sSellerCreditsUlad" />
                </div>
            </div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">M.</div>
                    <div class="qm-row-main-text">Other Credits</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sTotOtherCreditsUlad" name="sTotOtherCreditsUlad" readonly preset="money" type="text" />
                </div>
            </div>
            <div class="qm-total qm-row">
                <div class="qm-cell">
                    <div class="qm-row-label">N.</div>
                    <div class="qm-row-main-text">TOTAL CREDITS (Total of L and M)</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sTotCreditsUlad" name="sTotCreditsUlad" readonly preset="money" type="text" />
                </div>
            </div>
            <div class="qm-section-header">CALCULATION</div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-main-text">TOTAL DUE FROM BORROWER(s) (Line H)</div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sTotTransCUlad2" name="sTotTransCUlad2" data-field-id="sTotTransCUlad" name="sTotTransCUlad" readonly preset="money" type="text" />
                </div>
            </div>
            <div class="qm-row">
                <div class="qm-cell">
                    <div class="qm-row-main-text">LESS TOTAL MORTGAGE LOANS (Line K) AND TOTAL CREDITS (Line N)</div>
                    <div class="qm-row-special-notes"></div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix">–</span>
                    <input id="sTotMortLTotCreditUlad" name="sTotMortLTotCreditUlad" readonly preset="money" type="text" />
                </div>
            </div>
            <div class="qm-total qm-row">
                <div class="qm-cell">
                    <div class="qm-row-main-text">
                        <div></div>
                        Cash From/To the Borrower (Line H minus Line K and Line N)
                    </div>
                </div>
                <div class="qm-cell">
                    <span class="amount-prefix"></span>
                    <input id="sTransNetCashUlad" name="sTransNetCashUlad" readonly preset="money" type="text" />
                </div>
            </div>
        </div>
    </div>

<div class="calculator-popup" data-Calcid="sTotEstBorrCostUlad" name="sTotEstBorrCostUlad">
    <div class="popup-content">
        <div class="CssTable">
            <div class="CssTableRow">
                <div class="CssTableCell">
                    <label for="sTotEstCcNoDiscn1003">Total Estimated Closing Cost</label>
                    <input preset="money" type="text" id="sTotEstCcNoDiscnt1003" name="sTotEstCcNoDiscnt1003" />
                    <input type="checkbox" id="sTotEstCc1003Lckd" name="sTotEstCc1003Lckd" />
                </div>
                <div class="CssTableCell">
                    +
                </div>
                <div class="CssTableCell">
                    <label for="sTotEstPp1003">Total Estimated Prepaid Cost</label>
                    <input preset="money" type="text" id="sTotEstPp1003" name="sTotEstPp1003" />
                    <input type="checkbox" id="sTotEstPp1003Lckd" name="sTotEstPp1003Lckd" />
                </div>
            </div>
        </div>
        <div class="calculator-buttons">
            <button class="ok" type="button">OK</button>
            <button class="cancel" type="button">Cancel</button>
        </div>
    </div>
</div>

<div class="calculator-popup" data-Calcid="sSellerCreditsUlad" name="sSellerCreditsUlad">
    <div class="popup-content">
        <div class="CssTable">
            <div class="CssTableRow">
                <div class="CssTableCell">
                    <label for="sTotCcPbs">Borrower Closing Costs Paid By Seller</label>
                    <input preset="money" type="text" id="sTotCcPbs" name="sTotCcPbs" />
                    <input type="checkbox" id="sTotCcPbsLocked" name="sTotCcPbsLocked" />
                </div>
                <div class="CssTableCell">
                    +
                </div>
                <div class="CssTableCell">
                    <label for="sTRIDSellerCredits">Seller Credit From Adjustments</label>
                    <input type="text" preset="money" readonly id="sTRIDSellerCredits" name="sTRIDSellerCredits" />
                </div>
            </div>
        </div>
        <div class="calculator-buttons">
            <button type="button" class="ok">OK</button>
            <button type="button" class="cancel">Cancel</button>
        </div>
    </div>
</div>

</div>