<%@ Page language="c#" Codebehind="BorrowerList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.BorrowerList" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
		<title>BorrowerList</title>
  </head>
	<body class="RightBackground" MS_POSITIONING="FlowLayout">
		<script language="javascript">
    <!--

    function _init() {
      <% if (IsReadOnly) { %> 
        <%= AspxTools.JsGetElementById(m_addBtn) %>.disabled = true;
      <% } %>
    }
    function confirmPrimaryBorrower(name) {
      <% if (IsReadOnly) { %> 
        return;
      <% } %>      
      if (confirm('Do you want to set ' + name + ' as primary borrower?')) {
        document.getElementById("action").value = "UpdatePrimary";
        document.forms[0].submit();
      } else {
        document.forms[0].reset();
      }
    }
    <%-- Swap the order of previous primary borrower with the new id in drop down list --%>
    function updateBorrowerPrimary(id) {
      <% if (IsReadOnly) { %> 
        alert('Cannot update primary borrower because this loan is read-only.');
        return;
      <% } %>      
      parent.info.f_swapPrimary(id);
    }
    function deleteBorrower(id) {
      <% if (IsReadOnly) { %> 
        alert('Cannot delete borrower because this loan is read-only.');
        return;
      <% } %>    
      parent.info.f_removeBorrower(id);
    }

//-->
		</script>
		<form id="BorrowerList" method="post" runat="server">
			<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0" onclick="clearDirty();">
				<tr>
					<td noWrap><ml:nodoubleclickbutton id="m_addBtn" runat="server" WaitingMessage="Wait..." Text="Add new borrower" onclick="m_addBtn_Click"></ml:nodoubleclickbutton></td>
				</tr>
				<tr>
					<td noWrap><asp:datagrid id="m_dg" runat="server" DataKeyField="aAppId" AutoGenerateColumns="False" CssClass="DataGrid" cellpadding="5">
<alternatingitemstyle cssclass="GridAlternatingItem">
</AlternatingItemStyle>

<itemstyle cssclass="GridItem">
</ItemStyle>

<headerstyle cssclass="GridHeader">
</HeaderStyle>

<columns>
<asp:TemplateColumn HeaderText="Is Primary?">
<itemstyle horizontalalign="Center">
</ItemStyle>

<itemtemplate>
										<%#AspxTools.HtmlControl(DisplayRadioBox((bool)DataBinder.Eval(Container.DataItem, "IsPrimary"), (Guid)DataBinder.Eval(Container.DataItem, "aAppId"), (string)DataBinder.Eval(Container.DataItem, "Name")))%>
									
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn DataField="Name" HeaderText="Name" />
<asp:ButtonColumn Text="Swap borr &amp; spouse" CommandName="swap" />
<asp:ButtonColumn Text="Delete spouse" CommandName="deletecoborrower" />
<asp:ButtonColumn Text="Delete this borr &amp; spouse" CommandName="delete" />
</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
		</form>
	</body>
</html>
