﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConversationLog.aspx.cs" Inherits="LendersOfficeApp.newlos.ConversationLog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Conversation Log</title>
    <style type="text/css">
        body
        {            
            background-color: gainsboro;
            height:100%;
            overflow-y:scroll;
        }
        .isDefaultPermissionLevelForCategory
        {
            font-weight:normal;
        }
        .isNotDefaultPermissionLevelForCategory
        {
            font-weight:bold;
        }
        .commentRemovedText
        {
            font-weight:bold;
        }            
        .categorySelectorRequired
        {
            color:red;
        }
        .hiddenComment
        {
            color:darkgray;
            font-style:italic;
        }
        .hideOrShowLinkSection
        {
            position:absolute;
            right:10px;
        }
        .fa-img
        {
            width:11px;
            height:11px;
            vertical-align:text-top;
        }
        #viewControlsSection{
            padding-top:3px;
            padding-bottom:3px;
        }
        #orderByFilter
        {
            padding:2px;
            border-color:black;
            border-width:1px;
            border-style:solid;
            margin-right:0;
        }
        #categoriesFilterDisplayer
        {
            padding:3px;
            padding-right:5px;
            border-color:black;
            border-width:1px;
            border-style:solid;
            margin-left:0;
            background-color:white;
        }
        #filterDefaultCategoriesHeader{
            background-color:gainsboro;
            color:black;
            padding:2px;
            padding-left:7px;
            border-style:solid;
            border-width:1px;
            border-color:black;
        }
        td
        {
            min-width:70px;
        }
        #categoriesFilterSection
        {
            position:fixed;
            padding:6px;
            background-color:white;
            border-style:solid;
            border-width:1px;
            border-color:black;
            z-index:1;
            margin-left:70px;
        }
        #permissionLevelsDisplaySection
        {
            position:fixed;
            padding:6px;
            background-color:white;
            border-style:solid;
            border-width:1px;
            border-color:black;
            z-index:1;
            left:550px;
        }
        #categoriesFilters
        {
            padding-top:5px;
        }
        #container{
            /*min-width:600px;*/
            max-width:865px;
            min-width:700px;
            margin-left:10px;
            max-height:700px;
            min-height:500px;
            margin-right:3em;
        }
        .hidden
        {
            display:none
        }        
        .categoryNameSection
        {
            background-color:#003366;
            color:white;
            font-weight:bold;
            text-decoration:underline;
            padding:5px;
            padding-left:15px;
        }
        .commentSection
        {
            padding:10px;
            background-color:white;
            margin-bottom:3px;
            padding-bottom:5px;
            padding-right:0;
            margin-right:0;
            white-space:nowrap;
        }
        .commentInfoSection
        {
            width:125px;
            text-align:left;
            padding-left:5px;
            padding-right:5px;
            display:inline-block;
            white-space:normal;
            vertical-align:top;
            font-style:italic;
        }
        .commentTextAndReplyLinkSection
        {
            background-color:white;
            display:inline-block;
            padding-left:10px;
        }
        .commentTextSection
        {
            margin-bottom:1em;
            padding-right:1em;
            white-space:normal;
            word-wrap:break-word;
        }
        .commentsSection
        {   
            position:relative;
        }
        .conversationsSection
        {
            max-height:600px;
            min-height:300px;
            overflow-y:scroll;
            border-style:solid;
            border-width:1px;
            min-width:700px;
        }
        #newCommentSection
        {
            padding-top:10px;
            background-color:gainsboro;
            height:70px;
        }
        .newReplySection
        {
            padding-right:2px;
        }
        .categorySelectorLabelSection
        {
            display: inline-block;
            width: 70px;
        }
        .categorySelectorSection
        {
            display: inline-block;
        }
        .newCommentBtnAndLinkSection
        {
            margin: 2px;
        }
        .newCommentCategorySelectorSection
        {
            display: inline-block;
            vertical-align: top;
            width: 230px;
        }
        .newCommentTextAndAddSection
        {
            display: inline-block;
        }
        .newCommentTextSection
        {
            margin: 2px;
            margin-bottom: 5px;
            width: 97%;
        }
        select#categorySelector
        {
            width: 150px;
            padding:1px;
        }
        textarea#newCommentTextArea
        {
            width: 97%;
        }
        .newReplyTextArea{
            width:100%;
        }
        select::-ms-expand
        {
            <%-- this only works in ie10+ --%>
            background-color:white;
            border-style:none;
        }
    </style>
</head>
<body>
    <script id="commentTemplate" type="text/x-jquery-templ">        
        <div class="commentSection" data-id="${$data.id}" data-is-hidden="${$data.isHidden}" data-visual-depth="${Math.min($data.depth, ML.maxVisibleReplyDepth)}"  style="margin-left:${Math.min($data.depth, ML.maxVisibleReplyDepth)*2+ 'em'};">
            <div class="commentInfoSection">
                ${$data.commenterName}<br />
                ${$data.createdDateStringForDisplay}<br />
                {{if ML.enableConversationLogPermissions }}
                <br/>
                <span class="${($data.permissionLevelId == categoryById[$data.categoryId].defaultPermissionLevelId) ? 'isDefaultPermissionLevelForCategory' : 'isNotDefaultPermissionLevelForCategory'}">Permission: ${permissionLevelById[$data.permissionLevelId].name}</span> <br/>
                {{/if}}
            </div>
            <div class="commentTextAndReplyLinkSection">
                {{if  ML.enableConversationLogPermissions && permissionLevelById[$data.permissionLevelId].canHide}}
                    <div class="hideOrShowLinkSection">
                        <a class="hideOrShowLink">${$data.isHidden ? 'Show' : 'Hide'}</a>
                    </div>
                {{/if}}
                <div class="commentTextSection">
                    {{if  ML.enableConversationLogPermissions && $data.isHidden}}
                      <span class="commentRemovedText">Removed by ${$data.hiderFullName}</span> <br/><br/>
                    {{/if}}
                   {{each lines}}
                         <span class="commentLine ${(ML.enableConversationLogPermissions && $data.isHidden) ? 'hiddenComment' : 'normalComment'}">${$value}</span><br/>
                   {{/each}}
                </div>
                <hr />
                
                <div class="replySection">
                    {{if ML.enableConversationLogPermissions === false || (permissionLevelById[$data.permissionLevelId].canReply && (false == $data.isHidden))}}
                        <a class="createsReplySection">Reply</a>
                    {{else}}
                        <a class="disabledReplyLink" disabled="disabled">Reply</a>
                    {{/if}}
                </div>
            </div>
        </div>
    </script>
    
    <script id="conversationsTemplate" type="text/x-jquery-templ">
    {{each conversations}}
        <div class="categoryConversationSection ${categoryVisibilityByCategoryId[categoryId] ? '' : 'hidden'}">
            <div class="categoryNameSection">${categoryById[categoryId].displayName}</div>
            <div class="commentsSection">
                {{each comments}}
                    {{tmpl($value) "#commentTemplate"}}
                {{/each}}
            </div>
        </div>
    {{/each}}
    </script>

    <script id="categoryFilterTemplate" type="text/x-jquery-templ">
        <div class="categoryVisibilityCheckboxContainer">
            <label>
                <input type="checkbox" value="${id}" class="updatesCategoryVisibility" NotForEdit/>
                ${displayName}
            </label>
        </div>
    </script>

    <script id="categoryOptionTemplate" type="text/x-jquery-templ">
        <option value="${id}">${displayName}</option>
    </script>

    <script id="newReplySectionTemplate" type="text/x-jquery-templ">
        <div class="newReplySection" data-parent-id="${parentCommentId}">
            <textarea class="newReplyTextArea" rows="3" NotForEdit></textarea> <br />
            <input type="button" disabled="disabled" value="Reply" class="savesReply"/>
            <input type="button" value="Cancel" class="cancelsReply" />    
        </div>  
    </script>

    <script id="permissionLevelsBodySectionTemplate" type="text/x-jquery-tmpl">
        {{each postablePermissionLevels}}
            <tr>
                <td>
                    <a class="selectsPermissionLevel" data-id="${id}">${name}</a>
                </td>
                <td>${description}</td>
            </tr>
        {{/each}}
    </script>
    
    <script type="text/javascript">
        var categoryVisibilityByCategoryId = {};
        var selectedCategoryId = -1; // -1 will mean no selection.
        var selectedPermissionLevelId = -1; // -1 will mean no selection.
        var categoryById = {};
        var conversationDateSortBy = 'newest';
        var permissionLevelById = {}

        jQuery(function ($) {

            function keepBetween(number, min, max)
            {
                return Math.max(min, Math.min(max, number));
            }

            function resizeCommentContainers()
            {
                var $container = $('#container');
                var windowW = $(window).width();
                var windowH = $(window).height();

                var newContainerWidth = keepBetween((windowW * 8) / 10, 700, 865);
                var newContainerHeight = keepBetween(windowH - 70, 500, 700);
                $container.width(newContainerWidth);
                $container.height(newContainerHeight);
                $('form').height(newContainerHeight + 20);
                $('.newCommentTextAndAddSection').width(newContainerWidth - 250);

                var conversationsContainerWidth = newContainerWidth;
                var conversationsContainerHeight = newContainerHeight - 150;

                $('.conversationsSection').width(conversationsContainerWidth);
                $('.conversationsSection').height(conversationsContainerHeight);
                $('.categoryConversationSection').width(conversationsContainerWidth - 17);
                $('.commentsSection').width(conversationsContainerWidth - 17);
                $('.commentSection').each(function (index, el) {
                    var $el = $(el);
                    var visualDepth = $el.data('visual-depth');
                    $el.width($el.closest('.commentsSection').width() - 10 - 2 * 11 * visualDepth);
                });

                $('.commentTextAndReplyLinkSection').each(function (index, el) {
                    var $el = $(el);
                    $el.width($el.closest('.commentSection').width()-150);
                });
            }

            $(window).on('resize', resizeCommentContainers);
            $(window).on('beforeunload', function (e) {
                if (
                       ($('.newReplyTextArea').length > 0 && $('.newReplyTextArea').val() != '')
                    || $('#newCommentTextArea').val() != '')
                {
                    e.returnValue = 'You will lose any text entered for a new comment. Do you want to navigate to the new page?';
                    return e.returnValue;
                }
            })

            initializeModel();
            initializePageView();
            var $filterContainer = $('#categoriesFilterSection');
            var $filterActivators = $('#categoriesFilterDisplayer,#filterImg');
            initializeHandlers();

            function initializeModel()
            {
                categoryById = {};
                var numCategories = categories.length;
                var category;
                for (var i = 0; i < numCategories; i++) 
                {
                    category = categories[i];
                    if (typeof (categoryVisibilityByCategoryId[category.id]) === 'undefined') {
                        categoryVisibilityByCategoryId[category.id] = true;
                    }
                    categoryById[category.id] = category;
                }

                initializeSelectedCategoryId();

                initializeCommentDates();

                if (ML.enableConversationLogPermissions) {
                    permissionLevelById = arrayOfObjectsToDictionary(permissionLevels, function (p) { return p.id; }, identity);
                }

                initializeSelectedPermissionLevelId();
            }

            function initializeSelectedPermissionLevelId()
            {
                if (selectedCategoryId == -1 || ML.enableConversationLogPermissions === false)
                {
                    setSelectedPermissionLevelId(-1);
                }
                else {
                    if (selectedPermissionLevelId == -1) {
                        setSelectedPermissionLevelId(categoryById[selectedCategoryId].defaultPermissionLevelId);
                    }
                }
            }

            function arrayOfObjectsToDictionary(array, keyGetter, valueGetter) {
                var result = {};
                var arrayLength = array.length;

                for (var i = 0; i < arrayLength; i++) {
                    var item = array[i];
                    result[keyGetter(item)] = valueGetter(item);
                }

                return result;
            }

            function identity(p)
            {
                return p;
            }            

            function initializeCommentDates()
            {
                var numConversations = conversations.length;
                for (var i = 0; i < numConversations; i++) {
                    var conversation = conversations[i];
                    var comments = conversation.comments;
                    var numComments = comments.length;
                    for (var j = 0; j < numComments; j++) {
                        // only care about top-level comments atm.
                        var comment = comments[j];
                        setCommentsDate(comment);
                    }
                }
            }

            function setCommentsDate(newComment)
            {
                var d = newComment.createdDateForJs;
                if (typeof (newComment.createdDate) === 'undefined') {
                    newComment.createdDate = new Date(d.year, d.zeroBasedMonth, d.dateOfMonth, d.hour, d.minute, d.second);
                }
            }

            function initializePageView()
            {
                initializeCategoriesFilterSection();
                initializeCategorySelector();
                if (ML.enableConversationLogPermissions) {
                    initializePermissionLevelsSection();
                }
                updateViews();
            }

            function initializePermissionLevelsSection()
            {
                var $permissionLevelsBodySection = $('#permissionLevelsBodySection');
                $permissionLevelsBodySection.empty();
                var $permissionLevelsBodySectionTemplate = $('#permissionLevelsBodySectionTemplate').template();
                var postablePermissionLevels = arrayFilter(permissionLevels, function (p) { return p.canPost && p.isActive; });
                var $permissionLevelsBodySectionInner = $.tmpl($permissionLevelsBodySectionTemplate, { postablePermissionLevels: postablePermissionLevels });
                $permissionLevelsBodySection.append($permissionLevelsBodySectionInner)
            }

            function arrayFilter(array, condition)
            {
                var resultArray = [];
                var arrayLength = array.length;
                for(var i = 0; i < arrayLength; i++)
                {
                    var item = array[i];
                    if(condition(item))
                    {
                        resultArray.push(item);
                    }
                }

                return resultArray;
            }

            function enableOrDisablePostButton()
            {
                var $savesNewComment = $('.savesNewComment');
                var $newComment = $('#newCommentTextArea');
                var disable = $newComment.val() === '';
                disable |= (selectedCategoryId == -1)
                $savesNewComment.prop('disabled', disable);
            }

            function initializeHandlers() {
                var $categoriesFilterSection = $('#categoriesFilterSection');
                $('#categoriesFilterDisplayer').on('click',
                    function () {
                        $categoriesFilterSection.toggle();
                    });

                $('#orderByFilter').on('change', function () {
                    conversationDateSortBy = $(this).val();
                    updateConversationViews();
                });

                var $savesNewComment = $('.savesNewComment');
                $('#newCommentTextArea').on('keyup blur', function () {
                    enableOrDisablePostButton();
                });
                
                $('.conversationsSection').on('keyup blur', '.newReplyTextArea', function () {
                    var val = $(this).val();
                    var $newReplySection = $(this).closest('.newReplySection');
                    var $replyButton = $newReplySection.find('.savesReply');
                    $replyButton.prop('disabled', val === '');
                });


                $('#categoriesFilters').on('change', 'input.updatesCategoryVisibility', function () {
                    var $cb = $(this);
                    var isToBeVisible =  $cb.prop('checked');
                    var categoryId = $cb.val();
                    categoryVisibilityByCategoryId[categoryId] = isToBeVisible;
                    updateConversationViews();
                });

                $('.savesNewComment').on('click', saveNewComment);

                $('.conversationsSection').on('click', '.createsReplySection', createReplySection);
                
                // the below uses delegated events.  better for performance (fewer event handlers), and handles that .cancelsReply is templated and added at runtime.
                $('.conversationsSection').on('click', '.cancelsReply', cancelReply);
                $('.conversationsSection').on('click', '.savesReply', replyToComment);
                $('body').on('click', hidePopups);
                $('#categorySelector').on('change', function () {
                    selectedCategoryId = this.value;
                    enableOrDisablePostButton();
                    $('.categorySelectorRequired').toggle(selectedCategoryId == -1);
                    
                    if (false === ML.enableConversationLogPermissions) 
                    {
                        return;
                    }

                    if (selectedCategoryId == -1) {
                        setSelectedPermissionLevelId(-1);
                    }
                    else {
                        setSelectedPermissionLevelId(categoryById[selectedCategoryId].defaultPermissionLevelId);    
                    }
                });

                $('.conversationsSection').on('click', '.hideOrShowLink', hideOrShowComment);

                var $permissionLevelInfoSection = $('#selectedPermissionLevelInfoSection');
                $permissionLevelInfoSection.on('click', '.displaysPermissionLevelInfo', displaySelectedPermissionLevelInfo);
                $permissionLevelInfoSection.on('click', '.promptsForPermissionLevelChange', openPermissionLevelsSelector);

                var $permissionLevelsBodySection = $('#permissionLevelsBodySection');
                $('.cancelsPermissionLevelSelection').on('click', hidePermissionLevelsSelector);
                $permissionLevelsBodySection.on('click', '.selectsPermissionLevel', selectPermissionLevel);
            }
            
            function displaySelectedPermissionLevelInfo()
            {
                alert(permissionLevelById[selectedPermissionLevelId].description);
            }

            function selectPermissionLevel(eventObj)
            {
                var $permissionLevelLink = $(eventObj.target);
                var id = $permissionLevelLink.data('id');

                if (id == "")
                {
                    id = null;
                }

                setSelectedPermissionLevelId(id);
                hidePermissionLevelsSelector();
            }

            function setSelectedPermissionLevelId(id)
            {
                // choose the first permission level that the user can post to.
                if (id != -1 && !permissionLevelById[id].canPost)
                {    
                    for(var i = 0; i < permissionLevels.length; i++)
                    {
                        if(permissionLevels[i].canPost)
                        {
                            id = permissionLevels[i].id;
                            break;
                        }
                    }
                }

                selectedPermissionLevelId = id;
                updatePermissionLevelView();
            }
            
            function updatePermissionLevelView()
            {
                if(selectedPermissionLevelId == -1 || ML.enableConversationLogPermissions === false)
                {
                    $('#selectedPermissionLevelInfoSection').hide()
                }
                else
                {
                    $('.permissionLevelNameSection').text(permissionLevelById[selectedPermissionLevelId].name);
                    $('#selectedPermissionLevelInfoSection').show();
                }
            }

            function openPermissionLevelsSelector()
            {
                if (ML.enableConversationLogPermissions) {
                    $('#permissionLevelsDisplaySection').show();
                }
            }

            function hidePermissionLevelsSelector()
            {
                $('#permissionLevelsDisplaySection').hide();
            }

            function hidePopups(event)
            {
                hidePopup($filterContainer, $filterActivators, event);
            }

            function hidePopup($container, $activators, event)
            {
                <%-- http://stackoverflow.com/a/7385673/420667 --%>
                if (!$container.is(event.target)  // if the target of the click isn't the container...
                    && $container.has(event.target).length === 0 // ... nor a descendant of the container
                    && !$activators.is(event.target)) // and it's not the element that shows it.
                {
                    $container.hide();
                }
            }

            var $newReplySectionTemplate = $('#newReplySectionTemplate').template();

            function createReplySection($event)
            {
                if (hasDisabledAttr($($event.currentTarget))) {
                    return;
                }

                // close any non-empty existing replies, if the user agrees.
                var $otherReplySection = $('.newReplySection');
                if ($otherReplySection.length != 0)
                {
                    if ($('.newReplyTextArea').val() != '') {
                        if (!confirm('Another reply is already open. You will lose any text entered in the existing reply. Open new reply?')) {
                            return;
                        }
                    }

                    var $otherReplyLink = $otherReplySection.prev().find('.createsReplySection');
                    setDisabledAttr($otherReplyLink, false);
                    $otherReplySection.remove();
                }

                // continue to create the new reply section if no existing replies the user cares about.
                var $triggeringReplyLink = $($event.target);
                setDisabledAttr($triggeringReplyLink, true);
                var $triggeringCommentSection = $triggeringReplyLink.closest('div.commentSection');
                var parentCommentId = $triggeringCommentSection.data('id');
                var $newReplySection = $.tmpl($newReplySectionTemplate, { parentCommentId: parentCommentId });
                var $newReplyTextArea = $newReplySection.find('.newReplyTextArea');
                var $container = $triggeringReplyLink.closest('div.commentTextAndReplyLinkSection');
                $container.append($newReplySection);
                $newReplyTextArea.focus();
                var $scrollable = $('div.conversationsSection');
                if (false === isFullyInView($scrollable, $newReplySection)) {
                    scrollToBottom($scrollable, $newReplySection);
                }
            }

            function cancelReply($event)
            {
                var $triggeringButton = $($event.target);
                var $triggeringButtonReplySection = $triggeringButton.closest('div.newReplySection');
                var $container = $triggeringButtonReplySection.closest('div.commentTextAndReplyLinkSection');
                var $replyLink = $container.find('div.replySection a');
                $triggeringButtonReplySection.remove();
                setDisabledAttr($replyLink, false);
            }

            function reInitializePageFromResult(results, extraModelInitializer)
            {
                var wasSuccessful = typeof (results.value['alertMessage']) === 'undefined';

                if (wasSuccessful === false) {
                    alert(results.value['alertMessage']);
                }
                else
                {
                    var newConversationsJson = results.value['newConversationsJson'];
                    conversations = JSON.parse(newConversationsJson);
                }                
                
                
                ML.enableConversationLogPermissions = (results.value['enableConversationLogPermissions'] === 'True');

                if (ML.enableConversationLogPermissions === true)
                {
                    var newPermissionLevelsJson = results.value['newPermissionLevels'];
                    permissionLevels = JSON.parse(newPermissionLevelsJson);
                }
                else
                {
                    permissionLevels = [];
                }

                var newCategoriesJson = results.value['updatedCategoriesJson'];
                categories = JSON.parse(newCategoriesJson);
                initializeModel();
                if (wasSuccessful === true) {
                    extraModelInitializer();
                }
                initializePageView();
            }

            function saveNewComment()
            {
                var $newCommentTextArea = $('#newCommentTextArea');
                var categoryId = $('#categorySelector').val() 
                var args ={
                    resourceString: ML.resourceString,
                    commentText: $newCommentTextArea.val(),
                    categoryId: categoryId,
                    permissionLevelId:selectedPermissionLevelId
                };      

                var results = gService.loanedit.call("PostComment", args);
                if (!results.error) {
                    reInitializePageFromResult(results, function () {
                        categoryVisibilityByCategoryId[categoryId] = true;
                    });

                    if (typeof (results.value['alertMessage']) !== 'undefined')
                    {
                        return;
                    }

                    var newCommentId = results.value['newCommentId'];
                    $newCommentTextArea.val('').trigger('keyup');
                    
                    var $newCommentSection = $('.commentSection[data-id="' + newCommentId + '"');
                    var $conversationSection = $newCommentSection.closest('.categoryConversationSection');
                    scrollToTop($('div.conversationsSection'), $conversationSection);
                    var fadeTime = 1000;
                    fadeIn($conversationSection, fadeTime, 1);
                    $newCommentTextArea.closest('.newCommentTextAndAddSection').find('.savesNewComment').css('background-color', '');
                }
                else
                {
                    alert(results.UserMessage);
                }
            }

            function hideOrShowComment()
            {
                var $commentElement = $(this).closest('.commentSection');
                var commentId = $commentElement.data('id');
                var isHidden = $commentElement.data('is-hidden');
                var resp = confirm('Are you sure you wish to ' + (isHidden ? 'show' : 'hide') + ' this comment?');
                if (resp != true)
                {
                    return;
                }

                var args = {
                    resourceString: ML.resourceString,
                    commentId: commentId
                };
                var methodToCall = isHidden ? 'ShowComment' : 'HideComment';
                var results = gService.loanedit.call(methodToCall, args);
                if (!results.error) {
                    reInitializePageFromResult(results, noop);

                    if (typeof (results.value['alertMessage']) !== 'undefined') {
                        return;
                    }

                }
                else {
                    alert(results.UserMessage);
                }
            }

            function updateNewCommentSectionVisibility()
            {
                var hasActiveCategory = false;
                for (var i = 0; i < categories.length; i++) {
                    if (categories[i].isActive) {
                        hasActiveCategory = true;
                        break;
                    }
                }
                
                var hasPostablePermissionLevel = false;
                for (var i = 0; i < permissionLevels.length; i++) {
                    if (permissionLevels[i].canPost) {
                        hasPostablePermissionLevel = true;
                        break;
                    }
                }

                $('#newCommentSection').toggle(hasActiveCategory && (false === ML.enableConversationLogPermissions || hasPostablePermissionLevel));
            }

            function updateViews()
            {
                updateCategoriesFilterSection();
                updateConversationViews();
                updateNewCommentSectionVisibility();
                updatePermissionLevelView();
            }

            function isFullyInView($scrollable, $innerItem)
            {
                // false if the top part is above the top of the scrollable
                // false if the bottom part is below the bottom of the scrollable.
                // if the item spans greater than the entire height, it's not fully in view.

                var sTop = $scrollable.offset().top;
                var sBottom = sTop + $scrollable.height();
                var iTop = $innerItem.offset().top;
                var iBottom = iTop + $innerItem.height();

                if( iTop < sTop )
                {
                    return false;
                }
                if( iBottom > sBottom )
                {
                    return false;
                }
                return true;
            }

            function noop() { }

            function replyToComment(){
                var $newReplySection = $(this).closest('.newReplySection');
                var parentCommentId = $newReplySection.data('parent-id');
                var replyText = $newReplySection.find('.newReplyTextArea').val();

                var args = {
                    parentCommentId: parentCommentId,
                    replyText: replyText,
                    resourceString: ML.resourceString
                };

                var results = gService.loanedit.call("ReplyToComment", args);
                if (!results.error) {
                    reInitializePageFromResult(results, noop);

                    if (typeof (results.value['alertMessage']) !== 'undefined') {
                        return;
                    }

                    var newReplyId = results.value['newReplyId'];
                    var $newCommentSection = $('.commentSection[data-id="' + newReplyId + '"');
                    var $scrollable = $('div.conversationsSection');
                    if (false === isFullyInView($scrollable, $newCommentSection)) {
                        scrollToBottom($scrollable, $newCommentSection);
                    }
                    var fadeTime = 1000;
                    fadeIn($newCommentSection, fadeTime, 1);
                }
                else {
                    alert(results.UserMessage);
                }
            }

            function scrollToBottom($scrollable, $innerItem)
            {
                // scrolls the element to the bottom of the scrollable (+ a little.)
                $scrollable.scrollTop($innerItem.offset().top + $scrollable.scrollTop() - $scrollable.offset().top - $scrollable.height() + $innerItem.innerHeight() + 2*11);
            }
            
            function scrollToTop($scrollable, $innerItem)
            {
                // scrolls the element to the top of the scrollable.
                $scrollable.scrollTop($innerItem.offset().top + $scrollable.scrollTop() - $scrollable.offset().top);
            }

            function fadeIn($elements, fadeTime, numFades)
            {
                for(var i =0; i < numFades; i++)
                {
                    $elements.fadeOut(0).fadeIn(fadeTime);
                }
            }

            function conversationComparer(one, two)
            {
                if(conversationDateSortBy === 'newest')
                {
                    return two.comments[0].createdDate - one.comments[0].createdDate;
                }
                else
                {
                    return one.comments[0].createdDate - two.comments[0].createdDate;
                }
            }

            function updateConversationViews()
            {
                initializeCommentDates();

                for (var i = 0; i < conversations.length; i++)
                {
                    conversations.sort(conversationComparer);
                }

                var $conversationsTemplate = $('#conversationsTemplate').template();
                var $conversationsContainer = $('#conversationsSection');
                var $conversations = $.tmpl($conversationsTemplate,
                    {
                        conversations: conversations
                    });
                $conversationsContainer.html($conversations);

                resizeCommentContainers();
            }

            function initializeSelectedCategoryId()
            {
                if (selectedCategoryId == -1)
                {
                    return;
                }
                
                var numCategories = categories.length;                
                for (var i = 0; i < numCategories; i++) {
                    var category = categories[i];
                    if (category.isActive === false) {
                        continue;
                    }
                    if (category.id == selectedCategoryId) {
                        // nothing to do, the selected category is active.
                        return;
                    }
                }

                // if couldn't find the selected category, choose the first active one as selected.
                for (var i = 0; i < numCategories; i++) {
                    var category = categories[i];
                    if (category.isActive === false) {
                        continue;
                    }
                    selectedCategoryId = category.id;
                    break;
                }
            }

            function initializeCategorySelector() {
                var $categoryOptionTemplate = $('#categoryOptionTemplate').template();
                var $categoriesSelector = $('#categorySelector');
                $categoriesSelector.empty();

                var numCategories = categories.length;
                // append the categories, and mark the one that is selected as selected.
                for (var i = 0; i < numCategories; i++) {
                    var category = categories[i];
                    if (category.isActive === false)
                    {
                        continue;
                    }
                    var $categoryOptionElement = $.tmpl($categoryOptionTemplate, category);
                    if (category.id == selectedCategoryId)
                    {
                        $categoryOptionElement.prop('selected', 'selected');
                        $('.categorySelectorRequired').hide();
                    }
                    $categoriesSelector.append($categoryOptionElement);
                }

                if(selectedCategoryId == -1)
                {
                    var $blankCategoryOptionElement = $.tmpl($categoryOptionTemplate, { id: -1, displayName: '' });
                    $blankCategoryOptionElement.prop('selected', 'selected');
                    $categoriesSelector.prepend($blankCategoryOptionElement);
                    $('.categorySelectorRequired').show();
                }
            }

            function initializeCategoriesFilterSection() {
                var $categoryFilterTemplate = $('#categoryFilterTemplate').template();
                var $categoriesFilterContainer = $('#categoriesFilters');
				$categoriesFilterContainer.empty();
                var numCategories = categories.length;
                if (numCategories === 0)
                {
                    return;
                }
                var $table = $('<table></table>');
                var $tableBody = $('<tbody></tbody>');
                var $trow;
                // build empty table.
                // if even, create row, append it.
                // always add cell to row.
                for (var i = 0; i < numCategories; i++) {
                    if (i % 2 === 0)
                    {
                        $trow = $('<tr></tr>');
                        $tableBody.append($trow);
                    }
                    $td = $('<td></td>');
                    var category = categories[i];
                    var $categoryVisibilityCheckboxContainerElement = $.tmpl($categoryFilterTemplate, category);
                    $td.append($categoryVisibilityCheckboxContainerElement);
                    $trow.append($td);
                }
                if (numCategories % 2 === 1)
                {
                    $trow.append($('<td></td>'));
                }

                $table.append($tableBody);
                $categoriesFilterContainer.append($table);
                updateCategoriesFilterSection();
            }

            function updateCategoriesFilterSection()
            {
                var $categoriesFilterContainer = $('#categoriesFilters');
                for(var i = 0; i < categories.length; i++)
                {
                    var category = categories[i];
                    var categoryVisiblity = categoryVisibilityByCategoryId[category.id];
                    var $categoryVisibilityCheckboxElement = $categoriesFilterContainer.find('input.updatesCategoryVisibility[value="' + category.id +'"]');
                    $categoryVisibilityCheckboxElement.prop('checked', categoryVisiblity);
                }
            }

            function getCommentWhereNewReplyBelongs(commentId) {
                for (var i = 0; i < conversations.length; i++) {
                    var comments = conversations[i].comments;
                    for (var j = 0; j < comments.length; j++) {
                        var comment = comments[j];
                        var commentOrNull = getCommentWhereNewReplyBelongsRecursively(comment, commentId);
                        if (commentOrNull != null) {
                            return commentOrNull;
                        }
                    }
                }
                return null;
            }

            function getCommentWhereNewReplyBelongsRecursively(comment, commentId) {
                if (comment.id == commentId) {
                    return comment;
                }
                var replies = comment.replies;
                for (var i = 0; i < replies.length; i++) {
                    var reply = replies[i]
                    var commentOrNull = getCommentWhereNewReplyBelongsRecursively(reply, commentId)
                    if (commentOrNull != null) {
                        return commentOrNull;
                    }
                }
                return null;
            }
        });
    </script>

    <form id="ConvLogForm" runat="server">
    <div class="MainRightHeader" nowrap>Conversation Log</div>
    <div id="container">
        <div id="viewControlsSection">
            <select id="orderByFilter" NotForEdit>
                <option value="newest" selected="selected">Newest</option>
                <option value="oldest">Oldest</option>
            </select>
            <span id="categoriesFilterDisplayer">
                Categories                
                <img id="filterImg" src="../images/fa-filter.svg" class="fa-img"/>
            </span>
            <div id="categoriesFilterSection" class="hidden">
                <div id="filterDefaultCategoriesHeader">Filter Default Categories</div>
                <div id="categoriesFilters">
                    <!-- load from model -->
                </div>
            </div>
        </div>
        <div id="conversationsSection" class="conversationsSection">
            <!-- load from model -->
        </div>
        <div id="permissionLevelsDisplaySection" class="hidden">
            <table>
                <thead>
                    <th>Permission</th>
                    <th>Description</th>
                </thead>
                <tbody id="permissionLevelsBodySection">
                    <!-- load from model -->
                </tbody>
            </table>
            <div>
                <input type="button" value="Cancel"  class="cancelsPermissionLevelSelection"/>
            </div>
        </div>
        <div id="newCommentSection" style="display:none">
            <div class="newCommentTextAndAddSection">
                <div class="newCommentTextSection"><textarea id="newCommentTextArea" rows="5" cols="120" NotForEdit></textarea></div>
                <div class="newCommentBtnAndLinkSection"><input type="button" disabled="disabled" value="New Comment" class="savesNewComment"/></div>
            </div>
            <table class="newCommentCategorySelectorSection">
                <tr>
                    <td>
                        <div class="categorySelectorLabelSection">Category: <span class="categorySelectorRequired">*</span></div>
                    </td>
                    <td>
                        <div class="categorySelectorSection">
                            <select id="categorySelector" NotForEdit>
                                <!-- Load from model. -->
                            </select>
                        </div>
                    </td>
                </tr>
                <tr id="selectedPermissionLevelInfoSection" class="hidden">
                    <td>
                        <div class="permissionLevelSelectorLabelSection">Permission:</div>
                    </td>
                    <td>
                        <span class="permissionLevelNameSection">
                            <!-- load from model -->
                        </span>
                        <a class="displaysPermissionLevelInfo">?</a>
                        <a class="promptsForPermissionLevelChange">change</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    </form>
</body>
</html>
