﻿namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using Forms;
    using LendersOffice.Common;

    public partial class AdditionalHELOC : BaseLoanPage
    {
        internal static void BindDataFromControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.BindData(loan, item);
        }

        internal static void LoadDataForControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.LoadData(loan, item);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.PageID = "AdditionalHELOC";
            this.PageTitle = "Additional HELOC Data";

            this.EnableJquery = true;

            Tools.Bind_sHelocPmtBaseT(sHelocQualPmtBaseT);
            Tools.Bind_sHelocPmtFormulaT(sHelocQualPmtFormulaT);
            Tools.Bind_sHelocPmtFormulaRateT(sHelocQualPmtFormulaRateT);
            Tools.Bind_sHelocPmtAmortTermT(sHelocQualPmtAmortTermT);

            Tools.Bind_sHelocPmtBaseT(sHelocPmtBaseT);
            Tools.Bind_sHelocPmtFormulaT(sHelocPmtFormulaT);
            Tools.Bind_sHelocPmtFormulaRateT(sHelocPmtFormulaRateT);
            Tools.Bind_sHelocPmtAmortTermT(sHelocPmtAmortTermT);

            Tools.Bind_sInitialRateT(sInitialRateT);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            EnableJqueryMigrate = false;
            base.OnInit(e);
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(AdditionalHELOC));
            dataLoan.InitLoad();

            this.QualRatePopup.LoadData(dataLoan);

            sCreditLineAmt.Text = dataLoan.sCreditLineAmt_rep;
            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sQualTopR.Text = dataLoan.sQualTopR_rep;
            sQualBottomR.Text = dataLoan.sQualBottomR_rep;
            sLtvR.Text = dataLoan.sLtvR_rep;
            sCltvR.Text = dataLoan.sCltvR_rep;
            sHcltvR.Text = dataLoan.sHcltvR_rep;
            sTerm.Text = dataLoan.sTerm_rep;
            sDue.Text = dataLoan.sDue_rep;
            sRAdjIndexR.Text = dataLoan.sRAdjIndexR_rep;
            sRAdjMarginR.Text = dataLoan.sRAdjMarginR_rep;
            sQualIR.Text = dataLoan.sQualIR_rep;
            this.sQualIRLckd.Checked = dataLoan.sQualIRLckd;
            sLAmtLckd.Checked = dataLoan.sLAmtLckd;

            sHelocDraw.Text = dataLoan.sHelocDraw_rep;
            sHelocRepay.Text = dataLoan.sHelocRepay_rep;
            Tools.SetDropDownListValue(sHelocQualPmtBaseT, dataLoan.sHelocQualPmtBaseT);
            Tools.SetDropDownListValue(sHelocQualPmtFormulaT, dataLoan.sHelocQualPmtFormulaT);
            Tools.SetDropDownListValue(sHelocQualPmtFormulaRateT, dataLoan.sHelocQualPmtFormulaRateT);
            Tools.SetDropDownListValue(sHelocQualPmtAmortTermT, dataLoan.sHelocQualPmtAmortTermT);
            sHelocQualPmtAmortTerm.Text = dataLoan.sHelocQualPmtAmortTerm_rep;

            sHelocQualPmtBaseAmt.Text = dataLoan.sHelocQualPmtBaseAmt_rep;

            sHelocQualPmtPcBase.Text = dataLoan.sHelocQualPmtPcBase_rep;

            sHelocQualPmtMb.Text = dataLoan.sHelocQualPmtMb_rep;


            Tools.SetDropDownListValue(sHelocPmtBaseT, dataLoan.sHelocPmtBaseT);
            Tools.SetDropDownListValue(sHelocPmtFormulaT, dataLoan.sHelocPmtFormulaT);
            Tools.SetDropDownListValue(sHelocPmtFormulaRateT, dataLoan.sHelocPmtFormulaRateT);
            Tools.SetDropDownListValue(sHelocPmtAmortTermT, dataLoan.sHelocPmtAmortTermT);
            sHelocPmtAmortTerm.Text = dataLoan.sHelocPmtAmortTerm_rep;
            sHelocPmtBaseAmt.Text = dataLoan.sHelocPmtBaseAmt_rep;
            sHelocPmtPcBase.Text = dataLoan.sHelocPmtPcBase_rep;
            sHelocPmtMb.Text = dataLoan.sHelocPmtMb_rep;


            sProThisMQual.Text = dataLoan.sProThisMQual_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;

            sHelocAnnualFee.Text = dataLoan.sHelocAnnualFee_rep;
            sHelocDrawFee.Text = dataLoan.sHelocDrawFee_rep;
            sHelocMinimumAdvanceAmt.Text = dataLoan.sHelocMinimumAdvanceAmt_rep;
            sHelocReturnedCheckFee.Text = dataLoan.sHelocReturnedCheckFee_rep;
            sHelocStopPaymentFee.Text = dataLoan.sHelocStopPaymentFee_rep;
            sHelocCalculatePrepaidInterest.Checked = dataLoan.sHelocCalculatePrepaidInterest;

            sHelocTerminationFeePeriod.Text = dataLoan.sHelocTerminationFeePeriod_rep;
            sHelocTerminationFee.Text = dataLoan.sHelocTerminationFee_rep;
            sHelocMinimumPayment.Text = dataLoan.sHelocMinimumPayment_rep;
            sHelocMinimumPaymentPc.Text = dataLoan.sHelocMinimumPaymentPc_rep;

            sFullyIndexedR.Text = dataLoan.sFullyIndexedR_rep;
            sDaysInYr.Text = dataLoan.sDaysInYr_rep;
            sDailyPeriodicR.Text = dataLoan.sDailyPeriodicR_rep;
            sDailyPeriodicFullyIndexR.Text = dataLoan.sDailyPeriodicFullyIndexR_rep;

            sRAdj1stCapMon.Text = dataLoan.sRAdj1stCapMon_rep;
            sRLifeCapR.Text = dataLoan.sRLifeCapR_rep;

            Tools.SetDropDownListValue(sInitialRateT, dataLoan.sInitialRateT);
            sFirstLienTrusteeNm.Text = dataLoan.sFirstLienTrusteeNm;
            sFirstLienRecordingD.Text = dataLoan.sFirstLienRecordingD_rep;
            sFirstLienHolderNm.Text = dataLoan.sFirstLienHolderNm;
            sFirstLienRecordingBookNum.Text = dataLoan.sFirstLienRecordingBookNum;
            sFirstLienRecordingPageNum.Text = dataLoan.sFirstLienRecordingPageNum;
            sFirstLienRecordingInstrumentNum.Text = dataLoan.sFirstLienRecordingInstrumentNum;

            this.RegisterJsGlobalVariables("IsRenovationLoan", dataLoan.sIsRenovationLoan);
        }
    }
}
