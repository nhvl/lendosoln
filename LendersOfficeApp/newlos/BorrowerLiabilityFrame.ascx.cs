using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib;
using LendersOffice.ObjLib.TRID2;

namespace LendersOfficeApp.newlos
{
	public partial  class BorrowerLiabilityFrame : BaseLoanUserControl, IAutoLoadUserControl
	{
        protected string ListUrl
        {
            get { return Tools.VRoot + "/newlos/BorrowerLiabilityList.aspx?loanid=" + LoanID + "&appid=" + ApplicationID + "&islead=" + RequestHelper.GetSafeQueryString("islead") + "&RecordID=" + RequestHelper.GetSafeQueryString("RecordID"); }
        }
        protected string EditUrl
        {
            get { return Tools.VRoot + "/newlos/LiabilityRecord.aspx?loanid=" + LoanID + "&appid=" + ApplicationID + "&islead=" + RequestHelper.GetSafeQueryString("islead"); }
        }

        public void LoadData() 
        {
            CPageData dataLoan = new CLiaData(LoanID, false);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            var coll = dataApp.aLiaCollection;

            var alimony = coll.GetAlimony(false) ;
            if (null != alimony) 
            {
                Alimony_ComNm.Text             = alimony.OwedTo;
                Alimony_Pmt.Text               = alimony.Pmt_rep;
                Alimony_RemainMons.Text        = alimony.RemainMons_rep;
                Alimony_NotUsedInRatio.Checked = alimony.NotUsedInRatio;
            }

            var childSupport = coll.GetChildSupport(false);
            if (childSupport != null)
            {
                ChildSupport_ComNm.Text = childSupport.OwedTo;
                ChildSupport_Pmt.Text = childSupport.Pmt_rep;
                ChildSupport_RemainMons.Text = childSupport.RemainMons_rep;
                ChildSupport_NotUsedInRatio.Checked = childSupport.NotUsedInRatio;
            }

            var jobRelated = coll.GetJobRelated1(false);
            if (null != jobRelated) 
            {
                JobRelated1_ComNm.Text             = jobRelated.ExpenseDesc;
                JobRelated1_Pmt.Text               = jobRelated.Pmt_rep;
                JobRelated1_NotUsedInRatio.Checked = jobRelated.NotUsedInRatio;
            }

            jobRelated = coll.GetJobRelated2(false);
            if (null != jobRelated) 
            {
                JobRelated2_ComNm.Text             = jobRelated.ExpenseDesc;
                JobRelated2_Pmt.Text               = jobRelated.Pmt_rep;
                JobRelated2_NotUsedInRatio.Checked = jobRelated.NotUsedInRatio;
            }

            aLiaBalTot.InnerText   = dataApp.aLiaBalTot_rep;
            aLiaMonTot.InnerText   = dataApp.aLiaMonTot_rep;
            aLiaPdOffTot.InnerText = dataApp.aLiaPdOffTot_rep;
            sLiaMonLTot.InnerText  = dataLoan.sLiaMonLTot_rep;
            sLiaBalLTot.InnerText  = dataLoan.sLiaBalLTot_rep;
            sRefPdOffAmt.InnerText = dataLoan.sRefPdOffAmt_rep;

            Tools.Bind_ResponsibleLien(sLienToPayoffTotDebt);
            Tools.SetDropDownListValue(sLienToPayoffTotDebt, dataLoan.sLienToPayoffTotDebt);

            bool isTrid2 = dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017;
            bool enableLienToPayoffTotDebt = (dataLoan.sLienPosT == E_sLienPosT.First && dataLoan.sIsOFinNew && dataLoan.sConcurSubFin > 0) || dataLoan.sLienPosT == E_sLienPosT.Second;

            ((BasePage)this.Page).RegisterJsGlobalVariables("IsTrid2", isTrid2);
            ((BasePage)this.Page).RegisterJsGlobalVariables("EnableLienToPayoffTotDebt", enableLienToPayoffTotDebt);

            aAsstLiaCompletedJointly.Checked = !dataApp.aAsstLiaCompletedNotJointly;
            aAsstLiaCompletedNotJointly.Checked = dataApp.aAsstLiaCompletedNotJointly;
        }
        public void SaveData() 
        {
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
            // Only add this javascript when this usercontrol is visible, else borrower info, monthly income tab will not work.
            if (this.Visible) 
            {
                Page.ClientScript.RegisterHiddenField("ListLocation", "BorrowerInfo.aspx?loanid=" + LoanID + "&pg=4");

                ((BasePage)this.Page).RegisterJsScript("singleedit2.js");
                ((BasePage)this.Page).RegisterJsScript("LQBPrintFix.js");
            }

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
