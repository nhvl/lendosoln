﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanOfficerLicensingInfo.aspx.cs" Inherits="LendersOfficeApp.newlos.LoanOfficerLicensingInfo" %>
<%@ Import namespace="DataAccess"%>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../newlos/Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript">
        function _init() {
            <%= AspxTools.JsGetElementById(aIntrvwrMethodT) %>.disabled = !<%= AspxTools.JsGetElementById(aIntrvwrMethodTLckd) %>.checked;
            var a1003InterviewDLckdCb = document.getElementById('a1003InterviewDLckd');
            if (a1003InterviewDLckdCb) {
                lockField(a1003InterviewDLckdCb, 'a1003InterviewD');
            }
        }
    </script>
    <form id="form1" runat="server">
    <div>
    <table id="Table7" cellspacing="0" cellpadding="0" width="700" border="0">
    <tr>
            <td class="MainRightHeader" nowrap colspan = "2">
                Loan Officer Licensing Info
            </td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap colspan="2"><UC:CFM ID="CFM" runat="server" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">This application was taken by</td>
          <td>
            <asp:DropDownList ID="aIntrvwrMethodT" TabIndex="7" runat="server" Width="158px" Height="24px" />
              <label><input type="checkbox" runat="server" id="aIntrvwrMethodTLckd" onchange="refreshCalculation();" />Lock</label>
          </td>
        </tr>
        <tr>
          <td class="FieldLabel">Interview Date</td>
          <td>
              <ml:DateTextBox id="a1003InterviewD" runat="server" width="75" preset="date"></ml:DateTextBox>
              <asp:CheckBox ID="a1003InterviewDLckd" runat="server" onclick="refreshCalculation();" Text="Lckd" />
          </td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Originator's Name</td>
          <td>
            <asp:TextBox ID="LoanOfficerName" TabIndex="7" runat="server" Width="267px" /></td>
        </tr>
        <tr>
          <%--OPM 55321: changed the label to its current value, added the mask.js settings to the text field to will make it harder to submit non-numbers. --%>
          <td class="FieldLabel">Loan Originator NMLS ID</td>
          <td><asp:TextBox ID="LoanOfficerLoanOriginatorIdentifier" TabIndex="7" runat="server" CssClass="mask" preset="nmlsID"/></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Originator's License Number</td>
          <td>
            <asp:TextBox ID="LoanOfficerLicenseNumber" TabIndex="7" runat="server" Width="120px" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Originator's Phone</td>
          <td><ml:PhoneTextBox ID="LoanOfficerPhone" TabIndex="7" runat="server" preset="phone" Width="120" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Originator's Email</td>
          <td><ml:PhoneTextBox ID="LoanOfficerEmail" TabIndex="7" runat="server" Width="120" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Origination Company's Name</td>
          <td><asp:TextBox ID="BrokerName" TabIndex="7" runat="server" Width="357px"/></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Origination Company NMLS ID</td>
          <td><asp:TextBox ID="LoanOfficerCompanyLoanOriginatorIdentifier" runat="server" TabIndex="7" CssClass="mask" preset="nmlsID"/></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Origination Company's License Number</td>
          <td><asp:TextBox ID="BrokerLicenseNumber" TabIndex="7" runat="server" Width="120px" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Origination Company's Address</td>
          <td><asp:TextBox ID="BrokerStreetAddr" TabIndex="7" runat="server" Width="359px" /></td>
        </tr>
        <tr>
          <td></td>
          <td>
            <asp:TextBox ID="BrokerCity" TabIndex="7" runat="server" Width="265px"></asp:TextBox><ml:StateDropDownList ID="BrokerState" TabIndex="7" runat="server"></ml:StateDropDownList>
            <ml:ZipcodeTextBox ID="BrokerZip" TabIndex="7" runat="server" preset="zipcode" Width="50" CssClass="mask" /></td>
        </tr>
        <tr>
          <td class="FieldLabel">Loan Origination Company's Phone</td>
          <td class="FieldLabel"><ml:PhoneTextBox ID="BrokerPhone" TabIndex="7" runat="server" preset="phone" Width="120" CssClass="mask" /> 
          Fax <ml:PhoneTextBox ID="BrokerFax" TabIndex="7" runat="server" preset="phone" Width="120" CssClass="mask" /></td>
        </tr>
      </table>
    </div>
    </form>
    <script type="text/javascript">
        function _postSaveMe(result) {
            if (result.RefreshNavigation && parent && parent.treeview) {
                parent.treeview.location.reload();
                window.setTimeout(function() {
                    if (parent.treeview && typeof parent.treeview.selectPageID === 'function') {
                        parent.treeview.selectPageID(<%= AspxTools.JsString(this.PageID) %>);
                    }
                }, 500);
            }
        }
    </script>
</body>
</html>
