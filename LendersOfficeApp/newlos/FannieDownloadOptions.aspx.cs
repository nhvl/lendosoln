/// Author: David Dao

using System;
using DataAccess;

namespace LendersOfficeApp.newlos
{
	public partial class FannieDownloadOptions : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected bool m_isRateLocked;
    
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.DisplayCopyRight = false;
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FannieDownloadOptions));
            dataLoan.InitLoad();
            
            m_isRateLocked = dataLoan.sIsRateLocked;
            Is1003.Checked = Broker.ImportAus1003ByDefault;
            IsCreditReport.Checked = Broker.ImportAusCreditReportByDefault;
            IsDuFindings.Checked = Broker.ImportAusFindingsByDefault;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
