﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SeamlessLpa.aspx.cs" Inherits="LendersOffice.Integration.SeamlessLPA.SeamlessLpa" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Seamless LPA Interface</title>
    <script type="text/javascript">
        var virtualRoot = gVirtualRoot;
        var seamlessFolder = virtualRoot + '/newlos/SeamlessLPA';
    </script>
</head>
<body id="app" ng-controller="SeamlessLPAController as seamless">
    <h4 class="page-header ng-cloak">{{pageTitle}}</h4>
    <span ng-show="!loaded">Loading {{pageTitle}}...<span class="fa fa-spin fa-spinner fa-fw"></span></span>
    <div class="ng-cloak" ng-show="loaded && !loadSuccess">
        Something went wrong communicating with the server:
        <br />{{errorMessage}}
        <div class="bottomButtons"><button ng-click="closeWindow()">Close</button></div>
    </div>
    <div ng-view></div>
    
    <script type="text/javascript">
        if (typeof($) === 'function') {
            // Because of the multiple angular apps on the page, bootstrap explicitly.
            $(function () {
                angular.module('SeamlessLPA').config(['$compileProvider', function ($compileProvider) {
                    $compileProvider.debugInfoEnabled(<%= AspxTools.JsBool(ConstAppDavid.CurrentServerLocation == ServerLocation.LocalHost) %>);
                }]);

                angular.bootstrap(angular.element('#app'), ['SeamlessLPA']);
            });
        }
    </script>
</body>
</html>
