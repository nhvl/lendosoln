﻿namespace LendersOffice.Integration.SeamlessLPA
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Conversions.LoanProspector;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LoanProductAdvisor;
    using ObjLib.Conversions.Aus;
    using ObjLib.Conversions.LoanProspector;
    using ObjLib.Conversions.LoanProspector.Seamless;
    using ObjLib.Conversions.Templates;

    /// <summary>
    /// Defines the server-side service methods for the Seamless LPA submission dialog in the LQB UI.
    /// </summary>
    /// <remarks> Also see the TPO portal version of this service page: LendOSoln\PML\main\seamlesslpa\SeamlessLpaService.aspx.cs .</remarks>
    public partial class SeamlessLpaService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Process the method called from AJAX.
        /// </summary>
        /// <param name="methodName">The name of the method to execute.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.RunAudit):
                    this.RunAudit();
                    break;
                case nameof(this.GetLoginModel):
                    this.GetLoginModel();
                    break;
                case nameof(this.Submit):
                    this.Submit();
                    break;
                case nameof(this.Poll):
                    this.Poll();
                    break;
                case nameof(this.UpdateLoan):
                    this.UpdateLoan();
                    break;
                default:
                    throw new ArgumentException($"Unknown method: {methodName}.");
            }
        }

        /// <summary>
        /// Run an audit on the loan file to determine if it's suitable for submission to DU.
        /// </summary>
        protected void RunAudit()
        {
            LoanAuditResult auditResult = 
                LoanDataAuditor.RunAudit(
                    this.GetGuid("LoanId"), 
                    LpaSeamlessAuditConditions.Singleton, 
                    isPml: false);
            this.SetResult("AuditResult", SerializationHelper.JsonNetSerialize(auditResult));
        }

        /// <summary>
        /// Retrieves the necessary data points for displaying the Seamless DU login page.
        /// </summary>
        protected void GetLoginModel()
        {
            var loanData = CPageData.CreateUsingSmartDependency(this.GetGuid("LoanId"), typeof(SeamlessLpaService));
            loanData.InitLoad();
            var loginModel = new
            {
                pagemodel = new LpaSeamlessLoginModel(loanData, PrincipalFactory.CurrentPrincipal),
                creditProviderOptions = Tools.FreddieMacCreditReportingCompanyMapping.Select(cra => new { Key = cra.Key, Value = cra.Value, category = "Main Credit Reporting Companies" }).Concat(
                    Tools.FreddieMacTechnicalAffiliateMapping.Select(cra => new { Key = cra.Key, Value = cra.Value, category = "Technical Affiliates" })),

                lpaCaseStates = Enum.GetValues(typeof(E_sFredProcPointT)).Cast<E_sFredProcPointT>()
                        .Select(enumValue => new KeyValuePair<int, string>((int)enumValue, enumValue.GetDescription())),

                // Serialize the CreditReportType enum in a way so that it can be easily used in the javascript. "creditReportOptions.OrderNew.description", "creditReportOptions.OrderNew.value" etc.
                creditReportOptions = Enum.GetValues(typeof(CreditReportType)).Cast<CreditReportType>().Except(new List<CreditReportType> { CreditReportType.Blank })
                        .ToDictionary(option => option.ToString("G"), option => new { value = (int)option, description = LpaSeamlessLoginModel.CreditReportTypeDescription(option, loanData.Apps.Any(app => !string.IsNullOrEmpty(app.aCreditReportId))) })
            };
            this.SetResult("LoginModel", SerializationHelper.JsonNetAnonymousSerialize(loginModel));
        }

        /// <summary>
        /// Submits an LPA underwriting request to the LPA system, using data from the LPA seamless login page.
        /// </summary>
        protected void Submit()
        {
            AbstractUserPrincipal user = PrincipalFactory.CurrentPrincipal;
            var model = SerializationHelper.JsonNetDeserialize<LpaSeamlessLoginModel>(GetString("model"));

            LoanAuditResult auditErrorResult;
            LpaResponseViewModel responseModel = LoanSubmitRequest.SaveOptionsAndSubmit(model, GetGuid("LoanId"), user, auditErrorResult: out auditErrorResult, isPml: false, prioritizeUiCredentials: false);
            this.SetResult("LpaResultsModel", SerializationHelper.JsonNetAnonymousSerialize(responseModel));
        }

        /// <summary>
        /// Determines whether a credit report exists to export to LPA.
        /// </summary>
        /// <param name="loanData">The loan file to determine if a credit report exists for.</param>
        /// <returns>Whether an existing credit report can be exported to LPA.</returns>
        private static bool CreditReportExists(CPageData loanData)
        {
            return loanData.Apps.Any(app => !string.IsNullOrEmpty(app.aCreditReportId)) && FileDBTools.DoesFileExist(E_FileDB.Normal, LoanSubmitRequest.FreddieCraKey(loanData.sLId));
        }

        /// <summary>
        /// Responds to a polling request from the client requesting an update on the LPA request's status.
        /// </summary>
        private void Poll()
        {
            var loanId = this.GetGuid("LoanId");
            var publicJobId = this.GetGuid("PublicJobId");
            LpaResponseViewModel responseModel = LoanSubmitRequest.Poll(publicJobId, loanId, isPml: false, principal: PrincipalFactory.CurrentPrincipal);
            this.SetResult("LpaResultsModel", SerializationHelper.JsonNetAnonymousSerialize(responseModel));
        }

        /// <summary>
        /// Updates the loan file with information from the last successful LPA response.
        /// </summary>
        private void UpdateLoan()
        {
            LoanSubmitRequest.ImportSubmissionResponse(this.GetGuid("LoanId"), this.GetBool("ImportFindings"), this.GetBool("ImportCreditReport"), this.GetBool("ImportLiabilities"));
        }
    }
}