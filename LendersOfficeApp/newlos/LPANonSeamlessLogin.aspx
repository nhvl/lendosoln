﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LPANonSeamlessLogin.aspx.cs" Inherits="LendersOfficeApp.newlos.LPANonSeamlessLogin" %>
<%@ Import namespace="LendersOffice.Constants" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Loan Product Advisor Login</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }
        .max500 {
            max-width: 500px;
        }
        .max600 {
            max-width: 600px;
        }
        .row {
            width: 100%;
            margin: 2px 0;
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            align-content: center;
        }
        .row > div {
            flex: 0 0 auto;
        }
        .MainContainer {
            padding: 8px;
            margin-bottom: 1em;
        }
        .indented {
            padding-left: 25px;
            box-sizing: border-box;
        }
        .space-after {
            margin-bottom: 0.5em;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
    $j(function($) {
        function sendToLPANonSeamless() {
            if (<%= AspxTools.JQuery(sFreddieLoanId)%>.val() === ML.Constant_PendingLoanTransaction) {
                showModal('/newlos/LoanProspectorSendConfirmation.aspx', null, null, null, function(args){ 
                    if (args.Choice == 0) {
                        return f_launch(ML.Constant_LPAGoToMain, true);
                    } else if (args.Choice == 1) {
                        // NO-OP
                    } else {
                        return false;
                    }
                });
            }
            if (f_validate()) {
              return f_launch(ML.Constant_SendToLPA, true);
            }
        }

        function f_displayMask(bDisplay) {
            f_displayBlockMask(bDisplay);
            window.top.treeview.f_displayBlockMask(bDisplay);
            window.top.info.f_displayBlockMask(bDisplay);

            window.top.header.f_disableCloseLink(bDisplay);
        }

        function f_detectLpWindowStatus() {
            if (null != g_lpWindow && g_lpWindow.closed) {
                f_displayMask(false);
                parent.f_load('FreddieExport.aspx?loanid=' + ML.sLId);
                g_lpWindow = null;
                return;
            }
            window.setTimeout(f_detectLpWindowStatus, ML.pollingInterval);
        }

        function f_validate()
        {
            var bMissingRequiredFields = false;
            var errMsg = '';
            if (<%= AspxTools.JQuery(sFreddieSellerNum)%>.val() === '')
            {
                var productName = 'Loan Product Advisor';
                errMsg = productName + ' Seller Number cannot be empty.\n';
                bMissingRequiredFields = true;      
            }
            if (!ML.hasLpPassword && <%= AspxTools.JQuery(sFreddieLpPassword)%>.val() === '')
            {
                var productName = 'Loan Product Advisor';
                errMsg = productName + ' Password cannot be empty.\n';
                bMissingRequiredFields = true;
            }
            if ($('#orderMergedCredit').prop('checked'))
            {
                if ($('#FreddieCrcDDL').val() === '')
                {
                  errMsg += '\nCredit Reporting Company cannot be empty.';
                  bMissingRequiredFields = true;
                }
            }
            if (bMissingRequiredFields) {
                alert(errMsg);
                return false;
            }
            return true;
        }

        function f_launch(entryPoint,bIsBlockEditor) {
            if (typeof(saveMe) === 'function') {
                var b = saveMe();
                if (b == false) {
                    return;
                }
            }

            var bMaximize = true;
            var bCenter = false;

            var w = 0;
            var h = 0;
            var left = 0;
            var top = 0;

            if (!bMaximize) {
                var preferedWidth = 1024;
                var preferedHeight = 768;
                w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
                h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
            } else {
                w = screen.availWidth - 10;
                h = screen.availHeight - 50;
            }

            // opm 228663 ejm - LP will not allow us to open their site within a frame on a page with a different domain. So instead we'll open the page that loads up the LP page. No frame.
            g_lpWindow = window.open('LoanProspectorMain.aspx?loanid=' + ML.sLId + '&entrypoint=' + entryPoint, 'ExportLP' + ML.sLId, 'width=' + w + ',height=' + h + ',left=' + left + ',top=' + top + ',menu=no,status=yes,location=no,resizable=yes,scrollbars=yes');
            if (null == g_lpWindow) {
                alert('Unable to open new window.  This may be caused by a pop-up blocker program on your computer.  Please contact Technical Support for assistance.');
            } else {
                if (bIsBlockEditor) {
                    f_displayMask(true);
                    f_detectLpWindowStatus();
                }
            }
            return false;
        }

        $('#orderMergedCredit, #orderInfileCredit').change(function () {
            var infileCreditChecked = $('#orderInfileCredit').prop('checked');
            $('#FreddieCrcDDL').prop('disabled', infileCreditChecked);
            $('.creditReferenceNumber').prop('readOnly', infileCreditChecked)
        });

        $('#sendToLpButton').click(function () {
            sendToLPANonSeamless();
        });

        $('#cancelButton').click(function () {
            window.parent.LQBPopup.Hide();
        });

        $j("#FreddieCrcDDL").val(ML.cra);
    });

    function _postGetAllFormValues(args)
    {
        args.FreddieCrcDDL = $j('#FreddieCrcDDL').val();
        $('.creditReferenceNumber').each(
            function(index, element) {
            args[$(element).data('borrowerId')] = element && element.value;
        });
    }
    </script>
    <form id="aspform" runat="server">
        <div>
            <div class="MainRightHeader">
                Loan Product Advisor - Request Options
            </div>
            <div class="MainContainer">
                <div class="space-after">
                    <div class="row max500">
                        <label class="FieldLabel" for="sFreddieLoanId">LPA Loan Id</label>
                        <div class="FieldValue"><asp:TextBox ID="sFreddieLoanId" runat="server"></asp:TextBox></div>
                    </div>
                    <div class="row max500">
                        <label class="FieldLabel" for="sFreddieTransactionId">LPA Transaction ID</label>
                        <div class="FieldValue"><asp:TextBox ID="sFreddieTransactionId" runat="server"></asp:TextBox></div>
                    </div>
                    <div class="row max500">
                        <label class="FieldLabel" for="sLpAusKey">LPA AUS Key Number</label>
                        <div class="FieldValue"><asp:TextBox ID="sLpAusKey" runat="server"></asp:TextBox></div>
                    </div>
                    <div class="row max500">
                        <label class="FieldLabel" for="sFreddieSellerNum">LPA Seller Number</label>
                        <div class="FieldValue"><asp:TextBox ID="sFreddieSellerNum" runat="server"></asp:TextBox></div>
                    </div>
                    <div class="row max500">
                        <ml:PassthroughLabel runat="server" class="FieldLabel" AssociatedControlID="sFreddieLpPassword">LPA Seller Password<br />(Leave blank unless changing)</ml:PassthroughLabel>
                        <div class="FieldValue"><asp:TextBox ID="sFreddieLpPassword" TextMode="Password" runat="server"></asp:TextBox></div>
                    </div>
                    <div class="row max500">
                        <label class="FieldLabel" for="sFreddieTpoNum">LPA TPO Number</label>
                        <div class="FieldValue"><asp:TextBox ID="sFreddieTpoNum" runat="server"></asp:TextBox></div>
                    </div>
                    <div class="row max500">
                        <label class="FieldLabel" for="sFreddieNotpNum">LPA NOTP Number</label>
                        <div class="FieldValue"><asp:TextBox ID="sFreddieNotpNum" runat="server"></asp:TextBox></div>
                    </div>
                    <div class="row max500">
                        <label class="FieldLabel" for="sFredProcPointT">Case State Type</label>
                        <div class="FieldValue"><asp:DropDownList ID="sFredProcPointT" runat="server"></asp:DropDownList></div>
                    </div>
                    <div class="row max500">
                        <label class="FieldLabel" for="sFreddieLenderBranchId">Lender Branch Identifier</label>
                        <div class="FieldValue"><asp:TextBox ID="sFreddieLenderBranchId" runat="server"></asp:TextBox></div>
                    </div>
                </div>
                <div class="space-after">
                    <div class="FieldLabel max500 space-after">
                        <asp:RadioButton id="orderMergedCredit" runat="server" value="Merged" Text="Order Merged Credit" GroupName="CreditChoice" />
                        <asp:RadioButton id="orderInfileCredit" runat="server" value="Infile" Text="Credit Infile" GroupName="CreditChoice" />
                    </div>
                    <div class="max500">
                        <div class="row indented">
                            <label class="FieldLabel" for="FreddieCrcDDL">Credit Reporting Company</label>
                            <div class="FieldValue">
                                <select id="FreddieCrcDDL">
                                    <option value=""></option>
                                    <optgroup label="Main Credit Reporting Companies">
                                        <asp:Repeater ID="MainCreditReportingCompanies" runat="server">
                                            <ItemTemplate>
                                            <option value=<%# AspxTools.HtmlAttribute((string)Eval("Key")) %> ><%# AspxTools.HtmlString(Eval("Value")) %></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </optgroup>
                                    <optgroup label="Technical Affiliates">
                                        <asp:Repeater ID="TechnicalAffiliates" runat="server">
                                            <ItemTemplate>
                                            <option value=<%# AspxTools.HtmlAttribute((string)Eval("Key")) %> ><%# AspxTools.HtmlString(Eval("Value")) %></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="FieldLabel indented">Reference Numbers:</div>
                        <asp:Repeater ID="borrowerCreditReferenceRepeater" runat="server">
                            <ItemTemplate>
                                <div class="row indented">
                                    <ml:EncodedLabel runat="server" CssClass="FieldLabel" AssociatedControlID="BorrowerReferenceNumber" ><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BorrowerName").ToString())%></ml:EncodedLabel>
                                    <asp:TextBox runat="server" ID="BorrowerReferenceNumber" CssClass="creditReferenceNumber" data-borrower-id='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BorrowerId").ToString()) %>' value=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Reference").ToString()) %>></asp:TextBox>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="FieldLabel space-after">
                        <label><input type="checkbox" id="cbRequestReorderCredit" name="cbRequestReorderCredit" /> Request Reorder Credit</label>
                    </div>
                    Please select the borrower(s) for whom you want the Reorder Credit Services.
                    <asp:Repeater ID="borrowerCreditReorderRepeater" runat="server">
                        <ItemTemplate>
                            <div class="row indented">
                                <label class="FieldLabel">
                                    <input type="checkbox" id=<%# AspxTools.HtmlAttribute("Reorder" + DataBinder.Eval(Container.DataItem, "BorrowerId").ToString())%> name=<%# AspxTools.HtmlAttribute("Reorder" + DataBinder.Eval(Container.DataItem, "BorrowerId").ToString())%> />
                                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BorrowerName").ToString())%>
                                </label>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="row" style="display: flex; justify-content: space-around;">
                <div>
                    <input type="button" id="sendToLpButton" style="width: 220px; margin-right: 15px" value="Send to Loan Product Advisor" />
                    <input type="button" id="cancelButton" value="Cancel" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
