﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using System.Web.Services;

namespace LendersOfficeApp.newlos
{
    public partial class WarehouseLenderRolodexInfo : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserWrite };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageID = "WarehouseLenderRolodexInfo";
            EnableJqueryMigrate = false;
            CPageData data = CPageData.CreateUsingSmartDependency(LoanID, typeof(WarehouseLenderRolodexInfo));
            data.InitLoad();

            var lenderInfo = WarehouseLenderRolodexEntry.Get(PrincipalFactory.CurrentPrincipal.BrokerId, data.sWarehouseLenderRolodexId);
            BindWarehouseLenderNames(data.sWarehouseLenderRolodexId);
            BindAdvanceClassifications(lenderInfo.AdvanceClassifications, data.sWarehouseAdvanceClassification);
            sWarehouseMaxFundPc.Text = data.sWarehouseMaxFundPc_rep;
            sWarehouseFunderDesc.Text = data.sWarehouseFunderDesc;
            sWarehouseLenderFannieMaePayeeId.Text = data.sWarehouseLenderFannieMaePayeeId;
            sWarehouseLenderFreddieMacPayeeId.Text = data.sWarehouseLenderFreddieMacPayeeId;
            this.sWarehouseLenderFannieMaeId.Text = data.sWarehouseLenderFannieMaeId;
            this.sWarehouseLenderFreddieMacId.Text = data.sWarehouseLenderFreddieMacId;
            sWarehouseLenderMersOrganizationId.Text = data.sWarehouseLenderMersOrganizationId;
            sWarehouseLenderPayToBankName.Text = data.sWarehouseLenderPayToBankName;
            sWarehouseLenderPayToCity.Text = data.sWarehouseLenderPayToCity;
            sWarehouseLenderPayToState.Value = data.sWarehouseLenderPayToState;
            sWarehouseLenderPayToABANum.Text = data.sWarehouseLenderPayToABANum.Value;
            sWarehouseLenderPayToAccountName.Text = data.sWarehouseLenderPayToAccountName;
            sWarehouseLenderPayToAccountNum.Text = data.sWarehouseLenderPayToAccountNum.Value;
            sWarehouseLenderFurtherCreditToAccountName.Text = data.sWarehouseLenderFurtherCreditToAccountName;
            sWarehouseLenderFurtherCreditToAccountNum.Text = data.sWarehouseLenderFurtherCreditToAccountNum.Value;
            sWarehouseLenderMainCompanyName.Text = data.sWarehouseLenderMainCompanyName;
            sWarehouseLenderMainContactName.Text = data.sWarehouseLenderMainContactName;
            sWarehouseLenderMainAttention.Text = data.sWarehouseLenderMainAttention;
            sWarehouseLenderMainEmail.Text = data.sWarehouseLenderMainEmail;
            sWarehouseLenderMainAddress.Text = data.sWarehouseLenderMainAddress;
            sWarehouseLenderMainCity.Text = data.sWarehouseLenderMainCity;
            sWarehouseLenderMainState.Value = data.sWarehouseLenderMainState;
            sWarehouseLenderMainZip.Text = data.sWarehouseLenderMainZip;
            sWarehouseLenderMainPhone.Text = data.sWarehouseLenderMainPhone;
            sWarehouseLenderMainFax.Text = data.sWarehouseLenderMainFax;
            sWarehouseLenderCollateralPackageToAttention.Text = data.sWarehouseLenderCollateralPackageToAttention;
            sWarehouseLenderCollateralPackageToContactName.Text = data.sWarehouseLenderCollateralPackageToContactName;
            sWarehouseLenderCollateralPackageToEmail.Text = data.sWarehouseLenderCollateralPackageToEmail;
            sWarehouseLenderCollateralPackageToAddress.Text = data.sWarehouseLenderCollateralPackageToAddress;
            sWarehouseLenderCollateralPackageToCity.Text = data.sWarehouseLenderCollateralPackageToCity;
            sWarehouseLenderCollateralPackageToState.Value = data.sWarehouseLenderCollateralPackageToState;
            sWarehouseLenderCollateralPackageToZip.Text = data.sWarehouseLenderCollateralPackageToZip;
            sWarehouseLenderCollateralPackageToPhone.Text = data.sWarehouseLenderCollateralPackageToPhone;
            sWarehouseLenderCollateralPackageToFax.Text = data.sWarehouseLenderCollateralPackageToFax;
            sWarehouseLenderClosingPackageToAttention.Text = data.sWarehouseLenderClosingPackageToAttention;
            sWarehouseLenderClosingPackageToContactName.Text = data.sWarehouseLenderClosingPackageToContactName;
            sWarehouseLenderClosingPackageToEmail.Text = data.sWarehouseLenderClosingPackageToEmail;
            sWarehouseLenderClosingPackageToAddress.Text = data.sWarehouseLenderClosingPackageToAddress;
            sWarehouseLenderClosingPackageToCity.Text = data.sWarehouseLenderClosingPackageToCity;
            sWarehouseLenderClosingPackageToState.Value = data.sWarehouseLenderClosingPackageToState;
            sWarehouseLenderClosingPackageToZip.Text = data.sWarehouseLenderClosingPackageToZip;
            sWarehouseLenderClosingPackageToPhone.Text = data.sWarehouseLenderClosingPackageToPhone;
            sWarehouseLenderClosingPackageToFax.Text = data.sWarehouseLenderClosingPackageToFax;
        }

        private void BindWarehouseLenderNames(int selected)
        {
            List<WarehouseLenderRolodexEntry> entries = WarehouseLenderRolodexEntry.GetAll(PrincipalFactory.CurrentPrincipal.BrokerId, null).ToList();
            sWarehouseLenderRolodexEntries.DataSource = entries.Where(p => p.Status == E_WarehouseLenderStatus.Active || p.Id == selected);
            sWarehouseLenderRolodexEntries.DataTextField = "WarehouseLenderName";
            sWarehouseLenderRolodexEntries.DataValueField = "Id";
            sWarehouseLenderRolodexEntries.DataBind();

            sWarehouseLenderRolodexEntries.Items.Insert(0, new ListItem("<-- Select a Warehouse Lender -->", "-1"));
            // 8/22/2013 GF - If the value for "Other" changes, ensure StandardFundingWorksheetPDF.cs
            // still loads the lender name correctly. OPM 135072
            sWarehouseLenderRolodexEntries.Items.Add(new ListItem("Other", "-2"));
            Tools.SetDropDownListValue(sWarehouseLenderRolodexEntries, selected);
        }

        private void BindAdvanceClassifications(List<WarehouseLenderAdvanceClassification> entries, string selected)
        {
            sAdvanceClassification.DataSource = entries.Select(
                    item => item.Description + " - " + item.MaxFundingPercent_rep
                );
            sAdvanceClassification.DataBind();
            sAdvanceClassification.Items.Insert(0, new ListItem("", ""));
            sAdvanceClassification.SelectedValue = selected;
        }

        [WebMethod]
        public static object GetWarehouseLenderInfo(int id)
        {
            return WarehouseLenderRolodexEntry.Get(PrincipalFactory.CurrentPrincipal.BrokerId, id);
        }


        [WebMethod]
        public static void Save(Guid sLId, int id, string desc, string advClass, string maxFundPc, int fileVersion)
        {
            CPageData data = CPageData.CreateUsingSmartDependency(sLId, typeof(WarehouseLenderRolodexInfo));
            data.InitSave(fileVersion);
            data.sWarehouseLenderRolodexId = id;
            data.sWarehouseLenderRolodexId = id;
            if (id >= 0 || id == -2)
                data.sWarehouseFunderDesc = desc;
            else if (id == -1)
                data.sWarehouseFunderDesc = string.Empty;

            data.sWarehouseMaxFundPc_rep = maxFundPc;
            data.sWarehouseAdvanceClassification = advClass;
            data.Save();
        }
    }
}
