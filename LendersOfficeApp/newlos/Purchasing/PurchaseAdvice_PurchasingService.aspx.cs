﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Purchasing
{
    public class PurchaseAdvice_PurchasingServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PurchaseAdvice_PurchasingServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "PullDetailsFromLoanFile":
                    PullDetailsFromLoanFile();
                    break;
                default:
                    break;
            }
        }

        private void PullDetailsFromLoanFile()
        {
            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitLoad();
            dataLoan.ApplyLoanDataToPurchaseAdvice_PurchasingPage();
            LoadData(dataLoan, null);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {

            dataLoan.sPurchaseAdviceSummaryOtherAdjDesc_Purchasing = GetString("sPurchaseAdviceSummaryOtherAdjDesc_Purchasing");
            dataLoan.sPurchaseAdviceSummaryOtherAdjVal_Purchasing_rep = GetString("sPurchaseAdviceSummaryOtherAdjVal_Purchasing");
            dataLoan.sPurchaseAdviceSummaryInvNm_PurchasingLckd = GetBool("sPurchaseAdviceSummaryInvNm_PurchasingLckd");
            dataLoan.sPurchaseAdviceSummaryInvNm_Purchasing = GetString("sPurchaseAdviceSummaryInvNm_Purchasing");
            dataLoan.sPurchaseAdviceSummaryInvPgNm_PurchasingLckd = GetBool("sPurchaseAdviceSummaryInvPgNm_PurchasingLckd");
            dataLoan.sPurchaseAdviceSummaryInvPgNm_Purchasing = GetString("sPurchaseAdviceSummaryInvPgNm_Purchasing");
            dataLoan.sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd = GetBool("sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd");
            dataLoan.sPurchaseAdviceSummaryInvLoanNm_Purchasing = GetString("sPurchaseAdviceSummaryInvLoanNm_Purchasing");
            dataLoan.sPurchaseAdviceSummaryServicingStatus_Purchasing = GetString("sPurchaseAdviceSummaryServicingStatus_Purchasing");
            dataLoan.sClosedD_rep = GetString("sClosedD");
            dataLoan.sPurchasedD_rep = GetString("sPurchasedD");
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");
            dataLoan.sInvSchedDueD1_Purchasing_rep = GetString("sInvSchedDueD1_Purchasing");

            dataLoan.sPurchaseAdviceAmortCurtail_Purchasing_rep = GetString("sPurchaseAdviceAmortCurtail_Purchasing");
            //dataLoan.CalculateBasePriceFields(GetInt("numFixedField"), GetString("sPurchaseAdviceBasePrice_Field1_Purchasing"), GetString("sPurchaseAdviceBasePrice_Field2_Purchasing"), GetString("sPurchaseAdviceBasePrice_Field3_Purchasing"));
            dataLoan.sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing = (E_AmountPriceCalcMode)GetInt("sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing");
            dataLoan.sPurchaseAdviceBasePrice_Field1_Purchasing_rep = GetString("sPurchaseAdviceBasePrice_Field1_Purchasing");
            dataLoan.sPurchaseAdviceBasePrice_Field2_Purchasing_rep = GetString("sPurchaseAdviceBasePrice_Field2_Purchasing");
            dataLoan.sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing_rep = GetString("sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing");
            dataLoan.sPurchaseAdviceInterestReimbursement_Purchasing_rep = GetString("sPurchaseAdviceInterestReimbursement_Purchasing");

            dataLoan.sPurchaseAdviceEscrowTotCollAtClosing_Purchasing_rep = GetString("sPurchaseAdviceEscrowTotCollAtClosing_Purchasing");
            dataLoan.sPurchaseAdviceEscrowDisbursements_Purchasing_rep = GetString("sPurchaseAdviceEscrowDisbursements_Purchasing");
            dataLoan.sPurchaseAdviceEscrowDepDue_Purchasing_rep = GetString("sPurchaseAdviceEscrowDepDue_Purchasing");

            // 9/3/2013 AV - 64100 Purchase Advice Base Loan Amount -- This needs to be set last because it depends on the fields above 
            dataLoan.sPurchaseAdviceFees_Purchasing = ObsoleteSerializationHelper.JsonDeserialize<List<CPurchaseAdviceFeesFields>>(GetString("sFees"));
            dataLoan.sPurchaseAdviceAdjustments_Purchasing = ObsoleteSerializationHelper.JsonDeserialize<List<CPurchaseAdviceAdjustmentsFields>>(GetString("sAdjustments"));

        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            foreach (CPurchaseAdviceFeesFields fee in dataLoan.sPurchaseAdviceFees_Purchasing)
            {
                int rowNum = fee.RowNum;
                SetResult("FeeDesc" + rowNum, fee.FeeDesc);
                SetResult("FeeAmt" + rowNum, fee.FeeAmt_rep);
            }

            foreach (CPurchaseAdviceAdjustmentsFields adjustment in dataLoan.sPurchaseAdviceAdjustments_Purchasing)
            {
                int rowNum = adjustment.RowNum;
                SetResult("AdjDesc" + rowNum, adjustment.AdjDesc);
                SetResult("AdjPc" + rowNum, adjustment.AdjPc_rep);
                SetResult("AdjAmt" + rowNum, adjustment.AdjAmt_rep);
                SetResult("IsSRP" + rowNum, adjustment.IsSRP);
            }
            SetResult("adjustmentTableSize", dataLoan.sPurchaseAdviceAdjustments_Purchasing.Count);

            SetResult("sPurchaseAdviceUnpaidPBal_Purchasing2", dataLoan.sPurchaseAdviceUnpaidPBal_Purchasing_rep);
            SetResult("sPurchaseAdviceTotalPrice_Field2_Purchasing2", dataLoan.sPurchaseAdviceTotalPrice_Field2_Purchasing_rep);
            SetResult("sPurchaseAdviceInterestTotalAdj_Purchasing2", dataLoan.sPurchaseAdviceInterestTotalAdj_Purchasing_rep);
            SetResult("sPurchaseAdviceEscrowAmtDueInv_Purchasing2", dataLoan.sPurchaseAdviceEscrowAmtDueInv_Purchasing_rep);
            SetResult("sPurchaseAdviceSummaryOtherAdjDesc_Purchasing", dataLoan.sPurchaseAdviceSummaryOtherAdjDesc_Purchasing);
            SetResult("sPurchaseAdviceSummaryOtherAdjVal_Purchasing", dataLoan.sPurchaseAdviceSummaryOtherAdjVal_Purchasing_rep);
            SetResult("sPurchaseAdviceSummaryInvNm_PurchasingLckd", dataLoan.sPurchaseAdviceSummaryInvNm_PurchasingLckd);
            SetResult("sPurchaseAdviceSummaryInvNm_Purchasing", dataLoan.sPurchaseAdviceSummaryInvNm_Purchasing);
            SetResult("sPurchaseAdviceSummaryInvPgNm_PurchasingLckd", dataLoan.sPurchaseAdviceSummaryInvPgNm_PurchasingLckd);
            SetResult("sPurchaseAdviceSummaryInvPgNm_Purchasing", dataLoan.sPurchaseAdviceSummaryInvPgNm_Purchasing);
            SetResult("sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd", dataLoan.sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd);
            SetResult("sPurchaseAdviceSummaryInvLoanNm_Purchasing", dataLoan.sPurchaseAdviceSummaryInvLoanNm_Purchasing);
            SetResult("sPurchaseAdviceSummaryServicingStatus_Purchasing", dataLoan.sPurchaseAdviceSummaryServicingStatus_Purchasing);
            SetResult("sPurchaseAdviceFeesTotal2", dataLoan.sPurchaseAdviceFeesTotal_Purchasing_rep);
            SetResult("sPurchaseAdviceSummarySubtotal_Purchasing", dataLoan.sPurchaseAdviceSummarySubtotal_Purchasing_rep);
            SetResult("sPurchaseAdviceSummaryTotalDueSeller_Purchasing", dataLoan.sPurchaseAdviceSummaryTotalDueSeller_Purchasing_rep);
            SetResult("sClosedD", dataLoan.sClosedD_rep);
            SetResult("sPurchasedD", dataLoan.sPurchasedD_rep);
            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
            SetResult("sInvSchedDueD1_Purchasing", dataLoan.sInvSchedDueD1_Purchasing_rep);

            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sPurchaseAdviceAmortCurtail_Purchasing", dataLoan.sPurchaseAdviceAmortCurtail_Purchasing_rep);
            SetResult("sPurchaseAdviceUnpaidPBal_Purchasing", dataLoan.sPurchaseAdviceUnpaidPBal_Purchasing_rep);
            SetResult("sPurchaseAdviceBasePrice_Field1_Purchasing", dataLoan.sPurchaseAdviceBasePrice_Field1_Purchasing_rep);
            SetResult("sPurchaseAdviceBasePrice_Field2_Purchasing", dataLoan.sPurchaseAdviceBasePrice_Field2_Purchasing_rep);
            SetResult("sPurchaseAdviceBasePrice_Field3_Purchasing", dataLoan.sPurchaseAdviceBasePrice_Field3_Purchasing_rep);
            SetResult("sPurchaseAdviceAdjustments_Field2_Purchasing", dataLoan.sPurchaseAdviceAdjustments_Field2_Purchasing_rep);
            SetResult("sPurchaseAdviceAdjustments_Field3_Purchasing", dataLoan.sPurchaseAdviceAdjustments_Field3_Purchasing_rep);
            SetResult("sPurchaseAdviceNetPrice_Field1_Purchasing", dataLoan.sPurchaseAdviceNetPrice_Field1_Purchasing_rep);
            SetResult("sPurchaseAdviceNetPrice_Field2_Purchasing", dataLoan.sPurchaseAdviceNetPrice_Field2_Purchasing_rep);
            SetResult("sPurchaseAdviceNetPrice_Field3_Purchasing", dataLoan.sPurchaseAdviceNetPrice_Field3_Purchasing_rep);
            SetResult("sPurchaseAdviceSRP_Field2_Purchasing", dataLoan.sPurchaseAdviceSRP_Field2_Purchasing_rep);
            SetResult("sPurchaseAdviceSRP_Field3_Purchasing", dataLoan.sPurchaseAdviceSRP_Field3_Purchasing_rep);
            SetResult("sPurchaseAdviceTotalPrice_Field1_Purchasing", dataLoan.sPurchaseAdviceTotalPrice_Field1_Purchasing_rep);
            SetResult("sPurchaseAdviceTotalPrice_Field2_Purchasing", dataLoan.sPurchaseAdviceTotalPrice_Field2_Purchasing_rep);
            SetResult("sPurchaseAdviceTotalPrice_Field3_Purchasing", dataLoan.sPurchaseAdviceTotalPrice_Field3_Purchasing_rep);

            SetResult("sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing", dataLoan.sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing_rep);
            SetResult("sPurchaseAdviceInterestReimbursement_Purchasing", dataLoan.sPurchaseAdviceInterestReimbursement_Purchasing_rep);
            SetResult("sPurchaseAdviceInterestTotalAdj_Purchasing", dataLoan.sPurchaseAdviceInterestTotalAdj_Purchasing_rep);

            SetResult("sPurchaseAdviceEscrowTotCollAtClosing_Purchasing", dataLoan.sPurchaseAdviceEscrowTotCollAtClosing_Purchasing_rep);
            SetResult("sPurchaseAdviceEscrowDisbursements_Purchasing", dataLoan.sPurchaseAdviceEscrowDisbursements_Purchasing_rep);
            SetResult("sPurchaseAdviceEscrowDepDue_Purchasing", dataLoan.sPurchaseAdviceEscrowDepDue_Purchasing_rep);
            SetResult("sPurchaseAdviceEscrowAmtDueInv_Purchasing", dataLoan.sPurchaseAdviceEscrowAmtDueInv_Purchasing_rep);

            SetResult("sPurchaseAdviceFeesTotal_Purchasing", dataLoan.sPurchaseAdviceFeesTotal_Purchasing_rep);
        }
    }
    public partial class PurchaseAdvice_PurchasingService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new PurchaseAdvice_PurchasingServiceItem());
        }
    }


}
