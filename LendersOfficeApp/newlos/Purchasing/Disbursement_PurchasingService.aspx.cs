﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;

namespace LendersOfficeApp.newlos.Purchasing
{
    public class Disbursement_PurchasingServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(Disbursement_PurchasingServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sPurchasingDisbursementFundsTo = GetString("sPurchasingDisbursementFundsTo");
            dataLoan.sPurchasingDisbursementD_rep = GetString("sPurchasingDisbursementD");
            dataLoan.sPurchasingDisbursementNotes = GetString("sPurchasingDisbursementNotes");
            dataLoan.sPurchasingDisbursementBankName = GetString("sPurchasingDisbursementBankName");
            dataLoan.sPurchasingDisbursementBankCityState = GetString("sPurchasingDisbursementBankCityState");
            dataLoan.sPurchasingDisbursementABANumber = GetString("sPurchasingDisbursementABANumber");
            dataLoan.sPurchasingDisbursementAccountName = GetString("sPurchasingDisbursementAccountName");
            dataLoan.sPurchasingDisbursementAccountNumber = GetString("sPurchasingDisbursementAccountNumber");
            dataLoan.sPurchasingDisbursementFurtherCreditToAccountName = GetString("sPurchasingDisbursementFurtherCreditToAccountName");
            dataLoan.sPurchasingDisbursementFurtherCreditToAccountNumber = GetString("sPurchasingDisbursementFurtherCreditToAccountNumber");
            dataLoan.sPurchasingDisbursementAdditionalInstructionsLine1 = GetString("sPurchasingDisbursementAdditionalInstructionsLine1");
            dataLoan.sPurchasingDisbursementAdditionalInstructionsLine2 = GetString("sPurchasingDisbursementAdditionalInstructionsLine2");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sPurchaseAdviceSummaryTotalDueSeller_Purchasing", dataLoan.sPurchaseAdviceSummaryTotalDueSeller_Purchasing_rep);
            SetResult("sPurchasingDisbursementFundsTo", dataLoan.sPurchasingDisbursementFundsTo.Value);
            SetResult("sPurchasingDisbursementD", dataLoan.sPurchasingDisbursementD_rep);
            SetResult("sPurchasingDisbursementNotes", dataLoan.sPurchasingDisbursementNotes);
            SetResult("sPurchasingDisbursementBankName", dataLoan.sPurchasingDisbursementBankName);
            SetResult("sPurchasingDisbursementBankCityState", dataLoan.sPurchasingDisbursementBankCityState);
            SetResult("sPurchasingDisbursementABANumber", dataLoan.sPurchasingDisbursementABANumber.Value);
            SetResult("sPurchasingDisbursementAccountName", dataLoan.sPurchasingDisbursementAccountName);
            SetResult("sPurchasingDisbursementAccountNumber", dataLoan.sPurchasingDisbursementAccountNumber.Value);
            SetResult("sPurchasingDisbursementFurtherCreditToAccountName", dataLoan.sPurchasingDisbursementFurtherCreditToAccountName);
            SetResult("sPurchasingDisbursementFurtherCreditToAccountNumber", dataLoan.sPurchasingDisbursementFurtherCreditToAccountNumber.Value);
            SetResult("sPurchasingDisbursementAdditionalInstructionsLine1", dataLoan.sPurchasingDisbursementAdditionalInstructionsLine1.Value);
            SetResult("sPurchasingDisbursementAdditionalInstructionsLine2", dataLoan.sPurchasingDisbursementAdditionalInstructionsLine2.Value);
        }
    }
    public partial class Disbursement_PurchasingService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new Disbursement_PurchasingServiceItem());
        }
    }


}
