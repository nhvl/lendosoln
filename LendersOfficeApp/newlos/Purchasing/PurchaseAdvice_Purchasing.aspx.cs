﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;
using LendersOffice.ObjLib.Rolodex;

namespace LendersOfficeApp.newlos.Purchasing
{
    public partial class PurchaseAdvice_Purchasing : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantRead };
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(PurchaseAdvice_Purchasing));
            dataLoan.InitLoad();

            sPurchaseAdviceUnpaidPBal_Purchasing2.Text = dataLoan.sPurchaseAdviceUnpaidPBal_Purchasing_rep;
            sPurchaseAdviceTotalPrice_Field2_Purchasing2.Text = dataLoan.sPurchaseAdviceTotalPrice_Field2_Purchasing_rep;
            sPurchaseAdviceInterestTotalAdj_Purchasing2.Text = dataLoan.sPurchaseAdviceInterestTotalAdj_Purchasing_rep;
            sPurchaseAdviceEscrowAmtDueInv_Purchasing2.Text = dataLoan.sPurchaseAdviceEscrowAmtDueInv_Purchasing_rep;
            sPurchaseAdviceSummaryOtherAdjDesc_Purchasing.Text = dataLoan.sPurchaseAdviceSummaryOtherAdjDesc_Purchasing;
            sPurchaseAdviceSummaryOtherAdjVal_Purchasing.Text = dataLoan.sPurchaseAdviceSummaryOtherAdjVal_Purchasing_rep;
            sPurchaseAdviceSummaryInvNm_PurchasingLckd.Checked = dataLoan.sPurchaseAdviceSummaryInvNm_PurchasingLckd;
            sPurchaseAdviceSummaryInvNm_Purchasing.Text = dataLoan.sPurchaseAdviceSummaryInvNm_Purchasing;
            sPurchaseAdviceSummaryInvPgNm_PurchasingLckd.Checked = dataLoan.sPurchaseAdviceSummaryInvPgNm_PurchasingLckd;
            sPurchaseAdviceSummaryInvPgNm_Purchasing.Text = dataLoan.sPurchaseAdviceSummaryInvPgNm_Purchasing;
            sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd.Checked = dataLoan.sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd;
            sPurchaseAdviceSummaryInvLoanNm_Purchasing.Text = dataLoan.sPurchaseAdviceSummaryInvLoanNm_Purchasing;
            sPurchaseAdviceFeesTotal_Purchasing2.Text = dataLoan.sPurchaseAdviceFeesTotal_Purchasing_rep;
            sPurchaseAdviceSummarySubtotal_Purchasing.Text = dataLoan.sPurchaseAdviceSummarySubtotal_Purchasing_rep;
            sPurchaseAdviceSummaryTotalDueSeller_Purchasing.Text = dataLoan.sPurchaseAdviceSummaryTotalDueSeller_Purchasing_rep;
            sClosedD.Text = dataLoan.sClosedD_rep;
            sPurchasedD.Text = dataLoan.sPurchasedD_rep;
            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sInvSchedDueD1_Purchasing.Text = dataLoan.sInvSchedDueD1_Purchasing_rep;
            Tools.SetDropDownListValue(sPurchaseAdviceSummaryServicingStatus_Purchasing, dataLoan.sPurchaseAdviceSummaryServicingStatus_Purchasing);

            switch (dataLoan.sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing)
            {
                case E_AmountPriceCalcMode.Amount:
                    sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing_Amount.Checked = true;
                    break;
                case E_AmountPriceCalcMode.Price:
                    sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing_Price.Checked = true;
                    break;
                default:

                    throw new UnhandledEnumException(dataLoan.sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing);
            }

            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sPurchaseAdviceAmortCurtail_Purchasing.Text = dataLoan.sPurchaseAdviceAmortCurtail_Purchasing_rep;
            sPurchaseAdviceUnpaidPBal_Purchasing.Text = dataLoan.sPurchaseAdviceUnpaidPBal_Purchasing_rep;
            sPurchaseAdviceBasePrice_Field1_Purchasing.Text = dataLoan.sPurchaseAdviceBasePrice_Field1_Purchasing_rep;
            sPurchaseAdviceBasePrice_Field2_Purchasing.Text = dataLoan.sPurchaseAdviceBasePrice_Field2_Purchasing_rep;
            sPurchaseAdviceBasePrice_Field3_Purchasing.Text = dataLoan.sPurchaseAdviceBasePrice_Field3_Purchasing_rep;
            sPurchaseAdviceAdjustments_Field2_Purchasing.Text = dataLoan.sPurchaseAdviceAdjustments_Field2_Purchasing_rep;
            sPurchaseAdviceAdjustments_Field3_Purchasing.Text = dataLoan.sPurchaseAdviceAdjustments_Field3_Purchasing_rep;
            sPurchaseAdviceNetPrice_Field1_Purchasing.Text = dataLoan.sPurchaseAdviceNetPrice_Field1_Purchasing_rep;
            sPurchaseAdviceNetPrice_Field2_Purchasing.Text = dataLoan.sPurchaseAdviceNetPrice_Field2_Purchasing_rep;
            sPurchaseAdviceNetPrice_Field3_Purchasing.Text = dataLoan.sPurchaseAdviceNetPrice_Field3_Purchasing_rep;
            sPurchaseAdviceSRP_Field2_Purchasing.Text = dataLoan.sPurchaseAdviceSRP_Field2_Purchasing_rep;
            sPurchaseAdviceSRP_Field3_Purchasing.Text = dataLoan.sPurchaseAdviceSRP_Field3_Purchasing_rep;
            sPurchaseAdviceTotalPrice_Field1_Purchasing.Text = dataLoan.sPurchaseAdviceTotalPrice_Field1_Purchasing_rep;
            sPurchaseAdviceTotalPrice_Field2_Purchasing.Text = dataLoan.sPurchaseAdviceTotalPrice_Field2_Purchasing_rep;
            sPurchaseAdviceTotalPrice_Field3_Purchasing.Text = dataLoan.sPurchaseAdviceTotalPrice_Field3_Purchasing_rep;

            sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing.Text = dataLoan.sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing_rep;
            sPurchaseAdviceInterestReimbursement_Purchasing.Text = dataLoan.sPurchaseAdviceInterestReimbursement_Purchasing_rep;
            sPurchaseAdviceInterestTotalAdj_Purchasing.Text = dataLoan.sPurchaseAdviceInterestTotalAdj_Purchasing_rep;

            sPurchaseAdviceEscrowTotCollAtClosing_Purchasing.Text = dataLoan.sPurchaseAdviceEscrowTotCollAtClosing_Purchasing_rep;
            sPurchaseAdviceEscrowDisbursements_Purchasing.Text = dataLoan.sPurchaseAdviceEscrowDisbursements_Purchasing_rep;
            sPurchaseAdviceEscrowDepDue_Purchasing.Text = dataLoan.sPurchaseAdviceEscrowDepDue_Purchasing_rep;
            sPurchaseAdviceEscrowAmtDueInv_Purchasing.Text = dataLoan.sPurchaseAdviceEscrowAmtDueInv_Purchasing_rep;

            RegisterJsObject("Adjustments", dataLoan.sPurchaseAdviceAdjustments_Purchasing);
            RegisterJsObject("Fees", dataLoan.sPurchaseAdviceFees_Purchasing);
            sPurchaseAdviceFeesTotal_Purchasing.Text = dataLoan.sPurchaseAdviceFeesTotal_Purchasing_rep;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Purchase Advice - Purchasing";
            this.PageID = "PurchaseAdvice_Purchasing";

            RegisterJsScript("DynamicTable.js");

            if (!(BrokerUser.HasPermission(Permission.AllowLockDeskRead) &&
                BrokerUser.HasPermission(Permission.AllowCloserRead)))
                m_pullLoanDetailsBtn.Disabled = true;

            Tools.Bind_PurchaseAdviceServicingStatus(sPurchaseAdviceSummaryServicingStatus_Purchasing);

        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            EnableJqueryMigrate = false;

            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
