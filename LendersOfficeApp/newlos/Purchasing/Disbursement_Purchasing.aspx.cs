﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Purchasing
{
    public partial class Disbursement_Purchasing : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite, Permission.AllowAccountantRead };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead, Permission.AllowAccountantRead };
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Disbursement_Purchasing));
            dataLoan.InitLoad();

            sPurchaseAdviceSummaryTotalDueSeller_Purchasing.Text = dataLoan.sPurchaseAdviceSummaryTotalDueSeller_Purchasing_rep;
            sPurchasingDisbursementFundsTo.Text = dataLoan.sPurchasingDisbursementFundsTo.Value;

            sPurchasingDisbursementD.Text = dataLoan.sPurchasingDisbursementD_rep;
            sPurchasingDisbursementNotes.Text = dataLoan.sPurchasingDisbursementNotes;

            sPurchasingDisbursementBankName.Text = dataLoan.sPurchasingDisbursementBankName;
            sPurchasingDisbursementBankCityState.Text = dataLoan.sPurchasingDisbursementBankCityState;
            sPurchasingDisbursementABANumber.Text = dataLoan.sPurchasingDisbursementABANumber.Value;
            sPurchasingDisbursementAccountName.Text = dataLoan.sPurchasingDisbursementAccountName;
            sPurchasingDisbursementAccountNumber.Text = dataLoan.sPurchasingDisbursementAccountNumber.Value;
            sPurchasingDisbursementFurtherCreditToAccountName.Text = dataLoan.sPurchasingDisbursementFurtherCreditToAccountName;
            sPurchasingDisbursementFurtherCreditToAccountNumber.Text = dataLoan.sPurchasingDisbursementFurtherCreditToAccountNumber.Value;

            sPurchasingDisbursementAdditionalInstructionsLine1.Text = dataLoan.sPurchasingDisbursementAdditionalInstructionsLine1.Value;
            sPurchasingDisbursementAdditionalInstructionsLine2.Text = dataLoan.sPurchasingDisbursementAdditionalInstructionsLine2.Value;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Disbursement - Purchasing";
            this.PageID = "Disbursement_Purchasing";

            InitContactFieldMapper();

            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        private void InitContactFieldMapper()
        {
            CFM.Type = E_AgentRoleT.LoanPurchasePayee.ToString("D"); // "62"
            CFM.PayToBankNameField = sPurchasingDisbursementBankName.ClientID;
            CFM.PayToBankCityStateField = sPurchasingDisbursementBankCityState.ClientID;
            CFM.PayToABANumberField = sPurchasingDisbursementABANumber.ClientID;
            CFM.PayToAccountNameField = sPurchasingDisbursementAccountName.ClientID;
            CFM.PayToAccountNumberField = sPurchasingDisbursementAccountNumber.ClientID;
            CFM.FurtherCreditToAccountNumberField = sPurchasingDisbursementFurtherCreditToAccountName.ClientID;
            CFM.FurtherCreditToAccountNameField = sPurchasingDisbursementFurtherCreditToAccountNumber.ClientID;
        }
    }
}
