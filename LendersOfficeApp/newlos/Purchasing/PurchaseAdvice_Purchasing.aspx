﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="PurchaseAdvice_Purchasing.aspx.cs" Inherits="LendersOfficeApp.newlos.Purchasing.PurchaseAdvice_Purchasing" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Purchase Advice - Purchasing</title>
    <style type="text/css">
        .BorderTop { border-top:thin groove; }
        .BorderLeft { border-left:thin groove; }
        .BorderRight { border-right:thin groove; }
        .BorderBottom { border-bottom:thin groove; }
        .SectionHeader { color:navy; font-size:small; }
	    .ButtonStyle { overflow: visible; width: auto; padding: 2px;}
        .rateField { width: 65px; }
        .moneyFieldAdj { width: 80px; }
	    .descriptionFieldAdj { width: 270px; }
        .moneyFieldFee { width: 100px; }
	    .descriptionFieldFee { width: 364px; }
	    .PlayNiceWithLockLbl { display: inline-block; float: right; width: 10%; }
	    .PlayNiceWithLockCB { display: inline-block; float: left; width: 90%; padding-top: 2px; }
    </style>
</head>
<body bgcolor="gainsboro">    
    <script type="text/javascript" src="../../inc/AdjustmentTable.js"></script>
    <script type="text/javascript">
        var m_basePriceFixedField = 1;
        var FeesTable = {

            currentIndex: 0,

            tableData: null,

            tableName: 'FeesTable',

            deleteRowBtn: 'btn_deleteFees',
            
            rowsPerItem: 1,

            selectAllCheckbox: 'm_deleteFeesCheckbox',

            deleteRow: function(input) {
                input.parent().parent().remove();
            },

            __createAndAppendRow: function(index, data, newRow) {
                var oTBody, oTR, oTD, oInput;
                oTBody = this.tableData;

                var name_cb = 'DeleteEntry' + index;
                oTR = DynamicTable.__createAndAppendElement(oTBody, 'tr', {
                    'ItemIndex': index
                });
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_cb,
                    'name': name_cb,
                    'type': 'checkbox',
                    'DeleteEntry': index,
                    'NotForEdit': 'true'
                });
                    addEventHandler(oInput, 'click', function() { DynamicTable.uncheckDeleteAllCheckbox(FeesTable); }, false);

                var name_des = 'FeeDesc' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_des,
                    'name': name_des,
                    'value': newRow ? '' : data.FeeDesc,
                    'type': 'text',
                    'className': 'descriptionFieldFee'
                });

                var name_amt = 'FeeAmt' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_amt,
                    'name': name_amt,
                    'value': newRow ? '' : data.FeeAmt_rep,
                    'type': 'text',
                    'preset': 'money',
                    'className': 'moneyFieldFee'
                });
                addEventHandler(oInput, 'change', update, false);
            },

            serialize: function(index) {
                return {
                    RowNum: AdjustmentTable.__IE8NullFix(index),
                    FeeDesc: AdjustmentTable.__IE8NullFix($('#FeeDesc' + index).val()),
                    FeeAmt_rep: AdjustmentTable.__IE8NullFix($('#FeeAmt' + index).val())
                };
            }
        };

        var AdjustmentsTable = {

            currentIndex: 0,

            tableData: null,

            tableName: 'AdjustmentsTable',

            deleteRowBtn: 'btn_deleteAdjustments',

            rowsPerItem: 1,

            selectAllCheckbox: 'm_deleteAdjustmentsCheckbox',

            deleteRow: function(input) {
                input.parent().parent().remove();
            },

            __createAndAppendRow: function(index, data, newRow) {
                var oTBody, oTR, oTD, oInput;
                oTBody = this.tableData;

                var name_cb = 'DeleteEntry' + index;
                oTR = DynamicTable.__createAndAppendElement(oTBody, 'tr', {
                    'ItemIndex': index
                });
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_cb,
                    'name': name_cb,
                    'type': 'checkbox',
                    'DeleteEntry': index,
                    'NotForEdit': 'true'
                });
                    addEventHandler(oInput, 'click', function() { DynamicTable.uncheckDeleteAllCheckbox(AdjustmentsTable); }, false);

                var name_lcu = 'LastColumnUpdated' + index;
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_lcu,
                    'name': name_lcu,
                    'type': 'hidden',
                    'value': <%= AspxTools.JsNumeric(DataAccess.CPageBase.PURCHADV_ADJ_OTHER) %>
                });
                
                var name_des = 'AdjDesc' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_des,
                    'name': name_des,
                    'value': newRow ? '' : data.AdjDesc,
                    'type': 'text',
                    'className': 'descriptionFieldAdj'
                });
                addEventHandler(oInput, 'change', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.PURCHADV_ADJ_OTHER) %>);}, false);

                var name_pc = 'AdjPc' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_pc,
                    'name': name_pc,
                    'value': newRow ? '' : data.AdjPc_rep,
                    'type': 'text',
                    'preset': 'percent',
                    'className': 'rateField Price10',
                    'decimalDigits': '6'
                });
                addEventHandler(oInput, 'change', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.PURCHADV_ADJ_PC) %>);}, false);
                
                var name_amt = 'AdjAmt' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_amt,
                    'name': name_amt,
                    'value': newRow ? '' : data.AdjAmt_rep,
                    'type': 'text',
                    'preset': 'money',
                    'className': 'moneyFieldAdj Amt10'
                });
                addEventHandler(oInput, 'change', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.PURCHADV_ADJ_AMT) %>);}, false);
                
                var name_srp = 'IsSRP' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {
                    'align': 'center'
                });
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_srp,
                    'name': name_srp,
                    'type': 'checkbox'
                });
                addEventHandler(oInput, 'click', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.PURCHADV_ADJ_OTHER) %>);}, false);
                if (!newRow && data.IsSRP)
                    oInput.setAttribute('checked', 'checked');
            },

            serialize: function(index) {
                return {
                    RowNum: AdjustmentTable.__IE8NullFix(index),
                    AdjDesc: AdjustmentTable.__IE8NullFix($('#AdjDesc' + index).val()),
                    AdjPc_rep: AdjustmentTable.__IE8NullFix($('#AdjPc' + index).val()),
                    AdjAmt_rep: AdjustmentTable.__IE8NullFix($('#AdjAmt' + index).val()),
                    IsSRP: AdjustmentTable.__IE8NullFix($('#IsSRP' + index).attr('checked')),
                    LastColumnUpdated: AdjustmentTable.__IE8NullFix($('#LastColumnUpdated' + index).val())
                };
            }
        };
        var g_bIsInit = false;
        function _init() {
            if (g_bIsInit == false) {
              // 8/25/2010 dd - Only execute this code when page first load.
              DynamicTable.instantiateTable(FeesTable, Fees);
              DynamicTable.instantiateTable(AdjustmentsTable, Adjustments);
            } 
            lockField(<%=AspxTools.JsGetElementById(sSchedDueD1Lckd)%>, 'sSchedDueD1');
            
            
            lockField(<%=AspxTools.JsGetElementById(sPurchaseAdviceSummaryInvNm_PurchasingLckd)%>, 'sPurchaseAdviceSummaryInvNm_Purchasing');
            lockField(<%=AspxTools.JsGetElementById(sPurchaseAdviceSummaryInvPgNm_PurchasingLckd)%>, 'sPurchaseAdviceSummaryInvPgNm_Purchasing');
            lockField(<%=AspxTools.JsGetElementById(sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd)%>, 'sPurchaseAdviceSummaryInvLoanNm_Purchasing');
            
            
            
            
            g_bIsInit = true;
            updatePurchaseAdvice();
            
            $('#sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing_Price,#sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing_Amount').click(update);
            //clearDirty();
        }

        function addRow(table) {
            DynamicTable.__createAndAppendRow(null, true, table);
            updateDirtyBit();
            updatePurchaseAdvice();
        }
       
        function update() {
          refreshCalculation();
          updateDirtyBit();
        }
        function _postGetAllFormValues(args) {
          args.sFees = DynamicTable.serialize(FeesTable);
          args.sAdjustments = DynamicTable.serialize(AdjustmentsTable);
          args.numFixedField = m_basePriceFixedField;
        }
        

        function setChange(id, value) {
            document.getElementById(id).value = value;
            update();
        }
        
        function basePriceFieldModified(value) {
            m_basePriceFixedField = value;
            update();
        }
        
        function onPullDetailsFromLoanFileClicked(table) {
            var data = {};
            data.LoanId = document.getElementById('loanid').value;
            
            var results = gService.loanedit.call('PullDetailsFromLoanFile', data);
            updateDirtyBit();
            if (!results.error) {
                // Set calc mode to price in UI
                $('#sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing_Price').attr('checked', true);
                updatePurchaseAdvice();
            
                $('#' + table.tableName + ' tbody input[DeleteEntry]').each(function(i, e) {
                    table.deleteRow($(e));
                });
                table.currentIndex = 0;

                for (var i = 0; i < results.value.adjustmentTableSize; i++)
                    addRow(table);
                populateForm(results.value, null);
            }
        }
        
        function updatePurchaseAdvice() {
            var isPrice = $('#sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing_Price').is(':checked');
            var sPurchaseAdviceBasePrice_Field1_Purchasing = $('#sPurchaseAdviceBasePrice_Field1_Purchasing'); 
            var sPurchaseAdviceBasePrice_Field2_Purchasing = $('#sPurchaseAdviceBasePrice_Field2_Purchasing'); 
            
            sPurchaseAdviceBasePrice_Field1_Purchasing.attr('readonly', !isPrice);
            sPurchaseAdviceBasePrice_Field1_Purchasing.css('background-color', isPrice ? '' : 'lightgray' );
            sPurchaseAdviceBasePrice_Field2_Purchasing.attr('readonly', isPrice);
            sPurchaseAdviceBasePrice_Field2_Purchasing.css('background-color', !isPrice ? '' : 'lightgray' );

            $('input.Amt10').attr('readonly', isPrice).css('background-color', !isPrice ? '' : 'lightgray' );
            $('input.Price10').attr('readonly', !isPrice).css('background-color', isPrice ? '' : 'lightgray' );
        }        
    </script>
    
    <form id="PurchaseAdvice" runat="server" onreset="return false;">
        <table cellSpacing="0" cellPadding="0">
			<tr>
				<td class="MainRightHeader" noWrap>
				    Purchase Advice - Purchasing
				</td>
			</tr>
			<tr style="padding:4px">
			    <td>
			        <table cellpadding="2" cellspacing="0" class="FieldLabel">
			            <tr class="LoanFormHeader">
			                <td style="padding:3px" colspan="6">
			                    Purchase Summary
			                </td>
			                <td>
			                    &nbsp;			                    
			                </td>
			                <td>
			                    &nbsp;			                    
			                </td>
			            </tr>
                        <tr>
                            <td class="BorderTop BorderLeft" colspan="2">
                                Unpaid Principal Balance
                            </td>
                            <td class="BorderTop" colspan="3">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceUnpaidPBal_Purchasing2" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td class="BorderTop">
                                <ml:EncodedLabel ID="Label8" Text="Loan Closed Date" AssociatedControlID="sClosedD" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderTop BorderRight" colspan="2">
                                <ml:datetextbox ID="sClosedD" runat="server" preset="date" CssClass="mask" onchange="update();" Width="60px"></ml:datetextbox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft" colspan="2">
                                Total Price
                            </td>
                            <td colspan="3">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceTotalPrice_Field2_Purchasing2" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td>
                                <ml:EncodedLabel ID="Label7" Text="Loan Purchase Date" AssociatedControlID="sPurchasedD" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderRight" colspan="2">
                                <ml:datetextbox ID="sPurchasedD" runat="server" preset="date" CssClass="mask" onchange="update();" Width="60px"></ml:datetextbox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft" colspan="2">
                                Total Interest Adjustments
                            </td>
                            <td colspan="3">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceInterestTotalAdj_Purchasing2" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td>
                                <ml:EncodedLabel ID="Label6" Text="1st Payment Due" AssociatedControlID="sSchedDueD1" runat="server" CssClass="PlayNiceWithLockCB"></ml:EncodedLabel>
                                <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" onclick="lockField(this, 'sSchedDueD1'); update();" CssClass="PlayNiceWithLockLbl"/>
                            </td>
                            <td class="BorderRight" colspan="2">
                                <ml:datetextbox ID="sSchedDueD1" runat="server" preset="date" CssClass="mask" onchange="update();" Width="60px"></ml:datetextbox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft" colspan="2">
                                Total Escrow Amt. Due Investor
                            </td>
                            <td colspan="3">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceEscrowAmtDueInv_Purchasing2" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td>
                                <ml:EncodedLabel ID="Label18" Text="1st Payment Due Investor (us)" AssociatedControlID="sInvSchedDueD1_Purchasing" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderRight" colspan="2">
                                <ml:datetextbox ID="sInvSchedDueD1_Purchasing" runat="server" preset="date" CssClass="mask" onchange="update();" Width="60px"></ml:datetextbox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft" colspan="2">
                                Total Fees
                            </td>
                            <td colspan="3">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceFeesTotal_Purchasing2" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td>
                                <ml:EncodedLabel ID="Label1" Text="Investor (us)" AssociatedControlID="sPurchaseAdviceSummaryInvNm_Purchasing" runat="server" CssClass="PlayNiceWithLockCB"></ml:EncodedLabel>
                                <asp:CheckBox ID="sPurchaseAdviceSummaryInvNm_PurchasingLckd" runat="server" onclick="lockField(this, 'sPurchaseAdviceSummaryInvNm_Purchasing'); update();" CssClass="PlayNiceWithLockLbl"/>
                            </td>
                            <td class="BorderRight" colspan="2">
                                <asp:TextBox ID="sPurchaseAdviceSummaryInvNm_Purchasing" runat="server" Width="160px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft" colspan="2">
                                Subtotal
                            </td>
                            <td colspan="3">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceSummarySubtotal_Purchasing" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td>
                                <ml:EncodedLabel ID="Label2" Text="Investor (our) Program Name" AssociatedControlID="sPurchaseAdviceSummaryInvPgNm_Purchasing" runat="server" CssClass="PlayNiceWithLockCB"></ml:EncodedLabel>
                                <asp:CheckBox ID="sPurchaseAdviceSummaryInvPgNm_PurchasingLckd" runat="server" onclick="lockField(this, 'sPurchaseAdviceSummaryInvPgNm_Purchasing'); update();" CssClass="PlayNiceWithLockLbl"/>
                            </td>
                            <td class="BorderRight" colspan="2">
                                <asp:TextBox ID="sPurchaseAdviceSummaryInvPgNm_Purchasing" runat="server" onchange="update();" Width="160px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft">
                                <ml:EncodedLabel ID="Label5" Text="Other Adjustments" AssociatedControlID="sPurchaseAdviceSummaryOtherAdjDesc_Purchasing" runat="server"></ml:EncodedLabel>
                            </td>
                            <td align="right">
                                <asp:TextBox ID="sPurchaseAdviceSummaryOtherAdjDesc_Purchasing" runat="server" onchange="update();" Width="80px"></asp:TextBox>
                            </td>
                            <td colspan="3">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceSummaryOtherAdjVal_Purchasing" runat="server" onchange="update();" Width="80px"/>
                            </td>
                            <td>
                                <ml:EncodedLabel ID="Label3" Text="Investor (our) Loan Number" AssociatedControlID="sPurchaseAdviceSummaryInvLoanNm_Purchasing" runat="server" CssClass="PlayNiceWithLockCB"></ml:EncodedLabel>
                                <asp:CheckBox ID="sPurchaseAdviceSummaryInvLoanNm_PurchasingLckd" runat="server" onclick="lockField(this, 'sPurchaseAdviceSummaryInvLoanNm_Purchasing'); update();" CssClass="PlayNiceWithLockLbl"/>
                            </td>
                            <td class="BorderRight" colspan="2">
                                <asp:TextBox ID="sPurchaseAdviceSummaryInvLoanNm_Purchasing" runat="server" onchange="update();" Width="160px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft BorderBottom" colspan="2">
                                Total Due Seller
                            </td>
                            <td class="BorderBottom" colspan="3">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceSummaryTotalDueSeller_Purchasing" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td class="BorderBottom">
                                Servicing
                            </td>
                            <td class="BorderBottom BorderRight" colspan="2">
                                <asp:DropDownList ID="sPurchaseAdviceSummaryServicingStatus_Purchasing" runat="server" onchange="update();"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="LoanFormHeader" style="padding:3px">
			                <td colspan="2">
			                    Purchase Details
			                </td>
			                <td align="right" colspan="6">
			                    <input type="button" id="m_pullLoanDetailsBtn" value="Pull details from loan file" onclick="onPullDetailsFromLoanFileClicked(AdjustmentsTable);" runat="server" class="ButtonStyle" />
			                </td>
			            </tr>
			            <tr>
                            <td class="BorderTop BorderLeft BorderRight FormTableSubHeader" colspan="4">
                                Loan Price
                            </td>
                            <td style="width:8px">
                                &nbsp;
                            </td>
                            <td class="BorderTop BorderLeft BorderRight FormTableSubHeader" colspan="3">
                                Interest
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft" colspan="2">
                                Total Loan Amount
                            </td>
                            <td class="BorderRight" colspan="2">
			                    <ml:MoneyTextBox ID="sFinalLAmt" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="BorderLeft" colspan="2" noWrap>
                                <ml:EncodedLabel ID="Label11" Text="Interest Due Seller (+) / Due Investor (-)" AssociatedControlID="sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderRight" align="right">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceInterestDueSellerOrInvestor_Purchasing" runat="server" onchange="update();" Width="80px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft" colspan="2">
                                <ml:EncodedLabel ID="Label9" Text="Amortization/Curtailments" AssociatedControlID="sPurchaseAdviceAmortCurtail_Purchasing" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderRight" colspan="2">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceAmortCurtail_Purchasing" runat="server" onchange="update();" Width="80px"/>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="BorderLeft" colspan="2">
                                <ml:EncodedLabel ID="Label12" Text="Interest Reimbursement" AssociatedControlID="sPurchaseAdviceInterestReimbursement_Purchasing" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderRight" align="right">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceInterestReimbursement_Purchasing" runat="server" onchange="update();" Width="80px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft" colspan="2">
                                Unpaid Principal Balance
                            </td>
                            <td class="BorderRight" colspan="2">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceUnpaidPBal_Purchasing" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="BorderLeft BorderBottom" colspan="2">
                                Total Interest Adjustments
                            </td>
                            <td class="BorderRight BorderBottom" align="right">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceInterestTotalAdj_Purchasing" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft">
                                &nbsp;
                            </td>
                            <td style="padding-left:17px">
                               <asp:RadioButton runat="server" Value="1" ID="sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing_Price" GroupName="sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing" /> Price
                            </td>
                            <td align="center">
                               <asp:RadioButton runat="server" Value="0"  ID="sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing_Amount" GroupName="sPurchaseAdviceBasePrice_AmountPriceCalcMode_Purchasing" /> 
                                Amount
                            </td>
                            <td class="BorderRight" align="center">
                                % Adj.
                            </td>
                            <td colspan="4">
                            </td>
                        </tr>
                
                        <tr>
                            <td class="BorderLeft">
                                <ml:EncodedLabel ID="Label10" Text="Base Price" AssociatedControlID="sPurchaseAdviceBasePrice_Field1_Purchasing" runat="server"></ml:EncodedLabel>
                            </td>
                            <td>
                                <asp:TextBox ID="sPurchaseAdviceBasePrice_Field1_Purchasing" onchange="update();"  runat="server" Width="70" decimalDigits="6"></asp:TextBox>
                            </td>
                            <td>
			                    <ml:MoneyTextBox  onchange="update();" ID="sPurchaseAdviceBasePrice_Field2_Purchasing" runat="server" Width="80px"/>
                            </td>
                            <td class="BorderRight">
                                <ml:percenttextbox ID="sPurchaseAdviceBasePrice_Field3_Purchasing"  onchange="update();" runat="server" preset="percent" Width="67px" decimalDigits="6" ReadOnly="true"></ml:percenttextbox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="BorderLeft BorderRight BorderTop FormTableSubHeader" colspan="3">
                                Escrow Balance To Investor
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft" colspan="2">
                                Adjustments
                            </td>
                            <td>
			                    <ml:MoneyTextBox ID="sPurchaseAdviceAdjustments_Field2_Purchasing" ReadOnly="true" runat="server" Width="80px" />
                            </td>
                            <td class="BorderRight">
                                <ml:percenttextbox ID="sPurchaseAdviceAdjustments_Field3_Purchasing" ReadOnly="true" runat="server" preset="percent" Width="67px" decimalDigits="6"></ml:percenttextbox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="BorderLeft" colspan="2">
                                <ml:EncodedLabel ID="Label14" Text="Total Collected At Closing" AssociatedControlID="sPurchaseAdviceEscrowTotCollAtClosing_Purchasing" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderRight" align="right">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceEscrowTotCollAtClosing_Purchasing" runat="server" onchange="update();" Width="80px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft">
                                Net Price
                            </td>
                            <td>
                                <asp:TextBox ID="sPurchaseAdviceNetPrice_Field1_Purchasing" ReadOnly="true" runat="server" Width="70"></asp:TextBox>
                            </td>
                            <td>
			                    <ml:MoneyTextBox ID="sPurchaseAdviceNetPrice_Field2_Purchasing" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td class="BorderRight">
                                <ml:percenttextbox ID="sPurchaseAdviceNetPrice_Field3_Purchasing" ReadOnly="true" runat="server" preset="percent" Width="67px" decimalDigits="6"></ml:percenttextbox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="BorderLeft" colspan="2">
                                <ml:EncodedLabel ID="Label15" Text="Escrow Deposit With Amort. Pmt." AssociatedControlID="sPurchaseAdviceEscrowDepDue_Purchasing" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderRight" align="right">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceEscrowDepDue_Purchasing" runat="server" onchange="update();" Width="80px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft" colspan="2">
                                SRP
                            </td>
                            <td>
			                    <ml:MoneyTextBox ID="sPurchaseAdviceSRP_Field2_Purchasing" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td class="BorderRight">
                                <ml:percenttextbox ID="sPurchaseAdviceSRP_Field3_Purchasing" ReadOnly="true" runat="server" preset="percent" Width="67px" decimalDigits="6"></ml:percenttextbox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="BorderLeft" colspan="2">
                                <ml:EncodedLabel ID="Label16" Text="Escrow Disbursements" AssociatedControlID="sPurchaseAdviceEscrowDisbursements_Purchasing" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderRight" align="right">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceEscrowDisbursements_Purchasing" runat="server" onchange="update();" Width="80px"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="BorderLeft BorderBottom">
                                Total Price
                            </td>
                            <td class="BorderBottom">
                                <asp:TextBox ID="sPurchaseAdviceTotalPrice_Field1_Purchasing" ReadOnly="true" runat="server" Width="70"></asp:TextBox>
                            </td>
                            <td class="BorderBottom">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceTotalPrice_Field2_Purchasing" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                            <td class="BorderBottom BorderRight">
                                <ml:percenttextbox ID="sPurchaseAdviceTotalPrice_Field3_Purchasing" ReadOnly="true" runat="server" preset="percent" Width="67px" decimalDigits="6"></ml:percenttextbox>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td class="BorderBottom BorderLeft" colspan="2">
                                Total Escrow Amt. Due Investor
                            </td>
                            <td class="BorderBottom BorderRight" align="right">
			                    <ml:MoneyTextBox ID="sPurchaseAdviceEscrowAmtDueInv_Purchasing" ReadOnly="true" runat="server" Width="80px"/>
                            </td>
                        </tr>
			        </table>			        
			    </td>
			</tr>
			<tr style="padding:1px">
			    <td>
			        <table class="InsetBorder">
			            <tr>
				            <td class="FormTableSubHeader">
				                Adjustments
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <input type="button" class="ButtonStyle" NoHighlight value="Add new adjustment" onclick="addRow(AdjustmentsTable);" />
			                    <input type="button" class="ButtonStyle" NoHighlight value="Delete selected adjustment" onclick="DynamicTable.deleteSelected(AdjustmentsTable);" id="btn_deleteAdjustments" disabled="disabled" />
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <table id="AdjustmentsTable" cellspacing="0" class="FieldLabel">
			                        <thead>
			                            <tr class="GridHeader">
			                                <td>		                
			                                    <input type="checkbox" id="m_deleteAdjustmentsCheckbox" onclick="DynamicTable.setDeleteCheckboxes(AdjustmentsTable);" NotForEdit />
			                                </td>
			                                <td class="descriptionFieldAdj">
			                                    Description
			                                </td>
			                                <td class="rateField pricDis">
			                                    Price
			                                </td>
			                                <td class="moneyFieldAdj ">
			                                    Amount
			                                </td>
			                                <td style="width:50px">
			                                    SRP Adj
			                                </td>
			                            </tr>
			                        </thead>
			                        <tbody>
			                        </tbody>
			                    </table>
			                </td>
			            </tr>
			        </table>
			    </td>
            </tr>
			<tr style="padding:1px">
			    <td>
			        <table class="InsetBorder">
			            <tr>
				            <td class="FormTableSubHeader">
			                    Fees
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <input value="Add new fee" type="button" onclick="addRow(FeesTable);" NoHighlight class="ButtonStyle" />
			                    <input value="Delete selected fees" id="btn_deleteFees" disabled="disabled" type="button" onclick="DynamicTable.deleteSelected(FeesTable);" NoHighlight class="ButtonStyle" />
			                </td>
			            </tr>
			            <tr>
			                <td>
			                    <table id="FeesTable" cellspacing="0" class="FieldLabel">
			                        <thead>
			                            <tr class="GridHeader">
			                                <td>
			                                    <input type="checkbox" id="m_deleteFeesCheckbox" onclick="DynamicTable.setDeleteCheckboxes(FeesTable);" NotForEdit />
			                                </td>
			                                <td class="descriptionFieldFee">
			                                    Description
			                                </td>
			                                <td class="moneyFieldFee">
			                                    Amount
			                                </td>
			                            </tr>
			                        </thead>
			                        <tbody>
			                        </tbody>
			                        <tfoot>
			                            <tr>
			                                <td>
			                                </td>
			                                <td align="right">
			                                    Total fees
			                                </td>
			                                <td>
						                        <ml:MoneyTextBox ID="sPurchaseAdviceFeesTotal_Purchasing" ReadOnly="true" runat="server" Width="100" />
			                                </td>
			                            </tr>
			                        </tfoot>
			                    </table>
			                </td>
			            </tr>
			        </table>
			    </td>
            </tr>
		</table>
    </form>
</body>
</html>
