﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Disbursement_Purchasing.aspx.cs" Inherits="LendersOfficeApp.newlos.Purchasing.Disbursement_Purchasing" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Disbursement - Purchasing</title>
    
    <style type="text/css">
        .insetLabel { padding-left: 5px; width: 90px; }
        .insetLabel2 { padding-left: 5px; width: 100px; }
        .insetLabel3 { padding-left: 5px; width: 40px; }
        .padBottom { padding-bottom: .15em; }
        .Wide { width: 270px;}
    </style>
</head>
<body bgcolor="gainsboro">
    <form id="Disbursement" runat="server">
    <table class="FormTable" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="MainRightHeader" colspan="2">
                Disbursement - Purchasing
            </td>
        </tr>
        <tr>
            <td style="padding:4px">
                <table class="FieldLabel">
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="Label1" Text="Payment To Seller" AssociatedControlID="sPurchaseAdviceSummaryTotalDueSeller_Purchasing" runat="server"></ml:EncodedLabel>
                        </td>
                        <td colspan="2">
		                    <ml:MoneyTextBox ID="sPurchaseAdviceSummaryTotalDueSeller_Purchasing" ReadOnly="true" runat="server" width="80px" />
                        </td>
                        <td>
                            <ml:EncodedLabel ID="sPurchasingDisbursementDLabel" Text="Purchasing Disbursement Date" AssociatedControlID="sPurchasingDisbursementD" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPurchasingDisbursementD" runat="server" preset="date" Width="60px"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="2">
                            <uc:CFM ID="CFM" runat="server"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="Label2" Text="Purchasing Funds To" AssociatedControlID="sPurchasingDisbursementFundsTo" runat="server"></ml:EncodedLabel>
                        </td>
                        <td colspan="2">
		                    <asp:TextBox ID="sPurchasingDisbursementFundsTo" runat="server" class="Wide" ></asp:TextBox>
                        </td>
                        <td>
                            <ml:EncodedLabel ID="Label3" Text="Purchasing Disbursement Notes" AssociatedControlID="sPurchasingDisbursementNotes" runat="server"></ml:EncodedLabel>
                        </td>
                        <td rowspan="5" valign="top">
                            <asp:TextBox ID="sPurchasingDisbursementNotes" TextMode="MultiLine" Rows="7" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">First Credit To</td>
                        <td>
                            <ml:EncodedLabel ID="Label4" runat="server" AssociatedControlID="sPurchasingDisbursementBankName" Text="Bank Name" />
                            <br />
                            <asp:TextBox id="sPurchasingDisbursementBankName" runat="server" />
                        </td>
                        <td>
                            <ml:EncodedLabel ID="Label5" runat="server" AssociatedControlID="sPurchasingDisbursementBankCityState" Text="Bank City/State" />
                            <br />
                            <asp:TextBox id="sPurchasingDisbursementBankCityState" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <ml:EncodedLabel ID="Label6" runat="server" AssociatedControlID="sPurchasingDisbursementABANumber" Text="ABA Number" />
                            <br/>
                            <asp:TextBox id="sPurchasingDisbursementABANumber" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <ml:EncodedLabel ID="Label7" runat="server" AssociatedControlID="sPurchasingDisbursementAccountName" Text="Account Name" />
                            <br />
                            <asp:TextBox id="sPurchasingDisbursementAccountName" runat="server" />
                        </td>
                        <td>
                            <ml:EncodedLabel ID="Label8" runat="server" AssociatedControlID="sPurchasingDisbursementAccountNumber" Text="Account Number" />
                            <br />
                            <asp:TextBox id="sPurchasingDisbursementAccountNumber" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">Further Credit To</td>
                        <td >
                            <ml:EncodedLabel ID="Label9" runat="server" AssociatedControlID="sPurchasingDisbursementFurtherCreditToAccountName" Text="Account Name" />
                            <br />
                            <asp:TextBox ID="sPurchasingDisbursementFurtherCreditToAccountName" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <ml:EncodedLabel ID="Label10" runat="server" AssociatedControlID="sPurchasingDisbursementFurtherCreditToAccountNumber" Text="Account Number" />
                            <br />
                            <asp:TextBox ID="sPurchasingDisbursementFurtherCreditToAccountNumber" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="Label11" Text="Additional Instructions" AssociatedControlID="sPurchasingDisbursementAdditionalInstructionsLine1" runat="server"></ml:EncodedLabel>
                        </td>
                        <td colspan="2">
		                    <asp:TextBox id="sPurchasingDisbursementAdditionalInstructionsLine1" class="Wide" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="2">
		                    <asp:TextBox id="sPurchasingDisbursementAdditionalInstructionsLine2" class="Wide" runat="server" />
                        </td>
                    </tr> 
                </table>
            </td>
        </tr>
    </table>
    <uc1:cmodaldlg id="CModalDlg1" runat="server" />
    </form>
</body>
</html>
