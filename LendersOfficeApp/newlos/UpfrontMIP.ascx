<%@ Control Language="c#" AutoEventWireup="false" Codebehind="UpfrontMIP.ascx.cs" Inherits="LendersOfficeApp.newlos.UpfrontMIP" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Migration" %>

<style>
    .LeftPadding { padding-left:10px; }
    .month { width: 30px; }
</style>

<script type="text/javascript">
    var oldSaveMe = window.saveMe;
    window.saveMe = function(bRefreshScreen)
    {
        var shouldEnforceDisbSched = ML.LoanVersionTCurrent >= <%= AspxTools.JsNumeric(LoanVersionT.V16_EnforceDisbursementMonthTotalExpenses) %>;
        if(shouldEnforceDisbSched && !ValidateDisbSchedule())
        {
            alert("MI disbursement months must add up to 12.");
            return;
        }

        return oldSaveMe(bRefreshScreen);
    }

    function ValidateDisbSchedule()
    {
        var total = 0;
        $(".SchedMon").each(function() {
            var parsedVal = parseInt($(this).val());
            if(isNaN(parsedVal))
            {
                parsedVal = 0;
            }

            total += parsedVal;
        });

        if(total != 12)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

var m_sFfUfMipIsBeingFinancedRecentValue;

function _init() {
    var fhaLoan = isFHALoan();
    m_sFfUfMipIsBeingFinancedRecentValue = <%=AspxTools.JsGetElementById(sFfUfMipIsBeingFinanced_0)%>.checked;
    <%=AspxTools.JsGetElementById(m_sFfUfMipIsBeingFinancedChanged)%>.value = 'False';
    <%=AspxTools.JsGetElementById(sProMInsMb)%>.readOnly = fhaLoan;
    <%=AspxTools.JsGetElementById(sUfCashPdLckd)%>.disabled = fhaLoan || <%=AspxTools.JsGetElementById(sFfUfMipIsBeingFinanced_1)%>.checked;
    <%=AspxTools.JsGetElementById(sMipPiaMon)%>.readOnly = !<%=AspxTools.JsGetElementById(sMipFrequency_1)%>.checked;
    lockField(<%=AspxTools.JsGetElementById(sFfUfmip1003Lckd)%>, 'sFfUfmip1003');
    lockField(<%=AspxTools.JsGetElementById(sUfCashPdLckd)%>, 'sUfCashPd');
    lockField(<%=AspxTools.JsGetElementById(sProMInsLckd)%>, 'sProMIns');
    lockField(<%=AspxTools.JsGetElementById(sLenderUfmipLckd)%>, 'sLenderUfmip');
    lockField(<%=AspxTools.JsGetElementById(sMInsRsrvMonLckd)%>, 'sMInsRsrvMon');
    <%= AspxTools.JsGetElementById(sMInsRsrvEscrowedTri) %>.disabled = <%= AspxTools.JsGetElementById(sIssMInsRsrvEscrowedTriReadOnly) %>.checked;
    disableEnableCompanyNm();
    lockPaymentSchedule();
    locksMipFrequency();
}

    function locksMipFrequency()
    {
        var sForceSinglePaymentMipFrequency = <%=AspxTools.JsGetElementById(sForceSinglePaymentMipFrequency)%>.value == "True";
        var IsVersionAt_V4_ForceSinglePmtUfmipTypeForFhaVaUsda = <%=AspxTools.JsGetElementById(IsVersionAt_V4_ForceSinglePmtUfmipTypeForFhaVaUsda)%>.value == "True";
        if(sForceSinglePaymentMipFrequency && IsVersionAt_V4_ForceSinglePmtUfmipTypeForFhaVaUsda)
        {
            <%= AspxTools.JsGetElementById(sMipFrequency_0) %>.disabled = true;
            <%= AspxTools.JsGetElementById(sMipFrequency_1) %>.disabled = true;
        }
        else
        {
            <%= AspxTools.JsGetElementById(sMipFrequency_0) %>.disabled = false;
            <%= AspxTools.JsGetElementById(sMipFrequency_1) %>.disabled = false;
        }
    }

function lockPaymentSchedule()
{
    var scheduleRO = <%=AspxTools.JsGetElementById(sMIPaymentRepeat)%>.value == <%=AspxTools.JsString(E_DisbursementRepIntervalT.Annual)%>;
    
    <%=AspxTools.JsGetElementById(MIJan)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MIFeb)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MIMar)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MIApr)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MIMay)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MIJun)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MIJul)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MIAug)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MISep)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MIOct)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MINov)%>.readOnly = !scheduleRO;
    <%=AspxTools.JsGetElementById(MIDec)%>.readOnly = !scheduleRO;
}

function isFHALoan() {
    return <%=AspxTools.JsGetElementById(sLT)%>.value == <%=AspxTools.JsString(E_sLT.FHA)%>;
}

function isVALoan() {
    return <%=AspxTools.JsGetElementById(sLT)%>.value == <%=AspxTools.JsString(E_sLT.VA)%>;
}

function isUSDALoan() {
    return <%=AspxTools.JsGetElementById(sLT)%>.value == <%=AspxTools.JsString(E_sLT.UsdaRural)%>;
}

function onChange_sLT() {
    if (isFHALoan()) {
        bindsProMInsT(MipCalcTypeFha_map);
    }
    else {
        bindsProMInsT(MipCalcTypeNonFha_map);
    }
    
    updateCompanyNm();
    refreshCalculation();
}

function updateCompanyNm() {
    var sMiCompanyNmT = <%=AspxTools.JsGetElementById(sMiCompanyNmT)%>;
    sMiCompanyNmT.disabled = false;
    
    for(var i = 0; i < sMiCompanyNmT.length; i++) {
        if(sMiCompanyNmT[i].value == <%=AspxTools.JsString(E_sMiCompanyNmT.FHA)%>
            || sMiCompanyNmT[i].value == <%=AspxTools.JsString(E_sMiCompanyNmT.VA)%>
            || sMiCompanyNmT[i].value == <%=AspxTools.JsString(E_sMiCompanyNmT.USDA)%>) {
            sMiCompanyNmT.removeChild(sMiCompanyNmT[i]);
            i--;
        }
    }
    
    if (isFHALoan()) {
        var child = document.createElement('option');
        child.value = <%=AspxTools.JsString(E_sMiCompanyNmT.FHA)%>;
        child.innerText = 'FHA';
        sMiCompanyNmT.appendChild(child);
    }
    
    if (isVALoan()) {
        var child = document.createElement('option');
        child.value = <%=AspxTools.JsString(E_sMiCompanyNmT.VA)%>;
        child.innerText = 'VA';
        sMiCompanyNmT.appendChild(child);
    }
    
    if (isUSDALoan()) {
        var child = document.createElement('option');
        child.value = <%=AspxTools.JsString(E_sMiCompanyNmT.USDA)%>;
        child.innerText = 'USDA';
        sMiCompanyNmT.appendChild(child);
    }
    
    disableEnableCompanyNm();
}

function disableEnableCompanyNm() {
    var sMiCompanyNmT = <%=AspxTools.JsGetElementById(sMiCompanyNmT)%>;
    if (isFHALoan() || isVALoan() || isUSDALoan()) {
        sMiCompanyNmT.disabled = true;    
    }
}

function bindsProMInsT(list) {
    var sProMInsT = <%=AspxTools.JsGetElementById(sProMInsT)%>;
    var sProMInsTValue = sProMInsT.value;
    
    while(sProMInsT.hasChildNodes())
        sProMInsT.removeChild(sProMInsT.firstChild);
        
    for(var i = 0; i < list.length; i++){
        var child = document.createElement('option');
        child.value = list[i].Key;
        child.innerText = list[i].Value;
        if (child.value == sProMInsTValue)
            child.setAttribute('selected', 'selected');
        sProMInsT.appendChild(child);
    }
}

function sFfUfMipIsBeingFinancedChanged() {
    if(m_sFfUfMipIsBeingFinancedRecentValue != <%=AspxTools.JsGetElementById(sFfUfMipIsBeingFinanced_0)%>.checked)
        <%=AspxTools.JsGetElementById(m_sFfUfMipIsBeingFinancedChanged)%>.value = 'True';    
    refreshCalculation();
}

    function doAfterDateFormat(e)
    {
        refreshCalculation();
    }
</script>

<asp:HiddenField ID="sForceSinglePaymentMipFrequency" runat="server" />
<asp:HiddenField ID="m_sFfUfMipIsBeingFinancedChanged" runat="server" />
<asp:HiddenField ID="IsVersionAt_V4_ForceSinglePmtUfmipTypeForFhaVaUsda" runat="server" />
<table>
    <tr>
        <td class="FieldLabel" colspan="2">
            Loan Type
            <asp:DropDownList id="sLT" runat="server" onchange="onChange_sLT();" />
            Loan Amt
            <ml:MoneyTextBox ID="sLAmtCalc" runat="server" preset="money"></ml:MoneyTextBox>
            Total Loan Amt
            <ml:MoneyTextBox ID="sFinalLAmt" runat="server" preset="money" ReadOnly="true"></ml:MoneyTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <table class="InsetBorder FieldLabel" style="width: 360px;">
                            <tr>
                                <td colspan="2" class="FormTableSubHeader">
                                    Borrower Paid Upfront MIP/FF
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Total UFMIP / FF
                                    <ml:PercentTextBox ID="sFfUfmipR" Width="70" preset="percent" runat="server" onchange="refreshCalculation();" decimalDigits="6" />
                                </td>
                                <td align="right">
                                    <asp:CheckBox ID="sFfUfmip1003Lckd" runat="server" onclick="refreshCalculation();"></asp:CheckBox>
                                    <ml:MoneyTextBox ID="sFfUfmip1003" Width="90" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="LeftPadding">
                                    <input type="radio" id="sMipFrequency_0" value="0" name="sMipFrequency" onclick="refreshCalculation();" runat="server" />
                                    Single payment
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="LeftPadding">
                                    <input type="radio" id="sMipFrequency_1" value="1" name="sMipFrequency" onclick="refreshCalculation();" runat="server" />
                                    Annual rate for
                                    <asp:TextBox ID="sMipPiaMon" Width="30px" onchange="refreshCalculation();" runat="server"></asp:TextBox>
                                    months
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" nowrap>
                                    Is UFMIP/FF being financed?
                                    <input type="radio" id="sFfUfMipIsBeingFinanced_0" value="True" name="sFfUfMipIsBeingFinanced" onclick="sFfUfMipIsBeingFinancedChanged();"
                                        runat="server" />Yes
                                    <input type="radio" id="sFfUfMipIsBeingFinanced_1" value="False" name="sFfUfMipIsBeingFinanced" onclick="sFfUfMipIsBeingFinancedChanged();"
                                        runat="server" />No
                                </td>
                            </tr>
                            <tr>
                                <td class="LeftPadding">
                                    UFMIP/FF financed amount
                                </td>
                                <td align="right">
                                    <ml:MoneyTextBox ID="sFfUfmipFinanced" Width="90" preset="money" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="LeftPadding">
                                    Paid in Cash
                                </td>
                                <td align="right">
                                    <asp:CheckBox ID="sUfCashPdLckd" runat="server" onclick="refreshCalculation();"></asp:CheckBox>
                                    <ml:MoneyTextBox ID="sUfCashPd" Width="90" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                </td>
                            </tr>
                            <asp:PlaceHolder ID="phIsFinancedUFMIPIncludedInLTV" runat="server">
                                <tr>
                                    <td class="LeftPadding" colspan="2">
                                        <asp:CheckBox ID="sIncludeUfmipInLtvCalc" runat="server" onclick="refreshCalculation();"></asp:CheckBox>
                                        Include financed UFMIP when calculating LTV
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="InsetBorder FieldLabel" style="width: 360px;">
                            <tr>
                                <td colspan="2" class="FormTableSubHeader">
                                    Borrower Paid Monthly MIP
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Annual premium rate
                                </td>
                                <td align="right">
                                    <ml:PercentTextBox ID="sProMInsR" Width="70" preset="percent" runat="server" onchange="refreshCalculation();" decimalDigits="6"></ml:PercentTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="LeftPadding">
                                    of
                                    <asp:DropDownList ID="sProMInsT" runat="server" onchange="refreshCalculation();" />
                                </td>
                                <td align="right">
                                    <ml:MoneyTextBox ID="sProMInsBaseAmt" Width="90" preset="money" runat="server" onchange="refreshCalculation();" ReadOnly="true"></ml:MoneyTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Base monthly premium
                                </td>
                                <td align="right">
                                    <ml:MoneyTextBox ID="sProMInsBaseMonthlyPremium" Width="90" preset="money" runat="server" onchange="refreshCalculation();"
                                        ReadOnly="true"></ml:MoneyTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Monthly premium adjustment
                                </td>
                                <td align="right">
                                    <ml:MoneyTextBox ID="sProMInsMb" Width="90" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Monthly premium
                                </td>
                                <td align="right">
                                    <asp:CheckBox ID="sProMInsLckd" runat="server" onclick="refreshCalculation();"></asp:CheckBox>
                                    <ml:MoneyTextBox ID="sProMIns" Width="90" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="InsetBorder FieldLabel" style="width: 360px;">
                            <tr>
                                <td colspan="2" class="FormTableSubHeader">
                                    Lender Paid Single Premium
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Single premium rate
                                    <ml:PercentTextBox ID="sLenderUfmipR" Width="70" preset="percent" runat="server" onchange="refreshCalculation();" decimalDigits="6" />
                                </td>
                                <td align="right">
                                    <asp:CheckBox ID="sLenderUfmipLckd" runat="server" onclick="refreshCalculation();"></asp:CheckBox>
                                    <ml:MoneyTextBox ID="sLenderUfmip" Width="90" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table>
                <tr>
                    <td>
                        <table class="InsetBorder FieldLabel" style="width: 360px;">
                            <tr>
                                <td colspan="2" class="FormTableSubHeader">
                                    Borrower Paid MIP Renewal and Cancellation
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Term for initial rate
                                </td>
                                <td>
                                    <asp:TextBox ID="sProMInsMon" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Term for renewal annual rate
                                </td>
                                <td>
                                    <asp:TextBox ID="sProMIns2Mon" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Renewal annual premium rate
                                </td>
                                <td>
                                    <ml:PercentTextBox ID="sProMInsR2" runat="server" onchange="refreshCalculation();" decimalDigits="6"></ml:PercentTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Renewal monthly amount
                                </td>
                                <td>
                                    <ml:MoneyTextBox ID="sProMIns2" runat="server" preset="money" onchange="refreshCalculation();" ReadOnly="true"></ml:MoneyTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cancel at LTV
                                    <ml:PercentTextBox ID="sProMInsCancelLtv" runat="server" onchange="refreshCalculation();"></ml:PercentTextBox>
                                </td>
                                <td>
                                    <asp:CheckBox ID="sProMInsMidptCancel" runat="server" Text="Cancel at midpoint" onchange="refreshCalculation();" />
                                </td>
                            </tr>
                            <tr>
                                <td>Cancel at Appraisal LTV</td>
                                <td>
                                    <ml:PercentTextBox runat="server" ID="sProMInsCancelAppraisalLtv" onchange="refreshCalculation();"></ml:PercentTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Minimum number of payments before cancellation
                                    <asp:TextBox ID="sProMInsCancelMinPmts" runat="server" onchange="refreshCalculation();" Width="40px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2" valign="top">
                        <table class="InsetBorder FieldLabel" style="width: 360px;">
                            <tr>
                                <td colspan="2" class="FormTableSubHeader">
                                    MI Policy
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Mortgage insurance type
                                </td>
                                <td>
                                    <asp:DropDownList ID="sMiInsuranceT" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    MI coverage %
                                </td>
                                <td>
                                    <ml:PercentTextBox ID="sMiLenderPaidCoverage" runat="server" onchange="refreshCalculation();" decimalDigits="4"></ml:PercentTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    MI provider
                                </td>
                                <td>
                                    <asp:DropDownList ID="sMiCompanyNmT" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    MI commitment requested date
                                </td>
                                <td>
                                    <ml:DateTextBox ID="sMiCommitmentRequestedD" runat="server" Width="75" onchange="date_onblur(null, this);" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    MI commitment received date
                                </td>
                                <td>
                                    <ml:DateTextBox ID="sMiCommitmentReceivedD" runat="server" Width="75" onchange="date_onblur(null, this);" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    MI commitment expiration date
                                </td>
                                <td>
                                    <ml:DateTextBox ID="sMiCommitmentExpirationD" runat="server" Width="75" onchange="date_onblur(null, this);" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    MI certificate ID
                                </td>
                                <td>
                                    <asp:TextBox ID="sMiCertId" runat="server" onchange="refreshCalculation();"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    UFMIP is refundable on a pro-rata basis
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" ID="sUfmipIsRefundableOnProRataBasis" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table id="MIEscImpSection" runat="server" class="InsetBorder" style="width: 735px; margin-left: 9px;" cellpadding="0" border="0" cellspacing="0">
    <tr class="FormTableSubHeader">
        <td>
            Escrow/Impounds
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td class="FieldLabel">
                        Escrowed
                        <asp:CheckBox onchange="refreshCalculation();" runat="server" ID="sMInsRsrvEscrowedTri" />
                        <asp:CheckBox runat="server" ID="sIssMInsRsrvEscrowedTriReadOnly" Enabled="false" style="display: none;" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Reserves Cushion
                    </td>
                    <td>
                    </td>
                    <td align="right">
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MICush" class="month"></asp:TextBox>
                        months
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Reserves Amount
                    </td>
                    <td>
                        for
                    </td>
                    <td>
                        <asp:CheckBox onchange="refreshCalculation();" runat="server" ID="sMInsRsrvMonLckd" />
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="sMInsRsrvMon" class="month"></asp:TextBox>
                        months
                    </td>
                    <td>
                        <ml:MoneyTextBox ReadOnly="true" runat="server" preset="money" ID="sMInsRsrv"></ml:MoneyTextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="FormTableSubHeader">
        <td>
            Payment Schedule
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        Payment repeat
                    </td>
                    <td colspan="12">
                        <asp:DropDownList runat="server" ID="sMIPaymentRepeat" onchange="refreshCalculation();"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        Jan
                    </td>
                    <td>
                        Feb
                    </td>
                    <td>
                        Mar
                    </td>
                    <td>
                        Apr
                    </td>
                    <td>
                        May
                    </td>
                    <td>
                        Jun
                    </td>
                    <td>
                        Jul
                    </td>
                    <td>
                        Aug
                    </td>
                    <td>
                        Sep
                    </td>
                    <td>
                        Oct
                    </td>
                    <td>
                        Nov
                    </td>
                    <td>
                        Dec
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        Months
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MIJan" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MIFeb" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MIMar" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MIApr" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MIMay" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MIJun" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MIJul" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MIAug" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MISep" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MIOct" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MINov" class="month SchedMon"></asp:TextBox>
                    </td>
                    <td>
                        <asp:TextBox onchange="refreshCalculation();" runat="server" ID="MIDec" class="month SchedMon"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

