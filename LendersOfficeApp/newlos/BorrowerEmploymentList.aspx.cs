using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos
{

	public partial class BorrowerEmploymentList : BaseListEditPage
	{

        protected override void LoadData() 
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            IEmpCollection recordList = RequestHelper.GetBool("isborrower") ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
            m_dg.DataSource = recordList.SortedView;
            m_dg.DataBind();
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.m_dg.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid_ItemCreated);

        }
		#endregion
	}
}
