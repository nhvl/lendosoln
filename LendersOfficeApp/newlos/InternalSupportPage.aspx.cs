﻿namespace LendersOfficeApp.newlos
{
    using System;
    using System.Linq;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public partial class InternalSupportPage : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.CanModifyLoanPrograms };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.CanModifyLoanPrograms };
            }
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            base.OnInit(e);
        }
        protected void Page_Init(object sender, System.EventArgs e)
        {
            this.PageTitle = "Internal Support Page";
            this.PageID = "InternalSupportPage";
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            this.LegalEntityIdentifierValidator.ValidationExpression = LqbGrammar.DataTypes.RegularExpressionString.LegalEntityIdentifier.ToString();
            this.LegalEntityIdentifierValidator.ErrorMessage = ErrorMessages.InvalidLegalEntityIdentifier;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(InternalSupportPage));
            dataLoan.InitLoad();

            // OPM 228640. If any archive exists currently that is not in the legacy archive,
            // we cannot let users go back to legacy.
            var isMigratedWithNonLegacyArchive =
                dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                && !dataLoan.sClosingCostArchive.TrueForAll(newArchive => dataLoan.GFEArchives.Any(oldArchive => oldArchive.DateArchived == newArchive.DateArchived));

            var isTridMode = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

            Tools.Bind_sClosingCostFeeVersionT(sClosingCostFeeVersionT, isTridMode || isMigratedWithNonLegacyArchive);
            Tools.Bind_sLoanFileT(sLoanFileT);

            sIsAllowExcludeToleranceCure.Checked = dataLoan.sIsAllowExcludeToleranceCure;
            sIsHousingExpenseMigrated.Checked = dataLoan.sIsHousingExpenseMigrated;
            sIsManuallySetThirdPartyAffiliateProps.Checked = dataLoan.sIsManuallySetThirdPartyAffiliateProps;
            sLoads1003LineLFromAdjustments.Checked = dataLoan.sLoads1003LineLFromAdjustments;
            sLoads1003LineLFromAdjustments.Enabled = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

            Tools.SetDropDownListValue(sClosingCostFeeVersionT, dataLoan.sClosingCostFeeVersionT);
            Tools.SetDropDownListValue(sLoanFileT, dataLoan.sLoanFileT);

            // OPM 237779 - Allow overwritting of sTRIDLoanEstimateCashToCloseCalcMethodT in last disclosed archive.
            if (dataLoan.sHasLastDisclosedLoanEstimateArchive)
            {
                tr_sTRIDLoanEstimateCashToCloseCalcMethodT.Visible = true;
                sTRIDLoanEstimateCashToCloseCalcMethodT.SelectedValue = dataLoan.LastDisclosedLoanEstimateArchive.GetCountValue("sTRIDLoanEstimateCashToCloseCalcMethodT").ToString();
            }

            sUse2016ComboAndProrationUpdatesTo1003Details.Checked = dataLoan.sUse2016ComboAndProrationUpdatesTo1003Details;
            sBlockComplianceEaseIdFromRequest.Checked = dataLoan.sBlockComplianceEaseIdFromRequest;

            sRespa6D.Text = dataLoan.sRespa6D_rep;
            sRespa6DLckd.Checked = dataLoan.sRespa6DLckd;

            var preparer = dataLoan.GetPreparerOfForm(
                E_PreparerFormT.App1003Interviewer,
                E_ReturnOptionIfNotExist.ReturnEmptyObject);
            App1003PrepareDate.Text = preparer.PrepareDate_rep;

            sCalculateInitAprAndLastDiscApr.Checked = dataLoan.sCalculateInitAprAndLastDiscApr;
            sCalculateInitAprAndLastDiscApr.Enabled = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

            if (!(dataLoan.BrokerDB.EnableAutoPriceEligibilityProcess && dataLoan.sIsIncludeInAutoPriceEligibilityProcess) )
            {
                AutoProcessBtn.Disabled = true;
                EnqueueLenderBtn.Disabled = true;
                EnqueueLoanBtn.Disabled = true;
            }

            this.sLegalEntityIdentifier.Text = dataLoan.sLegalEntityIdentifier;
            sBorrowerApplicationCollectionT.Text = dataLoan.sBorrowerApplicationCollectionT.ToString();

            this.MigrateToLatestUladDataLayer.Visible = dataLoan.sBorrowerApplicationCollectionT != EnumHelper.MaxValue<E_sLqbCollectionT>();
            this.IsMigratedToLatestUladDataLayer.Checked = dataLoan.sBorrowerApplicationCollectionT == EnumHelper.MaxValue<E_sLqbCollectionT>();
        }
    }
}
