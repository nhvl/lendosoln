﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetailsofTransaction.aspx.cs" Inherits="LendersOfficeApp.newlos.DetailsofTransaction" %>
<%@ Register TagPrefix="uc1" TagName="DetailsofTransaction" Src="~/newlos/DetailsofTransaction.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Details of Transaction</title>
    <script type="text/javascript">
        function _init() {
            if (typeof _initControl === 'function') {
                _initControl();
            }   
        }
    </script>
</head>
    <body bgcolor="gainsboro">
        <form id="form1" runat="server">
            <uc1:DetailsofTransaction ID="dot" runat="server" />
        </form>
    </body>
</html>
