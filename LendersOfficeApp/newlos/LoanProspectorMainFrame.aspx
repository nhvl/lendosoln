<%@ Page language="c#" Codebehind="LoanProspectorMainFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LoanProspectorMainFrame" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<frameset rows="75,*">
  <frame src="LoanProspectorBranding.aspx" id="frmBranding">
  <% if (RequestHelper.GetSafeQueryString("cmd") == null || RequestHelper.GetSafeQueryString("cmd") == "") { %>
  <frame src="LoanProspectorMain.aspx?loanid=<%=AspxTools.HtmlStringFromQueryString("LoanID")%>&entrypoint=<%=AspxTools.HtmlStringFromQueryString("entrypoint")%>" id="frmMain">
  <% } else { %>
  <frame src="LoanProspectorSendToSystem.aspx?cmd=<%=AspxTools.HtmlStringFromQueryString("cmd")%>" id="frmMain">
  
  <% } %>
</frameset>
