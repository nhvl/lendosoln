﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using LendersOffice.Constants;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos
{
    public partial class DetailsofTransaction : BaseLoanPage
    {
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Details of Transaction";
            this.PageID = "DetailsofTransaction";
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
