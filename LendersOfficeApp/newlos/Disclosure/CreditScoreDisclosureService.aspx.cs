using System;
using DataAccess;
using LendersOffice.Migration;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class CreditScoreDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(CreditScoreDisclosureServiceItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("aBDecisionCreditScore", dataApp.aBDecisionCreditScore_rep);
            SetResult("aCDecisionCreditScore", dataApp.aCDecisionCreditScore_rep);

            SetResult("aBDecisionCreditSourceT", dataApp.aBDecisionCreditSourceT);
            SetResult("aCDecisionCreditSourceT", dataApp.aCDecisionCreditSourceT);

            SetResult("aBDecisionCreditSourceTLckd", dataApp.aBDecisionCreditSourceTLckd);
            SetResult("aCDecisionCreditSourceTLckd", dataApp.aCDecisionCreditSourceTLckd);

            this.SetResult(nameof(dataApp.aBOtherCreditModelName), dataApp.aBOtherCreditModelName);
            this.SetResult(nameof(dataApp.aCOtherCreditModelName), dataApp.aCOtherCreditModelName);

        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataApp.aBDecisionCreditSourceT = (E_aDecisionCreditSourceT)GetInt("aBDecisionCreditSourceT");
            dataApp.aCDecisionCreditSourceT = (E_aDecisionCreditSourceT)GetInt("aCDecisionCreditSourceT");

            dataApp.aBDecisionCreditSourceTLckd = GetBool("aBDecisionCreditSourceTLckd");
            dataApp.aCDecisionCreditSourceTLckd = GetBool("aCDecisionCreditSourceTLckd");

            dataApp.aBDecisionCreditScore_rep = this.GetString("aBDecisionCreditScore");
            dataApp.aCDecisionCreditScore_rep = this.GetString("aCDecisionCreditScore");

            dataApp.aBOtherCreditModelName = this.GetString(nameof(dataApp.aBOtherCreditModelName));
            dataApp.aCOtherCreditModelName = this.GetString(nameof(dataApp.aCOtherCreditModelName));

            dataApp.aBExperianPercentile_rep = GetString("aBExperianPercentile");
            dataApp.aBTransUnionPercentile_rep = GetString("aBTransUnionPercentile");
            dataApp.aBEquifaxPercentile_rep = GetString("aBEquifaxPercentile");
            dataApp.aCExperianPercentile_rep = GetString("aCExperianPercentile");
            dataApp.aCTransUnionPercentile_rep = GetString("aCTransUnionPercentile");
            dataApp.aCEquifaxPercentile_rep = GetString("aCEquifaxPercentile");

            if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V19_PopulateApplicationLevelCreditModelNames))
            {
                dataLoan.sExperianModelName = GetString("sExperianModelName");
                dataLoan.sEquifaxModelName = GetString("sEquifaxModelName");
                dataLoan.sTransUnionModelName = GetString("sTransUnionModelName");
            }

            dataLoan.sExperianScoreFrom_rep   = GetString("sExperianScoreFrom");
            dataLoan.sExperianScoreTo_rep     = GetString("sExperianScoreTo");
            dataLoan.sEquifaxScoreFrom_rep    = GetString("sEquifaxScoreFrom");
            dataLoan.sEquifaxScoreTo_rep      = GetString("sEquifaxScoreTo");
            dataLoan.sTransUnionScoreFrom_rep = GetString("sTransUnionScoreFrom");
            dataLoan.sTransUnionScoreTo_rep   = GetString("sTransUnionScoreTo");
            dataApp.aBExperianFactors         = GetString("aBExperianFactors");
            dataApp.aBExperianCreatedD_rep    = GetString("aBExperianCreatedD");
            dataApp.aBExperianScore_rep       = GetString("aBExperianScore");
            dataApp.aCExperianFactors         = GetString("aCExperianFactors");
            dataApp.aCExperianCreatedD_rep    = GetString("aCExperianCreatedD");
            dataApp.aCExperianScore_rep       = GetString("aCExperianScore");
            dataApp.aBEquifaxFactors          = GetString("aBEquifaxFactors");
            dataApp.aBEquifaxCreatedD_rep     = GetString("aBEquifaxCreatedD");
            dataApp.aBEquifaxScore_rep        = GetString("aBEquifaxScore");
            dataApp.aCEquifaxFactors          = GetString("aCEquifaxFactors");
            dataApp.aCEquifaxCreatedD_rep     = GetString("aCEquifaxCreatedD");
            dataApp.aCEquifaxScore_rep        = GetString("aCEquifaxScore");
            dataApp.aBTransUnionFactors       = GetString("aBTransUnionFactors");
            dataApp.aBTransUnionCreatedD_rep  = GetString("aBTransUnionCreatedD");
            dataApp.aBTransUnionScore_rep     = GetString("aBTransUnionScore");
            dataApp.aCTransUnionFactors       = GetString("aCTransUnionFactors");
            dataApp.aCTransUnionCreatedD_rep  = GetString("aCTransUnionCreatedD");
            dataApp.aCTransUnionScore_rep     = GetString("aCTransUnionScore");

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditEquifax, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName    = GetString("CreditEquifaxCompanyName");
            preparer.StreetAddr     = GetString("CreditEquifaxStreetAddr");
            preparer.City           = GetString("CreditEquifaxCity");
            preparer.State          = GetString("CreditEquifaxState");
            preparer.Zip            = GetString("CreditEquifaxZip");
            preparer.PhoneOfCompany = GetString("CreditEquifaxPhoneOfCompany");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditExperian, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName    = GetString("CreditExperianCompanyName");
            preparer.StreetAddr     = GetString("CreditExperianStreetAddr");
            preparer.City           = GetString("CreditExperianCity");
            preparer.State          = GetString("CreditExperianState");
            preparer.Zip            = GetString("CreditExperianZip");
            preparer.PhoneOfCompany = GetString("CreditExperianPhoneOfCompany");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditTransUnion, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName    = GetString("CreditTransUnionCompanyName");
            preparer.StreetAddr     = GetString("CreditTransUnionStreetAddr");
            preparer.City           = GetString("CreditTransUnionCity");
            preparer.State          = GetString("CreditTransUnionState");
            preparer.Zip            = GetString("CreditTransUnionZip");
            preparer.PhoneOfCompany = GetString("CreditTransUnionPhoneOfCompany");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditDisclosureLender, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("CreditDisclosureLenderCompanyName");
            preparer.StreetAddr  = GetString("CreditDisclosureLenderStreetAddr");
            preparer.City        = GetString("CreditDisclosureLenderCity");
            preparer.State       = GetString("CreditDisclosureLenderState");
            preparer.Zip         = GetString("CreditDisclosureLenderZip");
            preparer.Update();
        }
    }
	public partial class CreditScoreDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new CreditScoreDisclosureServiceItem());
        }
	}
}
