﻿namespace LendersOfficeApp.newlos.Disclosure
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web.Services;
    using System.Web.UI.WebControls;

    using DataAccess;
    using DataAccess.Sellers;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.Security;
    using LendersOffice.ObjLib.TRID2;
    using LendersOffice.ObjLib;
    using LendersOffice.Migration;

    [DataContract]
    public class ClosingDisclosureFeeViewModel
    {
        [DataMember]
        public IEnumerable<BorrowerClosingCostFeeSection> SectionList { get; set; }

        [DataMember]
        public string VRoot = Tools.VRoot;

        [DataMember]
        public ClosingDisclosureEditableFieldsModel editableFieldsModel { get; set; }

        [DataMember]
        public string sDocMagicClosingD { get; set; }

        [DataMember]
        public string sConsummationD { get; set; }

        [DataMember]
        public E_sClosingCostFeeVersionT sClosingCostFeeVersionT { get; set; }

        [DataMember]
        public bool DisableTPAffIfHasContact { get; set; }

        [DataMember]
        public bool sIPiaDyLckd { get; set; }
        [DataMember]
        public string sIPiaDy { get; set; }
        [DataMember]
        public bool sIPerDayLckd { get; set; }
        [DataMember]
        public string sIPerDay { get; set; }

        [DataMember]
        public string sTRIDLoanEstimateTotalLoanCosts { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateTotalClosingCosts { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateTotalOtherCosts { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateLenderCredits { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateTotalAllCosts { get; set; }
        [DataMember]
        public TridLoanPurposeType sTridLPurposeT { get; set; }
        [DataMember]
        public E_sLPurposeT sLPurposeT { get; set; }
        [DataMember]
        public string sToleranceCure { get; set; }
        [DataMember]
        public string sLenderPaidBrokerCompF { get; set; }

        [DataMember]
        public bool sGfeIsTPOTransaction { get; set; }

        [DataMember]
        public E_TriState sLoanTransactionInvolvesSeller { get; set; }
    }

    [DataContract]
    public class ClosingDisclosureEditableFieldsModel
    {
        [DataMember]
        public string sRLckdExpiredD { get; set; }

        [DataMember]
        public string sEstCloseD { get; set; }
        [DataMember]
        public bool sEstCloseDLckd { get; set; }
        [DataMember]
        public bool sGfeIsBalloon { get; set; }
        [DataMember]
        public string sSchedDueD1 { get; set; }

        [DataMember]
        public string sSchedDueD2 { get; set; }

        [DataMember]
        public string sSchedDueD3 { get; set; }

        [DataMember]
        public string sSchedDueD4 { get; set; }

        [DataMember]
        public string sSchedDueD5 { get; set; }

        [DataMember]
        public string sSchedDueD6 { get; set; }

        [DataMember]
        public string sSchedDueD7 { get; set; }

        [DataMember]
        public string sSchedDueD8 { get; set; }

        [DataMember]
        public string sSchedDueD9 { get; set; }

        [DataMember]
        public string sSchedDueD10 { get; set; }

        [DataMember]
        public bool sSchedDueD1Lckd { get; set; }

        [DataMember]
        public string sAPR { get; set; }
        [DataMember]
        public string sCFPBTotalInterestPercent { get; set; }

        [DataMember]
        public string sTRIDLoanEstimateTotalLoanCosts;
        [DataMember]
        public string sTRIDLoanEstimateTotalOtherCosts;
        [DataMember]
        public string sTRIDLoanEstimateLenderCredits;
        [DataMember]
        public string sTRIDLoanEstimateTotalClosingCosts;
        [DataMember]
        public string sTRIDLoanEstimateBorrowerFinancedClosingCosts;
        [DataMember]
        public string sTRIDLoanEstimateDownPaymentOrFundsFromBorrower;
        [DataMember]
        public string sTRIDCashDeposit;
        [DataMember]
        public string sTRIDLoanEstimateFundsForBorrower;
        [DataMember]
        public string sTRIDSellerCredits;
        [DataMember]
        public string sTRIDAdjustmentsAndOtherCredits;
        [DataMember]
        public string sTRIDLoanEstimateCashToClose;
        
        [DataMember]
        public string sDocMagicClosingD { get; set; }
        [DataMember]
        public bool sDocMagicClosingDLckd { get; set; }
        [DataMember]
        public string sConsummationD { get; set; }
        [DataMember]
        public bool sConsummationDLckd { get; set; }
        [DataMember]
        public string sDocMagicDisbursementD { get; set; }
        [DataMember]
        public bool sDocMagicDisbursementDLckd { get; set; }
        [DataMember]
        public string sSettlementAgentFileNum { get; set; }

        [DataMember]
        public Guid sTRIDLenderAgentId;
        [DataMember]
        public string sTRIDLenderName {get; set;}
        [DataMember]
        public string sTRIDLenderAddr{get; set;}
        [DataMember]
        public string sTRIDLenderCity{get; set;}
        [DataMember]
        public string sTRIDLenderState{get; set;}
        [DataMember]
        public string sTRIDLenderZip{get; set;}
        [DataMember]
        public string sTRIDLenderNMLSNum{get; set;}
        [DataMember]
        public string sTRIDLenderLicenseIDNum{get; set;}
        [DataMember]
        public string sTRIDLenderContact{get; set;}
        [DataMember]
        public string sTRIDLenderContactNMLSNum{get; set;}
        [DataMember]
        public string sTRIDLenderContactLicenseIDNum{get; set;}
        [DataMember]
        public string sTRIDLenderContactEmail{get; set;}
        [DataMember]
        public string sTRIDLenderContactPhoneNum { get; set; }
        [DataMember]
        public E_AgentRoleT sTRIDLenderAgentRoleT { get; set; }
        [DataMember]
        public bool sTRIDLenderIsLocked { get; set; }

        [DataMember]
        public Guid sTRIDMortgageBrokerAgentId { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerName { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerAddr { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerCity { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerState { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerZip { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerNMLSNum { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerLicenseIDNum { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerContact { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerContactNMLSNum { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerContactLicenseIDNum { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerEmail { get; set; }
        [DataMember]
        public string sTRIDMortgageBrokerPhoneNum { get; set; }
        [DataMember]
        public E_AgentRoleT sTRIDMortgageBrokerAgentRoleT { get; set; }
        [DataMember]
        public bool sTRIDMortgageBrokerIsLocked { get; set; }

        [DataMember]
        public Guid sTRIDSettlementAgentAgentId { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentName { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentAddr { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentCity { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentState { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentZip { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentNMLSNum { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentLicenseIDNum { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentContact { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentContactNMLSNum { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentContactLicenseIDNum { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentEmail { get; set; }
        [DataMember]
        public string sTRIDSettlementAgentPhoneNum { get; set; }
        [DataMember]
        public E_AgentRoleT sTRIDSettlementAgentAgentRoleT { get; set; }
        [DataMember]
        public bool sTRIDSettlementAgentIsLocked { get; set; }

        [DataMember]
        public Guid sTRIDRealEstateBrokerBuyerAgentId { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerName { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerAddr { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerCity { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerState { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerZip { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerNMLSNum { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerLicenseIDNum { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerContact { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerContactNMLSNum { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerContactLicenseIDNum { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerEmail { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerBuyerPhoneNum { get; set; }
        [DataMember]
        public E_AgentRoleT sTRIDRealEstateBrokerBuyerAgentRoleT { get; set; }
        [DataMember]
        public bool sTRIDRealEstateBrokerBuyerIsLocked { get; set; }

        [DataMember]
        public Guid sTRIDRealEstateBrokerSellerAgentId { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerName { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerAddr { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerCity { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerState { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerZip { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerNMLSNum { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerLicenseIDNum { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerContact { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerContactNMLSNum { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerContactLicenseIDNum { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerEmail { get; set; }
        [DataMember]
        public string sTRIDRealEstateBrokerSellerPhoneNum { get; set; }
        [DataMember]
        public E_AgentRoleT sTRIDRealEstateBrokerSellerAgentRoleT { get; set; }
        [DataMember]
        public bool sTRIDRealEstateBrokerSellerIsLocked { get; set; }

        [DataMember]
        public bool sTRIDPartialPaymentAcceptanceT_AcceptApplied { get; set; }
        [DataMember]
        public bool sTRIDPartialPaymentAcceptanceT_AcceptNotApplied { get; set; }
        [DataMember]
        public bool sTRIDPartialPaymentAcceptanceT_NotAccepted { get; set; }

        [DataMember]
        public bool sNonMIHousingExpensesEscrowedReasonT_LenderRequired { get; set; }
        [DataMember]
        public bool sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested { get; set; }

        [DataMember]
        public bool sNonMIHousingExpensesNotEscrowedReasonT_Declined { get; set; }
        [DataMember]
        public bool sNonMIHousingExpensesNotEscrowedReasonT_NotOffered { get; set; }

        [DataMember]
        public string sTRIDEscrowWaiverFee { get; set; }

        [DataMember]
        public bool sTRIDPropertyIsInNonRecourseLoanState_True { get; set; }
        [DataMember]
        public bool sTRIDPropertyIsInNonRecourseLoanState_False { get; set; }
        
        [DataMember]
        public string sLastDisclosedClosingDisclosureArchiveId { get; set; }

        [DataMember]
        public string sLenderCaseNum { get; set; }
        [DataMember]
        public bool sLenderCaseNumLckd { get; set; }
    }

    public partial class ClosingDisclosure : BaseLoanPage
    {
        public static List<string> EditableFields = new List<string>() {
            "sEstCloseD",
            "sEstCloseDLckd",
            "sGfeIsBalloon",
            "sSchedDueD1",
            "sSchedDueD2",
            "sSchedDueD3",
            "sSchedDueD4",
            "sSchedDueD5",
            "sSchedDueD6",
            "sSchedDueD7",
            "sSchedDueD8",
            "sSchedDueD9",
            "sSchedDueD10",
            "sSchedDueD1Lckd",
            "sAPR",
            "sCFPBTotalInterestPercent",
            "sTRIDLoanEstimateTotalLoanCosts",
            "sTRIDLoanEstimateTotalOtherCosts",
            "sTRIDLoanEstimateLenderCredits",
            "sTRIDLoanEstimateTotalClosingCosts",
            "sTRIDLoanEstimateBorrowerFinancedClosingCosts",
            "sTRIDLoanEstimateDownPaymentOrFundsFromBorrower",
            "sTRIDCashDeposit",
            "sTRIDLoanEstimateFundsForBorrower",
            "sTRIDSellerCredits",
            "sTRIDAdjustmentsAndOtherCredits",
            "sTRIDLoanEstimateCashToClose",
            "sDocMagicClosingD",
            "sDocMagicClosingDLckd",
            "sConsummationD",
            "sConsummationDLckd",
            "sDocMagicDisbursementD",
            "sDocMagicDisbursementDLckd",
            "sSettlementAgentFileNum",

            "sTRIDLenderAgentId",
            "sTRIDLenderName",
            "sTRIDLenderAddr",
            "sTRIDLenderCity",
            "sTRIDLenderState",
            "sTRIDLenderZip",
            "sTRIDLenderNMLSNum",
            "sTRIDLenderLicenseIDNum",
            "sTRIDLenderContact",
            "sTRIDLenderContactNMLSNum",
            "sTRIDLenderContactLicenseIDNum",
            "sTRIDLenderContactEmail",
            "sTRIDLenderContactPhoneNum",
            "sTRIDLenderAgentRoleT",
            "sTRIDLenderIsLocked",
            
            "sTRIDSettlementAgentAgentId",
            "sTRIDSettlementAgentName",
            "sTRIDSettlementAgentAddr",
            "sTRIDSettlementAgentCity",
            "sTRIDSettlementAgentState",
            "sTRIDSettlementAgentZip",
            "sTRIDSettlementAgentNMLSNum",
            "sTRIDSettlementAgentLicenseIDNum",
            "sTRIDSettlementAgentContact",
            "sTRIDSettlementAgentContactNMLSNum",
            "sTRIDSettlementAgentContactLicenseIDNum",
            "sTRIDSettlementAgentEmail",
            "sTRIDSettlementAgentPhoneNum",
            "sTRIDSettlementAgentAgentRoleT",
            "sTRIDSettlementAgentIsLocked",
            
            "sTRIDMortgageBrokerAgentId",
            "sTRIDMortgageBrokerName",
            "sTRIDMortgageBrokerAddr",
            "sTRIDMortgageBrokerCity",
            "sTRIDMortgageBrokerState",
            "sTRIDMortgageBrokerZip",
            "sTRIDMortgageBrokerNMLSNum",
            "sTRIDMortgageBrokerLicenseIDNum",
            "sTRIDMortgageBrokerContact",
            "sTRIDMortgageBrokerContactNMLSNum",
            "sTRIDMortgageBrokerContactLicenseIDNum",
            "sTRIDMortgageBrokerEmail",
            "sTRIDMortgageBrokerPhoneNum",
            "sTRIDMortgageBrokerAgentRoleT",
            "sTRIDMortgageBrokerIsLocked",
            
            "sTRIDRealEstateBrokerBuyerAgentId",
            "sTRIDRealEstateBrokerBuyerName",
            "sTRIDRealEstateBrokerBuyerAddr",
            "sTRIDRealEstateBrokerBuyerCity",
            "sTRIDRealEstateBrokerBuyerState",
            "sTRIDRealEstateBrokerBuyerZip",
            "sTRIDRealEstateBrokerBuyerNMLSNum",
            "sTRIDRealEstateBrokerBuyerLicenseIDNum",
            "sTRIDRealEstateBrokerBuyerContact",
            "sTRIDRealEstateBrokerBuyerContactNMLSNum",
            "sTRIDRealEstateBrokerBuyerContactLicenseIDNum",
            "sTRIDRealEstateBrokerBuyerEmail",
            "sTRIDRealEstateBrokerBuyerPhoneNum",
            "sTRIDRealEstateBrokerBuyerAgentRoleT",
            "sTRIDRealEstateBrokerBuyerIsLocked",
            
            "sTRIDRealEstateBrokerSellerAgentId",
            "sTRIDRealEstateBrokerSellerName",
            "sTRIDRealEstateBrokerSellerAddr",
            "sTRIDRealEstateBrokerSellerCity",
            "sTRIDRealEstateBrokerSellerState",
            "sTRIDRealEstateBrokerSellerZip",
            "sTRIDRealEstateBrokerSellerNMLSNum",
            "sTRIDRealEstateBrokerSellerLicenseIDNum",
            "sTRIDRealEstateBrokerSellerContact",
            "sTRIDRealEstateBrokerSellerContactNMLSNum",
            "sTRIDRealEstateBrokerSellerContactLicenseIDNum",
            "sTRIDRealEstateBrokerSellerEmail",
            "sTRIDRealEstateBrokerSellerPhoneNum",
            "sTRIDRealEstateBrokerSellerAgentRoleT",
            "sTRIDRealEstateBrokerSellerIsLocked",
            
            "sTRIDPartialPaymentAcceptanceT_AcceptApplied",
            "sTRIDPartialPaymentAcceptanceT_AcceptNotApplied",
            "sTRIDPartialPaymentAcceptanceT_NotAccepted",

            "sNonMIHousingExpensesEscrowedReasonT_LenderRequired",
            "sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested",

            "sNonMIHousingExpensesNotEscrowedReasonT_Declined",
            "sNonMIHousingExpensesNotEscrowedReasonT_NotOffered",

            "sTRIDEscrowWaiverFee",

            "sTRIDPropertyIsInNonRecourseLoanState_True",
            "sTRIDPropertyIsInNonRecourseLoanState_False",
            "sLastDisclosedClosingDisclosureArchiveId",
            "sLenderCaseNum",
            "sLenderCaseNumLckd"
        };

        LosConvert m_convertLos = new LosConvert();

        public bool IsArchivePage
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled
                    && RequestHelper.GetSafeQueryString("IsArchivePage") == "true"
                    && !IsLeadPage;
            }
        }

        protected bool ShowArchiveRecorder
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled && !IsArchivePage && !IsLeadPage;
            }
        }

        public void InitialiseCFM()
        {
            LenderContact.IsAllowLockableFeature = true;
            LenderContact.AgentRoleT = E_AgentRoleT.Lender;
            LenderContact.DisableAddTo = true;
            LenderContact.RecordIdField = sTRIDLenderAgentId.ClientID;
            LenderContact.CompanyNameField = sTRIDLenderName.ClientID;
            LenderContact.StreetAddressField = sTRIDLenderAddr.ClientID;
            LenderContact.CityField = sTRIDLenderCity.ClientID;
            LenderContact.StateField = sTRIDLenderState.ClientID;
            LenderContact.ZipField = sTRIDLenderZip.ClientID;
            LenderContact.CompanyLoanOriginatorIdentifierField = sTRIDLenderNMLSNum.ClientID;
            LenderContact.CompanyLicenseField = sTRIDLenderLicenseIDNum.ClientID;
            LenderContact.AgentNameField = sTRIDLenderContact.ClientID;
            LenderContact.LoanOriginatorIdentifierField = sTRIDLenderContactNMLSNum.ClientID;
            LenderContact.AgentLicenseField = sTRIDLenderContactLicenseIDNum.ClientID;
            LenderContact.EmailField = sTRIDLenderContactEmail.ClientID;
            LenderContact.PhoneField = sTRIDLenderContactPhoneNum.ClientID;

            MortgageBrokerContact.IsAllowLockableFeature = true;
            MortgageBrokerContact.AgentRoleT = E_AgentRoleT.Broker;
            MortgageBrokerContact.DisableAddTo = true;
            MortgageBrokerContact.RecordIdField = sTRIDMortgageBrokerAgentId.ClientID;
            MortgageBrokerContact.CompanyNameField = sTRIDMortgageBrokerName.ClientID;
            MortgageBrokerContact.StreetAddressField = sTRIDMortgageBrokerAddr.ClientID;
            MortgageBrokerContact.CityField = sTRIDMortgageBrokerCity.ClientID;
            MortgageBrokerContact.StateField = sTRIDMortgageBrokerState.ClientID;
            MortgageBrokerContact.ZipField = sTRIDMortgageBrokerZip.ClientID;
            MortgageBrokerContact.CompanyLoanOriginatorIdentifierField = sTRIDMortgageBrokerNMLSNum.ClientID;
            MortgageBrokerContact.CompanyLicenseField = sTRIDMortgageBrokerLicenseIDNum.ClientID;
            MortgageBrokerContact.AgentNameField = sTRIDMortgageBrokerContact.ClientID;
            MortgageBrokerContact.LoanOriginatorIdentifierField = sTRIDMortgageBrokerContactNMLSNum.ClientID;
            MortgageBrokerContact.AgentLicenseField = sTRIDMortgageBrokerContactLicenseIDNum.ClientID;
            MortgageBrokerContact.EmailField = sTRIDMortgageBrokerEmail.ClientID;
            MortgageBrokerContact.PhoneField = sTRIDMortgageBrokerPhoneNum.ClientID;

            SettlementAgentContact.IsAllowLockableFeature = true;
            SettlementAgentContact.AgentRoleT = E_AgentRoleT.ClosingAgent;
            SettlementAgentContact.DisableAddTo = true;
            SettlementAgentContact.RecordIdField = sTRIDSettlementAgentAgentId.ClientID;
            SettlementAgentContact.CompanyNameField = sTRIDSettlementAgentName.ClientID;
            SettlementAgentContact.StreetAddressField = sTRIDSettlementAgentAddr.ClientID;
            SettlementAgentContact.CityField = sTRIDSettlementAgentCity.ClientID;
            SettlementAgentContact.StateField = sTRIDSettlementAgentState.ClientID;
            SettlementAgentContact.ZipField = sTRIDSettlementAgentZip.ClientID;
            SettlementAgentContact.CompanyLoanOriginatorIdentifierField = sTRIDSettlementAgentNMLSNum.ClientID;
            SettlementAgentContact.CompanyLicenseField = sTRIDSettlementAgentLicenseIDNum.ClientID;
            SettlementAgentContact.AgentNameField = sTRIDSettlementAgentContact.ClientID;
            SettlementAgentContact.LoanOriginatorIdentifierField = sTRIDSettlementAgentContactNMLSNum.ClientID;
            SettlementAgentContact.AgentLicenseField = sTRIDSettlementAgentContactLicenseIDNum.ClientID;
            SettlementAgentContact.EmailField = sTRIDSettlementAgentEmail.ClientID;
            SettlementAgentContact.PhoneField = sTRIDSettlementAgentPhoneNum.ClientID;

            RealEstateBrokerBuyerContact.IsAllowLockableFeature = true;
            RealEstateBrokerBuyerContact.AgentRoleT = E_AgentRoleT.BuyerAgent;
            RealEstateBrokerBuyerContact.DisableAddTo = true;
            RealEstateBrokerBuyerContact.RecordIdField = sTRIDRealEstateBrokerBuyerAgentId.ClientID;
            RealEstateBrokerBuyerContact.CompanyNameField = sTRIDRealEstateBrokerBuyerName.ClientID;
            RealEstateBrokerBuyerContact.StreetAddressField = sTRIDRealEstateBrokerBuyerAddr.ClientID;
            RealEstateBrokerBuyerContact.CityField = sTRIDRealEstateBrokerBuyerCity.ClientID;
            RealEstateBrokerBuyerContact.StateField = sTRIDRealEstateBrokerBuyerState.ClientID;
            RealEstateBrokerBuyerContact.ZipField = sTRIDRealEstateBrokerBuyerZip.ClientID;
            RealEstateBrokerBuyerContact.CompanyLoanOriginatorIdentifierField = sTRIDRealEstateBrokerBuyerNMLSNum.ClientID;
            RealEstateBrokerBuyerContact.CompanyLicenseField = sTRIDRealEstateBrokerBuyerLicenseIDNum.ClientID;
            RealEstateBrokerBuyerContact.AgentNameField = sTRIDRealEstateBrokerBuyerContact.ClientID;
            RealEstateBrokerBuyerContact.LoanOriginatorIdentifierField = sTRIDRealEstateBrokerBuyerContactNMLSNum.ClientID;
            RealEstateBrokerBuyerContact.AgentLicenseField = sTRIDRealEstateBrokerBuyerContactLicenseIDNum.ClientID;
            RealEstateBrokerBuyerContact.EmailField = sTRIDRealEstateBrokerBuyerEmail.ClientID;
            RealEstateBrokerBuyerContact.PhoneField = sTRIDRealEstateBrokerBuyerPhoneNum.ClientID;

            RealEstateBrokerSellerContact.IsAllowLockableFeature = true;
            RealEstateBrokerSellerContact.AgentRoleT = E_AgentRoleT.ListingAgent;
            RealEstateBrokerSellerContact.DisableAddTo = true;
            RealEstateBrokerSellerContact.RecordIdField = sTRIDRealEstateBrokerSellerAgentId.ClientID;
            RealEstateBrokerSellerContact.CompanyNameField = sTRIDRealEstateBrokerSellerName.ClientID;
            RealEstateBrokerSellerContact.StreetAddressField = sTRIDRealEstateBrokerSellerAddr.ClientID;
            RealEstateBrokerSellerContact.CityField = sTRIDRealEstateBrokerSellerCity.ClientID;
            RealEstateBrokerSellerContact.StateField = sTRIDRealEstateBrokerSellerState.ClientID;
            RealEstateBrokerSellerContact.ZipField = sTRIDRealEstateBrokerSellerZip.ClientID;
            RealEstateBrokerSellerContact.CompanyLoanOriginatorIdentifierField = sTRIDRealEstateBrokerSellerNMLSNum.ClientID;
            RealEstateBrokerSellerContact.CompanyLicenseField = sTRIDRealEstateBrokerSellerLicenseIDNum.ClientID;
            RealEstateBrokerSellerContact.AgentNameField = sTRIDRealEstateBrokerSellerContact.ClientID;
            RealEstateBrokerSellerContact.LoanOriginatorIdentifierField = sTRIDRealEstateBrokerSellerContactNMLSNum.ClientID;
            RealEstateBrokerSellerContact.AgentLicenseField = sTRIDRealEstateBrokerSellerContactLicenseIDNum.ClientID;
            RealEstateBrokerSellerContact.EmailField = sTRIDRealEstateBrokerSellerEmail.ClientID;
            RealEstateBrokerSellerContact.PhoneField = sTRIDRealEstateBrokerSellerPhoneNum.ClientID;
        }

        protected override void OnInit(EventArgs e)
        {
            RegisterService("cfpb_utils", "/newlos/Test/RActiveGFEService.aspx");

            base.OnInit(e);

            if (IsArchivePage)
            {
                this.PageID = "ClosingDisclosureArchive";
            }
            else
            {
                this.PageID = "ClosingDisclosure";
            }

            var dataLoan = new CPageData(LoanID, new string[] { "sLastDisclosedClosingDisclosureArchiveId", "sClosingCostArchive" });
            dataLoan.InitLoad();

            if (ShowArchiveRecorder)
            {
                if (dataLoan.sLastDisclosedClosingDisclosureArchiveId != Guid.Empty)
                {
                    LastDisclosedClosingDisclosureDesc.Text = dataLoan.sLastDisclosedClosingDisclosureArchive.DateArchived;
                }
                else
                {
                    LastDisclosedClosingDisclosureDesc.Text = "None";
                }
            }
            if (IsArchivePage)
            {
                BindDDLArchives(dataLoan);
                if (!IsPostBack)
                {
                    var initialArchiveToDisplay = dataLoan.sLastDisclosedClosingDisclosureArchive;

                    if (initialArchiveToDisplay == null)
                    {
                        initialArchiveToDisplay = dataLoan.sClosingCostArchive
                            .First(a => a.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure);
                    }

                    Tools.SetDropDownListValue(ddlArchives, initialArchiveToDisplay.Id.ToString());
                    this.SetArchiveStatus(initialArchiveToDisplay.Status);
                }
            }
        }

        private void SetArchiveStatus(ClosingCostArchive.E_ClosingCostArchiveStatus status)
        {
            this.ArchiveStatus.Items.Clear();

            if (status == ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown)
            {
                this.ArchiveStatus.Items.Add(Tools.CreateEnumListItem("Unknown", ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown));
            }

            this.ArchiveStatus.Items.Add(Tools.CreateEnumListItem("Invalid", ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid));
            this.ArchiveStatus.Items.Add(Tools.CreateEnumListItem("Disclosed", ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed));

            Tools.SetDropDownListValue(this.ArchiveStatus, status);
        }

        public override bool IsReadOnly
        {
            get
            {
                return IsArchivePage || base.IsReadOnly;
            }
        }

        private void BindDDLArchives(CPageData dataLoan)
        {
            List<ListItem> items = new List<ListItem>();

            foreach (var archive in dataLoan.sClosingCostArchive.Where(p => p.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure).OrderBy(p => DateTime.Parse(p.DateArchived)))
            {
                items.Add(new ListItem(archive.DateArchived, archive.Id.ToString()));
            }

            ddlArchives.DataTextField = "Text";
            ddlArchives.DataValueField = "Value";
            ddlArchives.DataSource = items;
            ddlArchives.DataBind();
        }

        private void LoadCFM( CPageData dataLoan)
        {
            IPreparerFields lender = dataLoan.sTRIDLender;
            LenderContact.IsLocked = lender.IsLocked;
            LenderContact.AgentRoleT = lender.AgentRoleT;
            sTRIDLenderAgentId.Value = lender.AgentId.ToString();
            sTRIDLenderName.Text = lender.CompanyName;
            sTRIDLenderAddr.Text = lender.StreetAddr;
            sTRIDLenderCity.Value = lender.City;
            sTRIDLenderState.Value = lender.State;
            sTRIDLenderZip.Text = lender.Zip;
            sTRIDLenderNMLSNum.Text = lender.CompanyLoanOriginatorIdentifier;
            sTRIDLenderLicenseIDNum.Text = lender.LicenseNumOfCompany;
            sTRIDLenderContact.Text = lender.PreparerName;
            sTRIDLenderContactNMLSNum.Text = lender.LoanOriginatorIdentifier;
            sTRIDLenderContactLicenseIDNum.Text = lender.LicenseNumOfAgent;
            sTRIDLenderContactEmail.Text = lender.EmailAddr;
            sTRIDLenderContactPhoneNum.Text = lender.Phone;

            IPreparerFields MortgageBroker = dataLoan.sTRIDMortgageBroker;
            MortgageBrokerContact.IsLocked = MortgageBroker.IsLocked;
            MortgageBrokerContact.AgentRoleT = MortgageBroker.AgentRoleT;
            sTRIDMortgageBrokerAgentId.Value = MortgageBroker.AgentId.ToString();
            sTRIDMortgageBrokerName.Text = MortgageBroker.CompanyName;
            sTRIDMortgageBrokerAddr.Text = MortgageBroker.StreetAddr;
            sTRIDMortgageBrokerCity.Value = MortgageBroker.City;
            sTRIDMortgageBrokerState.Value = MortgageBroker.State;
            sTRIDMortgageBrokerZip.Text = MortgageBroker.Zip;
            sTRIDMortgageBrokerNMLSNum.Text = MortgageBroker.CompanyLoanOriginatorIdentifier;
            sTRIDMortgageBrokerLicenseIDNum.Text = MortgageBroker.LicenseNumOfCompany;
            sTRIDMortgageBrokerContact.Text = MortgageBroker.PreparerName;
            sTRIDMortgageBrokerContactNMLSNum.Text = MortgageBroker.LoanOriginatorIdentifier;
            sTRIDMortgageBrokerContactLicenseIDNum.Text = MortgageBroker.LicenseNumOfAgent;
            sTRIDMortgageBrokerEmail.Text = MortgageBroker.EmailAddr;
            sTRIDMortgageBrokerPhoneNum.Text = MortgageBroker.Phone;

            IPreparerFields SettlementAgent = dataLoan.sTRIDSettlementAgent;
            SettlementAgentContact.IsLocked = SettlementAgent.IsLocked;
            SettlementAgentContact.AgentRoleT = SettlementAgent.AgentRoleT;
            sTRIDSettlementAgentAgentId.Value = SettlementAgent.AgentId.ToString();
            sTRIDSettlementAgentName.Text = SettlementAgent.CompanyName;
            sTRIDSettlementAgentAddr.Text = SettlementAgent.StreetAddr;
            sTRIDSettlementAgentCity.Value = SettlementAgent.City;
            sTRIDSettlementAgentState.Value = SettlementAgent.State;
            sTRIDSettlementAgentZip.Text = SettlementAgent.Zip;
            sTRIDSettlementAgentNMLSNum.Text = SettlementAgent.CompanyLoanOriginatorIdentifier;
            sTRIDSettlementAgentLicenseIDNum.Text = SettlementAgent.LicenseNumOfCompany;
            sTRIDSettlementAgentContact.Text = SettlementAgent.PreparerName;
            sTRIDSettlementAgentContactNMLSNum.Text = SettlementAgent.LoanOriginatorIdentifier;
            sTRIDSettlementAgentContactLicenseIDNum.Text = SettlementAgent.LicenseNumOfAgent;
            sTRIDSettlementAgentEmail.Text = SettlementAgent.EmailAddr;
            sTRIDSettlementAgentPhoneNum.Text = SettlementAgent.Phone;

            IPreparerFields RealEstateBrokerBuyer = dataLoan.sTRIDRealEstateBrokerBuyer;
            RealEstateBrokerBuyerContact.IsLocked = RealEstateBrokerBuyer.IsLocked;
            RealEstateBrokerBuyerContact.AgentRoleT = RealEstateBrokerBuyer.AgentRoleT;
            sTRIDRealEstateBrokerBuyerAgentId.Value = RealEstateBrokerBuyer.AgentId.ToString();
            sTRIDRealEstateBrokerBuyerName.Text = RealEstateBrokerBuyer.CompanyName;
            sTRIDRealEstateBrokerBuyerAddr.Text = RealEstateBrokerBuyer.StreetAddr;
            sTRIDRealEstateBrokerBuyerCity.Value = RealEstateBrokerBuyer.City;
            sTRIDRealEstateBrokerBuyerState.Value = RealEstateBrokerBuyer.State;
            sTRIDRealEstateBrokerBuyerZip.Text = RealEstateBrokerBuyer.Zip;
            sTRIDRealEstateBrokerBuyerNMLSNum.Text = RealEstateBrokerBuyer.CompanyLoanOriginatorIdentifier;
            sTRIDRealEstateBrokerBuyerLicenseIDNum.Text = RealEstateBrokerBuyer.LicenseNumOfCompany;
            sTRIDRealEstateBrokerBuyerContact.Text = RealEstateBrokerBuyer.PreparerName;
            sTRIDRealEstateBrokerBuyerContactNMLSNum.Text = RealEstateBrokerBuyer.LoanOriginatorIdentifier;
            sTRIDRealEstateBrokerBuyerContactLicenseIDNum.Text = RealEstateBrokerBuyer.LicenseNumOfAgent;
            sTRIDRealEstateBrokerBuyerEmail.Text = RealEstateBrokerBuyer.EmailAddr;
            sTRIDRealEstateBrokerBuyerPhoneNum.Text = RealEstateBrokerBuyer.Phone;

            IPreparerFields RealEstateBrokerSeller = dataLoan.sTRIDRealEstateBrokerSeller;
            RealEstateBrokerSellerContact.IsLocked = RealEstateBrokerSeller.IsLocked;
            RealEstateBrokerSellerContact.AgentRoleT = RealEstateBrokerSeller.AgentRoleT;
            sTRIDRealEstateBrokerSellerAgentId.Value = RealEstateBrokerSeller.AgentId.ToString();
            sTRIDRealEstateBrokerSellerName.Text = RealEstateBrokerSeller.CompanyName;
            sTRIDRealEstateBrokerSellerAddr.Text = RealEstateBrokerSeller.StreetAddr;
            sTRIDRealEstateBrokerSellerCity.Value = RealEstateBrokerSeller.City;
            sTRIDRealEstateBrokerSellerState.Value = RealEstateBrokerSeller.State;
            sTRIDRealEstateBrokerSellerZip.Text = RealEstateBrokerSeller.Zip;
            sTRIDRealEstateBrokerSellerNMLSNum.Text = RealEstateBrokerSeller.CompanyLoanOriginatorIdentifier;
            sTRIDRealEstateBrokerSellerLicenseIDNum.Text = RealEstateBrokerSeller.LicenseNumOfCompany;
            sTRIDRealEstateBrokerSellerContact.Text = RealEstateBrokerSeller.PreparerName;
            sTRIDRealEstateBrokerSellerContactNMLSNum.Text = RealEstateBrokerSeller.LoanOriginatorIdentifier;
            sTRIDRealEstateBrokerSellerContactLicenseIDNum.Text = RealEstateBrokerSeller.LicenseNumOfAgent;
            sTRIDRealEstateBrokerSellerEmail.Text = RealEstateBrokerSeller.EmailAddr;
            sTRIDRealEstateBrokerSellerPhoneNum.Text = RealEstateBrokerSeller.Phone;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            RegisterJsScript("ThirdParty/ractive-legacy.min.js");
            RegisterJsScript("loanframework2.js");


            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("Ractive-Common.js");
            RegisterJsScript("LQBPopup.js");

            RegisterJsGlobalVariables("IsReadOnly", IsReadOnly);

            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_TridLoanPurposeType(sTridLPurposeT);
            Tools.Bind_sLT(sLT);

            ArchiveRow.Visible = ShowArchiveRecorder;
            SelectArchiveRow.Visible = IsArchivePage;
            this.ArchiveStatus.Enabled = this.BrokerUser.HasPermission(Permission.AllowManuallyArchivingGFE);

            CPageData dataLoan;

            var includeRenovationLineJCosts = this.Broker.EnableRenovationLoanSupport;
            if (IsArchivePage)
            {
                // Apply selected archive and load UI.

                var archiveFields = ClosingCostArchive.GetLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure);
                dataLoan = new CPageData(LoanID, archiveFields.Union(CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(ClosingDisclosure))));
                dataLoan.InitLoad();

                if (dataLoan.sClosingCostArchive.Count != 0)
                {
                    var selectedArchive = dataLoan.sClosingCostArchive.Where(p => p.Id.ToString() == ddlArchives.SelectedValue).FirstOrDefault();

                    if (selectedArchive == null)
                    {
                        throw new CBaseException("Archive not found", "Archive not found");
                    }

                    // Only include the breakdown of section J fees when the archive
                    // has the relevant fields. Otherwise, live data will be used, 
                    // which could lead to a discrepancy.
                    includeRenovationLineJCosts &= selectedArchive.HasValue("sTRIDClosingDisclosureAltCost") &&
                        selectedArchive.HasValue("sRenoFeesInClosingCosts");

                    if (dataLoan.sLastDisclosedClosingDisclosureArchiveId != Guid.Empty)
                    {
                        this.LastDisclosedClosingDisclosureArchiveDate.Text = dataLoan.sLastDisclosedClosingDisclosureArchive.DateArchived;
                    }
                    else
                    {
                        this.LastDisclosedClosingDisclosureArchiveDate.Text = "None";
                    }

                    dataLoan.ApplyClosingCostArchive(selectedArchive);

                    this.SetArchiveStatus(selectedArchive.Status);
                }
                else
                {
                    throw new CBaseException("No archives found", "No archives found.");
                }
            }
            else
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ClosingDisclosure));
                dataLoan.InitLoad();
            }

            if (dataLoan.sLoanTransactionInvolvesSeller == E_TriState.Yes)
            {
                SellerInfoRepeater.DataSource = dataLoan.sSellerCollection.ListOfSellers;
                SellerInfoRepeater.DataBind();
            }
            else
            {
                SellerInfoRow.Visible = false;
            }

            PurposeRow.Attributes.Add("class",
                LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges) ?
                "show-sTridLPurposeT" :
                "show-sLPurposeT");

            Func<Proration, bool> prorationPaidAdvance = p => p.IsPaidFromBorrowerToSeller;
            Func<Proration, bool> prorationUnpaid = p => p.IsPaidFromSellerToBorrower;

            IEnumerable<Proration> prorationPaidList = null;
            IEnumerable<Proration> prorationUnpaidList = null;

            bool excludeProrationsInLeCd = dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && !dataLoan.sProrationList.IncludeProrationsInLeCdForThisLien;

            if (!excludeProrationsInLeCd)
            {
                prorationPaidList = dataLoan.sProrationList.Where(prorationPaidAdvance);
                prorationUnpaidList = dataLoan.sProrationList.Where(prorationUnpaid);

                ProrationPaidBySellerAdvance1.DataSource = prorationPaidList;
                ProrationPaidBySellerAdvance1.DataBind();

                ProrationPaidBySellerAdvance2.DataSource = prorationPaidList;
                ProrationPaidBySellerAdvance2.DataBind();

                ProrationUnpaidBySeller1.DataSource = prorationUnpaidList;
                ProrationUnpaidBySeller1.DataBind();

                ProrationUnpaidBySeller2.DataSource = prorationUnpaidList;
                ProrationUnpaidBySeller2.DataBind();
            }

            sPurchasePrice1003_2.InnerText = dataLoan.sPurchasePrice1003_rep;
            sPurchasePrice1003_3.InnerText = dataLoan.sPurchasePrice1003_rep;

            sGrossDueFromBorrPersonalProperty.InnerText = dataLoan.sGrossDueFromBorrPersonalProperty_rep;
            sGrossDueFromBorrPersonalProperty2.InnerText = dataLoan.sGrossDueFromBorrPersonalProperty_rep;

            sTRIDClosingDisclosureBorrowerCostsPaidAtClosing.InnerText = dataLoan.sTRIDClosingDisclosureBorrowerCostsPaidAtClosing_rep;
            this.lbl_sTRIDClosingDisclosureAltCost.Text = dataLoan.sTRIDClosingDisclosureAltCostDescription;
            sTRIDClosingDisclosureAltCost.InnerText = dataLoan.sTRIDClosingDisclosureAltCost_rep;
            sLandIfAcquiredSeparately1003.InnerText = dataLoan.sLandIfAcquiredSeparately1003_rep;
            sTotalSellerPaidClosingCostsPaidAtClosing.InnerText = dataLoan.sTotalSellerPaidClosingCostsPaidAtClosing_rep;



            div_sTRIDClosingDisclosureAltCost.Visible = dataLoan.sTRIDClosingDisclosureAltCost != 0;
            div_sLandIfAcquiredSeparately1003.Visible = dataLoan.sLandIfAcquiredSeparately1003 != 0;

            sTRIDCashDeposit2.InnerText = dataLoan.sTRIDCashDeposit_rep;

            sFinalLAmt2.InnerText = dataLoan.sFinalLAmt_rep;

            string sellerCreditRep = dataLoan.sAdjustmentList.SellerCredit.Amount_rep;

            if (dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && !dataLoan.sAdjustmentList.SellerCredit.IncludeAdjustmentInLeCdForThisLien)
            {
                sellerCreditRep = dataLoan.m_convertLos.ToMoneyString(0M, FormatDirection.ToRep);
            }

            SellerCredit1.InnerText = sellerCreditRep;
            SellerCredit2.InnerText = sellerCreditRep;

            sReductionsDueToSellerExcessDeposit.InnerText = dataLoan.sReductionsDueToSellerExcessDeposit_rep;
            sReductionsDueToSellerPayoffOf1stMtgLoan.InnerText = dataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep;
            sReductionsDueToSellerPayoffOf2ndMtgLoan.InnerText = dataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep;

            if (dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017)
            {
                AdjustmentsPaidFromBorr.DataSource = dataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCredit().Where(p => p.PaidFromParty == E_PartyT.Borrower && p.IncludeAdjustmentInLeCdForThisLien);
                AdjustmentsPaidFromBorr.DataBind();

                AdjustmentsPaidToBorrower.DataSource = dataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCredit().Where(p => p.PaidToParty == E_PartyT.Borrower && p.IncludeAdjustmentInLeCdForThisLien);
                AdjustmentsPaidToBorrower.DataBind();

                AdjustmentsPaidToSeller.DataSource = dataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCredit().Where(p => p.PaidToParty == E_PartyT.Seller && p.IncludeAdjustmentInLeCdForThisLien);
                AdjustmentsPaidToSeller.DataBind();

                AdjustmentsPaidFromSeller.DataSource = dataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCredit().Where(p => p.PaidFromParty == E_PartyT.Seller && p.IncludeAdjustmentInLeCdForThisLien);
                AdjustmentsPaidFromSeller.DataBind();
            }
            else
            {
                AdjustmentsPaidFromBorr.DataSource = dataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCredit().Where(p => p.PaidFromParty == E_PartyT.Borrower);
                AdjustmentsPaidFromBorr.DataBind();

                AdjustmentsPaidToBorrower.DataSource = dataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCredit().Where(p => p.PaidToParty == E_PartyT.Borrower);
                AdjustmentsPaidToBorrower.DataBind();

                AdjustmentsPaidToSeller.DataSource = dataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCredit().Where(p => p.PaidToParty == E_PartyT.Seller);
                AdjustmentsPaidToSeller.DataBind();

                AdjustmentsPaidFromSeller.DataSource = dataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCredit().Where(p => p.PaidFromParty == E_PartyT.Seller);
                AdjustmentsPaidFromSeller.DataBind();
            }            

            sTRIDTotalDueFromBorrowerAtClosing.InnerText = dataLoan.sTRIDTotalDueFromBorrowerAtClosing_rep;
            sTRIDTotalPaidAlreadyByOrOnBehalfOfBorrowerAtClosing.InnerText = dataLoan.sTRIDTotalPaidAlreadyByOrOnBehalfOfBorrowerAtClosing_rep;
            sTRIDTotalDueToSellerAtClosing.InnerText = dataLoan.sTRIDTotalDueToSellerAtClosing_rep;
            sTRIDTotalDueFromSellerAtClosing.InnerText = dataLoan.sTRIDTotalDueFromSellerAtClosing_rep;

            sTRIDTotalDueFromBorrowerAtClosing2.InnerText = dataLoan.sTRIDTotalDueFromBorrowerAtClosing_rep;
            sTRIDTotalPaidAlreadyByOrOnBehalfOfBorrowerAtClosing2.InnerText = dataLoan.sTRIDTotalPaidAlreadyByOrOnBehalfOfBorrowerAtClosing_rep;
            sTRIDTotalDueToSellerAtClosing2.InnerText = dataLoan.sTRIDTotalDueToSellerAtClosing_rep;
            sTRIDTotalDueFromSellerAtClosing2.InnerText = dataLoan.sTRIDTotalDueFromSellerAtClosing_rep;

            sTRIDSummariesOfTransactionCashFromBorrower.InnerText = dataLoan.sTRIDSummariesOfTransactionCashFromBorrower_rep;
            sTRIDSummariesOfTransactionCashToSeller.InnerText = dataLoan.sTRIDSummariesOfTransactionCashToSeller_rep;

            InitialiseCFM();
            LoadCFM(dataLoan);

            sTRIDLenderZip.SmartZipcode(sTRIDLenderCity, sTRIDLenderState);
            sTRIDMortgageBrokerZip.SmartZipcode(sTRIDMortgageBrokerCity, sTRIDMortgageBrokerState);
            sTRIDSettlementAgentZip.SmartZipcode(sTRIDSettlementAgentCity, sTRIDSettlementAgentState);
            sTRIDRealEstateBrokerBuyerZip.SmartZipcode(sTRIDRealEstateBrokerBuyerCity, sTRIDRealEstateBrokerBuyerState);
            sTRIDRealEstateBrokerSellerZip.SmartZipcode(sTRIDRealEstateBrokerSellerCity, sTRIDRealEstateBrokerSellerState);

            ClosingDisclosureFeeViewModel viewModel = new ClosingDisclosureFeeViewModel();
            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;

            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;

            viewModel.sDocMagicClosingD = dataLoan.sDocMagicClosingD_rep;
            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;

            viewModel.sTRIDLoanEstimateTotalLoanCosts = dataLoan.sTRIDLoanEstimateTotalLoanCosts_rep;
            viewModel.sTRIDLoanEstimateTotalClosingCosts = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
            viewModel.sTRIDLoanEstimateTotalOtherCosts = dataLoan.sTRIDLoanEstimateTotalOtherCosts_rep;
            viewModel.sTRIDLoanEstimateLenderCredits = dataLoan.sTRIDLoanEstimateLenderCredits_rep;
            viewModel.sTRIDLoanEstimateTotalAllCosts = dataLoan.sTRIDLoanEstimateTotalAllCosts_rep;

            viewModel.sGfeIsTPOTransaction = dataLoan.sGfeIsTPOTransaction;

            viewModel.sLPurposeT = dataLoan.sLPurposeT;
            viewModel.sTridLPurposeT = dataLoan.sTridLPurposeT;
            viewModel.sLoanTransactionInvolvesSeller = dataLoan.sLoanTransactionInvolvesSeller;

            if (m_convertLos.ToMoney(dataLoan.sToleranceCure_rep) > 0)
            {
                viewModel.sToleranceCure = dataLoan.sToleranceCure_rep;
            }

            viewModel.sLenderPaidBrokerCompF = dataLoan.sLenderPaidBrokerCompF_rep;

            viewModel.SectionList = dataLoan.sBorrowerAndSellerClosingCostSet.GetCompressedView(E_ClosingCostViewT.CombinedLenderTypeEstimate, optionalCollapseBorrFin:true);

            RegisterJsObject("ClosingCostData", viewModel);
            ClientScript.RegisterHiddenField("loanid", LoanID.ToString());

            sApprVal.Text = dataLoan.sApprVal_rep;

            if (dataLoan.sLPurposeT == E_sLPurposeT.Purchase)
            {
                sPurchasePrice1003.Text = dataLoan.sPurchasePrice1003_rep;
                sApprVal.Visible = false;
                ValueDesc.InnerText = "Sale Price";
            }
            else
            {
                sPurchasePrice1003.Text = dataLoan.sApprVal_rep;
                sPurchasePrice1003.Visible = false;
                ValueDesc.InnerText = "Appraised Value";
            }

            sSpAddr.Value = dataLoan.sSpAddr;
            sSpCity.Value = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;
            sSpZip.SmartZipcode(sSpCity, sSpState);

            List<Address> addresses = new List<Address>();
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData app = dataLoan.GetAppData(i);
                Address address = new Address();
                address.aBAddrMail = app.aBAddrMail;
                address.aBCityMail = app.aBCityMail;
                address.aBNm = app.aBNm;
                address.aBStateMail = app.aBStateMail;
                address.aBZipMail = app.aBZipMail;
                address.aCAddrMail = app.aCAddrMail;
                address.aCCityMail = app.aCCityMail;
                address.aCNm = app.aCNm;
                address.aCStateMail = app.aCStateMail;
                address.aCZipMail = app.aCZipMail;

                address.aBHasSpouse = app.aBHasSpouse;
                address.sameAddr = app.aBAddrMail_SingleLine.Equals(app.aCAddrMail_SingleLine);

                addresses.Add(address);
            }

            DataModel dataModel = new DataModel();
            dataModel.addresses = addresses;

            dataModel.HousingExpenses = new List<ExpenseData>();
            dataModel.NonEscrowedDescriptions = new List<string>();
            dataModel.EscrowedDescriptions = new List<string>();

            foreach (BaseHousingExpense expense in dataLoan.sHousingExpenses.ExpensesToUse)
            {
                if (expense.MonthlyAmtTotal != 0)
                {
                    ExpenseData data = new ExpenseData();
                    data.amount = expense.MonthlyAmtTotal_rep;
                    data.inEscrow = expense.IsEscrowedAtClosing == E_TriState.Yes;
                    data.expenseName = expense.DescriptionOrDefault;
                    dataModel.HousingExpenses.Add(data);

                    if (data.inEscrow)
                    {
                        dataModel.EscrowedDescriptions.Add(data.expenseName);
                    }
                    else
                    {
                        dataModel.NonEscrowedDescriptions.Add(data.expenseName);
                    }
                }
            }

            if (dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && dataLoan.sProMIns > 0)
            {
                dataModel.EscrowedDescriptions.Add("Mortgage Insurance");
            }

            dataModel.ProjectedPayments = dataLoan.sTridProjectedPayments;
            if (dataModel.ProjectedPayments != null)
            {
                dataModel.ProjectedPayments.ForEach(p => p.SerializeMoneyRepFields = true);
            }

            RegisterJsObjectWithJsonNetSerializer("DataJSON", dataModel);
            RegisterJsObject("EditableFields", EditableFields);
            RegisterJsObject("EditableFieldsModel", new ClosingDisclosureEditableFieldsModel());

            sTerm.Value = dataLoan.sTerm_rep;
            sDue.Value = dataLoan.sDue_rep;
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sTridLPurposeT, dataLoan.sTridLPurposeT);
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            sLenderCaseNum.Value = dataLoan.sLenderCaseNum;
            sLenderCaseNumLckd.Checked = dataLoan.sLenderCaseNumLckd;

            //Loan Terms Section
            sFinalLamt.Text = dataLoan.sFinalLAmt_rep;
            sTridNoteIR.Text = dataLoan.sTridNoteIR_rep;
            sTridProThisMPmt.Text = dataLoan.sTridProThisMPmt_rep;
            sGfeCanLoanBalanceIncrease_True.Checked = dataLoan.sGfeCanLoanBalanceIncrease;
            sPmtAdjRecastStop.Value = dataLoan.sPmtAdjRecastStop_rep;
            sGfeMaxLoanBalance.Text = dataLoan.sGfeMaxLoanBalance_rep;
            sGfeCanRateIncrease_True.Checked = dataLoan.sGfeCanRateIncrease;
            sRAdjCapMon.Value = dataLoan.sRAdjCapMon_rep;
            sRAdj1stCapMon.Value = dataLoan.sRAdj1stCapMon_rep;
            sGfeCanPaymentIncrease_True.Checked = dataLoan.sGfeCanPaymentIncrease;
            sGfeHavePpmtPenalty_True.Checked = dataLoan.sGfeHavePpmtPenalty;
            sGfeMaxPpmtPenaltyAmt.Text = dataLoan.sGfeMaxPpmtPenaltyAmt_rep;
            //TotalPrepmtMonths
            sGfeIsBalloon_True.Enabled = dataLoan.sGfeIsBalloonLckd;
            sGfeIsBalloon_False.Enabled = dataLoan.sGfeIsBalloonLckd;
            sGfeIsBalloon_True.Checked = dataLoan.sGfeIsBalloon;
            sGfeBalloonPmt.Text = dataLoan.sGfeBalloonPmt_rep;
            sDue2.Value = dataLoan.sDue_rep;

            //Projected Payments Section
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;

            sMonthlyNonPINonMIExpenseTotalPITI.Text = dataLoan.sMonthlyNonPINonMIExpenseTotalPITI_rep;

            // Adjustable Payments Table
            IsInterestOnlyPmts.InnerText = dataLoan.sIOnlyMon > 0 ? "Yes, for first " + dataLoan.sIOnlyMon_rep + " payments" : "No";
            IsOptionalPmts.InnerText = dataLoan.sIsOptionArm ? "Yes, for first " + dataLoan.sPmtAdjRecastStop_rep + " payments" : "No";
            IsStepPmts.InnerText = dataLoan.sFinMethT == E_sFinMethT.Graduated ? "Yes, for first " + dataLoan.sGradPmtYrs_rep + " years" : "No";
            IsSeasonalPmts.InnerText = "No";

            //Adjustable Interest Rates Table
            if (dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                IndexAndMarginDesc.InnerText = "Index + Margin";
                IndexAndMarginVal.InnerText = dataLoan.sArmIndexNameVstr + " + " + dataLoan.sRAdjMarginR_rep;

                FrequencyFirstChange.InnerText = "1st payment after " + dataLoan.sRAdj1stCapMon_rep + " months";

                FrequencySubsequentChange.InnerText = "Every " + dataLoan.sRAdjCapMon_rep + " months after first change";

                
            }
            else if (dataLoan.sBuydwnMon1 > 0)
            {
                IndexAndMarginDesc.InnerText = "Step Rate Max Adjustment";

                IndexAndMarginVal.InnerText = dataLoan.sMaxBuydwnR_rep;

                FrequencyFirstChange.InnerText = "1st payment after " + dataLoan.sBuydwnMon1_rep + " months";

                if (dataLoan.sBuydwnMon2 > 0)
                {
                    FrequencySubsequentChange.InnerText = "Every " + dataLoan.sBuydwnMon2_rep + " months";
                }
                else
                {
                    FrequencySubsequentChange.InnerText = "No change after first change";
                }
            }
            else
            {
                IndexAndMarginDesc.InnerText = "Index + Margin";
                IndexAndMarginVal.InnerText = "N/A";
                FrequencyFirstChange.InnerText = "N/A";
                FrequencySubsequentChange.InnerText = "N/A";
                LimitFirstchange.InnerText = "N/A";
                LimitSubsequentChange.InnerText = "N/A";
            }

            MinMaxInterestRate.InnerText = dataLoan.sMinMaxInterestRate_rep;

            LimitFirstchange.InnerText = dataLoan.sLimitsOnInterestRateFirstChange_rep;
            LimitSubsequentChange.InnerText = dataLoan.sLimitsOnInterestRateSubsequentChanges_rep;

            InitialInterestRate.InnerText = dataLoan.sSchedIR1_rep;

            sAPR.Text = dataLoan.sApr_rep;

            sLateDays.Text = dataLoan.sLateDays;
            sLateChargePc.Text = dataLoan.sLateChargePc;
            sLateChargeBaseDesc.Text = dataLoan.sLateChargeBaseDesc;

            sAssumeLT_Allow.Checked = dataLoan.sAssumeLT == E_sAssumeLT.May || dataLoan.sAssumeLT == E_sAssumeLT.MaySubjectToCondition;
            sAssumeLT_NotAllow.Checked = !sAssumeLT_Allow.Checked;

            sCFPBTotalInterestPercent.Text = dataLoan.sCFPBTotalInterestPercent_rep;

            sTRIDClosingDisclosureTotalLoanCosts.Text = dataLoan.sTRIDClosingDisclosureTotalLoanCosts_rep;
            sTRIDClosingDisclosureTotalOtherCosts.Text = dataLoan.sTRIDClosingDisclosureTotalOtherCosts_rep;
            sTRIDNegativeClosingDisclosureGeneralLenderCredits.InnerText = dataLoan.sTRIDNegativeClosingDisclosureGeneralLenderCredits_rep;
            sTRIDClosingDisclosureGeneralLenderCredits.Text = dataLoan.sTRIDClosingDisclosureGeneralLenderCredits_rep;
            sTRIDClosingDisclosureTotalClosingCosts.Text = dataLoan.sTRIDClosingDisclosureTotalClosingCosts_rep;
            sTRIDClosingDisclosureTotalClosingCosts2.Text = dataLoan.sTRIDClosingDisclosureTotalClosingCosts_rep;
            sTRIDClosingDisclosureTotalClosingCosts3.InnerText = dataLoan.sTRIDClosingDisclosureTotalClosingCosts_rep;
            sTRIDClosingDisclosureLastLoanEstimateTotalClosingCosts.Text = dataLoan.sTRIDClosingDisclosureLastLoanEstimateTotalClosingCosts_rep;
            sTRIDClosingDisclosureClosingCostsPaidBeforeClosing.Text = dataLoan.sTRIDClosingDisclosureClosingCostsPaidBeforeClosing_rep;
            sTRIDClosingDisclosureBorrowerFinancedClosingCosts.Text = dataLoan.sTRIDClosingDisclosureBorrowerFinancedClosingCosts_rep;
            sTRIDClosingDisclosureLastLoanEstimateBorrowerFinancedClosingCosts.Text = dataLoan.sTRIDClosingDisclosureLastLoanEstimateBorrowerFinancedClosingCosts_rep;
            sTRIDClosingDisclosureDownPaymentOrFundsFromBorrower.Text = dataLoan.sTRIDClosingDisclosureDownPaymentOrFundsFromBorrower_rep;
            sTRIDClosingDisclosureLastLoanEstimateDownPaymentOrFundsFromBorrower.Text = dataLoan.sTRIDClosingDisclosureLastLoanEstimateDownPaymentOrFundsFromBorrower_rep;
            sTRIDClosingDisclosureLastLoanEstimateCashDeposit.Text = dataLoan.sTRIDClosingDisclosureLastLoanEstimateCashDeposit_rep;
            sTRIDClosingDisclosureFundsForBorrower.Text = dataLoan.sTRIDClosingDisclosureFundsForBorrower_rep;
            sTRIDClosingDisclosureLastLoanEstimateFundsForBorrower.Text = dataLoan.sTRIDClosingDisclosureLastLoanEstimateFundsForBorrower_rep;
            sTRIDClosingDisclosureLastLoanEstimateSellerCredits.Text = dataLoan.sTRIDClosingDisclosureLastLoanEstimateSellerCredits_rep;
            sTRIDClosingDisclosureLastLoanEstimateAdjustmentsAndOtherCredits.Text = dataLoan.sTRIDClosingDisclosureLastLoanEstimateAdjustmentsAndOtherCredits_rep;
            sTRIDClosingDisclosureCashToClose.Text = dataLoan.sTRIDClosingDisclosureCashToClose_rep;
            sTRIDClosingDisclosureLastLoanEstimateCashToClose.Text = dataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToClose_rep;

            sTRIDCashDeposit.Text = dataLoan.sTRIDCashDeposit_rep;
            sTRIDSellerCredits.Text = dataLoan.sTRIDSellerCredits_rep;
            sTRIDAdjustmentsAndOtherCredits.Text = dataLoan.sTRIDAdjustmentsAndOtherCredits_rep;

            sInterestRateMaxEverStartAfterMon.Value = dataLoan.sInterestRateMaxEverStartAfterMon_rep;
            sTRIDPIPmtSubsequentAdjustFreqMon.Value = dataLoan.sTRIDPIPmtSubsequentAdjustFreqMon_rep;
            sPIPmtFirstChangeAfterMon.Value = dataLoan.sPIPmtFirstChangeAfterMon_rep;
            sHardPlusSoftPrepmtPeriodMonths.Value = dataLoan.sHardPlusSoftPrepmtPeriodMonths_rep;
            sPIPmtMaxEverStartAfterMon.Value = dataLoan.sPIPmtMaxEverStartAfterMon_rep;

            sTRIDInterestRateMaxEver.Text = dataLoan.sTRIDInterestRateMaxEver_rep;
            sTRIDPIPmtMaxEver.Text = dataLoan.sTRIDPIPmtMaxEver_rep;
            sIOnlyMon.Value = dataLoan.sIOnlyMon_rep;

            sDocMagicClosingD.Text = dataLoan.sDocMagicClosingD_rep;
            sDocMagicClosingDLckd.Checked = dataLoan.sDocMagicClosingDLckd;
            sConsummationD.Text = dataLoan.sConsummationD_rep;
            sConsummationDLckd.Checked = dataLoan.sConsummationDLckd;
            sConsummationD2.Text = dataLoan.sConsummationD_rep;
            sConsummationD2Lckd.Checked = dataLoan.sConsummationDLckd;
            sDocMagicDisbursementD.Text = dataLoan.sDocMagicDisbursementD_rep;
            sDocMagicDisbursementDLckd.Checked = dataLoan.sDocMagicDisbursementDLckd;

            sMICertID.Value = dataLoan.sMiCertId;
            sSettlementAgentFileNum.Value = dataLoan.sSettlementAgentFileNum;

            sFinCharge.Text = dataLoan.sFinCharge_rep;
            sFinancedAmt.Text = dataLoan.sFinancedAmt_rep;
            sTRIDTotalOfPayments.Text = dataLoan.sTRIDTotalOfPayments_rep;
            
            // 4/17/2015 gf - We do not want to display all 3 tables for old files
            // that would display the worst case as the standard.
            if (dataLoan.sFinMethT == E_sFinMethT.ARM && !dataLoan.sRAdjWorstIndex)
            {
                this.ViewAmortizationScheduleBtn.Value = "View Amortization Schedules";
            }

            if (IsLeadPage)
            {
                PageHeader.InnerText = "Initial Fees Worksheet - CFPB";
            }

            string MIFirstChangeString = "";
            string MISubsequentChangeString = "";
            string MIMaximumPaymentString = "";

            MIMaximumPaymentString = dataLoan.sTRIDPIPmtMaxEver_rep + " after " + dataLoan.sPIPmtMaxEverStartAfterMon_rep + " months.";


            if (dataLoan.sGfeCanPaymentIncrease && dataLoan.sPIPmtFirstChangeAfterMon.HasValue)
            {
                try
                {
                    int index = dataLoan.sPIPmtFirstChangeAfterMon.Value + 1;
                    AmortItem bestAmortItem = dataLoan.GetAmortTable(E_AmortizationScheduleT.BestCase, false).Items[index];
                    AmortItem worstAmortItem = dataLoan.GetAmortTable(E_AmortizationScheduleT.WorstCase, false).Items[index];

                    if (bestAmortItem.PaymentWOMI == worstAmortItem.PaymentWOMI)
                    {
                        MIFirstChangeString = m_convertLos.ToMoneyString(bestAmortItem.PaymentWOMI, FormatDirection.ToRep) + " after " + dataLoan.sPIPmtFirstChangeAfterMon + " months.";
                    }
                    else
                    {
                        MIFirstChangeString = m_convertLos.ToMoneyString(bestAmortItem.PaymentWOMI, FormatDirection.ToRep) + " - " + m_convertLos.ToMoneyString(worstAmortItem.PaymentWOMI, FormatDirection.ToRep) + " after " + dataLoan.sPIPmtFirstChangeAfterMon + " months.";
                    }
                }
                catch (AmortTableInvalidArgumentException)
                {
                    MIFirstChangeString = "Unable to compute";
                }
            }
            else
            {
                MIFirstChangeString = "N/A";
            }

            if (dataLoan.sGfeCanPaymentIncrease && dataLoan.sTRIDPIPmtSubsequentAdjustFreqMon.HasValue && dataLoan.sTRIDPIPmtSubsequentAdjustFreqMon.Value > 0)
            {
                MISubsequentChangeString = "Every " + dataLoan.sTRIDPIPmtSubsequentAdjustFreqMon_rep + " months.";
            }
            else
            {
                MISubsequentChangeString = "N/A";
            }

            MIFirstChange.InnerText = MIFirstChangeString;
            MISubsequentChange.InnerText = MISubsequentChangeString;
            MIMaximumPayment.InnerText = MIMaximumPaymentString;

            SetSummedValues(dataLoan, m_convertLos);

            sHasDemandFeature_True.Checked = dataLoan.sHasDemandFeature;
            sHasDemandFeature_False.Checked = !dataLoan.sHasDemandFeature;

            sTRIDNegativeAmortizationT_Scheduled.Checked = dataLoan.sTRIDNegativeAmortizationT == E_sTRIDNegativeAmortizationT.Scheduled;
            sTRIDNegativeAmortizationT_NoNegativeAmort.Checked = dataLoan.sTRIDNegativeAmortizationT == E_sTRIDNegativeAmortizationT.NoNegativeAmortization;
            sTRIDNegativeAmortizationT_Potential.Checked = dataLoan.sTRIDNegativeAmortizationT == E_sTRIDNegativeAmortizationT.Potential;

            sSpFullAddr.Value = dataLoan.sSpFullAddr;

            sTRIDPartialPaymentAcceptanceT_AcceptApplied.Checked = dataLoan.sTRIDPartialPaymentAcceptanceT == E_sTRIDPartialPaymentAcceptanceT.AcceptedAndApplied;
            sTRIDPartialPaymentAcceptanceT_AcceptNotApplied.Checked = dataLoan.sTRIDPartialPaymentAcceptanceT == E_sTRIDPartialPaymentAcceptanceT.AcceptedButNotApplied;
            sTRIDPartialPaymentAcceptanceT_NotAccepted.Checked = dataLoan.sTRIDPartialPaymentAcceptanceT == E_sTRIDPartialPaymentAcceptanceT.NotAccepted;

            sTridEscrowAccountExists_True.Checked = dataLoan.sTridEscrowAccountExists;
            sTridEscrowAccountExists_False.Checked = !dataLoan.sTridEscrowAccountExists;

            sNonMIHousingExpensesEscrowedReasonT_LenderRequired.Enabled = dataLoan.sTridEscrowAccountExists;
            sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested.Enabled = dataLoan.sTridEscrowAccountExists;

            sNonMIHousingExpensesEscrowedReasonT_LenderRequired.Checked = dataLoan.sNonMIHousingExpensesEscrowedReasonT == E_sNonMIHousingExpensesEscrowedReasonT.LenderRequired;
            sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested.Checked = dataLoan.sNonMIHousingExpensesEscrowedReasonT == E_sNonMIHousingExpensesEscrowedReasonT.BorrowerRequested;

            sNonMIHousingExpensesNotEscrowedReasonT_Declined.Enabled = !dataLoan.sTridEscrowAccountExists;
            sNonMIHousingExpensesNotEscrowedReasonT_NotOffered.Enabled = !dataLoan.sTridEscrowAccountExists;

            sNonMIHousingExpensesNotEscrowedReasonT_Declined.Checked = dataLoan.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined;
            sNonMIHousingExpensesNotEscrowedReasonT_NotOffered.Checked = dataLoan.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.LenderDidNotOffer;

            sTRIDEscrowWaiverFee.Enabled = dataLoan.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined;
            sTRIDEscrowWaiverFee.Text = dataLoan.sTRIDEscrowWaiverFee_rep;

            if (!dataLoan.sTridEscrowAccountExists)
            {
                HasEscrowDiv.Visible = false;
            }

            sTRIDPropertyIsInNonRecourseLoanState_True.Checked = dataLoan.sTRIDPropertyIsInNonRecourseLoanState;
            sTRIDPropertyIsInNonRecourseLoanState_False.Checked = !dataLoan.sTRIDPropertyIsInNonRecourseLoanState;

            sClosingDisclosureEscrowedPropertyCostsFirstYear.InnerText = dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep;
            sClosingDisclosureNonEscrowedPropertyCostsFirstYear.InnerText = dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep;
            sGfeInitialImpoundDeposit.InnerText = dataLoan.sGfeInitialImpoundDeposit_rep;
            sClosingDisclosureMonthlyEscrowPayment.InnerText = dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep;

            sClosingDisclosurePropertyCostsFirstYear.InnerText = dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep;

            if (dataLoan.sTridEscrowAccountExists)
            {
                NonEscrowDiv.Visible = false;
            }

            sTRIDClosingDisclosureLastLoanEstimateFinalLAmt.Text = dataLoan.sTRIDClosingDisclosureLastLoanEstimateFinalLAmt_rep;
            sFinalLAmt3.Text = dataLoan.sFinalLAmt_rep;

            sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT_Standard.Checked = dataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Standard;
            sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT_Alternative.Checked = dataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative;

            sTRIDClosingDisclosureLastLoanEstimateTotalPayoffsAndPayments.Text = dataLoan.sTRIDClosingDisclosureLastLoanEstimateTotalPayoffsAndPayments_rep;
            sTRIDTotalPayoffsAndPayments.Text = dataLoan.sTRIDTotalPayoffsAndPayments_rep;

            sONewFinNetProceeds.InnerText = dataLoan.sONewFinNetProceeds_rep;
            sONewFinBal.Text = dataLoan.sONewFinBal_rep;

            bool trid2ExcludeSalesPriceAndFirstLienLoanAmountCredit = dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && dataLoan.sLienPosT == E_sLienPosT.Second && dataLoan.sIsOFinNew && dataLoan.sRemain1stMBal > 0;

            sONewFinNetProceedsDiv.Visible = (dataLoan.sONewFinNetProceeds != 0 || dataLoan.sONewFinBal != 0) && !trid2ExcludeSalesPriceAndFirstLienLoanAmountCredit;

            if (dataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Standard)
            {
                TotalLoanAmountRow.Visible = false;
                ClosingCostsPaidBeforeClosingTD.InnerText = "- " + ClosingCostsPaidBeforeClosingTD.InnerText;
                TotalPayoffsPaymentsRow.Visible = false;
            }
            else
            {
                TotalClosingCostsTD.InnerText = "- " + TotalClosingCostsTD.InnerText;
                ClosingCostsPaidBeforeClosingTD.InnerText = "+ " + ClosingCostsPaidBeforeClosingTD.InnerText;
                ClosingCostsFinancedRow.Visible = false;
                DownPaymentFundsRow.Visible = false;
                CashDepositRow.Visible = false;
                FundsForBorrRow.Visible = false;
                SellerCreditsRow.Visible = false;
                AdjustmentsCreditsRow.Visible = false;
            }

            sRefPdOffAmt1003.InnerText = dataLoan.sRefPdOffAmt1003_rep;
            sRefPdOffAmt1003Div.Visible = !dataLoan.sRefPdOffAmt1003_rep.Equals("$0.00") && !(dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && dataLoan.sLienToPayoffTotDebt == ResponsibleLien.OtherLienTransaction);

            bool isSellerCreditDivVisible = dataLoan.sAdjustmentList.SellerCredit.Amount != 0;

            if (dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017)
            {
                isSellerCreditDivVisible &= dataLoan.sAdjustmentList.SellerCredit.IncludeAdjustmentInLeCdForThisLien;
            }

            bool isTrid2AltMethodAnd2ndLien = dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && dataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative && dataLoan.sLienPosT == E_sLienPosT.Second;
            if (dataLoan.sLoanTransactionInvolvesSeller != E_TriState.Yes || isTrid2AltMethodAnd2ndLien)
            {
                SellerDiv1.Visible = false;
                SellerDiv2.Visible = false;
                SellerDiv3.Visible = false;
                SellerDiv4.Visible = false;
                SellerDiv5.Visible = false;

                sGrossDueFromBorrPersonalPropertyDiv.Visible = !dataLoan.sGrossDueFromBorrPersonalProperty_rep.Equals("$0.00");
                sTRIDCashDepositDiv.Visible = dataLoan.sTRIDCashDeposit != 0M;
                SellerCreditDiv.Visible = isSellerCreditDivVisible;
                ProrationPaidBySellerAdvanceDiv1.Visible = !excludeProrationsInLeCd && prorationPaidList.Count() > 0;
                ProrationUnpaidBySeller1Div.Visible = !excludeProrationsInLeCd && prorationUnpaidList.Count() > 0;
            }

            sPurchasePrice1003Div.Visible = dataLoan.sPurchasePrice1003 != 0 && !trid2ExcludeSalesPriceAndFirstLienLoanAmountCredit;
            sPurchasePrice1003DivSeller.Visible = !trid2ExcludeSalesPriceAndFirstLienLoanAmountCredit;
        }

        private void SetSummedValues(CPageData dataLoan, LosConvert m_convertLos)
        {
            Func<LoanClosingCostFee, bool> sectionPredicate = p =>
                p.GetTRIDSectionBasedOnDisclosure(usingClosingDisclosure: true) == E_IntegratedDisclosureSectionT.SectionA ||
                p.GetTRIDSectionBasedOnDisclosure(usingClosingDisclosure: true) == E_IntegratedDisclosureSectionT.SectionB ||
                p.GetTRIDSectionBasedOnDisclosure(usingClosingDisclosure: true) == E_IntegratedDisclosureSectionT.SectionC;

            Func<LoanClosingCostFeePayment, bool> sTRIDClosingDisclosureTotalLoanCostsOfBorrowerAtClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
            Func<LoanClosingCostFeePayment, bool> sTRIDClosingDisclosureTotalLoanCostsOfBorrowerBeforeClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);


            sTRIDClosingDisclosureTotalLoanCostsOfBorrowerAtClosingFees.InnerText = m_convertLos.ToMoneyString(dataLoan.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, sTRIDClosingDisclosureTotalLoanCostsOfBorrowerAtClosingFeesPred), FormatDirection.ToRep);
            sTRIDClosingDisclosureTotalLoanCostsOfBorrowerBeforeClosingFees.InnerText = m_convertLos.ToMoneyString(dataLoan.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, sTRIDClosingDisclosureTotalLoanCostsOfBorrowerBeforeClosingFeesPred), FormatDirection.ToRep);
            sTRIDClosingDisclosureTotalLoanCosts2.InnerText = dataLoan.sTRIDClosingDisclosureTotalLoanCosts_rep;

            /////////////

            sectionPredicate = p =>
                p.GetTRIDSectionBasedOnDisclosure(usingClosingDisclosure: true) == E_IntegratedDisclosureSectionT.SectionE ||
                p.GetTRIDSectionBasedOnDisclosure(usingClosingDisclosure: true) == E_IntegratedDisclosureSectionT.SectionF ||
                p.GetTRIDSectionBasedOnDisclosure(usingClosingDisclosure: true) == E_IntegratedDisclosureSectionT.SectionG ||
                p.GetTRIDSectionBasedOnDisclosure(usingClosingDisclosure: true) == E_IntegratedDisclosureSectionT.SectionH;

            Func<LoanClosingCostFeePayment, bool> sTRIDLoanEstimateTotalOtherBorrowerCostsAtClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
            Func<LoanClosingCostFeePayment, bool> sTRIDLoanEstimateTotalOtherBorrowerCostsBeforeClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
            Func<LoanClosingCostFeePayment, bool> sTRIDLoanEstimateTotalOtherBorrowerCosts = p => p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance;

            sTRIDTotalOfBorrowerAtClosingFees.InnerText = m_convertLos.ToMoneyString(dataLoan.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, sTRIDLoanEstimateTotalOtherBorrowerCostsAtClosingFeesPred), FormatDirection.ToRep);
            sTRIDTotalOfBorrowerBeforeClosingFees.InnerText = m_convertLos.ToMoneyString(dataLoan.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, sTRIDLoanEstimateTotalOtherBorrowerCostsBeforeClosingFeesPred), FormatDirection.ToRep);
            sTRIDTotalOfBorrowerFees.InnerText = m_convertLos.ToMoneyString(dataLoan.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, sTRIDLoanEstimateTotalOtherBorrowerCosts), FormatDirection.ToRep);

            ///////

            sectionPredicate = p => p != null;

            Func<LoanClosingCostFeePayment, bool> sTRIDClosingDisclosureTotalAllCostsBorrowerAtClosingPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
            Func<LoanClosingCostFeePayment, bool> sTRIDClosingDisclosureTotalAllCostsBorrowerBeforeClosingPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
            Func<LoanClosingCostFeePayment, bool> sTRIDLoanEstimateTotalAllCostsBorrowerPred = p => p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance;

            Func<LoanClosingCostFeePayment, bool> sTRIDClosingDisclosureTotalAllCostsSellerAtClosingPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Seller);
            Func<LoanClosingCostFeePayment, bool> sTRIDClosingDisclosureTotalAllCostsSellerBeforeClosingPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Seller);

            Func<LoanClosingCostFeePayment, bool> sTRIDClosingDisclosureTotalAllCostsOtherPred = p => 
                (p.PaidByT != E_ClosingCostFeePaymentPaidByT.BorrowerFinance && p.PaidByT != E_ClosingCostFeePaymentPaidByT.Borrower && p.PaidByT != E_ClosingCostFeePaymentPaidByT.Seller);

            sTRIDClosingDisclosureTotalAllCostsBorrowerAtClosing.InnerText = m_convertLos.ToMoneyString(dataLoan.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, sTRIDClosingDisclosureTotalAllCostsBorrowerAtClosingPred), FormatDirection.ToRep);
            sTRIDClosingDisclosureTotalAllCostsBorrowerBeforeClosing.InnerText = m_convertLos.ToMoneyString(dataLoan.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, sTRIDClosingDisclosureTotalAllCostsBorrowerBeforeClosingPred), FormatDirection.ToRep);

            sTRIDClosingDisclosureTotalAllCostsSellerAtClosing.InnerText = m_convertLos.ToMoneyString(dataLoan.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, sTRIDClosingDisclosureTotalAllCostsSellerAtClosingPred), FormatDirection.ToRep);
            sTRIDClosingDisclosureTotalAllCostsSellerBeforeClosing.InnerText = m_convertLos.ToMoneyString(dataLoan.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, sTRIDClosingDisclosureTotalAllCostsSellerBeforeClosingPred), FormatDirection.ToRep);

            decimal sTRIDClosingDisclosureTotalAllCostsOtherSum = dataLoan.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, sTRIDClosingDisclosureTotalAllCostsOtherPred);
            sTRIDClosingDisclosureTotalAllCostsOther.InnerText = m_convertLos.ToMoneyString(sTRIDClosingDisclosureTotalAllCostsOtherSum, FormatDirection.ToRep);
        }

        private static void BindToDataLoan(CPageData dataLoan, string viewModelJson, Guid? idToAddPayment, string editableFieldsJson)
        {
            ClosingDisclosureFeeViewModel viewModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<ClosingDisclosureFeeViewModel>(viewModelJson);

            dataLoan.sIPiaDyLckd = viewModel.sIPiaDyLckd;
            dataLoan.sIPiaDy_rep = viewModel.sIPiaDy;
            dataLoan.sIPerDayLckd = viewModel.sIPerDayLckd;
            dataLoan.sIPerDay_rep = viewModel.sIPerDay;

            if (!string.IsNullOrEmpty(editableFieldsJson))
            {
                //Bind the editable field data
                ClosingDisclosureEditableFieldsModel editableFieldsModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<ClosingDisclosureEditableFieldsModel>(editableFieldsJson);

                dataLoan.sEstCloseD_rep = editableFieldsModel.sEstCloseD;
                dataLoan.sEstCloseDLckd = editableFieldsModel.sEstCloseDLckd;
                dataLoan.sGfeIsBalloon = editableFieldsModel.sGfeIsBalloon;
                dataLoan.sSchedDueD1_rep = editableFieldsModel.sSchedDueD1;
                dataLoan.sSchedDueD1Lckd = editableFieldsModel.sSchedDueD1Lckd;

                //bind the contacts
                IPreparerFields lender = dataLoan.sTRIDLender;
                lender.AgentId = editableFieldsModel.sTRIDLenderAgentId;
                lender.CompanyName = editableFieldsModel.sTRIDLenderName;
                lender.StreetAddr = editableFieldsModel.sTRIDLenderAddr;
                lender.City = editableFieldsModel.sTRIDLenderCity;
                lender.State = editableFieldsModel.sTRIDLenderState;
                lender.Zip = editableFieldsModel.sTRIDLenderZip;
                lender.CompanyLoanOriginatorIdentifier = editableFieldsModel.sTRIDLenderNMLSNum;
                lender.LicenseNumOfCompany = editableFieldsModel.sTRIDLenderLicenseIDNum;
                lender.PreparerName = editableFieldsModel.sTRIDLenderContact;
                lender.LoanOriginatorIdentifier = editableFieldsModel.sTRIDLenderContactNMLSNum;
                lender.LicenseNumOfAgent = editableFieldsModel.sTRIDLenderContactLicenseIDNum;
                lender.EmailAddr = editableFieldsModel.sTRIDLenderContactEmail;
                lender.Phone = editableFieldsModel.sTRIDLenderContactPhoneNum;
                lender.IsLocked = editableFieldsModel.sTRIDLenderIsLocked;
                lender.AgentRoleT = editableFieldsModel.sTRIDLenderAgentRoleT;
                lender.Update();

                IPreparerFields MortgageBroker = dataLoan.sTRIDMortgageBroker;
                MortgageBroker.AgentId = editableFieldsModel.sTRIDMortgageBrokerAgentId;
                MortgageBroker.CompanyName = editableFieldsModel.sTRIDMortgageBrokerName;
                MortgageBroker.StreetAddr = editableFieldsModel.sTRIDMortgageBrokerAddr;
                MortgageBroker.City = editableFieldsModel.sTRIDMortgageBrokerCity;
                MortgageBroker.State = editableFieldsModel.sTRIDMortgageBrokerState;
                MortgageBroker.Zip = editableFieldsModel.sTRIDMortgageBrokerZip;
                MortgageBroker.CompanyLoanOriginatorIdentifier = editableFieldsModel.sTRIDMortgageBrokerNMLSNum;
                MortgageBroker.LicenseNumOfCompany = editableFieldsModel.sTRIDMortgageBrokerLicenseIDNum;
                MortgageBroker.PreparerName = editableFieldsModel.sTRIDMortgageBrokerContact;
                MortgageBroker.LoanOriginatorIdentifier = editableFieldsModel.sTRIDMortgageBrokerContactNMLSNum;
                MortgageBroker.LicenseNumOfAgent = editableFieldsModel.sTRIDMortgageBrokerContactLicenseIDNum;
                MortgageBroker.EmailAddr = editableFieldsModel.sTRIDMortgageBrokerEmail;
                MortgageBroker.Phone = editableFieldsModel.sTRIDMortgageBrokerPhoneNum;
                MortgageBroker.IsLocked = editableFieldsModel.sTRIDMortgageBrokerIsLocked;
                MortgageBroker.AgentRoleT = editableFieldsModel.sTRIDMortgageBrokerAgentRoleT;
                MortgageBroker.Update();

                IPreparerFields SettlementAgent = dataLoan.sTRIDSettlementAgent;
                SettlementAgent.AgentId = editableFieldsModel.sTRIDSettlementAgentAgentId;
                SettlementAgent.CompanyName = editableFieldsModel.sTRIDSettlementAgentName;
                SettlementAgent.StreetAddr = editableFieldsModel.sTRIDSettlementAgentAddr;
                SettlementAgent.City = editableFieldsModel.sTRIDSettlementAgentCity;
                SettlementAgent.State = editableFieldsModel.sTRIDSettlementAgentState;
                SettlementAgent.Zip = editableFieldsModel.sTRIDSettlementAgentZip;
                SettlementAgent.CompanyLoanOriginatorIdentifier = editableFieldsModel.sTRIDSettlementAgentNMLSNum;
                SettlementAgent.LicenseNumOfCompany = editableFieldsModel.sTRIDSettlementAgentLicenseIDNum;
                SettlementAgent.PreparerName = editableFieldsModel.sTRIDSettlementAgentContact;
                SettlementAgent.LoanOriginatorIdentifier = editableFieldsModel.sTRIDSettlementAgentContactNMLSNum;
                SettlementAgent.LicenseNumOfAgent = editableFieldsModel.sTRIDSettlementAgentContactLicenseIDNum;
                SettlementAgent.EmailAddr = editableFieldsModel.sTRIDSettlementAgentEmail;
                SettlementAgent.Phone = editableFieldsModel.sTRIDSettlementAgentPhoneNum;
                SettlementAgent.IsLocked = editableFieldsModel.sTRIDSettlementAgentIsLocked;
                SettlementAgent.AgentRoleT = editableFieldsModel.sTRIDSettlementAgentAgentRoleT;
                SettlementAgent.Update();

                IPreparerFields RealEstateBrokerBuyer = dataLoan.sTRIDRealEstateBrokerBuyer;
                RealEstateBrokerBuyer.AgentId = editableFieldsModel.sTRIDRealEstateBrokerBuyerAgentId;
                RealEstateBrokerBuyer.CompanyName = editableFieldsModel.sTRIDRealEstateBrokerBuyerName;
                RealEstateBrokerBuyer.StreetAddr = editableFieldsModel.sTRIDRealEstateBrokerBuyerAddr;
                RealEstateBrokerBuyer.City = editableFieldsModel.sTRIDRealEstateBrokerBuyerCity;
                RealEstateBrokerBuyer.State = editableFieldsModel.sTRIDRealEstateBrokerBuyerState;
                RealEstateBrokerBuyer.Zip = editableFieldsModel.sTRIDRealEstateBrokerBuyerZip;
                RealEstateBrokerBuyer.CompanyLoanOriginatorIdentifier = editableFieldsModel.sTRIDRealEstateBrokerBuyerNMLSNum;
                RealEstateBrokerBuyer.LicenseNumOfCompany = editableFieldsModel.sTRIDRealEstateBrokerBuyerLicenseIDNum;
                RealEstateBrokerBuyer.PreparerName = editableFieldsModel.sTRIDRealEstateBrokerBuyerContact;
                RealEstateBrokerBuyer.LoanOriginatorIdentifier = editableFieldsModel.sTRIDRealEstateBrokerBuyerContactNMLSNum;
                RealEstateBrokerBuyer.LicenseNumOfAgent = editableFieldsModel.sTRIDRealEstateBrokerBuyerContactLicenseIDNum;
                RealEstateBrokerBuyer.EmailAddr = editableFieldsModel.sTRIDRealEstateBrokerBuyerEmail;
                RealEstateBrokerBuyer.Phone = editableFieldsModel.sTRIDRealEstateBrokerBuyerPhoneNum;
                RealEstateBrokerBuyer.IsLocked = editableFieldsModel.sTRIDRealEstateBrokerBuyerIsLocked;
                RealEstateBrokerBuyer.AgentRoleT = editableFieldsModel.sTRIDRealEstateBrokerBuyerAgentRoleT;
                RealEstateBrokerBuyer.Update();

                IPreparerFields RealEstateBrokerSeller = dataLoan.sTRIDRealEstateBrokerSeller;
                RealEstateBrokerSeller.AgentId = editableFieldsModel.sTRIDRealEstateBrokerSellerAgentId;
                RealEstateBrokerSeller.CompanyName = editableFieldsModel.sTRIDRealEstateBrokerSellerName;
                RealEstateBrokerSeller.StreetAddr = editableFieldsModel.sTRIDRealEstateBrokerSellerAddr;
                RealEstateBrokerSeller.City = editableFieldsModel.sTRIDRealEstateBrokerSellerCity;
                RealEstateBrokerSeller.State = editableFieldsModel.sTRIDRealEstateBrokerSellerState;
                RealEstateBrokerSeller.Zip = editableFieldsModel.sTRIDRealEstateBrokerSellerZip;
                RealEstateBrokerSeller.CompanyLoanOriginatorIdentifier = editableFieldsModel.sTRIDRealEstateBrokerSellerNMLSNum;
                RealEstateBrokerSeller.LicenseNumOfCompany = editableFieldsModel.sTRIDRealEstateBrokerSellerLicenseIDNum;
                RealEstateBrokerSeller.PreparerName = editableFieldsModel.sTRIDRealEstateBrokerSellerContact;
                RealEstateBrokerSeller.LoanOriginatorIdentifier = editableFieldsModel.sTRIDRealEstateBrokerSellerContactNMLSNum;
                RealEstateBrokerSeller.LicenseNumOfAgent = editableFieldsModel.sTRIDRealEstateBrokerSellerContactLicenseIDNum;
                RealEstateBrokerSeller.EmailAddr = editableFieldsModel.sTRIDRealEstateBrokerSellerEmail;
                RealEstateBrokerSeller.Phone = editableFieldsModel.sTRIDRealEstateBrokerSellerPhoneNum;
                RealEstateBrokerSeller.IsLocked = editableFieldsModel.sTRIDRealEstateBrokerSellerIsLocked;
                RealEstateBrokerSeller.AgentRoleT = editableFieldsModel.sTRIDRealEstateBrokerSellerAgentRoleT;
                RealEstateBrokerSeller.Update();

                if (editableFieldsModel.sTRIDPartialPaymentAcceptanceT_AcceptApplied)
                {
                    dataLoan.sTRIDPartialPaymentAcceptanceT = E_sTRIDPartialPaymentAcceptanceT.AcceptedAndApplied;
                }
                else if (editableFieldsModel.sTRIDPartialPaymentAcceptanceT_AcceptNotApplied)
                {
                    dataLoan.sTRIDPartialPaymentAcceptanceT = E_sTRIDPartialPaymentAcceptanceT.AcceptedButNotApplied;
                }
                else if (editableFieldsModel.sTRIDPartialPaymentAcceptanceT_NotAccepted)
                {
                    dataLoan.sTRIDPartialPaymentAcceptanceT = E_sTRIDPartialPaymentAcceptanceT.NotAccepted;
                }

                if (editableFieldsModel.sNonMIHousingExpensesEscrowedReasonT_LenderRequired)
                {
                    dataLoan.sNonMIHousingExpensesEscrowedReasonT = E_sNonMIHousingExpensesEscrowedReasonT.LenderRequired;
                }
                else if (editableFieldsModel.sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested)
                {
                    dataLoan.sNonMIHousingExpensesEscrowedReasonT = E_sNonMIHousingExpensesEscrowedReasonT.BorrowerRequested;
                }

                if (editableFieldsModel.sNonMIHousingExpensesNotEscrowedReasonT_Declined)
                {
                    dataLoan.sNonMIHousingExpensesNotEscrowedReasonT = E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined;
                }
                else if (editableFieldsModel.sNonMIHousingExpensesNotEscrowedReasonT_NotOffered)
                {
                    dataLoan.sNonMIHousingExpensesNotEscrowedReasonT = E_sNonMIHousingExpensesNotEscrowedReasonT.LenderDidNotOffer;
                }

                dataLoan.sTRIDPropertyIsInNonRecourseLoanState = editableFieldsModel.sTRIDPropertyIsInNonRecourseLoanState_True;

                dataLoan.sTRIDEscrowWaiverFee_rep = editableFieldsModel.sTRIDEscrowWaiverFee;

                dataLoan.sDocMagicClosingDLckd = editableFieldsModel.sDocMagicClosingDLckd;
                dataLoan.sDocMagicClosingD_rep = editableFieldsModel.sDocMagicClosingD;
                dataLoan.sConsummationDLckd = editableFieldsModel.sConsummationDLckd;
                dataLoan.sConsummationD_rep = editableFieldsModel.sConsummationD;
                dataLoan.sDocMagicDisbursementDLckd = editableFieldsModel.sDocMagicDisbursementDLckd;
                dataLoan.sDocMagicDisbursementD_rep = editableFieldsModel.sDocMagicDisbursementD;

                dataLoan.sSettlementAgentFileNum = editableFieldsModel.sSettlementAgentFileNum;

                dataLoan.sLenderCaseNumLckd = editableFieldsModel.sLenderCaseNumLckd;
                if (dataLoan.sLenderCaseNumLckd)
                {
                    dataLoan.sLenderCaseNum = editableFieldsModel.sLenderCaseNum;
                }
            }
        }

        private static string CreateViewModel(CPageData dataLoan)
        {
            ClosingDisclosureFeeViewModel viewModel = new ClosingDisclosureFeeViewModel();
            viewModel.SectionList = dataLoan.sBorrowerAndSellerClosingCostSet.GetCompressedView(E_ClosingCostViewT.CombinedLenderTypeEstimate, optionalCollapseBorrFin:true);
            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;

            viewModel.sDocMagicClosingD = dataLoan.sDocMagicClosingD_rep;
            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;


            viewModel.sGfeIsTPOTransaction = dataLoan.sGfeIsTPOTransaction;
            viewModel.sLPurposeT = dataLoan.sLPurposeT;
            viewModel.sTridLPurposeT = dataLoan.sTridLPurposeT;
            viewModel.sLoanTransactionInvolvesSeller = dataLoan.sLoanTransactionInvolvesSeller;

            ClosingDisclosureEditableFieldsModel model = new ClosingDisclosureEditableFieldsModel();

            model.sRLckdExpiredD = dataLoan.sRLckdExpiredD_rep;

            model.sEstCloseD = dataLoan.sEstCloseD_rep;
            model.sEstCloseDLckd = dataLoan.sEstCloseDLckd;
            model.sSchedDueD1 = dataLoan.sSchedDueD1_rep;
            model.sSchedDueD1Lckd = dataLoan.sSchedDueD1Lckd;

            model.sLastDisclosedClosingDisclosureArchiveId = dataLoan.sLastDisclosedClosingDisclosureArchiveId_rep;        

            model.sSchedDueD2 = dataLoan.sSchedDueD2_rep;
            model.sSchedDueD3 = dataLoan.sSchedDueD3_rep;
            model.sSchedDueD4 = dataLoan.sSchedDueD4_rep;
            model.sSchedDueD5 = dataLoan.sSchedDueD5_rep;
            model.sSchedDueD6 = dataLoan.sSchedDueD6_rep;
            model.sSchedDueD7 = dataLoan.sSchedDueD7_rep;
            model.sSchedDueD8 = dataLoan.sSchedDueD8_rep;
            model.sSchedDueD9 = dataLoan.sSchedDueD9_rep;
            model.sSchedDueD10 = dataLoan.sSchedDueD10_rep;

            model.sAPR = dataLoan.sApr_rep;
            model.sCFPBTotalInterestPercent = dataLoan.sCFPBTotalInterestPercent_rep;

            model.sTRIDLoanEstimateTotalLoanCosts = dataLoan.sTRIDLoanEstimateTotalLoanCosts_rep;
            model.sTRIDLoanEstimateTotalOtherCosts = dataLoan.sTRIDLoanEstimateTotalOtherCosts_rep;
            model.sTRIDLoanEstimateLenderCredits = dataLoan.sTRIDLoanEstimateLenderCredits_rep;
            model.sTRIDLoanEstimateTotalClosingCosts = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
            model.sTRIDLoanEstimateBorrowerFinancedClosingCosts = dataLoan.sTRIDLoanEstimateBorrowerFinancedClosingCosts_rep;
            model.sTRIDLoanEstimateDownPaymentOrFundsFromBorrower = dataLoan.sTRIDLoanEstimateDownPaymentOrFundsFromBorrower_rep;
            model.sTRIDCashDeposit = dataLoan.sTRIDCashDeposit_rep;
            model.sTRIDLoanEstimateFundsForBorrower = dataLoan.sTRIDLoanEstimateFundsForBorrower_rep;
            model.sTRIDSellerCredits = dataLoan.sTRIDSellerCredits_rep;
            model.sTRIDAdjustmentsAndOtherCredits = dataLoan.sTRIDAdjustmentsAndOtherCredits_rep;
            model.sTRIDLoanEstimateCashToClose = dataLoan.sTRIDLoanEstimateCashToClose_rep;


            model.sDocMagicClosingD = dataLoan.sDocMagicClosingD_rep;
            model.sDocMagicClosingDLckd = dataLoan.sDocMagicClosingDLckd;
            model.sConsummationD = dataLoan.sConsummationD_rep;
            model.sConsummationDLckd = dataLoan.sConsummationDLckd;
            model.sDocMagicDisbursementD = dataLoan.sDocMagicDisbursementD_rep;
            model.sDocMagicDisbursementDLckd = dataLoan.sDocMagicDisbursementDLckd;

            model.sSettlementAgentFileNum = dataLoan.sSettlementAgentFileNum;

            IPreparerFields lender = dataLoan.sTRIDLender;
            model.sTRIDLenderAgentId = lender.AgentId;
            model.sTRIDLenderName = lender.CompanyName;
            model.sTRIDLenderAddr = lender.StreetAddr;
            model.sTRIDLenderCity = lender.City;
            model.sTRIDLenderState = lender.State;
            model.sTRIDLenderZip = lender.Zip;
            model.sTRIDLenderNMLSNum = lender.CompanyLoanOriginatorIdentifier;
            model.sTRIDLenderLicenseIDNum = lender.LicenseNumOfCompany;
            model.sTRIDLenderContact = lender.PreparerName;
            model.sTRIDLenderContactNMLSNum = lender.LoanOriginatorIdentifier;
            model.sTRIDLenderContactLicenseIDNum = lender.LicenseNumOfAgent;
            model.sTRIDLenderContactEmail = lender.EmailAddr;
            model.sTRIDLenderContactPhoneNum = lender.Phone;
            model.sTRIDLenderIsLocked = lender.IsLocked;
            model.sTRIDLenderAgentRoleT = lender.AgentRoleT;

            IPreparerFields MortgageBroker = dataLoan.sTRIDMortgageBroker;
            model.sTRIDMortgageBrokerAgentId = MortgageBroker.AgentId;
            model.sTRIDMortgageBrokerName = MortgageBroker.CompanyName;
            model.sTRIDMortgageBrokerAddr = MortgageBroker.StreetAddr;
            model.sTRIDMortgageBrokerCity = MortgageBroker.City;
            model.sTRIDMortgageBrokerState = MortgageBroker.State;
            model.sTRIDMortgageBrokerZip = MortgageBroker.Zip;
            model.sTRIDMortgageBrokerNMLSNum = MortgageBroker.CompanyLoanOriginatorIdentifier;
            model.sTRIDMortgageBrokerLicenseIDNum = MortgageBroker.LicenseNumOfCompany;
            model.sTRIDMortgageBrokerContact = MortgageBroker.PreparerName;
            model.sTRIDMortgageBrokerContactNMLSNum = MortgageBroker.LoanOriginatorIdentifier;
            model.sTRIDMortgageBrokerContactLicenseIDNum = MortgageBroker.LicenseNumOfAgent;
            model.sTRIDMortgageBrokerEmail = MortgageBroker.EmailAddr;
            model.sTRIDMortgageBrokerPhoneNum = MortgageBroker.Phone;
            model.sTRIDMortgageBrokerIsLocked = MortgageBroker.IsLocked;
            model.sTRIDMortgageBrokerAgentRoleT = MortgageBroker.AgentRoleT;

            IPreparerFields SettlementAgent = dataLoan.sTRIDSettlementAgent;
            model.sTRIDSettlementAgentAgentId = SettlementAgent.AgentId;
            model.sTRIDSettlementAgentName = SettlementAgent.CompanyName;
            model.sTRIDSettlementAgentAddr = SettlementAgent.StreetAddr;
            model.sTRIDSettlementAgentCity = SettlementAgent.City;
            model.sTRIDSettlementAgentState = SettlementAgent.State;
            model.sTRIDSettlementAgentZip = SettlementAgent.Zip;
            model.sTRIDSettlementAgentNMLSNum = SettlementAgent.CompanyLoanOriginatorIdentifier;
            model.sTRIDSettlementAgentLicenseIDNum = SettlementAgent.LicenseNumOfCompany;
            model.sTRIDSettlementAgentContact = SettlementAgent.PreparerName;
            model.sTRIDSettlementAgentContactNMLSNum = SettlementAgent.LoanOriginatorIdentifier;
            model.sTRIDSettlementAgentContactLicenseIDNum = SettlementAgent.LicenseNumOfAgent;
            model.sTRIDSettlementAgentEmail = SettlementAgent.EmailAddr;
            model.sTRIDSettlementAgentPhoneNum = SettlementAgent.Phone;
            model.sTRIDSettlementAgentIsLocked = SettlementAgent.IsLocked;
            model.sTRIDSettlementAgentAgentRoleT = SettlementAgent.AgentRoleT;

            IPreparerFields RealEstateBrokerBuyer = dataLoan.sTRIDRealEstateBrokerBuyer;
            model.sTRIDRealEstateBrokerBuyerAgentId = RealEstateBrokerBuyer.AgentId;
            model.sTRIDRealEstateBrokerBuyerName = RealEstateBrokerBuyer.CompanyName;
            model.sTRIDRealEstateBrokerBuyerAddr = RealEstateBrokerBuyer.StreetAddr;
            model.sTRIDRealEstateBrokerBuyerCity = RealEstateBrokerBuyer.City;
            model.sTRIDRealEstateBrokerBuyerState = RealEstateBrokerBuyer.State;
            model.sTRIDRealEstateBrokerBuyerZip = RealEstateBrokerBuyer.Zip;
            model.sTRIDRealEstateBrokerBuyerNMLSNum = RealEstateBrokerBuyer.CompanyLoanOriginatorIdentifier;
            model.sTRIDRealEstateBrokerBuyerLicenseIDNum = RealEstateBrokerBuyer.LicenseNumOfCompany;
            model.sTRIDRealEstateBrokerBuyerContact = RealEstateBrokerBuyer.PreparerName;
            model.sTRIDRealEstateBrokerBuyerContactNMLSNum = RealEstateBrokerBuyer.LoanOriginatorIdentifier;
            model.sTRIDRealEstateBrokerBuyerContactLicenseIDNum = RealEstateBrokerBuyer.LicenseNumOfAgent;
            model.sTRIDRealEstateBrokerBuyerEmail = RealEstateBrokerBuyer.EmailAddr;
            model.sTRIDRealEstateBrokerBuyerPhoneNum = RealEstateBrokerBuyer.Phone;
            model.sTRIDRealEstateBrokerBuyerIsLocked = RealEstateBrokerBuyer.IsLocked;
            model.sTRIDRealEstateBrokerBuyerAgentRoleT = RealEstateBrokerBuyer.AgentRoleT;

            IPreparerFields RealEstateBrokerSeller = dataLoan.sTRIDRealEstateBrokerSeller;
            model.sTRIDRealEstateBrokerSellerAgentId = RealEstateBrokerSeller.AgentId;
            model.sTRIDRealEstateBrokerSellerName = RealEstateBrokerSeller.CompanyName;
            model.sTRIDRealEstateBrokerSellerAddr = RealEstateBrokerSeller.StreetAddr;
            model.sTRIDRealEstateBrokerSellerCity = RealEstateBrokerSeller.City;
            model.sTRIDRealEstateBrokerSellerState = RealEstateBrokerSeller.State;
            model.sTRIDRealEstateBrokerSellerZip = RealEstateBrokerSeller.Zip;
            model.sTRIDRealEstateBrokerSellerNMLSNum = RealEstateBrokerSeller.CompanyLoanOriginatorIdentifier;
            model.sTRIDRealEstateBrokerSellerLicenseIDNum = RealEstateBrokerSeller.LicenseNumOfCompany;
            model.sTRIDRealEstateBrokerSellerContact = RealEstateBrokerSeller.PreparerName;
            model.sTRIDRealEstateBrokerSellerContactNMLSNum = RealEstateBrokerSeller.LoanOriginatorIdentifier;
            model.sTRIDRealEstateBrokerSellerContactLicenseIDNum = RealEstateBrokerSeller.LicenseNumOfAgent;
            model.sTRIDRealEstateBrokerSellerEmail = RealEstateBrokerSeller.EmailAddr;
            model.sTRIDRealEstateBrokerSellerPhoneNum = RealEstateBrokerSeller.Phone;
            model.sTRIDRealEstateBrokerSellerAgentRoleT = RealEstateBrokerSeller.AgentRoleT;
            model.sTRIDRealEstateBrokerSellerIsLocked = RealEstateBrokerSeller.IsLocked;

            model.sTRIDPartialPaymentAcceptanceT_AcceptApplied = dataLoan.sTRIDPartialPaymentAcceptanceT == E_sTRIDPartialPaymentAcceptanceT.AcceptedAndApplied;
            model.sTRIDPartialPaymentAcceptanceT_AcceptNotApplied = dataLoan.sTRIDPartialPaymentAcceptanceT == E_sTRIDPartialPaymentAcceptanceT.AcceptedButNotApplied;
            model.sTRIDPartialPaymentAcceptanceT_NotAccepted = dataLoan.sTRIDPartialPaymentAcceptanceT == E_sTRIDPartialPaymentAcceptanceT.NotAccepted;

            model.sNonMIHousingExpensesEscrowedReasonT_LenderRequired = dataLoan.sNonMIHousingExpensesEscrowedReasonT == E_sNonMIHousingExpensesEscrowedReasonT.LenderRequired;
            model.sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested = dataLoan.sNonMIHousingExpensesEscrowedReasonT == E_sNonMIHousingExpensesEscrowedReasonT.BorrowerRequested;

            model.sNonMIHousingExpensesNotEscrowedReasonT_Declined = dataLoan.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined;
            model.sNonMIHousingExpensesNotEscrowedReasonT_NotOffered = dataLoan.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.LenderDidNotOffer;

            model.sTRIDEscrowWaiverFee = dataLoan.sTRIDEscrowWaiverFee_rep;

            model.sTRIDPropertyIsInNonRecourseLoanState_True = dataLoan.sTRIDPropertyIsInNonRecourseLoanState;
            model.sTRIDPropertyIsInNonRecourseLoanState_False = !dataLoan.sTRIDPropertyIsInNonRecourseLoanState;

            model.sLenderCaseNumLckd = dataLoan.sLenderCaseNumLckd;
            model.sLenderCaseNum = dataLoan.sLenderCaseNum;
            
            viewModel.editableFieldsModel = model;

            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;

            viewModel.sTRIDLoanEstimateTotalLoanCosts = dataLoan.sTRIDLoanEstimateTotalLoanCosts_rep;
            viewModel.sTRIDLoanEstimateTotalClosingCosts = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
            viewModel.sTRIDLoanEstimateTotalOtherCosts = dataLoan.sTRIDLoanEstimateTotalOtherCosts_rep;
            viewModel.sTRIDLoanEstimateLenderCredits = dataLoan.sTRIDLoanEstimateLenderCredits_rep;
            viewModel.sTRIDLoanEstimateTotalAllCosts = dataLoan.sTRIDLoanEstimateTotalAllCosts_rep;

            viewModel.sLenderPaidBrokerCompF = dataLoan.sLenderPaidBrokerCompF_rep;

            return ObsoleteSerializationHelper.JsonSerialize(viewModel);
        }

        [WebMethod]
        public static string GetAvailableClosingCostFeeTypes(Guid loanId, string viewModelJson, string sectionName)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(ClosingDisclosure));
                dataLoan.InitLoad();

                BrokerDB db = PrincipalFactory.CurrentPrincipal.BrokerDB;

                ClosingDisclosureFeeViewModel viewModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<ClosingDisclosureFeeViewModel>(viewModelJson);

                FeeSetupClosingCostSet closingCostSet = db.GetUnlinkedClosingCostSet();
                FeeSetupClosingCostFeeSection section = (FeeSetupClosingCostFeeSection)closingCostSet.GetSection(E_ClosingCostViewT.LenderTypeEstimate, sectionName);

                List<BaseClosingCostFee> fees = new List<BaseClosingCostFee>();

                foreach (BorrowerClosingCostFeeSection closingSection in viewModel.SectionList)
                {
                    foreach (BorrowerClosingCostFee fee in closingSection.FilteredClosingCostFeeList)
                    {
                        fees.Add(fee.ConvertToFeeSetupFee());
                    }
                }

                section.SetExcludeFeeList(fees, dataLoan.sClosingCostFeeVersionT, o => o.CanManuallyAddToEditor == false);

                return ObsoleteSerializationHelper.JsonSerialize(section);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
            {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
            });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CalculateData(Guid loanId, string viewModelJson, string editableFieldsJson)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(ClosingDisclosure));
                dataLoan.InitLoad();

                BindToDataLoan(dataLoan, viewModelJson, null, editableFieldsJson);

                return CreateViewModel(dataLoan);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CreateAgent(Guid loanId, Guid id, bool populateFromRolodex, E_AgentRoleT agentType)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(ClosingDisclosure));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                CAgentFields agent = dataLoan.GetAgentFields(Guid.Empty);
                if (populateFromRolodex)
                {
                    agent.PopulateFromRolodex(id, BrokerUserPrincipal.CurrentPrincipal);
                }
                else
                {
                    CommonFunctions.CopyEmployeeInfoToAgent(PrincipalFactory.CurrentPrincipal.BrokerId, agent, id);
                }

                agent.AgentRoleT = agentType;
                agent.Update();

                dataLoan.Save();

                return "{\"RecordId\": \"" + agent.RecordId.ToString() + "\"}";
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string Save(Guid loanId, string viewModelJson, string editableFieldsJson)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(ClosingDisclosure));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                BindToDataLoan(dataLoan, viewModelJson, null, editableFieldsJson);

                dataLoan.Save();

                return CreateViewModel(dataLoan) ;
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string ClearAgent(Guid loanId, string viewModelJson, Guid recordId)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(ClosingDisclosure));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                // Bind unsaved data to loan
                BindToDataLoan(dataLoan, viewModelJson, null, null);

                DataSet ds = dataLoan.sAgentDataSet;
                DataTable table = ds.Tables[0];

                // Find agent to be deleted.
                DataRow toBeDeleted = null;
                foreach (DataRow row in table.Rows)
                {
                    if ((string)row["recordId"] == recordId.ToString())
                    {
                        // I could not invoke the Delete() on row object here.
                        // Doing so will modify the collection of table.Rows which cause
                        // the exception to be throw. dd 4/21/2003
                        toBeDeleted = row;
                        break;
                    }
                }

                // If agent found then delete and save. Otherwise do nothing.
                if (toBeDeleted != null)
                {
                    dataLoan.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(toBeDeleted, dataLoan.sSpState, AgentRecordChangeType.DeleteRecord));

                    // OPM 209868 - Clear beneficiary info in any closing cost fee where this agent is the beneficiary.
                    // If we have access to this page/method we can assume that the loan has been migrated.
                    dataLoan.sClosingCostSet.ClearBeneficiary(new Guid((string)toBeDeleted["recordId"]));
                    toBeDeleted.Delete();
                }

                dataLoan.sAgentDataSet = ds;
                dataLoan.Save();

                // Return deserialized data set.
                return CreateViewModel(dataLoan);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string SetArchiveStatus(Guid loanId, Guid archiveId, ClosingCostArchive.E_ClosingCostArchiveStatus status)
        {
            try
            {
                if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowManuallyArchivingGFE))
                {
                    var usrMsg = "You do not have permission to set the archive status.";
                    throw new AccessDenied(usrMsg, usrMsg);
                }

                var loan = CPageData.CreateUsingSmartDependency(
                    loanId,
                    typeof(LoanEstimate));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.UpdateArchiveStatus(archiveId, status);
                loan.Save();

                string lastDisclosedArchiveD = loan.sLastDisclosedClosingDisclosureArchiveId == Guid.Empty ?
                    "None" :
                    loan.sLastDisclosedClosingDisclosureArchive.DateArchived;

                return SerializationHelper.JsonNetAnonymousSerialize(new 
                { 
                    LastDisclosedClosingDisclosureArchiveDate = lastDisclosedArchiveD
                });
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        protected void SellerInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            Seller sellerInfo = e.Item.DataItem as Seller;

            Literal sellerName = e.Item.FindControl("SellerName") as Literal;
            sellerName.Text = sellerInfo.Name;

            Literal sellerStreetAddr = e.Item.FindControl("SellerStreetAddr") as Literal;
            sellerStreetAddr.Text = sellerInfo.Address.StreetAddress;

            Literal sellerCityStateZip = e.Item.FindControl("SellerCityStateZip") as Literal;
            sellerCityStateZip.Text = string.Format("{0}, {1} {2}", sellerInfo.Address.City, sellerInfo.Address.State, sellerInfo.Address.PostalCode);
        }

        protected void Adjustments_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            Adjustment adjustment = e.Item.DataItem as Adjustment;

            Literal adjustmentDescription = e.Item.FindControl("adjustmentDescription") as Literal;
            if (this.UsePaidToPartyForPocAdjustmentDescription(adjustment))
            {
                adjustmentDescription.Text = $"{adjustment.Description} P.O.C. {adjustment.Amount_rep} {adjustment.PaidToParty_rep}";
            }
            else if (adjustment.POC)
            {
                adjustmentDescription.Text = string.Format("{0} P.O.C. {1} {2}", adjustment.Description, adjustment.Amount_rep, adjustment.PaidFromParty_rep);
            }
            else
            {
                adjustmentDescription.Text = adjustment.Description;
            }

            Literal adjustmentAmount = e.Item.FindControl("adjustmentAmount") as Literal;
            if (adjustment.POC)
            {
                adjustmentAmount.Text = string.Empty;
            }
            else
            {
                adjustmentAmount.Text = adjustment.Amount_rep;
            }
        }

        /// <summary>
        /// Determines whether the description for the adjustment should include "POC"
        /// and the paid-to party.
        /// </summary>
        /// <param name="adjustment">
        /// The adjustment to check.
        /// </param>
        /// <returns>
        /// True if the description should include "POC" and the paid-to party,
        /// false otherwise.
        /// </returns>
        private bool UsePaidToPartyForPocAdjustmentDescription(Adjustment adjustment)
        {
            return adjustment.POC &&
                (adjustment.AdjustmentType == E_AdjustmentT.PrincipalReductionToCureToleranceViolation ||
                adjustment.AdjustmentType == E_AdjustmentT.PrincipalReductionToReduceCashToBorrower);
        }
    }
}
