///
/// Author: Matthew Flynn
/// 
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
	public partial class TXDisclosureMultipleRoles : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Texas Disclosure of Multiple Roles";
            this.PageID = "TXDisclosureMultipleRoles";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CTXDisclosureMultipleRolesPDF);
        }
		override protected void LoadData()
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TXDisclosureMultipleRoles));
			dataLoan.InitLoad();

			CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

			TXDisclosureMultipleRolesLoanOfficer.Text = agent.AgentName;

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
