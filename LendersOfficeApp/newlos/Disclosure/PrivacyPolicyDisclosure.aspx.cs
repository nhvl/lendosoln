///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
	public partial class PrivacyPolicyDisclosure : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Privacy Policy Disclosure";
            this.PageID = "PrivacyPolicyDisclosure";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CPrivacyPolicyDisclosurePDF);

			PrivacyPolicyDisclosureZip.SmartZipcode(PrivacyPolicyDisclosureCity, PrivacyPolicyDisclosureState);

            RegisterJsScript("jquery-1.6.min.js");
			RegisterJsScript("LQBPopup.js");
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		override protected void LoadData()
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(PrivacyPolicyDisclosure));
			dataLoan.InitLoad();

			IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.PrivacyPolicyDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject );
			
			PrivacyPolicyDisclosureCompanyName.Text = broker.CompanyName;
			PrivacyPolicyDisclosureStreetAddr.Text = broker.StreetAddr;
			PrivacyPolicyDisclosureCity.Text = broker.City;
			PrivacyPolicyDisclosureState.Value = broker.State;
			PrivacyPolicyDisclosureZip.Text = broker.Zip;
			PrivacyPolicyDisclosurePhoneOfCompany.Text = broker.PhoneOfCompany;
		}
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
