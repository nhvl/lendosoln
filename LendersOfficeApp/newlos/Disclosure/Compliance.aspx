﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Compliance.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.Compliance" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Compliance</title>
    <style type="text/css">
        body { background-color:gainsboro;}
    </style>
</head>
<body>
    <script type="text/javascript">
        function _init()
        {
            lockDropDown('sDisclosureRegulationTLckd', 'sDisclosureRegulationT');
            lockDropDown('sLoanRescindableTLckd', 'sLoanRescindableT');
            lockDropDown('sLoanTransactionInvolvesSellerLckd', 'sLoanTransactionInvolvesSeller');
            disableDDL(document.getElementById('sTridTargetRegulationVersionT'), !document.getElementById('sTridTargetRegulationVersionTLckd').checked);

            var isTrid = document.getElementById('sDisclosureRegulationT').value === "2";
            $('.require-borrowers-receive-cd').toggle(isTrid && ML.EnableBorrowerLevelDisclosureTracking);
            $('#tridRegulation').toggle(isTrid); // only show the Trid Versions if the disclosure type is TRID
            disableDDL(document.getElementById('sTridClosingDisclosureCalculateEscrowAccountFromT'), !isTrid);

            $('#sIsUseManualAprRow').toggle(ML.ShowIsUseManualApr);
        }

        function lockDropDown(lockboxId, dropdownId)
        {
            var checked = $('#' + lockboxId).is(':checked');
            $('#' + dropdownId).prop('disabled', !checked);
        }

        var oldSaveMe = window.saveMe;
        window.saveMe = function(bRefreshScreen)
        {
            var resultSave = oldSaveMe(bRefreshScreen);
            if (resultSave)
            {
                window.parent.treeview.location = window.parent.treeview.location;
            }
            return resultSave; 
        }

        jQuery(function ($) {
            $('.RefreshCalculation').change(function () {
                refreshCalculation();
            });

            $('#sLoanRescindableTLckd').change(function () {
                lockDropDown('sLoanRescindableTLckd', 'sLoanRescindableT');
            });

            $('#sDisclosureRegulationTLckd').change(function () {
                lockDropDown('sDisclosureRegulationTLckd', 'sDisclosureRegulationT');
            });

            $('#sLoanTransactionInvolvesSellerLckd').change(function () {
                lockDropDown('sLoanTransactionInvolvesSellerLckd', 'sLoanTransactionInvolvesSeller');
            });
        });
    </script>
    
    <form id="form1" runat="server">
    <div class="MainRightHeader" noWrap>Compliance</div>
    <table>
        <tbody>
            <tr>
                <td class="FieldLabel" nowrap>
                    Required disclosure type
                </td>
                <td>
                    <asp:DropDownList runat=server ID="sDisclosureRegulationT" class="RefreshCalculation"></asp:DropDownList>
                    <asp:CheckBox runat="server" ID="sDisclosureRegulationTLckd" class="RefreshCalculation" Text="Lock" />
                </td>
            </tr>
            <tr id="tridRegulation">
                <td class="FieldLabel">
                    TRID Target Regulation Version
                </td>
                <td>
                    <asp:DropDownList ID="sTridTargetRegulationVersionT" runat="server" class="RefreshCalculation"></asp:DropDownList>
                    <asp:CheckBox runat="server" ID="sTridTargetRegulationVersionTLckd" Text="Lock" class="RefreshCalculation"/>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" nowrap>
                    Origination fee is bona fide
                </td>
                <td>
                     <asp:RadioButtonList runat="server" ID="sLOrigFProps_BF" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="False"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" nowrap>
                    Discount points are bona fide 
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="sGfeDiscountPointFProps_BF" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                    <asp:ListItem Text="No" Value="False"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td  class="FieldLabel" nowrap>
                    General lender credit can offset APR fees 
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="sGfeLenderCreditFProps_Apr" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="False"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Flood certification determination
                </td>
                <td>
                    <asp:dropdownlist id="sFloodCertificationDeterminationT" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    APR Calculation
                </td>
                <td>
                    <asp:DropDownList ID="sAprCalculationT" runat="server" />
                </td>
            </tr>
            <tr id="sIsUseManualAprRow">
                <td class="FieldLabel">
                    Allow APR to be set manually (construction/construction-perm only)
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="sIsUseManualApr" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="False"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Rescindability - This loan is:
                </td>
                <td>
                    <asp:DropDownList ID="sLoanRescindableT" runat="server"></asp:DropDownList>
                    <asp:CheckBox runat="server" ID="sLoanRescindableTLckd" Text="Lock" class="RefreshCalculation" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Allow overriding balloon loan calculation?
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="sGfeIsBalloonLckd" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Loan Transaction Involves Seller
                </td>
                <td>
                    <asp:DropDownList ID="sLoanTransactionInvolvesSeller" runat="server"></asp:DropDownList>
                    <asp:CheckBox runat="server" ID="sLoanTransactionInvolvesSellerLckd" Text="Lock" class="RefreshCalculation"/>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Require Alternate Cash-to-Close for Non-Seller Transactions
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="sIsRequireAlternateCashToCloseForNonSellerTransactions" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                    <asp:ListItem Text="No" Value="False"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Calculate Closing Disclosure Escrow Account payments for one year from:
                </td>
                <td>
                    <asp:DropDownList ID="sTridClosingDisclosureCalculateEscrowAccountFromT" runat="server" class="RefreshCalculation"></asp:DropDownList>
                </td>
            </tr>
            <tr class="require-borrowers-receive-cd">
                <td class="FieldLabel">
                    Always require all borrowers to receive Closing Disclosure?
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="sAlwaysRequireAllBorrowersToReceiveClosingDisclosure" RepeatDirection="Horizontal" CssClass="RefreshCalculation">
                        <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="False"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </tbody>
    </table>
    </form>
</body>
</html>
