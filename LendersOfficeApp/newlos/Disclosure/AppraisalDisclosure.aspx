<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="AppraisalDisclosure.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Disclosure.AppraisalDisclosure" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>AppraisalDisclosure</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">        
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
  <script language="javascript">
<!--
      var oRolodex = null;
      function _init() {
          if (null == oRolodex)
              oRolodex = new cRolodex();
      }
//-->
  </script>
	
    <form id="AppraisalDisclosure" method="post" runat="server">
<TABLE id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TR>
    <TD noWrap class="MainRightHeader">Right To Receive Appraisal</TD></TR>
  <TR>
    <TD noWrap style="PADDING-LEFT:10px">
      <TABLE id=Table2 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <td></td>
          <td colspan=2>
			<uc:CFM id="CFM" runat="server" Type="21" CompanyNameField="AppraisalDisclosureCompanyName" StreetAddressField="AppraisalDisclosureStreetAddr" CityField="AppraisalDisclosureCity" StateField="AppraisalDisclosureState" ZipField="AppraisalDisclosureZip"></uc:CFM>
          </td>
        </TR>
        <tr>
          <TD noWrap class=FieldLabel>Contact Company&nbsp;</TD>
          
          <TD          
        noWrap><asp:TextBox id=AppraisalDisclosureCompanyName runat="server" Width="255px"></asp:TextBox></TD></TR>
        <TR>
          <TD noWrap class=FieldLabel>Address</TD>
          <TD noWrap><asp:TextBox id=AppraisalDisclosureStreetAddr runat="server" Width="255px"></asp:TextBox></TD></TR>
        <TR>
          <TD noWrap class=FieldLabel>City</TD>
          <TD noWrap><asp:TextBox id=AppraisalDisclosureCity runat="server" Width="160px"></asp:TextBox><ml:StateDropDownList id=AppraisalDisclosureState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=AppraisalDisclosureZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></TD></TR>
		</TABLE></TD></TR></TABLE>
	
     </form>
			<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>

  </body>
</html>
