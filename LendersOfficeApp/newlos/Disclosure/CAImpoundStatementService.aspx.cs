///
/// Author: Matthew Flynn
/// 
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class CAImpoundStatementServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(CAImpoundStatementServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.CACompoundStatement, E_ReturnOptionIfNotExist.CreateNew);

            broker.CompanyName = GetString("CACompoundStatementCompanyName");
            broker.StreetAddr = GetString("CACompoundStatementStreetAddr");
            broker.City = GetString("CACompoundStatementCity");
            broker.State = GetString("CACompoundStatementState");
            broker.Zip = GetString("CACompoundStatementZip");
            broker.PhoneOfCompany = GetString("CACompoundStatementPhoneOfCompany");
            broker.FaxOfCompany = GetString("CACompoundStatementFaxOfCompany");
            broker.Update();
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

	public partial class CAImpoundStatementService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new CAImpoundStatementServiceItem());
        }

	}
}
