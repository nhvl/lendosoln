///
/// Author: Matthew Flynn
/// 
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
	public partial class BorrowerAuthorizationForm : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }



        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Borrower Authorization Form";
            this.PageID = "BorrowerAuthorizationForm";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CBorrowerSignatureAuthorizationPDF);
            RegisterJsScript("jquery-1.6.min.js");
			RegisterJsScript("LQBPopup.js");
			BorrSignatureAuthorizationZip.SmartZipcode(BorrSignatureAuthorizationCity, BorrSignatureAuthorizationState);
        }

		protected override void LoadData() 
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(BorrowerAuthorizationForm));
			dataLoan.InitLoad();

			IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.BorrSignatureAuthorization, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 

			BorrSignatureAuthorizationCompanyName.Text = broker.CompanyName;
			BorrSignatureAuthorizationStreetAddr.Text = broker.StreetAddr;
			BorrSignatureAuthorizationCity.Text = broker.City;
			BorrSignatureAuthorizationState.Value = broker.State;
			BorrSignatureAuthorizationZip.Text = broker.Zip;

			IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
			GfeTilPrepareDate.Text = gfeTil.PrepareDate_rep;


		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
