﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdjustmentsAndOtherCredits.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.AdjustmentsAndOtherCredits" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="QualifyingBorrower" Src="~/newlos/QualifyingBorrower.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Adjustments and Other Credits</title>
    <style type="text/css">
        h1
        {
            margin-top: 0px;
        }
        h2
        {
            margin-top: 0px;
            margin-bottom: 0px;
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 7px;
        }
        body
        {
            background-color: gainsboro;
        }
        table th
        {
            text-align: left;
        }
        td.date-container
        {
            white-space: nowrap;
            width: 88px;
        }
        td.date-container input
        {
            position: relative;
            bottom: 2px;
        }
        td.proration-from-to
        {
            width: 55px;
        }
        input[type="text"].money
        {
            width: 90px;
            text-align: right;
        }
        select[disabled]
        {
            background-color: white !important;
        }
        .FormTableSubheader
        {
            padding-left: 7px;
        }
        .LoanFormHeader
        {
            font-weight: bold;
        }
        .combobox_img
        {
            border: none !important;
            position: relative;
            top: 4px;
            right: 4px;
        }
        .date-picker-button
        {
            position: relative;
            top: 3px;
            right: 4px;
        }
        .hidden
        {
            display: none;
        }
        .horizontal-section
        {
            width: 100%;
        }
        .LoanFormHeader
        {
            padding-left: 7px;
        }
        .proration-date-label
        {
            float: right;
            padding-right: 10px;
        }
        .proration-header
        {
            padding-left: 3px;
            padding-bottom: 2px;
        }
        .proration-header .date-picker-button
        {
            top: 5px;
        }
        .summary-table-container
        {
            vertical-align: top;
            width: 50%;
        }
        .summary-table-container input[type="text"]
        {
            width: 90px;
            text-align: right;
        }
        .summary-table-container table
        {
            width: 100%;
        }
        .summary-table-container td.FieldLabel
        {
            padding-left: 10px;
            width: 80%;
        }
        .to-from
        {
            text-align: right;
        }
        #sRefPdOffAmt1003Lckd, #sONewFinNetProceedsLckd, #sAltCostLckd, #sTRIDClosingDisclosureAltCostLckd
        {
            float:right;
        }
        input.lineLDescription
        {
            width:180px;
        }
        .include-in-1003, .POC
        {
            text-align:center;
        }
        .padDflp, .includeLeCd
        {
            text-align: center;
            padding-left: 15px;
        }
        #popupTemplate 
        {
            display: none;
        }
        #predefinedAdjustmentPopupDiv
        {
            margin: auto;
            text-align: left;
        }
        .predefined-adjustment-popup #LQBPopupElement
        {
            overflow-y: auto;
        }
        #restrictLabel
        {
            font-weight: bold;
        }
        .width260
        {
            width: 260px;
        }
        .disallowTogglingRestrictCheckbox .sIsRestrictAdjustmentsAndOtherCreditsDescriptionsRow
        {
            display: none;
        }
        .versionAtLeast24 .hideIfBelowVersion24
        {
            display: none;
        }
        .width130
        {
            width: 130px;
        }
        /* .dflp class definition will be provided by code behind if needed. */
    </style>
    <script id="DatePicker" type="text/ractive">
        <input type="text" class="mask date" readonly="{{readonly}}" preset="date" id="{{id}}" 
            intro="initMask" on-blur="updatePicker" value="{{value}}" />
        <a on-click="displayCalendar" href="#" on-click="showCalendar:{{id}}" class="date-picker-button">
            <img title="Open calendar" src="{{VRoot}}/images/pdate.gif" border="0">
        </a>
    </script>
    
    <script id="AdjustmentDescription" type="text/html">
        <input type="text" id="{{id}}" value="{{Description}}" intro="initInput" maxlength="60" />
        <img class="combobox_img" src= "../../images/dropdown_arrow.gif" />
    </script>
    
    <script id="AdjustmentPartySelect" type="text/html">
        <select value="{{value}}" disabled="{{disabled}}" on-change="recalc">
            <option value="0"></option>
            <option value="1">Borrower</option>
            <option value="2">Seller</option>
            <option value="3">Lender</option>
            <option value="4">Employer</option>
            <option value="5">Parent</option>
            <option value="6">Relative</option>
            <option value="11">Friend</option>
            <option value="7">Federal Agency</option>
            <option value="8">Builder or Developer</option>
            <option value="9">Real Estate Agent</option>
            <option value="10">Other</option>
        </select>
    </script>

    <script id="PageTemplate" type="text/html">
        {{>AdjustmentSection}}
        {{>ProrationSection}}
        {{>DetailsOfTransactionSection}}
        {{>SummariesOfTransactionsSection}}
    </script>
    
    <script id="AdjustmentSection" type="text/html">
        <h2 class="FormTableHeader">Adjustments and other credits</h2>
        <table>
            <thead>
                <tr>
                    <th>Type</th>
                    <th class="hideIfBelowVersion24">Description</th>
                    <th class="width130">From</th>
                    <th class="width130">To</th>
                    <th>Amount</th>
                    <th></th>
                    <th>Borrower amount</th>
                    <th></th>
                    <th>Seller amount</th>
                    <th style="padding: 0px 15px;">POC</th>
                    <th>Include in 1003 Details</th>
                    <th class="padDflp dflp">DFLP</th>
                    <th class="includeLeCd">Include in the LE/CD for this Lien</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {{#AdjustmentList.SellerCredit}}
                    <adjustmentRow
                        isSystemAdjustment="true"
                        disablePartySelection="true"
                        AdjustmentTypeString="SellerCredit"
                        Description="{{Description}}"
                        PaidFromParty="{{PaidFromParty}}"
                        PaidToParty="{{PaidToParty}}"
                        Amount="{{Amount}}"
                        BorrowerAmount="{{BorrowerAmount}}"
                        SellerAmount="{{SellerAmount}}" 
                        IsPopulateToLineLOn1003="{{IsPopulateToLineLOn1003}}"
                        POC="{{POC}}" 
                        DFLP="{{DFLP}}" 
                        IncludeAdjustmentInLeCdForThisLien="{{IncludeAdjustmentInLeCdForThisLien}}" />
                {{/AdjustmentList.SellerCredit}}

                {{#AdjustmentList.CustomItems:i}}
                    <adjustmentRow
                        isSystemAdjustment="false"
                        disablePartySelection="{{AdjustmentType == 32 || AdjustmentType == 33}}"
                        index="{{i}}"
                        AdjustmentType="{{AdjustmentType}}"
                        AdjustmentTypeString="{{AdjustmentTypeString}}"
                        Description="{{Description}}"
                        PaidFromParty="{{PaidFromParty}}"
                        PaidToParty="{{PaidToParty}}"
                        Amount="{{Amount}}"
                        BorrowerAmount="{{BorrowerAmount}}"
                        SellerAmount="{{SellerAmount}}" 
                        IsPopulateToLineLOn1003="{{IsPopulateToLineLOn1003}}"
                        POC="{{POC}}"
                        DFLP="{{DFLP}}"
                        IncludeAdjustmentInLeCdForThisLien="{{IncludeAdjustmentInLeCdForThisLien}}" />
                {{/AdjustmentList.CustomItems}}

                {{#AssetAdjustments:i}}
                    <adjustmentRow
                        disabled="true",
                        isSystemAdjustment="true"
                        disablePartySelection="true"
                        index="{{i}}"
                        AdjustmentType="{{AdjustmentType}}"
                        AdjustmentTypeString="{{AdjustmentTypeString}}"
                        Description="{{Description}}"
                        PaidFromParty="{{PaidFromParty}}"
                        PaidToParty="{{PaidToParty}}"
                        Amount="{{Amount}}"
                        BorrowerAmount="{{BorrowerAmount}}"
                        SellerAmount="{{SellerAmount}}" 
                        IsPopulateToLineLOn1003="{{IsPopulateToLineLOn1003}}"
                        POC="{{POC}}"
                        DFLP="{{DFLP}}"
                        IncludeAdjustmentInLeCdForThisLien="{{IncludeAdjustmentInLeCdForThisLien}}" />
                {{/AssetAdjustments}}
            </tbody>
            <tfoot>
                <tr>
                    <td><input type="button" value="Add" on-click="addAdjustment"/></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="padDflp dflp"></td>
                    <td class="includeLeCd"></td>
                    <td></td>
                </tr>
                <tr style="display:{{(DisableUserMigrationToAdjustmentsMode || sLoads1003LineLFromAdjustments) ? 'none' :'table-row'}}">
                    <td colspan="3">
                        <input type="button" value="Pull 1003 Details into Adjustments" on-click="Migrate1003ToAdjustments"/>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="padDflp dflp"></td>
                    <td class="includeLeCd"></td>
                    <td></td>
                </tr>
                <tr class="hideIfBelowVersion24 sIsRestrictAdjustmentsAndOtherCreditsDescriptionsRow">
                    <td colspan="3" id="restrictLabel">
                        <input id="sIsRestrictAdjustmentsAndOtherCreditsDescriptions" checked="{{sIsRestrictAdjustmentsAndOtherCreditsDescriptions}}" type="checkbox" on-click="restrictAdjustments"/>
                        Restrict adjustments and other credits descriptions
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="padDflp dflp"></td>
                    <td class="includeLeCd"></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>

        <div id="popupTemplate">
            <predefinedAdjustmentsTemplate>
            {{>predefinedAdjustmentsTemplate}}
        </div>
    </script>
    
    <script id="AdjustmentRow" type="text/html">
        <tr>
            <td class="hideIfBelowVersion24">
                <input type="text" value="{{PredefinedAdjustmentTypeMap[AdjustmentTypeString].Description}}" class="width260" readonly/>
            </td>
            <td class="FieldLabel">
                {{#if isSystemAdjustment}}
                    {{Description}}
                {{else}}
                    {{#if !isSystemAdjustment && Version < 24}}
                        <adjustmentDescription id="adjustmentDesc{{index}}" value="{{Description}}" class="width260" index="{{index}}" />
                    {{else}}
                        {{#if !sIsRestrictAdjustmentsAndOtherCreditsDescriptions}}
                            <input type="text" id="adjustmentDesc{{index}}" value="{{Description}}" class="width260" />
                        {{else}}
                            <input type="text" id="adjustmentDesc{{index}}" value="{{Description}}" class="width260" readonly/>
                        {{/if}}
                    {{/if}}
                {{/if}}                
            </td>
            <td>
                <adjustmentPartySelect value="{{PaidFromParty}}" disabled="{{isSystemAdjustment || disablePartySelection || disabled}}" />
            </td>
            <td>
                <adjustmentPartySelect value="{{PaidToParty}}" disabled="{{isSystemAdjustment || disablePartySelection || disabled}}" />
            </td>
            <td>
                <input type="text" readonly="{{disabled}}" value="{{Amount}}" on-change="recalc" class="money" intro="initInput" />
            </td>
            <td class="to-from">
                {{#if IsPaidToBorrower}}
                    <span>to</span>
                {{/if}}
                {{#if IsPaidFromBorrower}}
                    <span>from</span>
                {{/if}}
            </td>
            <td class="borr-seller-amount">
                <input type="text" value="{{BorrowerAmount}}" readonly="true" class="money" />
            </td>
            <td class="to-from">
                {{#if IsPaidToSeller}}
                    <span>to</span>
                {{/if}}
                {{#if IsPaidFromSeller}}
                    <span>from</span>
                {{/if}}
            </td>
            <td class="borr-seller-amount">
                <input type="text" value="{{SellerAmount}}" readonly="true" class="money" />
            </td>
            <td class="POC">
                <input disabled="{{disabled}}" type="checkbox" checked="{{POC}}" on-click="pocChecked" />
            </td>
            <td class="include-in-1003">
                <input type="checkbox" checked="{{IsPopulateToLineLOn1003}}" disabled="{{disabled || POC == true || false == sLoads1003LineLFromAdjustments || (PaidFromParty != 1 && PaidToParty != 1)}}"
                on-click='include1003Checked:{{i}},{{isSystemAdjustment}}'/> 
            </td>
            <td class="padDflp dflp">
                <input type="checkbox" checked="{{DFLP}}" disabled="{{PaidToParty != 3 || disabled}}" on-click="dflpChecked" />
            </td>
            <td class="includeLeCd">
                <input type="checkbox" checked="{{IncludeAdjustmentInLeCdForThisLien}}" disabled="{{IncludeInLeCdForThisLien == false || disabled}}" on-click="IncludeAdjustmentInLeCdForThisLienClick"/>
            </td>
            <td>
                {{#unless isSystemAdjustment}}
                    <input type="button" value=" - " on-click="removeAdjustment:{{i}}" />
                {{/unless}}
            </td>
        </tr>
    </script>
    
    <script id="ProrationSection" type="text/html">
        <h2 class="FormTableHeader">Proration</h2>
        <div>
            <div class="proration-header">
                <label class="FieldLabel">Property Transfer Date</label>
                <input type="checkbox" checked={{sPropertyTransferDLckd}} id="sPropertyTransferDLckd" on-click="sPropertyTransferDLckdChanged"/>
                <datePicker id="sPropertyTransferD" value={{sPropertyTransferD}} readonly="{{sPropertyTransferDLckd == false}}"></datePicker>
                <input intro="initInput" type="checkbox" checked="{{ProrationList.IsPopulateToLineLOn1003}}" disabled="{{false == sLoads1003LineLFromAdjustments}}" on-click="recalc" id="sIsIncludeProrationsIn1003Details" />
                <label class="FieldLabel" for="sIsIncludeProrationsIn1003Details">Include Prorations in 1003 Details</label>
                <input intro="initInput" type="checkbox" checked="{{ProrationList.IncludeProrationsInLeCdForThisLien}}" disabled="{{IncludeInLeCdForThisLien == false}}" id="IncludeProrationsInLeCdForThisLien" class="includeProrationInLeCdDiv" on-click="IncludeProrationsInLeCdForThisLienClick"/>
                <label class="FieldLabel includeProrationInLeCdDiv" for="IncludeProrationsInLeCdForThisLien">Include in the LE/CD for this Lien</label>
            </div>
        </div>
        {{>ProrationTable}}
    </script>

    <script id="ProrationTable" type="text/html">
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th>Paid through</th>
                    <th>Paid from</th>
                    <th>Paid to</th>
                    <th>Amount</th>
                    <th>From</th>
                    <th>To</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <prorationRow 
                    RowId="CityTownTaxes"
                    IsSystemProration="{{ProrationList.CityTownTaxes.IsSystemProration}}" 
                    Description="{{ProrationList.CityTownTaxes.Description}}" 
                    PaidThroughDate="{{ProrationList.CityTownTaxes.PaidThroughDate}}" 
                    PaidFromDate="{{ProrationList.CityTownTaxes.PaidFromDate}}" 
                    PaidToDate="{{ProrationList.CityTownTaxes.PaidToDate}}" 
                    Amount="{{ProrationList.CityTownTaxes.Amount}}" 
                    PaidFromParty="{{ProrationList.CityTownTaxes.PaidFromParty}}" 
                    PaidToParty="{{ProrationList.CityTownTaxes.PaidToParty}}"
                    IsPaidFromBorrowerToSeller="{{ProrationList.CityTownTaxes.IsPaidFromBorrowerToSeller}}" 
                    IsPaidFromSellerToBorrower="{{ProrationList.CityTownTaxes.IsPaidFromSellerToBorrower}}" />
                <prorationRow 
                    RowId="CountyTaxes"
                    IsSystemProration="{{ProrationList.CountyTaxes.IsSystemProration}}" 
                    Description="{{ProrationList.CountyTaxes.Description}}" 
                    PaidThroughDate="{{ProrationList.CountyTaxes.PaidThroughDate}}" 
                    PaidFromDate="{{ProrationList.CountyTaxes.PaidFromDate}}" 
                    PaidToDate="{{ProrationList.CountyTaxes.PaidToDate}}" 
                    Amount="{{ProrationList.CountyTaxes.Amount}}" 
                    PaidFromParty="{{ProrationList.CountyTaxes.PaidFromParty}}" 
                    PaidToParty="{{ProrationList.CountyTaxes.PaidToParty}}"
                    IsPaidFromBorrowerToSeller="{{ProrationList.CountyTaxes.IsPaidFromBorrowerToSeller}}" 
                    IsPaidFromSellerToBorrower="{{ProrationList.CountyTaxes.IsPaidFromSellerToBorrower}}" />
                <prorationRow 
                    RowId="Assessments"
                    IsSystemProration="{{ProrationList.Assessments.IsSystemProration}}" 
                    Description="{{ProrationList.Assessments.Description}}" 
                    PaidThroughDate="{{ProrationList.Assessments.PaidThroughDate}}" 
                    PaidFromDate="{{ProrationList.Assessments.PaidFromDate}}" 
                    PaidToDate="{{ProrationList.Assessments.PaidToDate}}" 
                    Amount="{{ProrationList.Assessments.Amount}}" 
                    PaidFromParty="{{ProrationList.Assessments.PaidFromParty}}" 
                    PaidToParty="{{ProrationList.Assessments.PaidToParty}}" 
                    IsPaidFromBorrowerToSeller="{{ProrationList.Assessments.IsPaidFromBorrowerToSeller}}" 
                    IsPaidFromSellerToBorrower="{{ProrationList.Assessments.IsPaidFromSellerToBorrower}}" />
                {{#ProrationList.CustomItems:i}}
                    <prorationRow 
                        index="{{i}}"
                        RowId="CustomProration{{i}}"
                        IsSystemProration="{{IsSystemProration}}" 
                        Description="{{Description}}" 
                        PaidThroughDate="{{PaidThroughDate}}" 
                        PaidFromDate="{{PaidFromDate}}" 
                        PaidToDate="{{PaidToDate}}" 
                        Amount="{{Amount}}" 
                        PaidFromParty="{{PaidFromParty}}" 
                        PaidToParty="{{PaidToParty}}" 
                        IsPaidFromBorrowerToSeller="{{IsPaidFromBorrowerToSeller}}" 
                        IsPaidFromSellerToBorrower="{{IsPaidFromSellerToBorrower}}" />
                {{/ProrationList.CustomItems}}
            </tbody>
            <tfoot>
                <tr>
                    <td><input type="button" value="Add" on-click="addProration"/></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </script>

    <script id="ProrationRow" type="text/html">
        <tr>
            <td class="FieldLabel">
                {{#if IsSystemProration}}
                    {{Description}}
                {{else}}
                    <input type="text" value="{{Description}}" intro="initInput" />
                {{/if}}
            </td>
            <td class="date-container">
                <datePicker id="{{RowId}}PaidThrough" value="{{PaidThroughDate}}" />
            </td>
            <td class="date-container">
                <datePicker id="{{RowId}}PaidFrom" value="{{PaidFromDate}}" readonly="true" />
            </td>
            <td class="date-container">
                <datePicker id="{{RowId}}PaidTo" value="{{PaidToDate}}" readonly="true" />
            </td>
            <td>
                <input type="text" on-change="recalc" value="{{Amount}}" class="money" on-change="recalc" intro="initInput" />
            </td>
            <td class="proration-from-to">
                {{#if IsPaidFromBorrowerToSeller}}
                    Borrower
                {{else}}
                    {{#if IsPaidFromSellerToBorrower}}
                        Seller
                    {{else}}
                    {{/if}}
                {{/if}}
            </td>
            <td class="proration-from-to">
                {{#if IsPaidFromBorrowerToSeller}}
                    Seller
                {{else}}
                    {{#if IsPaidFromSellerToBorrower}}
                        Borrower
                    {{else}}
                    {{/if}}
                {{/if}}
            </td>
            <td>
                {{#unless IsSystemProration}}
                    <input type="button" value=" - " on-click="removeProration" />            
                {{/unless}}
            </td>
        </tr>
    </script>
    
    <script id="DetailsOfTransactionSection" type="text/html">
        <div id="DetailsOfTransactionContainer">        
            <h2 class="FormTableHeader">Details of Transaction</h2>
            <div>
                <div class="dot-header">
                    <table style="border-color:White" id="Table1" cellspacing="1" cellpadding="1"  border="0">
                        <tr>
                            <td nowrap="nowrap">
                                <table id="Table4" cellspacing="1" cellpadding="1" border="0">
                                  <tr>
                                    <td></td>
                                    <td colspan="2" class="align-center">Locked</td>
                                    <td ></td>
                                    <td ></td>
                                    <td colspan="2">Locked</td>
                                  </tr>
                                  <tr>
                                    <td>a. Purchase price</td>
                                    <td style="width: 11px"></td>
                                    <td style="width: 79px"><input type="text" name="sPurchasePrice1003" class="money" readonly="readonly" value="{{sPurchasePrice1003}}"/></td>
                                    <td></td>
                                    <td>j. <a tabindex="-1" href="javascript:linkMe(gVirtualRoot + '/newlos/LoanInfo.aspx?pg=2');" >Subordinate financing</a></td>
                                    <td style="width: 6px"></td>
                                    <td><input type="text" id="sONewFinBal" class="money" readonly="readonly" value="{{sONewFinBal}}" /></td>
                                  </tr>
                                  <tr>
                                    <td>b. Alterations, improvements, repairs</td>
                                    <td style="width: 11px">
                                        <input type="checkbox" name="sAltCostLckd" disabled="disabled" checked="{{sAltCostLckd}}" />
                                    </td>
                                    <td style="width: 79px"><input type="text" name="sAltCost" class="money" value="{{sAltCost}}" readonly="readonly" /></td>
                                    <td></td>
                                    <td>k. Borrower's closing costs paid by Seller</td>
                                    <td style="width: 6px">
                                      <input type="checkbox" id="sTotCcPbsLocked" value="{{sTotCcPbsLocked}}" disabled="disabled"  /></td>
                                    <td><input type="text" id="sTotCcPbs"  class="money" readonly="readonly" value="{{sTotCcPbs}}"/></td>
                                  </tr>
                                  <tr>
                                    <td>c. Land (if acquired separately)</td>
                                    <td style="width: 11px"></td>
                                    <td style="width: 79px"><input type="text" id="sLandIfAcquiredSeparately1003" readonly="readonly" class="money" value="{{sLandIfAcquiredSeparately1003}}"  /></td>
                                    <td style="height: 23px"></td>
                                    <td>l.
                                      <input type="text" class="lineLDescription" id="sOCredit1Desc" readonly="readonly" value="{{sOCredit1Desc}}"  /></td>
                                    <td>
                                      <input type="checkbox" id="sOCredit1Lckd" checked="{{sOCredit1Lckd}}" disabled="disabled"  /></td>
                                    <td style="height: 23px"><input type="text" id="sOCredit1Amt" readonly="readonly" value="{{sOCredit1Amt}}"   class="money"  /></td>
                                  </tr>
                                  <tr>
                                    <td>d. Refi (incl. debts to be paid off)&nbsp;</td>
                                    <td style="width: 11px" class="align-right">
                                      <input type="checkbox" name="sRefPdOffAmt1003Lckd" disabled="disabled"  checked="{{sRefPdOffAmt1003Lckd}}"   /></td>
                                    <td style="width: 79px"><input type="text" name="sRefPdOffAmt1003" readonly="readonly" value="{{sRefPdOffAmt1003}}"   class="money"  /></td>
                                    <td></td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="text" class="lineLDescription" id="sOCredit2Desc" readonly="readonly" value="{{sOCredit2Desc}}"   />
                                    </td>
                                    <td>
                                      </td>
                                    <td><input type="text" id="sOCredit2Amt" readonly="readonly" value="{{sOCredit2Amt}}"   class="money"  /></td>
                                  </tr>
                                  <tr>
                                    <td>e. Estimated prepaid items&nbsp;</td>
                                    <td style="width: 11px" class="align-right">
                                      <input type="checkbox" id="sTotEstPp1003Lckd" disabled="disabled" checked="{{sTotEstPp1003Lckd}}"   /></td>
                                    <td style="width: 79px"><input type="text" id="sTotEstPp1003" readonly="readonly" value="{{sTotEstPp1003}}"   class="money"  /></td>
                                    <td></td>
                                    <td colspan="2">&nbsp;&nbsp;&nbsp;<input class="lineLDescription" id="sOCredit3Desc" readonly="readonly" value="{{sOCredit3Desc}}"   />
                                    </td>
                                    <td><input type="text" id="sOCredit3Amt" readonly="readonly" value="{{sOCredit3Amt}}"   class="money"   /></td>
                                  </tr>
                                  <tr>
                                    <td class="little-indent">
                                        {{#if sIsIncludeProrationsInTotPp && ProrationList.IsPopulateToLineLOn1003 }}
                                        Prepaids and reserves
                                        {{/if}}
                                    </td>
                                    <td>
                                        {{#if sIsIncludeProrationsInTotPp && ProrationList.IsPopulateToLineLOn1003 }}
                                        <input type="text" id="sTotEstPp" readonly="readonly" value="{{sTotEstPp}}" class="money" />
                                        {{/if}}
                                    </td>
                                    <td style="width: 79px"></td>
                                    <td></td>
                                    <td colspan="2">&nbsp;&nbsp;&nbsp;<input type="text" class="lineLDescription" id="sOCredit4Desc" readonly="readonly" value="{{sOCredit4Desc}}"   />
                                    </td>
                                    <td><input type="text" id="sOCredit4Amt" readonly="readonly" value="{{sOCredit4Amt}}"   class="money"   /></td>
                                  </tr>
                                  <tr>
                                    <td class="little-indent">
                                        {{#if sIsIncludeProrationsInTotPp && ProrationList.IsPopulateToLineLOn1003 }}
                                        Prorations paid by borrower
                                        {{/if}}
                                    </td>
                                    <td>
                                        {{#if sIsIncludeProrationsInTotPp && ProrationList.IsPopulateToLineLOn1003 }}
                                        <input type="text" id="sTotalBorrowerPaidProrations" readonly="readonly" value="{{sTotalBorrowerPaidProrations}}" class="money" />
                                        {{/if}}
                                    </td>
                                    <td style="width: 79px"></td>
                                    <td></td>
                                    <td colspan="2">&nbsp;&nbsp;&nbsp;Lender credit
                                    </td>
                                    <td><input type="text" id="sOCredit5Amt" readonly="readonly" value="{{sOCredit5Amt}}"   class="money"  readonly="readonly" /></td>
                                  </tr>              
                                  {{#if !sIsIncludeONewFinCcInTotEstCc}}
                                  <tr>
                                    <td></td>
                                    <td style="width: 11px"></td>
                                    <td style="width: 79px"></td>
                                    <td></td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Other financing closing costs</td>
                                    <td align="right">-&nbsp;</td>
                                    <td><input type="text" id="sONewFinCc" readonly="readonly" value="{{sONewFinCc}}"   class="money"  /></td>
                                  </tr>
                                  {{/if}}
                                  <tr>
                                    <td>f. Estimated closing costs</td>
                                    <td style="width: 11px" class="align-right">
                                      <input type="checkbox" id="sTotEstCc1003Lckd" disabled="disabled" checked="{{sTotEstCc1003Lckd}}"   /></td>
                                    <td style="width: 79px"><input type="text" id="sTotEstCcNoDiscnt1003" readonly="readonly" value="{{sTotEstCcNoDiscnt1003}}"   class="money"  /></td>
                                    <td colspan="2" {{#if sIsIncludeONewFinCcInTotEstCc }} rowspan="3" class="align-bottom" {{/if}}>m. Loan amount (exclude PMI, MIP, FF financed)</td>
                                    <td style="width: 6px" {{#if sIsIncludeONewFinCcInTotEstCc }} rowspan="3" class="align-bottom" {{/if}}>
                                      <input type="checkbox" id="sLAmtLckd" disabled="disabled" checked="{{sLAmtLckd}}"   /></td>
                                    <td {{#if sIsIncludeONewFinCcInTotEstCc }} rowspan="3" class="align-bottom" {{/if}}><input type="text" id="sLAmtCalc" readonly="readonly" value="{{sLAmtCalc}}"  readonly="readonly"  class="money" /></td>
                                  </tr>
                                  {{#if sIsIncludeONewFinCcInTotEstCc }}
                                  <tr>
                                    <td class="little-indent">This loan closing costs</td>
                                    <td>
                                      <input type="text" id="sTotEstCcNoDiscnt" readonly="readonly" value="{{sTotEstCcNoDiscnt}}" class="money" /></td>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                  </tr>
                                  <tr>
                                    <td class="little-indent">Other financing closing costs</td>
                                    <td>
                                      <input type="text" id="sONewFinCc2" data-field-id="sONewFinCc" readonly="readonly" value="{{sONewFinCc}}" class="money" /></td>
                                    <td></td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td></td>
                                  </tr>
                                  {{/if}}
                                  <tr>
                                    <td>g. PMI,&nbsp;MIP, Funding Fee</td>
                                    <td style="width: 11px" class="align-right">
                                      <input type="checkbox" id="sFfUfmip1003Lckd" disabled="disabled" checked="{{sFfUfmip1003Lckd}}"   /></td>
                                    <td style="width: 79px"><input type="text" id="sFfUfmip1003" readonly="readonly" value="{{sFfUfmip1003}}"   class="money"  /></td>
                                    <td></td>
                                    <td>n. PMI, MIP, Funding Fee financed</td>
                                    <td style="width: 6px"></td>
                                    <td><input type="text" id="sFfUfmipFinanced" readonly="readonly" value="{{sFfUfmipFinanced}}"  readonly="readonly"  class="money" /></td>
                                  </tr>
                                  <tr>
                                    <td>h. Discount (if Borrower will pay)</td>
                                    <td style="width: 11px" class="align-right">
                                      <input type="checkbox" id="sLDiscnt1003Lckd" disabled="disabled" checked="{{sLDiscnt1003Lckd}}"   /></td>
                                    <td style="width: 79px"><input type="text" id="sLDiscnt1003" readonly="readonly" value="{{sLDiscnt1003}}"   class="money"  /></td>
                                    <td></td>
                                    <td>o. Loan amount (add m &amp; n)</td>
                                    <td style="width: 6px"></td>
                                    <td><input type="text" name="sFinalLAmt" readonly="readonly" value="{{sFinalLAmt}}"  readonly="readonly"  class="money" /></td>
                                  </tr>
                                  <tr>
                                    <td class="FieldLabel">i. Total costs (add items a to h)</td>
                                    <td style="width: 11px"></td>
                                    <td style="width: 79px"><input type="text" id="sTotTransC" readonly="readonly" value="{{sTotTransC}}"  readonly="readonly"  class="money" /></td>
                                    <td></td>
                                    <td>p. Cash from / to Borr (subtract j, k, l &amp; o from i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td style="width: 6px">
                                      <input type="checkbox" id="sTransNetCashLckd" disabled="disabled" checked="{{sTransNetCashLckd}}"   /></td>
                                    <td><input type="text" id="sTransNetCash" readonly="readonly" value="{{sTransNetCash}}"   class="money" /></td>
                                  </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
        </div>

    </script>
    
    <script id="SummariesOfTransactionsSection" type="text/html">
        <h2 class="FormTableHeader">Summaries of Transactions</h2>
        <table class="horizontal-section">
            <tr>
                <td class="summary-table-container">
                    {{>SectionK}}
                </td>
                <td class="summary-table-container">
                    {{>SectionM}}
                </td>
            </tr>
        </table>
        {{#if ProrationList.IncludeProrationsInLeCdForThisLien}}
            <table class="horizontal-section">
                <tr>
                    <td class="summary-table-container">
                        {{>AdjustmentsForItemsPaidBySellerInAdvance}}
                    </td>
                    <td class="summary-table-container">
                        {{>AdjustmentsForItemsPaidBySellerInAdvance}}
                    </td>
                </tr>
            </table>
        {{/if}}
        <table class="horizontal-section">
            <tr>
                <td class="summary-table-container">
                    {{>SectionL}}
                </td>
                <td class="summary-table-container">
                    {{>SectionN}}
                </td>
            </tr>
        </table>
        {{#if ProrationList.IncludeProrationsInLeCdForThisLien}}
            <table class="horizontal-section">
                <tr>
                    <td class="summary-table-container">
                        {{>AdjustmentsForItemsUnpaidBySellerInAdvance}}
                    </td>
                    <td class="summary-table-container">
                        {{>AdjustmentsForItemsUnpaidBySellerInAdvance}}
                    </td>
                </tr>
            </table>
        {{/if}}
    </script>
    
    <script id="SectionK" type="text/html">
        <table>
            <tr>
                <td colspan="2" class="LoanFormHeader">K. Due from Borrower at Closing</td>
            </tr>
            {{#if !ExcludeSalesPriceAndFirstLienLoanAmountCredit}}
            <tr>
                <td class="FieldLabel">Sale Price of Property</td>
                <td><input type="text" id="sPurchasePrice1003" value="{{sPurchasePrice1003}}" readonly="true" /></td>
            </tr>
            {{/if}}
            <tr>
                <td class="FieldLabel">Sale Price of Any Personal Property Included in Sale</td>
                <td><input type="text" id="sGrossDueFromBorrPersonalProperty" value="{{sGrossDueFromBorrPersonalProperty}}" on-blur="formatMoney" intro="initInput" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Closing Costs Paid At Closing (J)</td>
                <td><input type="text" id="sTRIDClosingDisclosureBorrowerCostsPaidAtClosing" value="{{sTRIDClosingDisclosureBorrowerCostsPaidAtClosing}}" readonly="true" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Alterations, improvements, repairs {{#if IncludeAlterationsImprovementsRepairsBreakdown}}(less costs also included in J: {{sRenoFeesInClosingCosts}}){{/if}}
                    <input type="checkbox" id="sTRIDClosingDisclosureAltCostLckd" checked="{{sTRIDClosingDisclosureAltCostLckd}}" on-click="sTRIDClosingDisclosureAltCostLckdChanged" />
                </td>
                <td>
                    <input type="text" id="sTRIDClosingDisclosureAltCost" value="{{sTRIDClosingDisclosureAltCost}}" on-blur="formatMoney" intro="initInput" />
                </td>
            </tr>
            <tr>
            <tr>
                <td class="FieldLabel">Land (if acquired separately)</td>
                <td><input type="text" id="sLandIfAcquiredSeparately1003" value="{{sLandIfAcquiredSeparately1003}}" readonly="true" /></td>
            </tr>
            {{#if !ExcludeDebtsToBePaidOff}}
            <tr>
                <td class="FieldLabel">
                    Total Debts to be Paid Off
                    <input type="checkbox" id="sRefPdOffAmt1003Lckd" checked="{{sRefPdOffAmt1003Lckd}}" on-click="sRefPdOffAmt1003LckdChanged"/>
                </td>
                <td><input type="text" id="sRefPdOffAmt1003" value="{{sRefPdOffAmt1003}}"  on-blur="formatMoney" intro="initInput"/></td>
            </tr>
            {{/if}}
            {{#AdjustmentListWithAssets:i}}
                {{#if IsPaidFromBorrower && IncludeAdjustmentInLeCdForThisLien}}
                    <tr>
                        {{#if POC == true}}
                            <td class="FieldLabel">
                                {{Description}} P.O.C. {{Amount}} {{(AdjustmentType == 32 || AdjustmentType == 33) ? PaidToParty_rep : PaidFromParty_rep}}
                            </td>
                            <td>
                                <input type="text" value="" readonly="true" />
                            </td>
                        {{else}}
                            <td class="FieldLabel">{{Description}}</td>
                            <td><input type="text" value="{{Amount}}" readonly="true" /></td>
                        {{/if}}
                    </tr>
                {{/if}}
            {{/AdjustmentListWithAssets}}
        </table>
    </script>
    
    <script id="SectionM" type="text/html">
        <table>
            <tr>
                <td colspan="2" class="LoanFormHeader">M. Due to Seller at Closing</td>
            </tr>
            {{#if !ExcludeSalesPriceAndFirstLienLoanAmountCredit}}
            <tr>
                <td class="FieldLabel">Sale Price of Property</td>
                <td><input type="text" id="sPurchasePrice1003_2" value="{{sPurchasePrice1003}}" readonly="true" data-field-id="sPurchasePrice1003" /></td>
            </tr>
            {{/if}}
            <tr>
                <td class="FieldLabel">Sale Price of Any Personal Property Included in Sale</td>
                <td><input type="text" id="sGrossDueFromBorrPersonalProperty2" value="{{sGrossDueFromBorrPersonalProperty}}" readonly="true" /></td>
            </tr>
            {{#AdjustmentListWithAssets}}
                {{#if IsPaidToSeller && IncludeAdjustmentInLeCdForThisLien}}
                    <tr>
                        {{#if POC == true}}
                            <td class="FieldLabel">{{Description}} P.O.C. {{Amount}} {{PaidFromParty_rep}}
                            </td>
                        <td>
                            <input type="text" value="" readonly="true" />
                        </td>
                        {{else}}
                            <td class="FieldLabel">{{Description}}</td>
                        <td>
                            <input type="text" value="{{Amount}}" readonly="true" /></td>
                        {{/if}}
                    </tr>
                {{/if}}
            {{/AdjustmentListWithAssets}}
        </table>
    </script>
    
    <script id="AdjustmentsForItemsPaidBySellerInAdvance" type="text/html">
        <table>
            <tr>
                <td colspan="2" class="FormTableSubheader">Adjustments for Items Paid by Seller in Advance</td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    City/Town Taxes
                    {{#if ProrationList.CityTownTaxes.IsPaidFromBorrowerToSeller}}
                        <span class="proration-date-label">{{ProrationList.CityTownTaxes.PaidFromDate}} to {{ProrationList.CityTownTaxes.PaidToDate}}</span>
                    {{/if}}
                </td>
                <td><input type="text" readonly="true" value="{{ProrationList.CityTownTaxes.AmountPaidFromBorrowerToSeller}}" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    County Taxes
                    {{#if ProrationList.CountyTaxes.IsPaidFromBorrowerToSeller}}
                        <span class="proration-date-label">{{ProrationList.CountyTaxes.PaidFromDate}} to {{ProrationList.CountyTaxes.PaidToDate}}</span>
                    {{/if}}
                </td>
                <td><input type="text" readonly="true" value="{{ProrationList.CountyTaxes.AmountPaidFromBorrowerToSeller}}" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Assessments
                    {{#if ProrationList.Assessments.IsPaidFromBorrowerToSeller}}
                        <span class="proration-date-label">{{ProrationList.Assessments.PaidFromDate}} to {{ProrationList.Assessments.PaidToDate}}</span>
                    {{/if}}
                </td>
                <td><input type="text" readonly="true" value="{{ProrationList.Assessments.AmountPaidFromBorrowerToSeller}}" /></td>
            </tr>
            {{#ProrationList.CustomItems:i}}
                {{#if IsPaidFromBorrowerToSeller}}
                    <tr>
                        <td class="FieldLabel">{{Description}} <span class="proration-date-label">{{PaidFromDate}} to {{PaidToDate}}</span></td>
                        <td><input type="text" readonly="true" value="{{Amount}}" /></td>
                    </tr>
                {{/if}}
            {{/ProrationList.CustomItems}}
        </table>
    </script>
    
    <script id="SectionL" type="text/html">
        <table>
            <tr>
                <td colspan="2" class="LoanFormHeader">L. Paid Already by or on Behalf of the Borrower at Closing</td>
            </tr>
            <tr>
                <td class="FieldLabel">Deposit</td>
                <td><input type="text" id="sTRIDCashDeposit" value="{{sTRIDCashDeposit}}" readonly="true" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Loan Amount</td>
                <td><input type="text" id="sFinalLAmt" value="{{sFinalLAmt}}" readonly="true" /></td>
            </tr>
            {{#if !ExcludeSalesPriceAndFirstLienLoanAmountCredit}}
            <tr>
                <td class="FieldLabel">Other New Financing (Principal Balance 
                    <input type="text" data-field-id="sONewFinBal" id="sONewFinBal2" value="{{sONewFinBal}}" class="money" readonly on-blur="formatMoney" intro="initInput" /> )
                    <input type="checkbox" checked={{sONewFinNetProceedsLckd}} id="sONewFinNetProceedsLckd" on-click="sONewFinNetProceedsLckdChanged"/>
                </td>
                <td>
                    <input type="text" id ="sONewFinNetProceeds" readonly value="{{sONewFinNetProceeds}}" on-blur="formatMoney" intro="initInput" />
                </td>
            </tr>
            {{/if}}
            {{#if AdjustmentList.SellerCredit.IncludeAdjustmentInLeCdForThisLien}}
                <tr>
                    <td class="FieldLabel">Seller Credit</td>
                    <td><input type="text" value="{{AdjustmentList.SellerCredit.Amount}}" readonly="true" /></td>
                </tr>
            {{/if}}
            <tr>
                <td colspan="2" class="FormTableSubheader">Other Credits</td>
            </tr>
            {{#AdjustmentListWithAssets:i}}
                {{#if IsPaidToBorrower && IncludeAdjustmentInLeCdForThisLien}}
                    <tr>
                        {{#if POC == true}}
                            <td class="FieldLabel">{{Description}} P.O.C. {{Amount}} {{PaidFromParty_rep}}
                            </td>
                        <td>
                            <input type="text" value="" readonly="true" />
                        </td>
                        {{else}}
                            <td class="FieldLabel">{{Description}}</td>
                        <td>
                            <input type="text" value="{{Amount}}" readonly="true" /></td>
                        {{/if}}
                    </tr>
                {{/if}}
            {{/AdjustmentListWithAssets}}
        </table>
    </script>

    <script id="SectionN" type="text/html">
        <table>
            <tr>
                <td colspan="2" class="LoanFormHeader">N. Due from Seller at Closing</td>
            </tr>
            <tr>
                <td class="FieldLabel">Excess Deposit</td>
                <td><input type="text" id="sReductionsDueToSellerExcessDeposit" value="{{sReductionsDueToSellerExcessDeposit}}" on-blur="formatMoney" intro="initInput" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Closing Costs Paid at Closing (J)</td>
                <td><input type="text" id="sTotalSellerPaidClosingCostsPaidAtClosing" value="{{sTotalSellerPaidClosingCostsPaidAtClosing}}" readonly="true" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Payoff of First Mortgage Loan</td>
                <td><input type="text" id="sReductionsDueToSellerPayoffOf1stMtgLoan" value="{{sReductionsDueToSellerPayoffOf1stMtgLoan}}" on-blur="formatMoney" intro="initInput" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">Payoff of Second Mortgage Loan</td>
                <td><input type="text" id="sReductionsDueToSellerPayoffOf2ndMtgLoan" value="{{sReductionsDueToSellerPayoffOf2ndMtgLoan}}" on-blur="formatMoney" intro="initInput" /></td>
            </tr>
            {{#if AdjustmentList.SellerCredit.IncludeAdjustmentInLeCdForThisLien}}
                <tr>
                    <td class="FieldLabel">Seller Credit</td>
                    <td><input type="text" value="{{AdjustmentList.SellerCredit.Amount}}" readonly="true" /></td>
                </tr>
            {{/if}}
            {{#AdjustmentListWithAssets}}
                {{#if IsPaidFromSeller && IncludeAdjustmentInLeCdForThisLien}}
                    <tr>
                        {{#if POC == true}}
                        <td class="FieldLabel">
                            {{Description}} P.O.C. {{Amount}} {{PaidFromParty_rep}}
                        </td>
                        <td>
                            <input type="text" value="" readonly="true" />
                        </td>
                        {{else}}
                            <td class="FieldLabel">{{Description}}</td>
                        <td>
                            <input type="text" value="{{Amount}}" readonly="true" /></td>
                        {{/if}}
                    </tr>
                {{/if}}
            {{/AdjustmentListWithAssets}}
        </table>
    </script>
    
    <script id="AdjustmentsForItemsUnpaidBySellerInAdvance" type="text/html">
        <table>
            <tr>
                <td colspan="2" class="FormTableSubheader">Adjustments for Items Unpaid by Seller in Advance</td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    City/Town Taxes
                    {{#if ProrationList.CityTownTaxes.IsPaidFromSellerToBorrower}}
                        <span class="proration-date-label">{{ProrationList.CityTownTaxes.PaidFromDate}} to {{ProrationList.CityTownTaxes.PaidToDate}}</span>
                    {{/if}}
                </td>
                <td><input type="text" readonly="true" value="{{ProrationList.CityTownTaxes.AmountPaidFromSellerToBorrower}}" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    County Taxes
                    {{#if ProrationList.CountyTaxes.IsPaidFromSellerToBorrower}}
                        <span class="proration-date-label">{{ProrationList.CountyTaxes.PaidFromDate}} to {{ProrationList.CountyTaxes.PaidToDate}}</span>
                    {{/if}}
                </td>
                <td><input type="text" readonly="true" value="{{ProrationList.CountyTaxes.AmountPaidFromSellerToBorrower}}" /></td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    Assessments
                    {{#if ProrationList.Assessments.IsPaidFromSellerToBorrower}}
                        <span class="proration-date-label">{{ProrationList.Assessments.PaidFromDate}} to {{ProrationList.Assessments.PaidToDate}}</span>
                    {{/if}}
                </td>
                <td><input type="text" readonly="true" value="{{ProrationList.Assessments.AmountPaidFromSellerToBorrower}}" /></td>
            </tr>
            {{#ProrationList.CustomItems:i}}
                {{#if IsPaidFromSellerToBorrower}}
                    <tr>
                        <td class="FieldLabel">{{Description}} <span class="proration-date-label">{{PaidFromDate}} to {{PaidToDate}}</span></td>
                        <td><input type="text" readonly="true" value="{{Amount}}" /></td>
                    </tr>
                {{/if}}
            {{/ProrationList.CustomItems}}
        </table>
    </script>

    <script id="predefinedAdjustmentsTemplate" type="text/html">
            <h4 class="page-header">Add from Predefined Adjustments</h4>
            <div id="predefinedAdjustmentPopupDiv">
            <ul>
                {{#predefinedAdjustmentsList:i}}
                <li>
                    <a href="#" onclick="addPredefinedAdjustment('{{i}}');">{{PredefinedAdjustmentTypeMap[AdjustmentTypeString].Description}} - {{Description}}</a>
                </li>
                {{/predefinedAdjustmentsList}}
            </ul>            
        </div>
    </script>
</head>

<body id="bodyId" runat="server">
    <form id="form1" runat="server">
    <h1 class="MainRightHeader">Adjustments and Other Credits</h1>
    <div id="PageContainer">
    </div>
        <div class="Hidden">
            <div class="qualifying-borrower-section">
                <h2 class="FormTableHeader">Qualifying Borrower</h2>
                <uc1:QualifyingBorrower ID="QualifyingBorrower" runat="server" />
            </div>
        </div>

    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            var totalWidth = document.body.scrollWidth;
            var fth = $(".FormTableHeader");
            var mrh = $(".MainRightHeader");
            fth.width(totalWidth);
            mrh.width(totalWidth);
        });

        var globalRecalc;


        function addPredefinedAdjustment(i) {
            LQBPopup.Return({
                OK: true,
                index: i
            });
        }

        (function() {
            Ractive.transitions.initInput = function(t) {
                if (t.isIntro) {
                    _initDynamicInput(t.node);
                }
            }
            Ractive.transitions.initMask = Ractive.transitions.initInput;

            // Pull this into common spot.
            var DatePicker = Ractive.extend({
                template: '#DatePicker',
                data: {
                    value: '',
                    id: '',
                    readonly: false
                },
                init: function() {
                    var self = this;

                    this.on('showCalendar', function(e, id) {
                        var closeHandler = function(cal) {
                            var inputValue = self.find('input').value;
                            var modelValue = self.get('value');
                            var isReadonly = self.get('readonly');

                            if (!isReadonly && inputValue !== modelValue) {
                                self.updateModel('value');
                                self.fire('recalc');
                            }
                            cal.hide();
                        }
                        return displayCalendar(id, closeHandler);
                    });

                    this.on('domblur', function(event) {
                        var kp = event.keypath;
                        //sadly this blur event fires before the mask.js blur event 
                        //by using setTimeout we queue up the function until after all the other
                        //js code.
                        window.setTimeout(function() {
                            self.updateModel("value", true);
                        });
                    });

                    this.on('updatePicker', function(event) {
                        var ractive = this;
                        window.setTimeout(function() {
                            self.set("value", event.node.value);
                            if (!ractive.get('readonly')) {
                                ractive.fire('recalc');
                            }
                        }, 0);
                    });
                }
            });

            var AdjustmentDescription = Ractive.extend({
                template: '#AdjustmentDescription',
                data: {
                    id: '',
                    value: '',
                    index: null,
                    adjustmentTypes: [
                        'Employer Assisted Housing',
                        'Lease Purchase Fund',
                        'Relocation Funds',
                    ],
                    adjustmentTypeMap: {
                        'employer assisted housing': {
                            PaidFrom: '4',
                            PaidTo: '1',
                            IsPopulateToLineLOn1003: true,
                            PopulateTo: 1,
                        },
                        'lease purchase fund': {
                            PaidFrom: '2',
                            PaidTo: '1',
                            IsPopulateToLineLOn1003: true,
                            PopulateTo: 1,
                        },
                        'relocation funds': {
                            PaidFrom: '4',
                            PaidTo: '1',
                            IsPopulateToLineLOn1003: true,
                            PopulateTo: 1,
                        }
                    }
                },
                lazy: true,
                onrender: function() {
                    var ractive = this;
                    var input = this.find('input[type="text"]');
                    var textboxId = input.id;
                    var img = this.find('img');
                    var imgClickHandler = function() {
                        onSelect(input.id);
                    };
                    var keydownHandler = function(event) {
                        onComboboxKeypress(document.getElementById(input.id), event);
                    };
                    var changeHandler = function() {
                        var description = document.getElementById(textboxId).value;
                        var key = description.toLowerCase();
                        var map = ractive.get('adjustmentTypeMap');
                        var typeInfo, paidFrom, paidTo, isPopulateToLineLOn1003, keypath, modelItem, populateTo;

                        if (map && map.hasOwnProperty(key)) {
                            typeInfo = map[key];
                            keypath = 'AdjustmentList.CustomItems.' + ractive.get('index');

                            paidFrom = typeInfo.PaidFrom;
                            paidTo = typeInfo.PaidTo;
                            isPopulateToLineLOn1003 = typeInfo.IsPopulateToLineLOn1003;
                            populateTo = typeInfo.PopulateTo;

                            modelItem = ractive.get(keypath);
                            modelItem.Description = description;
                            modelItem.PaidFromParty = paidFrom;
                            modelItem.PaidToParty = paidTo;
                            modelItem.IsPopulateToLineLOn1003 = isPopulateToLineLOn1003;
                            modelItem.PopulateTo = populateTo;
                            
                            ractive.update(keypath);

                            ractive.fire('recalc');
                        }
                    };

                    createCombobox(this.get('id'), this.get('adjustmentTypes'));

                        addEventHandler(input, 'keydown', keydownHandler);
                        addEventHandler(input, 'change', changeHandler);

                        addEventHandler(img, 'click', imgClickHandler);
                }
            });

            var AdjustmentPartySelect = Ractive.extend({
                template: '#AdjustmentPartySelect',
                data: {
                    value: '',
                    readonly: false
                },
                oninit: function() {
                    this.on({
                        recalc: function() {
                            this.updateModel();
                        }
                    });
                }
            });

            var predefinedAdjustmentsTemplate = Ractive.extend({
                template: '#predefinedAdjustmentsTemplate',
                data: {
                    PredefinedAdjustmentTypeMap: AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap
                }
            });

            var adjustmentRow = Ractive.extend({
                template: '#AdjustmentRow',
                data: {
                    Id:null,
                    isSystemAdjustment: false,
                    disabled: false,
                    AdjustmentType: -1,
                    AdjustmentTypeString: "Blank",
                    PredefinedAdjustmentTypeMap: AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap,
                    Version: ML.LoanVersionTCurrent,
                    index: -1,
                    Description: '',
                    PaidFromParty: 0,
                    PaidToParty: 0,
                    Amount: '$0.00',
                    BorrowerAmount: '$0.00',
                    SellerAmount: '$0.00',
                    IsPopulateToLineLOn1003: false,
                    POC: false,
                    DFLP: false
                },
                components: {
                    adjustmentDescription: AdjustmentDescription,
                    adjustmentPartySelect: AdjustmentPartySelect
                },
                oninit: function () {
                    this.on({
                        include1003Checked: function (event, index, isSystemAdjustment) {
                            var include1003Checkbox = this.event.node;
                            if (typeof index === 'undefined') {
                                index = -1;
                            }

                            this.fire('recalc1003', include1003Checkbox.checked, index, isSystemAdjustment);
                        },
                        pocChecked: function (event) {
                            var pocCheckbox = this.event.node;
                            var dflp = this.get('DFLP');

                            if (pocCheckbox.checked && dflp) {
                                this.set('DFLP', false);
                            }

                            this.fire('recalc');
                        },
                        dflpChecked: function(event) {
                            var dflpCheckbox = this.event.node;
                            var poc = this.get('POC');

                            if (dflpCheckbox.checked && poc) {
                                this.set('POC', false);
                            }

                            this.fire('recalc');
                        },
                        IncludeAdjustmentInLeCdForThisLienClick: function (event) {
                            this.updateModel();
                            this.fire('recalc');
                        }
                    });
                }
            });

            var prorationRow = Ractive.extend({
                template: '#ProrationRow',
                data: {
                    Id:null,
                    index: -1,
                    RowId: '',
                    IsSystemProration: false,
                    Description: '',
                    PaidThroughDate: '',
                    PaidFromDate: '',
                    PaidToDate: '',
                    Amount: '$0.00',
                    PaidFromParty: '0',
                    PaidToParty: '0',
                    IsPaidFromBorrowerToSeller: false,
                    IsPaidFromSellerToBorrower: false
                },
                components: {
                    datePicker: DatePicker
                }
            });

            var ractive = new Ractive({
                el: '#PageContainer',
                template: '#PageTemplate',
                partials: '#AdjustmentSection,#ProrationSection,#ProrationTable,#SummariesOfTransactionsSection,#SectionK,#SectionL,#SectionM,#SectionN,#AdjustmentsForItemsPaidBySellerInAdvance,#AdjustmentsForItemsUnpaidBySellerInAdvance',
                components: {
                    datePicker: DatePicker,
                    adjustmentRow: adjustmentRow,
                    predefinedAdjustmentsTemplate: predefinedAdjustmentsTemplate,
                    prorationRow: prorationRow
                },
                data: AdjustmentsAndOtherCreditsData,
                oninit: function() {
                    var ractive = this;

                    var callServiceMethod = function(methodName, data, onSuccess) {
                        var errMsg;
                        var result = gService.loanedit.call(methodName, data);

                        if (!result.error) {
                            if (result.value["IsValid"] && result.value["IsValid"] === "False")
                            {
                                alert(result.value["ErrorMsg"]);
                            }
                            else
                            {
                                onSuccess(result.value);
                            }
                        } else {
                            if (result.ErrorType === 'LoanFieldWritePermissionDenied' &&
                                    typeof f_displayFieldWriteDenied === 'function') {
                                f_displayFieldWriteDenied(result.UserMessage);
                                return false;
                            }

                            errMsg = 'Unable to process data. Please try again.';

                            if (result.UserMessage) {
                                errMsg = result.UserMessage;
                            }

                            alert(errMsg);
                        }
                    };

                    function ValidateAdjustmentDescriptions(adjustmentList)
                    {
                        var validDescriptions = true;
                        $(adjustmentList).each(function () {
                            if (this.Description.length > 60) {
                                validDescriptions = false;
                            }
                        });

                        return validDescriptions;
                    }

                    window.f_saveMe = function () {
                        var modelData = ractive.get();
                        if (!ValidateAdjustmentDescriptions(modelData.AdjustmentList.CustomItems))
                        {
                            alert("Descriptions of adjustments on the Adjustments and Other Credits page cannot exceed 60 characters");
                            return;
                        }

                        var data = {
                            loanId: ML.sLId,
                            viewModelJson: JSON.stringify(modelData)
                        };
                        var successHandler = function(returnValue) {
                            window.setTimeout(function () {
                                var model = JSON.parse(returnValue.model);
                                ractive.set(model);
                                populateQualifyingBorrower(model.qualifyingBorrowerModel);
                                clearDirty();
                            }, 0);
                        };

                        callServiceMethod('Save', data, successHandler);
                    }

                    window.saveMe = window.f_saveMe;

                    function recalc1003(include1003Check, index, isSystemAdjustment) {
                        var ractive = this;

                        var ractiveModel = this.get();
                        if (ML.isUlad2019) {
                            ractiveModel.qualifyingBorrowerModel = retrieveQualifyingBorrowerModelForUpdate();
                        }

                        var data = {
                            loanId: ML.sLId,
                            viewModelJson: JSON.stringify(ractiveModel),
                            include1003Check: include1003Check,
                            index: index,
                            isSystemAdjustment: isSystemAdjustment
                        };

                        var successHandler = function (data) {
                            var model = JSON.parse(data.model);
                            ractive.set(model);
                            populateQualifyingBorrower(model.qualifyingBorrowerModel);
                        };

                        callServiceMethod('Calculate1003CheckboxToHandleCircularDependency', data, successHandler);
                    }

                    function recalc(event) {
                        var ractive = this;
                        
                        if (event && event.node && event.node.tagName &&
                                    event.node.tagName === 'SELECT') {
                            // Not sure of a way around this. The value from 
                            // the UI wasn't two-way binding.
                            ractive.updateModel();
                        }

                        var ractiveModel = this.get();
                        if (ML.isUlad2019) {
                            ractiveModel.qualifyingBorrowerModel = retrieveQualifyingBorrowerModelForUpdate();
                        }

                        var data = {
                            loanId: ML.sLId,
                            viewModelJson: JSON.stringify(ractiveModel)
                        };
                        var successHandler = function(data) {
                            var model = JSON.parse(data.model);
                            ractive.set(model);
                            populateQualifyingBorrower(model.qualifyingBorrowerModel);
                        };

                        callServiceMethod('CalculateData', data, successHandler);
                    }

                    this.on({
                        addProration: function(event) {
                            this.push('ProrationList.CustomItems', {
                                Id:null,
                                Amount: '$0.00',
                                Description: '',
                                IsSystemProration: false,
                                ProrationType: 0,
                                PaidFromDate: '',
                                PaidFromParty: 0,
                                PaidThroughDate: '',
                                PaidToDate: '',
                                PaidToParty: 0,
                                IsPaidFromBorrowerToSeller: false,
                                IsPaidFromSellerToBorrower: false
                            });
                            updateDirtyBit();
                        },
                        '*.removeProration': function(event) {
                            this.splice('ProrationList.CustomItems', event.context.index, 1);
                            updateDirtyBit();
                        },
                        addAdjustment: function (event) {
                            if (ML.LoanVersionTCurrent >= 24) {                                
                                var popup = $('#popupTemplate');
                                
                                if (predefinedAdjustments.length === 0) {
                                    alert("There are no Adjustments and Other Credits available to add to the file.");
                                    return;
                                }

                                LQBPopup.ShowElement(popup, {
                                    'width': 600,
                                    'height': 400, // Display the first 30 before adding a scrollbar
                                    'dialogClasses' : 'predefined-adjustment-popup',
                                    onReturn: function (dialogArgs) {
                                        if (dialogArgs.OK) {
                                            var typeInfo = predefinedAdjustments[dialogArgs.index];
                                            ractive.push('AdjustmentList.CustomItems', {
                                                Id: null,
                                                isSystemAdjustment: false,
                                                Description: typeInfo.Description,
                                                AdjustmentType: typeInfo.AdjustmentType,
                                                AdjustmentTypeString: typeInfo.AdjustmentTypeString,
                                                AdjustmentTypeDescription: typeInfo.Description,
                                                Version: ML.LoanVersionTCurrent,
                                                PaidFromParty: typeInfo.PaidFromParty,
                                                PaidToParty: typeInfo.PaidToParty,
                                                Amount: '$0.00',
                                                BorrowerAmount: '$0.00',
                                                SellerAmount: '$0.00',
                                                IsPopulateToLineLOn1003: typeInfo.IsPopulateToLineLOn1003,
                                                PopulateTo: typeInfo.PopulateTo,
                                                IsPaidFromBorrower: (typeInfo.PaidFromParty === 1),
                                                IsPaidFromSeller: (typeInfo.PaidFromParty === 2),
                                                IsPaidToBorrower: (typeInfo.PaidToParty === 1),
                                                IsPaidToSeller: (typeInfo.PaidToParty === 2),
                                                POC: typeInfo.POC,
                                                DFLP: typeInfo.DFLP,
                                                IncludeAdjustmentInLeCdForThisLien: true
                                            });
                                            updateDirtyBit();

                                            $j(".includeLeCd").toggle(AdjustmentsAndOtherCreditsData.IsTrid2);
                                        }
                                    }
                                }); 
                            }
                            else {
                                this.push('AdjustmentList.CustomItems', {
                                    Id: null,
                                    isSystemAdjustment: false,
                                    Description: '',
                                    AdjustmentType: -1,
                                    AdjustmentTypeString: "Blank",
                                    Version: ML.LoanVersionTCurrent,
                                    PaidFromParty: 0,
                                    PaidToParty: 0,
                                    Amount: '$0.00',
                                    BorrowerAmount: '$0.00',
                                    SellerAmount: '$0.00',
                                    IsPopulateToLineLOn1003: false,
                                    IsPaidFromBorrower: false,
                                    IsPaidFromSeller: false,
                                    IsPaidToBorrower: false,
                                    IsPaidToSeller: false,
                                    POC: false,
                                    DFLP: false,
                                    IncludeAdjustmentInLeCdForThisLien: true
                                });
                                updateDirtyBit();

                                $(".includeLeCd").toggle(AdjustmentsAndOtherCreditsData.IsTrid2);
                            }                            
                        },
                        '*.removeAdjustment': function (event, index) {
                            this.splice('AdjustmentList.CustomItems', index, 1);
                            ractive.fire('.recalc');
                            updateDirtyBit();
                        },
                        'Migrate1003ToAdjustments': function(event) {
                            if (isDirty()) {
                                alert('Please save before migrating.');
                                return;
                            }
                            var data =
                            {
                                loanId: ML.sLId
                            };
                            var successHandler = function(data) {
                                var model = JSON.parse(data.model);
                                ractive.set(model);
                                populateQualifyingBorrower(model.qualifyingBorrowerModel);
                            };

                            callServiceMethod('Migrate1003ToAdjustments', data, successHandler);
                        },
                        'recalc': recalc,
                        '*.recalc': recalc,
                        '*.recalc1003': recalc1003,
                        'IncludeProrationsInLeCdForThisLienClick': function(event) {
                            ractive.updateModel();
                            ractive.fire('.recalc');
                        },
                        'sRefPdOffAmt1003LckdChanged': function(event) {
                            $("#sRefPdOffAmt1003").prop("readonly", !$("#sRefPdOffAmt1003Lckd").prop("checked"));
                            ractive.updateModel();
                            ractive.fire('.recalc');
                        },
                        'sONewFinNetProceedsLckdChanged': function (event) {
                            $("#sONewFinNetProceeds").prop("readonly", !$("#sONewFinNetProceedsLckd").prop("checked"));
                            ractive.updateModel();
                            ractive.fire('.recalc');
                        },
                        'sPropertyTransferDLckdChanged': function (event) {
                            ractive.updateModel();
                            ractive.fire('.recalc');
                        },
                        'sAltCostLckdChanged': function (event) {
                            $("#sAltCost").prop("readonly", !$("#sAltCostLckd").prop("checked"));
                            ractive.updateModel();
                            ractive.fire('.recalc');
                        },
                        'sTRIDClosingDisclosureAltCostLckdChanged': function (event) {
                            $("#sTRIDClosingDisclosureAltCost").prop("readonly", !$("#sTRIDClosingDisclosureAltCostLckd").prop("checked"));
                            ractive.updateModel();
                            ractive.fire('.recalc');
                        },
                        formatMoney: function(event) {
                            format_money(event.node);
                        }
                    });
                }
            });

            var $qbContainer = $('.true-container');
            if (ML.isUlad2019) {
                $qbContainer.attr('class', $qbContainer.attr('class') + ' wrap');
                $('#DetailsOfTransactionContainer').html('').append($('.qualifying-borrower-section'));
                initializeQualifyingBorrower(false, function () { ractive.fire('recalc') }, true);
                populateQualifyingBorrower(AdjustmentsAndOtherCreditsData.qualifyingBorrowerModel);
            }
            else {
                $('.qualifying-borrower-section').remove();
            }

            $("#sRefPdOffAmt1003").prop("readonly", !$("#sRefPdOffAmt1003Lckd").prop("checked"));
            $("#sONewFinNetProceeds").prop("readonly", !$("#sONewFinNetProceedsLckd").prop("checked"));
            $("#sAltCost").prop("readonly", !$("#sAltCostLckd").prop("checked"));
            $("#sTRIDClosingDisclosureAltCost").prop("readonly", !$("#sTRIDClosingDisclosureAltCostLckd").prop("checked"));
            $(".includeLeCd").toggle(AdjustmentsAndOtherCreditsData.IsTrid2);
            $(".includeProrationInLeCdDiv").toggle(AdjustmentsAndOtherCreditsData.IsTrid2);
        })();
    </script>
</body>
</html>
