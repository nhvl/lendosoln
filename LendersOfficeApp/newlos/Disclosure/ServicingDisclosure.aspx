﻿<%@ Page Language="C#" CodeBehind="ServicingDisclosure.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.ServicingDisclosure" AutoEventWireup="false"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >


<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Servicing Disclosure Statement</title>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".Exclusive input").change(function() {
                var el = $j(this);
                var checked = el.prop("checked");

                $(".Exclusive input").prop("checked", false);
                el.prop("checked", checked);
            });
        });
    </script>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <table class="FormTable" width="100%" border="0">
      <tr><td class="MainRightHeader">Servicing Disclosure Statement</td></tr>
      <tr>
        <td style="padding-left:15px;">
          <table cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td class="FieldLabel" nowrap></td>
              <td nowrap>
                <UC:CFM ID="CFM1" Name="CFM" runat="server" Type="21" CompanyNameField="ServicingDisclosure_CompanyName" StreetAddressField="ServicingDisclosure_StreetAddr" CityField="ServicingDisclosure_City" StateField="ServicingDisclosure_State" ZipField="ServicingDisclosure_Zip"></UC:CFM>
              </td>
            </tr>
            <tr>
              <td class="FieldLabel">Lender Name</td>
              <td><asp:TextBox ID="ServicingDisclosure_CompanyName" runat="server" Width="296px" /></td>
            </tr>
            <tr>
              <td class="FieldLabel">Lender Address</td>
              <td><asp:TextBox ID="ServicingDisclosure_StreetAddr" runat="server" Width="296px" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><asp:TextBox ID="ServicingDisclosure_City" runat="server" Width="203px" /><ml:StateDropDownList ID="ServicingDisclosure_State" runat="server" /><ml:ZipcodeTextBox ID="ServicingDisclosure_Zip" runat="server" preset="zipcode" Width="50px" /></td>
            </tr>
            <tr>
              <td class="FieldLabel">Date</td>
              <td><ml:DateTextBox ID="GfeTil_PrepareDate" runat="server"  preset="date" CssClass="mask"></ml:DateTextBox></td>
            </tr>
            <tr>
              <td class="FieldLabel" colspan="2">&nbsp;</td>
            </tr>            
            <tr>
              <td class="FieldLabel" colspan="2">Servicing Transfer Estimates</td>
            </tr>
            <tr>
              <td colspan="2">
                <asp:CheckBox class="Exclusive" ID="sMayAssignSellTransfer" runat="server" Text="We may assign, sell or transfer the servicing of your loan while the loan is outstanding." />
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <asp:CheckBox class="Exclusive" ID="sDontService" runat="server" Text="We do not service mortgage loans of the type for which you applied. We intend to assign, sell, or transfer the servicing of your mortgage loan before the first payment is due." />
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <asp:CheckBox class="Exclusive" ID="sServiceDontIntendToTransfer" runat="server" Text="The loan for which you have applied will be serviced at this financial institution and we do not intend to sell, transfer, or assign the servicing of the loan." />
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    </form>
    <uc1:cModalDlg id="CModalDlg1" runat="server" />
</body>
</html>
