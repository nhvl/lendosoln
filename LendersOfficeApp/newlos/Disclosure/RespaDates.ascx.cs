﻿#region Generated Code -- A lie to dupe the merciless StyleCop.
namespace LendersOfficeApp.newlos.Disclosure
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Contains the dates relevant to the calculation of the sRespa6D field.
    /// </summary>
    public partial class RespaDates : System.Web.UI.UserControl
    {
        /// <summary>
        /// Loads the control data from the service item.
        /// </summary>
        /// <param name="loan">The loan to load.</param>
        /// <param name="service">The service item to send the data through.</param>
        /// <param name="clientId">The client id of the control.</param>
        public static void LoadData(CPageData loan, AbstractBackgroundServiceItem service, string clientId)
        {
            if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(
                loan.sLoanVersionT,
                LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                return;
            }

            service.SetResult(ToMangledId(nameof(loan.sRespa6D), clientId), loan.sRespa6D_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespa6DLckd), clientId), loan.sRespa6DLckd);
            service.SetResult(ToMangledId(nameof(loan.sRespaBorrowerNameCollectedD), clientId), loan.sRespaBorrowerNameCollectedD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaBorrowerNameCollectedDLckd), clientId), loan.sRespaBorrowerNameCollectedDLckd);
            service.SetResult(ToMangledId(nameof(loan.sRespaBorrowerNameFirstEnteredD), clientId), loan.sRespaBorrowerNameFirstEnteredD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaBorrowerSsnCollectedD), clientId), loan.sRespaBorrowerSsnCollectedD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaBorrowerSsnCollectedDLckd), clientId), loan.sRespaBorrowerSsnCollectedDLckd);
            service.SetResult(ToMangledId(nameof(loan.sRespaBorrowerSsnFirstEnteredD), clientId), loan.sRespaBorrowerSsnFirstEnteredD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaIncomeCollectedD), clientId), loan.sRespaIncomeCollectedD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaIncomeCollectedDLckd), clientId), loan.sRespaIncomeCollectedDLckd);
            service.SetResult(ToMangledId(nameof(loan.sRespaIncomeFirstEnteredD), clientId), loan.sRespaIncomeFirstEnteredD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaPropAddressCollectedD), clientId), loan.sRespaPropAddressCollectedD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaPropAddressCollectedDLckd), clientId), loan.sRespaPropAddressCollectedDLckd);
            service.SetResult(ToMangledId(nameof(loan.sRespaPropAddressFirstEnteredD), clientId), loan.sRespaPropAddressFirstEnteredD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaPropValueCollectedD), clientId), loan.sRespaPropValueCollectedD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaPropValueCollectedDLckd), clientId), loan.sRespaPropValueCollectedDLckd);
            service.SetResult(ToMangledId(nameof(loan.sRespaPropValueFirstEnteredD), clientId), loan.sRespaPropValueFirstEnteredD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaLoanAmountCollectedD), clientId), loan.sRespaLoanAmountCollectedD_rep);
            service.SetResult(ToMangledId(nameof(loan.sRespaLoanAmountCollectedDLckd), clientId), loan.sRespaLoanAmountCollectedDLckd);
            service.SetResult(ToMangledId(nameof(loan.sRespaLoanAmountFirstEnteredD), clientId), loan.sRespaLoanAmountFirstEnteredD_rep);
        }

        /// <summary>
        /// Binds the control data from the service item to the loan.
        /// </summary>
        /// <param name="loan">The loan to bind.</param>
        /// <param name="service">The service item to bind data from.</param>
        /// <param name="clientId">The client id of the control.</param>
        public static void BindData(CPageData loan, AbstractBackgroundServiceItem service, string clientId)
        {
            if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(
                loan.sLoanVersionT,
                LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                return;
            }

            loan.sRespa6D_rep = service.GetString(ToMangledId(nameof(loan.sRespa6D), clientId));
            loan.sRespa6DLckd = service.GetBool(ToMangledId(nameof(loan.sRespa6DLckd), clientId));
            loan.sRespaBorrowerNameCollectedD_rep = service.GetString(ToMangledId(nameof(loan.sRespaBorrowerNameCollectedD), clientId));
            loan.sRespaBorrowerNameCollectedDLckd = service.GetBool(ToMangledId(nameof(loan.sRespaBorrowerNameCollectedDLckd), clientId));
            loan.sRespaBorrowerSsnCollectedD_rep = service.GetString(ToMangledId(nameof(loan.sRespaBorrowerSsnCollectedD), clientId));
            loan.sRespaBorrowerSsnCollectedDLckd = service.GetBool(ToMangledId(nameof(loan.sRespaBorrowerSsnCollectedDLckd), clientId));
            loan.sRespaIncomeCollectedD_rep = service.GetString(ToMangledId(nameof(loan.sRespaIncomeCollectedD), clientId));
            loan.sRespaIncomeCollectedDLckd = service.GetBool(ToMangledId(nameof(loan.sRespaIncomeCollectedDLckd), clientId));
            loan.sRespaPropAddressCollectedD_rep = service.GetString(ToMangledId(nameof(loan.sRespaPropAddressCollectedD), clientId));
            loan.sRespaPropAddressCollectedDLckd = service.GetBool(ToMangledId(nameof(loan.sRespaPropAddressCollectedDLckd), clientId));
            loan.sRespaPropValueCollectedD_rep = service.GetString(ToMangledId(nameof(loan.sRespaPropValueCollectedD), clientId));
            loan.sRespaPropValueCollectedDLckd = service.GetBool(ToMangledId(nameof(loan.sRespaPropValueCollectedDLckd), clientId));
            loan.sRespaLoanAmountCollectedD_rep = service.GetString(ToMangledId(nameof(loan.sRespaLoanAmountCollectedD), clientId));
            loan.sRespaLoanAmountCollectedDLckd = service.GetBool(ToMangledId(nameof(loan.sRespaLoanAmountCollectedDLckd), clientId));
        }

        /// <summary>
        /// Performs the initial load of the control.
        /// </summary>
        /// <param name="loan">The loan to load data from.</param>
        /// <param name="userHasClearPermission">A value indicating whether the user has permission to clear entered dates.</param>
        public void LoadData(CPageData loan, bool userHasClearPermission)
        {
            if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(
                loan.sLoanVersionT,
                LoanVersionT.V17_UpdateApplicationSubmittedCalculation))
            {
                this.Visible = false;
                return;
            }

            var appRepeaterDataSource = loan.Apps.Select((app, index) => new AppRepeaterData()
            {
                Index = index + 1,
                Application = app,
                DisplayClearButton = userHasClearPermission
            });

            this.sRespa6D.Text = loan.sRespa6D_rep;
            this.sRespa6DLckd.Checked = loan.sRespa6DLckd;
            this.ConsumerPortalRow.Visible = loan.sConsumerPortalCreationD.IsValid;
            this.sConsumerPortalSubmissionToLenderD.Text = loan.sConsumerPortalSubmissionToLenderD_rep;
            this.sRespaBorrowerNameCollectedD.Text = loan.sRespaBorrowerNameCollectedD_rep;
            this.sRespaBorrowerNameCollectedDLckd.Checked = loan.sRespaBorrowerNameCollectedDLckd;
            this.sRespaBorrowerNameFirstEnteredD.Text = loan.sRespaBorrowerNameFirstEnteredD_rep;
            this.BorrowerNameEnteredDates.DataSource = appRepeaterDataSource;
            this.BorrowerNameEnteredDates.DataBind();
            this.sRespaBorrowerSsnCollectedD.Text = loan.sRespaBorrowerSsnCollectedD_rep;
            this.sRespaBorrowerSsnCollectedDLckd.Checked = loan.sRespaBorrowerSsnCollectedDLckd;
            this.sRespaBorrowerSsnFirstEnteredD.Text = loan.sRespaBorrowerSsnFirstEnteredD_rep;
            this.BorrowerSsnEnteredDates.DataSource = appRepeaterDataSource;
            this.BorrowerSsnEnteredDates.DataBind();
            this.sRespaIncomeCollectedD.Text = loan.sRespaIncomeCollectedD_rep;
            this.sRespaIncomeCollectedDLckd.Checked = loan.sRespaIncomeCollectedDLckd;
            this.sRespaIncomeFirstEnteredD.Text = loan.sRespaIncomeFirstEnteredD_rep;
            this.sRespaPropAddressCollectedD.Text = loan.sRespaPropAddressCollectedD_rep;
            this.sRespaPropAddressCollectedDLckd.Checked = loan.sRespaPropAddressCollectedDLckd;
            this.sRespaPropAddressFirstEnteredD.Text = loan.sRespaPropAddressFirstEnteredD_rep;
            this.sRespaPropValueCollectedD.Text = loan.sRespaPropValueCollectedD_rep;
            this.sRespaPropValueCollectedDLckd.Checked = loan.sRespaPropValueCollectedDLckd;
            this.sRespaPropValueFirstEnteredD.Text = loan.sRespaPropValueFirstEnteredD_rep;
            this.sRespaLoanAmountCollectedD.Text = loan.sRespaLoanAmountCollectedD_rep;
            this.sRespaLoanAmountCollectedDLckd.Checked = loan.sRespaLoanAmountCollectedDLckd;
            this.sRespaLoanAmountFirstEnteredD.Text = loan.sRespaLoanAmountFirstEnteredD_rep;

            var clearButtons = new List<HtmlInputButton>
            {
                this.Clear_sRespaIncomeFirstEnteredD,
                this.Clear_sRespaPropAddressFirstEnteredD,
                this.Clear_sRespaPropValueFirstEnteredD,
                this.Clear_sRespaLoanAmountFirstEnteredD
            };

            clearButtons.ForEach(c => c.Visible = userHasClearPermission);
        }

        /// <summary>
        /// Page load handler.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var basePage = this.Page as BasePage;
            if (basePage == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Can only use this control on BasePage");
            }

            basePage.RegisterCSS("RespaDates.min.css");
            basePage.RegisterJsScript("RespaDates.js");
            basePage.RegisterJsGlobalVariables("RespaDatesClientId", this.ClientID);
        }

        /// <summary>
        /// Event handler for borrower name entered dates repeater.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void BorrowerNameEnteredDates_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var dataItem = e.Item.DataItem as AppRepeaterData;
            if (dataItem == null)
            {
                return;
            }

            var borrAppLabel = e.Item.FindControl("AppLabel1") as Literal;
            borrAppLabel.Text = "App " + dataItem.Index;
            var coborrAppLabel = e.Item.FindControl("AppLabel2") as Literal;
            coborrAppLabel.Text = "App " + dataItem.Index;

            var name = e.Item.FindControl("aBNm") as Literal;
            name.Text = GetDisplayName(dataItem.Application, E_BorrowerModeT.Borrower);

            var enteredDate = e.Item.FindControl("aBNmFirstEnteredD") as DateTextBox;
            enteredDate.Text = dataItem.Application.aBNmFirstEnteredD_rep;

            var clear = e.Item.FindControl("ClearBorrower") as HtmlInputButton;
            clear.Attributes.Add("data-application-id", dataItem.Application.aAppId.ToString());
            clear.Visible = dataItem.DisplayClearButton;

            var coborrowerRow = e.Item.FindControl("CoborrowerNameRow") as HtmlGenericControl;

            if (dataItem.Application.aBHasSpouse || HasCoborrowerEnteredDates(dataItem.Application))
            {
                name = e.Item.FindControl("aCNm") as Literal;
                name.Text = GetDisplayName(dataItem.Application, E_BorrowerModeT.Coborrower);

                enteredDate = e.Item.FindControl("aCNmFirstEnteredD") as DateTextBox;
                enteredDate.Text = dataItem.Application.aCNmFirstEnteredD_rep;

                clear = e.Item.FindControl("ClearCoborrower") as HtmlInputButton;
                clear.Attributes.Add("data-application-id", dataItem.Application.aAppId.ToString());
                clear.Visible = dataItem.DisplayClearButton;
            }
            else
            {
                coborrowerRow.Visible = false;
            }
        }

        /// <summary>
        /// Event handler for borrower ssn entered dates repeater.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void BorrowerSsnEnteredDates_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var dataItem = e.Item.DataItem as AppRepeaterData;
            if (dataItem == null)
            {
                return;
            }

            var borrAppLabel = e.Item.FindControl("AppLabel3") as Literal;
            borrAppLabel.Text = "App " + dataItem.Index;
            var coborrAppLabel = e.Item.FindControl("AppLabel4") as Literal;
            coborrAppLabel.Text = "App " + dataItem.Index;

            var name = e.Item.FindControl("aBNm2") as Literal;
            name.Text = GetDisplayName(dataItem.Application, E_BorrowerModeT.Borrower);

            var enteredDate = e.Item.FindControl("aBSsnFirstEnteredD") as DateTextBox;
            enteredDate.Text = dataItem.Application.aBSsnFirstEnteredD_rep;

            var clear = e.Item.FindControl("ClearBorrower") as HtmlInputButton;
            clear.Attributes.Add("data-application-id", dataItem.Application.aAppId.ToString());
            clear.Visible = dataItem.DisplayClearButton;

            var coborrowerRow = e.Item.FindControl("CoborrowerSsnRow") as HtmlGenericControl;

            if (dataItem.Application.aBHasSpouse || HasCoborrowerEnteredDates(dataItem.Application))
            {
                name = e.Item.FindControl("aCNm2") as Literal;
                name.Text = GetDisplayName(dataItem.Application, E_BorrowerModeT.Coborrower);

                enteredDate = e.Item.FindControl("aCSsnFirstEnteredD") as DateTextBox;
                enteredDate.Text = dataItem.Application.aCSsnFirstEnteredD_rep;

                clear = e.Item.FindControl("ClearCoborrower") as HtmlInputButton;
                clear.Attributes.Add("data-application-id", dataItem.Application.aAppId.ToString());
                clear.Visible = dataItem.DisplayClearButton;
            }
            else
            {
                coborrowerRow.Visible = false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the application has a coborrower
        /// name or ssn first entered date filled out.
        /// </summary>
        /// <param name="application">The application to check.</param>
        /// <returns>True if either of the entered dates is value. Otherwise, false.</returns>
        private static bool HasCoborrowerEnteredDates(CAppData application)
        {
            return !string.IsNullOrEmpty(application.aCNmFirstEnteredD_rep)
                || !string.IsNullOrEmpty(application.aCSsnFirstEnteredD_rep);
        }

        /// <summary>
        /// Gets the display name for the borrower.
        /// </summary>
        /// <param name="application">The application.</param>
        /// <param name="borrower">The borrower.</param>
        /// <returns>The display name.</returns>
        private static string GetDisplayName(CAppData application, E_BorrowerModeT borrower)
        {
            var displayName = borrower == E_BorrowerModeT.Borrower
                ? application.aBNm
                : application.aCNm;

            if (string.IsNullOrWhiteSpace(displayName))
            {
                displayName = borrower == E_BorrowerModeT.Borrower
                    ? "(Borrower Name Blank)"
                    : "(Co-borrower Name Blank)";
            }

            return displayName;
        }

        /// <summary>
        /// Constructs the mangled element id for the given field id and client id.
        /// </summary>
        /// <param name="fieldId">The id of the field.</param>
        /// <param name="clientId">The client id of the control.</param>
        /// <returns>The mangled id.</returns>
        private static string ToMangledId(string fieldId, string clientId)
        {
            return $"{clientId}_{fieldId}";
        }

        /// <summary>
        /// Data source for the application data repeaters.
        /// </summary>
        private class AppRepeaterData
        {
            /// <summary>
            /// Gets or sets the 1-based index of the application.
            /// </summary>
            /// <value>The 1-based index of the application.</value>
            public int Index { get; set; }

            /// <summary>
            /// Gets or sets the loan application.
            /// </summary>
            public CAppData Application { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the "Clear" button should
            /// be displayed next to the entered date.
            /// </summary>
            public bool DisplayClearButton { get; set; }
        }
    }
}