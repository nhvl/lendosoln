﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class DocMagicLoanOptionsSafeHarborDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(DocMagicLoanOptionsSafeHarborDisclosureServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // Lowest Interest Rate
            dataLoan.sDocMagicAntiSteeringLowestInterestRateLenderName = GetString("sDocMagicAntiSteeringLowestInterestRateLenderName");
            dataLoan.sDocMagicAntiSteeringLowestInterestRateDescription = GetString("sDocMagicAntiSteeringLowestInterestRateDescription");
            dataLoan.sDocMagicAntiSteeringLowestInterestRateInitialRate_rep = GetString("sDocMagicAntiSteeringLowestInterestRateInitialRate");
            dataLoan.sDocMagicAntiSteeringLowestInterestRateDiscountPoints_rep = GetString("sDocMagicAntiSteeringLowestInterestRateDiscountPoints");
            dataLoan.sDocMagicAntiSteeringLowestInterestRateOriginationFee_rep = GetString("sDocMagicAntiSteeringLowestInterestRateOriginationFee");
            dataLoan.sDocMagicAntiSteeringLowestInterestRateOurCompensation_rep = GetString("sDocMagicAntiSteeringLowestInterestRateOurCompensation");
            dataLoan.sDocMagicAntiSteeringLowestInterestRatePrepaymentFeeAmount_rep = GetString("sDocMagicAntiSteeringLowestInterestRatePrepaymentFeeAmount");
            dataLoan.sDocMagicAntiSteeringLowestInterestRateBalloonDue_rep = GetString("sDocMagicAntiSteeringLowestInterestRateBalloonDue");
            dataLoan.sDocMagicAntiSteeringLowestInterestRateIsNegArm = GetInt("sDocMagicAntiSteeringLowestInterestRateIsNegArm") == 1;
            dataLoan.sDocMagicAntiSteeringLowestInterestRateIsDemandFeature = GetInt("sDocMagicAntiSteeringLowestInterestRateIsDemandFeature") == 1;
            dataLoan.sDocMagicAntiSteeringLowestInterestRateIsSharedEquity = GetInt("sDocMagicAntiSteeringLowestInterestRateIsSharedEquity") == 1;
            dataLoan.sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciation = GetInt("sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciation") == 1;

            // Lowest Origination Fees / Discount Points
            dataLoan.sDocMagicAntiSteeringLowestFeeLenderName = GetString("sDocMagicAntiSteeringLowestFeeLenderName");
            dataLoan.sDocMagicAntiSteeringLowestFeeDescription = GetString("sDocMagicAntiSteeringLowestFeeDescription");
            dataLoan.sDocMagicAntiSteeringLowestFeeInitialRate_rep = GetString("sDocMagicAntiSteeringLowestFeeInitialRate");
            dataLoan.sDocMagicAntiSteeringLowestFeeDiscountPoints_rep = GetString("sDocMagicAntiSteeringLowestFeeDiscountPoints");
            dataLoan.sDocMagicAntiSteeringLowestFeeOriginationFee_rep = GetString("sDocMagicAntiSteeringLowestFeeOriginationFee");
            dataLoan.sDocMagicAntiSteeringLowestFeeOurCompensation_rep = GetString("sDocMagicAntiSteeringLowestFeeOurCompensation");
            dataLoan.sDocMagicAntiSteeringLowestFeePrepaymentFeeAmount_rep = GetString("sDocMagicAntiSteeringLowestFeePrepaymentFeeAmount");
            dataLoan.sDocMagicAntiSteeringLowestFeeBalloonDue_rep = GetString("sDocMagicAntiSteeringLowestFeeBalloonDue");
            dataLoan.sDocMagicAntiSteeringLowestFeeIsNegArm = GetInt("sDocMagicAntiSteeringLowestFeeIsNegArm") == 1;
            dataLoan.sDocMagicAntiSteeringLowestFeeIsDemandFeature = GetInt("sDocMagicAntiSteeringLowestFeeIsDemandFeature") == 1;
            dataLoan.sDocMagicAntiSteeringLowestFeeIsSharedEquity = GetInt("sDocMagicAntiSteeringLowestFeeIsSharedEquity") == 1;
            dataLoan.sDocMagicAntiSteeringLowestFeeIsSharedAppreciation = GetInt("sDocMagicAntiSteeringLowestFeeIsSharedAppreciation") == 1;

            // Lowest Rate with No Risky Features
            dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureLenderName = GetString("sDocMagicAntiSteeringLowestRateNoRiskyFeatureLenderName");
            dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureDescription = GetString("sDocMagicAntiSteeringLowestRateNoRiskyFeatureDescription");
            dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureInitialRate_rep = GetString("sDocMagicAntiSteeringLowestRateNoRiskyFeatureInitialRate");
            dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureDiscountPoints_rep = GetString("sDocMagicAntiSteeringLowestRateNoRiskyFeatureDiscountPoints");
            dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureOriginationFee_rep = GetString("sDocMagicAntiSteeringLowestRateNoRiskyFeatureOriginationFee");
            dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureOurCompensation_rep = GetString("sDocMagicAntiSteeringLowestRateNoRiskyFeatureOurCompensation");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // SetResult("x", dataLoan.X);
        }
    }
    public partial class DocMagicLoanOptionsSafeHarborDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new DocMagicLoanOptionsSafeHarborDisclosureServiceItem());
        }
    }
}
