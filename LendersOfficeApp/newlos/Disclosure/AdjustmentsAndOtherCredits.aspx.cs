﻿namespace LendersOfficeApp.newlos.Disclosure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using DataAccess;
    using System.Web.Services;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using los.AdjustmentsAndOtherCreditsSetup;
    using LendersOffice.Migration;
    using LendersOffice.ConfigSystem.Operations;
    using MeridianLink.CommonControls;
    using LendersOffice.ObjLib.TRID2;

    public partial class AdjustmentsAndOtherCredits : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("ThirdParty/ractive-legacy.min.js");
            this.RegisterJsScript("combobox.js");
            this.RegisterCSS("combobox.css");
            this.RegisterJsScript("mask.js");
            this.UseNewFramework = true;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var loan = CPageData.CreateUsingSmartDependency(
                this.LoanID,
                typeof(AdjustmentsAndOtherCredits));
            loan.InitLoad();

            var isUlad2019 = loan.sGseTargetApplicationT == GseTargetApplicationT.Ulad2019;
            this.RegisterJsGlobalVariables("isUlad2019", isUlad2019);


            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowTogglingTheRestrictAdjustmentsAndOtherCreditsCheckbox))
            {
                this.bodyId.Attributes["class"] = "disallowTogglingRestrictCheckbox";
            }

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(loan.sLoanVersionT, LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments))
            {
                this.RegisterJsObjectWithJsonNetSerializer("predefinedAdjustments", PredefinedAdjustment.GetAllAdjustmentsForBroker(this.BrokerID));
            }
            else
            {
                this.bodyId.Attributes["class"] = "versionAtLeast24";
            }

            var model = CreateViewModel(loan);

            this.RegisterJsObjectWithJsonNetSerializer("AdjustmentsAndOtherCreditsData", model);

            // We don't want users who don't have access to the funding folder
            // to see the DFLP checkbox. It was clarified that we do *not* need to
            // prevent users who lack the permission from ever being able to see
            // the value. It is sufficient to just hide the data. GF
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead))
            {
                this.Page.Header.Controls.Add(new PassthroughLiteral() { Text = "<style>.dflp { display: none; } </style>"});
            }
        }

        /// <summary>
        /// Creates the model to be used by the view. Due to the limitations of
        /// CPageData.CreateUsingSmartDependency, this is duplicated on the
        /// service page to avoid unnecessarily hitting the loan tables.
        /// </summary>
        /// <param name="loan">The loan.</param>
        /// <returns>The model.</returns>
        private AdjustmentsAndOtherCreditsViewModel CreateViewModel(CPageData loan)
        {
            bool loanVersionBeyond24 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(loan.sLoanVersionT, LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments);
            bool isTrid2 = loan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017;
            bool trid2ExcludeSalesPriceAndFirstLienLoanAmountCredit = loan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && loan.sLienPosT == E_sLienPosT.Second && loan.sIsOFinNew && loan.sRemain1stMBal > 0;
            bool trid2ExcludeDebtsToBePaidOff = loan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && loan.sLienToPayoffTotDebt == LendersOffice.ObjLib.ResponsibleLien.OtherLienTransaction;
            bool includeInLeCdForThisLien = loan.sLienPosT == E_sLienPosT.Second || (loan.sLienPosT == E_sLienPosT.First && loan.sIsOFinNew && loan.sConcurSubFin > 0);

            return new AdjustmentsAndOtherCreditsViewModel()
            {
                VRoot = Tools.VRoot,
                DisableUserMigrationToAdjustmentsMode = ConstStage.DisableUserMigrationToAdjustmentsMode,
                AdjustmentList = loan.sAdjustmentList,
                AssetAdjustments = loan.sAdjustmentList.AssetAdjustments,
                AdjustmentListWithAssets = loan.sAdjustmentList.GetEnumeratorWithoutSellerCredit(),
                sPropertyTransferD = loan.sPropertyTransferD_rep,
                sPropertyTransferDLckd = loan.sPropertyTransferDLckd,
                ProrationList = loan.sProrationList,
                sPurchasePrice1003 = loan.sPurchasePrice1003_rep,
                sLoads1003LineLFromAdjustments = loan.sLoads1003LineLFromAdjustments,
                sGrossDueFromBorrPersonalProperty = loan.sGrossDueFromBorrPersonalProperty_rep,
                sTRIDClosingDisclosureBorrowerCostsPaidAtClosing = loan.sTRIDClosingDisclosureBorrowerCostsPaidAtClosing_rep,
                sTRIDCashDeposit = loan.sTRIDCashDeposit_rep,
                sFinalLAmt = loan.sFinalLAmt_rep,
                sReductionsDueToSellerExcessDeposit = loan.sReductionsDueToSellerExcessDeposit_rep,
                sTotalSellerPaidClosingCostsPaidAtClosing = loan.sTotalSellerPaidClosingCostsPaidAtClosing_rep,
                sReductionsDueToSellerPayoffOf1stMtgLoan = loan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep,
                sReductionsDueToSellerPayoffOf2ndMtgLoan = loan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep,
                sRefPdOffAmt1003 = loan.sRefPdOffAmt1003_rep,
                sRefPdOffAmt1003Lckd = loan.sRefPdOffAmt1003Lckd,
                sONewFinBal = loan.sONewFinBal_rep,
                sONewFinNetProceeds = loan.sONewFinNetProceeds_rep,
                sONewFinNetProceedsLckd = loan.sONewFinNetProceedsLckd,
                sIsRenovationLoan = loan.sIsRenovationLoan,
                sAltCostLckd = loan.sAltCostLckd,
                sAltCost = loan.sAltCost_rep,
                sTRIDClosingDisclosureAltCostLckd = loan.sTRIDClosingDisclosureAltCostLckd,
                sTRIDClosingDisclosureAltCost = loan.sTRIDClosingDisclosureAltCost_rep,
                sRenoFeesInClosingCosts = loan.sRenoFeesInClosingCosts_rep,
                IncludeAlterationsImprovementsRepairsBreakdown = loan.sIsRenovationLoan && loan.BrokerDB.EnableRenovationLoanSupport,
                sTotCcPbsLocked = loan.sTotCcPbsLocked,
                sTotCcPbs = loan.sTotCcPbs_rep,
                sLandIfAcquiredSeparately1003 = loan.sLandIfAcquiredSeparately1003_rep,
                sOCredit1Desc = loan.sOCredit1Desc,
                sOCredit1Lckd = loan.sOCredit1Lckd,
                sOCredit1Amt = loan.sOCredit1Amt_rep,
                sOCredit2Desc = loan.sOCredit2Desc,
                sOCredit2Amt = loan.sOCredit2Amt_rep,
                sTotEstPp1003Lckd = loan.sTotEstPp1003Lckd,
                sTotEstPp1003 = loan.sTotEstPp1003_rep,
                sOCredit3Desc = loan.sOCredit3Desc,
                sOCredit3Amt = loan.sOCredit3Amt_rep,
                sOCredit4Desc = loan.sOCredit4Desc,
                sOCredit4Amt = loan.sOCredit4Amt_rep,
                sOCredit5Amt = loan.sOCredit5Amt_rep,
                sONewFinCc = loan.sONewFinCc_rep,
                sTotEstCc1003Lckd = loan.sTotEstCc1003Lckd,
                sTotEstCcNoDiscnt1003 = loan.sTotEstCcNoDiscnt1003_rep,
                sLAmtLckd = loan.sLAmtLckd,
                sLAmtCalc = loan.sLAmtCalc_rep,
                sFfUfmip1003Lckd = loan.sFfUfmip1003Lckd,
                sFfUfmip1003 = loan.sFfUfmip1003_rep,
                sFfUfmipFinanced = loan.sFfUfmipFinanced_rep,
                sLDiscnt1003Lckd = loan.sLDiscnt1003Lckd,
                sLDiscnt1003 = loan.sLDiscnt1003_rep,
                sTotTransC = loan.sTotTransC_rep,
                sTransNetCashLckd = loan.sTransNetCashLckd,
                sTransNetCash = loan.sTransNetCash_rep,
                sTotEstPp = loan.sTotEstPp_rep,
                sTotalBorrowerPaidProrations = loan.sTotalBorrowerPaidProrations_rep,
                sTotEstCcNoDiscnt = loan.sTotEstCcNoDiscnt_rep,
                sIsIncludeProrationsInTotPp = loan.sIsIncludeProrationsInTotPp,
                sIsIncludeONewFinCcInTotEstCc = loan.sIsIncludeONewFinCcInTotEstCc,
                predefinedAdjustmentsList = loanVersionBeyond24 ? PredefinedAdjustment.GetAllAdjustmentsForBroker(loan.sBrokerId) : null,
                sIsRestrictAdjustmentsAndOtherCreditsDescriptions = loanVersionBeyond24 ? loan.sIsRestrictAdjustmentsAndOtherCreditsDescriptions : false,
                PredefinedAdjustmentTypeMap = loanVersionBeyond24 ? AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap : null,
                ExcludeSalesPriceAndFirstLienLoanAmountCredit = trid2ExcludeSalesPriceAndFirstLienLoanAmountCredit,
                ExcludeDebtsToBePaidOff = trid2ExcludeDebtsToBePaidOff,
                IsTrid2 = isTrid2,
                IncludeInLeCdForThisLien = isTrid2 && includeInLeCdForThisLien,
                qualifyingBorrowerModel = QualifyingBorrower.RetrieveQualifyingBorrowerModel(loan)
            };
        }
    }
}
