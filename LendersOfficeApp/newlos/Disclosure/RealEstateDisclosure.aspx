<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="RealEstateDisclosure.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Disclosure.RealEstateDisclosure" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>RealEstateDisclosure</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">        
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
  <script language="javascript">
<!--
      var oRolodex = null;  
      function _init() {
          if (null == oRolodex)
              oRolodex = new cRolodex();
      }
//-->
  </script>
	
    <form id="RealEstateDisclosure" method="post" runat="server">
<TABLE id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TR>
    <TD noWrap class="MainRightHeader">California Real Estate Agency Disclosure</TD></TR>
  <TR>
    <TD noWrap style="PADDING-LEFT:10px">
      <TABLE id=Table2 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <td></td>
          <td colspan=2>
			<uc:CFM id="CFM" runat="server" Type="21" CompanyNameField="RealEstateDisclosureCompanyName" StreetAddressField="RealEstateDisclosureStreetAddr" CityField="RealEstateDisclosureCity" StateField="RealEstateDisclosureState" ZipField="RealEstateDisclosureZip" CompanyPhoneField="RealEstateDisclosurePhoneOfCompany" CompanyFaxField="RealEstateDisclosureFaxOfCompany" CompanyLicenseField="RealEstateDisclosureLicenseNumOfCompany"></uc:CFM>
		  </td>
        </TR>
        <tr>
          <TD noWrap class=FieldLabel>Lender/Broker Name&nbsp;</TD>
          
          <TD          
        noWrap><asp:TextBox id=RealEstateDisclosureCompanyName runat="server" Width="255px"></asp:TextBox></TD></TR>
        <TR>
          <TD noWrap class=FieldLabel>Address</TD>
          <TD noWrap><asp:TextBox id=RealEstateDisclosureStreetAddr runat="server" Width="255px"></asp:TextBox></TD></TR>
        <TR>
          <TD noWrap class=FieldLabel>City</TD>
          <TD noWrap><asp:TextBox id=RealEstateDisclosureCity runat="server" Width="160px"></asp:TextBox><ml:StateDropDownList id=RealEstateDisclosureState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=RealEstateDisclosureZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></TD></TR>
        <tr>
		<td class="FieldLabel">Phone</td>
		<td><ml:phonetextbox id="RealEstateDisclosurePhoneOfCompany" runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox></td>
        </tr>
        <tr>
		<td class="FieldLabel">Fax</td>
		<td><ml:phonetextbox id="RealEstateDisclosureFaxOfCompany" runat="server" CssClass="mask" preset="phone" width="120"></ml:phonetextbox></td>
        </tr>
        <TR>
          <TD class=FieldLabel noWrap>&nbsp;</TD>
          <TD noWrap></TD></TR>
        <tr>
          <TD class=FieldLabel noWrap>License Number</TD>
          <TD noWrap><asp:TextBox ID="RealEstateDisclosureLicenseNumOfCompany" Runat="server" Width="100px"></asp:TextBox></TD></TR></TABLE></TD></TR></TABLE>
     </form>
	<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
  </body>
</html>
