<%@ Page language="c#" Codebehind="TXDisclosureMultipleRoles.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Disclosure.TXDisclosureMultipleRoles" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>TXDisclosureMultipleRoles</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">        
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
	
    <form id="TXDisclosureMultipleRoles" method="post" runat="server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
		<td nowrap class="MainRightHeader">Texas Disclosure of Multiple Roles</td>
    </tr>
    <tr>
    <td nowrap style="PADDING-LEFT:10px">
		<table cellpadding="0" cellspacing="2" border="0">
		<tr>
			<td class="FieldLabel">Loan Officer</td>
			<td><asp:TextBox ID="TXDisclosureMultipleRolesLoanOfficer" Runat="server" Width="255px"></asp:TextBox></td>
		</tr>
		</table>
	</td>
	</tr>
    </table>

     </form>
	
  </body>
</html>
