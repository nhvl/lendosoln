﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="WADisclosure.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.WADisclosure" %>

<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title></title>
</head>
<body bgcolor="gainsboro">
  <form id="form1" runat="server">
  <script type="text/javascript">
    function _init() {
    lockField(<%= AspxTools.JsGetElementById(sWAOtherFLckd) %>, <%= AspxTools.JsGetClientIdString(sWAOtherF) %>);
    }
  </script>
  <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
      <td nowrap="nowrap" class="MainRightHeader">Washington State Disclosure</td>
    </tr>
    <tr>
      <td style="padding-left: 10px">
        <table>
          <tr>
            <td>
              <table class="InsetBorder" width="98%">
                <tr>
                  <td class="FieldLabel">Prepared Date</td>
                  <td><ml:DateTextBox ID="sWADisclosurePrepareD" runat="server"  TabIndex="1"/></td>
                </tr>
                <tr>
                  <td class="FieldLabel">Version</td>
                  <td><asp:DropDownList ID="sWADisclosureVersionT" runat="server"  TabIndex="1"/>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="FormTableHeader">Borrower and Property Information</td>
          </tr>
          <tr>
            <td>
              <table class="InsetBorder" width="98%">
                <tr>
                  <td colspan="2" class="FieldLabel">Borrower</td>
                  <td colspan="2" class="FieldLabel">Co-Borrower</td>
                </tr>
                <tr>
                  <td class="FieldLabel">First Name</td>
                  <td><asp:TextBox ID="aBFirstNm" runat="server"  TabIndex="1"/></td>
                  <td class="FieldLabel">First Name</td>
                  <td><asp:TextBox ID="aCFirstNm" runat="server" tabindex="5"/></td>
                </tr>
                <tr>
                  <td class="FieldLabel">Middle Name</td>
                  <td><asp:TextBox ID="aBMidNm" runat="server"  TabIndex="1"/></td>
                  <td class="FieldLabel">Middle Name</td>
                  <td><asp:TextBox ID="aCMidNm" runat="server" tabindex="5"/></td>
                </tr>
                <tr>
                  <td class="FieldLabel">Last Name</td>
                  <td><asp:TextBox ID="aBLastNm" runat="server"  TabIndex="1"/></td>
                  <td class="FieldLabel">Last Name</td>
                  <td><asp:TextBox ID="aCLastNm" runat="server" tabindex="5"/></td>
                </tr>
                <tr>
                  <td class="FieldLabel">Suffix</td>
                  <td><ml:ComboBox ID="aBSuffix" runat="server"  TabIndex="1"></ml:ComboBox></td>
                  <td class="FieldLabel">Suffix</td>
                  <td><ml:ComboBox ID="aCSuffix" runat="server" tabindex="5"></ml:ComboBox></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <table class="InsetBorder">
                <tr>
                  <td class="FieldLabel">Property Address</td>
                  <td><asp:TextBox ID="sSpAddr" runat="server" width="359px" TabIndex="10"/></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><asp:TextBox ID="sSpCity" runat="server" width="258px" TabIndex="10"/>
                  <ml:StateDropDownList ID="sSpState" runat="server"  TabIndex="10"/>
                  <ml:ZipcodeTextBox ID="sSpZip" runat="server" TabIndex="10"/>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="FormTableHeader">Mortgage Information</td>
          </tr>
          <tr>
            <td>
              <table class="InsetBorder" width="98%">
                <tr>
                  <td class="FieldLabel">Mortgage Term</td>
                  <td colspan="2"><asp:TextBox ID="sTerm" runat="server" Width="40" TabIndex="10"/></td>
                </tr>
                <tr>
                  <td class="FieldLabel">Loan Amount</td>
                  <td colspan="2"><ml:MoneyTextBox ID="sFinalLAmt" runat="server" ReadOnly="true"/></td>
                </tr>
                <tr>
                  <td class="FieldLabel">Amort. Type</td>
                  <td colspan="2"><asp:DropDownList ID="sFinMethT" runat="server" onchange="refreshCalculation();" TabIndex="10"/></td>
                </tr>
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2">&nbsp;</td>
                  <td class="FieldLabel" style="width:50%">Monthly Payment</td>
                </tr>
                <tr>
                  <td class="FieldLabel">Initial Rate</td>
                  <td><ml:PercentTextBox ID="sNoteIR" runat="server" onchange="refreshCalculation();" TabIndex="10"/></td>
                  <td><ml:MoneyTextBox ID="sProThisMPmt" runat="server" readonly="true"/></td>
                </tr>
                <tr>
                  <td class="FieldLabel">Fully Indexed Rate</td>
                  <td><ml:PercentTextBox ID="sFullyIndexedR" runat="server" ReadOnly="true" /></td>
                  <td><ml:MoneyTextBox ID="sFullyIndexedMPmt" runat="server" ReadOnly="true" /></td>
                </tr>
                <tr>
                  <td class="FieldLabel">Lifetime Cap</td>
                  <td><ml:PercentTextBox ID="sRLifeCapR" runat="server" ReadOnly="true" /></td>
                  <td><ml:MoneyTextBox ID="sGfeMaxProThisMPmtAndMIns" runat="server" ReadOnly="true" /></td>
                </tr>
                <tr>
                  <td class="FieldLabel" colspan="2">Date of First Interest Rate Adjustment</td>
                  <td><ml:DateTextBox ID="sRAdj1stD" runat="server" ReadOnly="true" /></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="FormTableHeader">Monthly Reserves</td>
          </tr>
          <tr>
            <td>
              <table class="InsetBorder" width="98%">
                <tr>
                  <td class="FieldLabel" colspan="2">Included in monthly payment?</td>
                </tr>
                <tr>
                  <td class="FieldLabel">Real Estate Taxes</td>
                  <td style="width:60%"><asp:CheckBox ID="sIsRealEstateTaxIncludeInMPmt" runat="server"  TabIndex="10"/></td>
                </tr>
                <tr>
                  <td class="FieldLabel" nowrap="nowrap">Homeowners / Hazard Insurance</td>
                  <td><asp:CheckBox ID="sIsHazardInsuranceIncludeInMPmt" runat="server" TabIndex="10"/></td>
                </tr>
                <tr>
                  <td class="FieldLabel">Mortgage Insurance</td>
                  <td><asp:CheckBox ID="sIsMortgageInsuranceIncludeInMPmt" runat="server"  TabIndex="10"/></td>
                </tr>
                <tr>
                  <td class="FieldLabel">Homeowners' Association Dues</td>
                  <td><asp:CheckBox ID="sIsHOAIncludeInMPmt" runat="server"  TabIndex="10"/></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="FormTableHeader">Originator / Broker / Discount Fees</td>
          </tr>
          <tr>
            <td>
            <table class="InsetBorder" width="98%">
              <tr>
                <td class="FieldLabel">Loan Origination Fee</td>
                <td style="width:50%"><ml:MoneyTextBox ID="sLOrigF" runat="server" ReadOnly="true"/> </td>
              </tr>
              <tr>
                <td class="FieldLabel">Broker Fee</td>
                <td><ml:MoneyTextBox ID="sMBrokF" runat="server" ReadOnly="true"/></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap="nowrap">Loan Discount Fee (Points: <ml:PercentTextBox ID="sLDiscntPc" runat="server" onchange="refreshCalculation();" TabIndex="10"/>)</td>
                <td><ml:MoneyTextBox ID="sLDiscnt" runat="server" ReadOnly="true"/></td>
              </tr>
              <tr>
                <td class="FieldLabel">Other Fees <asp:CheckBox ID="sWAOtherFLckd" runat="server" Text="Lock" onclick="refreshCalculation();" TabIndex="10"/></td>
                <td><ml:MoneyTextBox ID="sWAOtherF" runat="server" TabIndex="10"/></td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td class="FormTableHeader">Other Conditions</td>
          </tr>
          <tr>
            <td>
            <table class="InsetBorder" width="98%">
              <tr>
                <td class="FieldLabel">Prepayment Penalty?</td>
                <td style="width:60%"><asp:CheckBox ID="sGfeHavePpmtPenalty" runat="server"  TabIndex="10"/></td>
              </tr>
              <tr>
                <td class="FieldLabel">Balloon Payment?</td>
                <td><asp:CheckBox ID="sGfeIsBalloon" runat="server"/></td>
              </tr>
              <tr>
                <td class="FieldLabel">Locked Rate?</td>
                <td><asp:CheckBox ID="sIsGfeRateLocked" runat="server" Enabled="false"/></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap="nowrap">Reduced Documentation?</td>
                <td><asp:CheckBox ID="sIsWAReduceDoc" runat="server"  TabIndex="10"/></td>
              </tr>
              <tr>
                <td class="FieldLabel">Yield Spread Premium Of</td>
                <td><ml:MoneyTextBox ID="sBrokComp1" runat="server" ReadOnly="true"  width="90" preset="money" /></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
</body>
</html>
