///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
	public partial class GeorgiaDisclosure : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Georgia Disclosure";
            this.PageID = "GeorgiaDisclosure";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CGeorgiaDisclosurePDF);
        }

		protected override void LoadData()
		{
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(GeorgiaDisclosure));
			dataLoan.InitLoad();

			IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.GeorgiaDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
			
			GeorgiaDisclosureCompanyName.Text = broker.CompanyName;

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
