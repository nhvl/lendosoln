﻿namespace LendersOfficeApp.newlos.Disclosure
{
    using System;
    using DataAccess;

    public class WADisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(WADisclosureServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // Set value from webform to CPageData
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.WADisclosure, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PrepareDate_rep = GetString("sWADisclosurePrepareD");
            preparer.Update();

            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBSuffix = GetString("aBSuffix");

            dataApp.aCFirstNm = GetString("aCFirstNm");
            dataApp.aCMidNm = GetString("aCMidNm");
            dataApp.aCLastNm = GetString("aCLastNm");
            dataApp.aCSuffix = GetString("aCSuffix");

            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");

            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sFinMethT = (E_sFinMethT)GetInt("sFinMethT");
            dataLoan.sWADisclosureVersionT = (E_sWADisclosureVersionT)GetInt("sWADisclosureVersionT");

            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sIsRealEstateTaxIncludeInMPmt = GetBool("sIsRealEstateTaxIncludeInMPmt");
            dataLoan.sIsHazardInsuranceIncludeInMPmt = GetBool("sIsHazardInsuranceIncludeInMPmt");
            dataLoan.sIsMortgageInsuranceIncludeInMPmt = GetBool("sIsMortgageInsuranceIncludeInMPmt");
            dataLoan.sIsHOAIncludeInMPmt = GetBool("sIsHOAIncludeInMPmt");

            dataLoan.sLDiscntPc_rep = GetString("sLDiscntPc");
            dataLoan.sWAOtherF_rep = GetString("sWAOtherF");
            dataLoan.sWAOtherFLckd = GetBool("sWAOtherFLckd");
            dataLoan.sGfeHavePpmtPenalty = GetBool("sGfeHavePpmtPenalty");
            dataLoan.sGfeIsBalloon = GetBool("sGfeIsBalloon");
            dataLoan.sIsWAReduceDoc = GetBool("sIsWAReduceDoc");
            dataLoan.sBrokComp1_rep = GetString("sBrokComp1");

        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // Set value from CPageData to webform

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.WADisclosure, E_ReturnOptionIfNotExist.CreateNew);
            SetResult("sWADisclosurePrepareD", preparer.PrepareDate_rep);

            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBSuffix", dataApp.aBSuffix);

            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCSuffix", dataApp.aCSuffix);

            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);

            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sWADisclosureVersionT", dataLoan.sWADisclosureVersionT);

            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            SetResult("sFullyIndexedR", dataLoan.sFullyIndexedR_rep);
            SetResult("sFullyIndexedMPmt", dataLoan.sFullyIndexedMPmt_rep);
            SetResult("sRLifeCapR", dataLoan.sRLifeCapR_rep);
            SetResult("sGfeMaxProThisMPmtAndMIns", dataLoan.sGfeMaxProThisMPmtAndMIns_rep);
            SetResult("sRAdj1stD", dataLoan.sRAdj1stD_rep);
            SetResult("sIsRealEstateTaxIncludeInMPmt", dataLoan.sIsRealEstateTaxIncludeInMPmt);
            SetResult("sIsHazardInsuranceIncludeInMPmt", dataLoan.sIsHazardInsuranceIncludeInMPmt);
            SetResult("sIsMortgageInsuranceIncludeInMPmt", dataLoan.sIsMortgageInsuranceIncludeInMPmt);
            SetResult("sIsHOAIncludeInMPmt", dataLoan.sIsHOAIncludeInMPmt);

            SetResult("sLOrigF", dataLoan.sLOrigF_rep);
            SetResult("sMBrokF", dataLoan.sMBrokF_rep);
            SetResult("sLDiscnt", dataLoan.sLDiscnt_rep);
            SetResult("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            SetResult("sWAOtherF", dataLoan.sWAOtherF_rep);
            SetResult("sWAOtherFLckd", dataLoan.sWAOtherFLckd);
            SetResult("sGfeHavePpmtPenalty", dataLoan.sGfeHavePpmtPenalty);
            SetResult("sGfeIsBalloon", dataLoan.sGfeIsBalloon);
            SetResult("sIsGfeRateLocked", dataLoan.sIsGfeRateLocked);
            SetResult("sIsWAReduceDoc", dataLoan.sIsWAReduceDoc);
            SetResult("sBrokComp1", dataLoan.sBrokComp1_rep);

        }
    }
    public partial class WADisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new WADisclosureServiceItem());
        }
    }


}
