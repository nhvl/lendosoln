///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
	/// <summary>
	/// Summary description for BorrowerCertificationForm.
	/// </summary>
	public partial class BorrowerCertificationForm : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Borrower Certification and Authorization Form";
            this.PageID = "BorrowerCertificationForm";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CBorrowerCertificationPDF);
        }

		protected override void LoadData()
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(BorrowerCertificationForm));
			dataLoan.InitLoad();

			IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.BorrCertification, E_ReturnOptionIfNotExist.ReturnEmptyObject);

			BorrCertificationCompanyName.Text = broker.CompanyName;

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
