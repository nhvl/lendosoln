<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="CreditScoreDisclosure.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Disclosure.CreditScoreDisclosure" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>CreditScoreDisclosure</title>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="RightBackground" style="MARGIN-LEFT:0px">
  <style type="text/css">
      #DecisionScoreTable {
          border-collapse: collapse;
          width: 520px;
      }

      #DecisionScoreTable td {
          white-space: nowrap;
      }

      #aBOtherCreditModelName, #aCOtherCreditModelName {
          width: 180px;
          max-width: 180px;
      }
  </style>
  <script type="text/javascript">
    var oRolodex = null;  
    function _init() {
        if (null == oRolodex)
        {
          oRolodex = new cRolodex();
        }

        var borrowerDecisionSourceDropdown = <%= AspxTools.JsGetElementById(aBDecisionCreditSourceT) %>;
        var coborrowerDecisionSourceDropdown = <%= AspxTools.JsGetElementById(aCDecisionCreditSourceT) %>;

        disableDDL(borrowerDecisionSourceDropdown, !<%= AspxTools.JsGetElementById(aBDecisionCreditSourceTLckd) %>.checked);
        disableDDL(coborrowerDecisionSourceDropdown, !<%= AspxTools.JsGetElementById(aCDecisionCreditSourceTLckd) %>.checked);

        setOtherCreditVisibility(borrowerDecisionSourceDropdown, coborrowerDecisionSourceDropdown);
    }

    function setOtherCreditVisibility(borrowerDecisionSourceDropdown, coborrowerDecisionSourceDropdown)
    {
        var borrowerDecisionSource = $(borrowerDecisionSourceDropdown);
        var coborrowerDecisionSource = $(coborrowerDecisionSourceDropdown);

        var borrowerIsOther = borrowerDecisionSource.val() === ML.OtherCreditSource;
        var coborrowerIsOther = coborrowerDecisionSource.val() === ML.OtherCreditSource;

        setDecisionScoreVisibility(borrowerDecisionSource, 'aBOtherCreditModelName', borrowerIsOther);
        setDecisionScoreVisibility(coborrowerDecisionSource, 'aCOtherCreditModelName', coborrowerIsOther);

        $('tr.other-credit-row').toggle(borrowerIsOther || coborrowerIsOther);
    }

    function setDecisionScoreVisibility(dropdown, otherModelNameId, isOther)
    {
        dropdown.next('input[type="text"]').prop('readonly', isOther ? '' : 'readonly');
        $('#' + otherModelNameId).parent().toggle(isOther);
    }
  </script>
	
    <form id="CreditScoreDisclosure" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 width="100%" border=0>
  <tr>
    <td nowrap class=MainRightHeader>Credit Score Information Disclosure</td></tr>
  <tr>
    <td nowrap>
      <table id=Table2 cellspacing=0 cellpadding=0 border=0 class=InsetBorder width=520>
        <tr>
          <td class=FieldLabel>Lender</td>
          <td colspan=2>
			<uc:CFM id="CFM" runat="server" Type="21" CompanyNameField="CreditDisclosureLenderCompanyName" StreetAddressField="CreditDisclosureLenderStreetAddr" CityField="CreditDisclosureLenderCity" StateField="AppraisalDisclosureState" ZipField="CreditDisclosureLenderZip"></uc:CFM>
          </td>
          </tr>
        <tr>
          <td class=FieldLabel>Name</td>
          <td><asp:TextBox id=CreditDisclosureLenderCompanyName runat="server" width="255px" /></td></tr>
        <tr>
          <td class=FieldLabel>Address</td>
          <td><asp:TextBox id=CreditDisclosureLenderStreetAddr runat="server" Width="255px" /></td></tr>
        <tr>
          <td></td>
          <td><asp:TextBox id=CreditDisclosureLenderCity runat="server" width="160px" /><ml:StateDropDownList id=CreditDisclosureLenderState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=CreditDisclosureLenderZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr></table></td>
        </tr>
        <tr>
        <td>
            <table id="DecisionScoreTable" class="InsetBorder">
                <tr>
                    <td class="FieldLabel">Borrower's Decision Score</td>
                    <td>
                        <asp:CheckBox ID="aBDecisionCreditSourceTLckd" runat="server" onclick="refreshCalculation();" />
                        <asp:DropDownList ID="aBDecisionCreditSourceT" runat="server" onchange="refreshCalculation();" />
                        <asp:TextBox ID="aBDecisionCreditScore" runat="server" Width="50px" />
                    </td>
                    <td>&nbsp;</td>
                    <td class="FieldLabel">Coborrower's Decision Score</td>
                    <td>
                        <asp:CheckBox ID="aCDecisionCreditSourceTLckd" runat="server" onclick="refreshCalculation();" />
                        <asp:DropDownList ID="aCDecisionCreditSourceT" runat="server" onchange="refreshCalculation();" />
                        <asp:TextBox ID="aCDecisionCreditScore" runat="server" Width="50px" />
                    </td>
                </tr>
                <tr class="other-credit-row">
                    <td>&nbsp;</td>
                    <td>
                        <span>
                            <ml:ComboBox ID="aBOtherCreditModelName" runat="server" MaxLength="100" CssClass="combobox"></ml:ComboBox>
                        </span>
                    </td>
                    <td colspan="2">&nbsp;</td>
                    <td>
                        <span>
                            <ml:ComboBox ID="aCOtherCreditModelName" runat="server" MaxLength="100" CssClass="combobox"></ml:ComboBox>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
        </tr>
  <tr>
    <td nowrap>
      <table id=Table3 cellspacing=0 cellpadding=0 border=0 class=InsetBorder width=520>
        <tr>
          <td nowrap class=FieldLabel colspan=2>Credit 
            Bureau - Experian</td></tr>
        <tr>
          <td nowrap class=FieldLabel>Name</td>
          <td 
        nowrap><asp:TextBox id=CreditExperianCompanyName runat="server" width="255px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Address</td>
          <td nowrap><asp:TextBox id=CreditExperianStreetAddr runat="server" Width="255px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap></td>
          <td nowrap><asp:TextBox id=CreditExperianCity runat="server" width="160px"></asp:TextBox><ml:StateDropDownList id=CreditExperianState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=CreditExperianZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Phone</td>
          <td nowrap><ml:PhoneTextBox id=CreditExperianPhoneOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Model Used</td>
          <td 
        nowrap><asp:TextBox id=sExperianModelName runat="server" width="255px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Range of Possible Scores</td>
          <td nowrap class=FieldLabel><asp:TextBox id=sExperianScoreFrom runat="server" width="50px"></asp:TextBox>&nbsp;to 
<asp:TextBox id=sExperianScoreTo runat="server" width="50px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Borrower Score</td>
          <td 
        nowrap><asp:TextBox id=aBExperianScore runat="server" width="50px" onchange="refreshCalculation();" /></td></tr>
        <tr>
        <td nowrap="nowrap" class="FieldLabel">Score Percentile</td>
        <td><asp:TextBox ID="aBExperianPercentile" runat="server"  width="50px"/></td>
        </tr>
        <tr>
          <td class=FieldLabel nowrap>Borrower Created Date</td>
          <td nowrap><ml:DateTextBox id=aBExperianCreatedD runat="server" width="75" preset="date"></ml:DateTextBox></td></tr>
        <tr>
          <td valign=top nowrap class=FieldLabel>Borrower Factors</td>
          <td nowrap><asp:TextBox id=aBExperianFactors runat="server" Width="500px" TextMode="MultiLine" Height="98px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Coborrower Score</td>
          <td 
        nowrap><asp:TextBox id=aCExperianScore runat="server" width="50px" onchange="refreshCalculation();"></asp:TextBox></td></tr>
        <tr>
        <td nowrap="nowrap" class="FieldLabel">Score Percentile</td>
        <td><asp:TextBox ID="aCExperianPercentile" runat="server"  width="50px"/></td>
        </tr>
        
        <tr>
          <td class=FieldLabel nowrap>Coborrower Created Date</td>
          <td nowrap><ml:DateTextBox id=aCExperianCreatedD runat="server" width="75" preset="date"></ml:DateTextBox></td></tr>
        <tr>
          <td valign=top nowrap class=FieldLabel>Coborrower Factors</td>
          <td nowrap><asp:TextBox id=aCExperianFactors runat="server" Width="500px" TextMode="MultiLine" Height="114px"></asp:TextBox></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table id=Table4 cellspacing=0 cellpadding=0 border=0 class=InsetBorder width=520>
        <tr>
          <td nowrap class=FieldLabel colspan=2>Credit 
            Bureau - Trans Union</td></tr>
        <tr>
          <td nowrap class=FieldLabel>Name</td>
          <td 
        nowrap><asp:TextBox id=CreditTransUnionCompanyName runat="server" width="255px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Address</td>
          <td nowrap><asp:TextBox id=CreditTransUnionStreetAddr runat="server" Width="255px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap></td>
          <td nowrap><asp:TextBox id=CreditTransUnionCity runat="server" width="160px"></asp:TextBox><ml:StateDropDownList id=CreditTransUnionState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=CreditTransUnionZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Phone</td>
          <td nowrap><ml:PhoneTextBox id=CreditTransUnionPhoneOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Model Used</td>
          <td 
        nowrap><asp:TextBox id=sTransUnionModelName runat="server" width="255px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Range of Possible Scores</td>
          <td nowrap class=FieldLabel><asp:TextBox id=sTransUnionScoreFrom runat="server" width="50px"></asp:TextBox>&nbsp;to 
<asp:TextBox id=sTransUnionScoreTo runat="server" width="50px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Borrower Score</td>
          <td 
        nowrap><asp:TextBox id=aBTransUnionScore runat="server" width="50px" onchange="refreshCalculation();"></asp:TextBox></td></tr>
        <tr>
        <td nowrap="nowrap" class="FieldLabel">Score Percentile</td>
        <td><asp:TextBox ID="aBTransUnionPercentile" runat="server"  width="50px"/></td>
        </tr>        
        <tr>
          <td class=FieldLabel nowrap>Borrower Created Date</td>
          <td nowrap><ml:DateTextBox id=aBTransUnionCreatedD runat="server" preset="date" width="75"></ml:DateTextBox></td></tr>
        <tr>
          <td valign=top nowrap class=FieldLabel>Borrower Factors</td>
          <td nowrap><asp:TextBox id=aBTransUnionFactors runat="server" Width="500px" TextMode="MultiLine" Height="98px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Coborrower Score</td>
          <td 
        nowrap><asp:TextBox id=aCTransUnionScore runat="server" width="50px" onchange="refreshCalculation();"></asp:TextBox></td></tr>
        <tr>
        <td nowrap="nowrap" class="FieldLabel">Score Percentile</td>
        <td><asp:TextBox ID="aCTransUnionPercentile" runat="server"  width="50px"/></td>
        </tr>          
        <tr>
          <td class=FieldLabel nowrap>Coborrower Created Date</td>
          <td nowrap><ml:DateTextBox id=aCTransUnionCreatedD runat="server" preset="date" width="75"></ml:DateTextBox></td></tr>
        <tr>
          <td valign=top nowrap class=FieldLabel>Coborrower Factors</td>
          <td nowrap><asp:TextBox id=aCTransUnionFactors runat="server" Width="500px" TextMode="MultiLine" Height="114px"></asp:TextBox></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table id=Table5 cellspacing=0 cellpadding=0 border=0 class=InsetBorder width=520>
        <tr>
          <td nowrap class=FieldLabel colspan=2>Credit 
            Bureau - Equifax</td></tr>
        <tr>
          <td nowrap class=FieldLabel>Name</td>
          <td 
        nowrap><asp:TextBox id=CreditEquifaxCompanyName runat="server" width="255px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Address</td>
          <td nowrap><asp:TextBox id=CreditEquifaxStreetAddr runat="server" Width="255px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap></td>
          <td nowrap><asp:TextBox id=CreditEquifaxCity runat="server" width="160px"></asp:TextBox><ml:StateDropDownList id=CreditEquifaxState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=CreditEquifaxZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Phone</td>
          <td nowrap><ml:PhoneTextBox id=CreditEquifaxPhoneOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Model Used</td>
          <td 
        nowrap><asp:TextBox id=sEquifaxModelName runat="server" width="255px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Range of Possible Scores</td>
          <td nowrap class=FieldLabel><asp:TextBox id=sEquifaxScoreFrom runat="server" width="50px"></asp:TextBox>&nbsp; 
            to&nbsp; <asp:TextBox id=sEquifaxScoreTo runat="server" width="50px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Borrower Score</td>
          <td 
        nowrap><asp:TextBox id=aBEquifaxScore runat="server" width="50px" onchange="refreshCalculation();"></asp:TextBox></td></tr>
        <tr>
        <td nowrap="nowrap" class="FieldLabel">Score Percentile</td>
        <td><asp:TextBox ID="aBEquifaxPercentile" runat="server"  width="50px"/></td>
        </tr>         
        <tr>
          <td class=FieldLabel nowrap>Borrower Created Date</td>
          <td nowrap><ml:DateTextBox id=aBEquifaxCreatedD runat="server" preset="date" width="75"></ml:DateTextBox></td></tr>
        <tr>
          <td valign=top nowrap class=FieldLabel>Borrower Factors</td>
          <td nowrap><asp:TextBox id=aBEquifaxFactors runat="server" Width="500px" TextMode="MultiLine" Height="98px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Coborrower Score</td>
          <td 
        nowrap><asp:TextBox id=aCEquifaxScore runat="server" width="50px" onchange="refreshCalculation();"></asp:TextBox></td></tr>
        <tr>
        <td nowrap="nowrap" class="FieldLabel">Score Percentile</td>
        <td><asp:TextBox ID="aCEquifaxPercentile" runat="server"  width="50px"/></td>
        </tr>          
        <tr>
          <td class=FieldLabel nowrap>Coborrower Created Date</td>
          <td nowrap><ml:DateTextBox id=aCEquifaxCreatedD runat="server" preset="date" width="75"></ml:DateTextBox></td></tr>
        <tr>
          <td valign=top nowrap class=FieldLabel>Coborrower Factors</td>
          <td nowrap><asp:TextBox id=aCEquifaxFactors runat="server" Width="500px" TextMode="MultiLine" Height="114px"></asp:TextBox></td></tr></table></td></tr></table>

     </form>
	<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
  </body>
</HTML>
