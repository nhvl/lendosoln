﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="LoanEstimate.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.LoanEstimate" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc" TagName="LoanEstimateFeeListTemplates" Src="LoanEstimateFeeList.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        
        body
        {
            background-color:gainsboro;
        }
        hr
        {
            clear:both;
        }
        .TopDiv
        {
            float:left;
        }
        .TopDivContainer
        {
            white-space:nowrap;
            margin:10px 15px 0px 15px;
            
        }
        .TopDivContainer>table
        {
            width:100%;
        }
        .TopDivContainer>table>tbody>tr>td
        {
            border: solid 1px black;
            vertical-align:top;
            
        }
        
        .TopDivContainer td
        {
            padding:5px 3px;
        }
        .InlineDiv
        {
            padding:5px;
            white-space:nowrap;
            display:inline-block;
        }
        table
        {
            white-space:nowrap;
        }
        
        input[preset='money']
        {
            width:90px;
        }
        
        .SectionDiv
        {
            white-space:nowrap;
            margin: 10px 15px;
            clear:both;
            
            border-style:solid;
            border-width:1px;
        }

        #TimeDiv
        {
            border-style:none;
        }
        
        .GridHeader
        {
            padding:3px;
        }
        
        .LoanTermsDesc
        {
            width:205px;
            font-weight:bold;
            display:inline-block;
        }
        
        .FieldDesc
        {
            width:115px;
            font-weight:bold;
            display:inline-block;
        }
        
        .FieldContainer
        {
            width:110px;
            font-weight:bold;
            display:inline-block;
        }
        
        .InlineDiv .FieldDesc
        {
            width:140px;
            font-weight:bold;
            display:inline-block;
        }
        
        .LoanTermsInfo
        {
            display:inline-block;
        }
        
        .LoanTermOptions
        {
            line-height:16px;
        }
        
        .LoanTermOptions > div
        {
            padding: 5px;
        }
        
        .Month
        {
            width:30px;
        }
        
        Form
        {
            float:left;
            clear:left;
            min-width:100%;
        }
        #AmortSchedule
        {
            border-collapse:collapse;
            width:100%;
            border:solid 1px black;
            margin-bottom:10px;
        }
        #ExpensesTable
        {
            border-collapse:collapse;
            width: 400px;
            border:solid 1px black;
        }
        #AmortSchedule thead td, #ExpensesTable thead td
        {
            text-align:center;
        }
        #AmortSchedule td, #ExpensesTable td
        {
            border:solid 1px black;
        }
        .AmortScheduleContainer
        {
            padding:10px;
        }
        
        #sSpAddr
        {
            width:230px;
        }
        
        #sLoanEstScAvailTillDLckd
        {
            margin-left:60px;
        }
        
        #AmortizationTableDiv
        {
            width:630px;
            height:500px;
            display:none;
        }
        #AmortizationTableFrame
        {
            width:100%;
            height:95%;
        }
        
        .ui-dialog
        {
            border: solid 1px black !important;
        }
        .ui-dialog-titlebar
        {
            background-color:Maroon !important;
            border: 1px solid Maroon !important;
        }
        
        .HalfDiv
        {
            width:49%;
            display:inline-block;
            vertical-align:top;
        }
        .HalfDiv .FieldLabel
        {
            
            width:250px;
        }
        .HalfDiv span
        {
            display:inline-block;
            margin: 3px 5px; 
        }
        .HalfDiv .MoneyContainer, .ThirdDiv .MoneyContainer
        {
            width:100px;
            text-align:right;
        }
        .CostsAtClosing .ThirdDiv
        {
            padding:0px;
        }
        .CostsAtClosing td
        {
            padding:5px;
        }
        .CostsAtClosing .Description
        {
            width:250px;
        }
        .CostsAtClosing .Description
        {
            padding:5px;
            width:250px;
        }
        .IndentedLine
        {
            padding-left:50px;
        }
        .CostsAtClosing
        {
            padding: 5px 5px;
        }
        
        .modal-background {
              position: fixed;
              top: 0; left: 0; width: 100%; height: 100%;
              background-color: rgba(0,0,0,0.5);
              padding: 0.5em;
              text-align: center;
              -moz-box-sizing: border-box; box-sizing: border-box;
            }
            .modal-outer {
              position: relative;
              width: 100%;
              height: 100%;
            }

            .modal {
              position: relative;
              background-color: white;
              padding: 2em;
              box-shadow: 1px 1px 3px rgba(0,0,0,0.1);
              margin: 0 auto;
              display: inline-block;
              max-width: 100%;
              max-height: 100%;
              overflow-y: auto;
              -moz-box-sizing: border-box; box-sizing: border-box;
            }

            .modal-button {
              text-align: center;
              background-color: rgb(70,70,180);
              color: white;
              padding: 0.5em 1em;
              display: inline-block;
              cursor: pointer;
            }

            .modal ul li {
                text-align: left;
            }
            
            .OptionsList
            {
                list-style-type: none;
                display: none;
                background-color: white;
                border-style: solid;
                border-width: 1px;
                z-index:1;
                max-height: 300px;
                overflow-y:auto;
                padding:0px;
                margin:0px;
                margin:0 auto;
                text-align:left;
                position:relative;
            }
            .DDLImg, .ComboboxImg
            {
                width: 21px;
                height: 21px;
                top: 6px;
            }
            input[readonly]:not(.RactiveDDL)
            {
                background-color:lightgrey;
            }
            .QMDDL
            {
                width:200px;
            }
            .RactiveDDLContainer
            {
                overflow:visible;
                height:13px;
                display:inline-block;
            }
            
            input[preset='money']
            {
                width:90px;
                text-align:right;
            }
            input[preset='date']
            {
                width:65px !important;
            }
            select[disabled=""]
            {
                background-color: lightgrey;
            }
            .table-closingcost tfooter td
            {
                text-align:left;
            }
            
           
            /*****Design for TPO Portal Closing Costs *******/
            .div-title {
	            width: auto;
	            font-size:14px;
	            font-family:arial, helvetica, sans-serif; 
	            padding: 3px 0px 3px 9px;
	            margin-top: 5px;
	            /*font-weight: bold;*/
            }
            .div-left {
	            float: left;
	            width: 50%;
	            min-width: 300px;
            }
            .div-right 
            {
	            padding-top: 3px;
	            float: right;
	            width: 40%;
	            margin-left: 10%
            }
            .div-right div{
	            padding: 3px
            }
            .table-closingcost {
	            width: 100%;
	            border-collapse: collapse;
	            font-weight: normal;
            }
            .table-closingcost th {
	            font-weight: normal;
            }
            .table-closingcost td,th {
	            padding: 3px;
	            text-align: center;
	            white-space:nowrap;
            }
            .table-calculation-1 {
	            width: 100%;
            }

            .table-calculation-1 td:nth-child(4) {
	            width: 5%;
            }
            .table-calculation-1 td:nth-child(6) *{
	            width: 100%;
	            min-width: 90px;
            }
            .table-calculation-1 td:nth-child(7) {
	            width: 5%;
            }
            .table-calculation-1 td:nth-child(9) *{
	            width: 100%;
	            min-width: 90px;
            }
            .table-calculation-1 td:nth-child(11) {
	            width: 10%;
            }
            .table-calculation-2 {
	            width: 100%;
	            border-collapse: collapse;
	            border: 1px solid #ebeff2;
            }
            .table-calculation-2 td,th {
	            padding: 3px 3px 3px 10px;
            }
            .image-style {
	            padding: 0px 4px 0px 4px;
            }
            .div-image {
	            padding: 5px 3px 0px 3px;
            }
            .checkbox-hidden {
	            display: none;
            }
            .date-image {
	            vertical-align: middle;
            }

            /**TPO Portal Closing Cost Changes **/
            img
            {
                border-style:none;
            }
            
            .table-closingcost thead
            {
                background-color: #999999;
                color: Black;
            }
            
            #APTable, #AIRTable
            {
                width:100%;
                text-align:left;
            }
            
            #APTable td, #AIRTable td
            {
                width:50%;
            }
            
            #AdjustableDiv
            {
                text-align:center;
                border-style:none;
            }
            
            #AdjustableDiv .HalfDiv
            {
                width: 49%;
                margin: 0px;
            }
            
            .ThirdDiv
            {
                min-width: 32%;
                display: inline-block;
                padding:5px;
                vertical-align:top;
            }
            
            .ThirdDiv span
            {
                display:inline-block;
                width:250px;
                margin: 5px 0px; 
            }
            
            .InBetweenDiv
            {
                border-left:solid 1px black;
                border-right: solid 1px black;
            }
            
            .ContactHeader
            {
                font-weight:bold;
                text-decoration:underline;
            }
            
            .CFMContainer
            {
                display:inline-block;
            }
            
            .CFMLabel
            {
                width:115px !important;
                display:inline-block;
                margin-top:10px;
            }
            
            #After5YrsTable
            {
                display:inline-table;
                border-collapse: collapse;
                margin: 10px;
                width:80%;
                vertical-align:top;
                text-align:left;
            }
            
            #After5YrsTable td
            {
                padding: 3px;
            }
            
            .ComparisonsDiv
            {
                text-align:center;
            }
            
            .ComparisonSpan
            {
                width:250px;
                display:inline-block;
                margin:5px;
            }
            
            .OtherConsiderationsTable
            {
                border-collapse:collapse;
            }
            
            .OtherConsiderationsTable td
            {
                padding: 5px;
                border-style:none;
            }
            
            #sLateChargeBaseDesc
            {
                width:200px;
            }
            
            .smallNum
            {
                width: 50px;
            }
            
            #OtherConsiderationsTable
            {
                white-space:normal;
            }
            
            #OtherConsiderationsTable td.FieldLabel
            {
                white-space:nowrap;
                padding: 5px;
            }
            #OtherConsiderationsTable td
            {
                padding: 5px;
            }
            #dialog-confirm
            {
                text-align: center;
            }
            .LQBDialogBox .ui-button.ui-widget
            {
                border: none;
                padding: 0px;
                float: none;
                font-weight: normal;
            }
            .LQBDialogBox .ui-dialog-buttonset
            {
                text-align: center;
            }
            
            .Hidden
            {
                display:none;
            }
            
            .ContactImg
            {
                vertical-align:middle;
                margin-left:3px;
            }
            .ContactImg:hover
            {
                cursor:pointer;
            }
            .Hidden
            {
                display:none;
            }
            .table-closingcost td, .RactiveDDL, .DDLImg, .ContactImg
            {
                vertical-align:top;
            }
            
            #ClosingCostSummaryContainer
            {
                text-align:center;
            }
            
            .CCDetailsHalfDiv
            {
                width:48%;
                display:inline-block;
                vertical-align:top;
                margin: 10px;
                text-align:left;
            }
            
            .CCDetailsHalfDiv .SectionContainer
            {
                border: solid 1px gray;
                margin: 5px 0px;
            }
            
            .CCDetailsHalfDiv .SectionContainer div
            {
                padding: 0px 10px;
                white-space: normal;
            }
            
            .CCDetailsHalfDiv .SectionContainer div.SectionHeader
            {
                padding: 0px 5px;
                background-color:rgb(8, 83, 147);
                color: White;
                font-weight:bold;
            }
            
            .CCDetailsHalfDiv span
            {
                display:inline-block;
                margin: 5px 0px; 
            }
            
            .BorrowerCCDiv
            {
                margin-left:16px;
                margin-top:10px;
                font-weight:bold;
                text-align:left;
            }
            
            .IndentedSection
            {
                padding-left:50px;
                margin-top:5px;
            }
            .LoanTermOptions div
            {
                vertical-align:top;
            }
            #ClosingCostSummaryContainer .GridAlternatingItem
            {
                background-color:White !important;
                border-top: solid 1px gray;
                border-bottom:solid 1px gray;
            }
            
            #ContactsTable
            {
                border-collapse:collapse;
                width:100%;
            }
            .ContactsTD
            {
                border:solid 1px black;
                border-bottom:none;
                padding:3px;
                vertical-align:top;
            }
            .ContactsLeft
            {
                border-left:none;
            }
            .ContactsRight
            {
                border-right:none;
            }
            
            .ContactsTD input[type='text']
            {
                width:200px;
            }
            
            .CFMContainer table td.FieldLabel
            {
                white-space:normal;
            }
            
            #ClosingCostDetailsSection .MoneyContainer
            {
                float:right;
                width:90px;
                text-align:right;
            }
            .FeeContainer
            {
                overflow:auto;
            }
            
            .CompactContainer
            {
                text-align:center;
            }
            .CompactContainer .HalfDiv
            {
                text-align:left;
            }
            #TotalClosingCostsMinus
            {
                margin:0px;
            }
            #ProjectedPaymentsHeaderRow span.FieldLabel
            {
                display: inline-block;
                padding-left: 10px;
            }
            #ViewAmortizationScheduleBtn
            {
                display:inline-block;
                margin-left: 15px;
            }
            #ProjectedPayments table
            {
                margin-left: 10px;
                background-color: white;
                border: 1px solid black;
                table-layout: fixed;
                border-collapse: collapse;
            }
            #ProjectedPayments table th
            {
                padding: 5px;
                background-color: rgb(8, 83, 147);
                color: White;
                font-weight:bold;
                text-align: center;
                width: 150px;
                border-left: 1px solid black;
                border-bottom: 1px solid black;
            }
            #ProjectedPayments table th:first-of-type
            {
                background-color: rgb(153, 153, 153);
            }
            #ProjectedPayments table td
            {
                text-align: right;
                background-color: white;
            }
            #ProjectedPayments table td.label
            {
                text-align: left;
                padding-left: 5px;
                background-color: gainsboro;
            }
            #ProjectedPayments table td.plus
            {
                border-left: 1px solid black;
                text-align: left;
                padding-left: 10px;
                width: 15px;
            }
            #ProjectedPayments table td.minMax
            {
                text-align: left;
                padding-left: 5px;
                width: 45px;
            }
            #ProjectedPayments table td.interestOnly
            {
                font-style: italic;
                font-weight: bold;
            }
            #ProjectedPayments table tfoot tr:first-of-type
            {
                border-top: 1px solid black;
            }
            #HousingExpensesTable
            {
                margin-left: 10px;
                margin-top: 5px;
                margin-bottom: 5px;
            }
            #HousingExpensesTable>tbody>tr>td:first-of-type
            {
                padding-left: 5px;
            }
            #HousingExpensesTable>tbody>tr>td:nth-of-type(2)
            {
                padding-left: 20px;
                padding-right: 20px;
            }

            .show-sLPurposeT .sTridLPurposeT, .show-sTridLPurposeT .sLPurposeT {
                display: none;
            }

    </style>
</head>
<body>
    <div style="display:none;" id="dialog-confirm" title="Delete fee?">
        <p style="width: 100%">
        </p>
    </div>
    <form id="form1" runat="server">
    
    <script type="text/javascript">

        function selectedDate(cal, date) {
            cal.sel.value = date;
            cal.callCloseHandler();
            if (typeof (updateDirtyBit) == 'function') updateDirtyBit();
            cal.sel.blur();
        } 
        
        function getNonRactiveData() {
            var nonRactiveData = {};

            for (i = 0; i < EditableFields.length; i++) {
                var id = EditableFields[i];
                var el = $("#" + id);
                if (el.length > 0) {
                    if (el.attr("type") == "radio" || el.attr("type") == "checkbox") {
                        EditableFieldsModel[id] = $("#" + id).prop("checked");
                    }
                    else {
                        EditableFieldsModel[id] = $("#" + id).val();
                    }
                }
            }

            //Handle the special cases where control Ids and field names don't match.
            UpdateEditableFieldsIfExists("sTRIDLenderIsLocked", "LenderContact_m_rbManualOverride", EditableFieldsModel, true);
            UpdateEditableFieldsIfExists("sTRIDLoanOfficerIsLocked", "LoanOfficerContact_m_rbManualOverride", EditableFieldsModel, true);
            UpdateEditableFieldsIfExists("sTRIDMortgageBrokerIsLocked", "MortgageBrokerContact_m_rbManualOverride", EditableFieldsModel, true);
            UpdateEditableFieldsIfExists("sTRIDLenderAgentRoleT", "LenderContact_m_officialContactList", EditableFieldsModel, false);
            UpdateEditableFieldsIfExists("sTRIDLoanOfficerAgentRoleT", "LoanOfficerContact_m_officialContactList", EditableFieldsModel, false);
            UpdateEditableFieldsIfExists("sTRIDMortgageBrokerAgentRoleT", "MortgageBrokerContact_m_officialContactList", EditableFieldsModel, false);
            UpdateEditableFieldsIfExists("sGfeIsBalloon", "sGfeIsBalloon_True", EditableFieldsModel, true);
            
            return nonRactiveData;
        }

        function UpdateEditableFieldsIfExists(modelId, fieldId, EditableFieldsModel, isBoolProp)
        {
            var el = $("#" + fieldId);
            if(el.length > 0)
            {
                if(!isBoolProp)
                {
                    EditableFieldsModel[modelId] = el.val();
                }
                else
                {
                    EditableFieldsModel[modelId] = el.prop("checked");
                }
            }
        }

        function setNonRactiveData(model) {
            var nonRactiveData = {};

            for (i = 0; i < EditableFields.length; i++) {
                var id = EditableFields[i];
                var el = $("#" + id);
                if (el.length != 0) {
                    if (el.attr("type") == "radio" || el.attr("type") == "checkbox") {
                        $("#" + id).prop("checked", model[id]);
                    }
                    else if (el.prop("tagName") == "SPAN") {
                        $("#" + id).text(model[id]);
                    }
                    else if (el.prop("tagName") == "INPUT" || el.prop("tagName") == "SELECT") {
                        $("#" + id).val(model[id]);
                    }
                    else
                    {
                        el.text(model[id]);
                    }
                    
                    //Handle special cases where extra behavior is needed.
                    if(id == "sTRIDLoanEstimateTotalClosingCosts")
                    {
                        $("#" + id + "1").text(model[id]);
                        $("#" + id + "2").text(model[id]);
                    }
                    if (id == "sSchedDueD1") {
                        $("#" + id + "2").text(model[id]);
                    }
                    else if (id.indexOf("sTRIDLoanEstimateNoteIRAvailTillDTime_Time") != -1) {
                        el.blur();
                    }
                    else if(id == "sTRIDIsLoanEstimateRateLocked_True")
                    {
                        $("#sTRIDIsLoanEstimateRateLocked_False").prop("checked", !model[id] );
                    }
                    else if(id == "sTRIDLoanEstimateLenderIntendsToServiceLoan_True")
                    {
                        $("#sTRIDLoanEstimateLenderIntendsToServiceLoan_False").prop("checked", !model[id] );
                    }
                    else if(id == "sTRIDLoanEstimateCashToCloseCalcMethodT_Standard")
                    {
                        $("#sTRIDLoanEstimateCashToCloseCalcMethodT_Alternative").prop("checked", !model[id] );
                    }
                }
            }
        
            //handle the special cases
            $("#LenderContact_m_rbManualOverride").prop("checked", model["sTRIDLenderIsLocked"]);
            $("#LoanOfficerContact_m_rbManualOverride").prop("checked", model["sTRIDLoanOfficerIsLocked"]);
            $("#MortgageBrokerContactContact_m_rbManualOverride").prop("checked", model["sTRIDMortgageBrokerIsLocked"]);
            $("#LenderContact_m_officialContactList").val(model["sTRIDLenderAgentRoleT"]);
            $("#LoanOfficerContact_m_officialContactList").val(model["sTRIDLoanOfficerAgentRoleT"]);
            $("#MortgageBroker_m_officialContactList").val(model["sTRIDMortgageBrokerAgentRoleT"]);
            
            return nonRactiveData;
        }
            
        function ArchiveData(){
            if(isDirty()){
                alert('Please save the page before attempting to archive');
            }
            else{
                if (ML && ML.FileHasArchiveInPendingStatusFromCoC) {
                    var msg = <%= AspxTools.JsString(this.FileHasArchiveInPendingStatusFromCoCMessage) %>;

                    if (!confirm(msg)) {
                        return;
                    }
                }

                callWebMethod('RecordDataToArchive', {LoanID: ML.sLId}, function(){ alert('Error'); }, 
                function(m){
                    data = JSON.parse(m.d);
                    if(data.Error)
                    {
                        alert(data.Error);
                    }
                    else
                    {
                        if (data.postArchiveMessage) {
                            alert(data.postArchiveMessage);
                        }

                        var body_url = <%=AspxTools.JsNumeric(E_UrlOption.Page_LoanEstimate) %>;
                        window.top.location = ML.VirtualRoot + '/newlos/LoanApp.aspx' + '?loanid='+ML.sLId+
                        "&appid="+ML.aAppId+'&body_url='+ body_url;
                    }
                });
            }
    }
            
        function lqblog(msg) {
            if (window.console && window.console.log) {
                window.console.log(msg);
            }
        }
        
        function callWebMethod(webMethodName, data, error, success, pageName) {
            if (!pageName) {
                pageName = '../Disclosure/LoanEstimate.aspx/';
            }
            
            var settings = {
                async: false,
                type: 'POST',
                url: pageName + webMethodName,
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: error,
                success: function (d){ checkWebError(d, success); }
            };

            callWebMethodAsync(settings);
        }

        function lockRateLockStatusT()
        {
            var disable = $("#sTRIDLoanEstimateSetLockStatusMethodT").val() == 0 || IsReadOnly == true;

            $("#sTRIDIsLoanEstimateRateLocked_False").prop("disabled", disable);
            $("#sTRIDIsLoanEstimateRateLocked_True").prop("disabled", disable);
        }

        function locksTRIDIsLoanEstimateRateLocked() {
            var ratelock = $("input:radio[name='sTRIDIsLoanEstimateRateLocked']:checked").val() == 'sTRIDIsLoanEstimateRateLocked_False';
            var lockstatus = $("#sTRIDLoanEstimateSetLockStatusMethodT").val() == 0;

            $("#sTRIDLoanEstimateNoteIRAvailTillD").prop("readonly", ratelock || lockstatus || IsReadOnly);

            if(IsReadOnly)
            {
                lockTimeControl("sTRIDLoanEstimateNoteIRAvailTillDTime_Time");
            }
        }

        function EventHandlers() {

            $("#sLenderCaseNumLckd").change(function(){ 
                lockElement("sLenderCaseNum");
                ractive.fire('recalc'); 
            });

            $("#sTRIDIsLoanEstimateRateLocked_True, #sTRIDIsLoanEstimateRateLocked_False, #sTRIDLoanEstimateSetLockStatusMethodT").change(function(event) {
                ractive.fire('recalc');
            });

            $("#sIsPrintTimeForsLoanEstScAvailTillD").change(function(event) {
                var editable = $(this).prop("checked");
                if (!editable) {
                    $("#sLoanEstScAvailTillD_Time").val("12");
                    $("#sLoanEstScAvailTillD_Time_minute").val("00");
                    $("#sLoanEstScAvailTillD_Time_am").val("AM");
                    $("#sLoanEstScAvailTillDTimeZoneT").val(0);
                }
            });

            $("#sSchedDueD1Lckd, #sLoanEstScAvailTillDLckd, #sIsPrintTimeForsLoanEstScAvailTillD,#sEstCloseDLckd, #sConsummationDLckd").change(function() {
                ractive.fire('recalc');
            });

            $("#sEstCloseD, #sConsummationD, #sSchedDueD1, #sTRIDLoanEstimateNoteIRAvailTillD").blur(function() {
                ractive.fire('recalc');
            });
            $("#sEstCloseD, #sConsummationD, #sSchedDueD1, #sTRIDLoanEstimateNoteIRAvailTillD").change(function() {
                date_onblur(null, this)
                $(this).blur();
            });

            $("select:disabled").each(function() {
                var el = $(this);
                el.removeProp('disabled');
                el.mousedown(function(event) {
                    event.preventDefault();
                    $(this).focus();
                });
                el.keydown(function(event) {
                    event.preventDefault();
                    $(this).focus();
                });
                el.css("background-color", "lightgrey");
            });
            
            $("#BorrowerCCLink").click(function(){
                linkMe('../Disclosure/BorrowerClosingCosts.aspx');
                return false;
            });
            
            $("#sTRIDLoanEstimateCashToCloseCalcMethodT_Standard, #sTRIDLoanEstimateCashToCloseCalcMethodT_Alternative").change(function(){
                
                if($("#sTRIDLoanEstimateCashToCloseCalcMethodT_Standard").prop("checked"))
                {
                    
                    $("#sFinalLAmtRow").css("display", "none");
                    $("#EstimatedPayoffsPaymentsRow").css("display", "none");
                    $("#TotalClosingCostsMinus").css("display", "none");
                    
                    $("#ClosingCostsFinancedRow").css("display", "");
                    $("#DownPaymentFundsBorrRow").css("display", "");
                    $("#CashDespotRow").css("display", "");
                    $("#FundsForBorrRow").css("display", "");
                    $("#SellerCreditsRow").css("display", "");
                    $("#AdjustmentsCreditsRow").css("display", "");
                }
                else
                {
                    $("#sFinalLAmtRow").css("display", "");
                    $("#EstimatedPayoffsPaymentsRow").css("display", "");
                    $("#TotalClosingCostsMinus").css("display", "");
                    
                    $("#ClosingCostsFinancedRow").css("display", "none");
                    $("#DownPaymentFundsBorrRow").css("display", "none");
                    $("#CashDespotRow").css("display", "none");
                    $("#FundsForBorrRow").css("display", "none");
                    $("#SellerCreditsRow").css("display", "none");
                    $("#AdjustmentsCreditsRow").css("display", "none");
                    
                }
                
                ractive.fire('recalc');
            });

            $('#ArchiveStatus').change(function() {
                var data, errorHandler, successHandler;

                data = {
                    loanId: ML.sLId,
                    archiveId: $('#ddlArchives').val(),
                    status: $(this).val()
                };

                errorHandler = function () {
                    alert('Unable to set archive status');
                };

                successHandler = function(msg) {
                    var response = JSON.parse(msg.d);
                    if (response.LastDisclosedLoanEstimateArchiveDate) {
                        $('#LastDisclosedLoanEstimateArchiveDateCell').html(response.LastDisclosedLoanEstimateArchiveDate);
                    }

                    if (window.parent 
                            && window.parent.info 
                            && window.parent.info.f_refreshGfeToleranceInfo) {
                        window.parent.info.f_refreshGfeToleranceInfo();
                    }
                };

                callWebMethod('SetArchiveStatus', data, errorHandler, successHandler);
            });
        }
        
        var loanID;
        var IsReadOnly;
        var IsArchivePage;
        $(document).ready(function() {            
            var disablesTRIDLoanEstimateCashToCloseCalcMethodT = (ML.sLoanTransactionInvolvesSeller == "Yes" || (ML.sLoanTransactionInvolvesSeller == "No" && ML.sIsRequireAlternateCashToCloseForNonSellerTransactions)) && !ML.IsLienPos2nd;
            $("input[name='sTRIDLoanEstimateCashToCloseCalcMethodT']").prop("disabled", disablesTRIDLoanEstimateCashToCloseCalcMethodT);

            IsReadOnly = $("#IsReadOnlyField").val() == "True";
            IsArchivePage = $("#IsArchivePageField").val() == "True";

            EventHandlers();

            lockElement("sLenderCaseNum");
            
            $("#OtherConsiderationsTable tr:nth-child(odd)").each(function(){ $(this).addClass("GridAlternatingItem"); });
            $("#OtherConsiderationsTable tr:nth-child(even)").each(function(){ $(this).addClass("GridItem"); });

            $('.Name').change(function () {
                $div = $(this).closest('div');
                $div.find('.AgentId').val(<%=AspxTools.JsString(Guid.Empty) %>);
            });

            window.refreshCalculation = function() { ractive.fire('recalc'); };
            

            window.f_saveMe = function() {
                if(IsArchivePage)
                {
                    return;
                }

                getNonRactiveData();
                var model = ractive.get();
                var data = { loanId: ML.sLId, viewModelJson: JSON.stringify(model), editableFieldsJson: JSON.stringify(EditableFieldsModel) },
                success = false;

                callWebMethod('Save', data, function() {
                    alert('Failed');
                },
            function(d) {
                success = true;
                clearDirty();
            });
                return success;
            }

            window.saveMe = window.f_saveMe;

            var AddressInfo = new Ractive({
                el: 'AddressInfo',
                append: 'false',
                template: '#AddressTemplate',
                data: DataJSON,
                twoway: false
            });

            var ExpenseInfo = new Ractive({
                el: 'ExpensesContainer',
                append: 'false',
                template: '#ExpensesTemplate',
                data: DataJSON,
                twoway: false
            });

            var projectedPayments = new Ractive({
                el: '#ProjectedPayments',
                template: '#ProjectedPaymentsTemplate',
                data: DataJSON,
                twoway: false
            });

            loanID = document.getElementById("loanid").value;
            $("#AmortizationTableFrame").attr("src", "../Forms/AmortizationTable.aspx?loanid=" + loanID);

            $("#AmortizationTableFrame").on('load', function() {

                var contents = $("#AmortizationTableFrame").contents();
                var closeBtn = contents.find("input[value='Close']");
                closeBtn.unbind("click");

                var header = contents.find("h4.page-header");
                header.hide();
                closeBtn.click(closeDialog);
            });

            lockElement("sLoanEstScAvailTillD_Time");
            lockElement("sLoanEstScAvailTillD");
            lockElement("sSchedDueD1");
            lockElement("sEstCloseD");
            lockElement("sConsummationD");
            lockRateLockStatusT();
            locksTRIDIsLoanEstimateRateLocked();
            $("#sTRIDLoanEstimateNoteIRAvailTillDTime_Time_am").attr("noteditable", true);

            var datepickerTransition = function(t) {
                if (t.isIntro) {
                    var node = $(t.node);
                    node.change(
            function() {
                $(this).blur();
                ractive.updateModel();
                ractive.fire('recalc');
            });

                    _initMask(node[0]);
                }
            };

            var numericTransition = function(t, recalc) {
                if (t.isIntro) {
                    var node = $(t.node);
                    if (recalc) {
                        node.change(
            function() {
                ractive.updateModel();
                ractive.fire('recalc');
            });
                    }

                    _initMask(node[0]);
                }
            };

            var ddlTransition = function(t) {

                if (t.isIntro) {
                    var node = $(t.node);
                    var options = $("." + this.data.OptionsId);

                    if (options.attr("sized")) {
                        node.css("width", options.width() - $(".DDLImg").width() - 2);
                    }
                }
            }

            var sectionTransition = function(t) {
                if (t.isIntro) {
                    var node = $(t.node);
                    var section = node.text();

                    var selector = determineShownSections(t.params.hudline, t.params.isSystem, t.params.gfeGroup);
                    node.find("option").not(selector).remove();

                    var numVisible = node.find("option").length;
                    if (numVisible < 2) {
                        node.prop('disabled', true);
                    }
                    if (numVisible < 1) {
                        node.css('display', 'none');
                    }
                }
            }
            
            var initMask = function(o) {
                _initMask(o.node, true);
                o.complete();
            };
        
            Ractive.transitions.initMask = initMask;

            Ractive.transitions.datepickerTransition = datepickerTransition;
            Ractive.transitions.numericTransition = numericTransition;
            Ractive.transitions.ddlIntro = ddlTransition;
            Ractive.transitions.sectionIntro = sectionTransition;

            function highlightLI(el) {
                el.css("background-color", "yellow");
                el.attr("class", "selected");
            }

            function unhighlightLI(el) {
                el.css("background-color", "");
                el.removeAttr("class");
            }

            ractive = new Ractive({
                // The `el` option can be a node, an ID, or a CSS selector.
                el: 'ClosingCostSummaryContainer',
                append: false,

                // We could pass in a string, but for the sake of convenience
                // we're passing the ID of the <script> tag above.
                template: '#ClosingCostSummary',

                // Here, we're passing in some initial data
                data: ClosingCostData,

                init: function() {

                }
            });
            
            ractive.on('onContactHover', function(event){
	    var el = $(event.node);
	    el.attr("src", '../../images/contacts_clicked.png');
	});
	
	ractive.on('onContactMouseOut', function(event){
	    var el = $(event.node);
	    el.attr("src", '../../images/contacts.png');
	});
	
	ractive.on('showAgentPicker', function(event, i, j){
	
	   var el = $(event.node);
       var rolodex = new cRolodex();
    
       var type = el.parent().find("input[type='hidden']").val();

        rolodex.chooseFromRolodex(type, ML.sLId, true, false, function(args){

            if (args.OK == true) {
                    if(args.Clear == true)
                    {
                        
                        $("#ClearAgent").dialog({
                            modal: true,
                            height: 150,
                            width: 350,
                            title: "",
                            dialogClass: "LQBDialogBox",
                            resizable: false,
                            buttons: [
                                {text: "OK",
                                    click: function(){
                                        // Remove Agent from official contacts
                                        var model = ractive.get();
                                        var data = {
                                            loanId: ML.sLId,
                                            viewModelJson: JSON.stringify(model),
                                            recordId: ractive.get("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id"),
                                        }
                                    
                                        callWebMethod("ClearAgent", 
                                            data,
                                            function(){
                                                alert("Error:  Could not remove agent.");
                                            }, 
                                            //On Success
                                            function(m){
                                                model = JSON.parse(m.d);
                                                ractive.set(model);
                                            });
                                        $(this).dialog("close");
                                    }
                                },
                                {text: "Cancel",
                                    click: function(){
                                        $(this).dialog("close");
                                    }
                                }
                            ]
                        }); 
                    }
                    else
                    {
                    var id = "";
                    var populateFromRolodex = false;
                    var agentType = args.AgentType;
                    
                    if(args.RecordId != "" && args.RecordId != <%=AspxTools.JsString(Guid.Empty) %> )
                    {
                        //If they're choosing from an agent record, simply set it's record id as the fee's bene_id.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", args.RecordId);
                        //Update paidTo DDL
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                        //Re-enable beneficiary automation.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                        
                        updateDirtyBit();
                        ractive.fire('recalc');
                    }
                    else
                    {
                        if(args.BrokerLevelAgentID != <%=AspxTools.JsString(Guid.Empty) %>)
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there is a BrokerLevelAgentID, populate using the rolodex and that id
                            
                            id=args.BrokerLevelAgentID;
                            populateFromRolodex = true;  
                        }
                        else
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there isn't a BrokerLevelAgentID, populate using the employee info
                            
                            id=args.EmployeeId;
                            populateFromRolodex = false;  
                        }
                        
                        
                        $("#ConfirmAgent").dialog({
                            modal: true,
                            height: 150,
                            width: 350,
                            title: "",
                            dialogClass: "LQBDialogBox",
                            resizable: false,
                            buttons: [
                                {text: "OK",
                                click: function(){
                                    callWebMethod("CreateAgent", 
                                        {loanId: ML.sLId, id: id, populateFromRolodex: populateFromRolodex, agentType: agentType},
                                        function(){alert("Error:  Could not create a new agent.");}, 
                                        //On Success
                                        function(d){
                                            var m = JSON.parse(d.d);
                                            // Get the recordID and set it for the bene_id
                                            recordId = m.RecordId;
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", recordId);
                                            //Update the fee's paidTo DDL
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                                            //Re-enable beneficiary automation.
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                                            
                                            updateDirtyBit();
                                            ractive.fire('recalc');
                                            
                                        });
                                    $(this).dialog("close");
                                    }
                                },
                                {text: "Cancel",
                                click: function(){
                                    $(this).dialog("close");
                                }}
                            ]
                        });
                    }

                }
            }
        });
	});

            ractive.on('domblur', function(event) {
                var kp = event.keypath;
                //sadly this blur event fires before the mask.js blur event 
                //by using setTimeout we queue up the function until after all the other
                //js code.
                window.setTimeout(function() {
                    ractive.updateModel(kp, true);
                });
            });

            ractive.on('delete', function(event, i, j, desc, amt, orgDesc) {
                var d = desc;
                if (!d)
                {
                    d = orgDesc;
                }
                $('#dialog-confirm').find('p').html('Are you sure you would like to remove the following fee? <br />').append(document.createTextNode(d + ' : ' + amt));
                $('#dialog-confirm').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function()
                        {
                            $(this).dialog("close");
                            updateDirtyBit();

                            var model = ractive.get();
                            var sectionList = model.SectionList;
                            sectionList[i].ClosingCostFeeList.splice(j, 1);
                        },
                        "No": function()
                        {
                            $(this).dialog("close");
                        }
                    },
                    closeOnEscape: false,
                    width: "400",
                    draggable: false,
                    resizable: false,
                    dialogClass: "LQBDialogBox"
                });
            });

            ractive.on('updatePaidBy', function(e, i, j, total) {
                var value = e.node.value;
                var obj = e.context;
                if (value == -1)
                {
                   var model = ractive.get(), 
                   fee = model.SectionList[i].ClosingCostFeeList[j];
                   fee.pmts[0].paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
                   
                   // Add new system payment
                   var newPayment = new Object();
                   newPayment.amt = "";
                   newPayment.ent = 0;
                   newPayment.is_fin = false;
                   newPayment.is_system = true;
                   newPayment.made = false;
                   newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
                   newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
                   newPayment.pmt_dt = "";
                   fee.pmts.push(newPayment);
                   
                   updateDirtyBit();
                   ractive.fire('recalc');
                }
                else if (value == 3)
                {
                    obj.pmts[0].paid_by = value;
                    obj.pmts[0].pmt_at = 1; // Set to 'at closing'
                    ractive.update(e.keypath);
                    ractive.fire('recalc');
                }
                else
                {
                    obj.pmts[0].paid_by = value;
                    ractive.update(e.keypath);
                }
            });

            ractive.on('updatePaidBySplit', function(e)
            {
                var value = e.node.value;
                var obj = e.context;
                if (value == 3)
                {
                    obj.paid_by = value;
                    obj.pmt_at = 1; // Set to 'at closing'
                    ractive.update(e.keypath);
                    ractive.fire('recalc');
                }
                else
                {
                    obj.paid_by = value;
                    ractive.update(e.keypath);
                }
            });
            
            ractive.on('AddSplitPayment', function(event, i, j) {
                updateDirtyBit();

                var model = ractive.get();
                var sectionList = model.SectionList;
                var fee = sectionList[i].ClosingCostFeeList[j];

                var newPayment = new Object();
                newPayment.amt = "";
                newPayment.ent = 0;
                newPayment.is_fin = false;
                newPayment.is_system = false;
                newPayment.made = false;
                newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
                newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
                newPayment.pmt_dt = "";

                fee.pmts.push(newPayment);
                ractive.fire('recalc');
            });

            ractive.on('updateCalc', function(event, i, j) {
                var model = ractive.get();
                var keypath = 'SectionList';
                var o = this.get(keypath);
                var m = new CalculationModal({ data: { fee: o[i].ClosingCostFeeList[j].f, loan: this.get()} });
                m.on('teardown', function() {
                    ractive.fire('recalc');
                    updateDirtyBit();
                });
            });

            ractive.on('recalc', function(event, success) {

                if(IsArchivePage == true)
                {
                    return;
                }

                getNonRactiveData();

                if (!ractive.data.ByPassBgCalcForGfeAsDefault) {
                    window.setTimeout(function() {
                        ractive.set('GfeTilAgentRoleT', $("#CFM_m_officialContactList").val());
                        ractive.set('GfeTilIsLocked', $("#CFM_m_rbManualOverride").prop("checked"));

                        var startRecalc = Date.now(),
                model = ractive.get(),
                data = { loanId: loanID, viewModelJson: JSON.stringify(ractive.get()), editableFieldsJson: JSON.stringify(EditableFieldsModel) },
                fin = function(m) {
                    var load = Date.now();
                    lqblog('Time To Load Data ' + (load - startRecalc) + 'ms.');
                    model = JSON.parse(m.d);
                    var d = Date.now();

                    model.ByPassBgCalcForGfeAsDefault = ractive.data.ByPassBgCalcForGfeAsDefault
                    model.viewT = ractive.data.viewT;

                    ractive.set(model);

                    setNonRactiveData(model.editableFieldsModel);
                    locksTRIDIsLoanEstimateRateLocked();

                    var c = Date.now();
                    lqblog('Time to set data ' + (c - d) + 'ms.');
                },
                error = function(e) {
                    alert('Error');
                };

                        callWebMethod('CalculateData', data, error, fin);
                    });
                }
            });
            
            function GetPaidByDesc(paidby)
            {
                var paidByDesc = "";
                if(paidby == 1)
                {
                    paidByDesc = "borr pd";
                }
                else if(paidby == 2)
                {
                    paidByDesc = "seller";
                }
                else if(paidby == 3)
                {
                    paidByDesc = "borr fin";
                }
                else if(paidby == 4)
                {
                    paidByDesc = "lender";
                }
                else if(paidby == 5)
                {
                    paidByDesc = "broker";
                }
                else if(paidby == 6)
                {
                    paidByDesc = "other";
                }
                
                return paidByDesc;
            }
            
            ractive.on('removePayment', function(e, i, j, k, amt, paidby, desc, orgDesc) {
                var d = desc;
                if (!d)
                {
                    d = orgDesc;
                }
                var paidByDesc = GetPaidByDesc(paidby);
                
                $('#dialog-confirm').find('p').text('Are you sure you would like to remove the ' + amt + ' ' + paidByDesc + ' split payment for the ' + d + '?');
                $('#dialog-confirm').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function()
                        {
                            $(this).dialog("close");
                            updateDirtyBit();
                            var model = ractive.get();
                            var sectionList = model.SectionList;
                            sectionList[i].ClosingCostFeeList[j].pmts.splice(k, 1);
                            ractive.fire('recalc');
                        },
                        "No": function()
                        {
                            $(this).dialog("close");
                        }
                    },
                    closeOnEscape: false,
                    width: "400",
                    draggable: false,
                    resizable: false,
                    dialogClass: "LQBDialogBox"
                });
            });

            ractive.on('addFee', function(event, i) {
                var model = ractive.get();

                var sectionList = model.SectionList;

                var section = sectionList[i],
             sectionName = section.SectionName,
             data = { loanId: loanID, viewModelJson: JSON.stringify(model), sectionName: sectionName };

                callWebMethod('GetAvailableClosingCostFeeTypes', data,

         function() {
             alert('Error');
         },

         function(m) {
             var result = JSON.parse(m.d);
             var y = new FeePickerModal({ data: result });
             y.on('selectFee', function(e, si) {
                 section.ClosingCostFeeList.push(result.FeeListAsBorrowerFees[si]);
                 y.teardown();
                 updateDirtyBit(e.original);
                 $(".RactiveDDL" + i + (section.ClosingCostFeeList.length - 1)).css("width", $(".OptionsList").width() - $(".DDLImg").width() - 2);
                 ractive.fire('recalc');
                 return false;
             });
         });
            });

            ractive.on('showCalendar', function(e, id) {
                return displayCalendar(id);
            });
            var canHideDescriptions = true;
            var currentFeeRow = 0;
            var currentSection = 0;

            ractive.on('onHoverDescriptions', function(event) {
                canHideDescriptions = false;
            });

            ractive.on('onMouseOutDescriptions', function(event) {
                canHideDescriptions = true;
            });

            ractive.on('highlightLI', function(event) {
                var node = $(event.node);
                var allLI = node.parent().find('li');
                unhighlightLI(allLI);
                highlightLI(node);

            });
            
            ractive.on('CanShopChange', function(e, i, j){
                var keypath = 'SectionList.' + i + '.ClosingCostFeeList.' + j + '.did_shop';
                var can_shop = $(e.node).prop('checked');
                if(!can_shop)
                {
                    ractive.set(keypath, false);
                }
            });

            ractive.on('tpClick', function(e, i, j) {
                var model = ractive.get();
                if (!model.SectionList[i].ClosingCostFeeList[j].tp) {
                    model.SectionList[i].ClosingCostFeeList[j].aff = false;
                    ractive.set(model);
                }
            });

            ractive.on('unhighlightLI', function(event) {
                var node = $(event.node);
                unhighlightLI(node);
            });


            ractive.on('selectDDL', function(event, fieldID) {
                var node = $(event.node);
                var text = node.text();

                var td = node.parent().parent();

                td.find(".RactiveDDL").val(node.text());
                td.find("input[type='hidden']").val(node.attr('val'));
                  
                var value = node.attr('val');
                  
                node.parent().hide();
                $("#OptionsContainer").append(node.parent());
                canHideDescriptions = true;
                 
                // Get Agent Id
                var args = new Object();
                args["loanid"] = ML.sLId;
                args["IsArchivePage"] = false;
                args["AgentType"] = value;
                  
                var result = gService.cfpb_utils.call("GetAgentIdByAgentType", args);
                
                if (!result.error) {
                    //Update the fee's Beneficiary ID
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_id", result.value.RecordId);
                    // Update the fee's Beneficiary Description
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_desc", result.value.CompanyName);
                    //Update the fee's paidTo DDL
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene", value);
                    //Re-enable beneficiary automation.
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".disable_bene_auto", false);
                }
                
                updateDirtyBit();
                ractive.fire('recalc');
            });

            ractive.on('reselectInput', function(event) {
                //this is called by the UL itself
                $(event.node).parent().find('input').focus();
            });

            $(".RactiveDDL").css("width", $(".BeneficiaryOptions").width());
            $(".OptionsList").css("width", $(".OptionsList").width() + $(".DDLImg").width() + 2);

        });

        function closeDialog() {
            $("#AmortizationTableDiv").dialog("close");
        }

        
        function showAmortTable() {
            $("#AmortizationTableDiv").dialog({
                width: 630,
                height: 540,
                modal: false,
                title: "Amortization Table",
                resizable: false,
                draggable: false,
            });
        }

        function lockTimeControl(id) {
            var minuteId = id + "_minute";
            var amId = id + "_am";

            $("#" + id).attr("readonly", true);
            $("#" + minuteId).attr("readonly", true);
            $("#" + amId).prop("disabled", true);

            $("#" + id).css("background-color", "lightgrey");
            $("#" + minuteId).css("background-color", "lightgrey");
            $("#" + amId).css("background-color", "lightgrey");
        }

        function lockElement(id) {

            var elem = $("#" + id);
            if (id == "sNMLSPeriodForDaysDelinquentT") {
                id = "sNMLSPeriodForDaysDelinquentLckd";
            }
            else if (id == "sNMLSServicingIntentT") {
                id = "sNMLSServicingIntentLckd";
            }
            else if (id.indexOf("_Time") != -1) {
                id = "sIsPrintTimeForsLoanEstScAvailTillD";
            }
            else {
                id = id + "Lckd";
            }
            var isChecked = $("#" + id).prop("checked");
            var disabled = !isChecked || IsReadOnly;

            if (elem.prop("tagName") == "SELECT") {
                elem.prop("disabled", disabled);

                if (isChecked) {
                    elem.css("background-color", "");
                }
                else {
                    elem.css("background-color", "lightgrey");
                }
            }
            else if (elem.attr("id").indexOf("_Time") != -1) {
                var minuteId = elem.attr("id") + "_minute";
                var amId = elem.attr("id") + "_am";

                elem.attr("readonly", disabled);
                $("#" + minuteId).attr("readonly", disabled);
                $("#" + amId).prop("disabled", disabled);

                if (isChecked) {
                    elem.css("background-color", "");
                    $("#"+minuteId).css("background-color", "");
                    $("#"+amId).css("background-color", "");
                }
                else {
                    elem.css("background-color", "lightgrey");
                    $("#"+minuteId).css("background-color", "lightgrey");
                    $("#" + amId).css("background-color", "lightgrey");
                }

                if (elem.attr("id") == "sLoanEstScAvailTillD_Time") {
                    id = "sLoanEstScAvailTillDTimeZoneT";
                    var el = $("#" + id);
                    
                     if (!disabled) {
                         el.css("background-color", "");
                         el.unbind();
                         el.removeAttr("disabled");
                    }
                     else {
                         el.attr("disabled", "true");
                        el.mousedown(function(event) {
                            event.preventDefault();
                            $(this).focus();
                        });
                        el.keydown(function(event) {
                            event.preventDefault();
                            $(this).focus();
                        });
                        el.css("background-color", "lightgrey");
                    }
                    
                }
                
            }
            else {
                elem.prop("readonly", disabled);
            }
        }
    </script>
    
    <script id="AddressTemplate" type="text/ractive">
                            {{#addresses}}
                                <br/>
                                {{aBNm}} {{#if aBHasSpouse && sameAddr}} and {{aCNm}} {{/if}} <br/>
                                {{aBAddrMail}} <br/>
                                {{aBCityMail}}, {{aBStateMail}} {{aBZipMail}} <br/>
                                
                                {{#if aBHasSpouse && !sameAddr}}
                                    <br/>
                                    {{aCNm}} {{#if aCHasSpouse && sameAddr}} and {{aCNm}} {{/if}} <br/>
                                    {{aCAddrMail}} <br/>
                                    {{aCCityMail}}, {{aCStateMail}} {{aCZipMail}} <br/>
                                {{/if}}
                            {{/addresses}}
    </script>
        
    <script id="ExpensesTemplate" type="text/ractive">
        <table id="ExpensesTable">
            <thead>
                <td class="GridHeader">This estimate includes:</td>
                <td class="GridHeader">Amount</td>
                <td class="GridHeader">In Escrow?</td>
            </thead>
            <tbody>
                {{#HousingExpenses}}
                    <tr>
                        <td>{{expenseName}}</td>
                        <td>{{amount}}</td>
                        <td>{{inEscrow? 'Yes':'No'}}</td>
                    </tr>
                {{/HousingExpenses}}
            </tbody>
        </table>
    </script>
    
    <uc:LoanEstimateFeeListTemplates id="FeeListTemplates" runat="server" />

  <script id="ClosingCostSummary" type="text/ractive">
    <div class="CCDetailsHalfDiv">
        {{#SectionList}}
            {{#if SectionName.indexOf("A -") > -1 || SectionName.indexOf("B -") > -1 || SectionName.indexOf("C -") > -1 }}
                {{>LoanEstimateFeeListSectionWithFees}}
            {{/if}}
        {{/SectionList}}
        {{>LoanEstimateFeeListSectionD}}
    </div>
    <div class="CCDetailsHalfDiv">
        {{#SectionList}}
            {{#if SectionName.indexOf("E -") > -1 || SectionName.indexOf("F -") > -1 || SectionName.indexOf("G -") > -1 || SectionName.indexOf("H -") > -1}}
                {{>LoanEstimateFeeListSectionWithFees}}
            {{/if}}
        {{/SectionList}}
        {{>LoanEstimateFeeListSectionI}}
        {{>LoanEstimateFeeListSectionJ}}
    </div>
  </script>

    <script id="ProjectedPaymentsTemplate" type="text/ractive">
        <table>
            <thead>
                <tr>
                    <th class="GridHeader">Payment Calculation</th>
                    {{#ProjectedPayments}}
                        <th colspan="3">
                            {{#if IsFinalBalloonPayment}}
                                Final Payment
                            {{else}}
                                {{#if StartYear === EndYear}}
                                    Year {{StartYear}}
                                {{else}}
                                    Years {{StartYear}} - {{EndYear}}
                                {{/if}}
                            {{/if}}
                        </th>
                    {{/ProjectedPayments}}
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="label">Principal & Interest</td>
                    {{#ProjectedPayments}}
                        <td class="plus">&nbsp;</td>
                        <td class="dollarAmount">
                            {{PIMin_rep}}
                        </td>
                        <td class="minMax">
                            {{#if PIMin !== PIMax}}
                                min
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                    {{/ProjectedPayments}}
                </tr>
                <tr>
                    <td class="label">&nbsp;</td>
                    {{#ProjectedPayments}}
                        <td class="plus">&nbsp;</td>
                        <td class="dollarAmount">
                            {{#if PIMin !== PIMax}}
                                {{PIMax_rep}} 
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                        <td class="minMax">
                            {{#if PIMin !== PIMax}}
                                max
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                    {{/ProjectedPayments}}
                </tr>
                <tr>
                    <td class="label">&nbsp;</td>
                    {{#ProjectedPayments}}
                        <td class="plus">&nbsp;</td>
                        <td class="interestOnly">
                            {{#if HasInterestOnly}}
                                only interest
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                        <td class="minMax">&nbsp;</td>
                    {{/ProjectedPayments}}
                </tr>
                <tr>
                    <td class="label">Mortgage Insurance</td>
                    {{#ProjectedPayments}}
                        <td class="plus">
                            +
                        </td>
                        <td class="dollarAmount">
                            {{#if MI !== 0}}
                                {{MI_rep}}
                            {{else}}
                                &#151; {{! long dash }}
                            {{/if}}
                        </td>
                        <td class="minMax">&nbsp;</td>
                    {{/ProjectedPayments}}
                </tr>
                <tr>
                    <td class="label">Estimated Escrow</td>
                    {{#ProjectedPayments}}
                        <td class="plus">
                            + 
                        </td>
                        <td class="dollarAmount">
                            {{#if Escrow !== 0}}
                                {{Escrow_rep}}
                            {{else}}
                                &#151; {{! long dash }}
                            {{/if}}
                        </td>
                        <td class="minMax">&nbsp;</td>
                    {{/ProjectedPayments}}
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td rowspan="2" class="label">Estimated Total<br />Monthly Payment</td>
                    {{#ProjectedPayments}}
                        <td class="plus">&nbsp;</td>
                        <td class="dollarAmount">
                            {{PaymentMin_rep}}
                        </td>
                        <td class="minMax">
                            {{#if PaymentMin_rep !== PaymentMax_rep}}
                                min
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                    {{/ProjectedPayments}}
                </tr>
                <tr>
                    {{#ProjectedPayments}}
                        <td class="plus">&nbsp;</td>
                        <td class="dollarAmount">
                            {{#if PaymentMin_rep !== PaymentMax_rep}}
                                {{PaymentMax_rep}}
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                        <td class="minMax">
                            {{#if PaymentMin_rep !== PaymentMax_rep}}
                                max
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                    {{/ProjectedPayments}}
                </tr>
            </tfoot>
        </table>
    </script>
  
  <asp:HiddenField runat="server" ID="IsReadOnlyField" />
  <asp:HiddenField runat="server" ID="IsArchivePageField" />
  <asp:HiddenField runat="server" ID="PendingArchiveDate" />
  <div id="ConfirmAgent" style="display:none;">Selecting this contact will add it to the official contact list.</div>
  <div id="ClearAgent" style="display:none;">This action will remove the contact from the official contact list.</div>
    
    <div id="AmortizationTableDiv">
        <iframe id="AmortizationTableFrame"></iframe>
    </div>
    <div class="MainRightHeader" runat="server" id="PageHeader">Loan Estimate</div>
    
    <div class="SectionDiv FieldLabel" runat="server" id="ArchiveRow">
        
        <input type="button" runat="server" ID="bRecordToArchive" value="Record Loan Estimate data to archive" onclick="ArchiveData()" />
        Last Disclosed Loan Estimate archive:
        <span style="font-weight: normal" id="LastDisclosedLoanEstimateDesc" runat="server"></span>
        <span id="sTolerance10BasisLEArchiveIdSection" runat="server">
            10% Tolerance Calculation Archive: 
            <asp:DropDownList runat="server" ID="sTolerance10BasisLEArchiveId_ddl" onchange="document.getElementById('sTolerance10BasisLEArchiveId').value = this.value;"></asp:DropDownList>
            <asp:HiddenField runat="server" ID="sTolerance10BasisLEArchiveId" />
        </span>
    </div>
    
          <div class="SectionDiv" runat="server" ID="SelectArchiveRow">
              <table>
                  <tr>
                      <td class="FieldLabel">Select Archived Loan Estimate:</td>
                      <td><asp:DropDownList runat="server" ID="ddlArchives" AutoPostBack="true" AlwaysEnable></asp:DropDownList></td>
                  </tr>
                  <tr>
                      <td class="FieldLabel">Selected Archived Loan Estimate Status:</td>
                      <td><asp:DropDownList runat="server" ID="ArchiveStatus" Enabled="false" AlwaysEnable></asp:DropDownList></td>
                  </tr>
                  <tr>
                      <td class="FieldLabel">Last Disclosed Loan Estimate Archive:</td>
                      <td id="LastDisclosedLoanEstimateArchiveDateCell" runat="server"></td>
                  </tr>
              </table>
          </div>

    
    <div class="PageContainer">
        <div class="TopDivContainer">
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="FieldLabel" style="vertical-align:top"><a href="#" onclick="linkMe('../BorrowerInfo.aspx');">Applicants</a></td>
                            <td style="white-space:normal"><div id="AddressInfo"></div></td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Property</td>
                            <td><input type="text" readonly runat="server" id="sSpAddr" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="text" readonly runat="server" id="sSpCity" />
                                <ml:StateDropDownList runat="server" ID="sSpState" disabled />
                                <ml:ZipcodeTextBox runat="server" ID="sSpZip" ReadOnly></ml:ZipcodeTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td id="ValueDesc" class="FieldLabel" readonly runat="server">Sale Price</td>
                            <td>
                                <ml:MoneyTextBox readonly runat="server" ID="sPurchasePrice1003"></ml:MoneyTextBox>
                                <ml:MoneyTextBox readonly runat="server" ID="sApprVal"></ml:MoneyTextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td class="FieldLabel">Term / Due (months)</td>
                            <td><input type="text" class="Month" runat="server" id="sTerm" readonly /> / <input type="text" class="Month" runat="server" id="sDue" readonly /></td>
                        </tr>
                        <tr id="PurposeRow" runat="server">
                            <td class="FieldLabel">
                                <span class="sTridLPurposeT">TRID Loan</span>
                                Purpose
                            </td>
                            <td>
                                <asp:DropDownList runat="server" CssClass="sLPurposeT" disabled ID="sLPurposeT"></asp:DropDownList>
                                <asp:DropDownList runat="server" CssClass="sTridLPurposeT" disabled ID="sTridLPurposeT"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Amortization Type</td>
                            <td><asp:DropDownList runat="server" disabled ID="sFinMethT"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Loan Type</td>
                            <td><asp:DropDownList runat="server" disabled ID="sLT"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Loan ID#</td>
                            <td><input type="checkbox" runat="server" id="sLenderCaseNumLckd" /><input type="text" runat="server" id="sLenderCaseNum" /></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table id="TimeDiv">
                        <tr>
                            <td>
                                <span class="FieldLabel">Rate Locked?</span>
                                <asp:DropDownList runat="server" ID="sTRIDLoanEstimateSetLockStatusMethodT" onchange="lockRateLockStatusT();"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:RadioButton runat="server" ID="sTRIDIsLoanEstimateRateLocked_False" GroupName="sTRIDIsLoanEstimateRateLocked"/>No
                                <asp:RadioButton runat="server" ID="sTRIDIsLoanEstimateRateLocked_True" GroupName="sTRIDIsLoanEstimateRateLocked" />Yes, until
                                <ml:DateTextBox runat="server" ID="sTRIDLoanEstimateNoteIRAvailTillD"></ml:DateTextBox> at
                                <ml:TimeTextBox runat="server" ID="sTRIDLoanEstimateNoteIRAvailTillDTime_Time"></ml:TimeTextBox>
                                <asp:DropDownList runat="server" ID="sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT" noteditable="true"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                            <td>
                                <span class="FieldLabel">Estimated closing costs expiration:</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="checkbox" onclick="lockElement('sLoanEstScAvailTillD')" runat="server" id="sLoanEstScAvailTillDLckd" />
                                <ml:DateTextBox runat="server" ID="sLoanEstScAvailTillD"></ml:DateTextBox> at
                                <input type="checkbox" onclick="lockElement('sLoanEstScAvailTillD_Time')" runat="server" id="sIsPrintTimeForsLoanEstScAvailTillD" />
                                <ml:TimeTextBox runat="server" ID="sLoanEstScAvailTillD_Time"></ml:TimeTextBox>
                                <asp:DropDownList runat="server" disabled ID="sLoanEstScAvailTillDTimeZoneT"></asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </div>
        <div class="LoanTermsSection SectionDiv">
            <div class="GridHeader"> Loan Terms</div>
            
            <div class="LoanTermOptions">
                <div>
                    <span class="FieldLabel FieldDesc"">Total Loan Amount</span>
                    <span class="FieldContainer"><ml:MoneyTextBox runat="server" ID="sFinalLamt" readonly></ml:MoneyTextBox></span>
                    <span class="FieldLabel LoanTermsDesc">Can Increase After Closing?</span>
                    <div class="LoanTermsInfo">
                        <asp:RadioButton ID="sGfeCanLoanBalanceIncrease_False" Checked="true" runat="server" GroupName="sGfeCanLoanBalanceIncrease" Enabled="false" />No
                        <asp:RadioButton ID="sGfeCanLoanBalanceIncrease_True" runat="server" GroupName="sGfeCanLoanBalanceIncrease" Enabled="false" /> Yes, within the first
                        <input type="text" class="Month" readonly runat="server" id="sPmtAdjRecastStop" /> months the balance may increase to a maximum of 
                        <ml:MoneyTextBox ReadOnly runat="server" ID="sGfeMaxLoanBalance"></ml:MoneyTextBox>
                    </div>
                </div>
                <div>
                    <span class="FieldLabel FieldDesc">Interest Rate</span>
                    <span class="FieldContainer"><ml:PercentTextBox runat="server" ID="sTridNoteIR" readonly></ml:PercentTextBox></span>
                    <span class="FieldLabel LoanTermsDesc">Can Increase After Closing?</span>
                    <div class="LoanTermsInfo">
                        <div>
                            <asp:RadioButton ID="sGfeCanRateIncrease_False" Checked="true" runat="server" GroupName="sGfeCanRateIncrease" Enabled="false" />No
                            <asp:RadioButton ID="sGfeCanRateIncrease_True" runat="server" GroupName="sGfeCanRateIncrease" Enabled="false" /> Yes, the interest rate adjusts every
                            <input type="text" class="Month" readonly runat="server" id="sRAdjCapMon" />months starting after the first
                            <input type="text" class="Month" readonly runat="server" id="sRAdj1stCapMon" /> months.
                        </div>
                        <div class="IndentedSection">
                            The maximum possible rate is <ml:PercentTextBox runat="server" ReadOnly ID="sTRIDInterestRateMaxEver"></ml:PercentTextBox> and may apply after the first
                            <input type="text" class="Month" readonly runat="server" id="sInterestRateMaxEverStartAfterMon" /> months.
                        </div>
                    </div>
                </div>
                <div>
                    <span class="FieldLabel FieldDesc">Monthly P&amp;I</span>
                    <span class="FieldContainer"><ml:MoneyTextBox runat="server" ID="sTridProThisMPmt" readonly></ml:MoneyTextBox></span>
                    <span class="FieldLabel LoanTermsDesc">Can Increase After Closing?</span>
                    <div class="LoanTermsInfo">
                        <asp:RadioButton ID="sGfeCanPaymentIncrease_False" Checked="true" runat="server" GroupName="sGfeCanPaymentIncrease" Enabled="false" />No
                        <asp:RadioButton ID="sGfeCanPaymentIncrease_True" runat="server" GroupName="sGfeCanPaymentIncrease" Enabled="false" /> Yes, the monthly P&I can adjust every
                        <input type="text" class="Month" readonly runat="server" id="sTRIDPIPmtSubsequentAdjustFreqMon" />months starting after
                        <input type="text" class="Month" readonly runat="server" id="sPIPmtFirstChangeAfterMon" /> months.
                        
                        
                        <div class="IndentedSection">
                            The maximum possible payment is <ml:MoneyTextBox readonly runat="server" ID="sTRIDPIPmtMaxEver"></ml:MoneyTextBox> and may apply after the first 
                            <input type="text" readonly class="Month" runat="server" id="sPIPmtMaxEverStartAfterMon" /> months.
                        </div>
                        <div class="IndentedSection">
                            The payment is interest only for the first <input type="text" class="Month" readonly runat="server" id="sIOnlyMon" /> months.
                        </div>
                        
                    </div>
                </div>
                <div>
                    <span class="FieldLabel FieldDesc"></span>
                    <span class="FieldContainer"></span>
                    <span class="FieldLabel LoanTermsDesc">This loan has a prepayment penalty?</span>
                    <div class="LoanTermsInfo">
                        <asp:RadioButton ID="sGfeHavePpmtPenalty_False" Checked="true" runat="server" GroupName="sGfeHavePpmtPenalty" Enabled="false" />No
                        <asp:RadioButton ID="sGfeHavePpmtPenalty_True" runat="server" GroupName="sGfeHavePpmtPenalty" Enabled="false" /> Yes, there is a maximum penalty of up to
                        <ml:MoneyTextBox ReadOnly runat="server" ID="sGfeMaxPpmtPenaltyAmt"></ml:MoneyTextBox> if the loan is paid off within the first
                        <div class="IndentedSection">
                            <input type="text" class="Month" readonly runat="server" id="sHardPlusSoftPrepmtPeriodMonths" /> months.
                        </div>
                    </div>
                </div>
                <div>
                    <span class="FieldLabel FieldDesc"></span>
                    <span class="FieldContainer"></span>
                    <span class="FieldLabel LoanTermsDesc">This loan has a balloon payment?</span>
                    <div class="LoanTermsInfo">
                        <asp:RadioButton ID="sGfeIsBalloon_False" Checked="true" runat="server" GroupName="sGfeIsBalloon" />No
                        <asp:RadioButton ID="sGfeIsBalloon_True" runat="server" GroupName="sGfeIsBalloon" /> Yes, a balloon payment of
                        <ml:MoneyTextBox ReadOnly runat="server" ID="sGfeBalloonPmt"></ml:MoneyTextBox> is due in
                        <input type="text" class="Month" readonly runat="server" id="sDue2" /> months.
                    </div>
                </div>
            </div>
        </div>
        
        <div class="SectionDiv">
            <div class="GridHeader">Projected Payments</div>
            <div>
                <div id="ProjectedPaymentsHeaderRow" class="InlineDiv">
                    <span class="FieldLabel">Estimated Closing Date</span>
                    <ml:DateTextBox runat="server" ID="sEstCloseD"></ml:DateTextBox>
                    <input type="checkbox" onclick="lockElement('sEstCloseD')" runat="server" value="Lock" id="sEstCloseDLckd" />

                    <span class="FieldLabel">Per-diem Interest Start Date</span>
                    <ml:DateTextBox runat="server" ID="sConsummationD"></ml:DateTextBox>
                    <input type="checkbox" onclick="lockElement('sConsummationD')" runat="server" value="Lock" id="sConsummationDLckd" />
                    
                    <span class="FieldLabel">1st Payment Date</span>
                    <ml:DateTextBox runat="server" ID="sSchedDueD1"></ml:DateTextBox>
                    <input type="checkbox" onclick="lockElement('sSchedDueD1')" runat="server" value="Lock" id="sSchedDueD1Lckd" />

                    <input type="button" value="View Amortization Schedule" onclick="showAmortTable();" id="ViewAmortizationScheduleBtn" alwaysenable="true" runat="server" />
                </div>

                <div id="ProjectedPayments"></div>

                <table id="HousingExpensesTable">
                    <tr>
                        <td class="align">
                            <a href="#" class="FieldLabel" onclick="linkMe('../Forms/PropHousingExpenses.aspx'); return false;">
                                Estimated Taxes Insurance & Assessments
                            </a>
                        </td>
                        <td class="align">
                            <ml:MoneyTextBox runat="server" ID="sMonthlyNonPINonMIExpenseTotalPITI" ReadOnly="true"></ml:MoneyTextBox> a month
                        </td>
                        <td id="ExpensesContainer">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
            <div id="ClosingCostDetailsSection" class="SectionDiv">
                <div class="GridHeader">Closing Cost Details</div>
                <div class="BorrowerCCDiv">
                    <a href="#" id="BorrowerCCLink">Borrower-Responsible Closing Costs</a>
                </div>
                <div id="ClosingCostSummaryContainer"></div>
                
            </div>
            
            <div class="CompactContainer">
                <div class="HalfDiv">
                    <div class="CostsAtClosingDiv SectionDiv">
                        <div class="GridHeader">Costs at Closing</div>
                        <div>
                            <span class="FieldLabel">Total Loan Costs (A+B+C)</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateTotalLoanCosts"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div>
                            <span class="FieldLabel">+ Total Other Costs (E+F+G+H)</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateTotalOtherCosts"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div>
                            <span class="FieldLabel">- <a href="#" onclick="linkMe('../Forms/LenderCredits.aspx'); return false;">Lender Credits</a></span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateLenderCredits"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <hr />
                        <div>
                            <span class="FieldLabel">Total Closing Costs</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateTotalClosingCosts1"></ml:MoneyTextBox>
                            </span>
                        </div>
                    </div>
                    
                    <div class="CashToCloseDiv SectionDiv">
                        
                        <div class="GridHeader">Cash to Close</div>
                        <div>
                            <span class="FieldLabel">Cash to Close Calculation Method</span> 
                            <span class="MoneyContainer">
                                <asp:RadioButton runat="server" ID="sTRIDLoanEstimateCashToCloseCalcMethodT_Standard" GroupName="sTRIDLoanEstimateCashToCloseCalcMethodT" value="0" /> Standard
                                <asp:RadioButton runat="server" ID="sTRIDLoanEstimateCashToCloseCalcMethodT_Alternative" GroupName="sTRIDLoanEstimateCashToCloseCalcMethodT" value="1" /> Alternative
                            </span>
                        </div>
                        <div id="sFinalLAmtRow" runat="server">
                            <span class="FieldLabel">Total Loan Amount</span> 
                            <span  class="MoneyContainer" >
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sFinalLAmt2"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div>
                            <span class="FieldLabel"><span id="TotalClosingCostsMinus" runat="server">-&nbsp;</span>Total Closing Costs</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateTotalClosingCosts2"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div id="EstimatedPayoffsPaymentsRow" runat="server">
                            <span class="FieldLabel">- Estimated Total Payoffs and Payments</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDTotalPayoffsAndPayments"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div id="ClosingCostsFinancedRow" runat="server">
                            <span class="FieldLabel">- Closing Costs Financed</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateBorrowerFinancedClosingCosts"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div id="DownPaymentFundsBorrRow" runat="server">
                            <span class="FieldLabel">+ Down Payment / Funds from Borrower</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateDownPaymentOrFundsFromBorrower"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div id="CashDespotRow" runat="server">
                            <span class="FieldLabel">- Cash Deposit</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDCashDeposit"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div id="FundsForBorrRow" runat="server">
                            <span class="FieldLabel">- Funds for Borrower</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateFundsForBorrower"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div id="SellerCreditsRow" runat="server">
                            <span class="FieldLabel">- Seller Credits</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateSellerCredits"></ml:MoneyTextBox>
                            </span>
                        </div>
                        
                        <div id="AdjustmentsCreditsRow" runat="server">
                            <span class="FieldLabel">- Adjustments and Other Credits</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateAdjustmentsAndOtherCredits"></ml:MoneyTextBox>
                            </span>
                        </div>
                        
                                <hr />
                        
                        <div>
                            <span class="FieldLabel">Estimated Cash to Close</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDLoanEstimateCashToClose"></ml:MoneyTextBox>
                            </span>
                </div>
                        
            </div>
            
                </div>
                
                <div class="HalfDiv">
                    <div class="SectionDiv">
                    <div class="GridHeader">Adjustable Payment (AP) Table</div>
                    <table runat="server" id="APTable">
                        <tr class="GridItem">
                            <td class="FieldLabel">Interest Only Payments?</td>
                            <td id="IsInterestOnlyPmts" runat="server"></td>
                        </tr>
                        <tr class="GridAlernatingItem">
                            <td class="FieldLabel">Optional Payments?</td>
                            <td id="IsOptionalPmts"></td>
                        </tr>
                        <tr class="GridItem">
                            <td class="FieldLabel">Step Payments?</td>
                            <td id="IsStepPmts" runat="server"></td>
                        </tr>
                        <tr class="GridAlernatingItem">
                            <td class="FieldLabel">Seasonal Payments?</td>
                            <td id="IsSeasonalPmts" runat="server"></td>
                        </tr>
                        <tr class="GridItem">
                            <td class="FieldLabel">Monthly Principal and Interest Payments</td>
                            <td></td>
                        </tr>
                        <tr class="GridAlernatingItem">
                            <td class="IndentedLine">First Change/Amount</td>
                            <td runat="server" id="MIFirstChange"></td>
                        </tr>
                        <tr class="GridItem">
                            <td class="IndentedLine">Subsequent Changes</td>
                            <td runat="server" id="MISubsequentChange"></td>
                        </tr>
                        <tr class="GridAlernatingItem">
                            <td class="IndentedLine">Maximum Payment</td>
                            <td runat="server" id="MIMaximumPayment"></td>
                        </tr>
                    </table>
                </div>
                    <div class="SectionDiv">
                    <div class="GridHeader">Adjustable Interest Rate (AIR) Table</div>
                    <table runat="server" id="AIRTable">
                        <tr class="GridItem">
                            <td class="FieldLabel" id="IndexAndMarginDesc" runat="server"></td>
                            <td id="IndexAndMarginVal" runat="server"></td>
                        </tr>
                        <tr class="GridAlernatingItem">
                            <td class="FieldLabel">Initial Interest Rate</td>
                            <td id="InitialInterestRate"></td>
                        </tr>
                        <tr class="GridItem">
                            <td class="FieldLabel">Minimum / Maximum Interest Rate</td>
                            <td id="MinMaxInterestRate" runat="server"></td>
                        </tr>
                        <tr class="GridAlernatingItem">
                            <td class="FieldLabel">Change Frequency</td>
                                <td id="Td1" runat="server"></td>
                        </tr>
                        <tr class="GridItem">
                            <td class="IndentedLine">First Change</td>
                            <td id="FrequencyFirstChange"></td>
                        </tr>
                        <tr class="GridAlernatingItem">
                            <td class="IndentedLine">Subsequent Change</td>
                            <td id="FrequencySubsequentChange" runat="server"></td>
                        </tr>
                        <tr class="GridItem">
                            <td class="FieldLabel">Limits on Interest Rate Changes</td>
                                <td id="Td2" runat="server"></td>
                        </tr>
                        <tr class="GridAlernatingItem">
                            <td class="IndentedLine">First Change</td>
                            <td id="LimitFirstchange" runat="server"></td>
                        </tr>
                        <tr class="GridItem">
                            <td class="IndentedLine">Subsequent Changes</td>
                            <td id="LimitSubsequentChange" runat="server"></td>
                        </tr>
                    </table>
                </div>
            </div>
            </div>
            
            <div id="AdjustableDiv" class="SectionDiv">
                
                
                
            </div>
            
            <div class="SectionDiv">
                    <div class="GridHeader">Contact Information</div>
                    <table id="ContactsTable">
                        <tr>
                            <td class="ContactsTD ContactsLeft">
                                <div>
                                    <div class="ContactHeader">Lender</div>
                                    <div class="CFMContainer" id="LenderCFMContainer">
                                        <UC:CFM ID="LenderContact" runat="server" />
                                    </div>
                                    <div>
                                        <input type="hidden" runat="server" ID="sTRIDLenderAgentId" class="AgentId"></input>
                                        <span class="FieldLabel CFMLabel">Name</span><asp:TextBox runat="server" ID="sTRIDLenderName" CssClass="Name"></asp:TextBox> <br />
                                        <span class="FieldLabel CFMLabel">Address</span><asp:TextBox runat="server" ID="sTRIDLenderAddr"></asp:TextBox> <br />
                                        <span class="FieldLabel CFMLabel">NMLS #</span><asp:TextBox runat="server" ID="sTRIDLenderNMLSNum"></asp:TextBox> <br />
                                        <span class="FieldLabel CFMLabel">License ID</span><asp:TextBox runat="server" ID="sTRIDLenderLicenseIDNum"></asp:TextBox> <br />
                                        <span class="FieldLabel CFMLabel">Phone</span><ml:PhoneTextBox runat="server" ID="sTRIDLenderPhoneNum"></ml:PhoneTextBox>
                                    </div>
                                </div>
                            </td>
                            <td class="ContactsTD">
                                <div>
                                    <div class="ContactHeader">Loan Officer</div>
                                    <div class="CFMContainer" id="LoanOfficerCFMContainer">
                                        <UC:CFM ID="LoanOfficerContact" runat="server" />
                                    </div>
                                    <div>
                                        <input type="hidden" runat="server" ID="sTRIDLoanOfficerAgentId" class="AgentId"></input>
                                        <span class="FieldLabel CFMLabel">Name</span><asp:TextBox runat="server" ID="sTRIDLoanOfficerName" CssClass="Name"></asp:TextBox> <br />
                                        <span class="FieldLabel CFMLabel">NMLS #</span><asp:TextBox runat="server" ID="sTRIDLoanOfficerNMLSNum"></asp:TextBox> <br />
                                        <span class="FieldLabel CFMLabel">License ID</span><asp:TextBox runat="server" ID="sTRIDLoanOfficerLicenseIDNum"></asp:TextBox> <br />
                                        <span class="FieldLabel CFMLabel">Email</span><asp:TextBox runat="server" ID="sTRIDLoanOfficerEmail"></asp:TextBox> <br />
                                        <span class="FieldLabel CFMLabel">Phone</span><asp:TextBox runat="server" ID="sTRIDLoanOfficerPhone"></asp:TextBox>
                                    </div>
                                </div>
                            </td>
                            <td class="ContactsTD ContactsRight">
                                <div>
                                    <div class="ContactHeader">Mortgage Broker</div>
                                    <div>
                                        <div class="CFMContainer" id="MortgageBrokerCFMContainer">
                                            <UC:CFM ID="MortgageBrokerContact" runat="server" />
                                        </div>
                                        <div>
                                            <input type="hidden" runat="server" ID="sTRIDMortgageBrokerAgentId" class="AgentId"></input>
                                            <span class="FieldLabel CFMLabel">Name</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerName" CssClass="Name"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">NMLS #</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">License ID</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerLicenseIDNum"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table> 
                </div>
                
                <div class="SectionDiv">
                    <div class="GridHeader">Comparisons</div>
                    <div class="ComparisonsDiv">
                    <table id="After5YrsTable">
                            <thead>
                            <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="FieldLabel">After 5 Years:</td>
                            </tr>
                        </thead>
                        <tbody>
                                <tr>
                                    <td class="FieldLabel">Annual Percentage Rate (APR)</td>
                                    <td><ml:PercentTextBox ID="sAPR" ReadOnly runat="server"></ml:PercentTextBox></td>
                                    <td class="FieldLabel">Total paid in principal, interest, mortgage insurance, and loan costs</td>
                                    <td><ml:MoneyTextBox runat="server" ID="sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs" ReadOnly></ml:MoneyTextBox></td>
                            </tr>
                                <tr>
                                    <td class="FieldLabel">Total Interest Percent (TIP)</td>
                                    <td><ml:PercentTextBox ID="sCFPBTotalInterestPercentLE" ReadOnly runat="server"></ml:PercentTextBox></td>
                                    <td class="FieldLabel">Total principal paid</td>
                                    <td><ml:MoneyTextBox runat="server" ID="sTRIDLoanEstimatePrincipalIn5Yrs" ReadOnly></ml:MoneyTextBox></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                
                <div class="SectionDiv">
                    <div class="GridHeader">Other Considerations</div>
                    <table id="OtherConsiderationsTable" style="width:100%">
                        <tr class="">
                            <td class="FieldLabel">Appraisal</td>
                            <td>We may order an appraisal to determine the property's value and charge you for this appraisal. We will promptly give you a copy of any appraisal, even if your loan does not close. You can pay for an additional appraisal for your own use at your own cost.</td>
                        </tr>
                        <tr class="">
                            <td class="FieldLabel">Assumptions</td>
                            <td>
                                If you sell or transfer this property to another person, we <br />
                                <asp:RadioButton Enabled="false" runat="server" ID="sAssumeLT_Allow" GroupName="sAssumeLT" value="True" /> will allow, under certain conditions, this person to assume this loan on the original terms. <br />
                                <asp:RadioButton Enabled="false" runat="server" ID="sAssumeLT_NotAllow" GroupName="sAssumeLT" value="False" />will not allow assumption of this loan on the original terms.
                            </td>
                        </tr>
                        <tr class="" runat="server" id="sReqPropInsRow">
                            <td class="FieldLabel">Homeowner's Insurance</td>
                            <td>This loan requires homeowner's insurance on the property, which you may obtain from a company of your choice that we find acceptable.</td>
                        </tr>
                        <tr class="">
                            <td class="FieldLabel">Late Payments</td>
                            <td>If your payment is more than <asp:TextBox class="smallNum" ReadOnly runat="server" ID="sLateDays"></asp:TextBox> days late, we will charge a late fee of  <ml:PercentTextBox ReadOnly runat="server" ID="sLateChargePc"></ml:PercentTextBox> of <asp:TextBox ReadOnly runat="server" ID="sLateChargeBaseDesc"></asp:TextBox>.</td>
                        </tr>
                        <tr class="">
                            <td class="FieldLabel">Refinance</td>
                            <td>Refinancing this loan will depend on your future financial situation, the property value, and market conditions. You may not be able to refinance this loan.</td>
                        </tr>
                        <tr class="">
                            <td class="FieldLabel">Servicing</td>
                            <td>
                                We intend <br />
                                <asp:RadioButton runat="server" ID="sTRIDLoanEstimateLenderIntendsToServiceLoan_True" GroupName="sTRIDLoanEstimateLenderIntendsToServiceLoan" /> to service your loan. If so, you will make payments to us.<br />
                                <asp:RadioButton runat="server" ID="sTRIDLoanEstimateLenderIntendsToServiceLoan_False" GroupName="sTRIDLoanEstimateLenderIntendsToServiceLoan" />to transfer servicing of your loan.
                            </td>
                        </tr>
                        <tr class="" runat="server" id="liabilityAfterForeclosureRow">
                            <td class="FieldLabel">Liability after Foreclosure</td>
                            <td>Certain State law protections against liability for any deficiency after foreclosure may be lost upon refinancing. The potential consequences of the loss of such protections include the consumer's liability for the unpaid balance. The consumer should consult an attorney for additional information.</td>
                        </tr>
                    </table>
                </div>
            
    </div>
    </form>
</body>
</html>
