///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class EqualCreditServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(EqualCreditServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.ECOA, E_ReturnOptionIfNotExist.CreateNew);

            broker.CompanyName = GetString("ECOACompanyName");
            broker.Update();

            // 06/26/06 mf - We save the ECOA name and address to the contact list,
            // creating a new entry or overwriting the first existing ECOA entry.

            CAgentFields ecoaAgent = dataLoan.GetAgentOfRole(E_AgentRoleT.ECOA, E_ReturnOptionIfNotExist.CreateNew);
            ecoaAgent.CompanyName = GetString("ECOAAgentCompanyName");
            ecoaAgent.StreetAddr = GetString("ECOAAgentStreetAddr");
            ecoaAgent.City = GetString("ECOAAgentCity");
            ecoaAgent.State = GetString("ECOAAgentState");
            ecoaAgent.Zip = GetString("ECOAAgentZip");
            ecoaAgent.Update();

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

	public partial class EqualCreditService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new EqualCreditServiceItem());
        }
	}
}
