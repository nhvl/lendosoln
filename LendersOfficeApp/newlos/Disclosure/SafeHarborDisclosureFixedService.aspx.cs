﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class SafeHarborDisclosureFixedServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(SafeHarborDisclosureFixedServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields SafeHarborFixed = dataLoan.GetPreparerOfForm(E_PreparerFormT.SafeHarborFixed, E_ReturnOptionIfNotExist.CreateNew);
            SafeHarborFixed.PreparerName = GetString("sSafeHarborFixedAgentName");
            SafeHarborFixed.LoanOriginatorIdentifier = GetString("sSafeHarborFixedLoanOriginatorIdentifier");
            SafeHarborFixed.LicenseNumOfAgent = GetString("sSafeHarborFixedAgentLicense");
            SafeHarborFixed.Phone = GetString("sSafeHarborFixedPhone");
            SafeHarborFixed.CompanyName = GetString("sSafeHarborFixedCompanyName");
            SafeHarborFixed.CompanyLoanOriginatorIdentifier = GetString("sSafeHarborFixedCompanyLoanOriginatorIdentifier");
            SafeHarborFixed.LicenseNumOfCompany = GetString("sSafeHarborFixedCompanyLicense");
            SafeHarborFixed.PhoneOfCompany = GetString("sSafeHarborFixedCompanyPhone");
            SafeHarborFixed.FaxOfCompany = GetString("sSafeHarborFixedCompanyFax");
            SafeHarborFixed.StreetAddr = GetString("sSafeHarborFixedCompanyStreetAddr");
            SafeHarborFixed.City = GetString("sSafeHarborFixedCompanyCity");
            SafeHarborFixed.State = GetString("sSafeHarborFixedCompanyState");
            SafeHarborFixed.Zip = GetString("sSafeHarborFixedCompanyZip");
            SafeHarborFixed.AgentRoleT = (E_AgentRoleT)GetInt("CFM_AgentRoleT");
            SafeHarborFixed.IsLocked = GetBool("CFM_IsLocked");
            SafeHarborFixed.Update();

            dataLoan.sDueSafeHarborFixedLowestCost_rep = GetString("sDueSafeHarborFixedLowestCost");
            dataLoan.sDueSafeHarborFixedRecLoan_rep = GetString("sDueSafeHarborFixedRecLoan");
            dataLoan.sDueSafeHarborFixedWithoutRiskyFeatures_rep = GetString("sDueSafeHarborFixedWithoutRiskyFeatures");
            dataLoan.sDueSafeHarborFixedWithRiskyFeatures_rep = GetString("sDueSafeHarborFixedWithRiskyFeatures");
            dataLoan.sEquityCalcSafeHarborFixedLowestCost_rep = GetString("sEquityCalcSafeHarborFixedLowestCost");
            dataLoan.sEquityCalcSafeHarborFixedRecLoan_rep = GetString("sEquityCalcSafeHarborFixedRecLoan");
            dataLoan.sEquityCalcSafeHarborFixedWithoutRiskyFeatures_rep = GetString("sEquityCalcSafeHarborFixedWithoutRiskyFeatures");
            dataLoan.sEquityCalcSafeHarborFixedWithRiskyFeatures_rep = GetString("sEquityCalcSafeHarborFixedWithRiskyFeatures");
            dataLoan.sFinalLAmtSafeHarborFixedLowestCost_rep = GetString("sFinalLAmtSafeHarborFixedLowestCost");
            dataLoan.sFinalLAmtSafeHarborFixedRecLoan_rep = GetString("sFinalLAmtSafeHarborFixedRecLoan");
            dataLoan.sFinalLAmtSafeHarborFixedWithoutRiskyFeatures_rep = GetString("sFinalLAmtSafeHarborFixedWithoutRiskyFeatures");
            dataLoan.sFinalLAmtSafeHarborFixedWithRiskyFeatures_rep = GetString("sFinalLAmtSafeHarborFixedWithRiskyFeatures");
            dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedLowestCost_rep = GetString("sGfeProThisMPmtAndMInsSafeHarborFixedLowestCost");
            dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan_rep = GetString("sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan");
            dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedWithoutRiskyFeatures_rep = GetString("sGfeProThisMPmtAndMInsSafeHarborFixedWithoutRiskyFeatures");
            dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures_rep = GetString("sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures");
            dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost_rep = GetString("sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost");
            dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan_rep = GetString("sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan");
            dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures_rep = GetString("sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures");
            dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures_rep = GetString("sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures");
            dataLoan.sLpTemplateNmSafeHarborFixedLowestCost = GetString("sLpTemplateNmSafeHarborFixedLowestCost");
            dataLoan.sLpTemplateNmSafeHarborFixedRecLoan = GetString("sLpTemplateNmSafeHarborFixedRecLoan");
            dataLoan.sLpTemplateNmSafeHarborFixedWithoutRiskyFeatures = GetString("sLpTemplateNmSafeHarborFixedWithoutRiskyFeatures");
            dataLoan.sLpTemplateNmSafeHarborFixedWithRiskyFeatures = GetString("sLpTemplateNmSafeHarborFixedWithRiskyFeatures");
            dataLoan.sNoteIRSafeHarborFixedLowestCost_rep = GetString("sNoteIRSafeHarborFixedLowestCost");
            dataLoan.sNoteIRSafeHarborFixedRecLoan_rep = GetString("sNoteIRSafeHarborFixedRecLoan");
            dataLoan.sNoteIRSafeHarborFixedWithoutRiskyFeatures_rep = GetString("sNoteIRSafeHarborFixedWithoutRiskyFeatures");
            dataLoan.sNoteIRSafeHarborFixedWithRiskyFeatures_rep = GetString("sNoteIRSafeHarborFixedWithRiskyFeatures");
            dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedLowestCost_rep = GetString("sOriginatorCompensationTotalAmountSafeHarborFixedLowestCost");
            dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan_rep = GetString("sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan");
            dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedWithoutRiskyFeatures_rep = GetString("sOriginatorCompensationTotalAmountSafeHarborFixedWithoutRiskyFeatures");
            dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures_rep = GetString("sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures");
            dataLoan.sSafeHarborFixedBalloonPmntIn7Yrs = GetBool("sSafeHarborFixedBalloonPmntIn7Yrs");
            dataLoan.sSafeHarborFixedDemandFeat = GetBool("sSafeHarborFixedDemandFeat");
            dataLoan.sSafeHarborFixedIntOnlyPmts = GetBool("sSafeHarborFixedIntOnlyPmts");
            dataLoan.sSafeHarborFixedNegAmort = GetBool("sSafeHarborFixedNegAmort");
            dataLoan.sSafeHarborFixedOurRecLowestCosts = GetBool("sSafeHarborFixedOurRecLowestCosts");
            dataLoan.sSafeHarborFixedOurRecOther = GetBool("sSafeHarborFixedOurRecOther");
            dataLoan.sSafeHarborFixedOurRecText = GetString("sSafeHarborFixedOurRecText");
            dataLoan.sSafeHarborFixedOurRecWithoutRiskyFeatures = GetBool("sSafeHarborFixedOurRecWithoutRiskyFeatures");
            dataLoan.sSafeHarborFixedPrepayPenalty = GetBool("sSafeHarborFixedPrepayPenalty");
            dataLoan.sSafeHarborFixedSharedAppreciation = GetBool("sSafeHarborFixedSharedAppreciation");
            dataLoan.sSafeHarborFixedSharedEquity = GetBool("sSafeHarborFixedSharedEquity");
            dataLoan.sTransNetCashSafeHarborFixedLowestCost_rep = GetString("sTransNetCashSafeHarborFixedLowestCost");
            dataLoan.sTransNetCashSafeHarborFixedRecLoan_rep = GetString("sTransNetCashSafeHarborFixedRecLoan");
            dataLoan.sTransNetCashSafeHarborFixedWithoutRiskyFeatures_rep = GetString("sTransNetCashSafeHarborFixedWithoutRiskyFeatures");
            dataLoan.sTransNetCashSafeHarborFixedWithRiskyFeatures_rep = GetString("sTransNetCashSafeHarborFixedWithRiskyFeatures");
            dataLoan.sIncludeSafeHarborFixedWithRiskyFeatures = GetInt("sIncludeSafeHarborFixedWithRiskyFeatures") == 1;
            dataLoan.sIncludeSafeHarborFixedRecLoan = GetInt("sIncludeSafeHarborFixedRecLoan") == 1;
            dataLoan.sSafeHarborFixedOurRecLowestRate = GetBool("sSafeHarborFixedOurRecLowestRate");

            dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures_rep = GetString("sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures");
            dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures_rep = GetString("sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures");
            dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost_rep = GetString("sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost");
            dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan_rep = GetString("sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan");

            switch (GetInt("m_copyToColumn"))
            {
                case 0:
                    dataLoan.sDueSafeHarborFixedWithRiskyFeatures = dataLoan.sDue;
                    dataLoan.sEquityCalcSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sEquityCalc_rep;
                    dataLoan.sFinalLAmtSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sFinalLAmt_rep;
                    dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sGfeProThisMPmtAndMIns_rep;
                    dataLoan.sNoteIRSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sNoteIR_rep;
                    dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sGfeOriginatorCompF_rep;
                    dataLoan.sLpTemplateNmSafeHarborFixedWithRiskyFeatures = dataLoan.sLpTemplateNm;
                    dataLoan.sIncludeSafeHarborFixedWithRiskyFeatures = true;

                    if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
                        dataLoan.sTransNetCashSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sTRIDLoanEstimateCashToClose_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sSumOfTridAFees_rep;
                    }
                    else
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sGfeTotalEstimateSettlementCharge_rep;
                        dataLoan.sTransNetCashSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sTransNetCash_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures_rep = dataLoan.sSumOfGEFA1FeesAndDiscountPoints_rep;
                    }

                    break;
                case 1:
                    dataLoan.sDueSafeHarborFixedWithoutRiskyFeatures = dataLoan.sDue;
                    dataLoan.sEquityCalcSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sEquityCalc_rep;
                    dataLoan.sFinalLAmtSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sFinalLAmt_rep;
                    dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sGfeProThisMPmtAndMIns_rep;
                    dataLoan.sNoteIRSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sNoteIR_rep;
                    dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sGfeOriginatorCompF_rep;
                    dataLoan.sLpTemplateNmSafeHarborFixedWithoutRiskyFeatures = dataLoan.sLpTemplateNm;

                    if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
                        dataLoan.sTransNetCashSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sTRIDLoanEstimateCashToClose_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sSumOfTridAFees_rep;
                    }
                    else
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sGfeTotalEstimateSettlementCharge_rep;
                        dataLoan.sTransNetCashSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sTransNetCash_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures_rep = dataLoan.sSumOfGEFA1FeesAndDiscountPoints_rep;
                    }

                    break;
                case 2:
                    dataLoan.sDueSafeHarborFixedLowestCost = dataLoan.sDue;
                    dataLoan.sEquityCalcSafeHarborFixedLowestCost_rep = dataLoan.sEquityCalc_rep;
                    dataLoan.sFinalLAmtSafeHarborFixedLowestCost_rep = dataLoan.sFinalLAmt_rep;
                    dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedLowestCost_rep = dataLoan.sGfeProThisMPmtAndMIns_rep;
                    dataLoan.sNoteIRSafeHarborFixedLowestCost_rep = dataLoan.sNoteIR_rep;
                    dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedLowestCost_rep = dataLoan.sGfeOriginatorCompF_rep;
                    dataLoan.sLpTemplateNmSafeHarborFixedLowestCost = dataLoan.sLpTemplateNm;

                    if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost_rep = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
                        dataLoan.sTransNetCashSafeHarborFixedLowestCost_rep = dataLoan.sTRIDLoanEstimateCashToClose_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost_rep = dataLoan.sSumOfTridAFees_rep;
                    }
                    else
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost_rep = dataLoan.sGfeTotalEstimateSettlementCharge_rep;
                        dataLoan.sTransNetCashSafeHarborFixedLowestCost_rep = dataLoan.sTransNetCash_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost_rep = dataLoan.sSumOfGEFA1FeesAndDiscountPoints_rep;
                    }

                    break;
                case 3:
                    dataLoan.sDueSafeHarborFixedRecLoan = dataLoan.sDue;
                    dataLoan.sEquityCalcSafeHarborFixedRecLoan_rep = dataLoan.sEquityCalc_rep;
                    dataLoan.sFinalLAmtSafeHarborFixedRecLoan_rep = dataLoan.sFinalLAmt_rep;
                    dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan_rep = dataLoan.sGfeProThisMPmtAndMIns_rep;
                    dataLoan.sNoteIRSafeHarborFixedRecLoan_rep = dataLoan.sNoteIR_rep;
                    dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan_rep = dataLoan.sGfeOriginatorCompF_rep;
                    dataLoan.sLpTemplateNmSafeHarborFixedRecLoan = dataLoan.sLpTemplateNm;
                    dataLoan.sIncludeSafeHarborFixedRecLoan = true;

                    if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan_rep = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
                        dataLoan.sTransNetCashSafeHarborFixedRecLoan_rep = dataLoan.sTRIDLoanEstimateCashToClose_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan_rep = dataLoan.sSumOfTridAFees_rep;
                    }
                    else
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan_rep = dataLoan.sGfeTotalEstimateSettlementCharge_rep;
                        dataLoan.sTransNetCashSafeHarborFixedRecLoan_rep = dataLoan.sTransNetCash_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan_rep = dataLoan.sSumOfGEFA1FeesAndDiscountPoints_rep;
                    }

                    break;
                default:
                    break;
            }
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields SafeHarborFixed = dataLoan.GetPreparerOfForm(E_PreparerFormT.SafeHarborFixed, E_ReturnOptionIfNotExist.CreateNew);
            SetResult("sSafeHarborFixedAgentName", SafeHarborFixed.PreparerName);
            SetResult("sSafeHarborFixedLoanOriginatorIdentifier", SafeHarborFixed.LoanOriginatorIdentifier);
            SetResult("sSafeHarborFixedAgentLicense", SafeHarborFixed.LicenseNumOfAgent);
            SetResult("sSafeHarborFixedPhone", SafeHarborFixed.Phone);
            SetResult("sSafeHarborFixedCompanyName", SafeHarborFixed.CompanyName);
            SetResult("sSafeHarborFixedCompanyLoanOriginatorIdentifier", SafeHarborFixed.CompanyLoanOriginatorIdentifier);
            SetResult("sSafeHarborFixedCompanyLicense", SafeHarborFixed.LicenseNumOfCompany);
            SetResult("sSafeHarborFixedCompanyPhone", SafeHarborFixed.PhoneOfCompany);
            SetResult("sSafeHarborFixedCompanyFax", SafeHarborFixed.FaxOfCompany);
            SetResult("sSafeHarborFixedCompanyStreetAddr", SafeHarborFixed.StreetAddr);
            SetResult("sSafeHarborFixedCompanyCity", SafeHarborFixed.City);
            SetResult("sSafeHarborFixedCompanyState", SafeHarborFixed.State);
            SetResult("sSafeHarborFixedCompanyZip", SafeHarborFixed.Zip);
            SetResult("CFM_AgentRoleT", SafeHarborFixed.AgentRoleT);
            SetResult("CFM_IsLocked", SafeHarborFixed.IsLocked);

            SetResult("sDueSafeHarborFixedLowestCost", dataLoan.sDueSafeHarborFixedLowestCost);
            SetResult("sDueSafeHarborFixedRecLoan", dataLoan.sDueSafeHarborFixedRecLoan);
            SetResult("sDueSafeHarborFixedWithoutRiskyFeatures", dataLoan.sDueSafeHarborFixedWithoutRiskyFeatures);
            SetResult("sDueSafeHarborFixedWithRiskyFeatures", dataLoan.sDueSafeHarborFixedWithRiskyFeatures);
            SetResult("sEquityCalcSafeHarborFixedLowestCost", dataLoan.sEquityCalcSafeHarborFixedLowestCost_rep);
            SetResult("sEquityCalcSafeHarborFixedRecLoan", dataLoan.sEquityCalcSafeHarborFixedRecLoan_rep);
            SetResult("sEquityCalcSafeHarborFixedWithoutRiskyFeatures", dataLoan.sEquityCalcSafeHarborFixedWithoutRiskyFeatures_rep);
            SetResult("sEquityCalcSafeHarborFixedWithRiskyFeatures", dataLoan.sEquityCalcSafeHarborFixedWithRiskyFeatures_rep);
            SetResult("sFinalLAmtSafeHarborFixedLowestCost", dataLoan.sFinalLAmtSafeHarborFixedLowestCost_rep);
            SetResult("sFinalLAmtSafeHarborFixedRecLoan", dataLoan.sFinalLAmtSafeHarborFixedRecLoan_rep);
            SetResult("sFinalLAmtSafeHarborFixedWithoutRiskyFeatures", dataLoan.sFinalLAmtSafeHarborFixedWithoutRiskyFeatures_rep);
            SetResult("sFinalLAmtSafeHarborFixedWithRiskyFeatures", dataLoan.sFinalLAmtSafeHarborFixedWithRiskyFeatures_rep);
            SetResult("sGfeProThisMPmtAndMInsSafeHarborFixedLowestCost", dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedLowestCost_rep);
            SetResult("sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan", dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan_rep);
            SetResult("sGfeProThisMPmtAndMInsSafeHarborFixedWithoutRiskyFeatures", dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedWithoutRiskyFeatures_rep);
            SetResult("sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures", dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures_rep);
            SetResult("sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost_rep);
            SetResult("sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan_rep);
            SetResult("sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures_rep);
            SetResult("sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures_rep);
            SetResult("sLpTemplateNmSafeHarborFixedLowestCost", dataLoan.sLpTemplateNmSafeHarborFixedLowestCost);
            SetResult("sLpTemplateNmSafeHarborFixedRecLoan", dataLoan.sLpTemplateNmSafeHarborFixedRecLoan);
            SetResult("sLpTemplateNmSafeHarborFixedWithoutRiskyFeatures", dataLoan.sLpTemplateNmSafeHarborFixedWithoutRiskyFeatures);
            SetResult("sLpTemplateNmSafeHarborFixedWithRiskyFeatures", dataLoan.sLpTemplateNmSafeHarborFixedWithRiskyFeatures);
            SetResult("sNoteIRSafeHarborFixedLowestCost", dataLoan.sNoteIRSafeHarborFixedLowestCost_rep);
            SetResult("sNoteIRSafeHarborFixedRecLoan", dataLoan.sNoteIRSafeHarborFixedRecLoan_rep);
            SetResult("sNoteIRSafeHarborFixedWithoutRiskyFeatures", dataLoan.sNoteIRSafeHarborFixedWithoutRiskyFeatures_rep);
            SetResult("sNoteIRSafeHarborFixedWithRiskyFeatures", dataLoan.sNoteIRSafeHarborFixedWithRiskyFeatures_rep);
            SetResult("sOriginatorCompensationTotalAmountSafeHarborFixedLowestCost", dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedLowestCost_rep);
            SetResult("sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan", dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan_rep);
            SetResult("sOriginatorCompensationTotalAmountSafeHarborFixedWithoutRiskyFeatures", dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedWithoutRiskyFeatures_rep);
            SetResult("sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures", dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures_rep);
            SetResult("sSafeHarborFixedBalloonPmntIn7Yrs", dataLoan.sSafeHarborFixedBalloonPmntIn7Yrs);
            SetResult("sSafeHarborFixedDemandFeat", dataLoan.sSafeHarborFixedDemandFeat);
            SetResult("sSafeHarborFixedIntOnlyPmts", dataLoan.sSafeHarborFixedIntOnlyPmts);
            SetResult("sSafeHarborFixedNegAmort", dataLoan.sSafeHarborFixedNegAmort);
            SetResult("sSafeHarborFixedOurRecLowestCosts", dataLoan.sSafeHarborFixedOurRecLowestCosts);
            SetResult("sSafeHarborFixedOurRecOther", dataLoan.sSafeHarborFixedOurRecOther);
            SetResult("sSafeHarborFixedOurRecText", dataLoan.sSafeHarborFixedOurRecText);
            SetResult("sSafeHarborFixedOurRecWithoutRiskyFeatures", dataLoan.sSafeHarborFixedOurRecWithoutRiskyFeatures);
            SetResult("sSafeHarborFixedPrepayPenalty", dataLoan.sSafeHarborFixedPrepayPenalty);
            SetResult("sSafeHarborFixedSharedAppreciation", dataLoan.sSafeHarborFixedSharedAppreciation);
            SetResult("sSafeHarborFixedSharedEquity", dataLoan.sSafeHarborFixedSharedEquity);
            SetResult("sTransNetCashSafeHarborFixedLowestCost", dataLoan.sTransNetCashSafeHarborFixedLowestCost_rep);
            SetResult("sTransNetCashSafeHarborFixedRecLoan", dataLoan.sTransNetCashSafeHarborFixedRecLoan_rep);
            SetResult("sTransNetCashSafeHarborFixedWithoutRiskyFeatures", dataLoan.sTransNetCashSafeHarborFixedWithoutRiskyFeatures_rep);
            SetResult("sTransNetCashSafeHarborFixedWithRiskyFeatures", dataLoan.sTransNetCashSafeHarborFixedWithRiskyFeatures_rep);
            SetResult("sIncludeSafeHarborFixedWithRiskyFeatures", dataLoan.sIncludeSafeHarborFixedWithRiskyFeatures ? 1 : 0);
            SetResult("sIncludeSafeHarborFixedRecLoan", dataLoan.sIncludeSafeHarborFixedRecLoan ? 1 : 0);
            SetResult("sSafeHarborFixedOurRecLowestRate", dataLoan.sSafeHarborFixedOurRecLowestRate);

            SetResult("sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures_rep);
            SetResult("sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures_rep);
            SetResult("sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost_rep);
            SetResult("sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan_rep);
        }
    }
    public partial class SafeHarborDisclosureFixedService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new SafeHarborDisclosureFixedServiceItem());
        }
    }
}
