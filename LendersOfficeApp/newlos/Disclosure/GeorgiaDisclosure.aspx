<%@ Page language="c#" Codebehind="GeorgiaDisclosure.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Disclosure.GeorgiaDisclosure" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>GeorgiaDisclosure</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">        
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
	
    <form id="GeorgiaDisclosure" method="post" runat="server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
		<td nowrap class="MainRightHeader">Georgia Disclosure</td>
    </tr>
    <tr>
    <td nowrap style="PADDING-LEFT:10px">
		<table cellpadding="0" cellspacing="2" border="0">
		<tr>
			<td class="FieldLabel">Broker/Lender Name</td>
			<td><asp:TextBox ID="GeorgiaDisclosureCompanyName" Runat="server" Width="255px"></asp:TextBox></td>
		</tr>
		</table>
	</td>
	</tr>
    </table>

     </form>
	
  </body>
</html>
