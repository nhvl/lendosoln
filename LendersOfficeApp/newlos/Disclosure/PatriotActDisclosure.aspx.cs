﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Disclosure
{
    public partial class PatriotActDisclosure : BaseLoanPage
    {

        private class BorrowerInfo
        {
            public string Type { get; set; }
            public string Abbrev { get; set; }
        }

        private BorrowerInfo[] BorrCoborr = 
        {
            new BorrowerInfo() { Type = "Borrower", Abbrev = "B" },
            new BorrowerInfo() { Type = "Coborrower", Abbrev = "C" },
        };

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;

            this.EnableJqueryMigrate = false;

            this.PageTitle = "Patriot Act Disclosure";
            this.PageID = "PatriotActDisclosure";

            this.PDFPrintClass = typeof(LendersOffice.Pdf.CPatriotActPDF);
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, this.GetType().BaseType);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            aCLastFirstNm.Value = dataApp.aCLastFirstNm;
            aCDob_rep.Value = dataApp.aCDob_rep;
            aCAddr_SingleLine.Value = dataApp.aCAddr_SingleLine;

            aBLastFirstNm.Value = dataApp.aBLastFirstNm;
            aBDob_rep.Value = dataApp.aBDob_rep;
            aBAddr_SingleLine.Value = dataApp.aBAddr_SingleLine;

            aBIdT.Value = dataApp.aBIdT.ToString();
            aBIdState.Value = dataApp.aBIdState;
            aBIdNumber.Value = dataApp.aBIdNumber;
            aBIdIssueD.Text = dataApp.aBIdIssueD_rep;
            aBIdExpireD.Text = dataApp.aBIdExpireD_rep;
            aBIdCountry.Value = dataApp.aBIdCountry;
            aBIdGovBranch.Value = dataApp.aBIdGovBranch;
            aBIdOther.Value = dataApp.aBIdOther;

            aB2ndIdT.Value = dataApp.aB2ndIdT.ToString();
            aB2ndIdState.Value = dataApp.aB2ndIdState;
            aB2ndIdNumber.Value = dataApp.aB2ndIdNumber;
            aB2ndIdIssueD.Text = dataApp.aB2ndIdIssueD_rep;
            aB2ndIdExpireD.Text = dataApp.aB2ndIdExpireD_rep;
            aB2ndIdCountry.Value = dataApp.aB2ndIdCountry;
            aB2ndIdGovBranch.Value = dataApp.aB2ndIdGovBranch;
            aB2ndIdOther.Value = dataApp.aB2ndIdOther;
            
            aCIdT.Value = dataApp.aCIdT.ToString();
            aCIdState.Value = dataApp.aCIdState;
            aCIdNumber.Value = dataApp.aCIdNumber;
            aCIdIssueD.Text = dataApp.aCIdIssueD_rep;
            aCIdExpireD.Text = dataApp.aCIdExpireD_rep;
            aCIdCountry.Value = dataApp.aCIdCountry;
            aCIdGovBranch.Value = dataApp.aCIdGovBranch;
            aCIdOther.Value = dataApp.aCIdOther;

            aC2ndIdT.Value = dataApp.aC2ndIdT.ToString();
            aC2ndIdState.Value = dataApp.aC2ndIdState;
            aC2ndIdNumber.Value = dataApp.aC2ndIdNumber;
            aC2ndIdIssueD.Text = dataApp.aC2ndIdIssueD_rep;
            aC2ndIdExpireD.Text = dataApp.aC2ndIdExpireD_rep;
            aC2ndIdCountry.Value = dataApp.aC2ndIdCountry;
            aC2ndIdGovBranch.Value = dataApp.aC2ndIdGovBranch;
            aC2ndIdOther.Value = dataApp.aC2ndIdOther;
            
            aIdResolution.Value = dataApp.aIdResolution;
            
            var Preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.PatriotActDisclosure, 
                                                      E_ReturnOptionIfNotExist.CreateNew);
            PreparerName.Value = Preparer.PreparerName;
            PrepareDate.Text = Preparer.PrepareDate_rep;
        }
    }
}
