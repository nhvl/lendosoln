﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class SafeHarborDisclosureAdjustableServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(SafeHarborDisclosureAdjustableServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields safeHarborARM = dataLoan.GetPreparerOfForm(E_PreparerFormT.SafeHarborARM, E_ReturnOptionIfNotExist.CreateNew);
            safeHarborARM.PreparerName = GetString("sSafeHarborARMAgentName");
            safeHarborARM.LoanOriginatorIdentifier = GetString("sSafeHarborARMLoanOriginatorIdentifier");
            safeHarborARM.LicenseNumOfAgent = GetString("sSafeHarborARMAgentLicense");
            safeHarborARM.Phone = GetString("sSafeHarborARMPhone");
            safeHarborARM.CompanyName = GetString("sSafeHarborARMCompanyName");
            safeHarborARM.CompanyLoanOriginatorIdentifier = GetString("sSafeHarborARMCompanyLoanOriginatorIdentifier");
            safeHarborARM.LicenseNumOfCompany = GetString("sSafeHarborARMCompanyLicense");
            safeHarborARM.PhoneOfCompany = GetString("sSafeHarborARMCompanyPhone");
            safeHarborARM.FaxOfCompany = GetString("sSafeHarborARMCompanyFax");
            safeHarborARM.StreetAddr = GetString("sSafeHarborARMCompanyStreetAddr");
            safeHarborARM.City = GetString("sSafeHarborARMCompanyCity");
            safeHarborARM.State = GetString("sSafeHarborARMCompanyState");
            safeHarborARM.Zip = GetString("sSafeHarborARMCompanyZip");
            safeHarborARM.AgentRoleT = (E_AgentRoleT)GetInt("CFM_AgentRoleT");
            safeHarborARM.IsLocked = GetBool("CFM_IsLocked");
            safeHarborARM.Update();


            dataLoan.sDueSafeHarborARMLowestCost_rep = GetString("sDueSafeHarborARMLowestCost");
            dataLoan.sDueSafeHarborARMRecLoan_rep = GetString("sDueSafeHarborARMRecLoan");
            dataLoan.sDueSafeHarborARMWithoutRiskyFeatures_rep = GetString("sDueSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sDueSafeHarborARMWithRiskyFeatures_rep = GetString("sDueSafeHarborARMWithRiskyFeatures");
            dataLoan.sEquityCalcSafeHarborARMLowestCost_rep = GetString("sEquityCalcSafeHarborARMLowestCost");
            dataLoan.sEquityCalcSafeHarborARMRecLoan_rep = GetString("sEquityCalcSafeHarborARMRecLoan");
            dataLoan.sEquityCalcSafeHarborARMWithoutRiskyFeatures_rep = GetString("sEquityCalcSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sEquityCalcSafeHarborARMWithRiskyFeatures_rep = GetString("sEquityCalcSafeHarborARMWithRiskyFeatures");
            dataLoan.sFinalLAmtSafeHarborARMLowestCost_rep = GetString("sFinalLAmtSafeHarborARMLowestCost");
            dataLoan.sFinalLAmtSafeHarborARMRecLoan_rep = GetString("sFinalLAmtSafeHarborARMRecLoan");
            dataLoan.sFinalLAmtSafeHarborARMWithoutRiskyFeatures_rep = GetString("sFinalLAmtSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sFinalLAmtSafeHarborARMWithRiskyFeatures_rep = GetString("sFinalLAmtSafeHarborARMWithRiskyFeatures");
            dataLoan.sFullyIndexedRSafeHarborARMLowestCost_rep = GetString("sFullyIndexedRSafeHarborARMLowestCost");
            dataLoan.sFullyIndexedRSafeHarborARMRecLoan_rep = GetString("sFullyIndexedRSafeHarborARMRecLoan");
            dataLoan.sFullyIndexedRSafeHarborARMWithoutRiskyFeatures_rep = GetString("sFullyIndexedRSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sFullyIndexedRSafeHarborARMWithRiskyFeatures_rep = GetString("sFullyIndexedRSafeHarborARMWithRiskyFeatures");
            dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMLowestCost_rep = GetString("sGfeProThisMPmtAndMInsSafeHarborARMLowestCost");
            dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMRecLoan_rep = GetString("sGfeProThisMPmtAndMInsSafeHarborARMRecLoan");
            dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMWithoutRiskyFeatures_rep = GetString("sGfeProThisMPmtAndMInsSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures_rep = GetString("sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures");
            dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost_rep = GetString("sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost");
            dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan_rep = GetString("sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan");
            dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures_rep = GetString("sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures_rep = GetString("sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures");
            dataLoan.sLpTemplateNmSafeHarborARMLowestCost = GetString("sLpTemplateNmSafeHarborARMLowestCost");
            dataLoan.sLpTemplateNmSafeHarborARMRecLoan = GetString("sLpTemplateNmSafeHarborARMRecLoan");
            dataLoan.sLpTemplateNmSafeHarborARMWithoutRiskyFeatures = GetString("sLpTemplateNmSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sLpTemplateNmSafeHarborARMWithRiskyFeatures = GetString("sLpTemplateNmSafeHarborARMWithRiskyFeatures");
            dataLoan.sNoteIRSafeHarborARMLowestCost_rep = GetString("sNoteIRSafeHarborARMLowestCost");
            dataLoan.sNoteIRSafeHarborARMRecLoan_rep = GetString("sNoteIRSafeHarborARMRecLoan");
            dataLoan.sNoteIRSafeHarborARMWithoutRiskyFeatures_rep = GetString("sNoteIRSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sNoteIRSafeHarborARMWithRiskyFeatures_rep = GetString("sNoteIRSafeHarborARMWithRiskyFeatures");
            dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMLowestCost_rep = GetString("sOriginatorCompensationTotalAmountSafeHarborARMLowestCost");
            dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMRecLoan_rep = GetString("sOriginatorCompensationTotalAmountSafeHarborARMRecLoan");
            dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMWithoutRiskyFeatures_rep = GetString("sOriginatorCompensationTotalAmountSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures_rep = GetString("sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures");
            dataLoan.sRLifeCapRSafeHarborARMLowestCost_rep = GetString("sRLifeCapRSafeHarborARMLowestCost");
            dataLoan.sRLifeCapRSafeHarborARMRecLoan_rep = GetString("sRLifeCapRSafeHarborARMRecLoan");
            dataLoan.sRLifeCapRSafeHarborARMWithoutRiskyFeatures_rep = GetString("sRLifeCapRSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sRLifeCapRSafeHarborARMWithRiskyFeatures_rep = GetString("sRLifeCapRSafeHarborARMWithRiskyFeatures");
            dataLoan.sSafeHarborARMBalloonPmntIn7Yrs = GetBool("sSafeHarborARMBalloonPmntIn7Yrs");
            dataLoan.sSafeHarborARMDemandFeat = GetBool("sSafeHarborARMDemandFeat");
            dataLoan.sSafeHarborARMIntOnlyPmts = GetBool("sSafeHarborARMIntOnlyPmts");
            dataLoan.sSafeHarborARMNegAmort = GetBool("sSafeHarborARMNegAmort");
            dataLoan.sSafeHarborARMOurRecLowestCosts = GetBool("sSafeHarborARMOurRecLowestCosts");
            dataLoan.sSafeHarborARMOurRecOther = GetBool("sSafeHarborARMOurRecOther");
            dataLoan.sSafeHarborARMOurRecText = GetString("sSafeHarborARMOurRecText");
            dataLoan.sSafeHarborARMOurRecWithoutRiskyFeatures = GetBool("sSafeHarborARMOurRecWithoutRiskyFeatures");
            dataLoan.sSafeHarborARMPrepayPenalty = GetBool("sSafeHarborARMPrepayPenalty");
            dataLoan.sSafeHarborARMSharedAppreciation = GetBool("sSafeHarborARMSharedAppreciation");
            dataLoan.sSafeHarborARMSharedEquity = GetBool("sSafeHarborARMSharedEquity");
            dataLoan.sTransNetCashSafeHarborARMLowestCost_rep = GetString("sTransNetCashSafeHarborARMLowestCost");
            dataLoan.sTransNetCashSafeHarborARMRecLoan_rep = GetString("sTransNetCashSafeHarborARMRecLoan");
            dataLoan.sTransNetCashSafeHarborARMWithoutRiskyFeatures_rep = GetString("sTransNetCashSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sTransNetCashSafeHarborARMWithRiskyFeatures_rep = GetString("sTransNetCashSafeHarborARMWithRiskyFeatures");
            dataLoan.sIncludeSafeHarborARMWithRiskyFeatures = GetInt("sIncludeSafeHarborARMWithRiskyFeatures") == 1;
            dataLoan.sIncludeSafeHarborARMRecLoan = GetInt("sIncludeSafeHarborARMRecLoan") == 1;
            dataLoan.sSafeHarborARMOurRecLowestRate = GetBool("sSafeHarborARMOurRecLowestRate");

            dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures_rep = GetString("sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures");
            dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures_rep = GetString("sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures");
            dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost_rep = GetString("sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost");
            dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan_rep = GetString("sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan");
            
            switch (GetInt("m_copyToColumn"))
            {
                case 0:
                    dataLoan.sDueSafeHarborARMWithRiskyFeatures = dataLoan.sDue;
                    dataLoan.sEquityCalcSafeHarborARMWithRiskyFeatures_rep = dataLoan.sEquityCalc_rep;
                    dataLoan.sFinalLAmtSafeHarborARMWithRiskyFeatures_rep = dataLoan.sFinalLAmt_rep;
                    dataLoan.sFullyIndexedRSafeHarborARMWithRiskyFeatures_rep = dataLoan.sFullyIndexedR_rep;
                    dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures_rep = dataLoan.sGfeProThisMPmtAndMIns_rep;
                    dataLoan.sNoteIRSafeHarborARMWithRiskyFeatures_rep = dataLoan.sNoteIR_rep;
                    dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures_rep = dataLoan.sGfeOriginatorCompF_rep;
                    dataLoan.sRLifeCapRSafeHarborARMWithRiskyFeatures_rep = dataLoan.sRLifeCapR_rep;
                    dataLoan.sLpTemplateNmSafeHarborARMWithRiskyFeatures = dataLoan.sLpTemplateNm;
                    dataLoan.sIncludeSafeHarborARMWithRiskyFeatures = true;

                    if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures_rep = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
                        dataLoan.sTransNetCashSafeHarborARMWithRiskyFeatures_rep = dataLoan.sTRIDLoanEstimateCashToClose_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures_rep = dataLoan.sSumOfTridAFees_rep;
                    }
                    else
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures_rep = dataLoan.sGfeTotalEstimateSettlementCharge_rep;
                        dataLoan.sTransNetCashSafeHarborARMWithRiskyFeatures_rep = dataLoan.sTransNetCash_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures_rep = dataLoan.sSumOfGEFA1FeesAndDiscountPoints_rep;
                    }

                    break;
                case 1:
                    dataLoan.sDueSafeHarborARMWithoutRiskyFeatures = dataLoan.sDue;
                    dataLoan.sEquityCalcSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sEquityCalc_rep;
                    dataLoan.sFinalLAmtSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sFinalLAmt_rep;
                    dataLoan.sFullyIndexedRSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sFullyIndexedR_rep;
                    dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sGfeProThisMPmtAndMIns_rep;
                    dataLoan.sNoteIRSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sNoteIR_rep;
                    dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sGfeOriginatorCompF_rep;
                    dataLoan.sRLifeCapRSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sRLifeCapR_rep;
                    dataLoan.sLpTemplateNmSafeHarborARMWithoutRiskyFeatures = dataLoan.sLpTemplateNm;

                    if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
                        dataLoan.sTransNetCashSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sTRIDLoanEstimateCashToClose_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sSumOfTridAFees_rep;
                    }
                    else
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sGfeTotalEstimateSettlementCharge_rep;
                        dataLoan.sTransNetCashSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sTransNetCash_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures_rep = dataLoan.sSumOfGEFA1FeesAndDiscountPoints_rep;
                    }

                    break;
                case 2:
                    dataLoan.sDueSafeHarborARMLowestCost = dataLoan.sDue;
                    dataLoan.sEquityCalcSafeHarborARMLowestCost_rep = dataLoan.sEquityCalc_rep;
                    dataLoan.sFinalLAmtSafeHarborARMLowestCost_rep = dataLoan.sFinalLAmt_rep;
                    dataLoan.sFullyIndexedRSafeHarborARMLowestCost_rep = dataLoan.sFullyIndexedR_rep;
                    dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMLowestCost_rep = dataLoan.sGfeProThisMPmtAndMIns_rep;
                    dataLoan.sNoteIRSafeHarborARMLowestCost_rep = dataLoan.sNoteIR_rep;
                    dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMLowestCost_rep = dataLoan.sGfeOriginatorCompF_rep;
                    dataLoan.sRLifeCapRSafeHarborARMLowestCost_rep = dataLoan.sRLifeCapR_rep;
                    dataLoan.sLpTemplateNmSafeHarborARMLowestCost = dataLoan.sLpTemplateNm;

                    if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost_rep = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
                        dataLoan.sTransNetCashSafeHarborARMLowestCost_rep = dataLoan.sTRIDLoanEstimateCashToClose_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost_rep = dataLoan.sSumOfTridAFees_rep;
                    }
                    else
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost_rep = dataLoan.sGfeTotalEstimateSettlementCharge_rep;
                        dataLoan.sTransNetCashSafeHarborARMLowestCost_rep = dataLoan.sTransNetCash_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost_rep = dataLoan.sSumOfGEFA1FeesAndDiscountPoints_rep;
                    }

                    break;
                case 3:
                    dataLoan.sDueSafeHarborARMRecLoan = dataLoan.sDue;
                    dataLoan.sEquityCalcSafeHarborARMRecLoan_rep = dataLoan.sEquityCalc_rep;
                    dataLoan.sFinalLAmtSafeHarborARMRecLoan_rep = dataLoan.sFinalLAmt_rep;
                    dataLoan.sFullyIndexedRSafeHarborARMRecLoan_rep = dataLoan.sFullyIndexedR_rep;
                    dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMRecLoan_rep = dataLoan.sGfeProThisMPmtAndMIns_rep;
                    dataLoan.sNoteIRSafeHarborARMRecLoan_rep = dataLoan.sNoteIR_rep;
                    dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMRecLoan_rep = dataLoan.sGfeOriginatorCompF_rep;
                    dataLoan.sRLifeCapRSafeHarborARMRecLoan_rep = dataLoan.sRLifeCapR_rep;
                    dataLoan.sLpTemplateNmSafeHarborARMRecLoan = dataLoan.sLpTemplateNm;
                    dataLoan.sIncludeSafeHarborARMRecLoan = true;

                    if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan_rep = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
                        dataLoan.sTransNetCashSafeHarborARMRecLoan_rep = dataLoan.sTRIDLoanEstimateCashToClose_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan_rep = dataLoan.sSumOfTridAFees_rep;
                    }
                    else
                    {
                        dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan_rep = dataLoan.sGfeTotalEstimateSettlementCharge_rep;
                        dataLoan.sTransNetCashSafeHarborARMRecLoan_rep = dataLoan.sTransNetCash_rep;
                        dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan_rep = dataLoan.sSumOfGEFA1FeesAndDiscountPoints_rep;
                    }

                    break;
                default:
                    break;
            }
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields safeHarborARM = dataLoan.GetPreparerOfForm(E_PreparerFormT.SafeHarborARM, E_ReturnOptionIfNotExist.CreateNew);
            SetResult("sSafeHarborARMAgentName", safeHarborARM.PreparerName);
            SetResult("sSafeHarborARMLoanOriginatorIdentifier", safeHarborARM.LoanOriginatorIdentifier);
            SetResult("sSafeHarborARMAgentLicense", safeHarborARM.LicenseNumOfAgent);
            SetResult("sSafeHarborARMPhone", safeHarborARM.Phone);
            SetResult("sSafeHarborARMCompanyName", safeHarborARM.CompanyName);
            SetResult("sSafeHarborARMCompanyLoanOriginatorIdentifier", safeHarborARM.CompanyLoanOriginatorIdentifier);
            SetResult("sSafeHarborARMCompanyLicense", safeHarborARM.LicenseNumOfCompany);
            SetResult("sSafeHarborARMCompanyPhone", safeHarborARM.PhoneOfCompany);
            SetResult("sSafeHarborARMCompanyFax", safeHarborARM.FaxOfCompany);
            SetResult("sSafeHarborARMCompanyStreetAddr", safeHarborARM.StreetAddr);
            SetResult("sSafeHarborARMCompanyCity", safeHarborARM.City);
            SetResult("sSafeHarborARMCompanyState", safeHarborARM.State);
            SetResult("sSafeHarborARMCompanyZip", safeHarborARM.Zip);
            SetResult("CFM_AgentRoleT", safeHarborARM.AgentRoleT);
            SetResult("CFM_IsLocked", safeHarborARM.IsLocked);

            SetResult("sDueSafeHarborARMLowestCost", dataLoan.sDueSafeHarborARMLowestCost);
            SetResult("sDueSafeHarborARMRecLoan", dataLoan.sDueSafeHarborARMRecLoan);
            SetResult("sDueSafeHarborARMWithoutRiskyFeatures", dataLoan.sDueSafeHarborARMWithoutRiskyFeatures);
            SetResult("sDueSafeHarborARMWithRiskyFeatures", dataLoan.sDueSafeHarborARMWithRiskyFeatures);
            SetResult("sEquityCalcSafeHarborARMLowestCost", dataLoan.sEquityCalcSafeHarborARMLowestCost_rep);
            SetResult("sEquityCalcSafeHarborARMRecLoan", dataLoan.sEquityCalcSafeHarborARMRecLoan_rep);
            SetResult("sEquityCalcSafeHarborARMWithoutRiskyFeatures", dataLoan.sEquityCalcSafeHarborARMWithoutRiskyFeatures_rep);
            SetResult("sEquityCalcSafeHarborARMWithRiskyFeatures", dataLoan.sEquityCalcSafeHarborARMWithRiskyFeatures_rep);
            SetResult("sFinalLAmtSafeHarborARMLowestCost", dataLoan.sFinalLAmtSafeHarborARMLowestCost_rep);
            SetResult("sFinalLAmtSafeHarborARMRecLoan", dataLoan.sFinalLAmtSafeHarborARMRecLoan_rep);
            SetResult("sFinalLAmtSafeHarborARMWithoutRiskyFeatures", dataLoan.sFinalLAmtSafeHarborARMWithoutRiskyFeatures_rep);
            SetResult("sFinalLAmtSafeHarborARMWithRiskyFeatures", dataLoan.sFinalLAmtSafeHarborARMWithRiskyFeatures_rep);
            SetResult("sFullyIndexedRSafeHarborARMLowestCost", dataLoan.sFullyIndexedRSafeHarborARMLowestCost_rep);
            SetResult("sFullyIndexedRSafeHarborARMRecLoan", dataLoan.sFullyIndexedRSafeHarborARMRecLoan_rep);
            SetResult("sFullyIndexedRSafeHarborARMWithoutRiskyFeatures", dataLoan.sFullyIndexedRSafeHarborARMWithoutRiskyFeatures_rep);
            SetResult("sFullyIndexedRSafeHarborARMWithRiskyFeatures", dataLoan.sFullyIndexedRSafeHarborARMWithRiskyFeatures_rep);
            SetResult("sGfeProThisMPmtAndMInsSafeHarborARMLowestCost", dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMLowestCost_rep);
            SetResult("sGfeProThisMPmtAndMInsSafeHarborARMRecLoan", dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMRecLoan_rep);
            SetResult("sGfeProThisMPmtAndMInsSafeHarborARMWithoutRiskyFeatures", dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMWithoutRiskyFeatures_rep);
            SetResult("sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures", dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures_rep);
            SetResult("sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost_rep);
            SetResult("sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan_rep);
            SetResult("sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures_rep);
            SetResult("sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures_rep);
            SetResult("sLpTemplateNmSafeHarborARMLowestCost", dataLoan.sLpTemplateNmSafeHarborARMLowestCost);
            SetResult("sLpTemplateNmSafeHarborARMRecLoan", dataLoan.sLpTemplateNmSafeHarborARMRecLoan);
            SetResult("sLpTemplateNmSafeHarborARMWithoutRiskyFeatures", dataLoan.sLpTemplateNmSafeHarborARMWithoutRiskyFeatures);
            SetResult("sLpTemplateNmSafeHarborARMWithRiskyFeatures", dataLoan.sLpTemplateNmSafeHarborARMWithRiskyFeatures);
            SetResult("sNoteIRSafeHarborARMLowestCost", dataLoan.sNoteIRSafeHarborARMLowestCost_rep);
            SetResult("sNoteIRSafeHarborARMRecLoan", dataLoan.sNoteIRSafeHarborARMRecLoan_rep);
            SetResult("sNoteIRSafeHarborARMWithoutRiskyFeatures", dataLoan.sNoteIRSafeHarborARMWithoutRiskyFeatures_rep);
            SetResult("sNoteIRSafeHarborARMWithRiskyFeatures", dataLoan.sNoteIRSafeHarborARMWithRiskyFeatures_rep);
            SetResult("sOriginatorCompensationTotalAmountSafeHarborARMLowestCost", dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMLowestCost_rep);
            SetResult("sOriginatorCompensationTotalAmountSafeHarborARMRecLoan", dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMRecLoan_rep);
            SetResult("sOriginatorCompensationTotalAmountSafeHarborARMWithoutRiskyFeatures", dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMWithoutRiskyFeatures_rep);
            SetResult("sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures", dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures_rep);
            SetResult("sRLifeCapRSafeHarborARMLowestCost", dataLoan.sRLifeCapRSafeHarborARMLowestCost_rep);
            SetResult("sRLifeCapRSafeHarborARMRecLoan", dataLoan.sRLifeCapRSafeHarborARMRecLoan_rep);
            SetResult("sRLifeCapRSafeHarborARMWithoutRiskyFeatures", dataLoan.sRLifeCapRSafeHarborARMWithoutRiskyFeatures_rep);
            SetResult("sRLifeCapRSafeHarborARMWithRiskyFeatures", dataLoan.sRLifeCapRSafeHarborARMWithRiskyFeatures_rep);
            SetResult("sSafeHarborARMBalloonPmntIn7Yrs", dataLoan.sSafeHarborARMBalloonPmntIn7Yrs);
            SetResult("sSafeHarborARMDemandFeat", dataLoan.sSafeHarborARMDemandFeat);
            SetResult("sSafeHarborARMIntOnlyPmts", dataLoan.sSafeHarborARMIntOnlyPmts);
            SetResult("sSafeHarborARMNegAmort", dataLoan.sSafeHarborARMNegAmort);
            SetResult("sSafeHarborARMOurRecLowestCosts", dataLoan.sSafeHarborARMOurRecLowestCosts);
            SetResult("sSafeHarborARMOurRecOther", dataLoan.sSafeHarborARMOurRecOther);
            SetResult("sSafeHarborARMOurRecText", dataLoan.sSafeHarborARMOurRecText);
            SetResult("sSafeHarborARMOurRecWithoutRiskyFeatures", dataLoan.sSafeHarborARMOurRecWithoutRiskyFeatures);
            SetResult("sSafeHarborARMPrepayPenalty", dataLoan.sSafeHarborARMPrepayPenalty);
            SetResult("sSafeHarborARMSharedAppreciation", dataLoan.sSafeHarborARMSharedAppreciation);
            SetResult("sSafeHarborARMSharedEquity", dataLoan.sSafeHarborARMSharedEquity);
            SetResult("sTransNetCashSafeHarborARMLowestCost", dataLoan.sTransNetCashSafeHarborARMLowestCost_rep);
            SetResult("sTransNetCashSafeHarborARMRecLoan", dataLoan.sTransNetCashSafeHarborARMRecLoan_rep);
            SetResult("sTransNetCashSafeHarborARMWithoutRiskyFeatures", dataLoan.sTransNetCashSafeHarborARMWithoutRiskyFeatures_rep);
            SetResult("sTransNetCashSafeHarborARMWithRiskyFeatures", dataLoan.sTransNetCashSafeHarborARMWithRiskyFeatures_rep);
            SetResult("sIncludeSafeHarborARMWithRiskyFeatures", dataLoan.sIncludeSafeHarborARMWithRiskyFeatures ? 1 : 0);
            SetResult("sIncludeSafeHarborARMRecLoan", dataLoan.sIncludeSafeHarborARMRecLoan ? 1 : 0);
            SetResult("sSafeHarborARMOurRecLowestRate", dataLoan.sSafeHarborARMOurRecLowestRate);

            SetResult("sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures_rep);
            SetResult("sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures_rep);
            SetResult("sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost_rep);
            SetResult("sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan_rep);
        }
    }
    public partial class SafeHarborDisclosureAdjustableService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new SafeHarborDisclosureAdjustableServiceItem());
        }
    }
}