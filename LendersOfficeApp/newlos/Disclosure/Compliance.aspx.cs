﻿namespace LendersOfficeApp.newlos.Disclosure
{
    using System;
    using DataAccess;
    using LendersOffice.Migration;

    public partial class Compliance : BaseLoanPage
    { 
        protected void PageInit(object sender, EventArgs e)
        {
            this.PageID = "Compliance";
            this.PageTitle = "Compliance";

            this.EnableJqueryMigrate = false;

            Tools.Bind_sDisclosureRegulationT(sDisclosureRegulationT);
            Tools.Bind_FloodCertificationDeterminationT(sFloodCertificationDeterminationT);
            Tools.Bind_sAprCalculationT(sAprCalculationT);
            Tools.Bind_LoanRescindableT(this.sLoanRescindableT);
            Tools.Bind_TriState(this.sLoanTransactionInvolvesSeller);
            Tools.Bind_sTridTargetRegulationVersionT(sTridTargetRegulationVersionT);
            Tools.Bind_sTridClosingDisclosureCalculateEscrowAccountFromT(sTridClosingDisclosureCalculateEscrowAccountFromT);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;

            this.Init += new System.EventHandler(this.PageInit);

            base.OnInit(e);
        }

        protected override void LoadData()
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Compliance));
            dataLoan.InitLoad();

            sLOrigFProps_BF.SelectedValue = dataLoan.sLOrigFProps_BF.ToString();
            sGfeDiscountPointFProps_BF.SelectedValue = dataLoan.sGfeDiscountPointFProps_BF.ToString();
            sGfeLenderCreditFProps_Apr.SelectedValue = dataLoan.sGfeLenderCreditFProps_Apr.ToString();

            Tools.SetDropDownListValue(sDisclosureRegulationT, dataLoan.sDisclosureRegulationT);
            sDisclosureRegulationTLckd.Checked = dataLoan.sDisclosureRegulationTLckd;

            Tools.SetDropDownListValue(sFloodCertificationDeterminationT, dataLoan.sFloodCertificationDeterminationT);

            Tools.SetDropDownListValue(sAprCalculationT, dataLoan.sAprCalculationT);
            this.sIsUseManualApr.SelectedValue = dataLoan.sIsUseManualApr.ToString();

            Tools.SetDropDownListValue(this.sLoanRescindableT, dataLoan.sLoanRescindableT);
            this.sLoanRescindableTLckd.Checked = dataLoan.sLoanRescindableTLckd;
            this.sGfeIsBalloonLckd.Checked = dataLoan.sGfeIsBalloonLckd;

            Tools.SetDropDownListValue(this.sLoanTransactionInvolvesSeller, dataLoan.sLoanTransactionInvolvesSeller);
            this.sLoanTransactionInvolvesSellerLckd.Checked = dataLoan.sLoanTransactionInvolvesSellerLckd;
            this.sIsRequireAlternateCashToCloseForNonSellerTransactions.SelectedValue = dataLoan.sIsRequireAlternateCashToCloseForNonSellerTransactions.ToString();

            Tools.SetDropDownListValue(this.sTridTargetRegulationVersionT, dataLoan.sTridTargetRegulationVersionT);
            this.sTridTargetRegulationVersionTLckd.Checked = dataLoan.sTridTargetRegulationVersionTLckd;

            Tools.SetDropDownListValue(this.sTridClosingDisclosureCalculateEscrowAccountFromT, dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT);

            this.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure.SelectedValue = dataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure.ToString();

            this.RegisterJsGlobalVariables("EnableBorrowerLevelDisclosureTracking", this.Broker.EnableBorrowerLevelDisclosureTracking);

            bool ShowIsUseManualApr = dataLoan.sIsConstructionLoan
                && (Broker.AllowManualApr
                    || (Broker.UseManualApr
                        && LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges)));

            this.RegisterJsGlobalVariables("ShowIsUseManualApr", ShowIsUseManualApr);
        }
    }
}
