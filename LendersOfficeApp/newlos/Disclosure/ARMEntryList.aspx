<%@ Page language="c#" Codebehind="ARMEntryList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Disclosure.ARMEntryList" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>ARMEntryList</title>
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body bgcolor=gainsboro margin=0 scroll=yes>
	<script language=javascript>
<!--
function _init() {
  resize(350, 350);
}
function f_select(i) {
		var arg = window.dialogArguments || {};
		arg.OK = true;
		arg.fannieMaeArmIndexT = fannieMaeArmIndexT[i] ;
		arg.freddieArmIndexT = freddieArmIndexT[i];
		arg.effectiveD = effectiveD[i];
		arg.indexBasedOnVstr = indexBasedOnVstr[i];
		arg.indexCanBeFoundVstr = indexCanBeFoundVstr[i];
		onClosePopup(arg);
}
//-->
</script>
    <h4 class="page-header">ARM Index</h4>
    <form id="ARMEntryList" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 width="100%" border=0>
  <tr>
    <td nowrap><ml:PassthroughLiteral id=m_html runat="server"></ml:PassthroughLiteral></td></tr>
  <tr>
    <td nowrap align=middle><input type=button value=Cancel 
      onclick="onClosePopup();" class=ButtonStyle NoHighlight></td></tr></table><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg></form></body>
</html>
