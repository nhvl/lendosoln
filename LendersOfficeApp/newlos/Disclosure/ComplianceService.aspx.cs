﻿namespace LendersOfficeApp.newlos.Disclosure
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.TRID2;

    public partial class ComplianceServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(ComplianceServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sLOrigFProps_BF = GetBool("sLOrigFProps_BF");
            dataLoan.sGfeDiscountPointFProps_BF = GetBool("sGfeDiscountPointFProps_BF");
            dataLoan.sGfeLenderCreditFProps_Apr = GetBool("sGfeLenderCreditFProps_Apr");
            dataLoan.sDisclosureRegulationT = GetEnum<E_sDisclosureRegulationT>("sDisclosureRegulationT");
            dataLoan.sDisclosureRegulationTLckd = GetBool("sDisclosureRegulationTLckd");
            dataLoan.sFloodCertificationDeterminationT = (E_FloodCertificationDeterminationT)GetInt("sFloodCertificationDeterminationT");
            dataLoan.sAprCalculationT = (E_sAprCalculationT)GetInt("sAprCalculationT");
            dataLoan.sIsUseManualApr = GetBool("sIsUseManualApr");
            dataLoan.sLoanRescindableT = GetEnum<LoanRescindableT>("sLoanRescindableT");
            dataLoan.sLoanRescindableTLckd = GetBool("sLoanRescindableTLckd");
            dataLoan.sGfeIsBalloonLckd = GetBool("sGfeIsBalloonLckd");
            dataLoan.sLoanTransactionInvolvesSellerLckd = GetBool("sLoanTransactionInvolvesSellerLckd");

            if (dataLoan.sLoanTransactionInvolvesSellerLckd)
            {
                dataLoan.sLoanTransactionInvolvesSeller = GetEnum<E_TriState>("sLoanTransactionInvolvesSeller");
            }

            dataLoan.sIsRequireAlternateCashToCloseForNonSellerTransactions = GetBool("sIsRequireAlternateCashToCloseForNonSellerTransactions");
            dataLoan.sTridTargetRegulationVersionT = GetEnum<TridTargetRegulationVersionT>("sTridTargetRegulationVersionT");
            dataLoan.sTridTargetRegulationVersionTLckd = GetBool("sTridTargetRegulationVersionTLckd");
            dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT = GetEnum<TridClosingDisclosureCalculateEscrowAccountFromT>("sTridClosingDisclosureCalculateEscrowAccountFromT");

            if (dataLoan.BrokerDB.EnableBorrowerLevelDisclosureTracking)
            {
                dataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure = this.GetBool("sAlwaysRequireAllBorrowersToReceiveClosingDisclosure");
            }
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sLOrigFProps_BF", dataLoan.sLOrigFProps_BF);
            SetResult("sGfeDiscountPointFProps_BF", dataLoan.sGfeDiscountPointFProps_BF);
            SetResult("sGfeLenderCreditFProps_Apr", dataLoan.sGfeLenderCreditFProps_Apr);
            SetResult("sDisclosureRegulationT", dataLoan.sDisclosureRegulationT);
            SetResult("sDisclosureRegulationTLckd", dataLoan.sDisclosureRegulationTLckd);
            SetResult("sFloodCertificationDeterminationT", dataLoan.sFloodCertificationDeterminationT);
            SetResult("sAprCalculationT", dataLoan.sAprCalculationT);
            SetResult("sIsUseManualApr", dataLoan.sIsUseManualApr);
            SetResult("sLoanRescindableT", dataLoan.sLoanRescindableT);
            SetResult("sLoanRescindableTLckd", dataLoan.sLoanRescindableTLckd);
            SetResult("sGfeIsBalloonLckd", dataLoan.sGfeIsBalloonLckd);
            SetResult("sLoanTransactionInvolvesSeller", (int)dataLoan.sLoanTransactionInvolvesSeller);
            SetResult("sLoanTransactionInvolvesSellerLckd", dataLoan.sLoanTransactionInvolvesSellerLckd);
            SetResult("sIsRequireAlternateCashToCloseForNonSellerTransactions", dataLoan.sIsRequireAlternateCashToCloseForNonSellerTransactions);
            SetResult("sTridTargetRegulationVersionT", dataLoan.sTridTargetRegulationVersionT);
            SetResult("sTridTargetRegulationVersionTLckd", dataLoan.sTridTargetRegulationVersionTLckd);
            SetResult("sTridClosingDisclosureCalculateEscrowAccountFromT", dataLoan.sTridClosingDisclosureCalculateEscrowAccountFromT);
            this.SetResult("sAlwaysRequireAllBorrowersToReceiveClosingDisclosure", dataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);
        }
    }

    public partial class ComplianceService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new ComplianceServiceItem());
        }
    }
}
