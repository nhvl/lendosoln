///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class RealEstateDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(RealEstateDisclosureServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.RealEstateDisclosure, E_ReturnOptionIfNotExist.CreateNew);

            broker.CompanyName = GetString("RealEstateDisclosureCompanyName");
            broker.StreetAddr = GetString("RealEstateDisclosureStreetAddr");
            broker.City = GetString("RealEstateDisclosureCity");
            broker.State = GetString("RealEstateDisclosureState");
            broker.Zip = GetString("RealEstateDisclosureZip");
            broker.PhoneOfCompany = GetString("RealEstateDisclosurePhoneOfCompany");
            broker.FaxOfCompany = GetString("RealEstateDisclosureFaxOfCompany");
            broker.LicenseNumOfCompany = GetString("RealEstateDisclosureLicenseNumOfCompany");
            broker.Update();
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

	public partial class RealEstateDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new RealEstateDisclosureServiceItem());
        }
	}
}
