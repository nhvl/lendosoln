﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public partial class PatriotActDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new PatriotActDisclosureServiceItem());
        }
    }

    public partial class PatriotActDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var Preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.PatriotActDisclosure, 
                                                    E_ReturnOptionIfNotExist.CreateNew);
            Preparer.PrepareDate_rep = GetString("PrepareDate");
            Preparer.PreparerName = GetString("PreparerName");
            Preparer.Update();

            dataApp.aBIdT = (E_IdType)Enum.Parse(typeof(E_IdType), GetString("aBIdT"));
            dataApp.aBIdState = GetString("aBIdState");
            dataApp.aBIdNumber = GetString("aBIdNumber");
            dataApp.aBIdIssueD = CDateTime.Create(GetString("aBIdIssueD"), dataApp.m_convertLos);
            dataApp.aBIdExpireD = CDateTime.Create(GetString("aBIdExpireD"), dataApp.m_convertLos);
            dataApp.aBIdCountry = GetString("aBIdCountry");
            dataApp.aBIdGovBranch = GetString("aBIdGovBranch");
            dataApp.aBIdOther = GetString("aBIdOther");

            dataApp.aB2ndIdT = (E_IdType)Enum.Parse(typeof(E_IdType), GetString("aB2ndIdT"));
            dataApp.aB2ndIdState = GetString("aB2ndIdState");
            dataApp.aB2ndIdNumber = GetString("aB2ndIdNumber");
            dataApp.aB2ndIdIssueD = CDateTime.Create(GetString("aB2ndIdIssueD"), dataApp.m_convertLos);
            dataApp.aB2ndIdExpireD = CDateTime.Create(GetString("aB2ndIdExpireD"), dataApp.m_convertLos);
            dataApp.aB2ndIdCountry = GetString("aB2ndIdCountry");
            dataApp.aB2ndIdGovBranch = GetString("aB2ndIdGovBranch");
            dataApp.aB2ndIdOther = GetString("aB2ndIdOther");

            dataApp.aCIdT = (E_IdType)Enum.Parse(typeof(E_IdType), GetString("aCIdT"));
            dataApp.aCIdState = GetString("aCIdState");
            dataApp.aCIdNumber = GetString("aCIdNumber");
            dataApp.aCIdIssueD = CDateTime.Create(GetString("aCIdIssueD"), dataApp.m_convertLos);
            dataApp.aCIdExpireD = CDateTime.Create(GetString("aCIdExpireD"), dataApp.m_convertLos);
            dataApp.aCIdCountry = GetString("aCIdCountry");
            dataApp.aCIdGovBranch = GetString("aCIdGovBranch");
            dataApp.aCIdOther = GetString("aCIdOther");

            dataApp.aC2ndIdT = (E_IdType)Enum.Parse(typeof(E_IdType), GetString("aC2ndIdT"));
            dataApp.aC2ndIdState = GetString("aC2ndIdState");
            dataApp.aC2ndIdNumber = GetString("aC2ndIdNumber");
            dataApp.aC2ndIdIssueD = CDateTime.Create(GetString("aC2ndIdIssueD"), dataApp.m_convertLos);
            dataApp.aC2ndIdExpireD = CDateTime.Create(GetString("aC2ndIdExpireD"), dataApp.m_convertLos);
            dataApp.aC2ndIdCountry = GetString("aC2ndIdCountry");
            dataApp.aC2ndIdGovBranch = GetString("aC2ndIdGovBranch");
            dataApp.aC2ndIdOther = GetString("aC2ndIdOther");
            
            dataApp.aIdResolution = GetString("aIdResolution");
            
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, this.GetType());
        }
    }
}
