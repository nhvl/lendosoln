///
/// Author: Matthew Flynn
/// 
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class PrivacyPolicyDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PrivacyPolicyDisclosureServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.PrivacyPolicyDisclosure, E_ReturnOptionIfNotExist.CreateNew);

            broker.CompanyName = GetString("PrivacyPolicyDisclosureCompanyName");
            broker.StreetAddr = GetString("PrivacyPolicyDisclosureStreetAddr");
            broker.City = GetString("PrivacyPolicyDisclosureCity");
            broker.State = GetString("PrivacyPolicyDisclosureState");
            broker.Zip = GetString("PrivacyPolicyDisclosureZip");
            broker.PhoneOfCompany = GetString("PrivacyPolicyDisclosurePhoneOfCompany");
            broker.Update();

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

	public partial class PrivacyPolicyDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new PrivacyPolicyDisclosureServiceItem());
        }

	}
}
