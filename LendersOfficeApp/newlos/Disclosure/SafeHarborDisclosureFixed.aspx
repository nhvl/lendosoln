﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="SafeHarborDisclosureFixed.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.SafeHarborDisclosureFixed" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SafeHarborDisclosureFixed</title>
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript">	               
        
        function _init() {
            document.getElementById('m_copyToColumn').value = '-1';

            var enableRiskyFeaturesColumn = !document.getElementById('sIncludeSafeHarborFixedWithRiskyFeaturesYes').checked;
            document.getElementById('sLpTemplateNmSafeHarborFixedWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
            document.getElementById('sFinalLAmtSafeHarborFixedWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
            document.getElementById('sDueSafeHarborFixedWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
            document.getElementById('sNoteIRSafeHarborFixedWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
            document.getElementById('sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
            document.getElementById('sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
            document.getElementById('sEquityCalcSafeHarborFixedWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
            document.getElementById('sTransNetCashSafeHarborFixedWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
            document.getElementById('sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;

            var enableRecLoanColumn = !document.getElementById('sIncludeSafeHarborFixedRecLoanYes').checked;
            document.getElementById('sLpTemplateNmSafeHarborFixedRecLoan').readOnly = enableRecLoanColumn;
            document.getElementById('sFinalLAmtSafeHarborFixedRecLoan').readOnly = enableRecLoanColumn;
            document.getElementById('sDueSafeHarborFixedRecLoan').readOnly = enableRecLoanColumn;
            document.getElementById('sNoteIRSafeHarborFixedRecLoan').readOnly = enableRecLoanColumn;
            document.getElementById('sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan').readOnly = enableRecLoanColumn;
            document.getElementById('sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan').readOnly = enableRecLoanColumn;
            document.getElementById('sEquityCalcSafeHarborFixedRecLoan').readOnly = enableRecLoanColumn;
            document.getElementById('sTransNetCashSafeHarborFixedRecLoan').readOnly = enableRecLoanColumn;
            document.getElementById('sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan').readOnly = enableRecLoanColumn;
        }
        function copyLoanToColumn(num) {
            updateDirtyBit();
            document.getElementById('m_copyToColumn').value = num;
            refreshCalculation();
        }
    </script>
    
    <asp:PlaceHolder ID="scriptHolderForPopup" runat="server" Visible="false">
        <script type="text/javascript">
        
            $(function(){
                $("#bSave").on("click", saveButtonClick)
            });

	        var SHDFPageObject = {};
	        SHDFPageObject.closedByCloseButton = false;
    	    
	        window.onbeforeunload = page_beforeunload;
	        resizeTo(800,700);
    	   
	        function updateDirtyBit_callback() {
                var saveBtn = <%=AspxTools.JsGetElementById(bSave)%>;
                saveBtn.disabled = false;
	        }
	        function clearDirty_callback() {
	            var saveBtn = <%=AspxTools.JsGetElementById(bSave)%>;
	            saveBtn.disabled = true;
	        }
            function closeClick() {  // careful not to name this fxn close.
                PolyShouldShowConfirmSave(isDirty(), onClosePopup, function(){
                    SHDFPageObject.closedByCloseButton = true;
                    saveMe();
                },function(){
                    SHDFPageObject.closedByCloseButton = true;
                    clearDirty();
                });	 
	            return false;
	        }
	        function page_beforeunload() {
	            if (isDirty() && !SHDFPageObject.closedByCloseButton) {
	                return UnloadMessage;
	            }
	        }
	        function saveButtonClick() {
	            saveMe(false);
	        }
        </script>
    </asp:PlaceHolder>
	
    <form id="form1" runat="server">
        <input type="hidden" name="m_copyToColumn" id="m_copyToColumn" value="-1" />
        <table cellSpacing=0 cellPadding=0 width="100%" border=0>
            <tr>
                <td noWrap class="MainRightHeader">
                    Anti-Steering Safe Harbor Disclosure for Brokered Loans - Fixed Rate Loans
                </td>
            </tr>
            <tr>
                <td>
                    <table class="InsetBorder FieldLabel" cellspacing="0">
                        <tr>
                            <td class="FormTableSubheader" colspan="5">
                                Loan Comparison
                            </td>
                        </tr>
                        <tr class="GridHeader">
                            <td></td>
                            <td>Loan with the lowest rate<br/> with risky features</td>
                            <td>Loan with the lowest rate<br/> without risky features</td>
                            <td>Loan with the lowest<br/> total discount points, <br />origination points, <br />or origination fees</td>
                            <td>Our recommended loan</td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Include</td>
                            <td align="center">
                                <asp:RadioButton ID="sIncludeSafeHarborFixedWithRiskyFeaturesYes" GroupName="sIncludeSafeHarborFixedWithRiskyFeatures" Text="Yes" runat="server" onclick="refreshCalculation();" value="1" />
                                <asp:RadioButton ID="sIncludeSafeHarborFixedWithRiskyFeaturesNo" GroupName="sIncludeSafeHarborFixedWithRiskyFeatures" Text="No" runat="server" onclick="refreshCalculation();" value="0" />
                            </td>
                            <td align="center">Yes</td>
                            <td align="center">Yes</td>
                            <td align="center">
                                <asp:RadioButton ID="sIncludeSafeHarborFixedRecLoanYes" GroupName="sIncludeSafeHarborFixedRecLoan" Text="Yes" runat="server" onclick="refreshCalculation();" value="1" />
                                <asp:RadioButton ID="sIncludeSafeHarborFixedRecLoanNo" GroupName="sIncludeSafeHarborFixedRecLoan" Text="No" runat="server" onclick="refreshCalculation();" value="0" />
                            </td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Description</td>
                            <td><asp:TextBox ID="sLpTemplateNmSafeHarborFixedWithRiskyFeatures" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sLpTemplateNmSafeHarborFixedWithoutRiskyFeatures" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sLpTemplateNmSafeHarborFixedLowestCost" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sLpTemplateNmSafeHarborFixedRecLoan" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Loan Amount</td>
                            <td><ml:moneytextbox id="sFinalLAmtSafeHarborFixedWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sFinalLAmtSafeHarborFixedWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sFinalLAmtSafeHarborFixedLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sFinalLAmtSafeHarborFixedRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Due</td>
                            <td><asp:TextBox ID="sDueSafeHarborFixedWithRiskyFeatures" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sDueSafeHarborFixedWithoutRiskyFeatures" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sDueSafeHarborFixedLowestCost" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sDueSafeHarborFixedRecLoan" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Initial interest rate</td>
                            <td><ml:PercentTextBox ID="sNoteIRSafeHarborFixedWithRiskyFeatures" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sNoteIRSafeHarborFixedWithoutRiskyFeatures" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sNoteIRSafeHarborFixedLowestCost" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sNoteIRSafeHarborFixedRecLoan" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Initial monthly amount owed</td>
                            <td style="padding-right:8px"><ml:moneytextbox id="sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td style="padding-right:8px"><ml:moneytextbox id="sGfeProThisMPmtAndMInsSafeHarborFixedWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td style="padding-right:8px"><ml:moneytextbox id="sGfeProThisMPmtAndMInsSafeHarborFixedLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Total discount points, loan origination fees or points</td>
                            <td>
                                <ml:MoneyTextBox ID="sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                            <td>
                                <ml:MoneyTextBox ID="sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                            <td>
                                <ml:MoneyTextBox ID="sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                            <td>
                                <ml:MoneyTextBox ID="sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Closing costs</td>
                            <td><ml:moneytextbox id="sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Down payment needed</td>
                            <td><ml:moneytextbox id="sEquityCalcSafeHarborFixedWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sEquityCalcSafeHarborFixedWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sEquityCalcSafeHarborFixedLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sEquityCalcSafeHarborFixedRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Total funds needed to close</td>
                            <td><ml:moneytextbox id="sTransNetCashSafeHarborFixedWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sTransNetCashSafeHarborFixedWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sTransNetCashSafeHarborFixedLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sTransNetCashSafeHarborFixedRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Our compensation</td>
                            <td><ml:moneytextbox id="sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sOriginatorCompensationTotalAmountSafeHarborFixedWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sOriginatorCompensationTotalAmountSafeHarborFixedLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td></td>
                            <td><input type="button" value="Copy from loan info" onclick="copyLoanToColumn(0);" /></td>
                            <td><input type="button" value="Copy from loan info" onclick="copyLoanToColumn(1);" /></td>
                            <td><input type="button" value="Copy from loan info" onclick="copyLoanToColumn(2);" /></td>
                            <td><input type="button" value="Copy from loan info" onclick="copyLoanToColumn(3);" /></td>
                        </tr>
                    </table>
                    <table class="InsetBorder FieldLabel">
                        <tr>
                            <td class="FormTableSubheader" colspan="2">
                                Risky features
                            </td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox ID="sSafeHarborFixedNegAmort" runat="server" Text="Negative amortization" onclick="refreshCalculation();" /></td>
                            <td><asp:CheckBox ID="sSafeHarborFixedDemandFeat" runat="server" Text="Demand feature" onclick="refreshCalculation();" /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox ID="sSafeHarborFixedPrepayPenalty" runat="server" Text="Prepayment penalty" onclick="refreshCalculation();" /></td>
                            <td><asp:CheckBox ID="sSafeHarborFixedSharedEquity" runat="server" Text="Shared equity" onclick="refreshCalculation();" /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox ID="sSafeHarborFixedIntOnlyPmts" runat="server" Text="Interest-only payments" onclick="refreshCalculation();" /></td>
                            <td><asp:CheckBox ID="sSafeHarborFixedSharedAppreciation" runat="server" Text="Shared appreciation" onclick="refreshCalculation();" /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox ID="sSafeHarborFixedBalloonPmntIn7Yrs" runat="server" Text="Balloon payment within 7 years" onclick="refreshCalculation();" /></td>
                        </tr>
                    </table>
                    <table class="InsetBorder FieldLabel">
                        <tr>
                            <td class="FormTableSubheader">
                                Our recommendation
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="sSafeHarborFixedOurRecLowestRate" runat="server" Text="This is the loan with the lowest rate" onclick="refreshCalculation();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="sSafeHarborFixedOurRecWithoutRiskyFeatures" runat="server" Text="This is the loan with the lowest rate without risky features"  onclick="refreshCalculation();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="sSafeHarborFixedOurRecLowestCosts" runat="server" Text="This is the loan with the lowest closing costs" onclick="refreshCalculation();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="sSafeHarborFixedOurRecOther" runat="server" Text="Other" onclick="refreshCalculation();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="sSafeHarborFixedOurRecText" runat="server" TextMode="MultiLine" Rows="4" Width="400px" onchange="refreshCalculation();"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table class="InsetBorder FieldLabel">
                        <tr>
                            <td class="FormTableSubheader" colspan="2">
                                Prepared by
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
								<uc:CFM ID="CFM" runat="server" />                                
                            </td>
                        </tr>
						<tr>
							<td>Broker Name</td>
							<td><asp:TextBox ID="sSafeHarborFixedAgentName" runat="server" width="280px" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker Identifier</td>
							<td><asp:TextBox ID="sSafeHarborFixedLoanOriginatorIdentifier" width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker's License Number</td>
							<td><asp:TextBox ID="sSafeHarborFixedAgentLicense" runat="server" width="140px" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker's Phone</td>
							<td><ml:phonetextbox id="sSafeHarborFixedPhone" runat="server" width="140px" preset="phone"></ml:phonetextbox></td>
						</tr>
						<tr>
							<td>Broker Company's Name</td>
							<td><asp:TextBox ID="sSafeHarborFixedCompanyName" runat="server" width="280px" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker Company Identifier</td>
							<td><asp:TextBox ID="sSafeHarborFixedCompanyLoanOriginatorIdentifier" width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker Company's License Number</td>
							<td><asp:TextBox ID="sSafeHarborFixedCompanyLicense" runat="server" width="140px" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker Company's Address</td>
							<td><asp:TextBox ID="sSafeHarborFixedCompanyStreetAddr" width="280px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
						    <td></td>
						    <td><asp:TextBox ID="sSafeHarborFixedCompanyCity" width="180px" runat="server" onchange="refreshCalculation();"></asp:TextBox><ml:statedropdownlist id="sSafeHarborFixedCompanyState" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="sSafeHarborFixedCompanyZip" runat="server" width="49" preset="zipcode"></ml:zipcodetextbox></td>
						</tr>
						<tr>
							<td>Broker Company's Phone</td>
							<td><ml:phonetextbox id="sSafeHarborFixedCompanyPhone" runat="server" preset="phone"></ml:phonetextbox> Fax <ml:phonetextbox id="sSafeHarborFixedCompanyFax" runat="server" preset="phone"></ml:phonetextbox></td>
						</tr>
                    </table>
                </td>
            </tr>
        </table>    
        <div>
        <asp:Button OnClientClick="saveButtonClick(); return false;" runat="server" ID="bSave" Enabled="false" Text="Save" />
        <asp:Button OnClientClick="return closeClick();" runat="server" ID="bClose" Text="Close"/>
        </div>
    </form>
</body>
</html>