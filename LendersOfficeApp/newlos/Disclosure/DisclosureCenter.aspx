﻿<%@ Import Namespace="LendersOffice.Common" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="RespaDates" Src="~/newlos/Disclosure/RespaDates.ascx" %>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisclosureCenter.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.DisclosureCenter" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head runat="server">
    <title>Disclosure Center</title>
    <style type="text/css">
        .table {
            display: table;
        }

        .tableRow {
            display: table-row;
        }

        .tableCell {
            display: table-cell;
        }

        #ChangeTpoStatusPopup {
            display: none;
        }

        #ChangeTpoStatusPopup textarea {
            width: 100%;
        }

        tr.tpo-initial-disclosure-row table {
            width: 80%;
        }

        tr.tpo-initial-disclosure-row table td {
            padding-bottom: 5px;
        }


        .leFrontSpacerColumn, .cdFrontSpacerColumn {
            width: 15px;
        }

        .leCreatedDateColumn, .cdCreatedDateColumn {
            width: 120px;
        }

        .leIssuedDateColumn, .cdIssuedDateColumn {
            width: 120px;
        }

        .leDeliveryMethodColumn, .cdDeliveryMethodColumn {
            width: 100px;
        }

        .leReceivedDateColumn, .cdReceivedDateColumn {
            width: 120px;
        }

        .leSignedDateColumn, .cdSignedDateColumn {
            width: 120px;
        }

        .leArchiveUsedColumn, .cdArchiveUsedColumn {
            width: 120px;
        }

        .leInitialColumn, .cdInitialColumn {
            width: 50px;
        }

        .leProductColumn, .cdProductColumn {
            width: 150px;
        }

        .cdUcdFileColumn {
            width: 175px;
        }

        .leDisclosedAprLckdColumn, .cdDisclosedAprLckdColumn {
            width: 25px;
        }

        .leDisclosedAprColumn, .cdDisclosedAprColumn {
            width: 100px;
        }

        .leLqbAprColumn, .cdLqbAprColumn {
            width: 90px;
        }

        .leDocVendorAprColumn, .cdDocVendorAprColumn {
            width: 100px;
        }

        .leBackSpacerColumn, .cdBackSpacerColumn {
            width: 25px;
        }

        .leLastDisclosedTRIDLoanProductDescription {
            width: 130px;
        }

        .cdPreviewColumn {
            width: 70px;
        }

        .cdFinalColumn {
            width: 50px;
        }

        .cdPostClosingColumn {
            width: 80px;
        }

        .cdPostClosingDisclosureReasonColumn {
            width: 220px;
        }

        .cdPostClosingDisclosureDatesColumn {
            width: 200px;
        }

        .cdLastDisclosedTRIDLoanProductDescription {
            width: 130px;
        }

        #LoanEstimatesTable {
            width: 1325px;
        }

        #ClosingDisclosuresTable {
            width: 2050px;
        }

        .hidden {
            display: none;
        }

        .blank-row {
            height: 15px;
        }

        .ImportantDatesTable {
            border-spacing: 15px 0;
        }

        .ImportantDatesTable tr td:nth-child(2) {
            text-align: right;
		}

        .ImportantDatesArea {
            border: 1px;
            border-style: solid;
            border-color:black;
            width: 50%;
            background-color: white;
        }
        
        .Error
        {
            color: red;
        }

        table {
            border-collapse: collapse;
        }

        table.no-border {
            border: 0;
        }

        td {
            padding: 0;
        }

        fieldset.RespaDates {
            width: 50%;
        }

        #AprTable td {
            padding-right: 5px;
        }

        .track-borr-level-dates-btn {
            margin: 4px 0;
            text-align: left;
        }

        .borrower-name {
            text-align: left;
            padding-left: 5px;
            padding-right: 5px;
        }
    </style>
</head>
<body class="RightBackground">
    <script language="javascript" type="text/javascript">
        function _init()
        {
            updateLoanEstimateDatesTable();
            updateClosingDatesTable();
            setTableStriping();
            $('.cdDateRow').not('#cdForCloning').each(function() {
                onUcdAssociationChanged(this);
            });
        }

        function setTableStriping() {
            $('.DatesTable').each(function (index, table) {
                var $rows = $(table).find('tr.leDateRow:not(#leForCloning)');
                if ($rows.length !== 0) {
                    setStripingForRows($rows);
                }

                $rows = $(table).find('tr.cdDateRow:not(#cdForCloning)');
                if ($rows.length !== 0) {
                    setStripingForRows($rows);
                }
            });
        }

        function setStripingForRows($rows) {
            var useGridItemClass = true;

            $rows.each(function (index, row) {
                var $row = $(row);

                // Hidden invalid archive rows should not be
                // striped until they are toggled visible by
                // the "Show Invalid" buttons.
                if ($row.is('.invalidArchive:hidden, .perma-hide')) {
                    return true;
                }

                var nextRow = $row.next();
                while (nextRow.is('.borr-level-dates') && !nextRow.is('.Hidden')) {
                    nextRow.removeClass('GridItem GridAlternatingItem').addClass(useGridItemClass ? 'GridItem' : 'GridAlternatingItem');
                    nextRow = nextRow.next();
                }

                $row.removeClass('GridItem GridAlternatingItem').addClass(useGridItemClass ? 'GridItem' : 'GridAlternatingItem');
                useGridItemClass = !useGridItemClass;
            });
        }

        function addBorrowerLevelTracking(input, forLe) {
            if (!ML.EnableBorrowerLevelDisclosureTracking) {
                return;
            }

            var $inputRow =  $(input).parent().parent();

            var rowClass = forLe ? '.leDateRow' : '.cdDateRow';
            var $dateRow = $inputRow.prev(rowClass);
            var uniqueId = $dateRow.find("input[id$='UniqueId']").val();

            for (var consumerId in ConsumerNamesByConsumerId) {
                var index = new Date().valueOf();
                var consumerName = ConsumerNamesByConsumerId[consumerId];

                var $clone = $('#BorrowerLevelDatesRowForCloning').clone();

                $clone.first('tr').attr('style', '');
                $clone.first('tr').attr('id', '');

                cloneCalendar($clone, '.issued-date', index);
                cloneCalendar($clone, '.received-date', index);
                cloneCalendar($clone, '.signed-date', index);

                $clone.find(':input').not("[preset='date']").each(function () {
                    $(this).removeAttr(gInputInitializedAttr);
                    $(this).find('option').filter(':selected').removeAttr('selected');
                    $(this).find('option').first().attr('selected', 'selected');
                    $(this).not("[id$='ManualID']").prop('checked', false);
                    _initDynamicInput(this);
                });

                $clone.find(":input[preset='date']").on('change keyup', function() {
                    updateDirtyBit();
                });

                $clone.attr('data-unique-id', uniqueId);
                $clone.find('.borrower-name-label').text(consumerName);
                $clone.find('.consumer-id').val(consumerId);
                $clone.find('.exclude-from-calculations-cell').attr('colspan', '15');

                $clone.insertBefore($inputRow);
            }

            $inputRow.remove();
            unlockDateRowFields(forLe, $dateRow);
            updateDirtyBit();
            setTableStriping();
        }

        function unlockDateRowFields(forLe, $dateRow) {
            var inputsToUnlock;
            if (forLe) {
                inputsToUnlock = [
                    { LockboxClass: '.leIssuedDLckd', AssociatedInput: '.leIssuedD' },
                    { LockboxClass: '.leDeliveryLckd', AssociatedInput: '.leDelivery' },
                    { LockboxClass: '.leReceivedDLckd', AssociatedInput: '.leReceivedD' },
                    { LockboxClass: '.leSignedDLckd', AssociatedInput: '.leSignedD' }
                ];
            }
            else {
                inputsToUnlock = [
                    { LockboxClass: '.cdIssuedDLckd', AssociatedInput: '.cdIssuedD' },
                    { LockboxClass: '.cdDeliveryLckd', AssociatedInput: '.cdDelivery' },
                    { LockboxClass: '.cdReceivedDLckd', AssociatedInput: '.cdReceivedD' },
                    { LockboxClass: '.cdSignedDLckd', AssociatedInput: '.cdSignedD' }
                ];
            }

            $.each(inputsToUnlock, function (index, inputToUnlock) {
                var $lockbox = $dateRow.find(inputToUnlock.LockboxClass);
                if (!$lockbox.is(':checked')) {
                    return true;
                }

                var shouldUnlock = false;
                var $input = $dateRow.find(inputToUnlock.AssociatedInput);
                if ($input.is('select')) {
                    shouldUnlock = $input.val() === "0";
                }
                else {
                    shouldUnlock = $input.val() == null || $input.val().trim().length === 0;
                }

                if (shouldUnlock) {
                    $lockbox.prop('checked', false);
                }
            });

            initLockFields($dateRow, false);
        }

        function initLockFields(parentSelector, setHandlers) {
            setupLockField(parentSelector, '.disclosedAprLckd', '.disclosedApr', setHandlers, percent_onblur);

            setupLockField(parentSelector, '.leIssuedDLckd', '.leIssuedD', setHandlers, date_onblur);
            setupLockField(parentSelector, '.leDeliveryLckd', '.leDelivery', setHandlers);
            setupLockField(parentSelector, '.leReceivedDLckd', '.leReceivedD', setHandlers, date_onblur);
            setupLockField(parentSelector, '.leSignedDLckd', '.leSignedD', setHandlers, date_onblur);

            setupLockField(parentSelector, '.cdIssuedDLckd', '.cdIssuedD', setHandlers, date_onblur);
            setupLockField(parentSelector, '.cdDeliveryLckd', '.cdDelivery', setHandlers);
            setupLockField(parentSelector, '.cdReceivedDLckd', '.cdReceivedD', setHandlers, date_onblur);
            setupLockField(parentSelector, '.cdSignedDLckd', '.cdSignedD', setHandlers, date_onblur);
        }

        function setupLockField(parentSelector, lockboxClass, associatedElementClass, setHandlers, blurFunction) {
            // Iterate over the existing checkboxes and lock the textboxes as needed.
            // Then delegate event handling for current / future checkboxes.
            lockFieldBySelector(parentSelector, lockboxClass, associatedElementClass);        

            if (!setHandlers) {
                return;
            }    

            $(parentSelector)
                .on('click', lockboxClass, function () {
                    // Dynamically added rows will re-use the same ids, so we can't use the LockField
                    // override that takes an element id.
                    var $element = $(this).parent().parent().find(associatedElementClass);
                    if ($element.is('select')) {
                        $element.attr('disabled', !this.checked)

                        if (this.checked) {
                            $element.css('background-color', '');
                        }
                    }
                    else {
                        lockFieldByElements(this, $element[0]);
                    }

                    refreshCalculation();
                })
                .on('change', associatedElementClass, function() {
                    if (typeof blurFunction === 'function') {
                        blurFunction(null, this);
                    }
                        
                    refreshCalculation();
                });
        }

        function doAfterDateFormat(jsElement) {
            refreshCalculation();
        }

        function lockFieldBySelector(parentSelector, lockboxClass, associatedElementClass) {
            $(parentSelector).find(lockboxClass).each(function () {
                var $this = $(this);
                if ($this.is('.perma-disable, :disabled')) {
                    return true;
                }

                var $element = $this.parent().parent().find(associatedElementClass);
                if ($element.is('select')) {
                    $element.attr('disabled', !this.checked)

                    if (this.checked) {
                        $element.css('background-color', '');
                    }
                }
                else {
                    // For existing checkboxes, we can use their id.
                    lockField(this, $element.attr('id'));
                }
            })
        }

        function _postSaveMe(results) {
            if (results["RefreshPage"]) {
                window.location = window.location;
            }
        }

        var leTooltip = "Loan Estimates cannot be deleted unless the Archive Used is set to NONE and it is NOT marked as Initial.";
        var cdTooltip = "Closing Disclosure cannot be deleted unless the Archive Used is set to NONE and it is NOT marked as Initial, Preview, or Final.";
        var emptyGuid = "00000000-0000-0000-0000-000000000000";

        var oldRefreshCalculation = window.refreshCalculation;
        window.refreshCalculation = function () {
            populateLoanEstimateDatesJSON();
            populateClosingDisclosureDatesJSON();

            oldRefreshCalculation();

            updateLoanEstimateDatesTable();
            updateClosingDatesTable();
            setTableStriping();
        }

        var oldSaveMe = window.saveMe;
        window.saveMe = function (bRefreshScreen) {
            populateLoanEstimateDatesJSON();
            populateClosingDisclosureDatesJSON();
            return oldSaveMe(bRefreshScreen);
        }

        jQuery(function ($) {
            $('#CompletedStatusWithoutLoanEstimateOnFile').toggle(ML.DisplayCompletedWarning);
            $('tr.tpo-initial-disclosure-row').toggle(ML.DisplayTpoDisclosureRequestsSection);

            $('.tpo-audit-link').click(openTpoPortalDisclosureAudit);

            $('.attached-docs-link').click(displayAttachedDisclosureRequestDoc);

            $(".change-tpo-status").click(displayChangeTpoRequestStatus);

            $('#LoanEstimatesTable').on('change', '.leInitial input', function () {
                onInitialLeCheckboxChange(this, true);
            });

            initLockFields('#LoanEstimatesTable,#ClosingDisclosuresTable', true);

            $('#ClosingDisclosuresTable').on('change', '.cdInitial input', function () {
                onInitialCdCheckboxChange(this, true);
            });

            $('#ClosingDisclosuresTable').on('change', '.cdPreview input', function () {
                var $warningImg = $(this).closest('td').find('img.cdPreviewWarning');
                var isPostClosingRow = $(this).closest('tr').find('span.cdPostClosing input').is(':checked');

                if (this.checked) {
                    $('.cdPreview input').not(this).each(function () {
                        var $warningImg = $(this).closest('td').find('img.cdPreviewWarning');
                        $(this).prop('checked', false);
                        checkIfCanDelete($(this).closest('tr')[0], cdTooltip);
                        $warningImg.hide();
                    });

                    if (isPostClosingRow) {
                        $warningImg.show();
                    }
                }
                else {
                    $warningImg.hide();
                }

                checkIfCanDelete($(this).closest('tr')[0], cdTooltip);
                refreshCalculation();
            });

            $('#ClosingDisclosuresTable').on('change', '.cdFinal input', function () {
                var $warningImg = $(this).closest('td').find('img.cdFinalWarning');
                var isPostClosingRow = $(this).closest('tr').find('span.cdPostClosing input').is(':checked');

                if (this.checked) {
                    $('.cdFinal input').not(this).each(function () {
                        var $warningImg = $(this).closest('td').find('img.cdFinalWarning');
                        $(this).prop('checked', false);
                        checkIfCanDelete($(this).closest('tr')[0], cdTooltip);
                        $warningImg.hide();
                    });

                    if (isPostClosingRow) {
                        $warningImg.show();
                    }
                }
                else {
                    $warningImg.hide();
                }

                checkIfCanDelete($(this).closest('tr')[0], cdTooltip);
                refreshCalculation();
            });

            $('#addLE').click(function () {
                var newRow = cloneRow(
                    '.leDateRow',
                    '#leForCloning',
                    'input.leCreatedD',
                    'input.leIssuedD',
                    'input.leReceivedD',
                    null,
                    null,
                    '#LoanEstimatesTable',
                    'select.leArchive',
                    leTooltip,
                    'input.leSignedD',
                    '#AddBorrowerLevelDatesRowForLeCloning');

                if (document.getElementById('sCalculateInitialLoanEstimate').checked) {
                    newRow.find('span.leInitial').find('input').prop('disabled', true);
                }

                clearInitialCheckboxes(true);
                refreshCalculation();
            });

            $('#addCD').click(function () {
                var newRow = cloneRow(
                    '.cdDateRow',
                    '#cdForCloning',
                    'input.cdCreatedD',
                    'input.cdIssuedD',
                    'input.cdReceivedD',
                    'input.cdPostConsummationRedisclosureReasonDate',
                    'input.cdPostConsummationKnowledgeOfEventDate',
                    '#ClosingDisclosuresTable',
                    'select.cdArchive',
                    cdTooltip,
                    'input.cdSignedD',
                    '#AddBorrowerLevelDatesRowForCdCloning');

                newRow.find('td.postClosingReason')
                    .find('div')
                    .hide();

                newRow.find('td.postClosingDates')
                    .find('div')
                    .hide();

                if (document.getElementById('sCalculateInitialClosingDisclosure').checked) {
                    newRow.find('span.cdInitial').find('input').prop('disabled', true);
                }

                clearInitialCheckboxes(false);
                refreshCalculation();
            });

            $('#toggleInvalidCD').click(function () {
                var isShow = this.value.substr(0, 4) === 'Show';
                var desc = isShow ? 'Hide' : 'Show';
                this.value = desc + ' Invalid Closing Disclosures';

                if (isShow) {
                    $('#ClosingDisclosuresTable .invalidArchive:not(.perma-hide)').show();
                } else {
                    $('#ClosingDisclosuresTable .invalidArchive:not(.perma-hide)').hide();
                }

                toggleInvalidLegend();
                setTableStriping();
            }).click();

            $('#toggleInvalidLE').click(function () {
                var isShow = this.value.substr(0, 4) === 'Show';
                var desc = isShow ? 'Hide' : 'Show';
                this.value = desc + ' Invalid Loan Estimates';

                if (isShow) {
                    $('#LoanEstimatesTable .invalidArchive:not(.perma-hide)').show();
                } else {
                    $('tr.leDateRow.invalidArchive:not(.perma-hide)').each(function (index, element) {
                        var $row = $(element);
                        var isInitial = $row.find('span.leInitial').find('input').is(':checked');
                       
                        if (!isInitial) {
                            var uniqueId = $row.find("input[id$='UniqueId']").val();
                            $('#LoanEstimatesTable').find('tr[data-unique-id="' + uniqueId + '"]').hide();
                            $row.hide();
                        }
                    });
                }

                toggleInvalidLegend();
                setTableStriping();
            }).click();

            $('.DatesTable').on('click', '.removeDates', function () {
                var dateRow = $(this).closest('tr');

                var nextRow = dateRow.next('tr');
                if (nextRow.is('.borr-level-dates') && nextRow.find('.track-borr-level-dates-btn').length !== 0) {
                    nextRow.remove();
                }
                else {
                    var uniqueId = dateRow.find("input[id$='UniqueId']").val();
                    var borrowerLevelDateRows = $('.DatesTable').find('tr[data-unique-id="' + uniqueId + '"]');
                    borrowerLevelDateRows.remove();
                }

                dateRow.remove();

                updateDirtyBit();
                refreshCalculation();
                setTableStriping();
            });

            $('tr.leDateRow').not('#leForCloning').each(function () {
                checkIfCanDelete(this, leTooltip);
            });

            $('tr.cdDateRow').not('#cdForCloning').each(function () {
                checkIfCanDelete(this, cdTooltip);
            });

            $('#CalculateInitAprAndLastDiscAPR').click(function () {
                if (isDirty()) {
                    alert('Please save your changes before attempting to calculate the Initial APR and the Last Disclosed APR.');
                    return;
                }

                var result = gService.loanedit.call('CalculateInitAprAndLastDiscApr', { loanid: ML.sLId });
                if (result.error) {
                    alert('Unable to migrate file. Please try again.');
                }
                else {
                    $('#sInitAPR,#sLastDiscAPR').prop('readonly', true);
                    this.style.display = 'none';
                    $('#sInitAPR').val(result.value.sInitAPR);
                    $('#sLastDiscAPR').val(result.value.sLastDiscAPR);
                }
            });
        });

        function onInitialLeCheckboxChange(initialCheckbox, doRefreshCalculation) {
            var $warningImg = $(initialCheckbox).closest('td').find('img.leInitialWarning');
            var isInvalidRow = $(initialCheckbox).closest('tr').hasClass('invalidArchive');

            if (initialCheckbox.checked) {
                $('.leInitial input').not(initialCheckbox).each(function () {
                    var $warningImg = $(this).closest('td').find('img.leInitialWarning');
                    $(this).prop('checked', false);
                    checkIfCanDelete($(this).closest('tr')[0], leTooltip);
                    $warningImg.hide();
                });

                if (isInvalidRow) {
                    $warningImg.show();
                }
            }
            else {
                $warningImg.hide();
            }

            checkIfCanDelete($(initialCheckbox).closest('tr')[0], leTooltip);

            if (doRefreshCalculation) {
                refreshCalculation();
            }
        }

        function onInitialCdCheckboxChange(initialCheckbox, doRefreshCalculation) {
            var $warningImg = $(initialCheckbox).closest('td').find('img.cdInitialWarning');
            var isPostClosingRow = $(initialCheckbox).closest('tr').find('span.cdPostClosing input').is(':checked');

            if (initialCheckbox.checked) {
                $('.cdInitial input').not(initialCheckbox).each(function () {
                    var $warningImg = $(this).closest('td').find('img.cdInitialWarning');
                    $(this).prop('checked', false);
                    checkIfCanDelete($(this).closest('tr')[0], cdTooltip);
                    $warningImg.hide();
                });

                if (isPostClosingRow) {
                    $warningImg.show();
                }
            }
            else {
                $warningImg.hide();
            }

            checkIfCanDelete($(initialCheckbox).closest('tr')[0], cdTooltip);

            if (doRefreshCalculation) {
                refreshCalculation();
            }
        }

        function checkforInitialDisclosureErrors(disclosureDates, isLe) {
            var numCandidateInitials = 0;
            var currentInitialIsACandidate = false;
            var archiveAppearsMultipleTimes = false;
            var archivesAssociatedWithCandidateInitials = [];

            for (var i = 0; i < disclosureDates.length; i++) {
                var disclosure = disclosureDates[i];
                if (disclosure.isCandidateInitial) {
                    numCandidateInitials++;

                    if (disclosure.isInitial) {
                        currentInitialIsACandidate = true;
                    }

                    if (disclosure.archiveId !== emptyGuid) {
                        if ($.inArray(disclosure.archiveId, archivesAssociatedWithCandidateInitials) !== -1)
                        {
                            archiveAppearsMultipleTimes = true;
                        }
                        else
                        {
                            archivesAssociatedWithCandidateInitials.push(disclosure.archiveId);
                        }
                    }
                }
            }

            var disclosureType = isLe ? "Loan Estimate" : "Closing Disclosure";

            if (archiveAppearsMultipleTimes) {
                return "Unable to determine initial " + disclosureType + " due to multiple rows associated with the same archive. Please remove duplicates or select manually.";
            }
            else if (numCandidateInitials > 1) {
                if (currentInitialIsACandidate) {
                    return "Unable to determine initial " + disclosureType + ". Please ensure the correct initial is selected.";
                } else {
                    return "Unable to determine initial " + disclosureType + ". Please select manually.";
                }
            }

            return '';
        }

        function updateLoanEstimateDatesTable() {
            var loanEstimatesObj = $.parseJSON($('#LoanEstimatesTableJson').val());
            var loanEstimateDates = loanEstimatesObj.loanEstimateDatesList;

            var errorMessage = '';
            if (document.getElementById('sCalculateInitialLoanEstimate').checked)
            {
                errorMessage = checkforInitialDisclosureErrors(loanEstimateDates, true);
            }

            if (errorMessage === '') {
                $('#CalculateInitialLeErrorMessage').text('');
                $('#CalculateInitialLeErrorSection').hide();
            }
            else {
                $('#CalculateInitialLeErrorMessage').text(errorMessage);
                $('#CalculateInitialLeErrorSection').show();
            }

            for (var i = 0; i < loanEstimateDates.length; ++i) {
                var le = loanEstimateDates[i];
                updateLeRow(le);
            }
        }

        function updateLeRow(le) {
            var row;

            $('.leDateRow').not('#leForCloning').each(function () {
                var rowUniqueId = $(this).find("input[id$='leUniqueId']").val();
                if (rowUniqueId === le.uniqueId) {
                    row = this;
                    return false;
                }
            });

            if (!row) {
                throw 'Unable to find loan estimate dates row.'
            }

            var calculateInitial = document.getElementById('sCalculateInitialLoanEstimate').checked;
            var initialShouldBeEnabled = !calculateInitial || le.isCandidateInitial;

            $isInitialCheckbox = $(row).find('span.leInitial input');
            $isInitialCheckbox.prop("disabled", !initialShouldBeEnabled);
            $isInitialCheckbox.prop("checked", le.isInitial);

            var discApr = $(row).find('.disclosedApr')[0];
            discApr.value = le.disclosedApr;
            format_percent(discApr);

            var lqbApr = $(row).find('.lqbApr')[0];
            lqbApr.value = le.lqbApr;
            format_percent(lqbApr);

            var issuedD = $(row).find('.leIssuedD')[0];
            issuedD.value = le.issuedDate;
            format_date(issuedD);

            var delivery = $(row).find('.leDelivery');
            delivery.val(le.deliveryMethod);

            var receivedD = $(row).find('.leReceivedD')[0];
            receivedD.value = le.receivedDate;
            format_date(receivedD);

            var signedD = $(row).find('.leSignedD')[0];
            signedD.value = le.signedDate;
            format_date(signedD);

            $(row).find('img.leLqbAprWarning')
                .toggle(!le.disclosedAprLckd && !le.docVendorApr && !!le.disclosedApr);

            checkIfCanDelete(row, leTooltip);
        }

        function updateClosingDatesTable() {
            var closingDatesObj = $.parseJSON($('#ClosingDisclosuresTableJSON').val());
            var closingDates = closingDatesObj.closingDisclosureDatesList;

            var errorMessage = '';
            if (document.getElementById('sCalculateInitialClosingDisclosure').checked) {
                errorMessage = checkforInitialDisclosureErrors(closingDates, false);
            }

            if (errorMessage === '') {
                $('#CalculateInitialCdErrorMessage').text('');
                $('#CalculateInitialCdErrorSection').hide();
            }
            else {
                $('#CalculateInitialCdErrorMessage').text(errorMessage);
                $('#CalculateInitialCdErrorSection').show();
            }

            for (var i = 0; i < closingDates.length; ++i) {
                var cd = closingDates[i];
                updateCdRow(cd);
            }
        }

        function updateCdRow(cd) {
            var row, $isPostClosingCheckbox, $postClosingReasonCell;

            $('.cdDateRow').not('#cdForCloning').each(function () {
                var rowUniqueId = $(this).find("input[id$='cdUniqueId']").val();
                if (rowUniqueId === cd.uniqueId) {
                    row = this;
                    return false;
                }
            });

            if (!row) {
                throw 'Unable to find closing disclosure dates row.'
            }

            $isPostClosingCheckbox = $(row).find('span.cdPostClosing').children('input').first();
            $isPostClosingCheckbox.prop('checked', cd.IsPostClosing);

            $postClosingReasonCell = $(row).find('td.postClosingReason');
            $postClosingDatesCell = $(row).find('td.postClosingDates');

            if (cd.IsPostClosing)
            {
                $postClosingReasonCell.find('div').show();
                $postClosingDatesCell.find('div').show();
            }
            else
            {
                $postClosingReasonCell.find('div').hide();
                $postClosingDatesCell.find('div').hide();
            }

            var calculateInitial = document.getElementById('sCalculateInitialClosingDisclosure').checked;
            var initialShouldBeEnabled = !calculateInitial || cd.isCandidateInitial;

            $isInitialCheckbox = $(row).find('span.cdInitial input');
            $isInitialCheckbox.prop("disabled", !initialShouldBeEnabled);
            $isInitialCheckbox.prop("checked", cd.isInitial);

            var discApr = $(row).find('.disclosedApr')[0];
            discApr.value = cd.disclosedApr;
            format_percent(discApr);

            var lqbApr = $(row).find('.lqbApr')[0];
            lqbApr.value = cd.lqbApr;
            format_percent(lqbApr);

            var issuedD = $(row).find('.cdIssuedD')[0];
            issuedD.value = cd.issuedDate;
            format_date(issuedD);

            var delivery = $(row).find('.cdDelivery');
            delivery.val(cd.deliveryMethod);

            var receivedD = $(row).find('.cdReceivedD')[0];
            receivedD.value = cd.receivedDate;
            format_date(receivedD);

            var signedD = $(row).find('.cdSignedD')[0];
            signedD.value = cd.signedDate;
            format_date(signedD);

            $(row).find('img.cdLqbAprWarning')
                .toggle(!cd.disclosedAprLckd && !cd.docVendorApr && !!cd.disclosedApr);

            checkIfCanDelete(row, cdTooltip);

            if ($(row).find('span.cdInitial input').is(':checked')) {
                $(row).find('img.cdInitialWarning').toggle(cd.IsPostClosing);
            }
            if ($(row).find('span.cdPreview input').is(':checked')) {
                $(row).find('img.cdPreviewWarning').toggle(cd.IsPostClosing);
            }

            if ($(row).find('span.cdFinal input').is(':checked')) {
                $(row).find('img.cdFinalWarning').toggle(cd.IsPostClosing);
            }
        }

        function clearInitialCheckboxes(isLoanEstimate)
        {
            var className = isLoanEstimate ? '.leInitial' : '.cdInitial';
            var onChangeMethod = isLoanEstimate ? onInitialLeCheckboxChange : onInitialCdCheckboxChange;
            $(className).find('input[type="checkbox"]').each(function () {
                this.checked = false;
                // Execute change handler without performing background calc.
                onChangeMethod(this, false);
            });
        }

        function _postRefreshCalculation(value) {
            setCompletedStatusWarningDisplay(value);
        }

        function _postSaveMe(value) {
            setCompletedStatusWarningDisplay(value);
        }

        function setCompletedStatusWarningDisplay(value) {
            $('#CompletedStatusWithoutLoanEstimateOnFile').toggle(value.DisplayCompletedWarning === "True");
        }

        function openTpoPortalDisclosureAudit() {
            var link = $(this);
            var auditId = link.attr('data-audit-id');
            var needNewAuditPrep = link.attr('data-need-new-audit-prep') === 'true' ? 't' : 'f';
            var url = gVirtualRoot + "/newlos/AuditDetail.aspx?loanid=" + ML.sLId + "&auditid=" + auditId + "&prepForNewAudit=" + needNewAuditPrep;
            window.open(url, "AuditDetail" + auditId, "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes");
        }

        function displayAttachedDisclosureRequestDoc() {
            var docId = $(this).attr('data-doc-id');
            var url = <%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + docId;
            window.open(url);
        }

        function displayChangeTpoRequestStatus() {
            var type = $(this).attr('data-status-type');
            var eventId = $(this).attr('data-event-id');

            var title, currentStatus;
            switch (type) {
                case <%=AspxTools.JsString(TpoDisclosureType.InitialDisclosure)%>:
                    title = 'Initial Disclosure';
                    currentStatus = $('#sTpoRequestForInitialDisclosureGenerationStatusTInt').val();
                    break;
                case <%=AspxTools.JsString(TpoDisclosureType.Redisclosure)%>:
                    title = 'Redisclosure';
                    
                    currentStatus = $('input[data-event-id="' + eventId + '"]')
                        .parent() // td
                        .parent() // tr
                        .find('input[type=hidden]').val();
                    break;
                case <%=AspxTools.JsString(TpoDisclosureType.InitialClosingDisclosure)%>:
                    title = 'Initial Closing Disclosure';
                    currentStatus = $('#sTpoRequestForInitialClosingDisclosureGenerationStatusTInt').val();
                    break;
                default:
                    throw new Error('Unhandled type ' + type);
            }

            $('#ChangeTpoStatusPopup').dialog({
                closeOnEscape: false,
                draggable: false,
                height: 'auto',
                modal: true,
                resizable: false,
                width: 375,
                title: 'Change TPO ' + title + ' Request Status',
                open: function() { getAvailableTpoStatusInitialDisclosureOptions(type, currentStatus); },
                buttons: {
                    'OK': function() { changeTpoDisclosureRequestStatus(type, eventId) },
                    'Cancel': function () { $(this).dialog('close'); }
                }
            });
        }

        function getAvailableTpoStatusInitialDisclosureOptions(type, currentStatus) {
            var args = {
                Type: type,
                CurrentStatus: currentStatus
            };

            var result = gService.loanedit.call('GetAvailableTpoDisclosureStatusOptionsForChange', args);
            if (result.error) {
                alert('Could not determine available options for TPO ' + type + ' status change. Please try again.');
                $('#ChangeTpoStatusPopup').dialog('close');
                return;
            }

            switch (type) {
                case <%=AspxTools.JsString(TpoDisclosureType.InitialDisclosure)%>:
                    $('#RedisclosureRows, #InitialClosingDisclosureRows').hide();
                    $('#InitialDisclosureRows').show();
                    $('#ActiveInitialDisclosureStatusRow').toggle(result.value.EnableActive === "True");
                    $('#CancelledInitialDisclosureStatusRow').toggle(result.value.EnableCancelled === "True");
                    $('#CompletedInitialDisclosureStatusRow').toggle(result.value.EnableCompleted === "True");
                    break;
                case <%=AspxTools.JsString(TpoDisclosureType.Redisclosure)%>:
                    $('#InitialDisclosureRows, #InitialClosingDisclosureRows').hide();
                    $('#RedisclosureRows').show();
                    $('#ActiveRedisclosureStatusRow').toggle(result.value.EnableActive === "True");
                    $('#CancelledRedisclosureStatusRow').toggle(result.value.EnableCancelled === "True");
                    $('#CompletedRedisclosureStatusRow').toggle(result.value.EnableCompleted === "True");
                    break;
                case <%=AspxTools.JsString(TpoDisclosureType.InitialClosingDisclosure)%>:
                    $('#InitialDisclosureRows, #RedisclosureRows').hide();
                    $('#InitialClosingDisclosureRows').show();
                    $('#ActiveInitialClosingDisclosureStatusRow').toggle(result.value.EnableActive === "True");
                    $('#CancelledInitialClosingDisclosureStatusRow').toggle(result.value.EnableCancelled === "True");
                    $('#CompletedInitialClosingDisclosureStatusRow').toggle(result.value.EnableCompleted === "True");
                    break;
                default:
                    throw new Error('Unhandled type ' + type);
            }

            $('#ChangeTpoStatusPopup span:visible input').eq(0).prop('checked', true);
            $('#StatusChangeNotes').val('');
        }

        function changeTpoDisclosureRequestStatus(type, eventId) {
            var args = getAllFormValues();
            args.Type = type;
            args.newStatus = $("input[name='TpoStatusOption']:checked").val();
            args.notes = $("#StatusChangeNotes").val();
            args.EventId = eventId || '';

            var result = gService.loanedit.call('ChangeTpoDisclosureStatus', args);

            $('#ChangeTpoStatusPopup').dialog('close');
            if (result.error) {
                alert('Could not perform Originator Portal disclosure status change. Please try again.');
                return;
            }

            switch (result.value.Type) {
                case <%=AspxTools.JsString(TpoDisclosureType.InitialDisclosure)%>:
                    $('#sTpoRequestForInitialDisclosureGenerationAuditLink')
                        .show()
                        .attr('data-audit-id', result.value.TpoDisclosureAuditId)
                        .attr('data-need-new-audit-prep', true);

                    $('#sTpoRequestForInitialDisclosureGenerationStatusT').val(result.value.TpoDisclosureStatusRep);
                    $('#sTpoRequestForInitialDisclosureGenerationStatusTInt').val(result.value.TpoDisclosureStatusInt);
                    $('#sTpoRequestForInitialDisclosureGenerationStatusD').val(result.value.TpoDisclosureStatusDate);
                    $('#sTpoRequestForInitialDisclosureGenerationStatusNotes').val(result.value.TpoDisclosureStatusNotes);
                    $('#CompletedStatusWithoutLoanEstimateOnFile').toggle(result.value.DisplayCompletedWarning === "True");
                    break;

                case <%=AspxTools.JsString(TpoDisclosureType.Redisclosure)%>:
                    var redisclosureRow = $('input[data-event-id="' + result.value.EventId + '"]')
                        .parent() // td
                        .parent(); // tr

                    redisclosureRow.find('a[id$="RedisclosureAuditLink"]')
                        .show()
                        .attr('data-audit-id', result.value.TpoDisclosureAuditId)
                        .attr('data-need-new-audit-prep', true);

                    redisclosureRow.find('input[id$="RedisclosureStatusT"]').val(result.value.TpoDisclosureStatusRep);
                    redisclosureRow.find('input[id$="RedisclosureStatusTInt"]').val(result.value.TpoDisclosureStatusInt);
                    redisclosureRow.find('input[id$="RedisclosureStatusD"]').val(result.value.TpoDisclosureStatusDate);
                    redisclosureRow.find('textarea[id$="RedisclosureStatusNotes"]').val(result.value.TpoDisclosureStatusNotes);

                    if (result.value.TpoDisclosureDocumentId === emptyGuid) {
                        redisclosureRow.find('a[id$="RedisclosureAttachedDocs"]').hide();
                    }
                    else {
                        redisclosureRow.find('a[id$="RedisclosureAttachedDocs"]').show().attr('data-doc-id', result.value.TpoDisclosureDocumentId);
                    }
                    break;

                case <%=AspxTools.JsString(TpoDisclosureType.InitialClosingDisclosure)%>:
                    $('#sTpoRequestForInitialClosingDisclosureGenerationAuditLink')
                        .show()
                        .attr('data-audit-id', result.value.TpoDisclosureAuditId)
                        .attr('data-need-new-audit-prep', true);

                    $('#sTpoRequestForInitialClosingDisclosureGenerationStatusT').val(result.value.TpoDisclosureStatusRep);
                    $('#sTpoRequestForInitialClosingDisclosureGenerationStatusTInt').val(result.value.TpoDisclosureStatusInt);
                    $('#sTpoRequestForInitialClosingDisclosureGenerationStatusD').val(result.value.TpoDisclosureStatusDate);
                    $('#sTpoRequestForInitialClosingDisclosureGenerationStatusNotes').val(result.value.TpoDisclosureStatusNotes);
                    
                    if (result.value.TpoDisclosureDocumentId === emptyGuid) {
                        $('#sTpoRequestForInitialClosingDisclosureGenerationStatusAttachedDocs').hide();
                    }
                    else {
                        $('#sTpoRequestForInitialClosingDisclosureGenerationStatusAttachedDocs').show().attr('data-doc-id', result.value.TpoDisclosureDocumentId);
                    }
                    break;

                default:
                    throw new Error('Unhandled type ' + result.value.Type);
            }
        }

        function populateLoanEstimateDatesJSON() {
            var ledatesList = [];
            $('tr.leDateRow').not('#leForCloning').each(function () {
                var uniqueId = $(this).find("input[id$='leUniqueId']").val();
                var borrowerLevelDateRows = $('#LoanEstimatesTable').find('tr[data-unique-id="' + uniqueId + '"]');

                ledatesList.push({
                    "createdDate": $(this).find("input.leCreatedD").val(),
                    "issuedDate": $(this).find("input.leIssuedD").val(),
                    "issuedDateLckd": $(this).find('input.leIssuedDLckd').prop('checked'),
                    "deliveryMethod": $(this).find("select.leDelivery").val(),
                    "deliveryMethodLckd": $(this).find('input.leDeliveryLckd').prop('checked'),
                    "isInitial": $(this).find("span.leInitial").children('input').first().is(':checked'),
                    "lastDisclosedTRIDLoanProductDescription": $(this).find('input.leLastDisclosedTRIDLoanProductDescription').val(),
                    "isManual": $(this).find("span.leManual").children('input').first().is(':checked'),
                    "receivedDate": $(this).find("input.leReceivedD").val(),
                    "receivedDateLckd": $(this).find('input.leReceivedDLckd').prop('checked'),
                    "signedDate": $(this).find("input.leSignedD").val(),
                    "signedDateLckd": $(this).find('input.leSignedDLckd').prop('checked'),
                    "archiveId": $(this).find("select.leArchive").val(),
                    "archiveDate": $(this).find("select.leArchive option:selected").text(),
                    "uniqueId": uniqueId,
                    "transactionId": $(this).find("input[id$='leTransactionId']").val(),
                    "disableManualArchiveAssociation": $(this).find("input[id$='leDisableManualArchiveAssociation']").val(),
                    "disclosedAprLckd": $(this).find("input[id$='leDisclosedAprLckd']").first().is(':checked'),
                    "disclosedApr": getRateValue($(this).find("input[id$='leDisclosedApr']").val()),
                    "docVendorApr": getRateValue($(this).find("input[id$='leDocVendorAprFull']").val()),
                    "disclosureDatesByConsumerId": getDisclosureDatesByConsumerId(borrowerLevelDateRows)
                });
            });

            var leDates = {
                "loanEstimateDatesList": ledatesList
            };

            $('#LoanEstimatesTableJson').val(JSON.stringify(leDates));
        }

        function populateClosingDisclosureDatesJSON() {
            var cddatesList = [];

            $('tr.cdDateRow').not('#cdForCloning').each(function () {
                var uniqueId = $(this).find("input[id$='cdUniqueId']").val();
                var borrowerLevelDateRows = $('#ClosingDisclosuresTable').find('tr[data-unique-id="' + uniqueId + '"]');

                var vendorId = $(this).find("input[id$='cdVendorId']").val();
                if (vendorId === '') {
                    vendorId = null;
                }

                cddatesList.push({
                    "createdDate": $(this).find("input.cdCreatedD").val(),
                    "issuedDate": $(this).find("input.cdIssuedD").val(),
                    "issuedDateLckd": $(this).find('input.cdIssuedDLckd').prop('checked'),
                    "deliveryMethod": $(this).find("select.cdDelivery").val(),
                    "deliveryMethodLckd": $(this).find('input.cdDeliveryLckd').prop('checked'),
                    "isInitial": $(this).find("span.cdInitial").children('input').first().is(':checked'),
                    "isManual": $(this).find("span.cdManual").children('input').first().is(':checked'),
                    "isPreview": $(this).find("span.cdPreview").children('input').first().is(':checked'),
                    "isFinal": $(this).find("span.cdFinal").children('input').first().is(':checked'),
                    "lastDisclosedTRIDLoanProductDescription": $(this).find('input.cdLastDisclosedTRIDLoanProductDescription').val(),
                    "receivedDate": $(this).find("input.cdReceivedD").val(),
                    "receivedDateLckd": $(this).find('input.cdReceivedDLckd').prop('checked'),
                    "signedDate": $(this).find("input.cdSignedD").val(),
                    "signedDateLckd": $(this).find('input.cdSignedDLckd').prop('checked'),
                    "archiveId": $(this).find("select.cdArchive").val(),
                    "archiveDate": $(this).find("select.cdArchive option:selected").text(),
                    "uniqueId": uniqueId,
                    "transactionId": $(this).find("input[id$='cdTransactionId']").val(),
                    "disableManualArchiveAssociation": $(this).find("input[id$='cdDisableManualArchiveAssociation']").val(),
                    "ucdDocument": $(this).find("[id$='cdUcdDocumentId']").val(),
                    "isDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower": $(this).find("span.cdPostClosingNumAmtPdByBorr").children('input').first().is(':checked'),
                    "isDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller": $(this).find("span.cdPostClosingNumAmtPdBySell").children('input').first().is(':checked'),
                    "isDisclosurePostClosingDueToNonNumericalClericalError": $(this).find("span.cdPostClosingNonNumClericalErr").children('input').first().is(':checked'),
                    "isDisclosurePostClosingDueToCureForToleranceViolation": $(this).find("span.cdPostClosingCureForToleranceViolation").children('input').first().is(':checked'),
                    "postConsummationRedisclosureReasonDate": $(this).find("input.cdPostConsummationRedisclosureReasonDate").val(),
                    "postConsummationKnowledgeOfEventDate": $(this).find("input.cdPostConsummationKnowledgeOfEventDate").val(),
                    "disclosedAprLckd": $(this).find("input[id$='cdDisclosedAprLckd']").first().is(':checked'),
                    "disclosedApr": getRateValue($(this).find("input[id$='cdDisclosedApr']").val()),
                    "docVendorApr": getRateValue($(this).find("input[id$='cdDocVendorAprFull']").val()),
                    "docCode": $(this).find("input[id$='cdDocCode']").val(),
                    "vendorId": vendorId,
                    "disclosureDatesByConsumerId": getDisclosureDatesByConsumerId(borrowerLevelDateRows)
                });
            });

            var cdDates = {
                "closingDisclosureDatesList": cddatesList
            };

            $('#ClosingDisclosuresTableJSON').val(JSON.stringify(cdDates));
        }

        function getRateValue(rateString) {
            var numericStr = rateString.replace('%', '');

            if ($.trim(numericStr) === '') {
                return null;
            }

            return numericStr;
        }

        function getDisclosureDatesByConsumerId(borrowerLevelDateRows) {
            var results = [];

            borrowerLevelDateRows.each(function(index, row) {
                var $row = $(row);

                var consumerId = $row.find('.consumer-id').val();
                var consumerName = $row.find('.borrower-name-label').text();
                var issuedDate = $row.find('.issued-date').val();
                var deliveryMethod = $row.find('.delivery-method').val();
                var receivedDate = $row.find('.received-date').val();
                var signedDate = $row.find('.signed-date').val();
                var excludeFromCalculations = $row.find('.exclude-from-calculations').prop('checked');

                var borrowerLevelDates = {
                    consumerName: consumerName,
                    excludeFromCalculations: excludeFromCalculations,
                    issuedDate: issuedDate,
                    deliveryMethod: deliveryMethod,
                    receivedDate: receivedDate,
                    signedDate: signedDate
                };

                results.push({
                    key: consumerId,
                    value: borrowerLevelDates
                });
            });

            return results;
        }

        function cloneRow(rowSel, cloneSel, createDSel, issueDSel, recDSel, postClosingReasonDate, postClosingKnowledgeDate, tableSel, archiveSel, tooltip, signedDSelector, borrowerTrackingRowSelector) {
            var index = $(rowSel).length;
            var clone = $(cloneSel).first().clone();

            $(clone).first('tr').attr('style', '');
            $(clone).first('tr').attr('id', '');

            cloneCalendar(clone, createDSel, index);
            cloneCalendar(clone, issueDSel, index);
            cloneCalendar(clone, recDSel, index);
            cloneCalendar(clone, signedDSelector, index);

            if (postClosingReasonDate !== null) {
                cloneCalendar(clone, postClosingReasonDate, index);
            }

            if (postClosingKnowledgeDate !== null) {
                cloneCalendar(clone, postClosingKnowledgeDate, index);
            }

            $(clone).find(':input').not("[preset='date']").each(function () {
                $(this).removeAttr(gInputInitializedAttr);
                $(this).find('option').filter(':selected').removeAttr('selected');
                $(this).find('option').first().attr('selected', 'selected');
                $(this).not("[id$='ManualID']").prop('checked', false);
                _initDynamicInput(this);
            });

            // make sure the new date textboxes update dirty bit
            $(clone).find(":input[preset='date']").on('change keyup', function() {
                updateDirtyBit();
            });

            var result = gService.loanedit.call("GetNewUniqueGuid", {});
            if (!result.error) {
                $(clone).find("[id$='UniqueId']").val(result.value["newGuid"]);
            }

            $(clone).find("input[id$='DisableManualArchiveAssociation']").val('false');

            // Add clone to next top-level row to keep borrowers grouped together
            // with the respective rows.
            var lastRow = $(tableSel + " " + rowSel + ":last");
            while (lastRow.next().is('.borr-level-dates')) {
                lastRow = lastRow.next();
            }

            clone.insertAfter(lastRow);

            var lockboxClasses = [
                '.leIssuedDLckd',
                '.leDeliveryLckd',
                '.leReceivedDLckd',
                '.leSignedDLckd',
                '.cdIssuedDLckd',
                '.cdDeliveryLckd',
                '.cdReceivedDLckd',
                '.cdSignedDLckd'
            ];

            var lockboxes = $(clone).find(lockboxClasses.join());
            lockboxes.prop('checked', true);
            initLockFields(clone, false);

            // Ensure enabled select boxes have their background color
            // removed. This appears to be caused by a conflict between
            // the cloned row and the lock field behavior for setting
            // the background color.
            $(clone).find('.leDelivery, .cdDelivery').css('background-color', '');

            if (ML.EnableBorrowerLevelDisclosureTracking) {
                var addBorrowerTrackingRow = $(borrowerTrackingRowSelector).first().clone();
                addBorrowerTrackingRow.prop('id', '');
                addBorrowerTrackingRow.insertAfter(clone);
            }
            else {
                lockboxes.hide();
            }

            checkIfCanDelete(clone, tooltip);
            updateDirtyBit();

            return clone;
        }

        function cloneCalendar(clone, calendarSelector, index) {
            var createdCalendar = $(clone).find(calendarSelector);
            var newID = $(createdCalendar).prop('id') + index;
            $(createdCalendar).prop('id', newID);

            $(createdCalendar).siblings('a').attr('onclick', '');

            // Rebind handlers. I hope that's all of them.
            $(createdCalendar).siblings('a').bind('click', function () {
                return displayCalendar(newID);
            });

            createdCalendar.removeAttr(gInputInitializedAttr);
            _initDynamicInput(createdCalendar[0], true);
        }

        function toggleInvalidLegend() {
            $('.invalidCDArchiveLegend').toggle($('tr.cdDateRow').filter(':visible').hasClass('invalidArchive'));
            $('.invalidLEArchiveLegend').toggle($('tr.leDateRow').filter(':visible').hasClass('invalidArchive'));
        }

        function onUcdAssociationChanged(control) {
            var $row = $(control).closest('tr');

            var $ucdDocId = $row.find("[id$='cdUcdDocumentId']");
            var hasAssociatedDoc = $ucdDocId.val() !== emptyGuid;

            var $ucdDocCheckbox = $row.find("[id$='cdUcdDocumentLinkedId']");
            var $unlinkedSpan = $row.find("[id$='cdUnlinkedUcdOptionsId']");
            var $linkedSpan = $row.find("[id$='cdLinkedUcdOptionsId']");

            if (hasAssociatedDoc) {
                $ucdDocCheckbox.prop('checked', true);
                $unlinkedSpan.hide();
                $linkedSpan.show();
            } else {
                $ucdDocCheckbox.prop('checked', false);
                $unlinkedSpan.show();
                $linkedSpan.hide();
            }
        }

        function onUploadUcdDocument(control) {
            LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/ElectronicDocs/SingleDocumentUploader.aspx?loanid=" + this.LoanID + "&appid=" + this.ApplicationID + "&restricttoucd=true")) %>, {
                onReturn: function (dialogArgs) {
                    if (dialogArgs.OK) {
                        if(dialogArgs.Document)
                        {
                            var $row = $(control).closest('tr');
                            var $ucdDocId = $row.find("[id$='cdUcdDocumentId']");
                            $ucdDocId.val(dialogArgs.Document.DocumentId);
                            onUcdAssociationChanged(control);
                            updateDirtyBit();
                        }
                    }
                    else {
                        alert("The document could not be uploaded. Please ensure you're uploading a valid UCD file.");
                    }
                },
                hideCloseButton: true
            });
        }

        function onAssociateUcdDocument(control) {
            var associatedIds = [];
            $('tr.cdDateRow').not('#cdForCloning').each(function () {
                var $ucdDocId = $(this).find("[id$='cdUcdDocumentId']");
                if ($ucdDocId.val() !== emptyGuid) {
                    associatedIds.push($ucdDocId.val());
                }
            });

            LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/ElectronicDocs/SingleDocumentPicker.aspx?loanid=" + this.LoanID  + "&restricttoucd=true&nor=true")) %> + "&associateducddocs=" + JSON.stringify(associatedIds), {
                onReturn: function (dialogArgs) {
                    if (dialogArgs.OK) {
                        var $row = $(control).closest('tr');
                        var $ucdDocId = $row.find("[id$='cdUcdDocumentId']");
                        $ucdDocId.val(dialogArgs.Document.DocumentId);
                        onUcdAssociationChanged(control);
                        updateDirtyBit();
                    }
                },
                hideCloseButton: true
            });
        }

        function onViewUcdDocument(control) {
            var $row = $(control).closest('tr');
            var $ucdDocId = $row.find("[id$='cdUcdDocumentId']");
            var documentId = $ucdDocId.val();

            window.open(<%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + documentId , '_self');
        }

        function onDownloadUcdDocument(control) {
            var $row = $(control).closest('tr');
            var $ucdDocId = $row.find("[id$='cdUcdDocumentId']");
            
            // UcdDocumentIdForDownload is outside the repeater for more efficient retrieval in the code behind.
            var $docIdForDownload = $('#UcdDocumentIdForDownload');
            $docIdForDownload.val($ucdDocId.val());
        }

        function onClearUcdDocument(control) {
            var $row = $(control).closest('tr');

            if (confirm("Are you sure you want to clear the UCD file associated with this Closing Disclosure row?")) {
                var $ucdDocId = $row.find("[id$='cdUcdDocumentId']");
                $ucdDocId.val(emptyGuid);
                onUcdAssociationChanged(control);
                updateDirtyBit();
            }

            return false;
        }

        function ArchiveChange(o, tooltip) {
            var $row = $(o).closest('tr');
            var selectedArchiveId = o.value;
            var isArchiveSelected = selectedArchiveId !== emptyGuid;
            var isSelectedArchiveDisclosed = false;

            if (isArchiveSelected) {
                isSelectedArchiveDisclosed = ArchiveIsDisclosedById[o.value];
            }

            checkIfCanDelete($row[0], tooltip);

            if ($row.hasClass('validate')) {
                var uniqueId = $row.find("input[id$='UniqueId']").val();
                var $borrowerLevelRows = $('.DatesTable').find('tr[data-unique-id="' + uniqueId + '"]');
                var $combinedRows = $row.add($borrowerLevelRows);

                if (!isArchiveSelected || isSelectedArchiveDisclosed) {
                    $row.removeClass('invalidArchive');
                    $borrowerLevelRows.removeClass('invalidArchive');

                    $row.find('img.associatedWithInvalidArchive').hide();
                    $row.find('img.warning').hide();

                    $combinedRows
                        .find('input.readonlyForInvalidArchives')
                        .not('.systemGenerated, .perma-disable')
                        .prop('readonly', false)
                        .prop('disabled', false);

                    $combinedRows
                        .find('span.readonlyForInvalidArchives, span.readonlyForInvalidArchives input')
                        .prop('disabled', false);

                    $combinedRows
                        .find('select.readonlyForInvalidArchives')
                        .prop('disabled', false)
                        .removeAttr('style');

                } else {
                    $row.addClass('invalidArchive');
                    $borrowerLevelRows.addClass('invalidArchive');

                    $row.find('img.associatedWithInvalidArchive').show();

                    $row.find('span.leInitial input').prop('checked', false);
                    $row.find('span.cdInitial input').prop('checked', false);
                    $row.find('span.cdPreview input').prop('checked', false);
                    $row.find('span.cdFinal input').prop('checked', false);

                    $combinedRows
                        .find('input.readonlyForInvalidArchives')
                        .not('.systemGenerated, .perma-disable')
                        .prop('readonly', true)
                        .prop('disabled', true);
                        
                    $combinedRows
                        .find('span.readonlyForInvalidArchives, span.readonlyForInvalidArchives input')
                        .prop('disabled', true);

                    $combinedRows
                        .find('select.readonlyForInvalidArchives')
                        .prop('disabled', true);

                    if ($row.find('span.leInitial').find('input').is(':checked')) {
                        $row.find('img.leInitialWarning').show();
                    }
                }

                initLockFields($row, false);
            } else if (isArchiveSelected && !isSelectedArchiveDisclosed) {
                throw 'All new rows should be associated with disclosed archive or no archive.';
            }

            toggleInvalidLegend();
            refreshCalculation();
        }

        function checkIfCanDelete(parentRow, tooltip) {
            var isInitial = $(parentRow).find("[id$='InitialID']")[0];
            var isPreview = $(parentRow).find("[id$='PreviewID']")[0];
            var isFinal = $(parentRow).find("[id$='FinalID']")[0];
            var archiveGuid = $(parentRow).find("[id$='ArchiveID']");

            var button = $(parentRow).find('.removeDates');

            var isLoanEstimate = isFinal === undefined; // only CDs have the Final checkbox
            var calculateInitial = isLoanEstimate
                ? document.getElementById('sCalculateInitialLoanEstimate')
                : document.getElementById('sCalculateInitialClosingDisclosure');
                

            if ((isInitial.checked && !calculateInitial.checked) ||
               (isPreview && isPreview.checked) ||
               (isFinal && isFinal.checked) ||
               archiveGuid.val() != emptyGuid) {
                // If any of these are true, then can't delete.
                button.prop('disabled', true);
                button.prop('title', tooltip);
            }
            else {
                button.prop('disabled', false);
                button.prop('title', "");
            }
        }

        function ClearRowCheckboxes(el) {
            $(el).closest("tr.[class$='cdDateRow']").find(':checkbox').not(el).not('[id$="cdManualID"]').prop('checked', false);
        }
    </script>
    <form id="DisclosureCenter" method="post" runat="server">
        <div id="ChangeTpoStatusPopup">
            New status: 
            <br />
            <br />

            <div id="InitialDisclosureRows">
                <span id="ActiveInitialDisclosureStatusRow">
                    <input id="InitialDisclosureRequestedStatus" name="TpoStatusOption" type="radio" doesntdirty="true" value="<%=AspxTools.JsNumeric(LendersOffice.ObjLib.TPO.TpoRequestForInitialDisclosureGenerationEventType.Requested)%>" />
                    Active 
                <br />
                </span>

                <span id="CancelledInitialDisclosureStatusRow">
                    <input id="InitialDisclosureCancelledStatus" name="TpoStatusOption" type="radio" doesntdirty="true" value="<%=AspxTools.JsNumeric(LendersOffice.ObjLib.TPO.TpoRequestForInitialDisclosureGenerationEventType.Cancelled)%>" />
                    Cancelled 
                <br />
                </span>

                <span id="CompletedInitialDisclosureStatusRow">
                    <input id="InitialDisclosureCompletedStatus" name="TpoStatusOption" type="radio" doesntdirty="true" value="<%=AspxTools.JsNumeric(LendersOffice.ObjLib.TPO.TpoRequestForInitialDisclosureGenerationEventType.Completed)%>" />
                    Completed 
                <br />
                </span>
            </div>
            <div id="RedisclosureRows">
                <span id="ActiveRedisclosureStatusRow">
                    <input id="RedisclosureRequestedStatus" name="TpoStatusOption" type="radio" doesntdirty="true" value="<%=AspxTools.JsNumeric(LendersOffice.ObjLib.TPO.TpoRequestForRedisclosureGenerationEventType.Active)%>" />
                    Active 
                <br />
                </span>

                <span id="CancelledRedisclosureStatusRow">
                    <input id="RedisclosureCancelledStatus" name="TpoStatusOption" type="radio" doesntdirty="true" value="<%=AspxTools.JsNumeric(LendersOffice.ObjLib.TPO.TpoRequestForRedisclosureGenerationEventType.Cancelled)%>" />
                    Cancelled 
                <br />
                </span>

                <span id="CompletedRedisclosureStatusRow">
                    <input id="RedisclosureCompletedStatus" name="TpoStatusOption" type="radio" doesntdirty="true" value="<%=AspxTools.JsNumeric(LendersOffice.ObjLib.TPO.TpoRequestForRedisclosureGenerationEventType.Completed)%>" />
                    Completed 
                <br />
                </span>
            </div>
            <div id="InitialClosingDisclosureRows">
                <span id="ActiveInitialClosingDisclosureStatusRow">
                    <input id="InitialClosingDisclosureRequestedStatus" name="TpoStatusOption" type="radio" doesntdirty="true" value="<%=AspxTools.JsNumeric(LendersOffice.ObjLib.TPO.TpoRequestForInitialClosingDisclosureGenerationEventType.Active)%>" />
                    Active 
                <br />
                </span>

                <span id="CancelledInitialClosingDisclosureStatusRow">
                    <input id="InitialClosingDisclosureCancelledStatus" name="TpoStatusOption" type="radio" doesntdirty="true" value="<%=AspxTools.JsNumeric(LendersOffice.ObjLib.TPO.TpoRequestForInitialClosingDisclosureGenerationEventType.Cancelled)%>" />
                    Cancelled 
                <br />
                </span>

                <span id="CompletedInitialClosingDisclosureStatusRow">
                    <input id="InitialClosingDisclosureCompletedStatus" name="TpoStatusOption" type="radio" doesntdirty="true" value="<%=AspxTools.JsNumeric(LendersOffice.ObjLib.TPO.TpoRequestForInitialClosingDisclosureGenerationEventType.Completed)%>" />
                    Completed 
                <br />
                </span>
            </div>

            <br />

            Notes:
            <br />
            <br />
            <textarea id="StatusChangeNotes" rows="8" cols="10" doesntDirty="true"></textarea>
        </div>
        <table class="FormTable">
            <tr>
                <td class="MainRightHeader no-wrap">Disclosure Center</td>
            </tr>
            <tr class="tpo-initial-disclosure-row">
                <td>
                    <table>
                        <tr>
                            <td colspan="7" class="LoanFormHeader">Originator Portal Disclosure Requests</td>
                        </tr>
                        <tr>
                            <td class="FormTableSubHeader">Type</td>
                            <td colspan="2" class="FormTableSubHeader">Status</td>
                            <td class="FormTableSubHeader">Timestamp</td>
                            <td class="FormTableSubHeader">Notes</td>
                            <td class="FormTableSubHeader">Attached Docs</td>
                            <td class="FormTableSubHeader">&nbsp</td>
                        </tr>
                        <tr>
                            <td>Initial Disclosure Request from Originator Portal User</td>
                            <td>
                                <input type="text" id="sTpoRequestForInitialDisclosureGenerationStatusT" runat="server" readonly="readonly" />
                                <input type="hidden" id="sTpoRequestForInitialDisclosureGenerationStatusTInt" runat="server" />
                            </td>
                            <td>
                                <input type="button" class="change-tpo-status" id="ChangeTpoRequestStatusButton" value="Change Status" data-status-type="<%=AspxTools.JsNumeric(TpoDisclosureType.InitialDisclosure)%>" />
                            </td>
                            <td>
                                <input type="text" id="sTpoRequestForInitialDisclosureGenerationStatusD" runat="server" readonly="readonly" />
                            </td>
                            <td>
                                <textarea id="sTpoRequestForInitialDisclosureGenerationStatusNotes" runat="server" rows="2" cols="55"></textarea>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <a id="sTpoRequestForInitialDisclosureGenerationAuditLink" runat="server" data-need-new-audit-prep="false" class="tpo-audit-link">view more</a>
                            </td>
                        </tr>
                        <tr>
                            <asp:Repeater runat="server" ID="RedisclosureRequests" OnItemDataBound="RedisclosureRequests_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td>Redisclosure Request from Originator Portal User</td>
                                        <td>
                                            <input type="text" id="RedisclosureStatusT" runat="server" readonly="readonly" />
                                            <input type="hidden" id="RedisclosureStatusTInt" runat="server" />
                                        </td>
                                        <td>
                                            <input type="button" class="change-tpo-status" id="ChangeTpoRedisclosureStatusButton" value="Change Status" runat="server" />
                                        </td>
                                        <td>
                                            <input type="text" id="RedisclosureStatusD" runat="server" readonly="readonly" />
                                        </td>
                                        <td>
                                            <textarea id="RedisclosureStatusNotes" runat="server" rows="2" cols="55"></textarea>
                                        </td>
                                        <td>
                                            <a id="RedisclosureAttachedDocs" runat="server" class="attached-docs-link">CoC Request Form</a>
                                        </td>
                                        <td>
                                            <a id="RedisclosureAuditLink" runat="server" data-need-new-audit-prep="false" class="tpo-audit-link">view more</a>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                        <tr>
                            <td>Initial Closing Disclosure Request from Originator Portal User</td>
                            <td>
                                <input type="text" id="sTpoRequestForInitialClosingDisclosureGenerationStatusT" runat="server" readonly="readonly" />
                                <input type="hidden" id="sTpoRequestForInitialClosingDisclosureGenerationStatusTInt" runat="server" />
                            </td>
                            <td>
                                <input type="button" class="change-tpo-status" id="ChangeTpoRedisclosureRequestStatusButton" value="Change Status" data-status-type="<%=AspxTools.JsNumeric(TpoDisclosureType.InitialClosingDisclosure)%>" />
                            </td>
                            <td>
                                <input type="text" id="sTpoRequestForInitialClosingDisclosureGenerationStatusD" runat="server" readonly="readonly" />
                            </td>
                            <td>
                                <textarea id="sTpoRequestForInitialClosingDisclosureGenerationStatusNotes" runat="server" rows="2" cols="55"></textarea>
                            </td>
                            <td>
                                <a id="sTpoRequestForInitialClosingDisclosureGenerationStatusAttachedDocs" runat="server" class="attached-docs-link">Initial CD Request Form</a>
                            </td>
                            <td>
                                <a id="sTpoRequestForInitialClosingDisclosureGenerationAuditLink" runat="server" data-need-new-audit-prep="false" class="tpo-audit-link">view more</a>
                            </td>
                        </tr>
                        <tr id="CompletedStatusWithoutLoanEstimateOnFile">
                            <td colspan="5">
                                <img src="../../images/fail.png" alt="Warning" />
                                <span class="FieldLabel Error">Initial Disclosure request status is "Completed" but no Loan Estimate event exists on file.</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="AprTable">
                        <tr>
                            <td class="FieldLabel">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Initial APR</td>
                            <td><ml:PercentTextBox runat="server" ID="sInitAPR" ReadOnly="true"></ml:PercentTextBox></td>
                            <td class="FieldLabel">Last Disclosed APR</td>
                            <td><ml:PercentTextBox runat="server" ID="sLastDiscAPR" ReadOnly="true"></ml:PercentTextBox></td>
                            <td><input type="button" id="CalculateInitAprAndLastDiscAPR" value="Calculate Initial and Last Disclosed APR" runat="server"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField runat="server" ID="LoanEstimatesTableJson" />
                    <div runat="server" id="LoanEstimateandClosingDiscDiv">
                        <table id="LoanEstimatesTable" class="DatesTable no-border">
                            <tr>
                                <td class="FieldLabel">&nbsp;
                                </td>
                                <td colspan="3"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="LoanFormHeader" colspan="14" rowspan="1">Loan Estimates
                                </td>
                            </tr>
                            <tr>
                                <td class="FormTableSubHeader leFrontSpacerColumn"></td>
                                <td class="FormTableSubHeader leCreatedDateColumn">Created Date</td>
                                <td class="FormTableSubHeader leIssuedDateColumn">Issued Date</td>
                                <td class="FormTableSubHeader leDeliveryMethodColumn">Delivery Method</td>
                                <td class="FormTableSubHeader leReceivedDateColumn">Received Date</td>
                                <td class="FormTableSubHeader leSignedDateColumn">Signed Date</td>
                                <td class="FormTableSubHeader leArchiveUsedColumn">Archive Used</td>
                                <td class="FormTableSubHeader leInitialColumn">Initial</td>
                                <td class="FormTableSubHeader leProductColumn">Product</td>
                                <td class="FormTableSubHeader leDisclosedAprLckdColumn"></td>
                                <td class="FormTableSubHeader leDisclosedAprColumn">Disclosed APR</td>
                                <td class="FormTableSubHeader leLqbAprColumn">LQB APR</td>
                                <td class="FormTableSubHeader leDocVendorAprColumn">Doc Vendor APR</td>
                                <td class="FormTableSubHeader leBackSpacerColumn"></td>
                            </tr>
                            <tr style="display: none;" class="leDateRow" id="leForCloning">
                                <td></td>                                
                                <td class="no-wrap">
                                    <asp:HiddenField runat="server" ID="leUniqueId" Value="00000000-0000-0000-0000-000000000000" />
                                    <asp:HiddenField runat="server" ID="leTransactionId" />
                                    <ml:DateTextBox runat="server" class="leCreatedD" ID="leCreatedDID" preset="date"></ml:DateTextBox>
                                    <asp:CheckBox Checked='true' Style="display: none;" runat="server" class="leManual" ID="leManualID" />
                                </td>
                                <td class="no-wrap">
                                    <ml:DateTextBox runat="server" class="leIssuedD" ID="leIssuedDID" preset="date"></ml:DateTextBox>
                                    <input type="checkbox" runat="server" id="leIssuedDLckdId" class="leIssuedDLckd" />
                                </td>
                                <td class="no-wrap">
                                    <asp:DropDownList runat="server" class="leDelivery" ID="leDeliveryID"></asp:DropDownList>
                                    <input type="checkbox" runat="server" id="leDeliveryLckdId" class="leDeliveryLckd" />
                                </td>
                                <td class="no-wrap">
                                    <ml:DateTextBox runat="server" class="leReceivedD" ID="leReceivedDID" preset="date"></ml:DateTextBox>
                                    <input type="checkbox" runat="server" id="leReceivedDLckdId" class="leReceivedDLckd" />
                                </td>
                                <td class="no-wrap">
                                    <ml:DateTextBox runat="server" class="leSignedD" ID="leSignedDID" preset="date"></ml:DateTextBox>
                                    <input type="checkbox" runat="server" id="leSignedDLckdId" class="leSignedDLckd" />
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="leDisableManualArchiveAssociation" />
                                    <asp:DropDownList runat="server" class="leArchive" ID="leArchiveID" onchange="ArchiveChange(this, leTooltip);">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" class="leInitial" ID="leInitialID" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="leLastDisclosedTRIDLoanProductDescription" ID="leLastDisclosedTRIDLoanProductDescriptionID" />
                                </td>
                                <td>
                                    <input type="checkbox" runat="server" ID="leDisclosedAprLckd" class="disclosedAprLckd" />
                                </td>
                                <td>
                                    <ml:PercentTextBox runat="server" ID="leDisclosedApr" CssClass="disclosedApr" ReadOnly="true"></ml:PercentTextBox>
                                    <img src="../../images/warn.png" alt="warning" class="warning leLqbAprWarning" runat="server" id="leLqbAprWarning" style="display: none;" title="Please check the actual disclosure to ensure this value is correct." />
                                </td>
                                <td>
                                    <ml:PercentTextBox runat="server" ID="leLqbApr" CssClass="lqbApr" ReadOnly="true"></ml:PercentTextBox>
                                </td>
                                <td>
                                    <ml:PercentTextBox runat="server" ID="leDocVendorApr" ReadOnly="true"></ml:PercentTextBox>
                                    <asp:HiddenField runat="server" ID="leDocVendorAprFull" />
                                </td>
                                <td>
                                    <input type="button" id="removeLE" value="-" class="removeDates" />
                                </td>
                            </tr>
                            <asp:Repeater runat="server" ID="LoanEstimatesDates" OnItemDataBound="LoanEstD_ItemDataBound">
                                <ItemTemplate>
                                    <tr class="leDateRow" runat="server" id="leDateRowID">
                                        <td>
                                            <img runat="server" id="leDateRowAssociatedWithInvalidArchive" class="associatedWithInvalidArchive" src="../../images/fail.png" alt="Loan Estimate associated with invalid archive." />
                                        </td>
                                        <td class="no-wrap">
                                            <asp:HiddenField runat="server" ID="leUniqueId" />
                                            <asp:HiddenField runat="server" ID="leTransactionId" />
                                            <ml:DateTextBox runat="server" class="leCreatedD readonlyForInvalidArchives" ID="leCreatedDID" preset="date"></ml:DateTextBox>
                                            <asp:CheckBox Style="display: none;" runat="server" class="leManual" ID="leManualID" />
                                        </td>
                                        <td class="no-wrap">
                                            <ml:DateTextBox runat="server" class="leIssuedD readonlyForInvalidArchives" ID="leIssuedDID" preset="date"></ml:DateTextBox>
                                            <input type="checkbox" runat="server" id="leIssuedDLckdId" class="leIssuedDLckd readonlyForInvalidArchives" />
                                        </td>
                                        <td class="no-wrap">
                                            <asp:DropDownList runat="server" class="leDelivery readonlyForInvalidArchives" ID="leDeliveryID"></asp:DropDownList>
                                            <input type="checkbox" runat="server" id="leDeliveryLckdId" class="leDeliveryLckd readonlyForInvalidArchives" />
                                        </td>
                                        <td class="no-wrap">
                                            <ml:DateTextBox runat="server" class="leReceivedD readonlyForInvalidArchives" ID="leReceivedDID" preset="date"></ml:DateTextBox>
                                            <input type="checkbox" runat="server" id="leReceivedDLckdId" class="leReceivedDLckd readonlyForInvalidArchives" />
                                        </td>
                                        <td class="no-wrap">
                                            <ml:DateTextBox runat="server" class="leSignedD readonlyForInvalidArchives" ID="leSignedDID" preset="date"></ml:DateTextBox>
                                            <input type="checkbox" runat="server" id="leSignedDLckdId" class="leSignedDLckd readonlyForInvalidArchives" />
                                        </td>
                                        <td>
                                            <asp:HiddenField runat="server" ID="leDisableManualArchiveAssociation" />
                                            <asp:DropDownList runat="server" class="leArchive" ID="leArchiveID" onchange="ArchiveChange(this, leTooltip);">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" class="leInitial readonlyForInvalidArchives" ID="leInitialID" />
                                            <img src="../../images/warn.png" alt="warning" class="warning leInitialWarning" runat="server" id="leInitialWarningID" title="This Loan Estimate is marked as invalid, please review." />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" class="leLastDisclosedTRIDLoanProductDescription" ID="leLastDisclosedTRIDLoanProductDescriptionID" />
                                        </td>
                                        <td>
                                            <input type="checkbox" runat="server" ID="leDisclosedAprLckd" class="disclosedAprLckd" />
                                        </td>
                                        <td>
                                            <ml:PercentTextBox runat="server" ID="leDisclosedApr" CssClass="disclosedApr"></ml:PercentTextBox>
                                            <img src="../../images/warn.png" alt="warning" class="warning leLqbAprWarning" runat="server" id="leLqbAprWarning" style="display: none;" title="Please check the actual disclosure to ensure this value is correct." />
                                        </td>
                                        <td>
                                            <ml:PercentTextBox runat="server" ID="leLqbApr" CssClass="lqbApr" ReadOnly="true"></ml:PercentTextBox>
                                        </td>
                                        <td>
                                            <ml:PercentTextBox runat="server" ID="leDocVendorApr" ReadOnly="true"></ml:PercentTextBox>
                                            <asp:HiddenField runat="server" ID="leDocVendorAprFull" />
                                        </td>
                                        <td>
                                            <input type="button" id="removeLE" value="-" class="removeDates" runat="server" />
                                        </td>
                                    </tr>

                                    <asp:Repeater runat="server" ID="LoanEstimateBorrowerLevelDates" OnItemDataBound="BorrowerLevelDates_ItemDataBound">
                                        <ItemTemplate>
                                            <tr id="BorrowerLevelDatesRow" runat="server" class="borr-level-dates">
                                                <td></td>
                                                <td class="borrower-name">
                                                    <input type="hidden" runat="server" ID="ConsumerId" class="consumer-id" />
                                                    <label class="FieldLabel borrower-name-label" runat="server" id="BorrowerName">&nbsp;</label>
                                                </td>
                                                <td>
                                                    <ml:DateTextBox runat="server" ID="IssuedDate" class="issued-date readonlyForInvalidArchives" preset="date"></ml:DateTextBox>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="DeliveryMethod" CssClass="delivery-method readonlyForInvalidArchives"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <ml:DateTextBox runat="server" ID="ReceivedDate" class="received-date readonlyForInvalidArchives" preset="date"></ml:DateTextBox>
                                                </td>
                                                <td runat="server" id="SignedDateCell">
                                                    <ml:DateTextBox runat="server" ID="SignedDate" class="signed-date readonlyForInvalidArchives" preset="date"></ml:DateTextBox>
                                                </td>
                                                <td runat="server" id="ExcludeFromCalculationsCell" class="exclude-from-calculations-cell">
                                                    <label class="FieldLabel">
                                                        Exclude from calculations?
                                                    </label>
                                                    <input type="checkbox" runat="server" id="ExcludeFromCalculations" class="exclude-from-calculations readonlyForInvalidArchives" onchange="refreshCalculation()" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                    <tr runat="server" id="AddBorrowerLevelDatesRow" class="borr-level-dates">
                                        <td></td>
                                        <td>
                                            <input type="button" value="Track Borrower Dates" class="track-borr-level-dates-btn" onclick="addBorrowerLevelTracking(this, true);" />
                                        </td>
                                        <td colspan="20">&nbsp;</td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr>
                                <td colspan="8">
                                    <input nohighlight type="button" id="addLE" value="Add Loan Estimate" />
                                    <input nohighlight type="button" value="Hide Invalid Loan Estimates" id="toggleInvalidLE" />
                                    <label class="FieldLabel" for="sCalculateInitialLoanEstimate">Calculate Initial Loan Estimate?</label>
                                    <asp:CheckBox ID="sCalculateInitialLoanEstimate" runat="server" onclick="clearInitialCheckboxes(true); refreshCalculation();" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <span class="invalidLEArchiveLegend">
                                        <img src="../../images/fail.png" alt="Loan Estimate associated with invalid archive." />
                                        Associated with an undisclosed archive
                                    </span>
                                </td>
                            </tr>
                            <tbody id="CalculateInitialLeErrorSection" class="hidden" runat="server">
                                <tr>
                                    <td colspan="8">
                                        <img src="../../images/fail.png" alt="Initial Loan Estimate Calculation Error" />
                                        <ml:EncodedLabel class="Error FieldLabel" ID="CalculateInitialLeErrorMessage" runat="server"></ml:EncodedLabel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <asp:HiddenField runat="server" ID="ClosingDisclosuresTableJSON" />
                        <table id="ClosingDisclosuresTable" class="DatesTable no-border">
                            <tr>
                                <td class="FieldLabel">&nbsp;
                                </td>
                                <td colspan="3"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="LoanFormHeader" colspan="20" rowspan="1">Closing Disclosures
                                </td>
                            </tr>
                            <tr runat="server" id="cdTableHeaders">
                                <td class="FormTableSubHeader cdFrontSpacerColumn"></td>
                                <td class="FormTableSubHeader cdCreatedDateColumn">Created Date</td>
                                <td class="FormTableSubHeader cdIssuedDateColumn">Issued Date</td>
                                <td class="FormTableSubHeader cdDeliveryMethodColumn">Delivery Method</td>
                                <td class="FormTableSubHeader cdReceivedDateColumn">Received Date</td>
                                <td class="FormTableSubHeader cdSignedDateColumn">Signed Date</td>
                                <td class="FormTableSubHeader cdArchiveUsedColumn">Archive Used</td>
                                <td class="FormTableSubHeader cdInitialColumn">Initial</td>
                                <td class="FormTableSubHeader cdPreviewColumn">Preview</td>
                                <td class="FormTableSubHeader cdFinalColumn">Final</td>
                                <td class="FormTableSubHeader cdProductColumn">Product</td>
                                <td class="FormTableSubHeader cdUcdFileColumn">UCD File</td>
                                <td class="FormTableSubHeader cdDisclosedAprLckdColumn"></td>
                                <td class="FormTableSubHeader cdDisclosedAprColumn">Disclosed APR</td>
                                <td class="FormTableSubHeader cdLqbAprColumn">LQB APR</td>
                                <td class="FormTableSubHeader cdDocVendorAprColumn">Doc Vendor APR</td>
                                <td class="FormTableSubHeader cdPostClosingColumn">Post-Closing</td>
                                <td class="FormTableSubHeader cdPostClosingDisclosureReasonColumn">Post-Closing Disclosure Reason</td>
                                <td class="FormTableSubHeader cdPostClosingDisclosureDatesColumn">Post-Closing Disclosure Dates</td>
                                <td class="FormTableSubHeader cdBackSpacerColumn"></td>
                            </tr>
                            <tr style="display: none;" class="cdDateRow" id="cdForCloning">
                                <td></td>
                                <td class="no-wrap">
                                    <asp:HiddenField runat="server" ID="cdUniqueId" Value="00000000-0000-0000-0000-000000000000" />
                                    <asp:HiddenField runat="server" ID="cdTransactionId" />
                                    <asp:HiddenField runat="server" ID="cdDocCode" />
                                    <asp:HiddenField runat="server" ID="cdVendorId" />
                                    <ml:DateTextBox runat="server" class="cdCreatedD" ID="cdCreatedDID" preset="date"></ml:DateTextBox>
                                    <asp:CheckBox Checked="true" Style="display: none;" runat="server" class="cdManual" ID="cdManualID" />
                                </td>
                                <td class="no-wrap">
                                    <ml:DateTextBox runat="server" class="cdIssuedD" ID="cdIssuedDID" preset="date"></ml:DateTextBox>
                                    <input type="checkbox" runat="server" id="cdIssuedDLckdId" class="cdIssuedDLckd" />
                                </td>
                                <td class="no-wrap">
                                    <asp:DropDownList runat="server" class="cdDelivery" ID="cdDeliveryID"></asp:DropDownList>
                                    <input type="checkbox" runat="server" id="cdDeliveryLckdId" class="cdDeliveryLckd" />
                                </td>
                                <td class="no-wrap">
                                    <ml:DateTextBox runat="server" class="cdReceivedD" ID="cdReceivedDID" preset="date"></ml:DateTextBox>
                                    <input type="checkbox" runat="server" id="cdReceivedDLckdId" class="cdReceivedDLckd" />
                                </td>
                                <td class="no-wrap">
                                    <ml:DateTextBox runat="server" class="cdSignedD" ID="cdSignedDID" preset="date"></ml:DateTextBox>
                                    <input type="checkbox" runat="server" id="cdSignedDLckdId" class="cdSignedDLckd" />
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="cdDisableManualArchiveAssociation" Value="false" />
                                    <asp:DropDownList runat="server" class="cdArchive" ID="cdArchiveID" onchange="ArchiveChange(this, cdTooltip);">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" class="cdInitial" ID="cdInitialID" />
                                    <img src="../../images/warn.png" alt="warning" class="warning cdInitialWarning" runat="server" id="cdInitialWarningID" style="display: none;" title="This post-closing Closing Disclosure is marked as initial, preview, or final, please review." />
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" class="cdPreview" ID="cdPreviewID" />
                                    <img src="../../images/warn.png" alt="warning" class="warning cdPreviewWarning" runat="server" id="cdPreviewWarningID" style="display: none;" title="This post-closing Closing Disclosure is marked as initial, preview, or final, please review." />
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" class="cdFinal" ID="cdFinalID" />
                                    <img src="../../images/warn.png" alt="warning" class="warning cdFinalWarning" runat="server" id="cdFinalWarningID" style="display: none;" title="This post-closing Closing Disclosure is marked as initial, preview, or final, please review." />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="cdLastDisclosedTRIDLoanProductDescription" ID="cdLastDisclosedTRIDLoanProductDescriptionID" />
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" class="cdUcdDocumentLinked" ID="cdUcdDocumentLinkedId" Enabled="false" />
                                    <asp:HiddenField runat="server" ID="cdUcdDocumentId" Value="00000000-0000-0000-0000-000000000000" />
                                    <span runat="server" id="cdUnlinkedUcdOptionsId">
                                        <a href="#" onclick="onUploadUcdDocument(this); return false;">upload</a>
                                        &nbsp;
                                        <a href="#" onclick="onAssociateUcdDocument(this); return false;">associate</a>
                                    </span>
                                    <span runat="server" id="cdLinkedUcdOptionsId">
                                        <a href="#" onclick="onViewUcdDocument(this); return false;">view</a>
                                        &nbsp;
                                        <asp:LinkButton runat="server" OnClientClick="onDownloadUcdDocument(this);" OnClick="DownloadUcdFile">download</asp:LinkButton>
                                        &nbsp;
                                        <a href="#" onclick="onClearUcdDocument(this); return false;">clear</a>
                                    </span>
                                </td>
                                <td>
                                    <input type="checkbox" runat="server" ID="cdDisclosedAprLckd" class="disclosedAprLckd" />
                                </td>
                                <td>
                                    <ml:PercentTextBox runat="server" ID="cdDisclosedApr" CssClass="disclosedApr" ReadOnly="true"></ml:PercentTextBox>
                                    <img src="../../images/warn.png" alt="warning" class="warning cdLqbAprWarning" runat="server" id="cdLqbAprWarning" style="display: none;" title="Please check the actual disclosure to ensure this value is correct." />
                                </td>
                                <td>
                                    <ml:PercentTextBox runat="server" ID="cdLqbApr" CssClass="lqbApr" ReadOnly="true"></ml:PercentTextBox>
                                </td>
                                <td>
                                    <ml:PercentTextBox runat="server" ID="cdDocVendorApr" ReadOnly="true"></ml:PercentTextBox>
                                    <asp:HiddenField runat="server" ID="cdDocVendorAprFull" />
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" class="cdPostClosing" ID="cdPostClosingID" Enabled="false" />
                                </td>
                                <td class="postClosingReason">
                                    <div class="table">
                                        <div class="tableRow">
                                            <div class="tableCell">Numerical amount paid by borrower</div>
                                            <div class="tableCell">
                                                <asp:CheckBox runat="server" class="cdPostClosingNumAmtPdByBorr" ID="cdPostClosingNumAmtPdByBorrID" />
                                            </div>
                                        </div>
                                        <div class="tableRow">
                                            <div class="tableCell">Numerical amount paid by seller</div>
                                            <div class="tableCell">
                                                <asp:CheckBox runat="server" class="cdPostClosingNumAmtPdBySell" ID="cdPostClosingNumAmtPdBySellID" />
                                            </div>
                                        </div>
                                        <div class="tableRow">
                                            <div class="tableCell">Non-numerical clerical error</div>
                                            <div class="tableCell">
                                                <asp:CheckBox runat="server" class="cdPostClosingNonNumClericalErr" ID="cdPostClosingNonNumClericalErrID" />
                                            </div>
                                        </div>
                                        <div class="tableRow">
                                            <div class="tableCell">Cure for tolerance violation</div>
                                            <div class="tableCell">
                                                <asp:CheckBox runat="server" class="cdPostClosingCureForToleranceViolation" ID="cdPostClosingCureForToleranceViolationID" />
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="postClosingDates">
                                    <div class="table">
                                        <div class="tableRow">
                                            <div class="tableCell">
                                                Event Requiring Redisclosure Date
                                            </div>
                                        </div>
                                        <div class="tableRow">
                                            <div class="tableCell centerChildren">
                                                <ml:DateTextBox runat="server" class="cdPostConsummationRedisclosureReasonDate" ID="cdPostConsummationRedisclosureReasonDateID" preset="date"></ml:DateTextBox>
                                            </div>
                                        </div>
                                        <div class="tableRow">
                                            <div class="tableCell">
                                                Date Creditor Received Knowledge
                                            </div>
                                        </div>
                                        <div class="tableRow">
                                            <div class="tableCell centerChildren">
                                                <ml:DateTextBox runat="server" class="cdPostConsummationKnowledgeOfEventDate" ID="cdPostConsummationKnowledgeOfEventDateID" preset="date"></ml:DateTextBox>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <input type="button" id="removeCD" value="-" class="removeDates" />
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UcdDocumentIdForDownload" />
                            <asp:Repeater runat="server" ID="ClosingDisclosuresDates" OnItemDataBound="ClosingDisc_ItemDataBound">
                                <ItemTemplate>
                                    <tr class="cdDateRow" runat="server" id="cdDateRowID">
                                        <td>
                                            <img runat="server" id="cdDateRowAssociatedWithInvalidArchive" class="associatedWithInvalidArchive" src="../../images/fail.png" alt="Closing Disclosure associated with invalid archive." />
                                        </td>
                                        <td class="no-wrap">
                                            <asp:HiddenField runat="server" ID="cdUniqueId" />
                                            <asp:HiddenField runat="server" ID="cdTransactionId" />
                                            <asp:HiddenField runat="server" ID="cdDocCode" />
                                            <asp:HiddenField runat="server" ID="cdVendorId" />
                                            <ml:DateTextBox runat="server" class="cdCreatedD readonlyForInvalidArchives" ID="cdCreatedDID" preset="date"></ml:DateTextBox>
                                            <asp:CheckBox Style="display: none;" runat="server" class="cdManual" ID="cdManualID" />
                                        </td>
                                        <td class="no-wrap">
                                            <ml:DateTextBox runat="server" class="cdIssuedD readonlyForInvalidArchives" ID="cdIssuedDID" preset="date"></ml:DateTextBox>
                                            <input type="checkbox" runat="server" id="cdIssuedDLckdId" class="cdIssuedDLckd readonlyForInvalidArchives" />
                                        </td>
                                        <td class="no-wrap">
                                            <asp:DropDownList runat="server" class="cdDelivery readonlyForInvalidArchives" ID="cdDeliveryID"></asp:DropDownList>
                                            <input type="checkbox" runat="server" id="cdDeliveryLckdId" class="cdDeliveryLckd readonlyForInvalidArchives" />
                                        </td>
                                        <td class="no-wrap">
                                            <ml:DateTextBox runat="server" class="cdReceivedD readonlyForInvalidArchives" ID="cdReceivedDID" preset="date"></ml:DateTextBox>
                                            <input type="checkbox" runat="server" id="cdReceivedDLckdId" class="cdReceivedDLckd readonlyForInvalidArchives" />
                                        </td>
                                        <td class="no-wrap">
                                            <ml:DateTextBox runat="server" class="cdSignedD readonlyForInvalidArchives" ID="cdSignedDID" preset="date"></ml:DateTextBox>
                                            <input type="checkbox" runat="server" id="cdSignedDLckdId" class="cdSignedDLckd readonlyForInvalidArchives" />
                                        </td>
                                        <td>
                                            <asp:HiddenField runat="server" ID="cdDisableManualArchiveAssociation" />
                                            <asp:DropDownList runat="server" class="cdArchive" ID="cdArchiveID" onchange="ArchiveChange(this, cdTooltip);">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" class="cdInitial readonlyForInvalidArchives" ID="cdInitialID" />
                                            <img src="../../images/warn.png" alt="warning" class="warning cdInitialWarning" runat="server" id="cdInitialWarningID" />
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" class="cdPreview readonlyForInvalidArchives" ID="cdPreviewID" />
                                            <img src="../../images/warn.png" alt="warning" class="warning cdPreviewWarning" runat="server" id="cdPreviewWarningID" />
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" class="cdFinal readonlyForInvalidArchives" ID="cdFinalID" />
                                            <img src="../../images/warn.png" alt="warning" class="warning cdFinalWarning" runat="server" id="cdFinalWarningID" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" class="cdLastDisclosedTRIDLoanProductDescription" ID="cdLastDisclosedTRIDLoanProductDescriptionID" />
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" class="cdUcdDocumentLinked" ID="cdUcdDocumentLinkedId" Enabled="false" />
                                            <asp:HiddenField runat="server" ID="cdUcdDocumentId" Value="00000000-0000-0000-0000-000000000000" />
                                            <span runat="server" id="cdUnlinkedUcdOptionsId">
                                                <a href="#" onclick="onUploadUcdDocument(this); return false;">upload</a>
                                                &nbsp;
                                                <a href="#" onclick="onAssociateUcdDocument(this); return false;">associate</a>
                                            </span>
                                            <span runat="server" id="cdLinkedUcdOptionsId">
                                                <a href="#" onclick="onViewUcdDocument(this); return false;">view</a>
                                                &nbsp;
                                                <asp:LinkButton runat="server" OnClientClick="onDownloadUcdDocument(this);" OnClick="DownloadUcdFile">download</asp:LinkButton>
                                                &nbsp;
                                                <a href="#" onclick="onClearUcdDocument(this); return false;">clear</a>
                                            </span>
                                        </td>
                                        <td>
                                            <input type="checkbox" runat="server" ID="cdDisclosedAprLckd" class="disclosedAprLckd" />
                                        </td>
                                        <td>
                                            <ml:PercentTextBox runat="server" ID="cdDisclosedApr" CssClass="disclosedApr"></ml:PercentTextBox>
                                            <img src="../../images/warn.png" alt="warning" class="warning cdLqbAprWarning" runat="server" id="cdLqbAprWarning" style="display: none;" title="Please check the actual disclosure to ensure this value is correct." />
                                        </td>
                                        <td>
                                            <ml:PercentTextBox runat="server" ID="cdLqbApr" CssClass="lqbApr" ReadOnly="true"></ml:PercentTextBox>
                                        </td>
                                        <td>
                                            <ml:PercentTextBox runat="server" ID="cdDocVendorApr" ReadOnly="true"></ml:PercentTextBox>
                                            <asp:HiddenField runat="server" ID="cdDocVendorAprFull" />
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" class="cdPostClosing" ID="cdPostClosingID" Enabled="false" />
                                        </td>
                                        <td class="postClosingReason">
                                            <div runat="server" id="postClosingReasonDiv">
                                                <div class="tableRow">
                                                    <div class="tableCell">Numerical Amount paid by borrower</div>
                                                    <div class="tableCell">
                                                        <asp:CheckBox runat="server" class="cdPostClosingNumAmtPdByBorr readonlyForInvalidArchives" ID="cdPostClosingNumAmtPdByBorrID" />
                                                    </div>
                                                </div>
                                                <div class="tableRow">
                                                    <div class="tableCell">Numerical Amount paid by seller</div>
                                                    <div class="tableCell">
                                                        <asp:CheckBox runat="server" class="cdPostClosingNumAmtPdBySell readonlyForInvalidArchives" ID="cdPostClosingNumAmtPdBySellID" />
                                                    </div>
                                                </div>
                                                <div class="tableRow">
                                                    <div class="tableCell">Non-Numerical clerical error</div>
                                                    <div class="tableCell">
                                                        <asp:CheckBox runat="server" class="cdPostClosingNonNumClericalErr readonlyForInvalidArchives" ID="cdPostClosingNonNumClericalErrID" />
                                                    </div>
                                                </div>
                                                <div class="tableRow">
                                                    <div class="tableCell">Cure for tolerance violation</div>
                                                    <div class="tableCell">
                                                        <asp:CheckBox runat="server" class="cdPostClosingCureForToleranceViolation readonlyForInvalidArchives" ID="cdPostClosingCureForToleranceViolationID" />
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="postClosingDates">
                                            <div class="table" runat="server" id="postClosingDatesDiv">
                                                <div class="tableRow">
                                                    <div class="tableCell">
                                                        Event Requiring Redisclosure Date
                                                    </div>
                                                </div>
                                                <div class="tableRow">
                                                    <div class="tableCell centerChildren">
                                                        <ml:DateTextBox runat="server" class="cdPostConsummationRedisclosureReasonDate" ID="cdPostConsummationRedisclosureReasonDateID" preset="date"></ml:DateTextBox>
                                                    </div>
                                                </div>
                                                <div class="tableRow">
                                                    <div class="tableCell">
                                                        Date Creditor Received Knowledge
                                                    </div>
                                                </div>
                                                <div class="tableRow">
                                                    <div class="tableCell centerChildren">
                                                        <ml:DateTextBox runat="server" class="cdPostConsummationKnowledgeOfEventDate" ID="cdPostConsummationKnowledgeOfEventDateID" preset="date"></ml:DateTextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="button" id="removeCD" value="-" class="removeDates" runat="server" />
                                        </td>
                                    </tr>

                                    <asp:Repeater runat="server" ID="ClosingDisclosureBorrowerLevelDates" OnItemDataBound="BorrowerLevelDates_ItemDataBound">
                                        <ItemTemplate>
                                            <tr id="BorrowerLevelDatesRow" runat="server" class="borr-level-dates">
                                                <td></td>
                                                <td class="borrower-name">
                                                    <input type="hidden" runat="server" ID="ConsumerId" class="consumer-id" />
                                                    <label class="FieldLabel borrower-name-label" runat="server" id="BorrowerName">&nbsp;</label>
                                                </td>
                                                <td>
                                                    <ml:DateTextBox runat="server" ID="IssuedDate" class="issued-date readonlyForInvalidArchives" preset="date"></ml:DateTextBox>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="DeliveryMethod" CssClass="delivery-method readonlyForInvalidArchives" onchange="refreshCalculation();"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <ml:DateTextBox runat="server" ID="ReceivedDate" class="received-date readonlyForInvalidArchives" preset="date"></ml:DateTextBox>
                                                </td>
                                                <td runat="server" id="SignedDateCell">
                                                    <ml:DateTextBox runat="server" ID="SignedDate" class="signed-date readonlyForInvalidArchives" preset="date"></ml:DateTextBox>
                                                </td>
                                                <td runat="server" id="ExcludeFromCalculationsCell" class="exclude-from-calculations-cell">
                                                    <label class="FieldLabel">
                                                        Exclude from calculations?
                                                    </label>
                                                    <input type="checkbox" runat="server" id="ExcludeFromCalculations" class="exclude-from-calculations readonlyForInvalidArchives" onchange="refreshCalculation()" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                    <tr runat="server" id="AddBorrowerLevelDatesRow" class="borr-level-dates">
                                        <td></td>
                                        <td>
                                            <input type="button" value="Track Borrower Dates" class="track-borr-level-dates-btn" onclick="addBorrowerLevelTracking(this, false);" />
                                        </td>
                                        <td colspan="20">&nbsp;</td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <tr>
                                <td colspan="8">
                                    <input nohighlight type="button" value="Add Closing Disclosure" id="addCD" />
                                    <input nohighlight type="button" value="Hide Invalid Closing Disclosures" id="toggleInvalidCD" />
                                    <label class="FieldLabel" for="sCalculateInitialClosingDisclosure">Calculate Initial Closing Disclosure?</label>
                                    <asp:CheckBox ID="sCalculateInitialClosingDisclosure" runat="server" onclick="clearInitialCheckboxes(false); refreshCalculation();" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <span class="invalidCDArchiveLegend">
                                        <img src="../../images/fail.png" alt="Closing Disclosure associated with invalid archive." />
                                        Associated with an undisclosed archive
                                    </span>
                                </td>
                            </tr>
                            <tbody id="CalculateInitialCdErrorSection" class="hidden" runat="server">
                                <tr>
                                    <td colspan="8">
                                        <img src="../../images/fail.png" alt="Initial Closing Disclosure Calculation Error" />
                                        <ml:EncodedLabel class="Error FieldLabel" ID="CalculateInitialCdErrorMessage" runat="server"></ml:EncodedLabel>
                                    </td>
                                </tr>
                            </tbody>
                            <tr>
                                <td class="FieldLabel">&nbsp;
                                </td>
                                <td colspan="3"></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset class="ImportantDatesArea">
                        <legend class="FieldLabel">Important Dates</legend>
                        <table class="ImportantDatesTable">
                            <tr>
                                <td class="FieldLabel" colspan="3">Loan Dates:</td>
                            </tr>
                            <tr>
                                <td>Application Submitted Date:</td>
                                <td>
                                    <ml:DateTextBox ID="sAppSubmittedD" runat="server" preset="date" readOnly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Estimated Closing Date:</td>
                                <td>
                                    <ml:DateTextBox ID="sEstCloseD" runat="server" preset="date" readOnly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr class="blank-row">
                                <td colspan="5"></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" colspan="3">Document Dates:</td>
                            </tr>
                            <tr>
                                <td>Initial Loan Estimate:</td>
                                <td>
                                    Issued
                                    <ml:DateTextBox ID="sInitialLoanEstimateIssuedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                                <td>
                                    Received
                                    <ml:DateTextBox ID="sInitialLoanEstimateReceivedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Last Disclosed Loan Estimate:</td>
                                <td>
                                    Issued
                                    <ml:DateTextBox ID="sLastDisclosedLoanEstimateIssuedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                                <td>
                                    Received
                                    <ml:DateTextBox ID="sLastDisclosedLoanEstimateReceivedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr class="blank-row">
                                <td colspan="5"></td>
                            </tr>
                            <tr>
                                <td>Initial Closing Disclosure:</td>
                                <td>
                                    Issued
                                    <ml:DateTextBox ID="sInitialClosingDisclosureIssuedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                                <td>
                                    Received
                                    <ml:DateTextBox ID="sInitialClosingDisclosureReceivedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Preview Closing Disclosure:</td>
                                <td>
                                    Issued
                                    <ml:DateTextBox ID="sPreviewClosingDisclosureIssuedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                                <td>
                                    Received
                                    <ml:DateTextBox ID="sPreviewClosingDisclosureReceivedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Last Closing Disclosure Before Consummation:</td>
                                <td>
                                    Issued
                                    <ml:DateTextBox ID="sLastClosingDisclosureBeforeConsummationIssuedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                                <td>
                                    Received
                                    <ml:DateTextBox ID="sLastClosingDisclosureBeforeConsummationReceivedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Final Closing Disclosure:</td>
                                <td>
                                    Issued
                                    <ml:DateTextBox ID="sFinalClosingDisclosureIssuedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                                <td>
                                    Received
                                    <ml:DateTextBox ID="sFinalClosingDisclosureReceivedD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr class="blank-row">
                                <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" colspan="5">Deadlines:</td>
                            </tr>
                            <tr>
                                <td colspan="2">Deadline (from current Application Submitted Date) to Mail or Deliver Initial Loan Estimate:</td>
                                <td>
                                    <ml:DateTextBox ID="sInitialLoanEstimateMailedOrReceivedDeadlineForCurrentAppD" runat="server" preset="date" readonly="true"></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Deadline (from Closing Date) to Mail or Deliver Initial Loan Estimate:</td>
                                <td>
                                    <ml:DateTextBox ID="sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentClosingD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Deadline for Borrower to Receive Revised Loan Estimate:</td>
                                <td>
                                    <ml:DateTextBox ID="sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Deadline to Mail Initial Closing Disclosure:</td>
                                <td>
                                    <ml:DateTextBox ID="sInitialClosingDisclosureMailedDeadlineD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Deadline for Borrower to Receive Initial Closing Disclosure:</td>
                                <td>
                                    <ml:DateTextBox ID="sInitialClosingDisclosureReceivedDeadlineD" runat="server" preset="date" readonly></ml:DateTextBox>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <uc:RespaDates runat="server" ID="RespaDates"></uc:RespaDates>
                </td>
            </tr>
        </table>
        <table class="Hidden">
            <tr id="BorrowerLevelDatesRowForCloning" class="borr-level-dates">
                <td></td>
                <td class="borrower-name">
                    <input type="hidden" class="consumer-id" />
                    <label class="FieldLabel borrower-name-label">nbsp;</label>
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="BorrowerLevelDatesRowForCloningIssuedDate" class="issued-date" preset="date"></ml:DateTextBox>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="BorrowerLevelDatesRowForCloningDeliveryMethod" CssClass="delivery-method" onchange="refreshCalculation();"></asp:DropDownList>
                </td>
                <td>
                    <ml:DateTextBox runat="server" ID="BorrowerLevelDatesRowForCloningReceivedDate" class="received-date" preset="date"></ml:DateTextBox>
                </td>
                <td id="SignedDateCell">
                    <ml:DateTextBox runat="server" ID="BorrowerLevelDatesRowForCloningSignedDate" class="signed-date" preset="date"></ml:DateTextBox>
                </td>
                <td class="exclude-from-calculations-cell">
                    <label class="FieldLabel">
                        Exclude from calculations?
                    </label>
                    <input type="checkbox" runat="server" id="BorrowerLevelDatesRowForCloningExcludeFromCalculations" class="exclude-from-calculations" onchange="refreshCalculation()" />
                </td>
            </tr>
            <tr id="AddBorrowerLevelDatesRowForLeCloning" class="borr-level-dates">
                <td></td>
                <td colspan="2">
                    <input type="button" value="Track Borrower Dates" class="track-borr-level-dates-btn" onclick="addBorrowerLevelTracking(this, true);" />
                </td>
                <td colspan="20">&nbsp;</td>
            </tr>
            <tr id="AddBorrowerLevelDatesRowForCdCloning" class="borr-level-dates">
                <td></td>
                <td>
                    <input type="button" value="Track Borrower Dates" class="track-borr-level-dates-btn" onclick="addBorrowerLevelTracking(this, false);" />
                </td>
                <td colspan="20">&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
