using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class ARMProgramDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(ARMProgramDisclosureServiceItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sArmIndexLeadDays", dataLoan.sArmIndexLeadDays_rep);

            // For RateFloorPopup
            SetResult("sRAdjFloorCalcT", dataLoan.sRAdjFloorCalcT);
            SetResult("sRAdjFloorCalcT_hidden", Tools.GetsRAdjFloorCalcTLabel(dataLoan.sRAdjFloorCalcT));
            SetResult("sRAdjFloorBaseR", dataLoan.sRAdjFloorBaseR_rep);
            SetResult("sRAdjFloorAddR", dataLoan.sRAdjFloorAddR_rep);
            SetResult("sRAdjFloorTotalR", dataLoan.sRAdjFloorTotalR_rep);
            SetResult("sRAdjFloorLifeCapR", dataLoan.sRAdjFloorLifeCapR_rep);
            SetResult("sIsRAdjFloorRReadOnly", dataLoan.sIsRAdjFloorRReadOnly.ToString());
            SetResult("sRAdjFloorR", dataLoan.sRAdjFloorR_rep);
            SetResult("sRAdjFloorR_readonly", dataLoan.sRAdjFloorR_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sNoteIR_popup", dataLoan.sNoteIR_rep);
            SetResult("sRAdjLifeCapR", dataLoan.sRAdjLifeCapR_rep);
            SetResult("sRAdjLifeCapR_popup", dataLoan.sRAdjLifeCapR_rep);
            
			SetResult("sArmIndexT", dataLoan.sArmIndexT);
            SetResult("sFreddieArmIndexT", dataLoan.sFreddieArmIndexT);
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sHasDemandFeature = GetBool("sHasDemandFeature");
            dataLoan.sRAdj1stCapR_rep = GetString("sRAdj1stCapR");
            dataLoan.sRAdj1stCapMon_rep = GetString("sRAdj1stCapMon");
            dataLoan.sRAdjCapR_rep = GetString("sRAdjCapR");
            dataLoan.sRAdjCapMon_rep = GetString("sRAdjCapMon");
            dataLoan.sRAdjLifeCapR_rep = GetString("sRAdjLifeCapR");
            dataLoan.sRAdjMarginR_rep = GetString("sRAdjMarginR");
            dataLoan.sRAdjRoundToR_rep = GetString("sRAdjRoundToR");
            dataLoan.sRAdjRoundT = (E_sRAdjRoundT) GetInt("sRAdjRoundT");
            dataLoan.sRAdjFloorR_rep = GetString("sRAdjFloorR");
            dataLoan.sArmIndexNotifyAtLeastDaysVstr = GetString("sArmIndexNotifyAtLeastDaysVstr");
            dataLoan.sArmIndexNotifyNotBeforeDaysVstr = GetString("sArmIndexNotifyNotBeforeDaysVstr");
            dataLoan.sArmIndexLeadDays_rep = GetString("sArmIndexLeadDays");
            
            dataLoan.sArmIndexBasedOnVstr = GetString("sArmIndexBasedOnVstr");
            dataLoan.sArmIndexCanBeFoundVstr = GetString("sArmIndexCanBeFoundVstr");
            dataLoan.sArmIndexAffectInitIRBit = GetBool("sArmIndexAffectInitIRBit");
            dataLoan.sArmIndexT = (E_sArmIndexT) GetInt("sArmIndexT");
            dataLoan.sArmIndexTLckd = GetBool("sArmIndexTLckd");
            dataLoan.sFreddieArmIndexT = (E_sFreddieArmIndexT) GetInt("sFreddieArmIndexT");
            dataLoan.sFreddieArmIndexTLckd = GetBool("sFreddieArmIndexTLckd");
            dataLoan.sArmIndexEffectiveD_rep = GetString("sArmIndexEffectiveD");

            // From RateFloorPopup
            dataLoan.sRAdjFloorCalcT = GetEnum<RateAdjFloorCalcT>("sRAdjFloorCalcT");
            dataLoan.sRAdjFloorAddR_rep = GetString("sRAdjFloorAddR");
            dataLoan.sRAdjFloorR_rep = GetString("sRAdjFloorR");

        }
    }

    public partial class ARMProgramDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new ARMProgramDisclosureServiceItem());
        }

	}
}
