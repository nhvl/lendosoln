﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SellerClosingDisclosure.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.SellerClosingDisclosure" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls"%>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Non Borrower-Responsible Closing Costs</title>
    <style type="text/css">
        
        body
        {
            background-color:gainsboro;
        }
        hr
        {
            clear:both;
        }
        .TopDiv
        {
            width: 30%;
            float:left;
            padding:5px;
        }
        .TopDivContainer
        {
            white-space:nowrap;
            margin:10px 15px 0px 15px;
            
        }
        .TopDivContainer table
        {
            width:30%;
            float:left;
        }
        .InlineDiv
        {
            padding:5px;
            white-space:nowrap;
        }
        table
        {
            white-space:nowrap;
        }
        
        input[preset='money']
        {
            width:90px;
        }
        
        .InlineDiv
        {
            min-width:30%;
            display:inline-block;
            text-align:center;
        }
        
        .SectionDiv
        {
            white-space:nowrap;
            margin: 10px 15px;
            clear:both;
            
            border-style:solid;
            border-width:2px;
        }
        #TimeDiv
        {
            border-style:none;
        }
        
        .InlineDiv span
        {
            margin-right:20px;
        }
        
        .GridHeader
        {
            padding:3px;
        }
        
        .LoanTermsDesc
        {
            width:230px;
            font-weight:bold;
            margin-right:40px;
            display:inline-block;
        }
        
        .LoanTermsInfo
        {
            display:inline-block;
        }
        
        .LoanTermOptions
        {
            line-height:16px;
        }
        
        .LoanTermOptions > div
        {
            padding: 5px;
        }
        
        .Month
        {
            width:30px;
        }
        
        Form
        {
            float:left;
            clear:left;
            min-width:100%;
        }
        #AmortSchedule
        {
            border-collapse:collapse;
            width:100%;
            border:solid 2px black;
            margin-bottom:10px;
        }
        #ExpensesTable
        {
            border-collapse:collapse;
            width: 400px;
            border:solid 2px black;
            margin-bottom:10px;
        }
        #AmortSchedule thead td, #ExpensesTable thead td
        {
            text-align:center;
        }
        #AmortSchedule td, #ExpensesTable td
        {
            border:solid 1px black;
        }
        .AmortScheduleContainer
        {
            padding:10px;
        }
        
        #sSpAddr
        {
            width:230px;
        }
        
        #sGfeEstScAvailTillDLckd
        {
            margin-left:60px;
        }
        
        #AmortizationTableDiv
        {
            width:630px;
            height:500px;
            display:none;
        }
        #AmortizationTableFrame
        {
            width:100%;
            height:95%;
        }
        
        .ui-dialog
        {
            border: solid 1px black !important;
        }
        .ui-dialog-titlebar
        {
            background-color:Maroon !important;
        }
        
        .HalfDiv
        {
            width:49%;
            display:inline-block;
            vertical-align:top;
        }
        .HalfDiv span
        {
            display:inline-block;
            width:250px;
            margin: 5px 0px; 
        }
        .IndentedLine
        {
            padding-left:50px;
        }
        .CostsAtClosing
        {
            padding: 5px 5px;
        }
        
        .modal-background {
              position: fixed;
              top: 0; left: 0; width: 100%; height: 100%;
              background-color: rgba(0,0,0,0.5);
              padding: 0.5em;
              text-align: center;
              -moz-box-sizing: border-box; box-sizing: border-box;
            }
            .modal-outer {
              position: relative;
              width: 100%;
              height: 100%;
            }

            .modal {
              position: relative;
              background-color: white;
              padding: 2em;
              box-shadow: 1px 1px 3px rgba(0,0,0,0.1);
              margin: 0 auto;
              display: inline-block;
              max-width: 100%;
              max-height: 100%;
              overflow-y: auto;
              -moz-box-sizing: border-box; box-sizing: border-box;
            }

            .modal-button {
              text-align: center;
              background-color: rgb(70,70,180);
              color: white;
              padding: 0.5em 1em;
              display: inline-block;
              cursor: pointer;
            }

            .modal ul li {
                text-align: left;
            }
            
            .OptionsList
            {
                list-style-type: none;
                display: none;
                background-color: white;
                border-style: solid;
                border-width: 1px;
                z-index:1;
                max-height: 300px;
                overflow-y:auto;
                padding:0px;
                margin:0px;
                margin:0 auto;
                text-align:left;
                position:relative;
            }
            .DDLImg, .ComboboxImg
            {
                width: 21px;
                height: 21px;
                top: 6px;
            }
            input[readonly]:not(.RactiveDDL)
            {
                background-color:lightgrey;
            }
            .QMDDL
            {
                width:200px;
            }
            .RactiveDDLContainer
            {
                overflow:visible;
                height:13px;
                display:inline-block;
            }
            
            input[preset='money']
            {
                width:90px;
                text-align:right;
            }
            input[preset='date']
            {
                width:75px;
            }
            select[disabled=""]
            {
                background-color: lightgrey;
            }
            .table-closingcost tfooter td
            {
                text-align:left;
            }
            
           
            /*****Design for TPO Portal Closing Costs *******/
            .div-title {
	            width: auto;
	            font-size:14px;
	            font-family:arial, helvetica, sans-serif; 
	            padding: 3px 0px 3px 9px;
	            margin-top: 5px;
	            /*font-weight: bold;*/
            }
            .div-left {
	            float: left;
	            width: 50%;
	            min-width: 300px;
            }
            .div-right 
            {
	            padding-top: 3px;
	            float: right;
	            width: 40%;
	            margin-left: 10%
            }
            .div-right div{
	            padding: 3px
            }
            .table-closingcost {
	            width: 100%;
	            border-collapse: collapse;
	            font-weight: normal;
            }
            .table-closingcost th {
	            font-weight: normal;
            }
            .table-closingcost td,th {
	            padding: 3px;
	            text-align: center;
	            white-space:nowrap;
            }
            .table-calculation-1 {
	            width: 100%;
            }

            .table-calculation-1 td:nth-child(4) {
	            width: 5%;
            }
            .table-calculation-1 td:nth-child(6) *{
	            width: 100%;
	            min-width: 90px;
            }
            .table-calculation-1 td:nth-child(7) {
	            width: 5%;
            }
            .table-calculation-1 td:nth-child(9) *{
	            width: 100%;
	            min-width: 90px;
            }
            .table-calculation-1 td:nth-child(11) {
	            width: 10%;
            }
            .table-calculation-2 {
	            width: 100%;
	            border-collapse: collapse;
	            border: 1px solid #ebeff2;
            }
            .table-calculation-2 td,th {
	            padding: 3px 3px 3px 3px;
            }
            .image-style {
	            padding: 0px 4px 0px 4px;
            }
            .div-image {
	            padding: 5px 3px 0px 3px;
	            width:0px;
            }
            .checkbox-hidden {
	            display: none;
            }
            .date-image {
	            vertical-align: middle;
            }

            /**TPO Portal Closing Cost Changes **/
            img
            {
                border-style:none;
            }
            
            .table-closingcost thead
            {
                background-color: #999999;
                color: Black;
            }
            
            #APTable, #AIRTable
            {
                width:100%;
                text-align:left;
            }
            
            #APTable td, #AIRTable td
            {
                width:50%;
            }
            
            #AdjustableDiv
            {
                text-align:center;
                border-style:none;
            }
            
            #AdjustableDiv .HalfDiv
            {
                width: 49%;
                margin: 0px;
            }
            
            .ThirdDiv
            {
                min-width: 30%;
                display: inline-block;
                padding:5px;
                vertical-align:top;
            }
            
            .InBetweenDiv
            {
                border-left:solid 2px black;
                border-right: solid 2px black;
            }
            
            .ContactHeader
            {
                font-weight:bold;
                text-decoration:underline;
            }
            
            .CFMContainer
            {
                display:inline-block;
            }
            
            .CFMLabel
            {
                width:80px;
                display:inline-block;
                margin-top:10px;
            }
            
            #After5YrsTable
            {
                display:inline-table;
                border-collapse: collapse;
                border: solid 1px black;
                margin: 10px;
                width:600px;
                vertical-align:top;
            }
            
            #After5YrsTable td
            {
                border-left: solid 1px black;
                border-right: solid 1px black;
                padding: 3px;
            }
            
            .ComparisonsDiv
            {
                display:inline-block;
                padding-top:20px;
            }
            
            .ComparisonSpan
            {
                width:250px;
                display:inline-block;
                margin:5px;
            }
            
            .OtherConsiderationsTable
            {
                border-collapse:collapse;
            }
            
            .OtherConsiderationsTable td
            {
                padding: 5px;
                border-style:none;
            }
            
            #sLateChargeBaseDesc
            {
                width:200px;
            }
            
            .smallNum
            {
                width: 50px;
            }
            
            #OtherConsiderationsTable
            {
                white-space:normal;
            }
            
            #OtherConsiderationsTable td.FieldLabel
            {
                white-space:nowrap;
                padding: 5px;
            }
            #OtherConsiderationsTable td
            {
                padding: 5px;
            }
            #dialog-confirm
            {
                text-align: center;
            }
            .LQBDialogBox .ui-button.ui-widget
            {
                border: none;
                padding: 0px;
                float: none;
                font-weight: normal;
            }
            .LQBDialogBox .ui-dialog-buttonset
            {
                text-align: center;
            }
            
            .Hidden
            {
                display:none;
            }
            
            .ContactImg
            {
                vertical-align:middle;
                margin-left:3px;
            }
            .ContactImg:hover
            {
                cursor:pointer;
            }
            .Hidden
            {
                display:none;
            }
            .table-closingcost td, .RactiveDDL, .DDLImg, .ContactImg
            {
                vertical-align:top !important;
            }

            .SubFeeConfirmOverwriteContent
            {
                text-align: left;
                width: 400px;
            }

            .SubFeeConfirmOverwriteContentBtns
            {
                text-align: center;
            }
    </style>
</head>
<body>


    <script type="text/javascript">
        function selectedDate(cal, date) {
            cal.sel.value = date;
            cal.callCloseHandler();
            if (typeof (updateDirtyBit) == 'function') updateDirtyBit();
            cal.sel.blur();
        } 
        
        function lqblog(msg) {
            if (window.console && window.console.log) {
                window.console.log(msg);
            }
        }
        
        function callWebMethod(webMethodName, data, error, success, pageName) {
            if (!pageName) {
                pageName = '../Disclosure/SellerClosingDisclosure.aspx/';
            }
            
            var settings = {
                async: false,
                type: 'POST',
                url: pageName + webMethodName,
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: error,
                success: function (d){ checkWebError(d, success); }
            };

            callWebMethodAsync(settings);
        }

        function EventHandlers() {
            $("select:disabled").each(function() {
                var el = $(this);
                el.removeProp('disabled');
                el.mousedown(function(event) {
                    event.preventDefault();
                    $(this).focus();
                });
                el.keydown(function(event) {
                    event.preventDefault();
                    $(this).focus();
                });
                el.css("background-color", "lightgrey");
            });

            $(document).on('change', 'input,select', updateDirtyBit);
        }
        
        var loanID;

        $(document).ready(function() {

            EventHandlers();

            window.refreshCalculation = function() { ractive.fire('recalc'); };

            window.f_saveMe = function() {
                var model = ractive.get();
                var data = { loanId: ML.sLId, viewModelJson: JSON.stringify(model)},
                success = false;

                callWebMethod('Save', data, function() {
                        alert('Failed');
                    },
                    function(d) {
                        success = true;
                        clearDirty();
                    });
                return success;
            }

            window.saveMe = window.f_saveMe;

 
            
            loanID = document.getElementById("loanid").value;
            
            var datepickerTransition = function(t) {
                if (t.isIntro) {
                    var node = $(t.node);
                    node.change(
                        function() {
                            $(this).blur();
                            ractive.updateModel();
                            ractive.fire('recalc');
                        });

                    _initMask(node[0]);
                }
            };

            var numericTransition = function(t, recalc) {
                if (t.isIntro) {
                    var node = $(t.node);
                    if (recalc) {
                        node.change(
                            function() {
                                ractive.updateModel();
                                ractive.fire('recalc');
                            });
                    }

                    _initMask(node[0]);
                }
            };

            var ddlTransition = function(t) {

                if (t.isIntro) {
                    var node = $(t.node);
                    var options = $("." + this.get("OptionsId"));

                    if (options.attr("sized")) {
                        node.css("width", options.width() - $(".DDLImg").width() - 2);
                    }
                }
            }

            var sectionTransition = function(t) {
                if (t.isIntro) {
                    var node = $(t.node);
                    var section = node.text();

                    var selector = determineShownSections(t.params.hudline, t.params.isSystem, t.params.gfeGroup);
                    node.find("option").not(selector).remove();

                    var numVisible = node.find("option").length;
                    if (numVisible < 2) {
                        node.prop('disabled', true);
                    }
                    if (numVisible < 1) {
                        node.css('display', 'none');
                    }
                }
            }
            
            var initMask = function(o) {
                _initMask(o.node, true);
                o.complete();
            };
        
            Ractive.transitions.initMask = initMask;

            Ractive.transitions.datepickerTransition = datepickerTransition;
            Ractive.transitions.numericTransition = numericTransition;
            Ractive.transitions.ddlIntro = ddlTransition;
            Ractive.transitions.sectionIntro = sectionTransition;

            function highlightLI(el) {
                el.css("background-color", "yellow");
                el.attr("class", "selected");
            }

            function unhighlightLI(el) {
                el.css("background-color", "");
                el.removeAttr("class");
            }

            var calculationModalContent = Ractive.parse(document.getElementById('CalculationModalContent').innerHTML),
            feePickerModalContent = Ractive.parse(document.getElementById('FeePickerModalContent').innerHTML);
            var subFeeConfirmOverwriteContent = Ractive.parse(document.getElementById('SubFeeConfirmOverwriteContent').innerHTML);

            Modal = Ractive.extend({
                el: document.body,
                append: true,
                template: '#modal',

                init: function() {
                    var self = this, resizeHandler;
                    this.outer = this.find('.modal-outer');
                    this.modal = this.find('.modal');

                    this.on('close', function(event) {
                        this.teardown();
                    });

                    $('window').resize(self.center);

                    this.on('teardown', function() {
                        $('window').off('resize', self.center);
                    }, false);

                    this.center();
                },

                center: function() {
                    var outerHeight, modalHeight, verticalSpace;
                    outerHeight = this.outer.clientHeight;
                    modalHeight = this.modal.clientHeight;
                    verticalSpace = (outerHeight - modalHeight) / 2;
                    this.modal.style.top = verticalSpace + 'px';
                }
            });

            var TimeControl = Ractive.extend({
                template: '#TimeControl',
                data: { value: '',
                    id: '',
                    readonly: false
                },
                init: function() {
                    this.on('minuteBlur', function(e) {
                        time_onminuteblur(e.node);
                    });

                    this.on('hourKeyUp', function(e) {
                        time_onhourkeyup(e.node, e.node.id + '_minute', e.original );
                    });

                    this.on('hourBlur', function(e) {
                        time_onhourblur(e.node);
                    });
                }

            });

            var DatePicker = Ractive.extend({
                template: '#DatePicker',
                data: {
                    value: '',
                    id: '',
                    readonly: false

                },
                init: function() {
                    var self = this;

                    this.on('showCalendar', function(e, id) {
                        return displayCalendar(id, function(cal) { 
                            if (cal && cal.sel) {
                                queueRactiveUpdate(ractive, self, cal.sel.value);
                                cal.hide();
                            }
                        });
                    });
                    this.on('domblur', function(event) {
                        var kp = event.keypath;
                        //sadly this blur event fires before the mask.js blur event 
                        //by using setTimeout we queue up the function until after all the other
                        //js code.
                        window.setTimeout(function() {
                            self.updateModel("value", true);
                        });
                    });

                    
                    this.on('updatePicker', function(event)
                    {
                        window.setTimeout(function(){
                            queueRactiveUpdate(ractive, self, event.node.value);
                        }, 0);
                    });
                }
            });
            
            var RactiveDDL = Ractive.extend({
              template: '#RactiveDDL',
              data: {
                  value: '',
                  Prefix: '',
                  disabled: ''
              },
              init: function() {

                  var input = $(this.el).find(".RactiveDDL");
                  var selectedListItem = $("."+ this.get("Prefix") + "Options li[val='" + this.get("value") + "']");
                  input.val(selectedListItem.text());
                  
                if(<%=AspxTools.JsBool(!IsReadOnly) %>)
                {
                      this.on('showOptions', function(event, i, j, k, OptionsId) {
                          if(!this.get("disabled"))
                          {
                              var options = $("." + OptionsId);
                              options.toggle();

                              currentFeeRow = j;
                              currentSection = i;
                              if(OptionsId == 'PaidBySingleOptions' || (OptionsId == 'PayableOptions' && k == undefined))
                              {
                                k = 0;
                              }
                              currentPayment = k;

                              var node = $(event.node);
                              if (node.prop('tagName').toLowerCase() == 'input') {
                              
                                  //called from the input
                                  var img = node.parent().find('img');
                                  img.after(options);
                              }
                              else {
                              
                                  //called from the img
                                  node.after(options);
                                  node = node.parent().find("input");
                                  node.focus();
                             }
                         }
                      });

                      this.on('hideDesc', function(event, OptionsId) {
                          var options = $("." + OptionsId);

                          if (canHideDescriptions) {
                              options.hide();
                              $("#OptionsContainer").append(options);
                          }

                      });

                      this.on('keyScroll', function(event){
                      
                        var code = event.original.which; //get the keycode
                        var ul = $(event.node).parent().find('ul'); //get the ul
                        
                        var selectedLI = ul.find(".selected");
                        
                        var hasSelectedLI = selectedLI.length > 0;

                        switch(code)
                        {
                            case 13:
                                if(hasSelectedLI)
                                {
                                    selectedLI.click();
                                }
                            break;
                            case 38: //up
                                if(hasSelectedLI){
                                   var nextLI = selectedLI.prev();
                                   
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                            case 40: //down
                            
                                if(!ul.is(":visible")){
                                   $(event.node).click();
                                }
                                
                                if(hasSelectedLI){
                                
                                   var nextLI = selectedLI.next();
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                        }
                      });

                      this.on('recalc', function(event, success) {

                          ractive.fire('recalc');

                      });
                  }
              }
          });

            var FeePickerModal = Modal.extend({
                partials: {
                    modalContent: feePickerModalContent
                }
            });

            var SubFeeConfirmOverwriteModal = Modal.extend({
                partials: {
                    modalContent: subFeeConfirmOverwriteContent
                }
            });

            var CalculationModal = Modal.extend({
                partials: {
                    modalContent: calculationModalContent
                },

                init: function(options) {
                    // wherever we overwrite methods, such as `init`, we can call the
                    // overwritten method as `this._super`
                    var that = this;

                    var originalData = $.extend(true, {}, that.get());

                    this.on('close', function(e, update) {
                        //this is needed for mask.js

                        if (!update) {
                            $.extend(that.get("fee"), originalData.fee);
                            $.extend(that.get("loan"), originalData.loan);
                        }

                        that.updateModel();
                    });

                    this._super(options);

                }
            });

            ractive = new Ractive({
                // The `el` option can be a node, an ID, or a CSS selector.
                el: 'ClosingCostFeessContainer',
                append: false,

                // We could pass in a string, but for the sake of convenience
                // we're passing the ID of the <script> tag above.
                template: '#ClosingCostTemplate',

                partials: '#ClosingCostFeePaymentTemplate, #ClosingCostSectionFeeTemplate',
                components: { ractiveDDL: RactiveDDL, datePicker: DatePicker, timeControl: TimeControl },
                // Here, we're passing in some initial data
                data: ClosingCostData,

                init: function() {

                }
            });
            
        var observer = ractive.observe('SectionList.*.ClosingCostFeeList.*.bene', function(newValue, oldValue, keypath, i, j, k){
            if(oldValue != null && newValue != null && oldValue != newValue)
            {
                var splitArray = keypath.split(".")
                var field = splitArray[splitArray.length - 1];
                
                var prefix = "Beneficiary";
                
                var DDLClass = prefix + "DDL.RactiveDDL" + i + j;
                
                var OptionsClass = prefix + "Options";
                
                $("." + DDLClass).val( $("." + OptionsClass).find("li[val='"+newValue+"']").text());
            }
        });
            
            ractive.on('onContactHover', function(event){
	    var el = $(event.node);
	    el.attr("src", '../../images/contacts_clicked.png');
	});
	
	ractive.on('onContactMouseOut', function(event){
	    var el = $(event.node);
	    el.attr("src", '../../images/contacts.png');
	});
	
	ractive.on('showAgentPicker', function(event, i, j){
	   var model = ractive.get();
       
       if (IsReadOnly){
        return; 
       }
       
	   var el = $(event.node);
       var rolodex = new cRolodex();
    
       var type = el.parent().find("input[type='hidden']").val();

       rolodex.chooseFromRolodex(type, ML.sLId, true, false, function(args){

            if (args.OK == true) {
                    if(args.Clear == true)
                    {
                        $("#ClearAgent").dialog({
                            modal: true,
                            height: 150,
                            width: 350,
                            title: "",
                            dialogClass: "LQBDialogBox",
                            resizable: false,
                            buttons: [
                                {text: "OK",
                                    click: function(){
                                        // Remove Agent from official contacts
                                        var model = ractive.get();
                                        var data = {
                                            loanId: ML.sLId,
                                            viewModelJson: JSON.stringify(model),
                                            recordId: ractive.get("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id"),
                                        }
                                        
                                        callWebMethod("ClearAgent", 
                                            data,
                                            function(){
                                                alert("Error:  Could not remove agent.");
                                            }, 
                                            //On Success
                                            function(m){
                                                model = JSON.parse(m.d);
                                                ractive.set(model);
                                            });
                                    
                                        $(this).dialog("close");
                                    }
                                },
                                {text: "Cancel",
                                    click: function(){
                                        $(this).dialog("close");
                                    }
                                }
                            ]
                        });
                    }
                    else
                    {
                    var id = "";
                    var populateFromRolodex = false;
                    var agentType = args.AgentType;
                    
                    if(args.RecordId != "" && args.RecordId != <%=AspxTools.JsString(Guid.Empty) %> )
                    {
                        //If they're choosing from an agent record, simply set it's record id as the fee's bene_id.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", args.RecordId);
                        //Update paidTo DDL
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                        //Re-enable beneficiary automation.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                        
                        updateDirtyBit();
                        ractive.fire('recalc');
                    }
                    else
                    {
                        if(args.BrokerLevelAgentID != <%=AspxTools.JsString(Guid.Empty) %>)
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there is a BrokerLevelAgentID, populate using the rolodex and that id
                            
                            id=args.BrokerLevelAgentID;
                            populateFromRolodex = true;  
                        }
                        else
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there isn't a BrokerLevelAgentID, populate using the employee info
                            
                            id=args.EmployeeId;
                            populateFromRolodex = false;  
                        }
                        
                        
                        $("#ConfirmAgent").dialog({
                            modal: true,
                            height: 150,
                            width: 350,
                            title: "",
                            dialogClass: "LQBDialogBox",
                            resizable: false,
                            buttons: [
                                {text: "OK",
                                click: function(){
                                    callWebMethod("CreateAgent", 
                                        {loanId: ML.sLId, id: id, populateFromRolodex: populateFromRolodex, agentType: agentType},
                                        function(){alert("Error:  Could not create a new agent.");}, 
                                        //On Success
                                        function(d){
                                            var m = JSON.parse(d.d);
                                            // Get the recordID and set it for the bene_id
                                            recordId = m.RecordId;
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", recordId);
                                            //Update the fee's paidTo DDL
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                                            //Re-enable beneficiary automation.
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                                            
                                            updateDirtyBit();
                                            ractive.fire('recalc');
                                            
                                        });
                                    $(this).dialog("close");
                                    }
                                },
                                {text: "Cancel",
                                click: function(){
                                    $(this).dialog("close");
                                }}
                            ]
                        });
                    }

                }
            }
        });
	});

            ractive.on('domblur', function(event) {
                var kp = event.keypath;
                //sadly this blur event fires before the mask.js blur event 
                //by using setTimeout we queue up the function until after all the other
                //js code.
                window.setTimeout(function() {
                    ractive.updateModel(kp, true);
                });
            });

            ractive.on('delete', function(event, i, j, desc, amt, orgDesc) {
                var d = desc;
                if (!d)
                {
                    d = orgDesc;
                }
                $('#dialog-confirm').find('p').html('Are you sure you would like to remove the following fee? <br />').append(document.createTextNode(d + ' : ' + amt));
                $('#dialog-confirm').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function()
                        {
                            $(this).dialog("close");
                            updateDirtyBit();

                            var model = ractive.get();
                            var sectionList = model.SectionList;
                            sectionList[i].ClosingCostFeeList.splice(j, 1);
                        },
                        "No": function()
                        {
                            $(this).dialog("close");
                        }
                    },
                    closeOnEscape: false,
                    width: "400",
                    draggable: false,
                    resizable: false,
                    dialogClass: "LQBDialogBox"
                });
            });

            ractive.on('updatePaidBy', function(e, i, j, total) {
                var value = e.node.value;
                var obj = e.context;
                if (value == -1)
                {
                   var model = ractive.get(), 
                   fee = model.SectionList[i].ClosingCostFeeList[j];
                   fee.pmts[0].paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Seller.ToString("D")) %>;
                   
                   // Add new system payment
                   var newPayment = new Object();
                   newPayment.amt = "";
                   newPayment.ent = 0;
                   newPayment.is_fin = false;
                   newPayment.is_system = true;
                   newPayment.made = false;
                   newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Seller.ToString("D")) %>;
                   newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
                   newPayment.pmt_dt = "";
                   fee.pmts.push(newPayment);
                   
                   updateDirtyBit();
                }
                else if (value == 3)
                {
                    obj.pmts[0].paid_by = value;
                    obj.pmts[0].pmt_at = 1; // Set to 'at closing'
                    ractive.update(e.keypath);
                }
                else
                {
                    obj.pmts[0].paid_by = value;
                    ractive.update(e.keypath);
                }
                
                ractive.fire('recalc');
            });

            ractive.on('updatePaidBySplit', function(e)
            {
                var value = e.node.value;
                var obj = e.context;
                if (value == 3)
                {
                    obj.paid_by = value;
                    obj.pmt_at = 1; // Set to 'at closing'
                    ractive.update(e.keypath);
                }
                else
                {
                    obj.paid_by = value;
                    ractive.update(e.keypath);
                }
                
                ractive.fire('recalc');
            });
            
            ractive.on('AddSplitPayment', function(event, i, j) {
                updateDirtyBit();

                var model = ractive.get();
                var sectionList = model.SectionList;
                var fee = sectionList[i].ClosingCostFeeList[j];

                var newPayment = new Object();
                newPayment.amt = "";
                newPayment.ent = 0;
                newPayment.is_fin = false;
                newPayment.is_system = false;
                newPayment.made = false;
                newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Seller.ToString("D")) %>;
                newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
                newPayment.pmt_dt = "";

                fee.pmts.push(newPayment);
                ractive.fire('recalc');
            });

            ractive.on('updateCalc', function(event, i, j) {
                var model = ractive.get();
                var keypath = 'SectionList';
                var o = this.get(keypath);
                var m = new CalculationModal({ data: { fee: o[i].ClosingCostFeeList[j].f, loan: this.get()} });
                m.on('teardown', function() {
                    ractive.fire('recalc');
                    updateDirtyBit();
                });
            });

            ractive.on('recalc', function(event, success) {

                    window.setTimeout(function() {
                        var startRecalc = Date.now(),
                            model = ractive.get(),
                            data = { loanId: loanID, viewModelJson: JSON.stringify(ractive.get())},
                            fin = function(m) {
                            var load = Date.now();
                            lqblog('Time To Load Data ' + (load - startRecalc) + 'ms.');
                            model = JSON.parse(m.d);
                            var d = Date.now();

                            model.viewT = ractive.get("viewT");

                            ractive.set(model);

                            var c = Date.now();
                            lqblog('Time to set data ' + (c - d) + 'ms.');
                    },
                error = function(e) {
                    alert('Error');
                };
                        callWebMethod('CalculateData', data, error, fin);
                    });
            });
            
            function GetPaidByDesc(paidby)
            {
                var paidByDesc = "";
                if(paidby == 1)
                {
                    paidByDesc = "borr pd";
                }
                else if(paidby == 2)
                {
                    paidByDesc = "seller";
                }
                else if(paidby == 3)
                {
                    paidByDesc = "borr fin";
                }
                else if(paidby == 4)
                {
                    paidByDesc = "lender";
                }
                else if(paidby == 5)
                {
                    paidByDesc = "broker";
                }
                else if(paidby == 6)
                {
                    paidByDesc = "other";
                }
                
                return paidByDesc;
            }
            
            ractive.on('removePayment', function(e, i, j, k, amt, paidby, desc, orgDesc) {
                var d = desc;
                if (!d)
                {
                    d = orgDesc;
                }
                var paidByDesc = GetPaidByDesc(paidby);
                
                $('#dialog-confirm').find('p').text('Are you sure you would like to remove the ' + amt + ' ' + paidByDesc + ' split payment for the ' + d + '?');
                $('#dialog-confirm').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function()
                        {
                            $(this).dialog("close");
                            updateDirtyBit();
                            var model = ractive.get();
                            var sectionList = model.SectionList;
                            sectionList[i].ClosingCostFeeList[j].pmts.splice(k, 1);
                            ractive.fire('recalc');
                        },
                        "No": function()
                        {
                            $(this).dialog("close");
                        }
                    },
                    closeOnEscape: false,
                    width: "400",
                    draggable: false,
                    resizable: false,
                    dialogClass: "LQBDialogBox"
                });
            });

            ractive.on('addFee', function(event, i) {
                var model = ractive.get();

                var sectionList = model.SectionList;

                var section = sectionList[i],
                 sectionName = section.SectionName,
                 data = { loanId: loanID, viewModelJson: JSON.stringify(model), sectionName: sectionName };

                callWebMethod('GetAvailableClosingCostFeeTypes', data,

                     function() {
                         alert('Error');
                     },

                     function(m) {
                         var result = JSON.parse(m.d);
                         var y = new FeePickerModal({ data: result.Section });
                         y.on('selectFee', function(e, si) {
                             // OPM 468071 - Check if selected fee has existing sub fees.
                             var selectedFee = result.Section.FeeListAsSellerFees[si];

                             var subFees;
                             for (var idx=0; idx< result.SubFeesBySourceFeeTypeId.length; idx++)
                             {
                                 if (result.SubFeesBySourceFeeTypeId[idx].SourceFeeTypeId == selectedFee.typeid)
                                 {
                                     subFees = result.SubFeesBySourceFeeTypeId[idx].SubFees;
                                     break;
                                 }
                             }

                             if (typeof(subFees) !== "undefined" && subFees.length > 0) {
                                 // Confirm sub fee overwrite.
                                 var confirmDlg = new SubFeeConfirmOverwriteModal({ data: {SubFees: subFees, ParentDesc: selectedFee.desc } });
                                 confirmDlg.on('subFeeDlgOk', function(e, si) {
                                     addSelectedFee(selectedFee, section, i);
                                     confirmDlg.teardown();
                                     y.teardown();
                                     return false;
                                 });
                             }
                             else {
                                 // No sub fees, just add seleced fee.
                                 addSelectedFee(selectedFee, section, i);
                                 y.teardown();
                                 return false;
                             }

                             return false;
                         });
                     });
            });

            function addSelectedFee (selectedFee, feeSection, sectionIndex) {
                selectedFee.pmts[0].paid_by = 2; // Set as seller paid by.
                feeSection.ClosingCostFeeList.push(selectedFee);
                updateDirtyBit();
                $(".RactiveDDL" + sectionIndex + (feeSection.ClosingCostFeeList.length - 1)).css("width", $(".OptionsList").width() - $(".DDLImg").width() - 2);
                ractive.fire('recalc');
            }

            ractive.on('showCalendar', function(e, id) {
                return displayCalendar(id);
            });
            var canHideDescriptions = true;
            var currentFeeRow = 0;
            var currentSection = 0;

            ractive.on('onHoverDescriptions', function(event) {
                canHideDescriptions = false;
            });

            ractive.on('onMouseOutDescriptions', function(event) {
                canHideDescriptions = true;
            });

            ractive.on('highlightLI', function(event) {
                var node = $(event.node);
                var allLI = node.parent().find('li');
                unhighlightLI(allLI);
                highlightLI(node);

            });
            
            ractive.on('CanShopChange', function(e, i, j){
                var keypath = 'SectionList.' + i + '.ClosingCostFeeList.' + j + '.did_shop';
                var can_shop = $(e.node).prop('checked');
                if(!can_shop)
                {
                    ractive.set(keypath, false);
                }
            });

            ractive.on('tpClick', function(e, i, j) {
                var model = ractive.get();
                if (!model.SectionList[i].ClosingCostFeeList[j].tp) {
                    model.SectionList[i].ClosingCostFeeList[j].aff = false;
                    ractive.set(model);
                }
            });

            ractive.on('unhighlightLI', function(event) {
                var node = $(event.node);
                unhighlightLI(node);
            });


            ractive.on('selectDDL', function(event) {
                var node = $(event.node);
                var text = node.text();

                var td = node.parent().parent();

                td.find(".RactiveDDL").val(node.text());
                td.find("input[type='hidden']").val(node.attr('val'));
              
                var value = node.attr('val');

                node.parent().hide();
                $("#OptionsContainer").append(node.parent());
                canHideDescriptions = true;
                
                // Get Agent Id
                var args = new Object();
                args["loanid"] = ML.sLId;
                args["IsArchivePage"] = false;
                args["AgentType"] = value;
                  
                var result = gService.cfpb_utils.call("GetAgentIdByAgentType", args);
                
                if (!result.error) {
                    //Update the fee's Beneficiary ID
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_id", result.value.RecordId);
                    // Update the fee's Beneficiary Description
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_desc", result.value.CompanyName);
                    //Update the fee's paidTo DDL
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene", value);
                    //Re-enable beneficiary automation.
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".disable_bene_auto", false);                
                }
                
                updateDirtyBit();
                ractive.fire('recalc');
            });

            ractive.on('reselectInput', function(event) {
                //this is called by the UL itself
                $(event.node).parent().find('input').focus();
            });

            $(".RactiveDDL").css("width", $(".BeneficiaryOptions").width());
            $(".OptionsList").css("width", $(".OptionsList").width() + $(".DDLImg").width() + 2);

            var IsReadOnly = $('#_ReadOnly').val() === 'True';
            if (IsReadOnly)
            {
                $("*").attr("readonly", "");
                $("input[type!='text'], select").attr("disabled", "true");
                $('input[type=text]').css('background-color', 'lightgrey');
            }
        });
    </script>
    
    <div style="display: none;" id="dialog-confirm" title="Delete fee?">
        <p style="width: 100%">
        </p>
    </div>
    <form id="form1" runat="server">

    <script id="TimeControl" type="text/ractive">
        Time: <input class="TimeInput" type="text" id="{{id + ''}}" value="{{value_hour}}" readonly="{{readonly}}" on-blur="hourBlur" on-keyup="hourKeyUp" maxlength="2"/>
        : <input class="TimeInput" type="text" id="{{id + '_minute'}}" value="{{value_min}}" readonly="{{readonly}}" on-blur="minuteBlur" maxlength="2"/>
        <select id="{{id + '_am'}}"  value="{{value_am}}" readonly="{{readonly}}">
          <option value="AM">AM</option>
          <option value="PM">PM</option>
        </select>
    </script>
        
    <script id="DatePicker" type="text/ractive">
        <input type="text" class="mask date" readonly="{{readonly}}"  preset="date" id="{{id}}" intro="initMask" on-blur="updatePicker"   value="{{value}}" />
        <a on-click="displayCalendar" href="#" on-click="showCalendar:{{id}}" >
            <img title="Open calendar" src="[[VRoot]]/images/pdate.gif" border="0">
        </a>
    </script>

    <script id='RactiveDDL' type='text/ractive'>
            <div class="RactiveDDLContainer">
                <input on-keydown='keyScroll' disabled='{{disabled}}' {{IsArchive || IsReadonly?"": "NotEditable"}} class="{{disabled? "Disabled": ""}} RactiveDDL {{'RactiveDDL'+i+j}} {{Prefix}}DDL" readonly type="text" on-click="showOptions: {{i}},{{j}},{{k}},'{{Prefix}}Options'"  on-blur="hideDesc: '{{Prefix}}Options'"  /><img class="DDLImg" src= "../../images/IEArrow.png" on-click="showOptions: {{i}},{{j}},{{k}}, '{{Prefix}}Options'" >
                <input type="hidden" value="{{value}}" />
            </div>
    </script>

    <script id="ClosingCostSectionFeeTemplate" type="text/ractive">
        <tr class="{{ j % 2 == 0 ? "GridItem" : "GridAlternatingItem"}}">
            <td class="FieldLabel"> 
                <input disabled="{{sIsRequireFeesFromDropDown}}" type="checkbox" checked="{{is_title}}" /> 
            </td>
            <td>
                <input type="text" value="{{desc}}" readonly="{{sIsRequireFeesFromDropDown}}" />
                {{#if desc != org_desc}}
                    <div >Type: {{org_desc}}</div>
                {{/if}}
            </td>
            <td>
                <input type="checkbox" checked="{{is_optional}}" disabled="{{sIsRequireFeesFromDropDown || SectionName.indexOf("H - ") == -1}}" />
            </td>
            <td>
                <input type="checkbox" on-change="recalc" checked="{{apr}}" disabled="{{sIsRequireFeesFromDropDown}}" /> 
            </td>
            <td>
               <input type="checkbox" checked="{{fha}}" disabled="{{sIsRequireFeesFromDropDown}}" />
            </td>
            <td>
                <ractiveDDL Prefix='Beneficiary' value = '{{bene}}' /><img class="ContactImg" on-mouseover="onContactHover" on-mouseout="onContactMouseOut" on-click="showAgentPicker : {{i}}, {{j}}" src="../../images/contacts.png">
                {{#if bene_desc || bene_id != <%=AspxTools.JsString(Guid.Empty) %>}}
                    <div> Company: {{bene_desc}} </div>
                {{/if}}
            </td>
            {{#if CanReadDFLP}}
                <td>
                    <input type="checkbox" disabled="{{!CanSetDFLP}}" checked="{{dflp}}" />
                </td>
            {{/if}}
            <td>
                <input type="checkbox" disabled = "{{DisableTPAffIfHasContact && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>')}}" checked="{{tp}}"  on-click="tpClick:{{i}},{{j}}" /> 
            </tD>
            <td>
               <input type="checkbox" checked="{{aff}}"  disabled="{{!tp || (DisableTPAffIfHasContact && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>'))}}" /> 
            </td>
            <td>
                <input type="checkbox" checked="{{can_shop}}" on-change="CanShopChange: {{i}}, {{j}}" disabled="{{sIsRequireFeesFromDropDown || disc_sect == 2 || disc_sect == 3}}" /> 
            </td>
            <td>
                <input type="checkbox" disabled="{{!can_shop}}" checked="{{did_shop}}" />
            </td>
            <td>
                {{#if SectionName.indexOf("G - ") == -1 && f.t !== 0 && f.t !== 4 && f.t !== 5 && f.t !== 14 && f.t !== 17 && f.t !== 18 && f.t !== 21}}
                <input type="button" value="C" on-click="updateCalc:{{i}},{{j}}"/>
                {{/if}}
            </td>
            <td>
                {{#if f.t === 3}} 
                    <input type="text" value="{{f.base}}" class="mask money" preset="money" intro="numericTransition: true" />
                {{else}}
                    <input type="text" value="{{total}}" class="money" preset="money"  readonly="true" />
                {{/if}}
            </td>
            <td>
                  {{#if (pmts && pmts.length == 1)}} 
                    <select value="[[pmts[0].paid_by == 0 ? 1 : pmts[0].paid_by]]" on-change="updatePaidBy:{{i}},{{j}},{{total}}">
                        <option value="2">seller</option>
                        {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                            <option value="4">lender</option>
                        {{/if }}
                        <option value="5">broker</option>
                    </select>
                    {{else}}
                    &nbsp;
                    {{/if}}
            </td>
            <td>
                {{#if (pmts && pmts.length == 1)}} 
                    <select disabled="{{pmts[0].paid_by == 3}}" value="{{pmts[0].pmt_at}}" on-change="recalc">
                        <option value="1">at closing</option>
                        <option value="2">outside of closing</option>                
                    </selecT>
                {{else}}
                    &nbsp;
                {{/if}}
                
            </td>
            <td>
                {{#if pmts && pmts.length == 1}} 
                    <datePicker readonly="{{pmts[0].pmt_at == 1}}" value="{{pmts[0].pmt_dt}}" id="d{{i}}{{j}}" />
                {{else}}
                    &nbsp;
                {{/if}}
            </td>
            <td>
                  {{#if (pmts && pmts.length == 1)}}  
                     <select disabled="true" value="{{pmts[0].responsible}}" >
                        <option value="2">seller</option>
                        <option value="3">lender</option>
                        <option value="4">broker</option>
                     </select>
                  {{else}}
                  &nbsp;
                  {{/if}}
            </td>
            <td> 
                {{#if f.t !== 4 && f.t !== 5 && f.t !== 21 && SectionName.indexOf("G - ") == -1 && (typeid !== <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId) %> || total == '$0.00')}}
                    {{#if f.t === 3}} 
                        <input type="button" value=" - " on-click="delete:{{i}},{{j}},{{desc}},{{f.base}},{{org_desc}}"/>
                    {{else}}
                        <input type="button" value=" - " on-click="delete:{{i}},{{j}},{{desc}},{{total}},{{org_desc}}"/>
                    {{/if}}
                {{/if}}
            </td>
        </tr>
        {{#if pmts.length > 1}} 
        {{#pmts:k}}
            {{>ClosingCostFeePaymentTemplate}}
        {{/pmts}}
        {{/if}}
    </script>

    <script id="ClosingCostFeePaymentTemplate" type="text/ractive">
        <tr>
            <td colspan="10">
                &nbsp;
            </td>
            <td>
                {{#if (is_system)}}
                    <input type="button" value="+" on-click="AddSplitPayment:{{i}},{{j}}" />
                {{/if}}
            </td>
            <td>
                <input readonly="{{is_system}}" style="text-align:right" type="text" value="{{amt}}" class="mask money" preset="money" intro="numericTransition: true"/>
            </td>
            <td>
                       <select value="{{paid_by}}" disabled="{{true}}" on-change="updatePaidBySplit" >
                        <option value="2">seller</option>
                        {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                            <option value="4">lender</option>
                        {{/if }}
                        <option value="5">broker</option>
                    </select>
            </td>
            <td>
                <select value="{{pmt_at}}" disabled="{{is_system || paid_by == 3}}" on-change="recalc">
                        <option value="1">at closing</option>
                        <option value="2">outside of closing</option>
                </select>
            </td>
            <td>
                <input type="text" preset="date" class="mask date" id="d{{i}}{{j}}{{k}}" intro="datepickerTransition" recalc value="{{pmt_dt}}" readonly="{{is_system || pmt_at == 1}}"/>&nbsp;
                <a on-click="displayCalendar" href="#" on-click="showCalendar:d{{i}}{{j}}{{k}}" >
                <img title="Open calendar" src="[[VRoot]]/images/pdate.gif" border="0" />
                </a>
            </td>
            <td>
                {{#if !is_system}}
                    <input type="button" value=" - " on-click="removePayment:{{i}},{{j}},{{k}},{{amt}},{{paid_by}},{{desc}},{{org_desc}}"/>
                {{/if}}
            </td>
        </tr>
    </script>

    <script id="ClosingCostTemplate" type="text/ractive">
        <div id="sec-closingcost8" class="div-main LenderTypeEstimate">
             {{#SectionList:i}}  
             <div class="FormTableHeader div-title">{{SectionName}}
                <span style="float:right; display:inline-block; padding-right: 5px;">Total: {{TotalAmount}}</span>
             </div>
             <table border="0" style="white-space: nowrap; padding-left: 5px;" cellpadding="2" cellspacing="0" class="table-closingcost" >
             {{#if ClosingCostFeeList.length > 0}}
                <thead>
                    <tr>
                        <th>
                            Title
                        </th>
                        <th>
                            Description/Memo 
                        </th>
                        <th>
                            Optional
                        </th>
                        <th>
                            APR
                        </th>
                        <th>
                            FHA
                        </th>
                        <th>
                            Paid to
                        </th>
                        {{#if CanReadDFLP}}
                            <th>
                                DFLP
                            </th>
                        {{/if}}
                        <th>
                            TP
                        </th>
                        <th>
                            AFF
                        </th>
                        <th>
                            Can<br/>Shop
                        </th>
                        <th>
                            Did<br/>Shop
                        </th>
                        <th>&nbsp;</th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Paid by
                        </th>
                        <th>
                            Payable
                        </th>
                        
                        <th>
                            Date paid
                        </th>
                        <th>
                            Responsible Party
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                {{/if}}
                <tbody>
                    {{#ClosingCostFeeList:j}}
                        {{>ClosingCostSectionFeeTemplate}}
                    {{/#ClosingCostFeeList}}
                </tbody>
                {{#if SectionName.indexOf("G - ") == -1}}
                <tfooter>
                    <tr>
                        <td>
                            <div class="div-image"><input type="button" value="Add" on-click="addFee:{{i}}"/></div>
                        </td>
                    </tr>
                </tfooter>
                {{/if}}
                   </table>
            {{/SectionList}}
        </div>
        <div ID="OptionsContainer" >
            <asp:Repeater runat="server" ID="BeneficiaryRepeater">
                <HeaderTemplate>
                    <ul class="BeneficiaryOptions OptionsList" tabindex="1" on-focus="reselectInput" on-blur="hideDesc" on-mouseenter="onHoverDescriptions" on-mouseleave="onMouseOutDescriptions">
                </HeaderTemplate>
                <ItemTemplate>
                    <li on-mouseenter="highlightLI" on-mouseleave="unhighlightLI" on-click="selectDDL" val="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"> <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></li>
                </ItemTemplate>
                <FooterTemplate>
                </ul>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </script>

    <script id="CalculationModalContent" type="text/ractive">
         <p>
            {{#if fee.t == 0 || fee.t == 4 || fee.t == 5 || fee.t == 17 || fee.t == 18}} 
                Not Supported
            
            {{/if}}
            {{#if fee.t == 1 || fee.t == 2 || fee.t == 3}}
                <p>
                    <select value="{{fee.t}}">
                        <option value="3">Flat Amount</option>
                        <option value="1">Full</option>
                        <option value="2">Percent Only</option>
                    </select>
                </p>
            {{/if}}  
            {{#if fee.t == 1}}
            <label>Percent <input type="text" value="{{fee.p}}" preset="percent" intro="initMask" class="money mask"/> </label>  of  
            <asp:Repeater runat="server" ID="PercentRepeater">
                        <HeaderTemplate>
                            <select NotEditable="true" value="{{fee.pt}}" >
                        </HeaderTemplate>
                        <ItemTemplate>
                            <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                        </ItemTemplate>
                        <FooterTemplate>
                        </select>
                        </FooterTemplate>
                      </asp:Repeater>
                        + <input type="text" value="{{fee.base}}" preset="money" intro="initMask" class="money mask" /> 
            {{/if}}
            {{#if fee.t == 2}} 
                <label>Percent <input type="text" class="mask"  value="{{fee.p}}" preset="percent" intro="initMask" /> </label>  of  
                <asp:Repeater runat="server" ID="PercentRepeater2">
                        <HeaderTemplate>
                            <select NotEditable="true" value="{{fee.pt}}" >
                        </HeaderTemplate>
                        <ItemTemplate>
                            <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                        </ItemTemplate>
                        <FooterTemplate>
                        </select>
                        </FooterTemplate>
                      </asp:Repeater> 
            {{/if}}
            {{#if fee.t == 3}} 
                <input type="text" value="{{fee.base}}" preset="money" class="mask money"  intro="initMask" /> 
            {{/if}}
            {{#if fee.t == 6}}
                <input type="text" value="{{fee.period}}" intro="initMask" preset="numeric" style="width: 40px;" /> period
            {{/if}}
            {{#if fee.t == 7}}
                <input on-change="recalc" type="checkbox" checked="{{loan.sIPiaDyLckd}}"> Lock
                <input type="text" value="{{loan.sIPiaDy}}" readonly="{{!loan.sIPiaDyLckd}}" class="mask money" preset="numeric" intro="initMask" /> days @
                <input on-change="recalc" type="checkbox" checked="{{loan.sIPerDayLckd}}" /> Lock
                <input type="text" value="{{loan.sIPerDay}}" readonly="{{!loan.sIPerDayLckd}}" decimalDigits="6" class="mask money" preset="money" intro="initMask" /> per day
            {{/if}}
            {{#if fee.t == 8}}
                Not Supported
            {{/if}}
            {{#if fee.t == 9 || fee.t == 10}}
                <input type="text" preset="numeric" value="{{fee.period}}" intro="initMask" /> months @ <input type="text" value="{{fee.base}}" class="mask money" preset="money" intro="initMask" readonly="true" /> / month
            {{/if}}
            {{#if fee.t == 11}}
                <input type="text" preset="numeric" value="{{fee.period}}" intro="initMask" readonly="true" /> periods
            {{/if}}
            {{#if fee.t == 12 || fee.t == 13 || fee.t == 15 || fee.t == 16}}
                <input type="text" preset="numeric" value="{{fee.period}}" intro="initMask" /> months @ <input type="text" readonly="true" value="{{fee.base}}" class="mask money" preset="money" intro="initMask" /> / month
            {{/if}}
          </p>
          
          <p>
            <input type="button" value=" OK " on-click="close: true"/> <input type="button" value=" Cancel " on-click="close: false"/> 
          </p>
    </script>

    <script id="FeePickerModalContent" type="text/ractive">
        <h3>Available Fees for {{SectionName}}</h3>
        <div>
            <ul>
            {{#FeeListAsSellerFees:i}} 
                <li><a href="#" on-click="selectFee:{{i}}">{{desc}}</a></li>
            {{/FeeListAsSellerFees}}
            {{^FeeListAsSellerFees}}
                <li>No Available Fees</li>
            {{/FeeListAsSellerFees}}
            </ul>
            <input type="button" value=" Cancel " on-click="close"/> 
        </div>
    </script>

    <script id="SubFeeConfirmOverwriteContent" type="text/ractive">
        <div class="SubFeeConfirmOverwriteContent">
            The following {{#if SubFees.length > 1}}fees{{else}}fee{{/if}}:

            <ul>
                {{#SubFees:i}} 
                    <li>{{desc}}</li>
                {{/SubFees}}
            </ul>

            {{#if SubFees.length > 1}}are{{else}}is{{/if}} considered to be the same fee type as the "{{ParentDesc}}"{{#if ParentDesc.substr(ParentDesc.length-3) !== "fee" }}&nbsp;fee{{/if}}.
            Adding the "{{ParentDesc}}"{{#if ParentDesc.substr(ParentDesc.length-3) !== "fee" }}&nbsp;fee{{/if}} will cause the current {{#if SubFees.length > 1}}fees{{else}}fee{{/if}}
            to be removed. Would you like to proceed?
         
            <br>
            <br>

            <div class="SubFeeConfirmOverwriteContentBtns">
                <input type="button" value=" OK " on-click="subFeeDlgOk:{{i}}"/> 
                <input type="button" value=" Cancel " on-click="close"/> 
            </div>
        </div>
    </script>

    <script id="modal" type="text/ractive">
        <div class='modal-background'><div class='modal-outer'><div class='modal'>{{>modalContent}}</div></div></div>
    </script>
    
    <div id="ConfirmAgent" style="display: none;">Selecting this contact will add it to the official contact list.</div>
    <div id="ClearAgent" style="display:none;">This action will remove the contact from the official contact list.</div>
    <div class="MainRightHeader" runat="server" id="PageHeader">Non Borrower-Responsible Closing Costs</div>
    
    <div class="PageContainer">
        <div class="GridHeader">Closing Cost Details</div>
        <div id="ClosingCostFeessContainer"></div>
    </div>
    </form>
</body>
</html>
