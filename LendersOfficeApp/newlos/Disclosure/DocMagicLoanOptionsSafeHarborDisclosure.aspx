﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocMagicLoanOptionsSafeHarborDisclosure.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.DocMagicLoanOptionsSafeHarborDisclosure" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        #LoanComparisonContainer
        {
            display: table;
        }
        #LoanComparisonTable
        {
            border-spacing: 0px;
            border-collapse: collapse;
            width: 100%;
        }
        #LoanComparisonTable col
        {
            width: 25%;
        }
        .DataFormatting
        {
            text-align: center;
            vertical-align: top;	
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            var $LoanComparisonTable = $('table#LoanComparisonTable');
            $LoanComparisonTable.find("tbody > tr:even").not(':first').addClass("GridItem");
            $LoanComparisonTable.find("tbody > tr:odd").not(':first').addClass("GridAlternatingItem");

            $LoanComparisonTable.find("tbody > tr").find("td:not(:first)").addClass("DataFormatting");
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="MainRightHeader">DocMagic Loan Options Disclosure with Anti-Steering Safe Harbor</div>
        <div id="LoanComparisonContainer" class="InsetBorder">
            <div class="FormTableSubheader">Loan Comparison</div>
            <table id="LoanComparisonTable" class="FieldLabel">
                <col />
                <col />
                <col />
                <col />
                <tr class="GridHeader">
                    <td></td>
                    <td>Lowest Interest Rate</td>
                    <td>Lowest Origination Fees / Discount Points</td>
                    <td>Lowest Rate with No Risky Features</td>
                </tr>
                <tr>
                    <td>Lender name</td>
                    <td><asp:TextBox ID="sDocMagicAntiSteeringLowestInterestRateLenderName" width="140px" runat="server" /></td>
                    <td><asp:TextBox ID="sDocMagicAntiSteeringLowestFeeLenderName" width="140px" runat="server" /></td>
                    <td><asp:TextBox ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureLenderName" width="140px" runat="server" /></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><asp:TextBox ID="sDocMagicAntiSteeringLowestInterestRateDescription" width="140px" runat="server" /></td>
                    <td><asp:TextBox ID="sDocMagicAntiSteeringLowestFeeDescription" width="140px" runat="server" /></td>
                    <td><asp:TextBox ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureDescription" width="140px" runat="server" /></td>
                </tr>
                <tr>
                    <td>Initial interest rate</td>
                    <td><ml:PercentTextBox ID="sDocMagicAntiSteeringLowestInterestRateInitialRate" width="140px" runat="server"></ml:PercentTextBox></td>
                    <td><ml:PercentTextBox ID="sDocMagicAntiSteeringLowestFeeInitialRate" width="140px" runat="server"></ml:PercentTextBox></td>
                    <td><ml:PercentTextBox ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureInitialRate" width="140px" runat="server"></ml:PercentTextBox></td>
                </tr>
                <tr>
                    <td>Discount points</td>
                    <td><ml:PercentTextBox ID="sDocMagicAntiSteeringLowestInterestRateDiscountPoints" width="140px" runat="server"></ml:PercentTextBox></td>
                    <td><ml:PercentTextBox ID="sDocMagicAntiSteeringLowestFeeDiscountPoints" width="140px" runat="server"></ml:PercentTextBox></td>
                    <td><ml:PercentTextBox ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureDiscountPoints" width="140px" runat="server"></ml:PercentTextBox></td>
                </tr>
                <tr>
                    <td>Origination fee</td>
                    <td><ml:MoneyTextBox ID="sDocMagicAntiSteeringLowestInterestRateOriginationFee" width="140px" runat="server"></ml:MoneyTextBox></td>
                    <td><ml:MoneyTextBox ID="sDocMagicAntiSteeringLowestFeeOriginationFee" width="140px" runat="server"></ml:MoneyTextBox></td>
                    <td><ml:MoneyTextBox ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureOriginationFee" width="140px" runat="server"></ml:MoneyTextBox></td>
                </tr>
                <tr>
                    <td>Our compensation</td>
                    <td><ml:MoneyTextBox ID="sDocMagicAntiSteeringLowestInterestRateOurCompensation" width="140px" runat="server"></ml:MoneyTextBox></td>
                    <td><ml:MoneyTextBox ID="sDocMagicAntiSteeringLowestFeeOurCompensation" width="140px" runat="server"></ml:MoneyTextBox></td>
                    <td><ml:MoneyTextBox ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureOurCompensation" width="140px" runat="server"></ml:MoneyTextBox></td>
                </tr>
                <tr>
                    <td>Prepayment penalty</td>
                    <td><ml:MoneyTextBox ID="sDocMagicAntiSteeringLowestInterestRatePrepaymentFeeAmount" width="140px" runat="server"></ml:MoneyTextBox></td>
                    <td><ml:MoneyTextBox ID="sDocMagicAntiSteeringLowestFeePrepaymentFeeAmount" width="140px" runat="server"></ml:MoneyTextBox></td>
                    <td><ml:MoneyTextBox ID="sDocMagicAntiSteeringLowestRateNoRiskyFeaturePrepaymentFeeAmount" width="140px" runat="server"></ml:MoneyTextBox></td>
                </tr>
                <tr>
                    <td>Balloon payment</td>
                    <td>Due after <asp:TextBox ID="sDocMagicAntiSteeringLowestInterestRateBalloonDue" width="20px" runat="server"></asp:TextBox> months</td>
                    <td>Due after <asp:TextBox ID="sDocMagicAntiSteeringLowestFeeBalloonDue" width="20px" runat="server"></asp:TextBox> months</td>
                    <td>Due after <asp:TextBox ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureBalloonDue" width="20px" runat="server"></asp:TextBox> months</td>
                </tr>
                <tr>
                    <td>Negative amortization</td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestInterestRateIsNegArmYes" GroupName="sDocMagicAntiSteeringLowestInterestRateIsNegArm" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestInterestRateIsNegArmNo" GroupName="sDocMagicAntiSteeringLowestInterestRateIsNegArm" Text="No" runat="server" value="0" />
                    </td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestFeeIsNegArmYes" GroupName="sDocMagicAntiSteeringLowestFeeIsNegArm" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestFeeIsNegArmNo" GroupName="sDocMagicAntiSteeringLowestFeeIsNegArm" Text="No" runat="server" value="0" />
                    </td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsNegArmYes" GroupName="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsNegArm" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsNegArmNo" GroupName="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsNegArm" Text="No" runat="server" value="0" />
                    </td>
                </tr>
                <tr>
                    <td>Demand feature</td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestInterestRateIsDemandFeatureYes" GroupName="sDocMagicAntiSteeringLowestInterestRateIsDemandFeature" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestInterestRateIsDemandFeatureNo" GroupName="sDocMagicAntiSteeringLowestInterestRateIsDemandFeature" Text="No" runat="server" value="0" />
                    </td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestFeeIsDemandFeatureYes" GroupName="sDocMagicAntiSteeringLowestFeeIsDemandFeature" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestFeeIsDemandFeatureNo" GroupName="sDocMagicAntiSteeringLowestFeeIsDemandFeature" Text="No" runat="server" value="0" />
                    </td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsDemandFeatureYes" GroupName="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsDemandFeature" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsDemandFeatureNo" GroupName="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsDemandFeature" Text="No" runat="server" value="0" />
                    </td>
                </tr>
                <tr>
                    <td>Shared equity</td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestInterestRateIsSharedEquityYes" GroupName="sDocMagicAntiSteeringLowestInterestRateIsSharedEquity" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestInterestRateIsSharedEquityNo" GroupName="sDocMagicAntiSteeringLowestInterestRateIsSharedEquity" Text="No" runat="server" value="0" />
                    </td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestFeeIsSharedEquityYes" GroupName="sDocMagicAntiSteeringLowestFeeIsSharedEquity" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestFeeIsSharedEquityNo" GroupName="sDocMagicAntiSteeringLowestFeeIsSharedEquity" Text="No" runat="server" value="0" />
                    </td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedEquityYes" GroupName="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedEquity" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedEquityNo" GroupName="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedEquity" Text="No" runat="server" value="0" />
                    </td>
                </tr>
                <tr>
                    <td>Shared appreciation</td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciationYes" GroupName="sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciation" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciationNo" GroupName="sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciation" Text="No" runat="server" value="0" />
                    </td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestFeeIsSharedAppreciationYes" GroupName="sDocMagicAntiSteeringLowestFeeIsSharedAppreciation" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestFeeIsSharedAppreciationNo" GroupName="sDocMagicAntiSteeringLowestFeeIsSharedAppreciation" Text="No" runat="server" value="0" />
                    </td>
                    <td>
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedAppreciationYes" GroupName="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedAppreciation" Text="Yes" runat="server" value="1" />
                        <asp:RadioButton ID="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedAppreciationNo" GroupName="sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedAppreciation" Text="No" runat="server" value="0" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
