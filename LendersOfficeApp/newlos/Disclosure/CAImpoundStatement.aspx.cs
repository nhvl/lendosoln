///
/// Author: Matthew Flynn
/// 
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
	public partial class CAImpoundStatement : LendersOfficeApp.newlos.BaseLoanPage
	{


        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "California Impound Statement";
            this.PageID = "CAImpoundStatement";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CCAImpoundStatementPDF);

			CACompoundStatementZip.SmartZipcode(CACompoundStatementCity, CACompoundStatementState);

            RegisterJsScript("jquery-1.6.min.js");
			RegisterJsScript("LQBPopup.js");
        }

		override protected void LoadData()
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(CAImpoundStatement));
			dataLoan.InitLoad();

			IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.CACompoundStatement, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 
			
			CACompoundStatementCompanyName.Text = broker.CompanyName;
			CACompoundStatementStreetAddr.Text = broker.StreetAddr;
			CACompoundStatementCity.Text = broker.City;
			CACompoundStatementState.Value = broker.State;
			CACompoundStatementZip.Text = broker.Zip;
			CACompoundStatementPhoneOfCompany.Text = broker.PhoneOfCompany;
			CACompoundStatementFaxOfCompany.Text = broker.FaxOfCompany;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
