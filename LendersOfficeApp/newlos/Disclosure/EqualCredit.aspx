<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="EqualCredit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Disclosure.EqualCredit" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>EqualCredit</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">        
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
  <script language="javascript">
<!--
      var oRolodex = null;  
      function _init() {
          if (null == oRolodex)
              oRolodex = new cRolodex();
      }
//-->
  </script>

    <form id="EqualCredit" method="post" runat="server">
    <table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
		<td nowrap class="MainRightHeader">Equal Credit Opportunity Act</td>
    </tr>
    <tr>
	<td style="PADDING-LEFT: 10px">
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td nowrap class="FieldLabel">Broker Name&nbsp;</td><td><asp:TextBox ID="ECOACompanyName" Runat="server" Width="255px"></asp:TextBox>
		</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td></td>
			<td>
				<uc:CFM id="CFM" runat="server" Type="28" CompanyNameField="ECOAAgentCompanyName" StreetAddressField="ECOAAgentStreetAddr" CityField="ECOAAgentCity" StateField="ECOAAgentState" ZipField="ECOAAgentZip"></uc:CFM>
			</td>
		</tr>
		
		<tr>
			<td nowrap class="FieldLabel">Federal Agency&nbsp;</td><td><asp:TextBox ID="ECOAAgentCompanyName" Runat="server" Width="255px" ></asp:TextBox></td>			
		</tr>
		<tr>
			<td nowrap class="FieldLabel">Address&nbsp;</td><td><asp:TextBox ID="ECOAAgentStreetAddr" Runat="server" Width="255px"></asp:TextBox></td>
		</tr>
        <TR>
          <TD noWrap class=FieldLabel>City</TD>
          <TD noWrap><asp:TextBox id=ECOAAgentCity runat="server" Width="160px"></asp:TextBox><ml:StateDropDownList id=ECOAAgentState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=ECOAAgentZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></TD>
        </TR>
		</table>
	</td>
	</tr>
	</table>

     </form>
	<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
  </body>
</html>
