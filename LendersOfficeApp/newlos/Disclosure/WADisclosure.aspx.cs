﻿namespace LendersOfficeApp.newlos.Disclosure
{
    using System;
    using DataAccess;

    public partial class WADisclosure : BaseLoanPage
    {
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(WADisclosure));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.WADisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            sWADisclosurePrepareD.Text = preparer.PrepareDate_rep;

            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;

            aCFirstNm.Text = dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;

            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;

            sTerm.Text = dataLoan.sTerm_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sWADisclosureVersionT, dataLoan.sWADisclosureVersionT);

            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
            sFullyIndexedR.Text = dataLoan.sFullyIndexedR_rep;
            sFullyIndexedMPmt.Text = dataLoan.sFullyIndexedMPmt_rep;
            sRLifeCapR.Text = dataLoan.sRLifeCapR_rep;
            sGfeMaxProThisMPmtAndMIns.Text = dataLoan.sGfeMaxProThisMPmtAndMIns_rep;
            sRAdj1stD.Text = dataLoan.sRAdj1stD_rep;
            sIsRealEstateTaxIncludeInMPmt.Checked = dataLoan.sIsRealEstateTaxIncludeInMPmt;
            sIsHazardInsuranceIncludeInMPmt.Checked = dataLoan.sIsHazardInsuranceIncludeInMPmt;
            sIsMortgageInsuranceIncludeInMPmt.Checked = dataLoan.sIsMortgageInsuranceIncludeInMPmt;
            sIsHOAIncludeInMPmt.Checked = dataLoan.sIsHOAIncludeInMPmt;

            sLOrigF.Text = dataLoan.sLOrigF_rep;
            sMBrokF.Text = dataLoan.sMBrokF_rep;
            sLDiscnt.Text = dataLoan.sLDiscnt_rep;
            sLDiscntPc.Text = dataLoan.sLDiscntPc_rep;
            sWAOtherF.Text = dataLoan.sWAOtherF_rep;
            sWAOtherFLckd.Checked = dataLoan.sWAOtherFLckd;
            sGfeHavePpmtPenalty.Checked = dataLoan.sGfeHavePpmtPenalty;
            sGfeIsBalloon.Enabled = dataLoan.sGfeIsBalloonLckd;
            sGfeIsBalloon.Checked = dataLoan.sGfeIsBalloon;
            sIsGfeRateLocked.Checked = dataLoan.sIsGfeRateLocked;
            sIsWAReduceDoc.Checked = dataLoan.sIsWAReduceDoc;
            sBrokComp1.Text = dataLoan.sBrokComp1_rep;

            // Bind dataloan value to user control.
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            // Init your user controls here, i.e: bind drop down list.
            this.PageTitle = "Washington State Disclosure";
            this.PageID = "WADisclosure"; 
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CWADisclosurePDF); // Only if this page has a pdf printing class.


            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sWADisclosureVersionT(sWADisclosureVersionT);
            sSpZip.SmartZipcode(sSpCity, sSpState);

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
