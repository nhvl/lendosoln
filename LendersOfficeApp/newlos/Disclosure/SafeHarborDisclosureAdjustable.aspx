﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="SafeHarborDisclosureAdjustable.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.SafeHarborDisclosureAdjustable" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SafeHarborDisclosureAdjustable</title>
</head>
<body bgcolor="gainsboro">
	<script type="text/javascript">
	    function _init() {
	        document.getElementById('m_copyToColumn').value = '-1';
	    
	        var enableRiskyFeaturesColumn = !document.getElementById('sIncludeSafeHarborARMWithRiskyFeaturesYes').checked;
	        document.getElementById('sLpTemplateNmSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sFinalLAmtSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sDueSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sNoteIRSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sFullyIndexedRSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sRLifeCapRSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sEquityCalcSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sTransNetCashSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;
	        document.getElementById('sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures').readOnly = enableRiskyFeaturesColumn;

	        var enableRecLoanColumn = !document.getElementById('sIncludeSafeHarborARMRecLoanYes').checked;
	        document.getElementById('sLpTemplateNmSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sFinalLAmtSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sDueSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sNoteIRSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sGfeProThisMPmtAndMInsSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sFullyIndexedRSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sRLifeCapRSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sEquityCalcSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sTransNetCashSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sOriginatorCompensationTotalAmountSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	        document.getElementById('sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan').readOnly = enableRecLoanColumn;
	    }

	    function copyLoanToColumn(num) {
	        updateDirtyBit();
	        document.getElementById('m_copyToColumn').value = num;
	        refreshCalculation();
	    }
    </script>
  <asp:PlaceHolder ID="scriptHolderForPopup" runat="server" Visible="false">
        <script type="text/javascript">

	        $(function(){
	            $("#bSave").on("click", saveButtonClick)
	        });

	        var SHDAPageObject = {};
	        SHDAPageObject.closedByCloseButton = false;
    	    
	        window.onbeforeunload = page_beforeunload;
	        resizeTo(800,700);
    	   
	        function updateDirtyBit_callback() {
                var saveBtn = <%=AspxTools.JsGetElementById(bSave)%>;
                saveBtn.disabled = false;
	        }
	        function clearDirty_callback() {
	            var saveBtn = <%=AspxTools.JsGetElementById(bSave)%>;
	            saveBtn.disabled = true;
	        }
	        function closeClick() {  // careful not to name this fxn close.
                SHDAPageObject.closedByCloseButton = true;
                PolyShouldShowConfirmSave(isDirty(), onClosePopup, function(){
                    SHDAPageObject.closedByCloseButton = true;
                    saveMe();
                },function(){
                    SHDAPageObject.closedByCloseButton = true;
                    clearDirty();
                });	            
	            return false;
            }
            
	        function page_beforeunload() {
	            if (isDirty() && !SHDAPageObject.closedByCloseButton) {
	                return UnloadMessage;
	            }
	        }
	        function saveButtonClick() {
	            saveMe(false);
	        }
        </script>
    </asp:PlaceHolder>
    <form id="form1" runat="server">
        <input type="hidden" name="m_copyToColumn" id="m_copyToColumn" value="-1" />
        <table cellSpacing=0 cellPadding=0 width="100%" border=0>
            <tr>
                <td noWrap class="MainRightHeader">
                    Anti-Steering Safe Harbor Disclosure for Brokered Loans - Adjustable Rate Loans
                </td>
            </tr>
            <tr>
                <td>
                    <table class="InsetBorder FieldLabel" cellspacing="0">
                        <tr>
                            <td class="FormTableSubheader" colspan="5">
                                Loan Comparison
                            </td>
                        </tr>
                        <tr class="GridHeader">
                            <td></td>
                            <td>Loan with the lowest rate<br/> with risky features</td>
                            <td>Loan with the lowest rate<br/> without risky features</td>
                            <td>Loan with the lowest<br/> total discount points, <br />origination points, <br />or origination fees</td>
                            <td>Our recommended loan</td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Include</td>
                            <td align="center">
                                <asp:RadioButton ID="sIncludeSafeHarborARMWithRiskyFeaturesYes" GroupName="sIncludeSafeHarborARMWithRiskyFeatures" Text="Yes" runat="server" onclick="refreshCalculation();" value="1" />
                                <asp:RadioButton ID="sIncludeSafeHarborARMWithRiskyFeaturesNo" GroupName="sIncludeSafeHarborARMWithRiskyFeatures" Text="No" runat="server" onclick="refreshCalculation();" value="0" />
                            </td>
                            <td align="center">Yes</td>
                            <td align="center">Yes</td>
                            <td align="center">
                                <asp:RadioButton ID="sIncludeSafeHarborARMRecLoanYes" GroupName="sIncludeSafeHarborARMRecLoan" Text="Yes" runat="server" onclick="refreshCalculation();" value="1" />
                                <asp:RadioButton ID="sIncludeSafeHarborARMRecLoanNo" GroupName="sIncludeSafeHarborARMRecLoan" Text="No" runat="server" onclick="refreshCalculation();" value="0" />
                            </td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Description</td>
                            <td><asp:TextBox ID="sLpTemplateNmSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sLpTemplateNmSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sLpTemplateNmSafeHarborARMLowestCost" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sLpTemplateNmSafeHarborARMRecLoan" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Loan Amount</td>
                            <td><ml:moneytextbox id="sFinalLAmtSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sFinalLAmtSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sFinalLAmtSafeHarborARMLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sFinalLAmtSafeHarborARMRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Due</td>
                            <td><asp:TextBox ID="sDueSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sDueSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sDueSafeHarborARMLowestCost" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                            <td><asp:TextBox ID="sDueSafeHarborARMRecLoan" Width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Initial interest rate</td>
                            <td><ml:PercentTextBox ID="sNoteIRSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sNoteIRSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sNoteIRSafeHarborARMLowestCost" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sNoteIRSafeHarborARMRecLoan" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Initial monthly amount owed</td>
                            <td style="padding-right:8px"><ml:moneytextbox id="sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td style="padding-right:8px"><ml:moneytextbox id="sGfeProThisMPmtAndMInsSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td style="padding-right:8px"><ml:moneytextbox id="sGfeProThisMPmtAndMInsSafeHarborARMLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sGfeProThisMPmtAndMInsSafeHarborARMRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Can interest rate rise?</td>
                            <td align="center">Yes</td>
                            <td align="center">Yes</td>
                            <td align="center">Yes</td>
                            <td align="center">Yes</td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Current fully-indexed rate</td>
                            <td><ml:PercentTextBox ID="sFullyIndexedRSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sFullyIndexedRSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sFullyIndexedRSafeHarborARMLowestCost" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sFullyIndexedRSafeHarborARMRecLoan" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Maximum interest rate</td>
                            <td><ml:PercentTextBox ID="sRLifeCapRSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sRLifeCapRSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sRLifeCapRSafeHarborARMLowestCost" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                            <td><ml:PercentTextBox ID="sRLifeCapRSafeHarborARMRecLoan" Width="140px" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td>
                        </tr>

                        <tr class="GridAutoItem">
                            <td>Total discount points, loan origination fees or points</td>
                            <td>
                                <ml:MoneyTextBox ID="sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                            <td>
                                <ml:MoneyTextBox ID="sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                            <td>
                                <ml:MoneyTextBox ID="sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                            <td>
                                <ml:MoneyTextBox ID="sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
                        </tr>

                        <tr class="GridAutoItem">
                            <td>Closing costs</td>
                            <td><ml:moneytextbox id="sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Down payment needed</td>
                            <td><ml:moneytextbox id="sEquityCalcSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sEquityCalcSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sEquityCalcSafeHarborARMLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sEquityCalcSafeHarborARMRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Total funds needed to close</td>
                            <td><ml:moneytextbox id="sTransNetCashSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sTransNetCashSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sTransNetCashSafeHarborARMLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sTransNetCashSafeHarborARMRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td>Our compensation</td>
                            <td><ml:moneytextbox id="sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sOriginatorCompensationTotalAmountSafeHarborARMWithoutRiskyFeatures" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sOriginatorCompensationTotalAmountSafeHarborARMLowestCost" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                            <td><ml:moneytextbox id="sOriginatorCompensationTotalAmountSafeHarborARMRecLoan" Width="140px" runat="server" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                        </tr>
                        <tr class="GridAutoItem">
                            <td></td>
                            <td><input type="button" value="Copy from loan info" onclick="copyLoanToColumn(0);" /></td>
                            <td><input type="button" value="Copy from loan info" onclick="copyLoanToColumn(1);" /></td>
                            <td><input type="button" value="Copy from loan info" onclick="copyLoanToColumn(2);" /></td>
                            <td><input type="button" value="Copy from loan info" onclick="copyLoanToColumn(3);" /></td>
                        </tr>
                    </table>
                    <table class="InsetBorder FieldLabel">
                        <tr>
                            <td class="FormTableSubheader" colspan="2">
                                Risky features
                            </td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox ID="sSafeHarborARMNegAmort" runat="server" Text="Negative amortization" onclick="refreshCalculation();" /></td>
                            <td><asp:CheckBox ID="sSafeHarborARMDemandFeat" runat="server" Text="Demand feature" onclick="refreshCalculation();" /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox ID="sSafeHarborARMPrepayPenalty" runat="server" Text="Prepayment penalty" onclick="refreshCalculation();" /></td>
                            <td><asp:CheckBox ID="sSafeHarborARMSharedEquity" runat="server" Text="Shared equity" onclick="refreshCalculation();" /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox ID="sSafeHarborARMIntOnlyPmts" runat="server" Text="Interest-only payments" onclick="refreshCalculation();" /></td>
                            <td><asp:CheckBox ID="sSafeHarborARMSharedAppreciation" runat="server" Text="Shared appreciation" onclick="refreshCalculation();" /></td>
                        </tr>
                        <tr>
                            <td><asp:CheckBox ID="sSafeHarborARMBalloonPmntIn7Yrs" runat="server" Text="Balloon payment within 7 years" onclick="refreshCalculation();" /></td>
                        </tr>
                    </table>
                    <table class="InsetBorder FieldLabel">
                        <tr>
                            <td class="FormTableSubheader">
                                Our recommendation
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="sSafeHarborARMOurRecLowestRate" runat="server" Text="This is the loan with the lowest rate" onclick="refreshCalculation();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="sSafeHarborARMOurRecWithoutRiskyFeatures" runat="server" Text="This is the loan with the lowest rate without risky features"  onclick="refreshCalculation();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="sSafeHarborARMOurRecLowestCosts" runat="server" Text="This is the loan with the lowest closing costs" onclick="refreshCalculation();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="sSafeHarborARMOurRecOther" runat="server" Text="Other" onclick="refreshCalculation();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="sSafeHarborARMOurRecText" runat="server" TextMode="MultiLine" Rows="4" Width="400px" onchange="refreshCalculation();"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table class="InsetBorder FieldLabel">
                        <tr>
                            <td class="FormTableSubheader" colspan="2">
                                Prepared by
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
								<uc:CFM ID="CFM" runat="server" />                                
                            </td>
                        </tr>
						<tr>
							<td>Broker Name</td>
							<td><asp:TextBox ID="sSafeHarborARMAgentName" runat="server" width="280px" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker Identifier</td>
							<td><asp:TextBox ID="sSafeHarborARMLoanOriginatorIdentifier" width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker's License Number</td>
							<td><asp:TextBox ID="sSafeHarborARMAgentLicense" runat="server" width="140px" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker's Phone</td>
							<td><ml:phonetextbox id="sSafeHarborARMPhone" runat="server" width="140px" preset="phone"></ml:phonetextbox></td>
						</tr>
						<tr>
							<td>Broker Company's Name</td>
							<td><asp:TextBox ID="sSafeHarborARMCompanyName" runat="server" width="280px" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker Company Identifier</td>
							<td><asp:TextBox ID="sSafeHarborARMCompanyLoanOriginatorIdentifier" width="140px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker Company's License Number</td>
							<td><asp:TextBox ID="sSafeHarborARMCompanyLicense" runat="server" width="140px" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Broker Company's Address</td>
							<td><asp:TextBox ID="sSafeHarborARMCompanyStreetAddr" width="280px" runat="server" onchange="refreshCalculation();"></asp:TextBox></td>
						</tr>
						<tr>
						    <td></td>
						    <td><asp:TextBox ID="sSafeHarborARMCompanyCity" width="180px" runat="server" onchange="refreshCalculation();"></asp:TextBox><ml:statedropdownlist id="sSafeHarborARMCompanyState" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="sSafeHarborARMCompanyZip" runat="server" width="49" preset="zipcode"></ml:zipcodetextbox></td>
						</tr>
						<tr>
							<td>Broker Company's Phone</td>
							<td><ml:phonetextbox id="sSafeHarborARMCompanyPhone" runat="server" preset="phone"></ml:phonetextbox> Fax <ml:phonetextbox id="sSafeHarborARMCompanyFax" runat="server" preset="phone"></ml:phonetextbox></td>
						</tr>
                    </table>
                </td>
            </tr>
        </table>
        <div>
            <asp:Button OnClientClick="saveButtonClick(); return false;" runat="server" ID="bSave" Enabled="false" Text="Save" />
            <asp:Button OnClientClick="return closeClick();" runat="server" ID="bClose" Text="Close"/>
        </div>
    </form>
</body>
</html>
