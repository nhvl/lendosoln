﻿#region Generated code
namespace LendersOfficeApp.newlos.Disclosure
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using EDocs.Contents;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.ObjLib.TPO;
    using MeridianLink.CommonControls;

    /// <summary>
    /// This page is intended to hold all information and functionality related to the disclosure process.
    /// </summary>
    public partial class DisclosureCenter : BaseLoanPage
    {
        /// <summary>
        /// The loan data object.
        /// </summary>
        private CPageData dataLoan = null;

        /// <summary>
        /// Gets or sets the loan data object, initializing it if necessary.
        /// </summary>
        private CPageData DataLoan
        {
            get
            {
                if (this.dataLoan == null)
                {
                    this.dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(DisclosureCenter));
                    this.dataLoan.InitLoad();
                }

                return this.dataLoan;
            }

            set
            {
                this.dataLoan = value;
            }
        }

        /// <summary>
        /// Loads the data from the service to the loan for the user controls.
        /// </summary>
        /// <param name="loan">The loan to load.</param>
        /// <param name="service">The service item.</param>
        public static void LoadDataForControls(CPageData loan, AbstractBackgroundServiceItem service)
        {
            RespaDates.LoadData(loan, service, nameof(RespaDates));
        }

        /// <summary>
        /// Binds the data from the service to the loan for the user controls.
        /// </summary>
        /// <param name="loan">The loan to bind.</param>
        /// <param name="service">The service item.</param>
        public static void BindDataForControls(CPageData loan, AbstractBackgroundServiceItem service)
        {
            RespaDates.BindData(loan, service, nameof(RespaDates));
        }

        /// <summary>
        /// Gets any extra workflow operations that need to be evaluated.
        /// </summary>
        /// <returns>The extra workflow operations.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new[] { WorkflowOperations.ClearRespaFirstEnteredDates };
        }

        /// <summary>
        /// Loads the page data when the page is opened.
        /// </summary>
        protected override void LoadData()
        {
            this.DataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(DisclosureCenter));
            this.DataLoan.InitLoad();

            this.SetupDataForBorrowerLevelTracking();
            this.SetupTpoDisclosureRequestsSection();

            this.sInitAPR.Text = dataLoan.sInitAPR_rep;
            this.sLastDiscAPR.Text = dataLoan.sLastDiscAPR_rep;

            this.sInitAPR.ReadOnly = dataLoan.sCalculateInitAprAndLastDiscApr;
            this.sLastDiscAPR.ReadOnly = dataLoan.sCalculateInitAprAndLastDiscApr;

            this.CalculateInitAprAndLastDiscAPR.Visible = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID
                && !dataLoan.sCalculateInitAprAndLastDiscApr;

            LoanEstimateDatesInfo leDates = this.DataLoan.sLoanEstimateDatesInfo;
            ClosingDisclosureDatesInfo cdDates = this.DataLoan.sClosingDisclosureDatesInfo;

            this.sCalculateInitialLoanEstimate.Checked = this.DataLoan.sCalculateInitialLoanEstimate;
            this.LoanEstimatesTableJson.Value = ObsoleteSerializationHelper.JsonSerialize(dataLoan.sLoanEstimateDatesInfo);
            this.LoanEstimatesDates.DataSource = leDates.LoanEstimateDatesList;
            this.LoanEstimatesDates.DataBind();

            this.sCalculateInitialClosingDisclosure.Checked = this.DataLoan.sCalculateInitialClosingDisclosure;
            this.ClosingDisclosuresTableJSON.Value = ObsoleteSerializationHelper.JsonSerialize(dataLoan.sClosingDisclosureDatesInfo);
            this.ClosingDisclosuresDates.DataSource = cdDates.ClosingDisclosureDatesList;
            this.ClosingDisclosuresDates.DataBind();

            Tools.Bind_DeliveryMethodT(this.leDeliveryID);
            Tools.Bind_DeliveryMethodT(this.cdDeliveryID);

            this.BindDisclosedLoanEstimateArchiveIds(this.leArchiveID, this.DataLoan);
            Tools.SetDropDownListValue(this.leArchiveID, Guid.Empty.ToString());

            this.BindDisclosedClosingDisclosureArchiveIds(this.cdArchiveID, this.DataLoan);
            Tools.SetDropDownListValue(this.cdArchiveID, Guid.Empty.ToString());

            var archiveIsDisclosedById = this.DataLoan.sClosingCostArchive.ToDictionary(
                a => a.Id,
                a => a.IsDisclosed);

            this.RegisterJsObjectWithJsonNetSerializer("ArchiveIsDisclosedById", archiveIsDisclosedById);

            sAppSubmittedD.Text = this.DataLoan.sAppSubmittedD_rep;
            sEstCloseD.Text = this.DataLoan.sEstCloseD_rep;
            sInitialLoanEstimateIssuedD.Text = this.DataLoan.sInitialLoanEstimateIssuedD_rep;
            sInitialLoanEstimateReceivedD.Text = this.DataLoan.sInitialLoanEstimateReceivedD_rep;
            sLastDisclosedLoanEstimateIssuedD.Text = this.DataLoan.sLastDisclosedLoanEstimateIssuedD_rep;
            sLastDisclosedLoanEstimateReceivedD.Text = this.DataLoan.sLastDisclosedLoanEstimateReceivedD_rep;
            sInitialClosingDisclosureIssuedD.Text = this.DataLoan.sInitialClosingDisclosureIssuedD_rep;
            sInitialClosingDisclosureReceivedD.Text = this.DataLoan.sInitialClosingDisclosureReceivedD_rep;
            sPreviewClosingDisclosureIssuedD.Text = this.DataLoan.sPreviewClosingDisclosureIssuedD_rep;
            sPreviewClosingDisclosureReceivedD.Text = this.DataLoan.sPreviewClosingDisclosureReceivedD_rep;
            sLastClosingDisclosureBeforeConsummationIssuedD.Text = this.DataLoan.sLastClosingDisclosureBeforeConsummationIssuedD_rep;
            sLastClosingDisclosureBeforeConsummationReceivedD.Text = this.DataLoan.sLastClosingDisclosureBeforeConsummationReceivedD_rep;
            sFinalClosingDisclosureIssuedD.Text = this.DataLoan.sFinalClosingDisclosureIssuedD_rep;
            sFinalClosingDisclosureReceivedD.Text = this.DataLoan.sFinalClosingDisclosureReceivedD_rep;
            sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentClosingD.Text = this.DataLoan.sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentClosingD_rep;
            this.sInitialLoanEstimateMailedOrReceivedDeadlineForCurrentAppD.Text = this.DataLoan.sInitialLoanEstimateMailedOrReceivedDeadlineForCurrentAppD_rep;
            sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD.Text = this.DataLoan.sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD_rep;
            sInitialClosingDisclosureMailedDeadlineD.Text = this.DataLoan.sInitialClosingDisclosureMailedDeadlineD_rep;
            sInitialClosingDisclosureReceivedDeadlineD.Text = this.DataLoan.sInitialClosingDisclosureReceivedDeadlineD_rep;

            Tools.Bind_DeliveryMethodT(this.BorrowerLevelDatesRowForCloningDeliveryMethod);

            this.RespaDates.LoadData(
                this.DataLoan,
                this.UserHasWorkflowPrivilege(WorkflowOperations.ClearRespaFirstEnteredDates));
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The object initializing the page.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            this.PageTitle = "Disclosure Center";
            this.PageID = "DisclosureCenter";

            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");

            this.RegisterJsScript("LQBPopup.js");
        }

        /// <summary>
        /// Binds a <see cref="LoanEstimateDates"/> object to the LE metadata area.
        /// </summary>
        /// <param name="sender">A <see cref="LoanEstimateDates"/> object.</param>
        /// <param name="args">Repeater item event arguments.</param>
        protected void LoanEstD_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            LoanEstimateDates dates = args.Item.DataItem as LoanEstimateDates;
            LosConvert converter = new LosConvert();

            // When borrower-level tracking is disabled, we will
            // auto-lock and disable the checkboxes in the UI. The 
            // datalayer will handle this situation automatically, 
            // so the auto-lock in the UI is to ensure the lock boxes 
            // appear as expected.
            var enableBorrowerLevelDisclosureTracking = this.Broker.EnableBorrowerLevelDisclosureTracking;

            var disclosureDatesByConsumerId = dates.DisclosureDatesByConsumerId;
            var hasBorrowerLevelDisclosureDates = disclosureDatesByConsumerId.Count != 0;

            var row = args.Item.FindControl("leDateRowID") as HtmlTableRow;

            DateTextBox createdD = args.Item.FindControl("leCreatedDID") as DateTextBox;
            createdD.ReadOnly = !dates.IsManual;
            createdD.Text = converter.ToDateTimeString(dates.CreatedDate);
            if (!dates.IsManual)
            {
                createdD.Attributes["class"] = createdD.Attributes["class"] + " systemGenerated";
            }

            DateTextBox issuedD = args.Item.FindControl("leIssuedDID") as DateTextBox;
            issuedD.Text = converter.ToDateTimeString(dates.IssuedDate);

            HtmlInputCheckBox issuedDLckd = args.Item.FindControl("leIssuedDLckdId") as HtmlInputCheckBox;
            issuedDLckd.Checked = dates.IssuedDateLckd || !enableBorrowerLevelDisclosureTracking;

            if (!enableBorrowerLevelDisclosureTracking)
            {
                issuedDLckd.Attributes["class"] += " Hidden perma-hide";
            }
            else if (!hasBorrowerLevelDisclosureDates)
            {
                issuedDLckd.Disabled = true;
                issuedDLckd.Attributes["class"] += " perma-disable";
            }

            DropDownList delivery = args.Item.FindControl("leDeliveryID") as DropDownList;
            Tools.Bind_DeliveryMethodT(delivery);
            Tools.SetDropDownListValue(delivery, dates.DeliveryMethod);

            HtmlInputCheckBox deliveryLckd = args.Item.FindControl("leDeliveryLckdId") as HtmlInputCheckBox;
            deliveryLckd.Checked = dates.DeliveryMethodLckd || !enableBorrowerLevelDisclosureTracking;

            if (!enableBorrowerLevelDisclosureTracking)
            {
                deliveryLckd.Attributes["class"] += " Hidden perma-hide";
            }
            else if (!hasBorrowerLevelDisclosureDates)
            {
                deliveryLckd.Disabled = true;
                deliveryLckd.Attributes["class"] += " perma-disable";
            }

            DateTextBox receivedD = args.Item.FindControl("leReceivedDID") as DateTextBox;
            receivedD.Text = converter.ToDateTimeString(dates.ReceivedDate);

            HtmlInputCheckBox receivedDLckd = args.Item.FindControl("leReceivedDLckdId") as HtmlInputCheckBox;
            receivedDLckd.Checked = dates.ReceivedDateLckd || !enableBorrowerLevelDisclosureTracking;

            if (!enableBorrowerLevelDisclosureTracking)
            {
                receivedDLckd.Attributes["class"] += " Hidden perma-hide";
            }
            else if (!hasBorrowerLevelDisclosureDates)
            {
                receivedDLckd.Disabled = true;
                receivedDLckd.Attributes["class"] += " perma-disable";
            }

            DateTextBox signedD = args.Item.FindControl("leSignedDID") as DateTextBox;
            signedD.Text = converter.ToDateTimeString(dates.SignedDate);

            HtmlInputCheckBox signedDLckd = args.Item.FindControl("leSignedDLckdId") as HtmlInputCheckBox;
            signedDLckd.Checked = dates.SignedDateLckd || !enableBorrowerLevelDisclosureTracking;

            if (!enableBorrowerLevelDisclosureTracking)
            {
                signedDLckd.Attributes["class"] += " Hidden perma-hide";
            }
            else if (!hasBorrowerLevelDisclosureDates)
            {
                signedDLckd.Disabled = true;
                signedDLckd.Attributes["class"] += " perma-disable";
            }

            HiddenField leDisableManualArchiveAssociation = args.Item.FindControl("leDisableManualArchiveAssociation") as HiddenField;
            leDisableManualArchiveAssociation.Value = dates.DisableManualArchiveAssociation.ToString().ToLower();

            DropDownList archiveDropdown = args.Item.FindControl("leArchiveID") as DropDownList;
            bool associatedArchiveIsInvalid = false;

            if (dates.ArchiveId == Guid.Empty)
            {
                this.BindDisclosedLoanEstimateArchiveIds(archiveDropdown, this.DataLoan);
            }
            else
            {
                var associatedArchive = this.DataLoan.sClosingCostArchive.SingleOrDefault(a => a.Id == dates.ArchiveId);

                // It is possible for system generated archives to be in non-disclosed statuses,
                // so we put the check at this level.
                // We check specifically for the Disclosed status instead of using
                // the IsDisclosed property because the LE dates should NOT include
                // LEs that were disclosed as part of a Closing Disclosure.
                if (associatedArchive.Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed)
                {
                    row.Attributes["class"] = row.Attributes["class"] + " invalidArchive validate";
                    associatedArchiveIsInvalid = true;
                }

                if (dates.DisableManualArchiveAssociation)
                {
                    archiveDropdown.Items.Add(new ListItem(associatedArchive.DateArchived, dates.ArchiveId.ToString()) { Selected = true });
                    archiveDropdown.Enabled = false;
                }
                else if (associatedArchive.Status == ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed)
                {
                    this.BindDisclosedLoanEstimateArchiveIds(archiveDropdown, this.DataLoan);
                    Tools.SetDropDownListValue(archiveDropdown, dates.ArchiveId.ToString());
                }
                else
                {
                    this.BindDisclosedLoanEstimateArchiveIds(archiveDropdown, this.DataLoan);
                    archiveDropdown.Items.Add(new ListItem(associatedArchive.DateArchived, dates.ArchiveId.ToString()) { Selected = true });
                }
            }

            var rowInvalidImg = args.Item.FindControl("leDateRowAssociatedWithInvalidArchive") as HtmlImage;
            if (!associatedArchiveIsInvalid)
            {
                rowInvalidImg.Attributes["style"] = rowInvalidImg.Attributes["style"] + " display: none;";
            }

            CheckBox isInitial = args.Item.FindControl("leInitialID") as CheckBox;
            isInitial.Checked = dates.IsInitial;

            var initialWarning = args.Item.FindControl("leInitialWarningId") as HtmlImage;
            bool showInitialWarning = dates.IsInitial && associatedArchiveIsInvalid;
            if (!showInitialWarning)
            {
                initialWarning.Attributes["style"] = initialWarning.Attributes["style"] + " display: none;";
            }

            TextBox product = args.Item.FindControl("leLastDisclosedTRIDLoanProductDescriptionID") as TextBox;
            product.Text = dates.LastDisclosedTRIDLoanProductDescription;

            HtmlInputCheckBox disclosedAprLckd = args.Item.FindControl("leDisclosedAprLckd") as HtmlInputCheckBox;
            disclosedAprLckd.Checked = dates.DisclosedAprLckd;

            PercentTextBox disclosedApr = args.Item.FindControl("leDisclosedApr") as PercentTextBox;
            disclosedApr.Text = dates.DisclosedApr_rep;

            HtmlImage lqbAprWarning = args.Item.FindControl("leLqbAprWarning") as HtmlImage;
            if (!dates.DisclosedAprLckd 
                && !dates.DocVendorApr.HasValue
                && dates.DisclosedApr.HasValue)
            {
                // This will remove the display: none rule.
                lqbAprWarning.Attributes["style"] = string.Empty;
            }

            PercentTextBox lqbApr = args.Item.FindControl("leLqbApr") as PercentTextBox;
            lqbApr.Text = dates.LqbApr_rep;

            PercentTextBox docVendorApr = args.Item.FindControl("leDocVendorApr") as PercentTextBox;
            docVendorApr.Text = dates.DocVendorApr_rep;

            HiddenField docVendorAprFull = args.Item.FindControl("leDocVendorAprFull") as HiddenField;
            docVendorAprFull.Value = dates.DocVendorApr.ToString();

            CheckBox isManual = args.Item.FindControl("leManualID") as CheckBox;
            isManual.Checked = dates.IsManual;

            HiddenField uniqueId = args.Item.FindControl("leUniqueId") as HiddenField;
            uniqueId.Value = dates.UniqueId.ToString();

            HiddenField transactionId = args.Item.FindControl("leTransactionId") as HiddenField;
            transactionId.Value = dates.TransactionId;

            var removeButton = args.Item.FindControl("removeLE") as HtmlInputButton;
            removeButton.Visible = !dates.DisableManualArchiveAssociation;

            var borrowerLevelDates = args.Item.FindControl("LoanEstimateBorrowerLevelDates") as Repeater;
            var datasource = disclosureDatesByConsumerId.Select(datePair =>
            {
                var metadata = new BorrowerLevelDisclosureDataBindingMetadata(dates, associatedArchiveIsInvalid, enableBorrowerLevelDisclosureTracking);
                return Tuple.Create(metadata, datePair);
            });

            borrowerLevelDates.DataSource = datasource;
            borrowerLevelDates.DataBind();

            if (!enableBorrowerLevelDisclosureTracking || !dates.IsManual || hasBorrowerLevelDisclosureDates)
            {
                var addBorrowerLevelDatesRow = args.Item.FindControl("AddBorrowerLevelDatesRow") as HtmlTableRow;
                addBorrowerLevelDatesRow.Attributes["class"] += " Hidden";
            }

            if (associatedArchiveIsInvalid)
            {
                createdD.ReadOnly = true;

                issuedD.ReadOnly = true;
                issuedDLckd.Disabled = true;

                delivery.Enabled = false;
                deliveryLckd.Disabled = true;

                receivedD.ReadOnly = true;
                receivedDLckd.Disabled = true;

                signedD.ReadOnly = true;
                signedDLckd.Disabled = true;

                if (!dates.IsInitial)
                {
                    isInitial.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Handles data binding for borrower-level disclosure dates.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="args">
        /// The arguments for the data binding event.
        /// </param>
        protected void BorrowerLevelDates_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var dateSource = (Tuple<BorrowerLevelDisclosureDataBindingMetadata, KeyValuePair<Guid, BorrowerDisclosureDates>>)args.Item.DataItem;

            var bindingMetadata = dateSource.Item1;
            var disclosureDatesPair = dateSource.Item2;

            var consumerId = disclosureDatesPair.Key;
            var borrowerLevelDisclosureDates = disclosureDatesPair.Value;

            var borrowerLevelDatesRow = args.Item.FindControl("BorrowerLevelDatesRow") as HtmlTableRow;
            borrowerLevelDatesRow.Attributes["data-unique-id"] = bindingMetadata.UniqueId.ToString();

            if (bindingMetadata.AssociatedArchiveIsInvalid)
            {
                borrowerLevelDatesRow.Attributes["class"] += " invalidArchive";
            }

            if (!bindingMetadata.EnableBorrowerLevelDisclosureTracking)
            {
                borrowerLevelDatesRow.Attributes["class"] += " Hidden perma-hide";
            }

            var consumerIdField = args.Item.FindControl("ConsumerId") as HtmlInputHidden;
            consumerIdField.Value = consumerId.ToString();

            var borrowerName = args.Item.FindControl("BorrowerName") as HtmlGenericControl;
            borrowerName.InnerText = borrowerLevelDisclosureDates.ConsumerName;

            var issuedDate = args.Item.FindControl("IssuedDate") as DateTextBox;
            issuedDate.Text = borrowerLevelDisclosureDates.IssuedDate?.ToShortDateString();

            var deliveryMethod = args.Item.FindControl("DeliveryMethod") as DropDownList;
            Tools.Bind_DeliveryMethodT(deliveryMethod);
            Tools.SetDropDownListValue(deliveryMethod, borrowerLevelDisclosureDates.DeliveryMethod);

            var receivedDate = args.Item.FindControl("ReceivedDate") as DateTextBox;
            receivedDate.Text = borrowerLevelDisclosureDates.ReceivedDate?.ToShortDateString();

            var signedDate = args.Item.FindControl("SignedDate") as DateTextBox;
            signedDate.Text = borrowerLevelDisclosureDates.SignedDate?.ToShortDateString();

            var excludeFromCalculationsCell = args.Item.FindControl("ExcludeFromCalculationsCell") as HtmlTableCell;
            var excludeFromCalculationsCheckbox = args.Item.FindControl("ExcludeFromCalculations") as HtmlInputCheckBox;

            if (bindingMetadata.IsManual)
            {
                excludeFromCalculationsCell.Attributes["colspan"] = "15";
                excludeFromCalculationsCheckbox.Checked = borrowerLevelDisclosureDates.ExcludeFromCalculations;
            }
            else
            {
                excludeFromCalculationsCell.Attributes["class"] += " Hidden";

                var signedDateCell = args.Item.FindControl("SignedDateCell") as HtmlTableCell;
                signedDateCell.Attributes["colspan"] = "15";
            }

            if (bindingMetadata.AssociatedArchiveIsInvalid)
            {
                issuedDate.ReadOnly = true;
                deliveryMethod.Enabled = false;
                receivedDate.ReadOnly = true;
                signedDate.ReadOnly = true;
                excludeFromCalculationsCheckbox.Disabled = true;
            }
        }

        /// <summary>
        /// Binds a <see cref="ClosingDisclosureDates"/> object to the CD metadata area.
        /// </summary>
        /// <param name="sender">A <see cref="ClosingDisclosureDates"/> object.</param>
        /// <param name="args">Repeater item event arguments.</param>
        protected void ClosingDisc_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            ClosingDisclosureDates dates = args.Item.DataItem as ClosingDisclosureDates;
            LosConvert converter = new LosConvert();

            var disclosureDatesByConsumerId = dates.DisclosureDatesByConsumerId;
            var hasBorrowerLevelDisclosureDates = disclosureDatesByConsumerId.Count != 0;

            // When borrower-level tracking is disabled, we will
            // auto-lock and disable the checkboxes in the UI. The 
            // datalayer will handle this situation automatically, 
            // so the auto-lock in the UI is to ensure the lock boxes 
            // appear as expected.
            var enableBorrowerLevelDisclosureTracking = this.Broker.EnableBorrowerLevelDisclosureTracking;

            var row = args.Item.FindControl("cdDateRowID") as HtmlTableRow;

            DateTextBox createdD = args.Item.FindControl("cdCreatedDID") as DateTextBox;
            createdD.ReadOnly = !dates.IsManual;
            createdD.Text = converter.ToDateTimeString(dates.CreatedDate);
            if (!dates.IsManual)
            {
                createdD.Attributes["class"] = createdD.Attributes["class"] + " systemGenerated";
            }

            DateTextBox issuedD = args.Item.FindControl("cdIssuedDID") as DateTextBox;
            issuedD.Text = converter.ToDateTimeString(dates.IssuedDate);

            HtmlInputCheckBox issuedDLckd = args.Item.FindControl("cdIssuedDLckdId") as HtmlInputCheckBox;
            issuedDLckd.Checked = dates.IssuedDateLckd || !enableBorrowerLevelDisclosureTracking;

            if (!enableBorrowerLevelDisclosureTracking)
            {
                issuedDLckd.Attributes["class"] += " Hidden perma-hide";
            }
            else if (!hasBorrowerLevelDisclosureDates)
            {
                issuedDLckd.Disabled = true;
                issuedDLckd.Attributes["class"] += " perma-disable";
            }

            DropDownList delivery = args.Item.FindControl("cdDeliveryID") as DropDownList;
            Tools.Bind_DeliveryMethodT(delivery);
            Tools.SetDropDownListValue(delivery, dates.DeliveryMethod);

            HtmlInputCheckBox deliveryLckd = args.Item.FindControl("cdDeliveryLckdId") as HtmlInputCheckBox;
            deliveryLckd.Checked = dates.DeliveryMethodLckd || !enableBorrowerLevelDisclosureTracking;

            if (!enableBorrowerLevelDisclosureTracking)
            {
                deliveryLckd.Attributes["class"] += " Hidden perma-hide";
            }
            else if (!hasBorrowerLevelDisclosureDates)
            {
                deliveryLckd.Disabled = true;
                deliveryLckd.Attributes["class"] += " perma-disable";
            }

            DateTextBox receivedD = args.Item.FindControl("cdReceivedDID") as DateTextBox;
            receivedD.Text = converter.ToDateTimeString(dates.ReceivedDate);

            HtmlInputCheckBox receivedDLckd = args.Item.FindControl("cdReceivedDLckdId") as HtmlInputCheckBox;
            receivedDLckd.Checked = dates.ReceivedDateLckd || !enableBorrowerLevelDisclosureTracking;

            if (!enableBorrowerLevelDisclosureTracking)
            {
                receivedDLckd.Attributes["class"] += " Hidden perma-hide";
            }
            else if (!hasBorrowerLevelDisclosureDates)
            {
                receivedDLckd.Disabled = true;
                receivedDLckd.Attributes["class"] += " perma-disable";
            }

            DateTextBox signedD = args.Item.FindControl("cdSignedDID") as DateTextBox;
            signedD.Text = converter.ToDateTimeString(dates.SignedDate);

            HtmlInputCheckBox signedDLckd = args.Item.FindControl("cdSignedDLckdId") as HtmlInputCheckBox;
            signedDLckd.Checked = dates.SignedDateLckd || !enableBorrowerLevelDisclosureTracking;

            if (!enableBorrowerLevelDisclosureTracking)
            {
                signedDLckd.Attributes["class"] += " Hidden perma-hide";
            }
            else if (!hasBorrowerLevelDisclosureDates)
            {
                signedDLckd.Disabled = true;
                signedDLckd.Attributes["class"] += " perma-disable";
            }

            HiddenField cdDisableManualArchiveAssociation = args.Item.FindControl("cdDisableManualArchiveAssociation") as HiddenField;
            cdDisableManualArchiveAssociation.Value = dates.DisableManualArchiveAssociation.ToString().ToLower();

            DropDownList archiveDropdown = args.Item.FindControl("cdArchiveID") as DropDownList;
            bool associatedArchiveIsInvalid = false;

            if (dates.ArchiveId == Guid.Empty)
            {
                this.BindDisclosedClosingDisclosureArchiveIds(archiveDropdown, this.DataLoan);
            }
            else
            {
                var associatedArchive = this.DataLoan.sClosingCostArchive.SingleOrDefault(a => a.Id == dates.ArchiveId);

                // It is possible for system generated archives to be in non-disclosed statuses,
                // so we put the check at this level.
                if (!associatedArchive.IsDisclosed)
                {
                    row.Attributes["class"] = row.Attributes["class"] + " invalidArchive validate";
                    associatedArchiveIsInvalid = true;
                }

                if (dates.DisableManualArchiveAssociation)
                {
                    archiveDropdown.Items.Add(new ListItem(associatedArchive.DateArchived, dates.ArchiveId.ToString()) { Selected = true });
                    archiveDropdown.Enabled = false;
                }
                else if (associatedArchive.IsDisclosed)
                {
                    this.BindDisclosedClosingDisclosureArchiveIds(archiveDropdown, this.DataLoan);
                    Tools.SetDropDownListValue(archiveDropdown, dates.ArchiveId.ToString());
                }
                else
                {
                    this.BindDisclosedClosingDisclosureArchiveIds(archiveDropdown, this.DataLoan);
                    archiveDropdown.Items.Add(new ListItem(associatedArchive.DateArchived, dates.ArchiveId.ToString()) { Selected = true });
                }
            }

            var rowInvalidImg = args.Item.FindControl("cdDateRowAssociatedWithInvalidArchive") as HtmlImage;
            if (!associatedArchiveIsInvalid)
            {
                rowInvalidImg.Attributes["style"] = rowInvalidImg.Attributes["style"] + " display: none;";
            }

            CheckBox isInitial = args.Item.FindControl("cdInitialID") as CheckBox;
            isInitial.Checked = dates.IsInitial;
            var initialWarning = args.Item.FindControl("cdInitialWarningId") as HtmlImage;
            bool showInitialWarning = dates.IsInitial && dates.IsPostClosing;
            if (!showInitialWarning)
            {
                initialWarning.Attributes["style"] = initialWarning.Attributes["style"] + "display: none;";
            }

            CheckBox isPreview = args.Item.FindControl("cdPreviewID") as CheckBox;
            isPreview.Checked = dates.IsPreview;
            var previewWarning = args.Item.FindControl("cdPreviewWarningId") as HtmlImage;
            bool showPreviewWarning = dates.IsPreview && (associatedArchiveIsInvalid || dates.IsPostClosing);
            if (!showPreviewWarning)
            {
                previewWarning.Attributes["style"] = initialWarning.Attributes["style"] + "display: none;";
            }

            CheckBox isFinal = args.Item.FindControl("cdFinalID") as CheckBox;
            isFinal.Checked = dates.IsFinal;
            var finalWarning = args.Item.FindControl("cdFinalWarningId") as HtmlImage;
            bool showFinalWarning = dates.IsFinal && (associatedArchiveIsInvalid || dates.IsPostClosing);
            if (!showFinalWarning)
            {
                finalWarning.Attributes["style"] = initialWarning.Attributes["style"] + "display: none;";
            }

            TextBox product = args.Item.FindControl("cdLastDisclosedTRIDLoanProductDescriptionID") as TextBox;
            product.Text = dates.LastDisclosedTRIDLoanProductDescription;

            CheckBox ucdDocumentLinked = args.Item.FindControl("cdUcdDocumentLinkedId") as CheckBox;
            ucdDocumentLinked.Enabled = false;
            ucdDocumentLinked.Checked = dates.UcdDocument != Guid.Empty;

            HiddenField ucdDocumentId = args.Item.FindControl("cdUcdDocumentId") as HiddenField;
            ucdDocumentId.Value = dates.UcdDocument.ToString();

            HtmlInputCheckBox disclosedAprLckd = args.Item.FindControl("cdDisclosedAprLckd") as HtmlInputCheckBox;
            disclosedAprLckd.Checked = dates.DisclosedAprLckd;

            PercentTextBox disclosedApr = args.Item.FindControl("cdDisclosedApr") as PercentTextBox;
            disclosedApr.Text = dates.DisclosedApr_rep;

            HtmlImage lqbAprWarning = args.Item.FindControl("cdLqbAprWarning") as HtmlImage;
            if (!dates.DisclosedAprLckd 
                && !dates.DocVendorApr.HasValue
                && dates.DisclosedApr.HasValue)
            {
                // This will remove the display: none rule.
                lqbAprWarning.Attributes["style"] = string.Empty;
            }

            PercentTextBox lqbApr = args.Item.FindControl("cdLqbApr") as PercentTextBox;
            lqbApr.Text = dates.LqbApr_rep;

            PercentTextBox docVendorApr = args.Item.FindControl("cdDocVendorApr") as PercentTextBox;
            docVendorApr.Text = dates.DocVendorApr_rep;

            HiddenField docVendorAprFull = args.Item.FindControl("cdDocVendorAprFull") as HiddenField;
            docVendorAprFull.Value = dates.DocVendorApr.ToString();

            CheckBox isManual = args.Item.FindControl("cdManualID") as CheckBox;
            isManual.Checked = dates.IsManual;

            HiddenField uniqueId = args.Item.FindControl("cdUniqueId") as HiddenField;
            uniqueId.Value = dates.UniqueId.ToString();

            HiddenField transactionId = args.Item.FindControl("cdTransactionId") as HiddenField;
            transactionId.Value = dates.TransactionId;

            HiddenField docCode = args.Item.FindControl("cdDocCode") as HiddenField;
            docCode.Value = dates.DocCode;

            HiddenField vendorId = args.Item.FindControl("cdVendorId") as HiddenField;
            vendorId.Value = dates.VendorId.HasValue ? dates.VendorId.Value.ToString() : string.Empty;

            CheckBox isPostClosing = args.Item.FindControl("cdPostClosingID") as CheckBox;
            isPostClosing.Checked = dates.IsPostClosing;

            if (!dates.IsPostClosing)
            {
                var postClosingReasonDiv = args.Item.FindControl("postClosingReasonDiv") as HtmlGenericControl;
                postClosingReasonDiv.Attributes["class"] = postClosingReasonDiv.Attributes["class"] + " hidden";

                var postClosingDatesDiv = args.Item.FindControl("postClosingDatesDiv") as HtmlGenericControl;
                postClosingDatesDiv.Attributes["class"] = postClosingDatesDiv.Attributes["class"] + " hidden";
            }

            CheckBox isPostClosingNumAmtPdByBorr = args.Item.FindControl("cdPostClosingNumAmtPdByBorrID") as CheckBox;
            isPostClosingNumAmtPdByBorr.Checked = dates.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower;

            CheckBox isPostClosingNumAmtPdBySell = args.Item.FindControl("cdPostClosingNumAmtPdBySellID") as CheckBox;
            isPostClosingNumAmtPdBySell.Checked = dates.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller;

            CheckBox isPostClosingNonNumClericalErr = args.Item.FindControl("cdPostClosingNonNumClericalErrID") as CheckBox;
            isPostClosingNonNumClericalErr.Checked = dates.IsDisclosurePostClosingDueToNonNumericalClericalError;

            CheckBox isPostClosingCureForToleranceViolation = args.Item.FindControl("cdPostClosingCureForToleranceViolationID") as CheckBox;
            isPostClosingCureForToleranceViolation.Checked = dates.IsDisclosurePostClosingDueToCureForToleranceViolation;

            DateTextBox postConsummationRedisclosureReasonDate = args.Item.FindControl("cdPostConsummationRedisclosureReasonDateID") as DateTextBox;
            postConsummationRedisclosureReasonDate.Text = converter.ToDateTimeString(dates.PostConsummationRedisclosureReasonDate);

            DateTextBox postConsummationKnowledgeOfEventDate = args.Item.FindControl("cdPostConsummationKnowledgeOfEventDateID") as DateTextBox;
            postConsummationKnowledgeOfEventDate.Text = converter.ToDateTimeString(dates.PostConsummationKnowledgeOfEventDate);

            var removeButton = args.Item.FindControl("removeCD") as HtmlInputButton;
            removeButton.Visible = !dates.DisableManualArchiveAssociation;

            var borrowerLevelDates = args.Item.FindControl("ClosingDisclosureBorrowerLevelDates") as Repeater;
            var datasource = disclosureDatesByConsumerId.Select(datePair =>
            {
                var metadata = new BorrowerLevelDisclosureDataBindingMetadata(dates, associatedArchiveIsInvalid, enableBorrowerLevelDisclosureTracking);
                return Tuple.Create(metadata, datePair);
            });

            borrowerLevelDates.DataSource = datasource;
            borrowerLevelDates.DataBind();

            if (!enableBorrowerLevelDisclosureTracking || !dates.IsManual || hasBorrowerLevelDisclosureDates)
            {
                var addBorrowerLevelDatesRow = args.Item.FindControl("AddBorrowerLevelDatesRow") as HtmlTableRow;
                addBorrowerLevelDatesRow.Attributes["class"] += " Hidden";
            }

            if (associatedArchiveIsInvalid)
            {
                createdD.ReadOnly = true;

                issuedD.ReadOnly = true;
                issuedDLckd.Disabled = true;

                delivery.Enabled = false;
                deliveryLckd.Disabled = true;

                receivedD.ReadOnly = true;
                receivedDLckd.Disabled = true;

                signedD.ReadOnly = true;
                signedDLckd.Disabled = true;

                isInitial.Enabled = false;
                isPreview.Enabled = false;
                isFinal.Enabled = false;
                isPostClosingNumAmtPdByBorr.Enabled = false;
                isPostClosingNumAmtPdBySell.Enabled = false;
                isPostClosingNonNumClericalErr.Enabled = false;
                isPostClosingCureForToleranceViolation.Enabled = false;
            }
        }

        /// <summary>
        /// Binds a re-disclosure request to the "TPO Portal Disclosure Requests" area.
        /// </summary>
        /// <param name="sender">A re-disclosure request object.</param>
        /// <param name="args">Repeater item event arguments.</param>
        protected void RedisclosureRequests_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var redisclosureCollection = args.Item.DataItem as TpoRequestForRedisclosureGenerationEventCollection;

            var status = args.Item.FindControl("RedisclosureStatusT") as HtmlInputText;
            var statusInt = args.Item.FindControl("RedisclosureStatusTInt") as HtmlInputHidden;
            var changeStatusButton = args.Item.FindControl("ChangeTpoRedisclosureStatusButton") as HtmlInputButton;
            var statusDate = args.Item.FindControl("RedisclosureStatusD") as HtmlInputText;
            var statusNotes = args.Item.FindControl("RedisclosureStatusNotes") as HtmlTextArea;
            var attachedDocs = args.Item.FindControl("RedisclosureAttachedDocs") as HtmlAnchor;
            var auditLink = args.Item.FindControl("RedisclosureAuditLink") as HtmlAnchor;

            status.Value = redisclosureCollection.LatestEventStatusRep;
            statusInt.Value = redisclosureCollection.LatestEventStatus.ToString("D");
            statusDate.Value = redisclosureCollection.LatestEventDatetimeRep;
            
            changeStatusButton.Attributes["data-event-id"] = (redisclosureCollection.LatestEventId ?? Guid.Empty).ToString();
            changeStatusButton.Attributes["data-status-type"] = TpoDisclosureType.Redisclosure.ToString("D");

            statusNotes.Value = redisclosureCollection.LatestEventNotes;
            statusNotes.Attributes["readonly"] = "readonly";

            if (redisclosureCollection.LatestEventAssociatedDocId.HasValue)
            {
                attachedDocs.Attributes["data-doc-id"] = redisclosureCollection.LatestEventAssociatedDocId.Value.ToString();
            }
            else
            {
                attachedDocs.Attributes["class"] += " hidden";
            }

            Guid? latestAuditItem = null;
            if (redisclosureCollection.LatestEventId.HasValue)
            {
                latestAuditItem = this.GetLatestTpoDisclosureAuditId(TpoDisclosureType.Redisclosure, redisclosureCollection.LatestEventId.Value);
            }

            if (latestAuditItem.HasValue)
            {
                auditLink.Attributes["data-audit-id"] = latestAuditItem.Value.ToString();
            }
            else
            {
                auditLink.Attributes["class"] += " hidden";
            }
        }

        /// <summary>
        /// Retrieves the EDoc associated with the Closing Disclosure and provides it for the user to download.
        /// </summary>
        /// <param name="sender">The sending element.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void DownloadUcdFile(object sender, EventArgs e)
        {
            Guid documentId;
            if (!Guid.TryParse(this.UcdDocumentIdForDownload.Value, out documentId))
            {
                string message = $"EDoc with ID {documentId} could not be loaded for broker {BrokerID}";
                Tools.LogError(message);
                throw new CBaseException(message, message);
            }

            var repo = EDocumentRepository.GetSystemRepository(BrokerID);
            UcdGenericEDocument linkedDoc;
            
            try
            {
                linkedDoc = repo.GetGenericDocumentById(documentId) as UcdGenericEDocument;
            }
            catch (CBaseException exc)
            {
                Tools.LogError(exc);
                throw;
            }
            
            if (linkedDoc == null || !linkedDoc.IsValid)
            {
                string message = $"EDoc with ID {documentId} could not be loaded for broker {BrokerID}";
                Tools.LogError(message);
                throw new CBaseException(message, message);
            }

            string path;
            try
            {
                path = linkedDoc.GetContentPath();
            }
            catch (FileNotFoundException exc)
            {
                string message = $"EDoc with ID {documentId} could not be loaded for broker {BrokerID}";
                Tools.LogError(message);
                throw new CBaseException(message, exc);
            }

            RequestHelper.SendFileToClient(this.Context, path, "text/xml", "UniformClosingDataset.xml");
            Response.End();
        }

        /// <summary>
        /// Binds existing archives to a CD metadata row.
        /// </summary>
        /// <param name="dropdown">The archive dropdown for a given row.</param>
        /// <param name="dataLoan">A loan data object.</param>
        protected void BindDisclosedClosingDisclosureArchiveIds(DropDownList dropdown, CPageData dataLoan)
        {
            var disclosedClosingDisclosureArchives = dataLoan.GetValidArchivesForClosingDisclosureDates().Values;

            dropdown.Items.Add(new ListItem("<-- NONE -->", Guid.Empty.ToString()));

            foreach (var archive in disclosedClosingDisclosureArchives)
            {
                dropdown.Items.Add(new ListItem(archive.DateArchived, archive.Id.ToString()));
            }
        }

        /// <summary>
        /// Binds existing archives to an LE metadata row.
        /// </summary>
        /// <param name="dropdown">The archive dropdown for a given row.</param>
        /// <param name="dataLoan">A loan data object.</param>
        protected void BindDisclosedLoanEstimateArchiveIds(DropDownList dropdown, CPageData dataLoan)
        {
            // We do not use the IsDisclosed property here because LEs that 
            // were disclosed as part of a Closing Disclosure should NOT have
            // entries in the LE dates table.
            var disclosedLoanEstimateArchives = dataLoan.GetValidArchivesForLoanEstimateDates().Values;

            dropdown.Items.Add(new ListItem("<-- NONE -->", Guid.Empty.ToString()));

            foreach (var archive in disclosedLoanEstimateArchives)
            {
                dropdown.Items.Add(new ListItem(archive.DateArchived, archive.Id.ToString()));
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion

        /// <summary>
        /// Registers data on the page for borrower-level disclosure tracking.
        /// </summary>
        private void SetupDataForBorrowerLevelTracking()
        {
            var consumerNamesByConsumerId = this.DataLoan.GetConsumerDisclosureMetadataByConsumerId().ToDictionary(
                metadata => metadata.ConsumerId.Value, // Use GUID backing field
                metadata => metadata.ConsumerName);

            this.RegisterJsObjectWithJsonNetSerializer("ConsumerNamesByConsumerId", consumerNamesByConsumerId);
            this.RegisterJsGlobalVariables("EnableBorrowerLevelDisclosureTracking", this.Broker.EnableBorrowerLevelDisclosureTracking);
        }

        /// <summary>
        /// Sets data on the page for the "TPO Portal Disclosure Requests" section.
        /// </summary>
        private void SetupTpoDisclosureRequestsSection()
        {
            var enableTpoDisclosureSection = this.IsTpoDisclosureRequestSectionEnabled();
            this.RegisterJsGlobalVariables("DisplayTpoDisclosureRequestsSection", enableTpoDisclosureSection);

            if (!enableTpoDisclosureSection)
            {
                return;
            }

            var displayCompletedWarning = this.DataLoan.sTpoRequestForInitialDisclosureGenerationStatusT == TpoRequestForInitialDisclosureGenerationEventType.Completed &&
                !this.DataLoan.sHasInitialDisclosure;
            this.RegisterJsGlobalVariables("DisplayCompletedWarning", displayCompletedWarning);

            this.sTpoRequestForInitialDisclosureGenerationStatusT.Value = this.DataLoan.sTpoRequestForInitialDisclosureGenerationStatusT_rep;
            this.sTpoRequestForInitialDisclosureGenerationStatusTInt.Value = this.DataLoan.sTpoRequestForInitialDisclosureGenerationStatusT.ToString("D");
            this.sTpoRequestForInitialDisclosureGenerationStatusD.Value = this.DataLoan.sTpoRequestForInitialDisclosureGenerationStatusD_rep;

            this.sTpoRequestForInitialDisclosureGenerationStatusNotes.Value = this.DataLoan.sTpoRequestForInitialDisclosureGenerationStatusNotes;
            this.sTpoRequestForInitialDisclosureGenerationStatusNotes.Attributes["readonly"] = "readonly";

            var associatedTpoInitialDisclosureAuditId = this.GetLatestTpoDisclosureAuditId(TpoDisclosureType.InitialDisclosure);
            if (associatedTpoInitialDisclosureAuditId.HasValue)
            {
                this.sTpoRequestForInitialDisclosureGenerationAuditLink.Attributes["data-audit-id"] = associatedTpoInitialDisclosureAuditId.Value.ToString();
            }
            else
            {
                this.sTpoRequestForInitialDisclosureGenerationAuditLink.Attributes["class"] += " hidden";
            }

            this.RedisclosureRequests.DataSource = this.DataLoan.sTpoRequestForRedisclosureGenerationEventCollection;
            this.RedisclosureRequests.DataBind();

            this.sTpoRequestForInitialClosingDisclosureGenerationStatusT.Value = this.DataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusT_rep;
            this.sTpoRequestForInitialClosingDisclosureGenerationStatusTInt.Value = this.DataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusT.ToString("D");
            this.sTpoRequestForInitialClosingDisclosureGenerationStatusD.Value = this.DataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusD_rep;

            this.sTpoRequestForInitialClosingDisclosureGenerationStatusNotes.Value = this.DataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusNotes;
            this.sTpoRequestForInitialClosingDisclosureGenerationStatusNotes.Attributes["readonly"] = "readonly";

            var associatedClosingDisclosureAuditId = this.GetLatestTpoDisclosureAuditId(TpoDisclosureType.InitialClosingDisclosure);
            if (associatedClosingDisclosureAuditId.HasValue)
            {
                this.sTpoRequestForInitialClosingDisclosureGenerationAuditLink.Attributes["data-audit-id"] = associatedClosingDisclosureAuditId.Value.ToString();
            }
            else
            {
                this.sTpoRequestForInitialClosingDisclosureGenerationAuditLink.Attributes["class"] += " hidden";
            }

            var associatedClosingDisclosureDocument = this.DataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusAssociatedDocId;

            if (associatedClosingDisclosureDocument.HasValue)
            {
                this.sTpoRequestForInitialClosingDisclosureGenerationStatusAttachedDocs.Attributes["data-doc-id"] = associatedClosingDisclosureDocument.Value.ToString();
            }
            else
            {
                this.sTpoRequestForInitialClosingDisclosureGenerationStatusAttachedDocs.Attributes["class"] += " hidden";
            }
        }

        /// <summary>
        /// Gets the ID of the audit associated with the latest TPO portal request for disclosures.
        /// </summary>
        /// <param name="type">
        /// The type of the disclosure.
        /// </param>
        /// <param name="eventId">
        /// The ID of the event for re-disclosure audits.
        /// </param>
        /// <returns>
        /// The ID of the audit associated with the latest TPO portal request or null if no association exists.
        /// </returns>
        private Guid? GetLatestTpoDisclosureAuditId(TpoDisclosureType type, Guid? eventId = null)
        {
            Guid? latestAssociatedId = null;
            string auditType = null;

            switch (type)
            {
                case TpoDisclosureType.InitialDisclosure:
                    if (this.DataLoan.sTpoRequestForInitialDisclosureGenerationStatusT == TpoRequestForInitialDisclosureGenerationEventType.NoRequest)
                    {
                        return null;
                    }

                    latestAssociatedId = this.DataLoan.sTpoRequestForInitialDisclosureGenerationEventCollection.LatestEventAssociatedAuditId;
                    if (latestAssociatedId.HasValue)
                    {
                        return latestAssociatedId.Value;
                    }

                    auditType = typeof(TpoRequestForInitialDisclosureGenerationEventAuditItem).ToString();
                    break;

                case TpoDisclosureType.Redisclosure:
                    var associatedRedisclosureCollection = this.DataLoan.sTpoRequestForRedisclosureGenerationEventCollection
                        .FirstOrDefault(collection => collection.HasEvent(eventId.Value));

                    if (associatedRedisclosureCollection == null || 
                        associatedRedisclosureCollection.LatestEventStatus == TpoRequestForRedisclosureGenerationEventType.NoRequest)
                    {
                        return null;
                    }

                    latestAssociatedId = associatedRedisclosureCollection.LatestEventAssociatedAuditId;
                    if (latestAssociatedId.HasValue)
                    {
                        return latestAssociatedId.Value;
                    }

                    auditType = typeof(TpoRequestForRedisclosureGenerationAuditItem).ToString();
                    break;

                case TpoDisclosureType.InitialClosingDisclosure:
                    if (this.DataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusT == TpoRequestForInitialClosingDisclosureGenerationEventType.NoRequest)
                    {
                        return null;
                    }

                    latestAssociatedId = this.DataLoan.sTpoRequestForInitialClosingDisclosureGenerationEventCollection.LatestEventAssociatedAuditId;
                    if (latestAssociatedId.HasValue)
                    {
                        return latestAssociatedId.Value;
                    }

                    auditType = typeof(TpoRequestForInitialClosingDisclosureGenerationEventAuditItem).ToString();
                    break;

                default:
                    throw new UnhandledEnumException(type);
            }

            // This event collection is from before we started associating audits
            // with loan events. In this case, we'll need to search the audit history
            // for the associated event.
            var auditHistory = AuditManager.RetrieveAuditList(this.LoanID);
            var associatedAudit = (from audit in auditHistory
                                   where audit.AuditType.Contains(auditType, StringComparison.OrdinalIgnoreCase)
                                   orderby audit.Timestamp descending
                                   select audit).FirstOrDefault();

            return associatedAudit?.ID;
        }

        /// <summary>
        /// Determines whether the TPO disclosure requests section is enabled.
        /// </summary>
        /// <returns>
        /// True if the section is enabled, false otherwise.
        /// </returns>
        private bool IsTpoDisclosureRequestSectionEnabled()
        {
            if (this.DataLoan.sPmlBrokerId == Guid.Empty &&
                !this.Broker.EnableRetailTpoPortalMode)
            {
                // Do not display the TPO disclosures section 
                // when the loan is not brokered and the lender
                // is not using the retail portal mode.
                return false;
            }

            return this.DataLoan.sTpoRequestForInitialDisclosureGenerationStatusT != TpoRequestForInitialDisclosureGenerationEventType.NoRequest ||
                this.DataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusT != TpoRequestForInitialClosingDisclosureGenerationEventType.NoRequest ||
                this.DataLoan.sTpoRequestForRedisclosureGenerationEventCollection.Any(collection => collection.LatestEventStatus != TpoRequestForRedisclosureGenerationEventType.NoRequest);
        }

        /// <summary>
        /// Provides a POCO for data necessary to data bind borrower-level 
        /// disclosure dates.
        /// </summary>
        private class BorrowerLevelDisclosureDataBindingMetadata
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="BorrowerLevelDisclosureDataBindingMetadata"/> class.
            /// </summary>
            /// <param name="dates">
            /// The loan estimate row associated with the borrower-level dates.
            /// </param>
            /// <param name="associatedArchiveIsInvalid">
            /// True if the LE is associated with an invalid archive, false otherwise.
            /// </param>
            /// <param name="enableBorrowerLevelDisclosureTracking">
            /// True if the lender enables the borrower-level disclosure tracking UI,
            /// false otherwise.
            /// </param>
            public BorrowerLevelDisclosureDataBindingMetadata(LoanEstimateDates dates, bool associatedArchiveIsInvalid, bool enableBorrowerLevelDisclosureTracking)
                : this(dates.UniqueId, dates.IsManual, associatedArchiveIsInvalid, enableBorrowerLevelDisclosureTracking)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="BorrowerLevelDisclosureDataBindingMetadata"/> class.
            /// </summary>
            /// <param name="dates">
            /// The closing disclosure row associated with the borrower-level dates.
            /// </param>
            /// <param name="associatedArchiveIsInvalid">
            /// True if the LE is associated with an invalid archive, false otherwise.
            /// </param>
            /// <param name="enableBorrowerLevelDisclosureTracking">
            /// True if the lender enables the borrower-level disclosure tracking UI,
            /// false otherwise.
            /// </param>
            public BorrowerLevelDisclosureDataBindingMetadata(ClosingDisclosureDates dates, bool associatedArchiveIsInvalid, bool enableBorrowerLevelDisclosureTracking)
                : this(dates.UniqueId, dates.IsManual, associatedArchiveIsInvalid, enableBorrowerLevelDisclosureTracking)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="BorrowerLevelDisclosureDataBindingMetadata"/> class.
            /// </summary>
            /// <param name="uniqueId">
            /// The unique ID of the LE or CD row associated with the borrower-level dates.
            /// </param>
            /// <param name="isManual">
            /// True if the associated LE or CD row was manually added, false otherwise.
            /// </param>
            /// <param name="associatedArchiveIsInvalid">
            /// True if the LE is associated with an invalid archive, false otherwise.
            /// </param>
            /// <param name="enableBorrowerLevelDisclosureTracking">
            /// True if the lender enables the borrower-level disclosure tracking UI,
            /// false otherwise.
            /// </param>
            private BorrowerLevelDisclosureDataBindingMetadata(Guid uniqueId, bool isManual, bool associatedArchiveIsInvalid, bool enableBorrowerLevelDisclosureTracking)
            {
                this.UniqueId = uniqueId;
                this.IsManual = isManual;
                this.AssociatedArchiveIsInvalid = associatedArchiveIsInvalid;
                this.EnableBorrowerLevelDisclosureTracking = enableBorrowerLevelDisclosureTracking;
            }

            /// <summary>
            /// Gets or sets the unique ID for the LE or CD row that
            /// the borrowers are associated with.
            /// </summary>
            public Guid UniqueId { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the associated
            /// LE or CD date row was manually added.
            /// </summary>
            public bool IsManual { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the associated
            /// LE or CD date row is associated with an invalid archive.
            /// </summary>
            public bool AssociatedArchiveIsInvalid { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the lender enables
            /// the borrower-level disclosure tracking.
            /// </summary>
            /// <remarks>
            /// If the lender does not enable this feature, the data will
            /// still be bound to ensure data integrity but will not appear
            /// in the UI for the user.
            /// </remarks>
            public bool EnableBorrowerLevelDisclosureTracking { get; set; }
        }
    }
}
