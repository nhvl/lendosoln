///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
	public partial class AppraisalDisclosure : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Right To Receive Appraisal";
            this.PageID = "AppraisalDisclosure";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CAppraisalDisclosurePDF);
            RegisterJsScript("jquery-1.6.min.js");
			RegisterJsScript("LQBPopup.js");
			AppraisalDisclosureZip.SmartZipcode(AppraisalDisclosureCity, AppraisalDisclosureState);
        }

		protected override void LoadData() 
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(AppraisalDisclosure));
			dataLoan.InitLoad();

			IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.AppraisalDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 

			AppraisalDisclosureCompanyName.Text = broker.CompanyName;
			AppraisalDisclosureStreetAddr.Text = broker.StreetAddr;
			AppraisalDisclosureCity.Text = broker.City;
			AppraisalDisclosureState.Value = broker.State;
			AppraisalDisclosureZip.Text = broker.Zip;

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
