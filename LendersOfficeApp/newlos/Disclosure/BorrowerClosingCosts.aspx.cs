namespace LendersOfficeApp.newlos.Disclosure
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web.Services;
    using System.Web.UI.WebControls;

    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Migration;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;

    [DataContract]
    public class BorrowerClosingCostFeeViewModel
    {
        [DataMember]
        public IEnumerable<BorrowerClosingCostFeeSection> SectionList { get; set; }

        [DataMember]
        public string VRoot = Tools.VRoot;

        [DataMember]
        public bool sLoads1003LineLFromAdjustments { get; set; }

        [DataMember]
        public bool sIsRequireFeesFromDropDown { get; set; }

        [DataMember]
        public BorrowerEditableFieldsModel editableFieldsModel { get; set; }

        [DataMember]
        public E_sClosingCostFeeVersionT sClosingCostFeeVersionT { get; set; }

        [DataMember]
        public bool DisableTPAffIfHasContact { get; set; }

        [DataMember]
        public bool sIPiaDyLckd { get; set; }
        [DataMember]
        public string sIPiaDy { get; set; }
        [DataMember]
        public bool sIPerDayLckd { get; set; }
        [DataMember]
        public string sIPerDay { get; set; }
        [DataMember]
        public bool CanReadDFLP { get; set; }
        [DataMember]
        public bool CanSetDFLP { get; set; }


        [DataMember]
        public string sLAmtCalc { get; set; }
        [DataMember]
        public bool sLAmtLckd { get; set; }
        [DataMember]
        public string sFinalLAmt { get; set; }
        [DataMember]
        public string sGfeTotalFundByLender { get; set; }
        [DataMember]
        public string sGfeTotalFundBySeller { get; set; }
        [DataMember]
        public string sPurchPrice { get; set; }
        [DataMember]
        public bool sIsRenovationLoan { get; set; }
        [DataMember]
        public bool sAltCostLckd { get; set; }
        [DataMember]
        public string sAltCost { get; set; }
        [DataMember]
        public string sLandCost { get; set; }
        [DataMember]
        public bool sRefPdOffAmt1003Lckd { get; set; }
        [DataMember]
        public string sRefPdOffAmt1003 { get; set; }
        [DataMember]
        public bool sTotEstPp1003Lckd { get; set; }
        [DataMember]
        public string sTotEstPp1003 { get; set; }
        [DataMember]
        public bool sTotEstCc1003Lckd { get; set; }
        [DataMember]
        public string sTotEstCcNoDiscnt1003 { get; set; }
        [DataMember]
        public bool sFfUfmip1003Lckd { get; set; }
        [DataMember]
        public string sFfUfmip1003 { get; set; }
        [DataMember]
        public bool sLDiscnt1003Lckd { get; set; }
        [DataMember]
        public string sLDiscnt1003 { get; set; }
        [DataMember]
        public string sTotTransC { get; set; }
        [DataMember]
        public string sOCredit1Desc { get; set; }
        [DataMember]
        public string sOCredit2Desc { get; set; }
        [DataMember]
        public string sOCredit3Desc { get; set; }
        [DataMember]
        public string sOCredit4Desc { get; set; }
        [DataMember]
        public string sONewFinBal { get; set; }
        [DataMember]
        public bool sTotCcPbsLocked { get; set; }
        [DataMember]
        public string sTotCcPbs { get; set; }
        [DataMember]
        public bool sOCredit1Lckd { get; set; }
        [DataMember]
        public string sOCredit1Amt { get; set; }
        [DataMember]
        public string sOCredit2Amt { get; set; }
        [DataMember]
        public string sOCredit3Amt { get; set; }
        [DataMember]
        public string sOCredit4Amt { get; set; }
        [DataMember]
        public string sOCredit5Amt { get; set; }
        [DataMember]
        public string sONewFinCc { get; set; }
        [DataMember]
        public string sFfUfmipFinanced { get; set; }
        [DataMember]
        public bool sTransNetCashLckd { get; set; }
        [DataMember]
        public string sTransNetCash { get; set; }

        [DataMember]
        public E_sLenderCreditCalculationMethodT sLenderCreditCalculationMethodT { get; set; }
        [DataMember]
        public string sBrokerLockOriginatorPriceBrokComp1PcPrice { get; set; }
        [DataMember]
        public string sBrokerLockOriginatorPriceBrokComp1PcAmt { get; set; }
        [DataMember]
        public E_PercentBaseT sLDiscntBaseT { get; set; }
        [DataMember]
        public string sLDiscntPc { get; set; }
        [DataMember]
        public string sLDiscntFMb { get; set; }
        [DataMember]
        public string sLDiscnt { get; set; }
        [DataMember]
        public E_CreditLenderPaidItemT sGfeCreditLenderPaidItemT { get; set; }
        [DataMember]
        public string sGfeCreditLenderPaidItemF { get; set; }
        [DataMember]
        public string sGfeLenderCreditF { get; set; }
        [DataMember]
        public string sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt { get; set; }
        [DataMember]
        public string sLenderCustomCredit1Description { get; set; }
        [DataMember]
        public string sLenderCustomCredit1Amount { get; set; }
        [DataMember]
        public string sLenderCustomCredit1AmountAsCharge { get; set; }
        [DataMember]
        public string sLenderCustomCredit2Description { get; set; }
        [DataMember]
        public string sLenderCustomCredit2Amount { get; set; }
        [DataMember]
        public string sLenderCustomCredit2AmountAsCharge { get; set; }
        [DataMember]
        public string sLenderActualTotalCreditAmt_Neg { get; set; }
        [DataMember]
        public string sLenderCreditAvailableAmt_Neg { get; set; }
        [DataMember]
        public E_LenderCreditDiscloseLocationT sLenderPaidFeeDiscloseLocationT { get; set; }
        [DataMember]
        public string sLenderPaidFeesAmt_Neg { get; set; }
        [DataMember]
        public E_LenderCreditDiscloseLocationT sLenderGeneralCreditDiscloseLocationT { get; set; }
        [DataMember]
        public string sLenderGeneralCreditAmt_Neg { get; set; }
        [DataMember]
        public E_LenderCreditDiscloseLocationT sLenderCustomCredit1DiscloseLocationT { get; set; }
        [DataMember]
        public E_LenderCreditDiscloseLocationT sLenderCustomCredit2DiscloseLocationT { get; set; }
        [DataMember]
        public E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT { get; set; }
        [DataMember]
        public E_sDisclosureRegulationT sDisclosureRegulationT { get; set; }
        [DataMember]
        public bool sGfeIsTPOTransaction { get; set; }
        [DataMember]
        public BorrowerClosingCostFee StandaloneLenderOrigCompFee { get; set; }
        [DataMember]
        public bool sIsManuallySetThirdPartyAffiliateProps { get; set; }
        [DataMember]
        public BorrowerClosingCostFee LastSavedBorrowerOrigCompFee { get; set; }
        [DataMember]
        public string sTotEstPp { get; set; }
        [DataMember]
        public string sTotalBorrowerPaidProrations { get; set; }
        [DataMember]
        public string sTotEstCcNoDiscnt { get; set; }
        [DataMember]
        public bool sIsIncludeProrationsInTotPp { get; set; }
        [DataMember]
        public bool sIsIncludeONewFinCcInTotEstCc { get; set; }
        [DataMember]
        public bool sIsIncludeProrationsIn1003Details { get; set; }

        [DataMember]
        public bool IsConstruction { get; set; }

        [DataMember]
        public bool AreConstructionLoanDataPointsMigrated { get; set; }

        [DataMember]
        public string sPurchasePrice1003 { get; set; }

        [DataMember]
        public string sLandIfAcquiredSeparately1003 { get; set; }
    }

    [DataContract]
    public class BorrowerEditableFieldsModel
    {
        
        [DataMember]
        public bool sIsRequireFeesFromDropDown { get; set; }

        [DataMember]
        public E_sClosingCostAutomationUpdateT sClosingCostAutomationUpdateT { get; set; }

        [DataMember]
        public E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT { get; set; }

        [DataMember]
        public bool sGfeIsTPOTransaction { get; set; }

        [DataMember]
        public string sLenderPaidBrokerCompF { get; set; }

        [DataMember]
        public string sPurchasePrice1003 { get; set; }
        [DataMember]
        public string sLandIfAcquiredSeparately1003 { get; set; }
        [DataMember]
        public string sRefTransMortgageBalancePayoffAmt { get; set; }
        [DataMember]
        public string sRefNotTransMortgageBalancePayoffAmt { get; set; }
        [DataMember]
        public string sTotEstBorrCostUlad { get; set; }
        [DataMember]
        public string sTotTransCUlad { get; set; }
        [DataMember]
        public string sLAmtCalc { get; set; }
        [DataMember]
        public string sFfUfmipFinanced { get; set; }
        [DataMember]
        public string sFinalLAmt { get; set; }
        [DataMember]
        public string sONewFinBal { get; set; }
        [DataMember]
        public string sTotMortLAmtUlad { get; set; }
        [DataMember]
        public string sSellerCreditsUlad { get; set; }
        [DataMember]
        public string sTotOtherCreditsUlad { get; set; }
        [DataMember]
        public string sTotCreditsUlad { get; set; }
        [DataMember]
        public string sTotMortLTotCreditUlad { get; set; }
        [DataMember]
        public string sTransNetCashUlad { get; set; }
        [DataMember]
        public string sTRIDSellerCredits { get; set; }


        [DataMember]
        public string sAltCost { get; set; }
        [DataMember]
        public bool sAltCostLckd { get; set; }
        [DataMember]
        public string sLDiscnt1003 { get; set; }
        [DataMember]
        public bool sLDiscnt1003Lckd { get; set; }
        [DataMember]
        public string sTotEstCcNoDiscnt1003 { get; set; }
        [DataMember]
        public bool sTotEstCc1003Lckd { get; set; }
        [DataMember]
        public string sTotEstPp1003 { get; set; }
        [DataMember]
        public bool sTotEstPp1003Lckd { get; set; }
        [DataMember]
        public string sTotCcPbs { get; set; }
        [DataMember]
        public bool sTotCcPbsLocked { get; set; }

        [DataMember]
        public bool sAltCostLckdDisabled { get; set; }
    }

    public partial class BorrowerClosingCosts : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            RegisterService("cfpb_utils", "/newlos/Test/RActiveGFEService.aspx");

            base.OnInit(e);

            this.PageID = "BorrowerClosingCosts";
        }

        public bool hideHeaderAndBoarder = true;
        protected bool m_isPurchase = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            RegisterJsScript("ractive-0.7.1.min.js");
            RegisterJsScript("loanframework2.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterJsScript("mask.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("Ractive-Common.js");
            RegisterJsScript("LQBPopup.js");

            Tools.Bind_sOriginatorCompensationPaymentSourceT(sOriginatorCompensationPaymentSourceT);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(BorrowerClosingCosts));
            dataLoan.InitLoad();

            sGfeIsTPOTransaction.Enabled = !dataLoan.sGfeIsTPOTransactionIsCalculated || dataLoan.sBranchChannelT == E_BranchChannelT.Blank || dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent;

            m_isPurchase = dataLoan.sLPurposeT != E_sLPurposeT.Purchase && dataLoan.sLPurposeT != E_sLPurposeT.Construct && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm;

            Tools.SetRadioButtonListValue(sOriginatorCompensationPaymentSourceT, dataLoan.sOriginatorCompensationPaymentSourceT);
            if (dataLoan.sIsOriginationCompensationSourceRequired)
            {
                // 2/22/2011 dd - Need to remove Not specified option.
                foreach (ListItem item in sOriginatorCompensationPaymentSourceT.Items)
                {
                    if (item.Value == E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified.ToString("D"))
                    {
                        sOriginatorCompensationPaymentSourceT.Items.Remove(item);
                        break;
                    }
                }
            }

            Tools.SetRadioButtonListValue(sClosingCostAutomationUpdateT, dataLoan.sClosingCostAutomationUpdateT);
            var feeServiceHistory = new DataAccess.FeeService.FeeServiceApplication.ClosingCostApplicationHistory(dataLoan.sFeeServiceApplicationHistoryXmlContent);
            ccTableDiv.Visible = Broker.CalculateclosingCostInPML && dataLoan.sIsLoanProgramRegistered && !feeServiceHistory.IsBlank;

            DropDownList ddl = new DropDownList();
            Tools.Bind_sGfeCreditLenderPaidItemT(ddl);
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                ddl.Items.Remove(ddl.Items.FindByValue(E_CreditLenderPaidItemT.OriginatorCompensationOnly.ToString("D")));
            }
            sGfeCreditLenderPaidItemTRep.DataSource = ddl.Items;
            sGfeCreditLenderPaidItemTRep.DataBind();

            ddl = new DropDownList();
            Tools.Bind_PercentBaseLoanAmountsT(ddl);
            sLDiscntBaseTRep.DataSource = ddl.Items;
            sLDiscntBaseTRep.DataBind();
            sLDiscntBaseTRep_calc.DataSource = ddl.Items;
            sLDiscntBaseTRep_calc.DataBind();

            ddl = new DropDownList();
            Tools.Bind_sLenderCreditCalculationMethodT(ddl);
            sLenderCreditCalculationMethodTRep.DataSource = ddl.Items;
            sLenderCreditCalculationMethodTRep.DataBind();

            ddl = new DropDownList();
            Tools.Bind_LenderCreditDiscloseLocationT(ddl);
            sLenderPaidFeeDiscloseLocationTRep.DataSource = ddl.Items;
            sLenderPaidFeeDiscloseLocationTRep.DataBind();
            sLenderGeneralCreditDiscloseLocationTRep.DataSource = ddl.Items;
            sLenderGeneralCreditDiscloseLocationTRep.DataBind();
            sLenderCustomCredit1DiscloseLocationTRep.DataSource = ddl.Items;
            sLenderCustomCredit1DiscloseLocationTRep.DataBind();
            sLenderCustomCredit2DiscloseLocationTRep.DataSource = ddl.Items;
            sLenderCustomCredit2DiscloseLocationTRep.DataBind();

            sGfeIsTPOTransaction.Checked = dataLoan.sGfeIsTPOTransaction;
            sIsRequireFeesFromDropDown.Checked = dataLoan.sIsRequireFeesFromDropDown;
            trIsRequireFeesFromDropDown.Visible = BrokerUser.HasPermission(Permission.AllowEnablingCustomFeeDescriptions);

            var viewModel = new BorrowerClosingCostFeeViewModel
            {
                DisableTPAffIfHasContact =
                    dataLoan.sBranchChannelT == E_BranchChannelT.Retail ||
                    dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale ||
                    dataLoan.sBranchChannelT == E_BranchChannelT.Broker,
                sOriginatorCompensationPaymentSourceT = dataLoan.sOriginatorCompensationPaymentSourceT,
                sDisclosureRegulationT = dataLoan.sDisclosureRegulationT,
                sGfeIsTPOTransaction = dataLoan.sGfeIsTPOTransaction,
                sIPiaDyLckd = dataLoan.sIPiaDyLckd,
                sIPiaDy = dataLoan.sIPiaDy_rep,
                sIPerDayLckd = dataLoan.sIPerDayLckd,
                sIPerDay = dataLoan.sIPerDay_rep,
                sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown,
                sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT,
                sLoads1003LineLFromAdjustments = dataLoan.sLoads1003LineLFromAdjustments,
                sIsManuallySetThirdPartyAffiliateProps = dataLoan.sIsManuallySetThirdPartyAffiliateProps,
                SectionList = dataLoan.sClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.LoanClosingCost)
            };

            // Add the lender paid originator comp if in TRID2015 mode and Compensation payment source is lender.
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                dataLoan.sGfeIsTPOTransaction)
            {
                viewModel.StandaloneLenderOrigCompFee = dataLoan.sClosingCostSet.StandaloneLenderOrigCompFee;
            }

            if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
            {
                // If we're starting with a borrower-paid orig comp fee, we want to keep track of it so we can restore it later on if the user switches back to borrower paid.
                viewModel.LastSavedBorrowerOrigCompFee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId);
            }

            viewModel.CanReadDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead);
            viewModel.CanSetDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite);

            viewModel.IsConstruction = dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            viewModel.AreConstructionLoanDataPointsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints);

            BorrowerClosingCosts.PopulateDetailsOfTransaction(dataLoan, viewModel);
            BorrowerClosingCosts.PopulateLenderCredits(dataLoan, viewModel);

            viewModel.editableFieldsModel = CreateEditableFieldsModel(dataLoan);
            RegisterJsObject("ClosingCostData", viewModel);
            ClientScript.RegisterHiddenField("loanid", LoanID.ToString());

            RegisterJsGlobalVariables("IsUlad2019", dataLoan.sGseTargetApplicationT == GseTargetApplicationT.Ulad2019);

            BindPercentRepeaters();
            BindCreditDesc();
            BindBeneficiaryDropDown(BeneficiaryRepeater);
        }

        private void BindBeneficiaryDropDown(Repeater repeater)
        {
            DropDownList dl = new DropDownList();
            RolodexDB.PopulateAgentTypeDropDownList(dl);
            repeater.DataSource = dl.Items;
            repeater.DataBind();
        }

        private void BindCreditDesc()
        {
            var items = new List<ListItem>
            {
                new ListItem("Cash Deposit on sales contract", "Cash Deposit on sales contract"),
                new ListItem("Seller Credit", "Seller Credit"),
                new ListItem("Lender Credit", "Lender Credit"),
                new ListItem("Relocation Funds", "Relocation Funds"),
                new ListItem("Employer Assisted Housing", "Employer Assisted Housing"),
                new ListItem("Lease Purchase Fund", "Lease Purchase Fund"),
                new ListItem("Borrower Paid Fees", "Borrower Paid Fees"),
                new ListItem("Paid Outside of Closing", "Paid Outside of Closing"),
                new ListItem("Broker Credit", "Broker Credit")
            };

            CreditDescRepeater.DataSource = items;
            CreditDescRepeater.DataBind();
        }

        public void BindPercentRepeaters()
        {
            var items = new List<ListItem>
            {
                Tools.CreateEnumListItem("Loan Amount", E_PercentBaseT.LoanAmount),
                Tools.CreateEnumListItem("Purchase Price", E_PercentBaseT.SalesPrice),
                Tools.CreateEnumListItem("Appraisal Value", E_PercentBaseT.AppraisalValue),
                Tools.CreateEnumListItem("Total Loan Amount", E_PercentBaseT.TotalLoanAmount)
            };

            PercentRepeater.DataSource = items;
            PercentRepeater.DataBind();

            PercentRepeater2.DataSource = items;
            PercentRepeater2.DataBind();
        }

        private static BorrowerClosingCostFeeViewModel BindToDataLoan(CPageData dataLoan, string viewModelJson, Guid? idToAddPayment, string editableFieldsJson)
        {
            BorrowerClosingCostFeeViewModel viewModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<BorrowerClosingCostFeeViewModel>(viewModelJson);

            if (!string.IsNullOrEmpty(editableFieldsJson))
            {
                //Bind the editable field data
                BorrowerEditableFieldsModel editableFieldsModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<BorrowerEditableFieldsModel>(editableFieldsJson);

                if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowEnablingCustomFeeDescriptions))
                {
                    dataLoan.sIsRequireFeesFromDropDown = editableFieldsModel.sIsRequireFeesFromDropDown;
                }

                dataLoan.sClosingCostAutomationUpdateT = editableFieldsModel.sClosingCostAutomationUpdateT;
                dataLoan.sOriginatorCompensationPaymentSourceT = editableFieldsModel.sOriginatorCompensationPaymentSourceT;
                dataLoan.sGfeIsTPOTransaction = editableFieldsModel.sGfeIsTPOTransaction;

                BorrowerClosingCosts.BindQualifyingBorrowerToLoan(dataLoan, editableFieldsModel);
            }

            dataLoan.sClosingCostSet.UpdateWith((IEnumerable<BaseClosingCostFeeSection>)viewModel.SectionList);

            // If the state of the viewmodel before this method shows that the lender paid fee was on the page, save it.
            if (viewModel.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                viewModel.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                viewModel.sGfeIsTPOTransaction == true)
            {
                dataLoan.sClosingCostSet.StandaloneLenderOrigCompFee = viewModel.StandaloneLenderOrigCompFee;
            }

            dataLoan.sIPiaDyLckd = viewModel.sIPiaDyLckd;
            dataLoan.sIPiaDy_rep = viewModel.sIPiaDy;
            dataLoan.sIPerDayLckd = viewModel.sIPerDayLckd;
            dataLoan.sIPerDay_rep = viewModel.sIPerDay;

            BorrowerClosingCosts.BindDetailsOfTransactionToLoan(dataLoan, viewModel);

            BorrowerClosingCosts.BindLenderCreditsToLoan(dataLoan, viewModel);

            dataLoan.InvalidateCache();

            return viewModel;
        }

        private static BorrowerEditableFieldsModel CreateEditableFieldsModel(CPageData dataLoan)
        {
            var isConstruction = dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            bool areConstructionLoanDataPointsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints);

            return new BorrowerEditableFieldsModel
            {
                sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown,
                sClosingCostAutomationUpdateT = dataLoan.sClosingCostAutomationUpdateT,
                sOriginatorCompensationPaymentSourceT = dataLoan.sOriginatorCompensationPaymentSourceT,
                sLenderPaidBrokerCompF = dataLoan.sLenderPaidBrokerCompF_rep,
                sGfeIsTPOTransaction = dataLoan.sGfeIsTPOTransaction,

                sPurchasePrice1003 = dataLoan.sPurchasePrice1003_rep,
                sAltCost = dataLoan.sAltCost_rep,
                sLandIfAcquiredSeparately1003 = dataLoan.sLandIfAcquiredSeparately1003_rep,
                sRefTransMortgageBalancePayoffAmt = dataLoan.sRefTransMortgageBalancePayoffAmt_rep,
                sRefNotTransMortgageBalancePayoffAmt = dataLoan.sRefNotTransMortgageBalancePayoffAmt_rep,
                sTotEstBorrCostUlad = dataLoan.sTotEstBorrCostUlad_rep,
                sLDiscnt1003 = dataLoan.sLDiscnt1003_rep,
                sLDiscnt1003Lckd = dataLoan.sLDiscnt1003Lckd,
                sTotTransCUlad = dataLoan.sTotTransCUlad_rep,
                sLAmtCalc = dataLoan.sLAmtCalc_rep,
                sFfUfmipFinanced = dataLoan.sFfUfmipFinanced_rep,
                sFinalLAmt = dataLoan.sFinalLAmt_rep,
                sONewFinBal = dataLoan.sONewFinBal_rep,
                sTotMortLAmtUlad = dataLoan.sTotMortLAmtUlad_rep,
                sSellerCreditsUlad = dataLoan.sSellerCreditsUlad_rep,
                sTotOtherCreditsUlad = dataLoan.sTotOtherCreditsUlad_rep,
                sTotCreditsUlad = dataLoan.sTotCreditsUlad_rep,
                sTotMortLTotCreditUlad = dataLoan.sTotMortLTotCreditUlad_rep,
                sTransNetCashUlad = dataLoan.sTransNetCashUlad_rep,
                sAltCostLckd = dataLoan.sAltCostLckd,
                sTotEstCcNoDiscnt1003 = dataLoan.sTotEstCcNoDiscnt1003_rep,
                sTotEstCc1003Lckd = dataLoan.sTotEstCc1003Lckd,
                sTotEstPp1003 = dataLoan.sTotEstPp1003_rep,
                sTotEstPp1003Lckd = dataLoan.sTotEstPp1003Lckd,
                sTotCcPbs = dataLoan.sTotCcPbs_rep,
                sTotCcPbsLocked = dataLoan.sTotCcPbsLocked,
                sTRIDSellerCredits = dataLoan.sTRIDSellerCredits_rep,
                sAltCostLckdDisabled = !dataLoan.sIsRenovationLoan || (areConstructionLoanDataPointsMigrated && isConstruction)
            };
        }

        private static void PopulateDetailsOfTransaction(CPageData dataLoan, BorrowerClosingCostFeeViewModel viewModel)
        {
            if (dataLoan.sGseTargetApplicationT == GseTargetApplicationT.Ulad2019)
            {
                return;
            }

            viewModel.sLAmtLckd = dataLoan.sLAmtLckd;
            viewModel.sLAmtCalc = dataLoan.sLAmtCalc_rep;
            viewModel.sFinalLAmt = dataLoan.sFinalLAmt_rep;
            viewModel.sGfeTotalFundByLender = dataLoan.sGfeTotalFundByLender_rep;
            viewModel.sGfeTotalFundBySeller = dataLoan.sGfeTotalFundBySeller_rep;
            viewModel.sPurchPrice = dataLoan.sPurchPrice_rep;
            viewModel.sPurchasePrice1003 = dataLoan.sPurchasePrice1003_rep;
            viewModel.sIsRenovationLoan = dataLoan.sIsRenovationLoan;
            viewModel.sAltCostLckd = dataLoan.sAltCostLckd;
            viewModel.sAltCost = dataLoan.sAltCost_rep;
            viewModel.sLandCost = dataLoan.sLandCost_rep;
            viewModel.sLandIfAcquiredSeparately1003 = dataLoan.sLandIfAcquiredSeparately1003_rep;
            viewModel.sRefPdOffAmt1003Lckd = dataLoan.sRefPdOffAmt1003Lckd;
            viewModel.sRefPdOffAmt1003 = dataLoan.sRefPdOffAmt1003_rep;
            viewModel.sTotEstPp1003Lckd = dataLoan.sTotEstPp1003Lckd;
            viewModel.sTotEstPp1003 = dataLoan.sTotEstPp1003_rep;
            viewModel.sTotEstCc1003Lckd = dataLoan.sTotEstCc1003Lckd;
            viewModel.sTotEstCcNoDiscnt1003 = dataLoan.sTotEstCcNoDiscnt1003_rep;
            viewModel.sFfUfmip1003Lckd = dataLoan.sFfUfmip1003Lckd;
            viewModel.sFfUfmip1003 = dataLoan.sFfUfmip1003_rep;
            viewModel.sLDiscnt1003Lckd = dataLoan.sLDiscnt1003Lckd;
            viewModel.sLDiscnt1003 = dataLoan.sLDiscnt1003_rep;
            viewModel.sTotTransC = dataLoan.sTotTransC_rep;
            viewModel.sOCredit1Desc = dataLoan.sOCredit1Desc;
            viewModel.sOCredit2Desc = dataLoan.sOCredit2Desc;
            viewModel.sOCredit3Desc = dataLoan.sOCredit3Desc;
            viewModel.sOCredit4Desc = dataLoan.sOCredit4Desc;
            viewModel.sONewFinBal = dataLoan.sONewFinBal_rep;
            viewModel.sTotCcPbsLocked = dataLoan.sTotCcPbsLocked;
            viewModel.sTotCcPbs = dataLoan.sTotCcPbs_rep;
            viewModel.sOCredit1Lckd = dataLoan.sOCredit1Lckd;
            viewModel.sOCredit1Amt = dataLoan.sOCredit1Amt_rep;
            viewModel.sOCredit2Amt = dataLoan.sOCredit2Amt_rep;
            viewModel.sOCredit3Amt = dataLoan.sOCredit3Amt_rep;
            viewModel.sOCredit4Amt = dataLoan.sOCredit4Amt_rep;
            viewModel.sOCredit5Amt = dataLoan.sOCredit5Amt_rep;
            viewModel.sONewFinCc = dataLoan.sONewFinCc_rep;
            viewModel.sFfUfmipFinanced = dataLoan.sFfUfmipFinanced_rep;
            viewModel.sTransNetCashLckd = dataLoan.sTransNetCashLckd;
            viewModel.sTransNetCash = dataLoan.sTransNetCash_rep;
        }

        private static void BindDetailsOfTransactionToLoan(CPageData dataLoan, BorrowerClosingCostFeeViewModel viewModel)
        {
            if (dataLoan.sGseTargetApplicationT == GseTargetApplicationT.Ulad2019)
            {
                return;
            }

            dataLoan.sLAmtLckd = viewModel.sLAmtLckd;
            dataLoan.sLAmtCalc_rep = viewModel.sLAmtCalc;

            dataLoan.sRefPdOffAmt1003Lckd = viewModel.sRefPdOffAmt1003Lckd;
            dataLoan.sTotEstPp1003Lckd = viewModel.sTotEstPp1003Lckd;
            dataLoan.sTotEstCc1003Lckd = viewModel.sTotEstCc1003Lckd;
            dataLoan.sFfUfmip1003Lckd = viewModel.sFfUfmip1003Lckd;
            dataLoan.sLDiscnt1003Lckd = viewModel.sLDiscnt1003Lckd;
            dataLoan.sOCredit1Lckd = viewModel.sOCredit1Lckd;
            dataLoan.sTransNetCashLckd = viewModel.sTransNetCashLckd;

            dataLoan.sPurchPrice_rep = viewModel.sPurchPrice;
            dataLoan.sAltCostLckd = viewModel.sAltCostLckd;
            dataLoan.sAltCost_rep = viewModel.sAltCost;
            dataLoan.sLandCost_rep = viewModel.sLandCost;
            dataLoan.sRefPdOffAmt1003_rep = viewModel.sRefPdOffAmt1003;
            dataLoan.sTotEstPp1003_rep = viewModel.sTotEstPp1003;
            dataLoan.sTotEstCcNoDiscnt1003_rep = viewModel.sTotEstCcNoDiscnt1003;
            dataLoan.sFfUfmip1003_rep = viewModel.sFfUfmip1003;
            dataLoan.sLDiscnt1003_rep = viewModel.sLDiscnt1003;
            dataLoan.sOCredit1Desc = viewModel.sOCredit1Desc;
            if (false == dataLoan.sLoads1003LineLFromAdjustments)
            {
                dataLoan.sOCredit2Desc = viewModel.sOCredit2Desc;
                dataLoan.sOCredit3Desc = viewModel.sOCredit3Desc;
                dataLoan.sOCredit4Desc = viewModel.sOCredit4Desc;
            }

            dataLoan.sTotCcPbsLocked = viewModel.sTotCcPbsLocked;
            dataLoan.sTotCcPbs_rep = viewModel.sTotCcPbs;
            dataLoan.sOCredit1Amt_rep = viewModel.sOCredit1Amt;
            if (false == dataLoan.sLoads1003LineLFromAdjustments)
            {
                dataLoan.sOCredit2Amt_rep = viewModel.sOCredit2Amt;
                dataLoan.sOCredit3Amt_rep = viewModel.sOCredit3Amt;
                dataLoan.sOCredit4Amt_rep = viewModel.sOCredit4Amt;
            }

            dataLoan.sONewFinCc_rep = viewModel.sONewFinCc;
            dataLoan.sTransNetCash_rep = viewModel.sTransNetCash;
        }

        private static void PopulateLenderCredits(CPageData dataLoan, BorrowerClosingCostFeeViewModel viewModel)
        {
            viewModel.sLenderCreditCalculationMethodT = dataLoan.sLenderCreditCalculationMethodT;
            viewModel.sBrokerLockOriginatorPriceBrokComp1PcPrice = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcPrice;
            viewModel.sBrokerLockOriginatorPriceBrokComp1PcAmt = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcAmt_rep;
            viewModel.sLDiscntPc = dataLoan.sLDiscntPc_rep;
            viewModel.sLDiscntBaseT = dataLoan.sLDiscntBaseT;
            viewModel.sLDiscntFMb = dataLoan.sLDiscntFMb_rep;
            viewModel.sLDiscnt = dataLoan.sLDiscnt_rep;
            viewModel.sGfeCreditLenderPaidItemT = dataLoan.sGfeCreditLenderPaidItemT;
            viewModel.sGfeCreditLenderPaidItemF = dataLoan.sGfeCreditLenderPaidItemF_rep;
            viewModel.sGfeLenderCreditF = dataLoan.sGfeLenderCreditF_rep;
            viewModel.sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt = dataLoan.sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt_rep;
            viewModel.sLenderCustomCredit1Description = dataLoan.sLenderCustomCredit1Description;
            viewModel.sLenderCustomCredit1Amount = dataLoan.sLenderCustomCredit1Amount_rep;
            viewModel.sLenderCustomCredit1AmountAsCharge = dataLoan.sLenderCustomCredit1AmountAsCharge_rep;
            viewModel.sLenderCustomCredit2Description = dataLoan.sLenderCustomCredit2Description;
            viewModel.sLenderCustomCredit2Amount = dataLoan.sLenderCustomCredit2Amount_rep;
            viewModel.sLenderCustomCredit2AmountAsCharge = dataLoan.sLenderCustomCredit2AmountAsCharge_rep;
            viewModel.sLenderActualTotalCreditAmt_Neg = dataLoan.sLenderActualTotalCreditAmt_Neg_rep;
            viewModel.sLenderCreditAvailableAmt_Neg = dataLoan.sLenderCreditAvailableAmt_Neg_rep;
            viewModel.sLenderPaidFeeDiscloseLocationT = dataLoan.sLenderPaidFeeDiscloseLocationT;
            viewModel.sLenderPaidFeesAmt_Neg = dataLoan.sLenderPaidFeesAmt_Neg_rep;
            viewModel.sLenderGeneralCreditDiscloseLocationT = dataLoan.sLenderGeneralCreditDiscloseLocationT;
            viewModel.sLenderGeneralCreditAmt_Neg = dataLoan.sLenderGeneralCreditAmt_Neg_rep;
            viewModel.sLenderCustomCredit1DiscloseLocationT = dataLoan.sLenderCustomCredit1DiscloseLocationT;
            viewModel.sLenderCustomCredit2DiscloseLocationT = dataLoan.sLenderCustomCredit2DiscloseLocationT;
        }

        private static void BindLenderCreditsToLoan(CPageData dataLoan, BorrowerClosingCostFeeViewModel viewModel)
        {
            dataLoan.sLenderCreditCalculationMethodT = viewModel.sLenderCreditCalculationMethodT;
            dataLoan.sLDiscntPc_rep = viewModel.sLDiscntPc;
            dataLoan.sLDiscntBaseT = viewModel.sLDiscntBaseT;
            dataLoan.sLDiscntFMb_rep = viewModel.sLDiscntFMb;
            dataLoan.sGfeCreditLenderPaidItemT = viewModel.sGfeCreditLenderPaidItemT;
            dataLoan.sLenderCustomCredit1Description = viewModel.sLenderCustomCredit1Description;
            dataLoan.sLenderCustomCredit1Amount_rep = viewModel.sLenderCustomCredit1Amount;
            dataLoan.sLenderCustomCredit2Description = viewModel.sLenderCustomCredit2Description;
            dataLoan.sLenderCustomCredit2Amount_rep = viewModel.sLenderCustomCredit2Amount;
            dataLoan.sLenderPaidFeeDiscloseLocationT = viewModel.sLenderPaidFeeDiscloseLocationT;
            dataLoan.sLenderGeneralCreditDiscloseLocationT = viewModel.sLenderGeneralCreditDiscloseLocationT;
            dataLoan.sLenderCustomCredit1DiscloseLocationT = viewModel.sLenderCustomCredit1DiscloseLocationT;
            dataLoan.sLenderCustomCredit2DiscloseLocationT = viewModel.sLenderCustomCredit2DiscloseLocationT;
        }

        private static void BindQualifyingBorrowerToLoan(CPageData dataLoan, BorrowerEditableFieldsModel editableFieldsModel)
        {
            if (dataLoan.sGseTargetApplicationT != GseTargetApplicationT.Ulad2019)
            {
                return;
            }

            dataLoan.sAltCost_rep = editableFieldsModel.sAltCost;
            dataLoan.sAltCostLckd = editableFieldsModel.sAltCostLckd;
            dataLoan.sLDiscnt1003_rep = editableFieldsModel.sLDiscnt1003;
            dataLoan.sLDiscnt1003Lckd = editableFieldsModel.sLDiscnt1003Lckd;
            dataLoan.sTotEstCcNoDiscnt1003_rep = editableFieldsModel.sTotEstCcNoDiscnt1003;
            dataLoan.sTotEstCc1003Lckd = editableFieldsModel.sTotEstCc1003Lckd;
            dataLoan.sTotEstPp1003_rep = editableFieldsModel.sTotEstPp1003;
            dataLoan.sTotEstPp1003Lckd = editableFieldsModel.sTotEstPp1003Lckd;
            dataLoan.sTotCcPbs_rep = editableFieldsModel.sTotCcPbs;
            dataLoan.sTotCcPbsLocked = editableFieldsModel.sTotCcPbsLocked;
        }

        private static string CreateViewModel(CPageData dataLoan, bool enableSorting, bool reinitialize, BorrowerClosingCostFeeViewModel oldViewModel)
        {
            var viewModel = new BorrowerClosingCostFeeViewModel
            {
                LastSavedBorrowerOrigCompFee = oldViewModel.LastSavedBorrowerOrigCompFee
            };


            if (oldViewModel.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
            {
                // We're updating the borrower paid originator compensation fee, so we should update the last saved originator compensation fee amounts.
                // No need to update when going from borrower paid to lender paid since the last saved should still be the most recent borrower paid amounts.
                viewModel.LastSavedBorrowerOrigCompFee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId);
            }

            if( reinitialize)
            {
                dataLoan.sClosingCostSet.Reinitialize();
            }

            viewModel.sOriginatorCompensationPaymentSourceT = dataLoan.sOriginatorCompensationPaymentSourceT;
            viewModel.sDisclosureRegulationT = dataLoan.sDisclosureRegulationT;
            viewModel.sGfeIsTPOTransaction = dataLoan.sGfeIsTPOTransaction; 
            viewModel.SectionList = dataLoan.sClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.LoanClosingCost);
            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;

            // Add the lender paid originator compensation fee seperately if the loan is in TRID and the source is Lender.
            if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                dataLoan.sGfeIsTPOTransaction)
            {
                viewModel.StandaloneLenderOrigCompFee = dataLoan.sClosingCostSet.StandaloneLenderOrigCompFee;
            }

            if (oldViewModel.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid && 
                dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid &&
                oldViewModel.LastSavedBorrowerOrigCompFee != null)
            {
                // The page before this recalc had a lender paid orig comp but it was switched to borrower paid.
                // We should try to restore the old borrower paid money amounts.
                BorrowerClosingCostFee currentOrigCompFee = (BorrowerClosingCostFee)dataLoan.sClosingCostSet.FindFeeByTypeId(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId);
                if (currentOrigCompFee != null)
                {
                    currentOrigCompFee.FormulaT = oldViewModel.LastSavedBorrowerOrigCompFee.FormulaT; 
                    currentOrigCompFee.BaseAmount = oldViewModel.LastSavedBorrowerOrigCompFee.BaseAmount;
                    currentOrigCompFee.Percent = oldViewModel.LastSavedBorrowerOrigCompFee.Percent;
                    currentOrigCompFee.PercentBaseT = oldViewModel.LastSavedBorrowerOrigCompFee.PercentBaseT;
                }

                viewModel.LastSavedBorrowerOrigCompFee = oldViewModel.LastSavedBorrowerOrigCompFee;
            }

            viewModel.sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;
            viewModel.sLoads1003LineLFromAdjustments = dataLoan.sLoads1003LineLFromAdjustments;
            viewModel.sIsManuallySetThirdPartyAffiliateProps = dataLoan.sIsManuallySetThirdPartyAffiliateProps;
            viewModel.sTotEstPp = dataLoan.sTotEstPp_rep;
            viewModel.sTotalBorrowerPaidProrations = dataLoan.sTotalBorrowerPaidProrations_rep;
            viewModel.sIsIncludeProrationsIn1003Details = dataLoan.sIsIncludeProrationsIn1003Details;
            viewModel.sTotEstCcNoDiscnt = dataLoan.sTotEstCcNoDiscnt_rep;
            viewModel.sIsIncludeProrationsInTotPp = dataLoan.sIsIncludeProrationsInTotPp;
            viewModel.sIsIncludeONewFinCcInTotEstCc = dataLoan.sIsIncludeONewFinCcInTotEstCc;

            viewModel.editableFieldsModel = CreateEditableFieldsModel(dataLoan);
            viewModel.CanReadDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead);
            viewModel.CanSetDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite);


            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;

            viewModel.IsConstruction = dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            viewModel.AreConstructionLoanDataPointsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints);

            BorrowerClosingCosts.PopulateDetailsOfTransaction(dataLoan, viewModel);            
            BorrowerClosingCosts.PopulateLenderCredits(dataLoan, viewModel);            

            return ObsoleteSerializationHelper.JsonSerialize(viewModel);
        }

        [WebMethod]
        public static string GetAvailableClosingCostFeeTypes(Guid loanId, string viewModelJson, string sectionName)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(BorrowerClosingCosts));
                dataLoan.InitLoad();

                BrokerDB db = PrincipalFactory.CurrentPrincipal.BrokerDB;

                BorrowerClosingCostFeeViewModel viewModel = BindToDataLoan(dataLoan, viewModelJson, null, null);

                FeeSetupClosingCostSet closingCostSet = db.GetUnlinkedClosingCostSet();
                FeeSetupClosingCostFeeSection section = (FeeSetupClosingCostFeeSection)closingCostSet.GetSection(E_ClosingCostViewT.LenderTypeEstimate, sectionName);

                List<BaseClosingCostFee> fees = viewModel.SectionList
                    .SelectMany(
                        closingSection => closingSection.FilteredClosingCostFeeList.Cast<BorrowerClosingCostFee>(),
                        (closingSection, fee) => fee.ConvertToFeeSetupFee())
                    .Cast<BaseClosingCostFee>()
                    .ToList();

                section.SetExcludeFeeList(fees, dataLoan.sClosingCostFeeVersionT, o => o.CanManuallyAddToEditor == false);

                List<object> subFeesBySourceFeeTypeId = section.FilteredClosingCostFeeList
                    .Select(f => new {
                        SourceFeeTypeId = f.ClosingCostFeeTypeId,
                        SubFees = dataLoan.sClosingCostSet.GetFees(fee => fee.SourceFeeTypeId == f.ClosingCostFeeTypeId).ToList() }).ToList<object>();

                return SerializationHelper.JsonNetAnonymousSerialize( new { Section = section, SubFeesBySourceFeeTypeId = subFeesBySourceFeeTypeId });
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                        Status = "Error",
                        UserMessage = e.UserMessage,
                        ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                    Tools.LogError(e);
                    throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CalculateData(Guid loanId, string viewModelJson, string editableFieldsJson)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(BorrowerClosingCosts));
                dataLoan.InitLoad();


                BorrowerClosingCostFeeViewModel oldViewModel = BindToDataLoan(dataLoan, viewModelJson, null, editableFieldsJson);


                return CreateViewModel(dataLoan, false, true, oldViewModel);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CreateAgent(Guid loanId, Guid id, bool populateFromRolodex, E_AgentRoleT agentType)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(BorrowerClosingCosts));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                CAgentFields agent = dataLoan.GetAgentFields(Guid.Empty);
                if (populateFromRolodex)
                {
                    agent.PopulateFromRolodex(id, BrokerUserPrincipal.CurrentPrincipal);
                }
                else
                {
                    CommonFunctions.CopyEmployeeInfoToAgent(PrincipalFactory.CurrentPrincipal.BrokerId, agent, id);
                }

                agent.AgentRoleT = agentType;
                agent.Update();

                dataLoan.Save();

                return "{\"RecordId\": \"" + agent.RecordId + "\"}";
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string Save(Guid loanId, string viewModelJson, string editableFieldsJson)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(BorrowerClosingCosts));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);


                BorrowerClosingCostFeeViewModel oldViewModel = BindToDataLoan(dataLoan, viewModelJson, null, editableFieldsJson);

                if (dataLoan.sClosingCostAutomationUpdateT == E_sClosingCostAutomationUpdateT.PreserveFeesOnLoan)
                {
                    // OPM 141471.
                    dataLoan.RecalculateClosingFeeConditions(true); //Force the calc regardless if revision changed.
                    dataLoan.sClosingCostAutomationUpdateT = E_sClosingCostAutomationUpdateT.UpdateOnConditionChange;
                }

                dataLoan.Save();
                return CreateViewModel(dataLoan, false, false, oldViewModel);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }

        }

        [WebMethod]
        public static string ClearAgent(Guid loanId, string viewModelJson, Guid recordId)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(BorrowerClosingCosts));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                // Bind unsaved data to loan
                BorrowerClosingCostFeeViewModel oldViewModel = BindToDataLoan(dataLoan, viewModelJson, null, null);

                DataSet ds = dataLoan.sAgentDataSet;
                DataTable table = ds.Tables[0];

                // Find agent to be deleted.
                DataRow toBeDeleted = table.Rows.Cast<DataRow>().FirstOrDefault(row => (string) row["recordId"] == recordId.ToString());

                // If agent found then delete and save. Otherwise do nothing.
                if (toBeDeleted != null)
                {
                    dataLoan.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(toBeDeleted, dataLoan.sSpState, AgentRecordChangeType.DeleteRecord));

                    // OPM 209868 - Clear beneficiary info in any closing cost fee where this agent is the beneficiary.
                    // If we have access to this page/method we can assume that the loan has been migrated.
                    dataLoan.sClosingCostSet.ClearBeneficiary(new Guid((string)toBeDeleted["recordId"]));
                    toBeDeleted.Delete();
                }

                dataLoan.sAgentDataSet = ds;
                dataLoan.Save();

                // Return deserialized data set.
                return CreateViewModel(dataLoan, false, false, oldViewModel);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }
    }
}
