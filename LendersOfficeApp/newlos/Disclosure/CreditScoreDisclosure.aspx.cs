using System;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Migration;

namespace LendersOfficeApp.newlos.Disclosure
{

	public partial class CreditScoreDisclosure : LendersOfficeApp.newlos.BaseLoanPage
	{
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageID = "CreditScoreDisclosure";
            this.PageTitle = "Credit Score Disclosure";

            this.EnableJqueryMigrate = false;

            this.PDFPrintClass = typeof(LendersOffice.Pdf.CCreditScoreInformationDisclosurePDF);

            CreditDisclosureLenderZip.SmartZipcode(CreditDisclosureLenderCity, CreditDisclosureLenderState);
            CreditTransUnionZip.SmartZipcode(CreditTransUnionCity, CreditTransUnionState);
            CreditExperianZip.SmartZipcode(CreditExperianCity, CreditExperianState);
            CreditEquifaxZip.SmartZipcode(CreditEquifaxCity, CreditEquifaxState);

            Tools.Bind_aDecisionCreditSourceT(aBDecisionCreditSourceT);
            Tools.Bind_aDecisionCreditSourceT(aCDecisionCreditSourceT);

            Tools.BindComboBox_CreditModelName(this.aBOtherCreditModelName);
            Tools.BindComboBox_CreditModelName(this.aCOtherCreditModelName);
            this.RegisterJsScript("LQBPopup.js");
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            this.RegisterJsGlobalVariables("OtherCreditSource", E_aDecisionCreditSourceT.Other.ToString("D"));
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CreditScoreDisclosure));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            Tools.SetDropDownListValue(aBDecisionCreditSourceT, dataApp.aBDecisionCreditSourceT);
            Tools.SetDropDownListValue(aCDecisionCreditSourceT, dataApp.aCDecisionCreditSourceT);

            aBDecisionCreditSourceTLckd.Checked = dataApp.aBDecisionCreditSourceTLckd;
            aCDecisionCreditSourceTLckd.Checked = dataApp.aCDecisionCreditSourceTLckd;

            aBDecisionCreditScore.Text = dataApp.aBDecisionCreditScore_rep;
            aCDecisionCreditScore.Text = dataApp.aCDecisionCreditScore_rep;

            this.aBOtherCreditModelName.Text = dataApp.aBOtherCreditModelName;
            this.aCOtherCreditModelName.Text = dataApp.aCOtherCreditModelName;

            aBExperianPercentile.Text = dataApp.aBExperianPercentile_rep;
            aBTransUnionPercentile.Text = dataApp.aBTransUnionPercentile_rep;
            aBEquifaxPercentile.Text = dataApp.aBEquifaxPercentile_rep;
            aCExperianPercentile.Text = dataApp.aCExperianPercentile_rep;
            aCTransUnionPercentile.Text = dataApp.aCTransUnionPercentile_rep;
            aCEquifaxPercentile.Text = dataApp.aCEquifaxPercentile_rep;

            this.sExperianModelName.Text = dataLoan.sExperianModelName;
            this.sEquifaxModelName.Text = dataLoan.sEquifaxModelName;
            this.sTransUnionModelName.Text = dataLoan.sTransUnionModelName;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V19_PopulateApplicationLevelCreditModelNames))
            {
                this.sExperianModelName.ReadOnly = true;
                this.sEquifaxModelName.ReadOnly = true;
                this.sTransUnionModelName.ReadOnly = true;
            }
            
            sExperianScoreFrom.Text   = dataLoan.sExperianScoreFrom_rep;
            sExperianScoreTo.Text     = dataLoan.sExperianScoreTo_rep;
            sEquifaxScoreFrom.Text    = dataLoan.sEquifaxScoreFrom_rep;
            sEquifaxScoreTo.Text      = dataLoan.sEquifaxScoreTo_rep;
            sTransUnionScoreFrom.Text = dataLoan.sTransUnionScoreFrom_rep;
            sTransUnionScoreTo.Text   = dataLoan.sTransUnionScoreTo_rep;

            aBExperianFactors.Text    = dataApp.aBExperianFactors;
            aBExperianCreatedD.Text   = dataApp.aBExperianCreatedD_rep;
            aBExperianScore.Text      = dataApp.aBExperianScore_rep;

            aCExperianFactors.Text    = dataApp.aCExperianFactors;
            aCExperianCreatedD.Text   = dataApp.aCExperianCreatedD_rep;
            aCExperianScore.Text      = dataApp.aCExperianScore_rep;

            aBEquifaxFactors.Text     = dataApp.aBEquifaxFactors;
            aBEquifaxCreatedD.Text    = dataApp.aBEquifaxCreatedD_rep;
            aBEquifaxScore.Text       = dataApp.aBEquifaxScore_rep;

            aCEquifaxFactors.Text     = dataApp.aCEquifaxFactors;
            aCEquifaxCreatedD.Text    = dataApp.aCEquifaxCreatedD_rep;
            aCEquifaxScore.Text       = dataApp.aCEquifaxScore_rep;

            aBTransUnionFactors.Text  = dataApp.aBTransUnionFactors;
            aBTransUnionCreatedD.Text = dataApp.aBTransUnionCreatedD_rep;
            aBTransUnionScore.Text    = dataApp.aBTransUnionScore_rep;

            aCTransUnionFactors.Text  = dataApp.aCTransUnionFactors;
            aCTransUnionCreatedD.Text = dataApp.aCTransUnionCreatedD_rep;
            aCTransUnionScore.Text    = dataApp.aCTransUnionScore_rep;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditEquifax, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            CreditEquifaxCompanyName.Text = preparer.CompanyName;
            CreditEquifaxStreetAddr.Text = preparer.StreetAddr;
            CreditEquifaxCity.Text = preparer.City;
            CreditEquifaxState.Value = preparer.State;
            CreditEquifaxZip.Text = preparer.Zip;
            CreditEquifaxPhoneOfCompany.Text = preparer.PhoneOfCompany;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditExperian, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            CreditExperianCompanyName.Text = preparer.CompanyName;
            CreditExperianStreetAddr.Text = preparer.StreetAddr;
            CreditExperianCity.Text = preparer.City;
            CreditExperianState.Value = preparer.State;
            CreditExperianZip.Text = preparer.Zip;
            CreditExperianPhoneOfCompany.Text = preparer.PhoneOfCompany;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditTransUnion, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            CreditTransUnionCompanyName.Text = preparer.CompanyName;
            CreditTransUnionStreetAddr.Text = preparer.StreetAddr;
            CreditTransUnionCity.Text = preparer.City;
            CreditTransUnionState.Value = preparer.State;
            CreditTransUnionZip.Text = preparer.Zip;
            CreditTransUnionPhoneOfCompany.Text = preparer.PhoneOfCompany;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditDisclosureLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            CreditDisclosureLenderCompanyName.Text = preparer.CompanyName;
            CreditDisclosureLenderStreetAddr.Text = preparer.StreetAddr;
            CreditDisclosureLenderCity.Text = preparer.City;
            CreditDisclosureLenderState.Value = preparer.State;
            CreditDisclosureLenderZip.Text = preparer.Zip;



        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            this.UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
