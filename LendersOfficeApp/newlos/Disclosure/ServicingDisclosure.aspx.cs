﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Disclosure
{
    public partial class ServicingDisclosure : BaseLoanPage
    {
        private void PageInit(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");

            this.EnableJqueryMigrate = false;

            this.PageTitle = "Servicing Disclosure Statement";
            this.PageID = "ServicingDisclosure2009";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CServicingDisclosureStatement2009PDF);
            ServicingDisclosure_Zip.SmartZipcode(ServicingDisclosure_City, ServicingDisclosure_State);
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ServicingDisclosure));
            dataLoan.InitLoad();

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.ServicingDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            ServicingDisclosure_CompanyName.Text = preparer.CompanyName;
            ServicingDisclosure_StreetAddr.Text = preparer.StreetAddr;
            ServicingDisclosure_City.Text = preparer.City;
            ServicingDisclosure_State.Value = preparer.State;
            ServicingDisclosure_Zip.Text = preparer.Zip;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            GfeTil_PrepareDate.Text = preparer.PrepareDate_rep;

            sMayAssignSellTransfer.Checked = dataLoan.sMayAssignSellTransfer;
            sDontService.Checked = dataLoan.sDontService;
            sServiceDontIntendToTransfer.Checked = dataLoan.sServiceDontIntendToTransfer;
        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            InitializeComponent();
            base.OnInit(e);
        }
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }
    }
}
