﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoanEstimateFeeList.ascx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.LoanEstimateFeeList" %>

<script id="LoanEstimateFeeListSectionWithFees" type="text/ractive">
    <div class="SectionContainer">
        <div class="SectionHeader">
            <span>{{SectionName}}</span>
            <span style="float:right; width:90px; text-align:right">{{TotalAmount}}</span>
        </div>
        {{#ClosingCostFeeList.length == 0}}
            <div class="GridItem">
                <span style="color:red">No fees are currently in this section</span>
            </div>
        {{/ClosingCostFeeList}}
        {{#ClosingCostFeeList:j}}
            <div  class="{{j%2 == 0? 'GridItem': 'GridAlternatingItem'}}">
                <span>
                    {{#if is_title}}
                        Title - 
                    {{/if}}
                    {{#if desc.length > 0}}
                    {{desc}}
                    {{else}}
                        <p style="display:inline">&nbsp;</p>
                    {{/if}}
                    {{#if is_optional}}
                        &nbsp; (optional)
                    {{/if}}
                </span>
                <span class="MoneyContainer">
                    {{total}}
                </span>
            </div>
        {{/ClosingCostFeeList}}
            
    </div>
</script>

<script id="LoanEstimateFeeListSectionD" type="text/ractive">
    <div class="SectionContainer">
        <div class="SectionHeader">
            <span>
                D. Total Loan Costs (A + B + C)
            </span>
            <span class="MoneyContainer">
                {{sTRIDLoanEstimateTotalLoanCosts}}
            </span>
        </div>
    </div>
</script>

<script id="LoanEstimateFeeListSectionI" type="text/ractive">
    <div class="SectionContainer">
        <div class="SectionHeader">
            <span>
                I. Total Other Costs (E + F + G + H)
            </span>
            <span class="MoneyContainer">
                {{sTRIDLoanEstimateTotalOtherCosts}}
            </span>
        </div> 
    </div>
</script>

<script id="LoanEstimateFeeListSectionJ" type="text/ractive">
    <div class="SectionContainer">
        <div class="SectionHeader">
            <span>
                J. Total Closing Costs
            </span>
            <span class="MoneyContainer">
                {{sTRIDLoanEstimateTotalClosingCosts}}
            </span>
        </div>

        <div class="GridItem">
            <span>D + I</span>
            <span class="MoneyContainer">
                {{sTRIDLoanEstimateTotalAllCosts}}
            </span>
        </div>

        <div class="GridAlternatingItem">
            <span> - Lender Credits</span>
            <span class="MoneyContainer">
                - {{sTRIDLoanEstimateLenderCredits}}
            </span>
        </div>
    </div>
</script>
