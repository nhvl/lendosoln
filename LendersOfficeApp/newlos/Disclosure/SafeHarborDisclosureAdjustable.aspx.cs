﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Disclosure
{
    public partial class SafeHarborDisclosureAdjustable : BaseLoanPage
    {
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SafeHarborDisclosureAdjustable));
            dataLoan.InitLoad();

            sIncludeSafeHarborARMWithRiskyFeaturesYes.Checked = dataLoan.sIncludeSafeHarborARMWithRiskyFeatures;
            sIncludeSafeHarborARMWithRiskyFeaturesNo.Checked = !sIncludeSafeHarborARMWithRiskyFeaturesYes.Checked;
            sIncludeSafeHarborARMRecLoanYes.Checked = dataLoan.sIncludeSafeHarborARMRecLoan;
            sIncludeSafeHarborARMRecLoanNo.Checked = !sIncludeSafeHarborARMRecLoanYes.Checked;

            sLpTemplateNmSafeHarborARMWithRiskyFeatures.Text = dataLoan.sLpTemplateNmSafeHarborARMWithRiskyFeatures;
            sLpTemplateNmSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sLpTemplateNmSafeHarborARMWithoutRiskyFeatures;
            sLpTemplateNmSafeHarborARMLowestCost.Text = dataLoan.sLpTemplateNmSafeHarborARMLowestCost;
            sLpTemplateNmSafeHarborARMRecLoan.Text = dataLoan.sLpTemplateNmSafeHarborARMRecLoan;
            sFinalLAmtSafeHarborARMWithRiskyFeatures.Text = dataLoan.sFinalLAmtSafeHarborARMWithRiskyFeatures_rep;
            sFinalLAmtSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sFinalLAmtSafeHarborARMWithoutRiskyFeatures_rep;
            sFinalLAmtSafeHarborARMLowestCost.Text = dataLoan.sFinalLAmtSafeHarborARMLowestCost_rep;
            sFinalLAmtSafeHarborARMRecLoan.Text = dataLoan.sFinalLAmtSafeHarborARMRecLoan_rep;
            sDueSafeHarborARMWithRiskyFeatures.Text = dataLoan.sDueSafeHarborARMWithRiskyFeatures_rep;
            sDueSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sDueSafeHarborARMWithoutRiskyFeatures_rep;
            sDueSafeHarborARMLowestCost.Text = dataLoan.sDueSafeHarborARMLowestCost_rep;
            sDueSafeHarborARMRecLoan.Text = dataLoan.sDueSafeHarborARMRecLoan_rep;
            sNoteIRSafeHarborARMWithRiskyFeatures.Text = dataLoan.sNoteIRSafeHarborARMWithRiskyFeatures_rep;
            sNoteIRSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sNoteIRSafeHarborARMWithoutRiskyFeatures_rep;
            sNoteIRSafeHarborARMLowestCost.Text = dataLoan.sNoteIRSafeHarborARMLowestCost_rep;
            sNoteIRSafeHarborARMRecLoan.Text = dataLoan.sNoteIRSafeHarborARMRecLoan_rep;
            sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures.Text = dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures_rep;
            sGfeProThisMPmtAndMInsSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMWithoutRiskyFeatures_rep;
            sGfeProThisMPmtAndMInsSafeHarborARMLowestCost.Text = dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMLowestCost_rep;
            sGfeProThisMPmtAndMInsSafeHarborARMRecLoan.Text = dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMRecLoan_rep;
            sFullyIndexedRSafeHarborARMWithRiskyFeatures.Text = dataLoan.sFullyIndexedRSafeHarborARMWithRiskyFeatures_rep;
            sFullyIndexedRSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sFullyIndexedRSafeHarborARMWithoutRiskyFeatures_rep;
            sFullyIndexedRSafeHarborARMLowestCost.Text = dataLoan.sFullyIndexedRSafeHarborARMLowestCost_rep;
            sFullyIndexedRSafeHarborARMRecLoan.Text = dataLoan.sFullyIndexedRSafeHarborARMRecLoan_rep;
            sRLifeCapRSafeHarborARMWithRiskyFeatures.Text = dataLoan.sRLifeCapRSafeHarborARMWithRiskyFeatures_rep;
            sRLifeCapRSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sRLifeCapRSafeHarborARMWithoutRiskyFeatures_rep;
            sRLifeCapRSafeHarborARMLowestCost.Text = dataLoan.sRLifeCapRSafeHarborARMLowestCost_rep;
            sRLifeCapRSafeHarborARMRecLoan.Text = dataLoan.sRLifeCapRSafeHarborARMRecLoan_rep;
            sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures.Text = dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures_rep;
            sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures_rep;
            sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost.Text = dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost_rep;
            sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan.Text = dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan_rep;
            sEquityCalcSafeHarborARMWithRiskyFeatures.Text = dataLoan.sEquityCalcSafeHarborARMWithRiskyFeatures_rep;
            sEquityCalcSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sEquityCalcSafeHarborARMWithoutRiskyFeatures_rep;
            sEquityCalcSafeHarborARMLowestCost.Text = dataLoan.sEquityCalcSafeHarborARMLowestCost_rep;
            sEquityCalcSafeHarborARMRecLoan.Text = dataLoan.sEquityCalcSafeHarborARMRecLoan_rep;
            sTransNetCashSafeHarborARMWithRiskyFeatures.Text = dataLoan.sTransNetCashSafeHarborARMWithRiskyFeatures_rep;
            sTransNetCashSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sTransNetCashSafeHarborARMWithoutRiskyFeatures_rep;
            sTransNetCashSafeHarborARMLowestCost.Text = dataLoan.sTransNetCashSafeHarborARMLowestCost_rep;
            sTransNetCashSafeHarborARMRecLoan.Text = dataLoan.sTransNetCashSafeHarborARMRecLoan_rep;
            sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures.Text = dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures_rep;
            sOriginatorCompensationTotalAmountSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMWithoutRiskyFeatures_rep;
            sOriginatorCompensationTotalAmountSafeHarborARMLowestCost.Text = dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMLowestCost_rep;
            sOriginatorCompensationTotalAmountSafeHarborARMRecLoan.Text = dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMRecLoan_rep;

            sSafeHarborARMNegAmort.Checked = dataLoan.sSafeHarborARMNegAmort;
            sSafeHarborARMDemandFeat.Checked = dataLoan.sSafeHarborARMDemandFeat;
            sSafeHarborARMPrepayPenalty.Checked = dataLoan.sSafeHarborARMPrepayPenalty;
            sSafeHarborARMSharedEquity.Checked = dataLoan.sSafeHarborARMSharedEquity;
            sSafeHarborARMIntOnlyPmts.Checked = dataLoan.sSafeHarborARMIntOnlyPmts;
            sSafeHarborARMSharedAppreciation.Checked = dataLoan.sSafeHarborARMSharedAppreciation;
            sSafeHarborARMBalloonPmntIn7Yrs.Checked = dataLoan.sSafeHarborARMBalloonPmntIn7Yrs;

            sSafeHarborARMOurRecLowestRate.Checked = dataLoan.sSafeHarborARMOurRecLowestRate;
            sSafeHarborARMOurRecWithoutRiskyFeatures.Checked = dataLoan.sSafeHarborARMOurRecWithoutRiskyFeatures;
            sSafeHarborARMOurRecLowestCosts.Checked = dataLoan.sSafeHarborARMOurRecLowestCosts;
            sSafeHarborARMOurRecOther.Checked = dataLoan.sSafeHarborARMOurRecOther;
            sSafeHarborARMOurRecText.Text = dataLoan.sSafeHarborARMOurRecText;

            sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures.Text = dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures_rep;
            sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures.Text = dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures_rep;
            sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost.Text = dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost_rep;
            sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan.Text = dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan_rep;

            IPreparerFields safeHarborARM = dataLoan.GetPreparerOfForm(E_PreparerFormT.SafeHarborARM, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            sSafeHarborARMAgentName.Text = safeHarborARM.PreparerName;
            sSafeHarborARMLoanOriginatorIdentifier.Text = safeHarborARM.LoanOriginatorIdentifier;
            sSafeHarborARMAgentLicense.Text = safeHarborARM.LicenseNumOfAgent;
            sSafeHarborARMPhone.Text = safeHarborARM.Phone;
            sSafeHarborARMCompanyName.Text = safeHarborARM.CompanyName;
            sSafeHarborARMCompanyLoanOriginatorIdentifier.Text = safeHarborARM.CompanyLoanOriginatorIdentifier;
            sSafeHarborARMCompanyLicense.Text = safeHarborARM.LicenseNumOfCompany;
            sSafeHarborARMCompanyPhone.Text = safeHarborARM.PhoneOfCompany;
            sSafeHarborARMCompanyFax.Text = safeHarborARM.FaxOfCompany;
            sSafeHarborARMCompanyStreetAddr.Text = safeHarborARM.StreetAddr;
            sSafeHarborARMCompanyCity.Text = safeHarborARM.City;
            sSafeHarborARMCompanyState.Value = safeHarborARM.State;
            sSafeHarborARMCompanyZip.Text = safeHarborARM.Zip;
            CFM.IsLocked = safeHarborARM.IsLocked;
            CFM.AgentRoleT = safeHarborARM.AgentRoleT;
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Safe Harbor Disclosure Adjustable";
            this.PageID = "SafeHarborDisclosureAdjustable";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CAntiSteeringSafeHarborDisclosureARMPDF);

            if (RequestHelper.GetInt("ispopup", 0) == 0)
            {
                bClose.Visible = false;
                bSave.Visible = false;
            }
            else
            {
                scriptHolderForPopup.Visible = true;
            }

            sSafeHarborARMCompanyZip.SmartZipcode(sSafeHarborARMCompanyCity, sSafeHarborARMCompanyState);
            CFM.Type = "26";
            CFM.AgentNameField = sSafeHarborARMAgentName.ClientID;
            CFM.LoanOriginatorIdentifierField = sSafeHarborARMLoanOriginatorIdentifier.ClientID;
            CFM.AgentLicenseField = sSafeHarborARMAgentLicense.ClientID;
            CFM.PhoneField = sSafeHarborARMPhone.ClientID;
            CFM.CompanyNameField = sSafeHarborARMCompanyName.ClientID;
            CFM.CompanyLoanOriginatorIdentifierField = sSafeHarborARMCompanyLoanOriginatorIdentifier.ClientID;
            CFM.CompanyLicenseField = sSafeHarborARMCompanyLicense.ClientID;
            CFM.StreetAddressField = sSafeHarborARMCompanyStreetAddr.ClientID;
            CFM.CityField = sSafeHarborARMCompanyCity.ClientID;
            CFM.StateField = sSafeHarborARMCompanyState.ClientID;
            CFM.ZipField = sSafeHarborARMCompanyZip.ClientID;
            CFM.CompanyPhoneField = sSafeHarborARMCompanyPhone.ClientID;
            CFM.CompanyFaxField = sSafeHarborARMCompanyFax.ClientID;
            CFM.IsAllowLockableFeature = true;

            this.RegisterJsScript("LQBPopup.js");
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
