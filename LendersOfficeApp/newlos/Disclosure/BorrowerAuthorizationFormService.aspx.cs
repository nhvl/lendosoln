///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class BorrowerAuthorizationFormServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(BorrowerAuthorizationFormServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.BorrSignatureAuthorization, E_ReturnOptionIfNotExist.CreateNew);

            broker.CompanyName = GetString("BorrSignatureAuthorizationCompanyName");
            broker.StreetAddr = GetString("BorrSignatureAuthorizationStreetAddr");
            broker.City = GetString("BorrSignatureAuthorizationCity");
            broker.State = GetString("BorrSignatureAuthorizationState");
            broker.Zip = GetString("BorrSignatureAuthorizationZip");
            broker.Update();

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
            gfeTil.PrepareDate_rep = GetString("GfeTilPrepareDate");
            gfeTil.Update();

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

	public partial class BorrowerAuthorizationFormService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new BorrowerAuthorizationFormServiceItem());
        }
	}
}
