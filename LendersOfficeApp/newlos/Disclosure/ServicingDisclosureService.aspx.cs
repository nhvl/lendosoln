﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class ServicingDisclosureServicingItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(ServicingDisclosureServicingItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.ServicingDisclosure, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("ServicingDisclosure_CompanyName");
            preparer.StreetAddr = GetString("ServicingDisclosure_StreetAddr");
            preparer.City = GetString("ServicingDisclosure_City");
            preparer.State = GetString("ServicingDisclosure_State");
            preparer.Zip = GetString("ServicingDisclosure_Zip");
            preparer.Update();
            
            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PrepareDate_rep = GetString("GfeTil_PrepareDate");
            preparer.Update();

            dataLoan.sMayAssignSellTransfer = GetBool("sMayAssignSellTransfer");
            dataLoan.sDontService = GetBool("sDontService");
            dataLoan.sServiceDontIntendToTransfer = GetBool("sServiceDontIntendToTransfer");
        }
    }
    public partial class ServicingDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new ServicingDisclosureServicingItem());
        }
    }
}
