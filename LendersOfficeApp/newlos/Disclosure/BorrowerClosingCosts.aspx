﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="BorrowerClosingCosts.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.BorrowerClosingCosts" %>
<%@ Register TagPrefix="uc1" TagName="QualifyingBorrower" Src="~/newlos/QualifyingBorrower.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title></title>
    <style type="text/css">        
        #Dot.tabContent {
            height: 450px;
        }

        body
        {
            background-color:gainsboro;
        }
        hr
        {
            clear:both;
        }
        .TopDiv
        {
            width: 30%;
            float:left;
            padding:5px;
        }
        .TopDivContainer
        {
            white-space:nowrap;
            margin:10px 15px 0px 15px;
            
        }
        .TopDivContainer table
        {
            width:30%;
            float:left;
        }
        .InlineDiv
        {
            padding:5px;
            white-space:nowrap;
        }
        table
        {
            white-space:nowrap;
        }
        
        input[preset='money']
        {
            width:90px;
        }
        
        .InlineDiv
        {
            min-width:30%;
            display:inline-block;
            text-align:center;
        }
        
        .SectionDiv
        {
            white-space:nowrap;
            margin: 10px 15px;
            clear:both;
            
            border-style:solid;
            border-width:2px;
        }
        #TimeDiv
        {
            border-style:none;
        }
        
        .InlineDiv span
        {
            margin-right:20px;
        }
        
        .GridHeader
        {
            padding:3px;
        }
        
        .LoanTermsDesc
        {
            width:230px;
            font-weight:bold;
            margin-right:40px;
            display:inline-block;
        }
        
        .LoanTermsInfo
        {
            display:inline-block;
        }
        
        .LoanTermOptions
        {
            line-height:16px;
        }
        
        .LoanTermOptions > div
        {
            padding: 5px;
        }
        
        .Month
        {
            width:30px;
        }
        
        Form
        {
            float:left;
            clear:left;
            min-width:100%;
        }
        #AmortSchedule
        {
            border-collapse:collapse;
            width:100%;
            border:solid 2px black;
            margin-bottom:10px;
        }
        #ExpensesTable
        {
            border-collapse:collapse;
            width: 400px;
            border:solid 2px black;
            margin-bottom:10px;
        }
        #AmortSchedule thead td, #ExpensesTable thead td
        {
            text-align:center;
        }
        #AmortSchedule td, #ExpensesTable td
        {
            border:solid 1px black;
        }
        .AmortScheduleContainer
        {
            padding:10px;
        }
        
        #sSpAddr
        {
            width:230px;
        }
        
        #sGfeEstScAvailTillDLckd
        {
            margin-left:60px;
        }
        
        #AmortizationTableDiv
        {
            width:630px;
            height:500px;
            display:none;
        }
        #AmortizationTableFrame
        {
            width:100%;
            height:95%;
        }
        
        .ui-dialog
        {
            border: solid 1px black !important;
        }
        .ui-dialog-titlebar
        {
            background-color:Maroon !important;
        }
        
        .HalfDiv
        {
            width:49%;
            display:inline-block;
            vertical-align:top;
        }
        .HalfDiv span
        {
            display:inline-block;
            width:250px;
            margin: 5px 0px; 
        }
        .IndentedLine
        {
            padding-left:50px;
        }
        .CostsAtClosing
        {
            padding: 5px 5px;
        }
        
        .modal-background {
              position: fixed;
              top: 0; left: 0; width: 100%; height: 100%;
              background-color: rgba(0,0,0,0.5);
              padding: 0.5em;
              text-align: center;
              -moz-box-sizing: border-box; box-sizing: border-box;
            }
            .modal-outer {
              position: relative;
              width: 100%;
              height: 100%;
            }

            .modal {
              position: relative;
              background-color: white;
              padding: 2em;
              box-shadow: 1px 1px 3px rgba(0,0,0,0.1);
              margin: 0 auto;
              display: inline-block;
              max-width: 100%;
              max-height: 100%;
              overflow-y: auto;
              -moz-box-sizing: border-box; box-sizing: border-box;
            }

            .modal-button {
              text-align: center;
              background-color: rgb(70,70,180);
              color: white;
              padding: 0.5em 1em;
              display: inline-block;
              cursor: pointer;
            }

            .modal ul li {
                text-align: left;
            }
            
            .OptionsList
            {
                list-style-type: none;
                display: none;
                background-color: white;
                border-style: solid;
                border-width: 1px;
                z-index:1;
                max-height: 300px;
                overflow-y:auto;
                padding:0px;
                margin:0px;
                margin:0 auto;
                text-align:left;
                position:relative;
            }
            .DDLImg, .ComboboxImg
            {
                width: 19px;
                height: 19px;
                top: 6px;
            }
            input[readonly]:not(.RactiveDDL)
            {
                background-color:lightgrey;
            }
            .QMDDL
            {
                width:200px;
            }
            .RactiveDDLContainer
            {
                overflow:visible;
                height:13px;
                display:inline-block;
            }
            
            input[preset='money']
            {
                width:90px;
                text-align:right;
            }
            input[preset='date']
            {
                width:75px;
            }
            select[disabled=""]
            {
                background-color: lightgrey;
            }
            .table-closingcost tfooter td
            {
                text-align:left;
            }
            
           
            /*****Design for TPO Portal Closing Costs *******/
            .div-title {
	            width: auto;
	            font-size:14px;
	            font-family:arial, helvetica, sans-serif; 
	            padding: 3px 0px 3px 9px;
	            margin-top: 5px;
	            white-space: normal;
            }
            .div-left {
	            float: left;
	            width: 50%;
	            min-width: 300px;
            }
            .div-right 
            {
	            padding-top: 3px;
	            float: right;
	            width: 40%;
	            margin-left: 10%
            }
            .div-right div{
	            padding: 3px
            }
            .table-closingcost {
	            width: 100%;
	            border-collapse: collapse;
	            font-weight: normal;
            }
            .table-closingcost th {
	            font-weight: normal;
            }
            .table-closingcost td,th {
	            padding: 3px;
	            text-align: center;
	            white-space:nowrap;
            }
            .table-calculation-1 {
	            width: 100%;
            }

            .table-calculation-1 td:nth-child(4) {
	            width: 5%;
            }
            .table-calculation-1 td:nth-child(6) *{
	            width: 100%;
	            min-width: 90px;
            }
            .table-calculation-1 td:nth-child(7) {
	            width: 5%;
            }
            .table-calculation-1 td:nth-child(9) *{
	            width: 100%;
	            min-width: 90px;
            }
            .table-calculation-1 td:nth-child(11) {
	            width: 10%;
            }
            .table-calculation-2 {
	            width: 100%;
	            border-collapse: collapse;
	            border: 1px solid #ebeff2;
            }
            .table-calculation-2 td,th {
	            padding: 3px;
            }
            .image-style {
	            padding: 0px 4px 0px 4px;
            }
            .div-image {
	            padding: 5px 3px 0px 3px;
	            width:0px;
            }
            .checkbox-hidden {
	            display: none;
            }
            .date-image {
	            vertical-align: middle;
            }

            /**TPO Portal Closing Cost Changes **/
            img
            {
                border-style:none;
            }
            
            .table-closingcost thead
            {
                background-color: #999999;
                color: Black;
            }
            
            #APTable, #AIRTable
            {
                width:100%;
                text-align:left;
            }
            
            #APTable td, #AIRTable td
            {
                width:50%;
            }
            
            #AdjustableDiv
            {
                text-align:center;
                border-style:none;
            }
            
            #AdjustableDiv .HalfDiv
            {
                width: 49%;
                margin: 0px;
            }
            
            .ThirdDiv
            {
                min-width: 30%;
                display: inline-block;
                padding:5px;
                vertical-align:top;
            }
            
            .InBetweenDiv
            {
                border-left:solid 2px black;
                border-right: solid 2px black;
            }
            
            .ContactHeader
            {
                font-weight:bold;
                text-decoration:underline;
            }
            
            .CFMContainer
            {
                display:inline-block;
            }
            
            .CFMLabel
            {
                width:80px;
                display:inline-block;
                margin-top:10px;
            }
            
            #After5YrsTable
            {
                display:inline-table;
                border-collapse: collapse;
                border: solid 1px black;
                margin: 10px;
                width:600px;
                vertical-align:top;
            }
            
            #After5YrsTable td
            {
                border-left: solid 1px black;
                border-right: solid 1px black;
                padding: 3px;
            }
            
            .ComparisonsDiv
            {
                display:inline-block;
                padding-top:20px;
            }
            
            .ComparisonSpan
            {
                width:250px;
                display:inline-block;
                margin:5px;
            }
            
            .OtherConsiderationsTable
            {
                border-collapse:collapse;
            }
            
            .OtherConsiderationsTable td
            {
                padding: 5px;
                border-style:none;
            }
            
            #sLateChargeBaseDesc
            {
                width:200px;
            }
            
            .smallNum
            {
                width: 50px;
            }
            
            #OtherConsiderationsTable
            {
                white-space:normal;
            }
            
            #OtherConsiderationsTable td.FieldLabel
            {
                white-space:nowrap;
                padding: 5px;
            }
            #OtherConsiderationsTable td
            {
                padding: 5px;
            }
            #dialog-confirm
            {
                text-align: center;
            }
            .LQBDialogBox .ui-button.ui-widget
            {
                border: none;
                padding: 0px;
                float: none;
                font-weight: normal;
            }
            .LQBDialogBox .ui-dialog-buttonset
            {
                text-align: center;
            }
            
            .Hidden
            {
                display:none;
            }
            
            .ContactImg
            {
                vertical-align:middle;
                margin-left:3px;
            }
            .ContactImg:hover
            {
                cursor:pointer;
            }
            .Hidden
            {
                display:none;
            }
            .table-closingcost td, .RactiveDDL, .DDLImg, .ContactImg, .RactiveDDLContainer
            {
                vertical-align:top  !important;
            }
            #footerWrapper 
		    {
		        position: fixed;
		        left: 0px;
		        bottom: 0px;
		        width: 100%;
		        
	            _position: absolute; /* Overrides position attribute in quirks mode */
            }
            
            #footer {
	            _margin-right: 17px; /* for right scroll bar in quirks mode */
	            
	            /**
	            /* Following properties needed to keep mouse clicks from falling through to underlying divs.
	            /* Placed at #footer level instead of #footerWrapper so that user can still click on #wrapper's scrollbar
	            **/
	            background: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7); /* background image set to 1x1 transparent GIF. */
	            width: 100%; /* needs to be set to 100%, or #footer width will match width of active tab */
            }
            .tabContent {
                background-color: gainsboro;
                padding: 10px;
            }
            #Tabs {
                background-color: Transparent;
            }
            
            #closeIcon{
                padding: 10px;
                float:right;
            }
            
            #closeIcon a,
            #closeIcon a:hover
            {
                font-size:15px;
                color: Gray;
                text-decoration: none;
            }
            #Dot
            {
                padding-bottom: 25px;
            }

            .SubFeeConfirmOverwriteContent
            {
                text-align: left;
                width: 400px;
            }

            .SubFeeConfirmOverwriteContentBtns
            {
                text-align: center;
            }
    </style>
</head>
<body>
    <div style="display:none;" id="dialog-confirm" title="Delete fee?">
        <p style="width: 100%">
        </p>
    </div>
    <form id="form1" runat="server">
    
    <div id="ConfirmAgent" style="display:none;">Selecting this contact will add it to the official contact list.</div>
    <div id="ClearAgent" style="display:none;">This action will remove the contact from the official contact list.</div>
    
    <script>

        function selectedDate(cal, date) {
            cal.sel.value = date;
            cal.callCloseHandler();
            if (typeof (updateDirtyBit) == 'function') updateDirtyBit();
            cal.sel.blur();
        } 
        
        function getNonRactiveData(editableFieldsModel) {
            var keys = Object.keys(editableFieldsModel);
            for (i = 0; i < keys.length; i++) {
                var id = keys[i];
                var el;

                if (id == "sClosingCostAutomationUpdateT" || id == "sOriginatorCompensationPaymentSourceT")
                {
                    el = $('input[name='+ id + ']');
                }
                else
                {
                    el = $("#" + id);
                }

                if (el.length > 0) {
                    if (el.attr("type") == "radio") {
                        editableFieldsModel[id] = el.filter(":checked").val();
                    } else if (el.attr("type") == "checkbox") {
                        editableFieldsModel[id] = el.prop("checked");
                    }
                    else {
                        editableFieldsModel[id] = el.val();
                    }
                }
            }
        }

        function setNonRactiveData(editableFieldsModel) {
            var keys = Object.keys(editableFieldsModel);
            for (i = 0; i < keys.length; i++) {
                var id = keys[i];
                var el;

                if (id == "sClosingCostAutomationUpdateT" || id == "sOriginatorCompensationPaymentSourceT")
                {
                    el = $('input[name='+ id + ']');
                }
                else
                {
                    el = $("#" + id + ", input[data-field-id='" + id + "'], span[data-field-id='" + id + "']");
                }

                if (el.length != 0) {
                    if (el.attr("type") == "radio") {
                        el.filter("[value='" + editableFieldsModel[id] + "']").prop("checked", "checked");
                    } else if (el.attr("type") == "checkbox") {
                        el.prop("checked", editableFieldsModel[id]);
                    }
                    else if (el.prop("tagName") == "SPAN") {
                        el.text(editableFieldsModel[id]);
                    }
                    else if (el.prop("tagName") == "INPUT" || el.prop("tagName") == "SELECT") {
                        el.val(editableFieldsModel[id]);
                    }
                    else
                    {
                        el.text(editableFieldsModel[id]);
                    }
                }
            }

            setQualifyingBorrowerLockCheckboxesState(editableFieldsModel);
        }

        function lqblog(msg) {
            if (window.console && window.console.log) {
                window.console.log(msg);
            }
        }
        
        function callWebMethod(webMethodName, data, error, success, pageName) {
            if (!pageName) {
                pageName = '../Disclosure/BorrowerClosingCosts.aspx/';
            }
            
            var settings = {
                async: false,
                type: 'POST',
                url: pageName + webMethodName,
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: error,
                success: function (d){ checkWebError(d, success); }
            };

            callWebMethodAsync(settings);
        }

        function EventHandlers() {
            $("#sIsPrintTimeForsGfeEstScAvailTillD").change(function(event) {
                var editable = $(this).prop("checked");
                if (!editable) {
                    $("#sGfeEstScAvailTillD_Time").val("12");
                    $("#sGfeEstScAvailTillD_Time_minute").val("00");
                    $("#sGfeEstScAvailTillD_Time_am").val("AM");
                    $("#sGfeEstScAvailTillDTimeZoneT").val(0);
                }
            });

            $("#sSchedDueD1Lckd, #sGfeEstScAvailTillDLckd, #sIsPrintTimeForsGfeEstScAvailTillD").change(function() {
                ractive.fire('recalc');
            });

            $("#sEstCloseD, #sSchedDueD1").blur(function() {
                ractive.fire('recalc');
            });
            $("#sEstCloseD, #sSchedDueD1").change(function() {
                date_onblur(null, this)
                $(this).blur();
            });

            $("select:disabled").each(function() {
                var el = $(this);
                el.removeProp('disabled');
                el.mousedown(function(event) {
                    event.preventDefault();
                    $(this).focus();
                });
                el.keydown(function(event) {
                    event.preventDefault();
                    $(this).focus();
                });
                el.css("background-color", "lightgrey");
            });

            $(document).on('change', 'input,select', updateDirtyBit);
        }
        
        var loanID;
        
        
        function sLenderPaidBrokerCompFToggle()
        {
            var paymentSource = $("#sOriginatorCompensationPaymentSourceT").find(":checked").val();

            var isGfeTransaction = $("#sGfeIsTPOTransaction").prop("checked");
            
            $("#sLenderPaidBrokerCompFLabel").toggle(paymentSource == <%= AspxTools.HtmlString(E_sOriginatorCompensationPaymentSourceT.LenderPaid.ToString("D")) %> && isGfeTransaction);
            $("#sLenderPaidBrokerCompF").toggle(paymentSource == <%= AspxTools.HtmlString(E_sOriginatorCompensationPaymentSourceT.LenderPaid.ToString("D")) %> && isGfeTransaction);
        }
        
        function _init() 
        {            
            EventHandlers();
            
            sLenderPaidBrokerCompFToggle();

            window.refreshCalculation = function() { ractive.fire('recalc'); };
            
            window.f_saveMe = function() {
                var model = ractive.get();
                getNonRactiveData(model.editableFieldsModel);
                var data = { loanId: ML.sLId, viewModelJson: JSON.stringify(model), editableFieldsJson: JSON.stringify(model.editableFieldsModel) },
                success = false;

                callWebMethod('Save', data, function() {
                    alert('Failed');
                },
            function(m) {
                success = true;
                clearDirty();
                if(parent.info && typeof(parent.info.f_refreshInfo) === 'function')
                {
                    parent.info.f_refreshInfo();
                }

                model = JSON.parse(m.d);
                ractive.set(model);
                setNonRactiveData(model.editableFieldsModel);
            });
                return success;
            }

            window.saveMe = window.f_saveMe;

            var IsReadOnly = $("#IsReadOnly").val() == "true";

            loanID = document.getElementById("loanid").value;
            $("#AmortizationTableFrame").attr("src", "../Forms/AmortizationTable.aspx?loanid=" + loanID);

            $("#AmortizationTableFrame").on('load', function() {

                var contents = $("#AmortizationTableFrame").contents();
                var closeBtn = contents.find("input[value='Close']");
                closeBtn.unbind("click");

                var header = contents.find("h4.page-header");
                header.hide();
                closeBtn.click(closeDialog);
            });

            var datepickerTransition = function(t) {
                if (t.isIntro) {
                    var node = $(t.node);
                    node.change(
            function() {
                $(this).blur();
                ractive.updateModel();
                ractive.fire('recalc');
            });

                    _initMask(node[0]);
                }
            };

            var numericTransition = function(t, recalc) {
                if (t.isIntro) {
                    var node = $(t.node);
                    if (recalc) {
                        node.change(
            function() {
                node.blur();
                ractive.updateModel();
                ractive.fire('recalc');
            });
                    }

                    _initMask(node[0]);
                }
            };

            var ddlTransition = function(t) {

                if (t.isIntro) {
                    var node = $(t.node);
                    var options = $("." + this.get("OptionsId"));

                    if (options.attr("sized")) {
                        node.css("width", options.width() - $(".DDLImg").width() - 2);
                    }
                }
            }
            
            var initMask = function(o) {
                _initMask(o.node, true);
                o.complete();
            };
        
            Ractive.transitions.initMask = initMask;

            Ractive.transitions.datepickerTransition = datepickerTransition;
            Ractive.transitions.numericTransition = numericTransition;
            Ractive.transitions.ddlIntro = ddlTransition;

            function highlightLI(el) {
                el.css("background-color", "yellow");
                el.attr("class", "selected");
            }

            function unhighlightLI(el) {
                el.css("background-color", "");
                el.removeAttr("class");
            }

            var calculationModalContent = Ractive.parse(document.getElementById('CalculationModalContent').innerHTML),
            feePickerModalContent = Ractive.parse(document.getElementById('FeePickerModalContent').innerHTML);
            var subFeeConfirmOverwriteContent = Ractive.parse(document.getElementById('SubFeeConfirmOverwriteContent').innerHTML);

            Modal = Ractive.extend({
                el: document.body,
                append: true,
                template: '#modal',

                init: function() {
                    var self = this, resizeHandler;
                    this.outer = this.find('.modal-outer');
                    this.modal = this.find('.modal');

                    this.on('close', function(event) {
                        this.teardown();
                    });

                    $('window').resize(self.center);

                    this.on('teardown', function() {
                        $('window').unbind('resize', self.center);
                    }, false);

                    this.center();
                },

                center: function() {
                    var outerHeight, modalHeight, verticalSpace;
                    outerHeight = this.outer.clientHeight;
                    modalHeight = this.modal.clientHeight;
                    verticalSpace = (outerHeight - modalHeight) / 2;
                    this.modal.style.top = verticalSpace + 'px';
                }
            });


            var TimeControl = Ractive.extend({
                template: '#TimeControl',
                data: { value: '',
                    id: '',
                    readonly: false
                },
                init: function() {
                    this.on('minuteBlur', function(e) {
                        time_onminuteblur(e.node);
                    });

                    this.on('hourKeyUp', function(e) {
                        time_onhourkeyup(e.node, e.node.id + '_minute', e.original );
                    });

                    this.on('hourBlur', function(e) {
                        time_onhourblur(e.node);
                    });
                }

            });

            var DatePicker = Ractive.extend({
                template: '#DatePicker',
                data: {
                    value: '',
                    id: '',
                    readonly: false

                },
                init: function() {
                    var self = this;

                    this.on('showCalendar', function(e, id) {
                        return displayCalendar(id, function(cal) { 
                            if (cal && cal.sel) {
                                queueRactiveUpdate(ractive, self, cal.sel.value);
                                cal.hide();
                            }
                        });
                    });
                    this.on('domblur', function(event) {
                        var kp = event.keypath;
                        //sadly this blur event fires before the mask.js blur event 
                        //by using setTimeout we queue up the function until after all the other
                        //js code.
                        window.setTimeout(function() {
                            self.updateModel("value", true);
                        });
                    });

                    this.on('updatePicker', function(event)
                    {
                        window.setTimeout(function(){
                            queueRactiveUpdate(ractive, self, event.node.value);
                        }, 0);
                    });
                }
            });
            
            var RactiveCombobox = Ractive.extend({
              template: '#RactiveCombobox',
              data: {
                  value: '',
                  Prefix: '',
                  disabled: '',
                  Field: ''
              },
              init: function() {

                  
                if(<%=AspxTools.JsBool(!IsReadOnly) %>)
                {
                      this.on('showOptions', function(event, i, j, k, OptionsId) {
                          if(!this.get("disabled"))
                          {
                              var options = $("." + OptionsId);
                              options.toggle();

                              var node = $(event.node);
                              if (node.prop('tagName').toLowerCase() == 'input') {
                              
                                  //called from the input
                                  var img = node.parent().find('img');
                                  img.after(options);
                              }
                              else {
                              
                                  //called from the img
                                  node.after(options);
                                  node = node.parent().find("input");
                                  node.focus();
                             }
                         }
                      });

                      this.on('hideDesc', function(event, OptionsId) {
                          var options = $("." + OptionsId);

                          if (canHideDescriptions) {
                              options.hide();
                              $("#OptionsContainer").append(options);
                          }

                      });

                      this.on('keyScroll', function(event){
                      
                        var code = event.original.which; //get the keycode
                        var ul = $(event.node).parent().find('ul'); //get the ul
                        
                        var selectedLI = ul.find(".selected");
                        
                        var hasSelectedLI = selectedLI.length > 0;

                        switch(code)
                        {
                            case 13:
                                if(hasSelectedLI)
                                {
                                    selectedLI.click();
                                }
                            break;
                            case 38: //up
                                if(hasSelectedLI){
                                   var nextLI = selectedLI.prev();
                                   
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                            case 40: //down
                            
                                if(!ul.is(":visible")){
                                   $(event.node).click();
                                }
                                
                                if(hasSelectedLI){
                                
                                   var nextLI = selectedLI.next();
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                        }
                      });

                      this.on('recalc', function(event, success) {

                          ractive.fire('recalc');

                      });
                  }
              }
          });
            
            var RactiveDDL = Ractive.extend({
              template: '#RactiveDDL',
              data: {
                  value: '',
                  Prefix: '',
                  disabled: ''
              },
              init: function() {

                  var input = $(this.el).find(".RactiveDDL");
                  var selectedListItem = $("."+ this.get("Prefix") + "Options li[val='" + this.get("value") + "']");
                  input.val(selectedListItem.text());
                  
                if(<%=AspxTools.JsBool(!IsReadOnly) %>)
                {
                      this.on('showOptions', function(event, i, j, k, OptionsId) {
                          if(!this.get("disabled"))
                          {
                              var options = $("." + OptionsId);
                              options.toggle();

                              currentFeeRow = j;
                              currentSection = i;
                              if(OptionsId == 'PaidBySingleOptions' || (OptionsId == 'PayableOptions' && k == undefined))
                              {
                                k = 0;
                              }
                              currentPayment = k;

                              var node = $(event.node);
                              if (node.prop('tagName').toLowerCase() == 'input') {
                              
                                  //called from the input
                                  var img = node.parent().find('img');
                                  img.after(options);
                              }
                              else {
                              
                                  //called from the img
                                  node.after(options);
                                  node = node.parent().find("input");
                                  node.focus();
                             }
                         }
                      });

                      this.on('hideDesc', function(event, OptionsId) {
                          var options = $("." + OptionsId);

                          if (canHideDescriptions) {
                              options.hide();
                              $("#OptionsContainer").append(options);
                          }

                      });

                      this.on('keyScroll', function(event){
                      
                        var code = event.original.which; //get the keycode
                        var ul = $(event.node).parent().find('ul'); //get the ul
                        
                        var selectedLI = ul.find(".selected");
                        
                        var hasSelectedLI = selectedLI.length > 0;

                        switch(code)
                        {
                            case 13:
                                if(hasSelectedLI)
                                {
                                    selectedLI.click();
                                }
                            break;
                            case 38: //up
                                if(hasSelectedLI){
                                   var nextLI = selectedLI.prev();
                                   
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                            case 40: //down
                            
                                if(!ul.is(":visible")){
                                   $(event.node).click();
                                }
                                
                                if(hasSelectedLI){
                                
                                   var nextLI = selectedLI.next();
                                   if(nextLI.length > 0){
                                        unhighlightLI(selectedLI);
                                        highlightLI(nextLI);
                                        ul.scrollTop(ul.scrollTop() + nextLI.position().top - ul.height()/2);
                                   }
                                }
                                else{
                                        //select the first LI if there is none
                                        highlightLI(ul.find('li').first());
                                }
                            break;
                        }
                      });

                      this.on('recalc', function(event, success) {

                          ractive.fire('recalc');

                      });
                  }
              }
          });            

            var FeePickerModal = Modal.extend({
                partials: {
                    modalContent: feePickerModalContent
                }
            });

            var SubFeeConfirmOverwriteModal = Modal.extend({
                partials: {
                    modalContent: subFeeConfirmOverwriteContent
                }
            });

            var CalculationModal = Modal.extend({
                partials: {
                    modalContent: calculationModalContent
                },

                init: function(options) {
                    // wherever we overwrite methods, such as `init`, we can call the
                    // overwritten method as `this._super`
                    var that = this;

                    var originalData = $.extend(true, {}, that.get());

                    this.on('close', function(e, update) {
                        //this is needed for mask.js

                        if (!update) {
                            $.extend(that.get("fee"), originalData.fee);
                            $.extend(that.get("loan"), originalData.loan);
                        }

                        that.updateModel();
                    });

                    this._super(options);

                }
            });

            ractive = new Ractive({
                // The `el` option can be a node, an ID, or a CSS selector.
                el: 'ClosingCostFeessContainer',
                append: false,

                // We could pass in a string, but for the sake of convenience
                // we're passing the ID of the <script> tag above.
                template: '#ClosingCostTemplate',

                partials: '#ClosingCostFeePaymentTemplate, #ClosingCostSectionFeeTemplate, #Footer',
                components: { ractiveDDL: RactiveDDL, ractiveCombobox: RactiveCombobox, datePicker: DatePicker, timeControl: TimeControl },
                // Here, we're passing in some initial data
                data: ClosingCostData,

                init: function() {

                }
            });
            
        var observer = ractive.observe('SectionList.*.ClosingCostFeeList.*.bene', function(newValue, oldValue, keypath, i, j, k){
            if(oldValue != null && newValue != null && oldValue != newValue)
            {
                var splitArray = keypath.split(".")
                var field = splitArray[splitArray.length - 1];
                
                var prefix = "Beneficiary";
                
                var DDLClass = prefix + "DDL.RactiveDDL" + i + j;
                
                var OptionsClass = prefix + "Options";
                
                $("." + DDLClass).val( $("." + OptionsClass).find("li[val='"+newValue+"']").text());
            }
        });
        
            ractive.on('onContactHover', function(event){
	    var el = $(event.node);
	    el.attr("src", '../../images/contacts_clicked.png');
	});
	
	ractive.on('onContactMouseOut', function(event){
	    var el = $(event.node);
	    el.attr("src", '../../images/contacts.png');
	});
	
	ractive.on('showAgentPicker', function(event, i, j){
	
	   var el = $(event.node);
       var rolodex = new cRolodex();
    
       var type = el.parent().find("input[type='hidden']").val();

       rolodex.chooseFromRolodex(type, ML.sLId, true, false, function(args){

            if (args.OK == true) {
                if(args.Clear == true) {  
                    $("#ClearAgent").dialog({
                        modal: true,
                        height: 150,
                        width: 350,
                        title: "",
                        dialogClass: "LQBDialogBox",
                        resizable: false,
                        buttons: [
                            {text: "OK",
                                click: function(){
                                    // Remove Agent from official contacts
                                    var model = ractive.get();
                                    var data = {
                                        loanId: ML.sLId,
                                        viewModelJson: JSON.stringify(model),
                                        recordId: ractive.get("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id"),
                                    }
                                    
                                    callWebMethod("ClearAgent", 
                                        data,
                                        function(){
                                            alert("Error:  Could not remove agent.");
                                        }, 
                                        //On Success
                                        function(m){
                                            model = JSON.parse(m.d);
                                            ractive.set(model);
                                        });
                                
                                    $(this).dialog("close");
                                }
                            },
                            {text: "Cancel",
                                click: function(){
                                    $(this).dialog("close");
                                }
                            }
                        ]
                    });   
                } else {
                    var id = "";
                    var populateFromRolodex = false;
                    var agentType = args.AgentType;
                    
                    if(args.RecordId != "" && args.RecordId != <%=AspxTools.JsString(Guid.Empty) %> )
                    {
                        //If they're choosing from an agent record, simply set it's record id as the fee's bene_id.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", args.RecordId);
                        //Update paidTo DDL
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                        //Re-enable beneficiary automation.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                        
                        updateDirtyBit();
                        ractive.fire('recalc');
                    }
                    else
                    {
                        if(args.BrokerLevelAgentID != <%=AspxTools.JsString(Guid.Empty) %>)
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there is a BrokerLevelAgentID, populate using the rolodex and that id
                            
                            id=args.BrokerLevelAgentID;
                            populateFromRolodex = true;  
                        }
                        else
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there isn't a BrokerLevelAgentID, populate using the employee info
                            
                            id=args.EmployeeId;
                            populateFromRolodex = false;  
                        }
                        
                        
                        $("#ConfirmAgent").dialog({
                            modal: true,
                            height: 150,
                            width: 350,
                            title: "",
                            dialogClass: "LQBDialogBox",
                            resizable: false,
                            buttons: [
                                {text: "OK",
                                click: function(){
                                    callWebMethod("CreateAgent", 
                                        {loanId: ML.sLId, id: id, populateFromRolodex: populateFromRolodex, agentType: agentType},
                                        function(){alert("Error:  Could not create a new agent.");}, 
                                        //On Success
                                        function(d){
                                            var m = JSON.parse(d.d);
                                            // Get the recordID and set it for the bene_id
                                            recordId = m.RecordId;
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", recordId);
                                            //Update the fee's paidTo DDL
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                                            //Re-enable beneficiary automation.
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                                            
                                            updateDirtyBit();
                                            ractive.fire('recalc');
                                            
                                        });
                                    $(this).dialog("close");
                                    }
                                },
                                {text: "Cancel",
                                click: function(){
                                    $(this).dialog("close");
                                }}
                            ]
                        });
                    }

                }
            }
        });
	});

            ractive.on('domblur', function(event) {
                var kp = event.keypath;
                //sadly this blur event fires before the mask.js blur event 
                //by using setTimeout we queue up the function until after all the other
                //js code.
                window.setTimeout(function() {
                    ractive.updateModel(kp, true);
                });
            });

            ractive.on('delete', function(event, i, j, desc, amt, orgDesc) {
                var d = desc;
                if (!d)
                {
                    d = orgDesc;
                }
                $('#dialog-confirm').find('p').html('Are you sure you would like to remove the following fee? <br />').append(document.createTextNode(d + ' : ' + amt));
                $('#dialog-confirm').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function()
                        {
                            $(this).dialog("close");
                            updateDirtyBit();

                            var model = ractive.get();
                            var sectionList = model.SectionList;
                            sectionList[i].ClosingCostFeeList.splice(j, 1);
                        },
                        "No": function()
                        {
                            $(this).dialog("close");
                        }
                    },
                    closeOnEscape: false,
                    width: "400",
                    draggable: false,
                    resizable: false,
                    dialogClass: "LQBDialogBox"
                });
            });

            ractive.on('updatePaidBy', function(e, i, j, total) {
                var value = e.node.value;
                var obj = e.context;
                if (value == -1)
                {
                   var model = ractive.get(), 
                   fee = model.SectionList[i].ClosingCostFeeList[j];
                   fee.pmts[0].paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
                   
                   // Add new system payment
                   var newPayment = new Object();
                   newPayment.amt = "";
                   newPayment.ent = 0;
                   newPayment.is_fin = false;
                   newPayment.is_system = true;
                   newPayment.made = false;
                   newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
                   newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
                   newPayment.pmt_dt = "";
                   fee.pmts.push(newPayment);
                   
                   updateDirtyBit();
                }
                else if (value == 3)
                {
                    obj.pmts[0].paid_by = value;
                    obj.pmts[0].pmt_at = 1; // Set to 'at closing'
                    ractive.update(e.keypath);
                }
                else
                {
                    obj.pmts[0].paid_by = value;
                    ractive.update(e.keypath);
                }
                
                
                ractive.fire('recalc');
            });

            ractive.on('updatePaidBySplit', function(e)
            {
                var value = e.node.value;
                var obj = e.context;
                if (value == 3)
                {
                    obj.paid_by = value;
                    obj.pmt_at = 1; // Set to 'at closing'
                    ractive.update(e.keypath);
                }
                else
                {
                    obj.paid_by = value;
                    ractive.update(e.keypath);
                }
                
                ractive.fire('recalc');
            });
            
            ractive.on('AddSplitPayment', function(event, i, j) {
                updateDirtyBit();

                var model = ractive.get();
                var sectionList = model.SectionList;
                var fee = sectionList[i].ClosingCostFeeList[j];

                var newPayment = new Object();
                newPayment.amt = "";
                newPayment.ent = 0;
                newPayment.is_fin = false;
                newPayment.is_system = false;
                newPayment.made = false;
                newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
                newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
                newPayment.pmt_dt = "";

                fee.pmts.push(newPayment);
                ractive.fire('recalc');
            });

            ractive.on('updateLenderOrigComp', function(event)
            {
                var model = ractive.get();
                var m = new CalculationModal(
                    {
                        data: {
                            fee: model.StandaloneLenderOrigCompFee.f, 
                            loan: this.get()
                        }
                    });

                m.on('teardown', function() {
                    ractive.fire('recalc');
                    updateDirtyBit();
                });
            });

            ractive.on('updateCalc', function(event, i, j) {
                var model = ractive.get();
                var keypath = 'SectionList';
                var o = this.get(keypath);
                var m = new CalculationModal({ data: { fee: o[i].ClosingCostFeeList[j].f, loan: this.get()} });
                m.on('teardown', function() {
                    ractive.fire('recalc');
                    updateDirtyBit();
                });
            });

            ractive.on('recalc', function(event, success) {
                if (!ractive.get("ByPassBgCalcForGfeAsDefault")) {
                    window.setTimeout(function () {
                        ractive.set('GfeTilAgentRoleT', $("#CFM_m_officialContactList").val());
                        ractive.set('GfeTilIsLocked', $("#CFM_m_rbManualOverride").prop("checked"));

                        var model = ractive.get();
                        getNonRactiveData(model.editableFieldsModel);
                        var startRecalc = Date.now(),
                            data = { loanId: loanID, viewModelJson: JSON.stringify(model), editableFieldsJson: JSON.stringify(model.editableFieldsModel) },
                            fin = function (m) {
                                var load = Date.now();
                                lqblog('Time To Load Data ' + (load - startRecalc) + 'ms.');
                                model = JSON.parse(m.d);
                                var d = Date.now();

                                model.ByPassBgCalcForGfeAsDefault = ractive.get("ByPassBgCalcForGfeAsDefault");
                                model.viewT = ractive.get("viewT");

                                ractive.set(model);

                                setNonRactiveData(model.editableFieldsModel);

                                var c = Date.now();
                                lqblog('Time to set data ' + (c - d) + 'ms.');
                            },
                            error = function (e) {
                                alert('Error');
                            };

                        callWebMethod('CalculateData', data, error, fin);
                    });
                }
            });
            
            function GetPaidByDesc(paidby)
            {
                var paidByDesc = "";
                if(paidby == 1)
                {
                    paidByDesc = "borr pd";
                }
                else if(paidby == 2)
                {
                    paidByDesc = "seller";
                }
                else if(paidby == 3)
                {
                    paidByDesc = "borr fin";
                }
                else if(paidby == 4)
                {
                    paidByDesc = "lender";
                }
                else if(paidby == 5)
                {
                    paidByDesc = "broker";
                }
                else if(paidby == 6)
                {
                    paidByDesc = "other";
                }
                
                return paidByDesc;
            }
            
            ractive.on('removePayment', function(e, i, j, k, amt, paidby, desc, orgDesc) {
                var d = desc;
                if (!d)
                {
                    d = orgDesc;
                }
                var paidByDesc = GetPaidByDesc(paidby);
                
                $('#dialog-confirm').find('p').text('Are you sure you would like to remove the ' + amt + ' ' + paidByDesc + ' split payment for the ' + d + '?');
                $('#dialog-confirm').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function()
                        {
                            $(this).dialog("close");
                            updateDirtyBit();
                            var model = ractive.get();
                            var sectionList = model.SectionList;
                            sectionList[i].ClosingCostFeeList[j].pmts.splice(k, 1);
                            ractive.fire('recalc');
                        },
                        "No": function()
                        {
                            $(this).dialog("close");
                        }
                    },
                    closeOnEscape: false,
                    width: "400",
                    draggable: false,
                    resizable: false,
                    dialogClass: "LQBDialogBox"
                });
            });

            ractive.on('addFee', function(event, i) {
                var model = ractive.get();

                var sectionList = model.SectionList;

                var section = sectionList[i],
                sectionName = section.SectionName,
                data = { loanId: loanID, viewModelJson: JSON.stringify(model), sectionName: sectionName };

                callWebMethod('GetAvailableClosingCostFeeTypes', data,
                 function() {
                     alert('Error');
                 },
                 function(m) {
                     var result = JSON.parse(m.d);
                     var y = new FeePickerModal({ data: result.Section });
                     y.on('selectFee', function(e, si) {
                        // OPM 468071 - Check if selected fee has existing sub fees.
                        var selectedFee = result.Section.FeeListAsBorrowerFees[si];

                        var subFees;
                        for (var idx=0; idx< result.SubFeesBySourceFeeTypeId.length; idx++)
                        {
                            if (result.SubFeesBySourceFeeTypeId[idx].SourceFeeTypeId == selectedFee.typeid)
                            {
                                subFees = result.SubFeesBySourceFeeTypeId[idx].SubFees;
                                break;
                            }
                        }

                        if (typeof(subFees) !== "undefined" && subFees.length > 0) {
                            // Confirm sub fee overwrite.
                            var confirmDlg = new SubFeeConfirmOverwriteModal({ data: {SubFees: subFees, ParentDesc: selectedFee.desc } });
                            confirmDlg.on('subFeeDlgOk', function(e, si) {
                                addSelectedFee(selectedFee, section, i);
                                confirmDlg.teardown();
                                y.teardown();
                                return false;
                            });
                        }
                        else {
                            // No sub fees, just add seleced fee.
                            addSelectedFee(selectedFee, section, i);
                            y.teardown();
                            return false;
                        }

                        return false;
                     });
                 });
            });

            function addSelectedFee (selectedFee, feeSection, sectionIndex) {
                feeSection.ClosingCostFeeList.push(selectedFee);
                updateDirtyBit();
                $(".RactiveDDL" + sectionIndex + (feeSection.ClosingCostFeeList.length - 1)).css("width", $(".OptionsList").width() - $(".DDLImg").width() - 2);
                ractive.fire('recalc');
            }

            ractive.on('showCalendar', function(e, id) {
                return displayCalendar(id);
            });
            var canHideDescriptions = true;
            var currentFeeRow = 0;
            var currentSection = 0;

            ractive.on('onHoverDescriptions', function(event) {
                canHideDescriptions = false;
            });

            ractive.on('onMouseOutDescriptions', function(event) {
                canHideDescriptions = true;
            });

            ractive.on('highlightLI', function(event) {
                var node = $(event.node);
                var allLI = node.parent().find('li');
                unhighlightLI(allLI);
                highlightLI(node);

            });
            
            ractive.on('CanShopChange', function(e, i, j){
                var keypath = 'SectionList.' + i + '.ClosingCostFeeList.' + j + '.did_shop';
                var can_shop = $(e.node).prop('checked');
                if(!can_shop)
                {
                    ractive.set(keypath, false);
                }
            });

            ractive.on('tpClick', function(e, i, j) {
                var model = ractive.get();
                if (!model.SectionList[i].ClosingCostFeeList[j].tp) {
                    model.SectionList[i].ClosingCostFeeList[j].aff = false;
                    ractive.set(model);
                }
            });

            ractive.on('unhighlightLI', function(event) {
                var node = $(event.node);
                unhighlightLI(node);
            });
            
            ractive.on('selectCombobox', function(event){
            var node = $(event.node);
            var text = node.text();
            
            //get the keypath to be updated
            var keypath = node.parent().parent().find("input").attr("field");
            
            ractive.set(keypath, text);
            updateDirtyBit(event.original);
            canHideDescriptions = true;
            
            node.parent().hide();
             $("#OptionsContainer").append(node.parent());
        });


            ractive.on('selectDDL', function(event) {
                var node = $(event.node);
                var text = node.text();

                var td = node.parent().parent();
                
                td.find(".RactiveDDL").val(node.text());
                td.find("input[type='hidden']").val(node.attr('val'));
              
                var value = node.attr('val');

                node.parent().hide();
                $("#OptionsContainer").append(node.parent());
                canHideDescriptions = true;
                
                // Get Agent Id
                var args = new Object();
                args["loanid"] = ML.sLId;
                args["IsArchivePage"] = false;
                args["AgentType"] = value;
                  
                var result = gService.cfpb_utils.call("GetAgentIdByAgentType", args);
                
                if (!result.error) {
                    //Update the fee's Beneficiary ID
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_id", result.value.RecordId);
                    // Update the fee's Beneficiary Description
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_desc", result.value.CompanyName);
                    //Update the fee's paidTo DDL
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene", value);
                    //Re-enable beneficiary automation.
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".disable_bene_auto", false);
                }

                updateDirtyBit(event.original);
                ractive.fire('recalc');
            });

            ractive.on('reselectInput', function(event) {
                //this is called by the UL itself
                $(event.node).parent().find('input').focus();
            });
           
            $wrapper = $("#wrapper");
            $closeIcon = $("#closeIcon");
            $tabLinks = $("#Tabs li");
            $contentDivs = $(".tabContent");
            switchTab("Min");

            $(".RactiveDDL").css("width", $(".BeneficiaryOptions").width());
            $(".OptionsList").css("width", $(".OptionsList").width() + $(".DDLImg").width() + 2);
            
                    
	        $(".RactiveCombobox").css("width", $(".CreditDescOptions").width());
            $(".CreditDescOptions").css("width", $(".CreditDescOptions").width() + $(".DDLImg").width() + 2);

            // Swap the QualifyingBorrower for Details of Transaction
            var $qbContainer = $('.true-container');
            if (ML.IsUlad2019) {
                $qbContainer.attr('class', $qbContainer.attr('class') + ' wrap');
                $('#Dot').html('').append($qbContainer);
                $('#DotLnk a').text('Qualifying Borrower');
                initializeQualifyingBorrower(false, refreshCalculation, true);
            }
            else {
                $qbContainer.remove();
            }

            setNonRactiveData(ractive.get().editableFieldsModel);
        }

        function closeDialog() {
            $("#AmortizationTableDiv").dialog("close");
        }

        
        function showAmortTable() {
            $("#AmortizationTableDiv").dialog({
                width: 630,
                height: 540,
                modal: false,
                title: "Amortization Table",
                resizable: false,
                draggable: false,
                modal: true
            });
        }

        function lockTimeControl(id) {
            var minuteId = id + "_minute";
            var amId = id + "_am";

            $("#" + id).attr("readonly", true);
            $("#" + minuteId).attr("readonly", true);
            $("#" + amId).prop("disabled", true);

            $("#" + id).css("background-color", "lightgrey");
            $("#" + minuteId).css("background-color", "lightgrey");
            $("#" + amId).css("background-color", "lightgrey");
        }

        function lockElement(id) {
            var test = $("#" + id + "Lckd");

            var elem = $("#" + id);
            if (id == "sNMLSPeriodForDaysDelinquentT") {
                id = "sNMLSPeriodForDaysDelinquentLckd";
            }
            else if (id == "sNMLSServicingIntentT") {
                id = "sNMLSServicingIntentLckd";
            }
            else if (id.indexOf("_Time") != -1) {
                id = "sIsPrintTimeForsGfeEstScAvailTillD";
            }
            else {
                id = id + "Lckd";
            }
            var isChecked = $("#" + id).prop("checked");
            if (elem.prop("tagName") == "SELECT") {
                elem.prop("disabled", !isChecked);

                if (isChecked) {
                    elem.css("background-color", "");
                }
                else {
                    elem.css("background-color", "lightgrey");
                }
            }
            else if (elem.attr("id").indexOf("_Time") != -1) {
                var minuteId = elem.attr("id") + "_minute";
                var amId = elem.attr("id") + "_am";

                elem.attr("readonly", !isChecked);
                $("#" + minuteId).attr("readonly", !isChecked);
                $("#" + amId).prop("disabled", !isChecked);

                if (isChecked) {
                    elem.css("background-color", "");
                    $("#"+minuteId).css("background-color", "");
                    $("#"+amId).css("background-color", "");
                }
                else {
                    elem.css("background-color", "lightgrey");
                    $("#"+minuteId).css("background-color", "lightgrey");
                    $("#" + amId).css("background-color", "lightgrey");
                }

                if (elem.attr("id") == "sGfeEstScAvailTillD_Time") {
                    id = "sGfeEstScAvailTillDTimeZoneT";
                    var el = $("#" + id);
                    
                     if (isChecked) {
                         el.css("background-color", "");
                         el.unbind();
                    }
                    else {
                        el.removeProp('disabled');
                        el.mousedown(function(event) {
                            event.preventDefault();
                            $(this).focus();
                        });
                        el.keydown(function(event) {
                            event.preventDefault();
                            $(this).focus();
                        });
                        el.css("background-color", "lightgrey");
                    }
                    
                }
                
            }
            else {
                elem.prop("readonly", !isChecked);
            }
        }
        
        /*<------------Footer Script------------------>*/
    var $wrapper;
    var $closeIcon;
    var $tabLinks;
    var $contentDivs;

    function switchTab(tabId) {
        $closeIcon.hide();
        $contentDivs.hide();
        $tabLinks.removeAttr('class');

        $tabLinks.filter("#" + tabId + "Lnk").addClass("selected");

        $div = $contentDivs.filter("#" + tabId);
        if ($div.length != 0) {   // need check because minimize has no content
            $div.show();
            $closeIcon.show();
        }

        $(window).trigger('footer/switchTab')
    }

    /*<-------------Details of Transaction Script---------------->*/

    function lockFieldByElement(cb, tb) {
        tb.readOnly = !cb.checked;

        if (!cb.checked) 
            tb.style.backgroundColor = "lightgrey";
        else
            tb.style.backgroundColor = "";
    }
    </script>
    
    <script id="AddressTemplate" type="text/ractive">
                            {{#addresses}}
                                <br/>
                                {{aBNm}} {{#if aBHasSpouse && sameAddr}} and {{aCNm}} {{/if}} <br/>
                                {{aBAddrMail}} <br/>
                                {{aBCityMail}}, {{aBStateMail}} {{aBZipMail}} <br/>
                                
                                {{#if aBHasSpouse && !sameAddr}}
                                    <br/>
                                    {{aCNm}} {{#if aCHasSpouse && sameAddr}} and {{aCNm}} {{/if}} <br/>
                                    {{aCAddrMail}} <br/>
                                    {{aCCityMail}}, {{aCStateMail}} {{aCZipMail}} <br/>
                                {{/if}}
                            {{/addresses}}
    </script>
        
    <script id="ExpensesTemplate" type="text/ractive">
        <table id="ExpensesTable">
            <thead>
                <td class="GridHeader">This estimate includes:</td>
                <td class="GridHeader">Amount</td>
                <td class="GridHeader">In Escrow?</td>
            </thead>
            <tbody>
                {{#HousingExpenses}}
                    <tr>
                        <td>{{expenseName}}</td>
                        <td>{{amount}}</td>
                        <td>{{inEscrow? 'Yes':'No'}}</td>
                    </tr>
                {{/HousingExpenses}}
            </tbody>
        </table>
    </script>
    
    <script id="TimeControl" type="text/ractive">
            Time: <input class="TimeInput" type="text" id="{{id + ''}}" value="{{value_hour}}" readonly="{{readonly}}" on-blur="hourBlur" on-keyup="hourKeyUp" maxlength="2"/>
            : <input class="TimeInput" type="text" id="{{id + '_minute'}}" value="{{value_min}}" readonly="{{readonly}}" on-blur="minuteBlur" maxlength="2"/>
            <select id="{{id + '_am'}}"  value="{{value_am}}" readonly="{{readonly}}">
              <option value="AM">AM</option>
              <option value="PM">PM</option>
            </select>
    </script>
             
    <script id="DatePicker" type="text/ractive">
        <input type="text" class="mask date" readonly="{{readonly}}"  preset="date" id="{{id}}" intro="initMask" on-blur="updatePicker"   value="{{value}}" />
        <a on-click="displayCalendar" href="#" on-click="showCalendar:{{id}}" >
            <img title="Open calendar" src="[[VRoot]]/images/pdate.gif" border="0">
        </a>
    </script>
    <script id='RactiveCombobox' type='text/ractive'>
            <div class="RactiveDDLContainer">
                <input on-keydown='keyScroll' field="{{Field}}" value="{{value}}" readonly='{{disabled}}' {{IsArchive || IsReadonly?"": "NotEditable"}} class="RactiveCombobox {{'RactiveCombobox'+i+j}} {{Prefix}}Combobox" type="text" on-blur="hideDesc: '{{Prefix}}Options'"  /><img class="DDLImg" src= "../../images/IEArrow.png" on-click="showOptions: {{i}},{{j}},{{k}}, '{{Prefix}}Options'" >
            </div>
   </script>
    <script id='RactiveDDL' type='text/ractive'>
            <div class="RactiveDDLContainer">
                <input on-keydown='keyScroll' disabled='{{disabled}}' {{IsArchive || IsReadonly?"": "NotEditable"}} class="{{disabled? "Disabled": ""}} RactiveDDL {{'RactiveDDL'+i+j}} {{Prefix}}DDL" readonly type="text" on-click="showOptions: {{i}},{{j}},{{k}},'{{Prefix}}Options'"  on-blur="hideDesc: '{{Prefix}}Options'"  /><img class="DDLImg" src= "../../images/IEArrow.png" on-click="showOptions: {{i}},{{j}},{{k}}, '{{Prefix}}Options'" >
                <input type="hidden" value="{{value}}" />
            </div>
    </script>
    
    <script id="ClosingCostSectionFeeTemplate" type="text/ractive">
                        <tr class="{{ j % 2 == 0 ? "GridItem" : "GridAlternatingItem"}}">
                            <td class="FieldLabel"> 
                                <input disabled="{{sIsRequireFeesFromDropDown}}" type="checkbox" checked="{{is_title}}" /> 
                            </td>
                            <td>
                                <input type="text" value="{{desc}}" readonly="{{sIsRequireFeesFromDropDown || f.t === 20 || f.t === 18}}" />
                                {{#if desc != org_desc}}
                                    <div >Type: {{org_desc}}</div>
                                {{/if}}
                            </td>
                            <td>
                                <input type="checkbox" checked="{{is_optional}}" disabled="{{sIsRequireFeesFromDropDown || SectionName.indexOf("H - ") == -1}}" />
                            </td>
                            <td>
                                <input type="checkbox" on-change="recalc" checked="{{apr}}" disabled="{{sIsRequireFeesFromDropDown}}" /> 
                            </td>
                            <td>
                               <input type="checkbox" checked="{{fha}}" disabled="{{sIsRequireFeesFromDropDown}}" />
                            </td>
                            <td>
                                <ractiveDDL Prefix="Beneficiary" value="{{bene}}" /><img class="ContactImg" on-mouseover="onContactHover" on-mouseout="onContactMouseOut" on-click="showAgentPicker : {{i}}, {{j}}" src="../../images/contacts.png">
			                    {{#if bene_desc || bene_id != <%=AspxTools.JsString(Guid.Empty) %>}}
			                        <div> Company: {{bene_desc}} </div>
			                    {{/if}}
                            </td>
                            {{#if CanReadDFLP}}
                                <td>
                                   <input type="checkbox" disabled="{{!CanSetDFLP}}" checked="{{dflp}}" />
                                </td>
                            {{/if}}
                            <td>
                                <input type="checkbox" disabled = "{{!sIsManuallySetThirdPartyAffiliateProps && DisableTPAffIfHasContact && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>')}}" checked="{{tp}}"  on-click="tpClick:{{i}},{{j}}" /> 
                            </td>
                            <td>
                               <input type="checkbox" checked="{{aff}}"  disabled="{{!tp || (!sIsManuallySetThirdPartyAffiliateProps && DisableTPAffIfHasContact && (bene_id != '<%=AspxTools.HtmlString(Guid.Empty) %>'))}}" /> 
                            </td>
                            <td>
                                <input type="checkbox" checked="{{can_shop}}" on-change="CanShopChange: {{i}}, {{j}}" disabled="{{sIsRequireFeesFromDropDown || disc_sect === 2 || disc_sect === 3}}" /> 
                            </td>
                            <td>
                                <input type="checkbox" disabled="{{!can_shop}}" checked="{{did_shop}}" />
                            </td>
                            <td style="text-align: right;">
                                {{#if SectionName.indexOf("G - ") == -1 && f.t !== 0 && f.t !== 4 && f.t !== 5 && f.t !== 14 && f.t !== 17 && f.t !== 18 && f.t !== 19 && f.t !== 20 && f.t !== 21}}
                                <input type="button" value="C" on-click="updateCalc:{{i}},{{j}}"/>
                                {{/if}}
                            </td>
                            <td>
                                {{#if f.t === 3}} 
                                    <input type="text" value="{{f.base}}" class="mask money" preset="money" intro="numericTransition: true" />
                                {{else}}
                                    <input type="text" value="{{total}}" class="money" preset="money"  readonly="true" />
                                {{/if}}
                            </td>
                            <td>
                                  {{#if (pmts && pmts.length == 1)}}
                                    {{#if typeid == <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId) %>}}
                                        <select value="{{pmts[0].paid_by}}" on-change="updatePaidBy:{{i}},{{j}},{{total}}" disabled="true" >
                                            <option value="-1">--split--</option>
                                            <option value="1">borr pd</option>
                                            <option value="2">seller</option>
                                            <option value="3">borr fin</option>
                                            <option value="4">lender</option>
                                            <option value="5">broker</option>
                                        </select>
                                    {{else}}
                                        <select value="{{pmts[0].paid_by}}" on-change="updatePaidBy:{{i}},{{j}},{{total}}" >
                                            {{#if sClosingCostFeeVersionT == 2 && (f.t != 4 && f.t != 21)}}
                                            <option value="-1">--split--</option>
                                            {{/if}}
                                            <option value="1">borr pd</option>
                                            <option value="2">seller</option>
                                            <option value="3">borr fin</option>
                                            {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                                                <option value="4">lender</option>
                                            {{/if }}
                                            <option value="5">broker</option>
                                        </select>
                                    {{/if}}
                                {{else}}
                                    &nbsp;
                                {{/if}}
                            </td>
                            <td>
                                {{#if (pmts && pmts.length == 1)}} 
                                    <select disabled="{{pmts[0].paid_by == 3}}" value="{{pmts[0].pmt_at}}" on-change="recalc">
                                        <option value="1">at closing</option>
                                        <option value="2">outside of closing</option>                
                                    </selecT>
                                {{else}}
                                &nbsp;
                                {{/if}}
                                
                            </td>
                            <td>
                                {{#if pmts && pmts.length == 1}} 
                                  <datePicker readonly="{{pmts[0].pmt_at == 1}}" value="{{pmts[0].pmt_dt}}" id="d{{i}}{{j}}" />
                                 {{else}}
                                    &nbsp;
                                 {{/if}}
                            </td>
                            <td> 
                                {{#if f.t !== 17 && f.t !== 4 && f.t !== 5 && f.t !== 21 && SectionName.indexOf("G - ") == -1 && typeid !== 'f17c6573-e1bb-447d-ba89-31183be07be8' && f.t !== 20 && (typeid !== <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId) %> || total == '$0.00')}}
                                    {{#if f.t === 3}} 
                                        <input type="button" value=" - " on-click="delete:{{i}},{{j}},{{desc}},{{f.base}},{{org_desc}}"/>
                                    {{else}}
                                        <input type="button" value=" - " on-click="delete:{{i}},{{j}},{{desc}},{{total}},{{org_desc}}"/>
                                    {{/if}}
                                {{/if}}
                            </td>
                        </tr>
                        {{#if pmts.length > 1}} 
                        {{#pmts:k}}
                            {{>ClosingCostFeePaymentTemplate}}
                        {{/pmts}}
                        {{/if}}
    </script>
    
    <script id="ClosingCostFeePaymentTemplate" type="text/ractive">
                        <tr>
                            <td colspan="10">
                                &nbsp;
                            </td>
                            <td>
                                {{#if (is_system && !mipFinancedPmt && !cashPdPmt)}}
                                    <input type="button" value="+" on-click="AddSplitPayment:{{i}},{{j}}" />
                                {{/if}}
                            </td>
                            <td>
                                <input readonly="{{is_system || cashPdPmt || mipFinancedPmt}}" style="text-align:right" type="text" value="{{amt}}" class="mask money" preset="money" intro="numericTransition: true"/>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <select value="{{paid_by}}" disabled="{{is_system || mipFinancedPmt}}" on-change="updatePaidBySplit" >
                                    <option value="1">borr pd</option>
                                    <option value="2">seller</option>
                                    {{#if !cashPdPmt}}
                                    <option value="3">borr fin</option>
                                    {{/if}}
                                    {{#if typeid != <%= AspxTools.JsString(DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId) %> }}
                                        <option value="4">lender</option>
                                    {{/if }}
                                    <option value="5">broker</option>
                                </select>
                            </td>
                            <td>
                                <select value="{{pmt_at}}" disabled="{{is_system || paid_by == 3 || mipFinancedPmt}}" on-change="recalc">
                                        <option value="1">at closing</option>
                                        <option value="2">outside of closing</option>
                                </select>
                            </td>
                            <td>
                                <input type="text" preset="date" class="mask date" id="d{{i}}{{j}}{{k}}" intro="datepickerTransition" recalc value="{{pmt_dt}}" readonly="{{is_system || pmt_at == 1}}"/>&nbsp;
                                <a on-click="displayCalendar" href="#" on-click="showCalendar:d{{i}}{{j}}{{k}}" >
                                <img title="Open calendar" src="[[VRoot]]/images/pdate.gif" border="0" />
                                </a>
                            </td>
                            <td>
                                {{#if !is_system && !cashPdPmt && !mipFinancedPmt}}
                                    <input type="button" value=" - " on-click="removePayment:{{i}},{{j}},{{k}},{{amt}},{{paid_by}},{{desc}},{{org_desc}}"/>
                                {{/if}}
                            </td>
                        </tr>
    </script>
    
    <script id="ClosingCostTemplate" type="text/ractive">
        {{#if sOriginatorCompensationPaymentSourceT == 2 && sDisclosureRegulationT == 2 && sGfeIsTPOTransaction}}
        <div class="div-main LenderTypeEstimate" id="StandaloneLenderOrigCompFee">
            <div class="FormTableHeader div-title">Lender-Paid Originator Compensation</div>
            <table border="0" style="white-space: nowrap; padding-left: 5px;" cellpadding="2" cellspacing="0" class="table-closingcost">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description/Memo</th>
                        <th>Optional</th>
                        <th>APR</th>
                        <th>FHA</th>
                        <th>Paid to</th>
                        {{#if CanReadDFLP}}
                            <th>DFLP</th>
                        {{/if}}
                        <th>TP</th>
                        <th>AFF</th>
                        <th>Can<br />Shop</th>
                        <th>Did<br />Shop</th>
                        <th>&nbsp;</th>
                        <th>Amount</th>
                        <th>Paid by</th>
                        <th>Payable</th>
                        <th>Date paid</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="GridItem">
                        <td class="FieldLabel">
                            <input disabled="true" type="checkbox" checked="{{StandaloneLenderOrigCompFee.is_title}}" id="StandaloneLenderOrigCompFee_isTitle" />
                        </td>
                        <td>
                            <input type="text" readonly="{{sIsRequireFeesFromDropDown}}" value="{{StandaloneLenderOrigCompFee.desc}}" id="StandaloneLenderOrigCompFee_description" />
                            {{#if StandaloneLenderOrigCompFee.desc != StandaloneLenderOrigCompFee.org_desc}}
                                <div >Type: {{StandaloneLenderOrigCompFee.org_desc}}</div>
                            {{/if}}
                        </td>
                        <td>
                            <input type="checkbox" checked="{{StandaloneLenderOrigCompFee.is_optional}}" disabled="true" id="StandaloneLenderOrigCompFee_isOptional" />
                        </td>
                        <td>
                            <input type="checkbox" checked="{{StandaloneLenderOrigCompFee.apr}}" disabled="{{sIsRequireFeesFromDropDown}}" id="StandaloneLenderOrigCompFee_isApr" />
                        </td>
                        <td>
                            <input type="checkbox" checked="{{StandaloneLenderOrigCompFee.fha}}" disabled="{{sIsRequireFeesFromDropDown}}" id="StandaloneLenderOrigCompFee_isFha" />
                        </td>
                        <td>
                            <ractiveDDL Prefix='Beneficiary' value ='{{StandaloneLenderOrigCompFee.bene}}' disabled="true"/>
                            <img class="ContactImg" src="../../images/contacts.png">
			                {{#if StandaloneLenderOrigCompFee.bene_desc || StandaloneLenderOrigCompFee.bene_id != <%=AspxTools.JsString(Guid.Empty) %>}}
			                    <div> Company: {{StandaloneLenderOrigCompFee.bene_desc}} </div>
			                {{/if}}
                        </td>
                        {{#if CanReadDFLP}}
                        <td>
                            <input type="checkbox" disabled="{{!CanSetDFLP}}" checked="{{StandaloneLenderOrigCompFee.dflp}}" id="StandaloneLenderOrigCompFee_DFLP" />
                        </td>
                        {{/if}}
                        <td>
                            <input type="checkbox" disabled="true" checked="{{StandaloneLenderOrigCompFee.tp}}" id="StandaloneLenderOrigCompFee_isThirdParty" />
                        </td>
                        <td>
                            <input type="checkbox" disabled="true" checked="{{StandaloneLenderOrigCompFee.aff}}" id="StandaloneLenderOrigCompFee_isAffiliate" />
                        </td>
                        <td>
                            <input type="checkbox" disabled="true" checked="{{StandaloneLenderOrigCompFee.can_shop}}" id="StandaloneLenderOrigCompFee_canShop" />
                        </td>
                        <td>
                            <input type="checkbox" disabled="true" checked="{{StandaloneLenderOrigCompFee.did_shop}}" id="StandaloneLenderOrigCompFee_didShop" />
                        </td>
                        <td style="text-align: right;">
                            {{#if StandaloneLenderOrigCompFee.f.t === 1 || StandaloneLenderOrigCompFee.f.t === 2 || StandaloneLenderOrigCompFee.f.t === 3}}
                            <input type="button" value="C" on-click="updateLenderOrigComp" />
                            {{else}}
                            &nbsp;
                            {{/if}}
                        </td>
                        <td>
                            <input type="text" value="{{StandaloneLenderOrigCompFee.total}}" id="StandaloneLenderOrigCompFee_amount" class="mask money" preset="money" readonly="true" />
                        </td>
                        <td>
                            <select value="{{StandaloneLenderOrigCompFee.pmts[0].paid_by}}" id="StandaloneLenderOrigCompFee_paidBy" disabled="true" >
                            <option value="1">borr pd</option>
                            <option value="2">seller</option>
                            <option value="3">borr fin</option>
                            <option value="4">lender</option>
                            <option value="5">broker</option>
                            </select>
                        </td>
                        <td>
                            <select value="{{StandaloneLenderOrigCompFee.pmts[0].pmt_at}}" id="StandaloneLenderOrigCompFee_gfeTiming" on-change="recalc">
                                <option value="1">at closing</option>
                                <option value="2">outside of closing</option>
                            </select>
                        </td>
                        <td>
                              <datePicker readonly="{{StandaloneLenderOrigCompFee.pmts[0].pmt_at == 1}}" value="{{StandaloneLenderOrigCompFee.pmts[0].pmt_dt}}" id="StandaloneLenderOrigCompFee_paymentDate" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br />
        {{/if}}
        <div class="GridHeader">Closing Cost Details</div>
        <div id="sec-closingcost8" class="div-main LenderTypeEstimate">
                {{#SectionList:i}}  
                <div class="FormTableHeader div-title">{{SectionName}}
                <span style="float:right; display:inline-block; padding-right: 5px;">Total: {{TotalAmount}}</span>
                </div>
                <table border="0" style="white-space: nowrap; padding-left: 5px;" cellpadding="2" cellspacing="0" class="table-closingcost" >
                {{#if ClosingCostFeeList.length > 0}}
                <thead>
                    <tr>
                        <th>
                            Title
                        </th>
                        <th>
                            Description/Memo 
                        </th>
                        <th>
                            Optional
                        </th>
                        <th>
                            APR
                        </th>
                        <th>
                            FHA
                        </th>
                        <th>
                            Paid to
                        </th>
                        {{#if CanReadDFLP}}
                            <th>
                                DFLP
                            </th>
                        {{/if}}
                        <th>
                            TP
                        </th>
                        <th>
                            AFF
                        </th>
                        <th>
                            Can<br/>Shop
                        </th>
                        <th>
                            Did<br/>Shop
                        </th>
                        <th>&nbsp;</th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Paid by
                        </th>
                        <th>
                            Payable
                        </th>
                        <th>
                            Date paid
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                {{/if}}
                <tbody>
                    {{#ClosingCostFeeList:j}}
                        {{>ClosingCostSectionFeeTemplate}}
                    {{/#ClosingCostFeeList}}
                </tbody>
                {{#if SectionName.indexOf("G - ") == -1}}
                <tfooter>
                    <tr>
                        <td>
                            <div class="div-image"><input type="button" value="Add" on-click="addFee:{{i}}"/></div>
                        </td>
                    </tr>
                </tfooter>
                {{/if}}
                    </table>
            {{/SectionList}}
        </div>
        <div ID="OptionsContainer" >
            <asp:Repeater runat="server" ID="BeneficiaryRepeater">
                <HeaderTemplate>
                    <ul class="BeneficiaryOptions OptionsList" tabindex="1" on-focus="reselectInput" on-blur="hideDesc" on-mouseenter="onHoverDescriptions" on-mouseleave="onMouseOutDescriptions">
                </HeaderTemplate>
                <ItemTemplate>
                    <li on-mouseenter="highlightLI" on-mouseleave="unhighlightLI" on-click="selectDDL" val="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"> <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></li>
                </ItemTemplate>
                <FooterTemplate>
                </ul>
                </FooterTemplate>
                </asp:Repeater>
                      
                <asp:Repeater runat="server" ID="CreditDescRepeater">
                <HeaderTemplate>
                    <ul class="CreditDescOptions OptionsList" tabindex="1" on-focus="reselectInput" on-blur="hideDesc" on-mouseenter="onHoverDescriptions" on-mouseleave="onMouseOutDescriptions">
                </HeaderTemplate>
                <ItemTemplate>
                    <li on-mouseenter="highlightLI" on-mouseleave="unhighlightLI" on-click="selectCombobox" val="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"> <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></li>
                </ItemTemplate>
                <FooterTemplate>
                </ul>
                </FooterTemplate>
                </asp:Repeater>
        </div>
    {{> Footer}}
    </script>
    
    <script id="CalculationModalContent" type="text/ractive">
 <p>
    {{#if fee.t == 0 || fee.t == 4 || fee.t == 5 || fee.t == 17 || fee.t == 18}} 
        Not Supported
    
    {{/if}}
    {{#if fee.t == 1 || fee.t == 2 || fee.t == 3}}
        <p>
            <select value="{{fee.t}}">
                <option value="3">Flat Amount</option>
                <option value="1">Full</option>
                <option value="2">Percent Only</option>
            </select>
        </p>
    {{/if}}  
    {{#if fee.t == 1 || fee.t == 8}}
    <label>Percent <input type="text" value="{{fee.p}}" preset="percent" intro="initMask" class="money mask"/> </label>  of  
    <asp:Repeater runat="server" ID="PercentRepeater">
                <HeaderTemplate>
                    <select NotEditable="true" value="{{fee.pt}}" >
                </HeaderTemplate>
                <ItemTemplate>
                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                </ItemTemplate>
                <FooterTemplate>
                </select>
                </FooterTemplate>
              </asp:Repeater>
                + <input type="text" value="{{fee.base}}" preset="money" intro="initMask" class="money mask" /> 
    {{/if}}
    {{#if fee.t == 2}} 
        <label>Percent <input type="text" class="mask"  value="{{fee.p}}" preset="percent" intro="initMask" /> </label>  of  
        <asp:Repeater runat="server" ID="PercentRepeater2">
                <HeaderTemplate>
                    <select NotEditable="true" value="{{fee.pt}}" >
                </HeaderTemplate>
                <ItemTemplate>
                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                </ItemTemplate>
                <FooterTemplate>
                </select>
                </FooterTemplate>
              </asp:Repeater> 
    {{/if}}
    {{#if fee.t == 3}} 
        <input type="text" value="{{fee.base}}" preset="money" class="mask money"  intro="initMask" /> 
    {{/if}}
    {{#if fee.t == 6}}
        <input type="text" value="{{fee.period}}" intro="initMask" preset="numeric" style="width: 40px;" /> period
    {{/if}}
    {{#if fee.t == 7}}
        <input on-change="recalc" type="checkbox" checked="{{loan.sIPiaDyLckd}}"> Lock
        <input type="text" value="{{loan.sIPiaDy}}" readonly="{{!loan.sIPiaDyLckd}}" class="mask money" preset="numeric" intro="initMask" /> days @
        <input on-change="recalc" type="checkbox" checked="{{loan.sIPerDayLckd}}" /> Lock
        <input type="text" value="{{loan.sIPerDay}}" readonly="{{!loan.sIPerDayLckd}}" decimalDigits="6" class="mask money" preset="money" intro="initMask" /> per day
    {{/if}}
    {{#if fee.t == 9 || fee.t == 10}}
        <input type="text" preset="numeric" value="{{fee.period}}" intro="initMask" /> months @ <input type="text" value="{{fee.base}}" class="mask money" preset="money" intro="initMask" readonly="true" /> / month
    {{/if}}
    {{#if fee.t == 11}}
        <input type="text" preset="numeric" value="{{fee.period}}" intro="initMask" readonly="true" /> periods
    {{/if}}
    {{#if fee.t == 12 || fee.t == 13 || fee.t == 15 || fee.t == 16}}
        <input type="text" preset="numeric" value="{{fee.period}}" intro="initMask" /> months @ <input type="text" readonly="true" value="{{fee.base}}" class="mask money" preset="money" intro="initMask" /> / month
    {{/if}}
  </p>
  
  <p>
    <input type="button" value=" OK " on-click="close: true"/> <input type="button" value=" Cancel " on-click="close: false"/> 
  </p>
  </script>
  
  <script id="FeePickerModalContent" type="text/ractive">
    <h3>Available Fees for {{SectionName}}</h3>
    <div>
        <ul>
        {{#FeeListAsBorrowerFees:i}} 
            <li><a href="#" on-click="selectFee:{{i}}">{{desc}}</a></li>
        {{/FeeListAsBorrowerFees}}
        {{^FeeListAsBorrowerFees}}
            <li>No Available Fees</li>
        {{/FeeListAsBorrowerFees}}
        </ul>
        <input type="button" value=" Cancel " on-click="close"/> 
    </div>
  </script>

<script id="SubFeeConfirmOverwriteContent" type="text/ractive">
    <div class="SubFeeConfirmOverwriteContent">
        The following {{#if SubFees.length > 1}}fees{{else}}fee{{/if}}:

        <ul>
            {{#SubFees:i}} 
                <li>{{desc}}</li>
            {{/SubFees}}
        </ul>

        {{#if SubFees.length > 1}}are{{else}}is{{/if}} considered to be the same fee type as the "{{ParentDesc}}"{{#if ParentDesc.substr(ParentDesc.length-3) !== "fee" }}&nbsp;fee{{/if}}.
        Adding the "{{ParentDesc}}"{{#if ParentDesc.substr(ParentDesc.length-3) !== "fee" }}&nbsp;fee{{/if}} will cause the current {{#if SubFees.length > 1}}fees{{else}}fee{{/if}}
        to be removed. Would you like to proceed?
         
        <br>
        <br>

        <div class="SubFeeConfirmOverwriteContentBtns">
            <input type="button" value=" OK " on-click="subFeeDlgOk:{{i}}"/> 
            <input type="button" value=" Cancel " on-click="close"/> 
        </div>
    </div>
  </script>
  
  <script id="modal" type="text/ractive">
  <div class='modal-background'><div class='modal-outer'><div class='modal'>{{>modalContent}}</div></div></div>
  </script>
  
  <script id="Footer" type="text/ractive">
        <div id="footerWrapper">
            <div id="footer">
                <ul id="Tabs" class="tabnav">
                  <li id="LenderCreditsLnk">
                    <a href="#LenderCreditsLnk" onclick="switchTab('LenderCredits');">Lender Credits</a>
                  </li>
                  <li id="PaidTotalsLnk"><a href="#PaidTotalsLnk" onclick="switchTab('PaidTotals');">Lender and Seller paid totals</a></li>
                  <li id="DotLnk"><a href="#DotLnk" onclick="switchTab('Dot');">Details of Transaction</a></li>
                  <li id="MinLnk"><a href="#MinLnk" onclick="switchTab('Min');">Minimize</a></li>
                </ul>

                <div id="closeIcon">
                    <a href="#MinLnk" onclick="switchTab('Min');">&times;</a>
                </div>

                <div id="LenderCredits" class="tabContent">
                    <table>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td class="FieldLabel">
                                            Credit/charge calculation method
                                        </td>
                                        <td style="text-align: right;">
                                            <asp:Repeater runat="server" ID="sLenderCreditCalculationMethodTRep">
                                                <HeaderTemplate>
                                                    <select on-change="recalc" on-blur="domblur" id="sLenderCreditCalculationMethodT" value="{{sLenderCreditCalculationMethodT}}">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                        <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>    
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {{#if sLenderCreditCalculationMethodT == 0}}
                                    <table>
                                        <tr>
                                            <td class="FieldLabel">
                                                Final price
                                            </td>
                                            <td style="text-align: right;">
                                                <input readonly="true" type="text" intro="initMask" value="{{sBrokerLockOriginatorPriceBrokComp1PcPrice}}" id="sBrokerLockOriginatorPriceBrokComp1PcPrice" preset="percent" />
                                            </td>
                                            <td style="text-align: right;">
                                                <input readonly="true" type="text" intro="initMask" value="{{sBrokerLockOriginatorPriceBrokComp1PcAmt}}" id="sBrokerLockOriginatorPriceBrokComp1PcAmt" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">
                                                Initial credit (-)/charge (+)
                                            </td>
                                            <td>
                                                <input type="text" intro="initMask" value="{{sLDiscntPc}}" id="sLDiscntPc" preset="percent" on-change="recalc" />
                                                of
                                                <asp:Repeater runat="server" ID="sLDiscntBaseTRep">
                                                    <HeaderTemplate>
                                                        <select on-change="recalc" on-blur="domblur" id="sLDiscntBaseT" value="{{sLDiscntBaseT}}">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </select>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                +
                                                <input type="text" intro="initMask" value="{{sLDiscntFMb}}" id="sLDiscntFMb" preset="money" on-change="recalc" />
                                            </td>
                                            <td>
                                                <input readonly="true" type="text" intro="initMask" value="{{sLDiscnt}}" id="sLDiscnt" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">
                                                Credit for lender paid fees
                                            </td>
                                            <td style="text-align: right;">
                                                <asp:Repeater runat="server" ID="sGfeCreditLenderPaidItemTRep">
                                                    <HeaderTemplate>
                                                        <select on-change="recalc" on-blur="domblur" id="sGfeCreditLenderPaidItemT" value="{{sGfeCreditLenderPaidItemT}}">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </select>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                            <td>
                                                <input readonly="true" type="text" intro="initMask" value="{{sGfeCreditLenderPaidItemF}}" id="sGfeCreditLenderPaidItemF" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">
                                                General lender credit
                                            </td>
                                            <td></td>
                                            <td>
                                                <input readonly="true" type="text" intro="initMask" value="{{sGfeLenderCreditF}}" id="sGfeLenderCreditF" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">
                                                Additional credits at closing
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">
                                                Lender paid items not included in initial credit
                                            </td>
                                            <td></td>
                                            <td>
                                                <input readonly="true" type="text" intro="initMask" value="{{sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt}}" id="sLenderPaidItemNotIncludedInInitialDisclosureAmtAsChargeAmt" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" intro="initMask" value="{{sLenderCustomCredit1Description}}" id="sLenderCustomCredit1Description" />
                                            </td>
                                            <td style="text-align: right;">
                                                <input type="text" intro="initMask" value="{{sLenderCustomCredit1Amount}}" id="sLenderCustomCredit1Amount" preset="money" on-change="recalc" />
                                            </td>
                                            <td>
                                                <input readonly="true" type="text" intro="initMask" value="{{sLenderCustomCredit1AmountAsCharge}}" id="sLenderCustomCredit1AmountAsCharge" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" intro="initMask" value="{{sLenderCustomCredit2Description}}" id="sLenderCustomCredit2Description" />
                                            </td>
                                            <td style="text-align: right;">
                                                <input type="text" intro="initMask" value="{{sLenderCustomCredit2Amount}}" id="sLenderCustomCredit2Amount" preset="money" on-change="recalc" />
                                            </td>
                                            <td>
                                                <input readonly="true" type="text" intro="initMask" value="{{sLenderCustomCredit2AmountAsCharge}}" id="sLenderCustomCredit2AmountAsCharge" preset="money" />
                                            </td>
                                        </tr>
                                    </table>
                                {{else}}
                                    <table>
                                        <tr>
                                            <td></td>
                                            <td class="FieldLabel">
                                                Final price
                                            </td>
                                            <td>
                                                <asp:Repeater runat="server" ID="sLDiscntBaseTRep_calc">
                                                    <HeaderTemplate>
                                                        <select on-change="recalc" on-blur="domblur" id="sLDiscntBaseT_calc" value="{{sLDiscntBaseT}}">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </select>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                &nbsp;
                                                <input readonly="true" type="text" intro="initMask" value="{{sBrokerLockOriginatorPriceBrokComp1PcPrice}}" id="sBrokerLockOriginatorPriceBrokComp1PcPrice_calc" preset="percent" />
                                            </td>                                      
                                            <td>
                                                <input readonly="true" type="text" intro="initMask" value="{{sBrokerLockOriginatorPriceBrokComp1PcAmt}}" id="sBrokerLockOriginatorPriceBrokComp1PcAmt_calc" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td class="FieldLabel">Lender credit target</td>
                                            <td></td>
                                            <td style="text-align: right;">
                                                <input readonly="true" type="text" intro="initMask" value="{{sLenderCreditAvailableAmt_Neg}}" id="sLenderCreditAvailableAmt_Neg" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">
                                                Disclose credit on
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Repeater runat="server" ID="sLenderPaidFeeDiscloseLocationTRep">
                                                    <HeaderTemplate>
                                                        <select on-change="recalc" on-blur="domblur" id="sLenderPaidFeeDiscloseLocationT" value="{{sLenderPaidFeeDiscloseLocationT}}">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </select>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                            <td class="FieldLabel">
                                                Credit for lender paid fees
                                            </td>
                                            <td></td>
                                            <td style="text-align: right;">
                                                <input readonly="true" type="text" intro="initMask" value="{{sLenderPaidFeesAmt_Neg}}" id="sLenderPaidFeesAmt_Neg" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Repeater runat="server" ID="sLenderGeneralCreditDiscloseLocationTRep">
                                                    <HeaderTemplate>
                                                        <select on-change="recalc" on-blur="domblur" id="sLenderGeneralCreditDiscloseLocationT" value="{{sLenderGeneralCreditDiscloseLocationT}}">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </select>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                            <td class="FieldLabel">
                                                General lender credit
                                            </td>
                                            <td></td>
                                            <td style="text-align: right;">
                                                <input readonly="true" type="text" intro="initMask" value="{{sLenderGeneralCreditAmt_Neg}}" id="sLenderGeneralCreditAmt_Neg" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Repeater runat="server" ID="sLenderCustomCredit1DiscloseLocationTRep">
                                                    <HeaderTemplate>
                                                        <select on-change="recalc" on-blur="domblur" id="sLenderCustomCredit1DiscloseLocationT" value="{{sLenderCustomCredit1DiscloseLocationT}}">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </select>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                            <td>
                                                <input type="text" intro="initMask" value="{{sLenderCustomCredit1Description}}" id="sLenderCustomCredit1Description_calc" />
                                            </td>
                                            <td style="text-align: right;">
                                                <input type="text" intro="initMask" value="{{sLenderCustomCredit1Amount}}" id="sLenderCustomCredit1Amount_calc" preset="money" on-change="recalc" />
                                            </td>
                                            <td style="text-align: right;">
                                                <input readonly="true" type="text" intro="initMask" value="{{sLenderCustomCredit1AmountAsCharge}}" id="sLenderCustomCredit1AmountAsCharge" preset="money" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Repeater runat="server" ID="sLenderCustomCredit2DiscloseLocationTRep">
                                                    <HeaderTemplate>
                                                        <select on-change="recalc" on-blur="domblur" id="sLenderCustomCredit2DiscloseLocationT" value="{{sLenderCustomCredit2DiscloseLocationT}}">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>">
                                                            <%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </select>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </td>
                                            <td>
                                                <input type="text" intro="initMask" value="{{sLenderCustomCredit2Description}}" id="sLenderCustomCredit2Description_calc" />
                                            </td>
                                            <td style="text-align: right;">
                                                <input type="text" intro="initMask" value="{{sLenderCustomCredit2Amount}}" id="sLenderCustomCredit2Amount_calc" preset="money" on-change="recalc" />
                                            </td>
                                            <td style="text-align: right;">
                                                <input readonly="true" type="text" intro="initMask" value="{{sLenderCustomCredit2AmountAsCharge}}" id="sLenderCustomCredit2AmountAsCharge" preset="money" />
                                            </td>
                                        </tr>
                                    </table>
                                {{/if}}
                            </td>
                            <td style="vertical-align: top;">
                                <table>
                                    <tr>
                                        <td class="FieldLabel">
                                            Initial credit (-)/charge (+)
                                        </td>
                                        <td>
                                            <input readonly="true" type="text" intro="initMask" value="{{sLDiscnt}}" id="sLDiscnt_init" preset="money" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            Total credit (-)/charge (+)
                                        </td>
                                        <td>
                                            <input readonly="true" type="text" intro="initMask" value="{{sLenderActualTotalCreditAmt_Neg}}" id="sLenderActualTotalCreditAmt_Neg" preset="money" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>          

                <div id="PaidTotals" class="tabContent">
                    <table>
                        <tr>
                            <td>
                                Total fees paid by lender: 
                            </td>
                            <td>
                                <input type="text" intro="initMask" id="sGfeTotalFundByLender" value="{{sGfeTotalFundByLender}}"  preset="money" width="77" ReadOnly="True" />
                            </td>
                            <td>
                                Total fees paid by seller: 
                            </td>
                            <td>
                                <input type="text" intro="initMask" id="sGfeTotalFundBySeller" value="{{sGfeTotalFundBySeller}}"  preset="money" width="77" ReadOnly="True" />
                            </td>
                        </tr>
                    </table>
                </div>

                <div id="Dot" class="tabContent">
                    <div>
                        <table>
                            <% if (!hideHeaderAndBoarder) {%>
                            <tr>
                                <td class="MainRightHeader" nowrap>
                                    Details of Transaction
                                </td>
                            </tr>
                            <% } %>
                            <tr>
                                <td>
                                    <table <%if(!hideHeaderAndBoarder) {%> class="InsetBorder" <% } %> style="border-color:White" id="Table1" cellspacing="0" cellpadding="0" width="700" border="0">
                                        <tr>
                                            <td nowrap>
                                                <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                                                  <tr>
                                                    <td></td>
                                                    <td colspan="2" class="align-center">Locked</td>
                                                    <td width="5"></td>
                                                    <td></td>
                                                    <td colspan="2">Locked</td>
                                                  </tr>
                                                  <tr>
                                                    <td>a. Purchase price</td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 79px">
                                                      {{ #if !(IsConstruction && AreConstructionLoanDataPointsMigrated) }}
                                                          <input type="text" intro="initMask" value="{{sPurchPrice}}" id="sPurchPrice" TabIndex="3" Width="86" preset="money"  on-change="recalc" runat="server" />
                                                      {{ else }}
                                                          <input type="text" readonly intro="initMask" value="{{sPurchasePrice1003}}" id="sPurchasePrice1003" TabIndex="3" Width="86" preset="money"  on-change="recalc" runat="server" />
                                                      {{ /if }}
                                                  </td>
                                                    <td></td>
                                                    <td>j. <a tabindex="-1" href="javascript:linkMe(gVirtualRoot + '/newlos/LoanInfo.aspx?pg=2');" >Subordinate financing</a></td>
                                                    <td style="width: 6px"></td>
                                                    <td><input type="text" intro="initMask" value="{{sONewFinBal}}" id="sONewFinBal" TabIndex="4" Width="86px" preset="money"  on-change="recalc" ReadOnly="True" /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>b. Alterations, improvements, repairs</td>
                                                    <td style="width: 11px">
                                                        <input disabled="{{IsConstruction && AreConstructionLoanDataPointsMigrated}}" type="checkbox" checked="{{sAltCostLckd}}" Disabled="{{!sIsRenovationLoan}}" id="sAltCostLckd" TabIndex="3" on-change="recalc"/>
                                                    </td>
                                                    <td style="width: 79px">
                                                        <input type="text" intro="initMask" readonly="{{!sAltCostLckd || (IsConstruction && AreConstructionLoanDataPointsMigrated)}}" value="{{sAltCost}}" id="sAltCost" TabIndex="3" Width="86px" preset="money" on-change="recalc"/>
                                                    </td>
                                                    <td></td>
                                                    <td>k. Borrower's closing costs paid by Seller</td>
                                                    <td style="width: 6px">
                                                      <input type="checkbox" checked="{{sTotCcPbsLocked}}" id="sTotCcPbsLocked" TabIndex="4"  on-change="recalc"/></td>
                                                    <td><input type="text" intro="initMask" readonly="{{!sTotCcPbsLocked}}" value="{{sTotCcPbs}}" id="sTotCcPbs" TabIndex="4" Width="86px" preset="money"  on-change="recalc"/></td>
                                                  </tr>
                                                  <tr>
                                                    <td>c. Land (if acquired separately)</td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 79px">
                                                      {{ #if !(IsConstruction && AreConstructionLoanDataPointsMigrated)  }}
                                                          <input type="text" intro="initMask" value="{{sLandCost}}" id="sLandCost" TabIndex="3" Width="86px" preset="money"  on-change="recalc"/>
                                                      {{ else }}
                                                          <input type="text" readonly intro="initMask" value="{{sLandIfAcquiredSeparately1003}}" id="sLandIfAcquiredSeparately1003" TabIndex="3" Width="86px" preset="money"  on-change="recalc"/>
                                                      {{ /if }}
                                                    </td>
                                                    <td style="height: 23px"></td>
                                                    <td>l.
                                                      <ractiveCombobox value="{{sOCredit1Desc}}" disabled="{{!sOCredit1Lckd}}" Prefix="CreditDesc" Field="sOCredit1Desc"/>
                                                    <td>
                                                      <input type="checkbox" checked="{{sOCredit1Lckd}}" id="sOCredit1Lckd"  disabled="{{sLoads1003LineLFromAdjustments}}" on-change="recalc" TabIndex="4"/></td>
                                                    <td style="height: 23px"><input readonly="{{!sOCredit1Lckd || sLoads1003LineLFromAdjustments}}" type="text" intro="initMask" value="{{sOCredit1Amt}}" id="sOCredit1Amt" TabIndex="4" Width="86px" preset="money"  on-change="recalc"/></td>
                                                  </tr>
                                                  <tr>
                                                    <td>d. Refi (incl. debts to be paid off)&nbsp;</td>
                                                    <td style="width: 11px" class="align-right">
                                                      <input type="checkbox" disabled="{{IsConstruction && AreConstructionLoanDataPointsMigrated}}" checked="{{sRefPdOffAmt1003Lckd}}" id="sRefPdOffAmt1003Lckd" TabIndex="3"  on-change="recalc"/></td>
                                                    <td style="width: 79px"><input readonly="{{!sRefPdOffAmt1003Lckd || (IsConstruction && AreConstructionLoanDataPointsMigrated)}}" type="text" intro="initMask" value="{{sRefPdOffAmt1003}}" id="sRefPdOffAmt1003" TabIndex="3" Width="86px" preset="money"  on-change="recalc"/></td>
                                                    <td></td>
                                                    <td>&nbsp;&nbsp;&nbsp;<ractiveCombobox value="{{sOCredit2Desc}}" Prefix="CreditDesc" Field="sOCredit2Desc" disabled="{{sLoads1003LineLFromAdjustments}}"/>
                                                    </td>
                                                    <td>
                                                      </td>
                                                    <td><input type="text" intro="initMask" value="{{sOCredit2Amt}}" id="sOCredit2Amt" TabIndex="4" Width="86px" preset="money" readonly="{{sLoads1003LineLFromAdjustments}}"  on-change="recalc"/></td>
                                                  </tr>
                                                  <tr>
                                                    <td>e. Estimated prepaid items&nbsp;</td>
                                                    <td style="width: 11px" class="align-right">
                                                      <input type="checkbox" checked="{{sTotEstPp1003Lckd}}" id="sTotEstPp1003Lckd" TabIndex="3"  on-change="recalc"/></td>
                                                    <td style="width: 79px"><input type="text" readonly="{{!sTotEstPp1003Lckd}}" intro="initMask" value="{{sTotEstPp1003}}" id="sTotEstPp1003" TabIndex="3" Width="86px" preset="money"  on-change="recalc"/></td>
                                                    <td></td>
                                                    <td colspan="2">&nbsp;&nbsp;&nbsp;<ractiveCombobox value="{{sOCredit3Desc}}" Prefix="CreditDesc" Field="sOCredit3Desc" disabled="{{sLoads1003LineLFromAdjustments}}"/>
                                                    </td>
                                                    <td><input type="text" intro="initMask" value="{{sOCredit3Amt}}" id="sOCredit3Amt" TabIndex="4" Width="86px" readonly="{{sLoads1003LineLFromAdjustments}}" preset="money"  on-change="recalc" /></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="little-indent">
                                                    {{#if sIsIncludeProrationsInTotPp && sIsIncludeProrationsIn1003Details }}
                                                    Prepaids and reserves
                                                    {{/if}} 
                                                    </td>
                                                    <td>
                                                        {{#if sIsIncludeProrationsInTotPp && sIsIncludeProrationsIn1003Details }}
                                                        <input type="text" id="sTotEstPp" readonly="readonly" value="{{sTotEstPp}}" preset="money" />
                                                        {{/if}}
                                                    </td>
                                                    <td style="width: 79px"></td>
                                                    <td></td>
                                                    <td colspan="2">&nbsp;&nbsp;&nbsp;<ractiveCombobox value="{{sOCredit4Desc}}" Prefix="CreditDesc" Field="sOCredit4Desc" disabled="{{sLoads1003LineLFromAdjustments}}"/>
                                                    </td>
                                                    <td><input type="text" intro="initMask" value="{{sOCredit4Amt}}" id="sOCredit4Amt" TabIndex="4" Width="86px" preset="money" readonly="{{sLoads1003LineLFromAdjustments}}"  on-change="recalc" /></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="little-indent">
                                                        {{#if sIsIncludeProrationsInTotPp && sIsIncludeProrationsIn1003Details }}
                                                        Prorations paid by borrower
                                                        {{/if}}
                                                    </td>
                                                    <td>
                                                        {{#if sIsIncludeProrationsInTotPp && sIsIncludeProrationsIn1003Details }}
                                                        <input type="text" id="sTotalBorrowerPaidProrations" readonly="readonly" value="{{sTotalBorrowerPaidProrations}}" preset="money" />
                                                        {{/if}}
                                                    </td>
                                                    <td style="width: 79px"></td>
                                                    <td></td>
                                                    <td colspan="2">&nbsp;&nbsp;&nbsp;Lender credit
                                                    </td>
                                                    <td><input type="text" intro="initMask" value="{{sOCredit5Amt}}" id="sOCredit5Amt" TabIndex="4" Width="86px" preset="money"  readonly="true" /></td>
                                                  </tr>              
                                                  {{#if !sIsIncludeONewFinCcInTotEstCc}}
                                                  <tr>
                                                    <td></td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 79px"></td>
                                                    <td></td>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Other financing closing costs</td>
                                                    <td align="right">-&nbsp;</td>
                                                    <td><input type="text" intro="initMask" value="{{sONewFinCc}}" id="sONewFinCc" TabIndex="4" Width="86px" preset="money" on-change="recalc"/></td>
                                                  </tr>
                                                  {{/if}}
                                                  <tr>
                                                    <td>f. Estimated closing costs</td>
                                                    <td style="width: 11px" class="align-right">
                                                      <input type="checkbox" checked="{{sTotEstCc1003Lckd}}" id="sTotEstCc1003Lckd" TabIndex="3"  on-change="recalc"/></td>
                                                    <td style="width: 79px"><input readonly="{{!sTotEstCc1003Lckd}}" type="text" intro="initMask" value="{{sTotEstCcNoDiscnt1003}}" id="sTotEstCcNoDiscnt1003" TabIndex="3" Width="86px" preset="money"  on-change="recalc"/></td>
                                                    <td colspan="2" {{#if sIsIncludeONewFinCcInTotEstCc }} rowspan="3" class="align-bottom" {{/if}}>m. Loan amount (exclude PMI, MIP, FF financed)</td>
                                                    <td style="width: 6px" {{#if sIsIncludeONewFinCcInTotEstCc }} rowspan="3" class="align-bottom" {{/if}}>
                                                      <input type="checkbox" checked="{{sLAmtLckd}}" disabled="{{sIsRenovationLoan}}" id="sLAmtLckd" on-change="recalc"  TabIndex="202"/></td>
                                                    <td {{#if sIsIncludeONewFinCcInTotEstCc }} rowspan="3" class="align-bottom" {{/if}}><input type="text" intro="initMask" value="{{sLAmtCalc}}" id="sLAmtCalc" TabIndex="4" ReadOnly="{{!sLAmtLckd}}" on-change="recalc" Width="86px" preset="money" /></td>
                                                  </tr>
                                                  {{#if sIsIncludeONewFinCcInTotEstCc }}
                                                  <tr>
                                                    <td class="little-indent">This loan closing costs</td>
                                                    <td>
                                                      <input type="text" id="sTotEstCcNoDiscnt" readonly="readonly" value="{{sTotEstCcNoDiscnt}}" preset="money" /></td>
                                                    <td></td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="little-indent">Other financing closing costs</td>
                                                    <td>
                                                      <input type="text" intro="initMask" value="{{sONewFinCc}}" id="sONewFinCc2" data-field-id="sONewFinCc" TabIndex="4" Width="86px" preset="money" on-change="recalc"/></td>
                                                    <td></td>
                                                    <td colspan="2"></td>
                                                    <td></td>
                                                    <td></td>
                                                  </tr>
                                                  {{/if}}
                                                  <tr>
                                                    <td>g. PMI,&nbsp;MIP, Funding Fee</td>
                                                    <td style="width: 11px" class="align-right">
                                                      <input type="checkbox" checked="{{sFfUfmip1003Lckd}}" id="sFfUfmip1003Lckd" TabIndex="3"  on-change="recalc"/></td>
                                                    <td style="width: 79px"><input readonly="{{!sFfUfmip1003Lckd}}" type="text" intro="initMask" value="{{sFfUfmip1003}}" id="sFfUfmip1003" TabIndex="3" Width="86px" preset="money"  on-change="recalc"/></td>
                                                    <td></td>
                                                    <td>n. PMI, MIP, Funding Fee financed</td>
                                                    <td style="width: 6px"></td>
                                                    <td><input type="text" intro="initMask" value="{{sFfUfmipFinanced}}" id="sFfUfmipFinanced" TabIndex="4" ReadOnly="True" Width="86px" preset="money" /></td>
                                                  </tr>
                                                  <tr>
                                                    <td>h. Discount (if Borrower will pay)</td>
                                                    <td style="width: 11px" class="align-right">
                                                      <input type="checkbox" checked="{{sLDiscnt1003Lckd}}" id="sLDiscnt1003Lckd" TabIndex="3"  on-change="recalc"/></td>
                                                    <td style="width: 79px"><input type="text" readonly="{{!sLDiscnt1003Lckd}}" intro="initMask" value="{{sLDiscnt1003}}" id="sLDiscnt1003" TabIndex="3" Width="86px" preset="money"  on-change="recalc"/></td>
                                                    <td></td>
                                                    <td>o. Loan amount (add m &amp; n)</td>
                                                    <td style="width: 6px"></td>
                                                    <td><input type="text" intro="initMask" value="{{sFinalLAmt}}" id="sFinalLAmt" TabIndex="4" ReadOnly="True" Width="86px" preset="money" /></td>
                                                  </tr>
                                                  <tr>
                                                    <td class="FieldLabel">i. Total costs (add items a to h)</td>
                                                    <td style="width: 11px"></td>
                                                    <td style="width: 79px"><input type="text" intro="initMask" value="{{sTotTransC}}" id="sTotTransC" TabIndex="3" ReadOnly="True" Width="86px" preset="money" /></td>
                                                    <td></td>   
                                                    <td>p. Cash from / to Borr (subtract j, k, l &amp; o from i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                    <td style="width: 6px">
                                                      <input type="checkbox" checked="{{sTransNetCashLckd}}" id="sTransNetCashLckd" TabIndex="4"  on-change="recalc"/></td>
                                                    <td><input type="text" readonly="{{!sTransNetCashLckd}}" intro="initMask" value="{{sTransNetCash}}" id="sTransNetCash" TabIndex="4" Width="86px" preset="money" /></td>
                                                  </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
  </script>
            
           
            <div class="MainRightHeader">Borrower-responsible Closing Costs</div>

            <div class="SectionDiv" id="ccTableDiv" runat="server">
                <table id="ccTable" width="100%">
	                <tr class="FormTableHeader">
	                    <td>
	                        Closing Cost Automation Options
	                    </td>
	                </tr>
	                <tr>
	                    <td>
                            <asp:RadioButtonList ID="sClosingCostAutomationUpdateT" runat="server" onchange="refreshCalculation()" >
                                <asp:ListItem Text="Use automation to update all closing costs." Value="2" />
                                <asp:ListItem Text="Use automation to update the fees whose conditions no longer apply." Value="0" />
                                <asp:ListItem Text="Use closing costs currently on the loan file for similar scenarios." Value="1" />
                            </asp:RadioButtonList>


                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" onchange="sLenderPaidBrokerCompFToggle();refreshCalculation();" >
                            </asp:RadioButtonList>
	                    </td>
	                </tr>
	            </table>
            </div>
            
            <div class="SectionDiv">
                <table width="100%">
                    <tr id="trIsRequireFeesFromDropDown" runat="server">
                        <td colspan="2" class="FieldLabel">
                            <asp:CheckBox runat="server" ID="sIsRequireFeesFromDropDown" onclick="refreshCalculation();" />
                            <span>Protect compliance settings for fees</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="FieldLabel">
                            <asp:CheckBox runat="server" ID="sGfeIsTPOTransaction" onclick="sLenderPaidBrokerCompFToggle();refreshCalculation();" />
                            <span>This transaction involves a TPO</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="FormTableHeader">
                            Loan originator compensation source
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="FieldLabel">Loan originator is paid by</span>
                            <asp:RadioButtonList ID="sOriginatorCompensationPaymentSourceT" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" onchange="sLenderPaidBrokerCompFToggle();refreshCalculation();" >
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </div>
            
            <div id="ClosingCostDetailsSection" class="SectionDiv">
                <div id="ClosingCostFeessContainer"></div>
            </div>

        <div class="Hidden">
            <uc1:QualifyingBorrower runat="server" ID="QualifyingBorrowerControl" />
        </div>
    </form>
</body>
</html>
