<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="ARMProgramDisclosure.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Disclosure.ARMProgramDisclosure" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="Rfp" TagName="RateFloorPopup" Src="~/newlos/Forms/RateFloorPopup.ascx" %>
<%@ Import namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>ARMProgramDisclosure</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%# AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
      <style type="text/css">
        .RateFloorPopup .ui-dialog-titlebar
        {
            background-color:Maroon;
        }
        .RedHeader .ui-dialog
        {
            border: solid 1px black;
        }
      </style>
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" leftmargin=0>
	<script language=javascript>
<!--

function _init()
{
	if (typeof (RateFloorPopupInit) == "function") {
	    RateFloorPopupInit();
	}

	f_setArmIndexView();
}

function f_setArmIndexView() {
    <%= AspxTools.JsGetElementById(sArmIndexT) %>.disabled = !<%= AspxTools.JsGetElementById(sArmIndexTLckd) %>.checked;
    <%= AspxTools.JsGetElementById(sFreddieArmIndexT) %>.disabled = !<%= AspxTools.JsGetElementById(sFreddieArmIndexTLckd) %>.checked;
}


function f_pickARMIndex() {
  showModal("/newlos/Disclosure/ARMEntryList.aspx", null, null, null, function(arg){
    if (arg.OK) {
    <%= AspxTools.JsGetElementById(sArmIndexT) %>.value = arg.fannieMaeArmIndexT;
    <%= AspxTools.JsGetElementById(sArmIndexTLckd) %>.checked = true;

    <%= AspxTools.JsGetElementById(sFreddieArmIndexT) %>.value = arg.freddieArmIndexT;
    <%= AspxTools.JsGetElementById(sFreddieArmIndexTLckd) %>.checked = true;
    <%= AspxTools.JsGetElementById(sArmIndexEffectiveD) %>.value = arg.effectiveD;
    <%= AspxTools.JsGetElementById(sArmIndexBasedOnVstr) %>.value = arg.indexBasedOnVstr;
    <%= AspxTools.JsGetElementById(sArmIndexCanBeFoundVstr) %>.value = arg.indexCanBeFoundVstr;
    f_setArmIndexView();
    updateDirtyBit();
  }
  },{ hideCloseButton: true });
}
//-->
</script>

<form id="ARMProgramDisclosure" method="post" runat="server">
<Rfp:RateFloorPopup runat="server" ID="RateFloorPopup" />
<table id=Table1 cellspacing=0 cellpadding=0 border=0 width=700>
  <tr>
    <td nowrap class=MainRightHeader style="PADDING-LEFT:5px">ARM Program Disclosure</td></tr>
  <tr>
    <td nowrap>
      <table class=InsetBorder id=Table2 cellspacing=0 cellpadding=0 width="98%" 
      border=0>
        <tr>
          <td class=FormTableSubheader nowrap style="WIDTH: 437px">How your interest rate and payments are determined</td></tr>
        <tr>
          <td class=FieldLabel style="WIDTH: 437px" nowrap><input class="ButtonStyle" type=button value="Pick from list" onclick="f_pickARMIndex();" NoHighlight></td></tr>
        <tr>
          <td class=FieldLabel style="WIDTH: 437px" nowrap>Fannie Mae 
            Recognized Index <asp:DropDownList id=sArmIndexT runat="server"></asp:DropDownList>&nbsp;<asp:CheckBox ID="sArmIndexTLckd" runat="server" onclick="refreshCalculation();" /></td></tr>
        <tr>
          <td class=FieldLabel style="WIDTH: 437px" nowrap>Freddie Mac ARM 
            Index Type <asp:DropDownList id=sFreddieArmIndexT runat="server"></asp:DropDownList>&nbsp;<asp:CheckBox ID="sFreddieArmIndexTLckd" runat="server" onclick="refreshCalculation();" /></td></tr>
        <tr>
          <td class=FieldLabel style="WIDTH: 437px" nowrap>ARM Index Effective 
            Date <ml:DateTextBox id=sArmIndexEffectiveD runat="server" preset="date" width="75"></ml:DateTextBox></td></tr>
        <tr>
          <td nowrap class="FieldLabel" style="WIDTH: 437px">The interest will be based on</td></tr>
        <tr>
          <td nowrap style="WIDTH: 437px"><asp:TextBox id=sArmIndexBasedOnVstr runat="server" Height="64px" Width="600px" TextMode="MultiLine" onkeyup="TextAreaMaxLength(this, 200);"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Information about the index can be 
          found</td></tr>
        <tr>
          <td style="WIDTH: 437px" nowrap><asp:TextBox id=sArmIndexCanBeFoundVstr runat="server" Height="64px" Width="600px" TextMode="MultiLine" onkeyup="TextAreaMaxLength(this, 200);"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel style="WIDTH: 437px" nowrap><asp:CheckBox id=sArmIndexAffectInitIRBit runat="server" Text="Your initial interest rate is not based on the index."></asp:CheckBox></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table class=InsetBorder id=Table3 cellspacing=0 cellpadding=0 width="98%" border=0>
        <tr>
          <td class=FormTableSubheader nowrap style="PADDING-LEFT:5px" colspan=2>How your interest rate can change</td></tr>
        <tr>
          <td nowrap class="FieldLabel">1st Adj 
            Cap&nbsp;&nbsp;</td>
          <td class=FieldLabel nowrap width="100%"><ml:PercentTextBox id=sRAdj1stCapR runat="server" width="70" preset="percent"></ml:PercentTextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>1st Change</td>
          <td class=FieldLabel nowrap><asp:TextBox id=sRAdj1stCapMon runat="server" Width="51px"></asp:TextBox>&nbsp;months</td></tr>
        <tr>
          <td class=FieldLabel nowrap>Adj Cap</td>
          <td nowrap><ml:PercentTextBox id=sRAdjCapR runat="server" width="70" preset="percent"></ml:PercentTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Adj Period</td>
          <td class=FieldLabel nowrap><asp:TextBox id=sRAdjCapMon runat="server" Width="51px"></asp:TextBox>&nbsp;months</td></tr>
        <tr>
          <td class=FieldLabel nowrap>Life Adj Cap</td>
          <td nowrap><ml:PercentTextBox id=sRAdjLifeCapR runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Margin</td>
          <td nowrap><ml:PercentTextBox id=sRAdjMarginR runat="server" width="70" preset="percent"></ml:PercentTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>
              Rate Floor
              <a href="javascript:void(0);" id="RateFloorCalcLink">calculate</a>
          </td>
          <td nowrap>
              <ml:PercentTextBox id=sRAdjFloorR runat="server" preset="percent" width="70" ReadOnly="true"></ml:PercentTextBox>
          </td>
        </tr>
        <tr>
          <td class=FieldLabel nowrap>Round&nbsp; <asp:DropDownList id=sRAdjRoundT runat="server"></asp:DropDownList></td>
          <td nowrap><ml:PercentTextBox id=sRAdjRoundToR runat="server" width="70" preset="percent"></ml:PercentTextBox></td></tr></table></td></tr>
  <tr>
    <td nowrap></td></tr>
  <tr>
    <td class="FieldLabel" style="PADDING-LEFT:5px">The index will be determined <asp:TextBox ID="sArmIndexLeadDays" runat="server" Width="38px" onchange="refreshCalculation();" /> days before the Change Date.</td></tr>
  <tr>
    <td class=FieldLabel nowrap style="PADDING-LEFT:5px">You will be notified in writing at 
            least <asp:TextBox id=sArmIndexNotifyAtLeastDaysVstr runat="server" Width="38px"></asp:TextBox>&nbsp;days</td></tr>
  <tr>
    <td class=FieldLabel nowrap style="PADDING-LEFT:5px">but not more than <asp:TextBox id=sArmIndexNotifyNotBeforeDaysVstr runat="server" Width="36px"></asp:TextBox>&nbsp;days</td></tr>
  <tr>
    <td nowrap class=FieldLabel><asp:CheckBox id=sHasDemandFeature runat="server" Text="DEMAND FEATURE: This obligation has a demand feature"></asp:CheckBox></td></tr></table>

     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</html>
