﻿namespace LendersOfficeApp.newlos.Disclosure
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.Security;
    using los.AdjustmentsAndOtherCreditsSetup;
    using LendersOffice.Migration;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.ObjLib.TRID2;

    public partial class AdjustmentsAndOtherCreditsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "CalculateData":
                    this.CalculateData();
                    break;
                case "Calculate1003CheckboxToHandleCircularDependency":
                    this.Calculate1003CheckboxToHandleCircularDependency();
                    break;
                case "Migrate1003ToAdjustments":
                    this.Migrate1003ToAdjustments();
                    break;
                case "Save":
                    this.Save();
                    break;
            }
        }

        // specific service call to handle circular dependency for Include in 1003 Details checkbox
        private void Calculate1003CheckboxToHandleCircularDependency()
        {
            Guid loanId = this.GetGuid("loanId");
            string viewModelJson = this.GetString("viewModelJson");
            bool include1003Check = this.GetBool("include1003Check");
            int index = this.GetInt("index");
            bool isSystemAdjustment = this.GetBool("isSystemAdjustment");

            var model = SerializationHelper.JsonNetDeserialize<AdjustmentsAndOtherCreditsViewModel>(viewModelJson);

            if (isSystemAdjustment)
            {
                model.AdjustmentList.SellerCredit.SetIsPopulateToLineLOn1003(include1003Check);
            }
            else
            {
                int i = 0;
                foreach (Adjustment adj in model.AdjustmentList.GetEnumeratorWithoutSellerCreditAndAssets())
                {
                    if (i == index)
                    {
                        adj.SetIsPopulateToLineLOn1003(include1003Check);
                        break;
                    }
                    i++;
                }                
            }
            var loan = CPageData.CreateUsingSmartDependency(
                loanId,
                typeof(AdjustmentsAndOtherCredits));
            loan.InitLoad();

            try
            {
                this.UpdateLoanFromView(loan, model);                
                model = this.CreateViewModel(loan);
                this.SetResult("model", SerializationHelper.JsonNetSerialize(model));
            }
            catch (CBaseException exception)
            {
                this.SetResult("IsValid", false);
                this.SetResult("ErrorMsg", exception.UserMessage);
            }
        }    

        private void CalculateData()
        {
            Guid loanId = this.GetGuid("loanId");
            string viewModelJson = this.GetString("viewModelJson");
            var model = SerializationHelper.JsonNetDeserialize<AdjustmentsAndOtherCreditsViewModel>(viewModelJson);
            var loan = CPageData.CreateUsingSmartDependency(
                loanId,
                typeof(AdjustmentsAndOtherCredits));
            loan.InitLoad();

            try
            {
                this.UpdateLoanFromView(loan, model);
                model = this.CreateViewModel(loan);

                this.SetResult("model", SerializationHelper.JsonNetSerialize(model));
            }
            catch (CBaseException exception)
            {
                this.SetResult("IsValid", false);
                this.SetResult("ErrorMsg", exception.UserMessage);
            }
        }        

        private void Migrate1003ToAdjustments()
        {
            if (ConstStage.DisableUserMigrationToAdjustmentsMode)
            {
                return;
            }

            Guid loanId = this.GetGuid("loanId");
            
            var loan = CPageData.CreateUsingSmartDependency(
                loanId,
                typeof(AdjustmentsAndOtherCredits));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            AdjustmentsAndOtherCreditsViewModel model;

            if (loan.sLoads1003LineLFromAdjustments)
            {
                Tools.LogError("Programming error.  Somehow Migrate1003ToAdjustments was called when sLoads1003LineLFromAdjustments was already true.");
                model = this.CreateViewModel(loan);
                this.SetResult("model", SerializationHelper.JsonNetSerialize(model));
                return;
            }

            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            if (db.DontLink1003ToAdjustmentsOnPull)
            {
                loan.MigrateExisting1003ToAdjustments();
            }
            else
            {
                loan.sLoads1003LineLFromAdjustments = true;
            }

            loan.Save();

            model = this.CreateViewModel(loan);

            this.SetResult("model", SerializationHelper.JsonNetSerialize(model));
        }

        private void Save()
        {
            Guid loanId = this.GetGuid("loanId");
            string viewModelJson = this.GetString("viewModelJson");
            var model = SerializationHelper.JsonNetDeserialize<AdjustmentsAndOtherCreditsViewModel>(viewModelJson);

            var loan = CPageData.CreateUsingSmartDependency(
                loanId,
                typeof(AdjustmentsAndOtherCredits));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            List<PredefinedAdjustment> allowedAdjustments = PredefinedAdjustment.GetAllAdjustmentsForBroker(PrincipalFactory.CurrentPrincipal.BrokerId);
            bool allowCustomDescriptions = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowTogglingTheRestrictAdjustmentsAndOtherCreditsCheckbox);

            try
            {
                UpdateLoanFromView(loan, model);

                loan.Save();
                var newModel = this.CreateViewModel(loan);
                this.SetResult("model", SerializationHelper.JsonNetSerialize(newModel));
            }
            catch (CBaseException exception)
            {
                this.SetResult("IsValid", false);
                this.SetResult("ErrorMsg", exception.UserMessage);
            }
        }

        /// <summary>
        /// Creates the model to be used by the view. Due to the limitations of
        /// CPageData.CreateUsingSmartDependency, this is duplicated on the
        /// non-service page to avoid unnecessarily hitting the loan tables.
        /// </summary>
        /// <param name="loan">The loan.</param>
        /// <returns>The model.</returns>
        private AdjustmentsAndOtherCreditsViewModel CreateViewModel(CPageData loan)
        {
            bool loanVersionBeyond24 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(loan.sLoanVersionT, LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments);
            bool isTrid2 = loan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017;
            bool trid2ExcludeSalesPriceAndFirstLienLoanAmountCredit = loan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && loan.sLienPosT == E_sLienPosT.Second && loan.sIsOFinNew && loan.sRemain1stMBal > 0;
            bool trid2ExcludeDebtsToBePaidOff = loan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && loan.sLienToPayoffTotDebt == LendersOffice.ObjLib.ResponsibleLien.OtherLienTransaction;
            bool includeInLeCdForThisLien = loan.sLienPosT == E_sLienPosT.Second || (loan.sLienPosT == E_sLienPosT.First && loan.sIsOFinNew && loan.sConcurSubFin > 0);

            return new AdjustmentsAndOtherCreditsViewModel()
            {
                VRoot = Tools.VRoot,
                DisableUserMigrationToAdjustmentsMode = ConstStage.DisableUserMigrationToAdjustmentsMode,
                AdjustmentList = loan.sAdjustmentList,
                AssetAdjustments = loan.sAdjustmentList.AssetAdjustments,
                AdjustmentListWithAssets = loan.sAdjustmentList.GetEnumeratorWithoutSellerCredit(),
                sPropertyTransferD = loan.sPropertyTransferD_rep,
                sPropertyTransferDLckd = loan.sPropertyTransferDLckd,
                ProrationList = loan.sProrationList,
                sPurchasePrice1003 = loan.sPurchasePrice1003_rep,
                sLoads1003LineLFromAdjustments = loan.sLoads1003LineLFromAdjustments,
                sGrossDueFromBorrPersonalProperty = loan.sGrossDueFromBorrPersonalProperty_rep,
                sTRIDClosingDisclosureBorrowerCostsPaidAtClosing = loan.sTRIDClosingDisclosureBorrowerCostsPaidAtClosing_rep,
                sTRIDCashDeposit = loan.sTRIDCashDeposit_rep,
                sFinalLAmt = loan.sFinalLAmt_rep,
                sReductionsDueToSellerExcessDeposit = loan.sReductionsDueToSellerExcessDeposit_rep,
                sTotalSellerPaidClosingCostsPaidAtClosing = loan.sTotalSellerPaidClosingCostsPaidAtClosing_rep,
                sReductionsDueToSellerPayoffOf1stMtgLoan = loan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep,
                sReductionsDueToSellerPayoffOf2ndMtgLoan = loan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep,
                sRefPdOffAmt1003 = loan.sRefPdOffAmt1003_rep,
                sRefPdOffAmt1003Lckd = loan.sRefPdOffAmt1003Lckd,
                sONewFinBal = loan.sONewFinBal_rep,
                sONewFinNetProceeds = loan.sONewFinNetProceeds_rep,
                sONewFinNetProceedsLckd = loan.sONewFinNetProceedsLckd,
                sIsRenovationLoan = loan.sIsRenovationLoan,
                sAltCostLckd = loan.sAltCostLckd,
                sAltCost = loan.sAltCost_rep,
                sTRIDClosingDisclosureAltCostLckd = loan.sTRIDClosingDisclosureAltCostLckd,
                sTRIDClosingDisclosureAltCost = loan.sTRIDClosingDisclosureAltCost_rep,
                sRenoFeesInClosingCosts = loan.sRenoFeesInClosingCosts_rep,
                IncludeAlterationsImprovementsRepairsBreakdown = loan.sIsRenovationLoan && loan.BrokerDB.EnableRenovationLoanSupport,
                sTotCcPbsLocked = loan.sTotCcPbsLocked,
                sTotCcPbs = loan.sTotCcPbs_rep,
                sLandIfAcquiredSeparately1003 = loan.sLandIfAcquiredSeparately1003_rep,
                sOCredit1Desc = loan.sOCredit1Desc,
                sOCredit1Lckd = loan.sOCredit1Lckd,
                sOCredit1Amt = loan.sOCredit1Amt_rep,
                sOCredit2Desc = loan.sOCredit2Desc,
                sOCredit2Amt = loan.sOCredit2Amt_rep,
                sTotEstPp1003Lckd = loan.sTotEstPp1003Lckd,
                sTotEstPp1003 = loan.sTotEstPp1003_rep,
                sOCredit3Desc = loan.sOCredit3Desc,
                sOCredit3Amt = loan.sOCredit3Amt_rep,
                sOCredit4Desc = loan.sOCredit4Desc,
                sOCredit4Amt = loan.sOCredit4Amt_rep,
                sOCredit5Amt = loan.sOCredit5Amt_rep,
                sONewFinCc = loan.sONewFinCc_rep,
                sTotEstCc1003Lckd = loan.sTotEstCc1003Lckd,
                sTotEstCcNoDiscnt1003 = loan.sTotEstCcNoDiscnt1003_rep,
                sLAmtLckd = loan.sLAmtLckd,
                sLAmtCalc = loan.sLAmtCalc_rep,
                sFfUfmip1003Lckd = loan.sFfUfmip1003Lckd,
                sFfUfmip1003 = loan.sFfUfmip1003_rep,
                sFfUfmipFinanced = loan.sFfUfmipFinanced_rep,
                sLDiscnt1003Lckd = loan.sLDiscnt1003Lckd,
                sLDiscnt1003 = loan.sLDiscnt1003_rep,
                sTotTransC = loan.sTotTransC_rep,
                sTransNetCashLckd = loan.sTransNetCashLckd,
                sTransNetCash = loan.sTransNetCash_rep,
                sTotEstPp = loan.sTotEstPp_rep,
                sTotalBorrowerPaidProrations = loan.sTotalBorrowerPaidProrations_rep,
                sTotEstCcNoDiscnt = loan.sTotEstCcNoDiscnt_rep,
                sIsIncludeProrationsInTotPp = loan.sIsIncludeProrationsInTotPp,
                sIsIncludeONewFinCcInTotEstCc = loan.sIsIncludeONewFinCcInTotEstCc,
                predefinedAdjustmentsList = loanVersionBeyond24 ? PredefinedAdjustment.GetAllAdjustmentsForBroker(loan.sBrokerId) : null,
                sIsRestrictAdjustmentsAndOtherCreditsDescriptions = loanVersionBeyond24 ? loan.sIsRestrictAdjustmentsAndOtherCreditsDescriptions : false,
                PredefinedAdjustmentTypeMap = loanVersionBeyond24 ? AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap : null,
                ExcludeSalesPriceAndFirstLienLoanAmountCredit = trid2ExcludeSalesPriceAndFirstLienLoanAmountCredit,
                ExcludeDebtsToBePaidOff = trid2ExcludeDebtsToBePaidOff,
                IsTrid2 = isTrid2,
                IncludeInLeCdForThisLien = isTrid2 && includeInLeCdForThisLien,
                qualifyingBorrowerModel = QualifyingBorrower.RetrieveQualifyingBorrowerModel(loan)
            };
        }

        private void UpdateLoanFromView(CPageData loan, AdjustmentsAndOtherCreditsViewModel model)
        {
            // not adding the details of transaction section here since it is supposed to be readonly.
            loan.sPropertyTransferD_rep = model.sPropertyTransferD;
            loan.sPropertyTransferDLckd = model.sPropertyTransferDLckd;
            loan.sProrationList = model.ProrationList;
            loan.sAdjustmentList = model.AdjustmentList;
            loan.sGrossDueFromBorrPersonalProperty_rep = model.sGrossDueFromBorrPersonalProperty;
            loan.sReductionsDueToSellerExcessDeposit_rep = model.sReductionsDueToSellerExcessDeposit;
            loan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep = model.sReductionsDueToSellerPayoffOf1stMtgLoan;
            loan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep = model.sReductionsDueToSellerPayoffOf2ndMtgLoan;
            loan.sRefPdOffAmt1003Lckd = model.sRefPdOffAmt1003Lckd;
            loan.sRefPdOffAmt1003_rep = model.sRefPdOffAmt1003;
            loan.sAltCostLckd = model.sAltCostLckd;
            loan.sAltCost_rep = model.sAltCost;
            loan.sONewFinNetProceeds_rep = model.sONewFinNetProceeds;
            loan.sONewFinNetProceedsLckd = model.sONewFinNetProceedsLckd;
            loan.sIsRestrictAdjustmentsAndOtherCreditsDescriptions = (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(loan.sLoanVersionT, LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments)) ? model.sIsRestrictAdjustmentsAndOtherCreditsDescriptions : false;
            loan.sTRIDClosingDisclosureAltCostLckd = model.sTRIDClosingDisclosureAltCostLckd;
            loan.sTRIDClosingDisclosureAltCost_rep = model.sTRIDClosingDisclosureAltCost;
            QualifyingBorrower.UpdateLoanFromQualifyingBorrowerModel(loan, model.qualifyingBorrowerModel);
        }
    }
}
