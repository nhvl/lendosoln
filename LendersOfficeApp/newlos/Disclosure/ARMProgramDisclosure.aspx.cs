using System;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.UI.DataContainers;

namespace LendersOfficeApp.newlos.Disclosure
{

    public partial class ARMProgramDisclosure : LendersOfficeApp.newlos.BaseLoanPage
	{
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "ARM Program Disclosure";
            this.PageID = "ARMProgramDisclosure";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CARMDisclosurePDF);
            
            Tools.Bind_sRAdjRoundT(sRAdjRoundT);
            Tools.Bind_sArmIndexT(sArmIndexT);
            Tools.Bind_sFreddieArmIndexT(sFreddieArmIndexT);
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ARMProgramDisclosure));
            dataLoan.InitLoad();

            sHasDemandFeature.Checked             = dataLoan.sHasDemandFeature;
            sRAdj1stCapR.Text                     = dataLoan.sRAdj1stCapR_rep;
            sRAdj1stCapMon.Text                   = dataLoan.sRAdj1stCapMon_rep;
            sRAdjCapR.Text                        = dataLoan.sRAdjCapR_rep;
            sRAdjCapMon.Text                      = dataLoan.sRAdjCapMon_rep;
            sRAdjLifeCapR.Text                    = dataLoan.sRAdjLifeCapR_rep;
            sRAdjMarginR.Text                     = dataLoan.sRAdjMarginR_rep;
            sRAdjMarginR.ReadOnly                 = dataLoan.sIsRateLocked || IsReadOnly;
            sRAdjRoundToR.Text                    = dataLoan.sRAdjRoundToR_rep;
            sRAdjFloorR.Text                      = dataLoan.sRAdjFloorR_rep;
            sArmIndexNotifyAtLeastDaysVstr.Text   = dataLoan.sArmIndexNotifyAtLeastDaysVstr;
            sArmIndexNotifyNotBeforeDaysVstr.Text = dataLoan.sArmIndexNotifyNotBeforeDaysVstr;
            sArmIndexEffectiveD.Text              = dataLoan.sArmIndexEffectiveD_rep;
            sArmIndexBasedOnVstr.Text             = dataLoan.sArmIndexBasedOnVstr;
            sArmIndexCanBeFoundVstr.Text          = dataLoan.sArmIndexCanBeFoundVstr;
            sArmIndexAffectInitIRBit.Checked      = dataLoan.sArmIndexAffectInitIRBit;
            sArmIndexLeadDays.Text                = dataLoan.sArmIndexLeadDays_rep;

            Tools.SetDropDownListValue(sArmIndexT, dataLoan.sArmIndexT);
            sArmIndexTLckd.Checked = dataLoan.sArmIndexTLckd;
            Tools.SetDropDownListValue(sFreddieArmIndexT, dataLoan.sFreddieArmIndexT);
            sFreddieArmIndexTLckd.Checked = dataLoan.sFreddieArmIndexTLckd;
            Tools.SetDropDownListValue(sRAdjRoundT, dataLoan.sRAdjRoundT);

            RateFloorPopupFields popupFields = new RateFloorPopupFields();
            popupFields.sIsRAdjFloorRReadOnly = dataLoan.sIsRAdjFloorRReadOnly.ToString();
            popupFields.sNoteIR = dataLoan.sNoteIR_rep;
            popupFields.sRAdjFloorAddR = dataLoan.sRAdjFloorAddR_rep;
            popupFields.sRAdjFloorBaseR = dataLoan.sRAdjFloorBaseR_rep;
            popupFields.sRAdjFloorCalcT = dataLoan.sRAdjFloorCalcT;
            popupFields.sRAdjFloorCalcTLabel = Tools.GetsRAdjFloorCalcTLabel(dataLoan.sRAdjFloorCalcT);
            popupFields.sRAdjFloorLifeCapR = dataLoan.sRAdjFloorLifeCapR_rep;
            popupFields.sRAdjFloorR = dataLoan.sRAdjFloorR_rep;
            popupFields.sRAdjFloorTotalR = dataLoan.sRAdjFloorTotalR_rep;
            popupFields.sRAdjLifeCapR = dataLoan.sRAdjLifeCapR_rep;

            RateFloorPopup.SetRateLockPopupFieldValues(popupFields);

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
