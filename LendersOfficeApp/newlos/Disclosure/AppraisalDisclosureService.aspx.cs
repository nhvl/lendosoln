///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class AppraisalDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(AppraisalDisclosureServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.AppraisalDisclosure, E_ReturnOptionIfNotExist.CreateNew);

            broker.CompanyName = GetString("AppraisalDisclosureCompanyName");
            broker.StreetAddr = GetString("AppraisalDisclosureStreetAddr");
            broker.City = GetString("AppraisalDisclosureCity");
            broker.State = GetString("AppraisalDisclosureState");
            broker.Zip = GetString("AppraisalDisclosureZip");
            broker.Update();

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

	public partial class AppraisalDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new AppraisalDisclosureServiceItem());
        }
	}
}
