﻿namespace LendersOfficeApp.newlos.Disclosure
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web.Services;
    using System.Web.UI.WebControls;

    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.TRID2;
    using LendersOffice.Pdf;
    using LendersOffice.Security;

    [DataContract]
    public class SampleClosingCostFeeViewModel
    {
        [DataMember]
        public IEnumerable<BorrowerClosingCostFeeSection> SectionList { get; set; }

        [DataMember]
        public string VRoot = Tools.VRoot;

        [DataMember]
        public EditableFieldsModel editableFieldsModel { get; set; }

        [DataMember]
        public string sConsummationD { get; set; }

        [DataMember]
        public E_sClosingCostFeeVersionT sClosingCostFeeVersionT { get; set; }

        [DataMember]
        public bool DisableTPAffIfHasContact { get; set; }

        [DataMember]
        public bool sIPiaDyLckd { get; set; }
        [DataMember]
        public string sIPiaDy { get; set; }
        [DataMember]
        public bool sIPerDayLckd { get; set; }
        [DataMember]
        public string sIPerDay { get; set; }

        [DataMember]
        public string sTRIDLoanEstimateTotalLoanCosts { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateTotalClosingCosts { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateTotalOtherCosts { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateLenderCredits { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateTotalAllCosts { get; set; }
    }

    [DataContract]
    public class EditableFieldsModel
    {
        [DataMember]
        public string sLoanEstimateSetLockStatusMethodT { get; set; }
        [DataMember]
        public bool sTRIDIsLoanEstimateRateLocked_True { get; set; }
        [DataMember]
        public string sRLckdExpiredD { get; set; }

        [DataMember]
        public string sTRIDLoanEstimateNoteIRAvailTillD { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateNoteIRAvailTillDTime_Time { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateNoteIRAvailTillDTime_Time_minute { get; set; }
        [DataMember]
        public string sTRIDLoanEstimateNoteIRAvailTillDTime_Time_am { get; set; }
        [DataMember]
        public E_sTimeZoneT sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT { get; set; }
        [DataMember]
        public E_sTRIDLoanEstimateSetLockStatusMethodT sTRIDLoanEstimateSetLockStatusMethodT { get; set; }

        [DataMember]
        public bool sLoanEstScAvailTillDLckd { get; set; }
        [DataMember]
        public string sLoanEstScAvailTillD { get; set; }
        [DataMember]
        public bool sIsPrintTimeForsLoanEstScAvailTillD { get; set; }
        [DataMember]
        public string sLoanEstScAvailTillD_Time { get; set; }
        [DataMember]
        public string sLoanEstScAvailTillD_Time_minute { get; set; }
        [DataMember]
        public string sLoanEstScAvailTillD_Time_am { get; set; }
        [DataMember]
        public E_sTimeZoneT sLoanEstScAvailTillDTimeZoneT { get; set; }
        [DataMember]
        public bool sGfeIsBalloon { get; set; }
        [DataMember]
        public string sEstCloseD { get; set; }
        [DataMember]
        public bool sEstCloseDLckd { get; set; }
        [DataMember]
        public string sConsummationD { get; set; }
        [DataMember]
        public bool sConsummationDLckd { get; set; }

        [DataMember]
        public string sSchedDueD1 { get; set; }

        [DataMember]
        public string sSchedDueD2 { get; set; }

        [DataMember]
        public string sSchedDueD3 { get; set; }

        [DataMember]
        public string sSchedDueD4 { get; set; }

        [DataMember]
        public string sSchedDueD5 { get; set; }

        [DataMember]
        public string sSchedDueD6 { get; set; }

        [DataMember]
        public string sSchedDueD7 { get; set; }

        [DataMember]
        public string sSchedDueD8 { get; set; }

        [DataMember]
        public string sSchedDueD9 { get; set; }

        [DataMember]
        public string sSchedDueD10 { get; set; }

        [DataMember]
        public bool sSchedDueD1Lckd { get; set; }


        [DataMember]
        public Guid sTRIDLenderAgentId;
        [DataMember]
        public string sTRIDLenderName;
        [DataMember]
        public string sTRIDLenderAddr;
        [DataMember]
        public string sTRIDLenderNMLSNum;
        [DataMember]
        public string sTRIDLenderLicenseIDNum;
        [DataMember]
        public string sTRIDLenderPhoneNum;
        [DataMember]
        public Guid sTRIDLoanOfficerAgentId;
        [DataMember]
        public string sTRIDLoanOfficerName;
        [DataMember]
        public string sTRIDLoanOfficerNMLSNum;
        [DataMember]
        public string sTRIDLoanOfficerLicenseIDNum;
        [DataMember]
        public string sTRIDLoanOfficerEmail;
        [DataMember]
        public string sTRIDLoanOfficerPhone;
        [DataMember]
        public string sTRIDLoanOfficerContact;
        [DataMember]
        public Guid sTRIDMortgageBrokerAgentId;
        [DataMember]
        public string sTRIDMortgageBrokerName;
        [DataMember]
        public string sTRIDMortgageBrokerNMLSNum;
        [DataMember]
        public string sTRIDMortgageBrokerLicenseIDNum;

        [DataMember]
        public bool sTRIDLenderIsLocked;
        [DataMember]
        public bool sTRIDLoanOfficerIsLocked;
        [DataMember]
        public bool sTRIDMortgageBrokerIsLocked;

        [DataMember]
        public E_AgentRoleT sTRIDLenderAgentRoleT;
        [DataMember]
        public E_AgentRoleT sTRIDLoanOfficerAgentRoleT;
        [DataMember]
        public E_AgentRoleT sTRIDMortgageBrokerAgentRoleT;

        [DataMember]
        public string sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs { get; set; }

        [DataMember]
        public string sAPR { get; set; }
        [DataMember]
        public string sCFPBTotalInterestPercentLE { get; set; }

        [DataMember]
        public string sTRIDLoanEstimateTotalLoanCosts;
        [DataMember]
        public string sTRIDLoanEstimateTotalOtherCosts;
        [DataMember]
        public string sTRIDLoanEstimateLenderCredits;
        [DataMember]
        public string sTRIDLoanEstimateTotalClosingCosts;
        [DataMember]
        public string sTRIDLoanEstimateBorrowerFinancedClosingCosts;
        [DataMember]
        public string sTRIDLoanEstimateDownPaymentOrFundsFromBorrower;
        [DataMember]
        public string sTRIDCashDeposit;
        [DataMember]
        public string sTRIDLoanEstimateFundsForBorrower;
        [DataMember]
        public string sTRIDLoanEstimateSellerCredits;
        [DataMember]
        public string sTRIDLoanEstimateAdjustmentsAndOtherCredits;
        [DataMember]
        public string sTRIDLoanEstimateCashToClose;
        [DataMember]
        public bool sTRIDLoanEstimateLenderIntendsToServiceLoan_True;
        [DataMember]
        public bool sTRIDLoanEstimateCashToCloseCalcMethodT_Standard;
        [DataMember]
        public string sLastDisclosedGFEArchiveId;
        [DataMember]
        public string sTolerance10BasisLEArchiveId;

        [DataMember]
        public string sLenderCaseNum;
        [DataMember]
        public bool sLenderCaseNumLckd;
    }

    [DataContract]
    public class DataModel
    {
        [DataMember]
        public List<Address> addresses;

        [DataMember]
        public List<ExpenseData> HousingExpenses;

        [DataMember]
        public List<string> EscrowedDescriptions { get; set; }

        [DataMember]
        public List<string> NonEscrowedDescriptions { get; set; }

        [DataMember]
        public List<TridProjectedPayment> ProjectedPayments { get; set; }
    }

    public class ExpenseData
    {
        [DataMember]
        public string amount;
        [DataMember]
        public bool inEscrow;
        [DataMember]
        public string expenseName;
    }

    [DataContract]
    public class Address
    {
        [DataMember]
        public string aBNm;
        [DataMember]
        public string aBStateMail;
        [DataMember]
        public string aBCityMail;
        [DataMember]
        public string aBZipMail;
        [DataMember]
        public string aBAddrMail;

        [DataMember]
        public string aCNm;
        [DataMember]
        public string aCStateMail;
        [DataMember]
        public string aCCityMail;
        [DataMember]
        public string aCZipMail;
        [DataMember]
        public string aCAddrMail;

        [DataMember]
        public bool aBHasSpouse;
        [DataMember]
        public bool sameAddr;
    }

    public partial class LoanEstimate : BaseLoanPage
    {
        public static List<string> EditableFields = new List<string>() {
            "sLoanEstimateSetLockStatusMethodT",
            "sTRIDIsLoanEstimateRateLocked_True",
            "sTRIDLoanEstimateNoteIRAvailTillD",
            "sTRIDLoanEstimateNoteIRAvailTillDTime_Time",
            "sTRIDLoanEstimateNoteIRAvailTillDTime_Time_minute",
            "sTRIDLoanEstimateNoteIRAvailTillDTime_Time_am",
            "sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT",
            "sTRIDLoanEstimateSetLockStatusMethodT",
            "sLoanEstScAvailTillDLckd",
            "sLoanEstScAvailTillD",
            "sIsPrintTimeForsLoanEstScAvailTillD",
            "sLoanEstScAvailTillD_Time",
            "sLoanEstScAvailTillD_Time_minute",
            "sLoanEstScAvailTillD_Time_am",
            "sLoanEstScAvailTillDTimeZoneT",
            "sGfeIsBalloon",
            "sEstCloseD",
            "sEstCloseDLckd",
            "sConsummationD",
            "sConsummationDLckd",
            "sSchedDueD1",
            "sSchedDueD2",
            "sSchedDueD3",
            "sSchedDueD4",
            "sSchedDueD5",
            "sSchedDueD6",
            "sSchedDueD7",
            "sSchedDueD8",
            "sSchedDueD9",
            "sSchedDueD10",
            "sSchedDueD1Lckd",
            "sTRIDLenderAgentId",
            "sTRIDLenderName",
            "sTRIDLenderAddr",
            "sTRIDLenderNMLSNum",
            "sTRIDLenderLicenseIDNum",
            "sTRIDLenderPhoneNum",
            "sTRIDLoanOfficerAgentId",
            "sTRIDLoanOfficerName",
            "sTRIDLoanOfficerNMLSNum",
            "sTRIDLoanOfficerLicenseIDNum",
            "sTRIDLoanOfficerEmail",
            "sTRIDLoanOfficerPhone",
            "sTRIDLoanOfficerContact",
            "sTRIDMortgageBrokerAgentId",
            "sTRIDMortgageBrokerName",
            "sTRIDMortgageBrokerNMLSNum",
            "sTRIDMortgageBrokerLicenseIDNum",
            "sTRIDLenderIsLocked",
            "sTRIDLoanOfficerIsLocked",
            "sTRIDMortgageBrokerIsLocked",
            "sTRIDLenderAgentRoleT",
            "sTRIDLoanOfficerAgentRoleT",
            "sTRIDMortgageBrokerAgentRoleT",
            "sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs",
            "sAPR",
            "sCFPBTotalInterestPercentLE",
            "sTRIDLoanEstimateTotalLoanCosts",
            "sTRIDLoanEstimateTotalOtherCosts",
            "sTRIDLoanEstimateLenderCredits",
            "sTRIDLoanEstimateTotalClosingCosts",
            "sTRIDLoanEstimateBorrowerFinancedClosingCosts",
            "sTRIDLoanEstimateDownPaymentOrFundsFromBorrower",
            "sTRIDCashDeposit",
            "sTRIDLoanEstimateFundsForBorrower",
            "sTRIDLoanEstimateSellerCredits",
            "sTRIDLoanEstimateAdjustmentsAndOtherCredits",
            "sTRIDLoanEstimateCashToClose",
            "sTRIDLoanEstimateLenderIntendsToServiceLoan_True",
            "sTRIDLoanEstimateCashToCloseCalcMethodT_Standard",
            "sLastDisclosedGFEArchiveId",
            "sTolerance10BasisLEArchiveId",
            "sLenderCaseNum",
            "sLenderCaseNumLckd"
        };

        public bool IsArchivePage
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled
                    && RequestHelper.GetSafeQueryString("IsArchivePage") == "true" 
                    && !IsLeadPage;
            }
        }

        protected bool ShowArchiveRecorder
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled && !IsArchivePage && !IsLeadPage;
            }
        }

        protected bool ShowToleranceBasisDDL
        {
            get
            {
                return ShowArchiveRecorder && BrokerUser.HasPermission(Permission.AllowManuallyArchivingGFE);
            }
        }

        protected string FileHasArchiveInPendingStatusFromCoCMessage { get; set; }

        public void InitialiseCFM()
        {
            LenderContact.IsAllowLockableFeature = true;
            LenderContact.AgentRoleT = E_AgentRoleT.Lender;
            LenderContact.RecordIdField = sTRIDLenderAgentId.ClientID;
            LenderContact.CompanyNameField = sTRIDLenderName.ClientID;
            LenderContact.CompanyLicenseField = sTRIDLenderLicenseIDNum.ClientID;
            LenderContact.CompanyLoanOriginatorIdentifierField = sTRIDLenderNMLSNum.ClientID;
            LenderContact.StreetAddressField = sTRIDLenderAddr.ClientID;
            LenderContact.PhoneField = sTRIDLenderPhoneNum.ClientID;
            LenderContact.DisableLinks = this.IsReadOnly;
            LenderContact.DisableAddTo = true;

            LoanOfficerContact.IsAllowLockableFeature = true;
            LoanOfficerContact.AgentRoleT = E_AgentRoleT.LoanOfficer;
            LoanOfficerContact.RecordIdField = sTRIDLoanOfficerAgentId.ClientID;
            LoanOfficerContact.AgentNameField = sTRIDLoanOfficerName.ClientID;
            LoanOfficerContact.AgentLicenseField = sTRIDLoanOfficerLicenseIDNum.ClientID;
            LoanOfficerContact.LoanOriginatorIdentifierField = sTRIDLoanOfficerNMLSNum.ClientID;
            LoanOfficerContact.EmailField = sTRIDLoanOfficerEmail.ClientID;
            LoanOfficerContact.PhoneField = sTRIDLoanOfficerPhone.ClientID;
            LoanOfficerContact.DisableLinks = this.IsReadOnly;
            LoanOfficerContact.DisableAddTo = true;

            MortgageBrokerContact.IsAllowLockableFeature = true;
            MortgageBrokerContact.AgentRoleT = E_AgentRoleT.Broker;
            MortgageBrokerContact.RecordIdField = sTRIDMortgageBrokerAgentId.ClientID;
            MortgageBrokerContact.CompanyNameField = sTRIDMortgageBrokerName.ClientID;
            MortgageBrokerContact.CompanyLicenseField = sTRIDMortgageBrokerLicenseIDNum.ClientID;
            MortgageBrokerContact.CompanyLoanOriginatorIdentifierField = sTRIDMortgageBrokerNMLSNum.ClientID;
            MortgageBrokerContact.DisableLinks = this.IsReadOnly;
            MortgageBrokerContact.DisableAddTo = true;
        }

        protected override void OnInit(EventArgs e)
        {
            RegisterService("cfpb_utils", "/newlos/Test/RActiveGFEService.aspx");

            base.OnInit(e);

            if (IsArchivePage)
            {
                this.PageID = "LoanEstimateArchive";
            }
            else
            {
                this.PageID = "LoanEstimate";
                if (IsLeadPage)
                {
                    PDFPrintClass = typeof(CIFW2015);
                }
            }

            var dataLoan = new CPageData(LoanID, new string[] { "sLastDisclosedGFEArchiveId", "sClosingCostArchive", "sTolerance10BasisLEArchiveId", "sLastDisclosedClosingCostArchive", "sLoanEstimateDatesInfo" });
            dataLoan.InitLoad();

            if (ShowArchiveRecorder)
            {
                SetLastDisclosedLoanEstimateDescription(dataLoan);
            }
            if (IsArchivePage)
            {
                BindDDLArchives(dataLoan);

                if (!IsPostBack)
                {
                    var initialArchiveToDisplay = dataLoan.sClosingCostArchive
                        .FirstOrDefault(a => a.Id == dataLoan.sLastDisclosedGFEArchiveId);

                    if (initialArchiveToDisplay == null)
                    {
                        var tridTargetVersion = dataLoan.sTridTargetRegulationVersionT;
                        initialArchiveToDisplay = dataLoan.sClosingCostArchive.First(a => 
                            a.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate &&
                            ClosingCostArchive.IsDisclosedDataArchive(tridTargetVersion, a));
                    }

                    Tools.SetDropDownListValue(ddlArchives, initialArchiveToDisplay.Id.ToString());
                    this.SetArchiveStatus(dataLoan, initialArchiveToDisplay);
                }
                else
                {
                    // Do not refresh the loan summary info for postbacks on
                    // the LE archive page. This will prevent unnecessary
                    // service calls.
                    this.IsRefreshLoanSummaryInfo = false;
                }
            }
            if (ShowToleranceBasisDDL)
            {
                Bind_sTolerance10LoanEstimateArchiveD_ddl(dataLoan);

                if (!IsPostBack)
                {
                    Tools.SetDropDownListValue(sTolerance10BasisLEArchiveId_ddl, dataLoan.sTolerance10BasisLEArchiveId_rep);
                }

                sTolerance10BasisLEArchiveId.Value = sTolerance10BasisLEArchiveId_ddl.SelectedValue;
            }
            else
            {
                sTolerance10BasisLEArchiveIdSection.Style["display"] = "none";
            }

            this.RegisterJsGlobalVariables("sIsRequireAlternateCashToCloseForNonSellerTransactions", dataLoan.sIsRequireAlternateCashToCloseForNonSellerTransactions);
            this.RegisterJsGlobalVariables("sLoanTransactionInvolvesSeller", dataLoan.sLoanTransactionInvolvesSeller.ToString());
            this.RegisterJsGlobalVariables("IsLienPos2nd", dataLoan.sLienPosT == E_sLienPosT.Second);
        }

        private void SetArchiveStatus(CPageData dataLoan, ClosingCostArchive archiveToDisplay)
        {
            this.ArchiveStatus.Items.Clear();
            var status = archiveToDisplay.Status;

            if (status == ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown)
            {
                this.ArchiveStatus.Items.Add(Tools.CreateEnumListItem("Unknown", ClosingCostArchive.E_ClosingCostArchiveStatus.Unknown));
            }
            else if (status == ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration)
            {
                this.ArchiveStatus.Items.Add(Tools.CreateEnumListItem("Pending Document Generation", ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration));
            }
            else if (status == ClosingCostArchive.E_ClosingCostArchiveStatus.SupersededCoC)
            {
                this.ArchiveStatus.Items.Add(Tools.CreateEnumListItem("Superseded CoC", ClosingCostArchive.E_ClosingCostArchiveStatus.SupersededCoC));
            }

            var tridTargetVersion = dataLoan.sTridTargetRegulationVersionT;
            var mostRecentNonInvalidArchive = dataLoan.sClosingCostArchive.FirstOrDefault(a =>
                a.Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid && 
                a.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate &&
                ClosingCostArchive.IsDisclosedDataArchive(tridTargetVersion, a));

            if (status == ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid ||
                (mostRecentNonInvalidArchive != null && mostRecentNonInvalidArchive.Id == archiveToDisplay.Id))
            {
                this.ArchiveStatus.Items.Add(Tools.CreateEnumListItem("Invalid", ClosingCostArchive.E_ClosingCostArchiveStatus.Invalid));
            }

            // All newer archives than the one that we're displaying
            bool olderArchiveIsMarkedAsIncludedInClosingDisclosure = dataLoan.sClosingCostArchive
                .SkipWhile(a => a.Id != archiveToDisplay.Id)
                .Skip(1)
                .Any(a => a.Status == ClosingCostArchive.E_ClosingCostArchiveStatus.IncludedInClosingDisclosure);

            if (status == ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed ||
                !olderArchiveIsMarkedAsIncludedInClosingDisclosure)
            {
                this.ArchiveStatus.Items.Add(Tools.CreateEnumListItem("Disclosed", ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed));
            }

            bool fileHasDisclosedClosingDisclosureArchive = dataLoan.sClosingCostArchive.Any(a =>
                a.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure
                && a.IsDisclosed);
            bool displayingPendingArchive = status == ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration;

            // We're doing this with our own calculation because the loan may have an archive applied,
            // which will interfere with the last disclosed archive id.
            var lastDisclosedArchive = dataLoan.sClosingCostArchive.FirstOrDefault(a =>
                a.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate && 
                a.IsDisclosed &&
                ClosingCostArchive.IsDisclosedDataArchive(tridTargetVersion, a));
            bool displayingLastDisclosedArchive = lastDisclosedArchive != null
                && archiveToDisplay.Id == lastDisclosedArchive.Id;
            bool lastDisclosedArchiveAssociatedWithLEDates = lastDisclosedArchive != null
                && dataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Any(d => d.ArchiveId == lastDisclosedArchive.Id);
            bool allowUserToSelectIncludedInCD = fileHasDisclosedClosingDisclosureArchive
                && (displayingPendingArchive || (displayingLastDisclosedArchive && !lastDisclosedArchiveAssociatedWithLEDates));

            if (status == ClosingCostArchive.E_ClosingCostArchiveStatus.IncludedInClosingDisclosure ||
                allowUserToSelectIncludedInCD)
            {
                this.ArchiveStatus.Items.Add(Tools.CreateEnumListItem("Included in Closing Disclosure", ClosingCostArchive.E_ClosingCostArchiveStatus.IncludedInClosingDisclosure));
            }

            Tools.SetDropDownListValue(this.ArchiveStatus, status);
        }

        public override bool IsReadOnly
        {
            get
            {
                return IsArchivePage || base.IsReadOnly;
            }
        }

        private void BindDDLArchives(CPageData dataLoan)
        {
            List<ListItem> items = new List<ListItem>();

            var tridTargetVersion = dataLoan.sTridTargetRegulationVersionT;
            var loanArchives = dataLoan.sClosingCostArchive.Where(p =>
                p.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate &&
                ClosingCostArchive.IsDisclosedDataArchive(tridTargetVersion, p));

            foreach (var archive in loanArchives.OrderBy(p => DateTime.Parse(p.DateArchived)))
            {
                items.Add(new ListItem(archive.DateArchived, archive.Id.ToString()));
            }

            ddlArchives.DataTextField = "Text";
            ddlArchives.DataValueField = "Value";
            ddlArchives.DataSource = items;
            ddlArchives.DataBind();
        }

        private void SetLastDisclosedLoanEstimateDescription(CPageData dataLoan)
        {
            if (dataLoan.sHasLastDisclosedLoanEstimateArchive)
            {
                this.LastDisclosedLoanEstimateDesc.InnerText = dataLoan.LastDisclosedLoanEstimateArchive.DateArchived;
            }
            else
            {
                this.LastDisclosedLoanEstimateDesc.InnerText = "NONE";
            }
        }

        private void Bind_sTolerance10LoanEstimateArchiveD_ddl(CPageData dataLoan)
        {
            if (BrokerUser.HasPermission(Permission.AllowManuallyArchivingGFE) || dataLoan.sTolerance10BasisLEArchiveId == Guid.Empty)
            {
                sTolerance10BasisLEArchiveId_ddl.Items.Add(new ListItem("<-- NONE -->", Guid.Empty.ToString("D")) { Selected = true });
            }

            var tridTargetVersion = dataLoan.sTridTargetRegulationVersionT;
            var loanArchives = dataLoan.sClosingCostArchive.Where(p =>
                p.Id == dataLoan.sTolerance10BasisLEArchiveId || (
                p.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate &&
                p.IsDisclosed &&
                ClosingCostArchive.IsToleranceDataArchive(tridTargetVersion, p)));

            foreach (var archive in loanArchives)
            {
                string archiveDate = archive.DateArchived;
                string label = archiveDate;

                if (archive.Id == dataLoan.sTolerance10BasisLEArchiveId)
                {
                    label += " Current Tolerance Basis";
                }
                else if (archive.WasBasisArchive)
                {
                    label += " Prior Tolerance Basis";
                }

                sTolerance10BasisLEArchiveId_ddl.Items.Add(new ListItem(label, archive.Id.ToString("D")));
            }
        }

        /// <summary>
        /// This method exdlues the Aggregate Adjustment fee from the Loan Estimate page.
        /// </summary>
        /// <param name="viewModel">The Closing Cost View Model.</param>
        /// <param name="dataLoan">The loan.</param>
        public static void SetSectionList(SampleClosingCostFeeViewModel viewModel, CPageData dataLoan)
        {
            viewModel.SectionList = dataLoan.sClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.LoanClosingCost);
            BaseClosingCostFeeSection sectionG = viewModel.SectionList.FirstOrDefault(p => p.SectionType == E_IntegratedDisclosureSectionT.SectionG);
            sectionG.ExclusionFilter = p => p.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            RegisterJsScript("ThirdParty/ractive-legacy.min.js");
            RegisterJsScript("loanframework2.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("Ractive-Common.js");
            RegisterJsScript("LQBPopup.js");

            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_TridLoanPurposeType(sTridLPurposeT);


            Tools.Bind_sLT(sLT);
            
            InitialiseCFM();

            bRecordToArchive.Visible = ShowArchiveRecorder && BrokerUser.HasPermission(Permission.AllowManuallyArchivingGFE);
            ArchiveRow.Visible = ShowArchiveRecorder;
            SelectArchiveRow.Visible = IsArchivePage;
            this.ArchiveStatus.Enabled = BrokerUser.HasPermission(Permission.AllowManuallyArchivingGFE);

            CPageData dataLoan; 

            if (IsArchivePage)
            {
                var loanEstimateArchiveFields = ClosingCostArchive.GetLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate);
                dataLoan = new CPageData(LoanID, loanEstimateArchiveFields.Union(CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(LoanEstimate))));
                dataLoan.InitLoad();

                if (dataLoan.sClosingCostArchive.Count != 0)
                {
                    var selectedArchive = dataLoan.sClosingCostArchive.Where(p => p.Id.ToString().Equals(ddlArchives.SelectedValue)).FirstOrDefault();

                    if (selectedArchive == null)
                    {
                        throw new CBaseException("Archive not found", "Archive not found");
                    }

                    if (dataLoan.sLastDisclosedGFEArchiveId != Guid.Empty)
                    {
                        this.LastDisclosedLoanEstimateArchiveDateCell.InnerText = dataLoan.sLastDisclosedClosingCostArchive.DateArchived;
                    }
                    else
                    {
                        this.LastDisclosedLoanEstimateArchiveDateCell.InnerText = "None";
                    }

                    dataLoan.ApplyClosingCostArchive(selectedArchive);

                    Tools.SetDropDownListValue(this.ArchiveStatus, selectedArchive.Status);
                    this.SetArchiveStatus(dataLoan, selectedArchive);
                }
                else
                {
                    throw new CBaseException("No archives found", "No archives found.");
                }
            }
            else
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LoanEstimate));
                dataLoan.InitLoad();
            }

            IsReadOnlyField.Value = this.IsReadOnly.ToString();
            IsArchivePageField.Value = this.IsArchivePage.ToString();

            PurposeRow.Attributes.Add("class",
                LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges) ?
                "show-sTridLPurposeT" :
                "show-sLPurposeT");

            IPreparerFields lender = dataLoan.sTRIDLender;
            LenderContact.IsLocked = lender.IsLocked;
            LenderContact.AgentRoleT = lender.AgentRoleT;
            sTRIDLenderAgentId.Value = lender.AgentId.ToString();
            sTRIDLenderName.Text = lender.CompanyName;
            sTRIDLenderAddr.Text = lender.StreetAddr;
            sTRIDLenderPhoneNum.Text = lender.PhoneOfCompany;
            sTRIDLenderNMLSNum.Text = lender.CompanyLoanOriginatorIdentifier;
            sTRIDLenderLicenseIDNum.Text = lender.LicenseNumOfCompany;

            IPreparerFields loanOfficer = dataLoan.sTRIDLoanOfficer;
            LoanOfficerContact.IsLocked = loanOfficer.IsLocked;
            LoanOfficerContact.AgentRoleT = loanOfficer.AgentRoleT;
            sTRIDLoanOfficerAgentId.Value = loanOfficer.AgentId.ToString();
            sTRIDLoanOfficerName.Text = loanOfficer.PreparerName;
            sTRIDLoanOfficerNMLSNum.Text = loanOfficer.LoanOriginatorIdentifier;
            sTRIDLoanOfficerLicenseIDNum.Text = loanOfficer.LicenseNumOfAgent;
            sTRIDLoanOfficerEmail.Text = loanOfficer.EmailAddr;
            sTRIDLoanOfficerPhone.Text = loanOfficer.Phone;

            IPreparerFields mortgageBroker = dataLoan.sTRIDMortgageBroker;
            MortgageBrokerContact.IsLocked = mortgageBroker.IsLocked;
            MortgageBrokerContact.AgentRoleT = mortgageBroker.AgentRoleT;
            sTRIDMortgageBrokerAgentId.Value = mortgageBroker.AgentId.ToString();
            sTRIDMortgageBrokerName.Text = mortgageBroker.CompanyName;
            sTRIDMortgageBrokerNMLSNum.Text = mortgageBroker.CompanyLoanOriginatorIdentifier;
            sTRIDMortgageBrokerLicenseIDNum.Text = mortgageBroker.LicenseNumOfCompany;

            SampleClosingCostFeeViewModel viewModel = new SampleClosingCostFeeViewModel();
            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;

            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;

            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;

            viewModel.sTRIDLoanEstimateTotalLoanCosts = dataLoan.sTRIDLoanEstimateTotalLoanCosts_rep;
            viewModel.sTRIDLoanEstimateTotalClosingCosts = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
            viewModel.sTRIDLoanEstimateTotalOtherCosts = dataLoan.sTRIDLoanEstimateTotalOtherCosts_rep;
            viewModel.sTRIDLoanEstimateLenderCredits = dataLoan.sTRIDLoanEstimateLenderCredits_rep;
            viewModel.sTRIDLoanEstimateTotalAllCosts = dataLoan.sTRIDLoanEstimateTotalAllCosts_rep;

            SetSectionList(viewModel, dataLoan);

            RegisterJsObject("ClosingCostData", viewModel);
            ClientScript.RegisterHiddenField("loanid", LoanID.ToString());

            sApprVal.Text = dataLoan.sApprVal_rep;

            if (dataLoan.sLPurposeT == E_sLPurposeT.Purchase)
            {
                sPurchasePrice1003.Text = dataLoan.sPurchasePrice1003_rep;
                sApprVal.Visible = false;
                ValueDesc.InnerText = "Sale Price";
            }
            else
            {
                sPurchasePrice1003.Text = dataLoan.sApprVal_rep;
                sPurchasePrice1003.Visible = false;
                ValueDesc.InnerText = "Appraised Value";
            }

            sSpAddr.Value = dataLoan.sSpAddr;
            sSpCity.Value = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;
            sSpZip.SmartZipcode(sSpCity, sSpState);

            List<Address> addresses = new List<Address>();
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData app = dataLoan.GetAppData(i);
                Address address = new Address();
                address.aBAddrMail = app.aBAddrMail;
                address.aBCityMail = app.aBCityMail;
                address.aBNm = app.aBNm;
                address.aBStateMail = app.aBStateMail;
                address.aBZipMail = app.aBZipMail;
                address.aCAddrMail = app.aCAddrMail;
                address.aCCityMail = app.aCCityMail;
                address.aCNm = app.aCNm;
                address.aCStateMail = app.aCStateMail;
                address.aCZipMail = app.aCZipMail;

                address.aBHasSpouse = app.aBHasSpouse;
                address.sameAddr = app.aBAddrMail_SingleLine.Equals(app.aCAddrMail_SingleLine);

                addresses.Add(address);
            }

            DataModel dataModel = new DataModel();
            dataModel.addresses = addresses;

            dataModel.HousingExpenses = new List<ExpenseData>();
            foreach (BaseHousingExpense expense in dataLoan.sHousingExpenses.ExpensesToUse)
            {
                if (expense.MonthlyAmtTotal != 0)
                {
                    ExpenseData data = new ExpenseData();
                    data.amount = expense.MonthlyAmtTotal_rep;
                    data.inEscrow = expense.IsEscrowedAtClosing == E_TriState.Yes;
                    data.expenseName = expense.DescriptionOrDefault;
                    dataModel.HousingExpenses.Add(data);
                }
            }

            dataModel.ProjectedPayments = dataLoan.sTridProjectedPayments;
            if (dataModel.ProjectedPayments != null)
            {
                dataModel.ProjectedPayments.ForEach(p => p.SerializeMoneyRepFields = true);
            }

            RegisterJsObjectWithJsonNetSerializer("DataJSON", dataModel);
            RegisterJsObject("EditableFields", EditableFields);
            RegisterJsObject("EditableFieldsModel", new EditableFieldsModel());

            Bind_sLoanEstimateSetLockStatusMethodT();

            sTerm.Value = dataLoan.sTerm_rep;
            sDue.Value = dataLoan.sDue_rep;
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sTridLPurposeT, dataLoan.sTridLPurposeT);
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);

            sLenderCaseNumLckd.Checked = dataLoan.sLenderCaseNumLckd;
            sLenderCaseNum.Value = dataLoan.sLenderCaseNum;

            //TO-DO
            //sLoanEstimateSetLockStatusMethodT.SelectedValue = dataLoan.sLoanEstimateSetLockStatusMethodT;

            //BUNCH OF TODO

            Tools.Bind_sTimeZoneT(sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT);
            sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT.SelectedValue = "" + (int)dataLoan.sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT;
            sTRIDLoanEstimateNoteIRAvailTillDTime_Time.Text = dataLoan.sTRIDLoanEstimateNoteIRAvailTillDTime_Time;
            sTRIDLoanEstimateNoteIRAvailTillD.Text = dataLoan.sTRIDLoanEstimateNoteIRAvailTillD_rep;

            sTRIDIsLoanEstimateRateLocked_True.Checked = dataLoan.sTRIDIsLoanEstimateRateLocked;
            sTRIDIsLoanEstimateRateLocked_False.Checked = !dataLoan.sTRIDIsLoanEstimateRateLocked;

            Tools.SetDropDownListValue(sTRIDLoanEstimateSetLockStatusMethodT, dataLoan.sTRIDLoanEstimateSetLockStatusMethodT);

            sTRIDLoanEstimateLenderIntendsToServiceLoan_True.Checked = dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan;
            sTRIDLoanEstimateLenderIntendsToServiceLoan_False.Checked = !dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan;

            sLoanEstScAvailTillDLckd.Checked = dataLoan.sLoanEstScAvailTillDLckd;
            sIsPrintTimeForsLoanEstScAvailTillD.Checked = dataLoan.sIsPrintTimeForsLoanEstScAvailTillD;
            Tools.Bind_sTimeZoneT(sLoanEstScAvailTillDTimeZoneT);
            sLoanEstScAvailTillDTimeZoneT.SelectedValue = "" + (int)dataLoan.sLoanEstScAvailTillDTimeZoneT;
            sLoanEstScAvailTillD_Time.Text = dataLoan.sLoanEstScAvailTillD_Time;
            sLoanEstScAvailTillD.Text = dataLoan.sLoanEstScAvailTillD_rep;

            //Loan Terms Section
            sFinalLamt.Text = dataLoan.sFinalLAmt_rep;
            sTridNoteIR.Text = dataLoan.sTridNoteIR_rep;
            sTridProThisMPmt.Text = dataLoan.sTridProThisMPmt_rep;
            sGfeCanLoanBalanceIncrease_True.Checked = dataLoan.sGfeCanLoanBalanceIncrease;
            sPmtAdjRecastStop.Value = dataLoan.sPmtAdjRecastStop_rep;
            sGfeMaxLoanBalance.Text = dataLoan.sGfeMaxLoanBalance_rep;
            sGfeCanRateIncrease_True.Checked = dataLoan.sGfeCanRateIncrease;
            sRAdjCapMon.Value = dataLoan.sRAdjCapMon_rep;
            sRAdj1stCapMon.Value = dataLoan.sRAdj1stCapMon_rep;
            sGfeCanPaymentIncrease_True.Checked = dataLoan.sGfeCanPaymentIncrease;
            sGfeHavePpmtPenalty_True.Checked = dataLoan.sGfeHavePpmtPenalty;
            sGfeMaxPpmtPenaltyAmt.Text = dataLoan.sGfeMaxPpmtPenaltyAmt_rep;
            //TotalPrepmtMonths
            sGfeIsBalloon_True.Enabled = dataLoan.sGfeIsBalloonLckd;
            sGfeIsBalloon_False.Enabled = dataLoan.sGfeIsBalloonLckd;
            sGfeIsBalloon_True.Checked = dataLoan.sGfeIsBalloon;
            sGfeBalloonPmt.Text = dataLoan.sGfeBalloonPmt_rep;
            sDue2.Value = dataLoan.sDue_rep;

            //Projected Payments Section
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;
            sConsummationD.Text = dataLoan.sConsummationD_rep;
            sConsummationDLckd.Checked = dataLoan.sConsummationDLckd;

            sMonthlyNonPINonMIExpenseTotalPITI.Text = dataLoan.sMonthlyNonPINonMIExpenseTotalPITI_rep;

            // Adjustable Payments Table
            IsInterestOnlyPmts.InnerText = dataLoan.sIOnlyMon > 0 ? "Yes, for first " + dataLoan.sIOnlyMon_rep + " payments" : "No";
            IsOptionalPmts.InnerText = dataLoan.sIsOptionArm ? "Yes, for first " + dataLoan.sPmtAdjRecastStop_rep + " payments" : "No";
            IsStepPmts.InnerText = dataLoan.sFinMethT == E_sFinMethT.Graduated ? "Yes, for first " + dataLoan.sGradPmtYrs_rep + " years" : "No";
            IsSeasonalPmts.InnerText = "No";

            //Adjustable Interest Rates Table
            if (dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                IndexAndMarginDesc.InnerText = "Index + Margin";
                IndexAndMarginVal.InnerText = dataLoan.sArmIndexNameVstr + " + " + dataLoan.sRAdjMarginR_rep;

                FrequencyFirstChange.InnerText = "1st payment after " + dataLoan.sRAdj1stCapMon_rep + " months";

                FrequencySubsequentChange.InnerText = "Every " + dataLoan.sRAdjCapMon_rep + " months after first change";

                
            }
            else if (dataLoan.sBuydwnMon1 > 0)
            {
                IndexAndMarginDesc.InnerText = "Step Rate Max Adjustment";

                IndexAndMarginVal.InnerText = dataLoan.sMaxBuydwnR_rep;

                FrequencyFirstChange.InnerText = "1st payment after " + dataLoan.sBuydwnMon1_rep + " months";

                if (dataLoan.sBuydwnMon2 > 0)
                {
                    FrequencySubsequentChange.InnerText = "Every " + dataLoan.sBuydwnMon2_rep + " months";
                }
                else
                {
                    FrequencySubsequentChange.InnerText = "No change after first change";
                }
            }
            else
            {
                IndexAndMarginDesc.InnerText = "Index + Margin";
                IndexAndMarginVal.InnerText = "N/A";
                FrequencyFirstChange.InnerText = "N/A";
                FrequencySubsequentChange.InnerText = "N/A";
                LimitFirstchange.InnerText = "N/A";
                LimitSubsequentChange.InnerText = "N/A";
            }

            sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs.Text = dataLoan.sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs_rep;

            MinMaxInterestRate.InnerText = dataLoan.sMinMaxInterestRate_rep;

            LimitFirstchange.InnerText = dataLoan.sLimitsOnInterestRateFirstChange_rep;
            LimitSubsequentChange.InnerText = dataLoan.sLimitsOnInterestRateSubsequentChanges_rep;

            InitialInterestRate.InnerText = dataLoan.sSchedIR1_rep;

            sAPR.Text = dataLoan.sApr_rep;

            sLateDays.Text = dataLoan.sLateDays;
            sLateChargePc.Text = dataLoan.sLateChargePc;
            sLateChargeBaseDesc.Text = dataLoan.sLateChargeBaseDesc;

            sAssumeLT_Allow.Checked = dataLoan.sAssumeLT == E_sAssumeLT.May || dataLoan.sAssumeLT == E_sAssumeLT.MaySubjectToCondition;
            sAssumeLT_NotAllow.Checked = !sAssumeLT_Allow.Checked;

            sReqPropInsRow.Visible = dataLoan.sReqPropIns;
            liabilityAfterForeclosureRow.Visible = dataLoan.sIsRefinancing;

            sCFPBTotalInterestPercentLE.Text = dataLoan.sCFPBTotalInterestPercentLE_rep;

            sTRIDLoanEstimatePrincipalIn5Yrs.Text = dataLoan.sTRIDLoanEstimatePrincipalIn5Yrs_rep;

            sTRIDLoanEstimateTotalLoanCosts.Text = dataLoan.sTRIDLoanEstimateTotalLoanCosts_rep;
            sTRIDLoanEstimateTotalOtherCosts.Text = dataLoan.sTRIDLoanEstimateTotalOtherCosts_rep;
            sTRIDLoanEstimateLenderCredits.Text = dataLoan.sTRIDLoanEstimateLenderCredits_rep;
            sTRIDLoanEstimateTotalClosingCosts1.Text = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
            sTRIDLoanEstimateTotalClosingCosts2.Text = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
            sTRIDLoanEstimateBorrowerFinancedClosingCosts.Text = dataLoan.sTRIDLoanEstimateBorrowerFinancedClosingCosts_rep;
            sTRIDLoanEstimateDownPaymentOrFundsFromBorrower.Text = dataLoan.sTRIDLoanEstimateDownPaymentOrFundsFromBorrower_rep;
            sTRIDCashDeposit.Text = dataLoan.sTRIDCashDeposit_rep;
            sTRIDLoanEstimateFundsForBorrower.Text = dataLoan.sTRIDLoanEstimateFundsForBorrower_rep;
            sTRIDLoanEstimateSellerCredits.Text = dataLoan.sTRIDLoanEstimateSellerCredits_rep;
            sTRIDLoanEstimateAdjustmentsAndOtherCredits.Text = dataLoan.sTRIDLoanEstimateAdjustmentsAndOtherCredits_rep;
            sTRIDLoanEstimateCashToClose.Text = dataLoan.sTRIDLoanEstimateCashToClose_rep;

            sInterestRateMaxEverStartAfterMon.Value = dataLoan.sInterestRateMaxEverStartAfterMon_rep;
            sTRIDPIPmtSubsequentAdjustFreqMon.Value = dataLoan.sTRIDPIPmtSubsequentAdjustFreqMon_rep;
            sPIPmtFirstChangeAfterMon.Value = dataLoan.sPIPmtFirstChangeAfterMon_rep;
            sHardPlusSoftPrepmtPeriodMonths.Value = dataLoan.sHardPlusSoftPrepmtPeriodMonths_rep;
            sPIPmtMaxEverStartAfterMon.Value = dataLoan.sPIPmtMaxEverStartAfterMon_rep;

            sTRIDInterestRateMaxEver.Text = dataLoan.sTRIDInterestRateMaxEver_rep;
            sTRIDPIPmtMaxEver.Text = dataLoan.sTRIDPIPmtMaxEver_rep;
            sIOnlyMon.Value = dataLoan.sIOnlyMon_rep;

            
            // 4/17/2015 gf - We do not want to display all 3 tables for old files
            // that would display the worst case as the standard.
            if (dataLoan.sFinMethT == E_sFinMethT.ARM && !dataLoan.sRAdjWorstIndex)
            {
                this.ViewAmortizationScheduleBtn.Value = "View Amortization Schedules";
            }

            if (IsLeadPage)
            {
                PageHeader.InnerText = "Initial Fees Worksheet";
            }

            LosConvert m_convertLos = new LosConvert();

            string MIFirstChangeString = "";
            string MISubsequentChangeString = "";
            string MIMaximumPaymentString = "";

            MIMaximumPaymentString = dataLoan.sTRIDPIPmtMaxEver_rep + " after " + dataLoan.sPIPmtMaxEverStartAfterMon_rep + " months.";


            if (dataLoan.sGfeCanPaymentIncrease && dataLoan.sPIPmtFirstChangeAfterMon.HasValue)
            {
                try
                {
                    int index = dataLoan.sPIPmtFirstChangeAfterMon.Value + 1;
                    AmortItem bestAmortItem = dataLoan.GetAmortTable(E_AmortizationScheduleT.BestCase, false).Items[index];
                    AmortItem worstAmortItem = dataLoan.GetAmortTable(E_AmortizationScheduleT.WorstCase, false).Items[index];

                    if (bestAmortItem.PaymentWOMI == worstAmortItem.PaymentWOMI)
                    {
                        MIFirstChangeString = m_convertLos.ToMoneyString(bestAmortItem.PaymentWOMI, FormatDirection.ToRep) + " after " + dataLoan.sPIPmtFirstChangeAfterMon + " months.";
                    }
                    else
                    {
                        MIFirstChangeString = m_convertLos.ToMoneyString(bestAmortItem.PaymentWOMI, FormatDirection.ToRep) + " - " + m_convertLos.ToMoneyString(worstAmortItem.PaymentWOMI, FormatDirection.ToRep) + " after " + dataLoan.sPIPmtFirstChangeAfterMon + " months.";
                    }
                }
                catch (AmortTableInvalidArgumentException)
                {
                    MIFirstChangeString = "Unable to compute";
                }
            }
            else
            {
                MIFirstChangeString = "N/A";
            }

            if (dataLoan.sGfeCanPaymentIncrease && dataLoan.sTRIDPIPmtSubsequentAdjustFreqMon.HasValue && dataLoan.sTRIDPIPmtSubsequentAdjustFreqMon.Value > 0)
            {
                MISubsequentChangeString = "Every " + dataLoan.sTRIDPIPmtSubsequentAdjustFreqMon_rep + " months.";
            }
            else
            {
                MISubsequentChangeString = "N/A";
            }



            MIFirstChange.InnerText = MIFirstChangeString;
            MISubsequentChange.InnerText = MISubsequentChangeString;
            MIMaximumPayment.InnerText = MIMaximumPaymentString;

            sTRIDLoanEstimateCashToCloseCalcMethodT_Alternative.Checked = dataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative;
            sTRIDLoanEstimateCashToCloseCalcMethodT_Standard.Checked = dataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Standard;

            sFinalLAmt2.Text = dataLoan.sFinalLAmt_rep;
            sTRIDTotalPayoffsAndPayments.Text = dataLoan.sTRIDTotalPayoffsAndPayments_rep;

            if (dataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Standard)
            {
                sFinalLAmtRow.Style.Add("display", "none");
                EstimatedPayoffsPaymentsRow.Style.Add("display", "none");
                TotalClosingCostsMinus.Style.Add("display", "none");
            }
            else
            {
                ClosingCostsFinancedRow.Style.Add("display", "none");
                DownPaymentFundsBorrRow.Style.Add("display", "none");
                CashDespotRow.Style.Add("display", "none");
                FundsForBorrRow.Style.Add("display", "none");
                SellerCreditsRow.Style.Add("display", "none");
                AdjustmentsCreditsRow.Style.Add("display", "none");
            }

            if (dataLoan.sHasLoanEstimateArchiveInPendingStatus &&
                dataLoan.sLoanEstimateArchiveInPendingStatus.Source == ClosingCostArchive.E_ClosingCostArchiveSource.ChangeOfCircumstance)
            {
                this.RegisterJsGlobalVariables("FileHasArchiveInPendingStatusFromCoC", true);
                this.FileHasArchiveInPendingStatusFromCoCMessage = ErrorMessages.ArchiveError.ManuallyArchivingWithPendingCoCArchive(
                    dataLoan.sLoanEstimateArchiveInPendingStatus.DateArchived);
            }
        }

        public void Bind_sLoanEstimateSetLockStatusMethodT()
        {

            sTRIDLoanEstimateSetLockStatusMethodT.Items.Add( Tools.CreateEnumListItem("Populate from Front-End Rate Lock", E_sTRIDLoanEstimateSetLockStatusMethodT.PopulateFromFrontEndRateLock));
            sTRIDLoanEstimateSetLockStatusMethodT.Items.Add( Tools.CreateEnumListItem("Set Manually", E_sTRIDLoanEstimateSetLockStatusMethodT.SetManually));
        }

        private static void BindToDataLoan(CPageData dataLoan, string viewModelJson, Guid? idToAddPayment, string editableFieldsJson)
        {
            SampleClosingCostFeeViewModel viewModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<SampleClosingCostFeeViewModel>(viewModelJson);

            dataLoan.sIPiaDyLckd = viewModel.sIPiaDyLckd;
            dataLoan.sIPiaDy_rep = viewModel.sIPiaDy;
            dataLoan.sIPerDayLckd = viewModel.sIPerDayLckd;
            dataLoan.sIPerDay_rep = viewModel.sIPerDay;

            if (!string.IsNullOrEmpty(editableFieldsJson))
            {
                //Bind the editable field data
                EditableFieldsModel editableFieldsModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<EditableFieldsModel>(editableFieldsJson);

                dataLoan.SetArchiveAsPreviousBasisById(dataLoan.sTolerance10BasisLEArchiveId);
                dataLoan.sTolerance10BasisLEArchiveId_rep = editableFieldsModel.sTolerance10BasisLEArchiveId;

                dataLoan.sTRIDIsLoanEstimateRateLocked = editableFieldsModel.sTRIDIsLoanEstimateRateLocked_True;
                dataLoan.sTRIDLoanEstimateSetLockStatusMethodT = editableFieldsModel.sTRIDLoanEstimateSetLockStatusMethodT;

                dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan = editableFieldsModel.sTRIDLoanEstimateLenderIntendsToServiceLoan_True;

                dataLoan.sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT = editableFieldsModel.sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT;
                dataLoan.sLoanEstScAvailTillDLckd = editableFieldsModel.sLoanEstScAvailTillDLckd;
                dataLoan.sIsPrintTimeForsLoanEstScAvailTillD = editableFieldsModel.sIsPrintTimeForsLoanEstScAvailTillD;

                DateTime dt = DateTime.MinValue;
                DateTime.TryParse(editableFieldsModel.sLoanEstScAvailTillD + " " + editableFieldsModel.sLoanEstScAvailTillD_Time + ":" + editableFieldsModel.sLoanEstScAvailTillD_Time_minute + " " + editableFieldsModel.sLoanEstScAvailTillD_Time_am, out dt);
                dataLoan.sLoanEstScAvailTillD = dt;

                dataLoan.sTRIDLoanEstimateNoteIRAvailTillD_rep = editableFieldsModel.sTRIDLoanEstimateNoteIRAvailTillD;

                // Sets just the time portion, sTRIDLoanEstimateNoteIRAvailTillDTime_Time. 
                DateTime justTime = DateTime.Now;
                DateTime.TryParse(DateTime.Now.ToShortDateString() + " " + editableFieldsModel.sTRIDLoanEstimateNoteIRAvailTillDTime_Time + ":" + editableFieldsModel.sTRIDLoanEstimateNoteIRAvailTillDTime_Time_minute + " " + editableFieldsModel.sTRIDLoanEstimateNoteIRAvailTillDTime_Time_am, out justTime);
                dataLoan.sTRIDLoanEstimateNoteIRAvailTillDTime = justTime;

                dataLoan.sGfeIsBalloon = editableFieldsModel.sGfeIsBalloon;
                dataLoan.sLoanEstScAvailTillDTimeZoneT = editableFieldsModel.sLoanEstScAvailTillDTimeZoneT;
                dataLoan.sEstCloseD_rep = editableFieldsModel.sEstCloseD;
                dataLoan.sEstCloseDLckd = editableFieldsModel.sEstCloseDLckd;
                dataLoan.sConsummationD_rep = editableFieldsModel.sConsummationD;
                dataLoan.sConsummationDLckd = editableFieldsModel.sConsummationDLckd;
                dataLoan.sSchedDueD1_rep = editableFieldsModel.sSchedDueD1;
                dataLoan.sSchedDueD1Lckd = editableFieldsModel.sSchedDueD1Lckd;

                //bind the contacts
                IPreparerFields lender = dataLoan.sTRIDLender;
                lender.AgentId = editableFieldsModel.sTRIDLenderAgentId;
                lender.CompanyName = editableFieldsModel.sTRIDLenderName;
                lender.StreetAddr = editableFieldsModel.sTRIDLenderAddr;
                lender.PhoneOfCompany = editableFieldsModel.sTRIDLenderPhoneNum;
                lender.CompanyLoanOriginatorIdentifier = editableFieldsModel.sTRIDLenderNMLSNum;
                lender.LicenseNumOfCompany = editableFieldsModel.sTRIDLenderLicenseIDNum;
                lender.AgentRoleT = editableFieldsModel.sTRIDLenderAgentRoleT;
                lender.IsLocked = editableFieldsModel.sTRIDLenderIsLocked;
                lender.Update();

                IPreparerFields loanOfficer = dataLoan.sTRIDLoanOfficer;
                loanOfficer.AgentId = editableFieldsModel.sTRIDLoanOfficerAgentId;
                loanOfficer.PreparerName = editableFieldsModel.sTRIDLoanOfficerName;
                loanOfficer.LoanOriginatorIdentifier = editableFieldsModel.sTRIDLoanOfficerNMLSNum;
                loanOfficer.LicenseNumOfAgent = editableFieldsModel.sTRIDLoanOfficerLicenseIDNum;
                loanOfficer.EmailAddr = editableFieldsModel.sTRIDLoanOfficerEmail;
                loanOfficer.Phone = editableFieldsModel.sTRIDLoanOfficerPhone;
                loanOfficer.AgentRoleT = editableFieldsModel.sTRIDLoanOfficerAgentRoleT;
                loanOfficer.IsLocked = editableFieldsModel.sTRIDLoanOfficerIsLocked;
                loanOfficer.Update();

                IPreparerFields mortgageBroker = dataLoan.sTRIDMortgageBroker;
                mortgageBroker.AgentId = editableFieldsModel.sTRIDMortgageBrokerAgentId;
                mortgageBroker.CompanyName = editableFieldsModel.sTRIDMortgageBrokerName;
                mortgageBroker.CompanyLoanOriginatorIdentifier = editableFieldsModel.sTRIDMortgageBrokerNMLSNum;
                mortgageBroker.LicenseNumOfCompany = editableFieldsModel.sTRIDMortgageBrokerLicenseIDNum;
                mortgageBroker.AgentRoleT = editableFieldsModel.sTRIDMortgageBrokerAgentRoleT;
                mortgageBroker.IsLocked = editableFieldsModel.sTRIDMortgageBrokerIsLocked;
                mortgageBroker.Update();

                dataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT = editableFieldsModel.sTRIDLoanEstimateCashToCloseCalcMethodT_Standard ? E_CashToCloseCalcMethodT.Standard : E_CashToCloseCalcMethodT.Alternative;

                dataLoan.sLenderCaseNumLckd = editableFieldsModel.sLenderCaseNumLckd;
                if (dataLoan.sLenderCaseNumLckd)
                {
                    dataLoan.sLenderCaseNum = editableFieldsModel.sLenderCaseNum;
                }
            }
        }

        private static string CreateViewModel(CPageData dataLoan)
        {
            SampleClosingCostFeeViewModel viewModel = new SampleClosingCostFeeViewModel();
            SetSectionList(viewModel, dataLoan);
            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;

            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;

            EditableFieldsModel model = new EditableFieldsModel();

            model.sTolerance10BasisLEArchiveId = dataLoan.sTolerance10BasisLEArchiveId_rep;

            model.sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs = dataLoan.sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs_rep;

            model.sTRIDIsLoanEstimateRateLocked_True = dataLoan.sTRIDIsLoanEstimateRateLocked;
            model.sRLckdExpiredD = dataLoan.sRLckdExpiredD_rep;

            model.sTRIDLoanEstimateLenderIntendsToServiceLoan_True = dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan;

            model.sTRIDLoanEstimateSetLockStatusMethodT = dataLoan.sTRIDLoanEstimateSetLockStatusMethodT;

            model.sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT = dataLoan.sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT;
            model.sLoanEstScAvailTillDLckd = dataLoan.sLoanEstScAvailTillDLckd;
            model.sIsPrintTimeForsLoanEstScAvailTillD = dataLoan.sIsPrintTimeForsLoanEstScAvailTillD;

            model.sLoanEstScAvailTillD = dataLoan.sLoanEstScAvailTillD_rep;
            model.sTRIDLoanEstimateNoteIRAvailTillD = dataLoan.sTRIDLoanEstimateNoteIRAvailTillD_rep;

            model.sTRIDLoanEstimateCashToCloseCalcMethodT_Standard = dataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Standard;

            string[] timeVal = dataLoan.sTRIDLoanEstimateNoteIRAvailTillDTime_Time.Split(':');
            if (!string.IsNullOrEmpty(dataLoan.sTRIDLoanEstimateNoteIRAvailTillDTime_Time))
            {
                model.sTRIDLoanEstimateNoteIRAvailTillDTime_Time_am = timeVal[1].Substring(3);
                model.sTRIDLoanEstimateNoteIRAvailTillDTime_Time = timeVal[0];
                model.sTRIDLoanEstimateNoteIRAvailTillDTime_Time_minute = timeVal[1].Substring(0, 2);
            }

            timeVal = dataLoan.sLoanEstScAvailTillD_Time.Split(':');
            model.sLoanEstScAvailTillD_Time_am = timeVal[1].Substring(3);
            model.sLoanEstScAvailTillD_Time = timeVal[0];
            model.sLoanEstScAvailTillD_Time_minute = timeVal[1].Substring(0, 2);

            model.sLoanEstScAvailTillDTimeZoneT = dataLoan.sLoanEstScAvailTillDTimeZoneT;
            model.sEstCloseD = dataLoan.sEstCloseD_rep;
            model.sEstCloseDLckd = dataLoan.sEstCloseDLckd;
            model.sConsummationD = dataLoan.sConsummationD_rep;
            model.sConsummationDLckd = dataLoan.sConsummationDLckd;
            model.sSchedDueD1 = dataLoan.sSchedDueD1_rep;
            model.sSchedDueD1Lckd = dataLoan.sSchedDueD1Lckd;
            model.sLoanEstimateSetLockStatusMethodT = "0";

            model.sSchedDueD2 = dataLoan.sSchedDueD2_rep;
            model.sSchedDueD3 = dataLoan.sSchedDueD3_rep;
            model.sSchedDueD4 = dataLoan.sSchedDueD4_rep;
            model.sSchedDueD5 = dataLoan.sSchedDueD5_rep;
            model.sSchedDueD6 = dataLoan.sSchedDueD6_rep;
            model.sSchedDueD7 = dataLoan.sSchedDueD7_rep;
            model.sSchedDueD8 = dataLoan.sSchedDueD8_rep;
            model.sSchedDueD9 = dataLoan.sSchedDueD9_rep;
            model.sSchedDueD10 = dataLoan.sSchedDueD10_rep;

            model.sLenderCaseNum = dataLoan.sLenderCaseNum;
            model.sLenderCaseNumLckd = dataLoan.sLenderCaseNumLckd;

            IPreparerFields lender = dataLoan.sTRIDLender;
            if (lender.IsValid)
            {
                model.sTRIDLenderAgentId = lender.AgentId;
                model.sTRIDLenderName = lender.CompanyName;
                model.sTRIDLenderNMLSNum = lender.CompanyLoanOriginatorIdentifier;
                model.sTRIDLenderLicenseIDNum = lender.LicenseNumOfCompany;
                model.sTRIDLenderAgentRoleT = lender.AgentRoleT;
                model.sTRIDLenderIsLocked = lender.IsLocked;
                model.sTRIDLenderAddr = lender.StreetAddr;
                model.sTRIDLenderPhoneNum = lender.PhoneOfCompany;
            }
            else
            {
                model.sTRIDLenderAgentRoleT = E_AgentRoleT.Lender;
            }

            IPreparerFields loanOfficer = dataLoan.sTRIDLoanOfficer;
            if (loanOfficer.IsValid)
            {
                model.sTRIDLoanOfficerAgentId = loanOfficer.AgentId;
                model.sTRIDLoanOfficerName = loanOfficer.PreparerName;
                model.sTRIDLoanOfficerNMLSNum = loanOfficer.LoanOriginatorIdentifier;
                model.sTRIDLoanOfficerLicenseIDNum = loanOfficer.LicenseNumOfAgent;
                model.sTRIDLoanOfficerEmail = loanOfficer.EmailAddr;
                model.sTRIDLoanOfficerPhone = loanOfficer.Phone;
                model.sTRIDLoanOfficerAgentRoleT = loanOfficer.AgentRoleT;
                model.sTRIDLoanOfficerIsLocked = loanOfficer.IsLocked;
            }
            else
            {
                model.sTRIDLoanOfficerAgentRoleT = E_AgentRoleT.LoanOfficer;
            }

            IPreparerFields mortgageBroker = dataLoan.sTRIDMortgageBroker;
            if (mortgageBroker.IsValid)
            {
                model.sTRIDMortgageBrokerAgentId = mortgageBroker.AgentId;
                model.sTRIDMortgageBrokerName = mortgageBroker.CompanyName;
                model.sTRIDMortgageBrokerNMLSNum = mortgageBroker.CompanyLoanOriginatorIdentifier;
                model.sTRIDMortgageBrokerLicenseIDNum = mortgageBroker.LicenseNumOfCompany;
                model.sTRIDMortgageBrokerAgentRoleT = mortgageBroker.AgentRoleT;
                model.sTRIDMortgageBrokerIsLocked = mortgageBroker.IsLocked;
            }
            else
            {
                model.sTRIDMortgageBrokerAgentRoleT = E_AgentRoleT.Broker;
            }

            model.sAPR = dataLoan.sApr_rep;
            model.sCFPBTotalInterestPercentLE = dataLoan.sCFPBTotalInterestPercentLE_rep;

            model.sTRIDLoanEstimateTotalLoanCosts = dataLoan.sTRIDLoanEstimateTotalLoanCosts_rep;
            model.sTRIDLoanEstimateTotalOtherCosts = dataLoan.sTRIDLoanEstimateTotalOtherCosts_rep;
            model.sTRIDLoanEstimateLenderCredits = dataLoan.sTRIDLoanEstimateLenderCredits_rep;
            model.sTRIDLoanEstimateTotalClosingCosts = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
            model.sTRIDLoanEstimateBorrowerFinancedClosingCosts = dataLoan.sTRIDLoanEstimateBorrowerFinancedClosingCosts_rep;
            model.sTRIDLoanEstimateDownPaymentOrFundsFromBorrower = dataLoan.sTRIDLoanEstimateDownPaymentOrFundsFromBorrower_rep;
            model.sTRIDCashDeposit = dataLoan.sTRIDCashDeposit_rep;
            model.sTRIDLoanEstimateFundsForBorrower = dataLoan.sTRIDLoanEstimateFundsForBorrower_rep;
            model.sTRIDLoanEstimateSellerCredits = dataLoan.sTRIDLoanEstimateSellerCredits_rep;
            model.sTRIDLoanEstimateAdjustmentsAndOtherCredits = dataLoan.sTRIDLoanEstimateAdjustmentsAndOtherCredits_rep;
            model.sTRIDLoanEstimateCashToClose = dataLoan.sTRIDLoanEstimateCashToClose_rep;
            model.sTRIDLoanEstimateCashToCloseCalcMethodT_Standard = dataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Standard;

            viewModel.editableFieldsModel = model;

            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;

            viewModel.sTRIDLoanEstimateTotalLoanCosts = dataLoan.sTRIDLoanEstimateTotalLoanCosts_rep;
            viewModel.sTRIDLoanEstimateTotalClosingCosts = dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep;
            viewModel.sTRIDLoanEstimateTotalOtherCosts = dataLoan.sTRIDLoanEstimateTotalOtherCosts_rep;
            viewModel.sTRIDLoanEstimateLenderCredits = dataLoan.sTRIDLoanEstimateLenderCredits_rep;
            viewModel.sTRIDLoanEstimateTotalAllCosts = dataLoan.sTRIDLoanEstimateTotalAllCosts_rep;

            return ObsoleteSerializationHelper.JsonSerialize(viewModel);
        }

        [WebMethod]
        public static string GetAvailableClosingCostFeeTypes(Guid loanId, string viewModelJson, string sectionName)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(LoanEstimate));
                dataLoan.InitLoad();

                BrokerDB db = PrincipalFactory.CurrentPrincipal.BrokerDB;

                SampleClosingCostFeeViewModel viewModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<SampleClosingCostFeeViewModel>(viewModelJson);

                FeeSetupClosingCostSet closingCostSet = db.GetUnlinkedClosingCostSet();

                FeeSetupClosingCostFeeSection section = (FeeSetupClosingCostFeeSection)closingCostSet.GetSection(E_ClosingCostViewT.LenderTypeEstimate, sectionName);

                List<BaseClosingCostFee> fees = new List<BaseClosingCostFee>();

                foreach (BorrowerClosingCostFeeSection closingSection in viewModel.SectionList)
                {
                    foreach (BorrowerClosingCostFee fee in closingSection.FilteredClosingCostFeeList)
                    {
                        fees.Add(fee.ConvertToFeeSetupFee());
                    }
                }

                section.SetExcludeFeeList(fees, dataLoan.sClosingCostFeeVersionT, o => o.CanManuallyAddToEditor == false);

                return ObsoleteSerializationHelper.JsonSerialize(section);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
            {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
            });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CalculateData(Guid loanId, string viewModelJson, string editableFieldsJson)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(LoanEstimate));
                dataLoan.InitLoad();


                BindToDataLoan(dataLoan, viewModelJson, null, editableFieldsJson);


                return CreateViewModel(dataLoan);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CreateAgent(Guid loanId, Guid id, bool populateFromRolodex, E_AgentRoleT agentType)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(LoanEstimate));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                CAgentFields agent = dataLoan.GetAgentFields(Guid.Empty);
                if (populateFromRolodex)
                {
                    agent.PopulateFromRolodex(id, BrokerUserPrincipal.CurrentPrincipal);
                }
                else
                {
                    CommonFunctions.CopyEmployeeInfoToAgent(PrincipalFactory.CurrentPrincipal.BrokerId, agent, id);
                }

                agent.AgentRoleT = agentType;
                agent.Update();

                dataLoan.Save();

                return "{\"RecordId\": \"" + agent.RecordId.ToString() + "\"}";
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string Save(Guid loanId, string viewModelJson, string editableFieldsJson)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(LoanEstimate));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);


                BindToDataLoan(dataLoan, viewModelJson, null, editableFieldsJson);

                dataLoan.Save();

                return CreateViewModel(dataLoan);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string RecordDataToArchive(Guid LoanID)
        {
            try
            {
                AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
                if (principal.HasPermission(Permission.AllowManuallyArchivingGFE))
                {
                    CPageData dataLoan = new CPageData(LoanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(LoanEstimate))
                        .Union(LendersOffice.Common.SerializationTypes.ClosingCostArchive.GetLoanPropertyList(
                            LendersOffice.Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate)));

                    dataLoan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);

                    bool hadLoanEstimateArchiveInUnknownStatus = dataLoan.sHasLoanEstimateArchiveInUnknownStatus;
                    string dateOfUnknownLoanEstimateArchive = null;
                    if (hadLoanEstimateArchiveInUnknownStatus)
                    {
                        dateOfUnknownLoanEstimateArchive = dataLoan.sLoanEstimateArchiveInUnknownStatus.DateArchived;
                    }

                    bool hadLoanEstimateArchiveInPendingStatus = dataLoan.sHasLoanEstimateArchiveInPendingStatus;
                    bool pendingArchiveWasFromManualArchive = hadLoanEstimateArchiveInPendingStatus &&
                        dataLoan.sLoanEstimateArchiveInPendingStatus.Source == ClosingCostArchive.E_ClosingCostArchiveSource.Manual;

                    dataLoan.ArchiveClosingCosts(
                        ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate,
                        E_GFEArchivedReasonT.ManuallyArchived,
                        ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration);

                    if (dataLoan.sClosingCostArchive.Count() == 1) // update sGfeInitialDisclosureD iff this the first time archiving.
                    {
                        dataLoan.Set_sGfeInitialDisclosureD(CDateTime.Create(DateTime.Now)
                            , E_GFEArchivedReasonT.InitialDislosureDateSetViaGFE);
                    }

                    try
                    {
                        dataLoan.Save();
                    }
                    catch (PageDataSaveDenied exc)
                    {
                        return SerializationHelper.JsonNetAnonymousSerialize(new { error = exc.UserMessage });
                    }

                    string postArchiveMessage = null;

                    if (hadLoanEstimateArchiveInUnknownStatus) {
                        postArchiveMessage = ErrorMessages.ArchiveError.ManuallyArchivingSetUnknownArchiveToInvalid(
                            dateOfUnknownLoanEstimateArchive);
                    }
                    else if (hadLoanEstimateArchiveInPendingStatus && pendingArchiveWasFromManualArchive) {
                        postArchiveMessage = ErrorMessages.ArchiveError.ManuallyArchivingSetPendingManualArchiveToInvalid;
                    }

                    return SerializationHelper.JsonNetAnonymousSerialize(new
                    {
                        sLastDisclosedGFEArchiveId = dataLoan.sLastDisclosedGFEArchiveId_rep,
                        postArchiveMessage = postArchiveMessage
                    });
                }
                else
                {
                    var usrMsg = "You do not have permission to manually archive the Loan Estimate.";
                    throw new AccessDenied(usrMsg, usrMsg);
                }
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception exc)
            {
                // Have to do this WebMethod.
                // Log the ugly error, and throw a more user-friendly one for UI.
                Tools.LogError(exc);
                
                throw new CBaseException("Unable to record to archive.", exc.Message);
            }
        }

        public static string ClearAgent(Guid loanId, string viewModelJson, Guid recordId)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(LoanEstimate));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                // Bind unsaved data to loan
                BindToDataLoan(dataLoan, viewModelJson, null, null);

                DataSet ds = dataLoan.sAgentDataSet;
                DataTable table = ds.Tables[0];

                // Find agent to be deleted.
                DataRow toBeDeleted = null;
                foreach (DataRow row in table.Rows)
                {
                    if ((string)row["recordId"] == recordId.ToString())
                    {
                        // I could not invoke the Delete() on row object here.
                        // Doing so will modify the collection of table.Rows which cause
                        // the exception to be throw. dd 4/21/2003
                        toBeDeleted = row;
                        break;
                    }
                }

                // If agent found then delete and save. Otherwise do nothing.
                if (toBeDeleted != null)
                {
                    dataLoan.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(toBeDeleted, dataLoan.sSpState, AgentRecordChangeType.DeleteRecord));

                    // OPM 209868 - Clear beneficiary info in any closing cost fee where this agent is the beneficiary.
                    // If we have access to this page/method we can assume that the loan has been migrated.
                    dataLoan.sClosingCostSet.ClearBeneficiary(new Guid((string)toBeDeleted["recordId"]));
                    toBeDeleted.Delete();
                }

                dataLoan.sAgentDataSet = ds;
                dataLoan.Save();

                // Return deserialized data set.
                return CreateViewModel(dataLoan);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string SetArchiveStatus(Guid loanId, Guid archiveId, ClosingCostArchive.E_ClosingCostArchiveStatus status)
        {
            try
            {
                if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowManuallyArchivingGFE))
                {
                    var usrMsg = "You do not have permission to set the archive status.";
                    throw new AccessDenied(usrMsg, usrMsg);
                }

                var loan = CPageData.CreateUsingSmartDependency(
                    loanId,
                    typeof(LoanEstimate));
                loan.InitSave(ConstAppDavid.SkipVersionCheck);
                loan.UpdateArchiveStatus(archiveId, status);
                loan.Save();

                string lastDisclosedArchiveD = loan.sLastDisclosedGFEArchiveId == Guid.Empty ?
                    "None" :
                    loan.sLastDisclosedClosingCostArchive.DateArchived;

                return SerializationHelper.JsonNetAnonymousSerialize(new 
                { 
                    LastDisclosedLoanEstimateArchiveDate = lastDisclosedArchiveD
                });
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }
    }
}
