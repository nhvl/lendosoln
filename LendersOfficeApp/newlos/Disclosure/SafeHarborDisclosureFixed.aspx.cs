﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Disclosure
{
    public partial class SafeHarborDisclosureFixed : BaseLoanPage
    {
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SafeHarborDisclosureFixed));
            dataLoan.InitLoad();

            sIncludeSafeHarborFixedWithRiskyFeaturesYes.Checked = dataLoan.sIncludeSafeHarborFixedWithRiskyFeatures;
            sIncludeSafeHarborFixedWithRiskyFeaturesNo.Checked = !sIncludeSafeHarborFixedWithRiskyFeaturesYes.Checked;
            sIncludeSafeHarborFixedRecLoanYes.Checked = dataLoan.sIncludeSafeHarborFixedRecLoan;
            sIncludeSafeHarborFixedRecLoanNo.Checked = !sIncludeSafeHarborFixedRecLoanYes.Checked;

            sLpTemplateNmSafeHarborFixedWithRiskyFeatures.Text = dataLoan.sLpTemplateNmSafeHarborFixedWithRiskyFeatures;
            sLpTemplateNmSafeHarborFixedWithoutRiskyFeatures.Text = dataLoan.sLpTemplateNmSafeHarborFixedWithoutRiskyFeatures;
            sLpTemplateNmSafeHarborFixedLowestCost.Text = dataLoan.sLpTemplateNmSafeHarborFixedLowestCost;
            sLpTemplateNmSafeHarborFixedRecLoan.Text = dataLoan.sLpTemplateNmSafeHarborFixedRecLoan;
            sFinalLAmtSafeHarborFixedWithRiskyFeatures.Text = dataLoan.sFinalLAmtSafeHarborFixedWithRiskyFeatures_rep;
            sFinalLAmtSafeHarborFixedWithoutRiskyFeatures.Text = dataLoan.sFinalLAmtSafeHarborFixedWithoutRiskyFeatures_rep;
            sFinalLAmtSafeHarborFixedLowestCost.Text = dataLoan.sFinalLAmtSafeHarborFixedLowestCost_rep;
            sFinalLAmtSafeHarborFixedRecLoan.Text = dataLoan.sFinalLAmtSafeHarborFixedRecLoan_rep;
            sDueSafeHarborFixedWithRiskyFeatures.Text = dataLoan.sDueSafeHarborFixedWithRiskyFeatures_rep;
            sDueSafeHarborFixedWithoutRiskyFeatures.Text = dataLoan.sDueSafeHarborFixedWithoutRiskyFeatures_rep;
            sDueSafeHarborFixedLowestCost.Text = dataLoan.sDueSafeHarborFixedLowestCost_rep;
            sDueSafeHarborFixedRecLoan.Text = dataLoan.sDueSafeHarborFixedRecLoan_rep;
            sNoteIRSafeHarborFixedWithRiskyFeatures.Text = dataLoan.sNoteIRSafeHarborFixedWithRiskyFeatures_rep;
            sNoteIRSafeHarborFixedWithoutRiskyFeatures.Text = dataLoan.sNoteIRSafeHarborFixedWithoutRiskyFeatures_rep;
            sNoteIRSafeHarborFixedLowestCost.Text = dataLoan.sNoteIRSafeHarborFixedLowestCost_rep;
            sNoteIRSafeHarborFixedRecLoan.Text = dataLoan.sNoteIRSafeHarborFixedRecLoan_rep;
            sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures.Text = dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures_rep;
            sGfeProThisMPmtAndMInsSafeHarborFixedWithoutRiskyFeatures.Text = dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedWithoutRiskyFeatures_rep;
            sGfeProThisMPmtAndMInsSafeHarborFixedLowestCost.Text = dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedLowestCost_rep;
            sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan.Text = dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan_rep;
            sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures.Text = dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures_rep;
            sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures.Text = dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures_rep;
            sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost.Text = dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost_rep;
            sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan.Text = dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan_rep;
            sEquityCalcSafeHarborFixedWithRiskyFeatures.Text = dataLoan.sEquityCalcSafeHarborFixedWithRiskyFeatures_rep;
            sEquityCalcSafeHarborFixedWithoutRiskyFeatures.Text = dataLoan.sEquityCalcSafeHarborFixedWithoutRiskyFeatures_rep;
            sEquityCalcSafeHarborFixedLowestCost.Text = dataLoan.sEquityCalcSafeHarborFixedLowestCost_rep;
            sEquityCalcSafeHarborFixedRecLoan.Text = dataLoan.sEquityCalcSafeHarborFixedRecLoan_rep;
            sTransNetCashSafeHarborFixedWithRiskyFeatures.Text = dataLoan.sTransNetCashSafeHarborFixedWithRiskyFeatures_rep;
            sTransNetCashSafeHarborFixedWithoutRiskyFeatures.Text = dataLoan.sTransNetCashSafeHarborFixedWithoutRiskyFeatures_rep;
            sTransNetCashSafeHarborFixedLowestCost.Text = dataLoan.sTransNetCashSafeHarborFixedLowestCost_rep;
            sTransNetCashSafeHarborFixedRecLoan.Text = dataLoan.sTransNetCashSafeHarborFixedRecLoan_rep;
            sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures.Text = dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures_rep;
            sOriginatorCompensationTotalAmountSafeHarborFixedWithoutRiskyFeatures.Text = dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedWithoutRiskyFeatures_rep;
            sOriginatorCompensationTotalAmountSafeHarborFixedLowestCost.Text = dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedLowestCost_rep;
            sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan.Text = dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan_rep;

            sSafeHarborFixedNegAmort.Checked = dataLoan.sSafeHarborFixedNegAmort;
            sSafeHarborFixedDemandFeat.Checked = dataLoan.sSafeHarborFixedDemandFeat;
            sSafeHarborFixedPrepayPenalty.Checked = dataLoan.sSafeHarborFixedPrepayPenalty;
            sSafeHarborFixedSharedEquity.Checked = dataLoan.sSafeHarborFixedSharedEquity;
            sSafeHarborFixedIntOnlyPmts.Checked = dataLoan.sSafeHarborFixedIntOnlyPmts;
            sSafeHarborFixedSharedAppreciation.Checked = dataLoan.sSafeHarborFixedSharedAppreciation;
            sSafeHarborFixedBalloonPmntIn7Yrs.Checked = dataLoan.sSafeHarborFixedBalloonPmntIn7Yrs;

            sSafeHarborFixedOurRecLowestRate.Checked = dataLoan.sSafeHarborFixedOurRecLowestRate;
            sSafeHarborFixedOurRecWithoutRiskyFeatures.Checked = dataLoan.sSafeHarborFixedOurRecWithoutRiskyFeatures;
            sSafeHarborFixedOurRecLowestCosts.Checked = dataLoan.sSafeHarborFixedOurRecLowestCosts;
            sSafeHarborFixedOurRecOther.Checked = dataLoan.sSafeHarborFixedOurRecOther;
            sSafeHarborFixedOurRecText.Text = dataLoan.sSafeHarborFixedOurRecText;

            sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures.Text = dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures_rep;
            sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures.Text = dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures_rep;
            sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost.Text = dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost_rep;
            sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan.Text = dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan_rep;

            IPreparerFields SafeHarborFixed = dataLoan.GetPreparerOfForm(E_PreparerFormT.SafeHarborFixed, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            sSafeHarborFixedAgentName.Text = SafeHarborFixed.PreparerName;
            sSafeHarborFixedLoanOriginatorIdentifier.Text = SafeHarborFixed.LoanOriginatorIdentifier;
            sSafeHarborFixedAgentLicense.Text = SafeHarborFixed.LicenseNumOfAgent;
            sSafeHarborFixedPhone.Text = SafeHarborFixed.Phone;
            sSafeHarborFixedCompanyName.Text = SafeHarborFixed.CompanyName;
            sSafeHarborFixedCompanyLoanOriginatorIdentifier.Text = SafeHarborFixed.CompanyLoanOriginatorIdentifier;
            sSafeHarborFixedCompanyLicense.Text = SafeHarborFixed.LicenseNumOfCompany;
            sSafeHarborFixedCompanyPhone.Text = SafeHarborFixed.PhoneOfCompany;
            sSafeHarborFixedCompanyFax.Text = SafeHarborFixed.FaxOfCompany;
            sSafeHarborFixedCompanyStreetAddr.Text = SafeHarborFixed.StreetAddr;
            sSafeHarborFixedCompanyCity.Text = SafeHarborFixed.City;
            sSafeHarborFixedCompanyState.Value = SafeHarborFixed.State;
            sSafeHarborFixedCompanyZip.Text = SafeHarborFixed.Zip;
            CFM.IsLocked = SafeHarborFixed.IsLocked;
            CFM.AgentRoleT = SafeHarborFixed.AgentRoleT;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Safe Harbor Disclosure Fixed";
            this.PageID = "SafeHarborDisclosureFixed";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CAntiSteeringSafeHarborDisclosureFixedPDF);
            if (RequestHelper.GetInt("ispopup", 0) == 0)
            {
                bClose.Visible = false;
                bSave.Visible = false;
            }
            else
            {
                scriptHolderForPopup.Visible = true;
            }

            sSafeHarborFixedCompanyZip.SmartZipcode(sSafeHarborFixedCompanyCity, sSafeHarborFixedCompanyState);
            CFM.Type = "26";
            CFM.AgentNameField = sSafeHarborFixedAgentName.ClientID;
            CFM.LoanOriginatorIdentifierField = sSafeHarborFixedLoanOriginatorIdentifier.ClientID;
            CFM.AgentLicenseField = sSafeHarborFixedAgentLicense.ClientID;
            CFM.PhoneField = sSafeHarborFixedPhone.ClientID;
            CFM.CompanyNameField = sSafeHarborFixedCompanyName.ClientID;
            CFM.CompanyLoanOriginatorIdentifierField = sSafeHarborFixedCompanyLoanOriginatorIdentifier.ClientID;
            CFM.CompanyLicenseField = sSafeHarborFixedCompanyLicense.ClientID;
            CFM.StreetAddressField = sSafeHarborFixedCompanyStreetAddr.ClientID;
            CFM.CityField = sSafeHarborFixedCompanyCity.ClientID;
            CFM.StateField = sSafeHarborFixedCompanyState.ClientID;
            CFM.ZipField = sSafeHarborFixedCompanyZip.ClientID;
            CFM.CompanyPhoneField = sSafeHarborFixedCompanyPhone.ClientID;
            CFM.CompanyFaxField = sSafeHarborFixedCompanyFax.ClientID;
            CFM.IsAllowLockableFeature = true;

            this.RegisterJsScript("LQBPopup.js");
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
