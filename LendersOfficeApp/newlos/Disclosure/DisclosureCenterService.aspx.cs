﻿#region Generated code
namespace LendersOfficeApp.newlos.Disclosure
#endregion
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Events;
    using LendersOffice.ObjLib.TPO;
    using LendersOffice.Security;

    /// <summary>
    /// A background service item for the Disclosure Center page.
    /// </summary>
    public partial class DisclosureCenterServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Constructs a loan data object.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <returns>An initialized loan data object.</returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(DisclosureCenterServiceItem));
        }

        /// <summary>
        /// Triggers a process by name.
        /// </summary>
        /// <param name="methodName">The name of the method to invoke.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(GetNewUniqueGuid):
                    this.GetNewUniqueGuid();
                    break;
                case nameof(GetAvailableTpoDisclosureStatusOptionsForChange):
                    this.GetAvailableTpoDisclosureStatusOptionsForChange();
                    return;
                case nameof(ChangeTpoDisclosureStatus):
                    this.ChangeTpoDisclosureStatus();
                    return;
                case nameof(CalculateInitAprAndLastDiscApr):
                    this.CalculateInitAprAndLastDiscApr();
                    return;
                default:
                    throw new ArgumentException($"Unknown method {methodName}");
            }
        }

        /// <summary>
        /// Creates a new GUID and places it on the page.
        /// </summary>
        protected void GetNewUniqueGuid()
        {
            string newGuid = Guid.NewGuid().ToString();
            SetResult("newGuid", newGuid);
        }

        /// <summary>
        /// Binds loan data from the page to a loan object.
        /// </summary>
        /// <param name="dataLoan">A loan data object.</param>
        /// <param name="dataApp">An application data object.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sInitAPR_rep = this.GetString("sInitAPR");
            dataLoan.sLastDiscAPR_rep = this.GetString("sLastDiscAPR");

            dataLoan.sCalculateInitialLoanEstimate = GetBool("sCalculateInitialLoanEstimate");
            var loanEstimateDateString = GetString("LoanEstimatesTableJson");

            var loanEstimateMetadata = new LoanEstimateDatesMetadata(
                dataLoan.GetClosingCostArchiveById,
                dataLoan.GetConsumerDisclosureMetadataByConsumerId(),
                dataLoan.sLoanRescindableT);

            dataLoan.sLoanEstimateDatesInfo = LoanEstimateDatesInfo.FromJson(loanEstimateDateString, loanEstimateMetadata);

            dataLoan.sCalculateInitialClosingDisclosure = GetBool("sCalculateInitialClosingDisclosure");

            var closingDisclosureDateString = GetString("ClosingDisclosuresTableJSON");

            var closingDisclosureMetadata = new ClosingDisclosureDatesMetadata(
                dataLoan.GetClosingCostArchiveById,
                dataLoan.GetConsumerDisclosureMetadataByConsumerId(),
                dataLoan.sLoanRescindableT,
                dataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);

            dataLoan.sClosingDisclosureDatesInfo = ClosingDisclosureDatesInfo.FromJson(closingDisclosureDateString, closingDisclosureMetadata);

            DisclosureCenter.BindDataForControls(dataLoan, this);
        }

        /// <summary>
        /// Loads data from a loan object to the page.
        /// </summary>
        /// <param name="dataLoan">A loan data object.</param>
        /// <param name="dataApp">An application data object.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sInitAPR", dataLoan.sInitAPR_rep);
            SetResult("sLastDiscAPR", dataLoan.sLastDiscAPR_rep);

            SetResult("sCalculateInitialLoanEstimate", dataLoan.sCalculateInitialLoanEstimate);
            SetResult("LoanEstimatesTableJson", ObsoleteSerializationHelper.JsonSerialize(dataLoan.sLoanEstimateDatesInfo));

            SetResult("sCalculateInitialClosingDisclosure", dataLoan.sCalculateInitialClosingDisclosure);
            SetResult("ClosingDisclosuresTableJSON", ObsoleteSerializationHelper.JsonSerialize(dataLoan.sClosingDisclosureDatesInfo));

            SetResult("sInitialLoanEstimateIssuedD", dataLoan.sInitialLoanEstimateIssuedD_rep);
            SetResult("sInitialLoanEstimateReceivedD", dataLoan.sInitialLoanEstimateReceivedD_rep);
            SetResult("sLastDisclosedLoanEstimateIssuedD", dataLoan.sLastDisclosedLoanEstimateIssuedD_rep);
            SetResult("sLastDisclosedLoanEstimateReceivedD", dataLoan.sLastDisclosedLoanEstimateReceivedD_rep);
            SetResult("sInitialClosingDisclosureIssuedD", dataLoan.sInitialClosingDisclosureIssuedD_rep);
            SetResult("sInitialClosingDisclosureReceivedD", dataLoan.sInitialClosingDisclosureReceivedD_rep);
            SetResult("sPreviewClosingDisclosureIssuedD", dataLoan.sPreviewClosingDisclosureIssuedD_rep);
            SetResult("sPreviewClosingDisclosureReceivedD", dataLoan.sPreviewClosingDisclosureReceivedD_rep);
            SetResult("sLastClosingDisclosureBeforeConsummationIssuedD", dataLoan.sLastClosingDisclosureBeforeConsummationIssuedD_rep);
            SetResult("sLastClosingDisclosureBeforeConsummationReceivedD", dataLoan.sLastClosingDisclosureBeforeConsummationReceivedD_rep);
            SetResult("sFinalClosingDisclosureIssuedD", dataLoan.sFinalClosingDisclosureIssuedD_rep);
            SetResult("sFinalClosingDisclosureReceivedD", dataLoan.sFinalClosingDisclosureReceivedD_rep);
            SetResult("sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentClosingD", dataLoan.sIntialLoanEstimateMailedOrReceivedDeadlineForCurrentClosingD_rep);
            this.SetResult(nameof(dataLoan.sInitialLoanEstimateMailedOrReceivedDeadlineForCurrentAppD), dataLoan.sInitialLoanEstimateMailedOrReceivedDeadlineForCurrentAppD_rep);
            SetResult("sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD", dataLoan.sRevisedLoanEstimateReceivedDeadlineForCurrentClosingD_rep);
            SetResult("sInitialClosingDisclosureMailedDeadlineD", dataLoan.sInitialClosingDisclosureMailedDeadlineD_rep);
            SetResult("sInitialClosingDisclosureReceivedDeadlineD", dataLoan.sInitialClosingDisclosureReceivedDeadlineD_rep);

            DisclosureCenter.LoadDataForControls(dataLoan, this);
        }

        /// <summary>
        /// Gets the available options for a change in a TPO disclosure request status.
        /// </summary>
        private void GetAvailableTpoDisclosureStatusOptionsForChange()
        {
            var type = (TpoDisclosureType)this.GetInt("Type");
            bool enableActive = false, enableCancelled = false, enableCompleted = false;

            switch (type)
            {
                case TpoDisclosureType.InitialDisclosure:
                    var currentInitialDisclosureStatus = (TpoRequestForInitialDisclosureGenerationEventType)this.GetInt("CurrentStatus");
                    var initialDisclosureResultDictionary = Tools.GetAvailableTransitionsForTpoInitialDisclosureRequestGeneration(currentInitialDisclosureStatus);

                    enableActive = initialDisclosureResultDictionary[TpoRequestForInitialDisclosureGenerationEventType.Requested];
                    enableCancelled = initialDisclosureResultDictionary[TpoRequestForInitialDisclosureGenerationEventType.Cancelled];
                    enableCompleted = initialDisclosureResultDictionary[TpoRequestForInitialDisclosureGenerationEventType.Completed];
                    break;

                case TpoDisclosureType.Redisclosure:
                    var currentRedisclosureStatus = (TpoRequestForRedisclosureGenerationEventType)this.GetInt("CurrentStatus");
                    var redisclosureResultDictionary = Tools.GetAvailableTransitionsForTpoRedisclosureRequestGeneration(currentRedisclosureStatus);

                    enableActive = redisclosureResultDictionary[TpoRequestForRedisclosureGenerationEventType.Active];
                    enableCancelled = redisclosureResultDictionary[TpoRequestForRedisclosureGenerationEventType.Cancelled];
                    enableCompleted = redisclosureResultDictionary[TpoRequestForRedisclosureGenerationEventType.Completed];
                    break;

                case TpoDisclosureType.InitialClosingDisclosure:
                    var currentInitialClosingDisclosureStatus = (TpoRequestForInitialClosingDisclosureGenerationEventType)this.GetInt("CurrentStatus");
                    var intialClosingDisclosureResultDictionary = Tools.GetAvailableTransitionsForTpoInitialClosingDisclosureRequestGeneration(currentInitialClosingDisclosureStatus);

                    enableActive = intialClosingDisclosureResultDictionary[TpoRequestForInitialClosingDisclosureGenerationEventType.Active];
                    enableCancelled = intialClosingDisclosureResultDictionary[TpoRequestForInitialClosingDisclosureGenerationEventType.Cancelled];
                    enableCompleted = intialClosingDisclosureResultDictionary[TpoRequestForInitialClosingDisclosureGenerationEventType.Completed];
                    break;
                default:
                    throw new UnhandledEnumException(type);
            }

            this.SetResult("EnableActive", enableActive);
            this.SetResult("EnableCancelled", enableCancelled);
            this.SetResult("EnableCompleted", enableCompleted);
        }

        /// <summary>
        /// Changes the TPO initial disclosure status to a new status if the transition is allowed.
        /// </summary>
        private void ChangeTpoDisclosureStatus()
        {
            var type = (TpoDisclosureType)this.GetInt("Type");
            var eventId = this.GetGuid("EventId", Guid.Empty);
            var notes = this.GetString("notes");

            var dataloan = this.ConstructPageDataClass(this.sLId);
            dataloan.InitSave(ConstAppDavid.SkipVersionCheck);

            switch (type)
            {
                case TpoDisclosureType.InitialDisclosure:
                    var newInitialDisclosureStatus = (TpoRequestForInitialDisclosureGenerationEventType)this.GetInt("newStatus");
                    var newInitialDisclosureEventSettings = new LoanEventAdditionSettings<TpoRequestForInitialDisclosureGenerationEventType>
                    {
                        EventType = newInitialDisclosureStatus,
                        Notes = notes,
                        Source = LoanEventSource.Disclosure.DisclosureCenter
                    };

                    dataloan.sTpoRequestForInitialDisclosureGenerationEventCollection.AddEvent(
                        PrincipalFactory.CurrentPrincipal,
                        newInitialDisclosureEventSettings);
                    break;

                case TpoDisclosureType.Redisclosure:
                    var newRedisclosureStatus = (TpoRequestForRedisclosureGenerationEventType)this.GetInt("newStatus");
                    var newRedisclosureEventSettings = new LoanEventAdditionSettings<TpoRequestForRedisclosureGenerationEventType>
                    {
                        EventType = newRedisclosureStatus,
                        Notes = notes,
                        Source = LoanEventSource.Disclosure.DisclosureCenter
                    };

                    dataloan.sTpoRequestForRedisclosureGenerationEventCollection
                        .FirstOrDefault(collection => collection.HasEvent(eventId))
                        ?.AddEvent(PrincipalFactory.CurrentPrincipal, newRedisclosureEventSettings);
                    break;

                case TpoDisclosureType.InitialClosingDisclosure:
                    var newInitialClosingDisclosureStatus = (TpoRequestForInitialClosingDisclosureGenerationEventType)this.GetInt("newStatus");
                    var newInitialClosingDisclosureEventSettings = new LoanEventAdditionSettings<TpoRequestForInitialClosingDisclosureGenerationEventType>
                    {
                        EventType = newInitialClosingDisclosureStatus,
                        Notes = notes,
                        Source = LoanEventSource.Disclosure.DisclosureCenter
                    };

                    dataloan.sTpoRequestForInitialClosingDisclosureGenerationEventCollection.AddEvent(
                        PrincipalFactory.CurrentPrincipal,
                        newInitialClosingDisclosureEventSettings);
                    break;
                default:
                    throw new UnhandledEnumException(type);
            }

            dataloan.Save();

            this.SetTpoDisclosureData(dataloan, type, eventId);
        }

        /// <summary>
        /// Sets data for the "TPO Portal Disclosure Requests" section.
        /// </summary>
        /// <param name="dataLoan">
        /// The loan with data to set.
        /// </param>
        /// <param name="type">
        /// The type of the disclosure data.
        /// </param>
        /// <param name="eventId">
        /// The ID of the event for re-disclosure changes.
        /// </param>
        private void SetTpoDisclosureData(CPageData dataLoan, TpoDisclosureType type, Guid eventId)
        {
            string statusRep = null, statusInt = null, statusDate = null, statusNotes = null;
            Guid? auditId = null;

            switch (type)
            {
                case TpoDisclosureType.InitialDisclosure:
                    statusRep = dataLoan.sTpoRequestForInitialDisclosureGenerationStatusT_rep;
                    statusInt = dataLoan.sTpoRequestForInitialDisclosureGenerationStatusT.ToString("D");
                    statusDate = dataLoan.sTpoRequestForInitialDisclosureGenerationStatusD_rep;
                    statusNotes = dataLoan.sTpoRequestForInitialDisclosureGenerationStatusNotes;
                    auditId = dataLoan.sTpoRequestForInitialDisclosureGenerationAssociatedAuditId;

                    var displayCompletedWarning = dataLoan.sTpoRequestForInitialDisclosureGenerationEventCollection.LatestEventStatus == TpoRequestForInitialDisclosureGenerationEventType.Completed && !dataLoan.sHasInitialDisclosure;
                    this.SetResult("DisplayCompletedWarning", displayCompletedWarning);
                    break;
                case TpoDisclosureType.Redisclosure:
                    var redisclosureCollection = dataLoan.sTpoRequestForRedisclosureGenerationEventCollection
                        .FirstOrDefault(collection => collection.HasEvent(eventId));

                    statusRep = redisclosureCollection.LatestEventStatusRep;
                    statusInt = redisclosureCollection.LatestEventStatus.ToString("D");
                    statusDate = redisclosureCollection.LatestEventDatetimeRep;
                    statusNotes = redisclosureCollection.LatestEventNotes;
                    auditId = redisclosureCollection.LatestEventAssociatedAuditId;

                    this.SetResult("TpoDisclosureDocumentId", redisclosureCollection.LatestEventAssociatedDocId ?? Guid.Empty);
                    break;

                case TpoDisclosureType.InitialClosingDisclosure:
                    statusRep = dataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusT_rep;
                    statusInt = dataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusT.ToString("D");
                    statusDate = dataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusD_rep;
                    statusNotes = dataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusNotes;
                    auditId = dataLoan.sTpoRequestForInitialClosingDisclosureGenerationAssociatedAuditId;

                    this.SetResult("TpoDisclosureDocumentId", dataLoan.sTpoRequestForInitialClosingDisclosureGenerationStatusAssociatedDocId ?? Guid.Empty);
                    break;

                default:
                    throw new UnhandledEnumException(type);
            }

            this.SetResult("Type", type);
            this.SetResult("EventId", eventId);
            this.SetResult("TpoDisclosureStatusRep", statusRep);
            this.SetResult("TpoDisclosureStatusInt", statusInt);
            this.SetResult("TpoDisclosureStatusDate", statusDate);
            this.SetResult("TpoDisclosureStatusNotes", statusNotes);
            this.SetResult("TpoDisclosureAuditId", auditId);
        }

        /// <summary>
        /// Updates the file to auto-calculate the initial and last disclosed APR values.
        /// </summary>
        private void CalculateInitAprAndLastDiscApr()
        {
            var loan = this.ConstructPageDataClass(this.sLId);
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (!loan.sCalculateInitAprAndLastDiscApr)
            {
                loan.sCalculateInitAprAndLastDiscApr = true;
                loan.Save();
            }

            this.SetResult("sInitAPR", loan.sInitAPR_rep);
            this.SetResult("sLastDiscAPR", loan.sLastDiscAPR_rep);
        }
    }

    /// <summary>
    /// Manages service items for the Disclosure Center page.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
    public partial class DisclosureCenterService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initializes a background service for the Disclosure Center.
        /// </summary>
        protected override void Initialize()
        {
            AddBackgroundItem(string.Empty, new DisclosureCenterServiceItem());
        }
    }
}