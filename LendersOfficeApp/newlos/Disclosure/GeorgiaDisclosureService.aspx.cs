///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class GeorgiaDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(GeorgiaDisclosureServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.GeorgiaDisclosure, E_ReturnOptionIfNotExist.CreateNew);

            broker.CompanyName = GetString("GeorgiaDisclosureCompanyName");
            broker.Update();
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }
	public partial class GeorgiaDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new GeorgiaDisclosureServiceItem());
        }
		
	}
}
