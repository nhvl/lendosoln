﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LendersOfficeApp.newlos.Disclosure {
    
    
    public partial class RespaDates {
        
        /// <summary>
        /// sRespa6D control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespa6D;
        
        /// <summary>
        /// sRespa6DLckd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox sRespa6DLckd;
        
        /// <summary>
        /// ConsumerPortalRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ConsumerPortalRow;
        
        /// <summary>
        /// sConsumerPortalSubmissionToLenderD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sConsumerPortalSubmissionToLenderD;
        
        /// <summary>
        /// sRespaBorrowerNameCollectedD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaBorrowerNameCollectedD;
        
        /// <summary>
        /// sRespaBorrowerNameCollectedDLckd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox sRespaBorrowerNameCollectedDLckd;
        
        /// <summary>
        /// sRespaBorrowerNameFirstEnteredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaBorrowerNameFirstEnteredD;
        
        /// <summary>
        /// BorrowerNameEnteredDates control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater BorrowerNameEnteredDates;
        
        /// <summary>
        /// sRespaBorrowerSsnCollectedD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaBorrowerSsnCollectedD;
        
        /// <summary>
        /// sRespaBorrowerSsnCollectedDLckd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox sRespaBorrowerSsnCollectedDLckd;
        
        /// <summary>
        /// sRespaBorrowerSsnFirstEnteredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaBorrowerSsnFirstEnteredD;
        
        /// <summary>
        /// BorrowerSsnEnteredDates control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater BorrowerSsnEnteredDates;
        
        /// <summary>
        /// sRespaIncomeCollectedD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaIncomeCollectedD;
        
        /// <summary>
        /// sRespaIncomeCollectedDLckd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox sRespaIncomeCollectedDLckd;
        
        /// <summary>
        /// sRespaIncomeFirstEnteredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaIncomeFirstEnteredD;
        
        /// <summary>
        /// Clear_sRespaIncomeFirstEnteredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputButton Clear_sRespaIncomeFirstEnteredD;
        
        /// <summary>
        /// sRespaPropAddressCollectedD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaPropAddressCollectedD;
        
        /// <summary>
        /// sRespaPropAddressCollectedDLckd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox sRespaPropAddressCollectedDLckd;
        
        /// <summary>
        /// sRespaPropAddressFirstEnteredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaPropAddressFirstEnteredD;
        
        /// <summary>
        /// Clear_sRespaPropAddressFirstEnteredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputButton Clear_sRespaPropAddressFirstEnteredD;
        
        /// <summary>
        /// sRespaPropValueCollectedD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaPropValueCollectedD;
        
        /// <summary>
        /// sRespaPropValueCollectedDLckd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox sRespaPropValueCollectedDLckd;
        
        /// <summary>
        /// sRespaPropValueFirstEnteredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaPropValueFirstEnteredD;
        
        /// <summary>
        /// Clear_sRespaPropValueFirstEnteredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputButton Clear_sRespaPropValueFirstEnteredD;
        
        /// <summary>
        /// sRespaLoanAmountCollectedD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaLoanAmountCollectedD;
        
        /// <summary>
        /// sRespaLoanAmountCollectedDLckd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputCheckBox sRespaLoanAmountCollectedDLckd;
        
        /// <summary>
        /// sRespaLoanAmountFirstEnteredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sRespaLoanAmountFirstEnteredD;
        
        /// <summary>
        /// Clear_sRespaLoanAmountFirstEnteredD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputButton Clear_sRespaLoanAmountFirstEnteredD;
    }
}
