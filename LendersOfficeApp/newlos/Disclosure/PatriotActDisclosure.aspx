﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PatriotActDisclosure.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Disclosure.PatriotActDisclosure" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Patriot Act Disclosure</title>
    <link rel="stylesheet" type="text/css" href="~/css/stylesheetnew.css" />
</head>
<body bgcolor="gainsboro">
    <style type="text/css">
        /*<%//Get rid of all other padding and margins%>*/*
        {
            margin: 0;
            padding: 0;
        }
        #MainContent
        {
            position: relative;
            left: 10px;
            width: 800px;
            padding-bottom: 10px;
        }
        .MainRightHeader
        {
            position: relative;
            left: -10px;
            padding-bottom: 10px;
        }
        .FormTableSubheader
        {
            position: relative;
            left: -5px;
            margin-top: 10px;
            margin-bottom: 5px;
            width: 99%;
        }
        .Section
        {
        }
        .HideBullets
        {
            list-style: none;
        }
        label
        {
            font-weight: bold;
            padding-right: 10px;
        }
        .InputList
        {
            padding-top: 10px;
        }
        .InputList li
        {
            padding-bottom: 10px;
        }
        .InputList li label
        {
            float: left;
            width: 213px;
            margin-right: 5px;
        }
        .InputList li input
        {
            width: 75px;
        }
        .InputList li input.Wide
        {
            width: 200px;
        }
        .DateWrapper
        {
            display: inline;
        }
        .DateWrapper img
        {
            vertical-align: bottom;
        }
        .VerificationInfo
        {
            /*<%--Without this, there will occasionally be an 
                  empty space below the id type entry area when 
                  the user switches types--%>*/
            overflow: hidden;
        }
        input, select
        {
            margin-left: 2px;
        }
    </style>

    <script type="text/javascript">

        var VisibilitySettings =
        {
            DriversLicense:
            [
                "State",
                "IdNumber",
                "IssueD",
                "ExpireD"
            ],
            
            Passport:
            [
                "IdNumber",
                "Country",
                "IssueD",
                "ExpireD"
            ],
            
            MilitaryId:
            [
                "IdNumber",
                "Country",
                "ExpireD"
            ],
            
            StateId:
            [
                "State",
                "IdNumber",
                "IssueD",
                "ExpireD"
            ],
            
            GreenCard:
            [
                "IdNumber",
                "ExpireD"
            ],
            
            ImmigrationCard:
            [
                "Country",
                "IdNumber",
                "ExpireD"
            ],
            
            Visa:
            [
                "IdNumber",
                "ExpireD",
                "GovtBranch"
            ],
            
            OtherDocument:
            [
                "Description",
                "IssueD",
                "ExpireD"
            ]
        };
        
        jQuery(document).ready(function($)
        {
            var oldSaveMe = saveMe;
            saveMe = function (bRefresh) {
                // Validate Id types - Primary and secondary IDs cannot be the same (unless blank).
                var aBIdT = $('.aBIdT').val();
                var aB2ndIdT = $('.aB2ndIdT').val();
                if (aBIdT != "Blank" && aBIdT == aB2ndIdT)
                {
                    alert("Borrower Primary and Secondary ID Method of Verification cannot be the same.");
                    return false;
                }

                var aCIdT = $('.aCIdT').val();
                var aC2ndIdT = $('.aC2ndIdT').val();
                if (aCIdT != "Blank" && aCIdT == aC2ndIdT) {
                    alert("Co-borrower Primary and Secondary ID Method of Verification cannot be the same.");
                    return false;
                }

                return oldSaveMe(bRefresh);
            }

            function ApplyVisibilitySettings(settingType, parentElement)
            {
                $(parentElement).children().hide();

                // Just keep all fields hidden if ID type is blank.
                if (settingType == "Blank") {
                    return;
                }
                
                var settings = VisibilitySettings[settingType];
                var len = settings.length;
                for(var i = 0; i < len; i++)
                {
                    //Some elements are complicated and have a Name and a Css value
                    if(settings[i].hasOwnProperty("Name"))
                    {
                        $(parentElement).children('.' + settings[i].Name).show()
                        .attr('class', settings[i].Css + " " + settings[i].Name);
                    }
                    //Others just implicitly have a name
                    else
                    {
                        $(parentElement).children('.' + settings[i]).show()
                        //But we might need to clear out the css
                        .attr('class', settings[i].Name);
                    }
                }
            }
            
            var BorrowerTypes = [ { Type: "Borrower", Abbrev: "B" }, { Type: "Coborrower", Abbrev: "C" } ]
            
            $('#MainContent').delegate('.VerificationType', "change", function(){
                ApplyVisibilitySettings(
                    $(this).find('option:selected').val(), 
                    $(this).parent().next());
            });
            
            $('.VerificationType').change();
            
        });
    
    </script>

    <form id="form1" runat="server">
    <div id="MainContent">
        <div class="MainRightHeader">
            Patriot Act Disclosure
        </div>
        <div id="Borrower">
            <div id="BorrowerInformation" class="Section">
                <div class="FormTableSubheader">
                    Borrower Information
                </div>
                <ul class="HideBullets InputList">
                    <li>
                        <label>
                            Borrower Name</label>
                        <input class="Wide" type="text" id="aBLastFirstNm" disabled="disabled" runat="server" />
                    </li>
                    <li>
                        <label>
                            Borrower Date of Birth</label>
                        <input type="text" id="aBDob_rep" disabled="disabled" runat="server" />
                    </li>
                    <li>
                        <label>
                            Borrower Current Physical Address</label>
                        <input class="Wide" type="text" id="aBAddr_SingleLine" disabled="disabled" runat="server" />
                    </li>
                </ul>
            </div>

            <div id="BorrowerVerification" class="Section">
                <div class="FormTableSubheader">
                    Borrower Primary Identity Verification
                </div>
                <label>
                    Method of Verification
                    <select id="aBIdT" class="VerificationType aBIdT" runat="server">
                        <option value="Blank"></option>
                        <option value="DriversLicense">Driver's License</option>
                        <option value="Passport">Passport</option>
                        <option value="MilitaryId">Military Id</option>
                        <option value="StateId">State Id</option>
                        <option value="GreenCard">Green Card</option>
                        <option value="ImmigrationCard">Immigration Card</option>
                        <option value="Visa">Government Id (Visa)</option>
                        <option value="OtherDocument">Other Document</option>
                    </select></label>
                <div id="BorrowerVerificationInfo" class="VerificationInfo">
                    <label class="Description">
                        Description<input type="text" id="aBIdOther" runat="server" /></label>
                    <label class="State">
                        State<ml:StateDropDownList ID="aBIdState" runat="server"></ml:StateDropDownList>
                    </label>
                    <label class="IdNumber">
                        #<input type="text" id="aBIdNumber" runat="server" /></label>
                    <label class="Country">
                        Country<input type="text" id="aBIdCountry" runat="server" /></label>
                    <label class="IssueD">
                        Issue Date
                        <div class="DateWrapper">
                            <ml:DateTextBox ID="aBIdIssueD" Width="75" preset="date" runat="server" CssClass="mask"></ml:DateTextBox></div>
                    </label>
                    <label class="ExpireD">
                        Expir. Date<div class="DateWrapper">
                            <ml:DateTextBox ID="aBIdExpireD" Width="75" preset="date" runat="server" CssClass="mask"></ml:DateTextBox></div>
                    </label>
                    <label class="GovtBranch">
                        Gov't Branch<input type="text" id="aBIdGovBranch" runat="server" /></label>
                </div>
            </div>

            <div id="BorrowerVerification_2nd" class="Section">
                <div class="FormTableSubheader">
                    Borrower Secondary Identity Verification
                </div>
                <label>
                    Method of Verification
                    <select id="aB2ndIdT" class="VerificationType aB2ndIdT" runat="server">
                        <option value="Blank"></option>
                        <option value="DriversLicense">Driver's License</option>
                        <option value="Passport">Passport</option>
                        <option value="MilitaryId">Military Id</option>
                        <option value="StateId">State Id</option>
                        <option value="GreenCard">Green Card</option>
                        <option value="ImmigrationCard">Immigration Card</option>
                        <option value="Visa">Government Id (Visa)</option>
                        <option value="OtherDocument">Other Document</option>
                    </select></label>
                <div id="BorrowerVerificationInfo_2nd" class="VerificationInfo">
                    <label class="Description">
                        Description<input type="text" id="aB2ndIdOther" runat="server" /></label>
                    <label class="State">
                        State<ml:StateDropDownList ID="aB2ndIdState" runat="server"></ml:StateDropDownList>
                    </label>
                    <label class="IdNumber">
                        #<input type="text" id="aB2ndIdNumber" runat="server" /></label>
                    <label class="Country">
                        Country<input type="text" id="aB2ndIdCountry" runat="server" /></label>
                    <label class="IssueD">
                        Issue Date
                        <div class="DateWrapper">
                            <ml:DateTextBox ID="aB2ndIdIssueD" Width="75" preset="date" runat="server" CssClass="mask"></ml:DateTextBox></div>
                    </label>
                    <label class="ExpireD">
                        Expir. Date<div class="DateWrapper">
                            <ml:DateTextBox ID="aB2ndIdExpireD" Width="75" preset="date" runat="server" CssClass="mask"></ml:DateTextBox></div>
                    </label>
                    <label class="GovtBranch">
                        Gov't Branch<input type="text" id="aB2ndIdGovBranch" runat="server" /></label>
                </div>
            </div>

            <div id="Coborrower">
                <div id="CoborrowerInformation" class="Section">
                    <div class="FormTableSubheader">
                        Co-borrower Information
                    </div>
                    <ul class="HideBullets InputList">
                        <li>
                            <label>
                                Co-borrower Name</label>
                            <input class="Wide" type="text" id="aCLastFirstNm" disabled="disabled" runat="server" />
                        </li>
                        <li>
                            <label>
                                Co-borrower Date of Birth</label>
                            <input type="text" id="aCDob_rep" disabled="disabled" runat="server" />
                        </li>
                        <li>
                            <label>
                                Co-borrower Current Physical Address</label>
                            <input class="Wide" type="text" id="aCAddr_SingleLine" disabled="disabled" runat="server" />
                        </li>
                    </ul>
                </div>
                
                <div id="CoborrowerVerification" class="Section">
                    <div class="FormTableSubheader">
                        Co-borrower Primary Identity Verification
                    </div>
                    <label>
                        Method of Verification
                        <select id="aCIdT" class="VerificationType aCIdT" runat="server">
                            <option value="Blank"></option>
                            <option value="DriversLicense">Driver's License</option>
                            <option value="Passport">Passport</option>
                            <option value="MilitaryId">Military Id</option>
                            <option value="StateId">State Id</option>
                            <option value="GreenCard">Green Card</option>
                            <option value="ImmigrationCard">Immigration Card</option>
                            <option value="Visa">Government Id (Visa)</option>
                            <option value="OtherDocument">Other Document</option>
                        </select></label>
                    <div id="CoborrowerVerificationInfo" class="VerificationInfo">
                        <label class="Description">
                            Description<input type="text" id="aCIdOther" runat="server" /></label>
                        <label class="State">
                            State<ml:StateDropDownList ID="aCIdState" runat="server"></ml:StateDropDownList>
                        </label>
                        <label class="IdNumber">
                            #<input type="text" id="aCIdNumber" runat="server" /></label>
                        <label class="Country">
                            Country<input type="text" id="aCIdCountry" runat="server" /></label>
                        <label class="IssueD">
                            Issue Date
                            <div class="DateWrapper">
                                <ml:DateTextBox ID="aCIdIssueD" Width="75" preset="date" runat="server" CssClass="mask"></ml:DateTextBox></div>
                        </label>
                        <label class="ExpireD">
                            Expir. Date<div class="DateWrapper">
                                <ml:DateTextBox ID="aCIdExpireD" Width="75" preset="date" runat="server" CssClass="mask"></ml:DateTextBox></div>
                        </label>
                        <label class="GovtBranch">
                            Gov't Branch<input type="text" id="aCIdGovBranch" runat="server" /></label>
                    </div>
                </div>

                <div id="CoborrowerVerification_2nd" class="Section">
                    <div class="FormTableSubheader">
                        Co-borrower Secondary Identity Verification
                    </div>
                    <label>
                        Method of Verification
                        <select id="aC2ndIdT" class="VerificationType aC2ndIdT" runat="server">
                            <option value="Blank"></option>
                            <option value="DriversLicense">Driver's License</option>
                            <option value="Passport">Passport</option>
                            <option value="MilitaryId">Military Id</option>
                            <option value="StateId">State Id</option>
                            <option value="GreenCard">Green Card</option>
                            <option value="ImmigrationCard">Immigration Card</option>
                            <option value="Visa">Government Id (Visa)</option>
                            <option value="OtherDocument">Other Document</option>
                        </select></label>
                    <div id="CoborrowerVerificationInfo_2nd" class="VerificationInfo">
                        <label class="Description">
                            Description<input type="text" id="aC2ndIdOther" runat="server" /></label>
                        <label class="State">
                            State<ml:StateDropDownList ID="aC2ndIdState" runat="server"></ml:StateDropDownList>
                        </label>
                        <label class="IdNumber">
                            #<input type="text" id="aC2ndIdNumber" runat="server" /></label>
                        <label class="Country">
                            Country<input type="text" id="aC2ndIdCountry" runat="server" /></label>
                        <label class="IssueD">
                            Issue Date
                            <div class="DateWrapper">
                                <ml:DateTextBox ID="aC2ndIdIssueD" Width="75" preset="date" runat="server" CssClass="mask"></ml:DateTextBox></div>
                        </label>
                        <label class="ExpireD">
                            Expir. Date<div class="DateWrapper">
                                <ml:DateTextBox ID="aC2ndIdExpireD" Width="75" preset="date" runat="server" CssClass="mask"></ml:DateTextBox></div>
                        </label>
                        <label class="GovtBranch">
                            Gov't Branch<input type="text" id="aC2ndIdGovBranch" runat="server" /></label>
                    </div>
                </div>

            </div>
            <div class="FormTableSubheader">
                Resolution of Any Discrepancy
            </div>
            <textarea id="aIdResolution" style="width: 95%; height: 260px" runat="server">
        </textarea>
            <div class="FormTableSubheader">
                Completed By
            </div>
            <label>
                Name<input id="PreparerName" type="text" runat="server" /></label>
            <label>
                Date
                <div class="DateWrapper">
                    <ml:DateTextBox ID="PrepareDate" Width="75" preset="date" runat="server" CssClass="mask"></ml:DateTextBox>
                </div>
            </label>
        </div>
    </form>
</body>
</html>
