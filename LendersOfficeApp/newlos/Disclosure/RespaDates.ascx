﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RespaDates.ascx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.RespaDates" %>
<fieldset class="RespaDates">
    <legend class="FieldLabel">RESPA Six Information</legend>
    <div>
        <div>
            <div>
                <div>RESPA 6 Date</div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespa6D"></ml:DateTextBox>
                    <input type="checkbox" runat="server" id="sRespa6DLckd" />
                    Lckd
                </div>
            </div>
            <div runat="server" id="ConsumerPortalRow">
                <div class="short-line-height">Consumer Portal Submitted to Lender</div>
                <div class="vertical-center">
                    <ml:DateTextBox runat="server" ID="sConsumerPortalSubmissionToLenderD" ReadOnly="true"></ml:DateTextBox>
                </div>
            </div>
        </div>

        <div>
            <div>
                <div></div>
                <div class="FieldLabel">Collected</div>
                <div class="FieldLabel">Entered</div>
            </div>
            <div>
                <div>Borrower Name</div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaBorrowerNameCollectedD"></ml:DateTextBox>
                    <input type="checkbox" runat="server" id="sRespaBorrowerNameCollectedDLckd" />
                    Lckd
                </div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaBorrowerNameFirstEnteredD" ReadOnly="true"></ml:DateTextBox>
                </div>
            </div>
            <asp:Repeater runat="server" ID="BorrowerNameEnteredDates" OnItemDataBound="BorrowerNameEnteredDates_ItemDataBound">
                <ItemTemplate>
                    <div>
                        <div class="right"><span class="pad-right">
                            <ml:EncodedLiteral runat="server" ID="AppLabel1"></ml:EncodedLiteral></span></div>
                        <div>
                            <ml:EncodedLiteral runat="server" ID="aBNm"></ml:EncodedLiteral>
                        </div>
                        <div>
                            <ml:DateTextBox runat="server" ID="aBNmFirstEnteredD" ReadOnly="true" data-field-id="aBNmFirstEnteredD"></ml:DateTextBox>
                        </div>
                        <div>
                            <input type="button" runat="server" id="ClearBorrower" value="Clear" />
                        </div>
                    </div>
                    <div runat="server" id="CoborrowerNameRow">
                        <div class="right"><span class="pad-right">
                            <ml:EncodedLiteral runat="server" ID="AppLabel2"></ml:EncodedLiteral></span></div>
                        <div>
                            <ml:EncodedLiteral runat="server" ID="aCNm"></ml:EncodedLiteral>
                        </div>
                        <div>
                            <ml:DateTextBox runat="server" ID="aCNmFirstEnteredD" ReadOnly="true" data-field-id="aCNmFirstEnteredD"></ml:DateTextBox>
                        </div>
                        <div>
                            <input type="button" runat="server" id="ClearCoborrower" value="Clear" />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div>
                <div>Borrower SSN</div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaBorrowerSsnCollectedD"></ml:DateTextBox>
                    <input type="checkbox" runat="server" id="sRespaBorrowerSsnCollectedDLckd" />
                    Lckd
                </div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaBorrowerSsnFirstEnteredD" ReadOnly="true"></ml:DateTextBox>
                </div>
            </div>
            <asp:Repeater runat="server" ID="BorrowerSsnEnteredDates" OnItemDataBound="BorrowerSsnEnteredDates_ItemDataBound">
                <ItemTemplate>
                    <div>
                        <div class="right"><span class="pad-right">
                            <ml:EncodedLiteral runat="server" ID="AppLabel3"></ml:EncodedLiteral></span></div>
                        <div>
                            <ml:EncodedLiteral runat="server" ID="aBNm2"></ml:EncodedLiteral>
                        </div>
                        <div>
                            <ml:DateTextBox runat="server" ID="aBSsnFirstEnteredD" ReadOnly="true" data-field-id="aBSsnFirstEnteredD"></ml:DateTextBox>
                        </div>
                        <div>
                            <input type="button" runat="server" id="ClearBorrower" value="Clear" />
                        </div>
                    </div>
                    <div runat="server" id="CoborrowerSsnRow">
                        <div class="right"><span class="pad-right">
                            <ml:EncodedLiteral runat="server" ID="AppLabel4"></ml:EncodedLiteral></span></div>
                        <div>
                            <ml:EncodedLiteral runat="server" ID="aCNm2"></ml:EncodedLiteral>
                        </div>
                        <div>
                            <ml:DateTextBox runat="server" ID="aCSsnFirstEnteredD" ReadOnly="true" data-field-id="aCSsnFirstEnteredD"></ml:DateTextBox>
                        </div>
                        <div>
                            <input type="button" runat="server" id="ClearCoborrower" value="Clear" />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div>
                <div>Income</div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaIncomeCollectedD"></ml:DateTextBox>
                    <input type="checkbox" runat="server" id="sRespaIncomeCollectedDLckd" />
                    Lckd
                </div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaIncomeFirstEnteredD" data-field-id="sRespaIncomeFirstEnteredD" ReadOnly="true"></ml:DateTextBox>
                </div>
                <div>
                    <input type="button" runat="server" id="Clear_sRespaIncomeFirstEnteredD" value="Clear" />
                </div>
            </div>
            <div>
                <div>Property Address</div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaPropAddressCollectedD"></ml:DateTextBox>
                    <input type="checkbox" runat="server" id="sRespaPropAddressCollectedDLckd" />
                    Lckd
                </div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaPropAddressFirstEnteredD" data-field-id="sRespaPropAddressFirstEnteredD" ReadOnly="true"></ml:DateTextBox>
                </div>
                <div>
                    <input type="button" runat="server" id="Clear_sRespaPropAddressFirstEnteredD" value="Clear" />
                </div>
            </div>
            <div>
                <div>Property Value</div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaPropValueCollectedD"></ml:DateTextBox>
                    <input type="checkbox" runat="server" id="sRespaPropValueCollectedDLckd" />
                    Lckd
                </div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaPropValueFirstEnteredD" data-field-id="sRespaPropValueFirstEnteredD" ReadOnly="true"></ml:DateTextBox>
                </div>
                <div>
                    <input type="button" runat="server" id="Clear_sRespaPropValueFirstEnteredD" value="Clear" />
                </div>
            </div>
            <div>
                <div>Loan Amount</div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaLoanAmountCollectedD"></ml:DateTextBox>
                    <input type="checkbox" runat="server" id="sRespaLoanAmountCollectedDLckd" />
                    Lckd
                </div>
                <div>
                    <ml:DateTextBox runat="server" ID="sRespaLoanAmountFirstEnteredD" data-field-id="sRespaLoanAmountFirstEnteredD" ReadOnly="true"></ml:DateTextBox>
                </div>
                <div>
                    <input type="button" runat="server" id="Clear_sRespaLoanAmountFirstEnteredD" value="Clear" />
                </div>
            </div>
        </div>
    </div>
</fieldset>
