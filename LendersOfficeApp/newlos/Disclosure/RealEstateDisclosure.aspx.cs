///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
	public partial class RealEstateDisclosure : LendersOfficeApp.newlos.BaseLoanPage
	{

		protected void PageLoad(object sender, System.EventArgs e)
		{
		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "California Real Estate Disclosure";
            this.PageID = "RealEstateDisclosure";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CRealEstateDisclosurePDF);

			RealEstateDisclosureZip.SmartZipcode(RealEstateDisclosureCity, RealEstateDisclosureState);

            RegisterJsScript("jquery-1.6.min.js");
			RegisterJsScript("LQBPopup.js");
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		override protected void LoadData()
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(RealEstateDisclosure));
			dataLoan.InitLoad();

			IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.RealEstateDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject );
			
			RealEstateDisclosureCompanyName.Text = broker.CompanyName;
			RealEstateDisclosureStreetAddr.Text = broker.StreetAddr;
			RealEstateDisclosureCity.Text = broker.City;
			RealEstateDisclosureState.Value = broker.State;
			RealEstateDisclosureZip.Text = broker.Zip;
			RealEstateDisclosurePhoneOfCompany.Text = broker.PhoneOfCompany;
			RealEstateDisclosureFaxOfCompany.Text = broker.FaxOfCompany;
			RealEstateDisclosureLicenseNumOfCompany.Text = broker.LicenseNumOfCompany;

		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
