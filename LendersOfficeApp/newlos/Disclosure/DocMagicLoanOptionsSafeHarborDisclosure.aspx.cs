﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public partial class DocMagicLoanOptionsSafeHarborDisclosure : BaseLoanPage
    {

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(DocMagicLoanOptionsSafeHarborDisclosure));
            dataLoan.InitLoad();

            // Lowest Interest Rate
            sDocMagicAntiSteeringLowestInterestRateLenderName.Text = dataLoan.sDocMagicAntiSteeringLowestInterestRateLenderName;
            sDocMagicAntiSteeringLowestInterestRateDescription.Text = dataLoan.sDocMagicAntiSteeringLowestInterestRateDescription;
            sDocMagicAntiSteeringLowestInterestRateInitialRate.Text = dataLoan.sDocMagicAntiSteeringLowestInterestRateInitialRate_rep;
            sDocMagicAntiSteeringLowestInterestRateDiscountPoints.Text = dataLoan.sDocMagicAntiSteeringLowestInterestRateDiscountPoints_rep;
            sDocMagicAntiSteeringLowestInterestRateOriginationFee.Text = dataLoan.sDocMagicAntiSteeringLowestInterestRateOriginationFee_rep;
            sDocMagicAntiSteeringLowestInterestRateOurCompensation.Text = dataLoan.sDocMagicAntiSteeringLowestInterestRateOurCompensation_rep;
            sDocMagicAntiSteeringLowestInterestRatePrepaymentFeeAmount.Text = dataLoan.sDocMagicAntiSteeringLowestInterestRatePrepaymentFeeAmount_rep;
            sDocMagicAntiSteeringLowestInterestRateBalloonDue.Text = dataLoan.sDocMagicAntiSteeringLowestInterestRateBalloonDue_rep;
            sDocMagicAntiSteeringLowestInterestRateIsNegArmYes.Checked = dataLoan.sDocMagicAntiSteeringLowestInterestRateIsNegArm;
            sDocMagicAntiSteeringLowestInterestRateIsNegArmNo.Checked = !sDocMagicAntiSteeringLowestInterestRateIsNegArmYes.Checked;
            sDocMagicAntiSteeringLowestInterestRateIsDemandFeatureYes.Checked = dataLoan.sDocMagicAntiSteeringLowestInterestRateIsDemandFeature;
            sDocMagicAntiSteeringLowestInterestRateIsDemandFeatureNo.Checked = !sDocMagicAntiSteeringLowestInterestRateIsDemandFeatureYes.Checked;
            sDocMagicAntiSteeringLowestInterestRateIsSharedEquityYes.Checked = dataLoan.sDocMagicAntiSteeringLowestInterestRateIsSharedEquity;
            sDocMagicAntiSteeringLowestInterestRateIsSharedEquityNo.Checked = !sDocMagicAntiSteeringLowestInterestRateIsSharedEquityYes.Checked;
            sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciationYes.Checked = dataLoan.sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciation;
            sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciationNo.Checked = !sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciationYes.Checked;
            
            // Lowest Origination Fees / Discount Points
            sDocMagicAntiSteeringLowestFeeLenderName.Text = dataLoan.sDocMagicAntiSteeringLowestFeeLenderName;
            sDocMagicAntiSteeringLowestFeeDescription.Text = dataLoan.sDocMagicAntiSteeringLowestFeeDescription;
            sDocMagicAntiSteeringLowestFeeInitialRate.Text = dataLoan.sDocMagicAntiSteeringLowestFeeInitialRate_rep;
            sDocMagicAntiSteeringLowestFeeDiscountPoints.Text = dataLoan.sDocMagicAntiSteeringLowestFeeDiscountPoints_rep;
            sDocMagicAntiSteeringLowestFeeOriginationFee.Text = dataLoan.sDocMagicAntiSteeringLowestFeeOriginationFee_rep;
            sDocMagicAntiSteeringLowestFeeOurCompensation.Text = dataLoan.sDocMagicAntiSteeringLowestFeeOurCompensation_rep;
            sDocMagicAntiSteeringLowestFeePrepaymentFeeAmount.Text = dataLoan.sDocMagicAntiSteeringLowestFeePrepaymentFeeAmount_rep;
            sDocMagicAntiSteeringLowestFeeBalloonDue.Text = dataLoan.sDocMagicAntiSteeringLowestFeeBalloonDue_rep;
            sDocMagicAntiSteeringLowestFeeIsNegArmYes.Checked = dataLoan.sDocMagicAntiSteeringLowestFeeIsNegArm;
            sDocMagicAntiSteeringLowestFeeIsNegArmNo.Checked = !sDocMagicAntiSteeringLowestFeeIsNegArmYes.Checked;
            sDocMagicAntiSteeringLowestFeeIsDemandFeatureYes.Checked = dataLoan.sDocMagicAntiSteeringLowestFeeIsDemandFeature;
            sDocMagicAntiSteeringLowestFeeIsDemandFeatureNo.Checked = !sDocMagicAntiSteeringLowestFeeIsDemandFeatureYes.Checked;
            sDocMagicAntiSteeringLowestFeeIsSharedEquityYes.Checked = dataLoan.sDocMagicAntiSteeringLowestFeeIsSharedEquity;
            sDocMagicAntiSteeringLowestFeeIsSharedEquityNo.Checked = !sDocMagicAntiSteeringLowestFeeIsSharedEquityYes.Checked;
            sDocMagicAntiSteeringLowestFeeIsSharedAppreciationYes.Checked = dataLoan.sDocMagicAntiSteeringLowestFeeIsSharedAppreciation;
            sDocMagicAntiSteeringLowestFeeIsSharedAppreciationNo.Checked = !sDocMagicAntiSteeringLowestFeeIsSharedAppreciationYes.Checked;
            
            // Lowest Rate with No Risky Features
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureLenderName.Text = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureLenderName;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureDescription.Text = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureDescription;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureInitialRate.Text = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureInitialRate_rep;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureDiscountPoints.Text = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureDiscountPoints_rep;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureOriginationFee.Text = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureOriginationFee_rep;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureOurCompensation.Text = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureOurCompensation_rep;
            sDocMagicAntiSteeringLowestRateNoRiskyFeaturePrepaymentFeeAmount.Text = "$0.00"; // This is constant
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureBalloonDue.Text = "0";
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            // this.PDFPrintClass = typeof(LendersOffice.Pdf.DocMagicLoanOptionsSafeHarborDisclosure); // does not exist

            // Lowest Interest Rate

            // Lowest Origination Fees / Discount Points

            // Lowest Rate with No Risky Features
            sDocMagicAntiSteeringLowestRateNoRiskyFeaturePrepaymentFeeAmount.Enabled = false;
            sDocMagicAntiSteeringLowestRateNoRiskyFeaturePrepaymentFeeAmount.ReadOnly = true;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureBalloonDue.Enabled = false;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureBalloonDue.ReadOnly = true;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsNegArmYes.Enabled = false;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsNegArmNo.Enabled = false;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsDemandFeatureYes.Enabled = false;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsDemandFeatureNo.Enabled = false;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedEquityYes.Enabled = false;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedEquityNo.Enabled = false;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedAppreciationYes.Enabled = false;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedAppreciationNo.Enabled = false;

            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsNegArmNo.Checked = true;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsDemandFeatureNo.Checked = true;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedEquityNo.Checked = true;
            sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedAppreciationNo.Checked = true;

            LoadData();
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IncludeStyleSheet(VirtualRoot + "/css/stylesheet.css");
            IncludeStyleSheet(VirtualRoot + "/css/stylesheetnew.css");
            this.PageTitle = "DocMagic Loan Options Disclosure with Anti-Steering Safe Harbor";
            this.PageID = "DocMagicLoanOptionsSafeHarborDisclosure";

            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
