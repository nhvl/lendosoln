///
/// Author: Matthew Flynn
/// 
using System;
using System.Collections;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Disclosure
{
	public partial class EqualCredit : LendersOfficeApp.newlos.BaseLoanPage
	{


        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Equal Credit Opportunity Act";
            this.PageID = "EqualCredit";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CEqualCreditPDF);

			ECOAAgentZip.SmartZipcode(ECOAAgentCity, ECOAAgentState);

            RegisterJsScript("jquery-1.6.min.js");
			RegisterJsScript("LQBPopup.js");
        }

		protected override void LoadData() 
		{
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(EqualCredit));
			dataLoan.InitLoad();

			IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.ECOA, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 

			ECOACompanyName.Text = broker.CompanyName;

			// For the address, we use the ECOA entry in the contact list, if it exists.
			// If not, we try to parse from the Broker's ECOA contact info.  We only
			// save to the agent list.

			CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ECOA, E_ReturnOptionIfNotExist.ReturnEmptyObject);

			if (agent.CompanyName != "" || agent.StreetAddr != "" || agent.City != "" || agent.State != "" || agent.Zip != "") 
			{

				ECOAAgentCompanyName.Text = agent.CompanyName;
				ECOAAgentStreetAddr.Text = agent.StreetAddr;
				ECOAAgentCity.Text = agent.City;
				ECOAAgentState.Value = agent.State;
				ECOAAgentZip.Text = agent.Zip;

			} 
			else 
			{
				Guid brokerID = LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.BrokerId;
				if (brokerID != Guid.Empty)
				{
					LendersOffice.Admin.BrokerDB brokerdb = this.Broker;

					// Unfortunately, we store the broker's default ECOA address
					// as a string, so we have to attempt to parse.  If the parsing
					// doesn't produce expected results, they can edit and save to the
					// associated ECOA agent.

					string[] lines = brokerdb.ECOAAddressDefault.Split(Environment.NewLine.ToCharArray());

					ArrayList addressBuilder = new ArrayList();
					foreach (string str in lines)
					{
						if (str.TrimWhitespaceAndBOM() != string.Empty)
						{
							addressBuilder.Add(str);
						}
					}
                    
					if (addressBuilder.Count >= 3)
					{
						CommonLib.Address ecoaAddress = new CommonLib.Address();
						ecoaAddress.ParseCityStateZip((string) addressBuilder[2]);
						ECOAAgentCompanyName.Text = (string) addressBuilder[0];
						ECOAAgentStreetAddr.Text = (string) addressBuilder[1];

						ECOAAgentCity.Text = ecoaAddress.City;
						ECOAAgentState.Value = ecoaAddress.State;
						ECOAAgentZip.Text = (ecoaAddress.Zipcode.Length > 5) ? ecoaAddress.Zipcode.Substring(0, 5) : ecoaAddress.Zipcode;
					}
				}
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
