﻿namespace LendersOfficeApp.newlos.Disclosure
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web.Services;
    using System.Web.UI.WebControls;

    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;

    public partial class SellerClosingDisclosure : BaseLoanPage
    {
        [DataContract]
        public class SellerClosingDisclosureView
        {
            [DataMember]
            public IEnumerable<SellerClosingCostFeeSection> SectionList { get; set; }

            [DataMember]
            public string VRoot = Tools.VRoot;

            [DataMember]
            public bool sIsRequireFeesFromDropDown { get; set; }

            [DataMember]
            public string sConsummationD { get; set; }

            [DataMember]
            public E_sClosingCostFeeVersionT sClosingCostFeeVersionT { get; set; }

            [DataMember]
            public bool DisableTPAffIfHasContact { get; set; }

            [DataMember]
            public bool sIPiaDyLckd { get; set; }
            [DataMember]
            public string sIPiaDy { get; set; }
            [DataMember]
            public bool sIPerDayLckd { get; set; }
            [DataMember]
            public string sIPerDay { get; set; }
            [DataMember]
            public bool CanReadDFLP { get; set; }
            [DataMember]
            public bool CanSetDFLP { get; set; }
        }

        protected override void OnInit(EventArgs e)
        {
            RegisterService("cfpb_utils", "/newlos/Test/RActiveGFEService.aspx");

            base.OnInit(e);

            if (this.IsArchivePage)
            {
                this.PageID = "SellerClosingDisclosureArchive";
            }
            else
            {
                this.PageID = "SellerClosingDisclosure";
            }
        }
        protected bool IsArchivePage
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled && RequestHelper.GetSafeQueryString("IsArchivePage") == "true";
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                return IsArchivePage || base.IsReadOnly;
            }
        }
        
        private void BindBeneficiaryDropDown(Repeater repeater)
        {
            DropDownList dl = new DropDownList();
            RolodexDB.PopulateAgentTypeDropDownList(dl);
            repeater.DataSource = dl.Items;
            repeater.DataBind();
        }

        public void BindPercentRepeaters()
        {
            List<ListItem> Items = new List<ListItem>();

            Items.Add(Tools.CreateEnumListItem("Loan Amount", E_PercentBaseT.LoanAmount));
            Items.Add(Tools.CreateEnumListItem("Purchase Price", E_PercentBaseT.SalesPrice));
            Items.Add(Tools.CreateEnumListItem("Appraisal Value", E_PercentBaseT.AppraisalValue));
            Items.Add(Tools.CreateEnumListItem("Total Loan Amount", E_PercentBaseT.TotalLoanAmount));

            PercentRepeater.DataSource = Items;
            PercentRepeater.DataBind();

            PercentRepeater2.DataSource = Items;
            PercentRepeater2.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            RegisterJsScript("ractive-0.7.1.min.js");
            RegisterJsScript("loanframework2.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterJsScript("mask.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("Ractive-Common.js");
            RegisterJsScript("LQBPopup.js");

            CPageData dataLoan;

            if (IsArchivePage)
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SellerClosingDisclosure));
                dataLoan.InitLoad();
                if (dataLoan.sClosingCostArchive.Count != 0)
                {
                    var tridTargetVersion = dataLoan.sTridTargetRegulationVersionT;
                    var latestArchive = dataLoan.sClosingCostArchive.First(a => ClosingCostArchive.IsDisclosedDataArchive(tridTargetVersion, a));
                    dataLoan.ApplyClosingCostArchive(latestArchive);
                }
                else
                {
                    throw new CBaseException("Archive not found", "Archive not found");
                }
            }
            else
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SellerClosingDisclosure));
                dataLoan.InitLoad();
            }
            
            SellerClosingDisclosureView viewModel = new SellerClosingDisclosureView();
            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;

            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;

            viewModel.sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown;
            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;

            viewModel.CanReadDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead);
            viewModel.CanSetDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite);

            viewModel.SectionList = dataLoan.sSellerResponsibleClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate);
            RegisterJsObject("ClosingCostData", viewModel);
            ClientScript.RegisterHiddenField("loanid", LoanID.ToString());

            BindPercentRepeaters();

            BindBeneficiaryDropDown(BeneficiaryRepeater);
        }

        private static void BindToDataLoan(CPageData dataLoan, string viewModelJson)
        {
            SellerClosingDisclosureView viewModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<SellerClosingDisclosureView>(viewModelJson);
            
            dataLoan.sSellerResponsibleClosingCostSet.UpdateWith((IEnumerable<BaseClosingCostFeeSection>)viewModel.SectionList);

            dataLoan.sIPiaDyLckd = viewModel.sIPiaDyLckd;
            dataLoan.sIPiaDy_rep = viewModel.sIPiaDy;
            dataLoan.sIPerDayLckd = viewModel.sIPerDayLckd;
            dataLoan.sIPerDay_rep = viewModel.sIPerDay;
        }

        private static string CreateViewModel(CPageData dataLoan)
        {
            SellerClosingDisclosureView viewModel = new SellerClosingDisclosureView();
            viewModel.SectionList = dataLoan.sSellerResponsibleClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate);
            viewModel.DisableTPAffIfHasContact = dataLoan.sBranchChannelT == E_BranchChannelT.Retail || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale || dataLoan.sBranchChannelT == E_BranchChannelT.Broker;

            viewModel.sIsRequireFeesFromDropDown = dataLoan.sIsRequireFeesFromDropDown;
            viewModel.sConsummationD = dataLoan.sConsummationD_rep;
            viewModel.sClosingCostFeeVersionT = dataLoan.sClosingCostFeeVersionT;

            viewModel.sIPiaDyLckd = dataLoan.sIPiaDyLckd;
            viewModel.sIPiaDy = dataLoan.sIPiaDy_rep;
            viewModel.sIPerDayLckd = dataLoan.sIPerDayLckd;
            viewModel.sIPerDay = dataLoan.sIPerDay_rep;

            viewModel.CanReadDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead);
            viewModel.CanSetDFLP = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite);

            return ObsoleteSerializationHelper.JsonSerialize(viewModel);
        }

        [WebMethod]
        public static string GetAvailableClosingCostFeeTypes(Guid loanId, string viewModelJson, string sectionName)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(SellerClosingDisclosure));
                dataLoan.InitLoad();

                BrokerDB db = PrincipalFactory.CurrentPrincipal.BrokerDB;

                BindToDataLoan(dataLoan, viewModelJson);

                SellerClosingDisclosureView viewModel = ObsoleteSerializationHelper.JsonDeserializeAndSanitize<SellerClosingDisclosureView>(viewModelJson);

                FeeSetupClosingCostSet closingCostSet = db.GetUnlinkedClosingCostSet();
                FeeSetupClosingCostFeeSection section = (FeeSetupClosingCostFeeSection)closingCostSet.GetSection(E_ClosingCostViewT.LenderTypeEstimate, sectionName);

                List<BaseClosingCostFee> fees = new List<BaseClosingCostFee>();

                foreach (var closingSection in viewModel.SectionList)
                {
                    foreach (SellerClosingCostFee fee in closingSection.FilteredClosingCostFeeList)
                    {
                        fees.Add(fee.ConvertToFeeSetupFee());
                    }
                }

                section.SetExcludeFeeList(fees, dataLoan.sClosingCostFeeVersionT, o => o.CanManuallyAddToEditor == false);

                List<object> subFeesBySourceFeeTypeId = section.FilteredClosingCostFeeList
                    .Select(f => new {
                        SourceFeeTypeId = f.ClosingCostFeeTypeId,
                        SubFees = dataLoan.sSellerResponsibleClosingCostSet.GetFees(fee => fee.SourceFeeTypeId == f.ClosingCostFeeTypeId).ToList()
                    }).ToList<object>();

                return SerializationHelper.JsonNetAnonymousSerialize(new { Section = section, SubFeesBySourceFeeTypeId = subFeesBySourceFeeTypeId });
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CalculateData(Guid loanId, string viewModelJson)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(SellerClosingDisclosure));
                dataLoan.InitLoad();

                BindToDataLoan(dataLoan, viewModelJson);

                return CreateViewModel(dataLoan);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string CreateAgent(Guid loanId, Guid id, bool populateFromRolodex, E_AgentRoleT agentType)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(SellerClosingDisclosure));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                CAgentFields agent = dataLoan.GetAgentFields(Guid.Empty);
                if (populateFromRolodex)
                {
                    agent.PopulateFromRolodex(id, BrokerUserPrincipal.CurrentPrincipal);
                }
                else
                {
                    CommonFunctions.CopyEmployeeInfoToAgent(PrincipalFactory.CurrentPrincipal.BrokerId, agent, id);
                }

                agent.AgentRoleT = agentType;
                agent.Update();

                dataLoan.Save();

                return "{\"RecordId\": \"" + agent.RecordId.ToString() + "\"}";
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string Save(Guid loanId, string viewModelJson)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(SellerClosingDisclosure));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);


                BindToDataLoan(dataLoan, viewModelJson);

                dataLoan.Save();

                return CreateViewModel(dataLoan);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        [WebMethod]
        public static string ClearAgent(Guid loanId, string viewModelJson, Guid recordId)
        {
            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(SellerClosingDisclosure));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                // Bind unsaved data to loan
                BindToDataLoan(dataLoan, viewModelJson);

                DataSet ds = dataLoan.sAgentDataSet;
                DataTable table = ds.Tables[0];

                // Find agent to be deleted.
                DataRow toBeDeleted = null;
                foreach (DataRow row in table.Rows)
                {
                    if ((string)row["recordId"] == recordId.ToString())
                    {
                        // I could not invoke the Delete() on row object here.
                        // Doing so will modify the collection of table.Rows which cause
                        // the exception to be throw. dd 4/21/2003
                        toBeDeleted = row;
                        break;
                    }
                }

                // If agent found then delete and save. Otherwise do nothing.
                if (toBeDeleted != null)
                {
                    dataLoan.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(toBeDeleted, dataLoan.sSpState, AgentRecordChangeType.DeleteRecord));

                    // OPM 209868 - Clear beneficiary info in any closing cost fee where this agent is the beneficiary.
                    // If we have access to this page/method we can assume that the loan has been migrated.
                    dataLoan.sSellerResponsibleClosingCostSet.ClearBeneficiary(new Guid((string)toBeDeleted["recordId"]));
                    toBeDeleted.Delete();
                }

                dataLoan.sAgentDataSet = ds;
                dataLoan.Save();

                // Return deserialized data set.
                return CreateViewModel(dataLoan);
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }
    }
}
