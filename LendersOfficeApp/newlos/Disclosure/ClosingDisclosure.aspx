﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="ClosingDisclosure.aspx.cs" Inherits="LendersOfficeApp.newlos.Disclosure.ClosingDisclosure" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.ObjLib.AdjustmentsAndOtherCredits" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style>
        
        body
        {
            background-color:gainsboro;
        }
        hr
        {
            clear:both;
        }
        .TopDiv
        {
            width: 30%;
            float:left;
            padding:5px;
        }
        .TopDivContainer
        {
            white-space:nowrap;
            margin:10px 15px 0px 15px;
            
        }
        .InlineDiv
        {
            padding:5px;
            margin-bottom: 10px;
            white-space:nowrap;
            display:inline-block;
        }
        
        table
        {
            white-space:nowrap;
        }
        
        input[preset='money']
        {
            width:90px;
        }
        
        .SectionDiv
        {
            white-space:nowrap;
            margin: 10px 15px;
            clear:both;
            
            border-style:solid;
            border-width:1px;
        }
        #TimeDiv
        {
            border-style:none;
        }
        
        .GridHeader
        {
            padding:3px;
        }
        
        .LoanTermsDesc
        {
            width:205px;
            font-weight:bold;
            display:inline-block;
        }
        
        .FieldDesc
        {
            width:115px;
            font-weight:bold;
            display:inline-block;
        }
        
        .AmortScheduleContainer .FieldDesc
        {
            width:130px;
        }
        .FieldContainer
        {
            width:110px;
            font-weight:bold;
            display:inline-block;
        }
        
        .LoanTermsInfo
        {
            display:inline-block;
        }
        
        .LoanTermOptions
        {
            line-height:16px;
        }
        
        .LoanTermOptions > div
        {
            padding: 5px;
        }
        
        .Month
        {
            width:30px;
        }
        
        Form
        {
            float:left;
            clear:left;
            min-width:100%;
        }
        #AmortSchedule
        {
            border-collapse:collapse;
            width:100%;
            border:solid 1px black;
            margin-bottom:10px;
        }
        #ExpensesTable
        {
            border-collapse:collapse;
            width: 400px;
            border:solid 1px black;
            margin-bottom:10px;
            vertical-align:top;
        }
        #AmortSchedule thead td, #ExpensesTable thead td
        {
            text-align:center;
        }
        #AmortSchedule td, #ExpensesTable td
        {
            border:solid 1px black;
        }
        .AmortScheduleContainer
        {
            padding:10px;
        }
        
        #sSpAddr
        {
            width:230px;
        }
        
        #AmortizationTableDiv
        {
            width:630px;
            height:500px;
            display:none;
            align-content: center;
        }
        #AmortizationTableFrame
        {
            width:100%;
            height:95%;
        }
        
        .ui-dialog
        {
            border: solid 1px black !important;
        }
        .ui-dialog-titlebar
        {
            background-color:Maroon !important;
            border: 1px solid Maroon !important;
        }
        
        .HalfDiv
        {
            width:49%;
            display:inline-block;
            vertical-align:top;
        }
        
        .HalfDiv > div {
            white-space: normal;
        }

        .HalfDiv .FieldLabel
        {
            
            width:250px;
        }
        .HalfDiv span
        {
            display:inline-block;
            margin: 3px 5px; 
        }
        .HalfDiv .MoneyContainer, .ThirdDiv .MoneyContainer
        {
            width:100px;
            text-align:right;
        }
        .HalfDiv .ColumnHeader
        {
            width:100px;
        }
        
        .MoneyConstant
        {
            text-align:right;
        }
        
        .IndentedLine
        {
            padding-left:50px;
        }
        .CostsAtClosing .ThirdDiv
        {
            padding:0px;
        }
        .CostsAtClosing
        {
            padding: 5px 5px;
        }
        
        .modal-background {
              position: fixed;
              top: 0; left: 0; width: 100%; height: 100%;
              background-color: rgba(0,0,0,0.5);
              padding: 0.5em;
              text-align: center;
              -moz-box-sizing: border-box; box-sizing: border-box;
            }
            .modal-outer {
              position: relative;
              width: 100%;
              height: 100%;
            }

            .modal {
              position: relative;
              background-color: white;
              padding: 2em;
              box-shadow: 1px 1px 3px rgba(0,0,0,0.1);
              margin: 0 auto;
              display: inline-block;
              max-width: 100%;
              max-height: 100%;
              overflow-y: auto;
              -moz-box-sizing: border-box; box-sizing: border-box;
            }

            .modal-button {
              text-align: center;
              background-color: rgb(70,70,180);
              color: white;
              padding: 0.5em 1em;
              display: inline-block;
              cursor: pointer;
            }

            .modal ul li {
                text-align: left;
            }
            
            .OptionsList
            {
                list-style-type: none;
                display: none;
                background-color: white;
                border-style: solid;
                border-width: 1px;
                z-index:1;
                max-height: 300px;
                overflow-y:auto;
                padding:0px;
                margin:0px;
                margin:0 auto;
                text-align:left;
                position:relative;
            }
            .DDLImg, .ComboboxImg
            {
                width: 21px;
                height: 21px;
                top: 6px;
            }
            input[readonly]:not(.RactiveDDL)
            {
                background-color:lightgrey;
            }
            .QMDDL
            {
                width:200px;
            }
            .RactiveDDLContainer
            {
                overflow:visible;
                height:13px;
                display:inline-block;
            }
            
            input[preset='money']
            {
                width:90px;
                text-align:right;
            }
            input[preset='date']
            {
                width:75px;
            }
            select[disabled=""]
            {
                background-color: lightgrey;
            }
            .table-closingcost tfooter td
            {
                text-align:left;
            }
            
           
            /*****Design for TPO Portal Closing Costs *******/
            .div-title {
	            width: auto;
	            font-size:14px;
	            font-family:arial, helvetica, sans-serif; 
	            padding: 3px 0px 3px 9px;
	            margin-top: 5px;
	            /*font-weight: bold;*/
            }
            .div-left {
	            float: left;
	            width: 50%;
	            min-width: 300px;
            }
            .div-right 
            {
	            padding-top: 3px;
	            float: right;
	            width: 40%;
	            margin-left: 10%
            }
            .div-right div{
	            padding: 3px
            }
            .table-closingcost {
	            width: 100%;
	            border-collapse: collapse;
	            font-weight: normal;
            }
            .table-closingcost th {
	            font-weight: normal;
            }
            .table-closingcost td,th {
	            padding: 3px;
	            text-align: center;
	            white-space:nowrap;
            }
            .table-calculation-1 {
	            width: 100%;
            }

            .table-calculation-1 td:nth-child(4) {
	            width: 5%;
            }
            .table-calculation-1 td:nth-child(6) *{
	            width: 100%;
	            min-width: 90px;
            }
            .table-calculation-1 td:nth-child(7) {
	            width: 5%;
            }
            .table-calculation-1 td:nth-child(9) *{
	            width: 100%;
	            min-width: 90px;
            }
            .table-calculation-1 td:nth-child(11) {
	            width: 10%;
            }
            .table-calculation-2 {
	            width: 100%;
	            border-collapse: collapse;
	            border: 1px solid #ebeff2;
            }
            .table-calculation-2 td,th {
	            padding: 3px 3px 3px 10px;
            }
            .image-style {
	            padding: 0px 4px 0px 4px;
            }
            .div-image {
	            padding: 5px 3px 0px 3px;
            }
            .checkbox-hidden {
	            display: none;
            }
            .date-image {
	            vertical-align: middle;
            }

            /**TPO Portal Closing Cost Changes **/
            img
            {
                border-style:none;
            }
            
            .table-closingcost thead
            {
                background-color: #999999;
                color: Black;
            }
            
            #APTable, #AIRTable
            {
                width:100%;
                text-align:left;
            }
            
            #APTable td, #AIRTable td
            {
                width:50%;
            }
            
            #AdjustableDiv
            {
                text-align:center;
                border-style:none;
            }
            
            #AdjustableDiv .HalfDiv
            {
                width: 49%;
                margin: 0px;
            }
            
            .ThirdDiv
            {
                min-width: 32%;
                display: inline-block;
                padding:5px;
                vertical-align:top;
            }
            
            .ThirdDiv span
            {
                display:inline-block;
                width:250px;
                margin: 5px 0px; 
            }
            
            .CostsAtClosing td
            {
                padding:5px;
            }
            
            .CostsAtClosing .Description
            {
                padding:5px;
                width:250px;
            }
            
            .ContactDiv .ThirdDiv
            {
                border: solid 1px black;
            }
            
            .ContactHeader
            {
                font-weight:bold;
                text-decoration:underline;
            }
            
            .CFMContainer
            {
                display:inline-block;
            }
            
            .CFMLabel
            {
                width:115px !important;
                display:inline-block;
                margin-top:10px;
            }
            
            .ComparisonsDiv
            {
                display:inline-block;
                padding-top:20px;
                vertical-align:top;
            }
            
            .ComparisonSpan
            {
                width:170px;
                display:inline-block;
                margin:5px;
            }
            
            .OtherConsiderationsTable
            {
                border-collapse:collapse;
            }
            
            .OtherConsiderationsTable td
            {
                padding: 5px;
                border-style:none;
            }
            
            #sLateChargeBaseDesc
            {
                width:200px;
            }
            
            .smallNum
            {
                width: 50px;
            }
            
            #OtherConsiderationsTable
            {
                white-space:normal;
            }
            
            #OtherConsiderationsTable td.FieldLabel
            {
                white-space:nowrap;
                padding: 5px;
            }
            #OtherConsiderationsTable td
            {
                padding: 5px;
            }
            #dialog-confirm
            {
                text-align: center;
            }
            .LQBDialogBox .ui-button.ui-widget
            {
                border: none;
                padding: 0px;
                float: none;
                font-weight: normal;
            }
            .LQBDialogBox .ui-dialog-buttonset
            {
                text-align: center;
            }
            
            .Hidden
            {
                display:none;
            }
            
            .ContactImg
            {
                vertical-align:middle;
                margin-left:3px;
            }
            .ContactImg:hover
            {
                cursor:pointer;
            }
            .Hidden
            {
                display:none;
            }
            .table-closingcost td, .RactiveDDL, .DDLImg, .ContactImg
            {
                vertical-align:top;
            }
            
            #ClosingCostSummaryContainer
            {
                text-align:center;
            }
            
            .CCDetailsHalfDiv
            {
                width:48%;
                display:inline-block;
                vertical-align:top;
                margin: 10px;
                text-align:left;
            }
            
            .CCDetailsHalfDiv .SectionContainer
            {
                border: solid 1px gray;
                margin: 5px 0px;
            }
            
            .CCDetailsHalfDiv .SectionContainer div
            {
                padding: 0px 10px;
            }
            
            .CCDetailsHalfDiv .SectionContainer div.SectionHeader
            {
                padding: 0px 5px;
                background-color:rgb(8, 83, 147);
                color: White;
                font-weight:bold;
            }
            
            .CCDetailsHalfDiv span
            {
                display:inline-block;
                margin: 5px 0px; 
            }
            
            .BorrowerCCDiv
            {
                padding-left:10px;
                margin-top:10px;
                font-weight:bold;
                text-align:left;
            }
            
            #BorrowerCCLink
            {
                margin-right:15px;
            }
            
            .IndentedSection
            {
                padding-left:50px;
                margin-top:5px;
            }
            .LoanTermOptions div
            {
                vertical-align:top;
            }
            .LoanCalculationsDiv input
            {
                margin-right:80px;
            }
            .LoanCalculationsDiv td
            {
                padding:5px;
            }
            .LoanCalculationsDiv table
            {
                width:100%;
            }
            .SectionHeader
            {
                padding: 0px 5px;
                background-color:rgb(8, 83, 147);
                color: White;
                font-weight:bold;
            }
            #ClosingCostSummaryTable
            {
                width:100%;
            }
            .SubSectionHeader
            {
                background-color:rgb(186, 188, 185);
                color:Black;
                font-weight:bold;
                padding-left:15px;
                white-space: normal;
            }
            .AlternatingGridItem
            {
                background-color:rgb(237, 237, 237);
            }
            .MoneyTD
            {
                text-align:right;
            }
            .DescriptionContainer
            {
                text-align:left;
                white-space:nowrap;
            }
            .FeeDescriptionSpan
            {
                width:45%;
                display:inline-block;
                text-align:left;
                padding-left:30px;
                white-space:normal;
                vertical-align:top;
                word-wrap: break-word;
            }
            
            .FixedSpan
            {
                text-align:left;
                padding-left:30px;
                width:100%;
                display:inline-block;
            }
            
            #ClosingCostSummaryContainer .GridAlternatingItem
            {
                background-color:White !important;
                border-top: solid 1px gray;
                border-bottom:solid 1px gray;
            }
            
            .ContractDetailPoints
            {
                padding-left:10px;
            }
            
            .EscrowRadioDiv
            {
                display:inline-block; 
                vertical-align:top;
            }
            
            .EscrowDataDiv
            {
                margin-left:30px;
                border:solid 1px black;
                display:inline-block;
            }
            
            .EscrowDataDiv span
            {
                padding:5px;
                vertical-align:top;
                display:inline-block;
                min-width:80px;
            }
            
            .EscrowDataDiv span.EscrowDescriptionSpan
            {
                min-width:250px;
            }
            
            #EscrowDescriptionContainer, #NonEscrowDescriptionContainer
            {
                font-style:italic;
            }
            
            #sSpFullAddr
            {
                width:500px;
            }
            #ContactsTable
            {
                border-collapse:collapse;
                width:100%;
            }
            .ContactsTD
            {
                border:solid 1px black;
                border-bottom:none;
                padding:10px;
                vertical-align:top;
            }
            .ContactsLeft
            {
                border-left:none;
            }
            .ContactsRight
            {
                border-right:none;
            }
            
            .ContactsTD input[type='text']
            {
                width:200px;
            }
            
            .ContactsTD input.CityTextBox
            {
                width:90px;
                margin-right:5px;
            }
            
            .ContactsTD input.ZipTextBox
            {
                width:50px;
                margin-left:5px;
            }
            
            .AdjustmentsAndOtherCreditsContainer
            {
                padding-top:5px;
                padding-right:5px;
                padding-bottom:5px;
                padding-left:.4%;
                background-color:White;
            }
            
            .AdjustmentsAndOtherCreditsContainer .HalfDiv .MoneyConstant
            {
                float:right;
            }
            
            .AdjustmentsAndOtherCreditsContainer .HalfDiv span
            {
                width:auto;
                padding-right:5px;
                padding-top:5px;
                padding-bottom:5px;
                padding-left:15px;
                margin:0px;
            }

            .AdjustmentsAndOtherCreditsContainer .SubSectionHeader span, .AdjustmentsAndOtherCreditsContainer .SubSectionHeader
            {
                padding-left:5px;
            }
            
            .AdjustmentsAndOtherCreditsContainer .FieldLabel
            {
                margin:0px;
                padding-left:5px;
                width: 100%;
            }
            
            .AdjustmentsAndOtherCreditsContainer div.FieldLabel span
            {
                padding-left:0px;
            }
            
            .CFMContainer table td.FieldLabel
            {
                white-space:normal;
            }
            
            #ExpensesDiv
            {
                display:inline-block;
                vertical-align:top;
                padding-top:3px;
                
            }
            
            .TopDivContainer > table
            {
                width:100%;
            }
            .TopDivContainer > table > tbody > tr > td
            {
                border: solid 1px black;
                vertical-align:top;
                
            }
            .TopDivContainer td
            {
                padding:5px 3px;
            }
            #SellerInfoTable td
            {
                padding: 0px 3px;
            }
            .CompactContainer
            {
                text-align:center;
            }
            .CompactContainer .HalfDiv
            {
                text-align:left;
            }
            #ProjectedPaymentsHeaderRow span.FieldLabel
            {
                display: inline-block;
                padding-left: 10px;
            }
            #ViewAmortizationScheduleBtn
            {
                display:inline-block;
                margin-left: 15px;
            }
            #ProjectedPayments table
            {
                margin-left: 10px;
                background-color: white;
                border: 1px solid black;
                table-layout: fixed;
                border-collapse: collapse;
            }
            #ProjectedPayments table th
            {
                padding: 5px;
                background-color: rgb(8, 83, 147);
                color: White;
                font-weight:bold;
                text-align: center;
                width: 150px;
                border-left: 1px solid black;
                border-bottom: 1px solid black;
            }
            #ProjectedPayments table th:first-of-type
            {
                background-color: rgb(153, 153, 153);
            }
            #ProjectedPayments table td
            {
                text-align: right;
                background-color: white;
            }
            #ProjectedPayments table td.label
            {
                text-align: left;
                padding-left: 5px;
                background-color: gainsboro;
            }
            #ProjectedPayments table td.plus
            {
                border-left: 1px solid black;
                text-align: left;
                padding-left: 10px;
                width: 15px;
            }
            #ProjectedPayments table td.minMax
            {
                text-align: left;
                padding-left: 5px;
                width: 45px;
            }
            #ProjectedPayments table td.interestOnly
            {
                font-style: italic;
                font-weight: bold;
            }
            #ProjectedPayments table tfoot tr:first-of-type
            {
                border-top: 1px solid black;
            }
            #HousingExpensesTable
            {
                margin-left: 10px;
                margin-top: 5px;
                margin-bottom: 5px;
            }
            #HousingExpensesTable>tbody>tr>td:first-of-type
            {
                padding-left: 5px;
            }
            #HousingExpensesTable>tbody>tr>td:nth-of-type(2)
            {
                padding-left: 20px;
                padding-right: 20px;
            }
            .display-table {
                display:table;
            }

            .show-sLPurposeT .sTridLPurposeT, .show-sTridLPurposeT .sLPurposeT {
                display: none;
            }

    </style>
</head>
<body>
    <div style="display:none;" id="dialog-confirm" title="Delete fee?">
        <p style="width: 100%">
        </p>
    </div>
    <form id="form1" runat="server">
    
    <script>

        function selectedDate(cal, date) {
            cal.sel.value = date;
            cal.callCloseHandler();
            if (typeof (updateDirtyBit) == 'function') updateDirtyBit();
            cal.sel.blur();
        } 
        
        function getNonRactiveData() {
            var nonRactiveData = {};

            for (i = 0; i < EditableFields.length; i++) {
                var id = EditableFields[i];
                var el = $("#" + id);
                if (el.length > 0) {
                    if (el.attr("type") == "radio" || el.attr("type") == "checkbox") {
                        EditableFieldsModel[id] = $("#" + id).prop("checked");
                    }
                    else {
                        EditableFieldsModel[id] = $("#" + id).val();
                    }
                }
            }

            //Handle the special cases where control Ids and field names don't match.
            UpdateEditableFieldsIfExists("sTRIDLenderIsLocked", "LenderContact_m_rbManualOverride", EditableFieldsModel, true);
            UpdateEditableFieldsIfExists("sTRIDLenderAgentRoleT", "LenderContact_m_officialContactList", EditableFieldsModel, false);
            UpdateEditableFieldsIfExists("sTRIDMortgageBrokerIsLocked", "MortgageBrokerContact_m_rbManualOverride", EditableFieldsModel, true);
            UpdateEditableFieldsIfExists("sTRIDMortgageBrokerAgentRoleT", "MortgageBrokerContact_m_officialContactList", EditableFieldsModel, false);
            UpdateEditableFieldsIfExists("sTRIDSettlementAgentIsLocked", "SettlementAgentContact_m_rbManualOverride", EditableFieldsModel, true);
            UpdateEditableFieldsIfExists("sTRIDSettlementAgentAgentRoleT", "SettlementAgentContact_m_officialContactList", EditableFieldsModel, false);
            UpdateEditableFieldsIfExists("sTRIDRealEstateBrokerBuyerIsLocked", "RealEstateBrokerBuyerContact_m_rbManualOverride", EditableFieldsModel, true);
            UpdateEditableFieldsIfExists("sTRIDRealEstateBrokerBuyerAgentRoleT", "RealEstateBrokerBuyerContact_m_officialContactList", EditableFieldsModel, false);
            UpdateEditableFieldsIfExists("sTRIDRealEstateBrokerSellerIsLocked", "RealEstateBrokerSellerContact_m_rbManualOverride", EditableFieldsModel, true);
            UpdateEditableFieldsIfExists("sTRIDRealEstateBrokerSellerAgentRoleT", "RealEstateBrokerSellerContact_m_officialContactList", EditableFieldsModel, false);
            UpdateEditableFieldsIfExists("sTRIDPropertySellerAgentRoleT", "PropertySellerContact_m_officialContactList", EditableFieldsModel, false);
            UpdateEditableFieldsIfExists("sGfeIsBalloon", "sGfeIsBalloon_True", EditableFieldsModel, true);
            
            return nonRactiveData;
        }

        function UpdateEditableFieldsIfExists(modelId, fieldId, EditableFieldsModel, isBoolProp)
        {
            var el = $("#" + fieldId);
            if(el.length > 0)
            {
                if(!isBoolProp)
                {
                    EditableFieldsModel[modelId] = el.val();
                }
                else
                {
                    EditableFieldsModel[modelId] = el.prop("checked");
                }
            }
        }

        function setNonRactiveData(model) {
            var nonRactiveData = {};

            for (i = 0; i < EditableFields.length; i++) {
                var id = EditableFields[i];
                var el = $("#" + id);
                if (el.length != 0) {
                    if (el.attr("type") == "radio" || el.attr("type") == "checkbox") {
                        $("#" + id).prop("checked", model[id]);
                    }
                    else if (el.prop("tagName") == "SPAN") {
                        $("#" + id).text(model[id]);
                    }
                    else if (el.prop("tagName") == "INPUT" || el.prop("tagName") == "SELECT") {
                        $("#" + id).val(model[id]);
                    }
                    else
                    {
                        el.text(model[id]);
                    }
                    
                    //Handle special cases where extra behavior is needed.
                    if(id == "sTRIDLoanEstimateTotalClosingCosts")
                    {
                        $("#" + id + "1").text(model[id]);
                        $("#" + id + "2").text(model[id]);
                    }
                    if (id == "sSchedDueD1") {
                        $("#" + id + "2").text(model[id]);
                    }
                }
            }
        
            //handle the special cases
            $("#sOriginatorCompensationPaymentSourceT").find("input[value="+model['sOriginatorCompensationPaymentSourceTRL']+"]").prop('checked', true);
            $("#sOriginatorCompensationPaymentSourceT").find("input[value!="+model['sOriginatorCompensationPaymentSourceTRL']+"]").prop('checked', false);
            $("#LenderContact_m_rbManualOverride").prop("checked", model["sTRIDLenderIsLocked"]);
            $("#LenderContact_m_officialContactList").val(model["sTRIDLenderAgentRoleT"]);
            $("#MortgageBrokerContact_m_rbManualOverride").prop("checked", model["sTRIDMortgageBrokerIsLocked"]);
            $("#MortgageBrokerContact_m_officialContactList").val(model["sTRIDMortgageBrokerAgentRoleT"]);
            $("#SettlementAgentContact_m_rbManualOverride").prop("checked", model["sTRIDSettlementAgentIsLocked"]);
            $("#SettlementAgentContact_m_officialContactList").val(model["sTRIDSettlementAgentAgentRoleT"]);
            $("#RealEstateBrokerBuyerContact_m_rbManualOverride").prop("checked", model["sTRIDRealEstateBrokerBuyerIsLocked"]);
            $("#RealEstateBrokerBuyerContact_m_officialContactList").val(model["sTRIDRealEstateBrokerBuyerAgentRoleT"]);
            $("#RealEstateBrokerSellerContact_m_rbManualOverride").prop("checked", model["sTRIDRealEstateBrokerSellerIsLocked"]);
            $("#RealEstateBrokerSellerContact_m_officialContactList").val(model["sTRIDRealEstateBrokerSellerAgentRoleT"]);
            $("#PropertySellerContact_m_officialContactList").val(model["sTRIDPropertySellerAgentRoleT"]);

            $("#sConsummationD2").val(model["sConsummationD"]);
            
            return nonRactiveData;
        }

        function lqblog(msg) {
            if (window.console && window.console.log) {
                window.console.log(msg);
            }
        }
        
        function callWebMethod(webMethodName, data, error, success, pageName) {
            if (!pageName) {
                pageName = '../Disclosure/ClosingDisclosure.aspx/';
            }
            
            var settings = {
                async: false,
                type: 'POST',
                url: pageName + webMethodName,
                data: JSON.stringify(data),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: error,
                success: function (d){ checkWebError(d, success); }
            };

            callWebMethodAsync(settings);
        }

        function EventHandlers() {

            
            $("#sLenderCaseNumLckd").change(function(){ 
                lockElement("sLenderCaseNum");
                ractive.fire('recalc'); 
            });

            $("#sNonMIHousingExpensesNotEscrowedReasonT_Declined, #sNonMIHousingExpensesNotEscrowedReasonT_NotOffered").change(function() {
                if(!$("#sNonMIHousingExpensesNotEscrowedReasonT_Declined").prop("checked"))
                {
                    $("#sTRIDEscrowWaiverFee").prop("disabled", true);
                }
                else
                {
                    $("#sTRIDEscrowWaiverFee").removeProp("disabled");
                }
            });
            
            $("#sSchedDueD1Lckd, #sDocMagicClosingDLckd, #sDocMagicDisbursementDLckd, #sNonMIHousingExpensesNotEscrowedReasonT_Declined, #sNonMIHousingExpensesNotEscrowedReasonT_NotOffered,#sEstCloseDLckd").change(function() {
                ractive.fire('recalc');
            });

            $("#sConsummationDLckd, #sConsummationD2Lckd").change(function() {
                $this = $(this);
                $("#sConsummationDLckd, #sConsummationD2Lckd").prop('checked', $this.prop('checked'));

                lockElement("sConsummationD");
                lockElement("sConsummationD2");

                ractive.fire('recalc');
            });
            
            $("#sEstCloseD, #sSchedDueD1, #sDocMagicClosingD, #sDocMagicDisbursementD").blur(function() {
                ractive.fire('recalc');
            });

            $("#sConsummationD, #sConsummationD2").blur(function() {
                $this = $(this);
                $("#sConsummationD, #sConsummationD2").val($this.val());

                ractive.fire('recalc');
            });

            $("#sEstCloseD, #sSchedDueD1, #sDocMagicClosingD, #sConsummationD, #sConsummationD2, #sDocMagicDisbursementD").change(function() {
                date_onblur(null, this)
                $(this).blur();
            });

            $("select:disabled").each(function() {
                var el = $(this);
                el.removeProp('disabled');
                el.mousedown(function(event) {
                    event.preventDefault();
                    $(this).focus();
                });
                el.keydown(function(event) {
                    event.preventDefault();
                    $(this).focus();
                });
                el.css("background-color", "lightgrey");
            });
            
            $("#BorrowerCCLink").click(function(){
                linkMe('../Disclosure/BorrowerClosingCosts.aspx');
                return false;
            });
            
            $("#SellerCCLink").click(function(){
                linkMe('../Disclosure/SellerClosingDisclosure.aspx');
                return false;
            });
            $("#EscrowLink").click(function(e){
                linkMe('../Forms/PropHousingExpenses.aspx');
                e.preventDefault();
            });

            $('#ArchiveStatus').change(function() {
                var data, errorHandler, successHandler;

                data = {
                    loanId: ML.sLId,
                    archiveId: $('#ddlArchives').val(),
                    status: $(this).val()
                };

                errorHandler = function () {
                    alert('Unable to set archive status');
                };

                successHandler = function(msg) {
                    var response = JSON.parse(msg.d);
                    if (response.LastDisclosedClosingDisclosureArchiveDate) {
                        $('#LastDisclosedClosingDisclosureArchiveDateCell').html(response.LastDisclosedClosingDisclosureArchiveDate);
                    }
                };

                callWebMethod('SetArchiveStatus', data, errorHandler, successHandler);
            });
        }
        
        var loanID;

        $(document).ready(function() {
            
            $("#OtherConsiderationsTable tr:nth-child(odd)").each(function(){ $(this).addClass("GridAlternatingItem"); });
            $("#OtherConsiderationsTable tr:nth-child(even)").each(function(){ $(this).addClass("GridItem"); });

            $('.Name').change(function () {
                $div = $(this).closest('div');
                $div.find('.AgentId').val(<%=AspxTools.JsString(Guid.Empty) %>);
            });

            window.refreshCalculation = function() { ractive.fire('recalc'); };
            

            window.f_saveMe = function() {
                getNonRactiveData();
                var model = ractive.get();
                var data = { loanId: ML.sLId, viewModelJson: JSON.stringify(model), editableFieldsJson: JSON.stringify(EditableFieldsModel) },
                success = false;

                callWebMethod('Save', data, function() {
                    alert('Failed');
                },
            function(d) {
                success = true;
                clearDirty();
            });
                return success;
            }

            window.saveMe = window.f_saveMe;

            var AddressInfo = new Ractive({
                el: 'AddressInfo',
                append: 'false',
                template: '#AddressTemplate',
                data: DataJSON,
                twoway: false
            });

            var ExpenseInfo = new Ractive({
                el: '#ExpensesContainer',
                append: 'false',
                template: '#ExpensesTemplate',
                data: DataJSON,
                twoway: false
            });
            
            var EscrowedDescriptions = new Ractive({
                el: 'EscrowDescriptionContainer',
                append: 'false',
                template: '#EscrowedDescriptionTemplate',
                data: DataJSON,
                twoway: false
            });
            
            var NonEscrowedDescriptions = new Ractive({
                el: 'NonEscrowDescriptionContainer',
                append: 'false',
                template: '#NonEscrowedDescriptionTemplate',
                data: DataJSON,
                twoway: false
            });

            var projectedPayments = new Ractive({
                el: '#ProjectedPayments',
                template: '#ProjectedPaymentsTemplate',
                data: DataJSON,
                twoway: false
            });


            loanID = document.getElementById("loanid").value;
            $("#AmortizationTableFrame").prop("src", "../Forms/AmortizationTable.aspx?loanid=" + loanID);

            $("#AmortizationTableFrame").on('load', function() {

                var contents = $("#AmortizationTableFrame").contents();
                var closeBtn = contents.find("input[value='Close']");
                closeBtn.unbind("click");

                var header = contents.find("h4.page-header");
                header.hide();
                closeBtn.click(closeDialog);
            });

            lockElement("sSchedDueD1");
            lockElement("sEstCloseD");
            lockElement('sDocMagicClosingD')
            lockElement('sConsummationD')
            lockElement('sConsummationD2')
            lockElement('sDocMagicDisbursementD')

            var datepickerTransition = function(t) {
                if (t.isIntro) {
                    var node = $(t.node);
                    node.change(
            function() {
                $(this).blur();
                ractive.updateModel();
                ractive.fire('recalc');
            });

                    _initMask(node[0]);
                }
            };

            var numericTransition = function(t, recalc) {
                if (t.isIntro) {
                    var node = $(t.node);
                    if (recalc) {
                        node.change(
            function() {
                ractive.updateModel();
                ractive.fire('recalc');
            });
                    }

                    _initMask(node[0]);
                }
            };

            var ddlTransition = function(t) {

                if (t.isIntro) {
                    var node = $(t.node);
                    var options = $("." + this.data.OptionsId);

                    if (options.attr("sized")) {
                        node.css("width", options.width() - $(".DDLImg").width() - 2);
                    }
                }
            }

            var sectionTransition = function(t) {
                if (t.isIntro) {
                    var node = $(t.node);
                    var section = node.text();

                    var selector = determineShownSections(t.params.hudline, t.params.isSystem, t.params.gfeGroup);
                    node.find("option").not(selector).remove();

                    var numVisible = node.find("option").length;
                    if (numVisible < 2) {
                        node.prop('disabled', true);
                    }
                    if (numVisible < 1) {
                        node.css('display', 'none');
                    }
                }
            }
            
            var initMask = function(o) {
                _initMask(o.node, true);
                o.complete();
            };
        
            Ractive.transitions.initMask = initMask;

            Ractive.transitions.datepickerTransition = datepickerTransition;
            Ractive.transitions.numericTransition = numericTransition;
            Ractive.transitions.ddlIntro = ddlTransition;
            Ractive.transitions.sectionIntro = sectionTransition;

            function highlightLI(el) {
                el.css("background-color", "yellow");
                el.attr("class", "selected");
            }

            function unhighlightLI(el) {
                el.css("background-color", "");
                el.removeAttr("class");
            }

            ractive = new Ractive({
                // The `el` option can be a node, an ID, or a CSS selector.
                el: 'ClosingCostSummaryContainer',
                append: false,

                // We could pass in a string, but for the sake of convenience
                // we're passing the ID of the <script> tag above.
                template: '#ClosingCostSummary',

                // Here, we're passing in some initial data
                data: ClosingCostData,

                init: function() {

                }
            });
            
            ractive.on('onContactHover', function(event){
	    var el = $(event.node);
	    el.attr("src", '../../images/contacts_clicked.png');
	});
	
	ractive.on('onContactMouseOut', function(event){
	    var el = $(event.node);
	    el.attr("src", '../../images/contacts.png');
	});
	
	ractive.on('showAgentPicker', function(event, i, j){
	
	   var el = $(event.node);
       var rolodex = new cRolodex();
    
       var type = el.parent().find("input[type='hidden']").val();

       rolodex.chooseFromRolodex(type, ML.sLId, true, false, function(args){

            if (args.OK == true) {
                    if(args.Clear == true)
                    {
                        $("#ClearAgent").dialog({
                            modal: true,
                            height: 150,
                            width: 350,
                            title: "",
                            dialogClass: "LQBDialogBox",
                            resizable: false,
                            buttons: [
                                {text: "OK",
                                    click: function(){
                                        // Remove Agent from official contacts
                                        var model = ractive.get();
                                        var data = {
                                            loanId: ML.sLId,
                                            viewModelJson: JSON.stringify(model),
                                            recordId: ractive.get("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id"),
                                        }
                                    
                                        callWebMethod("ClearAgent", 
                                            data,
                                            function(){
                                                alert("Error:  Could not remove agent.");
                                            }, 
                                            //On Success
                                            function(m){
                                                model = JSON.parse(m.d);
                                                ractive.set(model);
                                            });
                                    
                                        $(this).dialog("close");
                                    }
                                },
                                {text: "Cancel",
                                    click: function(){
                                        $(this).dialog("close");
                                    }
                                }
                            ]
                        });
                    }
                    else
                    {
                    var id = "";
                    var populateFromRolodex = false;
                    var agentType = args.AgentType;
                    
                    if(args.RecordId != "" && args.RecordId != <%=AspxTools.JsString(Guid.Empty) %> )
                    {
                        //If they're choosing from an agent record, simply set it's record id as the fee's bene_id.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", args.RecordId);
                        //Update paidTo DDL
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                        //Re-enable beneficiary automation.
                        ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                        
                        updateDirtyBit();
                        ractive.fire('recalc');
                    }
                    else
                    {
                        if(args.BrokerLevelAgentID != <%=AspxTools.JsString(Guid.Empty) %>)
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there is a BrokerLevelAgentID, populate using the rolodex and that id
                            
                            id=args.BrokerLevelAgentID;
                            populateFromRolodex = true;  
                        }
                        else
                        {
                            //If the selcted contact has no record id, then add it to the agents list.
                            //If there isn't a BrokerLevelAgentID, populate using the employee info
                            
                            id=args.EmployeeId;
                            populateFromRolodex = false;  
                        }
                        
                        
                        $("#ConfirmAgent").dialog({
                            modal: true,
                            height: 150,
                            width: 350,
                            title: "",
                            dialogClass: "LQBDialogBox",
                            resizable: false,
                            buttons: [
                                {text: "OK",
                                    click: function(){
                                    callWebMethod("CreateAgent", 
                                        {loanId: ML.sLId, id: id, populateFromRolodex: populateFromRolodex, agentType: agentType},
                                        function(){alert("Error:  Could not create a new agent.");}, 
                                        //On Success
                                        function(d){
                                            var m = JSON.parse(d.d);
                                            // Get the recordID and set it for the bene_id
                                            recordId = m.RecordId;
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene_id", recordId);
                                            //Update the fee's paidTo DDL
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".bene", agentType);
                                            //Re-enable beneficiary automation.
                                            ractive.set("SectionList." + i + ".ClosingCostFeeList." + j + ".disable_bene_auto", false);
                                            
                                            updateDirtyBit();
                                            ractive.fire('recalc');
                                            
                                        });
                                        $(this).dialog("close");
                                    }
                                    },
                                    {text: "Cancel",
                                    click: function(){
                                    $(this).dialog("close");
                                    }}
                            ]
                        });
                    }

                }
            }
        });
	});

            ractive.on('domblur', function(event) {
                var kp = event.keypath;
                //sadly this blur event fires before the mask.js blur event 
                //by using setTimeout we queue up the function until after all the other
                //js code.
                window.setTimeout(function() {
                    ractive.updateModel(kp, true);
                });
            });

            ractive.on('delete', function(event, i, j, desc, amt, orgDesc) {
                var d = desc;
                if (!d)
                {
                    d = orgDesc;
                }
                $('#dialog-confirm').find('p').html('Are you sure you would like to remove the following fee? <br />').append(document.createTextNode(d + ' : ' + amt));
                $('#dialog-confirm').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function()
                        {
                            $(this).dialog("close");
                            updateDirtyBit();

                            var model = ractive.get();
                            var sectionList = model.SectionList;
                            sectionList[i].ClosingCostFeeList.splice(j, 1);
                        },
                        "No": function()
                        {
                            $(this).dialog("close");
                        }
                    },
                    closeOnEscape: false,
                    width: "400",
                    draggable: false,
                    resizable: false,
                    dialogClass: "LQBDialogBox"
                });
            });

            ractive.on('updatePaidBy', function(e, i, j, total) {
                var value = e.node.value;
                var obj = e.context;
                if (value == -1)
                {
                   var model = ractive.get(), 
                   fee = model.SectionList[i].ClosingCostFeeList[j];
                   fee.pmts[0].paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
                   
                   // Add new system payment
                   var newPayment = new Object();
                   newPayment.amt = "";
                   newPayment.ent = 0;
                   newPayment.is_fin = false;
                   newPayment.is_system = true;
                   newPayment.made = false;
                   newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
                   newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
                   newPayment.pmt_dt = "";
                   fee.pmts.push(newPayment);
                   
                   updateDirtyBit();
                   ractive.fire('recalc');
                }
                else if (value == 3)
                {
                    obj.pmts[0].paid_by = value;
                    obj.pmts[0].pmt_at = 1; // Set to 'at closing'
                    ractive.update(e.keypath);
                    ractive.fire('recalc');
                }
                else
                {
                    obj.pmts[0].paid_by = value;
                    ractive.update(e.keypath);
                }
            });

            ractive.on('updatePaidBySplit', function(e)
            {
                var value = e.node.value;
                var obj = e.context;
                if (value == 3)
                {
                    obj.paid_by = value;
                    obj.pmt_at = 1; // Set to 'at closing'
                    ractive.update(e.keypath);
                    ractive.fire('recalc');
                }
                else
                {
                    obj.paid_by = value;
                    ractive.update(e.keypath);
                }
            });
            
            ractive.on('AddSplitPayment', function(event, i, j) {
                updateDirtyBit();

                var model = ractive.get();
                var sectionList = model.SectionList;
                var fee = sectionList[i].ClosingCostFeeList[j];

                var newPayment = new Object();
                newPayment.amt = "";
                newPayment.ent = 0;
                newPayment.is_fin = false;
                newPayment.is_system = false;
                newPayment.made = false;
                newPayment.paid_by = <%= AspxTools.HtmlString(E_ClosingCostFeePaymentPaidByT.Borrower.ToString("D")) %>;
                newPayment.pmt_at = <%= AspxTools.HtmlString(E_GfeClosingCostFeePaymentTimingT.AtClosing.ToString("D")) %>;
                newPayment.pmt_dt = "";

                fee.pmts.push(newPayment);
                ractive.fire('recalc');
            });

            ractive.on('updateCalc', function(event, i, j) {
                var model = ractive.get();
                var keypath = 'SectionList';
                var o = this.get(keypath);
                var m = new CalculationModal({ data: { fee: o[i].ClosingCostFeeList[j].f, loan: this.get()} });
                m.on('teardown', function() {
                    ractive.fire('recalc');
                    updateDirtyBit();
                });
            });

            ractive.on('recalc', function(event, success) {

                getNonRactiveData();

                if (!ractive.data.ByPassBgCalcForGfeAsDefault) {
                    window.setTimeout(function() {
                        ractive.set('GfeTilAgentRoleT', $("#CFM_m_officialContactList").val());
                        ractive.set('GfeTilIsLocked', $("#CFM_m_rbManualOverride").prop("checked"));

                        var startRecalc = Date.now(),
                model = ractive.get(),
                data = { loanId: loanID, viewModelJson: JSON.stringify(ractive.get()), editableFieldsJson: JSON.stringify(EditableFieldsModel) },
                fin = function(m) {
                    var load = Date.now();
                    lqblog('Time To Load Data ' + (load - startRecalc) + 'ms.');
                    model = JSON.parse(m.d);
                    var d = Date.now();

                    model.ByPassBgCalcForGfeAsDefault = ractive.data.ByPassBgCalcForGfeAsDefault
                    model.viewT = ractive.data.viewT;

                    ractive.set(model);

                    setNonRactiveData(model.editableFieldsModel);

                    var c = Date.now();
                    lqblog('Time to set data ' + (c - d) + 'ms.');
                },
                error = function(e) {
                    alert('Error');
                };

                        callWebMethod('CalculateData', data, error, fin);
                    });
                }
            });
            
            function GetPaidByDesc(paidby)
            {
                var paidByDesc = "";
                if(paidby == 1)
                {
                    paidByDesc = "borr pd";
                }
                else if(paidby == 2)
                {
                    paidByDesc = "seller";
                }
                else if(paidby == 3)
                {
                    paidByDesc = "borr fin";
                }
                else if(paidby == 4)
                {
                    paidByDesc = "lender";
                }
                else if(paidby == 5)
                {
                    paidByDesc = "broker";
                }
                else if(paidby == 6)
                {
                    paidByDesc = "other";
                }
                
                return paidByDesc;
            }
            
            ractive.on('removePayment', function(e, i, j, k, amt, paidby, desc, orgDesc) {
                var d = desc;
                if (!d)
                {
                    d = orgDesc;
                }
                var paidByDesc = GetPaidByDesc(paidby);
                
                $('#dialog-confirm').find('p').text('Are you sure you would like to remove the ' + amt + ' ' + paidByDesc + ' split payment for the ' + d + '?');
                $('#dialog-confirm').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function()
                        {
                            $(this).dialog("close");
                            updateDirtyBit();
                            var model = ractive.get();
                            var sectionList = model.SectionList;
                            sectionList[i].ClosingCostFeeList[j].pmts.splice(k, 1);
                            ractive.fire('recalc');
                        },
                        "No": function()
                        {
                            $(this).dialog("close");
                        }
                    },
                    closeOnEscape: false,
                    width: "400",
                    draggable: false,
                    resizable: false,
                    dialogClass: "LQBDialogBox"
                });
            });

            ractive.on('addFee', function(event, i) {
                var model = ractive.get();

                var sectionList = model.SectionList;

                var section = sectionList[i],
             sectionName = section.SectionName,
             data = { loanId: loanID, viewModelJson: JSON.stringify(model), sectionName: sectionName };

                callWebMethod('GetAvailableClosingCostFeeTypes', data,

         function() {
             alert('Error');
         },

         function(m) {
             var result = JSON.parse(m.d);
             var y = new FeePickerModal({ data: result });
             y.on('selectFee', function(e, si) {
                 section.ClosingCostFeeList.push(result.FeeListAsBorrowerFees[si]);
                 y.teardown();
                 updateDirtyBit(e.original);
                 $(".RactiveDDL" + i + (section.ClosingCostFeeList.length - 1)).css("width", $(".OptionsList").width() - $(".DDLImg").width() - 2);
                 ractive.fire('recalc');
                 return false;
             });
         });
            });

            ractive.on('showCalendar', function(e, id) {
                return displayCalendar(id);
            });
            var canHideDescriptions = true;
            var currentFeeRow = 0;
            var currentSection = 0;

            ractive.on('onHoverDescriptions', function(event) {
                canHideDescriptions = false;
            });

            ractive.on('onMouseOutDescriptions', function(event) {
                canHideDescriptions = true;
            });

            ractive.on('highlightLI', function(event) {
                var node = $(event.node);
                var allLI = node.parent().find('li');
                unhighlightLI(allLI);
                highlightLI(node);

            });
            
            ractive.on('CanShopChange', function(e, i, j){
                var keypath = 'SectionList.' + i + '.ClosingCostFeeList.' + j + '.did_shop';
                var can_shop = $(e.node).prop('checked');
                if(!can_shop)
                {
                    ractive.set(keypath, false);
                }
            });

            ractive.on('tpClick', function(e, i, j) {
                var model = ractive.get();
                if (!model.SectionList[i].ClosingCostFeeList[j].tp) {
                    model.SectionList[i].ClosingCostFeeList[j].aff = false;
                    ractive.set(model);
                }
            });

            ractive.on('unhighlightLI', function(event) {
                var node = $(event.node);
                unhighlightLI(node);
            });


            ractive.on('selectDDL', function(event) {
                var node = $(event.node);
                var text = node.text();

                var td = node.parent().parent();

                td.find(".RactiveDDL").val(node.text());
                td.find("input[type='hidden']").val(node.attr('val'));
              
                var value = node.attr('val');
              
                node.parent().hide();
                $("#OptionsContainer").append(node.parent());
                canHideDescriptions = true;
                 
                // Get Agent Id
                var args = new Object();
                args["loanid"] = ML.sLId;
                args["IsArchivePage"] = false;
                args["AgentType"] = value;
                  
                var result = gService.cfpb_utils.call("GetAgentIdByAgentType", args);
                
                if (!result.error) {
                    //Update the fee's Beneficiary ID
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_id", result.value.RecordId);
                    // Update the fee's Beneficiary Description
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene_desc", result.value.CompanyName);
                    //Update the fee's paidTo DDL
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".bene", value);
                    //Re-enable beneficiary automation.
                    ractive.set("SectionList." + currentSection + ".ClosingCostFeeList." + currentFeeRow + ".disable_bene_auto", false);
                }
                
                updateDirtyBit();
                ractive.fire('recalc');
            });

            ractive.on('reselectInput', function(event) {
                //this is called by the UL itself
                $(event.node).parent().find('input').focus();
            });

            $(".RactiveDDL").css("width", $(".BeneficiaryOptions").width());
            $(".OptionsList").css("width", $(".OptionsList").width() + $(".DDLImg").width() + 2);
            
            
            EventHandlers();
        
            lockElement("sLenderCaseNum");
        });

        function closeDialog() {
            $("#AmortizationTableDiv").dialog("close");
        }

        function showAmortTable() {
            $("#AmortizationTableDiv").dialog({
                width: 630,
                height: 540,
                modal: false,
                title: "Amortization Table",
                resizable: false,
                draggable: false,
            });
        }

        function lockTimeControl(id) {
            var minuteId = id + "_minute";
            var amId = id + "_am";

            $("#" + id).attr("readonly", true);
            $("#" + minuteId).attr("readonly", true);
            $("#" + amId).prop("disabled", true);

            $("#" + id).css("background-color", "lightgrey");
            $("#" + minuteId).css("background-color", "lightgrey");
            $("#" + amId).css("background-color", "lightgrey");
        }

        function lockElement(id) {

            var elem = $("#" + id);
            if (id == "sNMLSPeriodForDaysDelinquentT") {
                id = "sNMLSPeriodForDaysDelinquentLckd";
            }
            else if (id == "sNMLSServicingIntentT") {
                id = "sNMLSServicingIntentLckd";
            }
            else {
                id = id + "Lckd";
            }
            var isChecked = $("#" + id).prop("checked");
            if (elem.prop("tagName") == "SELECT") {
                elem.prop("disabled", !isChecked);

                if (isChecked) {
                    elem.css("background-color", "");
                }
                else {
                    elem.css("background-color", "lightgrey");
                }
            }
            else {
                elem.prop("readonly", !isChecked || ML.IsReadOnly == true);
            }
        }
    </script>
    
    <script id="AddressTemplate" type="text/ractive">
                            {{#addresses}}
                                <br/>
                                {{aBNm}} {{#if aBHasSpouse && sameAddr}} and {{aCNm}} {{/if}} <br/>
                                {{aBAddrMail}} <br/>
                                {{aBCityMail}}, {{aBStateMail}} {{aBZipMail}} <br/>
                                
                                {{#if aBHasSpouse && !sameAddr}}
                                    <br/>
                                    {{aCNm}} {{#if aCHasSpouse && sameAddr}} and {{aCNm}} {{/if}} <br/>
                                    {{aCAddrMail}} <br/>
                                    {{aCCityMail}}, {{aCStateMail}} {{aCZipMail}} <br/>
                                {{/if}}
                            {{/addresses}}
    </script>
        
    <script id="ExpensesTemplate" type="text/ractive">
        <table id="ExpensesTable">
            <thead>
                <td class="GridHeader">This estimate includes:</td>
                <td class="GridHeader">Amount</td>
                <td class="GridHeader">In Escrow?</td>
            </thead>
            <tbody>
                {{#HousingExpenses}}
                    <tr>
                        <td>{{expenseName}}</td>
                        <td>{{amount}}</td>
                        <td>{{inEscrow? 'Yes':'No'}}</td>
                    </tr>
                {{/HousingExpenses}}
            </tbody>
        </table>
    </script>
    
    <script id="EscrowedDescriptionTemplate" type="text/ractive">
        {{#EscrowedDescriptions: i}}
            <span class="EscrowDescriptionSpan">{{EscrowedDescriptions[i]}}</span>
            <br />
        {{/EscrowedDescriptions}}
    </script>
    <script id="NonEscrowedDescriptionTemplate" type="text/ractive">
        {{#NonEscrowedDescriptions: i}}
            <span class="EscrowDescriptionSpan">{{NonEscrowedDescriptions[i]}}</span>
    <br />
        {{/NonEscrowedDescriptions}}
    </script>
    
  <script id="ClosingCostSummary" type="text/ractive">
    <div>
        <table id="ClosingCostSummaryTable">
            <tr>
                <td>
                    <div class="BorrowerCCDiv">
                        <a href="#" id="BorrowerCCLink" >Borrower-Responsible Closing Costs</a>
                        <a href="#" id="SellerCCLink" >Non Borrower-Responsible Closing Costs</a>
                    </div>
                </td>
                <td colspan="2" class="SectionHeader">Borrower-Paid</td>
                {{#if sLoanTransactionInvolvesSeller == 1}}
                    <td colspan="2" class="SectionHeader">Seller-Paid</td>
                {{/if}}
                <td class="SectionHeader">Paid by Others</td>
            </tr>
            <tr class="SubSectionHeader">
                <td class="SectionHeader DescriptionContainer">Loan Costs</td>
                <td>At Closing</td>
                <td>Before Closing</td>
                
                {{#if sLoanTransactionInvolvesSeller == 1}}
                    <td>At Closing</td>
                    <td>Before Closing</td>
                {{/if}}
                
                <td></td>
            </tr>
            {{#SectionList:i}}
                {{#if SectionName.indexOf("A -") > -1 || SectionName.indexOf("B -") > -1 || SectionName.indexOf("C -") > -1 }}
                    <tr style="background-color: white">
                        <td class="SubSectionHeader DescriptionContainer"> {{SectionName}} </td>
                        <td colspan="2" class="SubSectionHeader">{{TotalAmountBorrPaid}}</td>
                        
                        {{#if sLoanTransactionInvolvesSeller == 1}}
                                <td></td>
                                <td></td>
                        {{/if}}
                        <td></td>
                    </tr>
                    
                    {{#ClosingCostFeeList:j}}
                        <tr class="{{j % 2 == 0 ? 'GridItem':'GridAlternatingItem'}}">
                            <td class="DescriptionTD">
                                <span class="FeeDescriptionSpan">  {{#if is_title}} Title - {{/if}} {{desc}} {{#if is_optional}} (optional) {{/if}} </span>
                                <span class="FeeDescriptionSpan">
                                    {{#if bene_desc != ""}}
                                        to  {{bene_desc}} 
                                    {{/if}}
                                 </span>
                            </td>
                                <td>
                                    {{#pmts: k}}
                                        {{#if (paid_by == 1 || paid_by == 3) && pmt_at == 1}}
                                            {{amt}}
                                        {{/if}}
                                    {{/pmts}}
                                </td>
                                <td>
                                    {{#pmts: k}}
                                        {{#if (paid_by == 1 || paid_by == 3) && pmt_at == 2}}
                                            {{amt}}
                                        {{/if}}
                                    {{/pmts}}
                                </td>
                                
                                {{#if sLoanTransactionInvolvesSeller == 1}}
                                <td>
                                    {{#pmts: k}}
                                        {{#if (paid_by == 2) && pmt_at == 1}}
                                            {{amt}}
                                        {{/if}}
                                    {{/pmts}}
                                </td>
                                <td>
                                    {{#pmts: k}}
                                        {{#if (paid_by == 2) && pmt_at == 2}}
                                            {{amt}}
                                        {{/if}}
                                    {{/pmts}}
                                </td>
                                {{/if}}
                                <td>
                                    {{#pmts: k}}
                                        {{#if (paid_by != 1 && paid_by != 3 && paid_by != 2)}}
                                            {{amt}}
                                        {{/if}}
                                    {{/pmts}}
                                </td>
                        </tr>
                    {{/ClosingCostFeeList}}
                {{/if}}
            {{/SectionList}}
            
            <tr style="background-color: white">
                <td class="SubSectionHeader DescriptionContainer"> D - Total Loan Costs (Borrower Paid) </td>
                <td colspan="2" class="SubSectionHeader" runat="server" id="sTRIDClosingDisclosureTotalLoanCosts2"></td>
                
                {{#if sLoanTransactionInvolvesSeller == 1}}
                <td></td>
                <td></td>
                {{/if}}
                <td></td>
            </tr>
            <tr class="GridItem">
                <td><span class="FixedSpan">Loan Costs Subtotals (A + B + C)</span></td>
                <td  runat="server" id="sTRIDClosingDisclosureTotalLoanCostsOfBorrowerAtClosingFees"></td>
                <td  runat="server" id="sTRIDClosingDisclosureTotalLoanCostsOfBorrowerBeforeClosingFees"></td>
                
                {{#if sLoanTransactionInvolvesSeller == 1}}
                    <td></td>
                    <td></td>
                {{/if}}
                <td></td>
            </tr>
            
            <tr class="SubSectionHeader">
                <td class="SectionHeader DescriptionContainer">Other Costs</td>
                <td colspan="2"></td>
                
                {{#if sLoanTransactionInvolvesSeller == 1}}
                    <td colspan="2"></td>
                {{/if}}
                <td></td>
            </tr>
            {{#SectionList:i}}
                {{#if SectionName.indexOf("E -") > -1 || SectionName.indexOf("F -") > -1 || SectionName.indexOf("G -") > -1 || SectionName.indexOf("H -") > -1}}
                    <tr style="background-color: white">
                        <td class="SubSectionHeader DescriptionContainer"> {{SectionName}} </td>
                        <td colspan="2" class="SubSectionHeader">{{TotalAmountBorrPaid}}</td>
                        
                        {{#if sLoanTransactionInvolvesSeller == 1}}
                        <td></td>
                        <td></td>
                        {{/if}}
                        <td></td>
                    </tr>
                    
                    {{#ClosingCostFeeList:j}}
                        <tr class="{{j % 2 == 1 ? 'GridItem':'GridAlternatingItem'}}">
                            <td  class="DescriptionTD">
                                <span class="FeeDescriptionSpan">  {{#if is_title}} Title - {{/if}}  {{desc}} {{#if is_optional}} (optional) {{/if}} </span>
                                <span class="FeeDescriptionSpan">
                                    {{#if bene_desc != ""}}
                                        to  {{bene_desc}} 
                                    {{/if}}
                                 </span>
                            </td>
                                <td>
                                    {{#pmts: k}}
                                        {{#if (paid_by == 1 || paid_by == 3) && pmt_at == 1}}
                                            {{amt}}
                                        {{/if}}
                                    {{/pmts}}
                                </td>
                                <td>
                                    {{#pmts: k}}
                                        {{#if (paid_by == 1 || paid_by == 3) && pmt_at == 2}}
                                            {{amt}}
                                        {{/if}}
                                    {{/pmts}}
                                </td>
                                
                                {{#if sLoanTransactionInvolvesSeller == 1}}
                                <td>
                                    {{#pmts: k}}
                                        {{#if (paid_by == 2) && pmt_at == 1}}
                                            {{amt}}
                                        {{/if}}
                                    {{/pmts}}
                                </td>
                                <td>
                                    {{#pmts: k}}
                                        {{#if (paid_by == 2) && pmt_at == 2}}
                                            {{amt}}
                                        {{/if}}
                                    {{/pmts}}
                                </td>
                                {{/if}}
                                <td>
                                    {{#pmts: k}}
                                        {{#if (paid_by != 1 && paid_by != 3 && paid_by != 2)}}
                                            {{amt}}
                                        {{/if}}
                                    {{/pmts}}
                                </td>
                        </tr>
                    {{/ClosingCostFeeList}}
                {{/if}}
            {{/SectionList}}
            
            <tr style="background-color: white">
                <td class="SubSectionHeader DescriptionContainer"> I - Total Other Costs (Borrower-Paid) </td>
                <td colspan="2" class="SubSectionHeader" runat="server" id="sTRIDTotalOfBorrowerFees"></td>
                
                {{#if sLoanTransactionInvolvesSeller == 1}}
                <td></td>
                <td></td>
                {{/if}}
                <td></td>
            </tr>
            <tr class="GridItem">
                <td><span class="FixedSpan">Other Costs Subtotals (E + F + G + H)</span></td>
                <td  runat="server" id="sTRIDTotalOfBorrowerAtClosingFees"></td>
                <td  runat="server" id="sTRIDTotalOfBorrowerBeforeClosingFees"></td>
                
                {{#if sLoanTransactionInvolvesSeller == 1}}
                <td></td>
                <td></td>
                {{/if}}
                <td></td>
            </tr>
            
            <tr style="background-color: white">
                <td class="SubSectionHeader DescriptionContainer"> J - Total Closing Costs (Borrower-Paid) </td>
                <td colspan="2" class="SubSectionHeader" runat="server" id="sTRIDClosingDisclosureTotalClosingCosts3"></td>
                
                {{#if sLoanTransactionInvolvesSeller == 1}}
                <td></td>
                <td></td>
                {{/if}}
                <td></td>
            </tr>
            <tr class="GridItem">
                <td><span class="FixedSpan">Closing Costs Subtotals (D + I)</span></td>
                <td runat="server" id="sTRIDClosingDisclosureTotalAllCostsBorrowerAtClosing"></td>
                <td runat="server" id="sTRIDClosingDisclosureTotalAllCostsBorrowerBeforeClosing" ></td>
                
                {{#if sLoanTransactionInvolvesSeller == 1}}
                <td runat="server" id="sTRIDClosingDisclosureTotalAllCostsSellerAtClosing"></td>
                <td runat="server" id="sTRIDClosingDisclosureTotalAllCostsSellerBeforeClosing"></td>
                {{/if}}
                <td runat="server" id="sTRIDClosingDisclosureTotalAllCostsOther"></td>
            </tr>
            <tr class="GridItem">
                <td><span class="FixedSpan">Lender Credits
                {{#if sToleranceCure != null}}
                    (Includes <span style="color:red;">{{sToleranceCure}}</span> credit for increase in Closing Costs above legal limit.)
                {{/if}}
                </span></td>
                <td runat="server" id="sTRIDNegativeClosingDisclosureGeneralLenderCredits"></td>
                <td></td>
                
                {{#if sLoanTransactionInvolvesSeller == 1}}
                <td></td>
                <td></td>
                {{/if}}
                <td></td>
            </tr>
        </table>
        
    </div>
    
  </script>
  
    <script id="ProjectedPaymentsTemplate" type="text/ractive">
        <table>
            <thead>
                <tr>
                    <th class="GridHeader">Payment Calculation</th>
                    {{#ProjectedPayments}}
                        <th colspan="3">
                            {{#if IsFinalBalloonPayment}}
                                Final Payment
                            {{else}}
                                {{#if StartYear === EndYear}}
                                    Year {{StartYear}}
                                {{else}}
                                    Years {{StartYear}} - {{EndYear}}
                                {{/if}}
                            {{/if}}
                        </th>
                    {{/ProjectedPayments}}
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="label">Principal & Interest</td>
                    {{#ProjectedPayments}}
                        <td class="plus">&nbsp;</td>
                        <td class="dollarAmount">
                            {{PIMin_rep}}
                        </td>
                        <td class="minMax">
                            {{#if PIMin !== PIMax}}
                                min
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                    {{/ProjectedPayments}}
                </tr>
                <tr>
                    <td class="label">&nbsp;</td>
                    {{#ProjectedPayments}}
                        <td class="plus">&nbsp;</td>
                        <td class="dollarAmount">
                            {{#if PIMin !== PIMax}}
                                {{PIMax_rep}} 
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                        <td class="minMax">
                            {{#if PIMin !== PIMax}}
                                max
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                    {{/ProjectedPayments}}
                </tr>
                <tr>
                    <td class="label">&nbsp;</td>
                    {{#ProjectedPayments}}
                        <td class="plus">&nbsp;</td>
                        <td class="interestOnly">
                            {{#if HasInterestOnly}}
                                only interest
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                        <td class="minMax">&nbsp;</td>
                    {{/ProjectedPayments}}
                </tr>
                <tr>
                    <td class="label">Mortgage Insurance</td>
                    {{#ProjectedPayments}}
                        <td class="plus">
                            +
                        </td>
                        <td class="dollarAmount">
                            {{#if MI !== 0}}
                                {{MI_rep}}
                            {{else}}
                                &#151; {{! long dash }}
                            {{/if}}
                        </td>
                        <td class="minMax">&nbsp;</td>
                    {{/ProjectedPayments}}
                </tr>
                <tr>
                    <td class="label">Estimated Escrow</td>
                    {{#ProjectedPayments}}
                        <td class="plus">
                            + 
                        </td>
                        <td class="dollarAmount">
                            {{#if Escrow !== 0}}
                                {{Escrow_rep}}
                            {{else}}
                                &#151; {{! long dash }}
                            {{/if}}
                        </td>
                        <td class="minMax">&nbsp;</td>
                    {{/ProjectedPayments}}
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td rowspan="2" class="label">Estimated Total<br />Monthly Payment</td>
                    {{#ProjectedPayments}}
                        <td class="plus">&nbsp;</td>
                        <td class="dollarAmount">
                            {{PaymentMin_rep}}
                        </td>
                        <td class="minMax">
                            {{#if PaymentMin_rep !== PaymentMax_rep}}
                                min
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                    {{/ProjectedPayments}}
                </tr>
                <tr>
                    {{#ProjectedPayments}}
                        <td class="plus">&nbsp;</td>
                        <td class="dollarAmount">
                            {{#if PaymentMin_rep !== PaymentMax_rep}}
                                {{PaymentMax_rep}}
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                        <td class="minMax">
                            {{#if PaymentMin_rep !== PaymentMax_rep}}
                                max
                            {{else}}
                                &nbsp;
                            {{/if}}
                        </td>
                    {{/ProjectedPayments}}
                </tr>
            </tfoot>
        </table>
    </script>
  <div id="ConfirmAgent" style="display:none;">Selecting this contact will add it to the official contact list.</div>
  <div id="ClearAgent" style="display:none;">This action will remove the contact from the official contact list.</div>
    
    <div id="AmortizationTableDiv">
        <iframe id="AmortizationTableFrame"></iframe>
    </div>
    <div class="MainRightHeader" runat="server" id="PageHeader">Closing Disclosure</div>
    <div class="SectionDiv FieldLabel" runat="server" id="ArchiveRow">
        Last Disclosed Closing Disclosure archive:
        <span style="font-weight: normal;"><ml:EncodedLiteral runat="server" ID="LastDisclosedClosingDisclosureDesc" ></ml:EncodedLiteral></span>
    </div>
      <div class="SectionDiv" runat="server" ID="SelectArchiveRow">
            <table>
                <tr>
                    <td class="FieldLabel">Select Archived Closing Disclosure:</td>
                    <td><asp:DropDownList runat="server" ID="ddlArchives" AutoPostBack="true" AlwaysEnable="true"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Selected Archived Closing Disclosure Status:</td>
                    <td><asp:DropDownList runat="server" ID="ArchiveStatus" Enabled="false" AlwaysEnable></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="FieldLabel">Last Disclosed Closing Disclosure Archive:</td>
                    <td id="LastDisclosedClosingDisclosureArchiveDateCell">
                        <ml:EncodedLiteral runat="server" ID="LastDisclosedClosingDisclosureArchiveDate" ></ml:EncodedLiteral>
                    </td>
                </tr>
            </table>
	  </div>
    <div class="PageContainer">
        <div class="TopDivContainer">
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="FieldLabel">Closing Date</td>
                                <td> <input type="checkbox" id="sDocMagicClosingDLckd" onclick="lockElement('sDocMagicClosingD');" runat="server"/><ml:DateTextBox ID="sDocMagicClosingD" runat="server"></ml:DateTextBox></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Per-diem Interest Start Date</td>
                                <td> <input type="checkbox" id="sConsummationDLckd" onclick="lockElement('sConsummationD');" runat="server"/><ml:DateTextBox ID="sConsummationD" ToolTip="This date is used to calculate Per-diem Interest" runat="server"></ml:DateTextBox></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Disbursement Date</td>
                                <td> <input type="checkbox" id="sDocMagicDisbursementDLckd" onclick="lockElement('sDocMagicDisbursementD');" runat="server"/><ml:DateTextBox ID="sDocMagicDisbursementD" runat="server"></ml:DateTextBox></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Settlement File#</td>
                                <td><input type="text" runat="server" id="sSettlementAgentFileNum" /></td>
                            </tr>
                            <tr>
                                <td id="ValueDesc" class="FieldLabel" readonly runat="server">Sale Price</td>
                                <td>
                                    <ml:MoneyTextBox readonly runat="server" ID="sPurchasePrice1003"></ml:MoneyTextBox>
                                    <ml:MoneyTextBox readonly runat="server" ID="sApprVal"></ml:MoneyTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Property</td>
                                <td><input type="text" readonly runat="server" id="sSpAddr" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="text" readonly runat="server" id="sSpCity" />
                                    <ml:StateDropDownList runat="server" ID="sSpState" disabled />
                                    <ml:ZipcodeTextBox runat="server" ID="sSpZip" ReadOnly></ml:ZipcodeTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td class="FieldLabel" style="vertical-align:top"><a href="#" onclick="linkMe('../BorrowerInfo.aspx');">Applicants</a></td>
                                <td><div id="AddressInfo"></div></td>
                            </tr>
                            <tr id="SellerInfoRow" runat="server">
                                <td class="FieldLabel" style="vertical-align: top;">
                                    <a href="#" onclick="linkMe('../TitleAndEstate.aspx');">Sellers</a>
                                </td>
                                <td>
                                    <br />
                                    <table cellspacing="0" cellpadding="0" id="SellerInfoTable">
                                        <asp:Repeater runat="server" ID="SellerInfoRepeater" OnItemDataBound="SellerInfo_ItemDataBound">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <ml:EncodedLiteral ID="SellerName" runat="server" ></ml:EncodedLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <ml:EncodedLiteral ID="SellerStreetAddr" runat="server" ></ml:EncodedLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <ml:EncodedLiteral ID="SellerCityStateZip" runat="server" ></ml:EncodedLiteral>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td class="FieldLabel">Loan Term / Due (months)</td>
                                <td><input type="text" class="Month" runat="server" id="sTerm" readonly /> / <input type="text" class="Month" runat="server" id="sDue" readonly /></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Amortization Type</td>
                                <td><asp:DropDownList runat="server" disabled ID="sFinMethT"></asp:DropDownList></td>
                            </tr>
                            <tr id="PurposeRow" runat="server">
                                <td class="FieldLabel">
                                    <span class="sTridLPurposeT">TRID Loan</span>
                                    Purpose
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" CssClass="sLPurposeT" disabled ID="sLPurposeT"></asp:DropDownList>
                                    <asp:DropDownList runat="server" CssClass="sTridLPurposeT" disabled ID="sTridLPurposeT"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Loan Type</td>
                                <td><asp:DropDownList runat="server" disabled ID="sLT"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Loan ID#</td>
                                <td><input type="checkbox" runat="server" id="sLenderCaseNumLckd" /><input type="text" runat="server" id="sLenderCaseNum" /></td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">MI Certificate ID</td>
                                <td><input type="text" runat="server" readonly id="sMICertID" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        
        </div>
        
        <div class="LoanTermsSection SectionDiv">
            <div class="GridHeader"> Loan Terms</div>
            <div class="LoanTermOptions">
                <div>
                    <span class="FieldLabel FieldDesc"">Total Loan Amount</span>
                    <span class="FieldContainer"><ml:MoneyTextBox runat="server" ID="sFinalLamt" readonly></ml:MoneyTextBox></span>
                    <span class="FieldLabel LoanTermsDesc">Can Increase After Closing?</span>
                    <div class="LoanTermsInfo">
                        <asp:RadioButton ID="sGfeCanLoanBalanceIncrease_False" Checked="true" runat="server" GroupName="sGfeCanLoanBalanceIncrease" Enabled="false" />No
                        <asp:RadioButton ID="sGfeCanLoanBalanceIncrease_True" runat="server" GroupName="sGfeCanLoanBalanceIncrease" Enabled="false" /> Yes, within the first
                        <input type="text" class="Month" readonly runat="server" id="sPmtAdjRecastStop" /> months the balance may increase to a maximum of 
                        <ml:MoneyTextBox ReadOnly runat="server" ID="sGfeMaxLoanBalance"></ml:MoneyTextBox>
                    </div>
                </div>
                <div>
                    <span class="FieldLabel FieldDesc">Interest Rate</span>
                    <span class="FieldContainer"><ml:PercentTextBox runat="server" ID="sTridNoteIR" readonly></ml:PercentTextBox></span>
                    <span class="FieldLabel LoanTermsDesc">Can Increase After Closing?</span>
                    <div class="LoanTermsInfo">
                        <div>
                            <asp:RadioButton ID="sGfeCanRateIncrease_False" Checked="true" runat="server" GroupName="sGfeCanRateIncrease" Enabled="false" />No
                            <asp:RadioButton ID="sGfeCanRateIncrease_True" runat="server" GroupName="sGfeCanRateIncrease" Enabled="false" /> Yes, the interest rate adjusts every
                            <input type="text" class="Month" readonly runat="server" id="sRAdjCapMon" />months starting after the first
                            <input type="text" class="Month" readonly runat="server" id="sRAdj1stCapMon" /> months.
                        </div>
                        <div class="IndentedSection">
                            The maximum possible rate is <ml:PercentTextBox runat="server" ReadOnly ID="sTRIDInterestRateMaxEver"></ml:PercentTextBox> and may apply after the first
                            <input type="text" class="Month" readonly runat="server" id="sInterestRateMaxEverStartAfterMon" /> months.
                        </div>
                    </div>
                </div>
                <div>
                    <span class="FieldLabel FieldDesc">Monthly P&amp;I</span>
                    <span class="FieldContainer"><ml:MoneyTextBox runat="server" ID="sTridProThisMPmt" readonly></ml:MoneyTextBox></span>
                    <span class="FieldLabel LoanTermsDesc">Can Increase After Closing?</span>
                    <div class="LoanTermsInfo">
                        <asp:RadioButton ID="sGfeCanPaymentIncrease_False" Checked="true" runat="server" GroupName="sGfeCanPaymentIncrease" Enabled="false" />No
                        <asp:RadioButton ID="sGfeCanPaymentIncrease_True" runat="server" GroupName="sGfeCanPaymentIncrease" Enabled="false" /> Yes, the monthly P&I can adjust every
                        <input type="text" class="Month" readonly runat="server" id="sTRIDPIPmtSubsequentAdjustFreqMon" />months starting after
                        <input type="text" class="Month" readonly runat="server" id="sPIPmtFirstChangeAfterMon" /> months.
                        
                        
                        <div class="IndentedSection">
                            The maximum possible payment is <ml:MoneyTextBox readonly runat="server" ID="sTRIDPIPmtMaxEver"></ml:MoneyTextBox> and may apply after the first 
                            <input type="text" readonly class="Month" runat="server" id="sPIPmtMaxEverStartAfterMon" /> months.
                        </div>
                        <div class="IndentedSection">
                            The payment is interest only for the first <input type="text" class="Month" readonly runat="server" id="sIOnlyMon" /> months.
                        </div>
                        
                    </div>
                </div>
                <div>
                    <span class="FieldLabel FieldDesc""></span>
                    <span class="FieldContainer"></span>
                    <span class="FieldLabel LoanTermsDesc">This loan has a prepayment penalty?</span>
                    <div class="LoanTermsInfo">
                        <asp:RadioButton ID="sGfeHavePpmtPenalty_False" Checked="true" runat="server" GroupName="sGfeHavePpmtPenalty" Enabled="false" />No
                        <asp:RadioButton ID="sGfeHavePpmtPenalty_True" runat="server" GroupName="sGfeHavePpmtPenalty" Enabled="false" /> Yes, there is a maximum penalty of up to
                        <ml:MoneyTextBox ReadOnly runat="server" ID="sGfeMaxPpmtPenaltyAmt"></ml:MoneyTextBox> if the loan is paid off within the first
                        <div class="IndentedSection">
                            <input type="text" class="Month" readonly runat="server" id="sHardPlusSoftPrepmtPeriodMonths" /> months.
                        </div>
                    </div>
                </div>
                <div>
                    <span class="FieldLabel FieldDesc""></span>
                    <span class="FieldContainer"></span>
                    <span class="FieldLabel LoanTermsDesc">This loan has a balloon payment?</span>
                    <div class="LoanTermsInfo">
                        <asp:RadioButton ID="sGfeIsBalloon_False" Checked="true" runat="server" GroupName="sGfeIsBalloon" />No
                        <asp:RadioButton ID="sGfeIsBalloon_True" runat="server" GroupName="sGfeIsBalloon" /> Yes, a balloon payment of
                        <ml:MoneyTextBox ReadOnly runat="server" ID="sGfeBalloonPmt"></ml:MoneyTextBox> is due in
                        <input type="text" class="Month" readonly runat="server" id="sDue2" /> months.
                    </div>
                </div>
            </div>
        </div>
        
        <div class="SectionDiv">
            <div class="GridHeader">Projected Payments</div>
            <div>
                <div id="ProjectedPaymentsHeaderRow" class="InlineDiv">
                    <span class="FieldLabel">Estimated Closing Date</span>
                    <ml:DateTextBox runat="server" ID="sEstCloseD"></ml:DateTextBox>
                    <input type="checkbox" onclick="lockElement('sEstCloseD')" runat="server" value="Lock" id="sEstCloseDLckd" />

                    <span class="FieldLabel">Per-diem Interest Start Date</span>
                    <ml:DateTextBox runat="server" ID="sConsummationD2"></ml:DateTextBox>
                    <input type="checkbox" onclick="lockElement('sConsummationD2')" runat="server" value="Lock" id="sConsummationD2Lckd" />

                    <span class="FieldLabel">1st Payment Date</span>
                    <ml:DateTextBox runat="server" ID="sSchedDueD1"></ml:DateTextBox>
                    <input type="checkbox" onclick="lockElement('sSchedDueD1')" runat="server" value="Lock" id="sSchedDueD1Lckd" />

                    <input type="button" value="View Amortization Schedule" onclick="showAmortTable();" id="ViewAmortizationScheduleBtn" alwaysenable="true" runat="server" />
                </div>

                <div id="ProjectedPayments"></div>

                <table id="HousingExpensesTable">
                    <tr>
                        <td class="align">
                            <a href="#" class="FieldLabel" onclick="linkMe('../Forms/PropHousingExpenses.aspx'); return false;">
                                Estimated Taxes Insurance & Assessments
                            </a>
                        </td>
                        <td class="align">
                            <ml:MoneyTextBox runat="server" ID="sMonthlyNonPINonMIExpenseTotalPITI" ReadOnly="true"></ml:MoneyTextBox>  a month
                        </td>
                        <td id="ExpensesContainer">
                        </td>
                    </tr>
                </table>
            </div>
        </div>

            <div id="ClosingCostDetailsSection" class="SectionDiv">
                <div class="GridHeader">Closing Cost Details</div>
                <div id="ClosingCostSummaryContainer"></div>
                
            </div>
            
            <div  class="CompactContainer">
                <div class="HalfDiv">
                    <div class="CostsAtClosingDiv SectionDiv">
                        <div class="GridHeader">Costs at Closing</div>
                        <div>
                            <span class="FieldLabel">Total Loan Costs (A+B+C)</span>
                            <span class="MoneyContainer">
                            <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureTotalLoanCosts"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div>
                            <span class="FieldLabel">+ Total Other Costs (E+F+G+H)</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureTotalOtherCosts"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <div>
                            <span class="FieldLabel">- <a href="#" onclick="linkMe('../Forms/LenderCredits.aspx'); return false;">Lender Credits</a> </span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureGeneralLenderCredits"></ml:MoneyTextBox>
                            </span>
                        </div>
                        <hr />
                        <div>
                            <span class="FieldLabel">Total Closing Costs</span> 
                            <span class="MoneyContainer">
                                <ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureTotalClosingCosts"></ml:MoneyTextBox>
                            </span>
                        </div>
                    </div>
                    
                    <div class="CashToCloseDiv SectionDiv">
                        <div class="GridHeader">Cash to Close</div>
                        <div>
                            <span class="FieldLabel Description">Cash to Close Calculation Method</span>
                            <span colspan="2">
                                <asp:RadioButton runat="server" Enabled="false" ID="sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT_Standard" GroupName="sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT" value="0" /> Standard
                                <asp:RadioButton runat="server" Enabled="false" ID="sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT_Alternative" GroupName="sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT" value="1" /> Alternative
                            </span>
                        </div>
                        <div>
                            <span class="FieldLabel"></span>
                            <span class="FieldLabel ColumnHeader">Loan Estimate</span>
                            <span class="FieldLabel ColumnHeader">Closing Disclosure</span>
                        </div>
                        <div  id="TotalLoanAmountRow" runat="server">
                            <span class="FieldLabel Description">Total Loan Amount</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureLastLoanEstimateFinalLAmt"></ml:MoneyTextBox></span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sFinalLAmt3"></ml:MoneyTextBox></span>
                        </div>
                        <div>
                            <span class="FieldLabel Description" id="TotalClosingCostsTD" runat="server">Total Closing Costs</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureLastLoanEstimateTotalClosingCosts"></ml:MoneyTextBox></span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureTotalClosingCosts2"></ml:MoneyTextBox></span>
                        </div>
                        <div>
                            <span class="FieldLabel Description" id="ClosingCostsPaidBeforeClosingTD" runat="server">Closing Costs Paid Before Closing</span>
                            <span class="FieldLabel MoneyContainer">$0.00</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureClosingCostsPaidBeforeClosing"></ml:MoneyTextBox></span>
                        </div>
                        <div id="TotalPayoffsPaymentsRow" runat="server">
                            <span class="FieldLabel Description" id="TotalPayoffsPaymentsTD">- Total Payoffs and Payments</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureLastLoanEstimateTotalPayoffsAndPayments"></ml:MoneyTextBox></span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDTotalPayoffsAndPayments"></ml:MoneyTextBox></span>
                        </div>
                        <div id="ClosingCostsFinancedRow" runat="server">
                            <span class="FieldLabel Description">- Closing Costs Financed</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureLastLoanEstimateBorrowerFinancedClosingCosts"></ml:MoneyTextBox></span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureBorrowerFinancedClosingCosts"></ml:MoneyTextBox></span>
                        </div>
                        <div id="DownPaymentFundsRow" runat="server">
                            <span class="FieldLabel Description">+ Down Payment / Funds from Borrower</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureLastLoanEstimateDownPaymentOrFundsFromBorrower"></ml:MoneyTextBox></span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureDownPaymentOrFundsFromBorrower"></ml:MoneyTextBox></span>
                        </div>
                        <div id="CashDepositRow" runat="server">
                            <span class="FieldLabel Description">- Cash Deposit</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureLastLoanEstimateCashDeposit"></ml:MoneyTextBox></span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDCashDeposit"></ml:MoneyTextBox></span>
                        </div>
                        <div id="FundsForBorrRow" runat="server">
                            <span class="FieldLabel Description">- Funds for Borrower</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureLastLoanEstimateFundsForBorrower"></ml:MoneyTextBox></span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureFundsForBorrower"></ml:MoneyTextBox></span>
                        </div>
                        <div id="SellerCreditsRow" runat="server">
                            <span class="FieldLabel Description">- Seller Credits</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureLastLoanEstimateSellerCredits"></ml:MoneyTextBox></span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDSellerCredits"></ml:MoneyTextBox></span>
                        </div>
                        <div id="AdjustmentsCreditsRow" runat="server">
                            <span class="FieldLabel Description">- Adjustments and Other Credits</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureLastLoanEstimateAdjustmentsAndOtherCredits"></ml:MoneyTextBox></span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDAdjustmentsAndOtherCredits"></ml:MoneyTextBox></span>
                        </div>
                        <hr />
                        <div>
                            <span class="FieldLabel Description">Cash to Close</span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureLastLoanEstimateCashToClose"></ml:MoneyTextBox></span>
                            <span class="MoneyContainer"><ml:MoneyTextBox ReadOnly runat="server" ID="sTRIDClosingDisclosureCashToClose"></ml:MoneyTextBox></span>
                        </div>
                    </div>
                </div>
                
                <div class="HalfDiv">
                    <div class="SectionDiv">
                    <div class="GridHeader">Adjustable Payment (AP) Table</div>
                        <table runat="server" id="APTable">
                            <tr class="GridItem">
                                <td class="FieldLabel">Interest Only Payments?</td>
                                <td id="IsInterestOnlyPmts" runat="server"></td>
                            </tr>
                            <tr class="GridAlernatingItem">
                                <td class="FieldLabel">Optional Payments?</td>
                                <td id="IsOptionalPmts"></td>
                            </tr>
                            <tr class="GridItem">
                                <td class="FieldLabel">Step Payments?</td>
                                <td id="IsStepPmts" runat="server"></td>
                            </tr>
                            <tr class="GridAlernatingItem">
                                <td class="FieldLabel">Seasonal Payments?</td>
                                <td id="IsSeasonalPmts" runat="server"></td>
                            </tr>
                            <tr class="GridItem">
                                <td class="FieldLabel">Monthly Principal and Interest Payments</td>
                                <td></td>
                            </tr>
                            <tr class="GridAlernatingItem">
                                <td class="IndentedLine">First Change/Amount</td>
                                <td runat="server" id="MIFirstChange"></td>
                            </tr>
                            <tr class="GridItem">
                                <td class="IndentedLine">Subsequent Changes</td>
                                <td runat="server" id="MISubsequentChange"></td>
                            </tr>
                            <tr class="GridAlernatingItem">
                                <td class="IndentedLine">Maximum Payment</td>
                                <td runat="server" id="MIMaximumPayment"></td>
                            </tr>
                        </table>
                    </div>
                    
                    <div class="SectionDiv" >
                    <div class="GridHeader">Adjustable Interest Rate (AIR) Table</div>
                        <table runat="server" id="AIRTable">
                            <tr class="GridItem">
                                <td class="FieldLabel" id="IndexAndMarginDesc" runat="server"></td>
                                <td id="IndexAndMarginVal" runat="server"></td>
                            </tr>
                            <tr class="GridAlernatingItem">
                                <td class="FieldLabel">Initial Interest Rate</td>
                                <td id="InitialInterestRate"></td>
                            </tr>
                            <tr class="GridItem">
                                <td class="FieldLabel">Minimum / Maximum Interest Rate</td>
                                <td id="MinMaxInterestRate" runat="server"></td>
                            </tr>
                            <tr class="GridAlernatingItem">
                                <td class="FieldLabel">Change Frequency</td>
                                <td id="Td1" runat="server"></td>
                            </tr>
                            <tr class="GridItem">
                                <td class="IndentedLine">First Change</td>
                                <td id="FrequencyFirstChange"></td>
                            </tr>
                            <tr class="GridAlernatingItem">
                                <td class="IndentedLine">Subsequent Change</td>
                                <td id="FrequencySubsequentChange" runat="server"></td>
                            </tr>
                            <tr class="GridItem">
                                <td class="FieldLabel">Limits on Interest Rate Changes</td>
                                <td id="Td2" runat="server"></td>
                            </tr>
                            <tr class="GridAlernatingItem">
                                <td class="IndentedLine">First Change</td>
                                <td id="LimitFirstchange" runat="server"></td>
                            </tr>
                            <tr class="GridItem">
                                <td class="IndentedLine">Subsequent Changes</td>
                                <td id="LimitSubsequentChange" runat="server"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="SectionDiv">
                <div class="GridHeader" >Summaries of Transaction</div>
                <div class="AdjustmentsAndOtherCreditsContainer">
                        <div>
                            <div class="HalfDiv">
                                <div class="SectionHeader">
                                    <span>Borrower's Transaction</span>
                                </div>
                                <div class="SubSectionHeader">
                                    <span>K - Due From Borrower At Closing</span>
                                    <span runat="server" class="MoneyConstant" id="sTRIDTotalDueFromBorrowerAtClosing"></span>
                                </div>
                                <div id="sPurchasePrice1003Div" runat="server">
                                    <span>Sale Price of Property</span>
                                    <span class="MoneyConstant" id="sPurchasePrice1003_2" runat="server"></span>
                                </div>
                                <div id="sGrossDueFromBorrPersonalPropertyDiv" runat="server">
                                    <span>Sale Price of Any Personal Property Included in Sale</span>
                                    <span class="MoneyConstant" id="sGrossDueFromBorrPersonalProperty" runat="server"></span>
                                </div>
                                <div>
                                    <span>Closing Costs Paid at Closing (J)</span>
                                    <span class="MoneyConstant" id="sTRIDClosingDisclosureBorrowerCostsPaidAtClosing" runat="server"></span>
                                </div>
                                <div id ="div_sTRIDClosingDisclosureAltCost" runat="server">
                                    <ml:EncodedLabel ID="lbl_sTRIDClosingDisclosureAltCost" runat="server" />
                                    <span class="MoneyConstant" id="sTRIDClosingDisclosureAltCost" runat="server"></span>
                                </div>
                                <div id ="div_sLandIfAcquiredSeparately1003" runat="server">
                                    <span>Land (if acquired separately)</span>
                                    <span class="MoneyConstant" id="sLandIfAcquiredSeparately1003" runat="server"></span>
                                </div>
                                <div id="sRefPdOffAmt1003Div" runat="server">
                                    <span>Total Debts to be Paid Off</span>
                                    <span class="MoneyConstant" id="sRefPdOffAmt1003" runat="server"></span>
                                </div>
                                
                                <asp:Repeater runat="server" ID="AdjustmentsPaidFromBorr" OnItemDataBound="Adjustments_OnItemDataBound">
                                <ItemTemplate>
                                    <div>
                                        <span>
                                            <ml:EncodedLiteral runat="server" ID="adjustmentDescription" ></ml:EncodedLiteral>
                                        </span>
                                        <span class="MoneyConstant">
                                            <ml:EncodedLiteral runat="server" ID="adjustmentAmount" ></ml:EncodedLiteral>
                                        </span>
                                    </div>
                                </ItemTemplate>
                                </asp:Repeater> 
                            </div>
                            
                            <div class="HalfDiv" id="SellerDiv1" runat="server">
                                <div class="SectionHeader">
                                    <span>Seller's Transaction</span>
                                </div>
                                <div class="SubSectionHeader">
                                    <span>M - Due to Seller at Closing</span>
                                    <span runat="server" class="MoneyConstant" id="sTRIDTotalDueToSellerAtClosing"></span>
                                </div>
                                <div id="sPurchasePrice1003DivSeller" runat="server">
                                    <span>Sale Price of Property</span>
                                    <span class="MoneyConstant" id="sPurchasePrice1003_3" runat="server"></span>
                                </div>
                                <div>
                                    <span>Sale Price of Any Personal Property Included in Sale</span>
                                    <span class="MoneyConstant" id="sGrossDueFromBorrPersonalProperty2" runat="server"></span>
                                </div>
                                
                                <asp:Repeater runat="server" ID="AdjustmentsPaidToSeller" OnItemDataBound="Adjustments_OnItemDataBound">
                                    <ItemTemplate>
                                        <div>
                                            <span>
                                                <ml:EncodedLiteral runat="server" ID="adjustmentDescription" ></ml:EncodedLiteral>
                                            </span>
                                            <span class="MoneyConstant">
                                                <ml:EncodedLiteral runat="server" ID="adjustmentAmount" ></ml:EncodedLiteral>
                                            </span>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater> 
                            </div>
                        </div>
                        
                        <div>
                            <div class="HalfDiv" id="ProrationPaidBySellerAdvanceDiv1" runat="server" >
                                <div class="FieldLabel">Adjustments for Items Paid by Seller in Advance</div>
                                <asp:Repeater runat="server" ID="ProrationPaidBySellerAdvance1">
                                <ItemTemplate>
                                    <div>
                                        <span>
                                            <%# AspxTools.HtmlString(((Proration)Container.DataItem).Description) %>
                                        </span>
                                        <span class="MoneyConstant">
                                            <%# AspxTools.HtmlString(((Proration)Container.DataItem).Amount_rep) %>
                                        </span>
                                    </div>
                                </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="HalfDiv"  id="SellerDiv2" runat="server">
                                <div class="FieldLabel">Adjustments for Items Paid by Seller in Advance</div>
                                <asp:Repeater runat="server" ID="ProrationPaidBySellerAdvance2">
                                <ItemTemplate>
                                    <div>
                                        <span>
                                            <%# AspxTools.HtmlString(((Proration)Container.DataItem).Description) %>
                                        </span>
                                        <span class="MoneyConstant">
                                            <%# AspxTools.HtmlString(((Proration)Container.DataItem).Amount_rep) %>
                                        </span>
                                    </div>
                                </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        
                        <div>
                            <div class="HalfDiv">
                                <div class="SubSectionHeader">
                                    <span>L - Paid Already by or on Behalf of Borrower at Closing</span>
                                    <span runat="server" class="MoneyConstant" id="sTRIDTotalPaidAlreadyByOrOnBehalfOfBorrowerAtClosing"></span>
                                    </div>
                                <div id="sTRIDCashDepositDiv" runat="server">
                                    <span>Deposit</span>
                                    <span class="MoneyConstant" id="sTRIDCashDeposit2" runat="server"></span>
                                </div>
                                <div>
                                    <span>Loan Amount</span>
                                    <span class="MoneyConstant" id="sFinalLAmt2" runat="server"></span>
                                </div>
                                <div id="sONewFinNetProceedsDiv" runat="server">
                                    <span>Other New Financing (Principal Balance <ml:EncodedLiteral runat="server" id="sONewFinBal" ></ml:EncodedLiteral>)</span>
                                    <span class="MoneyConstant" id="sONewFinNetProceeds" runat="server"></span>
                                </div>
                                <div id="SellerCreditDiv" runat="server">
                                    <span>Seller Credit</span>
                                    <span class="MoneyConstant" id="SellerCredit1" runat="server"></span>
                                </div>
                                
                                <div class="FieldLabel">Other Credits</div>
                                <asp:Repeater runat="server" ID="AdjustmentsPaidToBorrower" OnItemDataBound="Adjustments_OnItemDataBound">
                                    <ItemTemplate>
                                        <div>
                                            <span>
                                                <ml:EncodedLiteral runat="server" ID="adjustmentDescription" ></ml:EncodedLiteral>
                                            </span>
                                            <span class="MoneyConstant">
                                                <ml:EncodedLiteral runat="server" ID="adjustmentAmount" ></ml:EncodedLiteral>
                                            </span>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater> 
                            </div>
                            <div class="HalfDiv"  id="SellerDiv3" runat="server">
                                <div class="SubSectionHeader">
                                    <span>N - Due from Seller at Closing</span>
                                    <span runat="server" class="MoneyConstant" id="sTRIDTotalDueFromSellerAtClosing"></span>
                                </div>
                                <div>
                                    <span>Excess Deposit</span>
                                    <span class="MoneyConstant" id="sReductionsDueToSellerExcessDeposit" runat="server"></span>
                                </div>
                                <div>
                                    <span>Closing Costs Paid at Closing (J)</span>
                                    <span class="MoneyConstant" id="sTotalSellerPaidClosingCostsPaidAtClosing" runat="server"></span>
                                </div>
                                <div>
                                    <span>Payoff of First Mortgage Loan</span>
                                    <span class="MoneyConstant" id="sReductionsDueToSellerPayoffOf1stMtgLoan" runat="server"></span>
                                </div>
                                <div>
                                    <span>Payoff of Second Mortgage Loan</span>
                                    <span class="MoneyConstant" id="sReductionsDueToSellerPayoffOf2ndMtgLoan" runat="server"></span>
                                </div>
                                <div>
                                    <span>Seller Credit</span>
                                    <span class="MoneyConstant" id="SellerCredit2" runat="server"></span>
                                </div>
                                
                                <asp:Repeater runat="server" ID="AdjustmentsPaidFromSeller" OnItemDataBound="Adjustments_OnItemDataBound">
                                    <ItemTemplate>
                                        <div>
                                            <span>
                                                <ml:EncodedLiteral runat="server" ID="adjustmentDescription" ></ml:EncodedLiteral>
                                            </span>
                                            <span class="MoneyConstant">
                                                <ml:EncodedLiteral runat="server" ID="adjustmentAmount" ></ml:EncodedLiteral>
                                            </span>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater> 
                            </div>
                        </div>
                        
                        <div>
                            <div class="HalfDiv" id="ProrationUnpaidBySeller1Div" runat="server">
                                <div class="FieldLabel">Adjustments for Items Unpaid by Seller</div>
                                <asp:Repeater runat="server" ID="ProrationUnpaidBySeller1">
                                <ItemTemplate>
                                    <div>
                                        <span>
                                            <%# AspxTools.HtmlString(((Proration)Container.DataItem).Description) %>
                                        </span>
                                        <span class="MoneyConstant">
                                            <%# AspxTools.HtmlString(((Proration)Container.DataItem).Amount_rep) %>
                                        </span>
                                    </div>
                                </ItemTemplate>
                                </asp:Repeater> 
                            </div>
                        
                            <div class="HalfDiv"  id="SellerDiv4" runat="server">
                                <div class="FieldLabel">Adjustments for Items Unpaid by Seller</div>
                                <asp:Repeater runat="server" ID="ProrationUnpaidBySeller2">
                                <ItemTemplate>
                                    <div>
                                        <span>
                                            <%# AspxTools.HtmlString(((Proration)Container.DataItem).Description) %>
                                        </span>
                                        <span class="MoneyConstant">
                                            <%# AspxTools.HtmlString(((Proration)Container.DataItem).Amount_rep) %>
                                        </span>
                                    </div>
                                </ItemTemplate>
                                </asp:Repeater> 
                            </div>
                        </div>
                        
                        <div>
                            <div class="HalfDiv">
                                <div class="SubSectionHeader">
                                    <span>Calculation</span>
                                </div>
                                <div>
                                    <span>Total Due From Borrower at Closing (K)</span>
                                    <span id="sTRIDTotalDueFromBorrowerAtClosing2" class="MoneyConstant" runat="server"></span>
                                </div>
                                <div>
                                    <span>Total Paid Already by or on Behalf of Borrower At Closing (L)</span>
                                    <span id="sTRIDTotalPaidAlreadyByOrOnBehalfOfBorrowerAtClosing2" class="MoneyConstant" runat="server"></span>
                                </div>
                                <hr />
                                <div class="FieldLabel">
                                    <span>Cash to Close</span>
                                    <span id="sTRIDSummariesOfTransactionCashFromBorrower" class="MoneyConstant" runat="server"></span>
                                </div>
                            </div>
                            <div class="HalfDiv"  id="SellerDiv5" runat="server">
                                <div class="SubSectionHeader">
                                    <span>Calculation</span>
                                </div>
                                <div>
                                    <span>Total Due to Seller at Closing (M)</span>
                                    <span id="sTRIDTotalDueToSellerAtClosing2" class="MoneyConstant" runat="server"></span>
                                </div>
                                <div>
                                    <span>Total Due from Seller at Closing (N)</span>
                                    <span id="sTRIDTotalDueFromSellerAtClosing2" class="MoneyConstant" runat="server"></span>
                                </div>
                                <hr />
                                <div class="FieldLabel">
                                    <span>Cash to Seller</span>
                                    <span id="sTRIDSummariesOfTransactionCashToSeller" class="MoneyConstant" runat="server"></span>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            
                <div class="SectionDiv">
                    <div class="GridHeader">Loan Disclosures</div>
                    <table id="OtherConsiderationsTable" style="width:100%">
                        <tr class="">
                            <td class="FieldLabel">Assumptions</td>
                            <td>
                                If you sell or transfer this property to another person, we <br />
                                <asp:RadioButton Enabled="false" runat="server" ID="sAssumeLT_Allow" GroupName="sAssumeLT" value="True" /> will allow, under certain conditions, this person to assume this loan on the original terms. <br />
                                <asp:RadioButton Enabled="false" runat="server" ID="sAssumeLT_NotAllow" GroupName="sAssumeLT" value="False" />will not allow assumption of this loan on the original terms.
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Demand Feature</td>
                            <td>
                                Your loan <br />
                                <asp:RadioButton runat="server" Enabled="false" ID="sHasDemandFeature_True" GroupName="sHasDemandFeature" /> has a demand feature which permits your lender to require early repayment of the loan.
                                <br />
                                <asp:RadioButton runat="server" Enabled="false" ID="sHasDemandFeature_False" GroupName="sHasDemandFeature" /> does not have a demand feature.
                            </td>
                        </tr>
                        <tr class="">
                            <td class="FieldLabel">Late Payments</td>
                            <td>If your payment is more than <asp:TextBox class="smallNum" ReadOnly runat="server" ID="sLateDays"></asp:TextBox> days late, we will charge a late fee of  <ml:PercentTextBox ReadOnly runat="server" ID="sLateChargePc"></ml:PercentTextBox> of <asp:TextBox ReadOnly runat="server" ID="sLateChargeBaseDesc"></asp:TextBox>.</td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Negative Amortization</td>
                            <td>
                                Under your loan terms you <br />
                                <asp:RadioButton ID="sTRIDNegativeAmortizationT_Scheduled" Enabled="false" GroupName="sTRIDNegativeAmortizationT" runat="server" /> are scheduled to make monthly payments that do not pay all of the interest due that month.
                                <br />
                                <asp:RadioButton ID="sTRIDNegativeAmortizationT_Potential" Enabled="false" GroupName="sTRIDNegativeAmortizationT"  runat="server" /> may have monthly payments that do not pay all of the interest due that month.
                                <br />
                                <asp:RadioButton ID="sTRIDNegativeAmortizationT_NoNegativeAmort" Enabled="false" GroupName="sTRIDNegativeAmortizationT"  runat="server" /> do not have a negative amortization feature.
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Partial payments</td>
                            <td>
                                Your lender <br />
                                <asp:RadioButton GroupName="sTRIDPartialPaymentAcceptanceT" ID="sTRIDPartialPaymentAcceptanceT_AcceptApplied" runat="server" /> may accept partial payments that are less than the full amount due (partial payments) and apply them to your loan.
                                <br />
                                <asp:RadioButton GroupName="sTRIDPartialPaymentAcceptanceT" ID="sTRIDPartialPaymentAcceptanceT_AcceptNotApplied" runat="server" /> may hold them in a separate account until you pay the rest of the payment, and then apply the full payment to your loan.
                                <br />
                                <asp:RadioButton GroupName="sTRIDPartialPaymentAcceptanceT" ID="sTRIDPartialPaymentAcceptanceT_NotAccepted" runat="server" /> does not accept partial payments.
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Security Interest</td>
                            <td>
                                You are granting a security interest in <input type="text" readonly id="sSpFullAddr" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel"><a href="#" id="EscrowLink">Escrow Account</a></td>
                            <td class="EscrowAccountTD">
                                For now, your loan
                                <br />
                                <div class="EscrowRadioDiv">
                                    <asp:RadioButton ID="sTridEscrowAccountExists_True" Enabled="false" GroupName="sTridEscrowAccountExists" runat="server" /> will have an escrow account to pay the property costs listed below because
                                </div>
                                <div class="EscrowRadioDiv">
                                    <div class="EscrowRadioDiv">
                                        <asp:RadioButton runat="server" ID="sNonMIHousingExpensesEscrowedReasonT_LenderRequired" GroupName="sNonMIHousingExpensesEscrowedReasonT" /> your lender requires an escrow account.
                                    </div>
                                    <br />
                                    <div class="EscrowRadioDiv">
                                        <asp:RadioButton runat="server" ID="sNonMIHousingExpensesEscrowedReasonT_BorrowerRequested" GroupName="sNonMIHousingExpensesEscrowedReasonT" /> you requested an escrow account (lender does not require).
                                    </div>
                                </div>
                                <br />
                                <div class="EscrowDataDiv" id="HasEscrowDiv" runat="server">
                                    <div class="GridItem display-table">
                                        <span class="EscrowDescriptionSpan">Escrowed Property Costs over Year 1</span>
                                        <span id="sClosingDisclosureEscrowedPropertyCostsFirstYear" runat="server"></span>
                                        <div style="display:inline-block;" id="EscrowDescriptionContainer"></div>
                                    </div>
                                    <div class="GridAlternatingItem display-table">
                                        <span class="EscrowDescriptionSpan">Non-Escrowed Property Costs over Year 1</span>
                                        <span id="sClosingDisclosureNonEscrowedPropertyCostsFirstYear" runat="server"></span>
                                        <div style="display:inline-block;" id="NonEscrowDescriptionContainer"></div>
                                    </div>
                                    <div class="GridItem display-table">
                                        <span class="EscrowDescriptionSpan">Initial Escrow Payment</span>
                                        <span id="sGfeInitialImpoundDeposit" runat="server"></span>
                                        <span class="EscrowDescriptionSpan"></span>
                                    </div>
                                    <div class="GridAlternatingItem display-table">
                                        <span class="EscrowDescriptionSpan">Monthly Escrow Payment</span>
                                        <span id="sClosingDisclosureMonthlyEscrowPayment" runat="server"></span>
                                        <span class="EscrowDescriptionSpan"></span>
                                    </div>
                                </div>
                                <br />
                                <div class="EscrowRadioDiv">
                                    <asp:RadioButton ID="sTridEscrowAccountExists_False" Enabled="false" GroupName="sTridEscrowAccountExists" runat="server" /> will not have an escrow account because 
                                </div>
                                <div class="EscrowRadioDiv">
                                    <div class="EscrowRadioDiv">
                                        <asp:RadioButton runat="server" ID="sNonMIHousingExpensesNotEscrowedReasonT_Declined" GroupName="sNonMIHousingExpensesNotEscrowedReasonT" /> you declined it.
                                    </div>
                                    <br />
                                    <div class="EscrowRadioDiv">
                                        <asp:RadioButton runat="server" ID="sNonMIHousingExpensesNotEscrowedReasonT_NotOffered" GroupName="sNonMIHousingExpensesNotEscrowedReasonT" /> your lender does not offer one.
                                    </div>
                                </div>
                                <br />
                                <div class="EscrowDataDiv" id="NonEscrowDiv" runat="server">
                                    <div class="GridItem">
                                        <span class="EscrowDescriptionSpan">Estimated Property Costs over Year 1</span>
                                        <span id="sClosingDisclosurePropertyCostsFirstYear" runat="server"></span>
                                    </div>
                                    <div class="GridAlternatingItem">
                                        <span class="EscrowDescriptionSpan">Escrowed Waiver Fee</span>
                                        <span><ml:MoneyTextBox runat="server" ID="sTRIDEscrowWaiverFee"></ml:MoneyTextBox></span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    
                </div>
                
                <div class="SectionDiv LoanCalculationsDiv">
                    
                    <div class="GridHeader">Loan Calculations</div>
                    
                    <table>
                        <tr>
                            <td class="FieldLabel"> Annual Percentage Rate (APR)</td>
                            <td><ml:PercentTextBox ID="sAPR" ReadOnly runat="server"></ml:PercentTextBox></td>
                            <td class="FieldLabel"> Finance Charge</td>
                            <td><ml:MoneyTextBox ID="sFinCharge" ReadOnly runat="server"></ml:MoneyTextBox></td>
                            <td class="FieldLabel">TRID Total of Payments</td>
                            <td><ml:MoneyTextBox ID="sTRIDTotalOfPayments" ReadOnly runat="server"></ml:MoneyTextBox></td>
                        </tr>
                        <tr>
                            <td class="FieldLabel"> Total Interest Percent (TIP)</td>
                            <td><ml:PercentTextBox ID="sCFPBTotalInterestPercent" ReadOnly runat="server"></ml:PercentTextBox></td>
                            <td class="FieldLabel"> Amount Financed</td>
                            <td><ml:MoneyTextBox ID="sFinancedAmt" ReadOnly runat="server"></ml:MoneyTextBox></td>
                        </tr>
                    </table>
                    
                    
                </div>
                
                <div class="SectionDiv">
                    <div class="GridHeader">Other Considerations</div>
                    <table id="OtherConsiderationsTable" style="width:100%">
                        <tr class="">
                            <td class="FieldLabel">Appraisal</td>
                            <td>We may order an appraisal to determine the property’s value and charge you for this appraisal. We will promptly give you a copy of any appraisal, even if your loan does not close. You can pay for an additional appraisal for your own use at your own cost.</td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Contract Details</td>
                            <td>
                                See your note and security instrument for information about
                                <div class="ContractDetailPoints">
                                    -what happens if you fail to make your payments,
                                    <br />
                                    -what is a default on the loan,
                                    <br />
                                    -situations in which your lender can require early repayments of the loan, and
                                    <br />
                                    -the rules for making payments before they are due.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Liability after Foreclosure</td>
                            <td>
                                If your lender forecloses on this property and the foreclosure does not cover the amount of unpaid balance on this loan,
                                <br />
                                <asp:RadioButton GroupName="sTRIDPropertyIsInNonRecourseLoanState" ID="sTRIDPropertyIsInNonRecourseLoanState_True" runat="server" /> state law may protect you from liability for the unpaid balance.  If you refinance or take on any additional debt on this property, 
                                you may lose this protection and have to pay any debt remaining even after foreclosure.  You may want to consult a lawyer for more information.
                                <br />
                                <asp:RadioButton GroupName="sTRIDPropertyIsInNonRecourseLoanState" id="sTRIDPropertyIsInNonRecourseLoanState_False" runat="server" /> state law does not protect you from liability for the unpaid balance.
                            </td>
                        </tr>
                        <tr class="">
                            <td class="FieldLabel">Refinance</td>
                            <td>Refinancing this loan will depend on your future financial situation, the property value, and market conditions. You may not be able to refinance this loan.</td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Tax Deductions</td>
                            <td>If you borrow more than this property is worth, the interest on the loan amount above this property's fair market value is not deductible from your federal income taxes.
                              You should consult a tax advisor for more information.</td>
                        </tr>
                    </table>
                </div>
                
                <div class="SectionDiv ContactDiv">
                    <div class="GridHeader">Contact Information</div>
                    
                    <table id="ContactsTable">
                            <tr>
                                <td class="ContactsTD ContactsLeft">
                                    <span class="ContactHeader">Lender</span>
                                    <div>
                                        <div class="CFMContainer id="LenderCFMContainer">
                                            <UC:CFM ID="LenderContact" runat="server" />
                                        </div>
                                        <div>
                                            <input type="hidden" runat="server" ID="sTRIDLenderAgentId" class="AgentId"></input>
                                            <span class="FieldLabel CFMLabel">Name</span><asp:TextBox runat="server" ID="sTRIDLenderName" CssClass="Name"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Address</span><asp:TextBox runat="server" ID="sTRIDLenderAddr"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">&nbsp</span><input id="sTRIDLenderCity" class="CityTextBox" runat="server" type="text" /><ml:StateDropDownList ID="sTRIDLenderState" runat="server" /><ml:ZipcodeTextBox ID="sTRIDLenderZip" class="ZipTextBox" runat="server"></ml:ZipcodeTextBox>
                                            <br />
                                            <span class="FieldLabel CFMLabel">NMLS #</span><asp:TextBox runat="server" ID="sTRIDLenderNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">License ID</span><asp:TextBox runat="server" ID="sTRIDLenderLicenseIDNum"></asp:TextBox> <br />
                                            
                                            <span class="FieldLabel CFMLabel">Contact</span><asp:TextBox runat="server" ID="sTRIDLenderContact"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact NMLS#</span><asp:TextBox runat="server" ID="sTRIDLenderContactNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact License ID#</span><asp:TextBox runat="server" ID="sTRIDLenderContactLicenseIDNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact Email</span><asp:TextBox runat="server" ID="sTRIDLenderContactEmail"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact Phone</span><ml:PhoneTextBox runat="server" ID="sTRIDLenderContactPhoneNum"></ml:PhoneTextBox>
                                            
                                        </div>
                                    </div>
                                </td>
                                <td class="ContactsTD ContactsLeft">
                                    <span class="ContactHeader">Mortgage Broker</span>
                                    <div>
                                        <div class="CFMContainer id="MortgageBrokerCFMContainer">
                                            <UC:CFM ID="MortgageBrokerContact" runat="server" />
                                        </div>
                                        <div>
                                            <input type="hidden" runat="server" ID="sTRIDMortgageBrokerAgentId" class="AgentId"></input>
                                            <span class="FieldLabel CFMLabel">Name</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerName" CssClass="Name"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Address</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerAddr"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">&nbsp</span><input id="sTRIDMortgageBrokerCity" class="CityTextBox" runat="server" type="text" /><ml:StateDropDownList ID="sTRIDMortgageBrokerState" runat="server" /><ml:ZipcodeTextBox ID="sTRIDMortgageBrokerZip" class="ZipTextBox" runat="server"></ml:ZipcodeTextBox>
                                            <br />
                                            <span class="FieldLabel CFMLabel">NMLS #</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">License ID</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerLicenseIDNum"></asp:TextBox> <br />
                                            
                                            <span class="FieldLabel CFMLabel">Contact</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerContact"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact NMLS#</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerContactNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact License ID#</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerContactLicenseIDNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact Email</span><asp:TextBox runat="server" ID="sTRIDMortgageBrokerEmail"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact Phone</span><ml:PhoneTextBox runat="server" ID="sTRIDMortgageBrokerPhoneNum"></ml:PhoneTextBox>
                                            
                                        </div>
                                    </div>
                                </td>
                                <td class="ContactsTD ContactsRight">
                                    <span class="ContactHeader">Settlement Agent</span>
                                    <div>
                                        <div class="CFMContainer id="SettlementAgentCFMContainer">
                                            <UC:CFM ID="SettlementAgentContact" runat="server" />
                                        </div>
                                        <div>
                                            <input type="hidden" runat="server" ID="sTRIDSettlementAgentAgentId" class="AgentId"></input>
                                            <span class="FieldLabel CFMLabel">Name</span><asp:TextBox runat="server" ID="sTRIDSettlementAgentName" CssClass="Name"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Address</span><asp:TextBox runat="server" ID="sTRIDSettlementAgentAddr"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">&nbsp</span><input id="sTRIDSettlementAgentCity" class="CityTextBox" runat="server" type="text" /><ml:StateDropDownList ID="sTRIDSettlementAgentState" runat="server" /><ml:ZipcodeTextBox ID="sTRIDSettlementAgentZip" class="ZipTextBox" runat="server"></ml:ZipcodeTextBox>
                                            <br />
                                            <span class="FieldLabel CFMLabel">NMLS #</span><asp:TextBox runat="server" ID="sTRIDSettlementAgentNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">License ID</span><asp:TextBox runat="server" ID="sTRIDSettlementAgentLicenseIDNum"></asp:TextBox> <br />
                                            
                                            <span class="FieldLabel CFMLabel">Contact</span><asp:TextBox runat="server" ID="sTRIDSettlementAgentContact"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact NMLS#</span><asp:TextBox runat="server" ID="sTRIDSettlementAgentContactNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact License ID#</span><asp:TextBox runat="server" ID="sTRIDSettlementAgentContactLicenseIDNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact Email</span><asp:TextBox runat="server" ID="sTRIDSettlementAgentEmail"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact Phone</span><ml:PhoneTextBox runat="server" ID="sTRIDSettlementAgentPhoneNum"></ml:PhoneTextBox>
                                            
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr> 
                                <td class="ContactsTD ContactsLeft">
                                    <span class="ContactHeader">Real Estate Broker (Buyer)</span>
                                    <div>
                                        <div class="CFMContainer id="RealEstateBrokerBuyerCFMContainer">
                                            <UC:CFM ID="RealEstateBrokerBuyerContact" runat="server" />
                                        </div>
                                        <div>
                                            <input type="hidden" runat="server" ID="sTRIDRealEstateBrokerBuyerAgentId" class="AgentId"></input>
                                            <span class="FieldLabel CFMLabel">Name</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerBuyerName" CssClass="Name"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Address</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerBuyerAddr"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">&nbsp</span><input id="sTRIDRealEstateBrokerBuyerCity" class="CityTextBox" runat="server" type="text" /><ml:StateDropDownList ID="sTRIDRealEstateBrokerBuyerState" runat="server" /><ml:ZipcodeTextBox ID="sTRIDRealEstateBrokerBuyerZip" class="ZipTextBox" runat="server"></ml:ZipcodeTextBox>
                                            <br />
                                            <span class="FieldLabel CFMLabel">NMLS #</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerBuyerNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">License ID</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerBuyerLicenseIDNum"></asp:TextBox> <br />
                                            
                                            <span class="FieldLabel CFMLabel">Contact</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerBuyerContact"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact NMLS#</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerBuyerContactNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact License ID#</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerBuyerContactLicenseIDNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact Email</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerBuyerEmail"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact Phone</span><ml:PhoneTextBox runat="server" ID="sTRIDRealEstateBrokerBuyerPhoneNum"></ml:PhoneTextBox>
                                            
                                        </div>
                                    </div>
                                </td>
                                <td class="ContactsTD">
                                    <span class="ContactHeader">Real Estate Broker (Seller)</span>
                                    <div>
                                        <div class="CFMContainer id="RealEstateBrokerSellerCFMContainer">
                                            <UC:CFM ID="RealEstateBrokerSellerContact" runat="server" />
                                        </div>
                                        <div>
                                            <input type="hidden" runat="server" ID="sTRIDRealEstateBrokerSellerAgentId" class="AgentId"></input>
                                            <span class="FieldLabel CFMLabel">Name</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerSellerName" CssClass="Name"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Address</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerSellerAddr"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">&nbsp</span><input id="sTRIDRealEstateBrokerSellerCity" class="CityTextBox" runat="server" type="text" /><ml:StateDropDownList ID="sTRIDRealEstateBrokerSellerState" runat="server" /><ml:ZipcodeTextBox ID="sTRIDRealEstateBrokerSellerZip" class="ZipTextBox" runat="server"></ml:ZipcodeTextBox>
                                            <br />
                                            <span class="FieldLabel CFMLabel">NMLS #</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerSellerNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">License ID</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerSellerLicenseIDNum"></asp:TextBox> <br />
                                            
                                            <span class="FieldLabel CFMLabel">Contact</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerSellerContact"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact NMLS#</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerSellerContactNMLSNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact License ID#</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerSellerContactLicenseIDNum"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact Email</span><asp:TextBox runat="server" ID="sTRIDRealEstateBrokerSellerEmail"></asp:TextBox> <br />
                                            <span class="FieldLabel CFMLabel">Contact Phone</span><ml:PhoneTextBox runat="server" ID="sTRIDRealEstateBrokerSellerPhoneNum"></ml:PhoneTextBox>
                                            
                                        </div>
                                    </div>
                                </td>
                                <td class="ContactsTD ContactsRight">
                                </td>
                            </tr>
                        </table>
                </div>
            
    </div>
    </form>

</body>
</html>
