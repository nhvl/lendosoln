using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice;
using LendersOffice.LoanPrograms;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Disclosure
{
	public partial class ARMEntryList : LendersOffice.Common.BasePage
	{
    
		protected void PageLoad(object sender, System.EventArgs e)
		{

            ARMIndexDescriptorSet armSet = new ARMIndexDescriptorSet();
            armSet.Retrieve(/*((BrokerUserPrincipal) this.User)*/ BrokerUserPrincipal.CurrentPrincipal.BrokerId);

            string html = BrokerUserPrincipal.CurrentPrincipal.BrokerName +  "<ul>";

            ArrayList fannieMaeArmIndexT = new ArrayList();
            ArrayList freddieArmIndexT = new ArrayList();
            ArrayList effectiveD = new ArrayList();
            ArrayList indexBasedOnVstr = new ArrayList();
            ArrayList indexCanBeFoundVstr = new ArrayList();
            int i;
            for (i = 0; i < armSet.Items.Count; i++) 
            {
                ARMIndexDescriptor o = (ARMIndexDescriptor) armSet.Items[i];
                html += string.Format("<li><a href='#' onclick='f_select({0});'>{1}</a>", i, AspxTools.HtmlString(o.IndexNameVstr));

                fannieMaeArmIndexT.Add("'" + o.FannieMaeArmIndexT.ToString("D") + "'");
                freddieArmIndexT.Add("'" + o.FreddieArmIndexT.ToString("D") + "'");
                effectiveD.Add(AspxTools.JsString(o.EffectiveD.ToShortDateString()));
                indexBasedOnVstr.Add(AspxTools.JsString(o.IndexBasedOnVstr));
                indexCanBeFoundVstr.Add(AspxTools.JsString(o.IndexCanBeFoundVstr));
            }
            html += "</ul>LQB ARM Indexes<ul>";

            foreach (SystemArmIndex armIndex in SystemArmIndex.ListSystemArmIndexes())
            {
                html += string.Format("<li><a href='#' onclick='f_select({0});'>{1}</a>", i++, AspxTools.HtmlString(armIndex.IndexNameVstr));

                fannieMaeArmIndexT.Add("'" + armIndex.FannieMaeArmIndexT.ToString("D") + "'");
                freddieArmIndexT.Add("'" + armIndex.FreddieArmIndexT.ToString("D") + "'");
                effectiveD.Add(AspxTools.JsString(armIndex.EffectiveD.ToShortDateString()));
                indexBasedOnVstr.Add(AspxTools.JsString(armIndex.IndexBasedOnVstr));
                indexCanBeFoundVstr.Add(AspxTools.JsString(armIndex.IndexCanBeFoundVstr));
            }
            html += "</ul>";

            m_html.Text = html;

            
            ClientScript.RegisterArrayDeclaration("fannieMaeArmIndexT", string.Join(",", (string[]) fannieMaeArmIndexT.ToArray(typeof(string))));
            ClientScript.RegisterArrayDeclaration("freddieArmIndexT", string.Join(",", (string[]) freddieArmIndexT.ToArray(typeof(string))));
            ClientScript.RegisterArrayDeclaration("effectiveD", string.Join(",", (string[]) effectiveD.ToArray(typeof(string))));
            ClientScript.RegisterArrayDeclaration("indexBasedOnVstr", string.Join(",", (string[]) indexBasedOnVstr.ToArray(typeof(string))));
            ClientScript.RegisterArrayDeclaration("indexCanBeFoundVstr", string.Join(",", (string[]) indexCanBeFoundVstr.ToArray(typeof(string))));
            
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
