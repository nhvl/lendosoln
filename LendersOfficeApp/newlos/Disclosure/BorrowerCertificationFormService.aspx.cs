///
/// Author: Matthew Flynn
/// 
using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Disclosure
{
    public class BorrowerCertificationFormServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(BorrowerCertificationFormServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.BorrCertification, E_ReturnOptionIfNotExist.CreateNew);

            broker.CompanyName = GetString("BorrCertificationCompanyName");
            broker.Update();
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }
	public partial class BorrowerCertificationFormService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new BorrowerCertificationFormServiceItem());
        }
		

	}
}
