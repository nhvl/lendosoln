using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.Reminders;
using DataAccess;

namespace LendersOfficeApp.newlos
{
    /// <summary>
    /// Summary description for TaskList.
    /// </summary>
    public partial class TaskList : BaseLoanPage
    {
        protected int selectedTabIndex;
		private int m_statusFilter 
        {
            get { return RequestHelper.GetInt("statusFilter", -1); }
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            RegisterJsScript("LQBPopup.js");

            if (Request.Params["__EVENTTARGET"] == "changeTab")
                selectedTabIndex = Int32.Parse(Request.Params["__EVENTARGUMENT"]);
            else if (ViewState["selectedTabIndex"] != null)
                selectedTabIndex = (Int32)ViewState["selectedTabIndex"];
            ViewState["selectedTabIndex"] = selectedTabIndex;

            tab0.Attributes.Remove("class");
            tab1.Attributes.Remove("class");
            tab2.Attributes.Remove("class");
            switch (selectedTabIndex) 
            {
                case 0:
                    tab0.Attributes.Add("class", "selected");
                    BindYourTaskDG();
                    break;
                case 1:
                    tab1.Attributes.Add("class", "selected");
                    BindYourTaskTrackDG();
                    break;
                case 2:
                    tab2.Attributes.Add("class", "selected");
                    BindOtherTaskDG();
                    break;
            }
        }

        private void BindYourTaskDG() 
        {
            // HACK: Don't know why the OnDelete event never get fired for this datagrid.
            //       Have to handle it manually here. dd 5/27/2003
            if (Page.IsPostBack) 
            {
                string target = Request.Form["__EVENTTARGET"];
                string arg = Request.Form["__EVENTARGUMENT"];

                if (target == m_yourTasksDG.ClientID) 
                {
                    try 
                    {
                        Guid notifId = new Guid(arg);
                        CDiscNotif notif = new CDiscNotif( this.BrokerID, notifId, Guid.Empty );
                        notif.DeleteNotif();
                        Tools.LogInfo("Deleting task notification was successful.");
                    } 
                    catch {}
                }
            }

            SqlParameter[] parameters  =  {
                                            new SqlParameter("@LoanID", LoanID),
                                            new SqlParameter("@UserID", UserID)
                                        };
            DataSet ds = new DataSet();
            DataSetHelper.Fill(ds, this.BrokerID, "ListDiscNotifByLoanIdOfUser", parameters);
            var dv = ds.Tables[0].DefaultView;
            dv.Sort = "SortAlertDate ASC, DiscCreatedDate DESC";

            m_yourTasksDG.Visible = true;
            m_yourTasksDG.DataSource = dv;
            m_yourTasksDG.DataBind();

        }
        private void BindYourTaskTrackDG() 
        {
            SqlParameter[] parameters =  {
                                          new SqlParameter("@LoanID", LoanID),
                                          new SqlParameter("@UserID", UserID)
                                      };
            DataSet ds = new DataSet();
            DataSetHelper.Fill(ds, this.BrokerID, "ListDiscTracksByCreatorUserIdLoanId", parameters);

            m_yourTaskTracksDG.Visible = true;
            m_yourTaskTracksDG.DataSource = ds.Tables[0].DefaultView;
            m_yourTaskTracksDG.DataBind();

        }
        private void BindOtherTaskDG() 
        {
            SqlParameter[] parameters =  {
                                           new SqlParameter("@LoanID", LoanID),
                                           new SqlParameter("@ThisUserID", UserID)
                                       };
            DataSet ds = new DataSet();
            DataSetHelper.Fill(ds, this.BrokerID, "ListDiscNotifByLoanIdOfOtherUsers", parameters);
            m_otherTasksDG.Visible = true;
            m_otherTasksDG.DataSource = ds.Tables[0].DefaultView;
            m_otherTasksDG.DataBind();

        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
//            m_statusDDL.Items.Add(new ListItem("Show All", "-1"));
//            m_statusDDL.Items.Add(Tools.CreateEnumListItem("Show Active only", E_DiscStatus.Active));
//            m_statusDDL.Items.Add(Tools.CreateEnumListItem("Show Canceled only", E_DiscStatus.Cancelled));
//            m_statusDDL.Items.Add(Tools.CreateEnumListItem("Show Suspended only", E_DiscStatus.Suspended));
//            m_statusDDL.Items.Add(Tools.CreateEnumListItem("Show Done only", E_DiscStatus.Done));
//
//            ListItem item = m_statusDDL.Items.FindByValue(m_statusFilter.ToString());
//            if (null != item) item.Selected = true;

            this.PageID = "TaskList";
            this.PageTitle = "Task List";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CLoanTaskPDF);

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

        protected HtmlControl DisplayDiscStatus(int status) 
        {
            string css = GetDiscStatusCss( (E_DiscStatus) status );
            string desc = GetDiscStatusDesc( (E_DiscStatus) status);
            return TaskList.CreateSpan(css, desc);
        }
        protected HtmlControl DisplayDiscPriority(int discPriority )
        {
            string css = GetDiscPriorityCss( (E_DiscPriority) discPriority );
            string priorityDesc = GetDiscPriorityDesc( (E_DiscPriority) discPriority );
            return TaskList.CreateSpan(css, priorityDesc);
        }
        protected HtmlControl DisplayDiscSubject(string subject, bool isRead ) 
        {
            string css = GetDiscSubjectCss(isRead);
            return TaskList.CreateSpan(css, subject);
        }
        protected HtmlControl DisplayDiscDueDate( object dueDate )
        {
            if( dueDate is DateTime )
            {
                string css = GetDiscDueDateCss( (DateTime) dueDate );
                return TaskList.CreateSpan(css, ((DateTime) dueDate).ToString());
            }

            return null;
        }

        /// <summary>
        /// Creates an html span.
        /// </summary>
        /// <param name="className">The span's classes.</param>
        /// <param name="text">The span's string content.</param>
        /// <returns></returns>
        private static HtmlGenericControl CreateSpan(string className, string text)
        {
            var span = new HtmlGenericControl("span");
            span.InnerText = text;
            span.Attributes.Add("class", className);
            return span;
        }

        static private string GetDiscStatusCss( E_DiscStatus discStatus )
        {
            switch( discStatus )
            {
                case E_DiscStatus.Active:		return "DiscStatusActive";
                case E_DiscStatus.Cancelled:	return "DiscStatusCancelled";
                case E_DiscStatus.Done:			return "DiscStatusDone";
                case E_DiscStatus.Suspended:	return "DiscStatusSuspended";
                default:
                    return "";
            }
        }
        static public string GetDiscStatusDesc( E_DiscStatus discStatus )
        {
            switch( discStatus )
            {
                case E_DiscStatus.Active:		return "Active";
                case E_DiscStatus.Cancelled:	return "Cancelled";
                case E_DiscStatus.Done:			return "Done";
                case E_DiscStatus.Suspended:	return "Suspended";
                default:
                    return "???";
            }
        }		
        static private string GetDiscPriorityCss( E_DiscPriority discPriority )
        {
            switch( discPriority )
            {
                case E_DiscPriority.High:		return "DiscPriorityHigh";
                case E_DiscPriority.Low:		return "DiscPriorityLow";
                case E_DiscPriority.Normal:		return "DiscPriorityNormal";
                default:
                    return "";
            }
        }
        static public string GetDiscPriorityDesc( E_DiscPriority discPriority )
        {
            switch( discPriority )
            {
                case E_DiscPriority.High:		return "High";
                case E_DiscPriority.Low:		return "Low";
                case E_DiscPriority.Normal:		return "Normal";
                default:
                    return "";
            }
        }
        static private string GetDiscSubjectCss( bool isRead )
        {
            return isRead ? "DiscSubjectRead" : "DiscSubjectUnread";
        }
        static private string GetDiscDueDateCss( DateTime discDueDate )
        {
            if( discDueDate.CompareTo( DateTime.Now ) < 0 )
                return "DiscDue";
            else
                return "DiscDueNot";
        }
        protected HtmlControl DisplayTrackNotifIsRead(bool TrackNotifIsRead, bool NotifIsValid )
        {
            return TaskList.CreateSpan(GetTrackNotifIsReadCss(TrackNotifIsRead, NotifIsValid), TrackNotifIsRead ? "Yes" : "No");
        }
        static public string GetTrackNotifIsReadCss( bool isRead, bool isNotifValid )
        {
            if( isRead )
                return isNotifValid ? "DiscTrackNotifIsReadYesNotifValidYes" : "DiscTrackNotifIsReadYesNotifValidNo";
            else
                return isNotifValid ? "DiscTrackNotifIsReadNoNotifValidYes" : "DiscTrackNotifIsReadNoNotifValidNo";
        }
        protected HtmlControl GenerateDiscJoinLink(object discLogId, object notifId, object notifIsValid  )
        {
            if( DBNull.Value != notifIsValid )
            {
                if ((bool)notifIsValid)
                    return TaskList.CreateLink("editReminder", "edit", notifId.ToString());
                else
                    return TaskList.CreateLink("activateNotif", "rejoin", notifId.ToString());
            }
            else
                return TaskList.CreateLink("joinDiscussion", "join", notifId.ToString());
        }

        /// <summary>
        /// Creates a link.
        /// </summary>
        /// <param name="onClickMethodName">The name of the method to call on click.</param>
        /// <param name="text">The displayed by the link.</param>
        /// <param name="data">The data that will be passed in as the data-value attribute.  Intended for use in the onclick methods.</param>
        /// <returns></returns>
       private static HtmlControl CreateLink(string onClickMethodName, string text, string data)
        {
            var link = new HtmlAnchor();
            link.InnerText = text;
            link.Attributes.Add("onClick", onClickMethodName + "(event)");
            link.Attributes.Add("data-value", data);
            return link;
        }
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
    }
}
