﻿#region Generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Migration;
    using LendersOffice.Security;
    using LendersOfficeApp.LOAdmin.TaskBackendUtilities;

    /// <summary>
    /// Data Layer Migrations service page.
    /// </summary>
    public partial class DataLayerMigrationService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Performs service called from page.
        /// </summary>
        /// <param name="methodName">The method to run.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "ApplyMigrations":
                    this.ApplyMigrations();
                    break;
                case "TargetVersionChanged":
                    this.TargetVersionChanged();
                    break;
                case "RollbackVersion":
                    this.RollbackVersion();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Method <" + methodName + "> is not supported.");
            }
        }

        /// <summary>
        /// Rollbacks the loan version number to whatever was specified on the page.
        /// </summary>
        private void RollbackVersion()
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            bool displayRollback = principal.HasPermission(Permission.CanModifyLoanPrograms);

            if (!displayRollback)
            {
                throw new AccessDenied();
            }

            Guid loanId = this.GetGuid("LoanId");
            int targetVersionAsInt = this.GetInt("targetRollbackVersion");

            if (!Enum.IsDefined(typeof(LoanVersionT), targetVersionAsInt))
            {
                SetResult("IsSuccessful", false);
                SetResult("ErrorMessage", "Invalid version. Unable to rollback.");
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(DataLayerMigrationService));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (targetVersionAsInt >= (int)dataLoan.sLoanVersionT)
            {
                SetResult("IsSuccessful", false);
                SetResult("ErrorMessage", "Invalid version. Version must be below the current loan version.");
                return;
            }

            dataLoan.sLoanVersionT = (LoanVersionT)targetVersionAsInt;
            dataLoan.Save();

            LoanDataMigrationsRunner runner = new LoanDataMigrationsRunner(principal, loanId, isSystemBatchMigration: false);
            LoanDataMigrationViewModel model = runner.RunMigrationsToDisplay();
            FieldEnumerator.PopulateViewModelWithPages(model);

            model.DisplayRollback = displayRollback;

            SetResult("IsSuccessful", true);
            SetResult("MigrationPageModel", SerializationHelper.JsonNetSerialize(model));
        }

        /// <summary>
        /// Runs the migration up to the target version. This will actually save the changes.
        /// </summary>
        private void ApplyMigrations()
        {
            Guid loanId = this.GetGuid("LoanId");
            LoanVersionT targetVersion = (LoanVersionT)this.GetInt("TargetVersion");

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            LoanVersionT? failedVersion;
            LoanDataMigrationsRunner runner = new LoanDataMigrationsRunner(principal, loanId, isSystemBatchMigration: false);
            bool successful = runner.ApplyMigrations(targetVersion, shouldSaveAtEnd: true, canUpdateToMax: false, failedMigrationNumber: out failedVersion);
            LoanDataMigrationViewModel model = null;
            if (successful)
            {
                model = runner.RunMigrationsToDisplay();
            }
            else
            {
                model = runner.RunMigrationsToDisplay(targetVersion);
            }

            FieldEnumerator.PopulateViewModelWithPages(model);

            model.DisplayRollback = principal.HasPermission(Permission.CanModifyLoanPrograms);

            SetResult("IsSuccessful", successful);
            SetResult("MigrationPageModel", SerializationHelper.JsonNetSerialize(model));
        }

        /// <summary>
        /// Updates the page when the target migration version is changed. 
        /// </summary>
        /// <remarks>
        /// The page technically doesn't need this to work. Since the page is loaded up with the max number of migrations ran, I could have just hidden certain rows when the DDL is changed.
        /// However, that prevents the user from getting the most up-to-date info whenever they change the DDL. It is entirely possible that the loan was updated after load but before the target 
        /// version was selected. So this should take care of that. Also, refreshing all the time will hopefully reduce the number of times that a user tries to apply migrations but it stopped 
        /// because loan data has changed, causing some migration to fail. 
        /// </remarks>
        private void TargetVersionChanged()
        {
            Guid loanId = this.GetGuid("LoanId");
            LoanVersionT targetVersion = (LoanVersionT)this.GetInt("TargetVersion");

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            LoanDataMigrationsRunner runner = new LoanDataMigrationsRunner(principal, loanId, isSystemBatchMigration: false);
            LoanDataMigrationViewModel model = runner.RunMigrationsToDisplay(targetVersion);
            FieldEnumerator.PopulateViewModelWithPages(model);

            model.DisplayRollback = principal.HasPermission(Permission.CanModifyLoanPrograms);

            SetResult("MigrationPageModel", SerializationHelper.JsonNetSerialize(model));
        }
    }
}