﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.ObjLib.Task;

namespace LendersOfficeApp.newlos.Tasks
{
    public partial class BatchTaskErrorSummary : LendersOffice.Common.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            string cacheId = RequestHelper.GetSafeQueryString("id");

            if (string.IsNullOrEmpty(cacheId))
            {
                throw CBaseException.GenericException("Loaded BatchTaskErrorSummary without cache id.");
            }

            BatchTaskFailure tf = BatchTaskFailure.LoadFromCache(cacheId);

            ErrorList.DataSource = tf.Failures;
            ErrorList.DataBind();

            switch (tf.TaskSaveFailureOperation)
            {
                case E_TaskSaveFailureOperation.SAVE_CONDITIONS:
                    Message.Text = "Changes to the following conditions were not saved:";
                    Title = "Condition Save Failure";
                    break;
                case E_TaskSaveFailureOperation.SAVE_TASKLIST:
                    Message.Text = "The selected operation failed for the following tasks:";
                    Title = "Task List Save Failure";
                    break;
                default:
                    throw new UnhandledEnumException(tf.TaskSaveFailureOperation);
            }
        }
    }
}
