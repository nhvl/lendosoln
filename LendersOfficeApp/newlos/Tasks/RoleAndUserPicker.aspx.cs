﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using System.Collections;
using DataAccess;
using System.Data;
using LendersOffice.Security;
using System.Threading;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;
using LendersOffice.Admin;
using LendersOffice.ObjLib.Task;
using System.Text;
using LendersOffice.ConfigSystem.Operations;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOfficeApp.newlos.Tasks
{
    public partial class RoleAndUserPicker : BasePage
    {
        protected bool IsTemplate
        {
            get { return RequestHelper.GetBool("IsTemplate"); }
        }

        protected Guid LoanID
        {
            get { return RequestHelper.LoanID; }
        }

        protected Guid BrokerID
        {
            get { return RequestHelper.GetGuid("brokerid"); }
        }

        protected int PermissionLevelID
        {
            get { return RequestHelper.GetInt("PermLevel", -1); }
        }

        protected bool SkipSecurityCheck
        {
            get { return RequestHelper.GetBool("IsBatch"); }
        }
        
        protected bool IsPipelineQuery
        {
            get { return RequestHelper.GetBool("IsPipeline"); }
        }

        protected bool IsOwnerAssignment
        {
            get { return RequestHelper.GetBool("Owner"); }
        }

        protected bool NoPML
        {
            get { return RequestHelper.GetBool("NoPML"); }
        }

        private bool HideConditionsWarning => RequestHelper.GetBool("hideConditionsWarning");

        protected string NameFilter
        {
            get
            {
                string text = m_searchBox.Text;
                if (text.IndexOf(' ') < 0)
                {
                    return text;
                }
                else
                {
                    return text.Substring(0, text.IndexOf(' ')).TrimWhitespaceAndBOM();
                }
            }
        }

        protected string LastFilter
        {
            get
            {
                string text = m_searchBox.Text;
                if (text.IndexOf(' ') < 0)
                {
                    return String.Empty;
                }
                else
                {
                    return text.Substring(text.IndexOf(' ')).TrimWhitespaceAndBOM();
                }
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            RegisterJsScript("jquery.tablesorter.min.js");
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable m_dataTable = new DataTable();
            m_dataTable.Columns.Add("UserId", typeof(string));
            m_dataTable.Columns.Add("FullName", typeof(string));
            m_dataTable.Columns.Add("Type", typeof(string));
            m_dataTable.Columns.Add("Roles", typeof(string));
            m_HiddenTask.Visible = NoPML && !this.HideConditionsWarning;

            if (Page.IsPostBack)
            {               

                if (String.IsNullOrEmpty(m_searchBox.Text))
                {
                    return;
                }                

                AbstractUserPrincipal user = Thread.CurrentPrincipal as AbstractUserPrincipal;
                Guid brokerId;
                if (user.BrokerId != Guid.Empty)
                {
                    brokerId = user.BrokerId;
                }
                else
                {
                    brokerId = BrokerID;
                }
                CPageData loanData = null;
                LoanValueEvaluator valueEvaluator = null;
                List<SqlParameter> parameters = new List<SqlParameter>();
                if (!IsPipelineQuery)
                {
                    valueEvaluator = new LoanValueEvaluator(brokerId, LoanID, WorkflowOperations.ReadLoanOrTemplate, WorkflowOperations.WriteLoanOrTemplate);
                    loanData = CPageData.CreateUsingSmartDependency(LoanID, typeof(RoleAndUserPicker));
                    loanData.InitLoad();
                    parameters.Add(new SqlParameter("@BranchId", loanData.sBranchId));
                    parameters.Add(new SqlParameter("@LoanId", loanData.sLId));
                }

                parameters.Add(new SqlParameter("@BrokerId", brokerId));
                parameters.Add(new SqlParameter("@NameFilter", NameFilter));
                parameters.Add(new SqlParameter("@NoPML", NoPML));
                parameters.Add(new SqlParameter("@IsPipelineQuery", IsPipelineQuery));
                if (!string.IsNullOrEmpty(LastFilter))
                {
                    parameters.Add(new SqlParameter("@LastFilter", LastFilter));
                }

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_GetUsersForAssignment", parameters))
                {
                    while (reader.Read())
                    {
                        Guid employeeId = new Guid(reader["EmployeeId"].ToString());
                        Guid userId = new Guid(reader["UserId"].ToString());
                        string userName = reader["FullName"].ToString();
                        string type = reader["Type"].ToString();

                        Guid branchId = (Guid)reader["BranchId"];
                        Guid pmlBrokerId = (Guid)reader["PmlBrokerId"];
                        E_PmlLoanLevelAccess pmlLevelAccess = (E_PmlLoanLevelAccess)reader["PmlLevelAccess"];
                        string permissions = (string)reader["Permissions"]; 
                        E_ApplicationT applicationType = type == "B" ? E_ApplicationT.LendersOffice : E_ApplicationT.PriceMyLoan;

                        ExecutingEnginePrincipal principal = ExecutingEnginePrincipal.CreateFrom(brokerId, branchId, pmlBrokerId, employeeId, userId, pmlLevelAccess, applicationType, permissions, user.Type);
                        //AbstractUserPrincipal principal = PrincipalFactory.RetrievePrincipalForUser(userId, type) as AbstractUserPrincipal;

                        if (!SkipSecurityCheck)
                        {
                            if (principal == null)
                            {
                                continue;
                            }

                            // Workflow config check

                            valueEvaluator.SetEvaluatingPrincipal(principal);
                            if (!LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator))
                            {
                                continue;
                            }

                            // Permission level check
                            if (PermissionLevelID != -1)
                            {
                                PermissionLevel selectedPermission = PermissionLevel.Retrieve(user.BrokerId, PermissionLevelID);
                                List<Guid> UserGroups = new List<Guid>();
                                foreach (Group ugroup in GroupDB.ListInclusiveGroupForEmployee(principal.BrokerId, principal.EmployeeId))
                                {
                                    UserGroups.Add(ugroup.Id);
                                }
                                List<E_RoleT> Roles = new List<E_RoleT>();
                                foreach (E_RoleT role in principal.GetRoles())
                                {
                                    Roles.Add(role);
                                }
                                if (!selectedPermission.IsRoleOrGroupInLevel(E_PermissionLevel.WorkOn, Roles, UserGroups))
                                {
                                    continue;
                                }
                            }
                        }

                        DataRow row = m_dataTable.NewRow();
                        row["UserId"] = userId;
                        row["FullName"] = userName;
                        row["Type"] = String.Compare(type, "B", true) == 0 ? "Employee" : "PML";

                        if (!IsPipelineQuery)
                        {
                            string roles = "";
                            if (IsTemplate)
                            {
                                StringBuilder roleList = new StringBuilder();
                                foreach (var role in principal.GetRoles())
                                {
                                    roleList.Append(Role.GetRoleDescription(role));
                                    roleList.Append(",");
                                }
                                roles = roleList.ToString();
                            }
                            else
                            {
                                // Appending loan assignments to users who passed the checks            
                                if (employeeId == loanData.sEmployeeProcessorId)
                                {
                                    roles += "Processor,";
                                }
                                if (employeeId == loanData.sEmployeeBrokerProcessorId)
                                {
                                    roles += "Processor (External),";
                                }
                                if (employeeId == loanData.sEmployeeCallCenterAgentId)
                                {
                                    roles += "Call Center Agent,";
                                }
                                if (employeeId == loanData.sEmployeeCloserId)
                                {
                                    roles += "Closer,";
                                }
                                if (employeeId == loanData.sEmployeeLenderAccExecId)
                                {
                                    roles += "Lender Account Executive,";
                                }
                                if (employeeId == loanData.sEmployeeLoanOpenerId)
                                {
                                    roles += "Loan Opener,";
                                }
                                if (employeeId == loanData.sEmployeeLoanRepId)
                                {
                                    roles += "Loan Officer,";
                                }
                                if (employeeId == loanData.sEmployeeLockDeskId)
                                {
                                    roles += "Lock Desk,";
                                }
                                if (employeeId == loanData.sEmployeeManagerId)
                                {
                                    roles += "Manager,";
                                }
                                if (employeeId == loanData.sEmployeeRealEstateAgentId)
                                {
                                    roles += "Real Estate Agent,";
                                }
                                if (employeeId == loanData.sEmployeeUnderwriterId)
                                {
                                    roles += "Underwriter,";
                                }
                                if (employeeId == loanData.sEmployeeFunderId)
                                {
                                    roles += "Funder,";
                                }
                                if (employeeId == loanData.sEmployeeShipperId)
                                {
                                    roles += "Shipper,";
                                }
                                if (employeeId == loanData.sEmployeePostCloserId)
                                {
                                    roles += "Post-Closer,";
                                }
                                if (employeeId == loanData.sEmployeeInsuringId)
                                {
                                    roles += "Insuring,";
                                }
                                if (employeeId == loanData.sEmployeeCollateralAgentId)
                                {
                                    roles += "Collateral Agent,";
                                }
                                if (employeeId == loanData.sEmployeeDocDrawerId)
                                {
                                    roles += "Doc Drawer,";
                                }
                                if (employeeId == loanData.sEmployeeCreditAuditorId)
                                {
                                    roles += "Credit Auditor,";
                                }
                                if (employeeId == loanData.sEmployeeDisclosureDeskId)
                                {
                                    roles += "Disclosure Desk,";
                                }
                                if (employeeId == loanData.sEmployeeJuniorProcessorId)
                                {
                                    roles += "Junior Processor,";
                                }
                                if (employeeId == loanData.sEmployeeJuniorUnderwriterId)
                                {
                                    roles += "Junior Underwriter,";
                                }
                                if (employeeId == loanData.sEmployeeLegalAuditorId)
                                {
                                    roles += "Legal Auditor,";
                                }
                                if (employeeId == loanData.sEmployeeLoanOfficerAssistantId)
                                {
                                    roles += "Loan Officer Assistant,";
                                }
                                if (employeeId == loanData.sEmployeePurchaserId)
                                {
                                    roles += "Purchaser,";
                                }
                                if (employeeId == loanData.sEmployeeQCComplianceId)
                                {
                                    roles += "QC Compliance,";
                                }
                                if (employeeId == loanData.sEmployeeSecondaryId)
                                {
                                    roles += "Secondary,";
                                }
                                if (employeeId == loanData.sEmployeeServicingId)
                                {
                                    roles += "Servicing,";
                                }
                                if (employeeId == loanData.sEmployeeExternalSecondaryId)
                                {
                                    roles += "Secondary (External),";
                                }
                                if (employeeId == loanData.sEmployeeExternalPostCloserId)
                                {
                                    roles += "Post-Closer (External),";
                                }
                            }

                            if (roles.Length > 0)
                            {
                                roles = Tools.SortCommaDelimitedString(roles.Substring(0, roles.Length - 1));
                            }
                            row["Roles"] = roles;
                        }

                        m_dataTable.Rows.Add(row);
                    }
                }
            }
            else
            {
                if (IsTemplate)
                {
                    m_pageHeader.Text = string.Format("Set Task {0}", IsOwnerAssignment ? "Ownership" : "Assignment");
                    m_searchLiteral.Text = "Or select a user:";
                    m_dgUsers.Columns[2].HeaderText = "<a href='#'>User Role</a>";
                }
                else
                {
                    m_pageHeader.Text = "Assign a User";
                    RolePicker.Visible = false;
                    m_searchLiteral.Text = "Search for:";
                    m_dgUsers.Columns[2].HeaderText = "<a href='#'>Assigned Role</a>";
                }

                m_roleList.DataSource = Role.AllRoles.Where(r => r.RoleT != E_RoleT.Consumer).OrderBy(p => p.ModifiableDesc);
                m_roleList.DataTextField = "ModifiableDesc";
                m_roleList.DataValueField = "Id";
                m_roleList.DataBind();
                m_roleList.Items.Insert(0, new ListItem("<-- select a role -->", ""));

                if (!IsPipelineQuery)
                {
                    Guid brokerId = Guid.Empty;
                    DbConnectionInfo.GetConnectionInfoByLoanId(LoanID, out brokerId);

                    LoanAssignmentContactTable lTable = new LoanAssignmentContactTable(brokerId, LoanID);
                    foreach (EmployeeLoanAssignment lA in lTable.Items)
                    {
                        bool userFound = false;
                        
                        for (int i = 0; i < m_dataTable.Rows.Count; i++)
                        {
                            DataRow dr = m_dataTable.Rows[i];
                            if (new Guid(dr["UserId"].ToString()) == lA.UserId)
                            {
                                userFound = true;
                                if (!IsTemplate)
                                {
                                    dr["Roles"] = dr["Roles"].ToString() + "," + lA.RoleModifiableDesc;
                                }
                                break;
                            }
                        }

                        if (!userFound)
                        {
                            AbstractUserPrincipal principal = PrincipalFactory.RetrievePrincipalForUser(brokerId, lA.UserId, lA.IsPmlUser ? "P" : "B") as AbstractUserPrincipal;
                            if (principal == null)
                            {
                                continue;
                            }

                            if (NoPML && lA.IsPmlUser)
                            {
                                continue;
                            }

                            DataRow row = m_dataTable.NewRow();
                            row["UserId"] = lA.UserId;
                            row["FullName"] = lA.FullName;
                            row["Type"] = lA.IsPmlUser ? "PML" : "Employee";
                            if (IsTemplate)
                            {
                                StringBuilder roleList = new StringBuilder();
                                foreach (var role in principal.GetRoles())
                                {
                                    roleList.Append(Role.GetRoleDescription(role));
                                    roleList.Append(",");
                                }
                                string roles = roleList.ToString();
                                row["Roles"] = roles.Substring(0, roles.Length - 1);
                            }
                            else
                            {
                                row["Roles"] = lA.RoleModifiableDesc;
                            }
                            m_dataTable.Rows.Add(row);
                        }
                    }

                    foreach (DataRow dr in m_dataTable.Rows)
                    {
                        dr["Roles"] = Tools.SortCommaDelimitedString(dr["Roles"].ToString());
                    }
                }
            }

            m_dgUsers.DataSource = m_dataTable;
            m_dgUsers.DataBind();
            m_dgUsers.Columns[2].Visible = !IsPipelineQuery;
            m_userMessage.Visible = m_dgUsers.Rows.Count > 0 && !IsPipelineQuery;
            m_noUsersFound.Visible = m_dgUsers.Rows.Count < 1 && IsPostBack;

            // Use thead, tbody, and th so that tablesorter will work
            if (m_dgUsers.Rows.Count > 0)
            {
                m_dgUsers.UseAccessibleHeader = true;
                m_dgUsers.HeaderRow.TableSection = TableRowSection.TableHeader;
                m_dgUsers.FooterRow.TableSection = TableRowSection.TableFooter;
            }

        }
    }
}
