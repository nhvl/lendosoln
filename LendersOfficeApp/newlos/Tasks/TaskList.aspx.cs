﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using EDocs;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.ObjLib.DocumentConditionAssociation;
using LendersOffice.ObjLib.Task;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Tasks
{
    /// <summary>
    /// Tasks list page for the mid-2011 release
    /// </summary>
    public partial class TaskList : BaseLoanPage
    {
        // Be sure to update these whenever you add/remove a column
        // They determine which columns get hidden when editing a template loan
        enum E_OpenResolvedTasksColumns
        {
            Checkbox, Task, CondCategoryId, Subject, Status, DueDate, FollowupDate,
            LastUpdated, AssignedTo, Owner
        }
        enum E_ClosedTasksColumns
        {
            Task, Subject, LastUpdated, Owner
        }
        public override bool IsReadOnly
        {
            get
            {
                return false; // 6/15/2011 dd - OPM 67578
            }
        }

        private string AcceptedFileExtensions
        {
            get
            {
                if (this.Broker.AllowExcelFilesInEDocs)
                {
                    return ".xml, .pdf, .xls, .xlsx";
                }

                return ".xml, .pdf";
            }
        }

        private int FileUploadLimit => 12;

        private readonly string OPENRESOLVED_GRIDVIEWNAME = "OpenResolved";
        private readonly string OPENRESOLVED_DEFAULTSORT = "TaskDueDateComparison ASC, TaskFollowupDate ASC, TaskSubject ASC";
        private readonly string OPENRESOLVED_TASKFILTER = "TaskStatus <> " + (int)E_TaskStatus.Closed;
        private readonly string CLOSED_GRIDVIEWNAME = "Closed";
        private readonly string CLOSED_DEFAULTSORT = "TaskLastModifiedDate DESC, TaskSubject ASC";
        private readonly string CLOSED_TASKFILTER = "TaskStatus = " + (int)E_TaskStatus.Closed;
        private const string CONDFILTER_ALL = "ANYONE";
        private const string CONDFILTER_COND = "CONDITIONS";
        private const string CONDFILTER_NONCOND = "NONCONDITIONS";
        private const string FILTER_ANYONE = "ANYONE";
        private const string FILTER_ME = "ME";
        private const string FILTER_ROLE = "ROLE";
        private const string FILTER_USERID = "USERID";
        private const string FILTER_NOBODY = "NOBODY";
        private const int FILTER_DROPDOWN_LENGTH = 40;

        /// <summary>
        /// One cookie is stored per loan for each of the dropdown filters
        /// </summary>
        private const string FilterCookie = "SelectedFilter";
        private const string ConditionFilterCookie = "SelectedCondFilter";

        protected bool IsTemplate;

        protected EmployeeDB CurrentEmployee
        {
            get
            {
                EmployeeDB currentUserEmployee = new EmployeeDB(EmployeeID, BrokerID);
                currentUserEmployee.Retrieve();
                return currentUserEmployee;
            }
        }

        protected DataTable m_data;

        protected Dictionary<Guid, Guid> m_roleToUserMap; // <ROLEID, USERID> 

        /// <summary>
        /// Gets the extra workflow operations to check.
        /// </summary>
        /// <returns>The extra workflow operations to check.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.UploadEDocs
            };
        }

        /// <summary>
        /// Gets the reasons for privileges that aren't allowed.
        /// </summary>
        /// <returns>The reasons for privileges that aren't allowed.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return this.GetExtraOpsToCheck();
        }

        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("DragDropUpload.js");
            this.RegisterJsScript("DocumentUploadHelper.js");
            this.RegisterJsScript("ConditionDocumentUploadHelper.js");

            this.RegisterCSS("DragDropUpload.css");

            RegisterService("edoc", "/newlos/ElectronicDocs/EDocsUtilitiesService.aspx");

            // Populate condition dropdown
            m_conditionFilter.Items.Add(new ListItem("all tasks", CONDFILTER_ALL));
            m_conditionFilter.Items.Add(new ListItem("conditions", CONDFILTER_COND));
            m_conditionFilter.Items.Add(new ListItem("non-conditions", CONDFILTER_NONCOND));

            // Create DataTable
            m_data = new DataTable();

            m_data.Columns.Add(new DataColumn("TaskId"));
            m_data.Columns.Add(new DataColumn("TaskIdLength", typeof(int)));
            m_data.Columns.Add(new DataColumn("TaskRowVersion"));
            m_data.Columns.Add(new DataColumn("CondCategoryId"));
            m_data.Columns.Add(new DataColumn("TaskIsCondition"));
            m_data.Columns.Add(new DataColumn("TaskSubject"));
            m_data.Columns.Add(new DataColumn("TaskStatus", typeof(E_TaskStatus)));
            m_data.Columns.Add(new DataColumn("TaskDueDate", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskDueDateCalcDisplay"));
            m_data.Columns.Add(new DataColumn("TaskDueDateCalc"));
            m_data.Columns.Add(new DataColumn("TaskFollowUpDate", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskLastModifiedDate", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskAssignedUserId", typeof(Guid)));
            m_data.Columns.Add(new DataColumn("TaskToBeAssignedRoleId", typeof(Guid)));
            m_data.Columns.Add(new DataColumn("AssignedUserFullName"));
            m_data.Columns.Add(new DataColumn("OwnerFullName"));
            m_data.Columns.Add(new DataColumn("TaskDueDateComparison", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskFollowUpDateComparison", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("TaskLastModifiedDateComparison", typeof(DateTime)));
            m_data.Columns.Add(new DataColumn("CalcTaskStatus"));
            m_data.Columns.Add(new DataColumn("CalcTaskDueDateWarningCss"));
            m_data.Columns.Add(new DataColumn("CalcTaskFollowupDateWarningCss"));
            m_data.Columns.Add(new DataColumn("TaskHidden"));
            m_data.Columns.Add(new DataColumn("WarnOnResolve"));
            m_data.Columns.Add(new DataColumn("ResolutionBlockTriggerName"));
            m_data.Columns.Add(new DataColumn("ResolutionDateSetterFieldId"));
            m_data.Columns.Add(new DataColumn("CondRequiredDocType"));
            m_data.Columns.Add(new DataColumn("IsDocRequestSatisfied"));
            m_data.Columns.Add(new DataColumn("AssociatedDocs"));

            // Is it a template?
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TaskList));
            dataLoan.InitLoad();
            IsTemplate = dataLoan.IsTemplate;
            m_isTemplate.Value = IsTemplate.ToString();

            if (IsTemplate)
            {
                m_title.Text = "Tasks Template";
                m_openResolvedTasksTitle.Visible = false;

                m_openResolvedTasksGV.Columns[(int) E_OpenResolvedTasksColumns.Status].Visible = false;
                m_openResolvedTasksGV.Columns[(int) E_OpenResolvedTasksColumns.FollowupDate].Visible = false;

                m_closedTasksDiv.Visible = false;

            }

            this.PageID = "NewTaskList";
            this.PageTitle = "New Task List";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CTaskListPDF);

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

        protected void PagePostBack(object sender, EventArgs e)
        {

        }

        #region Page Load
        protected void PageLoad(object sender, EventArgs e)
        {
            //DateTime starttime = DateTime.Now;
            if (Page.IsPostBack) // Only every subsequent load of this page
            {
                PagePostBack(sender, e);
            }
            else // Only the first load of this page
            {
                try
                {
                    // Matches the selected value to the cookie value
                    m_assignmentFilter.SelectedValue = Request.Cookies[FilterCookie].Value;
                }
                catch (IndexOutOfRangeException) // This value is not in the list : possibly the user got unassigned
                {
                    m_assignmentFilter.SelectedValue = FILTER_ANYONE; // So just set it to ANYONE
                }
                catch (NullReferenceException) // The cookie doesn't exist
                {
                    m_assignmentFilter.SelectedValue = FILTER_ANYONE;
                }

                try
                {
                    // Matches the selected value to the cookie value
                    m_conditionFilter.SelectedValue = Request.Cookies[ConditionFilterCookie].Value;
                }
                catch (IndexOutOfRangeException) // This value is not in the list : how strange! go back to the default
                {
                    m_conditionFilter.SelectedValue = CONDFILTER_ALL;
                }
                catch (NullReferenceException) // The cookie doesn't exist
                {
                    m_conditionFilter.SelectedValue = CONDFILTER_ALL;
                }


            }
            // Every single load
            if (m_conditionFilter.SelectedValue == CONDFILTER_NONCOND)
            {
                m_openResolvedTasksGV.CssClass = "hide-supporting-docs";
            }

            // Populate gridviews and set user owned tasks.
            var ds = GetPopulatedDataSet();

            m_openResolvedTasksGV_Load(ds);
            m_closedTasksGV_Load(ds);

            this.RegisterJsGlobalVariables("CanUploadEdocs", this.UserHasWorkflowPrivilege(WorkflowOperations.UploadEDocs));
            this.RegisterJsGlobalVariables("UploadEdocsFailureMessage", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.UploadEDocs));

            //Tools.LogInfo("Total load time: " + (DateTime.Now - starttime).TotalMilliseconds + "ms");
        }

        protected void m_openResolvedTasksGV_Load(DataSet ds)
        {
            // For refactoring considerations, here's an alternate design:
            // Create a subclass of GridView
            // Create custom fields instead of using templatefields

            // And here's the design we went with

            DataView openResolvedTasksView = new DataView(ds.Tables[0]);
            // Filtering
            openResolvedTasksView.RowFilter = PopulateRowFilter(OPENRESOLVED_TASKFILTER);
            // Sorting
            try
            {
                openResolvedTasksView.Sort = PopulateSortUpdateViewState(OPENRESOLVED_GRIDVIEWNAME, OPENRESOLVED_DEFAULTSORT);
            }
            catch (IndexOutOfRangeException)
            {
                openResolvedTasksView.Sort = OPENRESOLVED_DEFAULTSORT;
            }

            GV_Load(
                m_openResolvedTasksGV,
                OPENRESOLVED_GRIDVIEWNAME,
                openResolvedTasksView
            );
        }

        protected void m_closedTasksGV_Load(DataSet ds)
        {
            DataView closedTasksView = new DataView(ds.Tables[0]);
            // Filtering
            closedTasksView.RowFilter = PopulateRowFilter(CLOSED_TASKFILTER);
            // Sorting
            try
            {
                closedTasksView.Sort = PopulateSortUpdateViewState(CLOSED_GRIDVIEWNAME, CLOSED_DEFAULTSORT);
            }
            catch (IndexOutOfRangeException)
            {
                closedTasksView.Sort = CLOSED_DEFAULTSORT;
            }

            GV_Load(
                m_closedTasksGV,
                CLOSED_GRIDVIEWNAME,
                closedTasksView
            );
        }

        private string PopulateRowFilter(string defaultFilter)
        {
            try
            {
                return BuildRowFilter(defaultFilter, m_assignmentFilter.SelectedValue, m_conditionFilter.SelectedValue);
            }
            catch (FormatException) // It's not a Guid
            {
                return defaultFilter;
            }
            catch (IndexOutOfRangeException)
            {
                return defaultFilter;
            }
            catch (NullReferenceException)
            {
                return defaultFilter;
            }
        }

        private string PopulateSortUpdateViewState(string gvName, string defaultSort)
        {
            string sort;
            try
            {
                sort = string.Format(
                    "{0} {1}",
                    Request.Cookies[gvName + "SortExpression"].Value,
                    Request.Cookies[gvName + "SortDirection"].Value
                );

                ViewState[gvName + "SortExpression"] = Request.Cookies[gvName + "SortExpression"].Value;
                ViewState[gvName + "SortDirection"] = Request.Cookies[gvName + "SortDirection"].Value;
            }
            catch (NullReferenceException) // The cookies DO NOT EXIST! WHERE DID THE COOKIES GO?! Were they ever here in the first place?
            {
                sort = defaultSort;
            }
            return sort;
        }

        private void GV_Load(GridView gv, string gvName, DataView dv)
        {

            gv.DataSource = dv;
            gv.DataBind();

            // Can only do this after DataBind
            if (Request.Cookies[gvName + "SortedColumn"] != null && Request.Cookies[gvName + "SortDirection"] != null)
            {
                AddArrowToColumn(
                    gv,
                    Request.Cookies[gvName + "SortedColumn"].Value,
                    Request.Cookies[gvName + "SortDirection"].Value
                );
            }

        }
        #endregion

        protected void PopulateAssignmentDropdown(object sender, EventArgs e)
        {
            m_assignmentFilter.Items.Add(new ListItem("Anyone", FILTER_ANYONE));
            m_assignmentFilter.Items.Add(new ListItem("Me", FILTER_ME));

            var roles = Task.GetRoleUsersForLoan(BrokerID, LoanID);
            m_roleToUserMap = new Dictionary<Guid, Guid>(roles.Count);
            foreach (var role in roles) // Item1: Name, Item2: RoleId, Item3: UserId
            {
                string displayText = role.Item1.Length > FILTER_DROPDOWN_LENGTH ? role.Item1.Substring(0, FILTER_DROPDOWN_LENGTH) : role.Item1;
                string listValue = string.Format("{0}:{1}", FILTER_ROLE, role.Item2.ToString()); // Store the roleID
                ListItem li = new ListItem(displayText, listValue);
                li.Attributes.Add("title", role.Item1);
                m_assignmentFilter.Items.Add(li);
                m_roleToUserMap.Add(role.Item2, role.Item3);
            }

            var otherUsers = Task.GetUnassignedUsersWithTasksForLoan(BrokerID, LoanID);
            foreach (var user in otherUsers) // Item1: Name, Item2: UserId
            {
                string displayText = user.Item1.Length > FILTER_DROPDOWN_LENGTH ? user.Item1.Substring(0, FILTER_DROPDOWN_LENGTH) : user.Item1;
                string listValue = string.Format("{0}:{1}", FILTER_USERID, user.Item2.ToString()); // Store the userID
                ListItem li = new ListItem(displayText, listValue);
                li.Attributes.Add("title", user.Item1);
                m_assignmentFilter.Items.Add(li);
            }
            m_assignmentFilter.Items.Add(new ListItem("Nobody", FILTER_NOBODY));
        }

        private DataSet GetPopulatedDataSet()
        {
            //DateTime spstarttime = DateTime.Now;
            List<Task> tasks = Task.GetTasksByEmployeeAccess(BrokerID, LoanID, EmployeeID);
            var taskPermissionProcessor = new TaskPermissionProcessor(PrincipalFactory.CurrentPrincipal);
            if (!Task.CanUserViewHiddenInformation(BrokerUser))
            {
                tasks.RemoveAll(task => task.CondIsHidden);
            }

            var userDocRepo = EDocumentRepository.GetUserRepository();
            var listLoanDocs = userDocRepo.GetDocumentsByLoanId(this.LoanID);
            var documentConditionAssociations = DocumentConditionAssociation.GetAssociationsByLoanHeavy(this.BrokerID, this.LoanID, PrincipalFactory.CurrentPrincipal);
            StringBuilder userOwnedTaskIds = new StringBuilder();

            foreach (Task task in tasks)
            {
                var row = m_data.NewRow();

                if (task.TaskOwnerUserId == BrokerUser.UserId)
                {
                    userOwnedTaskIds.Append(String.Format("{0},", task.TaskId));
                }

                row["TaskId"] = task.TaskId;
                row["TaskIdLength"] = task.TaskId.Length;
                row["TaskRowVersion"] = task.TaskRowVersion;
                row["CondCategoryId"] = task.CondCategoryId_rep == String.Empty ? "" : task.CondCategoryId_rep;
                row["TaskIsCondition"] = task.TaskIsCondition;

                row["TaskSubject"] = task.TaskSubject;
                row["TaskStatus"] = task.TaskStatus;

                row["CondRequiredDocType"] = task.CondRequiredDocType_rep;
                row["IsDocRequestSatisfied"] = task.ConditionFulfilsAssociatedDocs();

                var roleIds = PrincipalFactory.CurrentPrincipal.GetRoles().Select(roleT => Role.Get(roleT).Id);

                row["AssociatedDocs"] = SerializationHelper.JsonNetSerialize<List<SimplifiedDocAssociation>>(
                    documentConditionAssociations.Where(association => association.TaskId == task.TaskId).Select(association =>
                    new SimplifiedDocAssociation
                    {
                        DocumentId = association.DocumentId,
                        DocumentName = association.DocumentFolderName + " : " + association.DocumentTypeName,
                        HasAccess = listLoanDocs.Any(doc => doc.DocumentId == association.DocumentId)
                    }
                ).ToList());

                // We use a hidden comparison column to sort by date and ensure that blank dates go last
                if (task.TaskDueDate == null)
                {
                    row["TaskDueDate"] = DBNull.Value;
                    row["TaskDueDateComparison"] = DateTime.MaxValue;
                    if (!string.IsNullOrEmpty(task.TaskDueDateCalcField))
                    {
                        row["TaskDueDateCalcDisplay"] = "inherit";
                        row["TaskDueDateCalc"] = task.TaskSingleDueDate;
                    }
                    else
                    {
                        row["TaskDueDateCalcDisplay"] = "none";
                        row["TaskDueDateCalc"] = "";
                    }
                }
                else
                {
                    row["TaskDueDate"] = task.TaskDueDate.Value.Date;
                    row["TaskDueDateComparison"] = task.TaskDueDate.Value.Date;
                    row["TaskDueDateCalcDisplay"] = "none";
                    row["TaskDueDateCalc"] = "";
                }

                if (task.TaskFollowUpDate == null)
                {
                    row["TaskFollowUpDate"] = DBNull.Value;
                    row["TaskFollowUpDateComparison"] = DateTime.MaxValue;
                }
                else
                {
                    row["TaskFollowUpDate"] = task.TaskFollowUpDate.Value.Date;
                    row["TaskFollowUpDateComparison"] = task.TaskFollowUpDate.Value.Date;
                }

                if (task.TaskLastModifiedDate == null)
                {
                    row["TaskLastModifiedDate"] = DBNull.Value;
                    row["TaskLastModifiedDateComparison"] = DateTime.MaxValue;
                }
                else
                {
                    row["TaskLastModifiedDate"] = task.TaskLastModifiedDate.Date;
                    row["TaskLastModifiedDateComparison"] = task.TaskLastModifiedDate.Date;
                }

                row["TaskAssignedUserId"] = task.TaskAssignedUserId;
                row["TaskToBeAssignedRoleId"] = task.TaskToBeAssignedRoleId ?? Guid.Empty; // null coalescing operator
                row["AssignedUserFullName"] = task.AssignedUserFullName;
                row["OwnerFullName"] = task.OwnerFullName;

                row["CalcTaskStatus"] = task.TaskStatus_rep; // Not exactly calculated, but we need to differentiate it from the int TaskStatus
                row["CalcTaskDueDateWarningCss"] = GetDateWarningCss(task.TaskDueDate);
                row["CalcTaskFollowupDateWarningCss"] = GetDateWarningCss(task.TaskFollowUpDate);

                row["TaskHidden"] = task.CondIsHidden;

                // We want to warn them when the user has Manage Permission
                // and they are resolving/closing a condition that does not
                // have the required doc type associated.
                row["WarnOnResolve"] = task.TaskIsCondition 
                    && task.CondRequiredDocTypeId.HasValue 
                    && taskPermissionProcessor.Resolve(task) == E_UserTaskPermissionLevel.Manage
                    && !task.ConditionFulfilsAssociatedDocs();

                row["ResolutionBlockTriggerName"] = task.ResolutionBlockTriggerName;
                row["ResolutionDateSetterFieldId"] = task.ResolutionDateSetterFieldId;

                m_data.Rows.Add(row);
            }
            var ds = new DataSet();
            ds.Tables.Add(m_data);

            //Tools.LogInfo("Row creation time: " + (DateTime.Now - rowcreatestarttime).TotalMilliseconds + "ms");
            m_userOwnedTaskIds.Value = userOwnedTaskIds.ToString().TrimEnd(new char[] { ',' });

            return ds;
        }

        #region Filtering
        protected void m_openResolvedTasksGV_SelectedFilterChanged(object sender, EventArgs e)
        {
            RequestHelper.StoreToCookie(FilterCookie, m_assignmentFilter.SelectedItem.Value, SmallDateTime.MaxValue);
        }

        protected void m_openResolvedTasksGV_SelectedCondFilterChanged(object sender, EventArgs e)
        {
            RequestHelper.StoreToCookie(ConditionFilterCookie, m_conditionFilter.SelectedItem.Value, SmallDateTime.MaxValue);
        }

        protected string BuildRowFilter(string defaultFilter, string selectedValue, string selectedCondValue)
        {
            List<string> filterList = new List<string>(4);
            if ((string.IsNullOrEmpty(selectedValue) || selectedValue == FILTER_ANYONE)
                && (string.IsNullOrEmpty(selectedCondValue) || selectedCondValue == CONDFILTER_ALL))
            {
                m_filterErrorMessage.Text = "";
                return defaultFilter; // Don't need to filter
            }

            if (!string.IsNullOrEmpty(defaultFilter))
            {
                filterList.Add(defaultFilter);
            }

            // determine assignment filter
            if (!string.IsNullOrEmpty(selectedValue) && selectedValue != FILTER_ANYONE)
            {
                string[] splitString = selectedValue.Split(':');
                string filterType = splitString[0];
                Guid filterId;
                try
                {
                    filterId = new Guid(splitString[splitString.Length - 1]); // The guid is the last item, if we have a role or user
                }
                catch (FormatException)
                {
                    filterId = Guid.Empty;
                }
                Guid selectedUserId;
                switch (filterType)
                {
                    case FILTER_ME:
                        selectedUserId = UserID;
                        m_filterErrorMessage.Text = "";
                        break;
                    case FILTER_NOBODY:
                        selectedUserId = Guid.Empty;
                        m_filterErrorMessage.Text = "";
                        break;
                    case FILTER_ROLE:
                        selectedUserId = m_roleToUserMap[filterId];
                        m_filterErrorMessage.Text = "";
                        break;
                    case FILTER_USERID:
                        selectedUserId = filterId;
                        m_filterErrorMessage.Text = "";
                        break;
                    default:
                        Tools.LogError("Task List: unhandled filter type!");
                        selectedUserId = Guid.Empty;
                        m_filterErrorMessage.Text = "Could not find user id! Please pick another filter.";
                        break;
                }
                filterList.Add("TaskAssignedUserId = '" + selectedUserId.ToString() + "'");
            }
            // determine condition filter
            if (!string.IsNullOrEmpty(selectedCondValue) && selectedCondValue != CONDFILTER_ALL)
            {
                string filterType = selectedCondValue;
                string selectedConditionStatus;
                switch (filterType)
                {
                    case CONDFILTER_COND:
                        selectedConditionStatus = "True";
                        break;
                    case CONDFILTER_NONCOND:
                        selectedConditionStatus = "False";
                        break;
                    default:
                        selectedConditionStatus = "False";
                        Tools.LogError("Task List: unhandled condition filter type!");
                        break;
                }
                filterList.Add("TaskIsCondition = '" + selectedConditionStatus + "'");
            }

            return string.Join(" AND ", filterList.ToArray());
        }
        #endregion

        #region Sorting
        protected void m_openResolvedTasksGV_Sorting(object sender, CommandEventArgs e)
        {
            SortOperation(sender, e, m_openResolvedTasksGV, "OpenResolved");
        }

        protected void m_closedTasksGV_Sorting(object sender, CommandEventArgs e)
        {
            SortOperation(sender, e, m_closedTasksGV, "Closed");
        }

        private void SortOperation(object sender, CommandEventArgs e, GridView gv, string gvName)
        {
            string[] commandArguments = ((string)e.CommandArgument).Split(' ');
            string field = commandArguments[0];
            string headerID = commandArguments[1];

            string sortDirection = GetSortDirection(
                field,
                ViewState[gvName + "SortExpression"] as string,
                ViewState[gvName + "SortDirection"] as string
            );
            ViewState[gvName + "SortExpression"] = field;
            ViewState[gvName + "SortDirection"] = sortDirection;

            string sortExpression;
            if (field == "TaskId") // We want to sort by length first for taskIds
            {
                sortExpression = string.Format("TaskIdLength {0}, TaskId {0}", sortDirection);
            }
            else
            {
                sortExpression = field + " " + sortDirection;
            }

            // This is where the actual sorting happens
            ((DataView) gv.DataSource).Sort = sortExpression;
            gv.DataBind();

            // Get rid of the arrow from the last sorted column
            string lastSortedColumn = ViewState[gvName + "SortedColumn"] as string;
            if (lastSortedColumn != null)
            {
                RemoveArrowFromColumn(gv, lastSortedColumn);
            }

            AddArrowToColumn(gv, headerID, sortDirection);
            ViewState[gvName + "SortedColumn"] = headerID;

            // Now we can store the latest sort in a cookie
            RequestHelper.StoreToCookie(gvName + "SortedColumn", headerID, SmallDateTime.MaxValue);
            RequestHelper.StoreToCookie(gvName + "SortExpression", field, SmallDateTime.MaxValue);
            RequestHelper.StoreToCookie(gvName + "SortDirection", sortDirection, SmallDateTime.MaxValue);
        }

        private void RemoveArrowFromColumn(GridView gridview, string headerID)
        {
            // This might happen if there are no tasks, or if we called RemoveArrowFromColumn at the wrong time
            if (gridview.HeaderRow == null)
            {
                return;
            }
            Label header = gridview.HeaderRow.FindControl(headerID) as Label;
            if (header != null)
            {
                header.Text = "";
            }
        }
        private void AddArrowToColumn(GridView gridview, string headerID, string sortDirection)
        {
            // This might happen if there are no tasks, or if we called AddArrowToColumn at the wrong time
            if (gridview.HeaderRow == null)
            {
                return; // die silently, we don't care
            }
            // Add the arrow to the sorted column
            Label header = gridview.HeaderRow.FindControl(headerID) as Label;
            if (header != null)
            {
                header.Text = sortDirection == "DESC" ? "▼" : "▲";
            }
        }

        /// <summary>
        /// Get the sort direction based on the example from
        /// http://msdn.microsoft.com/en-us/library/system.web.ui.webcontrols.gridview.sorting.aspx
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        private string GetSortDirection(string column, string lastSortExpression, string lastDirection)
        {
            string sortDirection = "ASC";

            if (lastSortExpression != null)
            {
                if (lastSortExpression == column)
                {
                    if ((lastDirection != null) && (lastDirection == "ASC"))
                    {
                        sortDirection = "DESC";
                    }
                }
            }
            return sortDirection;
        }
        #endregion

        #region Datagrid Display

        /// <summary>
        /// Check if the date in question is today or before today, then return
        /// the necessary css.
        /// </summary>
        /// <returns></returns>
        private string GetDateWarningCss(object queryDate)
        {
            if (queryDate is DateTime)
            {
                DateTime date = ((DateTime)queryDate).Date;
                // If the date is today or before today, style it differently
                return date.CompareTo(DateTime.Now.Date) <= 0 ? "DiscDue" : "DiscDueNot";
                // We take care of the row using jQuery, in _init
            }
            return "";
        }
        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion

        protected void m_openResolvedTasksGV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var row = e.Row;
            if(e.Row.DataItem != null)
            {
                var rowView = row.DataItem as DataRowView;

                bool isCondition = bool.Parse(rowView.Row["TaskIsCondition"].ToString());

                if (isCondition)
                {
                    var requiredDocSection = row.FindControl("RequiredDocSection") as HtmlGenericControl;
                    var requiredStatusImg = row.FindControl("RequiredStatusImg") as Image;
                    var requiredStatusText = row.FindControl("RequiredStatusText") as HtmlGenericControl;
                    var documentSection = row.FindControl("DocumentSection") as HtmlGenericControl;
                    var linkSection = row.FindControl("LinkSection") as HtmlGenericControl;
                    var associatedDocRepeater = row.FindControl("AssociatedDocRepeater") as Repeater;
                    var condRequiredDocTypeControl = row.FindControl("CondRequiredDocType") as HtmlGenericControl;
                    var associatedDocLinkContainer = row.FindControl("AssociatedDocLinkContainer") as HtmlGenericControl;
                    var associatedDocSection = row.FindControl("AssociatedDocSection") as HtmlGenericControl;
                    var dropzone = row.FindControl("DragDropZone") as HtmlGenericControl;

                    e.Row.Attributes.Add("taskid", rowView.Row["TaskId"].ToString());

                    var condRequiredDocType = rowView.Row["CondRequiredDocType"].ToString();
                    var isDocRequestSatisfied = bool.Parse(rowView.Row["IsDocRequestSatisfied"].ToString());
                    var associatedDocs = SerializationHelper.JsonNetDeserialize<List<SimplifiedDocAssociation>>(rowView.Row["AssociatedDocs"].ToString());

                    E_TaskStatus taskStatus = (E_TaskStatus)Enum.Parse(typeof(E_TaskStatus), rowView.Row["TaskStatus"].ToString());
                    associatedDocSection.Visible = taskStatus == E_TaskStatus.Active;
                    requiredDocSection.Visible = !string.Equals(condRequiredDocType, "None") && !string.IsNullOrEmpty(condRequiredDocType)
                        && (taskStatus == E_TaskStatus.Active || taskStatus == E_TaskStatus.Resolved);

                    dropzone.Attributes.Add("limit", this.FileUploadLimit.ToString());
                    dropzone.Attributes.Add("extensions", this.AcceptedFileExtensions);

                    if (associatedDocs.Count > 0)
                    {
                        documentSection.Visible = true;
                        DataTable associatedDocsTable = new DataTable();
                        associatedDocsTable.Columns.Add("docType");
                        associatedDocsTable.Columns.Add("docId");
                        associatedDocsTable.Columns.Add("hasAccess");

                        foreach (SimplifiedDocAssociation association in associatedDocs)
                        {
                            DataRow repeaterRow = associatedDocsTable.NewRow();
                            repeaterRow["docType"] = association.DocumentName;
                            repeaterRow["docId"] = association.DocumentId;
                            repeaterRow["hasAccess"] = association.HasAccess;

                            associatedDocLinkContainer.Visible = associatedDocLinkContainer.Visible || association.HasAccess;

                            associatedDocsTable.Rows.Add(repeaterRow);
                        }

                        associatedDocRepeater.DataSource = associatedDocsTable;
                        associatedDocRepeater.DataBind();
                    }

                    condRequiredDocTypeControl.InnerText = condRequiredDocType;

                    requiredStatusImg.ImageUrl = isDocRequestSatisfied ? "../../images/pass.png" : "../../images/fail.png";
                    requiredStatusText.InnerText = isDocRequestSatisfied ? "Associated" : "Required";
                    requiredStatusText.Attributes.Add("class", isDocRequestSatisfied ? "associated" : "required");
                }
            }
        }

        protected void AssociatedDocRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if(e.Item.DataItem != null)
            {
                var rowView = (DataRowView)e.Item.DataItem;
                var associatedDoc = e.Item.FindControl("AssociatedDoc") as HtmlGenericControl;
                var unlinkLink = e.Item.FindControl("UnlinkLink") as HtmlAnchor;

                associatedDoc.InnerText = rowView["docType"].ToString();
                unlinkLink.Attributes.Add("docId", rowView["docId"].ToString());
                unlinkLink.Visible = bool.Parse(rowView["hasAccess"].ToString());
            }
        }

        public class SimplifiedDocAssociation
        {
            public Guid DocumentId { get; set; }
            public string DocumentName { get; set; }

            public SimplifiedDocAssociation() { }

            public bool HasAccess { get; set; }
        }
    }
}
