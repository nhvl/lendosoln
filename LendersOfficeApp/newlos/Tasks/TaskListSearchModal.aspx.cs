﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Tasks
{
    public partial class TaskListSearchModal : LendersOffice.Common.BasePage
    {
        protected string TaskID
        {
            get { return RequestHelper.GetSafeQueryString("taskId"); }
        }

        protected string TaskSearchError
        {
            get { return RequestHelper.GetSafeQueryString("TaskSearchError"); }
        }

        protected void PageInit(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            // It doesn't belong to the loan file
            switch (TaskSearchError)
            {
                case "TaskNotInLoanFile":
                    m_title.Text = "Message";
                    m_infoText.Text = string.Format("Task {0} does not belong to this loan file. " +
                        "Click OK to open task {0} and its loan file.", TaskID.ToUpper());
                    break;
                case "TaskPermission":
                    m_title.Text = "ERROR";
                    m_infoText.Text = "You do not have the required permissions to access this task.";
                    m_CancelBtn.Visible = false;
                    break;
                case "TaskNotFound":
                    m_title.Text = "ERROR";
                    m_infoText.Text = "The selected task does not exist.";
                    m_CancelBtn.Visible = false;
                    break;
                default:
                    m_title.Text = "ERROR";
                    m_infoText.Text = "Error: unhandled case";
                    Tools.LogBug("Unhandled TaskSearchError case for TaskListSearchModal.");
                    m_CancelBtn.Visible = false;
                    break;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
