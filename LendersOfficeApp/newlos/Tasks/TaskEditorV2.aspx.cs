﻿namespace LendersOfficeApp.newlos.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.Services;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Email;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;

    public partial class TaskEditorV2 : BaseLoanPage
    {
        protected const string EMAIL_REGEX_PATTERN = ConstApp.EmailValidationExpression;

        public override bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the extra workflow operations to check.
        /// </summary>
        /// <returns>The extra workflow operations to check.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.UploadEDocs
            };
        }

        /// <summary>
        /// Gets the reasons for privileges that aren't allowed.
        /// </summary>
        /// <returns>The reasons for privileges that aren't allowed.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return this.GetExtraOpsToCheck();
        }

        protected string TaskID
        {
            get { return RequestHelper.GetSafeQueryString("taskId"); }
        }

        protected string Mode
        {
            get { return RequestHelper.GetSafeQueryString("mode"); }
        }

        protected bool IsTemplate
        {
            get { return RequestHelper.GetBool("IsTemplate"); }
        }

        protected bool ForceCondition
        {
            get { return RequestHelper.GetBool("IsCond"); }
        }

        protected bool IsNewTask
        {
            get { return string.IsNullOrEmpty(TaskID); }
        }

        protected BrokerDB brokerDB
        {
            get { return BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId); }
        }

        private string m_missingFields;

        private ConditionCategoriesWithPermissionInfo Categories
        {
            get; set;
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        private string AcceptedFileExtensions
        {
            get
            {
                if (this.Broker.AllowExcelFilesInEDocs)
                {
                    return ".xml, .pdf, .xls, .xlsx";
                }

                return ".xml, .pdf";
            }
        }

        private int FileUploadLimit => 12;

        protected override void LoadData()
        {
            this.Categories = new ConditionCategoriesWithPermissionInfo(PrincipalFactory.CurrentPrincipal); 

            Task task;
            E_UserTaskPermissionLevel permissionLevel;
            RequiredDocTypeLabel.Visible = brokerDB.IsEDocsEnabled;
            DocTypePicker.Visible = brokerDB.IsEDocsEnabled;
            DocTypePicker.EnableClearLink();
            DocTypePicker.SetDefault("None", "-1");

            var isCommentCellVisible = true;
            var isDocAssociationCellVisible = brokerDB.IsEDocsEnabled && !IsTemplate && !IsNewTask;

            if (!IsNewTask)
            {
                // Loading an existing task
                task = Task.Retrieve(BrokerID, TaskID);

                isDocAssociationCellVisible &= task.TaskIsCondition;
                switch (Mode)
                {
                    case "email":
                    case "reply":
                    case "forward":
                        EmailRow1.Visible = true;
                        EmailRow2.Visible = true;
                        EmailRow3.Visible = true;
                        EmailRow4.Visible = true;
                        EmailRow5.Visible = true;
                        isCommentCellVisible = false;
                        if (Mode != "email")
                        {
                            EmailBody.Text = task.MostRecentEmailContent;
                        }
                        m_Save.Text = "Send & Save";
                        m_Header.Text = "E-mail via " + (task.TaskIsCondition ? "Condition " : "Task ") + (IsTemplate ? "Template " : string.Empty) + task.TaskId;
                        break;
                    case "resolve":
                        permissionLevel = task.GetPermissionLevelFor(BrokerUser);
                        m_Close.Visible = permissionLevel == E_UserTaskPermissionLevel.Manage ||
                                          permissionLevel == E_UserTaskPermissionLevel.Close;
                        m_Resolve.Visible = true;
                        m_Save.Visible = false;
                        StatusNext.Text = " -> Resolved";
                        changeTaskAssignmentLink.Visible = false;
                        AssignedPointer.Text = " -> ";
                        AssignedOwner.Text = task.OwnerFullName;
                        //DocTypePicker.DisablePicking();
                        if (!task.TaskIsCondition)
                        {
                            ConditionRow5.Style.Add("display", "none");
                        }
                        m_Header.Text = "Resolve " + (task.TaskIsCondition ? "Condition " : "Task ") + (IsTemplate ? "Template " : string.Empty) + task.TaskId;
                        break;
                    case "activate":
                        if (task.TaskDueDate.HasValue && task.TaskDueDate.Value < DateTime.Today)
                        {
                            task.TaskDueDate = DateTime.Today;
                            task.TaskDueDateCalcDays = 0;
                            task.TaskDueDateCalcField = "";
                            task.TaskDueDateLocked = true;
                        }
                        if (task.TaskFollowUpDate.HasValue && task.TaskFollowUpDate.Value < DateTime.Today)
                        {
                            task.TaskFollowUpDate = DateTime.Today;
                        }
                        if (task.AssignToOnReactivationUserId.HasValue)
                        {
                            task.AssignToOnReactivationUserId = task.AssignToOnReactivationUserId.Value;
                        }
                        else if (task.TaskResolvedUserId.HasValue)
                        {
                            task.TaskAssignedUserId = task.TaskResolvedUserId.Value;
                        }
                        StatusNext.Text = " -> Active";
                        m_Header.Text = "Re-activate " + (task.TaskIsCondition ? "Condition " : "Task ") + (IsTemplate ? "Template " : string.Empty) + task.TaskId;
                        break;
                    case "edit":
                    default:
                        m_Header.Text = "Edit " + (task.TaskIsCondition ? "Condition " : "Task ") + (IsTemplate ? "Template " : string.Empty) + task.TaskId;
                        break;
                }
                StatusResolved.Value = (task.TaskStatus == E_TaskStatus.Resolved).ToString();
                OwnerID.Value = task.TaskOwnerUserId_rep;
                AssignedID.Value = task.TaskAssignedUserId_rep;


                SavedVersion.Value = task.TaskRowVersion;
                DueDate.Text = task.TaskDueDate_rep;
                DueDateCalcDescription.Text = task.DueDateDescription(true);

                if (task.TaskStatus == E_TaskStatus.Resolved)
                {
                    changeTaskAssignmentLink.Visible = false;
                }
                if (task.TaskStatus == E_TaskStatus.Closed
                    || task.TaskOwnerUserId == BrokerUser.UserId)
                {
                    takeOwnershipLink.Visible = false;
                }
            }
            else
            {
                // Creating a new task
                task = Task.Create(LoanID, BrokerID);
                m_Header.Text = "New " + (ForceCondition ? "Condition " : "Task ") + (IsTemplate ? "Template " : string.Empty);
                HistoryDiv.Visible = false;
                ExistingTaskRow.Visible = false;
                DueDate.Text = DateTime.Today.ToShortDateString();

                if (ForceCondition)
                {
                    LoanAssignmentContactTable lTable = new LoanAssignmentContactTable(BrokerID, LoanID);
                    EmployeeLoanAssignment loanOfficer = lTable.FindByRoleT(E_RoleT.LoanOfficer);
                    if (loanOfficer != null)
                    {
                        AbstractUserPrincipal principal = PrincipalFactory.RetrievePrincipalForUser(BrokerID, loanOfficer.UserId, loanOfficer.IsPmlUser ? "P" : "B") as AbstractUserPrincipal;
                        if (principal != null)
                        {
                            task.TaskAssignedUserId = loanOfficer.UserId;
                        }
                    }
                }
            }

            this.RegisterJsGlobalVariables("IsCommentCellVisible", isCommentCellVisible);
            this.RegisterJsGlobalVariables("IsDocAssociationCellVisible", isDocAssociationCellVisible);

            Tools.Bind_ConditionCategoryDdl(task.UserPermissionLevel, this.Categories, ConditionCategories, task.CondCategoryId, task.CondCategoryId_rep);

            PermissionLevelID.Value = task.TaskPermissionLevelId.ToString();
            PermissionDescription.Value = task.PermissionLevelDescription;
            TaskPermission.Text = task.PermissionLevelName;

            if (ForceCondition && IsNewTask)
            {
                task.MakeCondition();
                PermissionLevel level = task.DefaultPermissionForCondition;
                if (level == null)
                {
                    // Per OPM 108448, Cannot let user create a condition they cannot manage.
                    throw new CBaseException("You do not have the necessary permissions to add a condition.", "You do not have the necessary permissions to add a condition.");
                }
                PermissionLevelID.Value = level.Id.ToString();
                PermissionDescription.Value = level.Description;
                TaskPermission.Text = level.Name;                
            }

            if (IsTemplate)
            {
                ExistingTaskRow.Visible = false;
                FollowUpDateRow.Visible = false;
                TaskRow.Visible = false;
                TaskTemplateOwner.Text = task.OwnerFullName;
                TaskTemplateAssigned.Text = task.AssignedUserFullName;
            }
            else
            {
                TemplateRow.Visible = false;
                BorrowerName.Text = task.BorrowerNmCached;
                sLNm.Text = task.LoanNumCached;
                TaskOwner.Text = task.OwnerFullName;
                Assigned.Text = task.AssignedUserFullName;
                FollowupDate.Text = task.TaskFollowUpDate_rep;
            }

            if (Task.CanViewField(BrokerUser, task.TaskDueDateCalcField))
            {
                DueDateCalcField.Value = task.TaskDueDateCalcField;
            }
            DueDateOffset.Value = task.TaskDueDateCalcDays.ToString();
            AssignedID.Value = task.TaskAssignedUserId.ToString();
            OwnerRole.Value = task.TaskToBeOwnerRoleId_rep;
            AssignedRole.Value = task.TaskToBeAssignedRoleId_rep;
            Status.Text = task.TaskStatus_rep;
            History.Text = task.TaskHistoryHtml;
            DueDateLocked.Checked = task.TaskDueDateLocked;
            Subject.Text = task.TaskSubject;
            FromAddress.Text = task.TaskFromAddress;

            if (!BrokerUser.HasPermission(Permission.AllowReadingFromRolodex))
            {
                m_addForCCAddress.Visible = false;
                m_addForToAddress.Visible = false;
            }



            if (task.TaskIsCondition)
            {
                IsCondition.Checked = true;
                DocRequestSatisfied.Text = task.ConditionFulfilsAssociatedDocs_rep();
                if (task.CondRequiredDocTypeId.HasValue)
                {
                    DocTypePicker.Select(task.CondRequiredDocTypeId.Value);
                }

                if (DocRequestSatisfied.Text.Equals("No", StringComparison.CurrentCultureIgnoreCase))
                {
                    DocRequestSatisfied.ForeColor = System.Drawing.Color.Red;
                }

                if (task.CondCategoryId.HasValue)
                {
                    ConditionCategories.SelectedValue = task.CondCategoryId.Value.ToString();
                }
                else
                {
                    if (!IsNewTask)
                    {
                        Tools.LogBug("Default value for condition category missing");
                    }
                }
                
                if (Task.CanUserViewHiddenInformation(BrokerUser))
                {
                    ConditionIsHidden.Checked = task.CondIsHidden;
                }
                else
                {
                    if (task.CondIsHidden)
                    {
                        throw new CBaseException(ErrorMessages.GenericAccessDenied, "User doesn't have permission to see hidden conditions");
                    }
                    ConditionIsHidden.Visible = false;
                    IsHiddenLabel.Visible = false;
                }

                ConditionInternalNotes.Text = task.CondInternalNotes;

                var associatedDocs = task.AssociatedDocs;
                if (associatedDocs.Count != 0)
                {
                    this.DocumentSection.Visible = true;

                    var userDocRepo = EDocumentRepository.GetUserRepository();
                    var listLoanDocs = new HashSet<Guid>(userDocRepo.GetDocumentsByLoanId(this.LoanID).Select(doc => doc.DocumentId));

                    var associatedDocsTable = new DataTable();
                    associatedDocsTable.Columns.Add("docType");
                    associatedDocsTable.Columns.Add("docId");
                    associatedDocsTable.Columns.Add("hasAccess");

                    foreach (var association in associatedDocs)
                    {
                        var hasAccess = listLoanDocs.Contains(association.DocumentId);

                        var repeaterRow = associatedDocsTable.NewRow();
                        repeaterRow["docType"] = association.DocumentFolderName + " : " + association.DocumentTypeName;
                        repeaterRow["docId"] = association.DocumentId;
                        repeaterRow["hasAccess"] = hasAccess;

                        this.AssociatedDocLinkContainer.Visible |= hasAccess;
                        associatedDocsTable.Rows.Add(repeaterRow);
                    }

                    this.AssociatedDocRepeater.DataSource = associatedDocsTable;
                    this.AssociatedDocRepeater.DataBind();
                }
            }
            else
            {
                ConditionRow1.Style.Add("display", "none");
                ConditionRow2.Style.Add("display", "none");
                ConditionRow3.Style.Add("display", "none");
                ConditionRow4.Style.Add("display", "none");
            }

            IsCondition.Disabled = (!ForceCondition && task.TaskIsCondition) || !task.PermissionLevelAppliesToConditions;
            if (task.TaskStatus == E_TaskStatus.Resolved)
            {
                IsCondition.Disabled = true;
            }

            permissionLevel = task.GetPermissionLevelFor(BrokerUser);
            if (permissionLevel == E_UserTaskPermissionLevel.WorkOn ||
                permissionLevel == E_UserTaskPermissionLevel.Close)
            {
                Subject.ReadOnly = true;
                DueDate.ReadOnly = true;
                DueDateLocked.Enabled = false;
                takeOwnershipLink.Visible = false;
                dueDateCalculation.Visible = false;
                changePermissionLevel.Visible = false;
                IsCondition.Disabled = true;
                ConditionCategories.Enabled = false;
                ConditionIsHidden.Enabled = false;
                ConditionInternalNotes.ReadOnly = true;
            }

            try
            {
                var assignedPrincipal = PrincipalFactory.RetrievePrincipalForUser(this.BrokerID, new Guid(AssignedID.Value), "?") as AbstractUserPrincipal;
                if (assignedPrincipal != null && assignedPrincipal.Type == "P")
                {
                    ConditionIsHidden.Checked = false;
                    ConditionIsHidden.InputAttributes["disabled"] = "true";
                }
            }
            catch (ArgumentNullException) { } // Then there is no assigned user. Do nothing.

            //hide if its not a condition
            if (!IsCondition.Checked)
            {
                RequiredDocTypeLabel.Attributes["class"] += " hidden";
                RequiredDocTypePicker.Attributes["class"] += " hidden";
            }

            ClientScript.RegisterHiddenField("AccessLevel", permissionLevel.ToString());
            if (permissionLevel != E_UserTaskPermissionLevel.Manage)
            {
                DocTypePicker.DisablePicking();
            }

            ResolutionSettingsRow.Visible = !string.IsNullOrEmpty(task.ResolutionBlockTriggerName) || !string.IsNullOrEmpty(task.ResolutionDateSetterFieldId);

            IsResolutionBlocked.Value = task.IsResolutionBlocked().ToString();
            ResolutionDenialMessage.Value = task.ResolutionDenialMessage;

            ResolutionBlockTrigger.Text = task.ResolutionBlockTriggerName;
            ResolutionBlockTriggerName.Value = task.ResolutionBlockTriggerName;
            if (string.IsNullOrEmpty(task.ResolutionBlockTriggerName) || permissionLevel != E_UserTaskPermissionLevel.Manage)
            {
                ClearResolutionBlockTrigger.Visible = false;
            }

            ResolutionDateSetterFieldIdLabel.Text = task.GetResolutionDateSetterFieldDescription();
            ResolutionDateSetterFieldId.Value = task.ResolutionDateSetterFieldId;
            if (string.IsNullOrEmpty(task.ResolutionDateSetterFieldId) || permissionLevel != E_UserTaskPermissionLevel.Manage)
            {
                ClearResolutionDateSetter.Visible = false;
            }

            IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation> triggers = TaskTriggerAssociation.GetTaskTriggerAssociations(task);

            AutoTriggersRow.Visible = triggers.ContainsKey(TaskTriggerType.AutoResolve) || triggers.ContainsKey(TaskTriggerType.AutoClose);
            AutoResolveTriggerLabel.Text = triggers.ContainsKey(TaskTriggerType.AutoResolve) ? triggers[TaskTriggerType.AutoResolve].TriggerName : "None";
            AutoCloseTriggerLabel.Text = triggers.ContainsKey(TaskTriggerType.AutoClose) ? triggers[TaskTriggerType.AutoClose].TriggerName : "None";

            this.RegisterJsGlobalVariables("CanUploadEdocs", this.UserHasWorkflowPrivilege(WorkflowOperations.UploadEDocs));
            this.RegisterJsGlobalVariables("UploadEdocsFailureMessage", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.UploadEDocs));
        }

        protected void AssociatedDocRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                var rowView = (DataRowView)e.Item.DataItem;
                var associatedDoc = e.Item.FindControl("AssociatedDoc") as HtmlGenericControl;
                var unlinkLink = e.Item.FindControl("UnlinkLink") as HtmlAnchor;

                associatedDoc.InnerText = rowView["docType"].ToString();
                unlinkLink.Attributes.Add("docId", rowView["docId"].ToString());
                unlinkLink.Visible = bool.Parse(rowView["hasAccess"].ToString());
            }
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            EmailBody.Attributes.Add("onfocus", "emailBodySetCursorPosition(this);");
            EmailBody.Attributes.Add("onblur", "emailBodyGetCursorPosition(this);");
            
            this.PageTitle = "Task Editor";

            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("DragDropUpload.js");
            this.RegisterJsScript("DocumentUploadHelper.js");
            this.RegisterJsScript("ConditionDocumentUploadHelper.js");

            this.RegisterCSS("DragDropUpload.css");

            this.RegisterService("edoc", "/newlos/ElectronicDocs/EDocsUtilitiesService.aspx");

            this.DragDropZone.Attributes.Add("limit", this.FileUploadLimit.ToString());
            this.DragDropZone.Attributes.Add("extensions", this.AcceptedFileExtensions);

            EnableJqueryMigrate = false;
        }

        [WebMethod]
        public static object GetTaskAssociationStatus(string taskid, int newDocTypeId)
        {
            Task t = Task.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId, taskid);
            if (newDocTypeId != -1)
            {
                t.CondRequiredDocTypeId = newDocTypeId;
            }
            else
            {
                t.CondRequiredDocTypeId = null;
            }
           

            return new { Status = t.ConditionFulfilsAssociatedDocs_rep() };
        }

        protected void OnSaveClicked(Object sender, CommandEventArgs e)
        {
            Task task;
            if (IsNewTask)
            {
                task = Task.Create(LoanID, BrokerID);
            }
            else
            {
                task = Task.Retrieve(BrokerID, TaskID);
            }

            m_missingFields = "";

            if (string.IsNullOrEmpty(Subject.Text.TrimWhitespaceAndBOM()))
            {
                appendMissingField("Subject");
                SubjectR.Visible = true;
            }
            else
            {
                SubjectR.Visible = false;
            }

            if ((DueDateLocked.Checked && string.IsNullOrEmpty(DueDate.Text))
                || (!DueDateLocked.Checked && string.IsNullOrEmpty(DueDateCalcField.Value) && string.IsNullOrEmpty(task.TaskDueDateCalcField)))
            {
                appendMissingField("Due Date");
                DueDateR.Visible = true;
            }
            else
            {
                DueDateR.Visible = false;
            }

            bool AddressValid = true;
            if ((Mode == "email" || Mode == "reply" || Mode == "forward"))
            {
                if (string.IsNullOrEmpty(ToAddress.Text.TrimWhitespaceAndBOM()))
                {
                    appendMissingField("To");
                    ToAddressR.Visible = true;
                }
                else
                {
                    ToAddressR.Visible = false;
                    Regex reEmail = new Regex(EMAIL_REGEX_PATTERN);

                    string[] toAddresses = ToAddress.Text.TrimWhitespaceAndBOM().Replace(',', ';').Split(';');
                    foreach (string s in toAddresses)
                    {
                        AddressValid &= reEmail.IsMatch(s.TrimWhitespaceAndBOM()); // sk 106415
                    }
                    if (!string.IsNullOrEmpty(CCAddress.Text.TrimWhitespaceAndBOM()))
                    {
                        string[] CCAddresses = CCAddress.Text.TrimWhitespaceAndBOM().Replace(',', ';').Split(';');
                        foreach (string s in CCAddresses)
                        {
                            AddressValid &= reEmail.IsMatch(s.TrimWhitespaceAndBOM());
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(m_missingFields) || !AddressValid)
            {
                int offset = 0;
                try
                {
                    offset = Convert.ToInt32(DueDateOffset.Value);
                }
                catch (FormatException) { }
                string fieldId = DueDateCalcField.Value;
                if (string.IsNullOrEmpty(fieldId))
                {
                    fieldId = task.TaskDueDateCalcField;
                }
                DueDateCalcDescription.Text = Tools.GetTaskDueDateDescription(offset, fieldId, true);
                if (!string.IsNullOrEmpty(m_missingFields))
                {
                    m_errorMessage.Text = "The following fields are required: " + m_missingFields;
                }
                if (!AddressValid)
                {
                    m_errorMessage.Text += Environment.NewLine + ErrorMessages.InvalidEmailAddresses;
                }
                return;
            }

            task.TaskDueDateLocked = DueDateLocked.Checked;
            if (DueDateLocked.Checked)
            {
                task.TaskDueDate_rep = DueDate.Text;
                task.TaskDueDateCalcField = "";
                task.TaskDueDateCalcDays = 0;
            }
            else
            {
                if (!string.IsNullOrEmpty(DueDateCalcField.Value.TrimWhitespaceAndBOM()))
                {
                    task.TaskDueDateCalcField = DueDateCalcField.Value;
                    task.TaskDueDateCalcDays = Convert.ToInt32(DueDateOffset.Value);
                    if (string.IsNullOrEmpty(DueDate.Text))
                    {
                        task.TaskDueDate = null;
                    }
                    else
                    {
                        task.TaskDueDate = DateTime.Parse(DueDate.Text);
                    }
                }
            }

            if (IsNewTask)
            {
                task.TaskCreatedByUserId = BrokerUser.UserId;
                if (!IsTemplate || (new Guid(OwnerID.Value) == Guid.Empty))
                {
                    OwnerID.Value = BrokerUser.UserId.ToString();
                }
            }

            task.TaskSubject = Subject.Text;
            if (new Guid(OwnerID.Value) != Guid.Empty)
            {
                task.TaskOwnerUserId = new Guid(OwnerID.Value);
            }
            else
            {
                task.TaskToBeOwnerRoleId = new Guid(OwnerRole.Value);
            }
            if (new Guid(AssignedID.Value) != Guid.Empty)
            {
                task.TaskAssignedUserId = new Guid(AssignedID.Value);
            }
            else
            {
                task.TaskToBeAssignedRoleId = new Guid(AssignedRole.Value);
            }
            task.TaskFollowUpDate_rep = FollowupDate.Text;
            task.Comments = m_Comments.Text;

            if (IsCondition.Checked)
            {
                task.MakeCondition();
            }

            var originalTaskPermissionLevelId = task.TaskPermissionLevelId;
            task.TaskPermissionLevelId = Convert.ToInt32(PermissionLevelID.Value);

            if (task.TaskIsCondition)
            {
                int typeId = int.Parse(DocTypePicker.Value);
                if (DocTypePicker.Value != "-1")
                {
                    task.CondRequiredDocTypeId = typeId;
                }
                else
                {
                    task.CondRequiredDocTypeId = null;
                }

                if (ConditionIsHidden.Visible)
                {
                    task.CondIsHidden = ConditionIsHidden.Checked;
                }

                task.CondInternalNotes = ConditionInternalNotes.Text;
                task.CondCategoryId = Convert.ToInt32(ConditionCategories.SelectedValue);

                if(originalTaskPermissionLevelId == Convert.ToInt32(PermissionLevelID.Value) && task.CondCategoryId.HasValue)
                {
                    task.TaskPermissionLevelId = ConditionCategory.GetCategory(BrokerID, task.CondCategoryId.Value).DefaultTaskPermissionLevelId;
                }
            }
            else
            {
                task.CondRequiredDocTypeId = null;
            }

            var permissionLevel = task.GetPermissionLevelFor(BrokerUser);
            if (task.GetPermissionLevelFor(BrokerUser) == E_UserTaskPermissionLevel.Manage)
            {
                task.ResolutionBlockTriggerName = ResolutionBlockTriggerName.Value;
                task.ResolutionDateSetterFieldId = ResolutionDateSetterFieldId.Value;
            }

            switch (Mode)
            {
                case "resolve":
                    if (e.CommandArgument.ToString() == "CloseTask")
                    {
                        task.ResolveAndClose();
                    }
                    else
                    {
                        task.Resolve();
                    }
                    ClosePage(TaskID);
                    break;
                case "activate":
                    if (e.CommandArgument.ToString() == "RemoveAndReactivate")
                    {
                        RemoveAndReactivate(task);
                    }
                    else
                    {
                        Reactivate(task);
                    }

                    break;
                case "email":
                case "reply":
                case "forward":
                    CBaseEmail email = new CBaseEmail(BrokerID);
                    email.From = task.TaskFromAddress;
                    email.To = ToAddress.Text;
                    email.CCRecipient = CCAddress.Text;
                    email.Subject = EmailSubject.Value;
                    email.Message = EmailBody.Text;
                    try
                    {
                        task.SaveAndSendEmail(SavedVersion.Value, email);
                    }
                    catch (TaskSaveAndSendWhenNotActiveException)
                    {
                        m_errorMessage.Text = "Another user has resolved or closed this task.";
                        Tools.LogError(string.Format("TaskSaveAndSendWhenNotActiveException see 114783.  loan {0}, task {1}", LoanID, task.TaskId));
                        return;
                    }
                    ClosePage(TaskID);
                    break;
                case "edit":
                default:
                    bool wasNew = IsNewTask;
                    if (task.Save(true))
                    {
                        if (wasNew)
                        {
                            ScriptManager.RegisterClientScriptBlock(
                                Page,
                                Page.GetType(),
                                "close",
                                string.Format(@"closePage('{0}');", task.TaskId),
                                true
                            );
                        }
                        else
                        {
                            ClosePage(task.TaskId);
                        }
                    }
                    else
                    {
                        LoadData();
                    }
                    break;
            }
        }

        private void Reactivate(Task task)
        {
            bool isAutoResolveTriggerTrue = task.IsAutoResolveTriggerTrue();
            bool isAutoCloseTriggerTrue = task.IsAutoCloseTriggerTrue();

            if (!isAutoResolveTriggerTrue && !isAutoCloseTriggerTrue)
            {
                // triggers not hit, reactivate and close.
                task.Reactivate();
                ClosePage(TaskID);
            }

            // Triggers hit. Open pop-up.
            string headerString = null;
            string warningString = null;
            if(isAutoResolveTriggerTrue && isAutoCloseTriggerTrue)
            {
                headerString = "Auto-Resolve and Auto-Close";
                warningString = "auto-resolve and auto-close";
            }
            else if (isAutoResolveTriggerTrue)
            {
                headerString = "Auto-Resolve";
                warningString = "auto-resolve";
            }
            else if (isAutoCloseTriggerTrue)
            {
                headerString = "Auto-Close";
                warningString = "auto-close";
            }

            this.IsAutoResolveTriggerTrue.Value = isAutoResolveTriggerTrue.ToYN();
            this.IsAutoCloseTriggerTrue.Value = isAutoCloseTriggerTrue.ToYN();

            this.warningHeaderLiteral.Text = headerString;
            this.warningLiteral1.Text = warningString;
            this.warningLiteral2.Text = warningString;
            this.warningLiteral3.Text = warningString;

            this.RegisterJsGlobalVariables("ShowReactivateWarning", true);
        }

        private void RemoveAndReactivate(Task task)
        {
            //remove triggers.
            if (this.IsAutoResolveTriggerTrue.Value == "Y")
            {
                TaskTriggerAssociation.DeleteTaskTriggerAssociations(task, TaskTriggerType.AutoResolve);
            }

            if (this.IsAutoCloseTriggerTrue.Value == "Y")
            {
                TaskTriggerAssociation.DeleteTaskTriggerAssociations(task, TaskTriggerType.AutoClose);
            }

            this.IsAutoResolveTriggerTrue.Value = null;
            this.IsAutoCloseTriggerTrue.Value = null;

            task.Reactivate();
            ClosePage(TaskID);
        }

        private void appendMissingField(string field)
        {
            if (string.IsNullOrEmpty(m_missingFields))
            {
                m_missingFields += field;
            }
            else
            {
                m_missingFields += ", " + field;
            }
        }

        private void ClosePage(string taskID)
        {
            Response.Redirect(String.Format("TaskViewer.aspx?taskId={0}&loanid={1}&IsTemplate={2}", taskID, LoanID, IsTemplate));
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            DisplayCopyRight = false;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
