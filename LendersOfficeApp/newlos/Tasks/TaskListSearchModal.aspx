﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="TaskListSearchModal.aspx.cs" Inherits="LendersOfficeApp.newlos.Tasks.TaskListSearchModal" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Task List Search Modal</title>
    <link href="../../css/stylesheet.css" type=text/css rel=stylesheet />
    <style type="text/css">
        #Window
        {
        	padding: 5px;
        }
        #BottomButtons
        {
            text-align: center;
        }
        span.MainRightHeader {
            padding-left: 5px; padding-right: 0;
        }
    </style>
</head>
<body>
    <h4 class="page-header"><ml:EncodedLabel runat="server" id="m_title">Search</ml:EncodedLabel></h4>
    <form id="TaskListSearchModal" runat="server">
    <script type="text/javascript">
        // This dialog will be shown when there is a warning or error concerning task search
        
        function _init() {
            resizeForIE6And7(300, 150);

            $("#m_OKBtn").click(function() {
                var result = window.dialogArguments || {};
                result.OK = true;
                onClosePopup(result);
            });

            $("#m_CancelBtn").click(function() {
                var result = window.dialogArguments || {};
                result.OK = false;
                onClosePopup(result);
            });
        }
	</script>
    <div id="Window">
        <ml:EncodedLabel runat="server" id="m_infoText" Text="Placeholder text!"></ml:EncodedLabel>
        <br />
        <br />
        
        <div id="BottomButtons">
            <asp:Button runat="server" text="OK" id="m_OKBtn" />
            <asp:Button runat="server" text="Cancel" id="m_CancelBtn" />
        </div>
    </div>
    <uc1:cModalDlg ID="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cModalDlg>
    </form>
</body>
</html>
