﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;

namespace LendersOfficeApp.newlos.Tasks
{
    public partial class DateCalculatorServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(DateCalculatorServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "generateOutput":
                    GenerateOutput();
                    break;
            }
        }

        private void GenerateOutput()
        {
            string field = GetString("field");
            int offset = GetInt("offset");
            bool addOffset = GetBool("addOffset");
            bool pastDate = false;
            if (!addOffset)
            {
                offset *= -1;
            }
            if (!string.IsNullOrEmpty(GetString("loanid")))
            {
                SetResult("dateDescription", Tools.GetTaskDueDateDescription(sLId, offset, field, true));
                string dateString = Task.GetCalculatedDueDate(sLId, offset, field);
                if (!string.IsNullOrEmpty(dateString) && Convert.ToDateTime(dateString) < DateTime.Today)
                {
                    pastDate = true;
                }
                SetResult("date", dateString);
            }
            else
            {
                SetResult("dateDescription", Tools.GetTaskDueDateDescription(offset, field, true));
                SetResult("date", string.Empty);
            }
            SetResult("pastDate", pastDate);
            SetResult("offset", offset);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
    public partial class DateCalculatorService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new DateCalculatorServiceItem());
        }
    }
}
