﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;
using System.Collections.Specialized;
using LendersOffice.Security;
using System.Xml.Serialization;
using System.Text;
using System.IO;

namespace LendersOfficeApp.newlos.Tasks
{
    public class TaskListServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        private Guid UserId
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.UserId; }
        }

        private Guid BrokerId
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
        }

        public const int TASKLIST_EXPORT_TO_PDF_CACHE_TIME_IN_MINUTES = 10;

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TaskListServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "TaskSearch":
                    TaskSearch();
                    break;
                case "Assign":
                    Assign();
                    break;
                case "SetDueDate":
                    SetDueDate();
                    break;
                case "SetFollowupDate":
                    SetFollowupDate();
                    break;
                case "TakeOwnership":
                    TakeOwnership();
                    break;
                case "Subscribe":
                    Subscribe();
                    break;
                case "Unsubscribe":
                    Unsubscribe();
                    break;
                case "ExportToPDF":
                    ExportToPDF();
                    break;
                case "Resolve":
                    Resolve();
                    break;
                case "Close":
                    Close();
                    break;
                default:
                    break;
            }
        }

        private void TaskSearch()
        {
            string taskID = GetString("TaskID");

            // search for the actual task, get a link to it

            Task retrievedTask;
            try
            {
                retrievedTask = Task.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId, taskID);
            }
            catch (TaskNotFoundException)
            {
                SetResult("OK", false);
                SetResult("TaskSearchError", "TaskNotFound");
                return;
            }
            catch (TaskPermissionException)
            {
                SetResult("OK", false);
                SetResult("TaskSearchError", "TaskPermission");
                return;
            }

            Guid loanID = GetGuid("LoanID");
            if (retrievedTask.LoanId != loanID)
            {
                CPageData dataLoan;
                try
                {
                    dataLoan = CPageData.CreateUsingSmartDependency(retrievedTask.LoanId, typeof(TaskListServiceItem));
                    dataLoan.InitLoad();
                }
                catch (PageDataAccessDenied) // If the user tries to read a task in a loan that you don't have access to
                {
                    SetResult("OK", false);
                    SetResult("TaskSearchError", "TaskPermission");
                    return;
                }
                SetResult("OK", false);
                SetResult("TaskSearchError", "TaskNotInLoanFile");
                SetResult("LoanId", retrievedTask.LoanId.ToString());
                SetResult("LoanName", dataLoan.sLNm.ToString());
                return;
            }

            SetResult("OK", true);
            SetResult("TaskSearchError", ""); // let's make it explicit
        }

        private void Assign()
        {
            ForEachServiceTask(delegate(Task task, string taskVersion)
            {

                Guid id = GetGuid("Id");
                string name = GetString("Name");
                string type = GetString("Type");

                if (task.TaskStatus == E_TaskStatus.Resolved)
                {
                    throw new TaskResolvedReassignmentException();
                }

                if (type == "user")
                {
                    task.TaskAssignedUserId = id;
                    task.TaskToBeAssignedRoleId = Guid.Empty;
                }
                else
                {
                    task.TaskAssignedUserId = Guid.Empty;
                    task.TaskToBeAssignedRoleId = id;
                }
            });
        }

        private void SetDueDate()
        {
            ForEachServiceTask(delegate(Task task, string taskVersion)
            {
                int offset = GetInt("Offset", 0);
                string dateField = GetString("Id", "");
                string dateString = GetString("Date", "");
                bool dateLocked = GetBool("DateLocked", false);

                task.TaskDueDateCalcDays = offset;
                task.TaskDueDateCalcField = dateField;
                task.TaskDueDateLocked = dateLocked;
                task.TaskDueDate_rep = dateString;
            });
        }

        private void SetFollowupDate()
        {
            ForEachServiceTask(delegate(Task task, string taskVersion)
            {
                task.TaskFollowUpDate_rep = GetString("Date");
            });
        }

        private void TakeOwnership()
        {
            ForEachServiceTask(delegate(Task task, string taskVersion)
            {
                task.TaskOwnerUserId = UserId;
            });
        }

        private void Subscribe()
        {
            ForEachServiceTask(delegate(Task task, string taskVersion)
            {
                TaskSubscription.Subscribe(this.BrokerId, task.TaskId, UserId);
            });
        }

        private void Unsubscribe()
        {
            ForEachServiceTask(delegate(Task task, string taskVersion)
            {
                TaskSubscription.Unsubscribe(this.BrokerId, task.TaskId, UserId);
            });
        }

        private void Resolve()
        {
            // Calling Resolve saves the task, so no need to save again.
            ForEachServiceTask(delegate(Task task, string taskVersion)
            {
                if (task.TaskStatus != E_TaskStatus.Resolved)
                {
                    task.Resolve(taskVersion);
                }
            }, false);
        }

        private void Close()
        {
            // Calling Close saves the task, so no need to save again.
            ForEachServiceTask(delegate(Task task, string taskVersion)
            {
                if (task.TaskStatus != E_TaskStatus.Closed)
                {
                    task.Close(taskVersion);
                }
            }, false);
        }

        /// <summary>
        /// Performs the given operation on the selected task ids.
        /// Saves the task after performing the operation.
        /// </summary>
        /// <param name="Operation"></param>
        private void ForEachServiceTask(Action<Task, string> Operation)
        {
            ForEachServiceTask(Operation, true);
        }

        private void ForEachServiceTask(Action<Task, string> Operation, bool saveTaskAfterOperation)
        {
            string taskIdsRawString = GetString("TaskIDs");
            string taskVersionsRawString = GetString("TaskVersions");
            string resolutionBlockTriggerNamesRawString = GetString("TaskResolutionBlockTriggerNames", "");
            string resolutionDateFieldIdsRawString = GetString("TaskResolutionDateFieldIds", "");
            Guid sLId = GetGuid("LoanId");
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;

            string[] taskIds = taskIdsRawString.Split(',');
            string[] taskVersions = taskVersionsRawString.Split(',');
            string[] resolutionBlockTriggerNames = resolutionBlockTriggerNamesRawString.Split(new char[] { ',' },
                StringSplitOptions.RemoveEmptyEntries);
            string[] resolutionDateFieldIds = resolutionDateFieldIdsRawString.Split(new char[] { ',' },
                StringSplitOptions.RemoveEmptyEntries);

            BatchTaskFailure errorTracker = new BatchTaskFailure(E_TaskSaveFailureOperation.SAVE_TASKLIST);

            ResolutionTriggerProcessor resolutionTriggerProcessor = null;
            ResolutionDateSetter resolutionDateSetter = null;

            if (resolutionBlockTriggerNames.Any())
            {
                resolutionTriggerProcessor = new ResolutionTriggerProcessor(
                    sLId,
                    brokerId,
                    PrincipalFactory.CurrentPrincipal,
                    resolutionBlockTriggerNames);
            }

            if (resolutionDateFieldIds.Any())
            {
                resolutionDateSetter = new ResolutionDateSetter(
                    sLId,
                    PrincipalFactory.CurrentPrincipal);
            }

            for (int i = 0; i < taskIds.Length; i++)
            {
                Task task;
                try
                {
                    task = Task.Retrieve(brokerId, taskIds[i]);
                }
                catch (TaskNotFoundException e)
                {
                    errorTracker.Add(new TaskFailure()
                    {
                        Id = taskIds[i],
                        Reason = e.UserMessage,
                        Subject = ""
                    });
                    continue;
                }

                // Set these properties to cache the workflow trigger results
                // and avoid saving the loan for every resolution date field.
                task.ResolutionTriggerProcessor = resolutionTriggerProcessor;
                task.ResolutionDateSetter = resolutionDateSetter;

                try
                {
                    Operation(task, taskVersions[i]); // run the operation that we passed in on the task
                    if (saveTaskAfterOperation)
                    {
                        task.Save(taskVersions[i], false);
                    }
                }
                catch (TaskException e)
                {
                    errorTracker.Add(new TaskFailure()
                    {
                        Id = task.TaskId,
                        Reason = e.UserMessage,
                        Subject = task.TaskSubject
                    });
                }
            }
            if (taskIds.Length > 0)
            {
                TaskUtilities.EnqueueTasksDueDateUpdate(brokerId, sLId);

                if (resolutionDateSetter != null)
                {
                    resolutionDateSetter.SetFieldsToCurrentDateAndTime();
                }
            }
            if (errorTracker.HasErrors())
            {
                string cacheId = errorTracker.SaveToCache();
                SetResult("ErrorCacheId", cacheId);
                SetResult("TotalFailure", errorTracker.Failures.Count() == taskIds.Length);
            }
        }

        private void ExportToPDF()
        {
            Guid sLId = GetGuid("LoanId");
            string taskIDs = GetString("TaskIDs"); // each task is delimited by a comma
            
            // Write the taskIDs to cache
            string cacheId = AutoExpiredTextCache.AddToCache(taskIDs, TimeSpan.FromMinutes(TASKLIST_EXPORT_TO_PDF_CACHE_TIME_IN_MINUTES));

            var pdfInstance = new LendersOffice.Pdf.CTaskListPDF(); // Create an instance just to get its name? Hmm. There should be a better way to do this.

            string filepath = string.Format(
                "{0}/pdf/{1}.aspx?loanid={2}&cacheId={3}",
                DataAccess.Tools.VRoot,
                pdfInstance.Name,
                sLId.ToString(),
                cacheId
            );
            SetResult("filepath", filepath);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }

    }
    public partial class TaskListService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override bool PerformResultOptimization
        {
            get
            {
                return true;
            }
        }
        protected override void Initialize()
        {
            AddBackgroundItem("", new TaskListServiceItem());
        }
    }
}
