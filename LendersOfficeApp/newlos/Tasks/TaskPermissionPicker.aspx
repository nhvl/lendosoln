﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskPermissionPicker.aspx.cs" Inherits="LendersOfficeApp.newlos.Tasks.TaskPermissionPicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Task Permission Picker</title>
</head>
<body bgcolor="gainsboro">
    <h4 class="page-header">Select task permission level</h4>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function _init() {
                resizeForIE6And7(600, 400);
            }

            function selectPermission(id, description, name, conditionsAllowed) {
                var args = window.dialogArguments || {};
                args.id = id;
                args.name = name;
                args.permissionDescription = description;
                args.conditionsAllowed = conditionsAllowed;
                args.OK = true;
                onClosePopup(args);
            }
        </script>
        <table>
            <tr>
                <td class="FieldLabel">
                    <asp:DataGrid runat="server" id="m_dgPermissionTypes" Width="95%" AutoGenerateColumns="false">
                        <alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
						<itemstyle cssclass="GridItem"></itemstyle>
						<headerstyle cssclass="GridHeader"></headerstyle>
						<Columns>
                            <asp:TemplateColumn HeaderText="Permission">
						        <ItemTemplate>
						            <a href="#" onclick="selectPermission(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Id").ToString()) %>, 
						            <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Description").ToString()) %>, 
						            <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>,
						            <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "IsAppliesToConditions").ToString()) %>);">
						                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>
						            </a>
						        </ItemTemplate>
						    </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Description" HeaderText="Description" />
                            <asp:TemplateColumn HeaderText="Can be<br />Condition">
						        <ItemTemplate>
									<%# AspxTools.HtmlString(Boolean.Parse(DataBinder.Eval(Container.DataItem, "IsAppliesToConditions").ToString()) ? "yes" : "no")%>
						        </ItemTemplate>
						    </asp:TemplateColumn>
						</Columns>
                    </asp:DataGrid>
                </td>
            </tr>
		    <tr>
		        <td align="center">
		            <input type="button" id="m_Cancel" value="Cancel" onclick="onClosePopup();" />
		        </td>
		    </tr>
		</table>
    </form>
</body>
</html>
