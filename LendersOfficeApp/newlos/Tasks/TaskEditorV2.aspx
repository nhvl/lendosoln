﻿
<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="TaskEditorV2.aspx.cs" Inherits="LendersOfficeApp.newlos.Tasks.TaskEditorV2" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Task Editor</title>
    <style type="text/css">
        .SubHeader
        {
            padding: 3px;
            text-align: right;
            font-weight: bold;
        }
        .hidden, #reactivateWarning { display: none; }

        .lqbpopup-content-container
        {
            background-color: gainsboro;
            padding: 5px;
        }

        .condition-upload-complete .lqbpopup-content-container
        {
            background-color: #FFF;
        }

        #CommentsRow {
            vertical-align: top;
        }

        #CommentsRow textarea {
            width: 95%;
        }

        .InsetBorder td.comment-spacer {
            padding-top: 22px;
        }

        #CalculationOverlay {
            position: absolute;
            left: 0;
            top: 0; 
            height: 100%;
            width: 100%;
            background-color: black;
            opacity: 0;
            z-index: 9999998;
        }

        #CalculationOverlay.darkened {
            opacity: .3;
        }

        .document-section {
            border-top: 1px solid black;
        }

        .associated-doc-row span {
            margin-left: 2.7em;
        }

        .associated-doc-row .unlink-link + span {
            margin-left: 0;
        }

        .associated-doc-section {
            text-align: center;
        }

        .associated-doc-link-container {
            text-align: center;
        }
    </style>
</head>
<body bgcolor="gainsboro">
	<script type="text/javascript">
        var taskIsDirty = false;
        var viewAssociatedDocsUrl = "/newlos/ElectronicDocs/EditEDocBatch.aspx";
	    
	    function _init() {
	        if (<%= AspxTools.HtmlString((Page.IsPostBack == true).ToString()).ToLower() %>) {
	            setTaskDirty();
	        }
	        resizeForIE6And7(850, 600);
	        checkDueDateStatus();
            setOwnerPermissionWarningStatus();

            $('.comment-cell').toggle(ML.IsCommentCellVisible);
            $('#CommentContainer').attr('colspan', ML.IsCommentCellVisible && !ML.IsDocAssociationCellVisible ? '6' : '4');
            $('#DocSatisfiedCellSpacer').toggle(!ML.IsCommentCellVisible && ML.IsDocAssociationCellVisible);
            $('#DocSatisfiedCell, .document-section').toggle(ML.IsDocAssociationCellVisible);
            
            $(".view-docs-link").click(viewAssociatedDocs);
            $(".unlink-link").click(unlinkAssociatedDoc);
	        
	        var emailBody = document.getElementById('EmailBody');
	        if (emailBody) {
	            emailBody.cursorPos = 0;
	        }
	        
            document.getElementById('Subject').focus();

            if (ML.ShowReactivateWarning) {
                LQBPopup.ShowElement($j('#reactivateWarning'), {'width': 325 , 'height':150 , 'hideCloseButton': true });
            }

            var dragDropSettings = {
                compact: true,
                onProcessAllFilesCallback: uploadEdoc,
                blockUpload: !ML.CanUploadEdocs,
                blockUploadMsg: ML.UploadEdocsFailureMessage,
                hideFilesInFileListParam: true
            };
            registerDragDropUpload(document.getElementById('DragDropZone'), dragDropSettings);
	    }
	    
	    function attemptTaskListRefresh() {	        
            if(window.opener != null && !window.opener.closed)
            {
                if (window.opener.document.forms[0].id &&
                    window.opener.document.forms[0].id != 'loanfind')
                window.opener.document.forms[0].submit();                
            }
        }

        function viewAssociatedDocs() {
            var $el = $(this);
            var $td = $el.parent().parent();
            var $row = $td.parent();
            var taskId = $row.attr("TaskId");
            var docIds = $td.find(".unlink-link").map(function () {
                return $(this).attr("docid");
            }).get();

            var data = {
                ID: JSON.stringify(docIds)
            }

            gService.edoc.callAsyncSimple('BatchStore', data, function (result) {
                if (result.error) {
                    alert(result.UserMessage);
                    return;
                }

                var args = {
                    loanid: ML.sLId,
                    key: result.value.key
                };
                var body_url = viewAssociatedDocsUrl + "?" + f_generateQueryString(args);
                window.open(gVirtualRoot + body_url, "Associated Doc Viewer", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=950,height=600");
            });
        }

        function unlinkAssociatedDoc() {
            var $el = $(this);
            var $row = $el.parent().parent().parent().parent();
            var taskId = <%=AspxTools.JsString(TaskID)%>;
            var docId = $el.attr("docid");

            var args = {
                TaskId: taskId,
                DocumentId: docId,
                LoanId: ML.sLId
            }

            gService.edoc.callAsyncSimple('UnlinkDoc', args, function (result) {
                if (result.error) {
                    alert(result.UserMessage);
                    return;
                }

                $el.parent().remove();
                if ($row.find(".unlink-link").length == 0) {
                    $row.find(".view-docs-link").remove();
                }

                
                docTypePickerChange();
            });
        }

        function documentUploadPostAssociationCallback() {
            window.location.href = window.location.href;
        }
	    
	    function pickEdoc() {
	        showModal('/newlos/ElectronicDocs/EdocPicker.aspx?mode=persist&loanid=' + ML.sLId + '&taskid=' + <%=AspxTools.JsString(TaskID)%>, null, null, null, function(windowResult){ 
				docTypePickerChange();
			}, {hideCloseButton: true} );
	    }
	    
        function uploadEdoc() {
            if (getFilesCount('DragDropZone') === 0) {
                // No files selected by the user, e.g.
                // the file types selected were invalid.
                return;
            }

            var dropzone = document.getElementById('DragDropZone');
            var taskId = <%=AspxTools.JsString(TaskID)%>;
            ConditionDocumentUploadHelper.UploadDocs(dropzone, ML.sLId, taskId, false/*isPml*/);
	    }
	    
	    function emailBodyGetCursorPosition(ctrl) { // this is bound to the control by the codebehind
	        // TODO: see case 68089
	    }
	    
	    function emailBodySetCursorPosition(ctrl) { // this is bound to the control by the codebehind
            if (ctrl.setSelectionRange) { // Firefox support
                ctrl.setSelectionRange(ctrl.cursorPos,ctrl.cursorPos);
            } else if (ctrl.createTextRange) { // IE support
                var range = ctrl.createTextRange();
                range.collapse(ctrl);
                range.moveEnd('character', ctrl.cursorPos);
                range.moveStart('character', ctrl.cursorPos);
                range.select();
            }
            
            return true;
	    }
	    
	    function calculationPopup() {
	        var offset = document.getElementById("DueDateOffset");
	        var field = document.getElementById("DueDateCalcField");
	        showModal("/newlos/Tasks/DateCalculator.aspx?loanid=" + <%=AspxTools.JsString(LoanID)%> + "&duration=" + offset.value + "&fieldId=" + field.value, null, null, null, function(args){
				if (args.OK) {
					offset.value = args.offset;
					field.value = args.id;
					document.getElementById("DueDateCalcDescription").innerText = args.dateDescription;
					document.getElementById("DueDate").value = args.date;
					document.getElementById("DueDateLocked").checked = false;
					checkDueDateStatus();
					setTaskDirty();
				}
			 }, {hideCloseButton: true} );
	    }
	    
	    function permissionPopup() {
	        var IsCondition = document.getElementById('IsCondition').checked;
	        showModal("/newlos/Tasks/TaskPermissionPicker.aspx?IsCond=" + IsCondition + '&IsTemplate=' + <%=AspxTools.JsString(IsTemplate.ToString())%>, null, null, null, function(args){ 
				if (args.OK) {
					var IsCondCb = document.getElementById("IsCondition");
					document.getElementById("PermissionDescription").value = args.permissionDescription;
					document.getElementById("PermissionLevelID").value = args.id;
					if (!IsCondCb.checked) {
						IsCondCb.disabled = args.conditionsAllowed != "True";
					}
					document.getElementById("TaskPermission").innerText = args.name;
					
					setOwnerPermissionWarningStatus();
					setTaskDirty();
				}
			}, {hideCloseButton: true} );
	    }
	    
	    function setOwnerPermissionWarningStatus() {
	        if(document.getElementById('OwnerID').value != <%= AspxTools.JsString(BrokerUser.UserId) %>
	          && document.getElementById('OwnerID').value != '00000000-0000-0000-0000-000000000000'
	          && <%=AspxTools.JsString(IsTemplate.ToString())%> != 'True') {
	            var input = new Object();
                input["ownerID"] = document.getElementById('OwnerID').value;
                input["permissionLevelID"] = document.getElementById('PermissionLevelID').value;
                var result = gService.loanedit.call("CheckOwnerPermission", input);
                
                if (!result.error && result.value["hideError"] == <%=AspxTools.JsString(false.ToString())%>) {
                    var owner = document.getElementById('TaskOwner').innerText;
                    document.getElementById('ownerPermissionWarning').innerText = 'Note: ' +
                        owner + ' can manage this task because he/she owns it. ' + owner + 
                        ' would not otherwise be able to manage tasks in this task permission level.';
                    return false;
                }
	        }
	        
            document.getElementById('ownerPermissionWarning').innerText = '';
	    }

	    function takeOwnership() {
	        var owner = <%= AspxTools.JsString(BrokerUser.FirstName + ' ' + BrokerUser.LastName) %>;
	        var ownerId = <%= AspxTools.JsString(BrokerUser.UserId) %>;
	        document.getElementById('OwnerID').value = ownerId;
	        document.getElementById('OwnerRole').value = <%=AspxTools.JsString(Guid.Empty)%>;
	        document.getElementById('TaskOwner').innerText = owner;
	        document.getElementById('takeOwnershipLink').style.display = 'none';
	        if (<%=AspxTools.JsString(Mode)%> == 'resolve') {
	            document.getElementById('AssignedOwner').innerText = owner;
	        }
	        if (document.getElementById('StatusResolved').value == 'True') {
	            document.getElementById('Assigned').innerText = owner;
	            document.getElementById('AssignedID').value = ownerId;
	            document.getElementById('AssignedRole').value = <%=AspxTools.JsString(Guid.Empty)%>;
	        }
	        setOwnerPermissionWarningStatus();
	        setTaskDirty();
	        return false;
	    }

	    function displayConditionFields() {
	        var checked = document.getElementById('IsCondition').checked;
	        var display = checked ? '' : 'none';
	        document.getElementById('ConditionRow1').style.display = display;
	        document.getElementById('ConditionRow2').style.display = display;
	        document.getElementById('ConditionRow3').style.display = display;
	        document.getElementById('ConditionRow4').style.display = display;
	        
	        $('td.ReqDocType').toggle(checked);
	    }

	    function addAddressFromContact(tbName) {
			var rolodex = new cRolodex();
	        rolodex.chooseFromRolodex(19, <%=AspxTools.JsString(LoanID)%>, false, false, function(args){
				if (!args.OK) {
					return;
				}
				var addressTextbox = document.getElementById(tbName);
				if (addressTextbox.value != '') {
					addressTextbox.value += ', ';
				}
				addressTextbox.value += args.AgentEmail;
				setTaskDirty();
			});
			
	        return false;
	    }
	    
	    function checkDueDateStatus() {
	        var dueDate = document.getElementById("DueDate");
	        if (document.getElementById("DueDateLocked").checked) {
	            document.getElementById("DueDateCalcDescription").innerText = '';
	            document.getElementById('DueDateOffset').value = '';
	            document.getElementById('DueDateCalcField').value = '';
	            dueDate.readOnly = document.getElementById("DueDateLocked").disabled;
	        }
	        else {
	            dueDate.readOnly = true;
	        }
	        dueDate.nextSibling.style.display = dueDate.readOnly ? 'none' : '';
	        updateEmailSubject();
	    }
	    
	    function clearManualDate() {
	        if (!document.getElementById("DueDateLocked").checked) {
	            document.getElementById("DueDate").value = '';
	        }
	    }
	    
	    function changeAssigned(mode) {
	        var permLevel = document.getElementById('PermissionLevelID').value;
	        var owner = mode == 'templateOwner';
	        if (mode == 'taskAssign') {
	            var id = document.getElementById("AssignedID");
	            var role = document.getElementById("AssignedRole");
	            var label = document.getElementById("Assigned");	            
	        }
	        else if (mode == 'templateOwner') {
	            var id = document.getElementById("OwnerID");
	            var role = document.getElementById("OwnerRole");
	            var label = document.getElementById("TaskTemplateOwner");	            
	        }
	        else {
	            var id = document.getElementById("AssignedID");
	            var role = document.getElementById("AssignedRole");
	            var label = document.getElementById("TaskTemplateAssigned");	            
	        }
	        var NoPML = false;
	        var hiddenCb = document.getElementById('ConditionIsHidden');
	        if(document.getElementById("IsCondition").checked && hiddenCb != null) {
	            NoPML = hiddenCb.checked;
	        }
	        showModal("/newlos/Tasks/RoleAndUserPicker.aspx?loanid=" + <%=AspxTools.JsString(LoanID)%> + "&PermLevel=" + permLevel + '&IsTemplate=' + <%=AspxTools.JsString(IsTemplate.ToString())%> + '&Owner=' + owner + '&NoPML=' + NoPML, null, null, null, function(args){ 
				if (args.OK) {
					if (args.type == 'user') {
						id.value = args.id;
						role.value = '00000000-0000-0000-0000-000000000000';
						label.innerText = args.name;
						if (hiddenCb != null) {
							if (args.pmlUser == 'True') {
								hiddenCb.checked = false;
								hiddenCb.disabled = true;
							} else {
								hiddenCb.disabled = false;	                    
							}
						}
					}
					else {
						role.value = args.id;
						id.value = '00000000-0000-0000-0000-000000000000';
						label.innerText = args.name;
					}
					setTaskDirty();
				}
			}, {hideCloseButton: true} );
	    }
	    
	    function updateEmailSubject() {
	        var subjectLabel = document.getElementById('TaskEmailSubject');
	        if (document.getElementById('TaskEmailSubject') == null) {
	            return;
	        }
	        
	        var prefix = '';
	        if (<%=AspxTools.JsString(Mode)%> == 'reply') {
	            prefix = 'RE: ';
	        }
	        if (<%=AspxTools.JsString(Mode)%> == 'forward') {
	            prefix = 'FWD: ';
	        }
	        var taskId = <%=AspxTools.JsString(TaskID)%>;
	        var ccode = <%=AspxTools.JsString(brokerDB.CustomerCode)%>;
	        var sId = document.getElementById('sLNm').innerText;
	        var brNm = document.getElementById('BorrowerName').innerText;
	        var dueDate = document.getElementById("DueDate").value != '' ? document.getElementById('DueDate').value : document.getElementById('DueDateCalcDescription').innerText;
	        var subject = document.getElementById('Subject').value;
	        var emailSubject = prefix + 'Task ' + ccode + '_' + taskId + ' Due: ' + dueDate + ' - ' + sId + ' - ' + brNm + ' - ' + subject;	        
	        subjectLabel.innerText = emailSubject;
	        document.getElementById('EmailSubject').value = emailSubject;
	    }
	    
	    function disabledSubmitButtons(act) {
	        if (act === 0 || act === 1) {
	            var resolveAction = $('#AccessLevel').val();
	            var satisfied = $('#DocRequestSatisfied').text();
	            if ((resolveAction === 'WorkOn' || resolveAction === 'Close') && satisfied === 'No') {
	                alert('This condition is missing an association with a required document type. Please associate with the required document type in order to resolve.');
	                return false;
	            } else if (resolveAction === 'Manage' && satisfied === 'No') {
	                if (!confirm('This condition is missing an association with a required document type. Are you sure you want to resolve?'))
	                {
	                    return false;
	                }
	            }
                
                var isResolutionBlocked = $('#IsResolutionBlocked').val() === 'True';
                var resolutionDenialMessage = $('#ResolutionDenialMessage').val();
                if (isResolutionBlocked) {
                    alert(resolutionDenialMessage);
                    return false;
                }
            }
	        disableButton('m_Close');
	        disableButton('m_Resolve');
	        disableButton('m_Save');
	        disableButton('m_Cancel');
	        return true;
	    }
	    
	    function disableButton(id) {
	        if (document.getElementById(id) != null) {
	            document.getElementById(id).disabled = true;
	        }
	    }
	    
	    function setTaskDirty() {
	        taskIsDirty = true;
	    }
	    
	    function confirmClose() {
            if (!taskIsDirty || confirm('Close task editor? Any unsaved changes will be lost.')) {	        
	            <% if(IsNewTask) { %>
	                onClosePopup();
	            <% } else { %>
    	            var url = "TaskViewer.aspx?taskId=<%=AspxTools.HtmlString(TaskID.ToString())%>&loanid=<%=AspxTools.HtmlString(LoanID.ToString())%>&IsTemplate=<%=AspxTools.HtmlString(IsTemplate.ToString())%>";
	                self.location = url;
	            <% } %>
	        }
	    }
	    
	    function closePage(taskId) { // This is called by the codebehind in a ClientScriptBlock
	        var args = {};
            if (typeof(args) !== 'undefined') {
                args.taskId = taskId;
                args.OK = true;
            }
            attemptTaskListRefresh();
            onClosePopup(args);
	    }
	    
	    function docTypePickerChange(){
	        setTaskDirty();
	        getLatestAssociationStatus();
	    }
	    
	    function getLatestAssociationStatus() {
	        var data = { 
	            taskid : <%=AspxTools.JsString(TaskID)%>, 
	            newDocTypeId : $('#<%= AspxTools.ClientId(DocTypePicker.ValueCtrl) %>').val()
	        };
	        
	        callWebMethodAsync({
	            type: 'POST',
	            contentType: 'application/json; charset=utf-8',
	            url: 'TaskEditorV2.aspx/GetTaskAssociationStatus',
	            data: JSON.stringify(data),
	            dataType: 'json',
	            async: false
	        }).then(
				function(d) {
	                var color = 'black';
	                if (d.d.Status === 'Yes') {
	                    color = 'green';
	                }
	                else if ( d.d.Status === 'No') {
	                    color = 'red';
	                }
	                
	                $('#DocRequestSatisfied').text(d.d.Status).css('color', color);
	            },
				function() {
	                var color = 'red';
	                $('#DocRequestSatisfied').text('Error').css('color', color);
	            }
			);
	    }
        
        function clearResolutionBlockTrigger() {
            if (confirm('Are you sure you want to clear the resolution block trigger from this task?')) {
                setTaskDirty();
                $('#ResolutionBlockTrigger').text('None');
                $('#ResolutionBlockTriggerName').val('');
                $('.HideOnResolutionBlockTriggerClear').hide();
            }
        }
        
        function clearResolutionDateSetter() {
            if (confirm('Are you sure you want to clear the resolution date setter from this task?')) {
                setTaskDirty();
                $('#ResolutionDateSetterFieldIdLabel').text('None');
                $('#ResolutionDateSetterFieldId').val('');
                $('.HideOnResolutionDateSetterClear').hide();
            }
        }
    </script>
    <form id="form1" runat="server">
        <asp:HiddenField ID="AssignedRole" runat="server" Value="00000000-0000-0000-0000-000000000000" />
        <asp:HiddenField ID="AssignedID" runat="server" Value="00000000-0000-0000-0000-000000000000" />
        <asp:HiddenField ID="OwnerRole" runat="server" Value="00000000-0000-0000-0000-000000000000" />
        <asp:HiddenField ID="OwnerID" runat="server" Value="00000000-0000-0000-0000-000000000000" />
        <asp:HiddenField ID="DueDateOffset" runat="server" />
        <asp:HiddenField ID="DueDateCalcField" runat="server" />
        <asp:HiddenField ID="PermissionLevelID" runat="server" />
        <asp:HiddenField ID="PermissionDescription" runat="server" />
        <asp:HiddenField ID="EmailSubject" runat="server" />
        <asp:HiddenField ID="StatusResolved" runat="server" />
        <asp:HiddenField ID="SavedVersion" runat="server" />
        <asp:HiddenField ID="ResolveAction" runat="server" />
        <asp:HiddenField ID="IsResolutionBlocked" runat="server" />
        <asp:HiddenField ID="ResolutionDenialMessage" runat="server" />
        <asp:HiddenField ID="ResolutionBlockTriggerName" runat="server" />
        <asp:HiddenField ID="ResolutionDateSetterFieldId" runat="server" />
        <asp:HiddenField ID="IsAutoResolveTriggerTrue" runat="server" />
        <asp:HiddenField ID="IsAutoCloseTriggerTrue" runat="server" />
        <div id='CalculationOverlay' class="Hidden"></div>
        <div class="MainRightHeader">
	        <ml:EncodedLabel ID="m_Header" runat="server"></ml:EncodedLabel>
	    </div>
	    <div class="InsetBorder">
	        <table cellSpacing="0" cellPadding="0"  border="0">
		        <tr>
		            <td class="SubHeader" valign="top">
		                Subject
		                <img id="SubjectR" src="~/images/error_icon_right.gif" alt="Required" visible="false" runat="server" />
		            </td>
		            <td colspan="6">
		                <asp:TextBox ID="Subject" Width="500px" runat="server" TextMode="MultiLine" Rows="3" onchange="updateEmailSubject();setTaskDirty();"></asp:TextBox>
		            </td>
		        </tr>
		        <tr id="TaskRow" runat="server">
		            <td class="SubHeader">
		                Assigned to
		            </td>
		            <td colspan="2">
		                <ml:EncodedLabel ID="Assigned" runat="server"></ml:EncodedLabel>
			            <ml:EncodedLiteral ID="AssignedPointer" runat="server"></ml:EncodedLiteral>
		                <ml:EncodedLabel ID="AssignedOwner" runat="server"></ml:EncodedLabel>
			            <a id="changeTaskAssignmentLink" runat="server" href="#" onclick="changeAssigned('taskAssign');">change</a>
		            </td>
		            <td class="SubHeader">
		                Borrower
		            </td>
		            <td>
		                <ml:EncodedLabel ID="BorrowerName" runat="server"></ml:EncodedLabel>
		            </td>
		            <td class="SubHeader">
		                Loan Number
		            </td>
		            <td>
		                <ml:EncodedLabel ID="sLNm" runat="server"></ml:EncodedLabel>
		            </td>
		        </tr>
		        <tr id="ExistingTaskRow" runat="server">
		            <td class="SubHeader">
		                Status
		            </td>
		            <td colspan="4">
		                <ml:EncodedLiteral ID="Status" runat="server"></ml:EncodedLiteral>
		                <ml:EncodedLiteral ID="StatusNext" runat="server"></ml:EncodedLiteral>
		            </td>
		            <td class="SubHeader">
		                Task Owner
		            </td>
		            <td>
		                <ml:EncodedLabel ID="TaskOwner" runat="server"></ml:EncodedLabel>
		                <a id="takeOwnershipLink" href="#" onclick="takeOwnership();" runat="server">take ownership</a>
		            </td>
		        </tr>
		        <tr id="TemplateRow" runat="server">
		            <td class="SubHeader" nowrap>
		                To Be Assigned To
		            </td>
		            <td colspan="4">
		                <ml:EncodedLabel ID="TaskTemplateAssigned" runat="server"></ml:EncodedLabel>
			            <a href="#" onclick="changeAssigned('templateAssign');">change</a>
		            </td>
		            <td class="SubHeader">
		                To Be Owned By
		            </td>
		            <td>
		                <ml:EncodedLabel ID="TaskTemplateOwner" runat="server"></ml:EncodedLabel>&nbsp;
			            <a href="#" onclick="changeAssigned('templateOwner');">change</a>
		            </td>
		        </tr>
                <tr id="ResolutionSettingsRow" runat="server">
                    <td class="SubHeader" nowrap>
                        Resolution Block Trigger
                    </td>
                    <td colspan="4">
                        <ml:EncodedLabel ID="ResolutionBlockTrigger" runat="server"></ml:EncodedLabel>
                        <a href="#" id="DisplayResolutionDenialMessageLink" runat="server" onclick="alert(document.getElementById('ResolutionDenialMessage').value);return false;" class="HideOnResolutionBlockTriggerClear">?</a>                    
                        <a href="#" id="ClearResolutionBlockTrigger" runat="server" onclick="clearResolutionBlockTrigger();" class="HideOnResolutionBlockTriggerClear">clear</a>
                    </td>
                    <td class="SubHeader">
                        Resolution Date Setter
                    </td>
                    <td>
                        <ml:EncodedLabel ID="ResolutionDateSetterFieldIdLabel" runat="server"></ml:EncodedLabel>&nbsp;
                        <a href="#" id="ClearResolutionDateSetter" runat="server" onclick="clearResolutionDateSetter();" class="HideOnResolutionDateSetterClear">clear</a>
                    </td>
                </tr>
                <tr id="AutoTriggersRow" runat="server">
                    <td class="SubHeader" nowrap>
                        Auto-Resolve Trigger
                    </td>
                    <td colspan="4">
                        <ml:EncodedLabel ID="AutoResolveTriggerLabel" runat="server"></ml:EncodedLabel>
                    </td>

                    <td class="SubHeader">
                        Auto-Close Trigger
                    </td>
                    <td>
                        <ml:EncodedLabel ID="AutoCloseTriggerLabel" runat="server"></ml:EncodedLabel>
                    </td>
                </tr>
		        <tr>
		            <td class="SubHeader">
		                Due Date
		                <img id="DueDateR" src="~/images/error_icon_right.gif" alt="Required" visible="false" runat="server" />
                    </td>
                    <td style="width:3%">
                        <asp:CheckBox ID="DueDateLocked" runat="server" onclick="checkDueDateStatus();setTaskDirty();clearManualDate();" />
                    </td>
                    <td colspan="3" nowrap>
			            <ml:datetextbox ID="DueDate" runat="server" preset="date" CssClass="mask" Width="60" onchange="updateEmailSubject();setTaskDirty();"></ml:datetextbox>                    
			            <a id="dueDateCalculation" runat="server" href="#" onclick="calculationPopup();">calculate</a>
			            <ml:EncodedLabel ID="DueDateCalcDescription" runat="server"></ml:EncodedLabel>
                    </td>
                    <td class="SubHeader">
                        Task Permission
                    </td>
                    <td>
		                <ml:EncodedLabel ID="TaskPermission" runat="server"></ml:EncodedLabel>
			            <a href="#" onclick="alert(document.getElementById('PermissionDescription').value);return false;">?</a>                    
			            <a id="changePermissionLevel" runat="server" href="#" onclick="permissionPopup();">change</a>
                    </td>
		        </tr>
		        <tr id="FollowUpDateRow" runat="server">
		            <td class="SubHeader">
		                Follow-up Date
		            </td>
		            <td style="width:10px">
		            </td>
		            <td>
			            <ml:datetextbox ID="FollowupDate" onchange="setTaskDirty();" runat="server" preset="date" CssClass="mask" Width="60"></ml:datetextbox>                    
		            </td>
		        </tr>
		        <tr id="ConditionRow1" runat="server">
		            <td colspan="7">
		                <hr />
		            </td>
		        </tr>
		        <tr id="ConditionRow5" runat="server">
		            <td class="SubHeader">
		                Is a Condition
		            </td>
		            <td >
                        <input id="IsCondition" type="checkbox" runat="server" onclick="displayConditionFields();setTaskDirty();" NoHighlight />
		            </td>
		            <td></td>
                    <td class="SubHeader ReqDocType" runat="server" id="RequiredDocTypeLabel">
                        Required Document Type
                    </td>
                    <td colspan="3" class="ReqDocType" runat="server" id="RequiredDocTypePicker" >
                        <uc:DocTypePicker runat="server" onchange="docTypePickerChange();" ID="DocTypePicker" Width="200px"></uc:DocTypePicker>
                    </td>
		        </tr>
		        <tr id="ConditionRow2" runat="server">
		            <td class="SubHeader">
		                Category
		            </td>
		            <td colspan="6" class="FieldLabel">
		                <asp:DropDownList ID="ConditionCategories" onchange="setTaskDirty();" runat="server"></asp:DropDownList>
		                <label ID="IsHiddenLabel" runat="server">
                            Hidden
		                    <asp:CheckBox ID="ConditionIsHidden" onclick="setTaskDirty();" runat="server" />
		                </label>
		            </td>
		        </tr>
		        <tr id="ConditionRow3" runat="server">
		            <td class="SubHeader" valign="top">
		                Internal Notes
		            </td>
		            <td colspan="6">
		                <asp:TextBox ID="ConditionInternalNotes" onchange="setTaskDirty();" TextMode="MultiLine" Rows="6" Width="500px" runat="server"></asp:TextBox>
		            </td>
		        </tr>
		        <tr id="ConditionRow4" runat="server">
		            <td colspan="7">
		                <hr />
		            </td>
		        </tr>
		        <tr id="EmailRow1" runat="server" visible="false">
		            <td class="SubHeader">
		                From
		            </td>
		            <td colspan="6">
		                <ml:EncodedLiteral ID="FromAddress" runat="server"></ml:EncodedLiteral>
		            </td>
		        </tr>
		        <tr id="EmailRow2" runat="server" visible="false">
		            <td class="SubHeader">
		                To
		                <img id="ToAddressR" src="~/images/error_icon_right.gif" alt="Required" visible="false" runat="server" />
		            </td>
		            <td colspan="6">
		                <asp:TextBox ID="ToAddress" Width="400px" runat="server" onchange="setTaskDirty();"></asp:TextBox>
		                <a href="#" id="m_addForToAddress" runat="server" onclick="addAddressFromContact('ToAddress');">add</a>
		            </td>
		        </tr>
		        <tr id="EmailRow3" runat="server" visible="false">
		            <td class="SubHeader">
		                CC
		            </td>
		            <td colspan="6">
		                <asp:TextBox ID="CCAddress" Width="400px" runat="server" onchange="setTaskDirty();"></asp:TextBox>
		                <a href="#" id="m_addForCCAddress" runat="server" onclick="addAddressFromContact('CCAddress');">add</a>
		            </td>
		        </tr>
		        <tr id="EmailRow4" runat="server" visible="false">
		            <td class="SubHeader">
		                E-mail Subject
		            </td>
		            <td colspan="6">
		                <span id="TaskEmailSubject"></span>
		            </td>
		        </tr>
		        <tr id="EmailRow5" runat="server" visible="false">
		            <td class="SubHeader">
		                Body
		            </td>
		            <td colspan="6">
		                <asp:TextBox ID="EmailBody" TextMode="MultiLine" Rows="6" Width="500px" runat="server" onchange="setTaskDirty();"></asp:TextBox>
		            </td>
		        </tr>
		        <tr id="CommentsRow" runat="server">
		            <td class="SubHeader comment-spacer comment-cell" valign="top">
		                Comments
		            </td>
		            <td id="CommentContainer" colspan="4" class="comment-spacer comment-cell">
		                <asp:TextBox ID="m_Comments" TextMode="MultiLine" Rows="6" runat="server" onchange="setTaskDirty();"></asp:TextBox>
		            </td>
                    <td id="DocSatisfiedCellSpacer" colspan="5">&nbsp;</td>
                    <td id="DocSatisfiedCell" colspan="2">
                        <span class="FieldLabel">Document requirement satisfied? </span>
		        
		                <ml:EncodedLabel runat="server" ID="DocRequestSatisfied"></ml:EncodedLabel> 
		        
		                <a href="#" onclick="return pickEdoc()">associate doc</a>
                        <div class="InsetBorder full-width">
                            <div id="DragDropZone" class="drag-drop-container" runat="server"></div>
                        </div>
                        <div class="drag-drop-error-div"></div>
                    </td>
		        </tr>
                <tr runat="server" class="document-section" id="DocumentSection" visible="false">
                    <td colspan="5">&nbsp;</td>
	                <td colspan="2">
                        <asp:Repeater runat="server" ID="AssociatedDocRepeater" OnItemDataBound="AssociatedDocRepeater_ItemDataBound">
		                    <ItemTemplate>
			                    <div class="associated-doc-row">
				                    <a runat="server" class="unlink-link" id="UnlinkLink">unlink</a>
				                    <span runat="server" id="AssociatedDoc"></span>
			                    </div>
		                    </ItemTemplate>
	                    </asp:Repeater>
	                    <div runat="server" id="AssociatedDocLinkContainer" class="associated-doc-link-container">
		                    <a class="view-docs-link">view associated docs</a>
	                    </div>
	                </td>
                </tr>
		        <tr>
		            <td colspan="7">
		                <ml:EncodedLabel ID="m_errorMessage" runat="server" ForeColor="Red"></ml:EncodedLabel>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="7" align="center">
		                <asp:Button ID="m_Close" Text="Resolve & Close" runat="server" Visible="false" OnCommand="OnSaveClicked" CommandArgument="CloseTask" Width="100px" UseSubmitBehavior="false" OnClientClick="if(!disabledSubmitButtons(0)){ return false; }" />
		                <asp:Button ID="m_Resolve" Text="Resolve" runat="server" Visible="false" OnCommand="OnSaveClicked" CommandArgument="SaveTask" Width="100px" UseSubmitBehavior="false" OnClientClick="if(!disabledSubmitButtons(1)){ return false; }" />
		                <asp:Button ID="m_Save" Text="OK" runat="server" OnCommand="OnSaveClicked" CommandArgument="SaveTask" Width="100px" UseSubmitBehavior="false" OnClientClick="disabledSubmitButtons();" />
		                <input id="m_Cancel" type="button" value="Cancel" onclick="confirmClose();" style="width:100px" />
		            </td>
		        </tr>
		        <tr style="color:Red">
		            <td id="ownerPermissionWarning" colspan="7">
		                Note: The current task owner would not normally be allowed to manage this task under the current permission level.
		            </td>
		        </tr>
		    </table>
	    </div>
        <div id="HistoryDiv" runat="server" class="InsetBorder">
            <ml:PassthroughLiteral ID="History" runat="server"></ml:PassthroughLiteral>
        </div>
        <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg> 

        <div id="reactivateWarning">
            <h4 class="page-header"><ml:EncodedLiteral id="warningHeaderLiteral" runat="server" /> Trigger Still True</h4>

            <div class="align-left">
                The <ml:EncodedLiteral id="warningLiteral1" runat="server" /> trigger for this task is still true.
                Reactivating it now will cause it to <ml:EncodedLiteral id="warningLiteral2" runat="server" /> again.
            </div>

            <div class="align-left PaddingTop">
                Do you want to remove the <ml:EncodedLiteral id="warningLiteral3" runat="server" /> trigger to prevent this from happening?
            </div>

            <div class="PaddingTop">
                <asp:Button ID="btnRemoveAndReactivate" Text="Remove trigger and re-activate the task." runat="server" OnCommand="OnSaveClicked" CommandArgument="RemoveAndReactivate" UseSubmitBehavior="false" />
            </div>

            <div class="PaddingTop">
                <input id="btnWarningCancel" type="button" value="Cancel" onclick="LQBPopup.Hide();" />
            </div>
        </div>
    </form>
</body>
</html>
