﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DateCalculator.aspx.cs" Inherits="LendersOfficeApp.newlos.Tasks.DateCalculator" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Date Calculator</title>
	<link href="~/css/stylesheet.css" type="text/css" rel="stylesheet" />
	<link href="~/css/stylesheetnew.css" type="text/css" rel="stylesheet" />
	<style>
	    .selected
	    {
	        background-color: #316AC5;
	        color: white
	    }
	    .hidden
	    {
	        display: none;
	    }
        .w-130 { width: 130px; }
	</style>
</head>
<body bgcolor="gainsboro">
    <h4 class="page-header"><ml:EncodedLiteral ID="HeaderText" runat="server" Text="Calculate the due date based on a loan date:"></ml:EncodedLiteral></h4>
    <form id="form1" runat="server">
        <script type="text/javascript">
            function _init() {
                <% if (this.IsSimpleDatePicker) { %>
                resizeForIE6And7(470, 325);
                <% } else { %>
                resizeForIE6And7(470, 400);
                <% } %>

                $('#m_duration').keypress(function(event) {
                    var validRegex = /^[0123456789]$/i;
                    if (!validRegex.test(String.fromCharCode(event.which))) {
                        return false;
                    }
                    return true;
                });

                selectField(<%= AspxTools.JsString(id) %>, <%= AspxTools.JsString(friendlyName) %>);
            }

            function verifyInput(event) {
                var validRegex = /^[-]$/i;
                if (!validRegex.test(String.fromCharCode(event.which))) {
                    return false;
                }
                return true;
            }

            function displaySearchResults() {
                var search = $('#searchCriteria').val().toLowerCase();
                var list = $('#m_FieldList div');
                var previousHeader = -1;
                var headerHasVisibleChildren = false;

                for (var i = 0; i < list.length; i++) {
                    if (list[i].getAttribute('isHeader') == 'true') {
                        setVisibility(list, previousHeader, headerHasVisibleChildren);
                        previousHeader = i;
                        headerHasVisibleChildren = false;
                        continue;
                    }

                    var text = list[i].innerText.toLowerCase();
                    if (text.indexOf(search) != -1) {
                        list[i].style.display = '';
                        headerHasVisibleChildren = true;
                    }
                    else {
                        list[i].style.display = 'none';
                    }
                }
                setVisibility(list, previousHeader, headerHasVisibleChildren);
            }

            function setVisibility(list, pos, visibility) {
                if (pos > -1) {
                    list[pos].style.display = visibility ? '' : 'none';
                }
            }

            function selectField(id, name) {
                var div;
                var list = document.getElementById('m_FieldList').children;
                for (var i = 0; i < list.length; i++) {
                    var node = list[i];

                    if (node.className.indexOf('selected') >= 0) {
                        node.className = node.className.replace("selected", "");
                    }

                    if (node.innerText.indexOf(name) >= 0 &&  node.innerText.indexOf(id) >= 0) {
                        div = list[i];
                    }
                }
                $('#m_selectedFieldId').val(id);
                $('#m_selectedFieldName').text(name);
                div.className = 'selected';
            }

            function confirmCancel() {
                if (($('#m_selectedFieldId').val() == '' && $('#m_duration').val() == '') || confirm('Close window?')) {
                    onClosePopup();
                }
            }

            function returnSelection() {
                var field = $('#m_selectedFieldId').val();
                if (field == '') {
                    alert('Select a date');
                    return;
                }

                <% if (this.IsSimpleDatePicker) { %>

                var fieldDescription = $('div.selected').html();
                var index = fieldDescription.indexOf(' (');
                if (index !== -1) {
                    fieldDescription = fieldDescription.substring(0, index);
                }

                var args = window.dialogArguments || {};
                args.fieldId = field;
                args.fieldDescription = fieldDescription;
                args.OK = true;
                onClosePopup(args);

                <% } else { %>

                var offset = $('#m_duration').val().match(/\d+/) * 1; // Multiplying by 1 to convert null strings to 0

                var input = new Object();
                input["field"] = field;
                input["offset"] = offset;
                input["addOffset"] = document.getElementById('m_after').checked;
                input["loanid"] = '<%=AspxTools.HtmlStringFromQueryString( "loanid" )%>';
                var result = gService.main.call("generateOutput", input);

                if (result.error) {
                    alert(result.UserMessage);
                    return;
                }

                if (result.value["pastDate"] == 'True') {
                    document.getElementById('errorMessage').innerText = 'The calculated date (' + result.value["date"] + ') has already passed. The due date must be either today or a future date.';
                } else {
                    var args = window.dialogArguments || {};
                    args.offset = result.value["offset"];
                    args.id = field;
                    args.dateDescription = result.value["dateDescription"];
                    args.date = result.value["date"];
                    args.OK = true;
                    onClosePopup(args);
                }

                <% } %>
            }
        </script>

        <input type="hidden" id="m_selectedFieldId" />
        <table>
            <tr>
                <td class="FieldLabel" nowrap colspan="3">
                    Search by Field Name or ID
                    <input type="text" id="searchCriteria" onkeyup="displaySearchResults();" class="w-130" />
                    <input type="button" id="m_search" value="Search" onclick="displaySearchResults();" />
                    <a href="#" onclick="window.open('https://support.lendingqb.com/index.php?/Knowledgebase/Article/View/497', 'fieldIDtutorial', 'resizable=yes,menubar=no,status=no,scrollbars=yes,width=750px');">How to find a Field ID</a>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="m_FieldList" style="height:200px; width:440px; overflow:auto; padding:3px; border:solid 1px black">
                        <asp:Repeater runat="server" ID="m_parameterList" OnItemDataBound="OnParameterListBound" EnableViewState="false">
                            <ItemTemplate>
                                <div id="m_headerDiv" runat="server" class="FieldLabel" isHeader="true">
                                    <ml:EncodedLiteral ID="m_headerName" runat="server"></ml:EncodedLiteral>
                                </div>
                                <div id="m_choiceDiv" runat="server">
                                    <ml:EncodedLiteral ID="m_parameterData" runat="server"></ml:EncodedLiteral>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
		    <tr>
		        <asp:PlaceHolder ID="CalculationDetailsCells" runat="server">
		        <td style="width:160px">
                    <input type="text" id="m_duration" runat="server" style="width:30px" /> business days
		        </td>
                <td style="width:140px">
                    <input type="radio" id="m_after" runat="server" name="offsetOption" checked /> after the<br />
                    <input type="radio" id="m_before" runat="server" name="offsetOption" /> prior to the
                </td>
                </asp:PlaceHolder>
                <td style="width:270px">
                    <label id="m_selectedFieldName"></label>
                </td>
		    </tr>
		    <tr>
		        <td colspan="3" align="center">
		            <input type="button" id="m_OK" value="OK" onclick="returnSelection();" />
		            <input type="button" id="m_Cancel" value="Cancel" onclick="confirmCancel();" />
		        </td>
		    </tr>
		    <tr>
		        <td colspan="3">
		            <label id="errorMessage" style="color:Red"></label>
		        </td>
		    </tr>
		</table>
    </form>
</body>
</html>
