﻿<%@
    Page
    Language="C#"
    AutoEventWireup="false"
    CodeBehind="TaskList.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Tasks.TaskList"
    MaintainScrollPositionOnPostback="true"
%>

<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.ObjLib.Task" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Reminders" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<%-- Tasks List Page for the mid-2011 release --%>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>TaskList</title>
    <meta content="C#" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet" />
    
    <style type="text/css">
        .TasksTitle
        {
            padding:    5px;
            margin:     5px 0px 5px 0px;
        }
        #m_openResolvedTasksGV
        {
            width:      110.5em;
        }
        #TopButtons
        {
        	width:      100%;
        	position:   relative;
        	padding:    5px;
        	height:     2.5em;
        }
        #TopButtonsLeftSide
        {
        	white-space:nowrap;
        }
        #TopButtonsRightSide
        {
        	white-space:nowrap;
        	text-align: right;
        }
        #BatchButtons
        {
        	padding:    5px;
        }
        .AlignLeft
        {
        	text-align: left;
        }
        .CheckboxColumn 
        {
        	width:      1.5em;
        }
        .TaskIDColumn
        {
            width:      6em;
        }
        .CondCategoryIdColumn
        {
        	width:      7em;
        	overflow:   hidden;
        }
        .SubjectColumn
        {
        	width:      50em;
        }
        .StatusColumn
        {
            width:      5em;
        }
        .DateColumn
        {
        	width:      11em;
        }
        .AssignedToColumn
        {
        	min-width:  8em;
        }
        .OwnerColumn
        {
            min-width:  8em;
        }

        .supporting-docs-col {
            white-space: nowrap;
            font-weight: normal;
        }

        th.supporting-docs-col {
            font-weight: bold;
        }

        .supporting-docs-col > div {
            padding: 5px;
        }

        .associated-doc-section {
            text-align: center;
        }

        .associated-doc-link-container {
            text-align: center;
        }

        .document-section {
            border-top: 1px solid black;
        }

        .associated {
            color: green;
        }

        .required {
            color: red;
        }

        .doc-type-required {
            font-weight: bold;
            display: inline-block;
        }

        .associated-doc-row span {
            margin-left: 2.7em;
        }

        .associated-doc-row .unlink-link + span {
            margin-left: 0;
        }

        .hide-supporting-docs .supporting-docs-col {
            display: none;
        }

        .white-space-pre-line {
            white-space: pre-line;
        }

        .drag-drop-error-div {
            color: red;
            display: none;
            padding: 0;
        }

        .drag-drop-error-div .upload-notice {
            color: blue;
        }
    </style>
</head>
<body>
    <form id="TaskList" runat="server">
    <script type="text/javascript">
        var allCB = "selectAllCB";
        var CBName = "taskCB_name";
        var CBInput = "input[name='"+CBName+"']";
        var CBSelected = CBInput+":checked";
        var taskSearchTextbox = "taskSearchTextbox";
        var taskEditorLink = "/newlos/Tasks/TaskEditorV2.aspx";
        var taskViewerLink = "/newlos/Tasks/TaskViewer.aspx";
        var batchButtonsDisabled = true;
        var taskDialogOptions = 'width=850,height=600,center=yes,resizable=yes,scrollbars=yes,status=yes,help=no';
        var userOwnedTasks = {};

        var associateDocUrl = "/newlos/ElectronicDocs/EdocPicker.aspx";
        var viewAssociatedDocsUrl = "/newlos/ElectronicDocs/EditEDocBatch.aspx";
        
        // ##### Checkboxes! #########################
        // See Task System Front End Q&A's wiki question 75 for more
        //   information about checkboxes

        var popupSettings = {
            width: "700",
            height: "500",
            hideCloseButton: true
        };

        function uploadDocs(dropzone) {
            if (getFilesCount(dropzone.id) === 0) {
                // No files selected by the user, e.g.
                // the file types selected were invalid.
                return;
            }

            var $el = $(dropzone);
            var parent = $el
                .parent() // Dropzone container
                .parent() // Document cell container
                .parent() // Table cell
                .parent(); // Table row

            var taskId = parent.attr("TaskId");
            ConditionDocumentUploadHelper.UploadDocs(dropzone, ML.sLId, taskId, false/*isPml*/);
        }

        function documentUploadPostAssociationCallback() {
            taskList_refresh();
        }

        function checkForChange(result) {
            if (result.HasChanged) {
                location.href = location.href;
            }
        }

        function associateDoc() {
            var el = $(this);
            var parent = el.parent().parent().parent();
            var taskId = parent.attr("TaskId");

            var args = {
                loanid: ML.sLId,
                TaskId: taskId,
                mode: "persist"
            };

            var settings = $.extend({}, popupSettings, {
                onReturn: checkForChange
            });

            LQBPopup.Show(gVirtualRoot + associateDocUrl + "?" + f_generateQueryString(args), settings);
        }

        function viewAssociatedDocs() {
            var el = $(this);
            var td = el.parent().parent();
            var row = td.parent();
            var taskId = row.attr("TaskId");
            var docIds = td.find(".unlink-link").map(function () {
                return $(this).attr("docid");
            }).get();

            var data = {
                ID: JSON.stringify(docIds)
            }

            var result = gService.edoc.call('BatchStore', data);
            if (result.error) {
                alert(result.UserMessage);
                return;
            }

            var args = {
                loanid: ML.sLId,
                key: result.value.key
            };
           var body_url = viewAssociatedDocsUrl + "?" + f_generateQueryString(args);
            window.open(gVirtualRoot + body_url, "Associated Doc Viewer", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=950,height=600");
        }

        function unlinkAssociatedDoc() {
            var el = $(this);
            var row = el.parent().parent().parent().parent();
            var taskId = row.attr("TaskId");
            var docId = el.attr("docid");

            var args = {
                TaskId: taskId,
                DocumentId: docId,
                LoanId: ML.sLId
            }

            var result = gService.edoc.call('UnlinkDoc', args);
            if (result.error) {
                alert(result.UserMessage);
                return;
            }
            else {
                el.parent().remove();
            }

            if (row.find(".unlink-link").length == 0) {
                row.find(".view-docs-link").remove();
            }
        }
        
        function selectAllCB_onClick() {
            if ($("#"+allCB).prop('checked')) {
                selectAllCB();
            } else {
                deselectAllCB();
            }
            checkIfUserOwnsTasks();
        }
        
        function selectAllCB() {
            $(CBInput).prop('checked', true);

            $("#BatchButtons input").prop('disabled', false);
            batchButtonsDisabled = false;
            
            // highlight the rows
            $(CBInput).each( function() {
                highlightRowByCheckbox(this);
            });
        }
        
        function deselectAllCB() {
            $(CBInput).prop('checked', false);

            $("#BatchButtons input").prop('disabled', true);
            batchButtonsDisabled = true;
            
            // highlight the rows
            $(CBInput).each( function() {
                highlightRowByCheckbox(this);
            });
        }
        
        function getUserOwnedTasks() {
            userOwnedTasks = {}; // reset the object
            var taskIdsCsv = $('#m_userOwnedTaskIds').val();
            var taskIds = taskIdsCsv.split(',');
            var len = taskIds.length;
            for (var i = 0; i < len; i++) {
                if (taskIds[i] == "") continue;
                userOwnedTasks[taskIds[i]] = true;
            }
        }
        
        function checkIfUserOwnsTasks() {
            var $CB = $(CBSelected);
            var userOwnsSelectedTasks = true;
            $CB.each( function() {
                var taskInfo = this.value.split(':');
                var taskID = taskInfo[0];
                if (userOwnedTasks[taskID] != true) {
                    userOwnsSelectedTasks = false;
                    return false; // exit the loop
                }
            });
            
            // If the user does not own all of the selected tasks,
            // enable the take ownership button
            $("#takeOwnershipBtn").prop('disabled', !(!batchButtonsDisabled && !userOwnsSelectedTasks));
        }
        
        function taskCB_onClick(checkbox) {
            highlightRowByCheckbox(checkbox);
            $("#"+allCB).prop('checked', false);
            if ($(CBSelected).length == 0) {
                $("#BatchButtons input").prop('disabled', true);
                batchButtonsDisabled = true;
            } else {
                $("#BatchButtons input").prop('disabled', false);
                batchButtonsDisabled = false;
                checkIfUserOwnsTasks();
            }
        }
        
        // ##### Top operations! ####################
        
        // This should show the task list's new state if it's been updated outside
        function taskList_refresh() {
            TaskList.submit();
            // perform a postback, mayhaps?
        }
        
        function showModalTaskCreator() {
            var queryString =
                '?loanid=' + ML.sLId +
                '&IsTemplate=' + $('#m_isTemplate').val();
            showModal(taskEditorLink + queryString, null, null, null, function(modalResult){
                if (modalResult.OK) {
                    taskList_refresh();
                } else {
                    //alert("CANCEL!");
                }
            },{hideCloseButton:true});
            
        }
        function showModalTaskViewer(taskID) {
            var queryString =
                '?loanid=' + ML.sLId +
                '&IsTemplate=' + $('#m_isTemplate').val();
            if (taskID !== null) {
                queryString += '&taskid=' + taskID;
            }
            showModal(taskViewerLink + queryString, null, null, null, function(modalResult){
                if (modalResult.OK) {
                    taskList_refresh();
                } else {
                    //alert("CANCEL!");
                }
            },{hideCloseButton:true});
        }
        
        function showModelessTaskViewer(taskID) {
            showTaskDialog(taskID, false);
        }
        
        function showModelessTaskCreator() {
            showTaskDialog('', true);
        }
        
        function showTaskDialog(taskID, isCreator) {
            var link;
            var windowName;
            var queryString;
            if (isCreator) { // creating a new task does not require a taskID
                link = taskEditorLink;
                queryString =
                    '?loanid=' + ML.sLId +
                    '&IsTemplate=' + encodeURIComponent($('#m_isTemplate').val());
                windowName = '_blank';
            } else {
                link = taskViewerLink;
                queryString =
                    '?loanid=' + ML.sLId +
                    '&IsTemplate=' + encodeURIComponent($('#m_isTemplate').val());
                if (taskID !== null) {
                    queryString += '&taskid=' + encodeURIComponent(taskID);
                }
                windowName = 'LOtaskWindow__' + taskID;
            }
            var url = VRoot + link + queryString;
            var handle = window.open('', windowName, taskDialogOptions);
            if (handle.location.href === 'about:blank') {
                handle.location.href = url;
            }
            handle.focus(); // if the window already exists, focus on it
        }
        
        function addNewTask_onClick() {
            showModelessTaskCreator();
        }
        
        function taskSearchBtn_onClick() {
            var $taskSearchTextbox = $("#"+taskSearchTextbox);
            var input = $taskSearchTextbox.val();
            
            var args = {
                TaskID: input,
                LoanID: ML.sLId
            };
            // Perform the search
            var result = gService.loanedit.call('TaskSearch', args);
            if (!result.error) {
                if (result.value['OK'] === 'True') {
                    // Go straight to the task viewer
                    showModelessTaskViewer(input);
                } else {
                    var taskSearchError = result.value['TaskSearchError'];
                    
                    // Go to the dialog
                    // Pass the arguments in as params so that we can get at them in the codebehind
                    showModal('/newlos/Tasks/TaskListSearchModal.aspx?taskId='+input+'&TaskSearchError='+taskSearchError, null, null, null, function(modalResult){
                        var loanId = result.value['LoanId'];
                        var loanName = result.value['LoanName'];
                        if (taskSearchError === 'TaskNotInLoanFile' && modalResult.OK) {
                            f_openLoan(loanId, loanName);
                            showModelessTaskViewer(input);
                        }
                    },{hideCloseButton:true});
                    
                }
            } else {
                alert(result.UserMessage);
            }
        }
        
        function refresh_onClick() {
            taskList_refresh();
        }

        function generateWarningMessage(taskIDs, warnOnResolveTasks, action) {
            var warnOnResolveMessages = [];
            for (var i = 0; i < warnOnResolveTasks.length; i++) {
                if (warnOnResolveTasks[i] === 'True') {
                    var taskWarnMsg = taskIDs[i];
                    var $taskSubjectLink = $('.TaskSubjectLink.' + taskIDs[i]);
                    if ($taskSubjectLink.length > 0) {
                        var subjectText = $taskSubjectLink.text();
                        taskWarnMsg += ': ' + subjectText.substr(0, 50);
                        if (subjectText.length > 50) {
                            taskWarnMsg += '...';
                        }
                    }
                    warnOnResolveMessages.push(taskWarnMsg);
                }
            }

            if (warnOnResolveMessages.length > 0) {
                var confirmMsg = 'The following conditions are missing an association with a required document type. Are you sure you want to ' + action + '?\n'
                confirmMsg += warnOnResolveMessages.join('\n');
                return confirmMsg;
            }

            return '';
        }
        
        // ##### Batch Operations! ###################
        // Anatomy of a batch operation call
        // 1. Extract the TaskIds (first part of batchSubmit)
        // 2. Perform the operation using the function that we passed in
        // 3. Update

        function resolve_onClick() {
            return batchSubmit(function(taskIDs, taskVersions, hiddenTasks, warnOnResolveTasks, resolutionBlockTriggerNames, resolutionDateFieldIds) {
                // We want to warn the user if they are trying to resolve any
                // tasks which do not have the required doc type associated.
                var warningMsg = generateWarningMessage(taskIDs, warnOnResolveTasks, 'resolve');
                if (warningMsg.length > 0 && !confirm(warningMsg)) {
                    return false;
                }

                var serviceArgs = {
                    TaskIDs: taskIDs.join(','),
                    TaskVersions: taskVersions.join(','),
                    TaskResolutionBlockTriggerNames: resolutionBlockTriggerNames.join(','),
                    TaskResolutionDateFieldIds: resolutionDateFieldIds.join(','),
                    LoanId: ML.sLId
                }
                return batchServiceCall('Resolve', serviceArgs);
            });
        }

        function close_onClick() {
            return batchSubmit(function(taskIDs, taskVersions, hiddenTasks, warnOnResolveTasks, resolutionBlockTriggerNames, resolutionDateFieldIds) {
                // We want to warn the user if they are trying to resolve any
                // tasks which do not have the required doc type associated.
                var warningMsg = generateWarningMessage(taskIDs, warnOnResolveTasks, 'close');
                if (warningMsg.length > 0 && !confirm(warningMsg)) {
                    return false;
                }
                var serviceArgs = {
                    TaskIDs : taskIDs.join(','),
                    TaskVersions: taskVersions.join(','),
                    TaskResolutionBlockTriggerNames: resolutionBlockTriggerNames.join(','),
                    TaskResolutionDateFieldIds: resolutionDateFieldIds.join(','),
                    LoanId : ML.sLId
                }
                return batchServiceCall('Close', serviceArgs);
            });
        }

        function assign_onClick() {
            return batchSubmit(function(taskIDs, taskVersions, hiddenTasks) {
                var queryString = "?"
                    + 'loanid=' + ML.sLId
                    + '&IsBatch=' + true
                    + '&IsTemplate=' + $('#m_isTemplate').val();
                var anyHidden = false;
                for (var i = 0; i < hiddenTasks.length; i++) {
                    if (hiddenTasks[i] === "True") {
                        anyHidden = true;
                        break;
                    }
                }
                if (anyHidden === true) {
                    queryString += "&nopml=true";
                }
                showModal("/newlos/Tasks/RoleAndUserPicker.aspx" + queryString, null, null, null, function(modalResult){
                    if (modalResult.OK) {
                        var serviceArgs = {
                            TaskIDs : taskIDs.join(','),
                            TaskVersions : taskVersions.join(','),
                            LoanId : ML.sLId,
                            Id : modalResult.id,
                            Name : modalResult.name,
                            Type : modalResult.type
                        }
                        return batchServiceCall('Assign', serviceArgs);
                    } else {
                        return false;
                    }
                },{hideCloseButton:true});
            });
        }
        function setDueDate_onClick() {
            return batchSubmit(function(taskIDs, taskVersions) {
                var modalArgs = cGenericArgumentObject();
                showModal('/newlos/Tasks/TaskListSetDueDateModal.aspx?loanid='+ML.sLId, modalArgs, null, null, function(modalResult){
                    if (modalResult.OK) {
                        var serviceArgs = {
                            TaskIDs : taskIDs.join(','),
                            TaskVersions : taskVersions.join(','),
                            LoanId : ML.sLId,
                            Offset : modalResult.offset,
                            Id : modalResult.id,
                            Date : modalResult.date,
                            DateLocked : modalResult.dateLocked
                        }
                        return batchServiceCall('SetDueDate', serviceArgs);
                    } else {
                        return false;
                    }
                },{hideCloseButton:true});
            });
        }
        function setFollowupDate_onClick() {
            return batchSubmit(function(taskIDs, taskVersions) {
                var modalArgs = cGenericArgumentObject();
                showModal('/newlos/Tasks/TaskListSetFollowupDateModal.aspx?loanid='+ML.sLId, modalArgs, null, null, function(modalResult){
                    if (modalResult.OK) {
                        var serviceArgs = {
                            TaskIDs : taskIDs.join(','),
                            TaskVersions : taskVersions.join(','),
                            LoanId : ML.sLId,
                            Date : modalResult.date
                        }
                        return batchServiceCall('SetFollowupDate', serviceArgs);
                    } else {
                        return false;
                    }
                },{hideCloseButton:true});
            });
        }
        function takeOwnership_onClick() {
            return batchSubmit(function(taskIDs, taskVersions) {
                var serviceArgs = {
                    TaskIDs : taskIDs.join(','),
                    TaskVersions : taskVersions.join(','),
                    LoanId : ML.sLId
                }
                return batchServiceCall('TakeOwnership', serviceArgs);
            });
        }

        function exportToPDF_onClick() {
            return batchSubmit(function(taskIDs, taskVersions) {
                var args = {
                    TaskIDs : taskIDs.join(','),
                    TaskVersions : taskVersions.join(','),
                    LoanId : ML.sLId
                }
                
                // We need pdfInstance.Name and VRoot, so call the service
                var result = gService.loanedit.call('ExportToPDF', args);
                if (!result.error) {
                    var filepath = result.value['filepath'];
                    LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(filepath);
                } else {
                    alert('Error: ' + result.UserMessage);
                }
            }, true);
        }
        
        function subscribe_onClick() {
            return batchSubmit(function(taskIDs, taskVersions) {
                var serviceArgs = {
                    TaskIDs : taskIDs.join(','),
                    TaskVersions : taskVersions.join(','),
                    LoanId : ML.sLId
                }
                if (batchServiceCall('Subscribe', serviceArgs)) {
                    alert("You are now subscribed to the selected tasks.");
                }
                return false;
            });
        }
        function unsubscribe_onClick() {
            return batchSubmit(function(taskIDs, taskVersions) {
                var serviceArgs = {
                    TaskIDs : taskIDs.join(','),
                    TaskVersions : taskVersions.join(','),
                    LoanId : ML.sLId
                }
                if (batchServiceCall('Unsubscribe', serviceArgs)) {
                    alert("You are now unsubscribed from the selected tasks.");
                }
                return false;
            });
        }
        
        function batchSubmit(lambda, bRefresh) {
            if (batchButtonsDisabled) {
                return false;
            }
            $("#BatchButtons input").prop('disabled', true);
            batchButtonsDisabled = true;
            var $CB = $(CBSelected);
            var taskIDs = [];
            var taskVersions = [];
            var hiddenTasks = [];
            var warnOnResolveTasks = [];
            var resolutionBlockTriggerNames = [];
            var dateResolutionFieldIds = [];
            $CB.each(function() {
                var taskInfo = this.value.split(':')
                taskIDs.push(taskInfo[0]);
                taskVersions.push(taskInfo[1]);
                hiddenTasks.push(taskInfo[2]);
                warnOnResolveTasks.push(taskInfo[3]);
                resolutionBlockTriggerNames.push(taskInfo[4]);
                dateResolutionFieldIds.push(taskInfo[5]);
            });
            
            var operationResult = lambda(taskIDs, taskVersions, hiddenTasks, warnOnResolveTasks, resolutionBlockTriggerNames, dateResolutionFieldIds);
            
            // Now update! Unless we explicitly don't want to update
            // Or if the operation failed
            if (typeof bRefresh == "undefined" && operationResult) {
                taskList_refresh();
            }
            $("#BatchButtons input").prop('disabled', false);
            batchButtonsDisabled = false;
        }
        
        function batchServiceCall(serviceName, serviceArgs) {
            var serviceResult = gService.loanedit.call(serviceName, serviceArgs);
            if (!serviceResult.error) {
                // Prep and bring up the error modal
                if (serviceResult.value.ErrorCacheId != null) {
                    var queryString = '?id=' + serviceResult.value.ErrorCacheId;
                    showModal('/newlos/Tasks/BatchTaskErrorSummary.aspx' + queryString, null, null, null, null,{hideCloseButton:true});
                    if (serviceResult.value.TotalFailure != null && serviceResult.value.TotalFailure === 'True') { // If all operations failed
                        return false;
                    } else {
                        return true;
                    }
                }
                return true;
            } else {
                alert('Error: ' + serviceResult.UserMessage);
                return false;
            }
        }

        // just pretend it's the same as $(document).ready
        function _init() {
            // Stuff that should only be done when everything's loaded

            $("input[type='button']").attr("NoHighlight", "true"); // turn off button highlighting
            $(".associate-link").click(associateDoc);
            $(".view-docs-link").click(viewAssociatedDocs);
            $(".unlink-link").click(unlinkAssociatedDoc);

            $('.drag-drop-container').each(function (index, dropzone) {
                var dragDropSettings = {
                    compact: true,
                    onProcessAllFilesCallback: function () { uploadDocs(dropzone); },
                    blockUpload: !ML.CanUploadEdocs,
                    blockUploadMsg: ML.UploadEdocsFailureMessage,
                    hideFilesInFileListParam: true
                };

                registerDragDropUpload(dropzone, dragDropSettings);
            });

            var $taskSearchTextbox = $("#"+taskSearchTextbox);
            var $taskSearchBtn = $("#taskSearchBtn");
            function updateSearchBtn (len) {
                $taskSearchBtn.prop('disabled', len <= 0);
            }
            $taskSearchTextbox.keypress(function(event){
                // Prevent users from entering invalid characters (we should still validate)
                // Case 64938 has a list of invalid characters
                var alphanumericRegex = /^[34679ACDEFGHJKLMNPRTWXY]$/i;
                if (!alphanumericRegex.test(String.fromCharCode(event.which))) {
                    return false;
                }
                return true;
            });
            $taskSearchTextbox.keyup(function(event) {
                // Click the search button when the user presses enter in the search textbox
                if (event.which === 13) {
                    $taskSearchBtn.click();
                }
                updateSearchBtn($taskSearchTextbox.val().length);
            });
            
            // If no checkboxes are selected, then disable all the batch operations
            batchButtonsDisabled = $(CBSelected).length <= 0;
            $("#BatchButtons input").prop('disabled', batchButtonsDisabled);

            updateSearchBtn($taskSearchTextbox.val().length);
            
            // Select the row of urgent items and bold the whole row
            // .DiscDue is given to a DueDate or FollowUpDate in the codebehind:GetDateWarningCss
            $(".DiscDue").closest(".GridItem, .GridAlternatingItem").css('font-weight', 'bold');
            getUserOwnedTasks();
        }
    </script>
    <asp:HiddenField ID="m_isTemplate" runat="server" Value="" />
    <asp:HiddenField ID="m_userOwnedTaskIds" runat="server" Value="" />
    <div class="MainRightHeader" nowrap="true">
        <ml:EncodedLabel runat="server" ID="m_title" Text="Task List"></ml:EncodedLabel>
    </div>
    <div>
        <table id="TopButtons">
            <tr>
                <td id="TopButtonsLeftSide">
                    <input type="button" value="Add new task" onclick="addNewTask_onClick()" />
                    <input type="button" value="Refresh" onclick="refresh_onClick()" NotForEdit />
                    &nbsp;
                    Display 
                    <asp:DropDownList runat="server" ID="m_conditionFilter" AutoPostBack="true" OnSelectedIndexChanged="m_openResolvedTasksGV_SelectedCondFilterChanged">
                    </asp:DropDownList>
                    assigned to:
                    <asp:DropDownList runat="server" ID="m_assignmentFilter" OnInit="PopulateAssignmentDropdown" AutoPostBack="true" OnSelectedIndexChanged="m_openResolvedTasksGV_SelectedFilterChanged">
                    </asp:DropDownList>
                    <ml:EncodedLabel runat="server" ID="m_filterErrorMessage"></ml:EncodedLabel>
                </td>
                <td id="TopButtonsRightSide">
                    Task #
                    <asp:TextBox name="taskSearch" runat="server" EnableViewState="true" id="taskSearchTextbox" NotForEdit></asp:TextBox>
                    <input type="button" name="taskSearch" id="taskSearchBtn" value="Search" onclick="taskSearchBtn_onClick()" NotForEdit />
                </td>
            </tr>
        </table>
    
        <div id="openResolvedTasksDiv">
        <div runat="server" id="m_openResolvedTasksTitle" class="GridHeader TasksTitle">
            Open Tasks
        </div>
       
        <asp:GridView id="m_openResolvedTasksGV" runat="server" AutoGenerateColumns="false" EnableViewState="false" OnRowDataBound="m_openResolvedTasksGV_RowDataBound">
            <HeaderStyle cssclass="GridHeader AlignLeft" />
            <AlternatingRowStyle cssclass="GridAlternatingItem" />
            <RowStyle cssclass="GridItem" />
            
            <columns>
                
                <asp:TemplateField>
                    <HeaderStyle CssClass="CheckboxColumn" />
                    <HeaderTemplate>
                        <input type="checkbox" name="selectAllCB_name" id="selectAllCB" onclick="selectAllCB_onClick();" NotForEdit />
                    </HeaderTemplate>
                    <ItemStyle CssClass="CheckboxColumn" />
                    <ItemTemplate>
                        <input type="checkbox" id="taskCB_id" name="taskCB_name" onclick="taskCB_onClick(this)"
                            value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>:<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskRowVersion").ToString()) %>:<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskHidden").ToString()) %>:<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "WarnOnResolve").ToString()) %>:<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ResolutionBlockTriggerName").ToString()) %>:<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ResolutionDateSetterFieldId").ToString()) %>'
                            NotForEdit />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle CssClass="StatusColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton
                            Text="Status"
                            CommandName="Sort"
                            CommandArgument="TaskStatus openResolvedTasksGV_StatusHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_StatusHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="StatusColumn" />
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CalcTaskStatus").ToString()) %>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton
                            runat="server"
                            CommandName="Sort"
                            CommandArgument="TaskId openResolvedTasksGV_TaskHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            Text="Task">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_TaskHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="TaskIDColumn" />
                    <ItemTemplate>
                        <a href="javascript: void(0)" class="OpenTaskIDLink TaskViewerLink" title="Open the task viewer for this task."
                            onclick="showModelessTaskViewer(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>)">
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle CssClass="CondCategoryIdColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton
                            runat="server"
                            CommandName="Sort"
                            CommandArgument="CondCategoryId openResolvedTasksGV_CondCategoryIdHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            Text="Category">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_CondCategoryIdHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="CondCategoryIdColumn"/>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CondCategoryId").ToString())%>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle CssClass="SubjectColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton
                            Text="Subject"
                            CommandName="Sort"
                            CommandArgument="TaskSubject openResolvedTasksGV_SubjectHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_SubjectHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="SubjectColumn" />
                    <ItemTemplate>
                        <span class="white-space-pre-line">
                            <a href="javascript: void(0)" class="OpenTaskIDLink TaskViewerLink TaskSubjectLink <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>" 
    		                    title="Open the task viewer for this task."
                                onclick="showModelessTaskViewer(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>)">
		                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskSubject").ToString()) %>
		                    </a>
                        </span>		                
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle CssClass="DateColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton
                            Text="Due Date"
                            CommandName="Sort"
                            CommandArgument="TaskDueDateComparison openResolvedTasksGV_DueDateHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_DueDateHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="DateColumn" />
                    <ItemTemplate>
                        <span class="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CalcTaskDueDateWarningCss").ToString()) %>">
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskDueDate", "{0:MM/dd/yyyy}"))%>
                        </span>
                        <a 
                            style="display: <%# AspxTools.HtmlString( DataBinder.Eval( Container.DataItem, "TaskDueDateCalcDisplay").ToString() ) %>"
                            title="<%# AspxTools.HtmlString( DataBinder.Eval( Container.DataItem, "TaskDueDateCalc").ToString() ) %>"
                        >
                            CALC
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle CssClass="DateColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton
                            Text="Follow-up Date"
                            CommandName="Sort"
                            CommandArgument="TaskFollowUpDateComparison openResolvedTasksGV_FollowupDateHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_FollowupDateHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="DateColumn" />
                    <ItemTemplate>
                        <span class="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CalcTaskFollowupDateWarningCss").ToString()) %>">
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskFollowUpDate", "{0:MM/dd/yyyy}"))%>
                        </span>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Last Updated">
                    <HeaderStyle CssClass="DateColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton
                            Text="Last Updated"
                            CommandName="Sort"
                            CommandArgument="TaskLastModifiedDateComparison openResolvedTasksGV_LastUpdatedHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_LastUpdatedHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="DateColumn" />
			        <ItemTemplate>
				        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskLastModifiedDate", "{0:MM/dd/yyyy}"))%>
			        </ItemTemplate>
		        </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle CssClass="supporting-docs-col" />
                    <HeaderTemplate>
                        <span>Supporting Documents</span>
                    </HeaderTemplate>
                    <ItemStyle CssClass="supporting-docs-col" />
                    <ItemTemplate>
                        <div runat="server" id="RequiredDocSection" visible="false">
                            <asp:Image runat="server" ID="RequiredStatusImg" />
                            <span runat="server" class="doc-type-required" id="CondRequiredDocType"></span>
                            <span runat="server" id="RequiredStatusText"></span>

                        </div>
                        <div runat="server" id="AssociatedDocSection" class="associated-doc-section" visible="false">
                            <a class="associate-link">associate doc</a>
                            <div class="InsetBorder full-width">
                                <div id="DragDropZone" class="drag-drop-container" runat="server"></div>
                            </div>
                            <div class="drag-drop-error-div"></div>
                        </div>
                        <div runat="server" class="document-section" id="DocumentSection" visible="false">
                            <asp:Repeater runat="server" ID="AssociatedDocRepeater" OnItemDataBound="AssociatedDocRepeater_ItemDataBound">
                                <ItemTemplate>
                                    <div class="associated-doc-row">
                                        <a runat="server" class="unlink-link" id="UnlinkLink">unlink</a>
                                        <span runat="server" id="AssociatedDoc"></span>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <div runat="server" id="AssociatedDocLinkContainer" class="associated-doc-link-container">
                                <a class="view-docs-link">view associated docs</a>
                            </div>
                        </div>
                        <div runat="server" id="LinkSection" visible="false"></div>
                    </ItemTemplate>
                </asp:TemplateField>
		        
		        <asp:TemplateField>
		            <HeaderStyle CssClass="AssignedToColumn" />
		            <HeaderTemplate>
                        <asp:LinkButton
                            Text="Assigned To"
                            CommandName="Sort"
                            CommandArgument="AssignedUserFullName openResolvedTasksGV_AssignedToHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_AssignedToHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
		            <ItemStyle CssClass="AssignedToColumn" />
			        <ItemTemplate>
			            <div style="width: 8em; overflow: hidden;"><!-- IE appeasement --></div>
				        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AssignedUserFullName").ToString()) %>
			        </ItemTemplate>
		        </asp:TemplateField>
		        
		        <asp:TemplateField>
		            <HeaderStyle CssClass="OwnerColumn" />
		            <HeaderTemplate>
                        <asp:LinkButton
                            Text="Owner"
                            CommandName="Sort"
                            CommandArgument="OwnerFullName openResolvedTasksGV_OwnerHeader"
                            OnCommand="m_openResolvedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="openResolvedTasksGV_OwnerHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
		            <ItemStyle CssClass="OwnerColumn" />
			        <ItemTemplate>
			            <div style="width: 8em; overflow: hidden;"><!-- IE appeasement --></div>
				        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OwnerFullName").ToString())%>
			        </ItemTemplate>
		        </asp:TemplateField>
            </columns>
        </asp:GridView>
        
        </div>
        
        <div id="BatchButtons">
            <input type="button" value="Resolve" id="resolveBtn" onclick="resolve_onClick()"/>
            <input type="button" value="Close" id="closeBtn" onclick="close_onClick()"/>
            <input type="button" value="Assign" id="assignBtn" onclick="assign_onClick()"/>
            <input type="button" value="Set Due Date" id="setDueDateBtn" onclick="setDueDate_onClick()"/>
            <% if (!IsTemplate) { %>
            <input type="button" value="Set Follow-up Date" id="setFollowupDateBtn" onclick="setFollowupDate_onClick()"/>
            <input type="button" value="Take Ownership" id="takeOwnershipBtn" onclick="takeOwnership_onClick()"/>
            <% } %>
            <input type="button" value="Export to PDF" id="exportToPDFBtn" onclick="exportToPDF_onClick();"/>
            
            <%-- Hide subscribe links if the user has opted out of e-mail notifications or if this is a loan template --%>
            <% if (!IsTemplate) { %>
                <% if (CurrentEmployee.TaskRelatedEmailOptionT == DataAccess.E_TaskRelatedEmailOptionT.ReceiveEmail) { %>
            <input type="button" value="Subscribe" id="subscribeBtn" onclick="subscribe_onClick()"/>
            <input type="button" value="Un-subscribe" id="unsubscribeBtn" onclick="unsubscribe_onClick()"/>
                <% } %>
            <% } %>
        </div>
        
        <div runat="server" id="m_closedTasksDiv">
        <div class="GridHeader TasksTitle">Closed Tasks</div>
        <asp:GridView id="m_closedTasksGV" runat="server" EnableViewState="false" AutoGenerateColumns="false" Width="100%">
            <HeaderStyle cssclass="GridHeader AlignLeft" />
            <AlternatingRowStyle cssclass="GridAlternatingItem" />
            <RowStyle cssclass="GridItem" />
            
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle CssClass="TaskIDColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton
                            Text="Task"
                            CommandName="Sort"
                            CommandArgument="TaskId closedTasksGV_TaskHeader"
                            OnCommand="m_closedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="closedTasksGV_TaskHeader" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="TaskIDColumn" />
                    <ItemTemplate>
                        <a href="javascript: void(0)" class="ClosedTaskIDLink TaskViewerLink" title="Open the task viewer for this task."
                            onclick="showModelessTaskViewer(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>)">
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString())%>
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField SortExpression="TaskSubject">
                    <HeaderStyle CssClass="SubjectColumn" />
                    <HeaderTemplate>
                        <asp:LinkButton
                            Text="Subject"
                            CommandName="Sort"
                            CommandArgument="TaskSubject closedTasksGV_TaskSubject"
                            OnCommand="m_closedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="closedTasksGV_TaskSubject" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemStyle CssClass="SubjectColumn" />
                    <ItemTemplate>
		                <a href="javascript: void(0)" class="ClosedTaskIDLink TaskViewerLink" title="Open the task viewer for this task."
                            onclick="showModelessTaskViewer(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>)">
		                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskSubject").ToString()) %>
		                </a>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton
                            Text="Last Updated"
                            CommandName="Sort"
                            CommandArgument="TaskLastModifiedDateComparison closedTasksGV_TaskLastModifiedDate"
                            OnCommand="m_closedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="closedTasksGV_TaskLastModifiedDate" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemTemplate>
		                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskLastModifiedDate", "{0:MM/dd/yyyy}"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton
                            Text="Owner"
                            CommandName="Sort"
                            CommandArgument="OwnerFullName closedTasksGV_TaskOwnerFullName"
                            OnCommand="m_closedTasksGV_Sorting"
                            runat="server">
                        </asp:LinkButton>
                        <ml:EncodedLabel runat="server" ID="closedTasksGV_TaskOwnerFullName" Text=""></ml:EncodedLabel>
                    </HeaderTemplate>
                    <ItemTemplate>
		                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OwnerFullName").ToString())%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        </div>
    </div>
    
    
    <uc1:cModalDlg ID="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cModalDlg>
    </form>
</body>
</html>

