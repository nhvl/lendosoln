﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos.Tasks
{
    public partial class TaskPermissionPicker : BaseServicePage
    {
        private BrokerUserPrincipal CurrentPrincipal
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private Guid BrokerId
        {
            get
            {
                if (CurrentPrincipal != null)
                    return CurrentPrincipal.BrokerId;
                else
                    return new Guid(RequestHelper.GetSafeQueryString("brokerid"));
            }
        }

        private bool ForceConditions
        {
            get { return RequestHelper.GetBool("IsCond"); }
        }

        private bool IsTemplate
        {
            get { return RequestHelper.GetBool("IsTemplate"); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            IEnumerable<PermissionLevel> permissionLevels;
            if (IsTemplate)
            {
                permissionLevels = PermissionLevel.RetrieveAll(BrokerId);
            }
            else
            {
                List<Guid> UserGroups = new List<Guid>();
                foreach (Group ugroup in GroupDB.ListInclusiveGroupForEmployee(CurrentPrincipal.BrokerId, CurrentPrincipal.EmployeeId))
                {
                    UserGroups.Add(ugroup.Id);
                }
                List<E_RoleT> Roles = new List<E_RoleT>();
                foreach (E_RoleT role in CurrentPrincipal.GetRoles())
                {
                    Roles.Add(role);
                }

                permissionLevels = PermissionLevel.RetrieveManageLevelsForUser(BrokerId, Roles, UserGroups);
            }

            if (ForceConditions)
            {
                permissionLevels = permissionLevels.Where(p => p.IsAppliesToConditions);
            }

            m_dgPermissionTypes.DataSource = permissionLevels.Where(p => p.IsActive);
            m_dgPermissionTypes.DataBind();
        }
    }
}
