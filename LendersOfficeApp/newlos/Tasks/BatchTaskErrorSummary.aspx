﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchTaskErrorSummary.aspx.cs" Inherits="LendersOfficeApp.newlos.Tasks.BatchTaskErrorSummary" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title></title>
    <style type="text/css">
         body { background-color: gainsboro; padding: 10px; font-weight: bold; }
        .buttonContainer  { text-align: center; margin-top: 10px;}
        .Grid { clear: both; height: 400px; overflow: scroll; padding-top: 10px; padding-bottom: 10px; }
        .RightHeader, .LeftHeader { float: left; font-size: larger; }
    </style>
</head>
<body>
    <h4 class="page-header">Error</h4>
    <form id="form1" runat="server">
    
    <div class="LeftHeader">
        
    </div>
    <div class="RightHeader">
        <ml:EncodedLabel runat="server" ID="Message"> </ml:EncodedLabel>
    </div>
    <div class="Grid">
        <asp:GridView runat="server" ID="ErrorList"  CellPadding="5" AutoGenerateColumns="false"> 
            <HeaderStyle CssClass="GridHeader" />
            <AlternatingRowStyle  CssClass="GridAlternatingItem" />
            <RowStyle CssClass="GridItem"  />
            
            <Columns>
                <asp:BoundField HeaderText="Task" DataField="Id" />
                <asp:BoundField HeaderText="Subject"  DataField="Subject" />
                <asp:BoundField HeaderText="Reason" DataField="Reason" />
            </Columns>
        </asp:GridView>
    </div>
    <div class="buttonContainer">
        <input type="button" value="OK" id="OkBtn" />
    </div>
    </form>
    <script type="text/javascript">
        $(function() {
            resizeForIE6And7(550, 500);

            $('#OkBtn').click(function() {
                onClosePopup();
            });
        });
    </script>
    <uc1:cModalDlg ID="CModalDlg1" runat="server" ></uc1:cModalDlg>
</body>
</html>
