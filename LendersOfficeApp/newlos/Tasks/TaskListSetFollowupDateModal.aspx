﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="TaskListSetFollowupDateModal.aspx.cs" Inherits="LendersOfficeApp.newlos.Tasks.TaskListSetFollowupDateModal" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Task List Set Follow-up Date Modal</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .ButtonStyle { overflow: visible; width: auto; padding: 2px; }
        #BottomButtons
        {
            text-align: center;
        }
    </style>
</head>
<body>
    <h4 class="page-header">Set Follow-up Date</h4>
    <form id="TaskListSetFollowupDateModal" runat="server">
    <script type="text/javascript">
        function _init() {
	        $("#OKBtn").click(function() {
	            var dateString = $("#TaskFollowUpDate").val();
	            var date = new Date(dateString);
	            if (dateString != "" && isNaN(date)) {
                    alert("Please select a valid date.");
                    return false;
	            }
                var result = window.dialogArguments || {};
                result.OK = true;
                result.date = dateString;
                onClosePopup(result);
	        });

	        $("#cancelBtn").click(function() {
	            var result = window.dialogArguments || {};
	            result.OK = false;
	            onClosePopup(result);
	        });
	        
	        resizeForIE6And7(400, 250);
        }
    </script>
    <div style="margin: 10px">
        <div style="text-align: center">
        <table> <!-- Makin a table for alignment issues with the datebox -->
            <tr>
                <td>
                    Follow-up Date
                </td>
                <td>
                    <ml:DateTextBox id="TaskFollowUpDate" runat="server"></ml:DateTextBox>
                </td>
            </tr>
        </table>
        </div>
        
        <div id="BottomButtons">
            <input type="button" id="OKBtn" value="Save" />
            <input type="button" id="cancelBtn" value="Cancel" />
        </div>
    </div>
    <uc1:cModalDlg ID="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cModalDlg>
    </form>
</body>
</html>
