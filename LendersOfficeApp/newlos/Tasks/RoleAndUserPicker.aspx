﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RoleAndUserPicker.aspx.cs" Inherits="LendersOfficeApp.newlos.Tasks.RoleAndUserPicker" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title></title>
	<link href="~/css/stylesheet.css" type="text/css" rel="stylesheet">
	<style type="text/css">
	th.headerSortUp
	{
        background-image: url(../../images/Tri_DESC.gif);
        background-repeat: no-repeat;
        background-position: right center;
	}
	
	th.headerSortDown 
	{
        background-image: url(../../images/Tri_ASC.gif);
        background-repeat: no-repeat;
        background-position: right center;
	}
	
	th.header
	{
	    cursor: pointer;
	}
	</style>
</head>
<body onload="resizeForIE6And7(500, 400);">
    <script type="text/javascript">
        function selectUser(id, name, pml) {
            var args = window.dialogArguments || {};
            args.id = id;
            args.type = 'user';
            args.name = name;
            args.pmlUser = pml;
            args.OK = true;
            onClosePopup(args);
        }

        function roleChanged() {
            var ddl = document.getElementById('m_roleList');
            document.getElementById('selectRole').disabled = ddl[ddl.selectedIndex].value == '';
        }
        
        function rolePicked() {
            var ddl = document.getElementById('m_roleList');
            if (ddl[ddl.selectedIndex].value != '') {
                var args = window.dialogArguments || {};
                args.id = ddl[ddl.selectedIndex].value;
                args.type = 'role';
                args.pmlUser = false;
                args.name = ddl[ddl.selectedIndex].text;
                args.OK = true;
                onClosePopup(args);
            }
        }

        $(document).ready(function() {
            $("#m_dgUsers").tablesorter({ sortList: [[0, 0]] });
            $("#m_dgUsers").on("sortEnd", function() { setClass($('#m_dgUsers tbody')[0]); });
        });

        function setClass(tBody) {
            for (var i = 0; i < tBody.rows.length; i++) {
                var row = tBody.getElementsByTagName("tr")[i];
                if (i % 2 < 1)
                    row.className = 'GridItem';
                else
                    row.className = 'GridAlternatingItem';
            }
        }
	</script>
    <h4 class="page-header"><ml:EncodedLiteral ID="m_pageHeader" runat="server"></ml:EncodedLiteral></h4>
	<form id="form1" runat="server">
		<table cellspacing="2" cellpadding="0" width="100%" height="100%">
		    <tr id="RolePicker" runat="server">
		        <td>
		            Select a role:
		        </td>
		        <td>
		            <asp:DropDownList ID="m_roleList" runat="server" onchange="roleChanged();"></asp:DropDownList>
		            <input type="button" value="Apply role" onclick="rolePicked();" id="selectRole" disabled="disabled" />
		        </td>
		    </tr>
		    <tr>
		        <td>
		            <ml:EncodedLiteral ID="m_searchLiteral" runat="server"></ml:EncodedLiteral>
		        </td>
		        <td>
		            <asp:TextBox ID="m_searchBox" runat="server"></asp:TextBox>
		            ("s" for John Smith or Sam Cash, "b s" for Bob Smith)
		        </td>
		    </tr>
		    <tr>
		        <td></td>
		        <td>
		            <asp:Button ID="m_searchBtn" runat="server" Text="Search" UseSubmitBehavior="false" OnClientClick="this.disabled = true;" />
		        </td>
		    </tr>
		    <tr>
		        <td colspan="2">
		            <ml:EncodedLabel ID="m_userMessage" runat="server" Text="The following users have read-access to the loan file:"></ml:EncodedLabel>		            
		        </td>
		    </tr>
		    <tr>
		        <td colspan="2">
		            <ml:EncodedLabel ID="m_HiddenTask" runat="server" Text="Hidden conditions may only be assigned to employees and not to PML users."></ml:EncodedLabel>		            
		        </td>
		    </tr>
		    <tr>
		        <td colspan="2">
					<asp:GridView id="m_dgUsers" runat="server" AutoGenerateColumns="false" Width="97%">
						<AlternatingRowStyle cssclass="GridAlternatingItem"></AlternatingRowStyle>
						<RowStyle cssclass="GridItem"></RowStyle>
						<HeaderStyle cssclass="GridHeader"></HeaderStyle>
					    <Columns>
							<asp:TemplateField HeaderStyle-HorizontalAlign="Left">
							    <HeaderTemplate>
							        <a href="#">User</a>
							    </HeaderTemplate>
								<ItemTemplate>
									<a href="#" onclick="selectUser(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "UserId").ToString())%>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FullName").ToString())%>, <%# AspxTools.JsString((DataBinder.Eval(Container.DataItem, "Type") == "PML").ToString())%>);">
									    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "FullName").ToString())%>
									</a>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderStyle-HorizontalAlign="Left">
							    <HeaderTemplate>
							        <a href="#">User Type</a>
							    </HeaderTemplate>
								<ItemTemplate>
								    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Type").ToString())%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderStyle-HorizontalAlign="Left">
								<ItemTemplate>
								    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Roles").ToString())%>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
		        </td>
		    </tr>
		    <tr>
		        <td colspan="2">
		            <ml:EncodedLabel runat="server" ID="m_noUsersFound" Text="No users match the specified criteria."></ml:EncodedLabel>
		        </td>
		    </tr>
		    <tr>
		        <td colspan="2" style="margin-top:10px" align="center">
		            <input type="button" value="Cancel" onclick="onClosePopup();" />
		        </td>
		    </tr>
        </table>
    </form>
</body>
</html>
