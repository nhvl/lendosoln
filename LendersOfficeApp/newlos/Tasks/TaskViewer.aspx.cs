﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOfficeApp.newlos.Underwriting.Conditions;
using DataAccess;
using System.Text.RegularExpressions;

namespace LendersOfficeApp.newlos.Tasks
{
    public partial class TaskViewer : BaseServicePage
    {
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        protected string TaskID
        {
            get { return RequestHelper.GetSafeQueryString("taskId"); }
        }

        protected string LoanID
        {
            get;set;
        }

        protected bool IsTemplate
        {
            get { return RequestHelper.GetBool("IsTemplate"); }
        }

        protected BrokerUserPrincipal BrokerUser
        {
            get
            {
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
                if (null == principal)
                    RequestHelper.Signout();

                return principal;
            }
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Task Viewer";
            
            Task task = null;
            try
            {
                task = Task.Retrieve(BrokerUser.BrokerId, TaskID);
                LoanID = task.LoanId.ToString();
            }
            catch (TaskNotFoundException)
            {
                LoanID = Guid.Empty.ToString();
                DisplayError("The selected task does not exist.");
                return;
            }
            catch (TaskPermissionException)
            {
                LoanID = Guid.Empty.ToString();
                DisplayError("You do not have access to the selected task.");
                return;
            }

            m_Header.Text = (task.TaskIsCondition ? "Condition " : "Task ") + task.TaskId;
            if (Task.CanUserViewHiddenInformation(BrokerUser))
            {
                ConditionIsHidden.Text = task.CondIsHidden_rep;
            }
            else
            {
                if (task.CondIsHidden)
                {
                    throw new CBaseException(ErrorMessages.GenericAccessDenied, "User doesn't have permission to see hidden conditions");
                }
                IsHiddenLabel.Visible = false;
            }
            Subject.Text = task.TaskSubject;
            BorrowerName.Text = task.BorrowerNmCached;
            TemplateOwner.Text = TaskOwner.Text = task.OwnerFullName;
            TemplateAssigned.Text = Assigned.Text = task.AssignedUserFullName;
            sLNm.Text = task.LoanNumCached;
            Status.Text = task.TaskStatus_rep;
            DueDateCalcDescription.Text = task.DueDateDescription(true);
            History.Text = task.TaskHistoryHtml;
            DueDate.Text = task.TaskDueDate_rep;
            if (task.TaskDueDate.HasValue && task.TaskDueDate.Value <= DateTime.Today && task.TaskStatus == E_TaskStatus.Active)
            {
                DueDate.Style.Add("color", "Red");
            }
            FollowupDate.Text = task.TaskFollowUpDate_rep;
            if (task.TaskFollowUpDate.HasValue && task.TaskFollowUpDate.Value <= DateTime.Today && task.TaskStatus == E_TaskStatus.Active)
            {
                FollowupDate.Style.Add("color", "Red");
            }
            ConditionCategoryChoice.Text = task.CondCategoryId_rep;
            ConditionInternalNotes.Text = task.CondInternalNotes;
            m_conditionSection.Visible = task.TaskIsCondition;
            RequiredDocTypeSection.Visible = BrokerDB.RetrieveById(BrokerUser.BrokerId).IsEDocsEnabled && task.TaskIsCondition;
            RequiredDocumentTypeName.Text = task.CondRequiredDocType_rep; 

            TaskPermission.Text = task.PermissionLevelName;
            PermissionDescription.Value = task.PermissionLevelDescription;
            TaskRow1.Visible = TaskRow2.Visible = FollowUpDateRow.Visible = !IsTemplate;
            TemplateRow.Visible = IsTemplate;

            if (TaskSubscription.IsSubscribed(BrokerUser.BrokerId, task.TaskId, BrokerUser.UserId))
            {
                IsNotSubscribed.Style.Add("display", "none");
            }
            else
            {
                IsSubscribed.Style.Add("display", "none");
            }

            bool isTaskActive = task.TaskStatus == E_TaskStatus.Active;
            bool isTaskResolved = task.TaskStatus == E_TaskStatus.Resolved;
            bool isTaskClosed = task.TaskStatus == E_TaskStatus.Closed;
            E_UserTaskPermissionLevel permissionLevel = task.GetPermissionLevelFor(BrokerUser);

            m_activateLink.Visible = !IsTemplate &&
                                     (isTaskClosed || isTaskResolved) &&
                                     permissionLevel == E_UserTaskPermissionLevel.Manage;
            
            m_assignLink.Visible = !IsTemplate &&
                                   isTaskActive;
            
            m_closeLink.Visible = !IsTemplate &&
                                  isTaskResolved &&
                                  (permissionLevel == E_UserTaskPermissionLevel.Manage || permissionLevel == E_UserTaskPermissionLevel.Close);
            
            m_editLink.Visible = isTaskActive ||
                                 isTaskResolved;
            
            m_emailLink.Visible = !IsTemplate &&
                                  isTaskActive &&
                                  !task.HasEmailEntry;
            
            m_replyLink.Visible = !IsTemplate &&
                                  isTaskActive &&
                                  task.HasEmailEntry;
            
            m_forwardLink.Visible = !IsTemplate &&
                                    isTaskActive &&
                                    task.HasEmailEntry;
            
            m_resolveLink.Visible = !IsTemplate &&
                                    isTaskActive;


            ClientScript.GetPostBackEventReference(this, "");
            if (permissionLevel == E_UserTaskPermissionLevel.Manage)
            {
                ClientScript.RegisterHiddenField("ClosePrompt", "Bypass");
            }
            else
            {
                ClientScript.RegisterHiddenField("ClosePrompt", "Block");
            }

            ResolutionSettingsRow.Visible = !string.IsNullOrEmpty(task.ResolutionBlockTriggerName) || !string.IsNullOrEmpty(task.ResolutionDateSetterFieldId);
            if (string.IsNullOrEmpty(task.ResolutionBlockTriggerName))
            {
                ResolutionBlockTriggerNameLabel.Text = "None";
                ResolutionBlockTriggerExplainLink.Visible = false;
            }
            else
            {
                ResolutionBlockTriggerNameLabel.Text = task.ResolutionBlockTriggerName;
            }
            
            ResolutionDenialMessage.Value = task.ResolutionDenialMessage;
            ResolutionDateSetterFieldDescription.Text = task.GetResolutionDateSetterFieldDescription();

            IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation> triggers = TaskTriggerAssociation.GetTaskTriggerAssociations(task);

            AutoTriggersRow.Visible = triggers.ContainsKey(TaskTriggerType.AutoResolve) || triggers.ContainsKey(TaskTriggerType.AutoClose);
            AutoResolveTriggerNameLabel.Text = triggers.ContainsKey(TaskTriggerType.AutoResolve) ? triggers[TaskTriggerType.AutoResolve].TriggerName : "None";
            AutoCloseTriggerNameLabel.Text = triggers.ContainsKey(TaskTriggerType.AutoClose) ? triggers[TaskTriggerType.AutoClose].TriggerName : "None";
        }

        protected override void OnInit(EventArgs e)
        {
            int l = VirtualRoot.Length;
            string url = Request.Path.Substring(l, Request.Path.Length - l - 5) + "Service.aspx";
            RegisterService("loanedit", url);
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        private void DisplayError(string msg)
        {
            pnlError.Visible = true;
            pnlTaskViewer.Visible = false;
            lblMessage.Text = msg;
        }
    }
}
