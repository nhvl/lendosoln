﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LendersOffice.ObjLib.Task;
using LendersOffice.Constants;
using LendersOffice.ConfigSystem;

namespace LendersOfficeApp.newlos.Tasks
{
    public partial class TaskEditorV2Service : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CheckOwnerPermission":
                    CheckOwnerPermission();
                    break;                   
            }
        }

        private void CheckOwnerPermission()
        {
            Guid ownerID = GetGuid("ownerID");
            int permissionLevelID = GetInt("permissionLevelID");
            Guid brokerID = BrokerUserPrincipal.CurrentPrincipal.BrokerId;

            AbstractUserPrincipal principal = PrincipalFactory.RetrievePrincipalForUser(brokerID, ownerID, "?") as AbstractUserPrincipal;
            if (principal == null)
            {
                SetResult("hideError", true);
                return;
            }

            // Permission level check
            List<Guid> UserGroups = new List<Guid>();
            foreach (Group ugroup in GroupDB.ListInclusiveGroupForEmployee(brokerID, principal.EmployeeId))
            {
                UserGroups.Add(ugroup.Id);
            }
            List<E_RoleT> Roles = new List<E_RoleT>();
            foreach (E_RoleT role in principal.GetRoles())
            {
                Roles.Add(role);
            }
            PermissionLevel selectedPermission = PermissionLevel.Retrieve(brokerID, permissionLevelID);            
            SetResult("hideError", selectedPermission.IsRoleOrGroupInLevel(E_PermissionLevel.Manage, Roles, UserGroups));
        }
    }
}
