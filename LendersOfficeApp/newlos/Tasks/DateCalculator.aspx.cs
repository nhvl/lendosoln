﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.ObjLib.Task;

namespace LendersOfficeApp.newlos.Tasks
{
    public partial class DateCalculator : BaseServicePage
    {
        public string duration
        {
            get
            {
                int d = RequestHelper.GetInt("duration", 0);
                if (d < 0)
                {
                    return (d * -1).ToString();
                }
                else
                {
                    return d.ToString();
                }
            }
        }

        public bool positiveDuration
        {
            get
            {
                return RequestHelper.GetInt("duration", 0) > -1;
            }
        }

        public string fieldId
        {
            get
            {
                return RequestHelper.GetSafeQueryString("fieldId");
            }
        }

        protected bool IsResolutionDateSetter
        {
            get
            {
                return RequestHelper.GetBool("IsResolutionDateSetter");
            }
        }

        protected bool IsConsumerPortalDates
        {
            get
            {
                return RequestHelper.GetBool("IsConsumerPortalDates");
            }
        }

        protected bool IsSimpleDatePicker
        {
            get
            {
                return this.IsResolutionDateSetter || this.IsConsumerPortalDates;
            }
        }

        public bool IsTaskTriggerTemplate
        {
            get
            {
                return RequestHelper.GetBool("IsTaskTriggerTemplate");
            }
        }

        public string friendlyName { get; set; }
        public string id { get; set; }

        string category = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsResolutionDateSetter)
            {
                m_parameterList.DataSource = Task.ListValidResolutionDateSetterParameters();
            }
            else if (IsTaskTriggerTemplate)
            {
                m_parameterList.DataSource = TaskTriggerTemplate.ListValidDateFieldParameters();
            }
            else if (this.IsConsumerPortalDates)
            {
                m_parameterList.DataSource = Task.ListValidDateFieldParameters()
                    .OrderBy(p => p.CategoryName)
                    .ThenBy(p => p.FriendlyName);
            }
            else
            {
                m_parameterList.DataSource = Task.ListValidDateFieldParameters();
            }

            m_parameterList.DataBind();

            m_duration.Value = duration;
            m_after.Checked = positiveDuration;
            m_before.Checked = !positiveDuration;

            if (this.IsSimpleDatePicker)
            {
                CalculationDetailsCells.Visible = false;
            }

            if (IsResolutionDateSetter)
            {
                HeaderText.Text = "Select a date to set on task resolution:";
            }
            else if (this.IsConsumerPortalDates)
            {
                this.HeaderText.Text = "Select a date to display to the consumer:";
            }

            IEnumerable<SecurityParameter.Parameter> parameters;
            if (IsResolutionDateSetter)
            {
                parameters = Task.ListValidResolutionDateSetterParameters();
            }
            else
            {
                parameters = Task.ListValidDateFieldParameters();
            }

            SecurityParameter.Parameter param = null;
            try
            {
                param = parameters.First(t => t.Id == fieldId);
            }
            catch (InvalidOperationException) { }

            if (string.IsNullOrEmpty(fieldId) || param == null)
            {
                friendlyName = parameters.ElementAt(0).FriendlyName;
                id = parameters.ElementAt(0).Id;
            }
            else
            {
                friendlyName = param.FriendlyName;
                id = fieldId;
            }
        }

        protected void OnParameterListBound(object sender, RepeaterItemEventArgs args)
        {            
            DataAccess.SecurityParameter.Parameter parameter = args.Item.DataItem as DataAccess.SecurityParameter.Parameter;
            Literal ParameterData = args.Item.FindControl("m_parameterData") as Literal;
            HtmlGenericControl ParamDiv = args.Item.FindControl("m_choiceDiv") as HtmlGenericControl;
            Literal HeaderData = args.Item.FindControl("m_headerName") as Literal;
            HtmlGenericControl HeaderDiv = args.Item.FindControl("m_headerDiv") as HtmlGenericControl;

            if (category != parameter.CategoryName)
            {
                category = parameter.CategoryName;
                HeaderData.Text = category;
            }
            else
            {
                HeaderDiv.Visible = false;
            }
            ParameterData.Text = string.Format("{0} ({1})", parameter.FriendlyName, parameter.Id);
            ParamDiv.Attributes.Add("onclick", string.Format("selectField('{0}', '{1}');return false;", parameter.Id, parameter.FriendlyName));
            ParamDiv.Attributes.Add("category", parameter.CategoryName);
        }

        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("common.js");
            this.RegisterJsScript("simpleservices.js");
            this.RegisterJsScript("loanframework2.js");
            int l = VirtualRoot.Length;
            string url = Request.Path.Substring(l, Request.Path.Length - l - 5) + "Service.aspx";
            RegisterService("main", url);
            RegisterJsScript("CrossBrowserPolyfill.js");
            RegisterJsScript("webmethodServices.js");
            RegisterJsScript("LQBPopup.js");
        }
    }
}
