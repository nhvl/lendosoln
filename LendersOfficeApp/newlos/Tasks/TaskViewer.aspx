﻿<%@ Page Language="C#" EnableViewState="false" AutoEventWireup="true" CodeBehind="TaskViewer.aspx.cs" Inherits="LendersOfficeApp.newlos.Tasks.TaskViewer" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Task Viewer</title>
        <style type="text/css">
        .SubHeader
        {
            padding: 3px;
            text-align: right;
            font-weight: bold;
        }
        
        .ErrorMessage
        {
            font-weight: bold;
            color:Red;
            padding:0.5em;
        }
        .w-80 { width: 80px; }

        .white-space-pre-line { white-space: pre-line; }
    </style>
    
    <script type="text/javascript">
        function onload() {
            var errorD = document.getElementById("pnlError");            
            if (errorD != null)
	            resizeForIE6And7(200, 100);
	         else
	             resizeForIE6And7(750, 400);
	    }
    </script>
</head>
<body bgcolor="gainsboro" onload="onload();">
	 <script type="text/javascript">
	    function subscribe(toSubscribe) {
            var input = new Object();
            input["UserId"] = <%= AspxTools.JsString(BrokerUser.UserId.ToString()) %>;
            input["TaskId"] = <%= AspxTools.JsString(TaskID) %>;
            input["ToSubscribe"] = toSubscribe;
            var result = gService.loanedit.call("ChangeSubscription", input);
            document.getElementById('IsNotSubscribed').style.display = toSubscribe ? 'none' : '';
            document.getElementById('IsSubscribed').style.display = toSubscribe ? '' : 'none';
	    }
	    
	    function closeTask() {
	        var prompt = document.getElementById('ClosePrompt'); 
	        
	        if (prompt) {
	            if (prompt.value === 'Bypass' && !confirm('This condition is missing an association with a required document type. Are you sure you want to close?')){
	                return false;
	            }
	            else if (prompt.value === 'Block') {
	                alert('This condition is missing an association with a required document type. Please associate with the required document type in order to close.');
	                return false;
	            }
	        }
	        
            var input = new Object();
            input["BrokerId"] = <%= AspxTools.JsString(BrokerUser.BrokerId.ToString()) %>;
            input["TaskId"] = <%= AspxTools.JsString(TaskID) %>;
            var result = gService.loanedit.call("CloseTask", input);
            attemptTaskListRefresh();
            onClosePopup();
	    }
	    
	    function attemptTaskListRefresh() {	        
            if(window.opener != null && !window.opener.closed)
            {
                window.opener.document.forms[0].submit();                
            }
	    }
	    
	    function openEditor(op) {
	        window.open("TaskEditorV2.aspx?loanId=<%=AspxTools.HtmlString(LoanID.ToString())%>&IsTemplate=<%=AspxTools.HtmlString(IsTemplate.ToString())%>&taskId=<%=AspxTools.HtmlString(TaskID.ToString())%>&mode=" + op,
	            '_self');
	    }
    </script>
    <form id="form1" runat="server">
        <asp:HiddenField runat="server" ID="RequiresDocAssociation" />
    <asp:Panel ID="pnlError" runat="server" Visible="false">
        <div class="FormTableSubheader" style=" padding:0.25em">ERROR</div>
        
        <div style="text-align:center; padding-top:0.5em">
        <ml:EncodedLabel runat="server" ID="lblMessage" CssClass="ErrorMessage"></ml:EncodedLabel>
        <br />
        <br />        
        <input type="button" value="Close" onclick="javascript:onClosePopup()" />
        </div>
        
    </asp:Panel>
    
    <asp:Panel ID="pnlTaskViewer" runat="server">
    
    <asp:HiddenField ID="PermissionDescription" runat="server" />
    
    <asp:HiddenField ID="ResolutionDenialMessage" runat="server" />
    
        <table width="100%" cellSpacing="0" cellPadding="4" border="0">
		    <tr class="FormTableSubheader" style="padding:3px">
			    <td class="w-80">
			        <ml:EncodedLabel ID="m_Header" runat="server"></ml:EncodedLabel>
			    </td>
			    <td colspan="5" align="right">
			        <a id="m_editLink" runat="server" href="#" onclick="openEditor('edit');">Edit</a>
			        <a id="m_assignLink" runat="server" href="#" onclick="openEditor('edit');">Assign</a>
			        <a id="m_emailLink" runat="server" href="#" onclick="openEditor('email');">E-mail</a>
			        <a id="m_replyLink" runat="server" href="#" onclick="openEditor('reply');">Reply</a>
			        <a id="m_forwardLink" runat="server" href="#" onclick="openEditor('forward');">Forward</a>
			        <a id="m_resolveLink" runat="server" href="#" onclick="openEditor('resolve');">Resolve</a>
			        <a id="m_activateLink" runat="server" href="#" onclick="openEditor('activate');">Re-activate</a>
			        <a id="m_closeLink" runat="server" href="#" onclick="closeTask();">Close</a>
			        <input type="button" value="Exit" onclick="onClosePopup();" />
			    </td>
		    </tr>
            <tr>
                <td class="SubHeader" style="font-size:small">
                    Subject
                </td>
		        <td class="white-space-pre-line" colspan="5" style="font-size:small">
		            <ml:EncodedLabel ID="Subject" runat="server"></ml:EncodedLabel>
		        </td>
		    </tr>
		    <tr id="TaskRow1" runat="server">
		        <td class="SubHeader">
		            Assigned to
		        </td>
		        <td>
		            <ml:EncodedLabel ID="Assigned" runat="server"></ml:EncodedLabel>
		        </td>
		        <td class="SubHeader">
		            Borrower
		        </td>
		        <td>
		            <ml:EncodedLabel ID="BorrowerName" runat="server"></ml:EncodedLabel>
		        </td>
		        <td class="SubHeader">
		            Loan Number
		        </td>
		        <td>
		            <ml:EncodedLabel ID="sLNm" runat="server"></ml:EncodedLabel>
		        </td>
            </tr>
		    <tr id="TaskRow2" runat="server">
		        <td class="SubHeader">
		            Status
                </td>
                <td>
		            <ml:EncodedLabel ID="Status" runat="server"></ml:EncodedLabel>
                </td>
                <td colspan="2">
                </td>
                <td class="SubHeader">
                    Task Owner
                </td>
                <td>
		            <ml:EncodedLabel ID="TaskOwner" runat="server"></ml:EncodedLabel>
                </td>
		    </tr>
		    <tr id="TemplateRow" runat="server">
		        <td class="SubHeader">
		            To Be Assigned To
		        </td>
		        <td colspan="3">
		            <ml:EncodedLabel ID="TemplateAssigned" runat="server"></ml:EncodedLabel>
		        </td>
		        <td class="SubHeader">
		            To Be Owned By
		        </td>
		        <td>
		            <ml:EncodedLabel ID="TemplateOwner" runat="server"></ml:EncodedLabel>
		        </td>
            </tr>
		    <tr id="ResolutionSettingsRow" runat="server">
		        <td class="SubHeader">
		            Resolution Block Trigger
                </td>
                <td colspan="3">
		            <ml:EncodedLabel ID="ResolutionBlockTriggerNameLabel" runat="server"></ml:EncodedLabel>
		            <a id="ResolutionBlockTriggerExplainLink" runat="server" onclick="alert(document.getElementById('ResolutionDenialMessage').value);return false;">?</a>                    
                </td>
                <td class="SubHeader">
                    Resolution Date Setter
                </td>
                <td>
		            <ml:EncodedLabel ID="ResolutionDateSetterFieldDescription" runat="server"></ml:EncodedLabel>
                </td>
		    </tr>
            <tr id="AutoTriggersRow" runat="server">
		        <td class="SubHeader">
		            Auto-Resolve Trigger
                </td>
                <td colspan="3">
		            <ml:EncodedLabel ID="AutoResolveTriggerNameLabel" runat="server"></ml:EncodedLabel>
                </td>
                <td class="SubHeader">
		            Auto-Close Trigger
                </td>
                <td colspan="3">
		            <ml:EncodedLabel ID="AutoCloseTriggerNameLabel" runat="server"></ml:EncodedLabel>
                </td>
		    </tr>
		    <tr>
		        <td class="SubHeader">
		            Due Date
                </td>
                <td colspan="3">
		            <ml:EncodedLabel ID="DueDate" runat="server"></ml:EncodedLabel>
		            <ml:EncodedLabel ID="DueDateCalcDescription" runat="server"></ml:EncodedLabel>
                </td>
                <td class="SubHeader">
                    Task Permission
                </td>
                <td>
		            <ml:EncodedLabel ID="TaskPermission" runat="server"></ml:EncodedLabel>
		            <a href="#" onclick="alert(document.getElementById('PermissionDescription').value);return false;">?</a>                    
                </td>
		    </tr>
		    <tr id="FollowUpDateRow" runat="server">
		        <td class="SubHeader">
		            Follow-up Date
		        </td>
		        <td>
		            <ml:EncodedLabel ID="FollowupDate" runat="server"></ml:EncodedLabel>
			    </td>
		    </tr>
		    <tbody id="m_conditionSection" runat="server">
		    <tr>
		        <td class="FormTableSubheader" colspan="6" style="padding:3px">
		            Condition Info
		        </td>
		    </tr>
		    <tr>
		        <td class="SubHeader">
		            Category
		        </td>
		        <td>
		            <ml:EncodedLabel ID="ConditionCategoryChoice" runat="server"></ml:EncodedLabel>
			    </td>
		        <td class="SubHeader">
		            <ml:EncodedLabel AssociatedControlID="ConditionIsHidden" ID="IsHiddenLabel" runat="server" Text="Hidden"></ml:EncodedLabel>		            
		        </td>
		        <td>
		            <ml:EncodedLabel ID="ConditionIsHidden" runat="server"></ml:EncodedLabel>
			    </td>
			    <asp:PlaceHolder runat="server" ID="RequiredDocTypeSection">
			    <td  class="SubHeader">
			        Required Document Type
			    </td>
			    <td>
			        <ml:EncodedLabel runat="server" ID="RequiredDocumentTypeName"></ml:EncodedLabel> 
			    </td>
			    </asp:PlaceHolder>
		    </tr>
		    <tr>
		        <td class="SubHeader" valign="top">
		            Internal Notes
		        </td>
		        <td colspan="5">
	                <asp:TextBox ID="ConditionInternalNotes" TextMode="MultiLine" Rows="6" Width="500px" runat="server" ReadOnly="true"></asp:TextBox>
		        </td>
		    </tr>
		    </tbody>
		    <tr>
		        <td colspan="6">
		            <hr />
		        </td>
		    </tr>
		    <tr>
		        <td colspan="6">
                    <ml:PassthroughLabel ID="History" runat="server"></ml:PassthroughLabel>
		        </td>
		    </tr>
		    <tr>
		        <td colspan="6">
		            <hr />
		        </td>
		    </tr>
		    <tr id="IsNotSubscribed" runat="server">
		        <td colspan="6">
		            <a href="#" onclick="subscribe(true);">Subscribe</a> to receive e-mail notification when this task is updated
		        </td>
		    </tr>
		    <tr id="IsSubscribed" runat="server">
		        <td colspan="6">
		            <a href="#" onclick="subscribe(false);">Unsubscribe</a> to be removed from e-mail notifications related to this task
		        </td>
		    </tr>
        </table>
        
        </asp:Panel>
    </form>
</body>
</html>
