﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="TaskListSetDueDateModal.aspx.cs" Inherits="LendersOfficeApp.newlos.Tasks.TaskListSetDueDateModal" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Task List Set Due Date Modal</title>
    <link href="../../css/stylesheet.css" type=text/css rel=stylesheet />
    <style type="text/css">
        #BottomButtons
        {
            text-align: center;
        }
        .td_DueDateCalcDescription
        {
        	width: 50%
        }
    </style>
</head>
<body>
    <h4 class="page-header">Set Due Date</h4>
    <form id="TaskListSetDueDateModal" runat="server">
    <script type="text/javascript">
        function _init() {
        
            function lockDateBox()
	        {
	            if ($m_DueDateLockBox.prop("checked")) {
	                $m_DueDate.prop("readonly", false);
	            } else {
	                $m_DueDate.prop("readonly", true);
	            }
	        }

            $m_DueDateLockBox = $("#TaskDueDateLocked");
            $m_DueDate = $("#TaskDueDate");
    	    $m_DueDateLockBox.click(function() {
    	        if ($m_DueDateLockBox.prop("checked")) {
    	            $("#TaskDueDateCalcDays").val("");
	                $("#TaskDueDateCalcField").val("");
	                $("#m_DueDateCalcDescription").text("");
	                $(".td_DueDateCalcDescription").css("display", "none");
    	            $m_DueDate.prop("readonly", false);
    	        } else {
    	            $m_DueDate.prop("readonly", true);
    	        }
    	    });

    	    $("#calculateLink").click(function() {
    	        // pre-populate in case user hits cancel or close and somehow sends an OK back
    	        var dateArgs = window.dialogArguments || {};
    	        dateArgs.offset = $("#TaskDueDateCalcDays").val();
                dateArgs.id = $("#TaskDueDateCalcField").val();
                dateArgs.dateDescription = $("#m_DueDateCalcDescription").text();
                dateArgs.date = $("#TaskDueDate").val();
                var dateCalculatorAddress = "/newlos/Tasks/DateCalculator.aspx";
                var queryString = "?duration=" + dateArgs.offset + "&fieldId=" + dateArgs.id;
                // Since we could also be called by the pipeline, we should account for the case where there is no loanId
                <% if (AspxTools.HtmlString(LoanID) != "") { %>
                    queryString += "&loanid=<%= AspxTools.HtmlString(LoanID) %>";
                <% } %>
    	        showModal(dateCalculatorAddress + queryString, null, null, null, function(dateArgs){
                    if (dateArgs.OK) {
                        $("#TaskDueDateCalcDays").val(dateArgs.offset);
                        $("#TaskDueDateCalcField").val(dateArgs.id);
                        $("#m_DueDateCalcDescription").text(dateArgs.dateDescription);
                        $(".td_DueDateCalcDescription").css("display", "");
                        $m_DueDate.val(dateArgs.date);
                        $m_DueDateLockBox.prop("checked", false);
                        $m_DueDate.prop("readonly", true);
                    }
                },{hideCloseButton:true});
            });

	        $("#OKBtn").click(function() {
	            var dateString = $("#TaskDueDate").val();
	            var dateDescription = $("#m_DueDateCalcDescription").text();
	            var dateLocked = $m_DueDateLockBox.prop("checked") ? true : false;
	            var date = new Date(dateString);
	            if (dateDescription == "")
	            {
	                if (isNaN(date)) {
	                    alert("Please select a valid date.");
	                    return false;
	                }
	            }
	            var result = window.dialogArguments || {};
	            result.offset = $("#TaskDueDateCalcDays").val();
                result.id = $("#TaskDueDateCalcField").val();
                result.dateDescription = dateDescription;
                result.dateLocked = dateLocked;
                result.date = dateString;
                result.OK = true;
                onClosePopup(result);
	        });

	        $("#cancelBtn").click(function() {
	            var result = window.dialogArguments || {};
	            result.OK = false;
	            onClosePopup(result);
	        });
	        
	        // Start it unlocked
	        $m_DueDateLockBox.click();
	        lockDateBox();

	        $(".td_DueDateCalcDescription").css("display", "none");

	        resizeForIE6And7(500, 300);
        }
    </script>
    <asp:HiddenField ID="TaskDueDateCalcDays" runat="server" />
    <asp:HiddenField ID="TaskDueDateCalcField" runat="server" />
    <div>        
        <div style="text-align: center">
        <table> <!-- Makin a table for alignment issues with the datebox -->
            <tr>
                <td>
                    Due Date
                </td>
                <td>
                    <asp:CheckBox ID="TaskDueDateLocked" runat="server" />
                </td>
                <td>
                    <ml:DateTextBox id="TaskDueDate" runat="server"></ml:DateTextBox>
                </td>
                <td>
                    <a id="calculateLink" href="#" title="Calculate the due date based on loan date">Calculate</a>
                </td>
                <td id="td_DueDateCalcDescription">
                    <span id="m_DueDateCalcDescription"></span>
                </td>
            </tr>
        </table>
        </div>
        <div id="BottomButtons">
            <input type="button" id="OKBtn" value="Save" />
            <input type="button" id="cancelBtn" value="Cancel" />
        </div>
    </div>
    <uc1:cModalDlg ID="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cModalDlg>
    </form>
</body>
</html>
