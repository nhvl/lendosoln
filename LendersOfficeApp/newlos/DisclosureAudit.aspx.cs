﻿namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public partial class DisclosureAudit : BaseLoanPage
    {
        private struct BorrowerAuditInfo
        {
            public readonly string Name;
            public readonly string Email;
            public readonly string EConsentReceivedD;
            public readonly string EConsentDeclinedD;

            public BorrowerAuditInfo(string name, string email, string eConsentReceivedD, string eConsentDeclinedD)
            {
                Name = name;
                Email = email;
                EConsentReceivedD = eConsentReceivedD;
                EConsentDeclinedD = eConsentDeclinedD;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            EnableJquery = true;

            var principal = PrincipalFactory.CurrentPrincipal;
            if (!principal.HasPermission(Permission.ResponsibleForInitialDiscRetail)
                && !principal.HasPermission(Permission.ResponsibleForInitialDiscWholesale)
                && !principal.HasPermission(Permission.ResponsibleForRedisclosures))
            {
                throw new AccessDenied();
            }

            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            Guid sLId = RequestHelper.LoanID;
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DisclosureAudit));
            dataLoan.InitLoad();

            // Make a list of borrowers 
            var borrowerInfo = new List<BorrowerAuditInfo>();
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);

                borrowerInfo.Add(new BorrowerAuditInfo(dataApp.aBNm, dataApp.aBEmail, 
                    dataApp.aBEConsentReceivedD_rep, dataApp.aBEConsentDeclinedD_rep));
                if (dataApp.aCIsValidNameSsn)
                {
                    borrowerInfo.Add(new BorrowerAuditInfo(dataApp.aCNm, dataApp.aCEmail,
                        dataApp.aCEConsentReceivedD_rep, dataApp.aCEConsentDeclinedD_rep));
                }
            }

            BorrowerEntries.DataSource = borrowerInfo;
            BorrowerEntries.DataBind();

            var disclosureAuditItems = AuditManager.RetrieveAuditList(sLId)
                .Where(audit => audit.CategoryT == E_AuditItemCategoryT.DisclosureESign);
            DisclosureHistoryEntries.DataSource = disclosureAuditItems;
            DisclosureHistoryEntries.DataBind();

            base.OnLoad(e);
        }

        protected void BorrowerEntries_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            var borrowerInfo = (BorrowerAuditInfo)args.Item.DataItem;

            var row = args.Item.FindControl("BorrRow") as HtmlTableRow;
            var cssClass = args.Item.ItemType == ListItemType.AlternatingItem ? "GridAlternatingItem" : "GridItem";
            row.Attributes["class"] = string.Format("{0} {1}", row.Attributes["class"], cssClass);

            var name = args.Item.FindControl("Name") as Literal;
            name.Text = borrowerInfo.Name;

            var email = args.Item.FindControl("Email") as HtmlAnchor;
            email.HRef = string.Format("mailto:{0}", AspxTools.HtmlString(borrowerInfo.Email));
            email.InnerText = borrowerInfo.Email;

            var recD = args.Item.FindControl("EConsentReceivedD") as Literal;
            recD.Text = borrowerInfo.EConsentReceivedD;

            var decD = args.Item.FindControl("EConsentDeclinedD") as Literal;
            decD.Text = borrowerInfo.EConsentDeclinedD;
        }
        
        protected void DisclosureHistoryEntries_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            var auditItem = args.Item.DataItem as LightWeightAuditItem;
            if (auditItem == null) return;

            var row = args.Item.FindControl("AuditRow") as HtmlTableRow;
            var cssClass = args.Item.ItemType == ListItemType.AlternatingItem ? "GridAlternatingItem" : "GridItem";
            row.Attributes["class"] = string.Format("{0} {1}", row.Attributes["class"], cssClass);

            var username = args.Item.FindControl("Username") as Literal;
            username.Text = auditItem.UserName;

            var timestampDesc = args.Item.FindControl("TimestampDescription") as Literal;
            timestampDesc.Text = auditItem.TimestampDescription;

            var desc = args.Item.FindControl("Description") as Literal;
            desc.Text = auditItem.Description;
        }
    }
}
