﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FreddieLoanQualityAdvisorExport.aspx.cs" Inherits="LendersOfficeApp.newlos.FreddieLoanQualityAdvisorExport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Freddie Mac Loan Quality Advisor</title>
</head>
<body>
<script type="text/javascript">
function f_download() {
    window.open('FreddieLoanQualityAdvisorExport.aspx?cmd=download&loanid=' + ML.sLId, '_parent');
    return false;
}
</script>
    <form id="form1" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td nowrap="nowrap" class="MainRightHeader">Freddie Mac Loan Quality Advisor</td>
    </tr>
    <tr>
        <td><input type=button value="Download FHLMC LQA file ..." onclick="return f_download();"/></td>
    </tr>
    </table>
    </form>
</body>
</html>
