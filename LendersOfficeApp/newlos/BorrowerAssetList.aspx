<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<%@ Page Language="c#" CodeBehind="BorrowerAssetList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.BorrowerAssetList" SmartNavigation="False" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html>
<head>
    <title>BorrowerAssetList</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
</head>
<body ms_positioning="FlowLayout" scroll="yes" leftmargin="0" bgcolor="gainsboro" style="cursor: default">
    <script language="javascript">
  <!--

    function _init() {
        list_oTable = document.getElementById('<%= AspxTools.ClientId(m_dg) %>');
        parent.parent_initScreen(2);
    }

    function refresh() {
        self.location = self.location;
    }
    function constructEmptyRow(tr) {
        // Construct empty row.
        var td = tr.insertCell();
        td.innerText = " ";
        td = tr.insertCell();
        td.innerText = " ";
        td = tr.insertCell();
        td.innerText = " ";
        td = tr.insertCell();
        td.setAttribute("align", "right");;
        td.innerText = " ";
    }
    //-->
    </script>

    <form id="BorrowerAssetList" method="post" runat="server">
        <asp:DataGrid ID="m_dg" runat="server" BorderColor="Gainsboro" Width="100%" AutoGenerateColumns="False" EnableViewState="False">
            <AlternatingItemStyle CssClass="GridItem"></AlternatingItemStyle>
            <ItemStyle CssClass="GridItem"></ItemStyle>
            <HeaderStyle CssClass="GridHeader"></HeaderStyle>
            <Columns>
                <asp:TemplateColumn SortExpression="OwnerT" HeaderText="Owner">
                    <HeaderStyle Width="40px"></HeaderStyle>
                    <ItemStyle Width="40px"></ItemStyle>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(displayOwnerType(DataBinder.Eval(Container.DataItem, "OwnerT")))%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn SortExpression="AssetT" HeaderText="Asset Type">
                    <ItemTemplate>
                        <%# AspxTools.HtmlString((displayAssetType(DataBinder.Eval(Container.DataItem, "AssetT"), DataBinder.Eval(Container.DataItem, "OtherTypeDesc").ToString()))) %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn SortExpression="Desc" HeaderText="Description">
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Desc")) %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn SortExpression="Val" HeaderText="Market Value">
                    <HeaderStyle Width="100px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Width="100px"></ItemStyle>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(displayMoneyString(DataBinder.Eval(Container.DataItem, "Val").ToString()))%>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </form>
</body>
</html>
