<%@ Import Namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LoanInfo.ascx.cs" Inherits="LendersOfficeApp.newlos.LoanInfoUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="QRP" TagName="QualRatePopup" Src="~/newlos/Forms/QualRateCalculationPopup.ascx" %>


<QRP:QualRatePopup ID="QualRatePopup" runat="server" />
<script type="text/javascript">
  function _init() {
    f_onchange_sLT();
    var bTemplate = <%= AspxTools.JsBool(m_isTemplate)%> ;
    var sLPurposeTValue = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value;
    var isConstruction = sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.Construct.ToString("D")) %> 
	    || sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.ConstructPerm.ToString("D")) %>;

    var bPurchase = (sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Purchase) %>
                    || (isConstruction && (!ML.AreConstructionLoanCalcsMigrated || ML.IsConstructionAndLotPurchase)));
    var bStreamlineRefi = sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance) %>;
    var bCannotModifyLoanName = <%= AspxTools.JsBool(m_cannotModifyLoanName) %>;
      var isRenovationLoan = <%= AspxTools.JQuery(sIsRenovationLoan)%>.is(':checked');

	document.getElementById('sIsStudentLoanCashoutRefiRow').style.display = sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D")) %> ? "" : "none";
    
    document.getElementById("btnGenerateNewLoanNumber").disabled = bCannotModifyLoanName || isReadOnly() || bTemplate;
    if (bCannotModifyLoanName)
        document.getElementById("btnGenerateNewLoanNumber").title = "You do not have permission to modify the loan number.";
    <%= AspxTools.JsGetElementById(sPurchPrice) %>.readOnly = (ML.AreConstructionLoanCalcsMigrated && isConstruction) || !bPurchase || isReadOnly();
      <%= AspxTools.JsGetElementById(sLAmtLckd) %>.disabled = !bPurchase || isRenovationLoan;
	  
	  var OrigAppraisedVal = document.getElementById("OriginalAppraisedValueSpan");
	  var bIsWoAppraisal = <%= AspxTools.JsGetElementById(sFHAPurposeIsStreamlineRefiWithoutAppr) %>.value == "1";
	  OrigAppraisedVal.style.display = (bStreamlineRefi && bIsWoAppraisal) ? "inline" : "none";
	  
	  var sLAmtLckdChecked = <%= AspxTools.JsGetElementById(sLAmtLckd) %>.checked;
	  <%= AspxTools.JsGetElementById(sDownPmtPc) %>.readOnly = sLAmtLckdChecked;
	  <%= AspxTools.JsGetElementById(sEquityCalc) %>.readOnly = sLAmtLckdChecked;
	  <%= AspxTools.JsGetElementById(sLAmtCalc) %>.readOnly = !sLAmtLckdChecked;
	  <%= AspxTools.JsGetElementById(sProMIns) %>.readOnly = !<%= AspxTools.JsGetElementById(sProMInsLckd) %>.checked;
	  <%= AspxTools.JsGetElementById(sProOHExp) %>.readOnly = !<%= AspxTools.JsGetElementById(sProOHExpLckd) %>.checked;
	  <%= AspxTools.JsGetElementById(sHighPricedMortgageT) %>.disabled = !<%= AspxTools.JsGetElementById(sHighPricedMortgageTLckd) %>.checked;
	  
	  f_onchange_sQualIR( true );
	  f_onchange_sIsOptionArm( true );
	  f_onchange_sIsLineOfCredit( true );

	  lockField(<%=AspxTools.JsGetElementById(this.sQualIRLckd)%>, 'sQualIR');
      lockField(<%=AspxTools.JsGetElementById(this.sTotalRenovationCostsLckd)%>, 'sTotalRenovationCosts');
	  
    document.getElementById('ConstructionPurposeData').style.display = (!ML.AreConstructionLoanCalcsMigrated && isConstruction) ? "" : "none";

    var isRefinance = sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance.ToString("D")) %>
    || sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.VaIrrrl.ToString("D")) %>
    || sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.Refin.ToString("D")) %>
    || sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.RefinCashout.ToString("D")) %>
    || (isConstruction && ML.AreConstructionLoanCalcsMigrated && ML.IsConstructionPurposeRefi);    
    
      if (isRefinance)
    {
        <%= AspxTools.JsGetElementById(sDownOrEquityLabel) %>.innerText = "Equity";
        <%= AspxTools.JsGetElementById(sDownOrEquityPcLabel) %>.innerText = "Equity (%)";
    }
    else
    {
        <%= AspxTools.JsGetElementById(sDownOrEquityLabel) %>.innerText = "Down Payment";
        <%= AspxTools.JsGetElementById(sDownOrEquityPcLabel) %>.innerText = "Down Payment (%)";
      }

    $("#asIsValueRow").toggle(isRenovationLoan);
    $("#navigateToRenovRow").toggle(isRenovationLoan);           
    $("#inducedPurchPriceRow").toggle(isRenovationLoan && (sLPurposeTValue == 0 || sLPurposeTValue == 3 || sLPurposeTValue == 4)); // loan sLPurposeTValue is purchase, construction, or construct perm   

    var IsEnableRenovationCheckboxInPML = document.getElementById("IsEnableRenovationCheckboxInPML").value === "True";
    var EnableRenovationLoanSupport = document.getElementById("EnableRenovationLoanSupport").value === "True";

    $("#navigateToRenovLink").toggle(EnableRenovationLoanSupport);
    $("#plainRenovText").toggle(IsEnableRenovationCheckboxInPML && !EnableRenovationLoanSupport);

    $(".RenovationContent").toggle(IsEnableRenovationCheckboxInPML || EnableRenovationLoanSupport);

    <%= AspxTools.JsGetElementById(sTotalRenovationCostsLckd) %>.disabled = IsEnableRenovationCheckboxInPML && !EnableRenovationLoanSupport;
            
    if (isRenovationLoan) {
        document.getElementById("appraisedValueLabel").innerText = "As-Completed Value";                
    }
    else {
        document.getElementById("appraisedValueLabel").innerText = "Appraised Value";
    }

    SetQualTermReadonly();
    onPropertyTypeChange();
  }

    function SetQualTermReadonly() {
        var isManual = $(".sQualTermCalculationType").val() == '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Manual)%>';
        $(".sQualTerm").prop("readonly", !isManual);
    }

    function onPropertyTypeChange() {
        var manufacturedTypes = [
            <%=AspxTools.JsString(E_sGseSpT.ManufacturedHomeCondominium)%>,
            <%=AspxTools.JsString(E_sGseSpT.ManufacturedHomeMultiwide)%>,
            <%=AspxTools.JsString(E_sGseSpT.ManufacturedHousing)%>,
            <%=AspxTools.JsString(E_sGseSpT.ManufacturedHousingSingleWide)%>
        ];

        var propertyType = <%=AspxTools.JQuery(this.sGseSpT)%>.val();
        $('#HomeIsMhAdvantageRow').toggle(manufacturedTypes.indexOf(propertyType) !== -1);
    }


    function f_navigateToRenovLoanPage() {
        if (<%=AspxTools.JsGetElementById(sLT)%>.value == 1) { // loan type is FHA
            linkMe('renovation/fha203kmaximummortgage.aspx');
        }
        else {
            linkMe('renovation/fanniemaehomestylemaximummortgage.aspx');
        }
    }

  function _onError(errMsg) {
    if (errMsg == <%= AspxTools.JsString(JsMessages.LoanInfo_DuplicateLoanNumber) %>) {
      
      var val = <%= AspxTools.JsGetElementById(sLNm_Validator) %>;
      val.isvalid = false;
      ValidatorUpdateDisplay(val);
    
    }
    alert(errMsg);
    updateDirtyBit();

  }

  function f_checkBlank() {
    var sLNm = <%= AspxTools.JsGetElementById(sLNm) %>.value;
    if (sLNm == "") {
      var val = <%= AspxTools.JsGetElementById(sLNm_Validator) %>;
      val.isvalid = false;
      ValidatorUpdateDisplay(val);
    }  
  }

  function f_fhaLimitedPage() {
    window.open("https://entp.hud.gov/idapp/html/hicostlook.cfm", 'fhalimit', 'width=780,height=600,menu=no,status=yes,location=no,resizable=yes');
    return false;
  }

  function findCCTemplate() {
    // Before choosing a closing cost template, you always need to save the loan.
    var findTemplate = function(){
      showModal(<%= AspxTools.JsString(ClosingCostPageUrl) %>, null, null, null, function(args){ 
        if (args.OK) {
          self.location = self.location; // Refresh;
        }
      });
    };

    if (isDirty()) {
      PolyConfirmSave(function(confirmed){
        var bSucceeded = confirmed ?  saveMe() : true;
        if (bSucceeded) findTemplate();
      }, true);
    } else {
      findTemplate();
    }
  }
  
  function generateNewLoanName()
  {
    <% if (!m_cannotModifyLoanName) { %>
	  var res;
	  var args = {};

	  args[ "sLNm"   ] = <%= AspxTools.JsGetElementById(sLNm) %>.value;
	  args[ "loanId" ] = ML.sLId;
	  args["_ClientID"] = ML._ClientID;

  	res = gService.loanedit.call(ML._ClientID + "_GenerateNewLoanNumber", args);
	
	if (res.error)
	{
	  alert( "Failed to update loan number.  Please contact your system administrator if the problem persists." );
	  return;	  
	}

	if (res.value.ErrMsg == null)
	{	
	  <%= AspxTools.JsGetElementById(sLNm) %>.value = res.value.NewLoanName;
    triggerEvent(<%= AspxTools.JsGetElementById(sLNm) %>,"change");
	  updateDirtyBit();
    }
    else
	{
	  alert( res.value.ErrMsg );
	}
	<% } %>
  }
 
  function on_sDownPmtPc_change() {
    var p = parseMoneyFloat(<%= AspxTools.JsGetElementById(sPurchPrice) %>.value);
    var sLPurposeTValue = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value;

    if (!ML.AreConstructionLoanCalcsMigrated
        && (sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.Construct) %> 
            || sLPurposeTValue == <%= AspxTools.JsString(E_sLPurposeT.ConstructPerm) %> ))
    {
        p = p + parseMoneyFloat(<%= AspxTools.JsGetElementById(sLandCost) %>.value);
    }

    var a = parseMoneyFloat(<%= AspxTools.JsGetElementById(sApprVal) %>.value);
    
    var minPurchaseOrAppraisal = p;
    if (a > 0 && a < p)
    {
      minPurchaseOrAppraisal = a;
    }
    var d = parseFloat(<%= AspxTools.JsGetElementById(sDownPmtPc) %>.value);
    var e = ((d / 100.0) * minPurchaseOrAppraisal) + 0.005;
    var ctl = <%= AspxTools.JsGetElementById(sEquityCalc) %>;
    ctl.value = e;
    format_money(ctl);
		updateDirtyBit(event);
		refreshCalculation();
  }
	<% if (m_isPmlEnabled) { %>  
  function f_runLPE() {
    linkMe('Template/RunEmbeddedPML.aspx?islpe=t');  
  }
  <% } %>
  function f_runNonLPE() {

    linkMe('Template/QualifiedLoanProgramsearchResultNew.aspx?islpe=f');  
  
  }
  
  function f_onchange_sLT() {
    var v = <%= AspxTools.JsGetElementById(sLT) %>.value;
    var bFHA = v == <%= AspxTools.JsString(E_sLT.FHA.ToString("D")) %>;
    document.getElementById("FHALimitPanel").style.display = bFHA ? "" : "none";
    
    
  }
  
function f_onchange_sIsOptionArm( bIsInit )
{
  if ( ! bIsInit ) refreshCalculation();

  var sOptionArmTeaserR = <%= AspxTools.JsGetElementById(sOptionArmTeaserR) %>;
  var sIsOptionArm = <%= AspxTools.JsGetElementById(sIsOptionArm) %>;
  
    sOptionArmTeaserR.readOnly = <%= AspxTools.JsBool(m_IsRateLocked) %> || ( ! sIsOptionArm.checked );
}
 

function f_onchange_sQualIR( bIsInit )
{
  if ( ! bIsInit ) refreshCalculation();

  var sQualIR = <%= AspxTools.JsGetElementById(sQualIR) %>;
  var sIsQRateIOnly = <%= AspxTools.JsGetElementById(sIsQRateIOnly) %>;
  var sUseQualRate = document.getElementById(QualRateFieldIds.ShouldUseQualRate).checked;
  var sQualIRLckd = <%= AspxTools.JsGetElementById(sQualIRLckd) %>.checked;

  sIsQRateIOnly.disabled = ( sQualIR.value == '0.000%' || sQualIR.value == '' || (!sUseQualRate && !sQualIRLckd));
}

function f_onchange_sIsLineOfCredit( bIsInit )
{
    //OPM 127698 : only show for FTFCU for now
    if (<%= AspxTools.JsBool(!m_showCreditLineFields)%>)
        return;
    if ( ! bIsInit ) refreshCalculation();

    var bIsLineOfCredit = <%= AspxTools.JsGetElementById(sIsLineOfCredit) %>.checked;
    document.getElementById('CreditLineAmtPanel').style.display = bIsLineOfCredit ? 'inline' : 'none';
}
</script>

<%--OPM 127698 : only show for FTFCU for now--%>
<% if (!m_showCreditLineFields) { %>
    <style type="text/css">
        .HideUnlessFTFCU
        {
            display:none;
        }
    </style>
<% } %>

<%--OPM 169478 : only show for brokers with CRMNow Integration--%>
<% if (!m_showCRMID)   { %>
    <style type="text/css">
        .HideUnlessCRMNowEnabled
        {
            display:none;
        }
    </style>
<% } %>

<table id=Table2 cellSpacing=0 cellPadding=0 border=0>
<tr>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table3 cellSpacing=0 cellPadding=0 width="99%" 
      border=0>
        <tr>
          <td class=FieldLabel noWrap>Loan Program</td>
          <td noWrap><asp:textbox id=sLpTemplateNm style="PADDING-LEFT: 4px" tabIndex=1 width="249px" runat="server"></asp:textbox><% if (m_isPmlEnabled) { %>								  
								  <input class=ButtonStyle runat="server" SkipMe="SkipMe" AlwaysEnable="AlwaysEnable" id=btnRunLPE style="WIDTH: 80px" onclick=f_runLPE(); tabIndex=1 type=button value="Run PML">&nbsp;&nbsp;
								<% } %><input class=ButtonStyle id=btnSelectProgram style="WIDTH: 110px" onclick=f_runNonLPE(); type=button value="Select Program"></td></tr>
          <tr id="sLoanProductIdentifierRow" runat="server">
              <td class="FieldLabel" noWrap>Loan Product Identifier</td>
              <td noWrap>
                  <asp:TextBox runat="server" ID="sLoanProductIdentifier" style="PADDING-LEFT: 4px" width="249px"></asp:TextBox>
              </td>
          </tr>
        <tr>
          <td class=FieldLabel noWrap>Closing Cost Template</td>
          <td noWrap><asp:textbox id=sCcTemplateNm style="PADDING-LEFT: 4px" tabIndex=1 width="249px" runat="server">
								</asp:textbox> <input class=ButtonStyle style="WIDTH: 210px" onclick=findCCTemplate(); tabIndex=1 type=button value="Find Closing Cost Template ..."> 
          </td></tr>
        <tr>
          <td class=FieldLabel noWrap>Loan Number</td>
          <td noWrap><asp:textbox id=sLNm style="PADDING-LEFT: 4px" tabIndex=1 width="249px" runat="server">
								</asp:textbox> <input class=ButtonStyle id=btnGenerateNewLoanNumber style="WIDTH: 160px" onclick="if( !parentElement.disabled ) generateNewLoanName();" tabIndex=1 type=button value="Generate new number">&nbsp;&nbsp;&nbsp;&nbsp;<asp:CustomValidator id=sLNm_Validator runat="server" ErrorMessage="Name Taken!" Display="Dynamic" Font-Bold="True"></asp:CustomValidator><asp:requiredfieldvalidator id=sLNm_RequiredValidator runat="server" font-bold="True" ControlToValidate="sLNm" Display="Dynamic" ErrorMessage=" * Loan number cannot be blank">
								</asp:requiredfieldvalidator></td></tr>
        <tr>
          <td class=FieldLabel noWrap>Loan Reference Number</td>
          <td noWrap><asp:textbox id="sLRefNm" style="PADDING-LEFT: 4px" tabIndex="1" width="249px" runat="server" Enabled="false"></asp:textbox></td>
        </tr>
		<tr>
		  <td class=FieldLabel noWrap><span class="HideUnlessCRMNowEnabled">CRM ID</span></td>
		  <td noWrap><asp:textbox id=sCrmNowLeadId style="PADDING-LEFT: 4px" tabIndex=1 width="249px" runat="server" CssClass="HideUnlessCRMNowEnabled">
								</asp:textbox></td></tr></table></td></tr>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table4 cellSpacing=0 cellPadding=0 width="99%" 
      border=0>

          <tr>
              <td>
                  <table>
                      <tr>
                          <td class="FieldLabel" nowrap>Loan Purpose</td>
                          <td nowrap>
                              <asp:DropDownList ID="sLPurposeT" TabIndex="1" Width="130px" runat="server" onchange="refreshCalculation();"></asp:DropDownList></td>
                          <td class="FieldLabel" nowrap>Lien Position</td>
                          <td nowrap>
                              <asp:DropDownList ID="sLienPosT" TabIndex="1" Width="90px" runat="server" onchange="refreshCalculation();"></asp:DropDownList></td>
                          <td class="FieldLabel" nowrap>Loan Type</td>
                          <td nowrap>
                              <asp:DropDownList ID="sLT" TabIndex="1" Width="120px" runat="server" onchange="refreshCalculation();"></asp:DropDownList></td>
                      </tr>
                      <tr id="sIsStudentLoanCashoutRefiRow">
                          <td class="FieldLabel" nowrap>Student Loan Cashout?</td>
                          <td nowrap>
                              <asp:CheckBox ID="sIsStudentLoanCashoutRefi" runat="server" /></td>
                      </tr>
                      <tr>
                          <td class="FieldLabel" nowrap>Subj Prop Occ.</td>
                          <td nowrap>
                              <asp:DropDownList ID="aOccT" TabIndex="1" Width="130px" runat="server" onchange="refreshCalculation();"></asp:DropDownList></td>
                          <td class="FieldLabel" nowrap>Amort. Type</td>
                          <td>
                              <asp:DropDownList ID="sFinMethT" TabIndex="1" Width="90px" runat="server" onchange="refreshCalculation();">
                              </asp:DropDownList></td>
                          <td class="FieldLabel" nowrap>Amort. Desc</td>
                          <td>
                              <asp:TextBox ID="sFinMethDesc" TabIndex="1" Width="120px" runat="server"></asp:TextBox></td>
                      </tr>
                  </table>
              </td>
          </tr>

        <tr id="ConstructionPurposeData">
            <td noWrap>
              <table class=InsetBorder id=Table5 cellSpacing=0 cellPadding=0 width="99%" border=0>
                <tr>
                  <td class=FieldLabel noWrap>Construction Period (mths)</td>
                  <td noWrap><asp:TextBox id=sConstructionPeriodMon runat="server" Width="51px" onchange="refreshCalculation();"></asp:TextBox></td>
                  <td class=FieldLabel noWrap>Construction Improvements Cost</td>
                  <td noWrap><ml:moneytextbox id="sConstructionImprovementAmt" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
                </tr>
                <tr>
                  <td class=FieldLabel noWrap>Construction Period Interest Rate</td>
                  <td noWrap><ml:percenttextbox id="sConstructionPeriodIR" runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
                  <td class=FieldLabel noWrap>Land (if acquired separately)</td>
                  <td noWrap><ml:moneytextbox id="sLandCost" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>          
                </tr>         
              </table>
            </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table5 cellSpacing=0 cellPadding=0 width="99%" 
      border=0>
        <tr>
          <td class=FieldLabel noWrap>Purchase Price</td>
          <td noWrap><ml:moneytextbox id=sPurchPrice tabIndex=2 width="90" runat="server" onchange="refreshCalculation();" preset="money"></ml:moneytextbox></td>
          <td class=FieldLabel noWrap><A title="Go to Upfront MIP/FF screen" tabIndex=-1 href="javascript:linkMe('LoanInfo.aspx?pg=1');" >Upfront 
            MIP / FF</A></td>
          <td noWrap><ml:moneytextbox id=sFfUfmipFinanced tabIndex=1 width="90" runat="server" preset="money" readonly="True"></ml:moneytextbox></td>
          <td class=FieldLabel noWrap><A tabIndex=-1 onclick="redirectToUladPage('MonthlyIncome');" >Monthly 
            Income</A></td>
          <td noWrap><ml:moneytextbox id=sLTotI tabIndex=1 width="90" runat="server" preset="money" readonly="True"></ml:moneytextbox></td>
        </tr>
        <tr id="inducedPurchPriceRow">
            <td class="FieldLabel">
                Inducements to Purchase Price
            </td>
            <td>
                <ml:MoneyTextBox ID="sInducementPurchPrice" runat="server"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
          <td id="appraisedValueLabel" class=FieldLabel noWrap>Appraised Value</td>
          <td noWrap><ml:moneytextbox id=sApprVal tabIndex=2 width="90" runat="server" onchange="refreshCalculation();" preset="money" designtimedragdrop="247"></ml:moneytextbox></td>
          <td class=FieldLabel noWrap>Total Loan Amt</td>
          <td noWrap><ml:moneytextbox id=sFinalLAmt tabIndex=1 width="90" runat="server" preset="money" readonly="True"></ml:moneytextbox></td>
          <td class=FieldLabel noWrap><A tabIndex=-1 onclick="redirectToUladPage('Liabilities');" >Liabilities</A></td>
          <td noWrap><ml:moneytextbox id=sLiaMonLTot tabIndex=1 width="90" runat="server" preset="money" readonly="True"></ml:moneytextbox></td>
        </tr>
        <tr id="asIsValueRow">
            <td class="FieldLabel">
                As-Is Value
            </td>
            <td>
                <ml:MoneyTextBox ID="sFHASpAsIsVal" runat="server"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
          <td class=FieldLabel id = sDownOrEquityPcLabel runat = "server" noWrap title="Includes Other Financing (if any)">Down Payment (%)</td>
          <td noWrap title="Includes Other Financing (if any)"><ml:percenttextbox id=sDownPmtPc tabIndex=2 width="70" runat="server" onchange="on_sDownPmtPc_change(event);" preset="percent"></ml:percenttextbox></td>
          <td class=FieldLabel noWrap>Note Rate</td>
          <td noWrap><ml:percenttextbox id=sNoteIR tabIndex=5 width="70" runat="server" onchange="refreshCalculation();" preset="percent"></ml:percenttextbox></td>
          <td class=FieldLabel noWrap><A href="javascript:linkMe('BorrowerInfo.aspx?pg=7');" title="Present Housing Expense Included in Ratios">Pres 
            House Exp.</A></td>
          <td noWrap title="Amount included in ratios"><ml:moneytextbox id=sPresLTotPersistentHExp tabIndex=1 width="90" runat="server" preset="money" readonly="True"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel id = sDownOrEquityLabel runat = "server" noWrap title="Includes Other Financing (if any)">Down Payment</td>
          <td noWrap title="Includes Other Financing (if any)"><ml:moneytextbox id=sEquityCalc tabIndex=2 width="90" runat="server" onchange="refreshCalculation();" preset="money"></ml:moneytextbox></td>
          <td class=FieldLabel noWrap>Term / Due 
(mths)</td>
          <td noWrap><asp:textbox id=sTerm tabIndex=5 width="37px" runat="server" onchange="refreshCalculation();" maxlength="3"></asp:textbox>/<asp:textbox id=sDue tabIndex=5 width="37px" runat="server" onchange="refreshCalculation();" maxlength="3"></asp:textbox></td>
          <td vAlign=top noWrap colSpan=2 rowSpan=2>
            <table id=Table1 cellSpacing=0 cellPadding=0 border=0 
            >
              <tr>
                <td class=FieldLabel noWrap>Top</td>
                <td noWrap><asp:textbox id=sQualTopR tabIndex=1 runat="server" width="56px" readonly="True"></asp:textbox></td>
                <td class=FieldLabel noWrap>&nbsp;LTV</td>
                <td noWrap><asp:textbox id=sLtvR tabIndex=1 runat="server" width="56px" readonly="True"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Bottom&nbsp;</td>
                <td noWrap><asp:textbox id=sQualBottomR tabIndex=1 width="56px" runat="server" readonly="True"></asp:textbox></td>
                <td class=FieldLabel noWrap>&nbsp;CLTV&nbsp;</td>
                <td noWrap><asp:textbox id=sCltvR tabIndex=1 runat="server" width="56px" readonly="True"></asp:textbox></td></tr></table></td></tr>
        <tr>
          <TD class=FieldLabel noWrap>Loan Amt <asp:checkbox id=sLAmtLckd onclick=refreshCalculation(); runat="server" Text="Lock"></asp:checkbox></TD>
          <TD noWrap><ml:moneytextbox id=sLAmtCalc tabIndex=2 runat="server" width="90" onchange="refreshCalculation();" preset="money"></ml:moneytextbox></TD>
          <TD class=FieldLabel noWrap><a id="QualRateCalcLink">Qual Rate</a></TD>
          <TD noWrap>
              <ml:percenttextbox id="sQualIR" tabIndex=5 runat="server" width="70" onchange="refreshCalculation();" preset="percent"></ml:percenttextbox>
              <asp:CheckBox ID="sQualIRLckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
          </TD>
        </tr>
        <tr>
            <td class="FieldLabel">
                <span class="RenovationContent">Is Renovation Loan</span>
            </td>
            <td>
                <span class="RenovationContent">
                    <asp:CheckBox id="sIsRenovationLoan" runat="server" onclick="refreshCalculation();"/>
                </span>
            </td>
            <td class="FieldLabel">
                Qual Term
            </td>
            <td colspan="4">
                <asp:DropDownList id="sQualTermCalculationType" runat="server" onchange="refreshCalculation();" CssClass="sQualTermCalculationType" />
                &nbsp
                <input type="text" id="sQualTerm" runat="server" preset="numeric" onchange="refreshCalculation();" class="sQualTerm" />
            </td>
        </tr>
        <tr id="navigateToRenovRow">
            <td class="FieldLabel">
                <a id="navigateToRenovLink" href="#" onclick="return f_navigateToRenovLoanPage();">Total Renovation Costs</a>
                <span id="plainRenovText">Total Renovation Costs</span>
            </td>
            <td>
                <ml:MoneyTextBox runat="server" ID="sTotalRenovationCosts" onchange="refreshCalculation();"></ml:MoneyTextBox>
                <asp:CheckBox ID="sTotalRenovationCostsLckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
            </td>
        </tr>
        <TR>
          <td class=FieldLabel nowrap><span class="HideUnlessFTFCU">Is Line of Credit</span></td>
          <td nowrap><asp:CheckBox runat="server" ID="sIsLineOfCredit" onclick="f_onchange_sIsLineOfCredit( false );" CssClass="HideUnlessFTFCU" /></td>
          <td class="FieldLabel" nowrap
            <% if(!m_showCreditLineFields) { %> rowspan="3" <% } %>
          >Qual Rate I/O</td>
          <td
            <% if(!m_showCreditLineFields) { %> rowspan="3" <% } %>
          ><asp:CheckBox runat="server" tabIndex=5 id="sIsQRateIOnly" onclick="f_onchange_sQualIR( false );"></asp:CheckBox></td>
          <td class="FieldLabel" nowrap
            <% if(!m_showCreditLineFields) { %> rowspan="3" <% } %>
          >Option ARM</td>
          <td class="FieldLabel"
            <% if(!m_showCreditLineFields) { %> rowspan="3" <% } %>
          ><asp:CheckBox tabIndex=5 id="sIsOptionArm" onclick="f_onchange_sIsOptionArm( false );" runat="server"></asp:CheckBox></td>
        </TR>
        <tr id="CreditLineAmtPanel" style="display: <%= AspxTools.HtmlString(m_lineAmtDisplay) %>;" >
            <td class="FieldLabel" noWrap>Line Amt</td>
            <td noWrap><ml:moneytextbox id="sCreditLineAmt" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
        </tr>
        <TR>
          <TD class=FieldLabel noWrap
            <% if(m_showCreditLineFields) { %> rowspan="2" <% } %>
          >Subj Prop State</TD>
          <TD noWrap
            <% if(m_showCreditLineFields) { %> rowspan="2" <% } %>
          ><ml:statedropdownlist id=sSpState tabIndex=2 runat="server" onchange="refreshCalculation();" IncludeTerritories="false"></ml:statedropdownlist></TD>
        </TR>
        <tr>
            <td
                <% if(m_showCreditLineFields) { %> colspan="2" <% } else { %> colspan="4"<% } %>
            </td>
            <td class="FieldLabel" nowrap
                <% if(!m_showCreditLineFields) { %> rowspan="2" <% } %>
            >Teaser Rate</td>
            <td nowrap    
            <% if(!m_showCreditLineFields) { %> rowspan="2" <% } %>
            ><ml:percenttextbox id="sOptionArmTeaserR" tabIndex=5 width="70" runat="server" onchange="refreshCalculation();" preset="percent"></ml:percenttextbox></td>
        </tr>
        <TR>
          <TD class=FieldLabel noWrap>Property Type</TD>
          <TD class=FieldLabel noWrap colspan=3><asp:DropDownList id=sGseSpT tabIndex=2 runat="server" onchange="refreshCalculation();"></asp:DropDownList></TD>
        </TR>
        <tr id="HomeIsMhAdvantageRow">
            <td class="FieldLabel">Is home MH Advantage?</td>
            <td>
                <asp:DropDownList runat="server" ID="sHomeIsMhAdvantageTri"></asp:DropDownList>
            </td>
        </tr>
        <TR>
          <td colspan=5>
          <div id="FHALimitPanel" class="FieldLabel">
          <a href="#" onclick= "return f_fhaLimitedPage();">See FHA Mortgage Limits Page ... </a>
          <span id="OriginalAppraisedValueSpan" class="FieldLabel">&nbsp;Original Appraised Value&nbsp;<asp:TextBox ID="sOriginalAppraisedValue" runat="server" onchange="refreshCalculation();" Width="90px" preset="money"></asp:TextBox>
            <input type="hidden" runat="server" id="sFHAPurposeIsStreamlineRefiWithoutAppr" value="0" />
          </span>
          </div>          
          </td>
        </TR>
        <tr>
          <td class=FieldLabel nowrap>
            Higher-priced indicator
          </td>
          <td nowrap>
            <asp:DropDownList runat="server" id="sHighPricedMortgageT"> </asp:DropDownList>
            <asp:CheckBox runat="server" ID="sHighPricedMortgageTLckd" onclick="refreshCalculation();" Text="Lock" />
          </td>
          <td class=FieldLabel nowrap>
            Employee Loan
          </td>
          <td>
            <asp:CheckBox ID="sIsEmployeeLoan" runat="server" />
          </td>
        </tr>
        <tr>
            <td class="FieldLabel" nowrap>
                New Construction
            </td>
            <td>
                <input type="checkbox" id="sIsNewConstruction" runat="server"/>
            </td>
        </tr>
          </table></td></tr>
  <TR>
    <TD noWrap>
      <TABLE class=InsetBorder id=Table6 cellSpacing=0 cellPadding=0 width="99%" 
      border=0>
        <TR>
          <TD class=FieldLabel noWrap colSpan=2><a tabIndex=10 runat="server" id="MortgagePILink" disableforlead>This Mortgage (P &amp; I)</a></TD>
          <TD noWrap></TD>
          <TD noWrap></TD>
          <TD class=FieldLabel noWrap align=right><% if (m_isInterestOnly) { %>(* Interest Only)<% } %></TD>
          <TD noWrap width="90%"><ml:moneytextbox id=sProThisMPmt tabIndex=10 runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap colSpan=2><A tabIndex=10 
            href="javascript:linkMe('LoanInfo.aspx?pg=2');">Other Financing (P 
            &amp; I)</A></TD>
          <TD class=FieldLabel noWrap align=right colSpan=2><ml:EncodedLiteral id=OtherFinancingDescription runat="server"></ml:EncodedLiteral></TD>
          <TD class=FieldLabel noWrap align=right><ml:EncodedLiteral id=sIsIOnlyForSubFin runat="server" Text="(* Interest Only)"></ml:EncodedLiteral></TD>
          <TD noWrap><ml:moneytextbox id=sProOFinPmt tabIndex=10 runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>Hazard Ins</TD>
          <TD noWrap>
              <asp:PlaceHolder runat="server" ID="sProHazInsTHolder">
                  <asp:dropdownlist id=sProHazInsT tabIndex=10 runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
              </asp:PlaceHolder>
          </TD>
          <TD noWrap>
              <asp:PlaceHolder runat="server" ID="sProHazInsBaseAmtHolder">
                  (<ml:moneytextbox id=sProHazInsBaseAmt tabIndex=10 runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox>
              </asp:PlaceHolder>
          </TD>
          <TD noWrap>
              <asp:PlaceHolder runat="server" ID="sProHazInsRHolder">
                  x<ml:percenttextbox id=sProHazInsR tabIndex=10 runat="server" width="70" onchange="refreshCalculation();" preset="percent"></ml:percenttextbox>&nbsp;)
              </asp:PlaceHolder>
          </TD>
          <TD noWrap>
              <asp:PlaceHolder runat="server" ID="sProHazInsMbHolder">
                  +<ml:MoneyTextBox ID="sProHazInsMb" TabIndex="10" runat="server" Width="90" onchange="refreshCalculation();" preset="money" decimalDigits="4"></ml:moneytextbox>&nbsp;=
              </asp:PlaceHolder>
          </TD>
          <TD noWrap>
              <ml:moneytextbox id=sProHazIns tabIndex=2 runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox>
          </TD>
        </TR>
        <tr>
            <td class="FieldLabel" nowrap>Property Taxes</td>
            <td nowrap>
                <asp:PlaceHolder runat="server" ID="sProRealETxTHolder">
                    <asp:DropDownList ID="sProRealETxT" TabIndex="10" runat="server" onchange="refreshCalculation();"></asp:DropDownList></td>
                </asp:PlaceHolder>
            <td nowrap>
                <asp:PlaceHolder runat="server" ID="sProRealETxBaseAmtHolder">
                    (<ml:MoneyTextBox ID="sProRealETxBaseAmt" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                </asp:PlaceHolder>
            </td>
            <td nowrap>
                <asp:PlaceHolder runat="server" ID="sProRealETxRHolder">
                    x<ml:PercentTextBox ID="sProRealETxR" TabIndex="10" runat="server" Width="70" onchange="refreshCalculation();" preset="percent"></ml:PercentTextBox>&nbsp;)
                </asp:PlaceHolder>
            </td>
            <td nowrap>
                <asp:PlaceHolder runat="server" ID="sProRealETxMbHolder">
                    +<ml:MoneyTextBox ID="sProRealETxMb" TabIndex="10" runat="server" Width="90" onchange="refreshCalculation();" preset="money" designtimedragdrop="1583"></ml:MoneyTextBox>&nbsp;=
                </asp:PlaceHolder>
            </td>
            <td nowrap>
                <ml:MoneyTextBox ID="sProRealETx" TabIndex="2" runat="server" Width="90" onchange="refreshCalculation();" preset="money" ReadOnly="True"></ml:MoneyTextBox>
            </td>
        </tr>
        <TR>
          <TD class=FieldLabel noWrap colspan="5"><A tabIndex=10 href="javascript:linkMe('LoanInfo.aspx?pg=1');">Mortgage Insurance</A></TD>
          <TD noWrap class="FieldLabel"><ml:moneytextbox id=sProMIns tabIndex=10 runat="server" width="90" preset="money" onchange="refreshCalculation();" /><asp:CheckBox ID="sProMInsLckd" runat="server" onclick="refreshCalculation();" TabIndex="10" Text="Lock"/></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>HOA</TD>
          <TD noWrap></TD>
          <TD noWrap></TD>
          <TD noWrap></TD>
          <TD noWrap></TD>
          <TD noWrap><ml:moneytextbox id=sProHoAssocDues tabIndex=10 runat="server" width="90" onchange="refreshCalculation();" preset="money"></ml:moneytextbox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap colSpan=2>Other Taxes and Expenses</TD>
          <TD noWrap></TD>
          <TD noWrap></TD>
          <TD noWrap></TD>
          <TD noWrap><ml:moneytextbox id=sProOHExp tabIndex=10 runat="server" width="90" onchange="refreshCalculation();" preset="money"></ml:moneytextbox><asp:CheckBox ID="sProOHExpLckd" runat="server" onclick="refreshCalculation();" TabIndex="10" Text="Lock"/></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>PITI</TD>
          <TD noWrap></TD>
          <TD noWrap></TD>
          <TD noWrap></TD>
          <TD noWrap></TD>
          <TD noWrap><ml:moneytextbox id=sMonthlyPmt tabIndex=2 runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox></TD></TR></TABLE></TD></TR></table>
    <table>
        <tr>
            <TD class=FieldLabel noWrap>Lead Source</TD>
            <TD noWrap><asp:DropDownList id="sLeadSrcDesc" runat="server"></asp:DropDownList></TD>
        </tr>
    </table>