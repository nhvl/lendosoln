using System;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos
{
	public class BaseListEditPage : BaseLoanPage
	{
        override protected void OnInit(EventArgs e)
        {
            // 2/10/2004 dd - This page always embed inside a frame. Therefore copyright message is not necessary.
            this.DisplayCopyRight = false;
            this.IsRefreshLoanSummaryInfo = false;

            RegisterJsScript("singleedit2.js");
            this.RegisterJsScript("LQBPrintFix.js");
            base.OnInit(e);
        }
        protected void DataGrid_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header) 
            {
                DataGrid dg = (DataGrid) sender;

                for (int index = 0; index < dg.Columns.Count; index++) 
                {
                    if (IsReadOnly)
                        dg.Columns[index].SortExpression = "";
                    else 
                    {
                        string sortExpr = dg.Columns[index].SortExpression;
                        if (sortExpr != "") 
                        {
                            e.Item.Cells[index].Controls.Clear();
                            HyperLink link = new HyperLink();

                            link.Text = AspxTools.HtmlString(dg.Columns[index].HeaderText);
                            link.NavigateUrl = string.Format("javascript:if (typeof(__sortMe) == 'function') __sortMe({0});", AspxTools.JsString(sortExpr));

                            e.Item.Cells[index].Controls.Add(link);
                        }
                    }
                }
            } 
            else if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer) 
            {
                e.Item.Attributes.Add("onclick", "list_selectRow(this.rowIndex); if (typeof(refreshCalculation) == 'function') refreshCalculation();");
                e.Item.Attributes.Add("recordid", DataBinder.Eval(e.Item.DataItem, "RecordId").ToString());
            }
        }
	}
}
