using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos
{
	public partial class REORecord : BaseSingleEditPage2
	{
        protected System.Web.UI.WebControls.Button OK_btn;

        private CAppData m_dataApp;

        protected ISubcollection subcoll;


        protected void PageInit(object sender, System.EventArgs e) 
        {
            Zip.SmartZipcode(City, State);

            Tools.Bind_ReoTypeT(Type);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(REORecord));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            m_dataApp = dataLoan.GetAppData(ApplicationID);
            ClientScript.RegisterHiddenField("sSpAddr", dataLoan.sSpAddr );
            ClientScript.RegisterHiddenField("sSpCity", dataLoan.sSpCity );
            ClientScript.RegisterHiddenField("sSpState", dataLoan.sSpState );
            ClientScript.RegisterHiddenField("sSpZip", dataLoan.sSpZip );
	        ClientScript.RegisterHiddenField("aBAddr", dataApp.aBAddr);
            ClientScript.RegisterHiddenField("aBCity", dataApp.aBCity);
            ClientScript.RegisterHiddenField("aBState", dataApp.aBState);
            ClientScript.RegisterHiddenField("aBZip", dataApp.aBZip);
            this.PageTitle = "REO record";

            ILiaCollection coll = dataApp.aLiaCollection;
            subcoll = coll.GetSubcollection(true, E_DebtGroupT.Mortgage);

            int i = 0;
            foreach (ILiabilityRegular liaField in subcoll)
            {
                ClientScript.RegisterHiddenField("RecordId" + i, liaField.MatchedReRecordId.ToString());
                ClientScript.RegisterHiddenField("liabilityRecordID" + i, liaField.RecordId.ToString());
                ClientScript.RegisterHiddenField("cNameHidden" + i, liaField.ComNm );
                ClientScript.RegisterHiddenField("cBalHidden" + i, liaField.Bal_rep );
                ClientScript.RegisterHiddenField("cPmtHidden" + i, liaField.Pmt_rep);
                i++;
            }


        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        
        }
	}
}
