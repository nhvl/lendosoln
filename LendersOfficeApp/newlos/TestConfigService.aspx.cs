﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos
{
    public class TestConfigServiceItem : AbstractBackgroundServiceItem
    {
        protected override DataAccess.CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TestConfigServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sTestLoanFileEnvironmentId = GetGuid("sTestLoanFileEnvironmentId");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sTestLoanFileEnvironmentId", dataLoan.sTestLoanFileEnvironmentId.ToString());
        }
    }

    public partial class TestConfigService : BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new TestConfigServiceItem());
        }
    }
}