using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Constants;
using LendersOffice.Security;
using DataAccess;

using IQueryArgs = LendersOffice.Common.IQueryArgs;
using RequestHelperForWeb = LendersOffice.Common.RequestHelperForWeb;


namespace LendersOfficeApp.newlos
{
    public class LoanCreatePresenter
    {
        IQueryArgs m_queryArgs;


        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        /// <summary>
        /// Create blank loan and return sLId
        /// </summary>
        /// <returns></returns>
        private Guid CreateBlankLoan()
        {
            Guid sLId = Guid.Empty;
            try
            {
                CLoanFileCreator creator = CLoanFileCreator.GetCreator(BrokerUser, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);

                bool isTest = m_queryArgs.QueryString["test"] == "1";
                if (isTest)
                {
                    sLId = creator.CreateNewTestFile(Guid.Empty);
                }
                else
                {
                    bool isLead = m_queryArgs.QueryString["islead"] == "t";
                    if (isLead)
                    {
                        sLId = creator.CreateLead(templateId: Guid.Empty);
                    }
                    else
                    {
                        sLId = creator.CreateBlankLoanFile();
                    }

                }
            }
            catch (CBaseException exc)
            {
                m_queryArgs.DisplayErrorPage(exc, true, BrokerUser.BrokerId, BrokerUser.EmployeeId);
            }
            catch (Exception exc)
            {
                m_queryArgs.DisplayErrorPage(exc, true, BrokerUser.BrokerId, BrokerUser.EmployeeId);
            }
            return sLId;
        }

        /// <summary>
        /// Create loan from existing template and return sLId
        /// </summary>
        /// <returns></returns>
        private Guid CreateLoanFromTemplate(Guid templateID)
        {
            Guid sLId = Guid.Empty;
            try
            {
                BrokerUserPrincipal principal = BrokerUser;

                CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.FromTemplate);

                bool isTest = m_queryArgs.QueryString["test"] == "1";
                if (isTest)
                {
                    sLId = creator.CreateNewTestFile(templateID);
                }
                else
                {
                    bool isLead = m_queryArgs.QueryString["islead"] == "t";
                    if (isLead)
                    {
                        sLId = creator.CreateLead(templateId: templateID);
                    }
                    else
                    {
                        sLId = creator.CreateLoanFromTemplate(templateId: templateID);
                    }
                }
            }
            catch (CBaseException exc)
            {
                m_queryArgs.DisplayErrorPage(exc, true, BrokerUser.BrokerId, BrokerUser.EmployeeId);
            }
            catch (Exception exc)
            {
                m_queryArgs.DisplayErrorPage(exc, true, BrokerUser.BrokerId, BrokerUser.EmployeeId);
            }
            return sLId;

        }

        private Guid CreateLoanTemplate()
        {
            Guid sLId = Guid.Empty;
            try
            {
                CLoanFileCreator creator = DataAccess.CLoanFileCreator.GetCreator(BrokerUser, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                sLId = creator.CreateBlankLoanTemplate();
            }
            catch (CBaseException exc)
            {
                m_queryArgs.DisplayErrorPage(exc, true, BrokerUser.BrokerId, BrokerUser.EmployeeId);
            }
            catch (Exception exc)
            {
                m_queryArgs.DisplayErrorPage(exc, true, BrokerUser.BrokerId, BrokerUser.EmployeeId);
            }

            return sLId;
        }


        public LoanCreatePresenter(IQueryArgs queryArgs)
        {
            m_queryArgs = queryArgs;
        }

        public Guid CreateLoan()
        {
            Guid sLId = Guid.Empty;
            string type = m_queryArgs.QueryString["type"]; // Either 'loan' or 'template'

            if (type == "template")
            {
                sLId = CreateLoanTemplate();
            }
            else
            {
                Guid templateID = m_queryArgs.GetGuid("templateid", Guid.Empty); // If templateid is Guid.Empty then create loan from blank.
                if (templateID == Guid.Empty)
                    sLId = CreateBlankLoan();
                else
                    sLId = CreateLoanFromTemplate(templateID);
                string loanType = m_queryArgs.QueryString["loantype"];
                // 10/22/2013 dd - If purpose=HELOC then set sIsLineOfCredit
                if (loanType != null && loanType.Equals("HELOC", StringComparison.OrdinalIgnoreCase))
                {

                    CPageBase pageBase = new CPageBaseWrapped(sLId, "LoanAHELOC",
    CSelectStatementProvider.GetProviderForTargets(new string[] { "IsTemplate", "sIsLineOfCredit" }, false));
                    pageBase.InitSave(ConstAppDavid.SkipVersionCheck);
                    pageBase.sIsLineOfCredit = true;
                    pageBase.Save();
                }
            }
            return sLId;
        }
    }



    public partial class LoanCreate : LendersOffice.Common.BasePage
    {

        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (Page.IsPostBack)
            {
                LoanCreatePresenter presenter = new LoanCreatePresenter(RequestHelperForWeb.Instance);
                Guid sLId = presenter.CreateLoan();
                Response.Redirect("loanapp.aspx?loanid=" + sLId + "&isnew=t");
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
