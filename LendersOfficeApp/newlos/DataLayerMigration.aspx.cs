﻿#region Generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Migration;
    using LendersOfficeApp.LOAdmin.TaskBackendUtilities;
    using LendersOffice.Security;

    /// <summary>
    /// Page to perform data layer migrations on individual loans.
    /// </summary>
    public partial class DataLayerMigration : BaseLoanPage
    {
        /// <summary>
        /// PageInit function.
        /// </summary>
        /// <param name="sender">Control that calls PageInit.</param>
        /// <param name="e">System event arguments.</param>
        protected void PageInit(object sender, EventArgs e)
        {
            EnableJquery = true;
            RegisterJsScript("ractive-0.7.1.min.js");
            RegisterJsScript("mask.js");
            PageID = "DataLayerMigration";
            UseNewFramework = true;
        }
        
        /// <summary>
        /// Runs the initial migrations.
        /// </summary>
        protected override void LoadData()
        {
            LoanDataMigrationsRunner runner = new LoanDataMigrationsRunner(this.BrokerUser.DisplayName, this.BrokerUser.UserId, this.LoanID, this.BrokerID, isSystemBatchMigration: false);
            LoanDataMigrationViewModel model = runner.RunMigrationsToDisplay();

            FieldEnumerator.PopulateViewModelWithPages(model);
            model.DisplayRollback = this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms);

            RegisterJsObjectWithJsonNetSerializer("MigrationPageModel", model);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}