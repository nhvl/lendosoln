﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GovernmentMonitoringData.aspx.cs" Inherits="LendersOfficeApp.newlos.GovernmentMonitoringData" %>
<%@ Register TagPrefix="HRED" TagName="HmdaRaceEthnicityData" Src="~/newlos/HmdaraceEthnicitydata.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        #Page
        {
            width: 700px;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
        <div id="Page">
            <div class="MainRightHeader" nowrap>
                Government Monitoring Data
            </div>
            <HRED:HmdaRaceEthnicityData runat="server" ID="HREData" />
        </div>
    </form>
</body>
</html>
