using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Migration;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos
{


    public partial class UpfrontMIP : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
    {
        protected void PageInit(object sender, System.EventArgs e)
        {
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            phIsFinancedUFMIPIncludedInLTV.Visible = this.BrokerDB.IsDisplayChoiceUfmipInLtvCalc;
        }

        public void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(UpfrontMIP));
            dataLoan.InitLoad();

            Tools.Bind_sLT(sLT);
            Tools.Bind_MipCalcType_rep(sProMInsT, dataLoan.sLT);
            Tools.Bind_sMiInsuranceT(sMiInsuranceT);
            Tools.Bind_sMiCompanyNmT(sMiCompanyNmT, dataLoan.sLT);

            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            Tools.SetDropDownListValue(sProMInsT, dataLoan.sProMInsT);
            Tools.SetDropDownListValue(sMiInsuranceT, dataLoan.sMiInsuranceT);
            Tools.SetDropDownListValue(sMiCompanyNmT, dataLoan.sMiCompanyNmT);

            if(Tools.ShouldBeReadonly_sLT(dataLoan.sLPurposeT))
            {
                Tools.ReadonlifyDropDown(sLT);
            }

            sFfUfmipR.Text = dataLoan.sFfUfmipR_rep;
            sUfCashPd.Text = dataLoan.sUfCashPd_rep;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sFfUfmip1003Lckd.Checked = dataLoan.sFfUfmip1003Lckd;

            sMipFrequency_0.Checked = sMipFrequency_0.Value == dataLoan.sMipFrequency.ToString("D");
            sMipFrequency_1.Checked = sMipFrequency_1.Value == dataLoan.sMipFrequency.ToString("D");
            if (dataLoan.sFfUfMipIsBeingFinanced)
                sFfUfMipIsBeingFinanced_0.Checked = true;
            else
                sFfUfMipIsBeingFinanced_1.Checked = true;
            sMipPiaMon.Text = dataLoan.sMipPiaMon_rep;
            sUfCashPdLckd.Checked = dataLoan.sUfCashPdLckd;

            sProMInsR.Text = dataLoan.sProMInsR_rep;
            sProMInsBaseAmt.Text = dataLoan.sProMInsBaseAmt_rep;
            sProMInsBaseMonthlyPremium.Text = dataLoan.sProMInsBaseMonthlyPremium_rep;
            sProMInsMb.Text = dataLoan.sProMInsMb_rep;
            sProMIns.Text = dataLoan.sProMIns_rep;
            sProMInsLckd.Checked = dataLoan.sProMInsLckd;
            sIncludeUfmipInLtvCalc.Checked = dataLoan.sIncludeUfmipInLtvCalc;

            sMiLenderPaidCoverage.Text = dataLoan.sMiLenderPaidCoverage_rep;
            sMiCommitmentRequestedD.Text = dataLoan.sMiCommitmentRequestedD_rep;
            sMiCommitmentReceivedD.Text = dataLoan.sMiCommitmentReceivedD_rep;
            sMiCommitmentExpirationD.Text = dataLoan.sMiCommitmentExpirationD_rep;
            sMiCertId.Text = dataLoan.sMiCertId;
            sProMInsMon.Text = dataLoan.sProMInsMon_rep;
            sProMIns2Mon.Text = dataLoan.sProMIns2Mon_rep;
            sProMInsR2.Text = dataLoan.sProMInsR2_rep;
            sProMIns2.Text = dataLoan.sProMIns2_rep;
            sProMInsCancelLtv.Text = dataLoan.sProMInsCancelLtv_rep;
            sProMInsCancelAppraisalLtv.Text = dataLoan.sProMInsCancelAppraisalLtv_rep;
            sProMInsCancelMinPmts.Text = dataLoan.sProMInsCancelMinPmts_rep;
            sProMInsMidptCancel.Checked = dataLoan.sProMInsMidptCancel;
            sLenderUfmipR.Text = dataLoan.sLenderUfmipR_rep;
            sLenderUfmip.Text = dataLoan.sLenderUfmip_rep;
            sLenderUfmipLckd.Checked = dataLoan.sLenderUfmipLckd;
            sUfmipIsRefundableOnProRataBasis.Checked = dataLoan.sUfmipIsRefundableOnProRataBasis;

            if (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && dataLoan.sIsHousingExpenseMigrated)
            {
                sMInsRsrvEscrowedTri.Checked = dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes;
                sIssMInsRsrvEscrowedTriReadOnly.Checked = dataLoan.sIssMInsRsrvEscrowedTriReadOnly;
                sMInsRsrvMonLckd.Checked = dataLoan.sMInsRsrvMonLckd;
                sMInsRsrvMon.Text = dataLoan.sMInsRsrvMon_rep;
                sMInsRsrv.Text = dataLoan.sMInsRsrv_rep;

                Tools.Bind_DisbursementReptIntervalT(sMIPaymentRepeat);
                Tools.SetDropDownListValue(sMIPaymentRepeat, dataLoan.sMIPaymentRepeat);

                // MI column is 2.
                int[,] initEscAcc = dataLoan.sInitialEscrowAcc;
                MICush.Text = initEscAcc[0, 2].ToString();
                MIJan.Text = initEscAcc[1, 2].ToString();
                MIFeb.Text = initEscAcc[2, 2].ToString();
                MIMar.Text = initEscAcc[3, 2].ToString();
                MIApr.Text = initEscAcc[4, 2].ToString();
                MIMay.Text = initEscAcc[5, 2].ToString();
                MIJun.Text = initEscAcc[6, 2].ToString();
                MIJul.Text = initEscAcc[7, 2].ToString();
                MIAug.Text = initEscAcc[8, 2].ToString();
                MISep.Text = initEscAcc[9, 2].ToString();
                MIOct.Text = initEscAcc[10, 2].ToString();
                MINov.Text = initEscAcc[11, 2].ToString();
                MIDec.Text = initEscAcc[12, 2].ToString();
            }
            else
            {
                // Unmigrated loan. Hide the new section.
                MIEscImpSection.Style["display"] = "none";
            }

            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            if (!dataLoan.sLAmtLckd)
            {
                sLAmtCalc.ReadOnly = true;
            }

            sForceSinglePaymentMipFrequency.Value = dataLoan.sForceSinglePaymentMipFrequency.ToString();
            IsVersionAt_V4_ForceSinglePmtUfmipTypeForFhaVaUsda.Value = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V4_ForceSinglePmtUfmipTypeForFhaVaUsda_Opm237060).ToString();
        }

        public void SaveData()
        {
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}