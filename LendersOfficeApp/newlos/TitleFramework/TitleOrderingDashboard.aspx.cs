﻿#region Auto generated code
namespace LendersOfficeApp.newlos.TitleFramework
#endregion
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.Security;

    /// <summary>
    /// The dashboard for Title Framework orders.
    /// </summary>
    public partial class TitleOrderingDashboard : BaseLoanPage
    {
        /// <summary>
        /// Gets the principal.
        /// </summary>
        /// <value>The principal.</value>
        protected AbstractUserPrincipal Principal => PrincipalFactory.CurrentPrincipal;

        /// <summary>
        /// Page load function.
        /// </summary>
        protected override void LoadData()
        {
            if (Principal.BrokerDB.DisableNewTitleFramework)
            {
                return;
            }

            var titleOrders = TitleOrder.LoadOrdersByLoanId(this.LoanID, this.BrokerID);
            List<TitleOrderViewModel> viewModels = new List<TitleOrderViewModel>();
            foreach (var order in titleOrders)
            {
                viewModels.Add(order.ToViewModel());
            }

            this.RegisterJsObjectWithJsonNetSerializer("TitleOrders", viewModels);
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void PageInit(object sender, System.EventArgs e)
        {
            if (this.Principal.BrokerDB.DisableNewTitleFramework)
            {
                return;
            }

            this.PageID = "TitleFrameworkDashboard";
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("SimplePopups.js");
            this.RegisterJsScript("TitleFramework/TitleOrderRow.js");
        }

        /// <summary>
        /// Initialize function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }
    }
}
