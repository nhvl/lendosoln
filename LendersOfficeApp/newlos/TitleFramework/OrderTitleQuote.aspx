﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderTitleQuote.aspx.cs" Inherits="LendersOfficeApp.newlos.TitleFramework.OrderTitleQuote" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Order Title Fee Quote</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }
        .page-body
        {
            padding : 2px;
        }
        div.left-label {
            display: inline-block;
            vertical-align: top;
            width: 20%;
            max-width: 150px;
            padding: 4px;
        }
        div.right-input {
            display: inline-block;
            max-width: 400px;
            padding: 4px;
        }
        a#RolodexIcon {
            color: dimgrey;
        }
        div.submissionButtons {
            text-align : center;
            position : absolute;
            bottom : 0;
            left : 0;
            width : 100%;
            padding-bottom: 5px;
        }
        #ClearServiceProvider {
            display: block; 
            float: right; 
            color: gray; 
            border-left: 1px solid gray; 
            padding-left: 2px; margin-left: 2px; 
            cursor: pointer; 
            height: inherit;
        }
        /* Values taken from IE disabled text input. */
        span.fake-disabled-input {
            display: inline-block;
            color: gray;
            border: 1px solid rgb(188, 188, 188);
            background-color: rgb(229, 229, 229);
            width: 150px;
            height: 12.65px;
            padding: 2px 3px;
            vertical-align: bottom;
        }
        input[type=text], input[type=password] {
            width: 150px;
        }
        #LQBPopupDiv {
            max-width: 100%;
            max-height: 100%;
        }
    </style>
    <script type="text/javascript">
        $(function ($) {
            function openRolodex() {
                var settings = {
                    onReturn: rolodexReturn,
                    hideCloseButton: true,
                    height: 400,
                    width: 800
                };

                LQBPopup.Show(ML.VirtualRoot + '/los/RolodexList.aspx?loanid=' + ML.sLId + '&tab=1' /*tab=CONTACT_ENTRIES*/ + '&type=4' /*type=Title*/ + '&tpcodes=true', settings);
            }

            function rolodexReturn(args) {
                if (args.EmployeeId && args.AgentCompanyName) {
                    $('#providerName').text(args.AgentCompanyName).parent().css('width', 'auto');
                    $('#providerId').val(args.EmployeeId);
                    $('#TitleProviderPreviousOrderId').val('');
                }
            }

            function GatherRequestData() {
                var includedFeesMap = {};
                var checkBoxes = $('.feeCheckbox')
                for (var i = 0; i < checkBoxes.length; i++) {
                    includedFeesMap[$(checkBoxes[i]).attr('id').match(/feeCheckbox_(.+)/)[1]] = $(checkBoxes[i]).prop('checked');
                }

                var args = {
                    LoanId: ML.sLId,
                    LenderServiceId: $('#QuoteProviders').val(),
                    TitleProviderId: $('#providerId').val(),
                    TitleProviderPreviousOrderId: $('#TitleProviderPreviousOrderId').val(),
                    FeesToIncludeMap: JSON.stringify(includedFeesMap),
                    AccountId: $('#AccountId').val(),
                    Username: $('#Username').val(),
                    Password: $('#Password').val()
                };

                return args;
            }

            function RunAudit() {
                var data = GatherRequestData();
                var result = gService.order.call("RunAudit", data);
                if (result.error) {
                    alert(result.UserMessage);
                } else if (result.value.Success.toLowerCase() !== 'true') {
                    alert(JSON.parse(result.value.Errors).join('\r\n'));
                } else {
                    var auditResults = JSON.parse(result.value["AuditResults"]);
                    var auditPassed = result.value["AuditPassed"].toLowerCase() === 'true';
                    var auditPageTitle = "Title Quote Request Data Audit";

                    var auditData = {
                        AuditPageTitle: auditPageTitle,
                        AuditResults: auditResults,
                        AuditPassed: auditPassed
                    };

                    var page = AuditPage.CreateAuditPage(auditData);
                    LQBPopup.ShowElement($('<div>').append(page), {
                        width: 650,
                        height: 900,
                        popupClass: 'EditBackground',
                        elementClasses: 'FullWidthHeight OverflowAuto',
                        hideCloseButton: true,
                        onReturn: function (returnArgs) {
                            if (returnArgs.OK == true) {
                                return function () { RunRequest(data); };
                            }

                            return null;
                        }
                    });
                }
            }

            function RunRequest(data) {
                var loadingPopup = SimplePopups.CreateLoadingPopup("Placing Order...");
                OpenPopupInLQBPopup(loadingPopup, null);

                gService.order.callAsyncSimple(
                    'PlaceOrder',
                    data,
                    function (result) {
                        LQBPopup.Return(null);
                        PlaceOrder_Success(result);
                    }, 
                    function (result) {
                        LQBPopup.Return(null);
                        PlaceOrder_Error(result);
                    });
            }

            function PlaceOrder_Success(result)
            {
                if (result.value.Success.toLowerCase() === 'true') {
                    var successPopup = SimplePopups.CreateAlertPopup("Order results have been received.", '', ML.VirtualRoot + '/images/success.png');
                    OpenPopupInLQBPopup(successPopup, function () {
                        var titleOrder = JSON.parse(result.value.TitleOrder);
                        var data = {
                            TitleOrder: titleOrder
                        };

                        parent.LQBPopup.Return(data);
                    });
                }
                else {
                    var errors = JSON.parse(result.value.Errors);
                    var errorPopup = SimplePopups.CreateErrorPopup("Something went wrong", errors);
                    OpenPopupInLQBPopup(errorPopup, null);
                }
            }

            function PlaceOrder_Error(result) {
                var errors = [];
                errors.push(result.UserMessage);
                var errorPopup = SimplePopups.CreateErrorPopup("Something went wrong", errors);
                OpenPopupInLQBPopup(errorPopup, null);
            }

            function OpenPopupInLQBPopup(popup, onReturnFunction) {
                var onReturn = onReturnFunction;
                if (typeof (onReturn) !== 'function') {
                    onReturn = null;
                }

                LQBPopup.ShowElement($('<div>').append(popup), {
                    width: 350,
                    height: 200,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight',
                    onReturn: onReturn
                });
            }

            function LoadLenderService() {
                var data = {
                    LenderServiceId: $QuoteProviders.val(),
                    LoanId: ML.sLId
                };

                var result = gService.order.call('LoadLenderService', data);
                if (result.error) {
                    alert(result.UserMessage);
                    return undefined;
                }
                else if (result.value.Success.toLowerCase() !== 'true') {
                    alert(result.value.Errors);
                    return undefined;
                }
                else {
                    return {
                        HasServiceCredentials: result.value.HasServiceCredential.toLowerCase() === 'true',
                        UsesAccountId: result.value.UsesAccountId.toLowerCase() === 'true',
                        RequiresAccountId: result.value.RequiresAccountId.toLowerCase() === 'true',
                        ServiceCredentialHasAccountId: result.value.ServiceCredentialHasAccountId.toLowerCase() === 'true'
                    };
                }
            }

            var $AccountId = $('#AccountId'), $Username = $('#Username'), $Password = $('#Password');
            function ToggleCredentials(lenderService) {
                if (typeof lenderService !== 'object') {
                    return;
                }

                var userMustInputCredentials = !lenderService.HasServiceCredentials;
                $('#UsernameRow, #PasswordRow').toggle(userMustInputCredentials);
                $('#Username, #Password').toggleClass('RequiredInput', userMustInputCredentials);
                $('#UsernameImg').toggle(userMustInputCredentials && $Username.val() === '');
                $('#PasswordImg').toggle(userMustInputCredentials && $Password.val() === '');

                var userCanInputAccountId = lenderService.UsesAccountId && !lenderService.ServiceCredentialHasAccountId;
                var userMustInputAccountId = userCanInputAccountId && lenderService.RequiresAccountId;
                $('#AccountIdRow').toggle(userCanInputAccountId);
                $('#AccountId').toggleClass('RequiredInput', userMustInputAccountId);
                $('#AccountIdImg').toggle(userMustInputAccountId && $AccountId.val() === '');
            }

            function joinAndList(stringArr) {
                if (stringArr.length === 0) {
                    return '';
                } else if (stringArr.length === 1) {
                    return stringArr[0];
                } else if (stringArr.length === 2) {
                    return stringArr[0] + ' and ' + stringArr[1];
                } else {
                    var result = '';
                    for (var i = 0; i < stringArr.length - 1; ++i) {
                        result += stringArr[i] + ', ';
                    }

                    return result + 'and ' + stringArr[stringArr.length - 1];
                }
            }

            function ValidateForm()
            {
                $('#OrderTitleWarningIcon').toggle(!ML.CanOrderTitleQuote).attr('title', ML.OrderTitleQuoteDenialReason);
                if (!ML.CanOrderTitleQuote) {
                    $('#PlaceOrder').prop('disabled', true).prop('title', ML.OrderTitleQuoteDenialReason);
                    return;
                }

                var tests = [
                    { Valid: $('#QuoteProviders').val() !== '-1', Desc: 'a Quote Provider' },
                    { Valid: $('.feeCheckbox').toArray().some(function (checkbox) { return checkbox.checked; }), Desc: 'one or more Fees to Include' },
                    { Valid: $AccountId.val() !== '' || !$AccountId.hasClass('RequiredInput'), Desc: 'an account id' },
                    { Valid: $Username.val() !== '' || !$Username.hasClass('RequiredInput'), Desc: 'a username' },
                    { Valid: $Password.val() !== '' || !$Password.hasClass('RequiredInput'), Desc: 'a password' }
                ];

                var invalidResults = [];
                $.each(tests, function(i, test) {
                    if (!test.Valid) {
                        invalidResults.push(test.Desc);
                    }
                });
                var hasError = invalidResults.length > 0;
                var validationList = joinAndList(invalidResults);
                $('#PlaceOrder').prop('disabled', hasError).prop('title', hasError ? 'Please select ' + validationList : '');
            }

            $('.feeCheckbox').change(ValidateForm);
            $('.Credentials input').on('change keyup input', function () {
                if ($(this).hasClass('RequiredInput')) {
                    $(this).siblings('.RequiredImg').toggle($(this).val() === '');
                }

                ValidateForm();
            });

            var $TitleCompanyRow = $('#TitleCompanyRow');
            var $QuoteProviders = $('#QuoteProviders');
            var $TitleFeeCheckbox = $('#feeCheckbox_1');
            function ToggleTitleCompanyVisibility() {
                var quoteProvider = $QuoteProviders.val();
                var quoteProviderShowsTitleCompany = usesProviderCodes[quoteProvider] || (quoteProvider === '-1' && anyProviderCodes);
                var userShowsTitleCompany = $TitleFeeCheckbox.prop('checked');
                $TitleCompanyRow.toggle(quoteProviderShowsTitleCompany && userShowsTitleCompany);
            }
            $TitleFeeCheckbox.change(ToggleTitleCompanyVisibility);
            $QuoteProviders.change(function ()
            {
                ToggleCredentials(LoadLenderService());
                $('#QuoteProviders option[value="-1"]').remove();
                ToggleTitleCompanyVisibility();
                ValidateForm();
            });

            $('#ClearServiceProvider').click(function () {
                $('#providerName').text('').parent().css('width', '150px');
                $('#providerId').val('');
                $('#TitleProviderPreviousOrderId').val('');
            });

            $('#RolodexIcon').click(function () {
                openRolodex();
                return false;
            });

            $('#PlaceOrder').click(function () {
                RunAudit();
                return false;
            });

            $('#Cancel').click(function () {
                parent.LQBPopup.Return(null);
                return false;
            });

            var anyProviderCodes = false;
            for (providerId in usesProviderCodes) {
                anyProviderCodes = anyProviderCodes || usesProviderCodes[providerId];
            }

            if (typeof existingTitleOrderServiceProvider === 'object') {
                $('#providerName').text(existingTitleOrderServiceProvider.ProviderName).parent().css('width', 'auto');
                $('#TitleProviderPreviousOrderId').val(existingTitleOrderServiceProvider.OrderId);
            }

            $('#AccountIdRow').hide();<%--default to hidden; ToggleCredentials will show if necessary--%>
            if (typeof InitialLenderService === 'object') {
                ToggleCredentials(InitialLenderService);
            }

            ToggleTitleCompanyVisibility();
            ValidateForm();
        });
    </script>
</head>
<body>
    <form id="aspForm" runat="server">
        <div class="MainRightHeader">Order Title Fee Quote</div>
        <div class="page-body">
            <h3>Order Options</h3>
            <div id="content">
                <div class="row">
                    <div class="left-label">Quote Provider</div>
                    <div class="right-input"><asp:DropDownList ID="QuoteProviders" runat="server"></asp:DropDownList></div>
                </div>
                <div class="row">
                    <div class="left-label">Fees to Include</div>
                    <div class="right-input">
                        <asp:Repeater ID="FeesToInclude" runat="server">
                            <ItemTemplate>
                                <div>
                                    <input type="checkbox" checked="checked" id="<%# AspxTools.HtmlString("feeCheckbox_" + (int)Eval("Value")) %>" class="feeCheckbox" />
                                    <label for="<%# AspxTools.HtmlString("feeCheckbox_" + (int)Eval("Value")) %>"><%# AspxTools.HtmlString(Eval("Description")) %></label>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="row" id="TitleCompanyRow">
                    <div class="left-label">Service Provider</div>
                    <div class="right-input">
                        <span class="fake-disabled-input" >
                            <span id="providerName"></span>
                            <span class="fa fa-lg fa-times" id="ClearServiceProvider"></span>
                        </span>
                        <input type="hidden" id="providerCode" />
                        <input type="hidden" id="providerId" />
                        <input type="hidden" id="TitleProviderPreviousOrderId" />
                        <a href="#" id="RolodexIcon">
                            <span class="fa fa-user fa-2x"></span>
                        </a>
                    </div>
                </div>
                <div class="row Credentials" id="AccountIdRow">
                    <div class="left-label">Account ID</div>
                    <div class="right-input">
                        <input type="text" id="AccountId" name="AccountId" class="RequiredInput" />
                        <img alt="Required" class="RequiredImg" id="AccountIdImg" src="../../images/require_icon_red.gif" />
                    </div>
                </div>
                <div class="row Credentials" id="UsernameRow">
                    <div class="left-label">Username</div>
                    <div class="right-input">
                        <input type="text" id="Username" name="Username" class="RequiredInput" />
                        <img alt="Required" class="RequiredImg" id="UsernameImg" src="../../images/require_icon_red.gif" />
                    </div>
                </div>
                <div class="row Credentials" id="PasswordRow">
                    <div class="left-label">Password</div>
                    <div class="right-input">
                        <input type="password" id="Password" name="Password" class="RequiredInput" />
                        <img alt="Required" class="RequiredImg" id="PasswordImg" src="../../images/require_icon_red.gif" />
                    </div>
                </div>
                <div class="submissionButtons">
                    <input type="button" id="PlaceOrder" value="Place Order" />
                    <img id="OrderTitleWarningIcon" src=<%= AspxTools.SafeUrl(this.VirtualRoot + "/images/warning25x25.png") %> alt="Warning" />
                    <input type="button" id="Cancel" value="Cancel" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
