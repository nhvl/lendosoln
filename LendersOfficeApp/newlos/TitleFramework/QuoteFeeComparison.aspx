﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteFeeComparison.aspx.cs" Inherits="LendersOfficeApp.newlos.TitleFramework.QuoteFeeComparison" %>
<%@ Register TagPrefix="uc" TagName="LoanEstimateFeeListTemplates" Src="~\newlos\Disclosure\LoanEstimateFeeList.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fee Comparison</title>
    <style type="text/css">
        h1
        {
            font-size: 1.5em;
        }
        .Buttons
        {
            padding: 10px;
            text-align: right;
        }
        input[type=button]
        {
            min-width: 20%;
        }
    </style>
    <style type="text/css" class="CopyToAudit">
        body
        {
            text-align: center;
        }
        #ComparisonTable
        {
            border-collapse: collapse;
            display: inline-block;
            text-align: left;
        }
        #ComparisonTable>thead>tr>th, #ComparisonTable>tbody>tr>td
        {
            width: 50%;
            padding: 0px 20px;
            vertical-align: top;
        }
        #ComparisonTable>thead>tr>th
        {
            font-size: 1.25em;
        }
        #ComparisonTable>tbody>tr.Spacer
        {
            height: 10px;
        }
        #ComparisonTable>tbody>tr.Spacer.BorderTop
        {
            border-top: 1px solid black;
        }
        #ComparisonTable>tbody>tr.Spacer.BorderBottom
        {
            border-bottom: 1px solid black;
        }
        #ComparisonTable>tbody>tr.FeeSection>td:first-child
        {
            border-right: 1px solid black;
        }

        #ComparisonTable>tbody>tr>td .SectionContainer
        {
            border: 1px solid gray;
            margin: 5px 0px;
        }
        #ComparisonTable>tbody>tr>td .SectionContainer div
        {
            padding: 0px 10px;
            overflow: hidden;
        }
        #ComparisonTable>tbody>tr>td .SectionContainer div.SectionHeader
        {
            padding: 0px 5px;
            background-color: rgb(8, 83, 147);
            color: White;
            font-weight: bold;
        }
        #ComparisonTable>tbody>tr>td span
        {
            display: inline-block;
            margin: 5px 0px; 
        }
        #ComparisonTable>tbody>tr>td .MoneyContainer
        {
            float: right;
            width: 90px;
            text-align: right;
        }
        #ComparisonTable>tbody>tr>td .GridAlternatingItem
        {
            background-color: White !important;
            border-top: 1px solid gray;
            border-bottom: 1px solid gray;
        }
    </style>
</head>
<body>
    <h4 class="page-header">Fee Comparison</h4>
    <form id="form1" runat="server">
        <script type="text/javascript">
            $(function () {
                function GetSectionLetter(sectionName) {
                    return (/^[A-Z] -/.test(sectionName) && sectionName.charAt(0)) || undefined;
                }
                function GetIndex(arr, predicate) {
                    for (var j = 0; j < arr.length; ++j) {
                        if (predicate(arr[j])) return j;
                    }
                }
                function ClosingCostSort(a, b) { return ((a.is_title ? 'Title - ' : '') + a.desc).localeCompare((b.is_title ? 'Title - ' : '') + b.desc); }

                var sectionListMaster = [];
                for (var i = 0; i < ClosingCostData_Current.SectionList.length; ++i) {
                    ClosingCostData_Current.SectionList[i].ClosingCostFeeList.sort(ClosingCostSort);
                    sectionListMaster.push({
                        SectionLetter: GetSectionLetter(ClosingCostData_Current.SectionList[i].SectionName),
                        CurrentIndex: i
                    });
                }
                for (var i = 0; i < ClosingCostData_WithQuote.SectionList.length; ++i) {
                    ClosingCostData_WithQuote.SectionList[i].ClosingCostFeeList.sort(ClosingCostSort);
                    var letter = GetSectionLetter(ClosingCostData_WithQuote.SectionList[i].SectionName);
                    var index = GetIndex(sectionListMaster, function (item) { return item.SectionLetter === letter; });
                    if (index >= 0) {
                        sectionListMaster[index].WithQuoteIndex = i;
                    } else {
                        sectionListMaster.push({
                            SectionLetter: letter,
                            WithQuoteIndex: i
                        });
                    }
                }
                sectionListMaster.sort(function (a, b) {
                    return a.SectionLetter.localeCompare(b.SectionLetter);
                });


                new Ractive({
                    el: 'Comparison',
                    append: false,
                    template: $('#ClosingCostComparison').html(),
                    data: {
                        Sections: sectionListMaster,
                        Current: ClosingCostData_Current,
                        WithQuote: ClosingCostData_WithQuote,
                        Providers: QuoteProviders,
                        ConfirmationMessages: QuoteWarnings,
                        ApplyQuotePermissions: { Enabled: ML.CanApplyTitleQuote, WorkflowMessage: ML.ApplyTitleQuoteDenialReason }
                    },
                    init: function () { }
                });

                var $ApplyQuoteBtn = $('#ApplyQuoteBtn');
                var $ConfirmationCheckboxes = $('.ConfirmationCheckbox');

                function RefreshApplyQuotePermission() {
                    var canApplyQuote = ML.CanApplyTitleQuote && $ConfirmationCheckboxes.not(':checked').length === 0;
                    var titleMessage = ML.CanApplyTitleQuote ? 'Please confirm all warnings' : ML.ApplyTitleQuoteDenialReason;
                    $ApplyQuoteBtn.prop('disabled', !canApplyQuote);
                    $ApplyQuoteBtn.attr('title', canApplyQuote ? '' : titleMessage);
                }

                $ConfirmationCheckboxes.change(RefreshApplyQuotePermission);
                RefreshApplyQuotePermission();

                $ApplyQuoteBtn.click(function () {
                    var style = $('style.CopyToAudit').clone();
                    var table = $('#ComparisonTable').clone();
                    table.find('.ConfirmationCheckbox').prop('disabled', true);
                    table.find('tfoot').remove();// buttons
                    var comparisonHtml = $('<div>').append(style).append(table).html();
                    var args = {
                        'LoanId': ML.sLId,
                        'OrderId': ML.OrderId,
                        'ComparisonHtml': comparisonHtml
                    }
                    var result = gService.QuoteService.call('ApplyQuote', args);
                    if (result.error) {
                        alert(result.UserMessage);
                        return;
                    } else if (result.value && result.value.Success === 'True') {
                        alert('Quote applied successfully.');
                        parent.LQBPopup.Return();
                        return;
                    } else {
                        alert(JSON.parse(result.value.Errors).join('\r\n'));
                        return;
                    }
                });
                $('#CancelBtn').click(function () {
                    parent.LQBPopup.Return();
                });
            });

        </script>
        <uc:LoanEstimateFeeListTemplates id="FeeListTemplates" runat="server" />
        <script id="ClosingCostComparison" type="text/ractive">
        <table id="ComparisonTable">
            <thead>
                <tr>
                    <th>Current Closing Costs</th>
                    <th>Closing Costs with Quote Applied</th>
                </tr>
            </thead>
            <tbody>
                <tr class="Spacer BorderTop">
                    <td colspan="2"></td>
                </tr>
                {{#Sections}}
                    {{#if SectionLetter === 'A' || SectionLetter === 'B' || SectionLetter === 'C' }}
                        <tr class="FeeSection">
                            <td>{{#if CurrentIndex >= 0}}{{>LoanEstimateFeeListSectionWithFees Current.SectionList[CurrentIndex]}}{{/if}}</td>
                            <td>{{#if WithQuoteIndex >= 0}}{{>LoanEstimateFeeListSectionWithFees WithQuote.SectionList[WithQuoteIndex]}}{{/if}}</td>
                        </tr>
                    {{/if}}
                {{/Sections}}
                <tr class="FeeSection">
                    <td>{{>LoanEstimateFeeListSectionD Current}}</td>
                    <td>{{>LoanEstimateFeeListSectionD WithQuote}}</td>
                </tr>
                {{#Sections}}
                    {{#if SectionLetter === 'E' || SectionLetter === 'F' || SectionLetter === 'G' || SectionLetter === 'H' }}
                        <tr class="FeeSection">
                            <td>{{#if CurrentIndex >= 0}}{{>LoanEstimateFeeListSectionWithFees Current.SectionList[CurrentIndex]}}{{/if}}</td>
                            <td>{{#if WithQuoteIndex >= 0}}{{>LoanEstimateFeeListSectionWithFees WithQuote.SectionList[WithQuoteIndex]}}{{/if}}</td>
                        </tr>
                    {{/if}}
                {{/Sections}}
                <tr class="FeeSection">
                    <td>{{>LoanEstimateFeeListSectionI Current}}</td>
                    <td>{{>LoanEstimateFeeListSectionI WithQuote}}</td>
                </tr>
                <tr class="FeeSection">
                    <td>{{>LoanEstimateFeeListSectionJ Current}}</td>
                    <td>{{>LoanEstimateFeeListSectionJ WithQuote}}</td>
                </tr>
                <tr class="Spacer BorderBottom">
                    <td colspan="2"></td>
                </tr>
                {{#Providers}}
                <tr class="Spacer">
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2"><p>{{Disclaimer}}</p></td>
                </tr>
                <tr class="Spacer BorderBottom">
                    <td colspan="2"></td>
                </tr>
                {{/Providers}}
                {{#if ConfirmationMessages}}
                <tr class="Spacer">
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="2" ><h3>Acknowledgments:</h3></td>
                </tr>
                {{#ConfirmationMessages}}
                <tr>
                    <td colspan="2"><input type="checkbox" class="ConfirmationCheckbox"/>{{this}}</td>
                </tr>
                {{/ConfirmationMessages}}
                <tr class="Spacer BorderBottom">
                    <td colspan="2"></td>
                </tr>
                {{/if}}
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2" class="Buttons">
                        <input type="button" value="Apply Quote to Loan File" id="ApplyQuoteBtn" disabled="{{!ApplyQuotePermissions.Enabled}}" />
                        {{#if !ApplyQuotePermissions.Enabled && ApplyQuotePermissions.WorkflowMessage}}
                        <img src=<%= AspxTools.SafeUrl(this.VirtualRoot + "/images/warning25x25.png") %> title="{{ApplyQuotePermissions.WorkflowMessage}}" alt="Warning" />
                        {{/if}}
                        <input type="button" value="Cancel" id="CancelBtn" />
                    </td>
                </tr>
            </tfoot>
        </table>
        </script>

    <div>
        <div id="Comparison"></div>
    </div>
    </form>
</body>
</html>
