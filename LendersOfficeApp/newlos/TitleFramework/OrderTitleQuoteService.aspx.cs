﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.TitleFramework
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.Templates;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// Service page for the <see cref="OrderTitleQuote"/> title ordering dialog.
    /// </summary>
    public partial class OrderTitleQuoteService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets the principal.
        /// </summary>
        /// <value>The principal.</value>
        protected AbstractUserPrincipal Principal => PrincipalFactory.CurrentPrincipal;

        /// <summary>
        /// Runs a service method.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        protected override void Process(string methodName)
        {
            if (this.Principal.BrokerDB.DisableNewTitleFramework)
            {
                this.SetResult("Success", false);
                this.SetResult("Error", "Unable to use new Title Ordering Framework.");
                return;
            }

            switch (methodName)
            {
                case nameof(this.PlaceOrder):
                    this.PlaceOrder();
                    return;
                case nameof(this.RunAudit):
                    this.RunAudit();
                    return;
                case nameof(this.LoadLenderService):
                    this.LoadLenderService();
                    return;
                case "ApplyQuote":
                    this.ApplyQuote();
                    return;
                default:
                    throw new CBaseException(ErrorMessages.Generic, $"Invalid method {methodName}.");
            }
        }

        /// <summary>
        /// Loads the lender service data.
        /// </summary>
        private void LoadLenderService()
        {
            int lenderServiceId = this.GetInt("LenderServiceId");
            Guid loanId = this.GetGuid("LoanId");
            var principal = PrincipalFactory.CurrentPrincipal;

            TitleLenderService lenderService = TitleLenderService.GetLenderServiceById(principal.BrokerId, lenderServiceId);
            if (lenderService == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", "Unable to find Service Provider.");
                return;
            }

            TitleVendor vendor = TitleVendor.LoadTitleVendor(lenderService.VendorId);
            if (vendor == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", "Unable to find vendor.");
                return;
            }

            var usesAccountId = vendor.QuotingPlatform.UsesAccountId;
            var requiresAccountId = vendor.QuotingPlatform.RequiresAccountId;

            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderTitleQuoteService));
            dataLoan.InitLoad();

            var credential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.TitleQuotes)
                .FirstOrDefault(cred => cred.TitleQuoteLenderServiceId.HasValue && cred.TitleQuoteLenderServiceId == lenderService.LenderServiceId);

            bool hasServiceCredential = false;
            bool serviceCredentialHasAccountId = false;
            if (credential != null)
            {
                hasServiceCredential = true;
                serviceCredentialHasAccountId = !string.IsNullOrEmpty(credential.AccountId);
            }

            this.SetResult("UsesAccountId", usesAccountId);
            this.SetResult("RequiresAccountId", requiresAccountId);
            this.SetResult("HasServiceCredential", hasServiceCredential);
            this.SetResult("ServiceCredentialHasAccountId", serviceCredentialHasAccountId);
            this.SetResult("Success", true);
        }

        /// <summary>
        /// Audits the request.
        /// </summary>
        private void RunAudit()
        {
            string error;
            List<string> errors = new List<string>();
            var requestData = this.ConstructRequestData(out error);
            if (requestData == null)
            {
                errors.Add(error);
                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(errors));
                return;
            }

            var requestHandler = AbstractTitleRequestHandler.CreateRequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            List<KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>> auditResultsForUi = new List<KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>>();
            foreach (var section in auditResults.SectionNames)
            {
                KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>> errorsForSection = new KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>(section, auditResults.GetErrorsForSection(section));
                auditResultsForUi.Add(errorsForSection);
            }

            SetResult("Success", true);
            SetResult("AuditResults", SerializationHelper.JsonNetSerialize(auditResultsForUi));
            SetResult("AuditPassed", !auditResults.HasErrors);
        }

        /// <summary>
        /// Place an order to the title quote provider.
        /// </summary>
        private void PlaceOrder()
        {
            string error;
            List<string> errors = new List<string>();
            var requestData = this.ConstructRequestData(out error);
            if (requestData == null)
            {
                errors.Add(error);
                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(errors));
                return;
            }

            var requestHandler = AbstractTitleRequestHandler.CreateRequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            if (auditResults.HasErrors)
            {
                errors.Add("Invalid data. Please run the audit again.");
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(errors));
            }

            var titleOrder = requestHandler.SubmitRequest(out errors, doAudit: false);
            this.SetResult("Success", titleOrder != null);
            if (titleOrder == null)
            {
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(errors));
            }
            else
            {
                this.SetResult("TitleOrder", SerializationHelper.JsonNetSerialize(titleOrder.ToViewModel()));
            }
        }

        /// <summary>
        /// Applies a quote to the loan file and saves it.
        /// </summary>
        private void ApplyQuote()
        {
            Guid loanId = this.GetGuid("LoanId");
            int orderId = this.GetInt("OrderId");
            string comparisonHtml = this.GetString("ComparisonHtml");

            TitleOrder order = TitleOrder.LoadOrderById(orderId, loanId, this.Principal.BrokerId);
            DataAccess.CFullAccessPageData loan = DataAccess.CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(OrderTitleQuoteService)); // Workflow is run at the ApplyQuote level
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
            Tuple<bool, string> workflowResult = order.CheckCanApplyAndSaveQuoteToLoan(loan, this.Principal);
            if (workflowResult.Item1)
            {
                order.ApplyAndSaveQuoteToLoan(loan, this.Principal, comparisonHtml, workflowResult);
                this.SetResult("Success", true);
            }
            else
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(new List<string> { workflowResult.Item2 }));
            }
        }

        /// <summary>
        /// Constructs the request data.
        /// </summary>
        /// <param name="error">Any errors during construction.</param>
        /// <returns>The request data if constructed. Null otherwise.</returns>
        private ErnstQuoteRequestData ConstructRequestData(out string error)
        {
            var accountId = this.GetString("AccountId", null);
            var username = this.GetString("Username", null);
            var password = this.GetString("Password", null);
            var lenderServiceId = this.GetInt("LenderServiceId");
            var loanId = this.GetGuid("LoanId");

            var flagsMap = SerializationHelper.JsonNetDeserialize<Dictionary<ErnstQuoteFeeCategoryFlags, bool>>(this.GetString("FeesToIncludeMap"));
            ErnstQuoteFeeCategoryFlags feeFlags = ErnstQuoteFeeCategoryFlags.None;
            foreach (KeyValuePair<ErnstQuoteFeeCategoryFlags, bool> kvp in flagsMap)
            {
                if (Enum.IsDefined(typeof(ErnstQuoteFeeCategoryFlags), kvp.Key) && kvp.Value)
                {
                    feeFlags = feeFlags | kvp.Key;
                }
            }

            var principal = PrincipalFactory.CurrentPrincipal;
            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderTitleQuoteService));
            dataLoan.InitLoad();

            var userType = principal.Type.Equals("P", StringComparison.OrdinalIgnoreCase) ? UserType.TPO : UserType.LQB;
            var credential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.TitleQuotes)
                .FirstOrDefault(cred => cred.TitleQuoteLenderServiceId == lenderServiceId);

            if (credential != null)
            {
                accountId = string.IsNullOrEmpty(credential.AccountId) ? accountId : credential.AccountId;
                username = credential.UserName;
                password = credential.UserPassword.Value;
            }

            Guid? titleProviderId = this.GetGuid("TitleProviderId", Guid.Empty);
            titleProviderId = titleProviderId == Guid.Empty ? null : titleProviderId;
            TitleQuoteOrderViewModel viewModel = new TitleQuoteOrderViewModel(
                loanId,
                lenderServiceId,
                titleProviderId,
                this.GetNullableInt("TitleProviderPreviousOrderId"),
                feeFlags,
                accountId,
                username,
                password);

            ErnstQuoteRequestData requestData = ErnstQuoteRequestData.CreateFromViewModel(viewModel, this.Principal, out error);
            return requestData;
        }
    }
}