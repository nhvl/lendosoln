﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TitleOrderingDashboard.aspx.cs" Inherits="LendersOfficeApp.newlos.TitleFramework.TitleOrderingDashboard" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Title Quote Ordering Dashboard</title>
    <style>
        body  { 
            background-color: gainsboro; 
        }
        a[data-sort] {
            color: white;
        }
        #LQBPopupElement {
            max-height: calc(100% - 25px);
            overflow: auto;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            $('#OrderTitleQuoteBtn').click(function () {
                var url = <%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/TitleFramework/OrderTitleQuote.aspx?loanid=" + this.LoanID + "&appid=" + this.ApplicationID)) %>;
                LQBPopup.Show(url, {
                    hideCloseButton: true,
                    width: Math.floor($(window).width() * .75),
                    height: Math.floor($(window).height() * .85),
                    onReturn: function(returnArgs) {
                        if(returnArgs) {
                            var order = returnArgs.TitleOrder;
                            var row = TitleOrderRow.CreateTitleOrderRow(order);
                            $('#QuoteTable tbody').append(row);
                            setTimeout(function(){ ShowFeeComparison(returnArgs.TitleOrder.OrderId); });
                        }
                    }
                }, null);
            });

            $('#QuoteTable').on('click', '.CompareApplyBtn', function() {
                var row = $(this).closest('.OrderRow');
                var orderId = row.find('.OrderId').val();
                ShowFeeComparison(orderId);
            });

            $('#QuoteTable').on('click', '.ViewTitleProviderLink', function() {
                var $link = $(this);
                var providerJson = $link.attr('data-provider');
                if (providerJson) {
                    var provider = JSON.parse(providerJson);
                    var createLine = function(root, text) {
                        if (text) {
                            root.appendChild(document.createTextNode(text));
                        }
                        root.appendChild(document.createElement('br'));
                    };
                    var p = document.createElement('p');
                    createLine(p, provider.Name);
                    createLine(p, provider.Address);
                    createLine(p, (provider.City ? provider.City + ", " : provider.City) + (provider.State ? provider.State + ' ' : provider.State) + provider.Zip);
                    createLine(p, provider.Phone);
                    createLine(p, provider.Website);
                    var disclaimer = document.createElement('p');
                    if (provider.Disclaimer) {
                        disclaimer.appendChild(document.createTextNode(provider.Disclaimer));
                    }
                    var $providerElement = $(document.createElement('div'));
                    $providerElement.append([p, disclaimer]);
                    var args = {modifyOverflow: false};
                    LQBPopup.ShowElement($providerElement, args);
                }
            });

            function ShowFeeComparison(orderId) {
                LQBPopup.Show('QuoteFeeComparison.aspx?loanid=' + ML.sLId + '&oId=' + orderId, {
                    hideCloseButton: true,
                    width: Math.floor($(window).width() * .9),
                    height: Math.floor($(window).height() * .9)
                });
            }

            function LoadTitleOrderRow() {
                $.each(TitleOrders, function (index, value) {
                    var row = TitleOrderRow.CreateTitleOrderRow(value);
                    $('#QuoteTable tbody').append(row);
                });
            }

            TitleOrderRow.PostTemplateLoadCallback(LoadTitleOrderRow);
        });
    </script>
    <form id="form1" runat="server">
    <div>
        <div class="MainRightHeader">Title Quote Ordering Dashboard</div>
        <div id="continaer">
            <div class="Content PaddingLeftRight5">
                <div id="QuoteContent">
                    <table class="Table no-wrap" id="QuoteTable">
                        <thead>
                            <tr class="LoanFormHeader">
                                <td><a data-sort="true" data-sorttarget="QuoteProvider" data-asc="false">Quote Provider</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="TitleProvider" data-asc="false">Service Provider</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="OrderNumber" data-asc="false">Order Number</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="CompareApplyFees" data-asc="false">Compare/Apply Fees</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="OrderedBy" data-asc="false">Ordered By</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                                <td><a data-sort="true" data-sorttarget="OrderedDate" data-asc="false">Date Ordered</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden" /></td>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <br />
                    <input type="button" class="MarginLeft" id="OrderTitleQuoteBtn" value="Order New Title Quote" />
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
