﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.TitleFramework
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Integration.TitleFramework;

    /// <summary>
    /// A page for comparing the result of a quote application with the current loan fee state.
    /// </summary>
    public partial class QuoteFeeComparison : BaseLoanPage
    {
        /// <inheritdoc />
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Handles the Page object's <c>Init</c> event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("ThirdParty/ractive-legacy.min.js");
            this.DisplayCopyRight = false;
            this.RegisterService("QuoteService", "/newlos/TitleFramework/OrderTitleQuoteService.aspx");
        }

        /// <summary>
        /// Handles the Page object's <c>Load</c> event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int orderId = RequestHelper.GetInt("oId");
            CPageData loan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(QuoteFeeComparison));
            loan.InitLoad();

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("ClosingCostData_Current", CreateAnonymousFeeViewModel(loan));

            // It's important that the first loan data is totally serialized before the quote application updates some
            // of the instances on the loan object.
            TitleOrder order = TitleOrder.LoadOrderById(orderId, this.LoanID, this.BrokerID);
            this.RegisterJsGlobalVariables("OrderId", order.OrderId);
            Tuple<bool, string> canApplyResult = order.CheckCanApplyAndSaveQuoteToLoan(loan, LendersOffice.Security.PrincipalFactory.CurrentPrincipal);
            this.RegisterJsGlobalVariables("CanApplyTitleQuote", canApplyResult.Item1);
            this.RegisterJsGlobalVariables("ApplyTitleQuoteDenialReason", !canApplyResult.Item1 && string.IsNullOrEmpty(canApplyResult.Item2) ? "You do not have permission to apply a title quote." : canApplyResult.Item2);
            order.ApplyQuoteToLoanForCalculation(loan);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("ClosingCostData_WithQuote", CreateAnonymousFeeViewModel(loan));
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("QuoteProviders", order.QuotedFees?.Providers ?? new ServiceProviderInfo[0]);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("QuoteWarnings", order.QuotedFees?.WarningMessages ?? new string[0]);
        }

        /// <summary>
        /// Creates an anonymous view model from the current state of the loan.
        /// </summary>
        /// <param name="loan">The loan to read the data from.</param>
        /// <returns>The anonymous object that will serve as the view model.</returns>
        private static object CreateAnonymousFeeViewModel(CPageData loan)
        {
            return new
            {
                SectionList = GetFeeSectionList(loan.sClosingCostSet),
                sTRIDLoanEstimateTotalLoanCosts = loan.sTRIDLoanEstimateTotalLoanCosts_rep,
                sTRIDLoanEstimateTotalOtherCosts = loan.sTRIDLoanEstimateTotalOtherCosts_rep,
                sTRIDLoanEstimateTotalClosingCosts = loan.sTRIDLoanEstimateTotalClosingCosts_rep,
                sTRIDLoanEstimateTotalAllCosts = loan.sTRIDLoanEstimateTotalAllCosts_rep,
                sTRIDLoanEstimateLenderCredits = loan.sTRIDLoanEstimateLenderCredits_rep
            };
        }

        /// <summary>
        /// Gets the fee sections to serialize and display, by excluding the Aggregate Adjustment.
        /// </summary>
        /// <param name="closingCostSet">The closing cost set to pull data from.</param>
        /// <returns>The list of sections to put in the UI.</returns>
        /// <remarks>This logic is copied from the LoanEstimate page code behind.</remarks>
        private static IEnumerable<BorrowerClosingCostFeeSection> GetFeeSectionList(BorrowerClosingCostSet closingCostSet)
        {
            IEnumerable<BorrowerClosingCostFeeSection> sectionList = closingCostSet.GetViewForSerialization(E_ClosingCostViewT.LoanClosingCost);
            BorrowerClosingCostFeeSection sectionG = sectionList.First(p => p.SectionType == E_IntegratedDisclosureSectionT.SectionG);
            sectionG.ExclusionFilter = p => p.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId;
            return sectionList;
        }
    }
}
