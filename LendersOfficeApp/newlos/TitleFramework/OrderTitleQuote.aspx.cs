﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.TitleFramework
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// The ordering page to specify the details for a Title Framework quote order.
    /// </summary>
    public partial class OrderTitleQuote : BaseLoanPage
    {
        /// <summary>
        /// Page load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.UseNewFramework = true;
            this.DisplayCopyRight = false;
            this.EnableJqueryMigrate = false;

            this.RegisterService("order", "/newlos/TitleFramework/OrderTitleQuoteService.aspx");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("SimplePopups.js");
            this.RegisterJsScript("AuditPage.js");
            this.RegisterCSS("font-awesome.css");

            var user = PrincipalFactory.CurrentPrincipal;

            IEnumerable<TitleLenderService> services = TitleLenderService.GetLenderServicesByBrokerId(user, forPolicy: false, forInteractive: true, forNonInteractive: false);
            List<ListItem> serviceDropdownOptions = services.Select(service => new ListItem(service.DisplayName, service.LenderServiceId.ToString())).ToList();
            if (serviceDropdownOptions.Count > 1)
            {
                serviceDropdownOptions.Insert(0, new ListItem(string.Empty, "-1"));
            }
            else if (serviceDropdownOptions.Count == 1)
            {
                var service = services.First();
                var vendor = TitleVendor.LoadTitleVendor(service.VendorId);
                var principal = PrincipalFactory.CurrentPrincipal;
                var dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(OrderTitleQuote));
                dataLoan.InitLoad();

                var credential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.TitleQuotes)
                    .FirstOrDefault(cred => cred.TitleQuoteLenderServiceId.HasValue && cred.TitleQuoteLenderServiceId == service.LenderServiceId);

                InitialLenderServiceData initialData = new InitialLenderServiceData();
                if (credential != null)
                {
                    initialData.HasServiceCredentials = true;
                    initialData.ServiceCredentialHasAccountId = !string.IsNullOrEmpty(credential.AccountId);
                }

                initialData.UsesAccountId = vendor.QuotingPlatform?.UsesAccountId;
                initialData.RequiresAccountId = vendor.QuotingPlatform?.RequiresAccountId;

                this.RegisterJsObjectWithJsonNetAnonymousSerializer("InitialLenderService", initialData);
            }

            this.RegisterJsObjectWithJsonNetSerializer(
                "usesProviderCodes",
                services.ToDictionary(config => config.LenderServiceId, config => config.UsesProviderCodes));

            QuoteProviders.Items.AddRange(serviceDropdownOptions.ToArray());
            QuoteProviders.DataBind();

            FeesToInclude.DataSource = Enum.GetValues(typeof(ErnstQuoteFeeCategoryFlags))
                .Cast<ErnstQuoteFeeCategoryFlags>()
                .Except( /* For initial release, no one will be using inspection or property tax for Ernst */
                    new ErnstQuoteFeeCategoryFlags[] { ErnstQuoteFeeCategoryFlags.None, ErnstQuoteFeeCategoryFlags.Inspection, ErnstQuoteFeeCategoryFlags.PropertyTax })
                .Select(enumValue => new { Description = DataAccess.EnumUtilities.GetDescription(enumValue), Value = enumValue });
            FeesToInclude.DataBind();

            Tuple<bool, string> workflowResult = DataAccess.Tools.IsWorkflowOperationAuthorized(user, this.LoanID, LendersOffice.ConfigSystem.Operations.WorkflowOperations.OrderTitleQuote);
            this.RegisterJsGlobalVariables("CanOrderTitleQuote", workflowResult.Item1);
            this.RegisterJsGlobalVariables("OrderTitleQuoteDenialReason", !workflowResult.Item1 && string.IsNullOrEmpty(workflowResult.Item2) ? "You do not have permission to order a title quote." : workflowResult.Item2);

            TitleOrder mostRecentlyAppliedOrder = TitleOrder.LoadOrdersByLoanId(this.LoanID, user.BrokerId)
                .Where(o => o.AppliedDate.HasValue)
                .OrderByDescending(o => o.AppliedDate.Value)
                .FirstOrDefault();

            if (mostRecentlyAppliedOrder?.TitleProviderCode != null)
            {
                this.RegisterJsObjectWithJsonNetAnonymousSerializer(
                    "existingTitleOrderServiceProvider",
                    new
                    {
                        mostRecentlyAppliedOrder.OrderId,
                        ProviderName = mostRecentlyAppliedOrder.QuotedFees?.Providers.FirstOrDefault(f => f.AgentRole == DataAccess.E_AgentRoleT.Title)?.Name
                    });
            }
        }

        /// <summary>
        /// Data holding info about the initial lender service data.
        /// </summary>
        private struct InitialLenderServiceData
        {
            /// <summary>
            /// Gets or sets a value indicating whether this lender service has service credentials.
            /// </summary>
            /// <value>Whether the lender service has service credentials.</value>
            public bool HasServiceCredentials { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the service credential has account id.
            /// </summary>
            /// <value>Whether the service credential has an account id.</value>
            public bool? ServiceCredentialHasAccountId { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether it uses an account id.
            /// </summary>
            /// <value>Whether it uses an account id.</value>
            public bool? UsesAccountId { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether it requires an account id.
            /// </summary>
            /// <value>Whether it requires an account id.</value>
            public bool? RequiresAccountId { get; set; }
        }
    }
}
