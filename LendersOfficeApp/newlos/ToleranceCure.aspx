﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToleranceCure.aspx.cs" Inherits="LendersOfficeApp.newlos.ToleranceCure" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Tolerance Cure</title>
        <style>
            body {
                background-color: gainsboro;
            }

            .NoArchives {
                margin-left:  5px;
                font-weight: bold;
                color: red;
            }

            .SectionHeader, .ToleranceTableSetHeader
            {
                background-color:rgb(8, 83, 147);
                color: White;
                font-weight:bold;
                margin-left:  5px;
                width: 910px;
                border: solid;
                border-color: black;
                border-width: 1px;
                border-collapse: collapse;
                padding: 0px 5px;
            }

            .ToleranceTable {
                margin-left:  10px;
                width: 900px;
                border: solid;
                border-color: black;
                border-width: 1px;
                border-collapse: collapse;
            }

            .ToleranceTable td {
                padding: 0px 5px;
            }

            .SectionHeader
            {
                background-color:rgb(8, 83, 147);
                color: White;
                font-weight:bold;
            }

            .SectionHeader td {
                border: solid;
                border-color: black;
                border-width: 1px;
            }

            .datarow td {
                background-color: white;
                border-left: solid;
                border-color: black;
                border-width: 1px;
            }

            .recFeeTotRow td {
                background-color: white;
                border-left: solid;
                border-color: black;
                border-width: 1px;
            }

            .recFeeDataRow td {
                background-color: lightgrey;
                border-left: solid;
                border-color: black;
                border-width: 1px;
            }

            .recFeeDataRow td:first-child {
                padding-left: 25px;
            }

            .sumrow td {
                background-color: white;
                border-left: solid;
                border-top: solid;
                border-color: black;
                border-width: 1px;
            }

            .feeDetails td {
                background-color: lightgrey;
                border-left: solid;
                border-color: black;
                border-width: 1px;
            }

           .feeDetails td:first-child {
                padding-left: 25px;
            }

           .feeDetails td.double-padded {
                padding-left: 50px;
            }

            .borderless td
            {
                border-left: none;
            }

            .centered {
                text-align: center;
            }

            .right {
                text-align: right;
            }

            .arrow, .recFeeTotArrow {
                float: right;
            }
        </style>
    </head>
    <body style="background-color:gainsboro;">
        <script>
            $(function () {
                $(".feeDetails").hide();
                $(".recFeeDataRow").hide();

                // Hide table headers and 10% tolerance sum/calc row if table is empty.
                $(".SectionHeader,.sumrow").each(function () {
                    var $this = $(this);

                    if ($this.parents(".ToleranceTable").find(".datarow,.recFeeDataRow").length == 0) {
                        $this.hide();
                    }
                });

                $(".recFeeTotRow").toggle($(".recFeeDataRow").length > 0); // Hide recording fee row if there are no recording fees.

                $(".Unlimited").toggle($(".Unlimited").find(".datarow").length > 0); // Hide Unlimited Tolerance Fee Table entirely if empty.

                $(".arrow").on("click", function() {
                    var $this = $(this);
                    if ($this.html() == String.fromCharCode(0x25c4)) {

                        // Show details.
                        $this.parents(".datarow,.recFeeDataRow").next(".feeDetails").show();
                        $this.html("&#x25bc;");
                    }
                    else {
                        // Hide details.
                        $this.parents(".datarow,.recFeeDataRow").next(".feeDetails").hide();
                        $this.html("&#x25c4;");
                    }
                });

                $(".recFeeTotArrow").click(function () {
                    var $this = $(this);
                    if ($this.html() == String.fromCharCode(0x25c4)) {
                        // Show details.
                        $this.parents(".ToleranceTable").find(".recFeeDataRow").show();
                        $this.html("&#x25bc;");
                    }
                    else {
                        // Hide details.
                        var $recFeeDataRow = $this.parents(".ToleranceTable").find(".recFeeDataRow");
                        $recFeeDataRow.hide();
                        $recFeeDataRow.next(".feeDetails").hide();
                        $recFeeDataRow.find(".arrow").html("&#x25c4;");
                        $this.html("&#x25c4;");
                    }
                });

                $(".cureArrow").on("click", function () {
                    var $this = $(this);
                    if ($this.html() == String.fromCharCode(0x25ba)) {

                        // Show details.
                        $this.parents(".ToleranceTableSet").find(".ToleranceCureTables").show();
                        $this.html("&#x25bc;");
                    }
                    else {
                        // Hide details.
                        $this.parents(".ToleranceTableSet").find(".ToleranceCureTables").hide();
                        $this.html("&#x25ba;");
                    }
                });

                $(".PendingToleranceTableSetHeader").find(".cureArrow").click();   // Close Pending tolerance cure accordian by default.
            });

            function _init() {
                lockField(<%= AspxTools.JsGetElementById(sToleranceZeroPercentCureLckd) %>, 'sToleranceZeroPercentCure');
                lockField(<%= AspxTools.JsGetElementById(sToleranceTenPercentCureLckd) %>, 'sToleranceTenPercentCure');
            }
        </script>
        <form id="form1" runat="server">
            <div class="MainRightHeader" runat="server" id="PageHeader">Tolerance Cure</div>
            <br />
            <div id="divNoArchives" runat="server" class="NoArchives" visible="false">No valid LE archives in file to calculate cures</div>

            <div class="ToleranceTableSet">
                <div id="CurrentToleranceCureHeader" runat="server" class="ToleranceTableSetHeader" visible="false">
                    <span class="cureArrow">&#x25bc</span>
                    <span>Current tolerance cure calculations</span>
                </div>
                
                <div class="ToleranceCureTables" >
                    <br />
                    <table class="ToleranceTable" cellspacing="0" cellpadding="0">
                        <tr class="GridHeader">
                            <td>
                                Total Tolerance Cure
                            </td>
                            <td width="42.5%" class="right">
                                <ml:MoneyTextBox ID="sToleranceCure" runat="server" width="90" preset="money" ReadOnly />
                            </td>
                        </tr>
                    </table>
            
                    <br />
                    <table class="ToleranceTable" cellspacing="0" cellpadding="0">
                        <tr class="GridHeader">
                            <td colspan="2">
                                Zero Tolerance Cure
                            </td>
                            <td colspan="3" class="right">
                                Lock <asp:CheckBox ID="sToleranceZeroPercentCureLckd" runat="server" onchange="refreshCalculation();"/>
                                <ml:MoneyTextBox ID="sToleranceZeroPercentCure" runat="server" width="90" preset="money" onchange="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr class="SectionHeader">
                            <td width="30%">
                                Fee Description
                            </td>
                            <td width="27.5%" class="centered">
                                Last Disclosed LE (<span id="sLastDisclosedClosingCostArchiveD" runat="server" />)
                            </td>
                            <td width="27.5%" class="centered">
                                Closing Disclosure (Live Data)
                            </td>
                            <td width="7.5%" class="centered">
                                Difference
                            </td>
                            <td width="7.5%" class="centered">
                                Cure
                            </td>
                        </tr>

                        <tr id="trLenderCredit" runat="server" class="datarow" visible="false">
                            <td>Lender Credit</td>
                            <td id="archiveAmount_lc" runat="server" class="centered" />
                            <td id="liveAmount_lc" runat="server" class="centered" />
                            <td id="diffAmount_lc" runat="server" class="centered" />
                            <td id="cureAmount_lc" runat="server" class="centered" />
                        </tr>

                        <asp:Repeater id="ZeroToleranceRepeater" runat="server" OnItemDataBound="ToleranceRepeater_ItemDataBound" >
                            <ItemTemplate>
                                <tr class="datarow">
                                    <td>
                                        <span id="desc" runat="server" />
                                        <span class="arrow">&#x25c4;</span>
                                    </td>
                                    <td id="archiveAmount" runat="server" class="centered" />
                                    <td id="liveAmount" runat="server" class="centered" />
                                    <td id="diffAmount" runat="server" class="centered" />
                                    <td id="cureAmount" runat="server" class="centered" />
                                </tr>

                                <tr class="feeDetails">
                                    <td>
                                        Fee Type: <span id="origDesc" runat="server" />
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="archiveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="archiveTp" runat="server" Enabled="false" /><label for="archiveTp">TP</label></td>
                                                <td><asp:CheckBox ID="archiveAff" runat="server" Enabled="false" /><label for="archiveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="archiveCanShop" runat="server" Enabled="false" /><label for="archiveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="archiveDidShop" runat="server" Enabled="false" /><label for="archiveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="liveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="liveTp" runat="server" Enabled="false" /><label for="liveTp">TP</label></td>
                                                <td><asp:CheckBox ID="liveAff" runat="server" Enabled="false" /><label for="liveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="liveCanShop" runat="server" Enabled="false" /><label for="liveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="liveDidShop" runat="server" Enabled="false" /><label for="liveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>

                    <br />
                    <table class="ToleranceTable" cellspacing="0" cellpadding="0">
                        <tr class="GridHeader">
                            <td colspan="2">
                                10% Cumulative Tolerance Cure
                            </td>
                            <td colspan="2" class="right">
                                Lock <asp:CheckBox ID="sToleranceTenPercentCureLckd" runat="server" onchange="refreshCalculation();"/>
                                <ml:MoneyTextBox ID="sToleranceTenPercentCure" runat="server" width="90" preset="money" onchange="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr class="SectionHeader">
                            <td width="30%">
                                Fee Description
                            </td>
                            <td width="27.5%" class="centered">
                                10% Tolerance Basis (<span id="sTolerance10BasisLEArchiveD" runat="server" />)
                            </td>
                            <td width="27.5%" class="centered">
                                Closing Disclosure (Live Data)
                            </td>
                            <td width="15%" class="centered">
                                Difference
                            </td>
                        </tr>

                        <tr class="recFeeTotRow">
                            <td>
                                Recording Fee
                                <span class="recFeeTotArrow">&#x25c4;</span>
                            </td>
                            <td id="recordingFee_archiveAmount" runat="server" class="centered" />
                            <td id="recordingFee_liveAmount" runat="server" class="centered" />
                            <td id="recordingFee_diffAmount" runat="server" class="centered" />
                        </tr>

                        <asp:Repeater id="RecordingFeeRepeater" runat="server" OnItemDataBound="ToleranceRepeater_ItemDataBound" >
                            <ItemTemplate>
                                <tr class="recFeeDataRow" >
                                    <td>
                                        <span id="desc" runat="server" />
                                        <span class="arrow">&#x25c4;</span>
                                    </td>
                                    <td id="archiveAmount" runat="server" class="centered" />
                                    <td id="liveAmount" runat="server" class="centered" />
                                    <td id="diffAmount" runat="server" class="centered" />
                                </tr>

                                <tr class="feeDetails">
                                    <td class="double-padded">
                                        Fee Type: <span id="origDesc" runat="server" />
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="archiveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="archiveTp" runat="server" Enabled="false" /><label for="archiveTp">TP</label></td>
                                                <td><asp:CheckBox ID="archiveAff" runat="server" Enabled="false" /><label for="archiveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="archiveCanShop" runat="server" Enabled="false" /><label for="archiveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="archiveDidShop" runat="server" Enabled="false" /><label for="archiveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="liveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="liveTp" runat="server" Enabled="false" /><label for="liveTp">TP</label></td>
                                                <td><asp:CheckBox ID="liveAff" runat="server" Enabled="false" /><label for="liveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="liveCanShop" runat="server" Enabled="false" /><label for="liveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="liveDidShop" runat="server" Enabled="false" /><label for="liveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>

                        <asp:Repeater id="TenToleranceRepeater" runat="server" OnItemDataBound="ToleranceRepeater_ItemDataBound" >
                            <ItemTemplate>
                                <tr class="datarow">
                                    <td>
                                        <span id="desc" runat="server" />
                                        <span class="arrow">&#x25c4;</span>
                                    </td>
                                    <td id="archiveAmount" runat="server" class="centered" />
                                    <td id="liveAmount" runat="server" class="centered" />
                                    <td id="diffAmount" runat="server" class="centered" />
                                </tr>

                                <tr class="feeDetails">
                                    <td>
                                        Fee Type: <span id="origDesc" runat="server" />
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="archiveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="archiveTp" runat="server" Enabled="false" /><label for="archiveTp">TP</label></td>
                                                <td><asp:CheckBox ID="archiveAff" runat="server" Enabled="false" /><label for="archiveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="archiveCanShop" runat="server" Enabled="false" /><label for="archiveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="archiveDidShop" runat="server" Enabled="false" /><label for="archiveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="liveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="liveTp" runat="server" Enabled="false" /><label for="liveTp">TP</label></td>
                                                <td><asp:CheckBox ID="liveAff" runat="server" Enabled="false" /><label for="liveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="liveCanShop" runat="server" Enabled="false" /><label for="liveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="liveDidShop" runat="server" Enabled="false" /><label for="liveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>

                        <tr class="sumrow">
                            <td>
                                Sum of 10% Fees
                            </td>
                            <td id="archiveSum" runat="server" class="centered" />
                            <td id="liveSum" runat="server" class="centered" />
                            <td id="diffSum" runat="server" class="centered" />
                        </tr>
                        <tr class="sumrow">
                            <td colspan="4">
                                [1] Sum of 10% Tolerance Basis = <span id="archiveSum_2" runat="server" /> 
                                <br />
                                [2] 10% of [1] = <span id="archiveSum_TenPercent" runat="server" /> 
                                <br />
                                [3] Sum of 10% Fees of Closing Disclosure = <span id="liveSum_2" runat="server" /> 
                                <br />
                                <br />
                                CALCULATION: Total 10% Cumulative Tolerance Cure = [3] - ([2] + [1]) = 
                                <span id="liveSum_3" runat="server" /> - (<span id="archiveSum_TenPercent_2" runat="server" /> + <span id="archiveSum_3" runat="server" />) = 
                                <b><span id="tenPercentCureCalc" runat="server" /> (*** If this value is less than 0, then show $0.00***)</b>
                            </td>
                        </tr>
                    </table>

                    <br />
                    <table class="ToleranceTable Unlimited" cellspacing="0" cellpadding="0">
                        <tr class="GridHeader">
                            <td colspan="4">
                                Unlimited Tolerance Fees
                            </td>
                        </tr>
                        <tr class="SectionHeader">
                            <td width="30%">
                                Fee Description
                            </td>
                            <td width="27.5%" class="centered">
                                Last Disclosed LE (<span id="sLastDisclosedClosingCostArchiveD_2" runat="server" />)
                            </td>
                            <td width="27.5%" class="centered">
                                Closing Disclosure (Live Data)
                            </td>
                            <td width="15%" class="centered">
                                Difference
                            </td>
                        </tr>

                        <asp:Repeater id="UnlimitedToleranceRepeater" runat="server" OnItemDataBound="ToleranceRepeater_ItemDataBound" >
                            <ItemTemplate>
                                <tr class="datarow">
                                    <td>
                                        <span id="desc" runat="server" />
                                        <span class="arrow">&#x25c4;</span>
                                    </td>
                                    <td id="archiveAmount" runat="server" class="centered" />
                                    <td id="liveAmount" runat="server" class="centered" />
                                    <td id="diffAmount" runat="server" class="centered" />
                                </tr>

                                <tr class="feeDetails">
                                    <td>
                                        Fee Type: <span id="origDesc" runat="server" />
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="archiveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="archiveTp" runat="server" Enabled="false" /><label for="archiveTp">TP</label></td>
                                                <td><asp:CheckBox ID="archiveAff" runat="server" Enabled="false" /><label for="archiveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="archiveCanShop" runat="server" Enabled="false" /><label for="archiveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="archiveDidShop" runat="server" Enabled="false" /><label for="archiveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="liveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="liveTp" runat="server" Enabled="false" /><label for="liveTp">TP</label></td>
                                                <td><asp:CheckBox ID="liveAff" runat="server" Enabled="false" /><label for="liveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="liveCanShop" runat="server" Enabled="false" /><label for="liveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="liveDidShop" runat="server" Enabled="false" /><label for="liveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>

            <div id="PendingToleranceCure" runat="server" class="ToleranceTableSet" visible="false">
                <br />
                <div class="ToleranceTableSetHeader PendingToleranceTableSetHeader">
                    <span class="cureArrow">&#x25bc</span>
                    <span>Preview of tolerance cure calculations if the pending archive is disclosed</span>
                </div>
                
                <div class="ToleranceCureTables" >
                    <br />
                    <table class="ToleranceTable" cellspacing="0" cellpadding="0">
                        <tr class="GridHeader">
                            <td>
                                Total Tolerance Cure
                            </td>
                            <td width="42.5%" class="right">
                                <ml:MoneyTextBox ID="sToleranceCureAfterPendingIsDisclosed" runat="server" width="90" preset="money" ReadOnly />
                            </td>
                        </tr>
                    </table>
            
                    <br />
                    <table class="ToleranceTable" cellspacing="0" cellpadding="0">
                        <tr class="GridHeader">
                            <td colspan="2">
                                Zero Tolerance Cure
                            </td>
                            <td colspan="3" class="right">
                                <ml:MoneyTextBox ID="sToleranceZeroPercentCureAfterPendingIsDisclosed" runat="server" width="90" preset="money" ReadOnly />
                            </td>
                        </tr>
                        <tr class="SectionHeader">
                            <td width="30%">
                                Fee Description
                            </td>
                            <td width="27.5%" class="centered">
                                Pending LE (<span id="sLoanEstimateArchiveInPendingStatusD" runat="server" />)
                            </td>
                            <td width="27.5%" class="centered">
                                Closing Disclosure (Live Data)
                            </td>
                            <td width="7.5%" class="centered">
                                Difference
                            </td>
                            <td width="7.5%" class="centered">
                                Cure
                            </td>
                        </tr>

                        <tr id="trPendingLenderCredit" runat="server" class="datarow" visible="false">
                            <td>Lender Credit</td>
                            <td id="pending_lc_archiveAmount" runat="server" class="centered" />
                            <td id="pending_lc_liveAmount" runat="server" class="centered" />
                            <td id="pending_lc_diffAmount" runat="server" class="centered" />
                            <td id="pending_lc_cureAmount" runat="server" class="centered" />
                        </tr>

                        <asp:Repeater id="PendingZeroToleranceRepeater" runat="server" OnItemDataBound="ToleranceRepeater_ItemDataBound" >
                            <ItemTemplate>
                                <tr class="datarow">
                                    <td>
                                        <span id="desc" runat="server" />
                                        <span class="arrow">&#x25c4;</span>
                                    </td>
                                    <td id="archiveAmount" runat="server" class="centered" />
                                    <td id="liveAmount" runat="server" class="centered" />
                                    <td id="diffAmount" runat="server" class="centered" />
                                    <td id="cureAmount" runat="server" class="centered" />
                                </tr>

                                <tr class="feeDetails">
                                    <td>
                                        Fee Type: <span id="origDesc" runat="server" />
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="archiveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="archiveTp" runat="server" Enabled="false" /><label for="archiveTp">TP</label></td>
                                                <td><asp:CheckBox ID="archiveAff" runat="server" Enabled="false" /><label for="archiveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="archiveCanShop" runat="server" Enabled="false" /><label for="archiveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="archiveDidShop" runat="server" Enabled="false" /><label for="archiveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="liveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="liveTp" runat="server" Enabled="false" /><label for="liveTp">TP</label></td>
                                                <td><asp:CheckBox ID="liveAff" runat="server" Enabled="false" /><label for="liveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="liveCanShop" runat="server" Enabled="false" /><label for="liveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="liveDidShop" runat="server" Enabled="false" /><label for="liveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>

                    <br />
                    <table class="ToleranceTable" cellspacing="0" cellpadding="0">
                        <tr class="GridHeader">
                            <td colspan="2">
                                10% Cumulative Tolerance Cure
                            </td>
                            <td colspan="2" class="right">
                                <ml:MoneyTextBox ID="sToleranceTenPercentCureAfterPendingIsDisclosed" runat="server" width="90" preset="money" ReadOnly />
                            </td>
                        </tr>
                        <tr class="SectionHeader">
                            <td width="30%">
                                Fee Description
                            </td>
                            <td width="27.5%" class="centered">
                                10% Tolerance Basis (<span id="pending10BasisArchiveD" runat="server" />)
                            </td>
                            <td width="27.5%" class="centered">
                                Closing Disclosure (Live Data)
                            </td>
                            <td width="15%" class="centered">
                                Difference
                            </td>
                        </tr>

                        <tr class="recFeeTotRow">
                            <td>
                                Recording Fee
                                <span class="recFeeTotArrow">&#x25c4;</span>
                            </td>
                            <td id="pending_recordingFee_archiveAmount" runat="server" class="centered" />
                            <td id="pending_recordingFee_liveAmount" runat="server" class="centered" />
                            <td id="pending_recordingFee_diffAmount" runat="server" class="centered" />
                        </tr>

                        <asp:Repeater id="PendingRecordingFeeRepeater" runat="server" OnItemDataBound="ToleranceRepeater_ItemDataBound" >
                            <ItemTemplate>
                                <tr class="recFeeDataRow" >
                                    <td>
                                        <span id="desc" runat="server" />
                                        <span class="arrow">&#x25c4;</span>
                                    </td>
                                    <td id="archiveAmount" runat="server" class="centered" />
                                    <td id="liveAmount" runat="server" class="centered" />
                                    <td id="diffAmount" runat="server" class="centered" />
                                </tr>

                                <tr class="feeDetails">
                                    <td class="double-padded">
                                        Fee Type: <span id="origDesc" runat="server" />
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="archiveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="archiveTp" runat="server" Enabled="false" /><label for="archiveTp">TP</label></td>
                                                <td><asp:CheckBox ID="archiveAff" runat="server" Enabled="false" /><label for="archiveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="archiveCanShop" runat="server" Enabled="false" /><label for="archiveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="archiveDidShop" runat="server" Enabled="false" /><label for="archiveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="liveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="liveTp" runat="server" Enabled="false" /><label for="liveTp">TP</label></td>
                                                <td><asp:CheckBox ID="liveAff" runat="server" Enabled="false" /><label for="liveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="liveCanShop" runat="server" Enabled="false" /><label for="liveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="liveDidShop" runat="server" Enabled="false" /><label for="liveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>

                        <asp:Repeater id="PendingTenToleranceRepeater" runat="server" OnItemDataBound="ToleranceRepeater_ItemDataBound" >
                            <ItemTemplate>
                                <tr class="datarow">
                                    <td>
                                        <span id="desc" runat="server" />
                                        <span class="arrow">&#x25c4;</span>
                                    </td>
                                    <td id="archiveAmount" runat="server" class="centered" />
                                    <td id="liveAmount" runat="server" class="centered" />
                                    <td id="diffAmount" runat="server" class="centered" />
                                </tr>

                                <tr class="feeDetails">
                                    <td>
                                        Fee Type: <span id="origDesc" runat="server" />
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="archiveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="archiveTp" runat="server" Enabled="false" /><label for="archiveTp">TP</label></td>
                                                <td><asp:CheckBox ID="archiveAff" runat="server" Enabled="false" /><label for="archiveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="archiveCanShop" runat="server" Enabled="false" /><label for="archiveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="archiveDidShop" runat="server" Enabled="false" /><label for="archiveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="liveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="liveTp" runat="server" Enabled="false" /><label for="liveTp">TP</label></td>
                                                <td><asp:CheckBox ID="liveAff" runat="server" Enabled="false" /><label for="liveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="liveCanShop" runat="server" Enabled="false" /><label for="liveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="liveDidShop" runat="server" Enabled="false" /><label for="liveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>

                        <tr class="sumrow">
                            <td>
                                Sum of 10% Fees
                            </td>
                            <td id="pending_archiveSum" runat="server" class="centered" />
                            <td id="pending_liveSum" runat="server" class="centered" />
                            <td id="pending_diffSum" runat="server" class="centered" />
                        </tr>
                        <tr class="sumrow">
                            <td colspan="4">
                                [1] Sum of 10% Tolerance Basis = <span id="pending_archiveSum_2" runat="server" /> 
                                <br />
                                [2] 10% of [1] = <span id="pending_archiveSum_TenPercent" runat="server" /> 
                                <br />
                                [3] Sum of 10% Fees of Closing Disclosure = <span id="pending_liveSum_2" runat="server" /> 
                                <br />
                                <br />
                                CALCULATION: Total 10% Cumulative Tolerance Cure = [3] - ([2] + [1]) = 
                                <span id="pending_liveSum_3" runat="server" /> - (<span id="pending_archiveSum_TenPercent_2" runat="server" /> + <span id="pending_archiveSum_3" runat="server" />) = 
                                <b><span id="pendingTenPercentCureCalc" runat="server" /> (*** If this value is less than 0, then show $0.00***)</b>
                            </td>
                        </tr>
                    </table>

                    <br />
                    <table class="ToleranceTable Unlimited" cellspacing="0" cellpadding="0">
                        <tr class="GridHeader">
                            <td colspan="4">
                                Unlimited Tolerance Fees
                            </td>
                        </tr>
                        <tr class="SectionHeader">
                            <td width="30%">
                                Fee Description
                            </td>
                            <td width="27.5%" class="centered">
                                Pending LE (<span id="sLoanEstimateArchiveInPendingStatusD_2" runat="server" />)
                            </td>
                            <td width="27.5%" class="centered">
                                Closing Disclosure (Live Data)
                            </td>
                            <td width="15%" class="centered">
                                Difference
                            </td>
                        </tr>

                        <asp:Repeater id="PendingUnlimitedToleranceRepeater" runat="server" OnItemDataBound="ToleranceRepeater_ItemDataBound" >
                            <ItemTemplate>
                                <tr class="datarow">
                                    <td>
                                        <span id="desc" runat="server" />
                                        <span class="arrow">&#x25c4;</span>
                                    </td>
                                    <td id="archiveAmount" runat="server" class="centered" />
                                    <td id="liveAmount" runat="server" class="centered" />
                                    <td id="diffAmount" runat="server" class="centered" />
                                </tr>

                                <tr class="feeDetails">
                                    <td>
                                        Fee Type: <span id="origDesc" runat="server" />
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="archiveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="archiveTp" runat="server" Enabled="false" /><label for="archiveTp">TP</label></td>
                                                <td><asp:CheckBox ID="archiveAff" runat="server" Enabled="false" /><label for="archiveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="archiveCanShop" runat="server" Enabled="false" /><label for="archiveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="archiveDidShop" runat="server" Enabled="false" /><label for="archiveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="borderless">
                                            <tr>
                                                <td>Section: <span id="liveSection" runat="server" /></td>
                                                <td><asp:CheckBox ID="liveTp" runat="server" Enabled="false" /><label for="liveTp">TP</label></td>
                                                <td><asp:CheckBox ID="liveAff" runat="server" Enabled="false" /><label for="liveAff">AFF</label></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><asp:CheckBox ID="liveCanShop" runat="server" Enabled="false" /><label for="liveCanShop">Can Shop</label></td>
                                                <td><asp:CheckBox ID="liveDidShop" runat="server" Enabled="false" /><label for="liveDidShop">Did Shop</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>

        </form>
    </body>
</html>
