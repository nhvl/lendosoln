<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="BorrowerInfoTab.ascx.cs" Inherits="LendersOfficeApp.newlos.BorrowerInfoTab" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<style type="text/css">
.languagePrefTable, .borrInfoTable {
    padding: 3px;
    width: 100%;
}
.borrInfoTableContainerTd {
    padding: 3px;
}
.DomesticRelationLabel {
    vertical-align: baseline;
}
.DomesticRelationshipRBList {
    display: inline-table;
    vertical-align:inherit;
}
.width-125 {
    width: 125px;
}
.width-80 {
    width: 80px;
}
.InsetBorder .indent {    
    padding-left: 30px;
}
.InsetBorder .extra-indent {
    padding-left: 60px;
}
#aBVServiceTDeceasedNotice, #aCVServiceTDeceasedNotice {
    color: red;
}
</style>

<asp:HiddenField ID="aBAlias" runat="server" />
<asp:HiddenField ID="aBPowerOfAttorneyNm" runat="server" />
<asp:HiddenField ID="aCAlias" runat="server" />
<asp:HiddenField ID="aCPowerOfAttorneyNm" runat="server" />

<table class="FormTable" id="Table2" cellspacing="2" cellpadding="0" border="0">
    <tr>
        <td class="FieldLabel" colspan="2">
            Subject property occupancy type
            <asp:DropDownList ID="aOccT" runat="server">
            </asp:DropDownList>
        </td>
    </tr>
	<tr>
		<td><asp:checkbox id="sMultiApps" ToolTip="The income or assets of a person other than the &quot;Borrower&quot; (including the Borrower's spouse) will be used as a basis for loan qualification." Height="18px" runat="server" Text='The income or assets of a person other than the "Borrower"  will be used...' ></asp:checkbox></td>
	</tr>
	<tr>
		<td><asp:checkbox id="aSpouseIExcl" ToolTip="The income or assets of the Borrower's spouse will not be used as a basis for loan qualification, but his or her liabilities must be considered because the Borrower resides in a community property state, the security property is located in a community property state, or the Borrower is relying on other property located in a community property state as a basis for repayment of the loan." runat="server" Text="The income or assets of the Borrower's spouse will not be used ..." ></asp:checkbox></td>
	</tr>
    <tr style="padding-top: 5px">
        <td class="FieldLabel" colspan="2">
            <a href="#" onclick="redirectToUladPage('ApplicationManagement');" title="Go to Borr/Coborr Management">Add and Swap co-borrower ...</a>
        </td>
    </tr>
    <tr runat="server" id="CoborrowerDefinedRow">
        <td>&nbsp;</td>
        <td class="FieldLabel" style="padding-left: 10px;">
            <input type="checkbox" runat="server" id="aHasCoborrowerData" data-field-id="aHasCoborrower" />
            <input type="checkbox" runat="server" id="aHasCoborrowerCalc" disabled="disabled" data-field-id="aHasCoborrower" />
            Co-borrower defined?
        </td>
    </tr>
    <tr>
        <td valign="top" class="borrInfoTableContainerTd">
            <table cellpadding="0" cellspacing="0" class="InsetBorder borrInfoTable">
                <tr>
                    <td>
                        <table id="Table4" cellspacing="0" cellpadding="0" class="borrowerInfoTable" >
                            <tr style="height: 21px">
                                <td class="FormTableSubHeader" colspan="4">
                                    Borrower Information
                                    <asp:HiddenField runat="server" ID="IsBeyondV1_CalcVaLoanElig" />
                                    <asp:HiddenField runat="server" ID="aIsBVAElig" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    First Name
                                </td>
                                <td class="FieldLabelNoPadding" nowrap colspan="3">
                                    <asp:TextBox ID="aBFirstNm" runat="server" MaxLength="21" Width="127px" onchange="updateBorrowerName();"></asp:TextBox>&nbsp;Middle Name<asp:TextBox ID="aBMidNm" MaxLength="21" runat="server" Width="70px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Preferred Name
                                </td>
                                <td class="FieldLabelNoPadding" nowrap colspan="3">
                                    <asp:TextBox ID="aBPreferredNm" runat="server" MaxLength="21" Width="127px"></asp:TextBox>&nbsp;
                                    <asp:CheckBox ID="aBPreferredNmLckd" runat="server" onclick="refreshCalculation();" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Last Name
                                </td>
                                <td class="FieldLabelNoPadding" colspan="3">
                                    <asp:TextBox ID="aBLastNm" runat="server" MaxLength="21" Width="127px" onchange="updateBorrowerName();"></asp:TextBox>&nbsp;Suffix
                                    <ml:ComboBox id="aBSuffix" runat="server" Width="30px"></ml:ComboBox>
                                    <a href="#" onclick="onEditBorrAliasesClick()">Edit Aliases/POA</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    SSN
                                </td>
                                <td class="FieldLabelNoPadding" colspan="3">
                                    <ml:SSNTextBox ID="aBSsn" runat="server" Width="90" preset="ssn"></ml:SSNTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" title="(MM/DD/YYYY)">
                                    DOB
                                </td>
                                <td class="FieldLabelNoPadding" colspan="3">
                                    <ml:DateTextBox ID="aBDob" runat="server" preset="date" Width="75"></ml:DateTextBox>&nbsp; Age
                                    <asp:TextBox ID="aBAge" runat="server" Width="34px" MaxLength="3"></asp:TextBox>&nbsp;Yrs. School
                                    <asp:TextBox ID="aBSchoolYrs" runat="server" Width="34px" MaxLength="3"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Citizenship</td>
                                <td colspan="3">
                                    <asp:dropdownlist id="aProdBCitizenT" runat="server"></asp:dropdownlist>                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="2">
                                    <span class="see-declarations-item-j-k-container">
                                        see <a href="#" onclick="goToDeclarations();">declarations</a> items j & k
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" nowrap>
                                    Marital Status
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="aBMaritalStatT" runat="server" onchange="onaBMaritalStatTChange()">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="ShowOnlyIfBorrowerNotMarried">
                                <td class="FieldLabel" colspan="4">
                                    <span class="DomesticRelationLabel">
                                        Is in a domestic relationship (real property rights similar to spouse)?
                                    </span>
                                    <asp:RadioButtonList class="DomesticRelationshipRBList" ID="aBDomesticRelationshipTri" runat="server" onclick="onaBDomesticRelationshipTriClick()" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="HideIfaBDomesticRelationshipTriIsNo">
                                <td class="FieldLabel" colspan="2">
                                    Type of relationship
                                </td>
                            </tr>
                            <tr class="HideIfaBDomesticRelationshipTriIsNo">
                                <td colspan="4">
                                    <asp:DropDownList runat="server" ID="aBDomesticRelationshipType" onchange="onaBDomesticRelationshipTypeChange()"></asp:DropDownList>
                                    <span>
                                        If other, explain 
                                    </span>
                                    <asp:TextBox class="width-125" runat="server" ID="aBDomesticRelationshipTypeOtherDescription"></asp:TextBox>
                                </td>                                
                            </tr>
                            <tr class="HideIfaBDomesticRelationshipTriIsNo">
                                <td class="FieldLabel" colspan="4">
                                    State where relation was formed <ml:StateDropDownList runat="server" ID="aBDomesticRelationshipStateCode"></ml:StateDropDownList>
                                </td>
                            </tr>
                            <tr class="HideIfaBDomesticRelationshipTriIsNo">
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    No. of Deps
                                </td>
                                <td class="FieldLabelNoPadding" colspan="3">
                                    <asp:TextBox ID="aBDependNum" runat="server" Width="34px" MaxLength="3" title="(not listed by Co-Borrower)"></asp:TextBox>&nbsp; Dependents' Ages&nbsp;<asp:TextBox ID="aBDependAges" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Home Phone
                                </td>
                                <td class="FieldLabelNoPadding" nowrap colspan="3">
                                    <ml:PhoneTextBox ID="aBHPhone" runat="server" preset="phone" Width="120"></ml:PhoneTextBox>&nbsp;Cellphone&nbsp;<ml:PhoneTextBox ID="aBCellphone" Width="120" runat="server" preset="phone"></ml:PhoneTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Work Phone
                                </td>
                                <td class="FieldLabel" colspan="3">
                                    <ml:PhoneTextBox ID="aBBusPhone" runat="server" preset="phone" Width="120"></ml:PhoneTextBox>&nbsp;Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                    <ml:PhoneTextBox ID="aBFax" runat="server" Width="120" preset="phone"></ml:PhoneTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Email
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="aBEmail" runat="server"></asp:TextBox><asp:regularexpressionvalidator id=RegularExpressionValidator1 runat="server" ControlToValidate="aBEmail" Display="Dynamic" ErrorMessage=" Invalid Email"></asp:regularexpressionvalidator>&nbsp;<a onclick="window.open('mailto:' + document.getElementById('<%= AspxTools.ClientId(aBEmail) %>').value);" href="#" tabindex="-1">send email</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Type
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="aBTypeT" runat="server"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="FieldLabel" id="aBFthbAndHousingHistContainer" runat="server" colspan="3">
                                    <span >
                                        <asp:CheckBox ID="aBTotalScoreIsFthb" runat="server" onclick="FthbClick('B');" /> First Time Home Buyer?
                                    </span>
                                    <span class="FthbExplanation">
                                        see <a href="#" onclick="goToDeclarations();">declarations</a> item <span class="first-time-home-buyer-declarations-item-number"></span>
                                    </span>
                                    <span style="display:none;"  runat="server" id="aBHasHousingHistContainer">
                                    <asp:CheckBox ID="aBHasHousingHist" runat="server" /> Has Housing History?
                                    </span>
                                </td>
                            </tr>
                            <tr id="BorrowerMembershipId" runat="server">
                                <td class="FieldLabel">
                                    Member ID
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="aBCoreSystemId" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="aBIsVeteranRow" runat="server">
                                <td class="FieldLabel">
                                    Veteran
                                </td>
                                <td colspan="2">
                                    <asp:RadioButtonList class="RefreshOnChange" ID="aBIsVeteran" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="BorrowerVeteranMilitaryServiceRow">
                                <td class="FieldLabel indent">
                                    Active Duty
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="aBActiveDuty" runat="server" RepeatDirection="Horizontal" onchange="onBorrowerActiveDutyChange();"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="BorrowerVeteranMilitaryServiceRow">
                                <td colspan="3" class="extra-indent">
                                    Expiration of service / tour: <ml:DateTextBox ID="aBActiveDutyExpirationDate" runat="server" preset="date"></ml:DateTextBox>
                                </td>                                
                            </tr>
                            <tr class="BorrowerVeteranMilitaryServiceRow">
                                <td class="FieldLabel width-80 indent">
                                    Retired / Discharged / Separated
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="aBRetiredDischargedOrSeparated" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="BorrowerVeteranMilitaryServiceRow">
                                <td class="FieldLabel width-80 indent">
                                    Non-activated Reserve / National Guard Member
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="aBReserveNationalGuardNeverActivated" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Surviving Spouse of Veteran
                                </td>
                                <td colspan="2">
                                    <asp:RadioButtonList class="RefreshOnChange" ID="aBIsSurvivingSpouseOfVeteran" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="HideThis">
                                <td class="FieldLabel">
                                    Service Type
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList runat="server" ID="aBVServiceT"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td id="aBVServiceTDeceasedNotice">
                                    Service type of deceased veteran
                                </td>
                            </tr>
                            <tr class="HideThis">
                                <td class="FieldLabel">
                                    VA Entitlement
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList runat="server" ID="aBVEnt"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="HideThis">
                                <td class="FieldLabel">
                                    VA Funding Fee Exempt
                                </td>
                                <td colspan="3">
                                    <asp:RadioButtonList runat="server" ID="aIsBVAFFEx" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" colspan="4">
                                    <table id="Table6" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="FormTableSubHeader" colspan="2">
                                                Present Address
                                            </td>
                                            <td class="FormTableSubHeader" align="right" colspan="4">
                                                <input onclick="fillPresentAddress();" type="button" value="Copy from property address" style="width: 180px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                Street
                                            </td>
                                            <td class="FieldLabel">
                                                City
                                            </td>
                                            <td class="FieldLabel">
                                                ST
                                            </td>
                                            <td class="FieldLabel">
                                                Zip
                                            </td>
                                            <td class="FieldLabel">
                                                Own/Rent
                                            </td>
                                            <td class="FieldLabel">
                                                # Yrs
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="aBAddr" runat="server" MaxLength="36" onchange="syncMailingAddress();enableVorBtn(true);"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aBCity" runat="server" Width="92px" onchange="syncMailingAddress();enableVorBtn(true);"></asp:TextBox>
                                            </td>
                                            <td>
                                                <ml:StateDropDownList ID="aBState" runat="server" onchange="syncMailingAddress();enableVorBtn(true);"></ml:StateDropDownList>
                                            </td>
                                            <td>
                                                <ml:ZipcodeTextBox ID="aBZip" runat="server" Width="50" preset="zipcode" onchange="syncMailingAddress();enableVorBtn(true);"></ml:ZipcodeTextBox>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="aBAddrT" runat="server" Width="59px" onchange="enableVorBtn(true);">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aBAddrYrs" runat="server" MaxLength="3" Width="34px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                Mailing Address
                                            </td>
                                            <td align="left" colspan="5" class="FieldLabel">
                                                <asp:DropDownList ID="aBAddrMailSourceT" runat="server" onchange="syncMailingAddress();"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                <asp:TextBox ID="aBAddrMail" MaxLength="36" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aBCityMail" runat="server" Width="92px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <ml:StateDropDownList ID="aBStateMail" runat="server"></ml:StateDropDownList>
                                            </td>
                                            <td>
                                                <ml:ZipcodeTextBox ID="aBZipMail" Width="50" runat="server" preset="zipcode"></ml:ZipcodeTextBox>
                                            </td>
                                            <td><input type="button" onclick="EditVOR(true);" id="EditVORB" value="Edit VOR" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                Former Addresses
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="aBPrev1Addr" runat="server" MaxLength="36"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aBPrev1City" runat="server" Width="92px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <ml:StateDropDownList ID="aBPrev1State" runat="server"></ml:StateDropDownList>
                                            </td>
                                            <td>
                                                <ml:ZipcodeTextBox ID="aBPrev1Zip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="aBPrev1AddrT" runat="server" Width="59px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aBPrev1AddrYrs" runat="server" MaxLength="3" Width="34px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="aBPrev2Addr" runat="server" MaxLength="36"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aBPrev2City" runat="server" Width="92px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <ml:StateDropDownList ID="aBPrev2State" runat="server"></ml:StateDropDownList>
                                            </td>
                                            <td>
                                                <ml:ZipcodeTextBox ID="aBPrev2Zip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="aBPrev2AddrT" runat="server" Width="59px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aBPrev2AddrYrs" runat="server" MaxLength="3" Width="34px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                Address After Closing
                                            </td>
                                            <td align="left" colspan="5" class="FieldLabel">
                                                <asp:CheckBox ID="aBAddrPostSourceTLckd" runat="server" onclick="syncMailingAddress();" />
                                                <asp:DropDownList ID="aBAddrPostSourceT" runat="server" onchange="syncMailingAddress();"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                <asp:TextBox ID="aBAddrPost" MaxLength="36" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aBCityPost" runat="server" Width="92px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <ml:StateDropDownList ID="aBStatePost" runat="server"></ml:StateDropDownList>
                                            </td>
                                            <td>
                                                <ml:ZipcodeTextBox ID="aBZipPost" Width="50" runat="server" preset="zipcode"></ml:ZipcodeTextBox>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                            
                        </table>
                    </td>
                </tr>
            </table>
            <table class="languagePrefTable InsetBorder" >
                <tr>
                    <td class="FieldLabel">
                        Language Preference
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="radio" name="aBLanguagePrefType" id="aBLanguagePrefTypeEng" runat="server" value="0" onclick="onaBLanguagePrefTypeClick()"/>English
                        <input type="radio" name="aBLanguagePrefType" id="aBLanguagePrefTypeChi" runat="server" value="1" onclick="onaBLanguagePrefTypeClick()"/>Chinese
                        <input type="radio" name="aBLanguagePrefType" id="aBLanguagePrefTypeKor" runat="server" value="2" onclick="onaBLanguagePrefTypeClick()"/>Korean
                        <input type="radio" name="aBLanguagePrefType" id="aBLanguagePrefTypeSpa" runat="server" value="3" onclick="onaBLanguagePrefTypeClick()"/>Spanish
                        <input type="radio" name="aBLanguagePrefType" id="aBLanguagePrefTypeTag" runat="server" value="4" onclick="onaBLanguagePrefTypeClick()"/>Tagalog
                        <input type="radio" name="aBLanguagePrefType" id="aBLanguagePrefTypeVie" runat="server" value="5" onclick="onaBLanguagePrefTypeClick()"/>Vietnamese
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="radio" name="aBLanguagePrefType" id="aBLanguagePrefTypeOth" runat="server" value="6" onclick="onaBLanguagePrefTypeClick()"/>Other: <asp:TextBox ID="aBLanguagePrefTypeOtherDesc" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="radio" name="aBLanguagePrefType" id="aBLanguagePrefTypeBla" runat="server" value="7" onclick="onaBLanguagePrefTypeClick()"/>Leave Blank
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="aBLanguageRefusal" /> Did not wish to respond
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top" class="borrInfoTableContainerTd">
            <table cellpadding="0" cellspacing="0" class="insetborder borrInfoTable">
                <tr>
                    <td>
                        <table id="Table5" cellspacing="0" cellpadding="0" class="borrowerInfoTable">
                            <tr>
                                <td class="FieldLabel CellWithNestedTable" colspan="4">
                                    <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="FormTableSubHeader">
                                                Co-borrower Information
                                                <asp:HiddenField runat="server" ID="aIsCVAElig" />
                                            </td>
                                            <td class="FormTableSubHeader" align="right">
                                                <input onclick="onCopyBorrowerClick();" type="button" value="Copy from borrower">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    First Name
                                </td>
                                <td class="FieldLabelNoPadding" nowrap colspan="3">
                                    <asp:TextBox ID="aCFirstNm" runat="server" MaxLength="21" Width="127px" onchange="refreshCalculation();"></asp:TextBox>&nbsp;Middle Name<asp:TextBox ID="aCMidNm" MaxLength="21" runat="server" Width="70px" onchange="refreshCalculation();"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Preferred Name
                                </td>
                                <td class="FieldLabelNoPadding" nowrap colspan="3">
                                    <asp:TextBox ID="aCPreferredNm" runat="server" MaxLength="21" Width="127px"></asp:TextBox>&nbsp;
                                    <asp:CheckBox ID="aCPreferredNmLckd" runat="server" onclick="refreshCalculation();" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Last Name
                                </td>
                                <td nowrap class="FieldLabelNoPadding" colspan="3">
                                    <asp:TextBox ID="aCLastNm" runat="server" MaxLength="21" Width="127px" onchange="refreshCalculation();"></asp:TextBox>&nbsp;Suffix
                                    <ml:ComboBox id="aCSuffix" runat="server" Width="30px" onchange="refreshCalculation();"></ml:ComboBox>
                                    <a href="#" onclick="onEditCoborrAliasesClick()">Edit Aliases/POA</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    SSN
                                </td>
                                <td class="FieldLabelNoPadding" colspan="3">
                                    <ml:SSNTextBox class="RefreshOnChange" ID="aCSsn" runat="server" Width="90" preset="ssn"></ml:SSNTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" title="(MM/DD/YYYY)">
                                    DOB
                                </td>
                                <td class="FieldLabelNoPadding" colspan="3">
                                    <ml:DateTextBox ID="aCDob" runat="server" preset="date" Width="75"></ml:DateTextBox>&nbsp;Age
                                    <asp:TextBox ID="aCAge" runat="server" Width="34px" MaxLength="3"></asp:TextBox>&nbsp;Yrs. School
                                    <asp:TextBox ID="aCSchoolYrs" runat="server" Width="34px" MaxLength="3"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Citizenship</td>
                                <td colspan="3">
                                    <asp:dropdownlist id="aProdCCitizenT" runat="server"></asp:dropdownlist>                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="2">
                                    <span class="see-declarations-item-j-k-container">
                                        see <a href="#" onclick="goToDeclarations();">declarations</a> items j & k
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" nowrap>
                                    Marital Status
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="aCMaritalStatT" runat="server" onchange="onaCMaritalStatTChange()">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="ShowOnlyIfCoBorrowerNotMarried">
                                <td class="FieldLabel" colspan="4">
                                    <span class="DomesticRelationLabel">
                                        Is in a domestic relationship (real property rights similar to spouse)?
                                    </span>
                                    <asp:RadioButtonList class="DomesticRelationshipRBList" ID="aCDomesticRelationshipTri" runat="server" onclick="onaCDomesticRelationshipTriClick()" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="HideIfaCDomesticRelationshipTriIsNo">
                                <td class="FieldLabel" colspan="2">
                                    Type of relationship
                                </td>
                            </tr>
                            <tr class="HideIfaCDomesticRelationshipTriIsNo">
                                <td colspan="4">
                                    <asp:DropDownList runat="server" ID="aCDomesticRelationshipType" onchange="onaCDomesticRelationshipTypeChange()"></asp:DropDownList>
                                    <span>
                                        If other, explain 
                                    </span>
                                    <asp:TextBox class="width-125" runat="server" ID="aCDomesticRelationshipTypeOtherDescription"></asp:TextBox>
                                </td>                                
                            </tr>
                            <tr class="HideIfaCDomesticRelationshipTriIsNo">
                                <td class="FieldLabel" colspan="4">
                                    State where relation was formed <ml:StateDropDownList runat="server" ID="aCDomesticRelationshipStateCode"></ml:StateDropDownList>
                                </td>
                            </tr>
                            <tr class="HideIfaCDomesticRelationshipTriIsNo">
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    No. of Deps
                                </td>
                                <td class="FieldLabelNoPadding" colspan="3">
                                    <asp:TextBox ID="aCDependNum" runat="server" MaxLength="3" Width="34px" title="(not listed by Borrower)"></asp:TextBox>&nbsp; Dependents' Ages&nbsp;<asp:TextBox ID="aCDependAges" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Home Phone
                                </td>
                                <td class="FieldLabelNoPadding" nowrap colspan="3">
                                    <ml:PhoneTextBox ID="aCHPhone" runat="server" Width="120" preset="phone"></ml:PhoneTextBox>&nbsp;Cellphone
                                    <ml:PhoneTextBox ID="aCCellphone" Width="120" runat="server" preset="phone"></ml:PhoneTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Work Phone
                                </td>
                                <td class="FieldLabel" colspan="3">
                                    <ml:PhoneTextBox ID="aCBusPhone" runat="server" Width="120" preset="phone"></ml:PhoneTextBox>&nbsp;Fax&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                    <ml:PhoneTextBox ID="aCFax" runat="server" Width="120" preset="phone"></ml:PhoneTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Email
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="aCEmail" runat="server"></asp:TextBox><asp:regularexpressionvalidator id=RegularExpressionValidator2 runat="server" ControlToValidate="aCEmail" Display="Dynamic" ErrorMessage=" Invalid Email"></asp:regularexpressionvalidator>&nbsp;<a onclick="window.open('mailto:' + document.getElementById('<%= AspxTools.ClientId(aCEmail) %>').value);" href="#" tabindex="-1">send email</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Type
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="aCTypeT" runat="server"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="FieldLabel" id="aCFthbAndHousingHistContainer" runat="server" colspan="3">
                                    <span >
                                        <asp:CheckBox ID="aCTotalScoreIsFthb" runat="server" onclick="FthbClick('C');" /> First Time Home Buyer?
                                    </span>
                                    <span class="FthbExplanation">
                                        see <a href="#" onclick="goToDeclarations();">declarations</a> item <span class="first-time-home-buyer-declarations-item-number"></span>
                                    </span>
                                    <span style="display:none;"  runat="server" id="aCHasHousingHistContainer">
                                    <asp:CheckBox ID="aCHasHousingHist" runat="server" /> Has Housing History?
                                    </span>
                                </td>
                            </tr>
                            <tr id="CoborrowerMembershipId" runat="server">
                                <td class="FieldLabel">
                                    Member ID
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="aCCoreSystemId" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="aCIsVeteranRow" runat="server">
                                <td class="FieldLabel">
                                    Veteran
                                </td>
                                <td colspan="2">
                                    <asp:RadioButtonList class="RefreshOnChange" ID="aCIsVeteran" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>                                
                            </tr>                            
                            <tr class="CoBorrowerVeteranMilitaryServiceRow">
                                <td class="FieldLabel indent">
                                    Active Duty
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="aCActiveDuty" runat="server" RepeatDirection="Horizontal" onchange="onCoBorrowerActiveDutyChange();"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="CoBorrowerVeteranMilitaryServiceRow">
                                <td colspan="3" class="extra-indent">
                                    Expiration of service / tour: <ml:DateTextBox ID="aCActiveDutyExpirationDate" runat="server" preset="date"></ml:DateTextBox>
                                </td>                                
                            </tr>
                            <tr class="CoBorrowerVeteranMilitaryServiceRow">
                                <td class="FieldLabel width-80 indent">
                                    Retired / Discharged / Separated
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="aCRetiredDischargedOrSeparated" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="CoBorrowerVeteranMilitaryServiceRow">
                                <td class="FieldLabel width-80 indent">
                                    Non-activated Reserve / National Guard Member
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="aCReserveNationalGuardNeverActivated" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Surviving Spouse of Veteran
                                </td>
                                <td>
                                    <asp:RadioButtonList class="RefreshOnChange" id="aCIsSurvivingSpouseOfVeteran" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="HideThis">
                                <td class="FieldLabel">
                                    Service Type
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList runat="server" ID="aCVServiceT"></asp:DropDownList>                                    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td id="aCVServiceTDeceasedNotice">
                                    Service type of deceased veteran
                                </td>
                            </tr>
                            <tr class="HideThis">
                                <td class="FieldLabel">
                                    VA Entitlement
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList runat="server" ID="aCVEnt"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="HideThis">
                                <td class="FieldLabel">
                                    VA Funding Fee Exempt
                                </td>
                                <td colspan="3">
                                    <asp:RadioButtonList runat="server" ID="aIsCVAFFEx" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="FormTableSubHeader" nowrap colspan="4" style="height: 21px">
                                    Present Address
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" colspan="4">
                                    <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                Street
                                            </td>
                                            <td class="FieldLabel">
                                                City
                                            </td>
                                            <td class="FieldLabel">
                                                ST
                                            </td>
                                            <td class="FieldLabel">
                                                Zip
                                            </td>
                                            <td class="FieldLabel">
                                                Own/Rent
                                            </td>
                                            <td class="FieldLabel">
                                                # Yrs
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="aCAddr" runat="server" MaxLength="36" onchange="syncMailingAddress();enableVorBtn(false);"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aCCity" runat="server" Width="92px" onchange="syncMailingAddress();enableVorBtn(false);"></asp:TextBox>
                                            </td>
                                            <td>
                                                <ml:StateDropDownList ID="aCState" runat="server" onchange="syncMailingAddress();enableVorBtn(false);"></ml:StateDropDownList>
                                            </td>
                                            <td>
                                                <ml:ZipcodeTextBox ID="aCZip" runat="server" Width="50" preset="zipcode" onchange="syncMailingAddress();enableVorBtn(false);"></ml:ZipcodeTextBox>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="aCAddrT" runat="server" Width="59px" onchange="enableVorBtn(false);">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aCAddrYrs" runat="server" MaxLength="3" Width="34px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                Mailing Address
                                            </td>
                                            <td colspan="5" class="FieldLabel">
                                                <asp:DropDownList ID="aCAddrMailSourceT" runat="server" onchange="syncMailingAddress();"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                <asp:TextBox ID="aCAddrMail" MaxLength="36" runat="server" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aCCityMail" runat="server" Width="92px" />
                                            </td>
                                            <td>
                                                <ml:StateDropDownList ID="aCStateMail" runat="server" />
                                            </td>
                                            <td>
                                                <ml:ZipcodeTextBox ID="aCZipMail" Width="50" runat="server" preset="zipcode" />
                                            </td>
                                            <td><input type="button" id="EditVORC" onclick="EditVOR(false);" value="Edit VOR" />
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                Former Addresses
                                            </td>
                                            <td colspan="5">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="aCPrev1Addr" runat="server" MaxLength="36" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aCPrev1City" runat="server" Width="92px" />
                                            </td>
                                            <td>
                                                <ml:StateDropDownList ID="aCPrev1State" runat="server" />
                                            </td>
                                            <td>
                                                <ml:ZipcodeTextBox ID="aCPrev1Zip" runat="server" Width="50" preset="zipcode" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="aCPrev1AddrT" runat="server" Width="59px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aCPrev1AddrYrs" runat="server" MaxLength="3" Width="34px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="aCPrev2Addr" runat="server" MaxLength="36" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aCPrev2City" runat="server" Width="92px" />
                                            </td>
                                            <td>
                                                <ml:StateDropDownList ID="aCPrev2State" runat="server" />
                                            </td>
                                            <td>
                                                <ml:ZipcodeTextBox ID="aCPrev2Zip" runat="server" Width="50" preset="zipcode" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="aCPrev2AddrT" runat="server" Width="59px" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aCPrev2AddrYrs" runat="server" MaxLength="3" Width="34px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                Address After Closing
                                            </td>
                                            <td align="left" colspan="5" class="FieldLabel">
                                                <asp:CheckBox ID="aCAddrPostSourceTLckd" runat="server" onclick="syncMailingAddress();" />
                                                <asp:DropDownList ID="aCAddrPostSourceT" runat="server" onchange="syncMailingAddress();"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabelNoPadding">
                                                <asp:TextBox ID="aCAddrPost" MaxLength="36" runat="server"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="aCCityPost" runat="server" Width="92px"></asp:TextBox>
                                            </td>
                                            <td>
                                                <ml:StateDropDownList ID="aCStatePost" runat="server"></ml:StateDropDownList>
                                            </td>
                                            <td>
                                                <ml:ZipcodeTextBox ID="aCZipPost" Width="50" runat="server" preset="zipcode"></ml:ZipcodeTextBox>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table class="languagePrefTable InsetBorder" >
                <tr>
                    <td class="FieldLabel">
                        Language Preference
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="radio" name="aCLanguagePrefType" id="aCLanguagePrefTypeEng" runat="server" value="0" onclick="onaCLanguagePrefTypeClick()"/>English
                        <input type="radio" name="aCLanguagePrefType" id="aCLanguagePrefTypeChi" runat="server" value="1" onclick="onaCLanguagePrefTypeClick()"/>Chinese
                        <input type="radio" name="aCLanguagePrefType" id="aCLanguagePrefTypeKor" runat="server" value="2" onclick="onaCLanguagePrefTypeClick()"/>Korean
                        <input type="radio" name="aCLanguagePrefType" id="aCLanguagePrefTypeSpa" runat="server" value="3" onclick="onaCLanguagePrefTypeClick()"/>Spanish
                        <input type="radio" name="aCLanguagePrefType" id="aCLanguagePrefTypeTag" runat="server" value="4" onclick="onaCLanguagePrefTypeClick()"/>Tagalog
                        <input type="radio" name="aCLanguagePrefType" id="aCLanguagePrefTypeVie" runat="server" value="5" onclick="onaCLanguagePrefTypeClick()"/>Vietnamese
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="radio" name="aCLanguagePrefType" id="aCLanguagePrefTypeOth" runat="server" value="6" onclick="onaCLanguagePrefTypeClick()"/>Other: <asp:TextBox ID="aCLanguagePrefTypeOtherDesc" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="radio" name="aCLanguagePrefType" id="aCLanguagePrefTypeBla" runat="server" value="7" onclick="onaCLanguagePrefTypeClick()"/>Leave Blank
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="aCLanguageRefusal" /> Did not wish to respond
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">
    jQuery(function ($j) {
        onaBLanguagePrefTypeClick();
        onaCLanguagePrefTypeClick();        

        onaBMaritalStatTChange();
        onaCMaritalStatTChange();
        
        onBorrowerActiveDutyChange();
        onCoBorrowerActiveDutyChange();

        $j('.first-time-home-buyer-declarations-item-number').text(ML.IsTargeting2019Ulad ? 'a' : 'm');
        $j('.see-declarations-item-j-k-container').toggle(!ML.IsTargeting2019Ulad);

        $j(".RefreshOnChange").change(function() {
            var opm204251VaElig = <%= AspxTools.JsGetElementById(IsBeyondV1_CalcVaLoanElig) %>.value;
            if(opm204251VaElig === 'False')
            {
                return;
            }
            else
            {
                refreshCalculation();
            }
        });
    });

if (!('trim' in String.prototype)) {
    String.prototype.trim = function() {
        return this.replace(/^\s+/, '').replace(/\s+$/, '');
    };
}

function onBorrowerActiveDutyChange() {
    var isaBActiveDuty = <%=AspxTools.JQuery(aBActiveDuty)%>.find(":checked").val() === "True";

    <%=AspxTools.JQuery(aBActiveDutyExpirationDate)%>.prop('disabled', !isaBActiveDuty);

    if (!isaBActiveDuty) {
        <%=AspxTools.JQuery(aBActiveDutyExpirationDate)%>.val("");
    }
}

function onCoBorrowerActiveDutyChange() {
    var isaCActiveDuty = <%=AspxTools.JQuery(aCActiveDuty)%>.find(":checked").val() === "True";

    <%=AspxTools.JQuery(aCActiveDutyExpirationDate)%>.prop('disabled', !isaCActiveDuty);

    if (!isaCActiveDuty) {
        <%=AspxTools.JQuery(aCActiveDutyExpirationDate)%>.val("");
    }
}

function clearUponaBDomesticRelationshipTriChange() {
    <%=AspxTools.JQuery(aBDomesticRelationshipType)%>.val("0");
    <%=AspxTools.JQuery(aBDomesticRelationshipTypeOtherDescription)%>.val("");
    <%=AspxTools.JQuery(aBDomesticRelationshipStateCode)%>.val("");
}

function clearUponaCDomesticRelationshipTriChange() {
    <%=AspxTools.JQuery(aCDomesticRelationshipType)%>.val("0");
    <%=AspxTools.JQuery(aCDomesticRelationshipTypeOtherDescription)%>.val("");
    <%=AspxTools.JQuery(aCDomesticRelationshipStateCode)%>.val("");
}

function onaBMaritalStatTChange() {
    var isUnmarried = <%=AspxTools.JQuery(aBMaritalStatT)%>.val() === "1";
    $(".ShowOnlyIfBorrowerNotMarried").toggle(isUnmarried);
    if (isUnmarried) {
        onaBDomesticRelationshipTriClick();
    }
    else {
        $(".HideIfaBDomesticRelationshipTriIsNo").hide();
        $("#" + <%=AspxTools.JsGetClientIdString(aBDomesticRelationshipTri)%> + "_0").prop("checked", false);
        $("#" + <%=AspxTools.JsGetClientIdString(aBDomesticRelationshipTri)%> + "_1").prop("checked", false);
        clearUponaBDomesticRelationshipTriChange();
    }
}

function onaCMaritalStatTChange() {
    var isUnmarried = <%=AspxTools.JQuery(aCMaritalStatT)%>.val() === "1";
    $(".ShowOnlyIfCoBorrowerNotMarried").toggle(isUnmarried);
    if (isUnmarried) {
        onaCDomesticRelationshipTriClick();
    }
    else {
        $(".HideIfaCDomesticRelationshipTriIsNo").hide();
        $("#" + <%=AspxTools.JsGetClientIdString(aCDomesticRelationshipTri)%> + "_0").prop("checked", false);
        $("#" + <%=AspxTools.JsGetClientIdString(aCDomesticRelationshipTri)%> + "_1").prop("checked", false);
        clearUponaCDomesticRelationshipTriChange();
    }
}

function onaBDomesticRelationshipTriClick() {
    var isYes = $("#" + <%=AspxTools.JsGetClientIdString(aBDomesticRelationshipTri)%> + "_0").prop("checked");
    $(".HideIfaBDomesticRelationshipTriIsNo").toggle(isYes);

    if (!isYes) {
        clearUponaBDomesticRelationshipTriChange();
    }

    onaBDomesticRelationshipTypeChange();
 }

function onaCDomesticRelationshipTriClick() {
    var isYes = $("#" + <%=AspxTools.JsGetClientIdString(aCDomesticRelationshipTri)%> + "_0").prop("checked");
    $(".HideIfaCDomesticRelationshipTriIsNo").toggle(isYes);

    if (!isYes) {
        clearUponaCDomesticRelationshipTriChange();
    }

    onaCDomesticRelationshipTypeChange();
}

function onaBLanguagePrefTypeClick() {
    var isOther = <%=AspxTools.JQuery(aBLanguagePrefTypeOth)%>.prop("checked");
    <%=AspxTools.JQuery(aBLanguagePrefTypeOtherDesc)%>.prop("readonly", !isOther);

    if (!isOther) {
        <%=AspxTools.JQuery(aBLanguagePrefTypeOtherDesc)%>.val("");
    }
}

function onaCLanguagePrefTypeClick() {
    var isOther = <%=AspxTools.JQuery(aCLanguagePrefTypeOth)%>.prop("checked");
    <%=AspxTools.JQuery(aCLanguagePrefTypeOtherDesc)%>.prop("readonly", !<%=AspxTools.JQuery(aCLanguagePrefTypeOth)%>.prop("checked"));

    if (!isOther) {
        <%=AspxTools.JQuery(aCLanguagePrefTypeOtherDesc)%>.val("");
    }
}

function onaBDomesticRelationshipTypeChange() {
    var isOther = <%=AspxTools.JQuery(aBDomesticRelationshipType)%>.val() === "4";
    <%=AspxTools.JQuery(aBDomesticRelationshipTypeOtherDescription)%>.prop("readonly", !isOther);

    if (!isOther) {
        <%=AspxTools.JQuery(aBDomesticRelationshipTypeOtherDescription)%>.val("");
    }
}

function onaCDomesticRelationshipTypeChange() {
    var isOther = <%=AspxTools.JQuery(aCDomesticRelationshipType)%>.val() === "4";
    <%=AspxTools.JQuery(aCDomesticRelationshipTypeOtherDescription)%>.prop("readonly", !isOther);

    if (!isOther) {
        <%=AspxTools.JQuery(aCDomesticRelationshipTypeOtherDescription)%>.val("");
    }
}

function _init() 
{
    var opm204251VaElig = <%= AspxTools.JsGetElementById(IsBeyondV1_CalcVaLoanElig) %>.value;

    lockField(<%= AspxTools.JsGetElementById(aBPreferredNmLckd) %>, 'aBPreferredNm');
    lockField(<%= AspxTools.JsGetElementById(aCPreferredNmLckd) %>, 'aCPreferredNm');

    addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(RegularExpressionValidator1) %>);
    addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(RegularExpressionValidator2) %>)

    if(opm204251VaElig === 'False')
    {
        $j(".HideThis").hide();
        return;
    }
    else
    {
        $j(".HideThis").show();
    }

    var aIsBVAElig = <%= AspxTools.JsGetElementById(aIsBVAElig) %>.value;
    if(aIsBVAElig === 'True')
    {
        $j("[name$='aIsBVAFFEx']").prop('disabled', false);
        $j("[id$='aBVServiceT']").prop('disabled', false);
        $j("[id$='aBVEnt']").prop('disabled', false);
    }
    else
    {
        $j("[name$='aIsBVAFFEx']").prop('disabled', true);
        $j("[id$='aBVServiceT']").prop('disabled', true);
        $j("[id$='aBVEnt']").prop('disabled', true);
    }

    var aIsCVAElig = <%= AspxTools.JsGetElementById(aIsCVAElig) %>.value;
    if(aIsCVAElig === 'True')
    {
        $j("[name$='aIsCVAFFEx']").prop('disabled', false);
        $j("[id$='aCVServiceT']").prop('disabled', false);
        $j("[id$='aCVEnt']").prop('disabled', false);
    }
    else
    {
        $j("[name$='aIsCVAFFEx']").prop('disabled', true);
        $j("[id$='aCVServiceT']").prop('disabled', true);
        $j("[id$='aCVEnt']").prop('disabled', true);
    }    

    var isaBVeteran = <%=AspxTools.JQuery(aBIsVeteran)%>.find(":checked").val() === "True";
    var isaBIsSurvivingSpouseOfVeteran = <%=AspxTools.JQuery(aBIsSurvivingSpouseOfVeteran)%>.find(":checked").val() === "True";

    var isaCVeteran = <%=AspxTools.JQuery(aCIsVeteran)%>.find(":checked").val() === "True";
    var isaCIsSurvivingSpouseOfVeteran = <%=AspxTools.JQuery(aCIsSurvivingSpouseOfVeteran)%>.find(":checked").val() === "True";

    $("#aBVServiceTDeceasedNotice").toggle(!isaBVeteran && isaBIsSurvivingSpouseOfVeteran);
    $("#aCVServiceTDeceasedNotice").toggle(!isaCVeteran && isaCIsSurvivingSpouseOfVeteran);

    toggleHasCoborrowerCheckboxes(<%= AspxTools.JQuery(aHasCoborrowerCalc) %>.prop('checked'));
}

    function _postRefreshCalculation(result)
    {
        var hasCoborrowerByCalc = result.aHasCoborrowerCalc === 'True';
        toggleHasCoborrowerCheckboxes(hasCoborrowerByCalc);
    }

    function toggleHasCoborrowerCheckboxes(hasCoborrowerByCalc)
    {
        var $hasCoborrowerDataCb = <%= AspxTools.JQuery(aHasCoborrowerData) %>;
        var $hasCoborrowerCalcCb = <%= AspxTools.JQuery(aHasCoborrowerCalc) %>;

        $hasCoborrowerDataCb.toggle(!hasCoborrowerByCalc);
        $hasCoborrowerCalcCb.toggle(hasCoborrowerByCalc);
    }

function FthbClick(type)
{
    var fthb;
    var hastHist;
    if(type == 'B')
    {
        fthb = <%= AspxTools.JsGetElementById(aBTotalScoreIsFthb)%>;
        hasHist = <%= AspxTools.JsGetElementById(aBHasHousingHistContainer)%>;
    }
    else
    {
        fthb = <%= AspxTools.JsGetElementById(aCTotalScoreIsFthb)%>;
        hasHist = <%= AspxTools.JsGetElementById(aCHasHousingHistContainer)%>;
    }
    
    if(fthb.checked)
        hasHist.style.display = "inline-block";
    else
        hasHist.style.display = "none";
}

function onEditBorrAliasesClick()
{
    editAliasesPOA('B');
}

function onEditCoborrAliasesClick()
{
    editAliasesPOA('C');
}

function editAliasesPOA(borrMode)
{
    var aliasPOAData = {
        alias : $j('input[id$=a' + borrMode + 'Alias]').val(),
        powerOfAttorney : $j('input[id$=a' + borrMode + 'PowerOfAttorneyNm]').val()
    };
    var args = {
        data : JSON.stringify(aliasPOAData)
    };
    
    var path = '/newlos/Borrower/EditAliasesPOA.aspx';
    var url = path
        + '?loanid=' + <%= AspxTools.JsString(LoanID) %>
        + '&borrmode=' + borrMode;
    showModal(url, args, null, null, function(modalResult){ 
        if (modalResult.OK) {
            var data = JSON.parse(modalResult.data);
            $j('input[id$=a' + borrMode + 'Alias]').val(data.alias);
            $j('input[id$=a' + borrMode + 'PowerOfAttorneyNm]').val(data.powerOfAttorney);
            // Mark page as dirty
            updateDirtyBit();
        }
    },{ hideCloseButton: true });
}

function doAfterDateFormat(o)
{
	if(o.id == <%= AspxTools.JsGetClientIdString(aBDob) %>)
	{
	    calculateAge(o.id, <%= AspxTools.JsGetClientIdString(aBAge) %>);
	}
	else if (o.id == <%= AspxTools.JsGetClientIdString(aCDob) %>)
	{
		calculateAge(o.id, <%= AspxTools.JsGetClientIdString(aCAge) %>);
	}
	
}

function calculateAge(aDobId, aAgeId)
{
    var args = {
        _ClientID : ML._ClientID,
        dob : document.getElementById(aDobId).value,
        age : document.getElementById(aAgeId).value
    };


    var result = gService.loanedit.call(ML._ClientID + "_CalculateAge", args);
    if (!result.error) 
    {
        if (result.value["age"] != null && result.value["age"] != "") 
        {
            document.getElementById(aAgeId).value = result.value["age"];
        }
    }
}

function updateBorrowerName() {
    var name = <%= AspxTools.JsGetElementById(aBLastNm) %>.value + ", " + <%= AspxTools.JsGetElementById(aBFirstNm) %>.value;
    parent.info.f_updateApplicantDDL(name, ML.aAppId);

    //Borrower preferred name
    var borrPreferredNmSet = <%= AspxTools.JsGetElementById(aBPreferredNmLckd) %>.checked;
    if (!borrPreferredNmSet)
    {
        <%= AspxTools.JsGetElementById(aBPreferredNm) %>.value = <%= AspxTools.JsGetElementById(aBFirstNm) %>.value;
    }
}

function onCopyBorrowerClick() {
    if('' == <%= AspxTools.JsGetElementById(aCLastNm) %>.value)
        <%= AspxTools.JsGetElementById(aCLastNm) %>.value = <%= AspxTools.JsGetElementById(aBLastNm) %>.value;

    <%= AspxTools.JsGetElementById(aCHPhone) %>.value = <%= AspxTools.JsGetElementById(aBHPhone) %>.value;
    <%= AspxTools.JsGetElementById(aCMaritalStatT) %>.value = <%= AspxTools.JsGetElementById(aBMaritalStatT) %>.value;
    <%= AspxTools.JsGetElementById(aCAddr) %>.value = <%= AspxTools.JsGetElementById(aBAddr) %>.value;
    <%= AspxTools.JsGetElementById(aCCity) %>.value = <%= AspxTools.JsGetElementById(aBCity) %>.value;
    <%= AspxTools.JsGetElementById(aCState) %>.value = <%= AspxTools.JsGetElementById(aBState) %>.value;
    <%= AspxTools.JsGetElementById(aCZip) %>.value = <%= AspxTools.JsGetElementById(aBZip) %>.value;
    <%= AspxTools.JsGetElementById(aCAddrT) %>.value = <%= AspxTools.JsGetElementById(aBAddrT) %>.value;
    <%= AspxTools.JsGetElementById(aCAddrYrs) %>.value = <%= AspxTools.JsGetElementById(aBAddrYrs) %>.value;
    <%= AspxTools.JsGetElementById(aCAddrMailSourceT) %>.value = <%= AspxTools.JsGetElementById(aBAddrMailSourceT) %>.value;
    <%= AspxTools.JsGetElementById(aCAddrMail) %>.value = <%= AspxTools.JsGetElementById(aBAddrMail) %>.value;
    <%= AspxTools.JsGetElementById(aCCityMail) %>.value = <%= AspxTools.JsGetElementById(aBCityMail) %>.value;
    <%= AspxTools.JsGetElementById(aCStateMail) %>.value = <%= AspxTools.JsGetElementById(aBStateMail) %>.value;
    <%= AspxTools.JsGetElementById(aCZipMail) %>.value = <%= AspxTools.JsGetElementById(aBZipMail) %>.value;

    <%= AspxTools.JsGetElementById(aCPrev1Addr) %>.value = <%= AspxTools.JsGetElementById(aBPrev1Addr) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1City) %>.value = <%= AspxTools.JsGetElementById(aBPrev1City) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1State) %>.value = <%= AspxTools.JsGetElementById(aBPrev1State) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1Zip) %>.value = <%= AspxTools.JsGetElementById(aBPrev1Zip) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1AddrT) %>.value = <%= AspxTools.JsGetElementById(aBPrev1AddrT) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev1AddrYrs) %>.value = <%= AspxTools.JsGetElementById(aBPrev1AddrYrs) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2Addr) %>.value = <%= AspxTools.JsGetElementById(aBPrev2Addr) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2City) %>.value = <%= AspxTools.JsGetElementById(aBPrev2City) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2State) %>.value = <%= AspxTools.JsGetElementById(aBPrev2State) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2Zip) %>.value = <%= AspxTools.JsGetElementById(aBPrev2Zip) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2AddrT) %>.value = <%= AspxTools.JsGetElementById(aBPrev2AddrT) %>.value;
    <%= AspxTools.JsGetElementById(aCPrev2AddrYrs) %>.value = <%= AspxTools.JsGetElementById(aBPrev2AddrYrs) %>.value;
    
    <%= AspxTools.JsGetElementById(aCAddrPostSourceTLckd) %>.checked = <%= AspxTools.JsGetElementById(aBAddrPostSourceTLckd) %>.checked;
    <%= AspxTools.JsGetElementById(aCAddrPostSourceT) %>.value = <%= AspxTools.JsGetElementById(aBAddrPostSourceT) %>.value;
    <%= AspxTools.JsGetElementById(aCAddrPost) %>.value = <%= AspxTools.JsGetElementById(aBAddrPost) %>.value;
    <%= AspxTools.JsGetElementById(aCCityPost) %>.value = <%= AspxTools.JsGetElementById(aBCityPost) %>.value;
    <%= AspxTools.JsGetElementById(aCStatePost) %>.value = <%= AspxTools.JsGetElementById(aBStatePost) %>.value;
    <%= AspxTools.JsGetElementById(aCZipPost) %>.value = <%= AspxTools.JsGetElementById(aBZipPost) %>.value;

    var isYes = $("#" + <%=AspxTools.JsGetClientIdString(aBDomesticRelationshipTri)%> + "_0").prop("checked");
    $("#" + <%=AspxTools.JsGetClientIdString(aCDomesticRelationshipTri)%> + "_0").prop("checked", isYes);
    var isNo = $("#" + <%=AspxTools.JsGetClientIdString(aBDomesticRelationshipTri)%> + "_1").prop("checked");
    $("#" + <%=AspxTools.JsGetClientIdString(aCDomesticRelationshipTri)%> + "_1").prop("checked", isNo);

    <%= AspxTools.JsGetElementById(aCDomesticRelationshipType)%>.value = <%= AspxTools.JsGetElementById(aBDomesticRelationshipType)%>.value;
    <%= AspxTools.JsGetElementById(aCDomesticRelationshipTypeOtherDescription)%>.value = <%= AspxTools.JsGetElementById(aBDomesticRelationshipTypeOtherDescription)%>.value;
    <%= AspxTools.JsGetElementById(aCDomesticRelationshipStateCode)%>.value = <%= AspxTools.JsGetElementById(aBDomesticRelationshipStateCode)%>.value;
    onaCMaritalStatTChange();

    updateMailingAddressFields();
    updateDirtyBit();
    refreshCalculation();
}	

function fillPresentAddress() {
    <%= AspxTools.JsGetElementById(aBAddr) %>.value = <%= AspxTools.JsString(sSpAddr) %>;
    <%= AspxTools.JsGetElementById(aBCity) %>.value = <%= AspxTools.JsString(sSpCity) %>;
    <%= AspxTools.JsGetElementById(aBZip) %>.value = <%= AspxTools.JsString(sSpZip) %>;
    <%= AspxTools.JsGetElementById(aBState) %>.value = <%= AspxTools.JsString(sSpState) %>;
    updateDirtyBit();
}
function syncMailingAddress() {
    var source = $j(<%= AspxTools.JsGetElementById(aBAddrMailSourceT) %>).val();
    switch(source)
    {
        case "0":   // PresentAddress
            <%= AspxTools.JsGetElementById(aBAddrMail) %>.value = <%= AspxTools.JsGetElementById(aBAddr) %>.value;
            <%= AspxTools.JsGetElementById(aBCityMail) %>.value = <%= AspxTools.JsGetElementById(aBCity) %>.value;
            <%= AspxTools.JsGetElementById(aBStateMail) %>.value = <%= AspxTools.JsGetElementById(aBState) %>.value;
            <%= AspxTools.JsGetElementById(aBZipMail) %>.value = <%= AspxTools.JsGetElementById(aBZip) %>.value;
            break;
        case "1":   // SubjectPropertyAddress
            <%= AspxTools.JsGetElementById(aBAddrMail) %>.value = <%= AspxTools.JsString(sSpAddr) %>;
            <%= AspxTools.JsGetElementById(aBCityMail) %>.value = <%= AspxTools.JsString(sSpCity) %>;
            <%= AspxTools.JsGetElementById(aBStateMail) %>.value = <%= AspxTools.JsString(sSpState) %>;
            <%= AspxTools.JsGetElementById(aBZipMail) %>.value = <%= AspxTools.JsString(sSpZip) %>;
            break;
    }

    source = $j(<%= AspxTools.JsGetElementById(aCAddrMailSourceT) %>).val();
    switch(source)
    {
        case "0":   // PresentAddress
            <%= AspxTools.JsGetElementById(aCAddrMail) %>.value = <%= AspxTools.JsGetElementById(aCAddr) %>.value;
            <%= AspxTools.JsGetElementById(aCCityMail) %>.value = <%= AspxTools.JsGetElementById(aCCity) %>.value;
            <%= AspxTools.JsGetElementById(aCStateMail) %>.value = <%= AspxTools.JsGetElementById(aCState) %>.value;
            <%= AspxTools.JsGetElementById(aCZipMail) %>.value = <%= AspxTools.JsGetElementById(aCZip) %>.value;
            break;
        case "1":   // SubjectPropertyAddress
            <%= AspxTools.JsGetElementById(aCAddrMail) %>.value = <%= AspxTools.JsString(sSpAddr) %>;
            <%= AspxTools.JsGetElementById(aCCityMail) %>.value = <%= AspxTools.JsString(sSpCity) %>;
            <%= AspxTools.JsGetElementById(aCStateMail) %>.value = <%= AspxTools.JsString(sSpState) %>;
            <%= AspxTools.JsGetElementById(aCZipMail) %>.value = <%= AspxTools.JsString(sSpZip) %>;
            break;
    }
    
    // OPM 59554 - Post-Closing Address Fields
    var $aBAddrPostSourceT = $j(<%= AspxTools.JsGetElementById(aBAddrPostSourceT) %>);
    var bLocked = <%= AspxTools.JsGetElementById(aBAddrPostSourceTLckd) %>.checked;
    
    if (!bLocked)
    {
        // If here aBAddrPostSourceTLckd is unlocked
        if (<%= AspxTools.JsBool(isPurchase) %> && <%= AspxTools.JsBool(aBDecOcc) %>)
        {
            $aBAddrPostSourceT.val("2");
        }
        else
        {
            $aBAddrPostSourceT.val("1");
        }
    }

    source = $aBAddrPostSourceT.val();
    switch(source)
    {
        case "0":   // PresentAddress
            <%= AspxTools.JsGetElementById(aBAddrPost) %>.value = <%= AspxTools.JsGetElementById(aBAddr) %>.value;
            <%= AspxTools.JsGetElementById(aBCityPost) %>.value = <%= AspxTools.JsGetElementById(aBCity) %>.value;
            <%= AspxTools.JsGetElementById(aBStatePost) %>.value = <%= AspxTools.JsGetElementById(aBState) %>.value;
            <%= AspxTools.JsGetElementById(aBZipPost) %>.value = <%= AspxTools.JsGetElementById(aBZip) %>.value;
            break;
        case "1":   // MailingAddress
            <%= AspxTools.JsGetElementById(aBAddrPost) %>.value = <%= AspxTools.JsGetElementById(aBAddrMail) %>.value;
            <%= AspxTools.JsGetElementById(aBCityPost) %>.value = <%= AspxTools.JsGetElementById(aBCityMail) %>.value;
            <%= AspxTools.JsGetElementById(aBStatePost) %>.value = <%= AspxTools.JsGetElementById(aBStateMail) %>.value;
            <%= AspxTools.JsGetElementById(aBZipPost) %>.value = <%= AspxTools.JsGetElementById(aBZipMail) %>.value;
            break;
        case "2":   // SubjectPropertyAddress
            <%= AspxTools.JsGetElementById(aBAddrPost) %>.value = <%= AspxTools.JsString(sSpAddr) %>;
            <%= AspxTools.JsGetElementById(aBCityPost) %>.value = <%= AspxTools.JsString(sSpCity) %>;
            <%= AspxTools.JsGetElementById(aBStatePost) %>.value = <%= AspxTools.JsString(sSpState) %>;
            <%= AspxTools.JsGetElementById(aBZipPost) %>.value = <%= AspxTools.JsString(sSpZip) %>;
            break;
    }
    
    var $aCAddrPostSourceT = $j(<%= AspxTools.JsGetElementById(aCAddrPostSourceT) %>);
    bLocked = <%= AspxTools.JsGetElementById(aCAddrPostSourceTLckd) %>.checked;
    
    if (!bLocked)
    {
        // If here aCAddrPostSourceTLckd is unlocked
        if (<%= AspxTools.JsBool(isPurchase) %> && <%= AspxTools.JsBool(aCDecOcc) %>)
        {
            $aCAddrPostSourceT.val("2");
        }
        else
        {
            $aCAddrPostSourceT.val("1");
        }
    }

    source = $aCAddrPostSourceT.val();
    switch(source)
    {
        case "0":   // PresentAddress
            <%= AspxTools.JsGetElementById(aCAddrPost) %>.value = <%= AspxTools.JsGetElementById(aCAddr) %>.value;
            <%= AspxTools.JsGetElementById(aCCityPost) %>.value = <%= AspxTools.JsGetElementById(aCCity) %>.value;
            <%= AspxTools.JsGetElementById(aCStatePost) %>.value = <%= AspxTools.JsGetElementById(aCState) %>.value;
            <%= AspxTools.JsGetElementById(aCZipPost) %>.value = <%= AspxTools.JsGetElementById(aCZip) %>.value;
            break;
        case "1":   // MailingAddress
            <%= AspxTools.JsGetElementById(aCAddrPost) %>.value = <%= AspxTools.JsGetElementById(aCAddrMail) %>.value;
            <%= AspxTools.JsGetElementById(aCCityPost) %>.value = <%= AspxTools.JsGetElementById(aCCityMail) %>.value;
            <%= AspxTools.JsGetElementById(aCStatePost) %>.value = <%= AspxTools.JsGetElementById(aCStateMail) %>.value;
            <%= AspxTools.JsGetElementById(aCZipPost) %>.value = <%= AspxTools.JsGetElementById(aCZipMail) %>.value;
            break;
        case "2":   // SubjectPropertyAddress
            <%= AspxTools.JsGetElementById(aCAddrPost) %>.value = <%= AspxTools.JsString(sSpAddr) %>;
            <%= AspxTools.JsGetElementById(aCCityPost) %>.value = <%= AspxTools.JsString(sSpCity) %>;
            <%= AspxTools.JsGetElementById(aCStatePost) %>.value = <%= AspxTools.JsString(sSpState) %>;
            <%= AspxTools.JsGetElementById(aCZipPost) %>.value = <%= AspxTools.JsString(sSpZip) %>;
            break;
    }

    updateMailingAddressFields();
}

function updateMailingAddressFields() {

    var b = $j(<%= AspxTools.JsGetElementById(aBAddrMailSourceT) %>).val() != "2";
    <%= AspxTools.JsGetElementById(aBAddrMail) %>.readOnly = b;
    <%= AspxTools.JsGetElementById(aBCityMail) %>.readOnly = b;
    disableDDL(<%= AspxTools.JsGetElementById(aBStateMail) %>, b);
    <%= AspxTools.JsGetElementById(aBZipMail) %>.readOnly = b;

    b = $j(<%= AspxTools.JsGetElementById(aCAddrMailSourceT) %>).val() != "2";
    <%= AspxTools.JsGetElementById(aCAddrMail) %>.readOnly = b;
    <%= AspxTools.JsGetElementById(aCCityMail) %>.readOnly = b;
    disableDDL(<%= AspxTools.JsGetElementById(aCStateMail) %>, b);
    <%= AspxTools.JsGetElementById(aCZipMail) %>.readOnly = b;
    
    b = $j(<%= AspxTools.JsGetElementById(aBAddrPostSourceT) %>).val() != "3";
    <%= AspxTools.JsGetElementById(aBAddrPost) %>.readOnly = b;
    <%= AspxTools.JsGetElementById(aBCityPost) %>.readOnly = b;
    disableDDL(<%= AspxTools.JsGetElementById(aBStatePost) %>, b);
    <%= AspxTools.JsGetElementById(aBZipPost) %>.readOnly = b;
    
    b = !<%= AspxTools.JsGetElementById(aBAddrPostSourceTLckd) %>.checked;
    disableDDL(<%= AspxTools.JsGetElementById(aBAddrPostSourceT) %>, b);
    
    b = $j(<%= AspxTools.JsGetElementById(aCAddrPostSourceT) %>).val() != "3";
    <%= AspxTools.JsGetElementById(aCAddrPost) %>.readOnly = b;
    <%= AspxTools.JsGetElementById(aCCityPost) %>.readOnly = b;
    disableDDL(<%= AspxTools.JsGetElementById(aCStatePost) %>, b);
    <%= AspxTools.JsGetElementById(aCZipPost) %>.readOnly = b;
    
     b = !<%= AspxTools.JsGetElementById(aCAddrPostSourceTLckd) %>.checked;
    disableDDL(<%= AspxTools.JsGetElementById(aCAddrPostSourceT) %>, b);
}
function enableVorBtn(isborrower) {
    var readonly = document.getElementById('_ReadOnly'); 
    if( readonly.value === 'True') {
        return;
    }
    var fields = null;
    if( isborrower ) {
        fields = [
            <%= AspxTools.JsGetElementById(aBAddr) %>.value,
            <%= AspxTools.JsGetElementById(aBCity) %>.value,
            <%= AspxTools.JsGetElementById(aBState) %>.value,
            <%= AspxTools.JsGetElementById(aBZip) %>.value
        ]; 
    } else {
        fields = [
            <%= AspxTools.JsGetElementById(aCAddr) %>.value,
            <%= AspxTools.JsGetElementById(aCCity) %>.value,
            <%= AspxTools.JsGetElementById(aCState) %>.value,
            <%= AspxTools.JsGetElementById(aCZip) %>.value
        ]; 
    }
    var enable = false;
    var rent = isborrower ?  <%= AspxTools.JsGetElementById(aBAddrT) %> :   <%= AspxTools.JsGetElementById(aCAddrT) %>; 
    var button = isborrower ? document.getElementById('EditVORB') : document.getElementById('EditVORC');
     
    for( var i = 0; i < fields.length; i++) {
        if( fields[i].trim().length > 0 ) {
            enable = true;
            break;
        }
    }
    
    if( rent.value !== "1" ) {
        enable = false;
    }
    
    if( enable ) {
        button.removeAttribute('disabled');
    }
    else {
        button.disabled = 'disabled';
    }
}
function EditVOR(isborrower) {
    var readonly = document.getElementById('_ReadOnly'); 
    if( readonly.value === 'True') {
        return;
    }
    if (isDirty()) {
        var ret = confirm('Save is required before proceeding'); 
        if (ret ) saveMe();
        else { return; }
    }
  
    var recordid = isborrower ? '11111111-1111-1111-1111-111111111111' : '44444444-4444-4444-4444-444444444444'; 
  
    linkMe('Verifications/VORRecord.aspx', 'recordid=' + recordid );
}

function goToDeclarations() {
    if (parent && parent.treeview && 
            parent.treeview.load && typeof parent.treeview.load === 'function') {

        if (ML.IsTargeting2019Ulad) {
            parent.treeview.load('Ulad/Declarations.aspx', false/*use lead prefix for lead editor, redirect whole window for loan editor*/);
        }
        else if (<%=AspxTools.JsBool(use1003LinkForDeclarations)%>) {
            parent.treeview.load('Forms/Loan1003.aspx?pg=2', true);
        }
        else {
            parent.treeview.load('Declarations.aspx');
        }
    }
}

updateMailingAddressFields();


    addEventHandler(<%= AspxTools.JsGetElementById(aBState) %>, "change", syncMailingAddress, false);
    addEventHandler(<%= AspxTools.JsGetElementById(aCState) %>, "change", syncMailingAddress, false);
enableVorBtn(true);
enableVorBtn(false);

var Page_Validators = new Array(document.getElementById("RegularExpressionValidator1"), document.getElementById("RegularExpressionValidator2"));
</script>

