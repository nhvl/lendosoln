﻿#region Generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// A Service used for asynchronously creating a loan file.
    /// </summary>
    public partial class LoanCreateService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets a value indicating whether a the loan is a test loan.
        /// </summary>
        private bool IsTest
        {
            get { return GetBool("isTest", false); }
        }

        /// <summary>
        /// Gets a value indicating whether a the loan is a template.
        /// </summary>
        private bool IsTemplate
        {
            get { return GetBool("isTemplate", false); }
        }

        /// <summary>
        /// Gets a value indicating whether a the loan is a lead.
        /// </summary>
        private bool IsLead
        {
            get { return GetBool("isLead", false); }
        }

        /// <summary>
        /// Gets the loan's loan type.  
        /// </summary>
        private string Purpose
        {
            get { return GetString("purpose", string.Empty); }
        }

        /// <summary>
        /// Gets the id of the template the loan is created from.
        /// </summary>
        private Guid TemplateId
        {
            get { return GetGuid("templateId", Guid.Empty); }
        }

        /// <summary>
        /// This method takes in a method's name and calls the corresponding method.
        /// </summary>
        /// <param name="methodName">The name of the method to be called.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CreateLoan":
                    this.CreateLoan();
                    break;
                default:
                    throw new CBaseException("Failed to create the loan.  Please try again.", "The method '" + methodName + "' is not handled in LoanCreateService.aspx.cs");
            }
        }

        /// <summary>
        /// Creates a loan template.
        /// </summary>
        private void CreateTemplate()
        {
            Guid loanId = Guid.Empty;

            try
            {
                CLoanFileCreator creator = CLoanFileCreator.GetCreator(PrincipalFactory.CurrentPrincipal, E_LoanCreationSource.UserCreateFromBlank);
                loanId = creator.CreateBlankLoanTemplate();
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                this.SetResult("Error", true);
            }

            this.SetResult("loanId", loanId);
        }

        /// <summary>
        /// Creates a blank loan file.
        /// </summary>
        private void CreateBlankLoan()
        {
            Guid loanId = Guid.Empty;

            try
            {
                CLoanFileCreator creator = CLoanFileCreator.GetCreator(PrincipalFactory.CurrentPrincipal, E_LoanCreationSource.UserCreateFromBlank);

                if (this.IsTest)
                {
                    loanId = creator.CreateNewTestFile(Guid.Empty);
                }
                else if (this.IsLead)
                {
                    loanId = creator.CreateLead(templateId: Guid.Empty);
                }
                else
                {
                    loanId = creator.CreateBlankLoanFile();
                }

                this.UpdateLoansAfterCreation(loanId);
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                this.SetResult("Error", true);
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                this.SetResult("Error", true);
                throw exc;
            }

            this.SetResult("loanId", loanId);
        }

        /// <summary>
        /// Creates a loan file from the given <see cref="LoanCreateService.TemplateId"/>
        /// </summary>
        private void CreateLoan()
        {
            if (Guid.Equals(this.TemplateId, Guid.Empty))
            {
                if (this.IsTemplate)
                {
                    CreateTemplate();
                }
                else
                {
                    CreateBlankLoan();
                }

                return;
            }

            Guid loanId = Guid.Empty;

            try
            {
                CLoanFileCreator creator = CLoanFileCreator.GetCreator(PrincipalFactory.CurrentPrincipal, E_LoanCreationSource.FromTemplate);

                if (this.IsTest)
                {
                    loanId = creator.CreateNewTestFile(templateId: this.TemplateId);
                }
                else if (this.IsLead)
                {
                    loanId = creator.CreateLead(templateId: this.TemplateId);
                }
                else
                {
                    loanId = creator.CreateLoanFromTemplate(templateId: this.TemplateId);
                }

                this.UpdateLoansAfterCreation(loanId);
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                this.SetResult("Error", true);
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                this.SetResult("Error", true);
                throw exc;
            }

            this.SetResult("loanId", loanId);
        }

        /// <summary>
        /// Sets sIsLineOfCredit to true if the loan's purpose is HELOC, and the purpose for leads created from blank templates.
        /// </summary>
        /// <param name="loanId">The id of the loan to be checked.</param>
        private void UpdateLoansAfterCreation(Guid loanId)
        {
            if (
                !string.IsNullOrEmpty(this.Purpose) && 
                (string.Equals(this.Purpose, "HELOC", StringComparison.OrdinalIgnoreCase) || (this.IsLead && Guid.Equals(this.TemplateId, Guid.Empty))))
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LoanCreateService));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                if (this.IsLead && Guid.Equals(this.TemplateId, Guid.Empty))
                {
                    switch (this.Purpose)
                    {
                        case "purchase":
                            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
                            break;
                        case "refinance":
                            dataLoan.sLPurposeT = E_sLPurposeT.Refin;
                            break;
                        case "construction":
                            dataLoan.sLPurposeT = E_sLPurposeT.ConstructPerm;
                            break;
                        case "HELOC":
                            break;
                        default:
                            throw new CBaseException("Error:  Invalid Lead Type.", "Leads can only be created with a Purchase, Refinance, or HELOC purpose.");
                    }
                }

                if (string.Equals(this.Purpose, "HELOC", StringComparison.OrdinalIgnoreCase))
                {
                    dataLoan.sIsLineOfCredit = true;
                }

                dataLoan.Save();
            }
        }
    }
}