<%@ Page Language="c#" CodeBehind="BorrowerEmploymentList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.BorrowerEmploymentList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>BorrowerEmploymentList</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%=AspxTools.SafeUrl(StyleSheet)%> type="text/css" rel="stylesheet">
</head>
<body ms_positioning="FlowLayout" scroll="yes" leftmargin="0" bgcolor="gainsboro" style="cursor: default">
    <script type="text/javascript">
        function _init() {
            list_oTable = document.getElementById('<%= AspxTools.ClientId(m_dg) %>');
            parent.parent_initScreen(2);
        }
        function constructEmptyRow(tr) {
            var td = tr.insertCell();
            td.innerText = " ";
            td = tr.insertCell();
            td.innerText = " ";
            td.setAttribute("align", "right");
        }
    </script>

    <form id="BorrowerEmploymentList" method="post" runat="server">
        <asp:DataGrid ID="m_dg" runat="server" DataKeyField="RecordId" BorderColor="Gainsboro" AutoGenerateColumns="False" Width="100%" EnableViewState="False">
            <AlternatingItemStyle CssClass="GridItem"></AlternatingItemStyle>
            <ItemStyle CssClass="GridItem"></ItemStyle>
            <HeaderStyle Wrap="False" CssClass="GridHeader"></HeaderStyle>
            <FooterStyle CssClass="GridFooter"></FooterStyle>
            <Columns>
                <asp:TemplateColumn HeaderText="Company">
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EmplrNm")) %>
                    </ItemTemplate>
                </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText="Monthly Income">
                    <ItemStyle HorizontalAlign="Right" Width="150px"></ItemStyle>
                    <ItemTemplate>
                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "MonI")) %>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </form>

</body>
</html>
