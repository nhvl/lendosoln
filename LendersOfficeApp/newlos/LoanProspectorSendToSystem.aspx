<%@ Page language="c#" Codebehind="LoanProspectorSendToSystem.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LoanProspectorSendToSystem" validateRequest="false" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%-- This page is very similar to the PML page of the same name, and both should be kept in sync --%>
<%-- Handy way to diff them: git diff --no-index LendersOfficeApp\newlos\LoanProspectorSendToSystem.aspx PML\main\LoanProspectorSendToSystem.aspx --%>
<!DOCTYPE html>
<html>
  <head runat="server">
    <title id="titlePrompt">Loan Product Advisor Assessment Summary</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }
        table {
            width: 100%;
        }
        td {
            padding: 5px;
        }
        #ImportOptions {
            padding: 10px;
        }
        .Hidden {
            display: none;
        }
        #Status, #Buttons {
            text-align: center;
        }
        #Status:after {
            display: inline-block;
            vertical-align: middle;<%-- http://chrisnager.com/simple-vertical-centering-with-css/ --%>
        }
        #Status.Loading:after {
            content: url("../images/status.gif");
            display: block;
        }
        #Status.Error:after {
            content: url("../images/error.png");<%-- I wanted to pass the VirtualRoot instead of .. but that causes issues since the head tag gets modified by the code behind in BasePage--%>
        }
        #Status.Success:after {
            content: url("../images/success.png");
        }
    </style>
  </head>
  <body>
    <form id="LoanProspectorSendToSystem" runat="server">
        <asp:HiddenField ID="LpResponseData" runat="server" />
        <div id="headerPrompt" class="MainRightHeader" runat="server">Loan Product Advisor Assessment Summary</div>
        <table>
          <asp:Repeater ID="AssessmentSummaryData" runat="server">
            <ItemTemplate>
              <tr class="<%# AspxTools.HtmlString(Container.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem") %>">
                <td><%# AspxTools.HtmlString(((KeyValuePair<string, string>)Container.DataItem).Key) %>:</td>
                <td><%# AspxTools.HtmlString(((KeyValuePair<string, string>)Container.DataItem).Value) %></td>
              </tr>
            </ItemTemplate>
          </asp:Repeater>
        </table>

        <div id="ImportOptions">
            <p runat="server" id="importPrompt">Import the following types of information from Loan Product Advisor:</p>
            <div><input type="checkbox" id="import1003Data" />1003 Data</div>
            <div><input type="checkbox" id="importLpFeedback" /><ml:EncodedLiteral id="lpFeedText" runat="server">LPA Feedback</ml:EncodedLiteral></div>
            <div><input type="checkbox" id="importCreditReport" />Credit Report</div>
        </div>

        <div id="Status"></div>

        <div id="Buttons">
            <input type="button" id="Import" value="Import" />
            <input type="button" id="Cancel" value="Cancel" />
        </div>
     </form>

    <script type="text/javascript"><!--
        (function () {
            var lpResponseDataHiddenInput = document.getElementById('LpResponseData');
            var importOptionsDiv = document.getElementById('ImportOptions');
            var import1003DataCheckbox = document.getElementById('import1003Data');
            var importLPFeedbackCheckbox = document.getElementById('importLpFeedback');
            var importCreditReportCheckbox = document.getElementById('importCreditReport');
            var importChoices = [import1003DataCheckbox, importLPFeedbackCheckbox, importCreditReportCheckbox];

            var statusDiv = document.getElementById('Status');

            var importButton = document.getElementById('Import');
            var cancelButton = document.getElementById('Cancel');

            var CssClasses = {
                BlankStatus: 'BlankStatus',
                Success: 'Success',
                Error: 'Error',
                Loading: 'Loading',
                Hidden: 'Hidden'
            };

            function ValidateImportCommand() {
                var validationResult = importChoices.reduce(function (previous, chkbox) { return previous || chkbox.checked; }, false);
                return {
                    isValid: validationResult,
                    message: validationResult ? '' : 'Import requires at least one checkbox to be selected.'
                };
            }

            function ToggleDisabledForAllControls(isDisabled) {
                isDisabled = isDisabled || false;
                importButton.disabled = isDisabled;
                cancelButton.disabled = isDisabled;
                importChoices.forEach(function(chkbox) { chkbox.disabled = isDisabled });
            }

            function Import() {
                var validation = ValidateImportCommand();
                if (!validation.isValid) {
                    alert(validation.message);
                    return;
                }

                ToggleDisabledForAllControls(true);
                UpdateStatus('Importing information. Please wait...', CssClasses.Loading);

                var payload = importChoices.reduce(function (obj, chkbox) { obj[chkbox.id] = chkbox.checked; return obj; }, {});
                payload['responseData'] = lpResponseDataHiddenInput.value;

                var request = new XMLHttpRequest();
                request.addEventListener('load', HandleResult);
                request.addEventListener('error', function () { SetErrorMessage(); });// Set default error message for network error
                request.open('POST', 'LoanProspectorSendToSystem.aspx/Import', true);
                request.setRequestHeader('Content-type', 'application/json; charset=utf-8');
                request.send(JSON.stringify(payload));
            }

            function HandleResult(event) {
                var responseText = event.target.responseText;
                var response = JSON.parse(responseText || '{}');
                if (response && response.d && response.d.IsSuccessful) {
                    UpdateStatus('Import complete.', CssClasses.Success);
                    AddClass(importButton, CssClasses.Hidden);
                    ConvertCancelToCloseButton();
                }
                else {
                    SetErrorMessage(response && response.d && response.d.UserMessage);
                }
            }

            function CheckImportButtonStatus() {
                var result = ValidateImportCommand();
                importButton.disabled = !result.isValid;
                UpdateStatus(result.message, CssClasses.BlankStatus);
            }

            function SetErrorMessage(message, unrecoverableError) {
                message = message || "Import failed. Please contact your LendingQB system administrator.";
                UpdateStatus(message, CssClasses.Error);
                if (!unrecoverableError) {
                    ToggleDisabledForAllControls(false);
                }
            }

            function UpdateStatus(statusMessage, cssClassToApply) {
                for (var css in CssClasses) {
                    var className = CssClasses[css];
                    if (className !== cssClassToApply) {
                        RemoveClass(statusDiv, className);
                    }
                }

                statusDiv.textContent = statusMessage;
                AddClass(statusDiv, cssClassToApply);
            }

            function ConvertCancelToCloseButton() {
                cancelButton.value = 'Close';
                cancelButton.disabled = false;
            }

            function CloseCurrentWindow() {
                onClosePopup();
            }
            function AddClass(el, className) {
                el.classList ? el.classList.add(className) : ((new RegExp('\\b' + className + '\\b', 'g')).test(el.className) ? '' : el.className += ' ' + className);
            }
            function RemoveClass(el, className) {
                el.classList ? el.classList.remove(className) : el.className = el.className.replace(new RegExp('\\b' + className + '\\b', 'g'), '');
            }

            _init = function () {
                if (ML && (ML.AutoClose || ML.GeneralErrorMessage)) {
                    ToggleDisabledForAllControls(true);
                    AddClass(importOptionsDiv, CssClasses.Hidden);
                    AddClass(importButton, CssClasses.Hidden);
                    ConvertCancelToCloseButton();
                    if (ML.GeneralErrorMessage) {
                        SetErrorMessage(ML.GeneralErrorMessage, true);
                    } else if (ML.AutoClose) {
                        CloseCurrentWindow();
                    }
                }

                importChoices.forEach(function (chkbox) {
                    chkbox.addEventListener('change', CheckImportButtonStatus);
                });
                importButton.addEventListener('click', Import);
                cancelButton.addEventListener('click', CloseCurrentWindow);

                importLPFeedbackCheckbox.checked = true;

                window.resizeTo(
                    600 + window.outerWidth - window.innerWidth,
                    500 + window.outerHeight - window.innerHeight);
            }

        })(); //-->
    </script>
  </body>
</html>
