<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="FannieDownloadOptions.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FannieDownloadOptions" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>Fannie Mae Download Options</title>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="RightBackground" style="MARGIN-LEFT:0px" scroll="auto">
	<script language=javascript>
<!--
var g_globallyUniqueIdentifier;
var g_pollingInterval = 10000;
var g_isDo = false;
var g_lenderInstitutionIdentifier = "";

function _init() {
  resize(600,490);
  f_getCasefileStatus();
  var args = getModalArgs();
  if (args){
    g_isDo = args.bIsDo;
  }
  
  
  var sDoDu = g_isDo ? "DO" : "DU";
  document.getElementById('StatementLabel').innerText = "Update this loan with the following information from " + sDoDu + ":";
  document.getElementById('Warning1003Label').innerText = "Updating 1003 data from " + sDoDu + " will delete employment, asset, liability, REO data in LendingQB and override with data from " + sDoDu + ".";
}


function f_onImportClick() {
  var bIs1003 = <%= AspxTools.JsGetElementById(Is1003) %>.checked;
  var bIsDuFindings = <%= AspxTools.JsGetElementById(IsDuFindings) %>.checked;
  var bIsCreditReport = <%= AspxTools.JsGetElementById(IsCreditReport) %>.checked;
  if (!bIs1003 && !bIsDuFindings && !bIsCreditReport) {
    alert(<%=AspxTools.JsString(JsMessages.SelectAtLeastOneOptionFnmaDownload)%>);
    return;
  }
  
  g_globallyUniqueIdentifier = "";
  f_displayWait(true);
  f_importToLOImpl();
}
function f_onCancelClick() {

  var dialogArgs = getModalArgs();
  var args = new Object();
  args["IsDo"] = dialogArgs.bIsDo ? "True" : "False";
  args["AutoLogin"] = dialogArgs.bHasAutoLogin ? "True" : "False";
  args["FannieMaeMORNETUserID"] = dialogArgs.sUserName;
  args["FannieMaeMORNETPassword"] = dialogArgs.sPassword;
    args["LoanID"] = <%=AspxTools.JsString(LoanID)%>;

  var result = gService.loanedit.call("LogoutDoDu", args);   
      
  onClosePopup();
}
function f_displayWait(bDisplay) {
  resize(300, 150);
  document.getElementById('MainPanel').style.display = bDisplay ? "none" : "";
  document.getElementById('ErrorPanel').style.display = bDisplay ? "none" : "";
  document.getElementById('WaitPanel').style.display = bDisplay ? "" : "none";
  
  document.getElementById('RetrieveDataLabel').innerText = "Retrieving data from " + (g_isDo ? "DO" : "DU");
}
function f_displayErrorMessage(errMsg) {
  resize(500, 650);
  document.getElementById('MainPanel').style.display = "";  
  document.getElementById('WaitPanel').style.display = "none";  
  document.getElementById('ErrorPanel').style.display = "";  
  document.getElementById('ErrorMessage').innerHTML = errMsg;
}

function f_displayComplete() {
  resize(500, 150);

  document.getElementById('MainPanel').style.display = "none";
  document.getElementById('WaitPanel').style.display = "none";
  document.getElementById('ErrorPanel').style.display = "none";  
  document.getElementById('CompletePanel').style.display = "";

}
function f_close() {
  onClosePopup();
}
function f_viewDuFindings() {
	var report_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/services/ViewDUFindingFrame.aspx?loanid=' + <%= AspxTools.JsString(RequestHelper.LoanID.ToString()) %>;
	var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600") ;
	win.focus() ;
	
	return false;
}
function f_viewCredit() {
  var dialogArgs = getModalArgs();
  if (dialogArgs){
    var applicationID = dialogArgs.ApplicationID;
    
    var report_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/services/ViewCreditFrame.aspx?loanid=' + <%= AspxTools.JsString(RequestHelper.LoanID.ToString()) %> + '&applicationid=' + applicationID;
    var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600") ;
    win.focus() ;	
    return false;

  }
	
}
function f_getCasefileStatus() {
  var dialogArgs = getModalArgs() || {};

  var args = new Object();
  args["LoanID"] = <%=AspxTools.JsString(LoanID)%>;
  args["IsDo"] = dialogArgs.bIsDo ? "True" : "False";
  args["AutoLogin"] = dialogArgs.bHasAutoLogin ? "True" : "False";
  args["FannieMaeMORNETUserID"] = dialogArgs.sUserName;
  args["FannieMaeMORNETPassword"] = dialogArgs.sPassword;
  args["sDuCaseId"] = dialogArgs.sDuCaseId;  
  var result = gService.loanedit.call("GetAndSaveCasefileStatus", args);
  
  if (!result.error) {
    document.getElementById('sDuCaseId').innerText = dialogArgs.sDuCaseId;
    document.getElementById('LastUnderwritingDate').innerText = result.value["LastUnderwritingDate"];
    document.getElementById('UnderwritingRecommendationDescription').innerText = result.value["UnderwritingRecommendationDescription"];
    document.getElementById('UnderwritingStatusDescription').innerText = result.value["UnderwritingStatusDescription"];
    document.getElementById('UnderwritingSubmissionType').innerText = result.value["UnderwritingSubmissionType"];
    document.getElementById('DesktopOriginatorSubmissionStatus').innerText = result.value["DesktopOriginatorSubmissionStatus"];
    document.getElementById('ProductName').innerText = result.value["ProductName"];
    document.getElementById('UpdateDate').innerText = result.value["UpdateDate"];
    document.getElementById('AllCreditStatusDescription').innerText = result.value["AllCreditStatusDescription"];
    
    g_lenderInstitutionIdentifier = result.value["LenderInstitutionIdentifier"];
    
  }
}
function f_importToLOImpl() {
  var dialogArgs = getModalArgs() || {};
  
  var args = new Object();
  args["LoanID"] = <%=AspxTools.JsString(LoanID)%>;
  args["IsDo"] = dialogArgs.bIsDo ? "True" : "False";
  args["AutoLogin"] = dialogArgs.bHasAutoLogin ? "True" : "False";
  args["FannieMaeMORNETUserID"] = dialogArgs.sUserName;
  args["FannieMaeMORNETPassword"] = dialogArgs.sPassword;
  args["GloballyUniqueIdentifier"] = g_globallyUniqueIdentifier;
  args["LenderInstitutionIdentifier"] = g_lenderInstitutionIdentifier;
  args["sDuCaseId"] = dialogArgs.sDuCaseId;  
  args["IsImport1003"] = <%= AspxTools.JsGetElementById(Is1003) %>.checked ? 'True' : 'False';
  args["IsImportDuFindings"] = <%= AspxTools.JsGetElementById(IsDuFindings) %>.checked ? 'True' : 'False';
  args["IsImportCreditReport"] = <%= AspxTools.JsGetElementById(IsCreditReport) %>.checked ? 'True' : 'False';

  var result = gService.loanedit.call("ImportToLO", args);
  if (!result.error) {
    switch (result.value["Status"]) {
      case "ERROR":
        f_displayErrorMessage(result.value["ErrorMessage"]);
        break;
      case "PROCESSING":
        g_globallyUniqueIdentifier = result.value["GloballyUniqueIdentifier"];
        setTimeout(f_importToLOImpl, g_pollingInterval);
        break;
      case "DONE":
        f_displayComplete();
        break;
    } 
  } else {
      var errMsg = 'Error.';
      if (null != result.UserMessage)
        errMsg = result.UserMessage;
      f_displayErrorMessage(errMsg);
      return false;
  }
}

//-->
</script>
    <h4 class="page-header">Casefile Status Summary</h4>
    <form id="FannieDownloadOptions" method="post" runat="server">
<table cellpadding=0 border=0 width="100%">
<tbody id="MainPanel">
<tr>
  <td>
    <table cellpadding=3 cellspacing=0 border=0>
      <tr><td style="padding-left:5px">Case ID</td><td class=fieldLabel><div id="sDuCaseId" /></td></tr>
      <tr><td style="padding-left:5px">Last Underwriting Date</td><td class=fieldLabel><div id="LastUnderwritingDate" /></td></tr>
      <tr><td style="padding-left:5px">Underwriting Recommendation</td><td class=fieldLabel><div id="UnderwritingRecommendationDescription" /></td></tr>
      <tr><td style="padding-left:5px">Underwriting Status</td><td class=fieldLabel><div id="UnderwritingStatusDescription" /></td></tr>
      <tr><td style="padding-left:5px">Underwriting Submission Type</td><td class=fieldLabel><div id="UnderwritingSubmissionType" /></td></tr>
      <tr><td style="padding-left:5px">Desktop Originator Submission Status</td><td class=fieldLabel><div id="DesktopOriginatorSubmissionStatus" /></td></tr>
      <tr><td style="padding-left:5px">Product Name</td><td class=fieldLabel><div id="ProductName" /></td></tr>
      <tr><td style="padding-left:5px">Date of Last Change to the Loan</td><td class=fieldLabel><div id="UpdateDate"/></td></tr>
      <tr><td style="padding-left:5px">Status of Credit Reports</td><td class=fieldLabel><div id="AllCreditStatusDescription"/></td></tr>
      
    </table>
  </td>
</tr>
<tr><td><hr></td></tr>
<tr><td class=fieldLabel style="PADDING-LEFT:10px"><div id="StatementLabel"></div></td></tr>
  <TR>
    <TD class=fieldLabel style="PADDING-LEFT:10px"><asp:CheckBox id=Is1003 runat="server" Text="1003 Data"></asp:CheckBox></TD></TR>
    <tr><td style="padding-left:30px"><span style="color:red">Warning:</span>&nbsp; <span id="Warning1003Label"></span>
    </td></tr>
    <% if (m_isRateLocked) { %>
    <tr><td style="padding-left:30px"><span style="color:red">Warning:</span>&nbsp; The following fields will not be updated because the rate is locked:&nbsp; Amortization Type, Due, Lock Period, Margin, Note Rate, Qualifying Rate, Rate Lock Date, Rate Lock Expiration Date, Teaser Rate, Term.</td></tr>
    <% } %>
  <TR>
    <TD class=fieldLabel style="PADDING-LEFT:10px"><asp:CheckBox id=IsDuFindings runat="server" Text="DU Findings"></asp:CheckBox></TD></TR>
  <TR>
    <TD class=fieldLabel style="PADDING-LEFT:10px"><asp:CheckBox id=IsCreditReport runat="server" Text="Credit Report"></asp:CheckBox></TD></TR>
  <TR>
    <TD align=center><input type="button" value="Update" onclick="f_onImportClick();" class="ButtonStyle">&nbsp;&nbsp;
    <input type="button" value="Do not update" onclick="f_onCancelClick();" class="ButtonStyle" style="width:112px"></TD></TR>
    </tbody>
  <tbody id="WaitPanel" style="display:none">
  <tr>
    <td valign=center align=center height=100px class="FieldLabel"><img src="../images/status.gif"><br><div id='RetrieveDataLabel'></div></td>
  </tr>
  </tbody>
  <tbody id="ErrorPanel" style="display:none">
    <tr><td><hr></td></tr>
    <tr><td style="color:red"><div id="ErrorMessage"></div></td></tr>
  </tbody>
  <tbody id="CompletePanel" style="display:none">
    <tr>
      <td class="FieldLabel" align=center>Updated data successfully.</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align=center><input type=button value="View DU Findings" onclick="f_viewDuFindings();" class="ButtonStyle" style="width:134px">&nbsp;&nbsp;
      <input type=button value="View Credit Report" onclick="f_viewCredit();" class="ButtonStyle" style="width:142px">&nbsp;&nbsp;
      <input type=button value="Close" onclick="f_close();" class="ButtonStyle">
      </td>
    </tr>
  </tbody>
  <tr>
    <td><div id="StatusMessage"></div></td>
  </tr>
</table>
     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</HTML>
