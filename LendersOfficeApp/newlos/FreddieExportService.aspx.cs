#region Auto-generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Integration.LoanProductAdvisor;
    using LendersOffice.ObjLib.Conversions.Aus;

    /// <summary>
    /// Represents the FreddieExportService page.
    /// </summary>
    public partial class FreddieExportService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initialize the page.
        /// </summary>
        protected override void Initialize() 
        {
            AddBackgroundItem(string.Empty, new FreddieExportServiceItem());
        }

        /// <summary>
        /// Handles loan framework service requests to the FreddieExportService page.
        /// </summary>
        private class FreddieExportServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
        {
            /// <summary>
            /// Implements the method for creating a new loan file object.
            /// </summary>
            /// <param name="loanId">The loan id.</param>
            /// <returns>A new loan data object with the proper dependencies for this page.</returns>
            protected override CPageData ConstructPageDataClass(Guid loanId)
            {
                return CPageData.CreateUsingSmartDependency(loanId, typeof(FreddieExportServiceItem));
            }

            /// <summary>
            /// Binds loan data on FreddieExport page save.
            /// </summary>
            /// <param name="dataLoan">The loan data to save data to.</param>
            /// <param name="dataApp">The current application to save.</param>
            protected override void BindData(CPageData dataLoan, CAppData dataApp)
            {
                dataLoan.sBuildingStatusT = (E_sBuildingStatusT)this.GetInt("sBuildingStatusT");
                dataLoan.sBuydown = this.GetBool("sBuydown");
                dataLoan.sBuydownContributorT = (E_sBuydownContributorT)this.GetInt("sBuydownContributorT");
                dataLoan.sFHASalesConcessions_rep = this.GetString("sFHASalesConcessions");
                dataLoan.sFredAffordProgId = this.GetString("sFredAffordProgId");
                dataLoan.sFreddieArmIndexT = (E_sFreddieArmIndexT)this.GetInt("sFreddieArmIndexT");
                dataLoan.sFreddieArmIndexTLckd = this.GetBool("sFreddieArmIndexTLckd");
                dataLoan.sFredieReservesAmt_rep = this.GetString("sFredieReservesAmt");
                dataLoan.sHelocBal_rep = this.GetString("sHelocBal");
                dataLoan.sHelocCreditLimit_rep = this.GetString("sHelocCreditLimit");
                dataLoan.sNegAmortT = (E_sNegAmortT)this.GetInt("sNegAmortT");
                dataLoan.sPayingOffSubordinate = this.GetBool("sPayingOffSubordinate");
                dataLoan.sProdCashoutAmt_rep = this.GetString("sProdCashoutAmt");
                dataLoan.sSpIsInPud = this.GetBool("sSpIsInPud");
                dataLoan.sSpMarketVal_rep = this.GetString("sSpMarketVal");
                dataLoan.sFHAFinancedDiscPtAmt_rep = this.GetString("sFHAFinancedDiscPtAmt");
                dataLoan.sFreddieConstructionT = (E_sFreddieConstructionT)this.GetInt("sFreddieConstructionT");
                dataLoan.sFredieReservesAmtLckd = this.GetBool("sFredieReservesAmtLckd");
                dataLoan.sFreddieLoanId = this.GetString("sFreddieLoanId");
                dataLoan.sFreddieTransactionId = this.GetString("sFreddieTransactionId");
                dataLoan.sLpAusKey = this.GetString("sLpAusKey");

                dataLoan.sIsUseDUDwnPmtSrc = GetBool("sIsUseDUDwnPmtSrc");
                var downpaymentSource = dataLoan.sDUDwnPmtSrc;
                downpaymentSource.Import(GetString("sDUDwnPmtSrc"));
                dataLoan.sDUDwnPmtSrc = downpaymentSource;
            }

            /// <summary>
            /// Loads data to update in the FreddieExport page after saving.
            /// </summary>
            /// <param name="dataLoan">The loan data to load data from.</param>
            /// <param name="dataApp">The current application to load from.</param>
            protected override void LoadData(CPageData dataLoan, CAppData dataApp)
            {
                SetResult("sFredieReservesAmt", dataLoan.sFredieReservesAmt_rep);
                SetResult("sFreddieArmIndexT", dataLoan.sFreddieArmIndexT);
                SetResult("sIsUseDUDwnPmtSrc", dataLoan.sIsUseDUDwnPmtSrc);
                SetResult("sDUDwnPmtSrc", dataLoan.sDUDwnPmtSrc.Serialize());
            }

            /// <summary>
            /// Generates a credit requesting data object for an application, based on service arguments.
            /// </summary>
            /// <param name="appData">The application to build a credit data object for.</param>
            /// <returns>The credit reference number to add to credit ordering info.</returns>
            private AusCreditOrderingInfo.AusCreditAppData GetAppCreditData(CAppData appData)
            {
                string key = "B" + appData.aAppId.ToString("N");
                string reference = this.GetString(key, string.Empty);
                
                string key2 = "ReorderB" + appData.aAppId.ToString("N");
                bool reorder = this.GetBool(key2, false);

                return new AusCreditOrderingInfo.AusCreditAppData(appData.aBNm, appData.aCNm, reference, appData.aAppId, reorder);
            }
        }
    }
}
