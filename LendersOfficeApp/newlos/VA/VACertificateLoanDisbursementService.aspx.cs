using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using System.Collections.Generic;

namespace LendersOfficeApp.newlos.VA
{
    public class VACertificateLoanDisbursementServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VACertificateLoanDisbursementServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataApp.aBAddr = GetString("aBAddr");
            dataApp.aBCity = GetString("aBCity");
            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBSsn = GetString("aBSsn");
            dataApp.aBState = GetString("aBState");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aBZip = GetString("aBZip");
            dataApp.aVaVestTitleODesc = GetString("aVaVestTitleODesc");
            dataApp.aVaVestTitleT = (E_aVaVestTitleT) GetInt("aVaVestTitleT");
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum");
            dataLoan.sClosedD_rep = GetString("sClosedD");
            dataLoan.sVALenderIdCode = GetString("sVALenderIdCode");
            // dataLoan.sFHAPropImprovBorrRelativeAddr = GetString("sFHAPropImprovBorrRelativeAddr");

            dataLoan.sFHAPropImprovBorrRelativeStreetAddress = GetString("sFHAPropImprovBorrRelativeStreetAddress");
            dataLoan.sFHAPropImprovBorrRelativeCity = GetString("sFHAPropImprovBorrRelativeCity");
            dataLoan.sFHAPropImprovBorrRelativeState = GetString("sFHAPropImprovBorrRelativeState");
            dataLoan.sFHAPropImprovBorrRelativeZip = GetString("sFHAPropImprovBorrRelativeZip");

            dataLoan.sFHAPropImprovBorrRelativeNm = GetString("sFHAPropImprovBorrRelativeNm");
            dataLoan.sFHAPropImprovBorrRelativePhone = GetString("sFHAPropImprovBorrRelativePhone");
            dataLoan.sFHAPropImprovBorrRelativeRelationship = GetString("sFHAPropImprovBorrRelativeRelationship");
            dataLoan.sFHAPurposeIsConstructHome = GetBool("sFHAPurposeIsConstructHome");
            dataLoan.sFHAPurposeIsFinanceCoopPurchase = GetBool("sFHAPurposeIsFinanceCoopPurchase");
            dataLoan.sFHAPurposeIsFinanceImprovement = GetBool("sFHAPurposeIsFinanceImprovement");
            dataLoan.sFHAPurposeIsManufacturedHomeAndLot = GetBool("sFHAPurposeIsManufacturedHomeAndLot");
            dataLoan.sFHAPurposeIsPurchaseExistCondo = GetBool("sFHAPurposeIsPurchaseExistCondo");
            dataLoan.sFHAPurposeIsPurchaseExistHome = GetBool("sFHAPurposeIsPurchaseExistHome");
            dataLoan.sFHAPurposeIsPurchaseManufacturedHome = GetBool("sFHAPurposeIsPurchaseManufacturedHome");
            dataLoan.sFHAPurposeIsPurchaseNewCondo = GetBool("sFHAPurposeIsPurchaseNewCondo");
            dataLoan.sFHAPurposeIsPurchaseNewHome = GetBool("sFHAPurposeIsPurchaseNewHome");
            dataLoan.sFHAPurposeIsRefiManufacturedHomeOrLotLoan = GetBool("sFHAPurposeIsRefiManufacturedHomeOrLotLoan");
            dataLoan.sFHAPurposeIsRefiManufacturedHomeToBuyLot = GetBool("sFHAPurposeIsRefiManufacturedHomeToBuyLot");
            dataLoan.sFHAPurposeIsRefinance = GetBool("sFHAPurposeIsRefinance");
            dataLoan.sIsAlterationCompleted = GetBool("sIsAlterationCompleted");
            dataLoan.sLNoteD_rep = GetString("sLNoteD");
            dataLoan.sLProceedPaidOutD_rep = GetString("sLProceedPaidOutD");
            dataLoan.sLeaseHoldExpireD_rep = GetString("sLeaseHoldExpireD");
            dataLoan.sLotAcquiredD_rep = GetString("sLotAcquiredD");
            dataLoan.sMaturityDLckd = GetBool("sMaturityDLckd");
            dataLoan.sMaturityD_rep = GetString("sMaturityD");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sProFloodInsFaceAmt_rep = GetString("sProFloodInsFaceAmt");
            dataLoan.sProFloodInsPerYr_rep = GetString("sProFloodInsPerYr");
            dataLoan.sProFloodInsPerYrLckd = GetBool("sProFloodInsPerYrLckd");
            dataLoan.sProHazInsFaceAmt_rep = GetString("sProHazInsFaceAmt");
            dataLoan.sProHazInsPerYr_rep = GetString("sProHazInsPerYr");
            dataLoan.sProHazInsPerYrLckd = GetBool("sProHazInsPerYrLckd");
            dataLoan.sProRealETxPerYrLckd = GetBool("sProRealETxPerYrLckd");
            dataLoan.sProRealETxPerYr_rep = GetString("sProRealETxPerYr");
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sFHAPropImprovBorrRelativeLot = GetString("sFHAPropImprovBorrRelativeLot");
            dataLoan.sFHAPropImprovBorrRelativeBlock = GetString("sFHAPropImprovBorrRelativeBlock");
            dataLoan.sFHAPropImprovBorrRelativeSubdivision = GetString("sFHAPropImprovBorrRelativeSubdivision");
            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sVaAdditionalSecurityTakenDesc = GetString("sVaAdditionalSecurityTakenDesc");
            dataLoan.sVaAdditionalSecurityTakenDescPrintSeparately = GetBool("sVaAdditionalSecurityTakenDescPrintSeparately");
            dataLoan.sVaAgent1RoleDesc = GetString("sVaAgent1RoleDesc");
            dataLoan.sVaAgent2RoleDesc = GetString("sVaAgent2RoleDesc");
            dataLoan.sVaAgent3RoleDesc = GetString("sVaAgent3RoleDesc");
            dataLoan.sVaAgent4RoleDesc = GetString("sVaAgent4RoleDesc");
            dataLoan.sVaAgent5RoleDesc = GetString("sVaAgent5RoleDesc");
            dataLoan.sVaEstateHeldOtherDesc = GetString("sVaEstateHeldOtherDesc");
            dataLoan.sVaEstateHeldT = (E_sVaEstateHeldT) GetInt("sVaEstateHeldT");
            dataLoan.sVaEstateHeldTLckd = GetBool("sVaEstateHeldTLckd");
            dataLoan.sVaIsAutoProc = GetBool("sVaIsAutoProc");
            dataLoan.sVaIsGuarantyEvidenceRequested = GetBool("sVaIsGuarantyEvidenceRequested");
            dataLoan.sVaIsInsuranceEvidenceRequested = GetBool("sVaIsInsuranceEvidenceRequested");
            dataLoan.sVaIsPriorApprovalProc = GetBool("sVaIsPriorApprovalProc");
            dataLoan.sVaLProceedDepositDesc = GetString("sVaLProceedDepositDesc");
            dataLoan.sVaLProceedDepositT = (E_sVaLProceedDepositT) GetInt("sVaLProceedDepositT");
            dataLoan.sVaLenderCaseNum = GetString("sVaLenderCaseNum");
            dataLoan.sVaLenderCaseNumLckd = GetBool("sVaLenderCaseNumLckd");
            dataLoan.sVaLienPosOtherDesc = GetString("sVaLienPosOtherDesc");
            dataLoan.sVaLienPosT = (E_sVaLienPosT) GetInt("sVaLienPosT");
            dataLoan.sVaLienPosTLckd = GetBool("sVaLienPosTLckd");
            dataLoan.sVaLotPurchPrice_rep = GetString("sVaLotPurchPrice");
            dataLoan.sVaMaintainAssessPmtPerYearLckd = GetBool("sVaMaintainAssessPmtPerYearLckd");
            dataLoan.sVaMaintainAssessPmtPerYear_rep = GetString("sVaMaintainAssessPmtPerYear");
            dataLoan.sVaNonrealtyAcquiredWithLDesc = GetString("sVaNonrealtyAcquiredWithLDesc");
            dataLoan.sVaNonrealtyAcquiredWithLDescPrintSeparately = GetBool("sVaNonrealtyAcquiredWithLDescPrintSeparately");

            dataLoan.sVaSpecialAssessUnpaid_rep = GetString("sVaSpecialAssessUnpaid");

            dataApp.aFHABorrCertInformedPropValNotAwareAtContractSigning = GetBool("aFHABorrCertInformedPropValNotAwareAtContractSigning");
            dataApp.aFHABorrCertInformedPropValAwareAtContractSigning = GetBool("aFHABorrCertInformedPropValAwareAtContractSigning");
            dataApp.aFHABorrCertInformedPropVal_rep = GetString("aFHABorrCertInformedPropVal");
            dataApp.aVaNotDischargedOrReleasedSinceCertOfEligibility = GetBool("aVaNotDischargedOrReleasedSinceCertOfEligibility");

            string aFHABorrCertOcc = GetString("aFHABorrCertOcc", "");
            if (!string.IsNullOrEmpty(aFHABorrCertOcc)) // pretend this set of bools is actually a single enum
            {
                dataApp.aFHABorrCertOccIsAsHome                     = (aFHABorrCertOcc == "aFHABorrCertOccIsAsHome");
                dataApp.aFHABorrCertOccIsAsHomeForActiveSpouse      = (aFHABorrCertOcc == "aFHABorrCertOccIsAsHomeForActiveSpouse");
                dataApp.aFHABorrCertOccIsChildAsHome                = (aFHABorrCertOcc == "aFHABorrCertOccIsChildAsHome");
                dataApp.aFHABorrCertOccIsAsHomePrev                 = (aFHABorrCertOcc == "aFHABorrCertOccIsAsHomePrev");
                dataApp.aFHABorrCertOccIsAsHomePrevForActiveSpouse  = (aFHABorrCertOcc == "aFHABorrCertOccIsAsHomePrevForActiveSpouse");
                dataApp.aFHABorrCertOccIsChildAsHomePrev            = (aFHABorrCertOcc == "aFHABorrCertOccIsChildAsHomePrev");
            }

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PrepareDate_rep = GetString("VA26_1820PrepareDate");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent1, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PreparerName = GetString("VA26_1820Agent1PreparerName");
            preparer.StreetAddr = GetString("VA26_1820Agent1StreetAddr");
            preparer.City = GetString("VA26_1820Agent1City");
            preparer.State = GetString("VA26_1820Agent1State");
            preparer.Zip = GetString("VA26_1820Agent1Zip");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent2, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PreparerName = GetString("VA26_1820Agent2PreparerName");
            preparer.StreetAddr = GetString("VA26_1820Agent2StreetAddr");
            preparer.City = GetString("VA26_1820Agent2City");
            preparer.State = GetString("VA26_1820Agent2State");
            preparer.Zip = GetString("VA26_1820Agent2Zip");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent3, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PreparerName = GetString("VA26_1820Agent3PreparerName");
            preparer.StreetAddr = GetString("VA26_1820Agent3StreetAddr");
            preparer.City = GetString("VA26_1820Agent3City");
            preparer.State = GetString("VA26_1820Agent3State");
            preparer.Zip = GetString("VA26_1820Agent3Zip");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent4, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PreparerName = GetString("VA26_1820Agent4PreparerName");
            preparer.StreetAddr = GetString("VA26_1820Agent4StreetAddr");
            preparer.City = GetString("VA26_1820Agent4City");
            preparer.State = GetString("VA26_1820Agent4State");
            preparer.Zip = GetString("VA26_1820Agent4Zip");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent5, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PreparerName = GetString("VA26_1820Agent5PreparerName");
            preparer.StreetAddr = GetString("VA26_1820Agent5StreetAddr");
            preparer.City = GetString("VA26_1820Agent5City");
            preparer.State = GetString("VA26_1820Agent5State");
            preparer.Zip = GetString("VA26_1820Agent5Zip");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Lender, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("VA26_1820LenderCompanyName");
            preparer.StreetAddr = GetString("VA26_1820LenderStreetAddr");
            preparer.City = GetString("VA26_1820LenderCity");
            preparer.State = GetString("VA26_1820LenderState");
            preparer.Zip = GetString("VA26_1820LenderZip");
            preparer.PhoneOfCompany = GetString("VA26_1820LenderPhoneOfCompany");
            preparer.Update();

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("aBAddr",                                        dataApp.aBAddr);
            SetResult("aBCity",                                        dataApp.aBCity);
            SetResult("aBFirstNm",                                     dataApp.aBFirstNm);
            SetResult("aBLastNm",                                      dataApp.aBLastNm);
            SetResult("aBMidNm",                                       dataApp.aBMidNm);
            SetResult("aBSsn",                                         dataApp.aBSsn);
            SetResult("aBState",                                       dataApp.aBState);
            SetResult("aBSuffix",                                      dataApp.aBSuffix);
            SetResult("aBZip",                                         dataApp.aBZip);
            SetResult("aVaVestTitleODesc",                             dataApp.aVaVestTitleODesc);
            SetResult("aVaVestTitleT",                                 dataApp.aVaVestTitleT);
            SetResult("sAgencyCaseNum",                                dataLoan.sAgencyCaseNum);
            SetResult("sClosedD",                                      dataLoan.sClosedD_rep);
            SetResult("sVALenderIdCode",                              dataLoan.sVALenderIdCode);
            //SetResult("sFHAPropImprovBorrRelativeAddr",                dataLoan.sFHAPropImprovBorrRelativeAddr);

            SetResult("sFHAPropImprovBorrRelativeStreetAddress",       dataLoan.sFHAPropImprovBorrRelativeStreetAddress);
            SetResult("sFHAPropImprovBorrRelativeCity",                dataLoan.sFHAPropImprovBorrRelativeCity);
            SetResult("sFHAPropImprovBorrRelativeState",               dataLoan.sFHAPropImprovBorrRelativeState);
            SetResult("sFHAPropImprovBorrRelativeZip",                 dataLoan.sFHAPropImprovBorrRelativeZip);

            SetResult("sFHAPropImprovBorrRelativeNm",                  dataLoan.sFHAPropImprovBorrRelativeNm);
            SetResult("sFHAPropImprovBorrRelativePhone",               dataLoan.sFHAPropImprovBorrRelativePhone);
            SetResult("sFHAPropImprovBorrRelativeRelationship",        dataLoan.sFHAPropImprovBorrRelativeRelationship);
            SetResult("sFHAPurposeIsConstructHome",                    dataLoan.sFHAPurposeIsConstructHome);
            SetResult("sFHAPurposeIsFinanceCoopPurchase",              dataLoan.sFHAPurposeIsFinanceCoopPurchase);
            SetResult("sFHAPurposeIsFinanceImprovement",               dataLoan.sFHAPurposeIsFinanceImprovement);
            SetResult("sFHAPurposeIsManufacturedHomeAndLot",           dataLoan.sFHAPurposeIsManufacturedHomeAndLot);
            SetResult("sFHAPurposeIsPurchaseExistCondo",               dataLoan.sFHAPurposeIsPurchaseExistCondo);
            SetResult("sFHAPurposeIsPurchaseExistHome",                dataLoan.sFHAPurposeIsPurchaseExistHome);
            SetResult("sFHAPurposeIsPurchaseManufacturedHome",         dataLoan.sFHAPurposeIsPurchaseManufacturedHome);
            SetResult("sFHAPurposeIsPurchaseNewCondo",                 dataLoan.sFHAPurposeIsPurchaseNewCondo);
            SetResult("sFHAPurposeIsPurchaseNewHome",                  dataLoan.sFHAPurposeIsPurchaseNewHome);
            SetResult("sFHAPurposeIsRefiManufacturedHomeOrLotLoan",    dataLoan.sFHAPurposeIsRefiManufacturedHomeOrLotLoan);
            SetResult("sFHAPurposeIsRefiManufacturedHomeToBuyLot",     dataLoan.sFHAPurposeIsRefiManufacturedHomeToBuyLot);
            SetResult("sFHAPurposeIsRefinance",                        dataLoan.sFHAPurposeIsRefinance);
            SetResult("sFinalLAmt",                                    dataLoan.sFinalLAmt_rep);
            SetResult("sIsAlterationCompleted",                        dataLoan.sIsAlterationCompleted);
            SetResult("sLNoteD",                                       dataLoan.sLNoteD_rep);
            SetResult("sLProceedPaidOutD",                             dataLoan.sLProceedPaidOutD_rep);
            SetResult("sLeaseHoldExpireD",                             dataLoan.sLeaseHoldExpireD_rep);
            SetResult("sLotAcquiredD",                                 dataLoan.sLotAcquiredD_rep);
            SetResult("sMaturityD",                                    dataLoan.sMaturityD_rep);
            SetResult("sMaturityDLckd",                                dataLoan.sMaturityDLckd);
            SetResult("sNoteIR",                                       dataLoan.sNoteIR_rep);
            SetResult("sProFloodInsFaceAmt",                           dataLoan.sProFloodInsFaceAmt_rep);
            SetResult("sProFloodInsPerYr",                             dataLoan.sProFloodInsPerYr_rep);
            SetResult("sProFloodInsPerYrLckd",                         dataLoan.sProFloodInsPerYrLckd);
            SetResult("sProHazInsFaceAmt",                             dataLoan.sProHazInsFaceAmt_rep);
            SetResult("sProHazInsPerYr",                               dataLoan.sProHazInsPerYr_rep);
            SetResult("sProHazInsPerYrLckd",                           dataLoan.sProHazInsPerYrLckd);
            SetResult("sProRealETxPerYr",                              dataLoan.sProRealETxPerYr_rep);
            SetResult("sProRealETxPerYrLckd",                          dataLoan.sProRealETxPerYrLckd);
            SetResult("sProThisMPmt",                                  dataLoan.sProThisMPmt_rep);
            SetResult("sSchedDueD1Lckd",                               dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1",                                   dataLoan.sSchedDueD1_rep);
            SetResult("sSpAddr",                                       dataLoan.sSpAddr);
            SetResult("sSpCity",                                       dataLoan.sSpCity);
            SetResult("sSpState",                                      dataLoan.sSpState);
            SetResult("sSpZip",                                        dataLoan.sSpZip);
            SetResult("sFHAPropImprovBorrRelativeLot",                 dataLoan.sFHAPropImprovBorrRelativeLot);
            SetResult("sFHAPropImprovBorrRelativeBlock",               dataLoan.sFHAPropImprovBorrRelativeBlock);
            SetResult("sFHAPropImprovBorrRelativeSubdivision",         dataLoan.sFHAPropImprovBorrRelativeSubdivision);
            SetResult("sTerm",                                         dataLoan.sTerm_rep);
            SetResult("sVaAdditionalSecurityTakenDesc",                dataLoan.sVaAdditionalSecurityTakenDesc);
            SetResult("sVaAdditionalSecurityTakenDescPrintSeparately", dataLoan.sVaAdditionalSecurityTakenDescPrintSeparately);
            SetResult("sVaAgent1RoleDesc",                             dataLoan.sVaAgent1RoleDesc);
            SetResult("sVaAgent2RoleDesc",                             dataLoan.sVaAgent2RoleDesc);
            SetResult("sVaAgent3RoleDesc",                             dataLoan.sVaAgent3RoleDesc);
            SetResult("sVaAgent4RoleDesc",                             dataLoan.sVaAgent4RoleDesc);
            SetResult("sVaAgent5RoleDesc",                             dataLoan.sVaAgent5RoleDesc);
            SetResult("sVaEstateHeldOtherDesc",                        dataLoan.sVaEstateHeldOtherDesc);
            SetResult("sVaEstateHeldT",                                dataLoan.sVaEstateHeldT);
            SetResult("sVaEstateHeldTLckd",                            dataLoan.sVaEstateHeldTLckd);
            SetResult("sVaIsAutoProc",                                 dataLoan.sVaIsAutoProc); 
            SetResult("sVaIsGuarantyEvidenceRequested",                dataLoan.sVaIsGuarantyEvidenceRequested); 
            SetResult("sVaIsInsuranceEvidenceRequested",               dataLoan.sVaIsInsuranceEvidenceRequested); 
            SetResult("sVaIsPriorApprovalProc",                        dataLoan.sVaIsPriorApprovalProc); 
            SetResult("sVaLProceedDepositDesc",                        dataLoan.sVaLProceedDepositDesc);
            SetResult("sVaLProceedDepositT",                           dataLoan.sVaLProceedDepositT);
            SetResult("sVaLenderCaseNum",                              dataLoan.sVaLenderCaseNum);
            SetResult("sVaLenderCaseNumLckd",                          dataLoan.sVaLenderCaseNumLckd);
            SetResult("sVaLienPosOtherDesc",                           dataLoan.sVaLienPosOtherDesc);
            SetResult("sVaLienPosT",                                   dataLoan.sVaLienPosT);
            SetResult("sVaLienPosTLckd",                               dataLoan.sVaLienPosTLckd);
            SetResult("sVaLotPurchPrice",                              dataLoan.sVaLotPurchPrice_rep);
            SetResult("sVaMaintainAssessPmtPerYear",                   dataLoan.sVaMaintainAssessPmtPerYear_rep);
            SetResult("sVaMaintainAssessPmtPerYearLckd",               dataLoan.sVaMaintainAssessPmtPerYearLckd);
            SetResult("sVaNonrealtyAcquiredWithLDesc",                 dataLoan.sVaNonrealtyAcquiredWithLDesc);
            SetResult("sVaNonrealtyAcquiredWithLDescPrintSeparately",  dataLoan.sVaNonrealtyAcquiredWithLDescPrintSeparately);
            SetResult("sVaSpecialAssessPmtPerYear",                    dataLoan.sVaSpecialAssessPmtPerYear_rep);
            SetResult("sVaSpecialAssessUnpaid",                        dataLoan.sVaSpecialAssessUnpaid_rep);

            SetResult("aFHABorrCertInformedPropValNotAwareAtContractSigning", dataApp.aFHABorrCertInformedPropValNotAwareAtContractSigning);
            SetResult("aFHABorrCertInformedPropValAwareAtContractSigning", dataApp.aFHABorrCertInformedPropValAwareAtContractSigning);
            SetResult("aFHABorrCertInformedPropVal", dataApp.aFHABorrCertInformedPropVal_rep);
            SetResult("aVaNotDischargedOrReleasedSinceCertOfEligibility", dataApp.aVaNotDischargedOrReleasedSinceCertOfEligibility);

            if (dataApp.aFHABorrCertOccIsAsHome)
                SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsAsHome");
            else if (dataApp.aFHABorrCertOccIsAsHomeForActiveSpouse)
                SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsAsHomeForActiveSpouse");
            else if (dataApp.aFHABorrCertOccIsChildAsHome)
                SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsChildAsHome");
            else if (dataApp.aFHABorrCertOccIsAsHomePrev)
                SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsAsHomePrev");
            else if (dataApp.aFHABorrCertOccIsAsHomePrevForActiveSpouse)
                SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsAsHomePrevForActiveSpouse");
            else if (dataApp.aFHABorrCertOccIsChildAsHomePrev)
                SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsChildAsHomePrev");

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1820PrepareDate", preparer.PrepareDate_rep);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent1, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1820Agent1PreparerName", preparer.PreparerName);
            SetResult("VA26_1820Agent1StreetAddr", preparer.StreetAddr);
            SetResult("VA26_1820Agent1City", preparer.City);
            SetResult("VA26_1820Agent1State", preparer.State);
            SetResult("VA26_1820Agent1Zip", preparer.Zip);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent2, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1820Agent2PreparerName", preparer.PreparerName);
            SetResult("VA26_1820Agent2StreetAddr", preparer.StreetAddr);
            SetResult("VA26_1820Agent2City", preparer.City);
            SetResult("VA26_1820Agent2State", preparer.State);
            SetResult("VA26_1820Agent2Zip", preparer.Zip);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent3, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1820Agent3PreparerName", preparer.PreparerName);
            SetResult("VA26_1820Agent3StreetAddr", preparer.StreetAddr);
            SetResult("VA26_1820Agent3City", preparer.City);
            SetResult("VA26_1820Agent3State", preparer.State);
            SetResult("VA26_1820Agent3Zip", preparer.Zip);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent4, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1820Agent4PreparerName", preparer.PreparerName);
            SetResult("VA26_1820Agent4StreetAddr", preparer.StreetAddr);
            SetResult("VA26_1820Agent4City", preparer.City);
            SetResult("VA26_1820Agent4State", preparer.State);
            SetResult("VA26_1820Agent4Zip", preparer.Zip);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent5, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1820Agent5PreparerName", preparer.PreparerName);
            SetResult("VA26_1820Agent5StreetAddr", preparer.StreetAddr);
            SetResult("VA26_1820Agent5City", preparer.City);
            SetResult("VA26_1820Agent5State", preparer.State);
            SetResult("VA26_1820Agent5Zip", preparer.Zip);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1820LenderCompanyName", preparer.CompanyName);
            SetResult("VA26_1820LenderStreetAddr", preparer.StreetAddr);
            SetResult("VA26_1820LenderCity", preparer.City);
            SetResult("VA26_1820LenderState", preparer.State);
            SetResult("VA26_1820LenderZip", preparer.Zip);
            SetResult("VA26_1820LenderPhoneOfCompany", preparer.PhoneOfCompany);
        }
    }
	/// <summary>
	/// Summary description for VACertificateLoanDisbursementService.
	/// </summary>
	public partial class VACertificateLoanDisbursementService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new VACertificateLoanDisbursementServiceItem());
        }
	}
}
