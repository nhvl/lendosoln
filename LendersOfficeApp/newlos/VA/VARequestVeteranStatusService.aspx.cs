using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
    public class VARequestVeteranStatusServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VARequestVeteranStatusServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataApp.aActiveMilitaryStatTri             = GetTriState("aActiveMilitaryStatTri");
            dataApp.aBAddr                             = GetString("aBAddr");
            dataApp.aBCity                             = GetString("aBCity");
            dataApp.aBDob_rep                          = GetString("aBDob");
            dataApp.aBFirstNm                          = GetString("aBFirstNm");
            dataApp.aBLastNm                           = GetString("aBLastNm");
            dataApp.aBMidNm                            = GetString("aBMidNm");
            dataApp.aBState                            = GetString("aBState");
            dataApp.aBSuffix                           = GetString("aBSuffix");
            dataApp.aBZip                              = GetString("aBZip");
            dataApp.aVaClaimNum                        = GetString("aVaClaimNum");
            dataApp.aVaServ1BranchNum                  = GetString("aVaServ1BranchNum");
            dataApp.aVaServ1EndD_rep                   = GetString("aVaServ1EndD");
            dataApp.aVaServ1FullNm                     = GetString("aVaServ1FullNm");
            dataApp.aVaServ1Num                        = GetString("aVaServ1Num");
            dataApp.aVaServ1Ssn                        = GetString("aVaServ1Ssn");
            dataApp.aVaServ1StartD_rep                 = GetString("aVaServ1StartD");
            dataApp.aVaServ2BranchNum                  = GetString("aVaServ2BranchNum");
            dataApp.aVaServ2EndD_rep                   = GetString("aVaServ2EndD");
            dataApp.aVaServ2FullNm                     = GetString("aVaServ2FullNm");
            dataApp.aVaServ2Num                        = GetString("aVaServ2Num");
            dataApp.aVaServ2Ssn                        = GetString("aVaServ2Ssn");
            dataApp.aVaServ2StartD_rep                 = GetString("aVaServ2StartD");
            dataApp.aWereActiveMilitaryDutyDayAfterTri = GetTriState("aWereActiveMilitaryDutyDayAfterTri");

        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("aActiveMilitaryStatTri",             dataApp.aActiveMilitaryStatTri);
            SetResult("aBAddr",                             dataApp.aBAddr);
            SetResult("aBCity",                             dataApp.aBCity);
            SetResult("aBDob",                              dataApp.aBDob_rep);
            SetResult("aBFirstNm",                          dataApp.aBFirstNm);
            SetResult("aBLastNm",                           dataApp.aBLastNm);
            SetResult("aBMidNm",                            dataApp.aBMidNm);
            SetResult("aBState",                            dataApp.aBState);
            SetResult("aBSuffix",                           dataApp.aBSuffix);
            SetResult("aBZip",                              dataApp.aBZip);
            SetResult("aVaClaimNum",                        dataApp.aVaClaimNum);
            SetResult("aVaServ1BranchNum",                  dataApp.aVaServ1BranchNum);
            SetResult("aVaServ1EndD",                       dataApp.aVaServ1EndD_rep);
            SetResult("aVaServ1FullNm",                     dataApp.aVaServ1FullNm);
            SetResult("aVaServ1Num",                        dataApp.aVaServ1Num);
            SetResult("aVaServ1Ssn",                        dataApp.aVaServ1Ssn);
            SetResult("aVaServ1StartD",                     dataApp.aVaServ1StartD_rep);
            SetResult("aVaServ2BranchNum",                  dataApp.aVaServ2BranchNum);
            SetResult("aVaServ2EndD",                       dataApp.aVaServ2EndD_rep);
            SetResult("aVaServ2FullNm",                     dataApp.aVaServ2FullNm);
            SetResult("aVaServ2Num",                        dataApp.aVaServ2Num);
            SetResult("aVaServ2Ssn",                        dataApp.aVaServ2Ssn);
            SetResult("aVaServ2StartD",                     dataApp.aVaServ2StartD_rep);
            SetResult("aWereActiveMilitaryDutyDayAfterTri", dataApp.aWereActiveMilitaryDutyDayAfterTri);

        }        
    }
	/// <summary>
	/// Summary description for VARequestVeteranStatusService.
	/// </summary>
	public partial class VARequestVeteranStatusService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new VARequestVeteranStatusServiceItem());
        }
	}
}
