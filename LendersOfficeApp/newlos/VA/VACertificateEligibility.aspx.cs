using System;
using System.Collections;
using System.Web.UI.WebControls;
using DataAccess;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.VA
{
	public partial class VACertificateEligibility : BaseLoanPage
	{
        #region Protected member variables
        protected System.Web.UI.WebControls.TextBox TextBox9;
        protected System.Web.UI.WebControls.TextBox TextBox10;
        protected System.Web.UI.WebControls.TextBox TextBox11;
        protected System.Web.UI.WebControls.TextBox TextBox12;
        protected System.Web.UI.WebControls.TextBox TextBox13;
        #endregion

        protected string sSpAddr;
        protected string sSpCity;
        protected string sSpState;
        protected string sSpZip;

        private void Bind_aVaServBranchNumActive(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("", ""));
            ddl.Items.Add(new ListItem("Air Force", "AirForce"));
            ddl.Items.Add(new ListItem("Army", "Army"));
            ddl.Items.Add(new ListItem("Marines", "Marines"));
            ddl.Items.Add(new ListItem("Navy", "Navy"));
        }
        private void Bind_aVaServBranchNumReserve(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("", ""));
            ddl.Items.Add(new ListItem("Air National Guard", "AirNationalGuard"));
            ddl.Items.Add(new ListItem("Army National Guard", "ArmyNationalGuard"));
            ddl.Items.Add(new ListItem("Army Reserves", "ArmyReserves"));
            ddl.Items.Add(new ListItem("Coast Guard", "CoastGuard"));
            ddl.Items.Add(new ListItem("Marines Reserves", "MarinesReserves"));
            ddl.Items.Add(new ListItem("Navy Reserves", "NavyReserves"));
        }

        //If value is not in DDL, add to dropdown as a legacy value
        private void SetPossibleDropDownListValue(DropDownList ddl, string value)
        {
            ListItem item = ddl.Items.FindByValue(value);
            if (item == null)
            {
                ddl.Items.Add(new ListItem(value, value));
            }
            ddl.SelectedValue = value;
        }
        private void BindLoanTypeDesc(MeridianLink.CommonControls.ComboBox cbl) 
        {
            cbl.Items.Add("N / A");
            cbl.Items.Add("Home");
            cbl.Items.Add("Refinance");
            cbl.Items.Add("Mfg. Home");
            cbl.Items.Add("Direct");
        }

        private void BindServTitle(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("", ""));
            ddl.Items.Add(new ListItem("CommissionedOfficer", "CommissionedOfficer"));
            ddl.Items.Add(new ListItem("NonCommissionedOfficer", "NonCommissionedOfficer"));
            ddl.Items.Add(new ListItem("Enlisted", "Enlisted"));
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            EnableJqueryMigrate = false;
            this.PageID = "VA_26_1880";
            this.PageTitle = "Request for a Certificate of Eligibility";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CVA_26_1880_1PDF);


            BindServTitle(aVaServ1Title);
            BindServTitle(aVaServ2Title);
            BindServTitle(aVaServ3Title);
            BindServTitle(aVaServ4Title);
            BindServTitle(aVaServ5Title);
            BindServTitle(aVaServ6Title);
            BindServTitle(aVaServ7Title);

            aVaOwnOtherVaLoanT.Items.Add(new ListItem() { Text = "Yes", Value = E_aVaOwnOtherVaLoanT.Yes.ToString("D") });
            aVaOwnOtherVaLoanT.Items.Add(new ListItem() { Text = "No", Value = E_aVaOwnOtherVaLoanT.No.ToString("D") });
            aVaOwnOtherVaLoanT.Items.Add(new ListItem() { Text = "Not Applicable- Never obtained a VA-guaranteed home loan", Value = E_aVaOwnOtherVaLoanT.NA.ToString("D") }); 

            aBZip.SmartZipcode(aBCity, aBState);
            aBZipMail.SmartZipcode(aBCityMail, aBStateMail);

            Tools.BindSuffix(aBSuffix);

            Tools.Bind_aAddrMailSourceT(aBAddrMailSourceT);

        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VACertificateEligibility));
            dataLoan.InitLoad();
            sFileVersion = dataLoan.sFileVersion;
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            aBFirstNm.Text                    = dataApp.aBFirstNm;
            aBMidNm.Text                      = dataApp.aBMidNm;
            aBLastNm.Text                     = dataApp.aBLastNm;
            aBSuffix.Text                     = dataApp.aBSuffix;
            aBDob.Text                        = dataApp.aBDob_rep;
            aBBusPhone.Text                   = dataApp.aBBusPhone;
            aBAddr.Text                       = dataApp.aBAddr;
            aBCity.Text                       = dataApp.aBCity;
            aBState.Value                     = dataApp.aBState;
            aBZip.Text                        = dataApp.aBZip;
            Tools.SetDropDownListValue(aBAddrMailSourceT, dataApp.aBAddrMailSourceT);
            aBAddrMail.Text                   = dataApp.aBAddrMail;
            aBCityMail.Text                   = dataApp.aBCityMail;
            aBStateMail.Value                 = dataApp.aBStateMail;
            aBZipMail.Text                    = dataApp.aBZipMail;

            aVaServedAltNameIs.Checked = dataApp.aVaServedAltNameIs;
            aVaServedAltName.Text = dataApp.aVaServedAltName;
            aVaDischargedDisabilityIs.Checked = dataApp.aVaDischargedDisabilityIs;
            aVaClaimNum.Text = dataApp.aVaClaimNum;

            aActiveMilitaryStatTri.Checked = dataApp.aActiveMilitaryStatTri == E_TriState.Yes;

            Bind_aVaServBranchNumActive(aVaServ1BranchNum);
            Bind_aVaServBranchNumActive(aVaServ2BranchNum);
            Bind_aVaServBranchNumActive(aVaServ3BranchNum);
            Bind_aVaServBranchNumReserve(aVaServ4BranchNum);
            Bind_aVaServBranchNumReserve(aVaServ5BranchNum);
            Bind_aVaServBranchNumReserve(aVaServ6BranchNum);
            Bind_aVaServBranchNumReserve(aVaServ7BranchNum);

            SetPossibleDropDownListValue(aVaServ1BranchNum, dataApp.aVaServ1BranchNum);
            aVaServ1StartD.Text = dataApp.aVaServ1StartD_rep;
            aVaServ1EndD.Text = dataApp.aVaServ1EndD_rep;
            Tools.SetDropDownListValue(aVaServ1Title, dataApp.aVaServ1Title);
            aVaServ1Num.Text = dataApp.aVaServ1Num;

            SetPossibleDropDownListValue(aVaServ2BranchNum, dataApp.aVaServ2BranchNum);
            aVaServ2StartD.Text = dataApp.aVaServ2StartD_rep;
            aVaServ2EndD.Text = dataApp.aVaServ2EndD_rep;
            Tools.SetDropDownListValue(aVaServ2Title, dataApp.aVaServ2Title);
            aVaServ2Num.Text = dataApp.aVaServ2Num;


            SetPossibleDropDownListValue(aVaServ3BranchNum, dataApp.aVaServ3BranchNum);
            aVaServ3StartD.Text = dataApp.aVaServ3StartD_rep;
            aVaServ3EndD.Text = dataApp.aVaServ3EndD_rep;
            Tools.SetDropDownListValue(aVaServ3Title, dataApp.aVaServ3Title);
            aVaServ3Num.Text = dataApp.aVaServ3Num;


            SetPossibleDropDownListValue(aVaServ4BranchNum, dataApp.aVaServ4BranchNum);
            aVaServ4StartD.Text = dataApp.aVaServ4StartD_rep;
            aVaServ4EndD.Text = dataApp.aVaServ4EndD_rep;
            Tools.SetDropDownListValue(aVaServ4Title, dataApp.aVaServ4Title);
            aVaServ4Num.Text = dataApp.aVaServ4Num;


            SetPossibleDropDownListValue(aVaServ5BranchNum, dataApp.aVaServ5BranchNum);
            aVaServ5StartD.Text = dataApp.aVaServ5StartD_rep;
            aVaServ5EndD.Text = dataApp.aVaServ5EndD_rep;
            Tools.SetDropDownListValue(aVaServ5Title, dataApp.aVaServ5Title);
            aVaServ5Num.Text = dataApp.aVaServ5Num;


            SetPossibleDropDownListValue(aVaServ6BranchNum, dataApp.aVaServ6BranchNum);
            aVaServ6StartD.Text = dataApp.aVaServ6StartD_rep;
            aVaServ6EndD.Text = dataApp.aVaServ6EndD_rep;
            Tools.SetDropDownListValue(aVaServ6Title, dataApp.aVaServ6Title);
            aVaServ6Num.Text = dataApp.aVaServ6Num;


            SetPossibleDropDownListValue(aVaServ7BranchNum, dataApp.aVaServ7BranchNum);
            aVaServ7StartD.Text = dataApp.aVaServ7StartD_rep;
            aVaServ7EndD.Text = dataApp.aVaServ7EndD_rep;
            aVaServ7Num.Text = dataApp.aVaServ7Num;
            Tools.SetDropDownListValue(aVaServ7Title, dataApp.aVaServ7Title);

            this.RegisterJsGlobalVariables("aVaOwnOtherVaLoanTReadOnly", dataApp.aVaOwnOtherVaLoanTReadOnly);
            this.RegisterJsGlobalVariables("aVaApplyOtherLoanIsCalculated", dataApp.aVaApplyOtherLoanIsCalculated);
            E_aVaOwnOtherVaLoanT t = dataApp.aVaOwnOtherVaLoanT; 
            if( t == E_aVaOwnOtherVaLoanT.LeaveBlank )
            {
                t = E_aVaOwnOtherVaLoanT.NA;
            }
            Tools.SetRadioButtonListValue(aVaOwnOtherVaLoanT,t );

            IVaPastLoanCollection collection = dataApp.aVaPastLCollection;

            int count = collection.CountRegular;

            // 4/8/2004 dd - Only display the first 6 records of CVaPastL  //3 a v 

            var cVaPastLRecords = new[] { 
                new TextBox[] { CVaPastL_DateOfLoan0,  CVaPastL_StreetAddress0, CVaPastL_CityState0 },
                new TextBox[] { CVaPastL_DateOfLoan1,  CVaPastL_StreetAddress1, CVaPastL_CityState1 },
                new TextBox[] { CVaPastL_DateOfLoan2,  CVaPastL_StreetAddress2, CVaPastL_CityState2 },
            };
            for (int i = 0; i < count && i < cVaPastLRecords.Length; i++) 
            {
                IVaPastLoan record = collection.GetRegularRecordAt(i);
                cVaPastLRecords[i][0].Text = record.DateOfLoan;
                cVaPastLRecords[i][1].Text = record.StAddr;
                cVaPastLRecords[i][2].Text = record.CityState; 
            }


            aVaApplyOneTimeRestorationTri.Checked = dataApp.aVaApplyOneTimeRestorationTri == E_TriState.Yes;
            aVaApplyOneTimeRestorationDateOfLoan.Text = dataApp.aVaApplyOneTimeRestorationDateOfLoan;
            aVaApplyOneTimeRestorationStAddr.Text = dataApp.aVaApplyOneTimeRestorationStAddr;
            aVaApplyOneTimeRestorationCityState.Text = dataApp.aVaApplyOneTimeRestorationCityState;


            aVaApplyRegularRefiCashoutTri.Checked = dataApp.aVaApplyRegularRefiCashoutTri == E_TriState.Yes;
            aVaApplyRegularRefiCashoutDateOfLoan.Text = dataApp.aVaApplyRegularRefiCashoutDateOfLoan;
            aVaApplyRegularRefiCashoutStAddr.Text = dataApp.aVaApplyRegularRefiCashoutStAddr;
            aVaApplyRegularRefiCashoutCityState.Text = dataApp.aVaApplyRegularRefiCashoutCityState;

            aVaApplyRefiNoCashoutTri.Checked = dataApp.aVaApplyRefiNoCashoutTri == E_TriState.Yes;
            aVaApplyRefiNoCashoutDateOfLoan.Text = dataApp.aVaApplyRefiNoCashoutDateOfLoan;
            aVaApplyRefiNoCashoutStAddr.Text = dataApp.aVaApplyRefiNoCashoutStAddr;
            aVaApplyRefiNoCashoutCityState.Text = dataApp.aVaApplyRefiNoCashoutCityState;

            // Populate information for subject property.
            sSpAddr = dataLoan.sSpAddr;
            sSpCity = dataLoan.sSpCity;
            sSpState = dataLoan.sSpState;
            sSpZip = dataLoan.sSpZip;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        
        }
	}
}
