using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
	/// <summary>
	/// Summary description for VACertificateLoanDisbursement.
	/// </summary>
	public partial class VACertificateLoanDisbursement : BaseLoanPage
	{
        #region Protected member variables

        protected System.Web.UI.WebControls.CheckBox sVaAdditionalSecurityTakenDescPrintSepar;
        #endregion

        private void Bind_AgentFunction(MeridianLink.CommonControls.ComboBox cbl) 
        {
            cbl.Items.Add("obtained information for loan application");
            cbl.Items.Add("ordered credit report");
            cbl.Items.Add("verification of employment");
            cbl.Items.Add("verification of deposits");
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageID = "VA_26_1820";
            this.PageTitle = "Certification of Loan Disbursement";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CVA_26_1820PDF);
            
            aBZip.SmartZipcode(aBCity, aBState);
            sSpZip.SmartZipcode(sSpCity, sSpState);
            sFHAPropImprovBorrRelativeZip.SmartZipcode(sFHAPropImprovBorrRelativeCity, sFHAPropImprovBorrRelativeState);

            VA26_1820Agent1Zip.SmartZipcode(VA26_1820Agent1City, VA26_1820Agent1State);
            VA26_1820Agent2Zip.SmartZipcode(VA26_1820Agent2City, VA26_1820Agent2State);
            VA26_1820Agent3Zip.SmartZipcode(VA26_1820Agent3City, VA26_1820Agent3State);
            VA26_1820Agent4Zip.SmartZipcode(VA26_1820Agent4City, VA26_1820Agent4State);
            VA26_1820Agent5Zip.SmartZipcode(VA26_1820Agent5City, VA26_1820Agent5State);
            VA26_1820LenderZip.SmartZipcode(VA26_1820LenderCity, VA26_1820LenderState);

            Tools.Bind_aVaVestTitleT(aVaVestTitleT);
            Tools.Bind_sVaLienPosT(sVaLienPosT);
            Tools.Bind_sVaEstateHeldT(sVaEstateHeldT);
            Tools.Bind_sVaLProceedDepositT(sVaLProceedDepositT);
            
            Bind_AgentFunction(sVaAgent1RoleDesc);
            Bind_AgentFunction(sVaAgent2RoleDesc);
            Bind_AgentFunction(sVaAgent3RoleDesc);
            Bind_AgentFunction(sVaAgent4RoleDesc);
            Bind_AgentFunction(sVaAgent5RoleDesc);

            Tools.BindSuffix(aBSuffix);
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here


        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VACertificateLoanDisbursement));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            aBAddr.Text                                           = dataApp.aBAddr;
            aBCity.Text                                           = dataApp.aBCity;
            aBFirstNm.Text                                        = dataApp.aBFirstNm;
            aBLastNm.Text                                         = dataApp.aBLastNm;
            aBMidNm.Text                                          = dataApp.aBMidNm;
            aBSsn.Text                                            = dataApp.aBSsn;
            aBState.Value                                         = dataApp.aBState;
            aBSuffix.Text                                         = dataApp.aBSuffix;
            aBZip.Text                                            = dataApp.aBZip;
            aVaVestTitleODesc.Text                                = dataApp.aVaVestTitleODesc;
            sAgencyCaseNum.Text                                   = dataLoan.sAgencyCaseNum;
            sClosedD.Text                                         = dataLoan.sClosedD_rep;
            sVALenderIdCode.Text                                  = dataLoan.sVALenderIdCode;
            //sFHAPropImprovBorrRelativeAddr.Text                   = dataLoan.sFHAPropImprovBorrRelativeAddr;
            sFHAPropImprovBorrRelativeStreetAddress.Text          = dataLoan.sFHAPropImprovBorrRelativeStreetAddress;
            sFHAPropImprovBorrRelativeCity.Text                   = dataLoan.sFHAPropImprovBorrRelativeCity;
            sFHAPropImprovBorrRelativeState.Value                 = dataLoan.sFHAPropImprovBorrRelativeState;
            sFHAPropImprovBorrRelativeZip.Text                    = dataLoan.sFHAPropImprovBorrRelativeZip;
            sFHAPropImprovBorrRelativeNm.Text                     = dataLoan.sFHAPropImprovBorrRelativeNm;
            sFHAPropImprovBorrRelativePhone.Text                  = dataLoan.sFHAPropImprovBorrRelativePhone;
            sFHAPropImprovBorrRelativeRelationship.Text           = dataLoan.sFHAPropImprovBorrRelativeRelationship;
            sFHAPurposeIsConstructHome.Checked                    = dataLoan.sFHAPurposeIsConstructHome;
            sFHAPurposeIsFinanceCoopPurchase.Checked              = dataLoan.sFHAPurposeIsFinanceCoopPurchase;
            sFHAPurposeIsFinanceImprovement.Checked               = dataLoan.sFHAPurposeIsFinanceImprovement;
            sFHAPurposeIsManufacturedHomeAndLot.Checked           = dataLoan.sFHAPurposeIsManufacturedHomeAndLot;
            sFHAPurposeIsPurchaseExistCondo.Checked               = dataLoan.sFHAPurposeIsPurchaseExistCondo;
            sFHAPurposeIsPurchaseExistHome.Checked                = dataLoan.sFHAPurposeIsPurchaseExistHome;
            sFHAPurposeIsPurchaseManufacturedHome.Checked         = dataLoan.sFHAPurposeIsPurchaseManufacturedHome;
            sFHAPurposeIsPurchaseNewCondo.Checked                 = dataLoan.sFHAPurposeIsPurchaseNewCondo;
            sFHAPurposeIsPurchaseNewHome.Checked                  = dataLoan.sFHAPurposeIsPurchaseNewHome;
            sFHAPurposeIsRefiManufacturedHomeOrLotLoan.Checked    = dataLoan.sFHAPurposeIsRefiManufacturedHomeOrLotLoan;
            sFHAPurposeIsRefiManufacturedHomeToBuyLot.Checked     = dataLoan.sFHAPurposeIsRefiManufacturedHomeToBuyLot;
            sFHAPurposeIsRefinance.Checked                        = dataLoan.sFHAPurposeIsRefinance;
            sFinalLAmt.Text                                       = dataLoan.sFinalLAmt_rep;
            sIsAlterationCompleted.Checked                        = dataLoan.sIsAlterationCompleted;
            sLNoteD.Text                                          = dataLoan.sLNoteD_rep;
            sLProceedPaidOutD.Text                                = dataLoan.sLProceedPaidOutD_rep;
            sLeaseHoldExpireD.Text                                = dataLoan.sLeaseHoldExpireD_rep;
            sLotAcquiredD.Text                                    = dataLoan.sLotAcquiredD_rep;
            sMaturityD.Text                                       = dataLoan.sMaturityD_rep;
            sMaturityDLckd.Checked                                = dataLoan.sMaturityDLckd;
            sNoteIR.Text                                          = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sProFloodInsFaceAmt.Text                              = dataLoan.sProFloodInsFaceAmt_rep;
            sProFloodInsPerYr.Text                                = dataLoan.sProFloodInsPerYr_rep;
            sProFloodInsPerYrLckd.Checked                         = dataLoan.sProFloodInsPerYrLckd;
            sProHazInsFaceAmt.Text                                = dataLoan.sProHazInsFaceAmt_rep;
            sProHazInsPerYr.Text                                  = dataLoan.sProHazInsPerYr_rep;
            sProHazInsPerYrLckd.Checked                           = dataLoan.sProHazInsPerYrLckd;
            sProRealETxPerYr.Text                                 = dataLoan.sProRealETxPerYr_rep;
            sProRealETxPerYrLckd.Checked                          = dataLoan.sProRealETxPerYrLckd;
            sProThisMPmt.Text                                     = dataLoan.sProThisMPmt_rep;
            sSchedDueD1Lckd.Checked                               = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text                                      = dataLoan.sSchedDueD1_rep;
            sSpAddr.Text                                          = dataLoan.sSpAddr;
            sSpCity.Text                                          = dataLoan.sSpCity;
            sSpState.Value                                        = dataLoan.sSpState;
            sSpZip.Text                                           = dataLoan.sSpZip;
            sFHAPropImprovBorrRelativeLot.Text                    = dataLoan.sFHAPropImprovBorrRelativeLot;
            sFHAPropImprovBorrRelativeBlock.Text                  = dataLoan.sFHAPropImprovBorrRelativeBlock;
            sFHAPropImprovBorrRelativeSubdivision.Text            = dataLoan.sFHAPropImprovBorrRelativeSubdivision;
            sTerm.Text                                            = dataLoan.sTerm_rep;
            sTerm.ReadOnly                                        = dataLoan.sIsRateLocked || IsReadOnly;
            sVaAdditionalSecurityTakenDesc.Text                   = dataLoan.sVaAdditionalSecurityTakenDesc;
            sVaAdditionalSecurityTakenDescPrintSeparately.Checked = dataLoan.sVaAdditionalSecurityTakenDescPrintSeparately;
            sVaAgent1RoleDesc.Text                                = dataLoan.sVaAgent1RoleDesc;
            sVaAgent2RoleDesc.Text                                = dataLoan.sVaAgent2RoleDesc;
            sVaAgent3RoleDesc.Text                                = dataLoan.sVaAgent3RoleDesc;
            sVaAgent4RoleDesc.Text                                = dataLoan.sVaAgent4RoleDesc;
            sVaAgent5RoleDesc.Text                                = dataLoan.sVaAgent5RoleDesc;
            sVaEstateHeldOtherDesc.Text                           = dataLoan.sVaEstateHeldOtherDesc;
            sVaEstateHeldTLckd.Checked                            = dataLoan.sVaEstateHeldTLckd;
            sVaIsAutoProc.Checked                                 = dataLoan.sVaIsAutoProc; 
            sVaIsGuarantyEvidenceRequested.Checked                = dataLoan.sVaIsGuarantyEvidenceRequested; 
            sVaIsInsuranceEvidenceRequested.Checked               = dataLoan.sVaIsInsuranceEvidenceRequested; 
            sVaIsPriorApprovalProc.Checked                        = dataLoan.sVaIsPriorApprovalProc; 
            sVaLProceedDepositDesc.Text                           = dataLoan.sVaLProceedDepositDesc;
            sVaLenderCaseNum.Text                                 = dataLoan.sVaLenderCaseNum;
            sVaLenderCaseNumLckd.Checked                          = dataLoan.sVaLenderCaseNumLckd;
            sVaLienPosOtherDesc.Text                              = dataLoan.sVaLienPosOtherDesc;
            sVaLienPosTLckd.Checked                               = dataLoan.sVaLienPosTLckd;
            sVaLotPurchPrice.Text                                 = dataLoan.sVaLotPurchPrice_rep;
            sVaMaintainAssessPmtPerYear.Text                      = dataLoan.sVaMaintainAssessPmtPerYear_rep;
            sVaMaintainAssessPmtPerYearLckd.Checked               = dataLoan.sVaMaintainAssessPmtPerYearLckd;
            sVaNonrealtyAcquiredWithLDesc.Text                    = dataLoan.sVaNonrealtyAcquiredWithLDesc;
            sVaNonrealtyAcquiredWithLDescPrintSeparately.Checked  = dataLoan.sVaNonrealtyAcquiredWithLDescPrintSeparately;
            sVaSpecialAssessPmtPerYear.Text                       = dataLoan.sVaSpecialAssessPmtPerYear_rep;
            sVaSpecialAssessUnpaid.Text                           = dataLoan.sVaSpecialAssessUnpaid_rep;

            aFHABorrCertInformedPropValNotAwareAtContractSigning.Checked = dataApp.aFHABorrCertInformedPropValNotAwareAtContractSigning;
            aFHABorrCertInformedPropValAwareAtContractSigning.Checked = dataApp.aFHABorrCertInformedPropValAwareAtContractSigning;
            aFHABorrCertInformedPropVal.Text = dataApp.aFHABorrCertInformedPropVal_rep;
            aVaNotDischargedOrReleasedSinceCertOfEligibility.Checked = dataApp.aVaNotDischargedOrReleasedSinceCertOfEligibility;

            Tools.SetDropDownListValue(aVaVestTitleT, dataApp.aVaVestTitleT);
            Tools.SetDropDownListValue(sVaEstateHeldT, dataLoan.sVaEstateHeldT);
            Tools.SetDropDownListValue(sVaLProceedDepositT, dataLoan.sVaLProceedDepositT);
            Tools.SetDropDownListValue(sVaLienPosT, dataLoan.sVaLienPosT);
            
            aFHABorrCertOccIsAsHome.Checked                              = dataApp.aFHABorrCertOccIsAsHome;
            aFHABorrCertOccIsAsHomeForActiveSpouse.Checked               = dataApp.aFHABorrCertOccIsAsHomeForActiveSpouse;
            aFHABorrCertOccIsChildAsHome.Checked                         = dataApp.aFHABorrCertOccIsChildAsHome;
            aFHABorrCertOccIsAsHomePrev.Checked                          = dataApp.aFHABorrCertOccIsAsHomePrev;
            aFHABorrCertOccIsAsHomePrevForActiveSpouse.Checked           = dataApp.aFHABorrCertOccIsAsHomePrevForActiveSpouse;
            aFHABorrCertOccIsChildAsHomePrev.Checked                     = dataApp.aFHABorrCertOccIsChildAsHomePrev;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1820PrepareDate.Text = preparer.PrepareDate_rep;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent1, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1820Agent1PreparerName.Text = preparer.PreparerName;
            VA26_1820Agent1StreetAddr.Text = preparer.StreetAddr;
            VA26_1820Agent1City.Text = preparer.City;
            VA26_1820Agent1State.Value = preparer.State;
            VA26_1820Agent1Zip.Text = preparer.Zip;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent2, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1820Agent2PreparerName.Text = preparer.PreparerName;
            VA26_1820Agent2StreetAddr.Text = preparer.StreetAddr;
            VA26_1820Agent2City.Text = preparer.City;
            VA26_1820Agent2State.Value = preparer.State;
            VA26_1820Agent2Zip.Text = preparer.Zip;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent3, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1820Agent3PreparerName.Text = preparer.PreparerName;
            VA26_1820Agent3StreetAddr.Text = preparer.StreetAddr;
            VA26_1820Agent3City.Text = preparer.City;
            VA26_1820Agent3State.Value = preparer.State;
            VA26_1820Agent3Zip.Text = preparer.Zip;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent4, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1820Agent4PreparerName.Text = preparer.PreparerName;
            VA26_1820Agent4StreetAddr.Text = preparer.StreetAddr;
            VA26_1820Agent4City.Text = preparer.City;
            VA26_1820Agent4State.Value = preparer.State;
            VA26_1820Agent4Zip.Text = preparer.Zip;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent5, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1820Agent5PreparerName.Text = preparer.PreparerName;
            VA26_1820Agent5StreetAddr.Text = preparer.StreetAddr;
            VA26_1820Agent5City.Text = preparer.City;
            VA26_1820Agent5State.Value = preparer.State;
            VA26_1820Agent5Zip.Text = preparer.Zip;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1820LenderCompanyName.Text = preparer.CompanyName;
            VA26_1820LenderStreetAddr.Text = preparer.StreetAddr;
            VA26_1820LenderCity.Text = preparer.City;
            VA26_1820LenderState.Value = preparer.State;
            VA26_1820LenderZip.Text = preparer.Zip;
            VA26_1820LenderPhoneOfCompany.Text = preparer.PhoneOfCompany;
        }


		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
                    /// Required method for Designer support - do not modify
                    /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}
}
