using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
    public class VACertificateEligibilityServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VACertificateEligibilityServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aBDob_rep = GetString("aBDob");
            dataApp.aBBusPhone = GetString("aBBusPhone");
            dataApp.aBAddr = GetString("aBAddr");
            dataApp.aBCity = GetString("aBCity");
            dataApp.aBState = GetString("aBState");
            dataApp.aBZip = GetString("aBZip");
            dataApp.aBAddrMailSourceT = (E_aAddrMailSourceT)GetInt("aBAddrMailSourceT");
            dataApp.aBAddrMail = GetString("aBAddrMail");
            dataApp.aBCityMail = GetString("aBCityMail");
            dataApp.aBStateMail = GetString("aBStateMail");
            dataApp.aBZipMail = GetString("aBZipMail");


            dataApp.aVaDischargedDisabilityIs = GetBool("aVaDischargedDisabilityIs");
            dataApp.aVaServedAltNameIs = GetBool("aVaServedAltNameIs");
            dataApp.aVaServedAltName = GetString("aVaServedAltName");
            dataApp.aVaClaimNum = GetString("aVaClaimNum");


            dataApp.aActiveMilitaryStatTri = GetBool("aActiveMilitaryStatTri") ? E_TriState.Yes : E_TriState.No;

            dataApp.aVaServ1BranchNum = GetString("aVaServ1BranchNum");
            dataApp.aVaServ1StartD_rep = GetString("aVaServ1StartD");
            dataApp.aVaServ1EndD_rep = GetString("aVaServ1EndD");
            dataApp.aVaServ1Title = GetString("aVaServ1Title");
            dataApp.aVaServ1Num = GetString("aVaServ1Num");

            dataApp.aVaServ2BranchNum = GetString("aVaServ2BranchNum");
            dataApp.aVaServ2StartD_rep = GetString("aVaServ2StartD");
            dataApp.aVaServ2EndD_rep = GetString("aVaServ2EndD");
            dataApp.aVaServ2Title = GetString("aVaServ2Title");
            dataApp.aVaServ2Num = GetString("aVaServ2Num");

            dataApp.aVaServ3BranchNum = GetString("aVaServ3BranchNum");
            dataApp.aVaServ3StartD_rep = GetString("aVaServ3StartD");
            dataApp.aVaServ3EndD_rep = GetString("aVaServ3EndD");
            dataApp.aVaServ3Title = GetString("aVaServ3Title");
            dataApp.aVaServ3Num = GetString("aVaServ3Num");

            dataApp.aVaServ4BranchNum = GetString("aVaServ4BranchNum");
            dataApp.aVaServ4StartD_rep = GetString("aVaServ4StartD");
            dataApp.aVaServ4EndD_rep = GetString("aVaServ4EndD");
            dataApp.aVaServ4Title = GetString("aVaServ4Title");
            dataApp.aVaServ4Num = GetString("aVaServ4Num");

            dataApp.aVaServ5BranchNum = GetString("aVaServ5BranchNum");
            dataApp.aVaServ5StartD_rep = GetString("aVaServ5StartD");
            dataApp.aVaServ5EndD_rep = GetString("aVaServ5EndD");
            dataApp.aVaServ5Title = GetString("aVaServ5Title");
            dataApp.aVaServ5Num = GetString("aVaServ5Num");

            dataApp.aVaServ6BranchNum = GetString("aVaServ6BranchNum");
            dataApp.aVaServ6StartD_rep = GetString("aVaServ6StartD");
            dataApp.aVaServ6EndD_rep = GetString("aVaServ6EndD");
            dataApp.aVaServ6Title = GetString("aVaServ6Title");
            dataApp.aVaServ6Num = GetString("aVaServ6Num");

            dataApp.aVaServ7BranchNum = GetString("aVaServ7BranchNum");
            dataApp.aVaServ7StartD_rep = GetString("aVaServ7StartD");
            dataApp.aVaServ7EndD_rep = GetString("aVaServ7EndD");
            dataApp.aVaServ7Title = GetString("aVaServ7Title");
            dataApp.aVaServ7Num = GetString("aVaServ7Num");





            dataApp.aVaOwnOtherVaLoanT = (E_aVaOwnOtherVaLoanT)GetInt("aVaOwnOtherVaLoanT", 3/*NA*/);

            while (dataApp.aVaPastLCollection.CountRegular < 3)
            {
                dataApp.aVaPastLCollection.AddRecord(E_VaPastLT.Regular);
            }

            ReadCVaPastLRecord(dataApp, 0);
            ReadCVaPastLRecord(dataApp, 1);
            ReadCVaPastLRecord(dataApp, 2);
            dataApp.aVaPastLCollection.Flush();
            
            dataApp.aVaApplyOneTimeRestorationTri = GetBool("aVaApplyOneTimeRestorationTri") ? E_TriState.Yes : E_TriState.No;
            dataApp.aVaApplyOneTimeRestorationDateOfLoan = GetString("aVaApplyOneTimeRestorationDateOfLoan");
            dataApp.aVaApplyOneTimeRestorationStAddr = GetString("aVaApplyOneTimeRestorationStAddr");
            dataApp.aVaApplyOneTimeRestorationCityState = GetString("aVaApplyOneTimeRestorationCityState");

            dataApp.aVaApplyRegularRefiCashoutTri = GetBool("aVaApplyRegularRefiCashoutTri") ? E_TriState.Yes : E_TriState.No;
            dataApp.aVaApplyRegularRefiCashoutDateOfLoan = GetString("aVaApplyRegularRefiCashoutDateOfLoan");
            dataApp.aVaApplyRegularRefiCashoutStAddr = GetString("aVaApplyRegularRefiCashoutStAddr");
            dataApp.aVaApplyRegularRefiCashoutCityState = GetString("aVaApplyRegularRefiCashoutCityState");

            dataApp.aVaApplyRefiNoCashoutTri = GetBool("aVaApplyRefiNoCashoutTri") ? E_TriState.Yes : E_TriState.No;
            dataApp.aVaApplyRefiNoCashoutDateOfLoan = GetString("aVaApplyRefiNoCashoutDateOfLoan");
            dataApp.aVaApplyRefiNoCashoutStAddr = GetString("aVaApplyRefiNoCashoutStAddr");
            dataApp.aVaApplyRefiNoCashoutCityState = GetString("aVaApplyRefiNoCashoutCityState");

        }


        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aBDob", dataApp.aBDob_rep);
            SetResult("aBBusPhone", dataApp.aBBusPhone);
            SetResult("aBAddr", dataApp.aBAddr);
            SetResult("aBCity", dataApp.aBCity);
            SetResult("aBState", dataApp.aBState);
            SetResult("aBZip", dataApp.aBZip);

            SetResult("aBAddrMailSourceT", dataApp.aBAddrMailSourceT);
            SetResult("aBAddrMail", dataApp.aBAddrMail);
            SetResult("aBCityMail", dataApp.aBCityMail);
            SetResult("aBStateMail", dataApp.aBStateMail);
            SetResult("aBZipMail", dataApp.aBZipMail);

            SetResult("aVaClaimNum", dataApp.aVaClaimNum);
            SetResult("aVaServedAltNameIs", dataApp.aVaServedAltNameIs);
            SetResult("aVaServedAltName", dataApp.aVaServedAltName);
            SetResult("aVaDischargedDisabilityIs", dataApp.aVaDischargedDisabilityIs);

            SetResult("aVaServ1BranchNum", dataApp.aVaServ1BranchNum);
            SetResult("aVaServ1StartD", dataApp.aVaServ1StartD_rep);
            SetResult("aVaServ1EndD", dataApp.aVaServ1EndD_rep);
            SetResult("aVaServ1Title", dataApp.aVaServ1Title);
            SetResult("aVaServ1Num", dataApp.aVaServ1Num);

            SetResult("aVaServ2BranchNum", dataApp.aVaServ2BranchNum);
            SetResult("aVaServ2StartD", dataApp.aVaServ2StartD_rep);
            SetResult("aVaServ2EndD", dataApp.aVaServ2EndD_rep);
            SetResult("aVaServ2Title", dataApp.aVaServ2Title);
            SetResult("aVaServ2Num", dataApp.aVaServ2Num);

            SetResult("aVaServ3BranchNum", dataApp.aVaServ3BranchNum);
            SetResult("aVaServ3StartD", dataApp.aVaServ3StartD_rep);
            SetResult("aVaServ3EndD", dataApp.aVaServ3EndD_rep);
            SetResult("aVaServ3Title", dataApp.aVaServ3Title);
            SetResult("aVaServ3Num", dataApp.aVaServ3Num);

            SetResult("aVaServ4BranchNum", dataApp.aVaServ4BranchNum);
            SetResult("aVaServ4StartD", dataApp.aVaServ4StartD_rep);
            SetResult("aVaServ4EndD", dataApp.aVaServ4EndD_rep);
            SetResult("aVaServ4Title", dataApp.aVaServ4Title);
            SetResult("aVaServ4Num", dataApp.aVaServ4Num);

            SetResult("aVaServ5BranchNum", dataApp.aVaServ5BranchNum);
            SetResult("aVaServ5StartD", dataApp.aVaServ5StartD_rep);
            SetResult("aVaServ5EndD", dataApp.aVaServ5EndD_rep);
            SetResult("aVaServ5Title", dataApp.aVaServ5Title);
            SetResult("aVaServ5Num", dataApp.aVaServ5Num);

            SetResult("aVaServ6BranchNum", dataApp.aVaServ6BranchNum);
            SetResult("aVaServ6StartD", dataApp.aVaServ6StartD_rep);
            SetResult("aVaServ6EndD", dataApp.aVaServ6EndD_rep);
            SetResult("aVaServ6Title", dataApp.aVaServ6Title);
            SetResult("aVaServ6Num", dataApp.aVaServ6Num);

            SetResult("aVaServ7BranchNum", dataApp.aVaServ7BranchNum);
            SetResult("aVaServ7StartD", dataApp.aVaServ7StartD_rep);
            SetResult("aVaServ7EndD", dataApp.aVaServ7EndD_rep);
            SetResult("aVaServ7Title", dataApp.aVaServ7Title);
            SetResult("aVaServ7Num", dataApp.aVaServ7Num);

            SetResult("aVaOwnOtherVaLoanTReadOnly", dataApp.aVaOwnOtherVaLoanTReadOnly);
            SetResult("aVaApplyOtherLoanIsCalculated", dataApp.aVaApplyOtherLoanIsCalculated);
            SetResult("aVaOwnOtherVaLoanT", dataApp.aVaOwnOtherVaLoanT.ToString("D"));

            WriteCVaPastLRecord(dataApp, 0);
            WriteCVaPastLRecord(dataApp, 1);
            WriteCVaPastLRecord(dataApp, 2);

            SetResult("aVaApplyOneTimeRestorationTri", dataApp.aVaApplyOneTimeRestorationTri == E_TriState.Yes);
            SetResult("aVaApplyOneTimeRestorationDateOfLoan", dataApp.aVaApplyOneTimeRestorationDateOfLoan);
            SetResult("aVaApplyOneTimeRestorationStAddr", dataApp.aVaApplyOneTimeRestorationStAddr);
            SetResult("aVaApplyOneTimeRestorationCityState", dataApp.aVaApplyOneTimeRestorationCityState);

            SetResult("aVaApplyRegularRefiCashoutTri", dataApp.aVaApplyRegularRefiCashoutTri == E_TriState.Yes);
            SetResult("aVaApplyRegularRefiCashoutDateOfLoan", dataApp.aVaApplyRegularRefiCashoutDateOfLoan);
            SetResult("aVaApplyRegularRefiCashoutStAddr", dataApp.aVaApplyRegularRefiCashoutStAddr);
            SetResult("aVaApplyRegularRefiCashoutCityState", dataApp.aVaApplyRegularRefiCashoutCityState);

            SetResult("aVaApplyRefiNoCashoutTri", dataApp.aVaApplyRefiNoCashoutTri == E_TriState.Yes);
            SetResult("aVaApplyRefiNoCashoutDateOfLoan", dataApp.aVaApplyRefiNoCashoutDateOfLoan);
            SetResult("aVaApplyRefiNoCashoutStAddr", dataApp.aVaApplyRefiNoCashoutStAddr);
            SetResult("aVaApplyRefiNoCashoutCityState", dataApp.aVaApplyRefiNoCashoutCityState);
        }

        private void WriteCVaPastLRecord(CAppData data, int num)
        {
            if (num >= data.aVaPastLCollection.CountRegular)
            {
                return;
            }
           IVaPastLoan record =  data.aVaPastLCollection.GetRegularRecordAt(num);
           SetResult("CVaPastL_DateOfLoan" + num, record.DateOfLoan);
           SetResult("CVaPastL_StreetAddress" + num, record.StAddr);
           SetResult("CVaPastL_CityState" + num, record.CityState);  
        }

        private void ReadCVaPastLRecord(CAppData data, int num)
        {
            IVaPastLoan record = data.aVaPastLCollection.GetRegularRecordAt(num);
            record.DateOfLoan = GetString("CVaPastL_DateOfLoan" + num);
            record.StAddr = GetString("CVaPastL_StreetAddress" + num);
            record.CityState = GetString("CVaPastL_CityState" + num);
        }
    }

    
	public partial class VACertificateEligibilityService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new VACertificateEligibilityServiceItem());
        }

	}
}
