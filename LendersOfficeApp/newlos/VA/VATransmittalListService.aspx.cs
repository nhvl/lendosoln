using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{

    public class VATransmittalListServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VATransmittalListServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum");

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_0285Lender, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PreparerName = GetString("VA26_0285LenderPreparerName");
            preparer.Title = GetString("VA26_0285LenderTitle");
            preparer.Phone = GetString("VA26_0285LenderPhone");
            preparer.Update();
            
            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_0285Lender, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("VA26_0285BrokerCompanyName");
            preparer.Update();
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_0285Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_0285LenderPreparerName", preparer.PreparerName);
            SetResult("VA26_0285LenderTitle", preparer.Title);
            SetResult("VA26_0285LenderPhone", preparer.Phone);
            
            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_0285Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_0285BrokerCompanyName", preparer.CompanyName);
        }
    }

	/// <summary>
	/// Summary description for VATransmittalListService.
	/// </summary>
	public partial class VATransmittalListService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new VATransmittalListServiceItem());
        }
	}
}
