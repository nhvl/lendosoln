using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
	public partial class VALoanAnalysis : BaseLoanPage
	{
        #region Protected member variables

        #endregion


        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CVA_26_6393CompletePDF);
            this.PageID = "VA_26_6393";
            this.PageTitle = "VA Loan Analysis";


            aVaUtilityIncludedTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            aVaCrRecordSatisfyTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            aVaLMeetCrStandardTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sVaRecommendAppApprovedT.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sVAFinalActionT.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VALoanAnalysis));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IPrimaryEmploymentRecord aBPrimaryEmp = dataApp.aBEmpCollection.GetPrimaryEmp(false);
            IPrimaryEmploymentRecord aCPrimaryEmp = dataApp.aCEmpCollection.GetPrimaryEmp(false);

            if (null != aBPrimaryEmp) 
            {
                aBPrimaryEmpEmplmtLenInMonths.Text = aBPrimaryEmp.EmplmtLenInMonths_rep;
                aBPrimaryEmpEmplmtLenInYrs.Text = aBPrimaryEmp.EmplmtLenInYrs_rep;
                aBPrimaryEmpJobTitle.Text = aBPrimaryEmp.JobTitle;
            }
            if (null != aCPrimaryEmp) 
            {
                aCPrimaryEmpEmplmtLenInMonths.Text = aCPrimaryEmp.EmplmtLenInMonths_rep;
                aCPrimaryEmpEmplmtLenInYrs.Text = aCPrimaryEmp.EmplmtLenInYrs_rep;
                aCPrimaryEmpJobTitle.Text = aCPrimaryEmp.JobTitle;
            }
            Tools.Set_TriState(aVaCrRecordSatisfyTri, dataApp.aVaCrRecordSatisfyTri);
            Tools.Set_TriState(aVaLMeetCrStandardTri, dataApp.aVaLMeetCrStandardTri);
            Tools.Set_TriState(aVaUtilityIncludedTri, dataApp.aVaUtilityIncludedTri);
            Tools.Set_TriState(sVaRecommendAppApprovedT, dataLoan.sVaRecommendAppApprovedT);
            Tools.Set_TriState(sVAFinalActionT, dataLoan.sVAFinalActionT);

            aAsstLiqTot.Text                 = dataApp.aAsstLiqTot_rep;
            aBAge.Text                       = dataApp.aBAge_rep;
            aBDependAges.Text                = dataApp.aBDependAges;
            aBFirstNm.Text                   = dataApp.aBFirstNm;
            aBLastNm.Text                    = dataApp.aBLastNm;
            aBMidNm.Text                     = dataApp.aBMidNm;
            aBSuffix.Text                    = dataApp.aBSuffix;
            aCAge.Text                       = dataApp.aCAge_rep;
            aCFirstNm.Text                   = dataApp.aCFirstNm;
            aCLastNm.Text                    = dataApp.aCLastNm;
            aCMidNm.Text                     = dataApp.aCMidNm;
            aCSuffix.Text                    = dataApp.aCSuffix;
            aFHABCaivrsNum.Text              = dataApp.aFHABCaivrsNum;
            aFHACCaivrsNum.Text              = dataApp.aFHACCaivrsNum;
            aPresTotHExp.Text                = dataApp.aPresTotHExp_rep;
            aVaBEmplmtI.Text                 = dataApp.aVaBEmplmtI_rep;
            aVaBEmplmtILckd.Checked          = dataApp.aVaBEmplmtILckd;
            aVaBFedITax.Text                 = dataApp.aVaBFedITax_rep;
            aVaBNetI.Text                    = dataApp.aVaBNetI_rep;
            aVaBOITax.Text                   = dataApp.aVaBOITax_rep;
            aVaBONetI.Text                   = dataApp.aVaBONetI_rep;
            aVaBONetILckd.Checked            = dataApp.aVaBONetILckd;
            aVaBSsnTax.Text                  = dataApp.aVaBSsnTax_rep;
            aVaBStateITax.Text               = dataApp.aVaBStateITax_rep;
            aVaBTakehomeEmplmtI.Text         = dataApp.aVaBTakehomeEmplmtI_rep;
            aVaBTotITax.Text                 = dataApp.aVaBTotITax_rep;
            aVaCEmplmtI.Text                 = dataApp.aVaCEmplmtI_rep;
            aVaCEmplmtILckd.Checked          = dataApp.aVaCEmplmtILckd;
            aVaCFedITax.Text                 = dataApp.aVaCFedITax_rep;
            aVaCNetI.Text                    = dataApp.aVaCNetI_rep;
            aVaCOITax.Text                   = dataApp.aVaCOITax_rep;
            aVaCONetI.Text                   = dataApp.aVaCONetI_rep;
            aVaCONetILckd.Checked            = dataApp.aVaCONetILckd;
            aVaCSsnTax.Text                  = dataApp.aVaCSsnTax_rep;
            aVaCStateITax.Text               = dataApp.aVaCStateITax_rep;
            aVaCTakehomeEmplmtI.Text         = dataApp.aVaCTakehomeEmplmtI_rep;
            aVaCTotITax.Text                 = dataApp.aVaCTotITax_rep;
            aVaFamilySuportGuidelineAmt.Text = dataApp.aVaFamilySuportGuidelineAmt_rep;
            aVaFamilySupportBal.Text         = dataApp.aVaFamilySupportBal_rep;
            aVaLAnalysisN.Text               = dataApp.aVaLAnalysisN;
            aVaLiaBalTotExisting.Text        = dataApp.aVaLiaBalTotExisting_rep;
            aVaLiaMonTotDeducted.Text        = dataApp.aVaLiaMonTotDeducted_rep;
            aVaLiaMonTotExisting.Text        = dataApp.aVaLiaMonTotExisting_rep;
            aVaNetEffectiveI.Text            = dataApp.aVaNetEffectiveI_rep;
            aVaNetI.Text                     = dataApp.aVaNetI_rep;
            aVaONetI.Text                    = dataApp.aVaONetI_rep;
            aVaONetIDesc.Text                = dataApp.aVaONetIDesc;
            aVaRatio.Text                    = dataApp.aVaRatio_rep;
            aVaTakehomeEmplmtI.Text          = dataApp.aVaTakehomeEmplmtI_rep;
            aVaTotEmplmtI.Text               = dataApp.aVaTotEmplmtI_rep;
            aVaTotITax.Text                  = dataApp.aVaTotITax_rep;
            sFinalLAmt.Text                  = dataLoan.sFinalLAmt_rep;
            sNoteIR.Text                     = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sTermInYr.Text                   = dataLoan.sTermInYr_rep;
            sVaCashdwnPmt.Text               = dataLoan.sVaCashdwnPmt_rep;
            sVaCashdwnPmtLckd.Checked        = dataLoan.sVaCashdwnPmtLckd;
            sVaMaintainAssessPmtLckd.Checked = dataLoan.sVaMaintainAssessPmtLckd;
            sVaMaintainAssessPmt.Text        = dataLoan.sVaMaintainAssessPmt_rep;
            sVaProHazIns.Text                = dataLoan.sVaProHazIns_rep;
            sVaProHazInsLckd.Checked         = dataLoan.sVaProHazInsLckd;
            sVaProMaintenancePmt.Text        = dataLoan.sVaProMaintenancePmt_rep;
			sVaProMaintenanceAndUtilityPmt.Text = dataLoan.sVaProMaintenanceAndUtilityPmt_rep;
            sVaProMonthlyPmt.Text            = dataLoan.sVaProMonthlyPmt_rep;
            sVaProMonthlyPmt2.Text           = dataLoan.sVaProMonthlyPmt_rep;
            sVaProRealETx.Text               = dataLoan.sVaProRealETx_rep;
            sVaProRealETxLckd.Checked        = dataLoan.sVaProRealETxLckd;
            sVaProThisMPmt.Text              = dataLoan.sVaProThisMPmt_rep;
            sVaProThisMPmtLckd.Checked       = dataLoan.sVaProThisMPmtLckd;
            sVaProUtilityPmt.Text            = dataLoan.sVaProUtilityPmt_rep;
            sVaSpecialAssessPmt.Text         = dataLoan.sVaSpecialAssessPmt_rep;
            aVaLiaMonTotExistingAndOpNegCf.Text = dataApp.aVaLiaMonTotExistingAndOpNegCf_rep;
            aOpNegCf.Text = dataApp.aOpNegCf_rep;

            aVALAnalysisValue.Text           = dataApp.aVALAnalysisValue_rep;
            aVALAnalysisExpirationD.Text     = dataApp.aVALAnalysisExpirationD_rep;
            aVALAnalysisEconomicLifeYrs.Text = dataApp.aVALAnalysisEconomicLifeYrs_rep;
        }

        protected override void SaveData() 
        {
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        
        }

        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));


        }
	}
}
