using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;


namespace LendersOfficeApp.newlos.VA
{
	public partial class VATransmittalList : BaseLoanPage
	{

        protected void PageInit(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            this.PageTitle = "VA Transmittal List";
            this.PageID = "VA_26_0285";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CVA_26_0285PDF);

        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VATransmittalList));
            dataLoan.InitLoad();


            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_0285Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_0285LenderPreparerName.Text = preparer.PreparerName;
            VA26_0285LenderTitle.Text = preparer.Title;
            VA26_0285LenderPhone.Text = preparer.Phone;
            
            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_0285Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_0285BrokerCompanyName.Text = preparer.CompanyName;
        }

        protected override void SaveData() 
        {
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            this.UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
