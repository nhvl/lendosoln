<%@ Page language="c#" Codebehind="VARequestVeteranStatus.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.VA.VARequestVeteranStatus" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>VARequestVeteranStatus</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	
    <form id="VARequestVeteranStatus" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 border=0>
  <tr>
    <td nowrap class=MainRightHeader>VA Request for Certificate of Veteran Status (VA 
    26-8261a)&nbsp;</td></tr>
  <tr>
    <td nowrap>
      <table id=Table4 cellspacing=0 cellpadding=0 border=0>
        <tr>
          <td class=FieldLabel nowrap>Veteran First Name</td>
          <td nowrap><asp:textbox id=aBFirstNm runat="server" Width="141px"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Veteran Middle Name</td>
          <td nowrap><asp:textbox id=aBMidNm runat="server" Width="141px"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Veteran Last Name</td>
          <td nowrap><asp:textbox id=aBLastNm runat="server" Width="141px"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Veteran Suffix</td>
          <td nowrap><ml:ComboBox id=aBSuffix runat="server" Width="141px"></ml:ComboBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Date of Birth</td>
          <td nowrap><ml:datetextbox id=aBDob runat="server" preset="date" width="75"></ml:datetextbox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Present Address</td>
          <td nowrap><asp:textbox id=aBAddr runat="server" Width="278px" onchange="syncMailingAddress();"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel nowrap></td>
          <td nowrap><asp:textbox id=aBCity runat="server" Width="141px" onchange="syncMailingAddress();"></asp:textbox><ml:statedropdownlist id=aBState runat="server" onchange="syncMailingAddress();"></ml:statedropdownlist><ml:zipcodetextbox id=aBZip runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table class=InsetBorder id=Table2 cellspacing=0 cellpadding=0 width="98%" 
      border=0>
        <tr>
          <td class=LoanFormHeader nowrap align=middle colSpan=7>MILITARY 
            SERVICE DATA</td></tr>
        <tr>
          <td nowrap></td>
          <td class=FieldLabel nowrap colspan=2>B. Periods of Active 
Service</td>
          <td class=FieldLabel nowrap>C. Name</td>
          <td class=FieldLabel nowrap>D. SSN</td>
          <td class=FieldLabel nowrap>E. Service Number</td>
          <td class=FieldLabel nowrap>F. Branch of Service</td></tr>
        <tr>
          <td nowrap></td>
          <td class=FieldLabel nowrap>Date From</td>
          <td class=FieldLabel nowrap>Date To</td>
          <td nowrap></td>
          <td nowrap></td>
          <td nowrap></td>
          <td nowrap></td></tr>
        <tr>
          <td class=FieldLabel nowrap>1. </td>
          <td nowrap><ml:datetextbox id=aVaServ1StartD runat="server" preset="date" width="75"></ml:datetextbox></td>
          <td nowrap><ml:datetextbox id=aVaServ1EndD runat="server" preset="date" width="75"></ml:datetextbox></td>
          <td nowrap><asp:textbox id=aVaServ1FullNm runat="server" Width="196px"></asp:textbox></td>
          <td nowrap><ml:ssntextbox id=aVaServ1Ssn runat="server" preset="ssn" width="75px"></ml:ssntextbox></td>
          <td nowrap><asp:textbox id=aVaServ1Num runat="server" Width="141px"></asp:textbox></td>
          <td nowrap><asp:textbox id=aVaServ1BranchNum runat="server" Width="130px"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>2.</td>
          <td nowrap><ml:datetextbox id=aVaServ2StartD runat="server" preset="date" width="75"></ml:datetextbox></td>
          <td nowrap><ml:datetextbox id=aVaServ2EndD runat="server" preset="date" width="75"></ml:datetextbox></td>
          <td nowrap><asp:textbox id=aVaServ2FullNm runat="server" Width="196px"></asp:textbox></td>
          <td nowrap><ml:ssntextbox id=aVaServ2Ssn runat="server" preset="ssn" width="75px"></ml:ssntextbox></td>
          <td nowrap><asp:textbox id=aVaServ2Num runat="server" Width="141px"></asp:textbox></td>
          <td nowrap><asp:textbox id=aVaServ2BranchNum runat="server" Width="130px"></asp:textbox></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table id=Table3 cellspacing=0 cellpadding=0 border=0>
        <tr>
          <td class=FieldLabel nowrap>VA Claim Number</td>
          <td 
          nowrap><asp:TextBox id=aVaClaimNum runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Are you now on active military duty?</td>
          <td nowrap colspan="1"><asp:CheckBoxList id=aActiveMilitaryStatTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Were you on active military duty on the 
            day following the date?</td>
          <td nowrap><asp:CheckBoxList id=aWereActiveMilitaryDutyDayAfterTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></td></tr></table></td></tr></table>

     </form>
	
  </body>
</html>
