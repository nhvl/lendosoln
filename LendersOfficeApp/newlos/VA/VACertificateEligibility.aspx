<%@ Page language="c#" Codebehind="VACertificateEligibility.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.VA.VACertificateEligibility" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
  <head runat="server">
    <title>VACertificateEligibility</title>
    <style type="text/css">
        #Table2 {  border-collapse: collapse; }
        #Table2 select { width:auto; }
        
        .AlignedItem { margin-left: 20px; }
        tr.topPaddedRow { padding-top: 20px; }
        .bottomInsetBorder { border-bottom: thin groove; }
        #Table4 td {
            padding-bottom: 4px;
        }
    </style>
  </head>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<form id=VACertificateEligibility method=post runat="server">


<table id="Table1" cellspacing="0" cellpadding="0" border="0" width="780px">
    <tr>
        <td class="MainRightHeader" nowrap>
            VA Request for a Certificate of Eligibility (VA 26-1880)
        </td>
    </tr>
    <tr><td height="10"></td></tr>
    <tr>
        <td nowrap> 
            <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td class="FieldLabel" nowrap>
                        Veteran First Name
                    </td>
                    <td nowrap>
                        <asp:TextBox ID="aBFirstNm" runat="server" Width="141px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                        Veteran Middle Name
                    </td>
                    <td nowrap>
                        <asp:TextBox ID="aBMidNm" runat="server" Width="141px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                        Veteran Last Name
                    </td>
                    <td nowrap>
                        <asp:TextBox ID="aBLastNm" runat="server" Width="141px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                        Veteran Suffix
                    </td>
                    <td nowrap>
                        <ml:ComboBox ID="aBSuffix" runat="server" Width="141px"></ml:ComboBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                        Date of Birth
                    </td>
                    <td nowrap>
                        <ml:DateTextBox ID="aBDob" runat="server" preset="date" Width="75"></ml:DateTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                        Daytime Phone
                    </td>
                    <td nowrap>
                        <ml:PhoneTextBox ID="aBBusPhone" runat="server" preset="phone" Width="120"></ml:PhoneTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                        Present Address
                    </td>
                    <td nowrap>
                        <asp:TextBox ID="aBAddr" runat="server" Width="278px" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                    </td>
                    <td nowrap>
                        <asp:TextBox ID="aBCity" runat="server" Width="141px" ></asp:TextBox><ml:StateDropDownList
                            ID="aBState" runat="server" ></ml:StateDropDownList>
                        <ml:ZipcodeTextBox ID="aBZip" runat="server" preset="zipcode" Width="50"></ml:ZipcodeTextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                    </td>
                    <td nowrap>
                        <asp:DropDownList ID="aBAddrMailSourceT" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                        Mail Certification of Eligibility To
                    </td>
                    <td nowrap>
                        <asp:TextBox ID="aBAddrMail" runat="server" Width="278px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" nowrap>
                        (if different than present address)
                    </td>
                    <td nowrap>
                        <asp:TextBox ID="aBCityMail" runat="server" Width="141px"></asp:TextBox>
                        <ml:StateDropDownList ID="aBStateMail" runat="server"></ml:StateDropDownList>
                        <ml:ZipcodeTextBox ID="aBZipMail" runat="server" preset="zipcode" Width="50"></ml:ZipcodeTextBox>
                    </td>
                </tr>
                <tr class="topPaddedRow">
                    <td class="FieldLabel" colspan="2">
                        <asp:CheckBox runat="server" ID="aVaServedAltNameIs" Text="Served under another name" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" colspan="2">
                      <label for="aVaServedAltName" class="AlignedItem">Name(s) used during military service</label> <asp:TextBox runat="server" ID="aVaServedAltName" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" colspan="2">
                        <asp:CheckBox runat="server" ID="aVaDischargedDisabilityIs"  runat="server" Text="Discharged, retired or separated due to disability" />                    
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <label for="aVaClaimNum" class="AlignedItem">VA Claim File Number</label> <asp:TextBox runat="server" ID="aVaClaimNum"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </table>
    <br />
<table class="InsetBorder" id="Table2" cellspacing="0" cellpadding="0" width="780px"
    border="0">
    <tr>
        <td class="LoanFormHeader" nowrap align="middle" colspan="2">
            MILITARY SERVICE DATA
        </td>
    </tr>
    <td colspan="2">
        <asp:CheckBox runat="server" ID="aActiveMilitaryStatTri" CssClass="FieldLabel" Text="Currently on Active Duty" />
        (if currently serving on active duty leave the Date Separated field blank)
    </td>
    <tr>
        <td width="110px " class="InsetBorder">
            <span class="FieldLabel">Active Service </span>
            <br />
            <span style="font-size: 10px">DO NOT include any periods of Active Duty for Training
                or Active Guard Reserve service. DO include activation for duty under Title 10 U.S.C.
            </span>
        </td>
        <td class="InsetBorder" valign="top">
            <table cellpadding="4" cellspacing="0" style="line-height: normal;">
                <tr>
                    <td class="FieldLabel" valign="top">
                        Branch of Service
                    </td>
                    <td class="FieldLabel" valign="top">
                        Date Entered
                    </td>
                    <td class="FieldLabel" valign="top">
                        Date Separated
                    </td>
                    <td class="FieldLabel" valign="top" width="120px">
                        Officer or Enlisted?
                    </td>
                    <td class="FieldLabel" valign="top">
                        Service Number
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ1BranchNum"></asp:DropDownList>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ1StartD"></ml:DateTextBox>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ1EndD"></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ1Title">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="aVaServ1Num"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ2BranchNum"></asp:DropDownList>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ2StartD"></ml:DateTextBox>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ2EndD"></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ2Title">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="aVaServ2Num"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ3BranchNum"></asp:DropDownList>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ3StartD"></ml:DateTextBox>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ3EndD"></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ3Title">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="aVaServ3Num"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="110px" class="InsetBorder">
            <span class="FieldLabel">Reserve or
                <br />
                National Guard </span>
            <br />
            <span style="font-size: 10px">DO NOT include any activation for duty under Title 10
                U.S.C. Do Include any periods of Active Duty for Training or Active Guard Reserve
                Service </span>
        </td>
        <td class="InsetBorder" valign="top">
            <table cellpadding="4" cellspacing="0" style="line-height: normal;">
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ4BranchNum"></asp:DropDownList>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ4StartD"></ml:DateTextBox>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ4EndD"></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ4Title">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="aVaServ4Num"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ5BranchNum"></asp:DropDownList>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ5StartD"></ml:DateTextBox>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ5EndD"></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ5Title">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="aVaServ5Num"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ6BranchNum"></asp:DropDownList>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ6StartD"></ml:DateTextBox>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ6EndD"></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ6Title">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="aVaServ6Num"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ7BranchNum"></asp:DropDownList>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ7StartD"></ml:DateTextBox>
                    </td>
                    <td>
                        <ml:DateTextBox runat="server" ID="aVaServ7EndD"></ml:DateTextBox>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="aVaServ7Title">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="aVaServ7Num"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<table class="InsetBorder" id="PreviousVaLoanTable" cellpadding="0" cellspacing="0"
    width="780px">
    <thead>
        <tr>
            <td colspan="3" class="LoanFormHeader" align="center">
                Previous VA Loans
            </td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="FieldLabel">
                Currently owns any home(s) purchased or refinanced with a VA-guaranteed loan?
                <br /> 
                <asp:RadioButtonList  RepeatLayout="Flow" RepeatDirection="Horizontal" CssClass="FieldLabel" runat="server" ID ="aVaOwnOtherVaLoanT" ></asp:RadioButtonList> 
            </td>
        </tr>
        <tr  > 
            <td  class="bottomInsetBorder">
                <table class="AlignedItem ">
                    <thead>
                        <tr>
                            <td>
                                <span class="FieldLabel">Date of Loan</span><br />
                                (Month and Year)
                            </td>
                            <td class="FieldLabel">
                                Street Address
                            </td>
                            <td class="FieldLabel">
                                City and State
                            </td>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            <asp:TextBox runat="server" ID="CVaPastL_DateOfLoan0"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="CVaPastL_StreetAddress0"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="CVaPastL_CityState0"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox runat="server" ID="CVaPastL_DateOfLoan1"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="CVaPastL_StreetAddress1"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="CVaPastL_CityState1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox runat="server" ID="CVaPastL_DateOfLoan2"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="CVaPastL_StreetAddress2"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="CVaPastL_CityState2"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox runat="server" ID="aVaApplyOneTimeRestorationTri" CssClass="FieldLabel"
                    Text="Applying for the One-Time Only Restoration of Entitlement to purchase another home" />
            </td>
        </tr>
        <tr>
            <td class="bottomInsetBorder">
                <table class="AlignedItem ">
                    <thead>
                        <tr>
                            <td>
                                <span class="FieldLabel">Date of Loan</span><br />
                                (Month and Year)
                            </td>
                            <td class="FieldLabel">
                                Street Address
                            </td>
                            <td class="FieldLabel">
                                City and State
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:TextBox runat="server" ID="aVaApplyOneTimeRestorationDateOfLoan"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="aVaApplyOneTimeRestorationStAddr"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="aVaApplyOneTimeRestorationCityState"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox runat="server" ID="aVaApplyRegularRefiCashoutTri" CssClass="FieldLabel"
                    Text="Applying for a restoration of entitlement to obtain a Regular (Cash-Out) Refinance on current home" />
            </td>
        </tr>
        <tr>
            <td class="bottomInsetBorder">
                <table class="AlignedItem">
                    <thead>
                        <tr>
                            <td>
                                <span class="FieldLabel">Date of Loan</span><br />
                                (Month and Year)
                            </td>
                            <td class="FieldLabel">
                                Street Address
                            </td>
                            <td class="FieldLabel">
                                City and State
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:TextBox runat="server" ID="aVaApplyRegularRefiCashoutDateOfLoan"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="aVaApplyRegularRefiCashoutStAddr"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="aVaApplyRegularRefiCashoutCityState"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        
       <tr>
            <td>
                <asp:CheckBox runat="server" ID="aVaApplyRefiNoCashoutTri" CssClass="FieldLabel"
                    Text="Refinancing an existing VA loan to obtain a lower interest rate without receiving any cash proceeds (IRRL)" />
            </td>
        </tr>
        <tr >
            <td >
                <table class="AlignedItem">
                    <thead>
                        <tr>
                            <td>
                                <span class="FieldLabel">Date of Loan</span><br />
                                (Month and Year)
                            </td>
                            <td class="FieldLabel">
                                Street Address
                            </td>
                            <td class="FieldLabel">
                                City and State
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:TextBox runat="server" ID="aVaApplyRefiNoCashoutDateOfLoan"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="aVaApplyRefiNoCashoutStAddr"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="aVaApplyRefiNoCashoutCityState"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</form>

    <script type="text/javascript">
    
        $(function ($) {
            var aBAddrMail = $('#<%= AspxTools.ClientId(aBAddrMail) %>'), 
                aBAddr = $('#<%= AspxTools.ClientId(aBAddr) %>'),
                aBCity = $('#<%= AspxTools.ClientId(aBCity) %>'),
                aBAddrMailSourceT = $('#<%= AspxTools.ClientId(aBAddrMailSourceT) %>'),
                aBCityMail = $('#<%= AspxTools.ClientId(aBCityMail) %>'),
                aBStateMail = $('#<%= AspxTools.ClientId(aBStateMail) %>'),
                aBZipMail = $('#<%= AspxTools.ClientId(aBZipMail) %>'),
                aBState = $('#<%= AspxTools.ClientId(aBState) %>'),
                aBZip = $('#<%= AspxTools.ClientId(aBZip) %>'),
                aVaServedAltName = $('#<%= AspxTools.ClientId(aVaServedAltName) %>'),
                aVaClaimNum = $('#<%= AspxTools.ClientId(aVaClaimNum) %>'),
                $aVaOwnOtherVaLoanT = $('#<%= AspxTools.ClientId(aVaOwnOtherVaLoanT) %>'),
                $aVaApplyOneTimeRestorationTri = $('#<%= AspxTools.ClientId(aVaApplyOneTimeRestorationTri) %>'),
                $aVaApplyOneTimeRestorationDateOfLoan = $('#<%= AspxTools.ClientId(aVaApplyOneTimeRestorationDateOfLoan) %>'),
                $aVaApplyOneTimeRestorationStAddr = $('#<%= AspxTools.ClientId(aVaApplyOneTimeRestorationStAddr) %>'),
                $aVaApplyOneTimeRestorationCityState = $('#<%= AspxTools.ClientId(aVaApplyOneTimeRestorationCityState) %>'),
                $aVaApplyRegularRefiCashoutTri = $('#<%= AspxTools.ClientId(aVaApplyRegularRefiCashoutTri) %>'),
                $aVaApplyRegularRefiCashoutDateOfLoan = $('#<%= AspxTools.ClientId(aVaApplyRegularRefiCashoutDateOfLoan) %>'),
                $aVaApplyRegularRefiCashoutStAddr = $('#<%= AspxTools.ClientId(aVaApplyRegularRefiCashoutStAddr) %>'),
                $aVaApplyRegularRefiCashoutCityState = $('#<%= AspxTools.ClientId(aVaApplyRegularRefiCashoutCityState) %>'),
                $aVaApplyRefiNoCashoutTri = $('#<%= AspxTools.ClientId(aVaApplyRefiNoCashoutTri) %>'),
                $aVaApplyRefiNoCashoutDateOfLoan = $('#<%= AspxTools.ClientId(aVaApplyRefiNoCashoutDateOfLoan) %>'),
                $aVaApplyRefiNoCashoutStAddr = $('#<%= AspxTools.ClientId(aVaApplyRefiNoCashoutStAddr) %>'),
                $aVaApplyRefiNoCashoutCityState = $('#<%= AspxTools.ClientId(aVaApplyRefiNoCashoutCityState) %>'),
                $CVaPastL_DateOfLoan0 = $('#<%= AspxTools.ClientId(CVaPastL_DateOfLoan0) %>'),
                $CVaPastL_StreetAddress0 = $('#<%= AspxTools.ClientId(CVaPastL_StreetAddress0) %>'),
                $CVaPastL_CityState0 = $('#<%= AspxTools.ClientId(CVaPastL_CityState0) %>'),
                $CVaPastL_DateOfLoan1 = $('#<%= AspxTools.ClientId(CVaPastL_DateOfLoan1) %>'),
                $CVaPastL_StreetAddress1 = $('#<%= AspxTools.ClientId(CVaPastL_StreetAddress1) %>'),
                $CVaPastL_CityState1 = $('#<%= AspxTools.ClientId(CVaPastL_CityState1) %>'),
                $CVaPastL_DateOfLoan2 = $('#<%= AspxTools.ClientId(CVaPastL_DateOfLoan2) %>'),
                $CVaPastL_StreetAddress2 = $('#<%= AspxTools.ClientId(CVaPastL_StreetAddress2) %>'),
                $CVaPastL_CityState2 = $('#<%= AspxTools.ClientId(CVaPastL_CityState2) %>');


                function syncMailingAddress() {
                    var source = $(<%= AspxTools.JsGetElementById(aBAddrMailSourceT) %>).val();
                    switch(source)
                    {
                        case "0":   // PresentAddress
                            aBAddrMail.val(aBAddr.val()).prop('readonly', true);
                            aBCityMail.val(aBCity.val()).prop('readonly', true);
                            aBStateMail.val(aBState.val()).prop('disabled', true);
                            aBZipMail.val(aBZip.val()).prop('readonly', true);
                            break;
                        case "1":   // SubjectPropertyAddress
                            aBAddrMail.val(<%= AspxTools.JsString(sSpAddr) %>).prop('readonly', true);
                            aBCityMail.val(<%= AspxTools.JsString(sSpCity) %>).prop('readonly', true);
                            aBStateMail.val(<%= AspxTools.JsString(sSpState) %>).prop('disabled', true);
                            aBZipMail.val(<%= AspxTools.JsString(sSpZip) %>).prop('readonly', true);
                            break;
                        default:
                            aBAddrMail.prop('readonly', false);
                            aBCityMail.prop('readonly', false);
                            aBStateMail.prop('disabled', false);
                            aBStateMail.removeAttr('style');
                            aBZipMail.prop('readonly', false);
                            break;
                    }
            }

            function disableIfUnChecked() {
                var len = arguments.length, disable = !this.checked;
                for( var i = 0; i < len; i++ ) {
                    var $field = arguments[i];
                    $field.prop('readonly', disable);
                    if (disable) $field.val("");
                }
            }
            
            aBAddrMailSourceT.change(syncMailingAddress); 
            aBAddr.keypress(syncMailingAddress);
            aBCity.keypress(syncMailingAddress);
            aBState.change(syncMailingAddress);
            aBZip.keypress(syncMailingAddress);

            syncMailingAddress();
            
            $('#<%= AspxTools.ClientId(aVaServedAltNameIs)%>').click(function(){
                disableIfUnChecked.call(this, aVaServedAltName);
            }).triggerHandler('click');
            
            $('#<%= AspxTools.ClientId(aVaDischargedDisabilityIs)%>').click(function(){
                disableIfUnChecked.call(this, aVaClaimNum);
            }).triggerHandler('click');


            $aVaApplyOneTimeRestorationTri.click(function () {
                updateModel({ aVaApplyOneTimeRestorationTri: this.checked });
            })

            $aVaApplyRegularRefiCashoutTri.click(function () {
                updateModel({ aVaApplyRegularRefiCashoutTri: this.checked });
            })

            $aVaApplyRefiNoCashoutTri.click(function () {
                updateModel({ aVaApplyRefiNoCashoutTri: this.checked });
            });

            $aVaOwnOtherVaLoanT.on('click', 'input', function () {
                var radioValue = this.checked ? this.value : $aVaOwnOtherVaLoanT.children('input:radio[name="<%= AspxTools.ClientId(aVaOwnOtherVaLoanT) %>"]:checked').val();
                updateModel({ aVaOwnOtherVaLoanT: radioValue });
                refreshCalculation();
            });
            

            window._postRefreshCalculation = function (result, form) {
                updateModel({
                    aVaOwnOtherVaLoanT: result["aVaOwnOtherVaLoanT"],
                    aVaOwnOtherVaLoanTReadOnly: convertToBoolean(result["aVaOwnOtherVaLoanTReadOnly"]),
                    aVaApplyOtherLoanIsCalculated: convertToBoolean(result["aVaApplyOtherLoanIsCalculated"]),
                    aVaApplyOneTimeRestorationTri: convertToBoolean(result["aVaApplyOneTimeRestorationTri"]),
                    aVaApplyRegularRefiCashoutTri: convertToBoolean(result["aVaApplyRegularRefiCashoutTri"]),
                    aVaApplyRefiNoCashoutTri: convertToBoolean(result["aVaApplyRefiNoCashoutTri"])
                });
            };


            <%-- Initialize the model, first from the ML object, then from the values set to the page via code behind. --%>
            var model = {};
            updateModel({
                aVaOwnOtherVaLoanTReadOnly: ML.aVaOwnOtherVaLoanTReadOnly,
                aVaApplyOtherLoanIsCalculated: ML.aVaApplyOtherLoanIsCalculated
            });
            updateModel({
                aVaOwnOtherVaLoanT: $aVaOwnOtherVaLoanT.children('input:radio[name="<%= AspxTools.ClientId(aVaOwnOtherVaLoanT) %>"]:checked').val(),
                aVaApplyOneTimeRestorationTri: $aVaApplyOneTimeRestorationTri.prop('checked'),
                aVaApplyRegularRefiCashoutTri: $aVaApplyRegularRefiCashoutTri.prop('checked'),
                aVaApplyRefiNoCashoutTri: $aVaApplyRefiNoCashoutTri.prop('checked')
            });

            function updateModel(newValues) {
                var newModel = $.extend({}, model, newValues);
                var diff = diffModels(model, newModel);
                diff.forEach(handleChange);
                model = newModel;
            }
            function diffModels(oldModel, newModel) {
                var diff = [];
                Object.keys(newModel).forEach(function (key, index) {
                    var oldValue = oldModel[key],
                        newValue = newModel[key];
                    if (oldValue !== newValue) {
                        diff.push({ key: key, oldValue: oldValue, newValue: newValue });
                    }
                });
                return diff;
            }

            function handleChange(change) {
                switch (change.key) {
                    case 'aVaOwnOtherVaLoanT':
                        [$CVaPastL_DateOfLoan0, $CVaPastL_StreetAddress0, $CVaPastL_CityState0,
                         $CVaPastL_DateOfLoan1, $CVaPastL_StreetAddress1, $CVaPastL_CityState1,
                         $CVaPastL_DateOfLoan2, $CVaPastL_StreetAddress2, $CVaPastL_CityState2].forEach(change.newValue === <%= AspxTools.JsString(DataAccess.E_aVaOwnOtherVaLoanT.Yes) %> ? enableItem : clearValueAndDisableItem);
                        break;
                    case 'aVaOwnOtherVaLoanTReadOnly':
                        (change.newValue ? disableItem : enableItem)($aVaOwnOtherVaLoanT.children('input'));
                        break;
                    case 'aVaApplyOtherLoanIsCalculated':
                        [$aVaApplyOneTimeRestorationTri, $aVaApplyRegularRefiCashoutTri, $aVaApplyRefiNoCashoutTri].forEach(change.newValue ? disableItem : enableItem);
                        break;
                    case 'aVaApplyOneTimeRestorationTri':
                        [$aVaApplyOneTimeRestorationDateOfLoan, $aVaApplyOneTimeRestorationStAddr, $aVaApplyOneTimeRestorationCityState].forEach(change.newValue ? enableItem : clearValueAndDisableItem);
                        break;
                    case 'aVaApplyRegularRefiCashoutTri':
                        [$aVaApplyRegularRefiCashoutDateOfLoan, $aVaApplyRegularRefiCashoutStAddr, $aVaApplyRegularRefiCashoutCityState].forEach(change.newValue ? enableItem : clearValueAndDisableItem);
                        break;
                    case 'aVaApplyRefiNoCashoutTri':
                        [$aVaApplyRefiNoCashoutDateOfLoan, $aVaApplyRefiNoCashoutStAddr, $aVaApplyRefiNoCashoutCityState].forEach(change.newValue ? enableItem : clearValueAndDisableItem);
                        break;
                    default:
                        console.log('Unhandled change.key="' + change.key + '"');
                }
            }

            function clearValueAndDisableItem($field) {
                disableItem($field.val(''));
            }
            function disableItem($field) {
                var fieldType = $field.prop('type');
                $field.prop('disabled', (fieldType === 'checkbox' || fieldType === 'radio'));
            }
            function enableItem($field) {
                var fieldType = $field.prop('type');
                if (fieldType === 'checkbox' || fieldType === 'radio') {
                    $field.prop('disabled', false);
                } else {
                    $field.prop('readonly', false);
                }
            }

            function convertToBoolean(value) {
                return value === true || value === 'true' || value === 'True';
            }
        });

        function switchApplicant() {
            f_load(<%= AspxTools.JsString(Request.Path + "?loanid=" + LoanID) %>);
        }
    </script>
  </body>
</html>
