<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="VARefiWorksheet.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.VA.VARefiWorksheet" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>VARefiWorksheet</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<script language=javascript>
<!--
    function _init() {
        //disableDDL(<%= AspxTools.JsGetElementById(sVaRefiWsExistingVaLBal) %>, !<%= AspxTools.JsGetElementById(sVaRefiWsExistingVaLBalLckd) %>.checked);
        lockField(<%= AspxTools.JsGetElementById(sVaRefiWsExistingVaLBalLckd) %>, <%= AspxTools.JsGetClientIdString(sVaRefiWsExistingVaLBal) %>);
    }
//-->
</script>
<form id=VARefiWorksheet method=post runat="server">
<table id=Table1 cellSpacing=0 cellPadding=0 border=0>
  <tr>
    <td class=MainRightHeader noWrap>Interest Rate Reduction Refinancing Worksheet (26-8923)</TD></TR>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table2 cellSpacing=0 cellPadding=0 width="98%" 
      border=0>
        <tr>
          <td class="LoanFormHeader FieldLabel" noWrap align=middle colSpan=3 
          >SECTION I - INITIAL COMPUTATION</TD></TR>
        <tr>
          <td class=FieldLabel noWrap></TD>
          <td class=FieldLabel noWrap>Item</TD>
          <td class=FieldLabel noWrap width="100%">Amount</TD></TR>
        <tr>
          <td class=FieldLabel noWrap>1.</td>
          <td class=FieldLabel noWrap>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td class="FieldLabel" style="text-align:left;"><a href="#" onclick="linkMe('../Forms/Loan1003.aspx?pg=0');">Existing VA Loan Balance</a></td>
                <td class="FieldLabel" style="text-align:right;"><asp:checkbox id=sVaRefiWsExistingVaLBalLckd onclick=refreshCalculation(); runat="server" Text="Lock"></asp:checkbox></td>
            </tr>
            </table>
            
          </td>
          <td noWrap><ml:moneytextbox id=sVaRefiWsExistingVaLBal runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>2. </TD>
          <td class=FieldLabel noWrap>Subtract Any Cash Payment From Veterans</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsCashPmtFromVet runat="server" preset="money" width="90" onchange="refreshCalculation();">></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>3. </TD>
          <td class=FieldLabel noWrap>Total</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsExistingVaLBalAfterCashPmt runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class="LoanFormHeader FieldLabel" noWrap align=middle colSpan=3>SECTION II - PRELIMINARY LOAN AMOUNT</TD></TR>
        <tr>
          <td class=FieldLabel noWrap>4.</TD>
          <td class=FieldLabel noWrap>Total from line 3</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsExistingVaLBalAfterCashPmt2 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>5.</TD>
          <td class=FieldLabel noWrap>Add <ml:percenttextbox id=sVaRefiWsDiscntPc runat="server" preset="percent" width="70" onchange="refreshCalculation();">></ml:percenttextbox>Discount 
            based on line 4</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsDiscnt runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>6.</TD>
          <td class=FieldLabel noWrap>Add <ml:percenttextbox id=sVaRefiWsOrigFeePc runat="server" preset="percent" width="70" onchange="refreshCalculation();">></ml:percenttextbox>Origination 
            Fee based on line 4</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsOrigFee runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>7.</TD>
          <td class=FieldLabel noWrap>Add <ml:percenttextbox id="sVaRefiWsFfPc" ReadOnly="true" runat="server" preset="percent" width="70" onchange="refreshCalculation();">></ml:percenttextbox>
          <a href="javascript:linkMe('../LoanInfo.aspx?pg=1');">Funding Fee</a> based on line 4</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsFf runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>8. </TD>
          <td class=FieldLabel noWrap>Add Other Allowable 
            Closing Costs and Prepaids</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsAllowableCcAndPp runat="server" preset="money" width="90" onchange="refreshCalculation();">></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>9. </TD>
          <td class=FieldLabel noWrap>Total</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsPreliminaryTot runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class="LoanFormHeader FieldLabel" noWrap align=middle colSpan=3 
          >SECTION III - FINAL COMPUTATION</TD></TR>
        <tr>
          <td class=FieldLabel noWrap>10. </TD>
          <td class=FieldLabel noWrap>Total from line 
9</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsPreliminaryTot2 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>11.</TD>
          <td class=FieldLabel noWrap>Add <ml:percenttextbox id=sVaRefiWsFinalDiscntPc runat="server" preset="percent" width="70" onchange="refreshCalculation();">></ml:percenttextbox>Discount 
            based on line 10</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsFinalDiscnt runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>12.</TD>
          <td class=FieldLabel noWrap>Subtotal</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsFinalSubtotalItem12 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>13.</TD>
          <td class=FieldLabel noWrap>Subtract amount 
            shown on line 5</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsDiscnt2 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>14.</TD>
          <td class=FieldLabel noWrap>Subtotal</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsFinalSubtotalItem14 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>15.</TD>
          <td class=FieldLabel noWrap>Subtract amount 
            shown on line 7</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsFf2 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>16.</TD>
          <td class=FieldLabel noWrap>Subtotal</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsFinalSubtotalItem16 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>17.</TD>
          <td class=FieldLabel noWrap>Add <ml:percenttextbox id=sVaRefiWsFfPc2 runat="server" preset="percent" width="70" readonly="True"></ml:percenttextbox>Funding 
            Fee based on line 16</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsFinalFf runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>18.</TD>
          <td class=FieldLabel noWrap>Total Maximum Loan 
            Amount</TD>
          <td noWrap><ml:moneytextbox id=sVaRefiWsMaxLAmt runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR></TABLE></TD></TR>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table3 cellSpacing=0 cellPadding=0 width="98%" 
      border=0>
        <tr>
          <td class=FieldLabel noWrap>Date</TD>
          <td noWrap><ml:datetextbox id=VA26_8928LenderPrepareDate runat="server" preset="date" width="75"></ml:datetextbox></TD>
          <td class=FieldLabel noWrap></TD>
          <td noWrap></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>Name Of Lender</TD>
          <td noWrap><asp:textbox id=VA26_8928LenderCompanyName runat="server"></asp:textbox></TD>
          <td class=FieldLabel noWrap>Title of Officer</TD>
          <td noWrap><asp:textbox id=VA26_8928LenderTitle runat="server"></asp:textbox></TD></TR></TABLE></TD></TR></TABLE></FORM>
	
  </body>
</html>
