<%@ Page Language="c#" CodeBehind="VALoanAnalysis.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.VA.VALoanAnalysis" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>VALoanAnalysis</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</head>
<body MS_POSITIONING="FlowLayout" class="RightBackground">

    <script language="javascript">
  <!--
  function _init() {

    lockField(<%= AspxTools.JsGetElementById(sVaCashdwnPmtLckd) %>, 'sVaCashdwnPmt');
    lockField(<%= AspxTools.JsGetElementById(sVaProThisMPmtLckd) %>, 'sVaProThisMPmt');
    lockField(<%= AspxTools.JsGetElementById(sVaProRealETxLckd) %>, 'sVaProRealETx');
    lockField(<%= AspxTools.JsGetElementById(sVaProHazInsLckd) %>, 'sVaProHazIns');
    lockField(<%= AspxTools.JsGetElementById(aVaCEmplmtILckd) %>, 'aVaCEmplmtI');
    lockField(<%= AspxTools.JsGetElementById(aVaBEmplmtILckd) %>, 'aVaBEmplmtI');
    lockField(<%= AspxTools.JsGetElementById(aVaCONetILckd) %>, 'aVaCONetI');
    lockField(<%= AspxTools.JsGetElementById(aVaBONetILckd) %>, 'aVaBONetI');
    lockField(<%= AspxTools.JsGetElementById(sVaMaintainAssessPmtLckd) %>, 'sVaMaintainAssessPmt');
  }
  //-->
    </script>

    <form id="VALoanAnalysis" method="post" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td nowrap class="MainRightHeader">VA Loan Analysis (26-6393)</td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table2" cellspacing="0" cellpadding="0" width="98%" border="0" class="InsetBorder">
                    <tr>
                        <td nowrap colspan="2" class="LoanFormHeader FieldLabel" align="middle">SECTION A - 
                            LOAN DATA</td>
                    </tr>
                    <tr>
                        <td nowrap colspan="2">
                            <table id="Table3" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td nowrap colspan="5" class="FieldLabel">1. Name of Borrower(s)</td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>Borrower First Name</td>
                                    <td class="FieldLabel" nowrap><asp:TextBox id="aBFirstNm" runat="server"></asp:TextBox></td>
                                    <td nowrap width="20"></td>
                                    <td class="FieldLabel" nowrap>Coborrower First Name</td>
                                    <td nowrap><asp:TextBox id="aCFirstNm" runat="server" tabindex=5></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>Borrower Middle Name</td>
                                    <td class="FieldLabel" nowrap><asp:TextBox id="aBMidNm" runat="server"></asp:TextBox></td>
                                    <td nowrap></td>
                                    <td class="FieldLabel" nowrap>Coborrower Middle Name</td>
                                    <td nowrap><asp:TextBox id="aCMidNm" runat="server" tabindex=5></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>Borrower Last Name</td>
                                    <td class="FieldLabel" nowrap><asp:TextBox id="aBLastNm" runat="server"></asp:TextBox></td>
                                    <td nowrap></td>
                                    <td class="FieldLabel" nowrap>Coborrower Last Name</td>
                                    <td nowrap><asp:TextBox id="aCLastNm" runat="server" tabindex=5></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>Borrower Suffix</td>
                                    <td class="FieldLabel" nowrap><ml:ComboBox id="aBSuffix" runat="server"></ml:ComboBox></td>
                                    <td nowrap></td>
                                    <td class="FieldLabel" nowrap>Coborrower Suffix</td>
                                    <td nowrap><ml:ComboBox id="aCSuffix" runat="server" tabindex=5></ml:ComboBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>2. Amount of Loan</td>
                        <td nowrap width="80%">
                            <ml:MoneyTextBox id="sFinalLAmt" runat="server" width="90" preset="money" readonly="True"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel">3. Cash Down Payment</td>
                        <td nowrap class=FieldLabel><asp:CheckBox id=sVaCashdwnPmtLckd runat="server" Text="Lock" onclick="refreshCalculation();" tabindex=5></asp:CheckBox><ml:MoneyTextBox id="sVaCashdwnPmt" runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=5></ml:MoneyTextBox></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table4" cellspacing="0" cellpadding="0" border="0" class="InsetBorder" width="98%">
                    <tr>
                        <td nowrap colspan="4" class="LoanFormHeader FieldLabel" align="middle">SECTION B - 
                            BORROWER'S PERSONAL AND FINANCIAL STATUS</td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel">4. Applicant's Age</td>
                        <td nowrap><asp:TextBox id="aBAge" runat="server" Width="44px" tabindex=10></asp:TextBox></td>
                        <td nowrap class="FieldLabel">5. Occupation of Applicant</td>
                        <td nowrap><asp:TextBox id="aBPrimaryEmpJobTitle" runat="server" tabindex=10></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel">
                            6. Years at Present Employment</td>
                        <td nowrap class=FieldLabel><asp:TextBox id="aBPrimaryEmpEmplmtLenInYrs" runat="server" Width="32px" readonly="True"></asp:TextBox>Yrs 
                            <asp:TextBox id=aBPrimaryEmpEmplmtLenInMonths runat="server" Width="32px" ReadOnly="True"></asp:TextBox>&nbsp;Mths</td>
                        <td nowrap class="FieldLabel">7. Liquid Assets</td>
                        <td nowrap><ml:MoneyTextBox id="aAsstLiqTot" runat="server" width="90" preset="money" readonly="True"></ml:MoneyTextBox></td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel">8. Current Monthly Housing Expense</td>
                        <td nowrap><ml:MoneyTextBox id="aPresTotHExp" runat="server" width="90" preset="money" readonly="True"></ml:MoneyTextBox></td>
                        <td nowrap class="FieldLabel">9. Utilities Included</td>
                        <td nowrap>
                            <asp:CheckBoxList id="aVaUtilityIncludedTri" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" tabindex=10>
                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                <asp:ListItem Value="2">No</asp:ListItem>
                            </asp:CheckBoxList></td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel">10. Spouse's Age
                        </td>
                        <td nowrap><asp:TextBox id="aCAge" runat="server" Width="44px" tabindex=10></asp:TextBox></td>
                        <td nowrap class="FieldLabel">11.&nbsp; Occupation of Spouse</td>
                        <td nowrap><asp:TextBox id="aCPrimaryEmpJobTitle" runat="server" tabindex=10></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel">
                            12. Years at Present Employment</td>
                        <td nowrap class=FieldLabel><asp:TextBox id="aCPrimaryEmpEmplmtLenInYrs" runat="server" Width="32px" readonly="True"></asp:TextBox>&nbsp;Yrs 
                            <asp:TextBox id=aCPrimaryEmpEmplmtLenInMonths runat="server" Width="32px" ReadOnly="True"></asp:TextBox>&nbsp;Mths</td>
                        <td nowrap class="FieldLabel">13. Age of Dependents</td>
                        <td nowrap><asp:TextBox id="aBDependAges" runat="server" tabindex=10></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td nowrap></td>
                        <td nowrap></td>
                        <td nowrap></td>
                        <td nowrap></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table5" cellspacing="0" cellpadding="0" width="98%" border="0" class="InsetBorder">
                    <tr>
                        <td nowrap class="LoanFormHeader FieldLabel" align="middle" colspan="3">SECTION C. ESTIMATED MONTHLY SHELTER EXPENSES</td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>14. Term of Loan</td>
                        <td></td>
                        <td nowrap width="100%"><asp:TextBox id="sTermInYr" runat="server" Width="71px" readonly="True"></asp:TextBox>&nbsp;yrs</td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>15. Mortgage Payment @
                            <ml:PercentTextBox id="sNoteIR" runat="server" preset="percent" width="70" onchange="refreshCalculation();" tabindex=15></ml:PercentTextBox>
                        </td>
                        <td><asp:CheckBox id=sVaProThisMPmtLckd runat="server" text="Lock" onclick="refreshCalculation();" tabindex=15></asp:CheckBox></td>
                        <td nowrap><ml:MoneyTextBox id="sVaProThisMPmt" runat="server" width="90" preset="money" ReadOnly="True" onchange="refreshCalculation();" tabindex=15></ml:MoneyTextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>16. Realty Taxes</td>
                        <td><asp:CheckBox id=sVaProRealETxLckd runat="server" text="Lock"  onclick="refreshCalculation();" tabindex=15></asp:CheckBox></td>

                        <td nowrap><ml:MoneyTextBox id="sVaProRealETx" runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=15></ml:MoneyTextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>17. Hazard Insurance</td>
                        <td><asp:CheckBox id=sVaProHazInsLckd runat="server" text="Lock" onclick="refreshCalculation();" tabindex=15></asp:CheckBox></td>

                        <td nowrap><ml:MoneyTextBox id="sVaProHazIns" runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=15></ml:MoneyTextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>18. Special Assessments</td>
                        <td></td>

                        <td nowrap><ml:MoneyTextBox id="sVaSpecialAssessPmt" runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=15></ml:MoneyTextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>19. Maintenance</td>
                        <td></td>

                        <td nowrap><ml:MoneyTextBox id="sVaProMaintenancePmt" runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=15></ml:MoneyTextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Utilities (including heat)</td>
                        <td></td>

                        <td nowrap><ml:MoneyTextBox id="sVaProUtilityPmt" runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=15></ml:MoneyTextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total of Maintenance and Utilities
                        </td>
                        <td></td>
                        <td nowrap>
                            <ml:MoneyTextBox id="sVaProMaintenanceAndUtilityPmt" runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>20. Other (HOA, Condo fees, etc.)</td>
                        <td>
                            <asp:CheckBox runat="server" ID="sVaMaintainAssessPmtLckd" text="Lock" onclick="refreshCalculation();" />
                        </td>

                        <td nowrap><ml:MoneyTextBox id="sVaMaintainAssessPmt" runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=15></ml:MoneyTextBox></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>21. Total</td>
                        <td></td>

                        <td nowrap><ml:MoneyTextBox id="sVaProMonthlyPmt" runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table6" cellspacing="0" cellpadding="0" width="98%" border="0" class="InsetBorder">
                    <tr>
                        <td nowrap class="LoanFormHeader FieldLabel" align="middle" colspan="7">SECTION D - 
                            DEBTS AND OBLIGATION</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                        <td class="FieldLabel" nowrap valign="bottom" width="100px"><a href="#" onclick="redirectToUladPage('Liabilities');" tabindex=-1>Liabilities</a></td>
                        <td>&nbsp;</td>
                        <td class="FieldLabel" valign="bottom" width="100px"><a href="#" onclick="redirectToUladPage('REO');" tabindex=-1>Negative REO Cash Flow</a></td>
                        <td>&nbsp;</td>
                        <td class="FieldLabel" valign="bottom">Monthly Total</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>30.&nbsp; Monthly Payment Total</td>
                        <td nowrap><ml:MoneyTextBox id="aVaLiaMonTotExisting" runat="server" width="90" preset="money" ReadOnly="True" /></td>
                        <td align="center">+</td>
                        <td nowrap><ml:MoneyTextBox ID="aOpNegCf" runat="server" Width="90" preset="money" ReadOnly="true" /></td>
                        <td align="center">=</td>
                        <td nowrap><ml:MoneyTextBox ID="aVaLiaMonTotExistingAndOpNegCf" runat="server" Width="90" preset="money" ReadOnly="true" /></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Unpaid Balance Total</td>
                      <td nowrap><ml:MoneyTextBox id="aVaLiaBalTotExisting" runat="server" width="90" preset="money" ReadOnly="True" /></td>
                      <td nowrap></td>
                      <td nowrap></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table7" cellspacing="0" cellpadding="0" width="98%" border="0" class="InsetBorder">
                    <tr>
                        <td nowrap colspan="4" class="LoanFormHeader FieldLabel" align="middle">SECTION E - 
                            MONTHLY INCOME AND DEDUCTIONS</td>
                    </tr>
                    <tr>
                        <td nowrap colspan="4">
                            <table id="Table8" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td nowrap></td>
                                    <td nowrap class="FieldLabel">Items</td>
                                    <td nowrap></td>
                                  <td></td>                                            
                                    <td nowrap class="FieldLabel">Spouse</td>
                                    <td></td>
                                    <td nowrap class="FieldLabel">Borrower</td>
                                    <td nowrap width="100%" class="FieldLabel">Total</td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>31. &nbsp;</td>
                                    <td class="FieldLabel" nowrap colspan="2">Gross Salary or Earnings From Employment</td>
                                    <td><asp:CheckBox id=aVaCEmplmtILckd runat="server" onclick="refreshCalculation();" tabindex=25></asp:CheckBox></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaCEmplmtI" runat="server" width="80px" preset="money" onchange="refreshCalculation();" tabindex=25></ml:MoneyTextBox></td>
                                    <td><asp:CheckBox id=aVaBEmplmtILckd runat="server" onclick="refreshCalculation();" tabindex=30></asp:CheckBox></td>
                                    <td nowrap><ml:MoneyTextBox id=aVaBEmplmtI runat="server" preset="money" width="80px" onchange="refreshCalculation();" tabindex=30></ml:MoneyTextBox></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaTotEmplmtI" runat="server" width="80px" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>32.</td>
                                    <td class="FieldLabel" valign="center" nowrap align="middle" rowSpan="5" style="BORDER-RIGHT: 2px inset; BORDER-TOP: 2px inset; BORDER-LEFT: 2px inset; MARGIN-RIGHT: 5px; BORDER-BOTTOM: 2px inset">Deductions</td>
                                    <td class="FieldLabel" nowrap>Federal Income Tax</td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaCFedITax" runat="server" width="80px" preset="money" onchange="refreshCalculation();" tabindex=25></ml:MoneyTextBox></td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaBFedITax" runat="server" width="80px" preset="money" onchange="refreshCalculation();" tabindex=30></ml:MoneyTextBox></td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>33.</td>
                                    <td class="FieldLabel" nowrap>State Income Tax</td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaCStateITax" runat="server" width="80px" preset="money" onchange="refreshCalculation();" tabindex=25></ml:MoneyTextBox></td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaBStateITax" runat="server" width="80px" preset="money" onchange="refreshCalculation();" tabindex=30></ml:MoneyTextBox></td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>34.</td>
                                    <td class="FieldLabel" nowrap>Retirement or Social Security</td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaCSsnTax" runat="server" width="80px" preset="money" onchange="refreshCalculation();" tabindex=25></ml:MoneyTextBox></td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaBSsnTax" runat="server" width="80px" preset="money" onchange="refreshCalculation();" tabindex=30></ml:MoneyTextBox></td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>35.</td>
                                    <td class="FieldLabel" nowrap>Other (Specify)</td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaCOITax" runat="server" width="80px" preset="money" onchange="refreshCalculation();" tabindex=25></ml:MoneyTextBox></td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaBOITax" runat="server" width="80px" preset="money" onchange="refreshCalculation();" tabindex=30></ml:MoneyTextBox></td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>36.</td>
                                    <td class="FieldLabel" nowrap>Total Deductions</td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaCTotITax" runat="server" width="81" preset="money" readonly="True" tabindex=25></ml:MoneyTextBox></td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaBTotITax" runat="server" width="80px" preset="money" readonly="True" tabindex=30></ml:MoneyTextBox></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaTotITax" runat="server" width="80px" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>37.</td>
                                    <td class="FieldLabel" nowrap colspan="2">Net Take-Home Pay</td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaCTakehomeEmplmtI" runat="server" width="80px" preset="money" readonly="True" tabindex=25></ml:MoneyTextBox></td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaBTakehomeEmplmtI" runat="server" width="80px" preset="money" readonly="True" tabindex=30></ml:MoneyTextBox></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaTakehomeEmplmtI" runat="server" width="80px" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>38.</td>
                                    <td class="FieldLabel" nowrap colspan="2">Pension, Compensation or Other Net Income</td>
                                    <td></td>
                                    <td nowrap></td>
                                    <td></td>
                                    <td nowrap></td>
                                    <td nowrap></td>
                                </tr>
                                <tr>
                                    <td class=FieldLabel nowrap></td>
                                    <td class=FieldLabel nowrap colspan=2>&nbsp;&nbsp;&nbsp; 
                                      Other: <asp:TextBox id=aVaONetIDesc runat="server" Width="197px" tabindex=25></asp:TextBox></td>
                                    <td><asp:CheckBox id=aVaCONetILckd runat="server" onclick="refreshCalculation();" tabindex=25></asp:CheckBox></td>
                                    <td nowrap><ml:MoneyTextBox id=aVaCONetI runat="server" preset="money" width="80px" onchange="refreshCalculation();" tabindex=25></ml:MoneyTextBox></td>
                                    <td><asp:CheckBox id=aVaBONetILckd runat="server" onclick="refreshCalculation();" tabindex=30></asp:CheckBox></td>
                                    <td nowrap><ml:MoneyTextBox id=aVaBONetI runat="server" preset="money" width="80px" onchange="refreshCalculation();" tabindex=30></ml:MoneyTextBox></td>
                                    <td nowrap><ml:MoneyTextBox id=aVaONetI runat="server" preset="money" width="80px" ReadOnly="True"></ml:MoneyTextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>39.</td>
                                    <td class="FieldLabel" nowrap colspan="2">Total (Sum of lines 37 and 38)</td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaCNetI" runat="server" width="80px" preset="money" readonly="True" tabindex=25></ml:MoneyTextBox></td>
                                    <td></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaBNetI" runat="server" width="80px" preset="money" readonly="True"></ml:MoneyTextBox></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaNetI" runat="server" width="80px" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>40.</td>
                                    <td class="FieldLabel" nowrap colspan="6">Less Those Obligations Listed in Section D Which Should Be Deducted From Income</td>
                                    <td nowrap><ml:MoneyTextBox id="aVaLiaMonTotDeducted" runat="server" width="80px" preset="money" readonly="True"></ml:MoneyTextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>41.</td>
                                    <td class="FieldLabel" nowrap colspan="6">Total Net Effective Income</td>
                                    <td nowrap><ml:MoneyTextBox id="aVaNetEffectiveI" runat="server" width="80px" preset="money" readonly="True"></ml:MoneyTextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>42.</td>
                                    <td class="FieldLabel" nowrap colspan="6">Less Estimated Monthly Shelter Expense 
                                        (Line 21)</td>
                                    <td nowrap><ml:MoneyTextBox id="sVaProMonthlyPmt2" runat="server" width="80px" preset="money" readonly="True"></ml:MoneyTextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>43.</td>
                                    <td class="FieldLabel" nowrap colspan="2">Balance Available for Family Support</td>
                                    <td nowrap class="FieldLabel" align="right" colspan=2>Guideline</td>
                                    <td nowrap colspan=2><ml:MoneyTextBox id="aVaFamilySuportGuidelineAmt" runat="server" width="80px" preset="money" tabindex=35></ml:MoneyTextBox></td>
                                    <td nowrap><ml:MoneyTextBox id="aVaFamilySupportBal" runat="server" width="80px" preset="money" readonly="True"></ml:MoneyTextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>44.</td>
                                    <td class="FieldLabel" nowrap colspan="6">Ratio (Sum of Items 15, 16, 17, 18, 20, 
                                        40&nbsp; / sum of items 31 and 38)</td>
                                    <td nowrap><ml:PercentTextBox id="aVaRatio" runat="server" width="70" preset="percent" readonly="True"></ml:PercentTextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>45.</td>
                                    <td class="FieldLabel" nowrap colspan="7">Past Credit Record
                                    <asp:CheckBoxList id="aVaCrRecordSatisfyTri" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" tabindex=40>
                                        <asp:ListItem Value="1">Satisfactory</asp:ListItem>
                                        <asp:ListItem Value="2">Unsatisfactory</asp:ListItem>
                                    </asp:CheckBoxList></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>46.</td>
                                    <td class="FieldLabel" nowrap colspan="7">Does Loan Meet VA Credit Standards?
                                        <asp:CheckBoxList id="aVaLMeetCrStandardTri" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" tabindex=40>
                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                            <asp:ListItem Value="2">No</asp:ListItem>
                                        </asp:CheckBoxList></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap></td>
                                    <td class="FieldLabel" nowrap colspan="7">Borrower CAIVR #
                                        <asp:TextBox id="aFHABCaivrsNum" runat="server" tabindex=40></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap></td>
                                    <td class="FieldLabel" nowrap colspan="7">Coborrower CAIVR #
                                        <asp:TextBox id="aFHACCaivrsNum" runat="server" tabindex=40></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>47.</td>
                                    <td class="FieldLabel" nowrap colspan="7">Remarks</td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap></td>
                                    <td class="FieldLabel" nowrap colspan="7"><asp:TextBox id="aVaLAnalysisN" runat="server" Width="502px" TextMode="MultiLine" Height="109px" tabindex=40></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table9" cellspacing="0" cellpadding="0" width="98%" border="0" class="InsetBorder">
                    <tr>
                        <td nowrap colspan="4" class="LoanFormHeader FieldLabel" align="middle">
                            CRV DATA (VA USE)
                        </td>
                    </tr>
                    <tr>
                        <td nowrap colspan="4">
                            <table id="Table10" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        48a. Value
                                    </td>
                                    <td>
                                        <ml:MoneyTextBox id="aVALAnalysisValue" runat="server" width="90" preset="money"></ml:MoneyTextBox>
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        48b. Expiration Date
                                    </td>
                                    <td>
                                         <ml:DateTextBox ID="aVALAnalysisExpirationD" runat="server"></ml:DateTextBox>
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        48c. Economic Life
                                    </td>
                                    <td>
                                        <asp:TextBox id="aVALAnalysisEconomicLifeYrs" runat="server" Width="71px"></asp:TextBox> yrs
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="SectionF" cellspacing="0" cellpadding="0" width="98%" border="0" class="InsetBorder">
                    <tr>
                        <td nowrap colspan="4" class="LoanFormHeader FieldLabel" align="middle">
                            SECTION F - DISPOSITION OF APPLICATION AND UNDERWRITER CERTIFICATION
                        </td>
                    </tr>
                    <tr>
                        <td nowrap colspan="4">
                            <table id="SectionFSub" cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBoxList ID="sVaRecommendAppApprovedT" runat="server" RepeatDirection=Vertical
                                            RepeatLayout="Flow">
                                            <asp:ListItem Value="1">Recommend that the application be approved since it meets all requirements of Chapter 37, Title 38, U.S. Code and applicable VA Regulations and directives.</asp:ListItem>
                                            <asp:ListItem Value="2">Recommend that the application be disapproved for the reasons stated under "Remarks" above.</asp:ListItem>
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" style="width: 30px;">
                                        51. Final Action
                                    </td>
                                    <td class="FieldLabel">
                                        <asp:CheckBoxList ID="sVAFinalActionT" runat="server" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow">
                                            <asp:ListItem Value="1">Approve Application</asp:ListItem>
                                            <asp:ListItem Value="2">Reject Application</asp:ListItem>
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>    
    </table>
    </form>
</body>
</html>
