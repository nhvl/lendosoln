﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.VA
#endregion
{
    using DataAccess;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>
    /// New VA Loan Comparison page.
    /// </summary>
    public partial class VALoanComparison2018 : BaseLoanPage
    {
        /// <summary>
        /// Page load method.
        /// </summary>
        /// <param name="sender">The object sender.</param>
        /// <param name="e">The event argument e.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoanWithArchive = CPageData.CreateUsingSmartDependency(LoanID, typeof(VALoanComparison2018));
            CPageData dataLoan = null;
            dataLoanWithArchive.InitLoad();

            var initialLEDates = dataLoanWithArchive.sLoanEstimateDatesInfo.InitialLoanEstimate;
            if (initialLEDates != null)
            {
                dataLoanWithArchive.ApplyClosingCostArchive(dataLoanWithArchive.sClosingCostArchive.FirstOrDefault(a => a.Id == initialLEDates.ArchiveId));
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VALoanComparison2018));
                dataLoan.InitLoad();
                dataLoan.ApplyClosingCostArchive(null);
            }
            else
            {
                dataLoanWithArchive.ApplyClosingCostArchive(null);
                dataLoan = dataLoanWithArchive;
            }

            // Existing Loan
            sVaLoanNumCurrentLoan.Text = dataLoan.sVaLoanNumCurrentLoan;
            sVaLoanAmtCurrentLoan.Text = dataLoan.sVaLoanAmtCurrentLoan_rep;
            sVaNoteIrCurrentLoan.Text = dataLoan.sVaNoteIrCurrentLoan_rep;
            sVaTermCurrentLoan.Text = dataLoan.sVaTermCurrentLoan_rep;
            sVaPICurrentLoan.Text = dataLoan.sVaPICurrentLoan_rep;
            sVaMonthlyPmtCurrentLoan.Text = dataLoan.sVaMonthlyPmtCurrentLoan_rep;
            
            // Initial Proposed Loan
            sAgencyCaseNum_2.Text = dataLoanWithArchive.sAgencyCaseNum;
            sVaInitialStatementLoanAmt.Text = dataLoanWithArchive.sFinalLAmt_rep;
            sVaInitialStatementNoteIR.Text = dataLoanWithArchive.sRateAmortT;
            sVaInitialStatementTerm.Text = dataLoanWithArchive.sTerm_rep;
            sVaInitialStatementPI.Text = dataLoanWithArchive.sProThisMPmt_rep;
            sVaInitialStatementMonthlyPmt.Text = dataLoanWithArchive.sMonthlyPmt_rep;

            // Final Proposed Loan
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sRateAmortT.Text = dataLoan.sRateAmortT;
            sTerm.Text = dataLoan.sTerm_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
            sMonthlyPmt.Text = dataLoan.sMonthlyPmt_rep;

            // Initial Statement
            sVaInitialOriginationCharge.Text = dataLoanWithArchive.sVaInitialOriginationCharge_rep;
            sVaInitialServYouCannotShopFor.Text = dataLoanWithArchive.sVaInitialServYouCannotShopFor_rep;
            sVaInitialFundingFee.Text = dataLoanWithArchive.sVaInitialFundingFee_rep;
            sVaInitialServYouCanShopFor.Text = dataLoanWithArchive.sVaInitialServYouCanShopFor_rep;
            sVaInitialTaxesOtherGovFee.Text = dataLoanWithArchive.sVaInitialTaxesOtherGovFee_rep;
            sVaInitialOtherFee.Text = dataLoanWithArchive.sVaInitialOtherFee_rep;
            sTRIDLoanEstimateLenderCredits.Text = dataLoanWithArchive.sTRIDLoanEstimateLenderCredits_rep;
            sVaInitialTotalClosingCost.Text = dataLoanWithArchive.sVaInitialTotalClosingCost_rep;
            sVaInitialLoanCompMonthlyPmtDecreaseAmt.Text = dataLoanWithArchive.sVaInitialLoanCompMonthlyPmtDecreaseAmt_rep;
            sVaInitialLoanCompRecoupCostsTime.Text = Tools.IsStringEmptyOrNegativeInt(dataLoanWithArchive.sVaInitialLoanCompRecoupCostsTime_rep) ? "N/A" : dataLoanWithArchive.sVaInitialLoanCompRecoupCostsTime_rep;

            // Final Statement
            sVaFinalOriginationCharge.Text = dataLoan.sVaFinalOriginationCharge_rep;
            sVaFinalServYouDidNotShopFor.Text = dataLoan.sVaFinalServYouDidNotShopFor_rep;
            sVaFinalFundingFee.Text = dataLoan.sVaFinalFundingFee_rep;
            sVaFinalServYouDidShopFor.Text = dataLoan.sVaFinalServYouDidShopFor_rep;
            sVaFinalTaxesOtherGovFee.Text = dataLoan.sVaFinalTaxesOtherGovFee_rep;
            sVaFinalOtherFee.Text = dataLoan.sVaFinalOtherFee_rep;
            sTRIDClosingDisclosureGeneralLenderCredits.Text = dataLoan.sTRIDClosingDisclosureGeneralLenderCredits_rep;
            sVaFinalTotalClosingCost.Text = dataLoan.sVaFinalTotalClosingCost_rep;
            sVaLoanCompMonthlyPmtDecreaseAmt.Text = dataLoan.sVaLoanCompMonthlyPmtDecreaseAmt_rep;
            sVaFinalLoanCompRecoupCostsTime.Text = Tools.IsStringEmptyOrNegativeInt(dataLoan.sVaFinalLoanCompRecoupCostsTime_rep) ? "N/A" : dataLoan.sVaFinalLoanCompRecoupCostsTime_rep;            

            // checkbox at the end
            sVaNewPmtQualified.Checked = dataLoan.sVaNewPmtQualified;
        }

        /// <summary>
        /// Page init method.
        /// </summary>
        /// <param name="e">Event args.</param>
        protected override void OnInit(EventArgs e)
        {
            this.PageTitle = "VA Loan Comparison";
            this.PageID = "VALoanComparison2018";
            this.UseNewFramework = true;
            this.RegisterCSS("stylesheetnew.css");
            this.PDFPrintClass = typeof(LendersOffice.Pdf.VALoanComparison2018InitialPDF);

            base.OnInit(e);
        }
    }
}