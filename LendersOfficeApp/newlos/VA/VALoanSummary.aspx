<%@ Page language="c#" Codebehind="VALoanSummary.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.VA.VALoanSummary" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>VALoanSummary</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
<style type="text/css">
  .WarningLabelStyle
  { BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: bold; PADDING-BOTTOM: 5px; MARGIN: 5px; BORDER-LEFT: black 1px solid; COLOR: red; PADDING-TOP: 5px; BORDER-BOTTOM: black 1px solid; BACKGROUND-COLOR: white }
</style>
</head>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<script language=javascript>
<!--
function _init() {

  disableDDL(<%= AspxTools.JsGetElementById(sVaLPurposeT) %>, !<%= AspxTools.JsGetElementById(sVaLPurposeTLckd) %>.checked);
  disableDDL(<%= AspxTools.JsGetElementById(sVaLCodeT) %>, !<%= AspxTools.JsGetElementById(sVaLCodeTLckd) %>.checked);
  disableDDL(<%= AspxTools.JsGetElementById(sVaFinMethT) %>, !<%= AspxTools.JsGetElementById(sVaFinMethTLckd) %>.checked);
  disableDDL(<%= AspxTools.JsGetElementById(sVaHybridArmT) %>, <%= AspxTools.JsGetElementById(sVaFinMethT) %>.value != <%= AspxTools.JsString(E_sVaFinMethT.HybridArm) %>);
  
  lockField(<%= AspxTools.JsGetElementById(sVaVetMedianCrScoreLckd) %>, <%= AspxTools.JsGetClientIdString(sVaVetMedianCrScore) %>);
  lockField(<%= AspxTools.JsGetElementById(sVaLDiscntLckd) %>, <%= AspxTools.JsGetClientIdString(sVaLDiscnt) %>);
  lockField(<%= AspxTools.JsGetElementById(sVaLDiscntLckd) %>, <%= AspxTools.JsGetClientIdString(sVaLDiscntPc) %>);
  lockField(<%= AspxTools.JsGetElementById(sVaLDiscntPbbLckd) %>, <%= AspxTools.JsGetClientIdString(sVaLDiscntPbb) %>);
  lockField(<%= AspxTools.JsGetElementById(sVaLDiscntPbbLckd) %>, <%= AspxTools.JsGetClientIdString(sVaLDiscntPcPbb) %>);
  lockField(<%= AspxTools.JsGetElementById(aVaCEmplmtILckd) %>, <%= AspxTools.JsGetClientIdString(aVaCEmplmtI) %>);
  lockField(<%= AspxTools.JsGetElementById(sLenderCaseNumLckd) %>, <%= AspxTools.JsGetClientIdString(sLenderCaseNum) %>);
}

function f_displayWarning(id, bVisible, o) {
    document.getElementById(id).style.display = bVisible ? "" : "none";
    if (typeof o != 'undefined') {
        try { o.focus(); } catch (e) { }
    }
}
//-->
</script>

<form id=VALoanSummary method=post runat="server">
<table id=Table1 cellSpacing=0 cellPadding=0 border=0>
  <tr>
    <td class=MainRightHeader noWrap>VA Loan Summary 
      Sheet (VA 26-0286)</td></tr>
  <tr>
    <td noWrap>
      <table class=InsetBorder id=Table2 cellSpacing=0 cellPadding=0 width="98%" 
      border=0>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>1. VA's 
            12-digit Loan Number</td>
          <td noWrap width="90%"><asp:textbox id=sAgencyCaseNum runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>2. 
            Veteran First Name</td>
          <td noWrap><asp:textbox id=aBFirstNm runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2 
            >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            Middle Name</td>
          <td noWrap><asp:textbox id=aBMidNm runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2 
            >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            Last Name</td>
          <td noWrap><asp:textbox id=aBLastNm runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2 
            >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Suffix</td>
          <td noWrap><ml:ComboBox id=aBSuffix runat="server"></ml:ComboBox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>3. Social 
            Security Number</td>
          <td noWrap><ml:ssntextbox id=aBSsn runat="server" width="75px" preset="ssn"></ml:ssntextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>4. 
          Gender</td>
          <td noWrap><asp:dropdownlist id=aBGenderT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>5. Date 
            of Birth</td>
          <td noWrap><ml:datetextbox id=aBDob runat="server" width="75" preset="date"></ml:datetextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>6A. 
            Ethnicity</td>
          <td noWrap><asp:dropdownlist id=aBHispanicT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap colSpan=2 
            >6B. Race</td>
          <td noWrap>
            <table id=Table3 cellSpacing=0 cellPadding=0 border=0 
            >
              <tr>
                <td noWrap><asp:checkbox id=aBIsAmericanIndian runat="server" Text="American Indian or Alaska Native"></asp:checkbox></td></tr>
              <tr>
                <td noWrap><asp:checkbox id=aBIsAsian runat="server" Text="Asian"></asp:checkbox></td></tr>
              <tr>
                <td noWrap><asp:checkbox id=aBIsBlack runat="server" Text="Black or African American"></asp:checkbox></td></tr>
              <tr>
                <td noWrap><asp:checkbox id=aBIsPacificIslander runat="server" Text="Native Hawaiian or Other Pacific Islander"></asp:checkbox></td></tr>
              <tr>
                <td noWrap><asp:checkbox id=aBIsWhite runat="server" Text="White"></asp:checkbox></td></tr></table></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>7. 
            Entitlement Code</td>
          <td noWrap><ml:combobox id=aVaEntitleCode runat="server"></ml:combobox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>8. Amount 
            of Entitlement Available</td>
          <td noWrap><ml:moneytextbox id=aVaEntitleAmt runat="server" width="90" preset="money"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>9. Branch 
            of Service </td>
          <td noWrap><asp:dropdownlist id=aVaServiceBranchT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>10. 
            Military Status</td>
          <td noWrap><asp:dropdownlist id=aVaMilitaryStatT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>11. First 
            Time Home Buyer</td>
          <td noWrap><asp:checkboxlist id=aVaIsVeteranFirstTimeBuyerTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:checkboxlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>12. Loan 
            Procedure</td>
          <td noWrap><asp:checkbox id=sVaIsAutoProc runat="server" Text="Automatic"></asp:checkbox>&nbsp; 
<asp:checkbox id=sVaIsAutoIrrrlProc runat="server" Text="Auto IRRRL"></asp:checkbox>&nbsp; 
<asp:checkbox id=sVaIsPriorApprovalProc runat="server" Text="VA Prior Approval"></asp:checkbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap>13. Purpose of 
          Loan</td>
          <td class=FieldLabel><asp:checkbox id=sVaLPurposeTLckd onclick=refreshCalculation(); runat="server" Text="Lock"></asp:checkbox></td>
          <td noWrap><asp:dropdownlist id=sVaLPurposeT runat="server"></asp:dropdownlist></td></tr>
          
        <tr>
          <td class=FieldLabel noWrap>14. Loan Code</td>
          <td class=FieldLabel><asp:checkbox id=sVaLCodeTLckd onclick=refreshCalculation(); runat="server" Text="Lock"></asp:checkbox></td>
          <td noWrap><asp:dropdownlist id=sVaLCodeT runat="server"></asp:dropdownlist></td></tr>
          <tr>
            <td class="FieldLabel" colspan="2">15. Prior Loan Type 
            <br/>(Note: Must be completed if Regular("Cash-out") Refinance is selected in item 14)</td>
            <td valign="top"><asp:DropDownList ID="sVaPriorLoanT" runat="server"></asp:DropDownList></td>
          </tr>
          
        <tr>
          <td class=FieldLabel noWrap>16. Type of 
          Mortgage</td>
          <td class=FieldLabel><asp:checkbox id=sVaFinMethTLckd onclick=refreshCalculation(); runat="server" Text="Lock"></asp:checkbox></td>
          <td noWrap><asp:dropdownlist id=sVaFinMethT runat="server" onchange="refreshCalculation();"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>17. Type 
            of Hybrid-ARM</td>
          <td noWrap><asp:dropdownlist id=sVaHybridArmT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>18. Type 
            of Ownership</td>
          <td noWrap><asp:dropdownlist id=sVaOwnershipT runat="server"></asp:dropdownlist></td></tr>
        <tr>
            <td colspan="3" id="CloseWarningLabel" style="display:none">
                <table width="485">
                  <tr>
                    <td class="WarningLabelStyle">
                       WARNING: &nbsp;Modifying the closed date could remove your permission to edit this loan.
                    </td>
                  </tr>
                </table>
            </td>
        </tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>19. 
            Closing Date</td>
          <td noWrap><ml:datetextbox id=sClosedD onblur="f_displayWarning('CloseWarningLabel', false);" onfocus="f_displayWarning('CloseWarningLabel', true);" onchange="f_displayWarning('CloseWarningLabel', true, this);" runat="server" width="75" preset="date"></ml:datetextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>20. 
            Purchase Price</td>
          <td noWrap><ml:moneytextbox id=sPurchPrice runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>21. 
            Reasonable Value</td>
          <td noWrap><ml:moneytextbox id="aFHABorrCertInformedPropVal" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap colSpan=2 >22. Energy Improvements</td>
          <td noWrap>
            <table id=Table4 cellSpacing=0 cellPadding=0 border=0 
            >
              <tr>
                <td noWrap></td>
                <td noWrap><asp:checkbox id=sVaEnergyImprovIsSolar runat="server" Text="Installation of Solar Heating/Cooling"></asp:checkbox></td></tr>
              <tr>
                <td noWrap><asp:checkbox id=sVaEnergyImprovIsMajorSystem runat="server" Text="Replacement of Major System"></asp:checkbox></td>
                <td noWrap><asp:checkbox id=sVaEnergyImprovIsNewFeature runat="server" Text="Addition of new feature"></asp:checkbox></td></tr>
              <tr>
                <td noWrap><asp:checkbox id=sVaEnergyImprovIsInsulation runat="server" Text="Insulation, caulking, etc.."></asp:checkbox></td>
                <td noWrap><asp:checkbox id=sVaEnergyImprovIsOther runat="server" Text="Other improvements"></asp:checkbox></td></tr>
              <tr>
                <td noWrap></td>
                <td noWrap></td></tr></table></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>Energy 
            Improvements Amount</td>
          <td noWrap><ml:moneytextbox id=sVaEnergyImprovAmt runat="server" width="90" preset="money"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>23. Loan 
            Amount</td>
          <td noWrap><ml:moneytextbox id=sFinalLAmt runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>24. 
            Property Type</td>
          <td noWrap><asp:dropdownlist id=sSpT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>25. 
            Appraisal Type</td>
          <td noWrap><asp:dropdownlist id=sVaApprT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>26. Type 
            of Structure</td>
          <td noWrap><asp:dropdownlist id=sVaStructureT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>27. 
            Property Designation</td>
          <td noWrap><asp:dropdownlist id=sVaPropDesignationT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>28. No. 
            Of Units</td>
          <td noWrap><asp:textbox id=sUnitsNum runat="server" Width="63px"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>29. MCRV 
            No.</td>
          <td noWrap><asp:textbox id=sVaMcrvNum runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>30. 
            Manufactured Home Category</td>
          <td noWrap><asp:dropdownlist id=sVaManufacturedHomeT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>31. 
            Property Address</td>
          <td noWrap><asp:textbox id=sSpAddr runat="server" Width="234px"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2></td>
          <td noWrap><asp:textbox id=sSpCity runat="server"></asp:textbox><ml:statedropdownlist id=sSpState runat="server" IncludeTerritories="false"></ml:statedropdownlist><ml:zipcodetextbox id=sSpZip runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>35. 
            Property County</td>
          <td noWrap><asp:DropDownList id=sSpCounty runat="server"></asp:DropDownList></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>36. 
            Lender VA ID Number</td>
          <td noWrap><ml:combobox id=sVALenderIdCode runat="server"></ml:combobox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>37. Agent 
            VA ID Number</td>
          <td noWrap><asp:textbox id=sVASponsorAgentIdCode runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap>38. 
            Lender Loan Number</td>
          <td class=FieldLabel><asp:CheckBox ID="sLenderCaseNumLckd" runat="server" onclick="refreshCalculation();"/>Lock</td>
          <td noWrap><asp:textbox id=sLenderCaseNum runat="server"></asp:textbox></td></tr>
        <tr>
          <td class="LoanFormHeader FieldLabel" noWrap align=middle colSpan=3 
          >For LAPP Cases Only</td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>39. 
            Lender SAR ID Number</td>
          <td noWrap><asp:textbox id=sVaLenSarId runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>40. Gross 
            Living Area (sq feet)</td>
          <td noWrap><asp:textbox id=sSpLivingSqf runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>41. Age 
            Of Property (Yrs)</td>
          <td noWrap><asp:textbox id=sSpAge runat="server" width="91px"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>42. Date 
            SAR Issued Notification Of Value</td>
          <td noWrap><ml:datetextbox id=sVaSarNotifIssuedD runat="server" width="75" preset="date"></ml:datetextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>43. Total 
            Room Count</td>
          <td noWrap><asp:textbox id=sSpRoomCount runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>44. 
          Baths</td>
          <td noWrap><asp:textbox id=sSpBathCount runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>45. 
            Bedrooms</td>
          <td noWrap><asp:textbox id=sSpBedroomCount runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel colSpan=3>46. If 
            processed under LAPP, was the fee appraiser's original value 
            estimate changed or repair recommendations revised, or did the SAR 
            otherwise make significant adjustments? <asp:checkboxlist id=sVaAppraisalOrSarAdjustmentTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:checkboxlist></td></tr>
        <tr>
          <td class="LoanFormHeader FieldLabel" noWrap align=middle colSpan=3 
          >Income Information (Not applicable for 
          IRRRLs)</td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=3>47a. Loan 
            Processed under VA recognized automated underwriting system?<asp:checkboxlist id=sIsProcessedUnderAutoUnderwritingTri runat="server" repeatlayout="Flow" repeatdirection="Horizontal"></asp:checkboxlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>47b. 
            Which system was used?</td>
          <td noWrap><asp:dropdownlist id=sVaAutoUnderwritingT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>47c. Risk 
            Classification</td>
          <td noWrap><asp:dropdownlist id=sVaRiskT runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap>48. Credit Score</td>
          <td class=FieldLabel><asp:checkbox id=sVaVetMedianCrScoreLckd onclick=refreshCalculation(); runat="server" Text="Lock"></asp:checkbox></td>
          <td noWrap><asp:textbox id=sVaVetMedianCrScore runat="server" Width="55px"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>49. 
            Liquid Assets</td>
          <td noWrap><ml:moneytextbox id=aAsstLiqTot runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>50. Total 
            Monthly Gross Income</td>
          <td noWrap><ml:moneytextbox id=aVaTotMonGrossI runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>51. 
            Residual Income</td>
          <td noWrap><ml:moneytextbox id=aVaFamilySupportBal runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>52. 
            Residual Income Guideline</td>
          <td noWrap><ml:moneytextbox id=aVaFamilySuportGuidelineAmt runat="server" width="90" preset="money"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colspan=2>53. Debt-Income 
            Ratio</td>
          <td noWrap><ml:percenttextbox id=aVaRatio runat="server" preset="percent" width="70" ReadOnly="True"></ml:percenttextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colspan=2>54. Spouse Income Considered</td>
          <td class=FieldLabel noWrap><asp:checkbox id=aVaIsCoborIConsidered runat="server" Text="Yes" enabled="False"></asp:checkbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap>55. Spouse Income Amount</td>
          <td class=FieldLabel><asp:CheckBox id=aVaCEmplmtILckd runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox></td>
          <td noWrap><ml:moneytextbox id=aVaCEmplmtI runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></td></tr>
        <tr>
          <td class="LoanFormHeader FieldLabel" noWrap align=middle colSpan=3 >Discount Information</td></tr>
        <tr>
          <td class=FieldLabel noWrap>56. Discount Points Charged</td>
          <td class=FieldLabel><asp:checkbox id=sVaLDiscntLckd runat="server" Text="Lock" onclick="refreshCalculation();"></asp:checkbox></td>
          <td noWrap><ml:percenttextbox id=sVaLDiscntPc runat="server" preset="percent" width="70"></ml:percenttextbox>&nbsp;or 
<ml:moneytextbox id=sVaLDiscnt runat="server" preset="money" width="90"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap>57. Discount Points Paid By Veteran</td>
          <td class=FieldLabel><asp:checkbox id=sVaLDiscntPbbLckd runat="server" Text="Lock" onclick="refreshCalculation();"></asp:checkbox></td>
          <td noWrap><ml:percenttextbox id=sVaLDiscntPcPbb runat="server" preset="percent" width="70"></ml:percenttextbox>&nbsp;or 
<ml:moneytextbox id=sVaLDiscntPbb runat="server" preset="money" width="90"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colspan=2>58. Term (months)</td>
          <td noWrap><asp:textbox id=sTerm runat="server" width="73px" onchange="refreshCalculation();"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colspan=2>59. Interest Rate</td>
          <td noWrap><ml:percenttextbox id=sNoteIR runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colspan=2>60. Funding Fee Exempt</td>
          <td class=FieldLabel noWrap><asp:checkboxlist id=sVaFfExemptTri runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
<asp:ListItem Value="1">Exempt</asp:ListItem>
<asp:ListItem Value="2">Not Exempt</asp:ListItem>
</asp:checkboxlist></td></tr>
        <tr>
          <td class="LoanFormHeader FieldLabel" noWrap align=middle colSpan=3 >For IRRRLs Only</td></tr>
        <tr>
          <td class=FieldLabel noWrap colspan=2>61. Paid in full VA Loan Number</td>
          <td noWrap><asp:textbox id=sVaIrrrlsUsedOnlyPdInFullLNum runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colspan=2>62. Original Loan Amount</td>
          <td noWrap><ml:moneytextbox id=sVaIrrrlsUsedOnlyOrigLAmt runat="server" preset="money" width="90"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap colspan=2>63. Original Interest Rate</td>
          <td nowrap><ml:PercentTextBox id=sVaIrrrlsUsedOnlyOrigIR runat="server" width="70" preset="percent"></ml:PercentTextBox></td></tr>
        <tr>
          <td class=FieldLabel noWrap>64. Remarks</td>
          <td class=FieldLabel></td>
          <td noWrap></td></tr>
        <tr>
          <td class=FieldLabel nowrap colspan=3><asp:TextBox id=sVaIrrrlsUsedOnlyRemarks runat="server" Width="493px" TextMode="MultiLine" Height="124px"></asp:TextBox></td></tr></table></td></tr></table></form>
	
  </body>
</html>
