using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
    public class VAReasonableValueServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VAReasonableValueServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sSpUtilElecT = (E_sSpUtilT) GetInt("sSpUtilElecT");
            dataLoan.sSpUtilGasT = (E_sSpUtilT) GetInt("sSpUtilGasT");
            dataLoan.sSpUtilSanSewerT = (E_sSpUtilT) GetInt("sSpUtilSanSewerT");
            dataLoan.sSpUtilWaterT = (E_sSpUtilT) GetInt("sSpUtilWaterT");
            dataLoan.sVaBuildingStatusT = (E_sVaBuildingStatusT) GetInt("sVaBuildingStatusT");
            dataLoan.sVaBuildingT = (E_sVaBuildingT) GetInt("sVaBuildingT");
            dataLoan.sVaSpOccT = (E_sVaSpOccT) GetInt("sVaSpOccT");


            dataLoan.sLotPurchaseSeparatelyTri = GetTriState("sLotPurchaseSeparatelyTri");
            dataLoan.sSpMineralRightsReservedTri = GetTriState("sSpMineralRightsReservedTri");
            dataLoan.sVaConstructWarrantyTri = GetTriState("sVaConstructWarrantyTri");
            dataLoan.sVaIsFactoryFabricatedTri = GetTriState("sVaIsFactoryFabricatedTri");
            dataLoan.sVaSaleContractAttachedTri = GetTriState("sVaSaleContractAttachedTri");
            dataLoan.sVaStreetAccessPrivateTri = GetTriState("sVaStreetAccessPrivateTri");
            dataLoan.sVaStreetMaintenancePrivateTri = GetTriState("sVaStreetMaintenancePrivateTri");
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum");
            
            dataLoan.sFHACondCommInstCaseRef = GetString("sFHACondCommInstCaseRef");

            dataLoan.sVALenderIdCode = GetString("sVALenderIdCode");

            dataLoan.sVASponsorAgentIdCode = GetString("sVASponsorAgentIdCode");

            dataLoan.sLeaseHoldExpireD_rep = GetString("sLeaseHoldExpireD");

            dataLoan.sProRealETxPerYr_rep = GetString("sProRealETxPerYr");

            dataLoan.sPurchPrice_rep = GetString("sPurchPrice");

            dataLoan.sSpAddr = GetString("sSpAddr");


            dataLoan.sSpCity = GetString("sSpCity");

            dataLoan.sSpCounty = GetString("sSpCounty");

            dataLoan.sSpHasClothesWasher = GetBool("sSpHasClothesWasher");

            dataLoan.sSpHasDishWasher = GetBool("sSpHasDishWasher");

            dataLoan.sSpHasDryer = GetBool("sSpHasDryer");

            dataLoan.sSpHasGarbageDisposal = GetBool("sSpHasGarbageDisposal");

            dataLoan.sSpHasOven = GetBool("sSpHasOven");

            dataLoan.sSpHasRefrig = GetBool("sSpHasRefrig");

            dataLoan.sSpHasVentFan = GetBool("sSpHasVentFan");

            dataLoan.sSpHasWwCarpet = GetBool("sSpHasWwCarpet");

            dataLoan.sSpLeaseAnnualGroundRent_rep = GetString("sSpLeaseAnnualGroundRent");

            dataLoan.sSpLeaseIs99Yrs = GetBool("sSpLeaseIs99Yrs");

            dataLoan.sSpLeaseIsRenewable = GetBool("sSpLeaseIsRenewable");

            dataLoan.sSpLegalDesc = GetString("sSpLegalDesc");

            dataLoan.sSpLotAcres = GetString("sSpLotAcres");

            dataLoan.sSpLotDimension = GetString("sSpLotDimension");

            dataLoan.sSpLotIrregularSqf = GetString("sSpLotIrregularSqf");

            dataLoan.sSpLotIsAcres = GetBool("sSpLotIsAcres");

            dataLoan.sSpLotIsIrregular = GetBool("sSpLotIsIrregular");

            dataLoan.sSpMineralRightsReservedExplain = GetString("sSpMineralRightsReservedExplain");

            dataLoan.sSpState = GetString("sSpState");

            dataLoan.sSpZip = GetString("sSpZip");

            dataLoan.sVaBuildingStatusTLckd = GetBool("sVaBuildingStatusTLckd");

            dataLoan.sVaConstructCompleteD_rep = GetString("sVaConstructCompleteD");

            dataLoan.sVaConstructExpiredD_rep = GetString("sVaConstructExpiredD");

            dataLoan.sVaConstructWarrantyProgramNm = GetString("sVaConstructWarrantyProgramNm");

            dataLoan.sVaNumOfBuildings = GetString("sVaNumOfBuildings");

            dataLoan.sVaNumOfLivingUnits = GetString("sVaNumOfLivingUnits");

            dataLoan.sVaOwnerNm = GetString("sVaOwnerNm");

            dataLoan.sVaRefiAmt_rep = GetString("sVaRefiAmt");

            dataLoan.sVaSpOccupantNm = GetString("sVaSpOccupantNm");

            dataLoan.sVaSpOccupantPhone = GetString("sVaSpOccupantPhone");

            dataLoan.sVaSpRentalMonthly_rep = GetString("sVaSpRentalMonthly");

            dataLoan.sVaTitleLimitDesc = GetString("sVaTitleLimitDesc");

            dataLoan.sVaTitleLimitIsCondo = GetBool("sVaTitleLimitIsCondo");

            dataLoan.sVaTitleLimitIsPud = GetBool("sVaTitleLimitIsPud");



            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805FirmMakingRequest, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("VA26_1805FirmMakingRequestCompanyName") ;
            preparer.StreetAddr = GetString("VA26_1805FirmMakingRequestStreetAddr") ;
            preparer.City = GetString("VA26_1805FirmMakingRequestCity") ;
            preparer.State = GetString("VA26_1805FirmMakingRequestState") ;
            preparer.Zip = GetString("VA26_1805FirmMakingRequestZip") ;
            preparer.EmailAddr = GetString("VA26_1805FirmMakingRequestNotificationEmail");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Broker, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("VA26_1805BrokerCompanyName") ;
            preparer.PhoneOfCompany = GetString("VA26_1805BrokerPhoneOfCompany") ;
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805KeyAt, E_ReturnOptionIfNotExist.CreateNew);
            preparer.StreetAddr = GetString("VA26_1805KeyAtStreetAddr") ;
            preparer.City = GetString("VA26_1805KeyAtCity") ;
            preparer.State = GetString("VA26_1805KeyAtState") ;
            preparer.Zip = GetString("VA26_1805KeyAtZip") ;
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Builder, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("VA26_1805BuilderCompanyName") ;
            preparer.StreetAddr = GetString("VA26_1805BuilderStreetAddr") ;
            preparer.City = GetString("VA26_1805BuilderCity") ;
            preparer.State = GetString("VA26_1805BuilderState") ;
            preparer.Zip = GetString("VA26_1805BuilderZip") ;
            preparer.PhoneOfCompany = GetString("VA26_1805BuilderPhoneOfCompany") ;
            preparer.LicenseNumOfCompany = GetString("VA26_1805BuilderId");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Warrantor, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("VA26_1805WarrantorCompanyName") ;
            preparer.StreetAddr = GetString("VA26_1805WarrantorStreetAddr") ;
            preparer.City = GetString("VA26_1805WarrantorCity") ;
            preparer.State = GetString("VA26_1805WarrantorState") ;
            preparer.Zip = GetString("VA26_1805WarrantorZip") ;
            preparer.PhoneOfCompany = GetString("VA26_1805WarrantorPhoneOfCompany") ;
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Authorize, E_ReturnOptionIfNotExist.CreateNew);
            preparer.Title = GetString("VA26_1805AuthorizeTitle") ;
            preparer.Phone = GetString("VA26_1805AuthorizePhone") ;
            preparer.PrepareDate_rep = GetString("VA26_1805AuthorizePrepareDate") ;
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Appraiser, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PreparerName = GetString("VA26_1805AppraiserPreparerName") ;
            preparer.CompanyName = GetString("VA26_1805AppraiserCompanyName") ;
            preparer.StreetAddr = GetString("VA26_1805AppraiserStreetAddr") ;
            preparer.City = GetString("VA26_1805AppraiserCity") ;
            preparer.State = GetString("VA26_1805AppraiserState") ;
            preparer.Zip = GetString("VA26_1805AppraiserZip") ;
            preparer.PrepareDate_rep = GetString("VA26_1805AppraiserPrepareDate") ;
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805ApplicablePointOfContact, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PreparerName = GetString("VA26_1805ApplicablePointOfContactName");
            preparer.StreetAddr = GetString("VA26_1805ApplicablePointOfContactAddress");
            preparer.City = GetString("VA26_1805ApplicablePointOfContactCity");
            preparer.State = GetString("VA26_1805ApplicablePointOfContactState");
            preparer.Zip = GetString("VA26_1805ApplicablePointOfContactZip");
            preparer.Phone = GetString("VA26_1805ApplicablePointOfContactPhone");
            preparer.Update();

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sSpUtilElecT", dataLoan.sSpUtilElecT);
            SetResult("sSpUtilGasT", dataLoan.sSpUtilGasT);
            SetResult("sSpUtilSanSewerT", dataLoan.sSpUtilSanSewerT);
            SetResult("sSpUtilWaterT", dataLoan.sSpUtilWaterT);
            SetResult("sVaBuildingStatusT", dataLoan.sVaBuildingStatusT);
            SetResult("sVaBuildingT", dataLoan.sVaBuildingT);
            SetResult("sVaSpOccT", dataLoan.sVaSpOccT);
            SetResult("sLotPurchaseSeparatelyTri", dataLoan.sLotPurchaseSeparatelyTri);
            SetResult("sSpMineralRightsReservedTri", dataLoan.sSpMineralRightsReservedTri);
            SetResult("sVaConstructWarrantyTri", dataLoan.sVaConstructWarrantyTri);
            SetResult("sVaIsFactoryFabricatedTri", dataLoan.sVaIsFactoryFabricatedTri);
            SetResult("sVaSaleContractAttachedTri", dataLoan.sVaSaleContractAttachedTri);
            SetResult("sVaStreetAccessPrivateTri", dataLoan.sVaStreetAccessPrivateTri);
            SetResult("sVaStreetMaintenancePrivateTri", dataLoan.sVaStreetMaintenancePrivateTri);

            SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            SetResult("sFHACondCommInstCaseRef", dataLoan.sFHACondCommInstCaseRef);
            SetResult("sVALenderIdCode", dataLoan.sVALenderIdCode);
            SetResult("sVASponsorAgentIdCode", dataLoan.sVASponsorAgentIdCode);
            SetResult("sLeaseHoldExpireD", dataLoan.sLeaseHoldExpireD_rep);
            SetResult("sProRealETxPerYr", dataLoan.sProRealETxPerYr_rep);
            SetResult("sPurchPrice", dataLoan.sPurchPrice_rep);
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpCounty", dataLoan.sSpCounty);
            SetResult("sSpHasClothesWasher", dataLoan.sSpHasClothesWasher);
            SetResult("sSpHasDishWasher", dataLoan.sSpHasDishWasher);
            SetResult("sSpHasDryer", dataLoan.sSpHasDryer);
            SetResult("sSpHasGarbageDisposal", dataLoan.sSpHasGarbageDisposal);
            SetResult("sSpHasOven", dataLoan.sSpHasOven);
            SetResult("sSpHasRefrig", dataLoan.sSpHasRefrig);
            SetResult("sSpHasVentFan", dataLoan.sSpHasVentFan);
            SetResult("sSpHasWwCarpet", dataLoan.sSpHasWwCarpet);
            SetResult("sSpLeaseAnnualGroundRent", dataLoan.sSpLeaseAnnualGroundRent_rep);
            SetResult("sSpLeaseIs99Yrs", dataLoan.sSpLeaseIs99Yrs);
            SetResult("sSpLeaseIsRenewable", dataLoan.sSpLeaseIsRenewable);
            SetResult("sSpLegalDesc", dataLoan.sSpLegalDesc);
            SetResult("sSpLotAcres", dataLoan.sSpLotAcres);
            SetResult("sSpLotDimension", dataLoan.sSpLotDimension);
            SetResult("sSpLotIrregularSqf", dataLoan.sSpLotIrregularSqf);
            SetResult("sSpLotIsAcres", dataLoan.sSpLotIsAcres);
            SetResult("sSpLotIsIrregular", dataLoan.sSpLotIsIrregular);
            SetResult("sSpMineralRightsReservedExplain", dataLoan.sSpMineralRightsReservedExplain);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);
            SetResult("sVaBuildingStatusTLckd", dataLoan.sVaBuildingStatusTLckd);
            SetResult("sVaConstructCompleteD", dataLoan.sVaConstructCompleteD_rep);
            SetResult("sVaConstructExpiredD", dataLoan.sVaConstructExpiredD_rep);
            SetResult("sVaConstructWarrantyProgramNm", dataLoan.sVaConstructWarrantyProgramNm);
            SetResult("sVaNumOfBuildings", dataLoan.sVaNumOfBuildings);
            SetResult("sVaNumOfLivingUnits", dataLoan.sVaNumOfLivingUnits);
            SetResult("sVaOwnerNm", dataLoan.sVaOwnerNm);
            SetResult("sVaRefiAmt", dataLoan.sVaRefiAmt_rep);
            SetResult("sVaSpOccupantNm", dataLoan.sVaSpOccupantNm);
            SetResult("sVaSpOccupantPhone", dataLoan.sVaSpOccupantPhone);
            SetResult("sVaSpRentalMonthly", dataLoan.sVaSpRentalMonthly_rep);
            SetResult("sVaTitleLimitDesc", dataLoan.sVaTitleLimitDesc);
            SetResult("sVaTitleLimitIsCondo", dataLoan.sVaTitleLimitIsCondo);
            SetResult("sVaTitleLimitIsPud", dataLoan.sVaTitleLimitIsPud);


            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805FirmMakingRequest, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1805FirmMakingRequestCompanyName", preparer.CompanyName);
            SetResult("VA26_1805FirmMakingRequestStreetAddr", preparer.StreetAddr);
            SetResult("VA26_1805FirmMakingRequestCity", preparer.City);
            SetResult("VA26_1805FirmMakingRequestState", preparer.State);
            SetResult("VA26_1805FirmMakingRequestZip", preparer.Zip);
            SetResult("VA26_1805FirmMakingRequestNotificationEmail", preparer.EmailAddr);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Broker, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1805BrokerCompanyName", preparer.CompanyName);
            SetResult("VA26_1805BrokerPhoneOfCompany", preparer.PhoneOfCompany);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805KeyAt, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1805KeyAtStreetAddr", preparer.StreetAddr);
            SetResult("VA26_1805KeyAtCity", preparer.City);
            SetResult("VA26_1805KeyAtState", preparer.State);
            SetResult("VA26_1805KeyAtZip", preparer.Zip);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Builder, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1805BuilderCompanyName", preparer.CompanyName);
            SetResult("VA26_1805BuilderStreetAddr", preparer.StreetAddr);
            SetResult("VA26_1805BuilderCity", preparer.City);
            SetResult("VA26_1805BuilderState", preparer.State);
            SetResult("VA26_1805BuilderZip", preparer.Zip);
            SetResult("VA26_1805BuilderPhoneOfCompany", preparer.PhoneOfCompany);
            SetResult("VA26_1805BuilderId", preparer.LicenseNumOfCompany);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Warrantor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1805WarrantorCompanyName", preparer.CompanyName);
            SetResult("VA26_1805WarrantorStreetAddr", preparer.StreetAddr);
            SetResult("VA26_1805WarrantorCity", preparer.City);
            SetResult("VA26_1805WarrantorState", preparer.State);
            SetResult("VA26_1805WarrantorZip", preparer.Zip);
            SetResult("VA26_1805WarrantorPhoneOfCompany", preparer.PhoneOfCompany);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Authorize, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1805AuthorizeTitle", preparer.Title);
            SetResult("VA26_1805AuthorizePhone", preparer.Phone);
            SetResult("VA26_1805AuthorizePrepareDate", preparer.PrepareDate_rep);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1805AppraiserPreparerName", preparer.PreparerName);
            SetResult("VA26_1805AppraiserCompanyName", preparer.CompanyName);
            SetResult("VA26_1805AppraiserStreetAddr", preparer.StreetAddr);
            SetResult("VA26_1805AppraiserCity", preparer.City);
            SetResult("VA26_1805AppraiserState", preparer.State);
            SetResult("VA26_1805AppraiserZip", preparer.Zip);
            SetResult("VA26_1805AppraiserPrepareDate", preparer.PrepareDate_rep);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805ApplicablePointOfContact, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_1805ApplicablePointOfContactName", preparer.PreparerName);
            SetResult("VA26_1805ApplicablePointOfContactAddress", preparer.StreetAddr);
            SetResult("VA26_1805ApplicablePointOfContactCity", preparer.City);
            SetResult("VA26_1805ApplicablePointOfContactState", preparer.State);
            SetResult("VA26_1805ApplicablePointOfContactZip", preparer.Zip);
            SetResult("VA26_1805ApplicablePointOfContactPhone", preparer.Phone);
        }

    }
	/// <summary>
	/// Summary description for VAReasonableValueService.
	/// </summary>
	public partial class VAReasonableValueService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new VAReasonableValueServiceItem());
        }
	}
}
