using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
    public class VARefiWorksheetServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VARefiWorksheetServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sVaRefiWsAllowableCcAndPp_rep = GetString("sVaRefiWsAllowableCcAndPp");
            dataLoan.sVaRefiWsCashPmtFromVet_rep   = GetString("sVaRefiWsCashPmtFromVet");
            dataLoan.sVaRefiWsDiscntPc_rep         = GetString("sVaRefiWsDiscntPc");
            dataLoan.sVaRefiWsExistingVaLBal_rep   = GetString("sVaRefiWsExistingVaLBal");
            dataLoan.sVaRefiWsExistingVaLBalLckd   = GetBool("sVaRefiWsExistingVaLBalLckd");
            dataLoan.sVaRefiWsFinalDiscntPc_rep    = GetString("sVaRefiWsFinalDiscntPc");
            dataLoan.sVaRefiWsOrigFeePc_rep        = GetString("sVaRefiWsOrigFeePc");

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_8928Lender, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PrepareDate_rep = GetString("VA26_8928LenderPrepareDate");
            preparer.Title = GetString("VA26_8928LenderTitle");
            preparer.CompanyName = GetString("VA26_8928LenderCompanyName");
            preparer.Update();
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sVaRefiWsAllowableCcAndPp",            dataLoan.sVaRefiWsAllowableCcAndPp_rep);
            SetResult("sVaRefiWsCashPmtFromVet",              dataLoan.sVaRefiWsCashPmtFromVet_rep);
            SetResult("sVaRefiWsDiscnt",                      dataLoan.sVaRefiWsDiscnt_rep);
            SetResult("sVaRefiWsDiscnt2",                     dataLoan.sVaRefiWsDiscnt_rep);
            SetResult("sVaRefiWsDiscntPc",                    dataLoan.sVaRefiWsDiscntPc_rep);
            SetResult("sVaRefiWsExistingVaLBal",              dataLoan.sVaRefiWsExistingVaLBal_rep);
            SetResult("sVaRefiWsExistingVaLBalLckd",          dataLoan.sVaRefiWsExistingVaLBalLckd);
            SetResult("sVaRefiWsExistingVaLBalAfterCashPmt",  dataLoan.sVaRefiWsExistingVaLBalAfterCashPmt_rep);
            SetResult("sVaRefiWsExistingVaLBalAfterCashPmt2", dataLoan.sVaRefiWsExistingVaLBalAfterCashPmt_rep);
            SetResult("sVaRefiWsFf",                          dataLoan.sVaRefiWsFf_rep);
            SetResult("sVaRefiWsFf2",                         dataLoan.sVaRefiWsFf_rep);
            SetResult("sVaRefiWsFfPc",                        dataLoan.sVaRefiWsFfPc_rep);
            SetResult("sVaRefiWsFfPc2",                       dataLoan.sVaRefiWsFfPc_rep);
            SetResult("sVaRefiWsFinalDiscnt",                 dataLoan.sVaRefiWsFinalDiscnt_rep);
            SetResult("sVaRefiWsFinalDiscntPc",               dataLoan.sVaRefiWsFinalDiscntPc_rep);
            SetResult("sVaRefiWsFinalFf",                     dataLoan.sVaRefiWsFinalFf_rep);
            SetResult("sVaRefiWsFinalSubtotalItem12",         dataLoan.sVaRefiWsFinalSubtotalItem12_rep);
            SetResult("sVaRefiWsFinalSubtotalItem14",         dataLoan.sVaRefiWsFinalSubtotalItem14_rep);
            SetResult("sVaRefiWsFinalSubtotalItem16",         dataLoan.sVaRefiWsFinalSubtotalItem16_rep);
            SetResult("sVaRefiWsMaxLAmt",                     dataLoan.sVaRefiWsMaxLAmt_rep);
            SetResult("sVaRefiWsOrigFee",                     dataLoan.sVaRefiWsOrigFee_rep);
            SetResult("sVaRefiWsOrigFeePc",                   dataLoan.sVaRefiWsOrigFeePc_rep);
            SetResult("sVaRefiWsPreliminaryTot",              dataLoan.sVaRefiWsPreliminaryTot_rep);
            SetResult("sVaRefiWsPreliminaryTot2",             dataLoan.sVaRefiWsPreliminaryTot_rep);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_8928Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_8928LenderPrepareDate", preparer.PrepareDate_rep);
            SetResult("VA26_8928LenderTitle", preparer.Title);
            SetResult("VA26_8928LenderCompanyName", preparer.CompanyName);
        }

    }

	/// <summary>
	/// Summary description for VARefiWorksheetService.
	/// </summary>
	public partial class VARefiWorksheetService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new VARefiWorksheetServiceItem());
        }

	}
}
