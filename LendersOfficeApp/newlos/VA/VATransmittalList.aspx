<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="VATransmittalList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.VA.VATransmittalList" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>VATransmittalList</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	
    <form id="VATransmittalList" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 border=0>
  <tr>
    <td nowrap colspan=2 class="MainRightHeader">VA Transmittal List (VA 26-0285)</td></tr>
  <tr>
    <td class=FieldLabel nowrap>VA Case Number</td>
    <td nowrap><asp:TextBox id=sAgencyCaseNum runat="server"></asp:TextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Name of Lender's Representative</td>
    <td nowrap><asp:TextBox id=VA26_0285LenderPreparerName runat="server"></asp:TextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Title of Lender's Representative</td>
    <td nowrap><asp:TextBox id=VA26_0285LenderTitle runat="server"></asp:TextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Direct Telephone for Representative</td>
    <td nowrap><ml:PhoneTextBox id=VA26_0285LenderPhone runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
  <tr>
    <td class=FieldLabel nowrap>Company Name</td>
    <td 
  nowrap><asp:TextBox id=VA26_0285BrokerCompanyName runat="server"></asp:TextBox></td></tr></table>

     </form>
	
  </body>
</html>
