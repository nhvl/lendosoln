﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
    public partial class VALoanComparison : BaseLoanPage
    {
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VALoanComparison));
            dataLoan.InitLoad();

            sVaLoanNumCurrentLoan.Text = dataLoan.sVaLoanNumCurrentLoan;
            sVaLoanAmtCurrentLoan.Text = dataLoan.sVaLoanAmtCurrentLoan_rep;
            sVaNoteIrCurrentLoan.Text = dataLoan.sVaNoteIrCurrentLoan_rep;
            sVaTermCurrentLoan.Text = dataLoan.sVaTermCurrentLoan_rep;
            sVaMonthlyPmtCurrentLoan.Text = dataLoan.sVaMonthlyPmtCurrentLoan_rep;
            sVaNewPmtQualified.Checked = dataLoan.sVaNewPmtQualified;
            aVaTotalClosingCost.Text = dataLoan.aVaTotalClosingCost_rep;
            aVaTotalClosingCostLckd.Checked = dataLoan.sVaTotalClosingCostLckd;
            sMonthlyPmt.Text = dataLoan.sMonthlyPmt_rep;
            sTerm.Text = dataLoan.sTerm_rep;
            sRateAmortT.Text = dataLoan.sRateAmortT;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            sVaLoanCompMonthlyPmtDecreaseAmt.Text = dataLoan.sVaLoanCompMonthlyPmtDecreaseAmt_rep;
            sVaLoanCompRecoupCostsTime.Text = dataLoan.sVaLoanCompRecoupCostsTime_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
            sVaPICurrentLoan.Text = dataLoan.sVaPICurrentLoan_rep;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "VA Loan Comparison (Obsolete for loans closing on or after 4/1/2018)";
            this.PageID = "VALoanComparison";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CVALoanComparison);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
