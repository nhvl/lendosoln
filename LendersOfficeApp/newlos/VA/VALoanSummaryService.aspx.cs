using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
    public class VALoanSummaryServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VALoanSummaryServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataApp.aBDob_rep                             = GetString("aBDob");
            dataApp.aBFirstNm                             = GetString("aBFirstNm");
            dataApp.aBGender                              = (E_GenderT) GetInt("aBGenderT");
            dataApp.aBHispanicT                           = (E_aHispanicT) GetInt("aBHispanicT");
            dataApp.aBIsAmericanIndian                    = GetBool("aBIsAmericanIndian");
            dataApp.aBIsAsian                             = GetBool("aBIsAsian");
            dataApp.aBIsBlack                             = GetBool("aBIsBlack");
            dataApp.aBIsPacificIslander                   = GetBool("aBIsPacificIslander");
            dataApp.aBIsWhite                             = GetBool("aBIsWhite");
            dataApp.aBLastNm                              = GetString("aBLastNm");
            dataApp.aBMidNm                               = GetString("aBMidNm");
            dataApp.aBSsn                                 = GetString("aBSsn");
            dataApp.aBSuffix                              = GetString("aBSuffix");
            dataApp.aVaCEmplmtI_rep                       = GetString("aVaCEmplmtI");
            dataApp.aVaCEmplmtILckd                       = GetBool("aVaCEmplmtILckd");
            dataApp.aVaEntitleAmt_rep                     = GetString("aVaEntitleAmt");
            dataApp.aVaEntitleCode                        = GetString("aVaEntitleCode");
            dataApp.aVaFamilySuportGuidelineAmt_rep       = GetString("aVaFamilySuportGuidelineAmt");
            dataApp.aVaIsVeteranFirstTimeBuyerTri         = GetTriState("aVaIsVeteranFirstTimeBuyerTri");
            dataApp.aVaMilitaryStatT                      = (E_aVaMilitaryStatT) GetInt("aVaMilitaryStatT");
            dataApp.aVaServiceBranchT                     = (E_aVaServiceBranchT) GetInt("aVaServiceBranchT");
            dataLoan.sVaPriorLoanT = (E_sVaPriorLoanT)GetInt("sVaPriorLoanT");
            dataLoan.sAgencyCaseNum                       = GetString("sAgencyCaseNum");
            dataApp.aFHABorrCertInformedPropVal_rep       = GetString("aFHABorrCertInformedPropVal");
            dataLoan.sClosedD_rep                         = GetString("sClosedD");
            dataLoan.sVALenderIdCode                     = GetString("sVALenderIdCode");
            dataLoan.sVASponsorAgentIdCode               = GetString("sVASponsorAgentIdCode");
            dataLoan.sIsProcessedUnderAutoUnderwritingTri = GetTriState("sIsProcessedUnderAutoUnderwritingTri");
            dataLoan.sLenderCaseNum                       = GetString("sLenderCaseNum");
            dataLoan.sLenderCaseNumLckd                   = GetBool("sLenderCaseNumLckd");
            dataLoan.sNoteIR_rep                          = GetString("sNoteIR");
            dataLoan.sPurchPrice_rep                      = GetString("sPurchPrice");
            dataLoan.sSpAddr                              = GetString("sSpAddr");
            dataLoan.sSpAge                               = GetString("sSpAge");
            dataLoan.sSpBathCount                         = GetString("sSpBathCount");
            dataLoan.sSpBedroomCount                      = GetString("sSpBedroomCount");
            dataLoan.sSpCity                              = GetString("sSpCity");
            dataLoan.sSpCounty                            = GetString("sSpCounty");
            dataLoan.sSpLivingSqf                         = GetString("sSpLivingSqf");
            dataLoan.sSpRoomCount                         = GetString("sSpRoomCount");
            dataLoan.sSpState                             = GetString("sSpState");
            dataLoan.sSpT                                 = (E_sSpT) GetInt("sSpT");
            dataLoan.sSpZip                               = GetString("sSpZip");
            dataLoan.sTerm_rep                            = GetString("sTerm");
            dataLoan.sUnitsNum_rep                        = GetString("sUnitsNum");
            dataLoan.sVaApprT                             = (E_sVaApprT) GetInt("sVaApprT");
            dataLoan.sVaAppraisalOrSarAdjustmentTri       = GetTriState("sVaAppraisalOrSarAdjustmentTri");
            dataLoan.sVaAutoUnderwritingT                 = (E_sVaAutoUnderwritingT) GetInt("sVaAutoUnderwritingT");
            dataLoan.sVaEnergyImprovAmt_rep               = GetString("sVaEnergyImprovAmt");
            dataLoan.sVaEnergyImprovIsInsulation          = GetBool("sVaEnergyImprovIsInsulation");
            dataLoan.sVaEnergyImprovIsMajorSystem         = GetBool("sVaEnergyImprovIsMajorSystem");
            dataLoan.sVaEnergyImprovIsNewFeature          = GetBool("sVaEnergyImprovIsNewFeature");
            dataLoan.sVaEnergyImprovIsOther               = GetBool("sVaEnergyImprovIsOther");
            dataLoan.sVaEnergyImprovIsSolar               = GetBool("sVaEnergyImprovIsSolar");
            dataLoan.sVaFinMethT                          = (E_sVaFinMethT) GetInt("sVaFinMethT");
            dataLoan.sVaFinMethTLckd                      = GetBool("sVaFinMethTLckd");
            dataLoan.sVaHybridArmT                        = (E_sVaHybridArmT) GetInt("sVaHybridArmT");
            dataLoan.sVaIsAutoIrrrlProc                   = GetBool("sVaIsAutoIrrrlProc");
            dataLoan.sVaIsAutoProc                        = GetBool("sVaIsAutoProc");
            dataLoan.sVaIsPriorApprovalProc               = GetBool("sVaIsPriorApprovalProc");
            dataLoan.sVaLCodeT                            = (E_sVaLCodeT) GetInt("sVaLCodeT");
            dataLoan.sVaLCodeTLckd                        = GetBool("sVaLCodeTLckd");
            dataLoan.sVaLDiscntLckd                       = GetBool("sVaLDiscntLckd");
            dataLoan.sVaLDiscntPbbLckd                    = GetBool("sVaLDiscntPbbLckd");
            dataLoan.sVaLDiscntPbb_rep                    = GetString("sVaLDiscntPbb");
            dataLoan.sVaLDiscntPcPbb_rep                  = GetString("sVaLDiscntPcPbb");
            dataLoan.sVaLDiscntPc_rep                     = GetString("sVaLDiscntPc");
            dataLoan.sVaLDiscnt_rep                       = GetString("sVaLDiscnt");
            dataLoan.sVaLPurposeT                         = (E_sVaLPurposeT) GetInt("sVaLPurposeT");
            dataLoan.sVaLPurposeTLckd                     = GetBool("sVaLPurposeTLckd");
            dataLoan.sVaLenSarId                          = GetString("sVaLenSarId");
            dataLoan.sVaManufacturedHomeT                 = (E_sVaManufacturedHomeT) GetInt("sVaManufacturedHomeT");
            dataLoan.sVaMcrvNum                           = GetString("sVaMcrvNum");
            dataLoan.sVaOwnershipT                        = (E_sVaOwnershipT) GetInt("sVaOwnershipT");
            dataLoan.sVaPropDesignationT                  = (E_sVaPropDesignationT) GetInt("sVaPropDesignationT");
            dataLoan.sVaRiskT                             = (E_sVaRiskT) GetInt("sVaRiskT");
            dataLoan.sVaSarNotifIssuedD_rep               = GetString("sVaSarNotifIssuedD");
            dataLoan.sVaStructureT                        = (E_sVaStructureT) GetInt("sVaStructureT");
            dataLoan.sVaVetMedianCrScoreLckd              = GetBool("sVaVetMedianCrScoreLckd");
            dataLoan.sVaVetMedianCrScore_rep              = GetString("sVaVetMedianCrScore");

            dataLoan.sVaFfExemptTri = GetTriState("sVaFfExemptTri");
            dataLoan.sVaIrrrlsUsedOnlyPdInFullLNum = GetString("sVaIrrrlsUsedOnlyPdInFullLNum");
            dataLoan.sVaIrrrlsUsedOnlyOrigLAmt_rep = GetString("sVaIrrrlsUsedOnlyOrigLAmt");
            dataLoan.sVaIrrrlsUsedOnlyOrigIR_rep = GetString("sVaIrrrlsUsedOnlyOrigIR");
            dataLoan.sVaIrrrlsUsedOnlyRemarks = GetString("sVaIrrrlsUsedOnlyRemarks");

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("aAsstLiqTot",                          dataApp.aAsstLiqTot_rep);
            SetResult("aBDob",                                dataApp.aBDob_rep);
            SetResult("aBFirstNm",                            dataApp.aBFirstNm);
            SetResult("aBGenderT",                            dataApp.aBGender);
            SetResult("aBHispanicT",                          dataApp.aBHispanicT);
            SetResult("aBIsAmericanIndian",                   dataApp.aBIsAmericanIndian);
            SetResult("aBIsAsian",                            dataApp.aBIsAsian);
            SetResult("aBIsBlack",                            dataApp.aBIsBlack);
            SetResult("aBIsPacificIslander",                  dataApp.aBIsPacificIslander);
            SetResult("aBIsWhite",                            dataApp.aBIsWhite);
            SetResult("aBLastNm",                             dataApp.aBLastNm);
            SetResult("aBMidNm",                              dataApp.aBMidNm);
            SetResult("aBSsn",                                dataApp.aBSsn);
            SetResult("aBSuffix",                             dataApp.aBSuffix);
            SetResult("aVaCEmplmtI",                          dataApp.aVaCEmplmtI_rep);
            SetResult("aVaCEmplmtILckd",                      dataApp.aVaCEmplmtILckd);
            SetResult("aVaEntitleAmt",                        dataApp.aVaEntitleAmt_rep);
            SetResult("aVaEntitleCode",                       dataApp.aVaEntitleCode);
            SetResult("aVaFamilySuportGuidelineAmt",          dataApp.aVaFamilySuportGuidelineAmt_rep);
            SetResult("aVaFamilySupportBal",                  dataApp.aVaFamilySupportBal_rep);
            SetResult("aVaIsCoborIConsidered",                dataApp.aVaIsCoborIConsidered);
            SetResult("aVaIsVeteranFirstTimeBuyerTri",        dataApp.aVaIsVeteranFirstTimeBuyerTri);
            SetResult("aVaMilitaryStatT",                     dataApp.aVaMilitaryStatT);
            SetResult("aVaRatio",                             dataApp.aVaRatio_rep);
            SetResult("aVaServiceBranchT",                    dataApp.aVaServiceBranchT);
            SetResult("aVaTotMonGrossI",                      dataApp.aVaTotMonGrossI_rep);
            SetResult("sAgencyCaseNum",                       dataLoan.sAgencyCaseNum);
            SetResult("aFHABorrCertInformedPropVal",          dataApp.aFHABorrCertInformedPropVal_rep);
            SetResult("sClosedD",                             dataLoan.sClosedD_rep);
            SetResult("sVALenderIdCode",                     dataLoan.sVALenderIdCode);
            SetResult("sVASponsorAgentIdCode",               dataLoan.sVASponsorAgentIdCode);
            SetResult("sFinalLAmt",                           dataLoan.sFinalLAmt_rep);
            SetResult("sIsProcessedUnderAutoUnderwritingTri", dataLoan.sIsProcessedUnderAutoUnderwritingTri);
            SetResult("sLenderCaseNum",                       dataLoan.sLenderCaseNum);
            SetResult("sLenderCaseNumLckd",                   dataLoan.sLenderCaseNumLckd);
            SetResult("sNoteIR",                              dataLoan.sNoteIR_rep);
            SetResult("sPurchPrice",                          dataLoan.sPurchPrice_rep);
            SetResult("sSpAddr",                              dataLoan.sSpAddr);
            SetResult("sSpAge",                               dataLoan.sSpAge);
            SetResult("sSpBathCount",                         dataLoan.sSpBathCount);
            SetResult("sSpBedroomCount",                      dataLoan.sSpBedroomCount);
            SetResult("sSpCity",                              dataLoan.sSpCity);
            SetResult("sSpCounty",                            dataLoan.sSpCounty);
            SetResult("sSpLivingSqf",                         dataLoan.sSpLivingSqf);
            SetResult("sSpRoomCount",                         dataLoan.sSpRoomCount);
            SetResult("sSpState",                             dataLoan.sSpState);
            SetResult("sSpT",                                 dataLoan.sSpT);
            SetResult("sSpZip",                               dataLoan.sSpZip);
            SetResult("sTerm",                                dataLoan.sTerm_rep);
            SetResult("sUnitsNum",                            dataLoan.sUnitsNum_rep);
            SetResult("sVaApprT",                             dataLoan.sVaApprT);
            SetResult("sVaAppraisalOrSarAdjustmentTri",       dataLoan.sVaAppraisalOrSarAdjustmentTri);
            SetResult("sVaAutoUnderwritingT",                 dataLoan.sVaAutoUnderwritingT);
            SetResult("sVaEnergyImprovAmt",                   dataLoan.sVaEnergyImprovAmt_rep);
            SetResult("sVaEnergyImprovIsInsulation",          dataLoan.sVaEnergyImprovIsInsulation);
            SetResult("sVaEnergyImprovIsMajorSystem",         dataLoan.sVaEnergyImprovIsMajorSystem);
            SetResult("sVaEnergyImprovIsNewFeature",          dataLoan.sVaEnergyImprovIsNewFeature);
            SetResult("sVaEnergyImprovIsOther",               dataLoan.sVaEnergyImprovIsOther);
            SetResult("sVaEnergyImprovIsSolar",               dataLoan.sVaEnergyImprovIsSolar);
            SetResult("sVaFinMethT",                          dataLoan.sVaFinMethT);
            SetResult("sVaFinMethTLckd",                      dataLoan.sVaFinMethTLckd);
            SetResult("sVaHybridArmT",                        dataLoan.sVaHybridArmT);
            SetResult("sVaIsAutoIrrrlProc",                   dataLoan.sVaIsAutoIrrrlProc);
            SetResult("sVaIsAutoProc",                        dataLoan.sVaIsAutoProc);
            SetResult("sVaIsPriorApprovalProc",               dataLoan.sVaIsPriorApprovalProc);
            SetResult("sVaLCodeT",                            dataLoan.sVaLCodeT);
            SetResult("sVaLCodeTLckd",                        dataLoan.sVaLCodeTLckd);
            SetResult("sVaLDiscntLckd",                       dataLoan.sVaLDiscntLckd);
            SetResult("sVaLDiscntPbbLckd",                    dataLoan.sVaLDiscntPbbLckd);
            SetResult("sVaLDiscntPbb",                        dataLoan.sVaLDiscntPbb_rep);
            SetResult("sVaLDiscntPcPbb",                      dataLoan.sVaLDiscntPcPbb_rep);
            SetResult("sVaLDiscntPc",                         dataLoan.sVaLDiscntPc_rep);
            SetResult("sVaLDiscnt",                           dataLoan.sVaLDiscnt_rep);
            SetResult("sVaLPurposeT",                         dataLoan.sVaLPurposeT);
            SetResult("sVaLPurposeTLckd",                     dataLoan.sVaLPurposeTLckd);
            SetResult("sVaLenSarId",                          dataLoan.sVaLenSarId);
            SetResult("sVaManufacturedHomeT",                 dataLoan.sVaManufacturedHomeT);
            SetResult("sVaMcrvNum",                           dataLoan.sVaMcrvNum);
            SetResult("sVaOwnershipT",                        dataLoan.sVaOwnershipT);
            SetResult("sVaPropDesignationT",                  dataLoan.sVaPropDesignationT);
            SetResult("sVaRiskT",                             dataLoan.sVaRiskT);
            SetResult("sVaSarNotifIssuedD",                   dataLoan.sVaSarNotifIssuedD_rep);
            SetResult("sVaStructureT",                        dataLoan.sVaStructureT);
            SetResult("sVaVetMedianCrScoreLckd",              dataLoan.sVaVetMedianCrScoreLckd);
            SetResult("sVaVetMedianCrScore",                  dataLoan.sVaVetMedianCrScore_rep);

            SetResult("sVaFfExemptTri", dataLoan.sVaFfExemptTri);
            SetResult("sVaIrrrlsUsedOnlyPdInFullLNum", dataLoan.sVaIrrrlsUsedOnlyPdInFullLNum);
            SetResult("sVaIrrrlsUsedOnlyOrigLAmt", dataLoan.sVaIrrrlsUsedOnlyOrigLAmt_rep);
            SetResult("sVaIrrrlsUsedOnlyOrigIR", dataLoan.sVaIrrrlsUsedOnlyOrigIR_rep);
            SetResult("sVaIrrrlsUsedOnlyRemarks", dataLoan.sVaIrrrlsUsedOnlyRemarks);
            SetResult("sVaPriorLoanT", dataLoan.sVaPriorLoanT);
        }

    }
	/// <summary>
	/// Summary description for VALoanSummaryService.
	/// </summary>
	public partial class VALoanSummaryService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new VALoanSummaryServiceItem());
        }
	}
}
