using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Migration;

namespace LendersOfficeApp.newlos.VA
{
	public partial class VALoanSummary : BaseLoanPage
	{
        #region Protected member variables

        protected System.Web.UI.WebControls.DropDownList DropDownList12;
        #endregion

        protected void PageInit(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            this.PageID = "VA_26_0286";
            this.PageTitle = "VA Loan Summary Sheet";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CVA_26_0286_1PDF);

            sSpZip.SmartZipcode(sSpCity, sSpState, sSpCounty);
            Tools.Bind_GenderT(aBGenderT, shouldIncludeBothOption: true);
            Tools.Bind_aHispanicT(aBHispanicT, shouldIncludeBothOption: true);
            Tools.Bind_sVaPriorLoanT(sVaPriorLoanT);

            Tools.Bind_TriState(sVaAppraisalOrSarAdjustmentTri);
            Tools.Bind_TriState(aVaIsVeteranFirstTimeBuyerTri);
            Tools.Bind_TriState(sIsProcessedUnderAutoUnderwritingTri);

            sVaAppraisalOrSarAdjustmentTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            aVaIsVeteranFirstTimeBuyerTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sIsProcessedUnderAutoUnderwritingTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sVaFfExemptTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);

            Tools.Bind_aVaServiceBranchT(aVaServiceBranchT);
            Tools.Bind_aVaMilitaryStatT(aVaMilitaryStatT);
            Tools.Bind_sVaLPurposeT(sVaLPurposeT);
            Tools.Bind_sVaLCodeT(sVaLCodeT);
            Tools.Bind_sVaFinMethT(sVaFinMethT);
            Tools.Bind_sVaHybridArmT(sVaHybridArmT);
            Tools.Bind_sVaOwnershipT(sVaOwnershipT);
            Tools.Bind_sSpT(sSpT);
            Tools.Bind_sVaApprT(sVaApprT);
            Tools.Bind_sVaStructureT(sVaStructureT);
            Tools.Bind_sVaPropDesignationT(sVaPropDesignationT);
            Tools.Bind_sVaManufacturedHomeT(sVaManufacturedHomeT);
            Tools.Bind_sVaAutoUnderwritingT(sVaAutoUnderwritingT);
            Tools.Bind_sVaRiskT(sVaRiskT);

            sVALenderIdCode.Items.Add("Key Person");
            sVALenderIdCode.Items.Add("Lending Institution");
            sVALenderIdCode.Items.Add("Loan Broker");

            aVaEntitleCode.Items.Add("01");
            aVaEntitleCode.Items.Add("02");
            aVaEntitleCode.Items.Add("03");
            aVaEntitleCode.Items.Add("04");
            aVaEntitleCode.Items.Add("05");
            aVaEntitleCode.Items.Add("06");
            aVaEntitleCode.Items.Add("07");
            aVaEntitleCode.Items.Add("08");
            aVaEntitleCode.Items.Add("09");
            aVaEntitleCode.Items.Add("10");
            aVaEntitleCode.Items.Add("11");

			 // 01-30-08 av 18913
			sSpState.Attributes.Add( "onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event);" );

            Tools.BindSuffix(aBSuffix);
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VALoanSummary));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            Tools.SetDropDownListValue(sVaPriorLoanT, dataLoan.sVaPriorLoanT);

            aAsstLiqTot.Text = dataApp.aAsstLiqTot_rep;
            aBDob.Text = dataApp.aBDob_rep;
            aBFirstNm.Text = dataApp.aBFirstNm;
            Tools.SetDropDownListValue(aBGenderT, dataApp.aBGender);
            Tools.SetDropDownListValue(aBHispanicT, dataApp.aBHispanicT);
            aBIsAmericanIndian.Checked = dataApp.aBIsAmericanIndian;
            aBIsAsian.Checked = dataApp.aBIsAsian;
            aBIsBlack.Checked = dataApp.aBIsBlack;
            aBIsPacificIslander.Checked = dataApp.aBIsPacificIslander;
            aBIsWhite.Checked = dataApp.aBIsWhite;
            aBLastNm.Text = dataApp.aBLastNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBSsn.Text = dataApp.aBSsn;
            aBSuffix.Text = dataApp.aBSuffix;
            aVaCEmplmtI.Text = dataApp.aVaCEmplmtI_rep;
            aVaCEmplmtILckd.Checked = dataApp.aVaCEmplmtILckd;
            aVaEntitleAmt.Text = dataApp.aVaEntitleAmt_rep;
            aVaEntitleCode.Text = dataApp.aVaEntitleCode;
            aVaFamilySuportGuidelineAmt.Text = dataApp.aVaFamilySuportGuidelineAmt_rep;
            aVaFamilySupportBal.Text = dataApp.aVaFamilySupportBal_rep;
            Tools.Set_TriState(aVaIsVeteranFirstTimeBuyerTri, dataApp.aVaIsVeteranFirstTimeBuyerTri);
            Tools.SetDropDownListValue(aVaMilitaryStatT, dataApp.aVaMilitaryStatT);
            aVaRatio.Text = dataApp.aVaRatio_rep;
            Tools.SetDropDownListValue(aVaServiceBranchT, dataApp.aVaServiceBranchT);
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            aFHABorrCertInformedPropVal.Text = dataApp.aFHABorrCertInformedPropVal_rep;
            sClosedD.Text = dataLoan.sClosedD_rep;
            sVALenderIdCode.Text = dataLoan.sVALenderIdCode;
            sVASponsorAgentIdCode.Text = dataLoan.sVASponsorAgentIdCode;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            Tools.Set_TriState(sIsProcessedUnderAutoUnderwritingTri, dataLoan.sIsProcessedUnderAutoUnderwritingTri);
            sLenderCaseNum.Text = dataLoan.sLenderCaseNum;
            sLenderCaseNumLckd.Checked = dataLoan.sLenderCaseNumLckd;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sPurchPrice.ReadOnly = (dataLoan.sLPurposeT != E_sLPurposeT.Purchase 
                && dataLoan.sLPurposeT != E_sLPurposeT.Construct 
                && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm) || IsReadOnly; // 11/05/07 mf. OPM 18801
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpAge.Text = dataLoan.sSpAge;
            sSpBathCount.Text = dataLoan.sSpBathCount;
            sSpBedroomCount.Text = dataLoan.sSpBedroomCount;
            sSpCity.Text = dataLoan.sSpCity;
			// 01-14-08 av 18913 
			Tools.Bind_sSpCounty( dataLoan.sSpState, sSpCounty, true) ; 
			Tools.SetDropDownListCaseInsensitive( sSpCounty, dataLoan.sSpCounty );
			
            sSpLivingSqf.Text = dataLoan.sSpLivingSqf;
            sSpRoomCount.Text = dataLoan.sSpRoomCount;
            sSpState.Value = dataLoan.sSpState;
            Tools.SetDropDownListValue(sSpT, dataLoan.sSpT);
            sSpZip.Text = dataLoan.sSpZip;
            sTerm.Text = dataLoan.sTerm_rep;
            sTerm.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sUnitsNum.Text = dataLoan.sUnitsNum_rep;
            Tools.SetDropDownListValue(sVaApprT, dataLoan.sVaApprT);
            Tools.Set_TriState(sVaAppraisalOrSarAdjustmentTri, dataLoan.sVaAppraisalOrSarAdjustmentTri);
            Tools.SetDropDownListValue(sVaAutoUnderwritingT, dataLoan.sVaAutoUnderwritingT);
            sVaEnergyImprovAmt.Text = dataLoan.sVaEnergyImprovAmt_rep;
            sVaEnergyImprovIsInsulation.Checked = dataLoan.sVaEnergyImprovIsInsulation;
            sVaEnergyImprovIsMajorSystem.Checked = dataLoan.sVaEnergyImprovIsMajorSystem;
            sVaEnergyImprovIsNewFeature.Checked = dataLoan.sVaEnergyImprovIsNewFeature;
            sVaEnergyImprovIsOther.Checked = dataLoan.sVaEnergyImprovIsOther;
            sVaEnergyImprovIsSolar.Checked = dataLoan.sVaEnergyImprovIsSolar;
            Tools.SetDropDownListValue(sVaFinMethT, dataLoan.sVaFinMethT);
            sVaFinMethTLckd.Checked = dataLoan.sVaFinMethTLckd;
            Tools.SetDropDownListValue(sVaHybridArmT, dataLoan.sVaHybridArmT);
            sVaIsAutoIrrrlProc.Checked = dataLoan.sVaIsAutoIrrrlProc;
            aVaIsCoborIConsidered.Checked = dataApp.aVaIsCoborIConsidered;
            sVaIsPriorApprovalProc.Checked = dataLoan.sVaIsPriorApprovalProc;
            Tools.SetDropDownListValue(sVaLCodeT, dataLoan.sVaLCodeT);
            sVaLCodeTLckd.Checked = dataLoan.sVaLCodeTLckd;
            sVaLDiscnt.Text = dataLoan.sVaLDiscnt_rep;
            sVaLDiscntLckd.Checked = dataLoan.sVaLDiscntLckd;
            sVaLDiscntPbb.Text = dataLoan.sVaLDiscntPbb_rep;
            sVaLDiscntPbbLckd.Checked = dataLoan.sVaLDiscntPbbLckd;
            sVaLDiscntPc.Text = dataLoan.sVaLDiscntPc_rep;
            sVaLDiscntPcPbb.Text = dataLoan.sVaLDiscntPcPbb_rep;
            Tools.SetDropDownListValue(sVaLPurposeT, dataLoan.sVaLPurposeT);
            sVaLPurposeTLckd.Checked = dataLoan.sVaLPurposeTLckd;
            sVaLenSarId.Text = dataLoan.sVaLenSarId;
            Tools.SetDropDownListValue(sVaManufacturedHomeT, dataLoan.sVaManufacturedHomeT);
            sVaMcrvNum.Text = dataLoan.sVaMcrvNum;
            Tools.SetDropDownListValue(sVaOwnershipT, dataLoan.sVaOwnershipT);
            Tools.SetDropDownListValue(sVaPropDesignationT, dataLoan.sVaPropDesignationT);
            Tools.SetDropDownListValue(sVaRiskT, dataLoan.sVaRiskT);
            sVaSarNotifIssuedD.Text = dataLoan.sVaSarNotifIssuedD_rep;
            Tools.SetDropDownListValue(sVaStructureT, dataLoan.sVaStructureT);
            aVaTotMonGrossI.Text = dataApp.aVaTotMonGrossI_rep;
            sVaVetMedianCrScore.Text = dataLoan.sVaVetMedianCrScore_rep;
            sVaVetMedianCrScoreLckd.Checked = dataLoan.sVaVetMedianCrScoreLckd;
            sVaIsAutoProc.Checked = dataLoan.sVaIsAutoProc;

            Tools.Set_TriState(sVaFfExemptTri, dataLoan.sVaFfExemptTri);
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V2_CalcVaFundFeeExempt_Opm204251))
            {
                sVaFfExemptTri.Enabled = false;
            }
            sVaIrrrlsUsedOnlyPdInFullLNum.Text = dataLoan.sVaIrrrlsUsedOnlyPdInFullLNum;
            sVaIrrrlsUsedOnlyOrigLAmt.Text = dataLoan.sVaIrrrlsUsedOnlyOrigLAmt_rep;
            sVaIrrrlsUsedOnlyOrigIR.Text = dataLoan.sVaIrrrlsUsedOnlyOrigIR_rep;
            sVaIrrrlsUsedOnlyRemarks.Text = dataLoan.sVaIrrrlsUsedOnlyRemarks;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            this.UseNewFramework = true;
            this.IsAppSpecific = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        
        }
        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));


        }
	}
}
