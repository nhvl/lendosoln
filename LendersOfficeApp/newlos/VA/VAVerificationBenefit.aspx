<%@ Page Language="c#" CodeBehind="VAVerificationBenefit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.VA.VAVerificationBenefit" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
  <title>VAVerificationBenefit</title>
</head>
<body ms_positioning="FlowLayout" class="RightBackground">
  <form id="VAVerificationBenefit" method="post" runat="server">
  <table id="Table1" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td class="MainRightHeader" nowrap>VA Verification of Benefit (26-8937)</td>
    </tr>
    <tr>
      <td nowrap>
        <table class="InsetBorder" id="Table2" cellspacing="0" cellpadding="0" width="98%" border="0">
          <tr>
            <td nowrap class="FieldLabel">Lender Name</td>
            <td nowrap width="98%">
              <asp:TextBox ID="VA26_8937LenderCompanyName" runat="server" Width="254px"></asp:TextBox></td>
          </tr>
          <tr>
            <td nowrap class="FieldLabel">Address</td>
            <td nowrap>
              <asp:TextBox ID="VA26_8937LenderStreetAddr" runat="server" Width="254px"></asp:TextBox></td>
          </tr>
          <tr>
            <td nowrap></td>
            <td nowrap>
              <asp:TextBox ID="VA26_8937LenderCity" runat="server"></asp:TextBox><ml:StateDropDownList ID="VA26_8937LenderState" runat="server"></ml:StateDropDownList>
              <ml:ZipcodeTextBox ID="VA26_8937LenderZip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td nowrap>
        <table class="InsetBorder" id="Table3" cellspacing="0" cellpadding="0" width="98%" border="0">
          <tr>
            <td class="FieldLabel" nowrap>1. Veteran First Name</td>
            <td nowrap width="100%">
              <asp:TextBox ID="aBFirstNm" runat="server"></asp:TextBox></td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Middle Name</td>
            <td nowrap width="100%">
              <asp:TextBox ID="aBMidNm" runat="server"></asp:TextBox></td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Last Name</td>
            <td nowrap width="100%">
              <asp:TextBox ID="aBLastNm" runat="server"></asp:TextBox></td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Suffix</td>
            <td nowrap width="100%">
              <ml:ComboBox ID="aBSuffix" runat="server"></ml:ComboBox></td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap>2. Current Address</td>
            <td nowrap width="100%">
              <asp:TextBox ID="aBAddr" runat="server" Width="254px"></asp:TextBox></td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap></td>
            <td nowrap width="100%">
              <asp:TextBox ID="aBCity" runat="server"></asp:TextBox><ml:StateDropDownList ID="aBState" runat="server"></ml:StateDropDownList>
              <ml:ZipcodeTextBox ID="aBZip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox></td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap>3. Date of Birth</td>
            <td nowrap width="100%">
              <ml:DateTextBox ID="aBDob" runat="server" Width="75" preset="date"></ml:DateTextBox></td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap>4. VA Claim Folder Number </td>
            <td nowrap width="100%"><asp:TextBox ID="aVaClaimFolderNum" runat="server" /></td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap>5. Service Number</td>
            <td nowrap width="100%"><asp:TextBox ID="aVaServiceNum" runat="server" /></td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap>6. Social Security Number</td>
            <td nowrap width="100%">
              <ml:SSNTextBox ID="aBSsn" runat="server" Width="75px" preset="ssn" /></td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap colspan="2">I hereby certify that I&nbsp;&nbsp;<asp:CheckBoxList ID="aVaIndebtCertifyTri" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
              <asp:ListItem Value="1">DO</asp:ListItem>
              <asp:ListItem Value="2">DO NOT</asp:ListItem>
            </asp:CheckBoxList>
              &nbsp;have a VA benefit related indebtedness to my knowledge</td>
          </tr>
          <tr>
            <td class="FieldLabel" nowrap="nowrap" colspan="2">I hereby certify that I&nbsp;&nbsp;<asp:CheckBoxList ID="aVAFileClaimDisabilityCertifyTri" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
              <asp:ListItem Value="1">HAVE</asp:ListItem>
              <asp:ListItem Value="2">HAVE NOT</asp:ListItem>
            </asp:CheckBoxList>
            &nbsp;filed a claim for VA disability benefits prior to discharge from active duty service (I am presently still on active duty.)
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
</body>
</html>
