﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
    public partial class VALoanComparisonServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VALoanComparisonServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sVaLoanNumCurrentLoan = GetString("sVaLoanNumCurrentLoan");
            dataLoan.sVaLoanAmtCurrentLoan_rep = GetString("sVaLoanAmtCurrentLoan");
            dataLoan.sVaNoteIrCurrentLoan_rep = GetString("sVaNoteIrCurrentLoan");
            dataLoan.sVaTermCurrentLoan_rep = GetString("sVaTermCurrentLoan");
            dataLoan.sVaPICurrentLoan_rep = GetString("sVaPICurrentLoan");
            dataLoan.sVaMonthlyPmtCurrentLoan_rep = GetString("sVaMonthlyPmtCurrentLoan");
            dataLoan.sVaNewPmtQualified = GetBool("sVaNewPmtQualified");
            dataLoan.aVaTotalClosingCost_rep = GetString("aVaTotalClosingCost");
            dataLoan.sVaTotalClosingCostLckd = GetBool("aVaTotalClosingCostLckd");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sVaLoanNumCurrentLoan", dataLoan.sVaLoanNumCurrentLoan);
            SetResult("sVaLoanAmtCurrentLoan", dataLoan.sVaLoanAmtCurrentLoan_rep);
            SetResult("sVaNoteIrCurrentLoan", dataLoan.sVaNoteIrCurrentLoan_rep);
            SetResult("sVaTermCurrentLoan", dataLoan.sVaTermCurrentLoan_rep);
            SetResult("sVaMonthlyPmtCurrentLoan", dataLoan.sVaMonthlyPmtCurrentLoan_rep);
            SetResult("sVaNewPmtQualified", dataLoan.sVaNewPmtQualified);
            SetResult("aVaTotalClosingCost", dataLoan.aVaTotalClosingCost_rep);
            SetResult("aVaTotalClosingCostLckd", dataLoan.sVaTotalClosingCostLckd);
            SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            SetResult("sTerm", dataLoan.sTerm);
            SetResult("sRateAmortT", dataLoan.sRateAmortT);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            SetResult("sVaLoanCompMonthlyPmtDecreaseAmt", dataLoan.sVaLoanCompMonthlyPmtDecreaseAmt_rep);
            SetResult("sVaLoanCompRecoupCostsTime", dataLoan.sVaLoanCompRecoupCostsTime_rep);
            SetResult("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            SetResult("sVaPICurrentLoan", dataLoan.sVaPICurrentLoan_rep);
        }
    }
    public partial class VALoanComparisonService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new VALoanComparisonServiceItem());
        }
    }
}
