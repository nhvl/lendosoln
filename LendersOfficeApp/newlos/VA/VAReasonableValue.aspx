<%@ Page language="c#" Codebehind="VAReasonableValue.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.VA.VAReasonableValue" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>VAReasonableValue</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
<body class=RightBackground scroll=yes MS_POSITIONING="FlowLayout">
<script language=javascript>
<!--
function _init() {
  disableDDL(<%= AspxTools.JsGetElementById(sVaBuildingStatusT) %>, !<%= AspxTools.JsGetElementById(sVaBuildingStatusTLckd) %>.checked);
}
//-->
</script>

<form id=VAReasonableValue method=post runat="server">
<table id=Table1 cellSpacing=0 cellPadding=0 border=0>
  <tr>
    <td class=MainRightHeader noWrap>Request for 
      Determination of Reasonable Value (VA 26-1805)</td></tr>
  <tr>
    <td noWrap>
      <table id=Table2 cellSpacing=0 cellPadding=0 border=0>
        <tr>
          <td class=FieldLabel noWrap>1. Case Number</td>
          <td noWrap><asp:textbox id=sAgencyCaseNum runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap>2. Property 
          Address</td>
          <td noWrap><asp:textbox id=sSpAddr runat="server" Width="389px"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap></td>
          <td noWrap><asp:textbox id=sSpCity runat="server" Width="281px"></asp:textbox><ml:statedropdownlist id=sSpState runat="server" IncludeTerritories="false"></ml:statedropdownlist><ml:zipcodetextbox id=sSpZip runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap>County</td>
          <td noWrap><asp:DropDownList id=sSpCounty runat="server"></asp:DropDownList></td></tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap>3. Legal 
            Description</td>
          <td noWrap></td></tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap colSpan=2 
            >&nbsp;&nbsp;&nbsp; <asp:textbox id=sSpLegalDesc runat="server" Width="441px" Height="77px" TextMode="MultiLine"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap colSpan=2 
            >4. Title Limitations</td></tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap colSpan=2 
            ><table id=Table3 cellSpacing=0 cellPadding=0 
            border=0>
              <tr>
                <td noWrap>&nbsp;&nbsp;&nbsp; <asp:textbox id=sVaTitleLimitDesc runat="server" Width="303px" Height="74px" TextMode="MultiLine"></asp:textbox></td></tr>
              <tr>
                <td noWrap class=FieldLabel>&nbsp;&nbsp;&nbsp; <asp:checkbox id=sVaTitleLimitIsCondo runat="server" Text="Condominium"></asp:checkbox><asp:checkbox id=sVaTitleLimitIsPud runat="server" Text="Planned Unit Dev"></asp:checkbox></td></tr></table></td></tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap colSpan=2 
            >5. Name and Address of Firm Making 
Request</td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>Name</td>
          <td class=FieldLabel vAlign=top noWrap ><asp:TextBox id=VA26_1805FirmMakingRequestCompanyName runat="server" Width="255px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>Address</td>
          <td class=FieldLabel valign=top nowrap><asp:TextBox id=VA26_1805FirmMakingRequestStreetAddr runat="server" Width="255px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap></td>
          <td class=FieldLabel valign=top nowrap><asp:TextBox id=VA26_1805FirmMakingRequestCity runat="server"></asp:TextBox><ml:StateDropDownList id=VA26_1805FirmMakingRequestState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=VA26_1805FirmMakingRequestZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td class="FieldLabel" valign="top" nowrap>5b. Email Address to be notified</td>
          <td class="FieldLabel" valign="top" nowrap><asp:TextBox id="VA26_1805FirmMakingRequestNotificationEmail" runat="server" width="255px"></asp:TextBox></td>
        </tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap>6. Lot 
            Dimensions</td>
          <td noWrap>
            <table id=Table4 cellSpacing=0 cellPadding=0 border=0 
            >
              <tr>
                <td noWrap><asp:textbox id=sSpLotDimension runat="server"></asp:textbox></td></tr>
              <tr>
                <td noWrap class=FieldLabel><asp:checkbox id=sSpLotIsIrregular runat="server" Text="Irregular - "></asp:checkbox>&nbsp;<asp:textbox id=sSpLotIrregularSqf runat="server" Width="68px"></asp:textbox>&nbsp;Sq/ft&nbsp;&nbsp;</td></tr>
              <tr>
                <td noWrap class=FieldLabel><asp:checkbox id=sSpLotIsAcres runat="server" Text="Acres -"></asp:checkbox><asp:textbox id=sSpLotAcres runat="server" Width="66px"></asp:textbox></td></tr></table></td></tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap>7. 
            Utilities</td>
          <td noWrap>
            <table id=Table5 cellSpacing=0 cellPadding=0 border=0>
              <tr>
                <td noWrap class=FieldLabel>Electricity</td>
                <td noWrap><asp:DropDownList id=sSpUtilElecT runat="server"></asp:DropDownList></td></tr>
              <tr>
                <td noWrap class=FieldLabel>Gas</td>
                <td noWrap align=left><asp:DropDownList id=sSpUtilGasT runat="server"></asp:DropDownList></td></tr>
              <tr>
                <td noWrap class=FieldLabel>Water</td>
                <td noWrap align=left><asp:DropDownList id=sSpUtilWaterT runat="server"></asp:DropDownList></td></tr>
              <tr>
                <td noWrap class=FieldLabel>San. Sewer</td>
                <td noWrap align=left><asp:DropDownList id=sSpUtilSanSewerT runat="server"></asp:DropDownList></td></tr></table></td></tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap>8. 
          Equip</td>
          <td noWrap>
            <table id=Table6 cellSpacing=0 cellPadding=0 border=0>
              <tr>
                <td noWrap class=FieldLabel><asp:checkbox id=sSpHasOven runat="server" Text="Range/Oven"></asp:checkbox></td>
                <td noWrap class=FieldLabel><asp:checkbox id=sSpHasClothesWasher runat="server" Text="Clothes Washer"></asp:checkbox></td>
                <td noWrap class=FieldLabel><asp:checkbox id=sSpHasVentFan runat="server" Text="Vent. Fan"></asp:checkbox></td></tr>
              <tr>
                <td noWrap class=FieldLabel><asp:checkbox id=sSpHasRefrig runat="server" Text="Refrig."></asp:checkbox></td>
                <td noWrap class=FieldLabel><asp:checkbox id=sSpHasDryer runat="server" Text="Dryer"></asp:checkbox></td>
                <td noWrap class=FieldLabel><asp:checkbox id=sSpHasWwCarpet runat="server" Text="W/W Carpet"></asp:checkbox></td></tr>
              <tr>
                <td noWrap class=FieldLabel><asp:checkbox id=sSpHasDishWasher runat="server" Text="Dish Washer"></asp:checkbox></td>
                <td noWrap class=FieldLabel><asp:checkbox id=sSpHasGarbageDisposal runat="server" Text="Garbage Disposal"></asp:checkbox></td>
              </tr>
            </table>
            </td>
        </tr>
        <tr>
          <td class=FieldLabel vAlign=top noWrap>9. 
            Building Status</td>
          <td noWrap class=FieldLabel><asp:CheckBox id=sVaBuildingStatusTLckd runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox><asp:DropDownList id=sVaBuildingStatusT runat="server"></asp:DropDownList></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>10. Building Type</td>
          <td nowrap><asp:DropDownList id=sVaBuildingT runat="server"></asp:DropDownList></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>11. Factory Fabricated</td>
          <td nowrap class=FieldLabel><asp:CheckBoxList id=sVaIsFactoryFabricatedTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
</asp:CheckBoxList></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>12a. No. Of Buildings</td>
          <td 
        nowrap><asp:TextBox id=sVaNumOfBuildings runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>12b. No. Of&nbsp; Living 
          Units</td>
          <td 
        nowrap><asp:TextBox id=sVaNumOfLivingUnits runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>13a. Street Access</td>
          <td nowrap class=FieldLabel><asp:CheckBoxList id=sVaStreetAccessPrivateTri runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
<asp:ListItem Value="1">Private</asp:ListItem>
<asp:ListItem Value="2">Public</asp:ListItem>
</asp:CheckBoxList></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>13b. Street Maintenance</td>
          <td nowrap class=FieldLabel><asp:CheckBoxList id=sVaStreetMaintenancePrivateTri runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
<asp:ListItem Value="1">Private</asp:ListItem>
<asp:ListItem Value="2">Public</asp:ListItem>
</asp:CheckBoxList></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>14a. Construction Warranty 
            Included</td>
          <td nowrap class=FieldLabel><asp:CheckBoxList id=sVaConstructWarrantyTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
</asp:CheckBoxList></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>14b. Name of Warranty 
          Program</td>
          <td 
        nowrap><asp:TextBox id=sVaConstructWarrantyProgramNm runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>14c. Expiration Date</td>
          <td nowrap><ml:DateTextBox id=sVaConstructExpiredD runat="server" preset="date" width="75"></ml:DateTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>15. Construction Completed</td>
          <td 
        nowrap><ml:DateTextBox id=sVaConstructCompleteD runat="server" width="75" preset="date"></ml:DateTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>16. Name of Owner</td>
          <td 
        nowrap><asp:TextBox id=sVaOwnerNm runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>17. Property</td>
          <td nowrap><asp:DropDownList id=sVaSpOccT runat="server"></asp:DropDownList></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>18. Rent (if applic.)</td>
          <td nowrap class=FieldLabel><ml:MoneyTextBox id=sVaSpRentalMonthly runat="server" preset="money" width="90"></ml:MoneyTextBox>&nbsp;/ 
            month</td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>19. Name of Occupant</td>
          <td 
            nowrap class=FieldLabel><asp:TextBox id=sVaSpOccupantNm runat="server"></asp:TextBox>&nbsp; 
            20. Phone <ml:PhoneTextBox id=sVaSpOccupantPhone runat="server" preset="phone" width="120"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>21. Name of Broker</td>
          <td 
            nowrap class=FieldLabel><asp:TextBox id=VA26_1805BrokerCompanyName runat="server"></asp:TextBox>&nbsp; 
            22. Phone <ml:PhoneTextBox id=VA26_1805BrokerPhoneOfCompany runat="server" preset="phone" width="120"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>23. Keys At (Address)</td>
          <td nowrap><asp:TextBox id=VA26_1805KeyAtStreetAddr runat="server" Width="284px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap></td>
          <td nowrap><asp:TextBox id=VA26_1805KeyAtCity runat="server" Width="185px"></asp:TextBox><ml:StateDropDownList id=VA26_1805KeyAtState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=VA26_1805KeyAtZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>24. Originator's Ident. No</td>
          <td 
        nowrap><asp:TextBox id=sVALenderIdCode runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>25. Sponsor's Ident. No</td>
          <td 
        nowrap><asp:TextBox id=sVASponsorAgentIdCode runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>26. Institution's Case No.</td>
          <td 
        nowrap><asp:TextBox id=sFHACondCommInstCaseRef runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap colspan=2>28. New or Proposed 
            Construction - Complete items 28A through 28E for new or proposed 
            construction cases only</td></tr>
          <td class=FieldLabel valign=top nowrap>28a. Builder Name</td>
          <td nowrap><asp:TextBox id=VA26_1805BuilderCompanyName runat="server" Width="284px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top 
            nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Address</td>
          <td nowrap><asp:TextBox id=VA26_1805BuilderStreetAddr runat="server" Width="284px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel style="HEIGHT: 24px" valign=top nowrap></td>
          <td style="HEIGHT: 24px" nowrap><asp:TextBox id=VA26_1805BuilderCity runat="server" Width="185px"></asp:TextBox><ml:StateDropDownList id=VA26_1805BuilderState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=VA26_1805BuilderZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top 
            nowrap>28b. VA Builder ID No.</td>
          <td nowrap><asp:TextBox id="VA26_1805BuilderId" runat="server" width="120"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top 
            nowrap>28c. Phone Number</td>
          <td nowrap><ml:PhoneTextBox id=VA26_1805BuilderPhoneOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top 
            nowrap>28d. Warrantor Name</td>
          <td nowrap><asp:TextBox id=VA26_1805WarrantorCompanyName runat="server" Width="284px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top 
            nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Address</td>
          <td nowrap><asp:TextBox id=VA26_1805WarrantorStreetAddr runat="server" Width="284px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap></td>
          <td nowrap><asp:TextBox id=VA26_1805WarrantorCity runat="server" Width="185px"></asp:TextBox><ml:StateDropDownList id=VA26_1805WarrantorState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=VA26_1805WarrantorZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top 
            nowrap>28e. Phone Number</td>
          <td nowrap><ml:PhoneTextBox id=VA26_1805WarrantorPhoneOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
        <tr>
            <td class="FieldLabel" valign="top" nowrap>29. Applicable Point of Contact (POC) Information</td>
        </tr>
        <tr>
            <td class="FieldLabel" valign="top" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Name</td>
            <td nowrap><asp:TextBox id="VA26_1805ApplicablePointOfContactName" runat="server" Width="284px"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="FieldLabel" valign="top" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Address</td>
            <td nowrap><asp:TextBox id="VA26_1805ApplicablePointOfContactAddress" runat="server" Width="284px"></asp:TextBox></td>
        </tr>
        <tr>
          <td class="FieldLabel" valign="top" nowrap></td>
          <td nowrap><asp:TextBox id="VA26_1805ApplicablePointOfContactCity" runat="server" Width="185px"></asp:TextBox><ml:StateDropDownList id="VA26_1805ApplicablePointOfContactState" runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id="VA26_1805ApplicablePointOfContactZip" runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
        <tr>
            <td class="FieldLabel" valign="top" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Phone Number</td>
            <td nowrap><ml:PhoneTextBox type="text" id="VA26_1805ApplicablePointOfContactPhone" runat="server" Width="120px" preset="phone"></ml:PhoneTextBox></td>
        </tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>30. Annual Real Estate 
          Taxes</td>
          <td nowrap><ml:MoneyTextBox id=sProRealETxPerYr runat="server" preset="money" width="90"></ml:MoneyTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>31. Mineral Rights 
Reserved</td>
          <td nowrap class=FieldLabel><asp:CheckBoxList id=sSpMineralRightsReservedTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
</asp:CheckBoxList>&nbsp;<div>Explained </div><asp:TextBox id=sSpMineralRightsReservedExplain runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>32. Leasehold Cases</td>
          <td nowrap></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap colspan=2>&nbsp;&nbsp; A. 
            Lease Is <asp:CheckBox id=sSpLeaseIs99Yrs runat="server" Text="99 Years"></asp:CheckBox><asp:CheckBox id=sSpLeaseIsRenewable runat="server" Text="Renewable"></asp:CheckBox>&nbsp;&nbsp;&nbsp; 
            B. Expires <ml:DateTextBox id=sLeaseHoldExpireD disableYearLimit='true' runat="server" preset="date" width="75" dttype="DateTime"></ml:DateTextBox>&nbsp;&nbsp;&nbsp; 
            C. Annual Ground Rent <ml:MoneyTextBox id=sSpLeaseAnnualGroundRent runat="server" preset="money" width="90"></ml:MoneyTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>33a. Sale Price of 
Property</td>
          <td class=FieldLabel valign=top nowrap><ml:MoneyTextBox id=sPurchPrice runat="server" preset="money" width="90"></ml:MoneyTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>33b. Is Buyer Purchasing Lot 
            Separately?</td>
          <td class=FieldLabel valign=top nowrap><asp:CheckBoxList id=sLotPurchaseSeparatelyTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
</asp:CheckBoxList></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>34. Refinancing Amount of 
            Proposed Loan</td>
          <td class=FieldLabel valign=top nowrap><ml:MoneyTextBox id=sVaRefiAmt runat="server" preset="money" width="90"></ml:MoneyTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>35. Proposed Sale Contract 
            Attached</td>
          <td class=FieldLabel valign=top nowrap><asp:CheckBoxList id=sVaSaleContractAttachedTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
</asp:CheckBoxList></td></tr>
        <tr>
          <td class="FieldLabel">CERTIFICATION OF SUBMISSION TO VA</td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>37. Title of Person 
            Authorizing</td>
          <td class=FieldLabel valign=top nowrap><asp:TextBox id=VA26_1805AuthorizeTitle runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>38. Phone Number</td>
          <td class=FieldLabel valign=top nowrap><ml:PhoneTextBox id=VA26_1805AuthorizePhone runat="server" preset="phone" width="120"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>39. Date</td>
          <td class=FieldLabel valign=top nowrap><ml:DateTextBox id=VA26_1805AuthorizePrepareDate runat="server" preset="date" width="75"></ml:DateTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>40. Date of Assignment</td>
          <td class=FieldLabel valign=top nowrap><ml:DateTextBox id=VA26_1805AppraiserPrepareDate runat="server" preset="date" width="75"></ml:DateTextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>41. Name of Appraiser</td>
          <td class=FieldLabel valign=top nowrap><asp:TextBox id=VA26_1805AppraiserPreparerName runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top 
            nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Company</td>
          <td class=FieldLabel valign=top nowrap><asp:TextBox id=VA26_1805AppraiserCompanyName runat="server"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top 
            nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Address</td>
          <td class=FieldLabel valign=top nowrap><asp:TextBox id=VA26_1805AppraiserStreetAddr runat="server" Width="287px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap></td>
          <td class=FieldLabel valign=top nowrap><asp:TextBox id=VA26_1805AppraiserCity runat="server" Width="188px"></asp:TextBox><ml:StateDropDownList id=VA26_1805AppraiserState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=VA26_1805AppraiserZip runat="server" preset="zipcode" width="50"></ml:ZipcodeTextBox></td></tr></table></td></tr></table></form>
	
  </body>
</html>
