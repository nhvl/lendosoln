using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
	public partial class VAVerificationBenefit : BaseLoanPage
	{

        protected void PageInit(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            this.PageID = "VA_26_8937";
            this.PageTitle = "VA Verification of Benefit";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CVA_26_8937PDF);

            aVaIndebtCertifyTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            aVAFileClaimDisabilityCertifyTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            VA26_8937LenderZip.SmartZipcode(VA26_8937LenderCity, VA26_8937LenderState);
            aBZip.SmartZipcode(aBCity, aBState);

            Tools.BindSuffix(aBSuffix);
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VAVerificationBenefit));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_8937Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_8937LenderCompanyName.Text = preparer.CompanyName;
            VA26_8937LenderStreetAddr.Text = preparer.StreetAddr;
            VA26_8937LenderCity.Text = preparer.City;
            VA26_8937LenderState.Value = preparer.State;
            VA26_8937LenderZip.Text = preparer.Zip;

            aBAddr.Text              = dataApp.aBAddr;
            aBCity.Text              = dataApp.aBCity;
            aBDob.Text               = dataApp.aBDob_rep;
            aBFirstNm.Text           = dataApp.aBFirstNm;
            aBLastNm.Text            = dataApp.aBLastNm;
            aBMidNm.Text             = dataApp.aBMidNm;
            aBSsn.Text               = dataApp.aBSsn;
            aBState.Value             = dataApp.aBState;
            aBSuffix.Text            = dataApp.aBSuffix;
            aBZip.Text               = dataApp.aBZip;
            aVaClaimFolderNum.Text   = dataApp.aVaClaimFolderNum;
            aVaServiceNum.Text       = dataApp.aVaServiceNum;
            Tools.Set_TriState(aVaIndebtCertifyTri, dataApp.aVaIndebtCertifyTri);
            Tools.Set_TriState(aVAFileClaimDisabilityCertifyTri, dataApp.aVAFileClaimDisabilityCertifyTri);
        }

        protected override void SaveData() 
        {
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));


        }
	}
}
