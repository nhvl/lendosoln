﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VALoanComparison.aspx.cs" Inherits="LendersOfficeApp.newlos.VA.VALoanComparison" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>VA Loan Comparison</title>
</head>
<body bgcolor="gainsboro">

    <script language="javascript">
  <!--
  function _init() {
      lockField(document.getElementById("<%=AspxTools.HtmlString(aVaTotalClosingCostLckd.ClientID)%>"), 'aVaTotalClosingCost');
  }
  //-->
    </script>
    <form id="form1" runat="server">
        <table cellSpacing="0" cellPadding="0" width="100%">
			<tr>
				<td class="MainRightHeader" noWrap>
				    VA Loan Comparison (Obsolete for Loans Closing on or After 4/1/2018)
				</td>
			</tr>
			<tr>
				<td>
				    <table class="FieldLabel InsetBorder">
				        <tr>
				            <td colspan="2">
				            </td>
				            <td align="center">
				                Existing Loan
				            </td>
				            <td align="center">
				                Proposed Loan
				            </td>
				        </tr>
				        <tr>
				            <td colspan="2">
				                Loan Number
				            </td>
				            <td>
				                <asp:TextBox ID="sVaLoanNumCurrentLoan" onchange="refreshCalculation();" Width="105px" runat="server"></asp:TextBox>
				            </td>
				            <td>
				                <asp:TextBox ID="sAgencyCaseNum" onchange="refreshCalculation();" Width="105px" ReadOnly="true" runat="server"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td colspan="2">
				                Loan Amount
				            </td>
				            <td>
								<ml:moneytextbox id="sVaLoanAmtCurrentLoan" onchange="refreshCalculation();" Width="105px" runat="server" preset="money"></ml:moneytextbox>
				            </td>
				            <td>
								<ml:moneytextbox id="sFinalLAmt" onchange="refreshCalculation();" Width="105px" ReadOnly="true" runat="server" preset="money"></ml:moneytextbox>
				            </td>
				        </tr>
				        <tr>
				            <td colspan="2">
				                Interest Rate
				            </td>
				            <td>
								<ml:percenttextbox id="sVaNoteIrCurrentLoan" onchange="refreshCalculation();" Width="105px" runat="server" preset="percent"></ml:percenttextbox>
				            </td>
				            <td>
								<asp:TextBox id="sRateAmortT" onchange="refreshCalculation();" Width="105px" ReadOnly="true" runat="server"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td colspan="2">
				                Term
				            </td>
				            <td>
								<asp:TextBox id="sVaTermCurrentLoan" onchange="refreshCalculation();" Width="105px" runat="server"></asp:TextBox>
				            </td>
				            <td>
								<asp:TextBox id="sTerm" onchange="refreshCalculation();" Width="105px" ReadOnly="true" runat="server"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td colspan="2">
				                Principal & Interest
				            </td>
				            <td>
				                <ml:moneytextbox ID="sVaPICurrentLoan" onchange="refreshCalculation();" Width="105px" runat="server"></ml:moneytextbox>
				            </td>
				            <td>
				                <ml:moneytextbox ID="sProThisMPmt" onchange="refreshCalculation();" ReadOnly="true" Width="105px" runat="server"></ml:moneytextbox>
				            </td>
				        </tr>
				        <tr>
				            <td colspan="2">
				                Principal & Interest & Taxes & Insurance
				            </td>
				            <td>
				                <ml:moneytextbox ID="sVaMonthlyPmtCurrentLoan" onchange="refreshCalculation();" Width="105px" runat="server"></ml:moneytextbox>
				            </td>
				            <td>
				                <ml:moneytextbox ID="sMonthlyPmt" onchange="refreshCalculation();" ReadOnly="true" Width="105px" runat="server"></ml:moneytextbox>
				            </td>
				        </tr>
				        <tr>
				            <td style="padding-top:20px">
				                Total Closing Costs
				            </td>
				            <td style="padding-top:20px"><asp:CheckBox id="aVaTotalClosingCostLckd" runat="server" text="Lock"  onclick="refreshCalculation();"></asp:CheckBox></td>
				            <td style="padding-top:20px">
								<ml:moneytextbox id="aVaTotalClosingCost" ReadOnly="true" Width="105px" runat="server" preset="money"></ml:moneytextbox>
				            </td>
				        </tr>
				        <tr>
				            <td colspan="2">
				                Monthly PI Payment Decrease
				            </td>
				            <td>
								<ml:moneytextbox id="sVaLoanCompMonthlyPmtDecreaseAmt" ReadOnly="true" Width="105px" runat="server" preset="money"></ml:moneytextbox>
				            </td>
				        </tr>
				        <tr>
				            <td colspan="2">
				                Time to Recoup Costs
				            </td>
				            <td>
				                <asp:TextBox ID="sVaLoanCompRecoupCostsTime" ReadOnly="true" Width="105px" runat="server"></asp:TextBox>
				            </td>
				            <td>
				                Months
				            </td>
				        </tr>
				    </table>
				    <asp:CheckBox ID="sVaNewPmtQualified" Text="The lender hereby certifies that the veteran qualifies for the new monthly payment which exceeds the previous payment by 20 percent or more." runat="server" />
				</td>
			</tr>
		</table>
    </form>
</body>
</html>
