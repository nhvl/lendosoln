<%@ Page Language="c#" CodeBehind="VACertificateLoanDisbursement.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.VA.VACertificateLoanDisbursement" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>VACertificateLoanDisbursement</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>
<body class="RightBackground" ms_positioning="FlowLayout">

    <script language="javascript">
  <!--
        function _init() {

            lockField(<%= AspxTools.JsGetElementById(sVaLenderCaseNumLckd) %>, 'sVaLenderCaseNum');
            lockField(<%= AspxTools.JsGetElementById(sMaturityDLckd) %>, 'sMaturityD');
            lockField(<%= AspxTools.JsGetElementById(sProRealETxPerYrLckd) %>, 'sProRealETxPerYr');
            lockField(<%= AspxTools.JsGetElementById(sVaMaintainAssessPmtPerYearLckd) %>, 'sVaMaintainAssessPmtPerYear');
            lockField(<%= AspxTools.JsGetElementById(sProFloodInsPerYrLckd) %>, 'sProFloodInsPerYr');
            lockField(<%= AspxTools.JsGetElementById(sProHazInsPerYrLckd) %>, 'sProHazInsPerYr');
            lockField(<%= AspxTools.JsGetElementById(sSchedDueD1Lckd) %>, 'sSchedDueD1');

            var b = <%= AspxTools.JsGetElementById(sVaLienPosTLckd) %>.checked;
            disableDDL(<%= AspxTools.JsGetElementById(sVaLienPosT) %>, !b);
            <%= AspxTools.JsGetElementById(sVaLienPosOtherDesc) %>.readOnly = !b || <%= AspxTools.JsGetElementById(sVaLienPosT) %>.value != <%= AspxTools.JsString(E_sVaLienPosT.Other) %>;

            b = <%= AspxTools.JsGetElementById(sVaEstateHeldTLckd) %>.checked;
            disableDDL(<%= AspxTools.JsGetElementById(sVaEstateHeldT) %>, !b);
            <%= AspxTools.JsGetElementById(sLeaseHoldExpireD) %>.readOnly = !b || <%= AspxTools.JsGetElementById(sVaEstateHeldT) %>.value != <%= AspxTools.JsString(E_sVaEstateHeldT.LeaseHold) %>;
            <%= AspxTools.JsGetElementById(sVaEstateHeldOtherDesc) %>.readOnly = !b || <%= AspxTools.JsGetElementById(sVaEstateHeldT) %>.value != <%= AspxTools.JsString(E_sVaEstateHeldT.Other) %>;

            aVaVestTitleT_onchange(<%= AspxTools.JsGetElementById(aVaVestTitleT) %>, 'aVaVestTitleODesc');

        }
        function aVaVestTitleT_onchange(ddl, tb) {
            document.getElementById(tb).readOnly = ddl.value != <%= AspxTools.JsString(E_aVaVestTitleT.Other) %>;
        }
        function mutualExclusive(cb, otherCb) {
            if (cb.checked) {
                document.getElementById(otherCb).checked = false;
            }
        }
  //-->
    </script>

    <form id="VACertificateLoanDisbursement" method="post" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="MainRightHeader" nowrap>
                Certification of Loan Disbursement (VA 26-1820)
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table2" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            <asp:CheckBox ID="sVaIsAutoProc" runat="server" Text="Automatic Procedure" onclick="mutualExclusive(this, 'sVaIsPriorApprovalProc');"></asp:CheckBox><asp:CheckBox ID="sVaIsPriorApprovalProc" runat="server" Text="Prior Approval Procedure" onclick="mutualExclusive(this, 'sVaIsAutoProc');"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            1. VA Loan Number
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sAgencyCaseNum" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            2A. Lender's Loan Number
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sVaLenderCaseNumLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox>
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sVaLenderCaseNum" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            2B. Lender's VA Number
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sVALenderIdCode" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            3. Date of Report
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="VA26_1820PrepareDate" runat="server" preset="date" Width="75"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            4A. Veteran First Name
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="aBFirstNm" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Middle Name
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="aBMidNm" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Last Name
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="aBLastNm" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Suffix
                        </td>
                        <td nowrap>
                            <ml:ComboBox ID="aBSuffix" runat="server"></ml:ComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            4B. Veteran's SSN
                        </td>
                        <td nowrap>
                            <ml:SSNTextBox ID="aBSsn" runat="server" preset="ssn" Width="75px"></ml:SSNTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="3">
                            5. Present Address of Veteran
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="3">
                            <asp:TextBox ID="aBAddr" runat="server" Width="254px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="3">
                            <asp:TextBox ID="aBCity" runat="server"></asp:TextBox><ml:StateDropDownList ID="aBState" runat="server"></ml:StateDropDownList>
                            <ml:ZipcodeTextBox ID="aBZip" runat="server" preset="zipcode" Width="50"></ml:ZipcodeTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="3">
                            6. Name &amp; Address of Relative not living with Veteran
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="3">
                            <table id="Table5" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Name
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="sFHAPropImprovBorrRelativeNm" runat="server" Width="254px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" valign="top" nowrap>
                                        Address
                                    </td>
                                    <td valign="top" nowrap>
                                        <%--
                                        <asp:TextBox ID="sFHAPropImprovBorrRelativeAddr" runat="server" Width="289px" TextMode="MultiLine" Height="58px"></asp:TextBox>
                                        <br />
                                        --%>
                                        <asp:TextBox ID="sFHAPropImprovBorrRelativeStreetAddress" runat="server" Width="289px"></asp:TextBox>
                                        <br />
                                        <asp:TextBox ID="sFHAPropImprovBorrRelativeCity" runat="server"></asp:TextBox>
                                        <ml:StateDropDownList ID="sFHAPropImprovBorrRelativeState" runat="server"></ml:StateDropDownList>
                                        <ml:ZipcodeTextBox ID="sFHAPropImprovBorrRelativeZip" runat="server" preset="zipcode" Width="50"></ml:ZipcodeTextBox>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Phone
                                    </td>
                                    <td nowrap>
                                        <ml:PhoneTextBox ID="sFHAPropImprovBorrRelativePhone" runat="server" preset="phone" Width="120"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" colspan="3">
                            Relationship to veteran <asp:TextBox runat="server" ID="sFHAPropImprovBorrRelativeRelationship" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="3">
                            <asp:CheckBox ID="sVaIsGuarantyEvidenceRequested" runat="server" Text="Guaranty" onclick="mutualExclusive(this, 'sVaIsInsuranceEvidenceRequested');"></asp:CheckBox>&nbsp;/
                            <asp:CheckBox ID="sVaIsInsuranceEvidenceRequested" runat="server" Text="Insurance" onclick="mutualExclusive(this, 'sVaIsGuarantyEvidenceRequested');"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap valign="top" colspan="2">
                            7. Purpose of Loan
                        </td>
                        <td nowrap>
                            <table id="Table13" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsPurchaseExistHome" TabIndex="25" runat="server" Text="Purchase Home (Prev Occupied)"></asp:CheckBox>
                                    </td>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsConstructHome" TabIndex="25" runat="server" Text="Construct Home"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsFinanceImprovement" TabIndex="25" runat="server" Text="Finance Improvement"></asp:CheckBox>
                                    </td>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsFinanceCoopPurchase" TabIndex="25" runat="server" Text="Finance Co-op Purchase"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsRefinance" TabIndex="25" runat="server" Text="Refinance"></asp:CheckBox>
                                    </td>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsPurchaseManufacturedHome" TabIndex="25" runat="server" Text="Purchase Manufactured Home"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsPurchaseNewCondo" TabIndex="25" runat="server" Text="Purchase New Condo Unit"></asp:CheckBox>
                                    </td>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsManufacturedHomeAndLot" TabIndex="25" runat="server" Text="Purchase Manufactured Home &amp; Lot"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsPurchaseExistCondo" TabIndex="25" runat="server" Text="Purchase Existing Condo Unit"></asp:CheckBox>
                                    </td>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsRefiManufacturedHomeToBuyLot" TabIndex="25" runat="server" Text="Refi Manufactured Home to buy lot"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsPurchaseNewHome" TabIndex="25" runat="server" Text="Purchase Home (Not Prev Occupied)"></asp:CheckBox>
                                    </td>
                                    <td nowrap>
                                        <asp:CheckBox ID="sFHAPurposeIsRefiManufacturedHomeOrLotLoan" TabIndex="25" runat="server" Text="Refi Manufactured Home/Lot Loan"></asp:CheckBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            8. Property Address
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sSpAddr" runat="server" Width="276px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sSpCity" runat="server"></asp:TextBox>
                            <ml:StateDropDownList ID="sSpState" runat="server"></ml:StateDropDownList>
                            <ml:ZipcodeTextBox ID="sSpZip" runat="server" preset="zipcode" Width="50"></ml:ZipcodeTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            8B. Lot
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sFHAPropImprovBorrRelativeLot" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            8C. Block
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sFHAPropImprovBorrRelativeBlock" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            8D. Subdivision
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sFHAPropImprovBorrRelativeSubdivision" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            9. Loan Amount
                        </td>
                        <td nowrap>
                            <ml:MoneyTextBox ID="sFinalLAmt" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            10A. P &amp; I each period
                        </td>
                        <td nowrap>
                            <ml:MoneyTextBox ID="sProThisMPmt" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            10B. Interest Rate
                        </td>
                        <td nowrap>
                            <ml:PercentTextBox ID="sNoteIR" runat="server" preset="percent" Width="70" onchange="refreshCalculation();"></ml:PercentTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            10C. Date of Note
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sLNoteD" runat="server" preset="date" Width="75" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            10D. Date of First Payment
                        </td>
                        <td nowrap>
                            <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" Text="Lock" onclick="lockField(this, 'sSchedDueD1'); refreshCalculation();"/>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sSchedDueD1" runat="server" preset="date" Width="75" onchange="refreshCalculation();"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            10E. Date Loan Was Closed
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sClosedD" runat="server" preset="date" Width="75"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            10F. Date Loan Proceeds Fully Paid Out
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sLProceedPaidOutD" runat="server" preset="date" Width="75"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            10G. Term (months)
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sTerm" runat="server" Width="57px" onchange="refreshCalculation();"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            10H. Date of Maturity
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sMaturityDLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox>
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sMaturityD" runat="server" Width="75" preset="date"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            <p>
                                11. Type of Lien</p>
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sVaLienPosTLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox>
                        </td>
                        <td nowrap>
                            <asp:DropDownList ID="sVaLienPosT" runat="server" onchange="refreshCalculation();">
                            </asp:DropDownList>
                            <asp:TextBox ID="sVaLienPosOtherDesc" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            12. Title of Property
                        </td>
                        <td nowrap>
                            <asp:DropDownList ID="aVaVestTitleT" runat="server" onchange="aVaVestTitleT_onchange(this, 'aVaVestTitleODesc');">
                            </asp:DropDownList>
                            <asp:TextBox ID="aVaVestTitleODesc" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            13. Estate in Property
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sVaEstateHeldTLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox>
                        </td>
                        <td nowrap>
                            <asp:DropDownList ID="sVaEstateHeldT" runat="server" onchange="refreshCalculation();">
                            </asp:DropDownList>
                            &nbsp;Expiration Date
                            <ml:DateTextBox ID="sLeaseHoldExpireD" disableYearLimit='true' runat="server" preset="date" Width="75" dttype="DateTime"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                        </td>
                        <td nowrap>
                            Other
                            <asp:TextBox ID="sVaEstateHeldOtherDesc" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            14. Approximate Annual Real Estate Taxes
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sProRealETxPerYrLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox>
                        </td>
                        <td nowrap>
                            <ml:MoneyTextBox ID="sProRealETxPerYr" runat="server" Width="90" preset="money"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="2">
                            15. Insurance
                        </td>
                        <td nowrap>
                            <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td nowrap>
                                    </td>
                                    <td nowrap>
                                    </td>
                                    <td nowrap>
                                        A. Hazard
                                    </td>
                                    <td nowrap>
                                    </td>
                                    <td nowrap>
                                        B. Flood
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        Face Amount
                                    </td>
                                    <td nowrap>
                                    </td>
                                    <td nowrap>
                                        <ml:MoneyTextBox ID="sProHazInsFaceAmt" runat="server" preset="money" Width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                    </td>
                                    <td nowrap>
                                    </td>
                                    <td nowrap>
                                        <ml:MoneyTextBox ID="sProFloodInsFaceAmt" runat="server" preset="money" Width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        Annual Premium
                                    </td>
                                    <td nowrap>
                                        <asp:CheckBox ID="sProHazInsPerYrLckd" runat="server" onclick="refreshCalculation();"></asp:CheckBox>
                                    </td>
                                    <td nowrap>
                                        <ml:MoneyTextBox ID="sProHazInsPerYr" runat="server" preset="money" Width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                    </td>
                                    <td nowrap>
                                        <asp:CheckBox ID="sProFloodInsPerYrLckd" runat="server" onclick="refreshCalculation();"></asp:CheckBox>
                                    </td>
                                    <td nowrap>
                                        <ml:MoneyTextBox ID="sProFloodInsPerYr" runat="server" preset="money" Width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            16. Approximate Annual Assessment Payment
                        </td>
                        <td nowrap>
                            <ml:MoneyTextBox ID="sVaSpecialAssessPmtPerYear" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            17. Total Unpaid Special Assessments
                        </td>
                        <td nowrap>
                            <ml:MoneyTextBox ID="sVaSpecialAssessUnpaid" runat="server" preset="money" Width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            18. Annual Maintenance Assessment
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sVaMaintainAssessPmtPerYearLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox>
                        </td>
                        <td nowrap>
                            <ml:MoneyTextBox ID="sVaMaintainAssessPmtPerYear" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="3">
                            19. Describe Nonrealty
                            <asp:CheckBox ID="sVaNonrealtyAcquiredWithLDescPrintSeparately" runat="server" Text="Print on separate page"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="3">
                            <asp:TextBox ID="sVaNonrealtyAcquiredWithLDesc" runat="server" Width="300px" Height="94px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="3">
                            20. Describe Additional Security
                            <asp:CheckBox ID="sVaAdditionalSecurityTakenDescPrintSeparately" runat="server" Text="Print on separate page"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" valign="top" nowrap colspan="3">
                            <asp:TextBox ID="sVaAdditionalSecurityTakenDesc" runat="server" Width="301px" TextMode="MultiLine" Height="89px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            If Land Acquired by separated transaction complete items 21 and 22
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            21. Date Acquired
                        </td>
                        <td nowrap>
                            <ml:DateTextBox ID="sLotAcquiredD" runat="server" preset="date" Width="75"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            22. Purchase Price
                        </td>
                        <td nowrap>
                            <ml:MoneyTextBox ID="sVaLotPurchPrice" runat="server" preset="money" Width="90"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="2">
                            23. Amount withheld from loan proceeds and deposited in
                        </td>
                        <td class="FieldLabel" nowrap>
                            <asp:DropDownList ID="sVaLProceedDepositT" runat="server">
                            </asp:DropDownList>
                            <asp:TextBox ID="sVaLProceedDepositDesc" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            <table id="Table3" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td nowrap class="FieldLabel">
                                        Authorized Agent
                                    </td>
                                    <td nowrap>
                                        &nbsp;
                                    </td>
                                    <td nowrap>
                                        <font color="#ff6633"></font>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="FieldLabel">
                                        Name
                                    </td>
                                    <td nowrap class="FieldLabel">
                                        Address
                                    </td>
                                    <td nowrap class="FieldLabel">
                                        Function
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" nowrap>
                                        <asp:TextBox ID="VA26_1820Agent1PreparerName" runat="server"></asp:TextBox>
                                    </td>
                                    <td nowrap>
                                        <table id="Table6" cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td nowrap>
                                                    <asp:TextBox ID="VA26_1820Agent1StreetAddr" runat="server" Width="254px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap>
                                                    <asp:TextBox ID="VA26_1820Agent1City" runat="server"></asp:TextBox><ml:StateDropDownList ID="VA26_1820Agent1State" runat="server"></ml:StateDropDownList>
                                                    <ml:ZipcodeTextBox ID="VA26_1820Agent1Zip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" nowrap>
                                        <ml:ComboBox ID="sVaAgent1RoleDesc" runat="server" Width="200px"></ml:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" nowrap>
                                        <asp:TextBox ID="VA26_1820Agent2PreparerName" runat="server"></asp:TextBox>
                                    </td>
                                    <td nowrap>
                                        <table id="Table7" cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td nowrap>
                                                    <asp:TextBox ID="VA26_1820Agent2StreetAddr" runat="server" Width="254px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap>
                                                    <asp:TextBox ID="VA26_1820Agent2City" runat="server"></asp:TextBox><ml:StateDropDownList ID="VA26_1820Agent2State" runat="server"></ml:StateDropDownList>
                                                    <ml:ZipcodeTextBox ID="VA26_1820Agent2Zip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" nowrap>
                                        <ml:ComboBox ID="sVaAgent2RoleDesc" runat="server" Width="200px"></ml:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" nowrap>
                                        <asp:TextBox ID="VA26_1820Agent3PreparerName" runat="server"></asp:TextBox>
                                    </td>
                                    <td nowrap>
                                        <table id="Table8" cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td nowrap>
                                                    <asp:TextBox ID="VA26_1820Agent3StreetAddr" runat="server" Width="254px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap>
                                                    <asp:TextBox ID="VA26_1820Agent3City" runat="server"></asp:TextBox><ml:StateDropDownList ID="VA26_1820Agent3State" runat="server"></ml:StateDropDownList>
                                                    <ml:ZipcodeTextBox ID="VA26_1820Agent3Zip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" nowrap>
                                        <ml:ComboBox ID="sVaAgent3RoleDesc" runat="server" Width="200px"></ml:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" nowrap>
                                        <asp:TextBox ID="VA26_1820Agent4PreparerName" runat="server"></asp:TextBox>
                                    </td>
                                    <td nowrap>
                                        <table id="Table9" cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td nowrap>
                                                    <asp:TextBox ID="VA26_1820Agent4StreetAddr" runat="server" Width="254px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap>
                                                    <asp:TextBox ID="VA26_1820Agent4City" runat="server"></asp:TextBox><ml:StateDropDownList ID="VA26_1820Agent4State" runat="server"></ml:StateDropDownList>
                                                    <ml:ZipcodeTextBox ID="VA26_1820Agent4Zip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" nowrap>
                                        <ml:ComboBox ID="sVaAgent4RoleDesc" runat="server" Width="200px"></ml:ComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" nowrap>
                                        <asp:TextBox ID="VA26_1820Agent5PreparerName" runat="server"></asp:TextBox>
                                    </td>
                                    <td nowrap>
                                        <table id="Table10" cellspacing="0" cellpadding="0" border="0">
                                            <tr>
                                                <td nowrap>
                                                    <asp:TextBox ID="VA26_1820Agent5StreetAddr" runat="server" Width="254px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap>
                                                    <asp:TextBox ID="VA26_1820Agent5City" runat="server"></asp:TextBox><ml:StateDropDownList ID="VA26_1820Agent5State" runat="server"></ml:StateDropDownList>
                                                    <ml:ZipcodeTextBox ID="VA26_1820Agent5Zip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" nowrap>
                                        <ml:ComboBox ID="sVaAgent5RoleDesc" runat="server" Width="200px"></ml:ComboBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            <asp:CheckBox ID="sIsAlterationCompleted" runat="server" Text="Any construction, repairs, alterations, or improvements"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            25. Lender Information
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            <table id="Table11" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Name
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="VA26_1820LenderCompanyName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Address
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="VA26_1820LenderStreetAddr" runat="server" Width="260px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="VA26_1820LenderCity" runat="server" Width="152px"></asp:TextBox><ml:StateDropDownList ID="VA26_1820LenderState" runat="server"></ml:StateDropDownList>
                                        <ml:ZipcodeTextBox ID="VA26_1820LenderZip" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Telephone of Lender
                                    </td>
                                    <td nowrap>
                                        <ml:PhoneTextBox ID="VA26_1820LenderPhoneOfCompany" runat="server" Width="120" preset="phone"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            27B. Occupancy
                            <br />
                            <asp:RadioButton GroupName="aFHABorrCertOcc" ID="aFHABorrCertOccIsAsHome" runat="server" Text="(1) I now actually occupy the above property as my home..."></asp:RadioButton>
                            <br />
                            <asp:RadioButton GroupName="aFHABorrCertOcc" ID="aFHABorrCertOccIsAsHomeForActiveSpouse" runat="server" Text="(2) My spouse is on active military duty and ... I occupy ..."></asp:RadioButton>
                            <br />
                            <asp:RadioButton GroupName="aFHABorrCertOcc" ID="aFHABorrCertOccIsChildAsHome" runat="server" Text="(3) The veteran is on active military duty and in his or her absence ... a dependent child of the veteran occupies ..."></asp:RadioButton>
                            <br />
                            <asp:RadioButton GroupName="aFHABorrCertOcc" ID="aFHABorrCertOccIsAsHomePrev" runat="server" Text="(4) I previously occupied the property... as my home"></asp:RadioButton>
                            <br />
                            <asp:RadioButton GroupName="aFHABorrCertOcc" ID="aFHABorrCertOccIsAsHomePrevForActiveSpouse" runat="server" Text="(5) While my spouse was on active military duty ... I previously ..."></asp:RadioButton>
                            <br />
                            <asp:RadioButton GroupName="aFHABorrCertOcc" ID="aFHABorrCertOccIsChildAsHomePrev" runat="server" Text="(6) While the veteran was on active military duty ... the property was occupied by the veteran's dependant child ..."></asp:RadioButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            27C. Reasonable value of the property as determined by VA
                            <ml:MoneyTextBox ID="aFHABorrCertInformedPropVal" runat="server" Width="90" preset="money"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            IF THE CONTRACT PRICE OR COST EXCEEDS THE VA REASONABLE VALUE, COMPLETE EITHER ITEM D OR E
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            <asp:CheckBox ID="aFHABorrCertInformedPropValAwareAtContractSigning" runat="server" Text="27D. I was aware of this valuation when I signed my contract"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            <asp:CheckBox ID="aFHABorrCertInformedPropValNotAwareAtContractSigning" runat="server" Text="27E. I was not aware of this valuation when I signed my contract"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                        <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap colspan="3">
                            <asp:CheckBox ID="aVaNotDischargedOrReleasedSinceCertOfEligibility" runat="server" Text="I certify that I have not been discharged or released from active duty since the date my Certificate of Eligibility was issued." />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
