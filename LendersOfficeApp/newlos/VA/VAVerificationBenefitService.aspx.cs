using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
    public class VAVerificationBenefitServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VAVerificationBenefitServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_8937Lender, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("VA26_8937LenderCompanyName");
            preparer.StreetAddr = GetString("VA26_8937LenderStreetAddr");
            preparer.City = GetString("VA26_8937LenderCity");
            preparer.State = GetString("VA26_8937LenderState");
            preparer.Zip = GetString("VA26_8937LenderZip");
            preparer.Update();

            dataApp.aBAddr              = GetString("aBAddr");
            dataApp.aBCity              = GetString("aBCity");
            dataApp.aBDob_rep           = GetString("aBDob");
            dataApp.aBFirstNm           = GetString("aBFirstNm");
            dataApp.aBLastNm            = GetString("aBLastNm");
            dataApp.aBMidNm             = GetString("aBMidNm");
            dataApp.aBSsn               = GetString("aBSsn");
            dataApp.aBState             = GetString("aBState");
            dataApp.aBSuffix            = GetString("aBSuffix");
            dataApp.aBZip               = GetString("aBZip");
            dataApp.aVaClaimFolderNum   = GetString("aVaClaimFolderNum");
            dataApp.aVaServiceNum       = GetString("aVaServiceNum");
            dataApp.aVaIndebtCertifyTri = GetTriState("aVaIndebtCertifyTri");
            dataApp.aVAFileClaimDisabilityCertifyTri = GetTriState("aVAFileClaimDisabilityCertifyTri");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_8937Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("VA26_8937LenderCompanyName", preparer.CompanyName);
            SetResult("VA26_8937LenderStreetAddr", preparer.StreetAddr);
            SetResult("VA26_8937LenderCity", preparer.City);
            SetResult("VA26_8937LenderState", preparer.State);
            SetResult("VA26_8937LenderZip", preparer.Zip);

            SetResult("aBAddr", dataApp.aBAddr);
            SetResult("aBCity", dataApp.aBCity);
            SetResult("aBDob", dataApp.aBDob_rep);
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBSsn", dataApp.aBSsn);
            SetResult("aBState", dataApp.aBState);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aBZip", dataApp.aBZip);
            SetResult("aVaClaimFolderNum", dataApp.aVaClaimFolderNum);
            SetResult("aVaServiceNum", dataApp.aVaServiceNum);
            SetResult("aVaIndebtCertifyTri", dataApp.aVaIndebtCertifyTri);
            SetResult("aVAFileClaimDisabilityCertifyTri", dataApp.aVAFileClaimDisabilityCertifyTri);
        }
    }
	/// <summary>
	/// Summary description for VAVerificationBenefitService.
	/// </summary>
	public partial class VAVerificationBenefitService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new VAVerificationBenefitServiceItem());
        }
	}
}
