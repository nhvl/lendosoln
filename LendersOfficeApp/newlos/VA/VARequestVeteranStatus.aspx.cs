using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
	/// <summary>
	/// Summary description for VARequestVeteranStatus.
	/// </summary>
	public partial class VARequestVeteranStatus : BaseLoanPage
	{

        protected void PageInit(object sender, System.EventArgs e) 
        {
            aBZip.SmartZipcode(aBCity, aBState);
            Tools.Bind_TriState(aActiveMilitaryStatTri);
            aActiveMilitaryStatTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            Tools.Bind_TriState(aWereActiveMilitaryDutyDayAfterTri);
            aWereActiveMilitaryDutyDayAfterTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);

            Tools.BindSuffix(aBSuffix);

            this.PageID = "VA_26_8261a";
            this.PageTitle = "Certificate of Veteran Status";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CVA_26_8261aPDF);
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here

        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VARequestVeteranStatus));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            Tools.Set_TriState(aActiveMilitaryStatTri, dataApp.aActiveMilitaryStatTri);
            aBAddr.Text = dataApp.aBAddr;
            aBCity.Text = dataApp.aBCity;
            aBDob.Text = dataApp.aBDob_rep;
            aBFirstNm.Text = dataApp.aBFirstNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBState.Value= dataApp.aBState;
            aBSuffix.Text = dataApp.aBSuffix;
            aBZip.Text = dataApp.aBZip;
            aVaClaimNum.Text = dataApp.aVaClaimNum;
            aVaServ1BranchNum.Text = dataApp.aVaServ1BranchNum;
            aVaServ1EndD.Text = dataApp.aVaServ1EndD_rep;
            aVaServ1FullNm.Text = dataApp.aVaServ1FullNm;
            aVaServ1Num.Text = dataApp.aVaServ1Num;
            aVaServ1Ssn.Text = dataApp.aVaServ1Ssn;
            aVaServ1StartD.Text = dataApp.aVaServ1StartD_rep;
            aVaServ2BranchNum.Text = dataApp.aVaServ2BranchNum;
            aVaServ2EndD.Text = dataApp.aVaServ2EndD_rep;
            aVaServ2FullNm.Text = dataApp.aVaServ2FullNm;
            aVaServ2Num.Text = dataApp.aVaServ2Num;
            aVaServ2Ssn.Text = dataApp.aVaServ2Ssn;
            aVaServ2StartD.Text = dataApp.aVaServ2StartD_rep;
            Tools.Set_TriState(aWereActiveMilitaryDutyDayAfterTri, dataApp.aWereActiveMilitaryDutyDayAfterTri);
        }
        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));


        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
