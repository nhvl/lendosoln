using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
namespace LendersOfficeApp.newlos.VA
{
    /// <summary>
    /// Summary description for VARefiWorksheet.
    /// </summary>
    public partial class VARefiWorksheet : LendersOfficeApp.newlos.BaseLoanPage
    {
    
        protected void PageInit(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            this.PageID = "VA_26_8923";
            this.PageTitle = "VA Refi Worksheet";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CVA_26_8923PDF);
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VARefiWorksheet));
            dataLoan.InitLoad();

            sVaRefiWsAllowableCcAndPp.Text            = dataLoan.sVaRefiWsAllowableCcAndPp_rep;
            sVaRefiWsCashPmtFromVet.Text              = dataLoan.sVaRefiWsCashPmtFromVet_rep;
            sVaRefiWsDiscnt.Text                      = dataLoan.sVaRefiWsDiscnt_rep;
            sVaRefiWsDiscnt2.Text                     = dataLoan.sVaRefiWsDiscnt_rep;
            sVaRefiWsDiscntPc.Text                    = dataLoan.sVaRefiWsDiscntPc_rep;
            sVaRefiWsExistingVaLBalLckd.Checked       = dataLoan.sVaRefiWsExistingVaLBalLckd;
            sVaRefiWsExistingVaLBal.Text              = dataLoan.sVaRefiWsExistingVaLBal_rep;
            sVaRefiWsExistingVaLBalAfterCashPmt.Text  = dataLoan.sVaRefiWsExistingVaLBalAfterCashPmt_rep;
            sVaRefiWsExistingVaLBalAfterCashPmt2.Text = dataLoan.sVaRefiWsExistingVaLBalAfterCashPmt_rep;
            sVaRefiWsFf.Text                          = dataLoan.sVaRefiWsFf_rep;
            sVaRefiWsFf2.Text                         = dataLoan.sVaRefiWsFf_rep;
            sVaRefiWsFfPc.Text                        = dataLoan.sVaRefiWsFfPc_rep;
            sVaRefiWsFfPc2.Text                       = dataLoan.sVaRefiWsFfPc_rep;
            sVaRefiWsFinalDiscnt.Text                 = dataLoan.sVaRefiWsFinalDiscnt_rep;
            sVaRefiWsFinalDiscntPc.Text               = dataLoan.sVaRefiWsFinalDiscntPc_rep;
            sVaRefiWsFinalFf.Text                     = dataLoan.sVaRefiWsFinalFf_rep;
            sVaRefiWsFinalSubtotalItem12.Text         = dataLoan.sVaRefiWsFinalSubtotalItem12_rep;
            sVaRefiWsFinalSubtotalItem14.Text         = dataLoan.sVaRefiWsFinalSubtotalItem14_rep;
            sVaRefiWsFinalSubtotalItem16.Text         = dataLoan.sVaRefiWsFinalSubtotalItem16_rep;
            sVaRefiWsMaxLAmt.Text                     = dataLoan.sVaRefiWsMaxLAmt_rep;
            sVaRefiWsOrigFee.Text                     = dataLoan.sVaRefiWsOrigFee_rep;
            sVaRefiWsOrigFeePc.Text                   = dataLoan.sVaRefiWsOrigFeePc_rep;
            sVaRefiWsPreliminaryTot.Text              = dataLoan.sVaRefiWsPreliminaryTot_rep;
            sVaRefiWsPreliminaryTot2.Text             = dataLoan.sVaRefiWsPreliminaryTot_rep;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_8928Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_8928LenderPrepareDate.Text = preparer.PrepareDate_rep;
            VA26_8928LenderTitle.Text = preparer.Title;
            VA26_8928LenderCompanyName.Text = preparer.CompanyName;



        }

        protected override void SaveData() 
        {
        }
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            IsAppSpecific = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
    }
}
