using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
	public partial class VAReasonableValue : BaseLoanPage
	{
        #region Protected member variables
        #endregion


        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageID = "VA_26_1805";
            this.PageTitle = "Request for Determination of Reasonable Value";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CVA_26_1805PDF);

            Tools.Bind_TriState(sVaIsFactoryFabricatedTri);
            Tools.Bind_TriState(sVaConstructWarrantyTri);
            Tools.Bind_TriState(sSpMineralRightsReservedTri);
            Tools.Bind_TriState(sLotPurchaseSeparatelyTri);
            Tools.Bind_TriState(sVaSaleContractAttachedTri);
            sVaIsFactoryFabricatedTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sVaConstructWarrantyTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sSpMineralRightsReservedTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sLotPurchaseSeparatelyTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sVaSaleContractAttachedTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sVaStreetAccessPrivateTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sVaStreetMaintenancePrivateTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);

            

            sSpZip.SmartZipcode(sSpCity, sSpState, sSpCounty);
            VA26_1805FirmMakingRequestZip.SmartZipcode(VA26_1805FirmMakingRequestCity, VA26_1805FirmMakingRequestState);
            VA26_1805KeyAtZip.SmartZipcode(VA26_1805KeyAtCity, VA26_1805KeyAtState);
            VA26_1805BuilderZip.SmartZipcode(VA26_1805BuilderCity, VA26_1805BuilderState);
            VA26_1805WarrantorZip.SmartZipcode(VA26_1805WarrantorCity, VA26_1805WarrantorState);
            VA26_1805AppraiserZip.SmartZipcode(VA26_1805AppraiserCity, VA26_1805AppraiserState);
            VA26_1805ApplicablePointOfContactZip.SmartZipcode(VA26_1805ApplicablePointOfContactCity, VA26_1805ApplicablePointOfContactState);

            Tools.Bind_sSpUtilT(sSpUtilElecT);
            Tools.Bind_sSpUtilT(sSpUtilGasT);
            Tools.Bind_sSpUtilT(sSpUtilSanSewerT);
            Tools.Bind_sSpUtilT(sSpUtilWaterT);
            Tools.Bind_sVaBuildingStatusT(sVaBuildingStatusT);
            Tools.Bind_sVaBuildingT(sVaBuildingT);
            Tools.Bind_sVaSpOccT(sVaSpOccT);

			 // 01-30-08 av 18913
			sSpState.Attributes.Add( "onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event);" );
        }


        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VAReasonableValue));
            dataLoan.InitLoad();

            Tools.SetDropDownListValue(sSpUtilElecT, dataLoan.sSpUtilElecT);
            Tools.SetDropDownListValue(sSpUtilGasT, dataLoan.sSpUtilGasT);
            Tools.SetDropDownListValue(sSpUtilSanSewerT, dataLoan.sSpUtilSanSewerT);
            Tools.SetDropDownListValue(sSpUtilWaterT, dataLoan.sSpUtilWaterT);
            Tools.SetDropDownListValue(sVaBuildingStatusT, dataLoan.sVaBuildingStatusT);
            Tools.SetDropDownListValue(sVaBuildingT, dataLoan.sVaBuildingT);
            Tools.SetDropDownListValue(sVaSpOccT, dataLoan.sVaSpOccT);
            Tools.Set_TriState(sLotPurchaseSeparatelyTri, dataLoan.sLotPurchaseSeparatelyTri);
            Tools.Set_TriState(sSpMineralRightsReservedTri, dataLoan.sSpMineralRightsReservedTri);
            Tools.Set_TriState(sVaConstructWarrantyTri, dataLoan.sVaConstructWarrantyTri);
            Tools.Set_TriState(sVaIsFactoryFabricatedTri, dataLoan.sVaIsFactoryFabricatedTri);
            Tools.Set_TriState(sVaSaleContractAttachedTri, dataLoan.sVaSaleContractAttachedTri);
            Tools.Set_TriState(sVaStreetAccessPrivateTri, dataLoan.sVaStreetAccessPrivateTri);
            Tools.Set_TriState(sVaStreetMaintenancePrivateTri, dataLoan.sVaStreetMaintenancePrivateTri);
            sAgencyCaseNum.Text                  = dataLoan.sAgencyCaseNum;
            sFHACondCommInstCaseRef.Text         = dataLoan.sFHACondCommInstCaseRef;
            sVALenderIdCode.Text                = dataLoan.sVALenderIdCode;
            sVASponsorAgentIdCode.Text          = dataLoan.sVASponsorAgentIdCode;
            sLeaseHoldExpireD.Text               = dataLoan.sLeaseHoldExpireD_rep;
            sProRealETxPerYr.Text                = dataLoan.sProRealETxPerYr_rep;
            sPurchPrice.Text                     = dataLoan.sPurchPrice_rep;
            sPurchPrice.ReadOnly = (dataLoan.sLPurposeT != E_sLPurposeT.Purchase
                && dataLoan.sLPurposeT != E_sLPurposeT.Construct
                && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm) || IsReadOnly; // 11/05/07 mf. OPM 18801

            sSpAddr.Text                         = dataLoan.sSpAddr;
            sSpCity.Text                         = dataLoan.sSpCity;
			// 01-14-08 av 18913 
			Tools.Bind_sSpCounty( dataLoan.sSpState, sSpCounty, true) ; 
			Tools.SetDropDownListCaseInsensitive( sSpCounty, dataLoan.sSpCounty );
			
            sSpHasClothesWasher.Checked          = dataLoan.sSpHasClothesWasher;
            sSpHasDishWasher.Checked             = dataLoan.sSpHasDishWasher;
            sSpHasDryer.Checked                  = dataLoan.sSpHasDryer;
            sSpHasGarbageDisposal.Checked        = dataLoan.sSpHasGarbageDisposal;
            sSpHasOven.Checked                   = dataLoan.sSpHasOven;
            sSpHasRefrig.Checked                 = dataLoan.sSpHasRefrig;
            sSpHasVentFan.Checked                = dataLoan.sSpHasVentFan;
            sSpHasWwCarpet.Checked               = dataLoan.sSpHasWwCarpet;
            sSpLeaseAnnualGroundRent.Text        = dataLoan.sSpLeaseAnnualGroundRent_rep;
            sSpLeaseAnnualGroundRent.ReadOnly    = (dataLoan.sIsHousingExpenseMigrated && dataLoan.sLT == E_sLT.VA
                                                    && dataLoan.sGroundRentExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            sSpLeaseIs99Yrs.Checked              = dataLoan.sSpLeaseIs99Yrs;
            sSpLeaseIsRenewable.Checked          = dataLoan.sSpLeaseIsRenewable;
            sSpLegalDesc.Text                    = dataLoan.sSpLegalDesc;
            sSpLotAcres.Text                     = dataLoan.sSpLotAcres;
            sSpLotDimension.Text                 = dataLoan.sSpLotDimension;
            sSpLotIrregularSqf.Text              = dataLoan.sSpLotIrregularSqf;
            sSpLotIsAcres.Checked                = dataLoan.sSpLotIsAcres;
            sSpLotIsIrregular.Checked            = dataLoan.sSpLotIsIrregular;
            sSpMineralRightsReservedExplain.Text = dataLoan.sSpMineralRightsReservedExplain;
            sSpState.Value                       = dataLoan.sSpState;
            sSpZip.Text                          = dataLoan.sSpZip;
            sVaBuildingStatusTLckd.Checked       = dataLoan.sVaBuildingStatusTLckd;
            sVaConstructCompleteD.Text           = dataLoan.sVaConstructCompleteD_rep;
            sVaConstructExpiredD.Text            = dataLoan.sVaConstructExpiredD_rep;
            sVaConstructWarrantyProgramNm.Text   = dataLoan.sVaConstructWarrantyProgramNm;
            sVaNumOfBuildings.Text               = dataLoan.sVaNumOfBuildings;
            sVaNumOfLivingUnits.Text             = dataLoan.sVaNumOfLivingUnits;
            sVaOwnerNm.Text                      = dataLoan.sVaOwnerNm;
            sVaRefiAmt.Text                      = dataLoan.sVaRefiAmt_rep;
            sVaSpOccupantNm.Text                 = dataLoan.sVaSpOccupantNm;
            sVaSpOccupantPhone.Text              = dataLoan.sVaSpOccupantPhone;
            sVaSpRentalMonthly.Text              = dataLoan.sVaSpRentalMonthly_rep;
            sVaTitleLimitDesc.Text               = dataLoan.sVaTitleLimitDesc;
            sVaTitleLimitIsCondo.Checked         = dataLoan.sVaTitleLimitIsCondo;
            sVaTitleLimitIsPud.Checked           = dataLoan.sVaTitleLimitIsPud;


            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805FirmMakingRequest, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1805FirmMakingRequestCompanyName.Text = preparer.CompanyName;
            VA26_1805FirmMakingRequestStreetAddr.Text = preparer.StreetAddr;
            VA26_1805FirmMakingRequestCity.Text = preparer.City;
            VA26_1805FirmMakingRequestState.Value = preparer.State;
            VA26_1805FirmMakingRequestZip.Text = preparer.Zip;
            VA26_1805FirmMakingRequestNotificationEmail.Text = preparer.EmailAddr;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Broker, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1805BrokerCompanyName.Text = preparer.CompanyName;
            VA26_1805BrokerPhoneOfCompany.Text = preparer.PhoneOfCompany;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805KeyAt, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1805KeyAtStreetAddr.Text = preparer.StreetAddr;
            VA26_1805KeyAtCity.Text = preparer.City;
            VA26_1805KeyAtState.Value = preparer.State;
            VA26_1805KeyAtZip.Text = preparer.Zip;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Builder, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1805BuilderCompanyName.Text = preparer.CompanyName;
            VA26_1805BuilderStreetAddr.Text = preparer.StreetAddr;
            VA26_1805BuilderCity.Text = preparer.City;
            VA26_1805BuilderState.Value = preparer.State;
            VA26_1805BuilderZip.Text = preparer.Zip;
            VA26_1805BuilderPhoneOfCompany.Text = preparer.PhoneOfCompany;
            VA26_1805BuilderId.Text = preparer.LicenseNumOfCompany;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Warrantor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1805WarrantorCompanyName.Text = preparer.CompanyName;
            VA26_1805WarrantorStreetAddr.Text = preparer.StreetAddr;
            VA26_1805WarrantorCity.Text = preparer.City;
            VA26_1805WarrantorState.Value = preparer.State;
            VA26_1805WarrantorZip.Text = preparer.Zip;
            VA26_1805WarrantorPhoneOfCompany.Text = preparer.PhoneOfCompany;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Authorize, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1805AuthorizeTitle.Text = preparer.Title;
            VA26_1805AuthorizePhone.Text = preparer.Phone;
            VA26_1805AuthorizePrepareDate.Text = preparer.PrepareDate_rep;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1805AppraiserPreparerName.Text = preparer.PreparerName;
            VA26_1805AppraiserCompanyName.Text = preparer.CompanyName;
            VA26_1805AppraiserStreetAddr.Text = preparer.StreetAddr;
            VA26_1805AppraiserCity.Text = preparer.City;
            VA26_1805AppraiserState.Value = preparer.State;
            VA26_1805AppraiserZip.Text = preparer.Zip;
            VA26_1805AppraiserPrepareDate.Text = preparer.PrepareDate_rep;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805ApplicablePointOfContact, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            VA26_1805ApplicablePointOfContactName.Text = preparer.PreparerName;
            VA26_1805ApplicablePointOfContactAddress.Text = preparer.StreetAddr;
            VA26_1805ApplicablePointOfContactCity.Text = preparer.City;
            VA26_1805ApplicablePointOfContactState.Value = preparer.State;
            VA26_1805ApplicablePointOfContactZip.Text = preparer.Zip;
            VA26_1805ApplicablePointOfContactPhone.Text = preparer.Phone;
        }

        protected override void SaveData() 
        {
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework  = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));


        }
    }
}
