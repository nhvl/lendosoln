﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VALoanComparison2018.aspx.cs" Inherits="LendersOfficeApp.newlos.VA.VALoanComparison2018" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        body {
             background-color: gainsboro;
        }
        table {
            padding: 0px;
            border-spacing: 0px;
            border-collapse: collapse;
        }
        td {
            padding: 4px 0px 4px 0px;
        }
        .align-center {
            align-content: center;
        }
        .align-right {
            float: right;
            text-align: right;
            padding-right: 5px;
        }
        h1 {
            margin: 0px;
        }        
        .bottom-border {
            border-bottom: solid 1px black;
        }
        .field-set {
            border: 1px;
            border-style: solid;
            border-color:black;
        }
        input[preset="money"][class="width-105"], input[preset="percent"][class="width-105"] {
            width: 105px !important; /* this gets overwritten by mask.js unless !important is used. */
        }
    </style>
    <title>VA Loan Comparison</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1 class="MainRightHeader">
            VA Loan Comparison
        </h1>
		<table class="FieldLabel InsetBorder">
			<tr>
				<td>
				</td>
				<td class="align-center">
				    Existing Loan
				</td>
				<td class="align-center">
				    Initial Proposed Loan
				</td>
                <td class="align-center">
				    Final Proposed Loan
				</td>
			</tr>
			<tr>
				<td>
				    Loan Number
				</td>
				<td>
				    <asp:TextBox ID="sVaLoanNumCurrentLoan" class="width-105" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				</td>
                <td>
                    <asp:TextBox ID="sAgencyCaseNum" runat="server" class="width-105" Readonly="true"></asp:TextBox>
                </td>
				<td>
				    <asp:TextBox data-field-id="sAgencyCaseNum" ID="sAgencyCaseNum_2" class="width-105" ReadOnly="true" runat="server"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td>
				    Loan Amount
				</td>
				<td>
					<ml:MoneyTextBox id="sVaLoanAmtCurrentLoan" class="width-105" runat="server" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
				</td>
                <td>
                    <ml:MoneyTextBox id="sVaInitialStatementLoanAmt" runat="server" class="width-105" Readonly="true"></ml:MoneyTextBox>
                </td>
                <td>
					<ml:MoneyTextBox id="sFinalLAmt" class="width-105" ReadOnly="true" runat="server" preset="money"></ml:MoneyTextBox>
				</td>
			</tr>
			<tr>
				<td>
				    Interest Rate
				</td>
				<td>
					<ml:PercentTextBox id="sVaNoteIrCurrentLoan" class="width-105" runat="server" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox>
				</td>
                <td>
                    <asp:TextBox id="sVaInitialStatementNoteIR" runat="server" class="width-105" Readonly="true"></asp:TextBox>
                </td>
				<td>
					<asp:TextBox id="sRateAmortT" class="width-105" ReadOnly="true" runat="server"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td>
				    Term
				</td>
				<td>
					<asp:TextBox id="sVaTermCurrentLoan" class="width-105" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				</td>
                <td>
                    <asp:TextBox id="sVaInitialStatementTerm" runat="server" class="width-105" Readonly="true"></asp:TextBox>
                </td>
				<td>
					<asp:TextBox id="sTerm" class="width-105" ReadOnly="true" runat="server"></asp:TextBox>
				</td>
			</tr>
			<tr>
				<td>
				    Principal & Interest
				</td>
				<td>
				    <ml:MoneyTextBox ID="sVaPICurrentLoan" class="width-105" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
				</td>
                <td>
                    <ml:MoneyTextBox id="sVaInitialStatementPI" runat="server" class="width-105" Readonly="true"></ml:MoneyTextBox>
                </td>
				<td>
				    <ml:MoneyTextBox ID="sProThisMPmt" ReadOnly="true" class="width-105" runat="server"></ml:MoneyTextBox>
				</td>
			</tr>
            <tr>
				<td>
				    PITI
				</td>
				<td>
				    <ml:MoneyTextBox ID="sVaMonthlyPmtCurrentLoan" class="width-105" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
				</td>
                <td>
                    <ml:MoneyTextBox id="sVaInitialStatementMonthlyPmt" runat="server" class="width-105" Readonly="true"></ml:MoneyTextBox>
                </td>
				<td>
				    <ml:MoneyTextBox ID="sMonthlyPmt" ReadOnly="true" class="width-105" runat="server"></ml:MoneyTextBox>
				</td>                
			</tr>
		</table>
        <table>
            <tr>
                <td>
                    <fieldset class="field-set">
                        <legend class="FieldLabel">Initial Statement</legend>
                        <table class="FieldLabel">                        
                            <tr>
                                <td colspan="3">
                                    Origination Charges
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaInitialOriginationCharge"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="width-350">
                                    Services You Cannot Shop For (including VA Funding Fee)
                                </td>
                                <td class="width-10 align-center">
                                    +
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaInitialServYouCannotShopFor"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="padding-left-20">
                                    VA Funding Fee
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105 align-right" Readonly="true" ID="sVaInitialFundingFee"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Services You Can Shop For                                    
                                </td>
                                <td class="align-center">
                                    +
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" id="sVaInitialServYouCanShopFor"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Taxes and Other Government Fees
                                </td>
                                <td class="align-center">
                                    +
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaInitialTaxesOtherGovFee"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Other
                                </td>
                                <td class="align-center">
                                    +
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaInitialOtherFee"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="bottom-border">
                                <td colspan="2">
                                    Lender Credits (from section J)
                                </td>
                                <td class="align-center">
                                    −
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sTRIDLoanEstimateLenderCredits"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Total Closing Costs
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaInitialTotalClosingCost"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Monthly PI Payment Decrease
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaInitialLoanCompMonthlyPmtDecreaseAmt"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Months to Recoup Costs
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaInitialLoanCompRecoupCostsTime"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </fieldset>              
                </td>
                <td class="width-10">
                </td>
                <td>
                    <fieldset class="field-set">
                        <legend class="FieldLabel">Final Statement</legend>
                        <table class="FieldLabel">                        
                            <tr>
                                <td colspan="3">
                                    Origination Charges
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaFinalOriginationCharge"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="width-350">
                                    Services You Did Not Shop For (including VA Funding Fee)
                                </td>
                                <td class="width-10 align-center">
                                    +
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaFinalServYouDidNotShopFor"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="padding-left-20">
                                    VA Funding Fee
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105 align-right" Readonly="true" ID="sVaFinalFundingFee"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Services You Did Shop For                                    
                                </td>
                                <td class="align-center">
                                    +
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaFinalServYouDidShopFor"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Taxes and Other Government Fees
                                </td>
                                <td class="align-center">
                                    +
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaFinalTaxesOtherGovFee"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Other
                                </td>
                                <td class="align-center">
                                    +
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaFinalOtherFee"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="bottom-border">
                                <td colspan="2">
                                    Lender Credits (from section J)
                                </td>
                                <td class="align-center">
                                    −
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sTRIDClosingDisclosureGeneralLenderCredits"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Total Closing Costs
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaFinalTotalClosingCost"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Monthly PI Payment Decrease
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaLoanCompMonthlyPmtDecreaseAmt"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    Months to Recoup Costs
                                </td>
                                <td>
                                    <asp:TextBox runat="server" class="width-105" Readonly="true" ID="sVaFinalLoanCompRecoupCostsTime"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
        <asp:CheckBox ID="sVaNewPmtQualified" Text="The lender hereby certifies that the veteran qualifies for the new monthly payment which exceeds the previous payment by 20 percent or more." runat="server" />
    </div>   
    </form>
</body>
</html>
