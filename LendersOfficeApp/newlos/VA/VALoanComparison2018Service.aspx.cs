﻿namespace LendersOfficeApp.newlos.VA
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Service Item for VA Loan Comparison 2018.
    /// </summary>
    public class VALoanComparison2018ServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Construct page data class for VA Loan Comparison 2018.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <returns>CPageData object.</returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(VALoanComparison2018ServiceItem));
        }

        /// <summary>
        /// Binds data from the page.
        /// </summary>
        /// <param name="dataLoan">Data loan object.</param>
        /// <param name="dataApp">Data app object.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sVaLoanNumCurrentLoan = this.GetString("sVaLoanNumCurrentLoan");
            dataLoan.sVaLoanAmtCurrentLoan_rep = this.GetString("sVaLoanAmtCurrentLoan");
            dataLoan.sVaNoteIrCurrentLoan_rep = this.GetString("sVaNoteIrCurrentLoan");
            dataLoan.sVaTermCurrentLoan_rep = this.GetString("sVaTermCurrentLoan");
            dataLoan.sVaPICurrentLoan_rep = this.GetString("sVaPICurrentLoan");
            dataLoan.sVaMonthlyPmtCurrentLoan_rep = this.GetString("sVaMonthlyPmtCurrentLoan");

            dataLoan.sVaNewPmtQualified = this.GetBool("sVaNewPmtQualified");            
        }

        /// <summary>
        /// Loads data to the page.
        /// </summary>
        /// <param name="dataLoan">Data loan object.</param>
        /// <param name="dataApp">Data app object.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            CPageData dataLoanWithArchive = null;            

            var initialLEDates = dataLoan.sLoanEstimateDatesInfo.InitialLoanEstimate;
            if (initialLEDates != null)
            {
                // Initialize the 2nd loan with the archive and the user values
                dataLoanWithArchive = CPageData.CreateUsingSmartDependency(sLId, typeof(VALoanComparison2018));
                dataLoanWithArchive.InitLoad();

                BindData(dataLoanWithArchive, null);

                dataLoanWithArchive.ApplyClosingCostArchive(dataLoanWithArchive.sClosingCostArchive.FirstOrDefault(a => a.Id == initialLEDates.ArchiveId));
                dataLoan.ApplyClosingCostArchive(null);
            }
            else
            {
                dataLoan.ApplyClosingCostArchive(null);
                dataLoanWithArchive = dataLoan;
            }

            // Existing Loan
            this.SetResult("sVaLoanNumCurrentLoan", dataLoan.sVaLoanNumCurrentLoan);
            this.SetResult("sVaLoanAmtCurrentLoan", dataLoan.sVaLoanAmtCurrentLoan_rep);
            this.SetResult("sVaNoteIrCurrentLoan", dataLoan.sVaNoteIrCurrentLoan_rep);
            this.SetResult("sVaTermCurrentLoan", dataLoan.sVaTermCurrentLoan_rep);
            this.SetResult("sVaPICurrentLoan", dataLoan.sVaPICurrentLoan_rep);
            this.SetResult("sVaMonthlyPmtCurrentLoan", dataLoan.sVaMonthlyPmtCurrentLoan_rep);

            // Initial Proposed Loan
            this.SetResult("sAgencyCaseNum_2", dataLoanWithArchive.sAgencyCaseNum);
            this.SetResult("sVaInitialStatementLoanAmt", dataLoanWithArchive.sFinalLAmt_rep);
            this.SetResult("sVaInitialStatementNoteIR", dataLoanWithArchive.sRateAmortT);
            this.SetResult("sVaInitialStatementTerm", dataLoanWithArchive.sTerm_rep);
            this.SetResult("sVaInitialStatementPI", dataLoanWithArchive.sProThisMPmt_rep);
            this.SetResult("sVaInitialStatementMonthlyPmt", dataLoanWithArchive.sMonthlyPmt_rep);

            // Final Proposed Loan
            this.SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            this.SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            this.SetResult("sRateAmortT", dataLoan.sRateAmortT);
            this.SetResult("sTerm", dataLoan.sTerm_rep);
            this.SetResult("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            this.SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);

            // Initial Statement
            this.SetResult("sVaInitialOriginationCharge", dataLoanWithArchive.sVaInitialOriginationCharge_rep);
            this.SetResult("sVaInitialServYouCannotShopFor", dataLoanWithArchive.sVaInitialServYouCannotShopFor_rep);
            this.SetResult("sVaInitialFundingFee", dataLoanWithArchive.sVaInitialFundingFee_rep);
            this.SetResult("sVaInitialServYouCanShopFor", dataLoanWithArchive.sVaInitialServYouCanShopFor_rep);
            this.SetResult("sVaInitialTaxesOtherGovFee", dataLoanWithArchive.sVaInitialTaxesOtherGovFee_rep);
            this.SetResult("sVaInitialOtherFee", dataLoanWithArchive.sVaInitialOtherFee_rep);
            this.SetResult("sTRIDLoanEstimateLenderCredits", dataLoanWithArchive.sTRIDLoanEstimateLenderCredits_rep);
            this.SetResult("sVaInitialTotalClosingCost", dataLoanWithArchive.sVaInitialTotalClosingCost_rep);
            this.SetResult("sVaInitialLoanCompMonthlyPmtDecreaseAmt", dataLoanWithArchive.sVaInitialLoanCompMonthlyPmtDecreaseAmt_rep);
            this.SetResult("sVaInitialLoanCompRecoupCostsTime", dataLoanWithArchive.sVaInitialLoanCompRecoupCostsTime_rep);

            // Final Statement
            this.SetResult("sVaFinalOriginationCharge", dataLoan.sVaFinalOriginationCharge_rep);
            this.SetResult("sVaFinalServYouDidNotShopFor", dataLoan.sVaFinalServYouDidNotShopFor_rep);
            this.SetResult("sVaFinalFundingFee", dataLoan.sVaFinalFundingFee_rep);
            this.SetResult("sVaFinalServYouDidShopFor", dataLoan.sVaFinalServYouDidShopFor_rep);
            this.SetResult("sVaFinalTaxesOtherGovFee", dataLoan.sVaFinalTaxesOtherGovFee_rep);
            this.SetResult("sVaFinalOtherFee", dataLoan.sVaFinalOtherFee_rep);
            this.SetResult("sTRIDClosingDisclosureGeneralLenderCredits", dataLoan.sTRIDClosingDisclosureGeneralLenderCredits_rep);
            this.SetResult("sVaFinalTotalClosingCost", dataLoan.sVaFinalTotalClosingCost_rep);
            this.SetResult("sVaLoanCompMonthlyPmtDecreaseAmt", dataLoan.sVaLoanCompMonthlyPmtDecreaseAmt_rep);
            this.SetResult("sVaFinalLoanCompRecoupCostsTime", dataLoan.sVaFinalLoanCompRecoupCostsTime_rep);

            // checkbox at the end
            this.SetResult("sVaNewPmtQualified", dataLoan.sVaNewPmtQualified);
        }
    }

    /// <summary>
    /// Service page for VA Loan Comparison 2018.
    /// </summary>
    public partial class VALoanComparison2018Service : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initialize the service items.
        /// </summary>
        protected override void Initialize()
        {
            AddBackgroundItem(string.Empty, new VALoanComparison2018ServiceItem());
        }
    }
}