using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.VA
{
    public class VALoanAnalysisServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VALoanAnalysisServiceItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            IPrimaryEmploymentRecord aBPrimaryEmp = dataApp.aBEmpCollection.GetPrimaryEmp(false);
            IPrimaryEmploymentRecord aCPrimaryEmp = dataApp.aCEmpCollection.GetPrimaryEmp(false);


            if (null != aBPrimaryEmp) 
            {
                SetResult("aBPrimaryEmpEmplmtLenInMonths", aBPrimaryEmp.EmplmtLenInMonths_rep);
                SetResult("aBPrimaryEmpEmplmtLenInYrs", aBPrimaryEmp.EmplmtLenInYrs_rep);
                SetResult("aBPrimaryEmpJobTitle", aBPrimaryEmp.JobTitle);
            }
            if (null != aCPrimaryEmp) 
            {
                SetResult("aCPrimaryEmpEmplmtLenInMonths", aCPrimaryEmp.EmplmtLenInMonths_rep);
                SetResult("aCPrimaryEmpEmplmtLenInYrs", aCPrimaryEmp.EmplmtLenInYrs_rep);
                SetResult("aCPrimaryEmpJobTitle", aCPrimaryEmp.JobTitle);
            }
            SetResult("aAsstLiqTot",                 dataApp.aAsstLiqTot_rep);
            SetResult("aBAge",                       dataApp.aBAge_rep);
            SetResult("aBDependAges",                dataApp.aBDependAges);
            SetResult("aBFirstNm",                   dataApp.aBFirstNm);
            SetResult("aBLastNm",                    dataApp.aBLastNm);
            SetResult("aBMidNm",                     dataApp.aBMidNm);
            SetResult("aBSuffix",                    dataApp.aBSuffix);
            SetResult("aCAge",                       dataApp.aCAge_rep);
            SetResult("aCFirstNm",                   dataApp.aCFirstNm);
            SetResult("aCLastNm",                    dataApp.aCLastNm);
            SetResult("aCMidNm",                     dataApp.aCMidNm);
            SetResult("aCSuffix",                    dataApp.aCSuffix);
            SetResult("aFHABCaivrsNum",              dataApp.aFHABCaivrsNum);
            SetResult("aFHACCaivrsNum",              dataApp.aFHACCaivrsNum);
            SetResult("aPresTotHExp",                dataApp.aPresTotHExp_rep);
            SetResult("aVaBEmplmtI",                 dataApp.aVaBEmplmtI_rep);
            SetResult("aVaBEmplmtILckd",             dataApp.aVaBEmplmtILckd);
            SetResult("aVaBFedITax",                 dataApp.aVaBFedITax_rep);
            SetResult("aVaBNetI",                    dataApp.aVaBNetI_rep);
            SetResult("aVaBOITax",                   dataApp.aVaBOITax_rep);
            SetResult("aVaBONetI",                   dataApp.aVaBONetI_rep);
            SetResult("aVaBONetILckd",               dataApp.aVaBONetILckd);
            SetResult("aVaBSsnTax",                  dataApp.aVaBSsnTax_rep);
            SetResult("aVaBStateITax",               dataApp.aVaBStateITax_rep);
            SetResult("aVaBTakehomeEmplmtI",         dataApp.aVaBTakehomeEmplmtI_rep);
            SetResult("aVaBTotITax",                 dataApp.aVaBTotITax_rep);
            SetResult("aVaCEmplmtI",                 dataApp.aVaCEmplmtI_rep);
            SetResult("aVaCEmplmtILckd",             dataApp.aVaCEmplmtILckd);
            SetResult("aVaCFedITax",                 dataApp.aVaCFedITax_rep);
            SetResult("aVaCNetI",                    dataApp.aVaCNetI_rep);
            SetResult("aVaCOITax",                   dataApp.aVaCOITax_rep);
            SetResult("aVaCONetI",                   dataApp.aVaCONetI_rep);
            SetResult("aVaCONetILckd",               dataApp.aVaCONetILckd);
            SetResult("aVaCSsnTax",                  dataApp.aVaCSsnTax_rep);
            SetResult("aVaCStateITax",               dataApp.aVaCStateITax_rep);
            SetResult("aVaCTakehomeEmplmtI",         dataApp.aVaCTakehomeEmplmtI_rep);
            SetResult("aVaCTotITax",                 dataApp.aVaCTotITax_rep);
            SetResult("aVaCrRecordSatisfyTri",       dataApp.aVaCrRecordSatisfyTri);
            SetResult("aVaFamilySuportGuidelineAmt", dataApp.aVaFamilySuportGuidelineAmt_rep);
            SetResult("aVaFamilySupportBal",         dataApp.aVaFamilySupportBal_rep);
            SetResult("aVaLAnalysisN",               dataApp.aVaLAnalysisN);
            SetResult("aVaLMeetCrStandardTri",       dataApp.aVaLMeetCrStandardTri);
            SetResult("aVaLiaBalTotExisting",        dataApp.aVaLiaBalTotExisting_rep);
            SetResult("aVaLiaMonTotDeducted",        dataApp.aVaLiaMonTotDeducted_rep);
            SetResult("aVaLiaMonTotExisting",        dataApp.aVaLiaMonTotExisting_rep);
            SetResult("aVaNetEffectiveI",            dataApp.aVaNetEffectiveI_rep);
            SetResult("aVaNetI",                     dataApp.aVaNetI_rep);
            SetResult("aVaONetI",                    dataApp.aVaONetI_rep);
            SetResult("aVaONetIDesc",                dataApp.aVaONetIDesc);
            SetResult("aVaRatio",                    dataApp.aVaRatio_rep);
            SetResult("aVaTakehomeEmplmtI",          dataApp.aVaTakehomeEmplmtI_rep);
            SetResult("aVaTotEmplmtI",               dataApp.aVaTotEmplmtI_rep);
            SetResult("aVaTotITax",                  dataApp.aVaTotITax_rep);
            SetResult("aVaUtilityIncludedTri",       dataApp.aVaUtilityIncludedTri);
            SetResult("sFinalLAmt",                  dataLoan.sFinalLAmt_rep);
            SetResult("sNoteIR",                     dataLoan.sNoteIR_rep);
            SetResult("sTermInYr",                   dataLoan.sTermInYr_rep);
            SetResult("sVaCashdwnPmt",               dataLoan.sVaCashdwnPmt_rep);
            SetResult("sVaCashdwnPmtLckd",           dataLoan.sVaCashdwnPmtLckd);
            SetResult("sVaMaintainAssessPmt",        dataLoan.sVaMaintainAssessPmt_rep);
            SetResult("sVaMaintainAssessPmtLckd",    dataLoan.sVaMaintainAssessPmtLckd);
            SetResult("sVaProHazIns",                dataLoan.sVaProHazIns_rep);
            SetResult("sVaProHazInsLckd",            dataLoan.sVaProHazInsLckd);
            SetResult("sVaProMaintenancePmt",        dataLoan.sVaProMaintenancePmt_rep);
			SetResult("sVaProMaintenanceAndUtilityPmt", dataLoan.sVaProMaintenanceAndUtilityPmt_rep);
            SetResult("sVaProMonthlyPmt",            dataLoan.sVaProMonthlyPmt_rep);
            SetResult("sVaProMonthlyPmt2",           dataLoan.sVaProMonthlyPmt_rep);
            SetResult("sVaProRealETx",               dataLoan.sVaProRealETx_rep);
            SetResult("sVaProRealETxLckd",           dataLoan.sVaProRealETxLckd);
            SetResult("sVaProThisMPmt",              dataLoan.sVaProThisMPmt_rep);
            SetResult("sVaProThisMPmtLckd",          dataLoan.sVaProThisMPmtLckd);
            SetResult("sVaProUtilityPmt",            dataLoan.sVaProUtilityPmt_rep);
            SetResult("sVaSpecialAssessPmt",         dataLoan.sVaSpecialAssessPmt_rep);

            SetResult("sVaRecommendAppApprovedT",    dataLoan.sVaRecommendAppApprovedT);
            SetResult("sVAFinalActionT",             dataLoan.sVAFinalActionT);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            string aBPrimaryEmpJobTitle = this.GetString("aBPrimaryEmpJobTitle");
            IPrimaryEmploymentRecord aBPrimaryEmp = dataApp.aBEmpCollection.GetPrimaryEmp(!string.IsNullOrWhiteSpace(aBPrimaryEmpJobTitle)); // if the value is blank, don't force create
            if (aBPrimaryEmp != null)
            {
                aBPrimaryEmp.JobTitle = aBPrimaryEmpJobTitle;
                aBPrimaryEmp.Update();
            }

            string aCPrimaryEmpJobTitle = this.GetString("aCPrimaryEmpJobTitle");
            IPrimaryEmploymentRecord aCPrimaryEmp = dataApp.aCEmpCollection.GetPrimaryEmp(!string.IsNullOrWhiteSpace(aCPrimaryEmpJobTitle));
            if (aCPrimaryEmp != null)
            {
                aCPrimaryEmp.JobTitle = aBPrimaryEmpJobTitle;
                aCPrimaryEmp.Update();
            }

            dataApp.aBAge_rep                       = GetString("aBAge");
            dataApp.aBDependAges                    = GetString("aBDependAges");
            dataApp.aBFirstNm                       = GetString("aBFirstNm");
            dataApp.aBLastNm                        = GetString("aBLastNm");
            dataApp.aBMidNm                         = GetString("aBMidNm");
            dataApp.aBSuffix                        = GetString("aBSuffix");
            dataApp.aCAge_rep                       = GetString("aCAge");
            dataApp.aCFirstNm                       = GetString("aCFirstNm");
            dataApp.aCLastNm                        = GetString("aCLastNm");
            dataApp.aCMidNm                         = GetString("aCMidNm");
            dataApp.aCSuffix                        = GetString("aCSuffix");
            dataApp.aFHABCaivrsNum                  = GetString("aFHABCaivrsNum");
            dataApp.aFHACCaivrsNum                  = GetString("aFHACCaivrsNum");
            dataApp.aVaBEmplmtI_rep                 = GetString("aVaBEmplmtI");
            dataApp.aVaBEmplmtILckd                 = GetBool("aVaBEmplmtILckd");
            dataApp.aVaBFedITax_rep                 = GetString("aVaBFedITax");
            dataApp.aVaBOITax_rep                   = GetString("aVaBOITax");
            dataApp.aVaBONetI_rep                   = GetString("aVaBONetI");
            dataApp.aVaBONetILckd                   = GetBool("aVaBONetILckd");
            dataApp.aVaBSsnTax_rep                  = GetString("aVaBSsnTax");
            dataApp.aVaBStateITax_rep               = GetString("aVaBStateITax");
            dataApp.aVaCEmplmtI_rep                 = GetString("aVaCEmplmtI");
            dataApp.aVaCEmplmtILckd                 = GetBool("aVaCEmplmtILckd");
            dataApp.aVaCFedITax_rep                 = GetString("aVaCFedITax");
            dataApp.aVaCOITax_rep                   = GetString("aVaCOITax");
            dataApp.aVaCONetI_rep                   = GetString("aVaCONetI");
            dataApp.aVaCONetILckd                   = GetBool("aVaCONetILckd");
            dataApp.aVaCSsnTax_rep                  = GetString("aVaCSsnTax");
            dataApp.aVaCStateITax_rep               = GetString("aVaCStateITax");
            dataApp.aVaCrRecordSatisfyTri           = GetTriState("aVaCrRecordSatisfyTri");
            dataApp.aVaFamilySuportGuidelineAmt_rep = GetString("aVaFamilySuportGuidelineAmt");
            dataApp.aVaLAnalysisN                   = GetString("aVaLAnalysisN");
            dataApp.aVaLMeetCrStandardTri           = GetTriState("aVaLMeetCrStandardTri");
            dataApp.aVaONetIDesc                    = GetString("aVaONetIDesc");
            dataApp.aVaUtilityIncludedTri           = GetTriState("aVaUtilityIncludedTri");
            dataLoan.sNoteIR_rep                    = GetString("sNoteIR");
            dataLoan.sVaCashdwnPmt_rep              = GetString("sVaCashdwnPmt");
            dataLoan.sVaCashdwnPmtLckd              = GetBool("sVaCashdwnPmtLckd");
            dataLoan.sVaMaintainAssessPmtLckd       = GetBool("sVaMaintainAssessPmtLckd");
            dataLoan.sVaMaintainAssessPmt_rep       = GetString("sVaMaintainAssessPmt");
            dataLoan.sVaProHazIns_rep               = GetString("sVaProHazIns");
            dataLoan.sVaProHazInsLckd               = GetBool("sVaProHazInsLckd");
            dataLoan.sVaProMaintenancePmt_rep       = GetString("sVaProMaintenancePmt");
            dataLoan.sVaProRealETx_rep              = GetString("sVaProRealETx");
            dataLoan.sVaProRealETxLckd              = GetBool("sVaProRealETxLckd");
            dataLoan.sVaProThisMPmt_rep             = GetString("sVaProThisMPmt");
            dataLoan.sVaProThisMPmtLckd             = GetBool("sVaProThisMPmtLckd");
            dataLoan.sVaProUtilityPmt_rep           = GetString("sVaProUtilityPmt");
            dataLoan.sVaSpecialAssessPmt_rep        = GetString("sVaSpecialAssessPmt");
            dataApp.aVALAnalysisValue_rep           = GetString("aVALAnalysisValue");
            dataApp.aVALAnalysisExpirationD_rep     = GetString("aVALAnalysisExpirationD");
            dataApp.aVALAnalysisEconomicLifeYrs_rep = GetString("aVALAnalysisEconomicLifeYrs");

            dataLoan.sVaRecommendAppApprovedT       = GetTriState("sVaRecommendAppApprovedT");
            dataLoan.sVAFinalActionT                = GetTriState("sVAFinalActionT");
        }
    }
	/// <summary>
	/// Summary description for VALoanAnalysisService.
	/// </summary>
	public partial class VALoanAnalysisService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new VALoanAnalysisServiceItem());
        }
	}
}
