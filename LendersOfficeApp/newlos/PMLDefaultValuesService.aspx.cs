namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using LendersOffice.Security;

    public class PMLDefaultValuesServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreatePmlPageDataUsingSmartDependency(sLId, typeof(PMLDefaultValuesServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sCreditScoreEstimatePe_rep = GetString("sCreditScoreEstimatePe");
            dataApp.aBExperianScorePe_rep = GetString("aBExperianScorePe");
            dataApp.aBTransUnionScorePe_rep = GetString("aBTransUnionScorePe");
            dataApp.aBEquifaxScorePe_rep = GetString("aBEquifaxScorePe");
            dataApp.aCExperianScorePe_rep = GetString("aCExperianScorePe");
            dataApp.aCTransUnionScorePe_rep = GetString("aCTransUnionScorePe");
            dataApp.aCEquifaxScorePe_rep = GetString("aCEquifaxScorePe");
            dataLoan.sTransmOMonPmtPe_rep = GetString("sTransmOMonPmtPe");
            dataLoan.sProdCrManualNonRolling30MortLateCount_rep = GetString("sProdCrManualNonRolling30MortLateCount");
            dataLoan.sProdCrManual60MortLateCount_rep = GetString("sProdCrManual60MortLateCount");
            dataLoan.sProdCrManual90MortLateCount_rep = GetString("sProdCrManual90MortLateCount");
            dataLoan.sProdCrManual120MortLateCount_rep = GetString("sProdCrManual120MortLateCount");
            dataLoan.sProdCrManual150MortLateCount_rep = GetString("sProdCrManual150MortLateCount");
            dataLoan.sProdCrManual30MortLateCount_rep = GetString("sProdCrManual30MortLateCount");
            dataLoan.sProdCrManualForeclosureHas = GetBool("sProdCrManualForeclosureHas");
            dataLoan.sProdCrManualForeclosureRecentFileMon_rep = GetString("sProdCrManualForeclosureRecentFileMon");
            dataLoan.sProdCrManualForeclosureRecentFileYr_rep = GetString("sProdCrManualForeclosureRecentFileYr");
            dataLoan.sProdCrManualForeclosureRecentStatusT = (E_sProdCrManualDerogRecentStatusT) GetInt("sProdCrManualForeclosureRecentStatusT");
            dataLoan.sProdCrManualForeclosureRecentSatisfiedMon_rep = GetString("sProdCrManualForeclosureRecentSatisfiedMon");
            dataLoan.sProdCrManualForeclosureRecentSatisfiedYr_rep = GetString("sProdCrManualForeclosureRecentSatisfiedYr");
            dataLoan.sProdCrManualBk7Has = GetBool("sProdCrManualBk7Has");
            dataLoan.sProdCrManualBk7RecentFileMon_rep = GetString("sProdCrManualBk7RecentFileMon");
            dataLoan.sProdCrManualBk7RecentFileYr_rep = GetString("sProdCrManualBk7RecentFileYr");
            dataLoan.sProdCrManualBk7RecentStatusT = (E_sProdCrManualDerogRecentStatusT) GetInt("sProdCrManualBk7RecentStatusT");
            dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep = GetString("sProdCrManualBk7RecentSatisfiedMon");
            dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep = GetString("sProdCrManualBk7RecentSatisfiedYr");
            dataLoan.sProdCrManualBk13Has = GetBool("sProdCrManualBk13Has");
            dataLoan.sProdCrManualBk13RecentFileMon_rep = GetString("sProdCrManualBk13RecentFileMon");
            dataLoan.sProdCrManualBk13RecentFileYr_rep = GetString("sProdCrManualBk13RecentFileYr");
            dataLoan.sProdCrManualBk13RecentStatusT = (E_sProdCrManualDerogRecentStatusT) GetInt("sProdCrManualBk13RecentStatusT");
            dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep = GetString("sProdCrManualBk13RecentSatisfiedMon");
            dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep = GetString("sProdCrManualBk13RecentSatisfiedYr");
			dataLoan.sProdCrManualRolling60MortLateCount_rep = GetString("sProdCrManualRolling60MortLateCount");
			dataLoan.sProdCrManualRolling90MortLateCount_rep = GetString("sProdCrManualRolling90MortLateCount");

            // Step 2
            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aBSsn = GetString("aBSsn");
            dataApp.aProdBCitizenT = (E_aProdCitizenT) GetInt("aProdBCitizenT");
            dataApp.aCFirstNm = GetString("aCFirstNm");
            dataApp.aCMidNm = GetString("aCFirstNm");
            dataApp.aCLastNm = GetString("aCLastNm");
            dataApp.aCSuffix = GetString("aCSuffix");
            dataApp.aCSsn = GetString("aCSsn");
            dataApp.aIsBorrSpousePrimaryWageEarner = GetBool("aIsBorrSpousePrimaryWageEarner");
            dataLoan.sIsSelfEmployed = GetBool("sIsSelfEmployed");
            dataApp.aLiquidAssetsPe_rep = GetString("aLiquidAssetsPe");
            dataLoan.sAppTotLiqAsset_rep = GetString("aLiquidAssetsPe");
            dataLoan.sOpNegCfPe_rep = GetString("sOpNegCfPe");

            // Step 3
            dataLoan.sSpStatePe = GetString("sSpStatePe");
            dataLoan.sOccTPe = (E_sOccT) GetInt("sOccTPe");
            dataLoan.sProRealETxPe_rep = GetString("sProRealETxPe");
            dataLoan.sProOHExpPe_rep = GetString("sProOHExpPe");
            dataLoan.sOccRPe_rep = GetString("sOccRPe");
            dataLoan.sSpGrossRentPe_rep = GetString("sSpGrossRentPe");
            dataLoan.sProdSpT = (E_sProdSpT) GetInt("sProdSpT");
            dataLoan.sIsNotPermanentlyAffixed = GetBool(nameof(CPageBase.sIsNotPermanentlyAffixed));
            dataLoan.sProdSpStructureT = (E_sProdSpStructureT) GetInt("sProdSpStructureT");
            dataLoan.sProdCondoStories_rep = GetString("sProdCondoStories");
            dataLoan.sProdIsSpInRuralArea = GetBool("sProdIsSpInRuralArea");
            dataLoan.sProdIsCondotel = GetBool("sProdIsCondotel");
            dataLoan.sProdIsNonwarrantableProj = GetBool("sProdIsNonwarrantableProj");
            dataApp.aPresOHExpPe_rep = GetString("aPresOHExpPe");
            dataLoan.sLPurposeTPe = (E_sLPurposeT) GetInt("sLPurposeTPe");
            dataLoan.sIsStudentLoanCashoutRefi = GetBool("sIsStudentLoanCashoutRefi");
            dataLoan.sProdCashoutAmt_rep = GetString("sProdCashoutAmt");
            dataLoan.sHas1stTimeBuyerPe = GetBool("sHas1stTimeBuyerPe");
            dataLoan.sNumFinancedProperties_rep = GetString("sNumFinancedProperties");
            dataLoan.sProdImpound = GetBool("sProdImpound");
            dataLoan.sProdDocT = (E_sProdDocT) GetInt("sProdDocT");
            //dataLoan.sFannieDocT = CommonFunctions.ProductTToFannieDocT(dataLoan.sProdDocT, dataLoan.sFannieDocT);//OPM 58372
            dataLoan.sPrimAppTotNonspIPe_rep = GetString("sPrimAppTotNonspIPe");

            var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
            if (broker.IsEnableRenovationCheckboxInPML || broker.EnableRenovationLoanSupport)
            {
                dataLoan.sIsRenovationLoan = GetBool("sIsRenovationLoan");
                dataLoan.sTotalRenovationCostsLckd = GetBool("sTotalRenovationCostsLckd");
                dataLoan.sTotalRenovationCosts_rep = dataLoan.sIsRenovationLoan ? GetString("sTotalRenovationCosts") : "$0.00";
            }
            dataLoan.sProdRLckdDays_rep = GetString("sProdRLckdDays");
            dataLoan.sProdAvailReserveMonths_rep = GetString("sProdAvailReserveMonths");
            dataLoan.sProd3rdPartyUwResultT = (E_sProd3rdPartyUwResultT) GetInt("sProd3rdPartyUwResultT");
			dataLoan.sProdEstimatedResidualI_rep = GetString("sProdEstimatedResidualI");

            if (dataLoan.BrokerDB.IsLenderFeeBuyoutEnabledForBranchChannel(dataLoan.sBranchChannelT))
            {
                dataLoan.sLenderFeeBuyoutRequestedT = (E_sLenderFeeBuyoutRequestedT)GetInt("sLenderFeeBuyoutRequestedT");
            }

            if (LendersOffice.Admin.BrokerDB.RetrieveById(
                LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerId).HasEnabledPMI || dataLoan.IsNewPMLEnabled)
            {
                // OPM 109393.
                dataLoan.sProdConvMIOptionT = (E_sProdConvMIOptionT)GetInt("sProdConvMIOptionT");
                dataLoan.sConvSplitMIRT = (E_sConvSplitMIRT)GetInt("sConvSplitMIRT");
            }
            else
            {
                dataLoan.sProdMIOptionT = (E_sProdMIOptionT)GetInt("sProdMIOptionT");
            }
            
            bool bProdFilterDueOther = dataLoan.sProdFilterDueOther = GetBool("sProdFilterDueOther", false);
            dataLoan.sProdFilterDue10Yrs = GetBool("sProdFilterDue10Yrs", bProdFilterDueOther);
            dataLoan.sProdFilterDue15Yrs = GetBool("sProdFilterDue15Yrs", bProdFilterDueOther);
            dataLoan.sProdFilterDue20Yrs = GetBool("sProdFilterDue20Yrs", bProdFilterDueOther);
            dataLoan.sProdFilterDue25Yrs = GetBool("sProdFilterDue25Yrs", bProdFilterDueOther);
            dataLoan.sProdFilterDue30Yrs = GetBool("sProdFilterDue30Yrs", bProdFilterDueOther);
            bool bProdFilterFinMethOther = dataLoan.sProdFilterFinMethOther = GetBool("sProdFilterFinMethOther", false);
            dataLoan.sProdFilterFinMethFixed = GetBool("sProdFilterFinMethFixed", bProdFilterFinMethOther);
            dataLoan.sProdFilterFinMeth3YrsArm = GetBool("sProdFilterFinMeth3YrsArm", bProdFilterFinMethOther);
            dataLoan.sProdFilterFinMeth5YrsArm = GetBool("sProdFilterFinMeth5YrsArm", bProdFilterFinMethOther);
            dataLoan.sProdFilterFinMeth7YrsArm = GetBool("sProdFilterFinMeth7YrsArm", bProdFilterFinMethOther);
            dataLoan.sProdFilterFinMeth10YrsArm = GetBool("sProdFilterFinMeth10YrsArm", bProdFilterFinMethOther);
            dataLoan.sProdFilterPmtTPI = GetBool("sProdFilterPmtTPI");
            dataLoan.sProdFilterPmtTIOnly = GetBool("sProdFilterPmtTIOnly");

            dataLoan.sProdIncludeMyCommunityProc = GetBool("sProdIncludeMyCommunityProc");
            dataLoan.sProdIncludeHomePossibleProc = GetBool("sProdIncludeHomePossibleProc");
            dataLoan.sProdIncludeNormalProc = GetBool("sProdIncludeNormalProc");
            dataLoan.sProdIncludeFHATotalProc = GetBool("sProdIncludeFHATotalProc");
            dataLoan.sProdIncludeUSDARuralProc = GetBool("sProdIncludeUSDARuralProc");
            dataLoan.sProdIncludeVAProc = GetBool("sProdIncludeVAProc");

            ProductCodeFilterPopup.BindData(this, dataLoan, PrincipalFactory.CurrentPrincipal);

            dataLoan.sIsCreditQualifying = GetBool("sIsCreditQualifying");

            // Custom PML fields
            dataLoan.sCustomPMLField1_rep = GetString("sCustomPMLField1", "");
            dataLoan.sCustomPMLField2_rep = GetString("sCustomPMLField2", "");
            dataLoan.sCustomPMLField3_rep = GetString("sCustomPMLField3", "");
            dataLoan.sCustomPMLField4_rep = GetString("sCustomPMLField4", "");
            dataLoan.sCustomPMLField5_rep = GetString("sCustomPMLField5", "");
            dataLoan.sCustomPMLField6_rep = GetString("sCustomPMLField6", "");
            dataLoan.sCustomPMLField7_rep = GetString("sCustomPMLField7", "");
            dataLoan.sCustomPMLField8_rep = GetString("sCustomPMLField8", "");
            dataLoan.sCustomPMLField9_rep = GetString("sCustomPMLField9", "");
            dataLoan.sCustomPMLField10_rep = GetString("sCustomPMLField10", "");
            dataLoan.sCustomPMLField11_rep = GetString("sCustomPMLField11", "");
            dataLoan.sCustomPMLField12_rep = GetString("sCustomPMLField12", "");
            dataLoan.sCustomPMLField13_rep = GetString("sCustomPMLField13", "");
            dataLoan.sCustomPMLField14_rep = GetString("sCustomPMLField14", "");
            dataLoan.sCustomPMLField15_rep = GetString("sCustomPMLField15", "");
            dataLoan.sCustomPMLField16_rep = GetString("sCustomPMLField16", "");
            dataLoan.sCustomPMLField17_rep = GetString("sCustomPMLField17", "");
            dataLoan.sCustomPMLField18_rep = GetString("sCustomPMLField18", "");
            dataLoan.sCustomPMLField19_rep = GetString("sCustomPMLField19", "");
            dataLoan.sCustomPMLField20_rep = GetString("sCustomPMLField20", "");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            ProductCodeFilterPopup.LoadData(this, dataLoan, PrincipalFactory.CurrentPrincipal);
        }
    }
	public partial class PMLDefaultValuesService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new PMLDefaultValuesServiceItem());
        }
	}
}
