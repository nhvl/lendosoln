<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="FannieMaeExportLogin.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FannieMaeExportLogin" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>FannieMaeExportLogin</title>
  </HEAD>
<body class=RightBackground style="MARGIN-LEFT: 0px" scroll=no MS_POSITIONING="FlowLayout">
<script language=javascript>
<!--

var g_sDoDuHeader = <%= AspxTools.JsString(m_doDuHeader) %>;  
function _init() {
  <% if (m_hasAutoLoginInformation) { %>
    f_submit();
  <% } else { %>
    resize(480,230);
    if (<%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value != '')
      <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.focus();
    else
      <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.focus();
  <% } %>  
  document.getElementById('HeaderLabel').innerText = "Submit directly to " + g_sDoDuHeader;
  document.getElementById('CaseIdLabel').innerText = g_sDoDuHeader + " Case ID";
  document.getElementById('LoginLabel').innerText = g_sDoDuHeader + " User ID";
  document.getElementById('PasswordLabel').innerText = g_sDoDuHeader + " Password";  
  document.getElementById('FannieInstitutionIdLabel').innerText = "Lender Institution ID";
  
}
function f_submit() {
  f_displayWait(true);
  window.setTimeout(f_submitImpl, 200);
  
}
function f_submitImpl() {
  var args = new Object();
  args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
  
  <% if (m_hasAutoLoginInformation) { %>
    args["AutoLogin"] = "True";
  <% } else { %>
    args["FannieMaeMORNETUserID"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value;
    args["FannieMaeMORNETPassword"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.value;
    if (<%= AspxTools.JsGetElementById(m_rememberLoginCB) %>) {
        args["RememberLastLogin"] = <%= AspxTools.JsGetElementById(m_rememberLoginCB) %>.checked ? 'True' : 'False';
    }
    args["sDuCaseId"] = <%= AspxTools.JsGetElementById(sDuCaseId) %>.value;  
    args["sDuLenderInstitutionId"] = <%= AspxTools.JsGetElementById(sDuLenderInstitutionId) %>.value;
  <% } %>
  args["IsDo"] = <%= AspxTools.JsBool(m_isDo) %>;
  
 
  var result = gService.loanedit.call("ExportToDuDo", args);
  if (!result.error) {
    switch (result.value["Status"]) {
      case "ERROR":
        f_displayError(result.value["ErrorMessage"]);
        break;
      case "DONE":
        f_displayComplete(result.value["id"], result.value["sDuCaseId"]);
        break;
    }
  } else {
    var errMsg = <%=AspxTools.JsString(ErrorMessages.Generic) %>;
    
    if (null != result.UserMessage)
      errMsg = result.UserMessage;
      
    f_displayError(errMsg);
  }
  
}
function f_displayComplete(id, sDuCaseId) {

  var ret = window.dialogArguments || {};
  ret.OK = true;
  ret.CacheId = id;
  ret.bIsDo = <%= AspxTools.JsBool(m_isDo) %>;
  ret.sUserName = <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value;
  ret.sPassword = <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.value;
  ret.sDuCaseId = sDuCaseId;
  ret.bHasAutoLogin = <%= AspxTools.JsBool( m_hasAutoLoginInformation) %>;
  onClosePopup(ret);
}
function f_close() {
  var ret = window.dialogArguments || {};
  ret.OK = false;
  onClosePopup(ret);
}
function f_displayWait(bDisplay) {
  document.getElementById('MainPanel').style.display = 'none';
  document.getElementById('WaitPanel').style.display = '';
  document.getElementById('ErrorPanel').style.display = 'none';
}
function f_displayError(errMsg) {
  document.getElementById('WaitPanel').style.display = 'none';
  document.getElementById('ErrorPanel').style.display = '';
  <%= AspxTools.JsGetElementById(ErrorMessage) %>.value = errMsg;

  <% if (m_hasAutoLoginInformation) { %>
    document.getElementById('ErrorPanelOkButton').style.display = '';
    resize(600, 350);
  <% } else { %>
    document.getElementById('MainPanel').style.display = '';
    resize(600, 450);
  <% } %>
}
//-->
</script>
<h4 class="page-header" id="HeaderLabel"></h4>
<form id=FannieMaeExportLogin method=post runat="server">
<table id="MainTable" cellspacing="0" cellpadding="0" width="100%" border="0">
  <tbody id="WaitPanel" style="display: none">
    <tr>
      <td style="font-weight: bold; font-size: 16px" valign="middle" align="center" colspan="2" height="50">Please Wait ...<br><img height="10" src="../images/status.gif"></td>
    </tr>
  </tbody>
  <tbody id="MainPanel">
    <tr style="padding-top:8px">
      <td class="FieldLabel" style="padding-left: 5px">
        <div id="LoginLabel">
        </div>
      </td>
      <td><asp:TextBox ID="FannieMaeMORNETUserID" runat="server" Width="155px" /></td>
    </tr>
    <tr>
      <td class="FieldLabel" style="padding-left: 5px">
        <div id="PasswordLabel">
        </div>
      </td>
      <td><asp:TextBox ID="FannieMaeMORNETPassword" runat="server" Width="155px" TextMode="Password" /></td>
    </tr>
    <tr>
      <td class="FieldLabel" style="padding-left: 5px">
        <div id="FannieInstitutionIdLabel">
        </div>
      </td>
      <td>
        <asp:TextBox ID="sDuLenderInstitutionId" runat="server" Width="155px" />
        (Leave blank to use default)</td>
    </tr>
    <tr>
      <td class="FieldLabel" style="padding-left: 5px">
        <div id="CaseIdLabel">
        </div>
      </td>
      <td>
        <asp:TextBox ID="sDuCaseId" runat="server" Width="155px" />
        </td>
    </tr>
    <tr>
      <td class="FieldLabel" style="padding-left: 5px"></td>
      <td class="FieldLabel" style="padding-top: 10px"><asp:CheckBox ID="m_rememberLoginCB" runat="server" Text="Remember login name" /></td>
    </tr>
    <tr>
      <td align="center" colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td><input class="buttonstyle" onclick="f_submit();" type="button" value="Submit">&nbsp;<input class="buttonstyle" onclick="f_close();" type="button" value="Cancel"> </td>
    </tr>
    <tr style="padding-top:6px">
      <td></td>
      <td><a href="#" onclick="window.open('https://profile-manager.efanniemae.com/integration/service/password/UserSelfService', 'FannieMae', 'width=740,height=420,menubar=no,status=yes,resizable=yes');">Forgot your User ID or Password?</a></td>
    </tr>
  </tbody>
  <tbody id="ErrorPanel" style="display: none">
    <tr>
      <td class="FieldLabel" style="padding-left: 5px; padding-top: 10px" nowrap colspan="2">
      <% if (m_hasAutoLoginInformation) { %>
        An error has occured. Please provide these details to your account administrator:
      <% } else { %>
      Error Message
      <% } %>
      </td>
    </tr>
    <tr>
      <td style="padding-left: 5px" nowrap colspan="2"><asp:TextBox ID="ErrorMessage" runat="server" Width="550px" TextMode="MultiLine" ReadOnly="True" Height="201px" ForeColor="Red" /></td>
    </tr>
  </tbody>
    <tbody id="ErrorPanelOkButton" style="DISPLAY:none">
  <tr><td colspan=2 align=center><input class=buttonstyle onclick="f_close();" type=button value=OK width="30px"></td></tr>
  <tr><td>&nbsp;</td></tr>
  </tbody>
</table>
      </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</HTML>
