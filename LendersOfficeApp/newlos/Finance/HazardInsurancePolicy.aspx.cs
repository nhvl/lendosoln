﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class HazardInsurancePolicy : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(HazardInsurancePolicy));
            dataLoan.InitLoad();

            sHazInsCompanyNm.Text = dataLoan.sHazInsCompanyNm;
            sHazInsCompanyId.Text = dataLoan.sHazInsCompanyId;
            sHazInsPolicyNum.Text = dataLoan.sHazInsPolicyNum;
            sHazInsPolicyPayeeCode.Text = dataLoan.sHazInsPolicyPayeeCode;

            Tools.SetDropDownListValue(sHazInsPaidByT, dataLoan.sHazInsPaidByT);
            Tools.SetDropDownListValue(sHazInsPolicyPaymentTypeT, dataLoan.sHazInsPolicyPaymentTypeT);

            // Money
            sHazInsCoverageAmt.Text = dataLoan.sHazInsCoverageAmt_rep;
            sHazInsDeductibleAmt.Text = dataLoan.sHazInsDeductibleAmt_rep;
            sHazInsPaymentDueAmtLckd.Checked = dataLoan.sHazInsPaymentDueAmtLckd;
            sHazInsPaymentDueAmt.Text = dataLoan.sHazInsPaymentDueAmt_rep;

            // Dates
            sHazInsPolicyActivationD.Text = dataLoan.sHazInsPolicyActivationD_rep;
            sHazInsPaymentDueD.Text = dataLoan.sHazInsPaymentDueD_rep;
            sHazInsPaymentMadeD.Text = dataLoan.sHazInsPaymentMadeD_rep;
            sHazInsPaidThroughD.Text = dataLoan.sHazInsPaidThroughD_rep;
            sHazInsPolicyExpirationD.Text = dataLoan.sHazInsPolicyExpirationD_rep;
            sHazInsPolicyLossPayeeTransD.Text = dataLoan.sHazInsPolicyLossPayeeTransD_rep;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "HazardInsurancePolicy";
            this.PageID = "HazardInsurancePolicy";

            Tools.Bind_sInsPaidByWithHOA(sHazInsPaidByT);
            Tools.Bind_sInsPolicyPaymentTypeT(sHazInsPolicyPaymentTypeT);
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
