﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Disbursement.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.Disbursement" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Disbursement</title>
    <style type="text/css">
        .floatLeft { float: left; }
        .floatRight { float: right; }
    </style>
    <script type="text/javascript">
        function _init() {
            // Enable/Disable the lockable inputs
            f_onclick_sDisbursementDaysOfInterestLckd(false);
            f_onclick_sDisbursementWarehouseLineIntLckd(false);
        }

        function f_onclick_sDisbursementDaysOfInterestLckd(bRefreshCalc) {
            var bReadOnlyDaysOfInterest = !document.getElementById('sDisbursementDaysOfInterestLckd').checked;
            document.getElementById('sDisbursementDaysOfInterest').readOnly = bReadOnlyDaysOfInterest;

            if (bRefreshCalc) {
                refreshCalculation();
            }
        }

        function f_onclick_sDisbursementWarehouseLineIntLckd(bRefreshCalc) {
            var bReadOnlyWarehouseLineInt = !document.getElementById('sDisbursementWarehouseLineIntLckd').checked;
            document.getElementById('sDisbursementWarehouseLineInt').readOnly = bReadOnlyWarehouseLineInt;

            if (bRefreshCalc) {
                refreshCalculation();
            }
        }

        function doAfterDateFormat(e)
        {
            if (e.id === "sDisbursementD")
            {
                refreshCalculation();
            }
        }
        
    </script>
</head>
<body bgcolor="gainsboro">
    <form id="Disbursement" runat="server">
    <table class="FormTable" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="MainRightHeader" colspan="2">
                Disbursement
            </td>
        </tr>
        <tr>
            <td style="padding:4px">
                <table class="FieldLabel">
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="Label1" Text="Investor payment amount" AssociatedControlID="sPurchaseAdviceSummaryTotalDueSeller" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
		                    <ml:MoneyTextBox ID="sPurchaseAdviceSummaryTotalDueSeller" ReadOnly="true" runat="server" width="80px" />
                        </td>
                        <td>
                            <ml:EncodedLabel ID="sFundDLabel" Text="Funded date" AssociatedControlID="sFundD" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sFundD" runat="server" preset="date" ReadOnly="true" Width="60px"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="Label2" Text="Amount funded from warehouse line" AssociatedControlID="sDisbursementAmtFundFromWarehouseLine" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
		                    <ml:MoneyTextBox ID="sDisbursementAmtFundFromWarehouseLine" ReadOnly="true" runat="server" width="80px" />
                        </td>
                        <td>
                            <ml:EncodedLabel ID="Label7" Text="Investor funds disbursement date" AssociatedControlID="sDisbursementD" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
			                <ml:datetextbox ID="sDisbursementD" runat="server" preset="date" CssClass="mask" Width="60px" onchange="date_onblur(null, this);"></ml:datetextbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="Label3" Text="Warehouse line fees" AssociatedControlID="sDisbursementWarehouseLineFees" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
		                    <ml:MoneyTextBox ID="sDisbursementWarehouseLineFees" onchange="refreshCalculation();" runat="server" width="80px" />
                        </td>
                        <td>
                            <ml:EncodedLabel ID="Label8" Text="Notes" AssociatedControlID="sDisbursementNotes" runat="server"></ml:EncodedLabel>
                        </td>
                        <td rowspan="5" valign="top">
                            <asp:TextBox ID="sDisbursementNotes" TextMode="MultiLine" Rows="7" onchange="refreshCalculation();" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="sDisbursementWarehousePerDiemInterestLabel" Text="Warehouse per diem interest" AssociatedControlID="sDisbursementWarehousePerDiemInterest" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="sDisbursementWarehousePerDiemInterest" onchange="refreshCalculation();" decimalDigits="4" runat="server" Width="80px"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="sDisbursementDaysOfInterestLabel" Text="Days of interest" class="floatLeft" runat="server"></ml:EncodedLabel>
                            <asp:CheckBox ID="sDisbursementDaysOfInterestLckd" onclick="f_onclick_sDisbursementDaysOfInterestLckd(true);" class="floatRight" runat="server" />
                        </td>
                        <td>
                            <asp:TextBox ID="sDisbursementDaysOfInterest" onchange="refreshCalculation();" ReadOnly="true" runat="server" Width="40px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="Label4" Text="Warehouse line interest" AssociatedControlID="sDisbursementWarehouseLineInt" class="floatLeft" runat="server"></ml:EncodedLabel>
                            <asp:CheckBox ID="sDisbursementWarehouseLineIntLckd" onclick="f_onclick_sDisbursementWarehouseLineIntLckd(true);" class="floatRight" runat="server" />
                        </td>
                        <td>
		                    <ml:MoneyTextBox ID="sDisbursementWarehouseLineInt" onchange="refreshCalculation();" ReadOnly="true" runat="server" width="80px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="Label5" Text="Other" AssociatedControlID="sDisbursementOther" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
		                    <ml:MoneyTextBox ID="sDisbursementOther" onchange="refreshCalculation();" runat="server" width="80px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ml:EncodedLabel ID="Label6" Text="Net received" AssociatedControlID="sDisbursementNetReceived" runat="server"></ml:EncodedLabel>
                        </td>
                        <td>
		                    <ml:MoneyTextBox ID="sDisbursementNetReceived" ReadOnly="true" runat="server" width="80px" />
                        </td>
                    </tr> 
                </table>
            </td>
        </tr>
    </table>
    <uc1:cmodaldlg id="CModalDlg1" runat="server" />
    </form>
</body>
</html>
