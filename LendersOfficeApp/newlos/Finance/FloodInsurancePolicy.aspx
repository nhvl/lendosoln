﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FloodInsurancePolicy.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.FloodInsurancePolicy" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<script language="javascript">
  function _init() {
    if(<%= AspxTools.JsGetElementById(sFloodInsPaymentDueAmtLckd) %>.checked) {
      <%= AspxTools.JsGetElementById(sFloodInsPaymentDueAmt) %>.readOnly = false;
	} else {
	  <%= AspxTools.JsGetElementById(sFloodInsPaymentDueAmt) %>.readOnly = true;
	}
  }
  
  function clearCompanyId()
  {
    document.getElementById("sFloodInsCompanyId").value = "";
  }
</script>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Flood Insurance Policy</title>  
</head>

<body bgcolor="gainsboro">
    <form id="FloodPolicy" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="MainRightHeader" noWrap>
                    Flood Insurance Policy
                </td>
            </tr>            
            <tr>
                <td>
                    <table class="FieldLabel">
                        <tr>
                            <td></td>
                            <td>
                                <uc:CFM id="CFM" runat="server" CompanyNameField="sFloodInsCompanyNm" CompanyIdField="sFloodInsCompanyId" DisableAddTo="true"></uc:CFM>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Insurance Company
                            </td>
                            <td>
                                <asp:TextBox onchange="clearCompanyId()" ID="sFloodInsCompanyNm" width="150" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Policy Number
                            </td>
                            <td>
                                <asp:TextBox ID="sFloodInsPolicyNum" Width="150" runat="server" ></asp:TextBox>
                            </td>                            
                        </tr>
                        <tr>
                            <td>
                                Policy Activation Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sFloodInsPolicyActivationD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Policy Expiration Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sFloodInsPolicyExpirationD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Paid By
                            </td>
                            <td>
                                <asp:DropDownList ID="sFloodInsPaidByT" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Coverage Amount
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sFloodInsCoverageAmt" runat="server" CssClass="mask" preset="money" width="110"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Deductible Amount
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sFloodInsDeductibleAmt" runat="server" CssClass="mask" preset="money" Width="110"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Due
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sFloodInsPaymentDueAmt" runat="server" CssClass="mask" preset="money" ReadOnly="true" Width="110"></ml:MoneyTextBox>
                                <asp:CheckBox ID="sFloodInsPaymentDueAmtLckd" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Type
                            </td>
                            <td>
                                <asp:DropDownList ID="sFloodInsPolicyPaymentTypeT" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Due Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sFloodInsPaymentDueD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Made Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sFloodInsPaymentMadeD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Paid Through Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sFloodInsPaidThroughD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payee Code
                            </td>
                            <td>
                                <asp:TextBox ID="sFloodInsPolicyPayeeCode" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Loss Payee Transfer Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sFloodInsPolicyLossPayeeTransD" runat="server" Width="75" />
                            </td>
                        </tr>
                        <tr>
                            <td>Company Id</td>
                            <td>
                                <asp:TextBox runat="server" ID="sFloodInsCompanyId" Text=""></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form> 
</body>
</html>