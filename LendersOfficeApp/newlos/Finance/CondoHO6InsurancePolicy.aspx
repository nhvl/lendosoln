﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CondoHO6InsurancePolicy.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.CondoHO6InsurancePolicy" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<script language="javascript">
  function _init() {
	lockField(<%= AspxTools.JsGetElementById(sCondoHO6InsPaymentDueAmtLckd) %>, 'sCondoHO6InsPaymentDueAmt');
  }
  
    function clearCompanyId()
  {
    document.getElementById("sCondoHO6InsCompanyId").value = "";
  }
</script>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Condo Insurance Policy</title>  
</head>

<body bgcolor="gainsboro">
    <form id="CondoHO6Policy" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="MainRightHeader" noWrap>
                    Condo HO-6 Insurance Policy
                </td>
            </tr>            
            <tr>
                <td>
                    <table class="FieldLabel">
                        <tr>
                            <td></td>
                            <td>
                                <uc:CFM id="CFM" runat="server" CompanyNameField="sCondoHO6InsCompanyNm" CompanyIdField="sCondoHO6InsCompanyId" DisableAddTo="true"></uc:CFM>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Insurance Company
                            </td>
                            <td>
                                <asp:TextBox onchange="clearCompanyId()" ID="sCondoHO6InsCompanyNm" Width="150" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Policy Number
                            </td>
                            <td>
                                <asp:TextBox ID="sCondoHO6InsPolicyNum" Width="150" runat="server" ></asp:TextBox>
                            </td>                            
                        </tr>
                        <tr>
                            <td>
                                Policy Activation Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sCondoHO6InsPolicyActivationD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Policy Expiration Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sCondoHO6InsPolicyExpirationD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Paid By
                            </td>
                            <td>
                                <asp:DropDownList ID="sCondoHO6InsPaidByT" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Coverage Amount
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sCondoHO6InsCoverageAmt" runat="server" CssClass="mask" preset="money" width="110"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Deductible Amount
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sCondoHO6InsDeductibleAmt" runat="server" CssClass="mask" preset="money" Width="110"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Due
                            </td>
                            <td>
                                <asp:DropDownList ID="sCondoHO6InsSettlementChargeT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                                <ml:MoneyTextBox ID="sCondoHO6InsPaymentDueAmt" runat="server" CssClass="mask" preset="money" ReadOnly="true" Width="110"></ml:MoneyTextBox>
                                <asp:CheckBox ID="sCondoHO6InsPaymentDueAmtLckd" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Type
                            </td>
                            <td>
                                <asp:DropDownList ID="sCondoHO6InsPolicyPaymentTypeT" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Due Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sCondoHO6InsPaymentDueD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Made Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sCondoHO6InsPaymentMadeD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Paid Through Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sCondoHO6InsPaidThroughD" runat="server" width="75"/>
                            </td>
                        </tr>  
                        <tr>
                            <td>
                                Payee Code
                            </td>
                            <td>
                                <asp:TextBox ID="sCondoHO6InsPolicyPayeeCode" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Loss Payee Transfer Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sCondoHO6InsPolicyLossPayeeTransD" runat="server" Width="75" />
                            </td>
                        </tr>      
                        <tr>
                        <td>
                            Company Id
                        </td>
                            <td>
                                <asp:TextBox runat="server" ID="sCondoHO6InsCompanyId" Text=""></asp:TextBox>
                            </td>
                        </tr>                
                    </table>
                </td>
            </tr>
        </table>
    </form> 
</body>
</html>

