﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;
using MeridianLink.CommonControls;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class SecondaryStatus : BaseLoanPage
    {
        private ComboBox[] m_comboBoxes = null;

        private bool UserHasReadPermission
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowCloserRead) && BrokerUser.HasPermission(Permission.AllowAccountantRead) && BrokerUser.HasPermission(Permission.AllowLockDeskRead);
            }
        }

        private bool UserHasWritePermission
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowCloserWrite) && BrokerUser.HasPermission(Permission.AllowAccountantWrite);
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                if (!base.IsReadOnly)
                {
                    if (UserHasWritePermission)
                        return false;
                    else if (UserHasReadPermission)
                        return true;
                }

                return base.IsReadOnly;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!UserHasReadPermission)
                throw new AccessDenied();

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SecondaryStatus));
            dataLoan.InitLoad();

            sInvRLckdD.Text = dataLoan.sInvestorLockRLckdD_rep;
            sInvRLckdExpiredD.Text = dataLoan.sInvestorLockRLckExpiredD_rep;
            sInvDelvExpD.Text = dataLoan.sInvestorLockDeliveryExpiredD_rep;

            bool hasPricingEngine = BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine);
            bool hasLockDeskWritePermission = BrokerUser.HasPermission(Permission.AllowLockDeskWrite);

            sInvRLckdD.ReadOnly = dataLoan.sIsInvestorRateLocked || IsReadOnly || !hasLockDeskWritePermission || hasPricingEngine || Broker.HasLenderDefaultFeatures;
            sInvRLckdExpiredD.ReadOnly = dataLoan.sIsInvestorRateLocked || IsReadOnly || hasPricingEngine || Broker.HasLenderDefaultFeatures || !hasLockDeskWritePermission;
            sInvDelvExpD.ReadOnly = dataLoan.sIsInvestorRateLocked || IsReadOnly || hasPricingEngine || Broker.HasLenderDefaultFeatures || !hasLockDeskWritePermission;
            
            sInvRLckdD.IsDisplayCalendarHelper = !sInvRLckdD.ReadOnly;
            sInvRLckdExpiredD.IsDisplayCalendarHelper = !sInvRLckdExpiredD.ReadOnly;
            sInvDelvExpD.IsDisplayCalendarHelper = !sInvDelvExpD.ReadOnly;

            sInvDelvExpD.IsDisplayCalendarHelper = !sInvDelvExpD.ReadOnly;

            sSecondStatusLckd.Checked = dataLoan.sSecondStatusLckd;
            sSecondStatus.Text = dataLoan.sSecondStatusT_rep;
            sSecondStatusT.Value = dataLoan.sSecondStatusT.ToString("D");

            sFundD.Text = dataLoan.sFundD_rep;
            sLPurchaseD.Text = dataLoan.sLPurchaseD_rep;
            sDisbursementD.Text = dataLoan.sDisbursementD_rep;
            sReconciledD.Text = dataLoan.sReconciledD_rep;
            sShippedToWarehouseD.Text = dataLoan.sShippedToWarehouseD_rep;
            sShippedToInvestorD.Text = dataLoan.sShippedToInvestorD_rep;
            sSuspendedByInvestorD.Text = dataLoan.sSuspendedByInvestorD_rep;
            sCondSentToInvestorD.Text = dataLoan.sCondSentToInvestorD_rep;
            sAdditionalCondSentD.Text = dataLoan.sAdditionalCondSentD_rep;
            sGoodByLetterD.Text = dataLoan.sGoodByLetterD_rep;

            sMersRegistrationD.Text = dataLoan.sMersRegistrationD_rep;
            sMersTobD.Text = dataLoan.sMersTobD_rep;
            sMersTosD.Text = dataLoan.sMersTosD_rep;
            sMersTosDLckd.Checked = dataLoan.sMersTosDLckd;

            sServicingStartD.Text = dataLoan.sServicingStartD_rep;
            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sInvSchedDueD1.Text = dataLoan.sInvSchedDueD1_rep;

            // OPM 80416
            sLoanPackageOrderedD.Text = dataLoan.sLoanPackageOrderedD_rep;
            sLoanPackageDocumentD.Text = dataLoan.sLoanPackageDocumentD_rep;
            sLoanPackageReceivedD.Text = dataLoan.sLoanPackageReceivedD_rep;
            sLoanPackageN.Text = dataLoan.sLoanPackageN;
            sLoanPackageShippedD.Text = dataLoan.sLoanPackageShippedD_rep;
            sLoanPackageMethod.Text = dataLoan.sLoanPackageMethod;
            sLoanPackageTracking.Text = dataLoan.sLoanPackageTracking;

            sMortgageDOTOrderedD.Text = dataLoan.sMortgageDOTOrderedD_rep;
            sMortgageDOTDocumentD.Text = dataLoan.sMortgageDOTDocumentD_rep;
            sMortgageDOTReceivedD.Text = dataLoan.sMortgageDOTReceivedD_rep;
            sMortgageDOTN.Text = dataLoan.sMortgageDOTN;
            sMortgageDOTShippedD.Text = dataLoan.sMortgageDOTShippedD_rep;
            sMortgageDOTMethod.Text = dataLoan.sMortgageDOTMethod;
            sMortgageDOTTracking.Text = dataLoan.sMortgageDOTTracking;

            sNoteOrderedD.Text = dataLoan.sNoteOrderedD_rep;
            sDocumentNoteD.Text = dataLoan.sDocumentNoteD_rep;
            sNoteReceivedD.Text = dataLoan.sNoteReceivedD_rep;
            sNoteN.Text = dataLoan.sNoteN;
            sNoteShippedD.Text = dataLoan.sNoteShippedD_rep;
            sNoteMethod.Text = dataLoan.sNoteMethod;
            sNoteTracking.Text = dataLoan.sNoteTracking;

            sFinalHUD1SttlmtStmtOrderedD.Text = dataLoan.sFinalHUD1SttlmtStmtOrderedD_rep;
            sFinalHUD1SttlmtStmtDocumentD.Text = dataLoan.sFinalHUD1SttlmtStmtDocumentD_rep;
            sFinalHUD1SttlmtStmtReceivedD.Text = dataLoan.sFinalHUD1SttlmtStmtReceivedD_rep;
            sFinalHUD1SttlmtStmtN.Text = dataLoan.sFinalHUD1SttlmtStmtN;
            sFinalHUD1SttlmtStmtShippedD.Text = dataLoan.sFinalHUD1SttlmtStmtShippedD_rep;
            sFinalHUD1SttlmtStmtMethod.Text = dataLoan.sFinalHUD1SttlmtStmtMethod;
            sFinalHUD1SttlmtStmtTracking.Text = dataLoan.sFinalHUD1SttlmtStmtTracking;

            sTitleInsPolicyOrderedD.Text = dataLoan.sTitleInsPolicyOrderedD_rep;
            sTitleInsPolicyDocumentD.Text = dataLoan.sTitleInsPolicyDocumentD_rep;
            sTitleInsPolicyReceivedD.Text = dataLoan.sTitleInsPolicyReceivedD_rep;
            sTitleInsPolicyN.Text = dataLoan.sTitleInsPolicyN;
            sTitleInsPolicyShippedD.Text = dataLoan.sTitleInsPolicyShippedD_rep;
            sTitleInsPolicyMethod.Text = dataLoan.sTitleInsPolicyMethod;
            sTitleInsPolicyTracking.Text = dataLoan.sTitleInsPolicyTracking;

            sMiCertOrderedD.Text = dataLoan.sMiCertOrderedD_rep;
            sMiCertIssuedD.Text = dataLoan.sMiCertIssuedD_rep;
            sMiCertReceivedD.Text = dataLoan.sMiCertReceivedD_rep;
            sMiCertN.Text = dataLoan.sMiCertN;
            sMiCertShippedD.Text = dataLoan.sMiCertShippedD_rep;
            sMiCertMethod.Text = dataLoan.sMiCertMethod;
            sMiCertTracking.Text = dataLoan.sMiCertTracking;

            sFinalHazInsPolicyOrderedD.Text = dataLoan.sFinalHazInsPolicyOrderedD_rep;
            sFinalHazInsPolicyDocumentD.Text = dataLoan.sFinalHazInsPolicyDocumentD_rep;
            sFinalHazInsPolicyReceivedD.Text = dataLoan.sFinalHazInsPolicyReceivedD_rep;
            sFinalHazInsPolicyN.Text = dataLoan.sFinalHazInsPolicyN;
            sFinalHazInsPolicyShippedD.Text = dataLoan.sFinalHazInsPolicyShippedD_rep;
            sFinalHazInsPolicyMethod.Text = dataLoan.sFinalHazInsPolicyMethod;
            sFinalHazInsPolicyTracking.Text = dataLoan.sFinalHazInsPolicyTracking;

            sFinalFloodInsPolicyOrderedD.Text = dataLoan.sFinalFloodInsPolicyOrderedD_rep;
            sFinalFloodInsPolicyDocumentD.Text = dataLoan.sFinalFloodInsPolicyDocumentD_rep;
            sFinalFloodInsPolicyReceivedD.Text = dataLoan.sFinalFloodInsPolicyReceivedD_rep;
            sFinalFloodInsPolicyN.Text = dataLoan.sFinalFloodInsPolicyN;
            sFinalFloodInsPolicyShippedD.Text = dataLoan.sFinalFloodInsPolicyShippedD_rep;
            sFinalFloodInsPolicyMethod.Text = dataLoan.sFinalFloodInsPolicyMethod;
            sFinalFloodInsPolicyTracking.Text = dataLoan.sFinalFloodInsPolicyTracking;

            sCollateralPkgOrderedD.Text = dataLoan.sCollateralPkgOrderedD_rep;
            sCollateralPkgShippedD.Text = dataLoan.sCollateralPkgShippedD_rep;
            sCollateralPkgMethod.Text = dataLoan.sCollateralPkgMethod;
            sCollateralPkgTracking.Text = dataLoan.sCollateralPkgTracking;

            sLoanDocsArchiveD.Text = dataLoan.sLoanDocsArchiveD_rep;
            sTrailingDocsArchiveD.Text = dataLoan.sTrailingDocsArchiveD_rep;

            sCustomTrailingDoc1Desc.Text = dataLoan.sCustomTrailingDoc1Desc;
            sCustomTrailingDoc1OrderedD.Text = dataLoan.sCustomTrailingDoc1OrderedD_rep;
            sCustomTrailingDoc1DocumentD.Text = dataLoan.sCustomTrailingDoc1DocumentD_rep;
            sCustomTrailingDoc1ReceivedD.Text = dataLoan.sCustomTrailingDoc1ReceivedD_rep;
            sCustomTrailingDoc1ShippedD.Text = dataLoan.sCustomTrailingDoc1ShippedD_rep;
            sCustomTrailingDoc1Method.Text = dataLoan.sCustomTrailingDoc1Method;
            sCustomTrailingDoc1Tracking.Text = dataLoan.sCustomTrailingDoc1Tracking;
            sCustomTrailingDoc1N.Text = dataLoan.sCustomTrailingDoc1N;

            sCustomTrailingDoc2Desc.Text = dataLoan.sCustomTrailingDoc2Desc;
            sCustomTrailingDoc2OrderedD.Text = dataLoan.sCustomTrailingDoc2OrderedD_rep;
            sCustomTrailingDoc2DocumentD.Text = dataLoan.sCustomTrailingDoc2DocumentD_rep;
            sCustomTrailingDoc2ReceivedD.Text = dataLoan.sCustomTrailingDoc2ReceivedD_rep;
            sCustomTrailingDoc2ShippedD.Text = dataLoan.sCustomTrailingDoc2ShippedD_rep;
            sCustomTrailingDoc2Method.Text = dataLoan.sCustomTrailingDoc2Method;
            sCustomTrailingDoc2Tracking.Text = dataLoan.sCustomTrailingDoc2Tracking;
            sCustomTrailingDoc2N.Text = dataLoan.sCustomTrailingDoc2N;

            sCustomTrailingDoc3Desc.Text = dataLoan.sCustomTrailingDoc3Desc;
            sCustomTrailingDoc3OrderedD.Text = dataLoan.sCustomTrailingDoc3OrderedD_rep;
            sCustomTrailingDoc3DocumentD.Text = dataLoan.sCustomTrailingDoc3DocumentD_rep;
            sCustomTrailingDoc3ReceivedD.Text = dataLoan.sCustomTrailingDoc3ReceivedD_rep;
            sCustomTrailingDoc3ShippedD.Text = dataLoan.sCustomTrailingDoc3ShippedD_rep;
            sCustomTrailingDoc3Method.Text = dataLoan.sCustomTrailingDoc3Method;
            sCustomTrailingDoc3Tracking.Text = dataLoan.sCustomTrailingDoc3Tracking;
            sCustomTrailingDoc3N.Text = dataLoan.sCustomTrailingDoc3N;

            sCustomTrailingDoc4Desc.Text = dataLoan.sCustomTrailingDoc4Desc;
            sCustomTrailingDoc4OrderedD.Text = dataLoan.sCustomTrailingDoc4OrderedD_rep;
            sCustomTrailingDoc4DocumentD.Text = dataLoan.sCustomTrailingDoc4DocumentD_rep;
            sCustomTrailingDoc4ReceivedD.Text = dataLoan.sCustomTrailingDoc4ReceivedD_rep;
            sCustomTrailingDoc4ShippedD.Text = dataLoan.sCustomTrailingDoc4ShippedD_rep;
            sCustomTrailingDoc4Method.Text = dataLoan.sCustomTrailingDoc4Method;
            sCustomTrailingDoc4Tracking.Text = dataLoan.sCustomTrailingDoc4Tracking;
            sCustomTrailingDoc4N.Text = dataLoan.sCustomTrailingDoc4N;
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
            this.PageTitle = "Secondary Status";
            this.PageID = "SecondaryStatus";

            m_comboBoxes = new ComboBox[] { sLoanPackageMethod, sMortgageDOTMethod, sNoteMethod, sFinalHUD1SttlmtStmtMethod,
                sTitleInsPolicyMethod, sMiCertMethod, sFinalHazInsPolicyMethod, sFinalFloodInsPolicyMethod, sCollateralPkgMethod };
            for (int i = 0; i < m_comboBoxes.Length; i++)
            {
                Tools.Bind_ShippingMethod(m_comboBoxes[i]);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
