﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="PoolAssignmentPopUp.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Finance.PoolAssignmentPopUp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
<style type="text/css">
    .button
    {
        padding: 0 1em 0 1em;
        width: auto;
        overflow: visible;
    }
</style>
<head runat="server">
    <title></title>
</head>

<script type="text/javascript">
    function _init() {
        resize(440, 160);
    }
    jQuery(function($) {
        var PoolId = null;
        $('#Lookup').click(function() {
            var $button = $(this);
            $button.prop('disabled', true);
            var oldVal = $button.val();
            $button.val('Please wait...');

            var DTO = {
                PoolName: $('#sPoolNumber').val(),
                Agency: $('#sPoolAgencyT').val()
            };

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'PoolAssignmentPopUp.aspx/FindPool',
                data: JSON.stringify(DTO),
                dataType: 'json',
                async: false,

                success: function(msg) {
                    if (!msg.d.FoundMatch) {
                        $('#Message').text("Pool number not found.").removeClass('Success').addClass('Error');
                        $('#AssignLoanToPool').prop('disabled', true);
                        PoolId = null;
                        return;
                    }

                    if (msg.d.FoundMatch && msg.d.MultipleResults) {
                        $('#Message').text("More than one matching pool found. " +
                                           "Please assign loan from the loan assignment page in the pool editor.")
                                           .removeClass('Success')
                                           .addClass('Error'); ;
                        $('#AssignLoanToPool').prop('disabled', true);
                        PoolId = null;
                        return;
                    }

                    $('#Message').text("Found!").addClass('Success').removeClass('Error');
                    $('#AssignLoanToPool').prop('disabled', false);
                    PoolId = msg.d.PoolId;
                },

                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("An unexpected error happened, please try again later");
                },

                complete: function() {
                    $button.prop('disabled', false);
                    $button.val(oldVal);
                    $button.css('background-color', '');
                }
            });
        });

        $('#sPoolNumber').keypress(function(e) {
            var keyCode = e.keyCode ? e.keyCode : e.charCode;
            if (keyCode === 13) { // enter
                $('#Lookup').click();
                return false;
            }
        });

        $('#AssignLoanToPool').click(function() {
            if (!PoolId) {
                alert("Please select a mortgage pool before assigning this loan.");
                return;
            }

            var $button = $(this);
            $button.prop('disabled', true);
            var oldVal = $button.val();
            $button.val('Please wait...');

            var DTO = {
                LoanId: ML.sLId,
                PoolId: PoolId
            };
            
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'PoolAssignmentPopUp.aspx/AssignLoan',
                data: JSON.stringify(DTO),
                dataType: 'json',
                async: false,

                success: function(msg) {
                    onClosePopup();
                },

                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    
                    alert("An unexpected error happened, please try again later");
                },

                complete: function() {
                    $button.prop('disabled', false);
                    $button.val(oldVal);
                    $button.css('background-color', '');
                }
            });
        });
    });
</script>

<style type="text/css">
    form
    {
        padding-left: 20px;
        padding-top: 15px;
    }
    
    label
    {
        font-weight: bold;
        width: 85px;
        display: inline-block;
    }
    
    div
    {
        margin-bottom: 7px;
    }
    
    select
    {
        width: 127px;
    }
    
    #AssignLoanToPool
    {
        margin-left: 30%;
        margin-top: 30px;
    }
    
    .Error
    {
        color: Red;
    }
    
    .Success
    {
        color: Green;
    }
    
    #Message
    {
        float:right;
        width: 100px;
        margin-right: 15px;
        position:relative;
        top: 5px;
        right: 5px;
    }
</style>

<body>
    <h4 class="page-header">Change Pool Assignment</h4>
    <form id="form1" runat="server">
    <div>
        <label>
            Pool Agency
        </label>
        <asp:DropDownList ID="sPoolAgencyT" runat="server">
        </asp:DropDownList>
    </div>
    <div>
        <span id="Message"></span>
        <label>
            Pool Number
        </label>
        <input ID="sPoolNumber"></input>
        <input type="button" class="FieldLabel" id="Lookup"
            value="Lookup" /> 
    </div>
    <input type="button" class="FieldLabel" id="AssignLoanToPool" value="Assign Loan To Pool" disabled="disabled" />
    </form>
</body>
</html>
