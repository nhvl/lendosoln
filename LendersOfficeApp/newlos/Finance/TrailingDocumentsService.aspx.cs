﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Finance
{
    public class TrailingDocumentsServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TrailingDocumentsServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {

            dataLoan.sRecordedD_rep = GetString("sRecordedD");

            
            dataLoan.sRecordingInstrumentNum = GetString("sRecordingInstrumentNum");
            dataLoan.sRecordingVolumeNum = GetString("sRecordingVolumeNum"); ;
            dataLoan.sRecordingBookNum = GetString("sRecordingBookNum"); ;
            dataLoan.sRecordingPageNum = GetString("sRecordingPageNum"); ;

            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.sMersRegistrationD_rep = GetString("sMersRegistrationD");
            dataLoan.sMersTobD_rep = GetString("sMersTobD");
            dataLoan.sMersTosD_rep = GetString("sMersTosD");
            dataLoan.sMersTosDLckd = GetBool("sMersTosDLckd");
            dataLoan.DisableMersMinCheck = true;
            dataLoan.sMersMin = GetString("sMersMin");
            dataLoan.DisableMersMinCheck = false;
            dataLoan.ByPassFieldSecurityCheck = false;

            dataLoan.sLoanPackageOrderedD_rep = GetString("sLoanPackageOrderedD");
            dataLoan.sLoanPackageDocumentD_rep = GetString("sLoanPackageDocumentD");
            dataLoan.sLoanPackageReceivedD_rep = GetString("sLoanPackageReceivedD");
            dataLoan.sLoanPackageShippedD_rep = GetString("sLoanPackageShippedD");
            dataLoan.sLoanPackageMethod = GetString("sLoanPackageMethod");
            dataLoan.sLoanPackageTracking = GetString("sLoanPackageTracking");
            dataLoan.sLoanPackageRequired = GetBool("sLoanPackageRequired");
            dataLoan.sLoanPackageReviewedD_rep = GetString("sLoanPackageReviewedD");
            dataLoan.sLoanPackageN = GetString("sLoanPackageN");

            dataLoan.sMortgageDOTOrderedD_rep = GetString("sMortgageDOTOrderedD");
            dataLoan.sMortgageDOTDocumentD_rep = GetString("sMortgageDOTDocumentD");
            dataLoan.sMortgageDOTReceivedD_rep = GetString("sMortgageDOTReceivedD");
            dataLoan.sMortgageDOTShippedD_rep = GetString("sMortgageDOTShippedD");
            dataLoan.sMortgageDOTMethod = GetString("sMortgageDOTMethod");
            dataLoan.sMortgageDOTTracking = GetString("sMortgageDOTTracking");
            dataLoan.sMortgageDOTRequired = GetBool("sMortgageDOTRequired");
            dataLoan.sMortgageDOTReviewedD_rep = GetString("sMortgageDOTReviewedD");
            dataLoan.sMortgageDOTN = GetString("sMortgageDOTN");

            dataLoan.sNoteOrderedD_rep = GetString("sNoteOrderedD");
            dataLoan.sDocumentNoteD_rep = GetString("sDocumentNoteD");
            dataLoan.sNoteReceivedD_rep = GetString("sNoteReceivedD");
            dataLoan.sNoteShippedD_rep = GetString("sNoteShippedD");
            dataLoan.sNoteMethod = GetString("sNoteMethod");
            dataLoan.sNoteTracking = GetString("sNoteTracking");
            dataLoan.sNoteRequired = GetBool("sNoteRequired");
            dataLoan.sNoteReviewedD_rep = GetString("sNoteReviewedD");
            dataLoan.sNoteN = GetString("sNoteN");

            dataLoan.sFinalHUD1SttlmtStmtOrderedD_rep = GetString("sFinalHUD1SttlmtStmtOrderedD");
            dataLoan.sFinalHUD1SttlmtStmtDocumentD_rep = GetString("sFinalHUD1SttlmtStmtDocumentD");
            dataLoan.sFinalHUD1SttlmtStmtReceivedD_rep = GetString("sFinalHUD1SttlmtStmtReceivedD");
            dataLoan.sFinalHUD1SttlmtStmtShippedD_rep = GetString("sFinalHUD1SttlmtStmtShippedD");
            dataLoan.sFinalHUD1SttlmtStmtMethod = GetString("sFinalHUD1SttlmtStmtMethod");
            dataLoan.sFinalHUD1SttlmtStmtTracking = GetString("sFinalHUD1SttlmtStmtTracking");
            dataLoan.sFinalHUD1SttlmtStmtRequired = GetBool("sFinalHUD1SttlmtStmtRequired");
            dataLoan.sFinalHUD1SttlmtStmtReviewedD_rep = GetString("sFinalHUD1SttlmtStmtReviewedD");
            dataLoan.sFinalHUD1SttlmtStmtN = GetString("sFinalHUD1SttlmtStmtN");

            dataLoan.sTitleInsPolicyOrderedD_rep = GetString("sTitleInsPolicyOrderedD");
            dataLoan.sTitleInsPolicyDocumentD_rep = GetString("sTitleInsPolicyDocumentD");
            dataLoan.sTitleInsPolicyReceivedD_rep = GetString("sTitleInsPolicyReceivedD");
            dataLoan.sTitleInsPolicyShippedD_rep = GetString("sTitleInsPolicyShippedD");
            dataLoan.sTitleInsPolicyMethod = GetString("sTitleInsPolicyMethod");
            dataLoan.sTitleInsPolicyTracking = GetString("sTitleInsPolicyTracking");
            dataLoan.sTitleInsPolicyRequired = GetBool("sTitleInsPolicyRequired");
            dataLoan.sTitleInsPolicyReviewedD_rep = GetString("sTitleInsPolicyReviewedD");
            dataLoan.sTitleInsPolicyN = GetString("sTitleInsPolicyN");

            dataLoan.sMiCertOrderedD_rep = GetString("sMiCertOrderedD");
            dataLoan.sMiCertIssuedD_rep = GetString("sMiCertIssuedD");
            dataLoan.sMiCertReceivedD_rep = GetString("sMiCertReceivedD");
            dataLoan.sMiCertShippedD_rep = GetString("sMiCertShippedD");
            dataLoan.sMiCertMethod = GetString("sMiCertMethod");
            dataLoan.sMiCertTracking = GetString("sMiCertTracking");
            dataLoan.sMiCertRequired = GetBool("sMiCertRequired");
            dataLoan.sMiCertReviewedD_rep = GetString("sMiCertReviewedD");
            dataLoan.sMiCertN = GetString("sMiCertN");

            dataLoan.sFinalHazInsPolicyOrderedD_rep = GetString("sFinalHazInsPolicyOrderedD");
            dataLoan.sFinalHazInsPolicyDocumentD_rep = GetString("sFinalHazInsPolicyDocumentD"); ;
            dataLoan.sFinalHazInsPolicyReceivedD_rep = GetString("sFinalHazInsPolicyReceivedD");
            dataLoan.sFinalHazInsPolicyShippedD_rep = GetString("sFinalHazInsPolicyShippedD");
            dataLoan.sFinalHazInsPolicyMethod = GetString("sFinalHazInsPolicyMethod");
            dataLoan.sFinalHazInsPolicyTracking = GetString("sFinalHazInsPolicyTracking");
            dataLoan.sFinalHazInsPolicyRequired = GetBool("sFinalHazInsPolicyRequired");
            dataLoan.sFinalHazInsPolicyReviewedD_rep = GetString("sFinalHazInsPolicyReviewedD");
            dataLoan.sFinalHazInsPolicyN = GetString("sFinalHazInsPolicyN");

            dataLoan.sFinalFloodInsPolicyOrderedD_rep = GetString("sFinalFloodInsPolicyOrderedD");
            dataLoan.sFinalFloodInsPolicyDocumentD_rep = GetString("sFinalFloodInsPolicyDocumentD");
            dataLoan.sFinalFloodInsPolicyReceivedD_rep = GetString("sFinalFloodInsPolicyReceivedD");
            dataLoan.sFinalFloodInsPolicyShippedD_rep = GetString("sFinalFloodInsPolicyShippedD");
            dataLoan.sFinalFloodInsPolicyMethod = GetString("sFinalFloodInsPolicyMethod");
            dataLoan.sFinalFloodInsPolicyTracking = GetString("sFinalFloodInsPolicyTracking");
            dataLoan.sFinalFloodInsPolicyRequired = GetBool("sFinalFloodInsPolicyRequired");
            dataLoan.sFinalFloodInsPolicyReviewedD_rep = GetString("sFinalFloodInsPolicyReviewedD");
            dataLoan.sFinalFloodInsPolicyN = GetString("sFinalFloodInsPolicyN");

            dataLoan.sCustomTrailingDoc1Desc = GetString("sCustomTrailingDoc1Desc");
            dataLoan.sCustomTrailingDoc1DocumentD_rep = GetString("sCustomTrailingDoc1DocumentD");
            dataLoan.sCustomTrailingDoc1Method = GetString("sCustomTrailingDoc1Method");
            dataLoan.sCustomTrailingDoc1OrderedD_rep = GetString("sCustomTrailingDoc1OrderedD");
            dataLoan.sCustomTrailingDoc1ReceivedD_rep = GetString("sCustomTrailingDoc1ReceivedD");
            dataLoan.sCustomTrailingDoc1Required = GetBool("sCustomTrailingDoc1Required");
            dataLoan.sCustomTrailingDoc1ReviewedD_rep = GetString("sCustomTrailingDoc1ReviewedD");
            dataLoan.sCustomTrailingDoc1ShippedD_rep = GetString("sCustomTrailingDoc1ShippedD");
            dataLoan.sCustomTrailingDoc1Tracking = GetString("sCustomTrailingDoc1Tracking");
            dataLoan.sCustomTrailingDoc1N = GetString("sCustomTrailingDoc1N");

            dataLoan.sCustomTrailingDoc2Desc = GetString("sCustomTrailingDoc2Desc");
            dataLoan.sCustomTrailingDoc2DocumentD_rep = GetString("sCustomTrailingDoc2DocumentD");
            dataLoan.sCustomTrailingDoc2Method = GetString("sCustomTrailingDoc2Method");
            dataLoan.sCustomTrailingDoc2OrderedD_rep = GetString("sCustomTrailingDoc2OrderedD");
            dataLoan.sCustomTrailingDoc2ReceivedD_rep = GetString("sCustomTrailingDoc2ReceivedD");
            dataLoan.sCustomTrailingDoc2Required = GetBool("sCustomTrailingDoc2Required");
            dataLoan.sCustomTrailingDoc2ReviewedD_rep = GetString("sCustomTrailingDoc2ReviewedD");
            dataLoan.sCustomTrailingDoc2ShippedD_rep = GetString("sCustomTrailingDoc2ShippedD");
            dataLoan.sCustomTrailingDoc2Tracking = GetString("sCustomTrailingDoc2Tracking");
            dataLoan.sCustomTrailingDoc2N = GetString("sCustomTrailingDoc2N");

            dataLoan.sCustomTrailingDoc3Desc = GetString("sCustomTrailingDoc3Desc");
            dataLoan.sCustomTrailingDoc3DocumentD_rep = GetString("sCustomTrailingDoc3DocumentD");
            dataLoan.sCustomTrailingDoc3Method = GetString("sCustomTrailingDoc3Method");
            dataLoan.sCustomTrailingDoc3OrderedD_rep = GetString("sCustomTrailingDoc3OrderedD");
            dataLoan.sCustomTrailingDoc3ReceivedD_rep = GetString("sCustomTrailingDoc3ReceivedD");
            dataLoan.sCustomTrailingDoc3Required = GetBool("sCustomTrailingDoc3Required");
            dataLoan.sCustomTrailingDoc3ReviewedD_rep = GetString("sCustomTrailingDoc3ReviewedD");
            dataLoan.sCustomTrailingDoc3ShippedD_rep = GetString("sCustomTrailingDoc3ShippedD");
            dataLoan.sCustomTrailingDoc3Tracking = GetString("sCustomTrailingDoc3Tracking");
            dataLoan.sCustomTrailingDoc3N = GetString("sCustomTrailingDoc3N");

            dataLoan.sCustomTrailingDoc4Desc = GetString("sCustomTrailingDoc4Desc");
            dataLoan.sCustomTrailingDoc4DocumentD_rep = GetString("sCustomTrailingDoc4DocumentD");
            dataLoan.sCustomTrailingDoc4Method = GetString("sCustomTrailingDoc4Method");
            dataLoan.sCustomTrailingDoc4OrderedD_rep = GetString("sCustomTrailingDoc4OrderedD");
            dataLoan.sCustomTrailingDoc4ReceivedD_rep = GetString("sCustomTrailingDoc4ReceivedD");
            dataLoan.sCustomTrailingDoc4Required = GetBool("sCustomTrailingDoc4Required");
            dataLoan.sCustomTrailingDoc4ReviewedD_rep = GetString("sCustomTrailingDoc4ReviewedD");
            dataLoan.sCustomTrailingDoc4ShippedD_rep = GetString("sCustomTrailingDoc4ShippedD");
            dataLoan.sCustomTrailingDoc4Tracking = GetString("sCustomTrailingDoc4Tracking");
            dataLoan.sCustomTrailingDoc4N = GetString("sCustomTrailingDoc4N");

           // dataLoan.sRequiredDocsNotOrderedCount = dataLoan.sRequiredDocsNotOrderedCount;

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sRequiredDocsNotOrderedCount", dataLoan.sRequiredDocsNotOrderedCount);
        }
    }

    public partial class TrailingDocumentsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new TrailingDocumentsServiceItem());
        }
    }
}
