﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class Transactions : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantRead };
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Transactions));
            dataLoan.InitLoad();

            sTransactionsTotPmtsRecvd.Text = dataLoan.sTransactionsTotPmtsRecvd_rep;
            sTransactionsTotPmtsMade.Text = dataLoan.sTransactionsTotPmtsMade_rep;
            sTransactionsNetPmtsToDate.Text = dataLoan.sTransactionsNetPmtsToDate_rep;
            sTransactionsTotPmtsReceivable.Text = dataLoan.sTransactionsTotPmtsReceivable_rep;
            sTransactionsTotPmtsPayable.Text = dataLoan.sTransactionsTotPmtsPayable_rep;
            sTransactionsNetPmtsDue.Text = dataLoan.sTransactionsNetPmtsDue_rep;
            RegisterJsObject("sTransactions", dataLoan.sTransactions);
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Transactions";
            this.PageID = "Transactions";
            RegisterJsObject("PmtDirOptions", DataAccess.CPageBase.sTransaction_map);
            RegisterJsScript("DynamicTable.js");
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
