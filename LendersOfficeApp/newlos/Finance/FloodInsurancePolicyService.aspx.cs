﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class FloodInsurancePolicyServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FloodInsurancePolicyServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sFloodInsCompanyNm = GetString("sFloodInsCompanyNm");
            dataLoan.sFloodInsPolicyNum = GetString("sFloodInsPolicyNum");
            dataLoan.sFloodInsPolicyActivationD_rep = GetString("sFloodInsPolicyActivationD");
            dataLoan.sFloodInsPaidByT = (E_sInsPaidByT)GetInt("sFloodInsPaidByT");
            dataLoan.sFloodInsCoverageAmt_rep = GetString("sFloodInsCoverageAmt");
            dataLoan.sFloodInsDeductibleAmt_rep = GetString("sFloodInsDeductibleAmt");
            dataLoan.sFloodInsPaymentDueAmt_rep = GetString("sFloodInsPaymentDueAmt");
            dataLoan.sFloodInsPaymentDueAmtLckd = GetBool("sFloodInsPaymentDueAmtLckd");
            dataLoan.sFloodInsPaymentDueD_rep = GetString("sFloodInsPaymentDueD");
            dataLoan.sFloodInsPaymentMadeD_rep = GetString("sFloodInsPaymentMadeD");
            dataLoan.sFloodInsPaidThroughD_rep = GetString("sFloodInsPaidThroughD");
            dataLoan.sFloodInsPolicyPayeeCode = GetString("sFloodInsPolicyPayeeCode");
            dataLoan.sFloodInsPolicyExpirationD_rep = GetString("sFloodInsPolicyExpirationD");
            dataLoan.sFloodInsPolicyPaymentTypeT = (E_InsPolicyPaymentTypeT)GetInt("sFloodInsPolicyPaymentTypeT");
            dataLoan.sFloodInsCompanyId = GetString("sFloodInsCompanyId");

            dataLoan.sFloodInsPolicyLossPayeeTransD_rep = GetString("sFloodInsPolicyLossPayeeTransD");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sFloodInsCompanyNm", dataLoan.sFloodInsCompanyNm);
            SetResult("sFloodInsPolicyNum", dataLoan.sFloodInsPolicyNum);
            SetResult("sFloodInsPolicyActivationD", dataLoan.sFloodInsPolicyActivationD_rep);
            SetResult("sFloodInsPaidByT", dataLoan.sFloodInsPaidByT);
            SetResult("sFloodInsCoverageAmt", dataLoan.sFloodInsCoverageAmt_rep);
            SetResult("sFloodInsDeductibleAmt", dataLoan.sFloodInsDeductibleAmt_rep);
            SetResult("sFloodInsPaymentDueAmt", dataLoan.sFloodInsPaymentDueAmt_rep);
            SetResult("sFloodInsPaymentDueAmtLckd", dataLoan.sFloodInsPaymentDueAmtLckd);
            SetResult("sFloodInsPaymentDueD", dataLoan.sFloodInsPaymentDueD_rep);
            SetResult("sFloodInsPaymentMadeD", dataLoan.sFloodInsPaymentMadeD_rep);
            SetResult("sFloodInsPaidThroughD", dataLoan.sFloodInsPaidThroughD_rep);
            SetResult("sFloodInsPolicyPayeeCode", dataLoan.sFloodInsPolicyPayeeCode);
            SetResult("sFloodInsPolicyExpirationD", dataLoan.sFloodInsPolicyExpirationD_rep);
            SetResult("sFloodInsPolicyPaymentTypeT", dataLoan.sFloodInsPolicyPaymentTypeT);
            SetResult("sFloodInsCompanyId", dataLoan.sFloodInsCompanyId);

            SetResult("sFloodInsPolicyLossPayeeTransD", dataLoan.sFloodInsPolicyLossPayeeTransD_rep);
        }
    }
    public partial class FloodInsurancePolicyService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new FloodInsurancePolicyServiceItem());
        }
    }
}
