﻿#region Generated Code
namespace LendersOfficeApp.newlos.Finance
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.UcdDelivery;
    using LendersOffice.Integration.UcdDelivery.FannieMae;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// UCD Delivery Service Page.
    /// </summary>
    public partial class UcdDeliveryServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Triggers a process by name.
        /// </summary>
        /// <param name="methodName">The name of the method to invoke.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadUcdDeliveries":
                    this.LoadUcdDeliveries();
                    break;
                case "SaveUcdDeliveries":
                    this.SaveUcdDeliveries();
                    break;
                case nameof(this.RetrieveFromFannie):
                    this.RetrieveFromFannie();
                    break;
                default:
                    throw new ArgumentException($"Unknown method {methodName}");
            }
        }

        /// <summary>
        /// Bind data from page to loan.
        /// </summary>
        /// <param name="dataLoan">The Loan data.</param>
        /// <param name="dataApp">The App data.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // DO NOTHING.
        }

        /// <summary>
        /// Constructs CPageData object.
        /// </summary>
        /// <param name="loanId">The Loan Id.</param>
        /// <returns>CPageData object.</returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(UcdDeliveryServiceItem));
        }

        /// <summary>
        /// Load data from loan to page.
        /// </summary>
        /// <param name="dataLoan">The Loan data.</param>
        /// <param name="dataApp">The App data.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            // DO NOTHING.
        }

        /// <summary>
        /// Loads UCD Deliveries from DB.
        /// </summary>
        private void LoadUcdDeliveries()
        {
            Guid loanId = GetGuid("LoanId");

            CPageData dataLoan = ConstructPageDataClass(loanId);
            dataLoan.InitLoad();

            UpdateViewModel(dataLoan);
        }

        /// <summary>
        /// Save UCD Deliveries to DB.
        /// </summary>
        private void SaveUcdDeliveries()
        {
            Guid loanId = GetGuid("LoanId");
            string updatedDeliveriesJson = GetString("UpdatedDeliveries");
            string deletedDeliveriesJson = GetString("DeletedDeliveries");

            List<UcdDeliveryView> updatedDeliveries = SerializationHelper.JsonNetDeserialize<List<UcdDeliveryView>>(updatedDeliveriesJson);
            List<UcdDeliveryView> deletedDeliveries = SerializationHelper.JsonNetDeserialize<List<UcdDeliveryView>>(deletedDeliveriesJson);

            if (!updatedDeliveries.Any() && !deletedDeliveries.Any())
            {
                return;
            }

            CPageData dataLoan = ConstructPageDataClass(loanId);
            dataLoan.InitSave(sFileVersion);

            foreach (UcdDeliveryView view in updatedDeliveries)
            {
                // Add new deliveries.
                if (view.UcdDeliveryId < 0)
                {
                    // But only if they are Manually entered deliveries.
                    if (view.UcdDeliveryType != UcdDeliveryType.Manual)
                    {
                        // Log warning and continue.
                        Tools.LogWarning($"[UcdDeliveryService.aspx] Attempted to add an automated delivery. LoanId=[{dataLoan.sLId}]. Delivery type=[{view.UcdDeliveryType}].");
                        continue;
                    }

                    dataLoan.sUcdDeliveryCollection.Add(new ManualUcdDelivery(view));
                    continue;
                }

                // Update existing delivery.
                LendersOffice.Integration.UcdDelivery.UcdDelivery delivery = dataLoan.sUcdDeliveryCollection.Get(view.UcdDeliveryId);

                // Make sure object is in collection. Throw if it is not. Something is wrong here.
                if (delivery == null)
                {
                    throw new CBaseException(
                        ErrorMessages.Generic,
                        $"[UcdDeliveryService.aspx] Attempted to update a delivery that is not in the collection. LoanId=[{dataLoan.sLId}]. Delivery ID=[{view.UcdDeliveryId}].");
                }

                // Update fields of Manually entered deliveries.
                // NOTE: Automated deliveries can only update the Note field.
                if (delivery.UcdDeliveryType == UcdDeliveryType.Manual)
                {
                    delivery.CaseFileId = view.CaseFileId;
                    delivery.DeliveredTo = view.DeliveredTo;
                    delivery.FileDeliveredDocumentId = view.FileDeliveredDocumentId;
                    delivery.DateOfDelivery = view.DateOfDelivery;
                }

                delivery.IncludeInUldd = view.IncludeInUldd;
                delivery.Note = view.Note;
            }

            foreach (UcdDeliveryView deletedDelivery in deletedDeliveries)
            {
                // We don't need to deleted new deliveries that aren't yet in DB.
                if (deletedDelivery.UcdDeliveryId < 0)
                {
                    continue;
                }

                // Only manually entered deliveries can be deleted.
                if (deletedDelivery.UcdDeliveryType != UcdDeliveryType.Manual)
                {
                    // Log warning, but don't error out.
                    Tools.LogWarning($"[UcdDeliveryService.aspx] Attempted to delete automated delivery. LoanId=[{dataLoan.sLId}]. Delivery type=[{deletedDelivery.UcdDeliveryType}]. Delivery ID=[{deletedDelivery.UcdDeliveryId}].");
                    continue;
                }

                dataLoan.sUcdDeliveryCollection.Remove(deletedDelivery.UcdDeliveryId);
            }

            dataLoan.Save();

            // Update View.
            UpdateViewModel(dataLoan);
        }

        /// <summary>
        /// Updates the view model with data from the loan.
        /// </summary>
        /// <param name="dataLoan">Loan data.</param>
        private void UpdateViewModel(CPageData dataLoan)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            UcdDeliveryView[] views = dataLoan.sUcdDeliveryCollection.ToView();
            SetResult("UcdDeliveries", SerializationHelper.JsonNetAnonymousSerialize(views));
            SetResult("HasNonManual", views.Any(v => v.UcdDeliveryType != UcdDeliveryType.Manual));
            var ucdConfig = principal.BrokerDB.GetUcdConfig();
            SetResult("HasDocMagicVendor", principal.BrokerDB.AllActiveDocumentVendors.Any(vendorSettings => vendorSettings.VendorId != Guid.Empty && VendorConfig.Retrieve(vendorSettings.VendorId).PlatformType == E_DocumentVendor.DocMagic));
            SetResult("DocMagicDeliveryEnabled", ucdConfig.AllowDocMagicDeliveryToFannieMae || ucdConfig.AllowDocMagicDeliveryToFreddieMac);
            SetResult("DirectIntegrationEnabled", ucdConfig.AllowLqbDeliveryToFannieMae || (ucdConfig.AllowLqbDeliveryToFreddieMac && LendersOffice.Constants.ConstStage.EnableLoanClosingAdvisor));
        }

        /// <summary>
        /// Retrieves UCD findings from Fannie.
        /// </summary>
        private void RetrieveFromFannie()
        {
            var loanId = this.GetGuid("LoanId");
            var appId = this.GetGuid("AppId");
            var deliveryId = this.GetInt("DeliveryId");
            var principal = PrincipalFactory.CurrentPrincipal;
            var username = this.GetString("Username");
            var password = this.GetString("Password");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(UcdDeliveryServiceItem));
            dataLoan.InitLoad();

            var deliveryItem = dataLoan.sUcdDeliveryCollection.Get(deliveryId);
            if (deliveryItem == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(new List<string>() { "Unable to find UCD delivery item." }));
                return;
            }

            var serviceCredential = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.UcdDelivery)
                .FirstOrDefault((cred) => cred.UcdDeliveryTarget == UcdDeliveryTargetOption.FannieMaeUcdCollectionSolution);
            if (serviceCredential != null)
            {
                username = serviceCredential.UserName;
                password = serviceCredential.UserPassword.Value;
            }

            var requestData = FannieMaeUcdDeliveryRequestData.CreateFannieMaeGetRequestData(loanId, appId, deliveryItem, principal);
            requestData.GseUserName = username;
            requestData.GsePassword = password;

            string error;
            if (!requestData.Validate(out error))
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(new List<string>() { error }));
                return;
            }

            AbstractUcdDeliveryRequestHandler requestHandler = AbstractUcdDeliveryRequestHandler.GenerateRequestHandler(requestData);
            LendersOffice.Integration.UcdDelivery.UcdDelivery deliveryOrder;
            List<string> errors;

            var result = requestHandler.SubmitRequest(out deliveryOrder, out errors);
            if (result == UcdDeliveryResultStatus.Retrieved)
            {
                this.SetResult("Success", true);
                this.SetResult("Message", "UCD findings have been retrieved.");
                this.SetResult("DeliveryItem", SerializationHelper.JsonNetAnonymousSerialize(deliveryOrder.ToView()));
            }
            else if (result == UcdDeliveryResultStatus.Error)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(errors));
            }
            else
            {
                // Any other result is not expected. We'll log this.
                Tools.LogError($"Unexpected result {result}.");
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(new List<string>() { ErrorMessages.Generic }));
            }
        }
    }

    /// <summary>
    /// UCD Delivery Service Page.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Reviewed.")]
    public partial class UcdDeliveryService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initialize function for UcdDeliveryService.
        /// </summary>
        protected override void Initialize()
        {
            this.AddBackgroundItem(string.Empty, new UcdDeliveryServiceItem());
        }
    }
}