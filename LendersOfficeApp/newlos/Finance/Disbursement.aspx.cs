﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class Disbursement : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite, Permission.AllowAccountantRead };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead, Permission.AllowAccountantRead };
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Disbursement));
            dataLoan.InitLoad();

            sDisbursementWarehouseLineIntLckd.Checked = dataLoan.sDisbursementWarehouseLineIntLckd;
            sDisbursementWarehouseLineInt.Text = dataLoan.sDisbursementWarehouseLineInt_rep;
            sDisbursementOther.Text = dataLoan.sDisbursementOther_rep;
            sDisbursementWarehouseLineFees.Text = dataLoan.sDisbursementWarehouseLineFees_rep;
            sDisbursementWarehousePerDiemInterest.Text = dataLoan.sDisbursementWarehousePerDiemInterest_rep;
            sDisbursementDaysOfInterest.Text = dataLoan.sDisbursementDaysOfInterest_rep;
            sDisbursementDaysOfInterestLckd.Checked = dataLoan.sDisbursementDaysOfInterestLckd;
            sDisbursementNotes.Text = dataLoan.sDisbursementNotes;
            sFundD.Text = dataLoan.sFundD_rep;
            sDisbursementD.Text = dataLoan.sDisbursementD_rep;
            sDisbursementAmtFundFromWarehouseLine.Text = dataLoan.sDisbursementAmtFundFromWarehouseLine_rep;
            sDisbursementNetReceived.Text = dataLoan.sDisbursementNetReceived_rep;
            sPurchaseAdviceSummaryTotalDueSeller.Text = dataLoan.sPurchaseAdviceSummaryTotalDueSeller_rep;
            
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Disbursement";
            this.PageID = "Disbursement";
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
