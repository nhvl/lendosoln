﻿using System;
using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class LoanRepurchaseServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanRepurchaseServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sRepurchD_rep = GetString("sRepurchD");
            dataLoan.sFirstPmtAfterRepurchDueD_rep = GetString("sFirstPmtAfterRepurchDueD");
            dataLoan.sUnpaidBalAtRepurch_rep = GetString("sUnpaidBalAtRepurch");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sRepurchD", dataLoan.sRepurchD_rep);
            SetResult("sFirstPmtAfterRepurchDueD", dataLoan.sFirstPmtAfterRepurchDueD_rep);
            SetResult("sUnpaidBalAtRepurch", dataLoan.sUnpaidBalAtRepurch_rep);
        }
    }

    public partial class LoanRepurchaseService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new LoanRepurchaseServiceItem());
        }
    }
}
