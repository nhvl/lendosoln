﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Accounting.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.Accounting" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Accounting</title>
</head>
<body bgcolor="gainsboro">
    <form id="Accounting" runat="server">
        <table cellSpacing="0" cellPadding="0" width="100%">
			<tr>
				<td class="MainRightHeader" noWrap>
				    Accounting
				</td>
			</tr>
			<tr>
				<td style="padding:4px">
				    <table class="FieldLabel">
				        <tr>
				            <td style="width:200px" colspan="2">&nbsp;</td>
				            <td>Cash</td>
				            <td>Payables</td>
				            <td>Receivables</td>
				            <td>Loans held for sale</td>
				        </tr>
                        <tr>
                            <td colspan="2"> <a href="javascript:linkMe('../Closer/Funding.aspx');" title="Go to Funding page">Funding</a></td>
                            <td><ml:MoneyTextBox ID="sFundingCash" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sFundingPayables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sFundingReceivables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sFundingLHS" ReadOnly="true" runat="server"/></td>
                        </tr>
                        <tr>
                            <td colspan="2"><a href="javascript:linkMe('../Status/Commission.aspx');" title="Go to Commissions page">Commissions</a></td>
                            <td><ml:MoneyTextBox ID="sCommissionsCash" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sCommissionsPayables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sCommissionsReceivables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sCommissionsLHS" ReadOnly="true" runat="server"/></td>
                        </tr>
                        <tr>
                            <td colspan="2"><a href="javascript:linkMe('Servicing.aspx');" title="Go to Servicing page">Servicing</a></td>
                            <td><ml:MoneyTextBox ID="sServicingCash" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sServicingPayables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sServicingReceivables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sServicingLHS" ReadOnly="true" runat="server"/></td>
                        </tr>
				        <tr>
                            <td colspan="2"><a href="javascript:linkMe('Disbursement.aspx');" title="Go to Disbursement page">Disbursement</a></td>
                            <td><ml:MoneyTextBox ID="sDisbursementCash" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sDisbursementPayables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sDisbursementReceivables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sDisbursementLHS" ReadOnly="true" runat="server"/></td>
                        </tr>
                        <tr>
                            <td colspan="2"><a href="javascript:linkMe('../Status/TrustAccount.aspx');" title="Go to Trust account page">Trust account balance</a></td>
                            <td><ml:MoneyTextBox ID="sTrustAccountCash" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sTrustAccountPayables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sTrustAccountReceivables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sTrustAccountLHS" ReadOnly="true" runat="server"/></td>
                        </tr>
                        <tr>
                            <td colspan="2"><a href="javascript:linkMe('Transactions.aspx');" title="Go to Transactions page">Transactions</a></td>
                            <td><ml:MoneyTextBox ID="sTransactionsCash" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sTransactionsPayables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sTransactionsReceivables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sTransactionsLHS" ReadOnly="true" runat="server"/></td>
                        </tr>				       
				        <tr>
				            <td colspan="6"><hr /></td>
				        </tr>
				        <tr>
				            <td>Total gain</td>
				            <td align="right"><ml:PercentTextBox ID="sTotalGainPercent" ReadOnly="true" runat="server" /></td>
                            <td><ml:MoneyTextBox ID="sTotalCash" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sTotalPayables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sTotalReceivables" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sTotalLHS" ReadOnly="true" runat="server"/></td>                            
				        </tr>
				        <tr>
                            <td colspan='2'>Gross Projected Profit / Loss</td>
                            <td><ml:PercentTextBox Width="100%" ID="sInvestorLockProjectedProfit" ReadOnly="true" runat="server"/></td>
                            <td><ml:MoneyTextBox ID="sInvestorLockProjectedProfitAmt" ReadOnly="true" runat="server"/></td>
                        </tr>
				    </table>
				</td>
			</tr>
		</table>
	    <uc1:cmodaldlg id="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cmodaldlg>
    </form>
</body>
</html>
