﻿using System;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class LoanRepurchase : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LoanRepurchase));
            dataLoan.InitLoad();

            sRepurchD.Text = dataLoan.sRepurchD_rep;
            sFirstPmtAfterRepurchDueD.Text = dataLoan.sFirstPmtAfterRepurchDueD_rep;
            sUnpaidBalAtRepurch.Text = dataLoan.sUnpaidBalAtRepurch_rep;

        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "LoanRepurchase";
            this.PageID = "LoanRepurchase";
        }
    }
}
