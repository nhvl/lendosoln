﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanRepurchase.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.LoanRepurchase" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Loan Repurchase</title>
    <script type="text/javascript" src="../../inc/utilities.js" ></script>
    <style type="text/css">
        .FieldLabel { width: 220px; }
    </style>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
        <table cellSpacing="0" cellPadding="0" width="100%">
			<tr>
				<td class="MainRightHeader" colspan="2" noWrap>
				    Loan Repurchase
				</td>
			</tr>
			<tr>
			    <td class="FieldLabel">
			        Repurchase Date
			    </td>
			    <td>
			        <ml:DateTextBox id="sRepurchD" runat="server"></ml:DateTextBox>
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel">
			        First Pmt After Repurchase Due Date
			    </td>
			    <td>
			        <ml:DateTextBox id="sFirstPmtAfterRepurchDueD" runat="server"></ml:DateTextBox>
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel">
			        Unpaid Balance At Repurchase Date
			    </td>
			    <td>
			        <ml:MoneyTextBox ID="sUnpaidBalAtRepurch" runat="server"></ml:MoneyTextBox>
			    </td>
			</tr>
	    </table>
    </form>
</body>
</html>
