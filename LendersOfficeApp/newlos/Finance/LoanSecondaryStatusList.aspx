﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanSecondaryStatusList.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.LoanSecondaryStatusList" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <title>Loan Secondary Status List</title>
  <style type="text/css">
    ul{ margin-left: 5px; }
  </style>
</head>
<body onload="init();" bgcolor="gainsboro">
		<script language="javascript">
        <!--
        function init() {
              resize(300, 200);
        }
        //-->
		</script>
        <h4 class="page-header">Loan Secondary Status List</h4>
		<form id="LoanStatusList" method="post" runat="server">
			<table width="100%" border="0">
				<tr>
					<td class="FieldLabel">Change secondary status to</td>
				</tr>
				<tr>
					<td>
					    <ul id="m_loanLinks" runat="server" />
                    </td>
				</tr>
				<tr>
					<td align="center"><input type="button" value="Cancel" onclick="onClosePopup();" /></td>
				</tr>
			</table>
			<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
		</form>
	</body>
</html>
