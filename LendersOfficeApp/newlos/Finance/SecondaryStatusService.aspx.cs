﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public class SecondaryStatusServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(SecondaryStatusServiceItem));
        }

        private BrokerUserPrincipal CurrentUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sSecondStatusT = (E_sSecondStatusT)GetInt("sSecondStatusT");
            dataLoan.sSecondStatusLckd = GetBool("sSecondStatusLckd");

            if(CurrentUser.HasPermission(Permission.AllowLockDeskWrite))
            {
                dataLoan.sInvestorLockRLckdD_rep = GetString("sInvRLckdD");
                dataLoan.sInvestorLockRLckExpiredD_rep = GetString("sInvRLckdExpiredD");
                dataLoan.sInvestorLockDeliveryExpiredD_rep = GetString("sInvDelvExpD");
            }

            dataLoan.sFundD_rep = GetString("sFundD");
            dataLoan.sLPurchaseD_rep = GetString("sLPurchaseD");
            dataLoan.sDisbursementD_rep = GetString("sDisbursementD");
            dataLoan.sReconciledD_rep = GetString("sReconciledD");
            dataLoan.sShippedToWarehouseD_rep = GetString("sShippedToWarehouseD");
            dataLoan.sShippedToInvestorD_rep = GetString("sShippedToInvestorD");
            dataLoan.sSuspendedByInvestorD_rep = GetString("sSuspendedByInvestorD");
            dataLoan.sCondSentToInvestorD_rep = GetString("sCondSentToInvestorD");
            dataLoan.sAdditionalCondSentD_rep = GetString("sAdditionalCondSentD");
            dataLoan.sGoodByLetterD_rep = GetString("sGoodByLetterD");

            dataLoan.sMersRegistrationD_rep = GetString("sMersRegistrationD");
            dataLoan.sMersTobD_rep = GetString("sMersTobD");
            dataLoan.sMersTosD_rep = GetString("sMersTosD");
            dataLoan.sMersTosDLckd = GetBool("sMersTosDLckd");

            dataLoan.sServicingStartD_rep = GetString("sServicingStartD");
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");
            dataLoan.sInvSchedDueD1_rep = GetString("sInvSchedDueD1");

            // OPM 80416
            dataLoan.sLoanPackageOrderedD_rep = GetString("sLoanPackageOrderedD");
            dataLoan.sLoanPackageDocumentD_rep = GetString("sLoanPackageDocumentD");
            dataLoan.sLoanPackageReceivedD_rep = GetString("sLoanPackageReceivedD");
            dataLoan.sLoanPackageN = GetString("sLoanPackageN");
            dataLoan.sLoanPackageShippedD_rep = GetString("sLoanPackageShippedD");
            dataLoan.sLoanPackageMethod = GetString("sLoanPackageMethod");
            dataLoan.sLoanPackageTracking = GetString("sLoanPackageTracking");

            dataLoan.sMortgageDOTOrderedD_rep = GetString("sMortgageDOTOrderedD");
            dataLoan.sMortgageDOTDocumentD_rep = GetString("sMortgageDOTDocumentD");
            dataLoan.sMortgageDOTReceivedD_rep = GetString("sMortgageDOTReceivedD");
            dataLoan.sMortgageDOTN = GetString("sMortgageDOTN");
            dataLoan.sMortgageDOTShippedD_rep = GetString("sMortgageDOTShippedD");
            dataLoan.sMortgageDOTMethod = GetString("sMortgageDOTMethod");
            dataLoan.sMortgageDOTTracking = GetString("sMortgageDOTTracking");

            dataLoan.sNoteOrderedD_rep = GetString("sNoteOrderedD");
            dataLoan.sDocumentNoteD_rep = GetString("sDocumentNoteD");
            dataLoan.sNoteReceivedD_rep = GetString("sNoteReceivedD");
            dataLoan.sNoteN = GetString("sNoteN");
            dataLoan.sNoteShippedD_rep = GetString("sNoteShippedD");
            dataLoan.sNoteMethod = GetString("sNoteMethod");
            dataLoan.sNoteTracking = GetString("sNoteTracking");

            dataLoan.sFinalHUD1SttlmtStmtOrderedD_rep = GetString("sFinalHUD1SttlmtStmtOrderedD");
            dataLoan.sFinalHUD1SttlmtStmtDocumentD_rep = GetString("sFinalHUD1SttlmtStmtDocumentD");
            dataLoan.sFinalHUD1SttlmtStmtReceivedD_rep = GetString("sFinalHUD1SttlmtStmtReceivedD");
            dataLoan.sFinalHUD1SttlmtStmtN = GetString("sFinalHUD1SttlmtStmtN");
            dataLoan.sFinalHUD1SttlmtStmtShippedD_rep = GetString("sFinalHUD1SttlmtStmtShippedD");
            dataLoan.sFinalHUD1SttlmtStmtMethod = GetString("sFinalHUD1SttlmtStmtMethod");
            dataLoan.sFinalHUD1SttlmtStmtTracking = GetString("sFinalHUD1SttlmtStmtTracking");

            dataLoan.sTitleInsPolicyOrderedD_rep = GetString("sTitleInsPolicyOrderedD");
            dataLoan.sTitleInsPolicyDocumentD_rep = GetString("sTitleInsPolicyDocumentD");
            dataLoan.sTitleInsPolicyReceivedD_rep = GetString("sTitleInsPolicyReceivedD");
            dataLoan.sTitleInsPolicyN = GetString("sTitleInsPolicyN");
            dataLoan.sTitleInsPolicyShippedD_rep = GetString("sTitleInsPolicyShippedD");
            dataLoan.sTitleInsPolicyMethod = GetString("sTitleInsPolicyMethod");
            dataLoan.sTitleInsPolicyTracking = GetString("sTitleInsPolicyTracking");

            dataLoan.sMiCertOrderedD_rep = GetString("sMiCertOrderedD");
            dataLoan.sMiCertIssuedD_rep = GetString("sMiCertIssuedD");
            dataLoan.sMiCertReceivedD_rep = GetString("sMiCertReceivedD");
            dataLoan.sMiCertN = GetString("sMiCertN");
            dataLoan.sMiCertShippedD_rep = GetString("sMiCertShippedD");
            dataLoan.sMiCertMethod = GetString("sMiCertMethod");
            dataLoan.sMiCertTracking = GetString("sMiCertTracking");

            dataLoan.sFinalHazInsPolicyOrderedD_rep = GetString("sFinalHazInsPolicyOrderedD");
            dataLoan.sFinalHazInsPolicyDocumentD_rep = GetString("sFinalHazInsPolicyDocumentD"); ;
            dataLoan.sFinalHazInsPolicyReceivedD_rep = GetString("sFinalHazInsPolicyReceivedD");
            dataLoan.sFinalHazInsPolicyN = GetString("sFinalHazInsPolicyN");
            dataLoan.sFinalHazInsPolicyShippedD_rep = GetString("sFinalHazInsPolicyShippedD");
            dataLoan.sFinalHazInsPolicyMethod = GetString("sFinalHazInsPolicyMethod");
            dataLoan.sFinalHazInsPolicyTracking = GetString("sFinalHazInsPolicyTracking");

            dataLoan.sFinalFloodInsPolicyOrderedD_rep = GetString("sFinalFloodInsPolicyOrderedD");
            dataLoan.sFinalFloodInsPolicyDocumentD_rep = GetString("sFinalFloodInsPolicyDocumentD");
            dataLoan.sFinalFloodInsPolicyReceivedD_rep = GetString("sFinalFloodInsPolicyReceivedD");
            dataLoan.sFinalFloodInsPolicyN = GetString("sFinalFloodInsPolicyN");
            dataLoan.sFinalFloodInsPolicyShippedD_rep = GetString("sFinalFloodInsPolicyShippedD");
            dataLoan.sFinalFloodInsPolicyMethod = GetString("sFinalFloodInsPolicyMethod");
            dataLoan.sFinalFloodInsPolicyTracking = GetString("sFinalFloodInsPolicyTracking");

            dataLoan.sCollateralPkgOrderedD_rep = GetString("sCollateralPkgOrderedD");
            dataLoan.sCollateralPkgShippedD_rep = GetString("sCollateralPkgShippedD");
            dataLoan.sCollateralPkgMethod = GetString("sCollateralPkgMethod");
            dataLoan.sCollateralPkgTracking = GetString("sCollateralPkgTracking");

            dataLoan.sLoanDocsArchiveD_rep = GetString("sLoanDocsArchiveD");
            dataLoan.sTrailingDocsArchiveD_rep = GetString("sTrailingDocsArchiveD");

            dataLoan.sCustomTrailingDoc1Desc = GetString("sCustomTrailingDoc1Desc");
            dataLoan.sCustomTrailingDoc1DocumentD_rep = GetString("sCustomTrailingDoc1DocumentD");
            dataLoan.sCustomTrailingDoc1Method = GetString("sCustomTrailingDoc1Method");
            dataLoan.sCustomTrailingDoc1OrderedD_rep = GetString("sCustomTrailingDoc1OrderedD");
            dataLoan.sCustomTrailingDoc1ReceivedD_rep = GetString("sCustomTrailingDoc1ReceivedD");
            dataLoan.sCustomTrailingDoc1ShippedD_rep = GetString("sCustomTrailingDoc1ShippedD");
            dataLoan.sCustomTrailingDoc1Tracking = GetString("sCustomTrailingDoc1Tracking");
            dataLoan.sCustomTrailingDoc1N = GetString("sCustomTrailingDoc1N");

            dataLoan.sCustomTrailingDoc2Desc = GetString("sCustomTrailingDoc2Desc");
            dataLoan.sCustomTrailingDoc2DocumentD_rep = GetString("sCustomTrailingDoc2DocumentD");
            dataLoan.sCustomTrailingDoc2Method = GetString("sCustomTrailingDoc2Method");
            dataLoan.sCustomTrailingDoc2OrderedD_rep = GetString("sCustomTrailingDoc2OrderedD");
            dataLoan.sCustomTrailingDoc2ReceivedD_rep = GetString("sCustomTrailingDoc2ReceivedD");
            dataLoan.sCustomTrailingDoc2ShippedD_rep = GetString("sCustomTrailingDoc2ShippedD");
            dataLoan.sCustomTrailingDoc2Tracking = GetString("sCustomTrailingDoc2Tracking");
            dataLoan.sCustomTrailingDoc2N = GetString("sCustomTrailingDoc2N");

            dataLoan.sCustomTrailingDoc3Desc = GetString("sCustomTrailingDoc3Desc");
            dataLoan.sCustomTrailingDoc3DocumentD_rep = GetString("sCustomTrailingDoc3DocumentD");
            dataLoan.sCustomTrailingDoc3Method = GetString("sCustomTrailingDoc3Method");
            dataLoan.sCustomTrailingDoc3OrderedD_rep = GetString("sCustomTrailingDoc3OrderedD");
            dataLoan.sCustomTrailingDoc3ReceivedD_rep = GetString("sCustomTrailingDoc3ReceivedD");
            dataLoan.sCustomTrailingDoc3ShippedD_rep = GetString("sCustomTrailingDoc3ShippedD");
            dataLoan.sCustomTrailingDoc3Tracking = GetString("sCustomTrailingDoc3Tracking");
            dataLoan.sCustomTrailingDoc3N = GetString("sCustomTrailingDoc3N");

            dataLoan.sCustomTrailingDoc4Desc = GetString("sCustomTrailingDoc4Desc");
            dataLoan.sCustomTrailingDoc4DocumentD_rep = GetString("sCustomTrailingDoc4DocumentD");
            dataLoan.sCustomTrailingDoc4Method = GetString("sCustomTrailingDoc4Method");
            dataLoan.sCustomTrailingDoc4OrderedD_rep = GetString("sCustomTrailingDoc4OrderedD");
            dataLoan.sCustomTrailingDoc4ReceivedD_rep = GetString("sCustomTrailingDoc4ReceivedD");
            dataLoan.sCustomTrailingDoc4ShippedD_rep = GetString("sCustomTrailingDoc4ShippedD");
            dataLoan.sCustomTrailingDoc4Tracking = GetString("sCustomTrailingDoc4Tracking");
            dataLoan.sCustomTrailingDoc4N = GetString("sCustomTrailingDoc4N");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sSecondStatusT", dataLoan.sSecondStatusT);
            SetResult("sSecondStatus", dataLoan.sSecondStatusT_rep);
            SetResult("sMersTosD", dataLoan.sMersTosD_rep);
            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
            SetResult("sLoanDocsArchiveD", dataLoan.sLoanDocsArchiveD_rep);
            SetResult("sTrailingDocsArchiveD", dataLoan.sTrailingDocsArchiveD_rep);
        }
    }

    public partial class SecondaryStatusService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new SecondaryStatusServiceItem());
        }
    }
}
