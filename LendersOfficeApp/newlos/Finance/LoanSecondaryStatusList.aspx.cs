﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class LoanSecondaryStatusList : BasePage
    {
        private void AddLinkItem(HtmlGenericControl parent, E_sSecondStatusT sSecondStatusT)
        {
            HtmlGenericControl li = new HtmlGenericControl("li");
            LinkButton linkButton = new LinkButton();

            linkButton.Text = CPageBase.sSecondStatusT_map_rep(sSecondStatusT);
            linkButton.CommandArgument = sSecondStatusT.ToString("D");
            linkButton.Command += new CommandEventHandler(LinkButton_Command);

            li.Controls.Add(linkButton);
            parent.Controls.Add(li);
        }

        private void LinkButton_Command(object sender, CommandEventArgs e)
        {
            E_sSecondStatusT sSecondStatusT = (E_sSecondStatusT)Enum.Parse(typeof(E_sSecondStatusT), e.CommandArgument.ToString());

            CloseDialog(sSecondStatusT);
        }

        private void CloseDialog(E_sSecondStatusT sSecondStatusT)
        {
            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, new string[] {
                "StatusCode=" + sSecondStatusT.ToString("D"),
                "Status=" + AspxTools.JsString(CPageBase.sSecondStatusT_map_rep(sSecondStatusT)),
                "OK=true"
            });
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            E_sSecondStatusT[] loanStatuses = {
                                            E_sSecondStatusT.None,
                                            E_sSecondStatusT.Loan_Funded,
                                            E_sSecondStatusT.Loan_Purchased,
                                            E_sSecondStatusT.Loan_Disbursed,
                                            E_sSecondStatusT.Loan_Reconciled
                                        };
            foreach (E_sSecondStatusT sSecondStatusT in loanStatuses)
            {
                AddLinkItem(m_loanLinks, sSecondStatusT);
            }

        }
    }
}
