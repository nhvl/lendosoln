﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class WindstormInsurancePolicyServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(WindstormInsurancePolicyServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sWindInsCompanyNm = GetString("sWindInsCompanyNm");
            dataLoan.sWindInsPolicyNum = GetString("sWindInsPolicyNum");
            dataLoan.sWindInsPolicyActivationD_rep = GetString("sWindInsPolicyActivationD");
            dataLoan.sWindInsSettlementChargeT = (E_sInsSettlementChargeT)GetInt("sWindInsSettlementChargeT");
            dataLoan.sWindInsPaidByT = (E_sInsPaidByT)GetInt("sWindInsPaidByT");
            dataLoan.sWindInsCoverageAmt_rep = GetString("sWindInsCoverageAmt");
            dataLoan.sWindInsDeductibleAmt_rep = GetString("sWindInsDeductibleAmt");
            dataLoan.sWindInsPaymentDueAmt_rep = GetString("sWindInsPaymentDueAmt");
            dataLoan.sWindInsPaymentDueAmtLckd = GetBool("sWindInsPaymentDueAmtLckd");
            dataLoan.sWindInsPaymentDueD_rep = GetString("sWindInsPaymentDueD");
            dataLoan.sWindInsPaymentMadeD_rep = GetString("sWindInsPaymentMadeD");
            dataLoan.sWindInsPaidThroughD_rep = GetString("sWindInsPaidThroughD");
            dataLoan.sWindInsCompanyId = GetString("sWindInsCompanyId");
            dataLoan.sWindInsPolicyPaymentTypeT = (E_InsPolicyPaymentTypeT)GetInt("sWindInsPolicyPaymentTypeT");
            dataLoan.sWindInsPolicyPayeeCode = GetString("sWindInsPolicyPayeeCode");
            dataLoan.sWindInsPolicyExpirationD_rep = GetString("sWindInsPolicyExpirationD");

            dataLoan.sWindInsPolicyLossPayeeTransD_rep = GetString("sWindInsPolicyLossPayeeTransD");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sWindInsCompanyNm", dataLoan.sWindInsCompanyNm);
            SetResult("sWindInsPolicyNum", dataLoan.sWindInsPolicyNum);
            SetResult("sWindInsPolicyActivationD", dataLoan.sWindInsPolicyActivationD_rep);
            SetResult("sWindInsSettlementChargeT", dataLoan.sWindInsSettlementChargeT);
            SetResult("sWindInsPaidByT", dataLoan.sWindInsPaidByT);
            SetResult("sWindInsCoverageAmt", dataLoan.sWindInsCoverageAmt_rep);
            SetResult("sWindInsDeductibleAmt", dataLoan.sWindInsDeductibleAmt_rep);
            SetResult("sWindInsPaymentDueAmt", dataLoan.sWindInsPaymentDueAmt_rep);
            SetResult("sWindInsPaymentDueAmtLckd", dataLoan.sWindInsPaymentDueAmtLckd);
            SetResult("sWindInsPaymentDueD", dataLoan.sWindInsPaymentDueD_rep);
            SetResult("sWindInsPaymentMadeD", dataLoan.sWindInsPaymentMadeD_rep);
            SetResult("sWindInsPaidThroughD", dataLoan.sWindInsPaidThroughD_rep);
            SetResult("sWindInsCompanyId", dataLoan.sWindInsCompanyId);
            SetResult("sWindInsPolicyPaymentTypeT", dataLoan.sWindInsPolicyPaymentTypeT);
            SetResult("sWindInsPolicyPayeeCode", dataLoan.sWindInsPolicyPayeeCode);
            SetResult("sWindInsPolicyExpirationD", dataLoan.sWindInsPolicyExpirationD_rep);

            SetResult("sWindInsPolicyLossPayeeTransD", dataLoan.sWindInsPolicyLossPayeeTransD_rep);
        }
    }
    public partial class WindstormInsurancePolicyService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new WindstormInsurancePolicyServiceItem());
        }
    }
}
