﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PayoffStatement.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.PayoffStatement" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Payoff Statement</title>
</head>

<body bgcolor="gainsboro">
    <script language="javascript" >
        function f_showHideDaysInYear() {
            var sPayoffStatementInterestCalcT = <%= AspxTools.JsGetElementById(sPayoffStatementInterestCalcT) %>;
            
            if (sPayoffStatementInterestCalcT.value == <%= AspxTools.JsString(E_sPayoffStatementInterestCalcT.PerDiem) %>) {
                $("#trDaysInYear").show();
                $(".tad2").hide();
            }
            else {
                $("#trDaysInYear").hide();
                $(".tad2").show();
            }
        }
        
        function f_lockRecipientAddress()
        {
            var sPayoffStatementRecipientT = <%= AspxTools.JsGetElementById(sPayoffStatementRecipientT) %>;
            var sPayoffStatementRecipientState = <%= AspxTools.JsGetElementById(sPayoffStatementRecipientState) %>;
            
            if(sPayoffStatementRecipientT.value == <%= AspxTools.JsString(E_sPayoffStatementRecipientT.Other) %>) {
                $(".RecipientAddress").prop("readonly", false);
                sPayoffStatementRecipientState.disabled = false;
                sPayoffStatementRecipientState.style.background  = "";
            } else {
                $(".RecipientAddress").prop("readonly", true);
                sPayoffStatementRecipientState.disabled = true;
                sPayoffStatementRecipientState.style.background  = "Gainsboro";
            }
        }
        
        function _init()
        {
            f_showHideDaysInYear();
            f_lockRecipientAddress();
        }

        function doAfterDateFormat(e)
        {
            if(e.id === "sPayoffStatementPayoffD")
            {
                refreshCalculation();
            }
        }
    </script>
    <form id="PayoffStatement" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="MainRightHeader" noWrap>
                    Payoff Statement
                </td>
            </tr>            
            <tr>
                <td>
                    <table class="FieldLabel">
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                Payoff date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sPayoffStatementPayoffD" runat="server" width="75" onchange="date_onblur(null, this);"/>
                            </td>
                            <td>
                                Unpaid principal balance
                            </td>
                            <td></td>
                            <td>
                                <ml:MoneyTextBox ID="sServicingUnpaidPrincipalBalance_col2" runat="server" CssClass="mask" preset="money" width="80" ReadOnly="true"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Last payment due servicer
                            </td>
                            <td>
                                <ml:DateTextBox ID="sServicingLastPmtDueD" runat="server" width="75" ReadOnly="true"/>
                            </td>
                            <td>
                                Total interest due
                            </td>
                            <td>
                                +
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sPayoffStatementTotalInterestDueAmt_col2" runat="server" CssClass="mask" preset="money" width="80" ReadOnly="true"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Next payment due date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sServicingNextPmtDue" runat="server" width="75" ReadOnly="true"/>
                            </td>
                            <td>
                                Monthly MI
                            </td>
                            <td>
                                +
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sPayoffStatementMonthlyMiDueAmt" runat="server" CssClass="mask" preset="money" width="80" ReadOnly="true"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Interest rate
                            </td>
                            <td>
                                <ml:PercentTextBox ID="sNoteIR" runat="server" width="70" CssClass="mask" preset="percent" ReadOnly="True"></ml:PercentTextBox>
                            </td>
                            
                            <td>
                                Escrow balance
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sServicingTotalDueFunds_Escrow" runat="server" CssClass="mask" preset="money" width="80" ReadOnly="true"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Unpaid principal balance
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sServicingUnpaidPrincipalBalance" runat="server" CssClass="mask" preset="money" width="80" ReadOnly="true"></ml:moneytextbox>
                            </td>
                            <td>
                                Current late charges
                            </td>
                            <td>
                                +
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sServicingTotalDueFunds_LateFees" runat="server" CssClass="mask" preset="money" width="80" ReadOnly="true"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Interest calculation method
                            </td>
                            <td>
                                <asp:DropDownList ID="sPayoffStatementInterestCalcT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trDaysInYear">
                            <td style="padding-left:20px;">
                                Days in year
                            </td>
                            <td>
                                <asp:textbox id="sDaysInYr" runat="server" Width="55px" MaxLength="3" ReadOnly="true"></asp:textbox>
                            </td>
                            <td class="tad1">
                                Total amount due
                            </td>
                            <td></td>
                            <td>
                                <ml:MoneyTextBox ID="sPayoffStatementTotalDueAmt" runat="server" CssClass="mask" preset="money" width="80" ReadOnly="true"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:20px;">
                                Per-diem interest
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sSettlementIPerDay" runat="server" decimalDigits="6" CssClass="mask" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                            </td>
                            
                            <td class="tad2">
                                Total amount due
                            </td class="tad2">
                            <td></td>
                            <td class="tad2">
                                <ml:MoneyTextBox ID="sPayoffStatementTotalDueAmt_2" runat="server" CssClass="mask" preset="money" width="80" ReadOnly="true"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:20px;">
                                Whole months
                            </td>
                            <td>
                                <asp:textbox id="sPayoffStatementInterestWholeMonths" runat="server" Width="55px" MaxLength="3" ReadOnly="true"></asp:textbox>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:20px;">
                                Odd days
                            </td>
                            <td>
                                <asp:textbox id="sPayoffStatementInterestOddDays" runat="server" Width="55px" MaxLength="3" ReadOnly="true"></asp:textbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Total interest due
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sPayoffStatementTotalInterestDueAmt" runat="server" CssClass="mask" preset="money" width="80" ReadOnly="true"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Recipient
                            </td>
                            <td>
                                <asp:DropDownList ID="sPayoffStatementRecipientT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                            </td>
                        </tr>
                        
                        
                        <tr>
                            <td>
                                Addressee
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="sPayoffStatementRecipientAddressee" class="RecipientAddress" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Attention
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="sPayoffStatementRecipientAttention" class="RecipientAddress" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Address
                            </td>
                            <td >
                                <asp:TextBox runat="server" ID="sPayoffStatementRecipientStreetAddress" class="RecipientAddress" Width="250px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:TextBox runat="server" ID="sPayoffStatementRecipientCity" class="RecipientAddress"></asp:TextBox>
                                <ml:StateDropDownList runat="server" ID="sPayoffStatementRecipientState" class="RecipientAddress"/>
                                <ml:ZipcodeTextBox runat="server" ID="sPayoffStatementRecipientZip" preset="longzipcode" class="RecipientAddress" Width="70px"></ml:ZipcodeTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Notes to recipient
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:TextBox ID="sPayoffStatementNotesToRecipient" runat="server" TextMode="MultiLine" width="100%" Rows="10"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
