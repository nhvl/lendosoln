﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanOfficerPricingPolicy.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.LoanOfficerPricingPolicy" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Loan Officer Pricing Policy</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        .FormTableHeader { padding-left: 1em; padding-top: 0.2em; padding-bottom: 0.2em; }
        .LabelContainer { margin-top: 0.5em; margin-left: 1em; width: 600px; }
        .TableContainer { border: 1px solid black; width: 600px; margin-top: 0.5em; margin-bottom: 1em; margin-left: 1em; }
        #CustomLOPricingPolicyTable td { padding: 0.2em 1em 0.2em 1.5em; }
        #sCustomPricingPolicySourceEmployeeNm A:link { color: White; }
        #sCustomPricingPolicySourceEmployeeNm A:hover { color: Red; }
        #sCustomPricingPolicySourceEmployeeNm A:visited { color: White; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="MainRightHeader">Loan Officer Pricing Policy</div>
        <div class="LabelContainer">
            <input type="button" NoHighlight="true" class="FieldLabel" style="width: 40%;" id="setPolicyFromLORecord" value="Set policy from loan officer record" />
            <input type="button" NoHighlight="true" class="FieldLabel" style="width: 35%;" id="setPolicyManually" value="Set compensation manually" />
            <input type="button" NoHighlight="true" class="FieldLabel" style="width: 20%;" id="clearPolicy" value="Clear policy" />
        </div>
        <div class="TableContainer">
            <div class="FormTableHeader">
                <ml:EncodedLabel ID="PolicySourceExplanation" runat="server" Text="No loan officer pricing policy set"></ml:EncodedLabel>
                <ml:PassThroughLabel  ID="sCustomPricingPolicySourceEmployeeNm" runat="server"></ml:PassThroughLabel>
                &nbsp<ml:EncodedLabel ID="sCustomPricingPolicyAuditD" runat="server"></ml:EncodedLabel>
            </div>
            <table runat="server" id="CustomLOPricingPolicyTable" cellspacing="0">
            </table>
        </div>
    </div>
    </form>
    <script type="text/javascript">
        jQuery(function($) {
            $.fn.readOnly = function() {
                if (arguments.length == 0 && this.length > 0) {
                    var e = this.first(), tagName = e.prop('tagName');
                    switch (tagName) {
                        case 'INPUT':
                            if (e.is('[type=checkbox]')) {
                                return e.prop('disabled');
                            }
                            else {
                                return e.prop('readonly');
                            }
                        case 'SELECT':
                            return e.prop('disabled');
                        case 'TEXTAREA':
                            return e.prop('readonly');
                        default:
                            return false;
                    }
                }
                var isReadOnly = arguments[0], readOnlyFn = formatReadonlyField, disableDDLFn = disableDDL;
                return this.each(function() {
                    var $this = $(this), tagName = $this.prop('tagName');
                    if (tagName == 'INPUT' || tagName == 'TEXTAREA') {
                        if (isReadOnly) {
                            readOnlyFn(this);
                        }
                        else {
                            $this.css('background-color', '');
                            this.tabIndex = this.oldTabIndex != null ? this.oldTabIndex : 0;
                        }

                        $this.prop('readonly', isReadOnly);
                        if ($this.is('[type=checkbox]')) {
                            $this.prop('disabled', isReadOnly);
                        }
                    }
                    else if (tagName == 'SELECT') {
                        disableDDLFn(this, isReadOnly);
                    }
                });
            }
            
            function setPolicyFromLORecord() {
                var args = { LoanId: ML.sLId };
                var result = gService.loanedit.call('SetPolicyFromLORecord', args);
                if (!result.error) {
                    populateForm(result.value);
                    updatePolicyDescription(result);
                } else {
                    var errMsg = 'Unable to set loan officer pricing policy. Please try again.';
                    if (null != result.UserMessage)
                        errMsg = result.UserMessage;
                    alert(errMsg);
                }
                $('.CustomPMLField').readOnly(true);
                $('#setPolicyManually').prop('disabled', false);
            }
            function setPolicyManually() {
                // Make service call to do this.
                var args = {
                    LoanId: ML.sLId,
                    sCustomPricingPolicyField1: $('#sCustomPricingPolicyField1').val(),
                    sCustomPricingPolicyField2: $('#sCustomPricingPolicyField2').val(),
                    sCustomPricingPolicyField3: $('#sCustomPricingPolicyField3').val(),
                    sCustomPricingPolicyField4: $('#sCustomPricingPolicyField4').val(),
                    sCustomPricingPolicyField5: $('#sCustomPricingPolicyField5').val()
                };
                var result = gService.loanedit.call('SetPolicyManually', args);
                if (!result.error) {
                    populateForm(result.value);
                    updatePolicyDescription(result);
                    $('.CustomPMLField').readOnly(false);
                } else {
                    var errMsg = 'Unable to set loan officer pricing policy. Please try again.';
                    if (null != result.UserMessage)
                        errMsg = result.UserMessage;
                    alert(errMsg);
                }

                $('#setPolicyManually').prop('disabled', true);
            }

            function updatePolicyDescription(result) {
                $('#PolicySourceExplanation').text(result.value['PolicySourceExplanation']);

                // Craft up the employee link as is done on the originator comp page.
                if (result.value['sCustomPricingPolicySourceEmployeeId']) {
                    // If the sCustomPricingPolicySourceEmployeeId is empty, then the plan is either manual or the user doesn't
                    //   have permission to access that employee's page
                    $('#sCustomPricingPolicySourceEmployeeNm').text("");
                    var editorLink;
                    var lqbSettings;
                    if (result.value['CustomPricingPolicySourceUserType'] == 'B') {
                        // Put in the link to the broker employee
                        editorLink = '/los/admin/EditEmployee.aspx?cmd=edit&employeeId=';
                        lqbSettings = '{width:1000, hideCloseButton: true}';
                    } else if (result.value['CustomPricingPolicySourceUserType'] == 'P') {
                        // Put in the link to the PML employee
                        editorLink = '/los/BrokerAdmin/EditPmlUser.aspx?cmd=edit&employeeId=';
                        lqbSettings = '{width:890, hideCloseButton: true}';
                    }
                    var link = '<a href="#" '
                       + 'onclick="showModal(\'' + editorLink + result.value['sCustomPricingPolicySourceEmployeeId'] + '\', null, null, null, null, '+lqbSettings+');" >'
                       + result.value['sCustomPricingPolicySourceEmployeeNm']
                       + '</a>';
                    $(link).appendTo('#sCustomPricingPolicySourceEmployeeNm');
                } else {
                    $('#sCustomPricingPolicySourceEmployeeNm').text(result.value['sCustomPricingPolicySourceEmployeeNm']);
                }

                $('#sCustomPricingPolicyAuditD').text(result.value['sCustomPricingPolicyAuditD']);
            }

            function clearPolicy() {
                var args = { LoanId: ML.sLId };
                var result = gService.loanedit.call('ClearPolicy', args);
                if (!result.error) {
                    populateForm(result.value);
                } else {
                    if (null != result.UserMessage) {
                        alert(result.UserMessage);
                    }
                }

                $('#PolicySourceExplanation').text('No loan officer pricing policy set');
                $('#sCustomPricingPolicySourceEmployeeNm').text('');
                $('#sCustomPricingPolicyAuditD').text('');

                $('#setPolicyManually').prop('disabled', false);
                $('.CustomPMLField').readOnly(true);
            }

            $('#setPolicyFromLORecord').on('click', setPolicyFromLORecord);
            $('#setPolicyManually').on('click', setPolicyManually);
            $('#clearPolicy').on('click', clearPolicy);
            
            $('.CustomPMLField').readOnly(true);
        });
        
    </script>
</body>
</html>
