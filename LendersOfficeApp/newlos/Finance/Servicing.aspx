﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Servicing.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.Servicing" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="sl" TagName="ServicingLineItem" Src="ServicingLineItem.ascx" %>
<%@ Register TagPrefix="uc" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Servicing</title>
    <link href="../../css/combobox.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        .BorderTop{border-top:thin groove;}
        .BorderLeft{border-left:thin groove;}
        .BorderRight{border-right:thin groove;}
        .BorderBottom{border-bottom:thin groove;}
	    .ButtonStyle { overflow: visible; width: auto; padding: 2px;}
        .moneyField { width: 70px; }
        .dateField { width: 60px; }
        .notesField { width: 450px; }
        .paymentStatementLink { padding-left: 3px; }
	    .descriptionField { width: 160px; }
	    .hidden { display: none; }
	    #Billing label span { display: inline-block; width: 160px; }
	    #Billing label span.narrow { width: 100px; }
	    #Billing label { display: block;}
	    #BillingDetails { position: relative; left: 10px;}
	    .payeeField { width: 180px; }
	    .row2Label { padding-right: 3px; }
	    .PaymentNumber { width: 110px; }
	    .DueDateHeader { width: 100px; padding-left: 27px; }
        .PaymentStatementLabel { width: 125px; }
        .ContactInfo div label { width: 100px; }
        #PaymentStatementServicer .wide { width: 226px; }
        .TableServicingInfo { width: 1020px; }
        #TransactionTable { width: 1395px; overflow: visible; }
        .width8px { width: 8px; }
    </style>
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript" >
        $(function($){
            $details = $('#BillingDetails');
            $('#sBillingMethodT').change(function(){
                if($(this).val() == 3 /*AutomaticDraft*/){
                    $details.removeClass('hidden');
                }
                else
                {
                    $details.addClass('hidden');
                }
            }).change();
            $('.mask.jq').each(function(){
                var length = +$(this).data('maxLength');
                //Creates a string composed of #length question marks.
                var mask = new Array(length + 1).join('?');

                this.maxLength = length;
                this.setAttribute('mask', mask);
                addEventHandler(this, "keyup", mask_keyup, false);
            });
        });

        var linkedAdjustmentReadonlyMessage = <%=AspxTools.JsString(LendersOffice.Common.ErrorMessages.LinkedServicingPaymentAmountsAreReadonly)%>;

        var TransactionTable = {

            currentIndex: 0,

            tableData: null,

            isReadOnly: false,

            tableName: 'TransactionTable',

            rowsPerItem: 2,

            deleteRowBtn: 'btn_delete',

            selectAllCheckbox: 'm_deleteAllCheckbox',

            deleteRow : function(input) {
                var row = input.parent().parent();
                row.next().remove();
                row.remove();
            },

            // called by instantiateTable
            // newRow is boolean
            __createAndAppendRow: function(index, data, newRow) {
                var oTBody, oTR, oTD, oInput, linkedPayment;
                oTBody = this.tableData;

                linkedPayment = !newRow && data && data.Id && AdjustmentLinkedPayments.indexOf(data.Id) !== -1;

                var name_cb = 'DeleteEntry' + index;
                oTR = DynamicTable.__createAndAppendElement(oTBody, 'tr', {
                    'ItemIndex' : index
                });
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});

                if (!linkedPayment) {
                    oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                        'id': name_cb,
                        'name': name_cb,
                        'type': 'checkbox',
                        'DeleteEntry': index,
                        'NotForEdit': 'true',
                        'disabled': linkedPayment,
                    });

                    addEventHandler(oInput, 'click', function() { DynamicTable.uncheckDeleteAllCheckbox(TransactionTable); }, false);
                }

                if (!newRow && data && data.Id) {
                    var name_transactionId = 'Id' + index;
                    DynamicTable.__createAndAppendElement(oTD, 'input', {
                        'id': name_transactionId,
                        'name': name_transactionId,
                        'type': 'hidden',
                        'value': data.Id
                    });
                }

                var name_lcu = 'LastColumnUpdated' + index;
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_lcu,
                    'name': name_lcu,
                    'type': 'hidden',
                    'value': '-1'
                });

                var name_transactionDate = 'TransactionD' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_transactionDate,
                    'name': name_transactionDate,
                    'type': 'text',
                    'value': newRow ? toSimpleDateStr(new Date()) : data.TransactionD_rep,
                    'preset': 'date',
                    'class' : 'mask',
                    'className': 'dateField'
                });
                oInput = DynamicTable.__createAndAppendElement(oTD, 'a', {
                    'href': '#'
                });
                oImg = DynamicTable.__createAndAppendElement(oInput, 'img', DynamicTable.calImg);
                $(oInput).on('click', function() {
                    return displayCalendar(name_transactionDate);
                });

                var name_des = 'ServicingTransactionT' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_des,
                    'name': name_des,
                    'value': newRow ? '' : data.ServicingTransactionT,
                    'type': 'text',
                    'readOnly': this.isReadOnly || linkedPayment,
                    'disabled': this.isReadOnly || linkedPayment,
                    'className': 'descriptionField'
                });

                var servicingTransactionT = oInput;
                if (!linkedPayment) {
                    addEventHandler(oInput, 'change', function(event) {
                        togglePaymentStatement(this);
                        setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SVCPMT_DESCCOL) %>);
                    }, false);
                }

                if(!this.isReadOnly && !linkedPayment) {
                    addEventHandler(oInput, 'keydown', function(event) {onComboboxKeypress(document.getElementById(name_des), event);}, false);
                    createCombobox(name_des, comboBoxOptions);
                }

                oInput = DynamicTable.__createAndAppendElement(oTD, 'img', {
                    'src': gVirtualRoot + '/images/dropdown_arrow.gif',
                    'className': 'combobox_img',
                    'align': 'absbottom'
                });
                if(!this.isReadOnly && !linkedPayment) {
                        addEventHandler(oInput, 'click', function() {onSelect(name_des);}, false);
                }

                var name_dueDate = 'DueD' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_dueDate,
                    'name': name_dueDate,
                    'type': 'text',
                    'value': newRow ? '' : data.DueD_rep,
                    'preset': 'date',
                    'class' : 'mask',
                    'className': 'dateField'
                });

                addEventHandler(oInput, 'change', function() {date_onblur();setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SVCPMT_DUEDCOL) %>);}, false);
                addEventHandler(oInput, 'blur', function() {date_onblur();setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SVCPMT_DUEDCOL) %>);}, false);
                oInput = DynamicTable.__createAndAppendElement(oTD, 'a', {
                    'href': '#'
                });
                oImg = DynamicTable.__createAndAppendElement(oInput, 'img', DynamicTable.calImg);
                $(oInput).on('click', function() {
                    return displayCalendar(name_dueDate);
                });
                oInput = DynamicTable.__createAndAppendElement(oTD, 'img', {
                    'alt': 'Required',
                    'src': gVirtualRoot + '/images/error_icon.gif',
                    'id': name_dueDate + 'R'
                });
                oInput.style.display = 'none';

                var name_dueAmt = 'DueAmt' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_dueAmt,
                    'name': name_dueAmt,
                    'value': newRow ? '' : data.DueAmt_rep,
                    'type': 'text',
                    'preset': 'money',
                    'className': 'moneyField',
                    'disabled': linkedPayment
                });

                if (linkedPayment) {
                    oInput.title = linkedAdjustmentReadonlyMessage;
                }
                else {
                    addEventHandler(oInput, 'change', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SVCPMT_DUEAMTCOL) %>);}, false);
                }

                var name_payRcvdLckd = 'PaymentReceivedDLckd'+index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_payRcvdLckd,
                    'name': name_payRcvdLckd,
                    'type': 'checkbox'
                });
                $(oInput).prop('checked', newRow ? false : data.PaymentReceivedDLckd);

                var name_payReceivedD = 'PaymentReceivedD' + index;
                $(oInput).change(
                    function(){
                        lockField(this, name_payReceivedD);
                        setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SCVPMT_PMTRCVDDCOL) %>);
                });

                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_payReceivedD,
                    'name': name_payReceivedD,
                    'type': 'text',
                    'value': newRow ? '' : (data.PaymentReceivedDLckd ? data.PaymentReceivedDOverride_rep : data.PaymentD_rep),
                    'preset': 'date',
                    'class' : 'mask',
                    'className': 'dateField'
                });
                addEventHandler(oInput, 'change', function() {date_onblur();setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SCVPMT_PMTRCVDDCOL) %>);}, false);
                addEventHandler(oInput, 'blur', function() {date_onblur();setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SCVPMT_PMTRCVDDCOL) %>);}, false);
                oInput = DynamicTable.__createAndAppendElement(oTD, 'a', {
                    'href': '#'
                });
                oImg = DynamicTable.__createAndAppendElement(oInput, 'img', DynamicTable.calImg);
                $(oInput).on('click', function() {
                    return displayCalendar(name_payReceivedD);
                });

                if(newRow || false == data.PaymentReceivedDLckd)
                {
                    lockField(document.getElementById('PaymentReceivedDLckd'+index), 'PaymentReceivedD'+index);
                }

                var name_payDate = 'PaymentD' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_payDate,
                    'name': name_payDate,
                    'type': 'text',
                    'value': newRow ? '' : data.PaymentD_rep,
                    'preset': 'date',
                    'class' : 'mask',
                    'className': 'dateField'
                });
                addEventHandler(oInput, 'change', function() {date_onblur();setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SVCPMT_PMTDCOL) %>);}, false);
                addEventHandler(oInput, 'blur', function() {date_onblur();setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SVCPMT_PMTDCOL) %>);}, false);
                oInput = DynamicTable.__createAndAppendElement(oTD, 'a', {
                    'href': '#'
                });
                oImg = DynamicTable.__createAndAppendElement(oInput, 'img', DynamicTable.calImg);
                $(oInput).on('click', function() {
                    return displayCalendar(name_payDate);
                });
                oInput = DynamicTable.__createAndAppendElement(oTD, 'img', {
                    'alt': 'Required',
                    'src': gVirtualRoot + '/images/error_icon.gif',
                    'id': name_payDate + 'R'
                });
                oInput.style.display = 'none';

                var name_payAmt = 'PaymentAmt' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_payAmt,
                    'name': name_payAmt,
                    'value': newRow ? '' : data.PaymentAmt_rep,
                    'type': 'text',
                    'preset': 'money',
                    'className': 'moneyField',
                    'disabled': linkedPayment
                });
                
                if (linkedPayment) {
                    oInput.title = linkedAdjustmentReadonlyMessage;
                }
                else {
                    addEventHandler(oInput, 'change', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SVCPMT_PAYMENTCOL) %>);}, false);
                }

                var name_principal = 'Principal' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_principal,
                    'name': name_principal,
                    'value': newRow ? '' : data.Principal_rep,
                    'type': 'text',
                    'preset': 'money',
                    'className': 'moneyField',
                    'disabled': linkedPayment
                });
                
                if (linkedPayment) {
                    oInput.title = linkedAdjustmentReadonlyMessage;
                }
                else {
                    addEventHandler(oInput, 'change', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SVCPMT_PRINCIPALCOL) %>);}, false);
                }

                var name_int = 'Interest' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_int,
                    'name': name_int,
                    'value': newRow ? '' : data.Interest_rep,
                    'type': 'text',
                    'preset': 'money',
                    'className': 'moneyField',
                    'disabled': linkedPayment
                });
                
                if (!linkedPayment) {
                    addEventHandler(oInput, 'change', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SCVPMT_INTERESTCOL) %>);}, false);
                }

                var name_esc = 'Escrow' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_esc,
                    'name': name_esc,
                    'value': newRow ? '' : data.Escrow_rep,
                    'type': 'text',
                    'preset': 'money',
                    'className': 'moneyField',
                    'disabled': linkedPayment
                });
                
                if (!linkedPayment) {
                    addEventHandler(oInput, 'change', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SCVPMT_ESCROWCOL) %>);}, false);
                }

                var name_ltFee = 'LateFee' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_ltFee,
                    'name': name_ltFee,
                    'value': newRow ? '' : data.LateFee_rep,
                    'type': 'text',
                    'preset': 'money',
                    'className': 'moneyField',
                    'disabled': linkedPayment
                });
                
                if (!linkedPayment) {
                    addEventHandler(oInput, 'change', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SCVPMT_LATEFEECOL) %>);}, false);
                }

                var name_other = 'Other' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_other,
                    'name': name_other,
                    'value': newRow ? '' : data.Other_rep,
                    'type': 'text',
                    'preset': 'money',
                    'className': 'moneyField',
                    'disabled': linkedPayment
                });
                
                if (!linkedPayment) {
                    addEventHandler(oInput, 'change', function() {setChange(name_lcu, <%= AspxTools.JsNumeric(DataAccess.CPageBase.SCVPMT_OTHERCOL) %>);}, false);
                }

                oTR = DynamicTable.__createAndAppendElement(oTBody, 'tr', {});
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {
                    'colSpan': '2',
                    'align': 'right'
                });
                DynamicTable.__createAndAppendElement(oTD, 'label', {
                    'innerText': 'Payee:',
                    'className': 'row2Label'
                });
                var name_payee = 'Payee' + index;
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_payee,
                    'name': name_payee,
                    'value': newRow ? '' : data.Payee,
                    'type': 'text',
                    'className': 'payeeField'
                });

                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {
                    'colSpan' : '6',
                    'align' : 'left'
                });
                DynamicTable.__createAndAppendElement(oTD, 'label', {
                    'innerText': 'Notes:',
                    'className': 'row2Label'
                });
                var name_notes = 'Notes' + index;
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_notes,
                    'name': name_notes,
                    'value': newRow ? '' : data.Notes,
                    'type': 'text',
                    'className': 'notesField'
                });

                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {
                    'colSpan' : '4',
                    'align' : 'left',
                    'className': 'paymentStatement'
                });

                DynamicTable.__createAndAppendElement(oTD, 'label', {
                    'innerText': 'Payment Statement:',
                    'className': 'row2Label'
                });

                var name_documentId = 'PaymentStatementDocumentId' + index;
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_documentId,
                    'name': name_documentId,
                    'value': newRow ? '00000000-0000-0000-0000-000000000000' : data.PaymentStatementDocumentId,
                    'type': 'hidden',
                });

                var hasDocument = !newRow && data.PaymentStatementDocumentId !== '' && data.PaymentStatementDocumentId !== '00000000-0000-0000-0000-000000000000';
                var generateViewLink = DynamicTable.__createAndAppendElement(oTD, 'a', {
                    'innerText': hasDocument ? 'view' : 'generate',
                    'class': 'paymentStatementLink generateView',
                    'href': hasDocument ? getLinkToViewDoc(data.PaymentStatementDocumentId) : '#'
                });

                function rowHasDocument($row) {
                    var documentId = $row.find('input[type="hidden"][id^="PaymentStatementDocumentId"]').val();
                    return documentId !== '' && documentId !== '00000000-0000-0000-0000-000000000000';
                }

                $(generateViewLink).click(function() {
                    var generateViewLink = this;
                    var $row = $(this).parent().parent();
                    var $documentId = $row.find('input[type="hidden"][id^="PaymentStatementDocumentId"]');
                    var index = Math.floor($row.index() / 2);

                    if(!rowHasDocument($row)) {
                        if (isDirty()) {
                            var msg = 'You must save before proceeding. Save now?';
                            if (confirm(msg)) {
                                saveMe(true);
                            } else {
                                return false;
                            }
                        }

                        var startDate = getPreviousMonthlyPaymentTransactionDate($row);
                        var endDate = getTransactionDate($row);
                        var periodQuery = '';

                        if (startDate && endDate) {
                            periodQuery =  '&start=' + toSimpleDateStr(startDate) + '&end=' + toSimpleDateStr(endDate);
                        } else if (startDate) {
                            periodQuery =  '&start=' + toSimpleDateStr(startDate);
                        } else if (endDate) {
                            periodQuery =  '&end=' + toSimpleDateStr(endDate);
                        }

                        LQBPopup.Show('GeneratePaymentStatement.aspx?loanid=' + ML.sLId + '&appId=' + ML.PrimaryApplicationId + '&index=' + index + periodQuery, {
                            width: 815,
                            height: 600,
                            hideCloseButton: true,
                            onReturn: function(results) {
                                if (results && results.documentId) {
                                    $documentId.val(results.documentId);
                                    generateViewLink.href = getLinkToViewDoc(results.documentId);
                                    generateViewLink.innerText = 'view';
                                    $(generateViewLink).siblings('.uploadClear').text('clear');
                                }
                            }
                        });

                        return false;
                    }

                    function getPreviousMonthlyPaymentTransactionDate($generatingRow) {
                        var $targetPayment, transactionDateStr;
                        // There are two rows per payment, so we want to skip over the other row
                        // associated with this payment and then iterate over all remaining rows.
                        $generatingRow.prev().prevAll().each(function(index, element) {
                            var $this = $(this);
                            var description = $this.find('input[type="text"][id^="ServicingTransactionT"]').val();
                            if (description && description.toUpperCase() === 'MONTHLY PAYMENT') {
                                $targetPayment = $this;
                                return false;
                            }
                        });

                        if ($targetPayment) {
                            var transactionDateStr = $targetPayment.find('input[type="text"][id^="TransactionD"]').val();
                            if (transactionDateStr && !isNaN(Date.parse(transactionDateStr))) {
                                return new Date(transactionDateStr);
                            }
                        } else {
                            var closingDateStr = $('#sDocMagicClosingD').val();
                            if (closingDateStr && !isNaN(Date.parse(closingDateStr))) {
                                return new Date(closingDateStr);
                            }
                        }

                        return null;
                    }

                    function getTransactionDate($generatingRow) {
                        var transactionDateStr = $row.prev().find('input[type="text"][id^="TransactionD"]').val();
                        if (transactionDateStr && !isNaN(Date.parse(transactionDateStr))) {
                            var transactionDate = new Date(transactionDateStr);
                            return new Date(transactionDate.getFullYear(), transactionDate.getMonth(), transactionDate.getDate() - 1);
                        }

                        return null;
                    }
                });

                var clearUploadLink = DynamicTable.__createAndAppendElement(oTD, 'a', {
                    'innerText': hasDocument ? 'clear' : 'upload',
                    'class': 'paymentStatementLink uploadClear',
                    'href': '#'
                });

                $(clearUploadLink).click(function() {
                    var $row = $(this).parent().parent();
                    var $documentId = $row.find('input[type="hidden"][id^="PaymentStatementDocumentId"]');
                    var clearUploadLink = this;
                    var generateViewLink = $(this).siblings('.generateView')[0];

                    if(rowHasDocument($row)) {
                        $documentId.val('00000000-0000-0000-0000-000000000000');
                        this.innerText = 'upload';
                        $(this).siblings('.generateView').text('generate');
                        $(this).siblings('.generateView').attr('href', '#');
                        updateDirtyBit();
                    } else {
                        LQBPopup.Show('UploadPaymentStatement.aspx?loanid=' + ML.sLId + '&appId=' + ML.PrimaryApplicationId, {
                            width: 600,
                            height: 210,
                            hideCloseButton: true,
                            onReturn: function(results) {
                                if (results && results.documentId) {
                                    $documentId.val(results.documentId);
                                    generateViewLink.href = getLinkToViewDoc(results.documentId);
                                    generateViewLink.innerText = 'view';
                                    clearUploadLink.innerText = 'clear';
                                    updateDirtyBit();
                                }
                            }
                        });
                    }

                    return false;
                });

                togglePaymentStatement(servicingTransactionT);

                function toSimpleDateStr(date) {
                    return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                }

                function getLinkToViewDoc(documentId) {
                    return <%= AspxTools.JsString(Tools.GetEDocsLink(ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx")))%> + '?docid=' + documentId;
                }
            },

            serialize: function(index) {
                 return {
                    RowNum: AdjustmentTable.__IE8NullFix(index),
                    Id: AdjustmentTable.__IE8NullFix($('#Id' + index).val()),
                    ServicingTransactionT: AdjustmentTable.__IE8NullFix($('#ServicingTransactionT' + index).val()),
                    DueD_rep: AdjustmentTable.__IE8NullFix($('#DueD' + index).val()),
                    DueAmt_rep: AdjustmentTable.__IE8NullFix($('#DueAmt' + index).val()),
                    PaymentD_rep: AdjustmentTable.__IE8NullFix($('#PaymentD' + index).val()),
                    PaymentReceivedDLckd: $('#PaymentReceivedDLckd' + index).prop('checked'),
                    PaymentReceivedDOverride_rep: AdjustmentTable.__IE8NullFix($('#PaymentReceivedD' + index).val()),
                    PaymentAmt_rep: AdjustmentTable.__IE8NullFix($('#PaymentAmt' + index).val()),
                    Principal_rep: AdjustmentTable.__IE8NullFix($('#Principal' + index).val()),
                    Interest_rep: AdjustmentTable.__IE8NullFix($('#Interest' + index).val()),
                    Escrow_rep: AdjustmentTable.__IE8NullFix($('#Escrow' + index).val()),
                    Other_rep: AdjustmentTable.__IE8NullFix($('#Other' + index).val()),
                    LateFee_rep: AdjustmentTable.__IE8NullFix($('#LateFee' + index).val()),
                    Notes: AdjustmentTable.__IE8NullFix($('#Notes' + index).val()),
                    LastColumnUpdated: AdjustmentTable.__IE8NullFix($('#LastColumnUpdated' + index).val()),
                    Payee: AdjustmentTable.__IE8NullFix($('#Payee' + index).val()),
                    PaymentStatementDocumentId: AdjustmentTable.__IE8NullFix($('#PaymentStatementDocumentId' + index).val()),
                    TransactionD_rep: AdjustmentTable.__IE8NullFix($('#TransactionD' + index).val())
                };
            }
        };

        function togglePaymentStatement(descriptionTextbox) {
            var $paymentStatementTd = $(descriptionTextbox).closest('tr').next().find('td.paymentStatement');
            $paymentStatementTd.find('label, a').toggle(descriptionTextbox.value.toUpperCase() === 'MONTHLY PAYMENT');
        }

        var g_bIsInit = false;
        function _init() {
          if (g_bIsInit == false) {
            TransactionTable.isReadOnly = document.getElementById("_ReadOnly").value === "True";
            DynamicTable.instantiateTable(TransactionTable, sServicingPayments);
            validatePayments();
            clearDirty();
            }
            for(var i = 0; i < sServicingPayments.length; i++)
            {
                var lockCheckbox = document.getElementById('PaymentReceivedDLckd'+i);

                if (lockCheckbox) {
                    lockField(lockCheckbox, 'PaymentReceivedD'+i);
                }
            }
            lockField(<%=AspxTools.JsGetElementById(sSchedDueD1Lckd)%>,          'sSchedDueD1');
            lockField(<%=AspxTools.JsGetElementById(sTaxTableRealEtxDueDLckd)%>, 'sTaxTableRealEtxDueD');
            lockField(<%=AspxTools.JsGetElementById(sTaxTableSchoolDueDLckd)%>,  'sTaxTableSchoolDueD');
            lockField(<%=AspxTools.JsGetElementById(sTaxTable1008DueDLckd)%>,    'sTaxTable1008DueD');
            lockField(<%=AspxTools.JsGetElementById(sTaxTable1009DueDLckd)%>,    'sTaxTable1009DueD');
            <% if (DisplayAdditionalTaxLines) { %>
                lockField(<%=AspxTools.JsGetElementById(sTaxTableU3DueDLckd)%>,      'sTaxTableU3DueD');
                lockField(<%=AspxTools.JsGetElementById(sTaxTableU4DueDLckd)%>,      'sTaxTableU4DueD');
            <% } %>

            disableDDL(document.getElementById('s1098FilingYear'), !document.getElementById("s1098FilingYearLckd").checked);
            g_bIsInit = true;
        }

        function addPayment() {
            var sServicingNextPmtDue = <%=AspxTools.JsGetElementById(sServicingNextPmtDue)%>.value;
            var sInvSchedDueD1 = <%=AspxTools.JsGetElementById(sInvSchedDueD1)%>.value;
            var sIsNextPaymentBefore1stPaymentDueInvestor = <%=AspxTools.JsGetElementById(sIsNextPaymentBefore1stPaymentDueInvestor)%>.value;

            if(sIsNextPaymentBefore1stPaymentDueInvestor !== "Y")
            {
                alert("Creation of new payment entry blocked: new payment date " + sServicingNextPmtDue
                + " is on or after the 1st Payment Due Investor date " + sInvSchedDueD1 + ".");
                return;
            }

            <%=AspxTools.JsGetElementById(addNextPayment)%>.value = 'true';
            addTransaction();
            update();
        }

        function addTransaction() {
            if(!validatePayments())
                return;

            DynamicTable.__createAndAppendRow(null, true, TransactionTable);
            updateDirtyBit();
            validatePayments();
        }

        function deleteTransaction() {
            updateDirtyBit();
            DynamicTable.deleteSelected(TransactionTable);
        }

        function getTrimmedValue(id) {
            return document.getElementById(id).value.replace(/^\s+|\s+$/g, '');
        }

        function validatePayments() {
            var sSchedDueD1R = document.getElementById('m_sSchedDueD1R');
            var addTransactionBtn = document.getElementById('btn_addTransaction');
            var addPaymentBtn = document.getElementById('btn_addPayment');
            var valid = true;

            if(TransactionTable.isReadOnly)
                return;

            $('tr', TransactionTable.tableData).each(function(i, item) {
                var index = item.getAttribute("itemIndex");
                if(index != null) {
                    var dueD = getTrimmedValue('DueD' + index);
                    var paymentD = getTrimmedValue('PaymentD' + index);
                    var dueDR = document.getElementById('DueD' + index + 'R');
                    var paymentDR = document.getElementById('PaymentD' + index + 'R');
                    if(dueD == '' && paymentD == '') {
                        valid = false;
                        dueDR.style.display = '';
                        paymentDR.style.display = '';
                    }
                    else {
                        dueDR.style.display = 'none';
                        paymentDR.style.display = 'none';
                    }
                }
            });

            if(getTrimmedValue('sSchedDueD1') == '') {
                sSchedDueD1R.style.display = '';
                valid = false;
            }
            else
                sSchedDueD1R.style.display = 'none';

            if (valid) {
                addTransactionBtn.disabled = false;
                addPaymentBtn.disabled = false;
            }
            else {
                addTransactionBtn.disabled = true;
                addPaymentBtn.disabled = true;
            }

            return valid;
        }

        function setChange(id, value) {
            document.getElementById(id).value = value;
            update();
        }

        function update() {
            if(!validatePayments() && <%=AspxTools.JsGetElementById(addNextPayment)%>.value == 'false')
                return;

            refreshCalculation();
        }
        function _postGetAllFormValues(args)
        {
            args.sTransactions = DynamicTable.serialize(TransactionTable);
            args.addNextPayment = <%=AspxTools.JsGetElementById(addNextPayment)%>.value;
        }
        function _postSaveMe(value)
        {
          _postServiceCallFunction(value);
        }
        function _postRefreshCalculation(value)
        {
          _postServiceCallFunction(value);
        }
        function _postServiceCallFunction(value) {
            <%=AspxTools.JsGetElementById(addNextPayment)%>.value = 'false';
            for(var attr in value){
              if(value.hasOwnProperty(attr)){
                var element = document.getElementById(attr);
                if(element && element.tagName == 'SPAN')
                  element.textContent = value[attr];
              }
            }
            validatePayments();

            $('input[type="text"][id^="ServicingTransactionT"]').each(function(index, element) {
                togglePaymentStatement(this);
            });
        }
        function addNote() {
            var txtNewNote = <%=AspxTools.JsGetElementById(txtNewNote)%>;
            var newNote = $.trim(txtNewNote.value);

            // Only add note if not blank
            if(newNote) {
                // Add note with date/time and name
                var now = new Date();
                var d = now.getDate();
                var month = now.getMonth() + 1;
                var y = now.getFullYear();
                var h = now.getHours() > 12 ? now.getHours() - 12 : (now.getHours() == 0 ? 12 : now.getHours());
                var m = now.getMinutes() < 10 ? "0" + now.getMinutes() : now.getMinutes();
                var ampm = now.getHours() >= 12 ? "PM" : "AM";
                var name = "<%=AspxTools.HtmlString(employeeDb.FullName) %>";

                var s = month + "/" + d + "/" + y + " " + h + ":" + m + " " + ampm + " " + name + " - " + newNote;

                var sServicingNotes = <%=AspxTools.JsGetElementById(sServicingNotes)%>;

                // Add new lines to string if there is already text in servicing notes.
                if(sServicingNotes.value) {
                    s += "\n\n";
                }

                sServicingNotes.value = s + sServicingNotes.value;
            }

            // Clear textbox
            txtNewNote.value = "";
        }

        function doAfterDateFormat(e)
        {
            if(e.id === "sSchedDueD1")
            {
                update();
            }
        }
    </script>
    <script type="text/javascript">
        //Opens modal dialog for choosing tax payee
        //Gets return from modal dialog and sets field specified by "fieldId"
        function pickPayee(fieldId) {
            showModal('/newlos/Finance/ServicingPayeeDlg.aspx' +
            '?state=' + document.getElementById("m_state").value,
             null, 'dialogWidth:400px;dialogHeight:450px;center:yes;resizable:yes;scroll:no;status=no;help=no;', null, function(dialogArguments){
                 var payee = dialogArguments.returnValue;
                 if (payee && payee.PayeeName) {
                     document.getElementById("sTaxTable" + fieldId + "PayeeNm").innerText = payee.PayeeName + " | ";
                     document.getElementById("sTaxTable" + fieldId + "PayeeId").value = payee.PayeeId;
                     document.getElementById("sTaxTable" + fieldId + "PayeeNmField").value = payee.PayeeName;
                     updateDirtyBit();
                 }
             },{ hideCloseButton: true });
        }
    </script>
    <form id="Servicing" runat="server" onreset="return false;">
	    <asp:HiddenField ID="addNextPayment" runat="server" Visible="true" />
	    <asp:HiddenField ID="sInvSchedDueD1" runat="server" Visible="true" />
	    <asp:HiddenField ID="sIsNextPaymentBefore1stPaymentDueInvestor" runat="server" Visible="true" />
	    <asp:HiddenField ID="sDocMagicClosingD" runat="server" Visible="true" />
        <table cellSpacing="0" cellPadding="0">
			<tr>
				<td class="MainRightHeader" noWrap>
				    Servicing
				</td>
			</tr>
			<tr>
				<td style="padding:4px">
                    <table cellpadding="4" cellspacing="0" class="FieldLabel TableServicingInfo">
                        <tbody>
                            <tr>
                                <td colspan="6" style="vertical-align:top;" rowspan="7" style="background-color:Transparent;color:Black;">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="FieldLabel" nowrap>
                                                Property Address
                                            </td>
                                            <td nowrap>
                                                <asp:TextBox ID="sSpAddr" Width="359px" MaxLength="60" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap>
                                            </td>
                                            <td nowrap>
                                                <asp:TextBox ID="sSpCity" MaxLength="72" runat="server" Width="258px"></asp:TextBox><ml:StateDropDownList ID="sSpState" runat="server" IncludeTerritories="false"></ml:StateDropDownList>
                                                <ml:ZipcodeTextBox ID="sSpZip" Width="50" runat="server" preset="zipcode"></ml:ZipcodeTextBox>
                                            </td>
                                        </tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr>
                                            <td class="FieldLabel" nowrap>
                                            Current ARM Index&nbsp;
                                            </td>
                                            <td nowrap>
                                                 <ml:PercentTextBox runat="server" ID="sServicingCurrentArmIndexR"></ml:PercentTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" colspan="2" nowrap>
                                                Type of late charge that can be assessed
                                                <asp:DropDownList runat="server" ID="sServicingCurrentLateChargeT">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="Billing" runat="server">
                                        <label><span class="narrow"> Billing Method </span> <asp:DropDownList runat="server" id="sBillingMethodT"></asp:DropDownList></label>
                                        <div id="BillingDetails" class="hidden">
                                            <label><span> Account location </span> <asp:DropDownList runat="server" id="sBillingAccountLocationT"></asp:DropDownList></label>
                                            <label><span> ABA number </span> <input type="text" runat="server" id="sBillingABANum" class="mask jq" data-max-length="9"/></label>
                                            <label><span> Account number </span> <input type="text" runat="server" id="sBillingAccountNum" class="mask jq" data-max-length="50" /></label>
                                            <label><span> Name on account </span> <input type="text" runat="server" id="sBillingNameOnAccount" data-max-length="50"/></label>
                                            <label><span> Account type </span> <asp:DropDownList runat="server" id="sBillingAccountT"></asp:DropDownList></label>
                                            <label><span> Additional principal amount </span> <ml:MoneyTextBox ID="sBillingAdditionalAmt" runat="server" Width="80" /> </label>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    Principal
                                </td>
                                <td>
                                    Interest
                                </td>
                                <td>
                                    Escrow
                                </td>
                                <td>
                                    Late Fee
                                </td>
                                <td>
                                    Other
                                </td>

                            </tr>
                            <sl:ServicingLineItem ID="sServicingClosingFunds" Text="Funds at closing" Class="GridItem" runat="server" />
                            <sl:ServicingLineItem ID="sServicingCollectedFunds" Text="Collected" Class="GridAlternatingItem" runat="server" />
                            <sl:ServicingLineItem ID="sServicingDisbursedFunds" Text="Disbursed" Class="GridItem" runat="server" />
                            <sl:ServicingLineItem ID="sServicingCurrentTotalFunds" Text="Current total" Class="GridAlternatingItem" runat="server" />
                            <sl:ServicingLineItem ID="sServicingDueFunds" Text="Due" Class="GridItem" runat="server" />
                            <sl:ServicingLineItem ID="sServicingTotalDueFunds" Text="Total w/ pmts due" Class="GridAlternatingItem" runat="server" />
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <table>
                                        <tr>
                                            <td class="FieldLabel PaymentStatementLabel">
                                                Payment Statement Delinquency Notice
                                            </td>
                                            <td>
                                                <textarea id="sPaymentStatementDelinquencyNotice" runat="server" rows="4" cols="75" maxlength="500"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel PaymentStatementLabel">
                                                Payment Statement Suspense Account Notice
                                            </td>
                                            <td>
                                                <textarea id="sPaymentStatementSuspenseAccountNotice" runat="server" rows="4" cols="75" maxlength="500"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel PaymentStatementLabel">
                                                Payment Statement Other Notices
                                            </td>
                                            <td>
                                                <textarea id="sPaymentStatementOtherNotices" runat="server" rows="4" cols="75" maxlength="500"></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td colspan="6">
                                    <table id="PaymentStatementServicer">
                                        <tr>
                                            <td class="FieldLabel">Servicing Contact Info</td>
                                            <td>
                                                <uc:cfm runat="server" ID="CFM"
                                                    CompanyNameField="PaymentStatementServicerCompanyNm"
                                                    StreetAddressField="PaymentStatementServicerStreetAddr"
                                                    CityField="PaymentStatementServicerCity"
                                                    StateField="PaymentStatementServicerState"
                                                    ZipField="PaymentStatementServicerZip"
                                                    CompanyPhoneField="PaymentStatementServicerPhoneOfCompany"
                                                    EmailField="PaymentStatementServicerEmailAddr" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">Company Name</td>
                                            <td><asp:TextBox runat="server" ID="PaymentStatementServicerCompanyNm" class="wide"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">Address</td>
                                            <td><asp:TextBox runat="server" ID="PaymentStatementServicerStreetAddr" class="wide"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:TextBox runat="server" ID="PaymentStatementServicerCity"></asp:TextBox>
                                                <ml:StateDropDownList runat="server" ID="PaymentStatementServicerState" />
                                                <ml:ZipcodeTextBox runat="server" ID="PaymentStatementServicerZip"></ml:ZipcodeTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">Company Phone</td>
                                            <td><asp:Textbox runat="server" ID="PaymentStatementServicerPhoneOfCompany" preset="phone"></asp:Textbox></td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel">Email</td>
                                            <td><asp:TextBox runat="server" ID="PaymentStatementServicerEmailAddr" class="wide"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
			    </td>
			</tr>
            <tr>
				<td style="padding:4px">
                    <table id="TransactionTable" cellpadding="4" cellspacing="0" class="FieldLabel">
                        <thead>
                            <tr class="GridHeader">
                                <td>
                                    <input type="checkbox" id="m_deleteAllCheckbox" onclick="DynamicTable.setDeleteCheckboxes(TransactionTable);" NotForEdit />
                                </td>
                                <td>
                                    Transaction Date
                                </td>
                                <td style="width:180px">
                                    Transaction Type
                                </td>
                                <td style="width:120px">
                                    Due Date
                                </td>
                                <td class="moneyField">
                                    Due Amount
                                </td>
                                <td style="width:124px;text-align:center">
                                    Payment Received Date
                                </td>
                                <td style="width:120px;text-align:center">
                                    Payment Credited Date
                                </td>
                                <td class="moneyField">
                                    Payment Amount
                                </td>
                                <td class="moneyField">
                                    Principal
                                </td>
                                <td class="moneyField">
                                    Interest
                                </td>
                                <td class="moneyField">
                                    Escrow
                                </td>
                                <td class="moneyField">
                                    Late Fee
                                </td>
                                <td class="moneyField">
                                    Other
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </td>
            </tr>
			<tr style="padding:4px">
			    <td>
					<input type="button" class="ButtonStyle" id="btn_addPayment" value="Add next payment" onclick="addPayment();" NoHighlight />
					<input type="button" class="ButtonStyle" id="btn_addTransaction" value="Add transaction" onclick="addTransaction();" NoHighlight />
					<input type="button" class="ButtonStyle" id="btn_delete" value="Delete" onclick="deleteTransaction();" disabled="disabled" NoHighlight />
			    </td>
			</tr>
			<tr>
			    <td style="padding:20px 4px 4px 4px">
			        <table class="FieldLabel" cellspacing="0">
			            <tr>
			                <td colspan="5">
			                    Payment basis
			                </td>
			                <td colspan="7">
			                    Accrual basis
			                </td>
                            <td>
                                1098 Info
                            </td>
			            </tr>
			            <tr>
			                <td class="BorderTop BorderLeft" noWrap>
			                    <ml:EncodedLabel Text="Principal basis" ID="Label1" AssociatedControlID="sServicingPrincipalBasis" runat="server"></ml:EncodedLabel>
			                </td>
			                <td class="BorderTop">
						        <ml:MoneyTextBox ID="sServicingPrincipalBasis" ReadOnly="true" runat="server" Width="80" />
			                </td>
			                <td class="BorderTop" noWrap>
			                    <ml:EncodedLabel Text="Escrow pmt" ID="Label2" AssociatedControlID="sServicingEscrowPmt" runat="server"></ml:EncodedLabel>
			                </td>
			                <td class="BorderTop BorderRight">
						        <ml:MoneyTextBox ID="sServicingEscrowPmt" ReadOnly="true" runat="server" Width="60" />
			                </td>
			                <td style="width:8px"></td>
			                <td class="BorderTop BorderLeft" noWrap>
			                    <ml:EncodedLabel Text="Unpaid principal balance" ID="Label3" AssociatedControlID="sServicingUnpaidPrincipalBalance" runat="server"></ml:EncodedLabel>
			                </td>
			                <td class="BorderTop BorderRight">
						        <ml:MoneyTextBox ID="sServicingUnpaidPrincipalBalance" ReadOnly="true" runat="server" Width="80" />
			                </td>
			                <td style="width:8px"></td>
			                <td noWrap>
			                    <ml:EncodedLabel Text="1st payment due" ID="Label12" AssociatedControlID="sSchedDueD1" runat="server"></ml:EncodedLabel>
                                <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" onclick="lockField(this, 'sSchedDueD1'); update();"/>
			                </td>
			                <td>
			                    <ml:datetextbox ID="sSchedDueD1" runat="server" preset="date" CssClass="mask" Width="60" onblur="date_onblur(null, this);" onchange="date_onblur(null, this);"></ml:datetextbox>
			                </td>
			                <td>
					            <img id="m_sSchedDueD1R" src="../../images/error_icon.gif" alt="Required" style="display:none" />
			                </td>
                            <td class="width8px">
                            </td>
                            <td class="BorderLeft BorderTop">
                                Filing Year
                            </td>
                            <td class="BorderTop width8px">
                            <td class="BorderTop">
                                <asp:DropDownList runat="server" ID="s1098FilingYear"></asp:DropDownList>
                            </td>
                            <td class="width8px BorderTop">
                            </td>
                            <td class="BorderTop">
                                <asp:CheckBox runat="server" ID="s1098FilingYearLckd" onclick="refreshCalculation();"/>
                            </td>
                            <td class="BorderTop BorderRight">
                                Locked
                            </td>
			            </tr>
			            <tr>
			                <td class="BorderLeft">
			                    <ml:EncodedLabel Text="Rate" ID="Label4" AssociatedControlID="sNoteIR" runat="server"></ml:EncodedLabel>
			                </td>
			                <td>
						        <ml:PercentTextBox ID="sNoteIR" ReadOnly="true" runat="server" Width="50" />
						    </td>
			                <td>
			                    <ml:EncodedLabel Text="Other pmts" ID="Label5" AssociatedControlID="sServicingOtherPmts" runat="server"></ml:EncodedLabel>
			                </td>
			                <td class="BorderRight">
						        <ml:MoneyTextBox ID="sServicingOtherPmts" onchange="update();" runat="server" Width="60" />
			                </td>
			                <td></td>
			                <td class="BorderLeft">
			                    <ml:EncodedLabel Text="Rate" ID="Label6" AssociatedControlID="sNoteIR2" runat="server"></ml:EncodedLabel>
			                </td>
			                <td class="BorderRight">
						        <ml:PercentTextBox ID="sNoteIR2" ReadOnly="true" runat="server" Width="50" />
			                </td>
			                <td></td>
			                <td align="right">
			                    <ml:EncodedLabel Text="Current payment due" ID="Label13" AssociatedControlID="sServicingCurrentPmtDueD" runat="server"></ml:EncodedLabel>
			                </td>
			                <td>
			                    <asp:TextBox ID="sServicingCurrentPmtDueD" preset="date" CssClass="mask" ReadOnly="true" Width="60" runat="server"></asp:TextBox>
			                </td>
                            <td></td>
                            <td class="width8px">
                            </td>
                            <td class="BorderLeft">
                                Current Year Interest and Points
                            </td>
                            <td class="width8px">
                            <td colspan="4" class="BorderRight">
                                <ml:MoneyTextBox runat="server" ID="s1098CurrentYearInterestAndPoints" ReadOnly="true"></ml:MoneyTextBox>
                            </td>
			            </tr>
			            <tr>
			                <td class="BorderLeft">
			                    <ml:EncodedLabel Text="Term" ID="Label7" AssociatedControlID="sTerm" runat="server"></ml:EncodedLabel>
			                </td>
			                <td>
						        <asp:TextBox ID="sTerm" ReadOnly="true" Width="50" runat="server"></asp:TextBox>
			                </td>
			                <td colspan="2" class="BorderRight">
			                    &nbsp;
			                </td>
			                <td></td>
			                <td class="BorderLeft">
			                    <ml:EncodedLabel Text="Original term" ID="Label9" AssociatedControlID="sTerm2" runat="server"></ml:EncodedLabel>
			                </td>
			                <td class="BorderRight">
						        <asp:TextBox ID="sTerm2" ReadOnly="true" Width="50" runat="server"></asp:TextBox>
			                </td>
			                <td></td>
			                     <td align="right">
			                    <ml:EncodedLabel Text="Next payment" ID="Label14" AssociatedControlID="sServicingNextPmtDue" runat="server"></ml:EncodedLabel>
			                </td>
			                <td>
			                    <asp:TextBox ID="sServicingNextPmtDue" preset="date" CssClass="mask" ReadOnly="true" Width="60" runat="server"></asp:TextBox>
			                </td>
                            <td></td>
                            <td class="width8px">
                            </td>
                            <td class="BorderLeft BorderBottom">
                                Previous Year Interest and Points
                            </td>
                            <td class="width8px BorderBottom">
                            <td class="BorderBottom BorderRight" colspan="4">
                                <ml:MoneyTextBox runat="server" ID="s1098PreviousYearInterestAndPoints" ReadOnly="true"></ml:MoneyTextBox>
                            </td>
			            </tr>
			            <tr>
			                <td class="BorderLeft BorderBottom">
			                    <ml:EncodedLabel Text="Payment type" ID="Label8" AssociatedControlID="sServicingPmtT" runat="server"></ml:EncodedLabel>
			                </td>
			                <td class="BorderBottom">
						        <asp:DropDownList ID="sServicingPmtT" Enabled="false" runat="server"></asp:DropDownList>
						    </td>
			                <td colspan="2" class="BorderBottom BorderRight">
			                    &nbsp;
			                </td>
			                <td></td>
			                <td colspan="2" class="BorderBottom BorderLeft BorderRight">
			                    &nbsp;
						    </td>
			            </tr>
			            <tr>
			                <td colspan="4" noWrap>
			                    <ml:EncodedLabel Text="Next payment amount" ID="Label10" AssociatedControlID="sServicingNextPmtAmt" runat="server"></ml:EncodedLabel>
						        <ml:MoneyTextBox ID="sServicingNextPmtAmt" ReadOnly="true" runat="server" Width="80" />
			                </td>
			                <td></td>
			                <td>
			                    <ml:EncodedLabel Text="Interest due" ID="Label11" AssociatedControlID="sServicingInterestDue" runat="server"></ml:EncodedLabel>
			                </td>
			                <td>
						        <ml:MoneyTextBox ID="sServicingInterestDue" ReadOnly="true" runat="server" Width="80" />
			                </td>
			            </tr>
			        </table>
			    </td>
			</tr>
			<tr>
			    <td>
			        &nbsp;
			    </td>
			</tr>
			<tr>
			    <td colspan="11">
			    <table cellpadding="4" cellspacing="0" class="FieldLabel" width="1050px;">
			        <%--Header--%>
			        <tr class="GridHeader">
			            <td>Tax</td>
			            <td>Type</td>
			            <td>Parcel Number</td>
			            <td>Paid By</td>
			            <td>Pay To</td>
			            <td class="PaymentNumber">Payment Number of Most Recent Pmt</td>
			            <td class="DueDateHeader">Due Date</td>
			            <td>Paid Date</td>
			        </tr>

			        <%--Entry 1--%>
			        <tr>
			            <td>Real Estate Taxes</td>
			            <td><asp:DropDownList ID="sTaxTableRealETxT" runat="server"></asp:DropDownList></td>
			            <td><asp:TextBox ID="sTaxTableRealETxParcelNum" runat="server" MaxLength="50"></asp:TextBox></td>
			            <td><asp:DropDownList ID="sTaxTableRealETxPaidByT" runat="server"></asp:DropDownList></td>
			            <td>
			                <ml:EncodedLabel ID="sTaxTableRealEtxPayeeNm" runat="server"></ml:EncodedLabel>
			                [ <a href="javascript:void(0);" onclick="pickPayee('RealEtx');">pick a payee</a> ]
			                <asp:HiddenField ID="sTaxTableRealEtxPayeeId" runat="server" />
			                <asp:HiddenField ID="sTaxTableRealEtxPayeeNmField" runat="server" />
			            </td>
			            <td><asp:TextBox ID="sTaxTableRealEtxNumOfPmt" runat="server" class="PaymentNumber"></asp:TextBox></td>
			            <td>
			                <asp:CheckBox runat="server" ID="sTaxTableRealEtxDueDLckd" onclick="lockField(this, 'sTaxTableRealEtxDueD'); update();" />
			                <ml:DateTextBox runat="server" ID="sTaxTableRealEtxDueD"></ml:DateTextBox>
		                </td>
		                <td><ml:DateTextBox runat="server" ID="sTaxTableRealEtxPaidD"></ml:DateTextBox></td>
			        </tr>

			        <%--Entry 2--%>
			        <tr>
			            <td>School Taxes</td>
			            <td><asp:DropDownList ID="sTaxTableSchoolT" runat="server"></asp:DropDownList></td>
			            <td><asp:TextBox ID="sTaxTableSchoolParcelNum" runat="server" MaxLength="50"></asp:TextBox></td>
			            <td><asp:DropDownList ID="sTaxTableSchoolPaidByT" runat="server"></asp:DropDownList></td>
			            <td>
			                <ml:EncodedLabel ID="sTaxTableSchoolPayeeNm" runat="server"></ml:EncodedLabel>
			                [ <a href="javascript:void(0);" onclick="pickPayee('School');">pick a payee</a> ]
			                <asp:HiddenField ID="sTaxTableSchoolPayeeId" runat="server" />
			                <asp:HiddenField ID="sTaxTableSchoolPayeeNmField" runat="server" />
			            </td>
			            <td><asp:TextBox ID="sTaxTableSchoolNumOfPmt" runat="server" class="PaymentNumber"></asp:TextBox></td>
			            <td>
			                <asp:CheckBox runat="server" ID="sTaxTableSchoolDueDLckd" onclick="lockField(this, 'sTaxTableSchoolDueD'); update();" />
			                <ml:DateTextBox runat="server" ID="sTaxTableSchoolDueD"></ml:DateTextBox>
		                </td>
		                <td><ml:DateTextBox runat="server" ID="sTaxTableSchoolPaidD"></ml:DateTextBox></td>
			        </tr>

			        <%--Entry 3--%>
			        <tr>
			            <td><asp:DropDownList ID="sTaxTable1008DescT" runat="server"></asp:DropDownList></td>
			            <td><asp:DropDownList ID="sTaxTable1008T" runat="server"></asp:DropDownList></td>
			            <td><asp:TextBox ID="sTaxTable1008ParcelNum" runat="server" MaxLength="50"></asp:TextBox></td>
			            <td><asp:DropDownList ID="sTaxTable1008PaidByT" runat="server"></asp:DropDownList></td>
			            <td>
			                <ml:EncodedLabel ID="sTaxTable1008PayeeNm" runat="server"></ml:EncodedLabel>
			                [ <a href="javascript:void(0);" onclick="pickPayee('1008');">pick a payee</a> ]
			                <asp:HiddenField ID="sTaxTable1008PayeeId" runat="server" />
			                <asp:HiddenField ID="sTaxTable1008PayeeNmField" runat="server" />
			            </td>
			            <td><asp:TextBox ID="sTaxTable1008NumOfPmt" runat="server" class="PaymentNumber"></asp:TextBox></td>
			            <td>
			                <asp:CheckBox runat="server" ID="sTaxTable1008DueDLckd" onclick="lockField(this, 'sTaxTable1008DueD'); update();" />
			                <ml:DateTextBox runat="server" ID="sTaxTable1008DueD"></ml:DateTextBox>
		                </td>
		                <td><ml:DateTextBox runat="server" ID="sTaxTable1008PaidD"></ml:DateTextBox></td>
			        </tr>

			        <%--Entry 4--%>
                    <tr>
			            <td><asp:DropDownList ID="sTaxTable1009DescT" runat="server"></asp:DropDownList></td>
			            <td><asp:DropDownList ID="sTaxTable1009T" runat="server"></asp:DropDownList></td>
			            <td><asp:TextBox ID="sTaxTable1009ParcelNum" runat="server" MaxLength="50"></asp:TextBox></td>
			            <td><asp:DropDownList ID="sTaxTable1009PaidByT" runat="server"></asp:DropDownList></td>
			            <td>
			                <ml:EncodedLabel ID="sTaxTable1009PayeeNm" runat="server"></ml:EncodedLabel>
			                [ <a href="javascript:void(0);" onclick="pickPayee('1009');">pick a payee</a> ]
			                <asp:HiddenField ID="sTaxTable1009PayeeId" runat="server" />
			                <asp:HiddenField ID="sTaxTable1009PayeeNmField" runat="server" />
			            </td>
			            <td><asp:TextBox ID="sTaxTable1009NumOfPmt" runat="server" class="PaymentNumber"></asp:TextBox></td>
			            <td>
			                <asp:CheckBox runat="server" ID="sTaxTable1009DueDLckd" onclick="lockField(this, 'sTaxTable1009DueD'); update();" />
			                <ml:DateTextBox runat="server" ID="sTaxTable1009DueD"></ml:DateTextBox>
		                </td>
		                <td><ml:DateTextBox runat="server" ID="sTaxTable1009PaidD"></ml:DateTextBox></td>
			        </tr>

			        <asp:PlaceHolder runat="server" ID="phAdditionalTaxLines">
    			        <%--Entry 5--%>
                        <tr>
    			            <td><asp:DropDownList ID="sTaxTableU3DescT" runat="server"></asp:DropDownList></td>
    			            <td><asp:DropDownList ID="sTaxTableU3T" runat="server"></asp:DropDownList></td>
    			            <td><asp:TextBox ID="sTaxTableU3ParcelNum" runat="server" MaxLength="50"></asp:TextBox></td>
    			            <td><asp:DropDownList ID="sTaxTableU3PaidByT" runat="server"></asp:DropDownList></td>
    			            <td>
    			                <ml:EncodedLabel ID="sTaxTableU3PayeeNm" runat="server"></ml:EncodedLabel>
    			                [ <a href="javascript:void(0);" onclick="pickPayee('U3');">pick a payee</a> ]
    			                <asp:HiddenField ID="sTaxTableU3PayeeId" runat="server" />
    			                <asp:HiddenField ID="sTaxTableU3PayeeNmField" runat="server" />
    			            </td>
    			            <td><asp:TextBox ID="sTaxTableU3NumOfPmt" runat="server" class="PaymentNumber"></asp:TextBox></td>
    			            <td>
    			                <asp:CheckBox runat="server" ID="sTaxTableU3DueDLckd" onclick="lockField(this, 'sTaxTableU3DueD'); update();" />
    			                <ml:DateTextBox runat="server" ID="sTaxTableU3DueD"></ml:DateTextBox>
    		                </td>
    		                <td><ml:DateTextBox runat="server" ID="sTaxTableU3PaidD"></ml:DateTextBox></td>
    			        </tr>

    			        <%--Entry 6--%>
                        <tr>
    			            <td><asp:DropDownList ID="sTaxTableU4DescT" runat="server"></asp:DropDownList></td>
    			            <td><asp:DropDownList ID="sTaxTableU4T" runat="server"></asp:DropDownList></td>
    			            <td><asp:TextBox ID="sTaxTableU4ParcelNum" runat="server" MaxLength="50"></asp:TextBox></td>
    			            <td><asp:DropDownList ID="sTaxTableU4PaidByT" runat="server"></asp:DropDownList></td>
    			            <td>
    			                <ml:EncodedLabel ID="sTaxTableU4PayeeNm" runat="server"></ml:EncodedLabel>
    			                [ <a href="javascript:void(0);" onclick="pickPayee('U4');">pick a payee</a> ]
    			                <asp:HiddenField ID="sTaxTableU4PayeeId" runat="server" />
    			                <asp:HiddenField ID="sTaxTableU4PayeeNmField" runat="server" />
    			            </td>
    			            <td><asp:TextBox ID="sTaxTableU4NumOfPmt" runat="server" class="PaymentNumber"></asp:TextBox></td>
    			            <td>
    			                <asp:CheckBox runat="server" ID="sTaxTableU4DueDLckd" onclick="lockField(this, 'sTaxTableU4DueD'); update();" />
    			                <ml:DateTextBox runat="server" ID="sTaxTableU4DueD"></ml:DateTextBox>
    		                </td>
    		                <td><ml:DateTextBox runat="server" ID="sTaxTableU4PaidD"></ml:DateTextBox></td>
    			        </tr>
			        </asp:PlaceHolder>


			    </table>
			    </td>
			</tr>
			<tr>
			    <td>
			        &nbsp;
			    </td>
			</tr>
			<tr>
			    <td colspan="11">
			    <table cellpadding="4" cellspacing="0" class="FieldLabel" width="1050px;">
			        <%--Header--%>
			        <tr class="GridHeader">
			            <td>Notes</td>
			        </tr>
			        <tr>
		                <td>
		                    <asp:TextBox ID="txtNewNote" runat="server" Width="425px" MaxLength="1000" />
		                    <input type="button" id="AddBtn" value="Add note" onclick="addNote();" />
			            </td>
			        </tr>
			        <tr>
			            <td>
			                <asp:TextBox ID="sServicingNotes" runat="server" Width="500px" TextMode="MultiLine" Rows="10" ReadOnly="true" />
			            </td>
			        </tr>
		        </table>
		        </td>
			</tr>
		</table>
    </form>
</body>
</html>
