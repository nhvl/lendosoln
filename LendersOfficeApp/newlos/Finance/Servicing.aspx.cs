﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class Servicing : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite, Permission.AllowCloserRead };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantRead, Permission.AllowCloserRead };
            }
        }

        protected bool DisplayAdditionalTaxLines
        {
            get
            {
                return Broker.EnableAdditionalSection1000CustomFees;
            }
        }

        private EmployeeDB _employeeDb = null;
        protected EmployeeDB employeeDb
        {
            get
            {
                if( _employeeDb == null)
                    _employeeDb = EmployeeDB.RetrieveById(BrokerID, EmployeeID);

                return _employeeDb;
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Servicing));
            dataLoan.InitLoad();

            this.RegisterJsGlobalVariables("PrimaryApplicationId", dataLoan.GetAppData(0).aAppId);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer(
                "AdjustmentLinkedPayments",
                dataLoan.sAdjustmentList.Where(adj => adj.LinkedServicingPaymentId.HasValue).Select(adj => adj.LinkedServicingPaymentId.Value));

            Tools.Bind_Generic(sBillingMethodT, dataLoan.sBillingMethodT);
            Tools.Bind_Generic(sBillingAccountLocationT, dataLoan.sBillingAccountLocationT);
            Tools.Bind_Generic(sBillingAccountT, dataLoan.sBillingAccountT);


            sBillingABANum.Value = dataLoan.sBillingABANum.Value;
            sBillingAccountNum.Value = dataLoan.sBillingAccountNum.Value;
            sBillingNameOnAccount.Value = dataLoan.sBillingNameOnAccount;
            sBillingAdditionalAmt.Text = dataLoan.sBillingAdditionalAmt_rep;

            this.sPaymentStatementDelinquencyNotice.Value = dataLoan.sPaymentStatementDelinquencyNotice;
            this.sPaymentStatementSuspenseAccountNotice.Value = dataLoan.sPaymentStatementSuspenseAccountNotice;
            this.sPaymentStatementOtherNotices.Value = dataLoan.sPaymentStatementOtherNotices;
            var preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.PaymentStatementServicer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            this.PaymentStatementServicerCompanyNm.Text = preparer.CompanyName;
            this.PaymentStatementServicerStreetAddr.Text = preparer.StreetAddr;
            this.PaymentStatementServicerCity.Text = preparer.City;
            this.PaymentStatementServicerState.Value = preparer.State;
            this.PaymentStatementServicerZip.Text = preparer.Zip;
            this.PaymentStatementServicerPhoneOfCompany.Text = preparer.PhoneOfCompany;
            this.PaymentStatementServicerEmailAddr.Text = preparer.EmailAddr;

            sServicingClosingFunds.setData(dataLoan.sServicingClosingFunds_Principal_rep, dataLoan.sServicingClosingFunds_Interest_rep, dataLoan.sInitialDeposit_Escrow_rep, dataLoan.sServicingClosingFunds_Other_rep, dataLoan.sServicingClosingFunds_LateFees_rep);
            sServicingCollectedFunds.setData(dataLoan.sServicingCollectedFunds_Principal_rep, dataLoan.sServicingCollectedFunds_Interest_rep, dataLoan.sServicingCollectedFunds_Escrow_rep, dataLoan.sServicingCollectedFunds_Other_rep, dataLoan.sServicingCollectedFunds_LateFees_rep);
            sServicingDisbursedFunds.setData(dataLoan.sServicingDisbursedFunds_Principal_rep, dataLoan.sServicingDisbursedFunds_Interest_rep, dataLoan.sServicingDisbursedFunds_Escrow_rep, dataLoan.sServicingDisbursedFunds_Other_rep, dataLoan.sServicingDisbursedFunds_LateFees_rep);
            sServicingCurrentTotalFunds.setData(dataLoan.sServicingCurrentTotalFunds_Principal_rep, dataLoan.sServicingCurrentTotalFunds_Interest_rep, dataLoan.sServicingCurrentTotalFunds_Escrow_rep, dataLoan.sServicingCurrentTotalFunds_Other_rep, dataLoan.sServicingCurrentTotalFunds_LateFees_rep);
            sServicingDueFunds.setData(dataLoan.sServicingDueFunds_Principal_rep, dataLoan.sServicingDueFunds_Interest_rep, dataLoan.sServicingDueFunds_Escrow_rep, dataLoan.sServicingDueFunds_Other_rep, dataLoan.sServicingDueFunds_LateFees_rep);
            sServicingTotalDueFunds.setData(dataLoan.sServicingTotalDueFunds_Principal_rep, dataLoan.sServicingTotalDueFunds_Interest_rep, dataLoan.sServicingTotalDueFunds_Escrow_rep, dataLoan.sServicingTotalDueFunds_Other_rep, dataLoan.sServicingTotalDueFunds_LateFees_rep);

            sServicingEscrowPmt.Text = dataLoan.sServicingEscrowPmt_rep;
            sServicingPrincipalBasis.Text = dataLoan.sServicingPrincipalBasis_rep;
            sServicingUnpaidPrincipalBalance.Text = dataLoan.sServicingUnpaidPrincipalBalance_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR2.Text = dataLoan.sNoteIR_rep;
            sTerm.Text = dataLoan.sTerm_rep;
            sTerm2.Text = dataLoan.sTerm_rep;
            sServicingOtherPmts.Text = dataLoan.sServicingOtherPmts_rep;
            Tools.SetDropDownListValue(sServicingPmtT, dataLoan.sServicingPmtT);

            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sServicingNextPmtDue.Text = dataLoan.sServicingNextPmtDue_rep;
            sServicingNextPmtAmt.Text = dataLoan.sServicingNextPmtAmt_rep;
            sServicingInterestDue.Text = dataLoan.sServicingInterestDue_rep;
            sServicingCurrentPmtDueD.Text = dataLoan.sServicingCurrentPmtDueD_rep;

            sServicingCurrentArmIndexR.Text = dataLoan.sServicingCurrentArmIndexR_rep;
            Tools.SetDropDownListValue(sServicingCurrentLateChargeT, dataLoan.sServicingCurrentLateChargeT);

            sTaxTableRealETxParcelNum.Text = dataLoan.sTaxTableRealETxParcelNum;
            sTaxTableSchoolParcelNum.Text = dataLoan.sTaxTableSchoolParcelNum;
            sTaxTable1008ParcelNum.Text = dataLoan.sTaxTable1008ParcelNum;
            sTaxTable1009ParcelNum.Text = dataLoan.sTaxTable1009ParcelNum;
            sTaxTableU3ParcelNum.Text = dataLoan.sTaxTableU3ParcelNum;
            sTaxTableU4ParcelNum.Text = dataLoan.sTaxTableU4ParcelNum;

            sTaxTableRealEtxPayeeNm.Text = dataLoan.sTaxTableRealEtxPayeeNm + (dataLoan.sTaxTableRealEtxPayeeNm== "" ? "" : " | ");
            sTaxTableSchoolPayeeNm.Text = dataLoan.sTaxTableSchoolPayeeNm + (dataLoan.sTaxTableSchoolPayeeNm == "" ? "" : " | ");
            sTaxTable1008PayeeNm.Text = dataLoan.sTaxTable1008PayeeNm + (dataLoan.sTaxTable1008PayeeNm == "" ? "" : " | ");
            sTaxTable1009PayeeNm.Text = dataLoan.sTaxTable1009PayeeNm + (dataLoan.sTaxTable1009PayeeNm == "" ? "" : " | ");
            sTaxTableU3PayeeNm.Text = dataLoan.sTaxTableU3PayeeNm + (dataLoan.sTaxTableU3PayeeNm == "" ? "" : " | ");
            sTaxTableU4PayeeNm.Text = dataLoan.sTaxTableU4PayeeNm + (dataLoan.sTaxTableU4PayeeNm == "" ? "" : " | ");

            sTaxTableRealEtxPayeeNmField.Value = dataLoan.sTaxTableRealEtxPayeeNm;
            sTaxTableSchoolPayeeNmField.Value = dataLoan.sTaxTableSchoolPayeeNm;
            sTaxTable1008PayeeNmField.Value = dataLoan.sTaxTable1008PayeeNm;
            sTaxTable1009PayeeNmField.Value = dataLoan.sTaxTable1009PayeeNm;
            sTaxTableU3PayeeNmField.Value = dataLoan.sTaxTableU3PayeeNm;
            sTaxTableU4PayeeNmField.Value = dataLoan.sTaxTableU4PayeeNm;

            sTaxTableRealEtxPayeeId.Value = dataLoan.sTaxTableRealEtxPayeeId.ToString();
            sTaxTableSchoolPayeeId.Value = dataLoan.sTaxTableSchoolPayeeId.ToString();
            sTaxTable1008PayeeId.Value = dataLoan.sTaxTable1008PayeeId.ToString();
            sTaxTable1009PayeeId.Value = dataLoan.sTaxTable1009PayeeId.ToString();
            sTaxTableU3PayeeId.Value = dataLoan.sTaxTableU3PayeeId.ToString();
            sTaxTableU4PayeeId.Value = dataLoan.sTaxTableU4PayeeId.ToString();


            sTaxTableRealEtxNumOfPmt.Text = dataLoan.sTaxTableRealEtxNumOfPmt;
            sTaxTableSchoolNumOfPmt.Text = dataLoan.sTaxTableSchoolNumOfPmt;
            sTaxTable1008NumOfPmt.Text = dataLoan.sTaxTable1008NumOfPmt;
            sTaxTable1009NumOfPmt.Text = dataLoan.sTaxTable1009NumOfPmt;
            sTaxTableU3NumOfPmt.Text = dataLoan.sTaxTableU3NumOfPmt;
            sTaxTableU4NumOfPmt.Text = dataLoan.sTaxTableU4NumOfPmt;

            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip.TrimWhitespaceAndBOM();

            RegisterJsObject("sServicingPayments", dataLoan.sServicingPayments);
            ClientScript.RegisterHiddenField("m_state", dataLoan.sSpState);

            Tools.SetDropDownListValue(sTaxTableRealETxT, dataLoan.sTaxTableRealETxT);
            Tools.SetDropDownListValue(sTaxTableSchoolT, dataLoan.sTaxTableSchoolT);
            Tools.SetDropDownListValue(sTaxTable1008T, dataLoan.sTaxTable1008T);
            Tools.SetDropDownListValue(sTaxTable1009T, dataLoan.sTaxTable1009T);
            Tools.SetDropDownListValue(sTaxTableU3T, dataLoan.sTaxTableU3T);
            Tools.SetDropDownListValue(sTaxTableU4T, dataLoan.sTaxTableU4T);

            Tools.SetDropDownListValue(sTaxTableRealETxPaidByT, dataLoan.sTaxTableRealETxPaidByT);
            Tools.SetDropDownListValue(sTaxTableSchoolPaidByT, dataLoan.sTaxTableSchoolPaidByT);
            Tools.SetDropDownListValue(sTaxTable1008PaidByT, dataLoan.sTaxTable1008PaidByT);
            Tools.SetDropDownListValue(sTaxTable1009PaidByT, dataLoan.sTaxTable1009PaidByT);
            Tools.SetDropDownListValue(sTaxTableU3PaidByT, dataLoan.sTaxTableU3PaidByT);
            Tools.SetDropDownListValue(sTaxTableU4PaidByT, dataLoan.sTaxTableU4PaidByT);

            sTaxTableRealEtxDueD.Text = dataLoan.sTaxTableRealEtxDueD_rep;
            sTaxTableSchoolDueD.Text = dataLoan.sTaxTableSchoolDueD_rep;
            sTaxTable1008DueD.Text = dataLoan.sTaxTable1008DueD_rep;
            sTaxTable1009DueD.Text = dataLoan.sTaxTable1009DueD_rep;
            sTaxTableU3DueD.Text = dataLoan.sTaxTableU3DueD_rep;
            sTaxTableU4DueD.Text = dataLoan.sTaxTableU4DueD_rep;

            sTaxTableRealEtxDueDLckd.Checked = dataLoan.sTaxTableRealEtxDueDLckd;
            sTaxTableSchoolDueDLckd.Checked = dataLoan.sTaxTableSchoolDueDLckd;
            sTaxTable1008DueDLckd.Checked = dataLoan.sTaxTable1008DueDLckd;
            sTaxTable1009DueDLckd.Checked = dataLoan.sTaxTable1009DueDLckd;
            sTaxTableU3DueDLckd.Checked = dataLoan.sTaxTableU3DueDLckd;
            sTaxTableU4DueDLckd.Checked = dataLoan.sTaxTableU4DueDLckd;

            sTaxTableRealEtxPaidD.Text = dataLoan.sTaxTableRealEtxPaidD_rep;
            sTaxTableSchoolPaidD.Text = dataLoan.sTaxTableSchoolPaidD_rep;
            sTaxTable1008PaidD.Text = dataLoan.sTaxTable1008PaidD_rep;
            sTaxTable1009PaidD.Text = dataLoan.sTaxTable1009PaidD_rep;
            sTaxTableU3PaidD.Text = dataLoan.sTaxTableU3PaidD_rep;
            sTaxTableU4PaidD.Text = dataLoan.sTaxTableU4PaidD_rep;

            sInvSchedDueD1.Value = dataLoan.sInvSchedDueD1_rep;
            sIsNextPaymentBefore1stPaymentDueInvestor.Value = dataLoan.sIsNextPaymentBefore1stPaymentDueInvestor_rep;
            sDocMagicClosingD.Value = dataLoan.sDocMagicClosingD_rep;

            sServicingNotes.Text = dataLoan.sServicingNotes;

            InitSettlementDescription(dataLoan);

            Tools.Bind_1098FilingYear(s1098FilingYear, dataLoan.sCreatedD.DateTimeForComputation.Year);
            Tools.SetDropDownListValue(s1098FilingYear, dataLoan.s1098FilingYear);

            s1098FilingYearLckd.Checked = dataLoan.s1098FilingYearLckd;
            s1098CurrentYearInterestAndPoints.Text = dataLoan.s1098CurrentYearInterestAndPoints_rep;
            s1098PreviousYearInterestAndPoints.Text = dataLoan.s1098PreviousYearInterestAndPoints_rep;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Servicing";
            this.PageID = "Servicing";

            RegisterJsScript("json.js");
            RegisterJsScript("AdjustmentTable.js");
            RegisterJsScript("combobox.js");
            RegisterJsObject("comboBoxOptions", DataAccess.CPageBase.sServicingTransTComboBox_map);
            RegisterJsScript("DynamicTable.js");
            RegisterJsScript("LQBPopup.js");

            Tools.Bind_ServicingPaymentType(sServicingPmtT);
            Tools.Bind_sServicingCurrentLateChargeT(sServicingCurrentLateChargeT);

            Tools.Bind_sTaxTableT(sTaxTableRealETxT);
            Tools.Bind_sTaxTableT(sTaxTableSchoolT);
            Tools.Bind_sTaxTableT(sTaxTable1008T);
            Tools.Bind_sTaxTableT(sTaxTable1009T);
            Tools.Bind_sTaxTableT(sTaxTableU3T);
            Tools.Bind_sTaxTableT(sTaxTableU4T);

            Tools.Bind_sTaxTablePaidBy(sTaxTableRealETxPaidByT);
            Tools.Bind_sTaxTablePaidBy(sTaxTableSchoolPaidByT);
            Tools.Bind_sTaxTablePaidBy(sTaxTable1008PaidByT);
            Tools.Bind_sTaxTablePaidBy(sTaxTable1009PaidByT);
            Tools.Bind_sTaxTablePaidBy(sTaxTableU3PaidByT);
            Tools.Bind_sTaxTablePaidBy(sTaxTableU4PaidByT);

            sSpZip.SmartZipcode(sSpCity, sSpState);
            PaymentStatementServicerZip.SmartZipcode(PaymentStatementServicerCity, PaymentStatementServicerState);

            this.PDFPrintClass = typeof(LendersOffice.Pdf.CPaymentHistoryPDF);

            phAdditionalTaxLines.Visible = DisplayAdditionalTaxLines;
        }
        
        //Populates options, initial selections for Tax Table Settlement Description DDLs
        private void InitSettlementDescription(CPageData dataLoan)
        {
            var taxDescTDDLs = new List<DropDownList>()
            {
                sTaxTable1008DescT,
                sTaxTable1009DescT,
                sTaxTableU3DescT,
                sTaxTableU4DescT
            };

            foreach (var ddl in taxDescTDDLs)
            {
                ddl.Items.AddRange(new ListItem[3]
                {
                    Tools.CreateEnumListItem("<- Select a settlement charge ->", E_TableSettlementDescT.NoInfo),
                    Tools.CreateEnumListItem(dataLoan.s1006ProHExpDesc, E_TableSettlementDescT.Use1008DescT),
                    Tools.CreateEnumListItem(dataLoan.s1007ProHExpDesc, E_TableSettlementDescT.Use1009DescT),
                });

                if (Broker.EnableAdditionalSection1000CustomFees)
                {
                    ddl.Items.AddRange(new ListItem[2]
                    {
                        Tools.CreateEnumListItem(dataLoan.sU3RsrvDesc, E_TableSettlementDescT.Use1010DescT),
                        Tools.CreateEnumListItem(dataLoan.sU4RsrvDesc, E_TableSettlementDescT.Use1011DescT)
                    });
                }
            }

            Tools.SetDropDownListValue(sTaxTable1008DescT, dataLoan.sTaxTable1008DescT);
            Tools.SetDropDownListValue(sTaxTable1009DescT, dataLoan.sTaxTable1009DescT);
            Tools.SetDropDownListValue(sTaxTableU3DescT, dataLoan.sTaxTableU3DescT);
            Tools.SetDropDownListValue(sTaxTableU4DescT, dataLoan.sTaxTableU4DescT);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            EnableJqueryMigrate = false;

            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
        
    }
}
