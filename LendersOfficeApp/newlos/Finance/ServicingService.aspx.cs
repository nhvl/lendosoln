﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public class ServicingServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(ServicingServiceItem));
        }

        protected BrokerDB Broker
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerDB; }
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sServicingOtherPmts_rep = GetString("sServicingOtherPmts");
            dataLoan.sServicingNextPmtDue_rep = GetString("sServicingNextPmtDue");
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");
            
            string serializedTransactions = GetString("sTransactions");
            List<CServicingPaymentFields> transactions = ObsoleteSerializationHelper.JsonDeserialize<List<CServicingPaymentFields>>(serializedTransactions);
            if (GetString("addNextPayment") == "true")
                dataLoan.AddNextServicingPaymentDue(transactions);
            else
                dataLoan.sServicingPayments = transactions;

            dataLoan.sServicingCurrentArmIndexR_rep = GetString("sServicingCurrentArmIndexR");
            dataLoan.sServicingCurrentLateChargeT = (E_TaxLateChargeT) GetInt("sServicingCurrentLateChargeT");

            dataLoan.sTaxTable1008DescT = (E_TableSettlementDescT)GetInt("sTaxTable1008DescT");
            dataLoan.sTaxTable1009DescT = (E_TableSettlementDescT)GetInt("sTaxTable1009DescT");

            dataLoan.sTaxTableRealETxT = (E_TaxTableTaxT)GetInt("sTaxTableRealETxT");
            dataLoan.sTaxTableSchoolT = (E_TaxTableTaxT)GetInt("sTaxTableSchoolT");
            dataLoan.sTaxTable1008T = (E_TaxTableTaxT)GetInt("sTaxTable1008T");
            dataLoan.sTaxTable1009T = (E_TaxTableTaxT)GetInt("sTaxTable1009T");

            dataLoan.sTaxTableRealETxParcelNum = GetString("sTaxTableRealETxParcelNum");
            dataLoan.sTaxTableSchoolParcelNum = GetString("sTaxTableSchoolParcelNum");
            dataLoan.sTaxTable1008ParcelNum = GetString("sTaxTable1008ParcelNum");
            dataLoan.sTaxTable1009ParcelNum = GetString("sTaxTable1009ParcelNum");

            dataLoan.sTaxTableRealETxPaidByT = (E_TaxTablePaidBy)GetInt("sTaxTableRealETxPaidByT");
            dataLoan.sTaxTableSchoolPaidByT = (E_TaxTablePaidBy)GetInt("sTaxTableSchoolPaidByT");
            dataLoan.sTaxTable1008PaidByT = (E_TaxTablePaidBy)GetInt("sTaxTable1008PaidByT");
            dataLoan.sTaxTable1009PaidByT = (E_TaxTablePaidBy)GetInt("sTaxTable1009PaidByT");

            dataLoan.sTaxTableRealEtxPayeeId = GetInt("sTaxTableRealEtxPayeeId");
            dataLoan.sTaxTableSchoolPayeeId = GetInt("sTaxTableSchoolPayeeId");
            dataLoan.sTaxTable1008PayeeId = GetInt("sTaxTable1008PayeeId");
            dataLoan.sTaxTable1009PayeeId = GetInt("sTaxTable1009PayeeId");

            dataLoan.sTaxTableRealEtxPayeeNm = GetString("sTaxTableRealEtxPayeeNmField");
            dataLoan.sTaxTableSchoolPayeeNm = GetString("sTaxTableSchoolPayeeNmField");
            dataLoan.sTaxTable1008PayeeNm = GetString("sTaxTable1008PayeeNmField");
            dataLoan.sTaxTable1009PayeeNm = GetString("sTaxTable1009PayeeNmField");

            dataLoan.sTaxTableRealEtxNumOfPmt = GetString("sTaxTableRealEtxNumOfPmt");
            dataLoan.sTaxTableSchoolNumOfPmt = GetString("sTaxTableSchoolNumOfPmt");
            dataLoan.sTaxTable1008NumOfPmt = GetString("sTaxTable1008NumOfPmt");
            dataLoan.sTaxTable1009NumOfPmt = GetString("sTaxTable1009NumOfPmt");

            dataLoan.sTaxTableRealEtxDueD_rep = GetString("sTaxTableRealEtxDueD");
            dataLoan.sTaxTableSchoolDueD_rep = GetString("sTaxTableSchoolDueD");
            dataLoan.sTaxTable1008DueD_rep = GetString("sTaxTable1008DueD");
            dataLoan.sTaxTable1009DueD_rep = GetString("sTaxTable1009DueD");

            dataLoan.sTaxTableRealEtxDueDLckd = GetBool("sTaxTableRealEtxDueDLckd");
            dataLoan.sTaxTableSchoolDueDLckd = GetBool("sTaxTableSchoolDueDLckd");
            dataLoan.sTaxTable1008DueDLckd = GetBool("sTaxTable1008DueDLckd");
            dataLoan.sTaxTable1009DueDLckd = GetBool("sTaxTable1009DueDLckd");

            dataLoan.sTaxTableRealEtxPaidD_rep = GetString("sTaxTableRealEtxPaidD");
            dataLoan.sTaxTableSchoolPaidD_rep = GetString("sTaxTableSchoolPaidD");
            dataLoan.sTaxTable1008PaidD_rep = GetString("sTaxTable1008PaidD");
            dataLoan.sTaxTable1009PaidD_rep = GetString("sTaxTable1009PaidD");

            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sTaxTableU3DescT = (E_TableSettlementDescT)GetInt("sTaxTableU3DescT");
                dataLoan.sTaxTableU4DescT = (E_TableSettlementDescT)GetInt("sTaxTableU4DescT");
                dataLoan.sTaxTableU3T = (E_TaxTableTaxT)GetInt("sTaxTableU3T");
                dataLoan.sTaxTableU4T = (E_TaxTableTaxT)GetInt("sTaxTableU4T");
                dataLoan.sTaxTableU3ParcelNum = GetString("sTaxTableU3ParcelNum");
                dataLoan.sTaxTableU4ParcelNum = GetString("sTaxTableU4ParcelNum");
                dataLoan.sTaxTableU3PaidByT = (E_TaxTablePaidBy)GetInt("sTaxTableU3PaidByT");
                dataLoan.sTaxTableU4PaidByT = (E_TaxTablePaidBy)GetInt("sTaxTableU4PaidByT");
                dataLoan.sTaxTableU3PayeeId = GetInt("sTaxTableU3PayeeId");
                dataLoan.sTaxTableU4PayeeId = GetInt("sTaxTableU4PayeeId");
                dataLoan.sTaxTableU3PayeeNm = GetString("sTaxTableU3PayeeNmField");
                dataLoan.sTaxTableU4PayeeNm = GetString("sTaxTableU4PayeeNmField");
                dataLoan.sTaxTableU3NumOfPmt = GetString("sTaxTableU3NumOfPmt");
                dataLoan.sTaxTableU4NumOfPmt = GetString("sTaxTableU4NumOfPmt");
                dataLoan.sTaxTableU3DueD_rep = GetString("sTaxTableU3DueD");
                dataLoan.sTaxTableU4DueD_rep = GetString("sTaxTableU4DueD");
                dataLoan.sTaxTableU3DueDLckd = GetBool("sTaxTableU3DueDLckd");
                dataLoan.sTaxTableU4DueDLckd = GetBool("sTaxTableU4DueDLckd");
                dataLoan.sTaxTableU3PaidD_rep = GetString("sTaxTableU3PaidD");
                dataLoan.sTaxTableU4PaidD_rep = GetString("sTaxTableU4PaidD");
            }

            dataLoan.sBillingMethodT = (E_BillingMethodT)GetInt("sBillingMethodT");
            dataLoan.sBillingAccountLocationT = (E_BillingAccountLocationT)GetInt("sBillingAccountLocationT");
            dataLoan.sBillingABANum = GetString("sBillingABANum");
            dataLoan.sBillingAccountNum = GetString("sBillingAccountNum");
            dataLoan.sBillingNameOnAccount = GetString("sBillingNameOnAccount");
            dataLoan.sBillingAccountT = (E_BillingAccountT)GetInt("sBillingAccountT");
            dataLoan.sBillingAdditionalAmt_rep = GetString("sBillingAdditionalAmt");

            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");

            dataLoan.sServicingNotes = GetString("sServicingNotes");

            dataLoan.sPaymentStatementDelinquencyNotice = GetString("sPaymentStatementDelinquencyNotice");
            dataLoan.sPaymentStatementSuspenseAccountNotice = GetString("sPaymentStatementSuspenseAccountNotice");
            dataLoan.sPaymentStatementOtherNotices = GetString("sPaymentStatementOtherNotices");

            var paymentStatementServicer = dataLoan.GetPreparerOfForm(E_PreparerFormT.PaymentStatementServicer, E_ReturnOptionIfNotExist.CreateNew);
            paymentStatementServicer.CompanyName = GetString("PaymentStatementServicerCompanyNm");
            paymentStatementServicer.StreetAddr = GetString("PaymentStatementServicerStreetAddr");
            paymentStatementServicer.City = GetString("PaymentStatementServicerCity");
            paymentStatementServicer.State = GetString("PaymentStatementServicerState");
            paymentStatementServicer.Zip = GetString("PaymentStatementServicerZip");
            paymentStatementServicer.PhoneOfCompany = GetString("PaymentStatementServicerPhoneOfCompany");
            paymentStatementServicer.EmailAddr = GetString("PaymentStatementServicerEmailAddr");
            paymentStatementServicer.Update();

            dataLoan.s1098FilingYear = GetInt("s1098FilingYear");
            dataLoan.s1098FilingYearLckd = GetBool("s1098FilingYearLckd");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            foreach (CServicingPaymentFields transaction in dataLoan.sServicingPayments)
            {
                int rowNum = transaction.RowNum;
                SetResult("ServicingTransactionT" + rowNum, transaction.ServicingTransactionT);
                SetResult("DueD" + rowNum, transaction.DueD_rep);
                SetResult("DueAmt" + rowNum, transaction.DueAmt_rep);
                SetResult("PaymentD" + rowNum, transaction.PaymentD_rep);
                SetResult("PaymentReceivedDLckd" + rowNum, transaction.PaymentReceivedDLckd);
                SetResult("PaymentReceivedD" + rowNum, transaction.PaymentReceivedD_rep);
                SetResult("PaymentAmt" + rowNum, transaction.PaymentAmt_rep);
                SetResult("Principal" + rowNum, transaction.Principal_rep);
                SetResult("Interest" + rowNum, transaction.Interest_rep);
                SetResult("Escrow" + rowNum, transaction.Escrow_rep);
                SetResult("Other" + rowNum, transaction.Other_rep);
                SetResult("LateFee" + rowNum, transaction.LateFee_rep);
                SetResult("Notes" + rowNum, transaction.Notes);
                SetResult("Payee" + rowNum, transaction.Payee);
            }

            SetResult("sServicingUnpaidPrincipalBalance", dataLoan.sServicingUnpaidPrincipalBalance_rep);
            SetResult("sServicingEscrowPmt", dataLoan.sServicingEscrowPmt_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sNoteIR2", dataLoan.sNoteIR_rep);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sTerm2", dataLoan.sTerm_rep);
            SetResult("sServicingPrincipalBasis", dataLoan.sServicingPrincipalBasis_rep);
            SetResult("sServicingInterestDue", dataLoan.sServicingInterestDue_rep);
            SetResult("sServicingNextPmtAmt", dataLoan.sServicingNextPmtAmt_rep);
            SetResult("sServicingOtherPmts", dataLoan.sServicingOtherPmts_rep);

            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
            SetResult("sServicingNextPmtDue", dataLoan.sServicingNextPmtDue_rep);
            SetResult("sServicingPmtT", dataLoan.sServicingPmtT);

            SetResult("sServicingClosingFunds_Principal", dataLoan.sServicingClosingFunds_Principal_rep);
            SetResult("sServicingClosingFunds_Interest", dataLoan.sServicingClosingFunds_Interest_rep);
            SetResult("sServicingClosingFunds_Escrow", dataLoan.sInitialDeposit_Escrow_rep);
            SetResult("sServicingClosingFunds_Other", dataLoan.sServicingClosingFunds_Other_rep);
            SetResult("sServicingClosingFunds_LateFees", dataLoan.sServicingClosingFunds_LateFees_rep);
            SetResult("sServicingCollectedFunds_Principal", dataLoan.sServicingCollectedFunds_Principal_rep);
            SetResult("sServicingCollectedFunds_Interest", dataLoan.sServicingCollectedFunds_Interest_rep);
            SetResult("sServicingCollectedFunds_Escrow", dataLoan.sServicingCollectedFunds_Escrow_rep);
            SetResult("sServicingCollectedFunds_Other", dataLoan.sServicingCollectedFunds_Other_rep);
            SetResult("sServicingCollectedFunds_LateFees", dataLoan.sServicingCollectedFunds_LateFees_rep);
            SetResult("sServicingDisbursedFunds_Principal", dataLoan.sServicingDisbursedFunds_Principal_rep);
            SetResult("sServicingDisbursedFunds_Interest", dataLoan.sServicingDisbursedFunds_Interest_rep);
            SetResult("sServicingDisbursedFunds_Escrow", dataLoan.sServicingDisbursedFunds_Escrow_rep);
            SetResult("sServicingDisbursedFunds_Other", dataLoan.sServicingDisbursedFunds_Other_rep);
            SetResult("sServicingDisbursedFunds_LateFees", dataLoan.sServicingDisbursedFunds_LateFees_rep);
            SetResult("sServicingCurrentTotalFunds_Principal", dataLoan.sServicingCurrentTotalFunds_Principal_rep);
            SetResult("sServicingCurrentTotalFunds_Interest", dataLoan.sServicingCurrentTotalFunds_Interest_rep);
            SetResult("sServicingCurrentTotalFunds_Escrow", dataLoan.sServicingCurrentTotalFunds_Escrow_rep);
            SetResult("sServicingCurrentTotalFunds_Other", dataLoan.sServicingCurrentTotalFunds_Other_rep);
            SetResult("sServicingCurrentTotalFunds_LateFees", dataLoan.sServicingCurrentTotalFunds_LateFees_rep);
            SetResult("sServicingDueFunds_Principal", dataLoan.sServicingDueFunds_Principal_rep);
            SetResult("sServicingDueFunds_Interest", dataLoan.sServicingDueFunds_Interest_rep);
            SetResult("sServicingDueFunds_Escrow", dataLoan.sServicingDueFunds_Escrow_rep);
            SetResult("sServicingDueFunds_Other", dataLoan.sServicingDueFunds_Other_rep);
            SetResult("sServicingDueFunds_LateFees", dataLoan.sServicingDueFunds_LateFees_rep);
            SetResult("sServicingTotalDueFunds_Principal", dataLoan.sServicingTotalDueFunds_Principal_rep);
            SetResult("sServicingTotalDueFunds_Interest", dataLoan.sServicingTotalDueFunds_Interest_rep);
            SetResult("sServicingTotalDueFunds_Escrow", dataLoan.sServicingTotalDueFunds_Escrow_rep);
            SetResult("sServicingTotalDueFunds_Other", dataLoan.sServicingTotalDueFunds_Other_rep);
            SetResult("sServicingTotalDueFunds_LateFees", dataLoan.sServicingTotalDueFunds_LateFees_rep);
            SetResult("sServicingCurrentPmtDueD", dataLoan.sServicingCurrentPmtDueD_rep);

            SetResult("sServicingCurrentArmIndexR", dataLoan.sServicingCurrentArmIndexR_rep);
            SetResult("sServicingCurrentLateChargeT", dataLoan.sServicingCurrentLateChargeT);

            SetResult("sTaxTableRealETxT", dataLoan.sTaxTableRealETxT);
            SetResult("sTaxTableSchoolT", dataLoan.sTaxTableSchoolT);
            SetResult("sTaxTable1008T", dataLoan.sTaxTable1008T);
            SetResult("sTaxTable1009T", dataLoan.sTaxTable1009T);
            SetResult("sTaxTableU3T", dataLoan.sTaxTableU3T);
            SetResult("sTaxTableU4T", dataLoan.sTaxTableU4T);

            SetResult("sTaxTableRealETxParcelNum", dataLoan.sTaxTableRealETxParcelNum);
            SetResult("sTaxTableSchoolParcelNum", dataLoan.sTaxTableSchoolParcelNum);
            SetResult("sTaxTable1008ParcelNum", dataLoan.sTaxTable1008ParcelNum);
            SetResult("sTaxTable1009ParcelNum", dataLoan.sTaxTable1009ParcelNum);
            SetResult("sTaxTableU3ParcelNum", dataLoan.sTaxTableU3ParcelNum);
            SetResult("sTaxTableU4ParcelNum", dataLoan.sTaxTableU4ParcelNum);

            SetResult("sTaxTableRealEtxPayeeNm", dataLoan.sTaxTableRealEtxPayeeNm + (dataLoan.sTaxTableRealEtxPayeeNm== "" ? "" : " | "));
            SetResult("sTaxTableSchoolPayeeNm", dataLoan.sTaxTableSchoolPayeeNm + (dataLoan.sTaxTableSchoolPayeeNm == "" ? "" : " | "));
            SetResult("sTaxTable1008PayeeNm", dataLoan.sTaxTable1008PayeeNm + (dataLoan.sTaxTable1008PayeeNm == "" ? "" : " | "));
            SetResult("sTaxTable1009PayeeNm", dataLoan.sTaxTable1009PayeeNm + (dataLoan.sTaxTable1009PayeeNm == "" ? "" : " | "));
            SetResult("sTaxTableU3PayeeNm", dataLoan.sTaxTableU3PayeeNm + (dataLoan.sTaxTableU3PayeeNm == "" ? "" : " | "));
            SetResult("sTaxTableU4PayeeNm", dataLoan.sTaxTableU4PayeeNm + (dataLoan.sTaxTableU4PayeeNm == "" ? "" : " | "));

            SetResult("sTaxTableRealEtxPayeeNmField", dataLoan.sTaxTableRealEtxPayeeNm);
            SetResult("sTaxTableSchoolPayeeNmField", dataLoan.sTaxTableSchoolPayeeNm);
            SetResult("sTaxTable1008PayeeNmField", dataLoan.sTaxTable1008PayeeNm);
            SetResult("sTaxTable1009PayeeNmField", dataLoan.sTaxTable1009PayeeNm);
            SetResult("sTaxTableU3PayeeNmField", dataLoan.sTaxTableU3PayeeNm);
            SetResult("sTaxTableU4PayeeNmField", dataLoan.sTaxTableU4PayeeNm);

            SetResult("sTaxTableRealEtxPayeeId", dataLoan.sTaxTableRealEtxPayeeId);
            SetResult("sTaxTableSchoolPayeeId", dataLoan.sTaxTableSchoolPayeeId);
            SetResult("sTaxTable1008PayeeId", dataLoan.sTaxTable1008PayeeId);
            SetResult("sTaxTable1009PayeeId", dataLoan.sTaxTable1009PayeeId);
            SetResult("sTaxTableU3PayeeId", dataLoan.sTaxTableU3PayeeId);
            SetResult("sTaxTableU4PayeeId", dataLoan.sTaxTableU4PayeeId);

            SetResult("sTaxTableRealEtxNumOfPmt", dataLoan.sTaxTableRealEtxNumOfPmt);
            SetResult("sTaxTableSchoolNumOfPmt", dataLoan.sTaxTableSchoolNumOfPmt);
            SetResult("sTaxTable1008NumOfPmt", dataLoan.sTaxTable1008NumOfPmt);
            SetResult("sTaxTable1009NumOfPmt", dataLoan.sTaxTable1009NumOfPmt);
            SetResult("sTaxTableU3NumOfPmt", dataLoan.sTaxTableU3NumOfPmt);
            SetResult("sTaxTableU4NumOfPmt", dataLoan.sTaxTableU4NumOfPmt);

            SetResult("sTaxTableRealEtxDueD", dataLoan.sTaxTableRealEtxDueD_rep);
            SetResult("sTaxTableSchoolDueD", dataLoan.sTaxTableSchoolDueD_rep);
            SetResult("sTaxTable1008DueD", dataLoan.sTaxTable1008DueD_rep);
            SetResult("sTaxTable1009DueD", dataLoan.sTaxTable1009DueD_rep);
            SetResult("sTaxTableU3DueD", dataLoan.sTaxTableU3DueD_rep);
            SetResult("sTaxTableU4DueD", dataLoan.sTaxTableU4DueD_rep);

            SetResult("sTaxTableRealEtxDueDLckd", dataLoan.sTaxTableRealEtxDueDLckd);
            SetResult("sTaxTableSchoolDueDLckd", dataLoan.sTaxTableSchoolDueDLckd);
            SetResult("sTaxTable1008DueDLckd", dataLoan.sTaxTable1008DueDLckd);
            SetResult("sTaxTable1009DueDLckd", dataLoan.sTaxTable1009DueDLckd);
            SetResult("sTaxTableU3DueDLckd", dataLoan.sTaxTableU3DueDLckd);
            SetResult("sTaxTableU4DueDLckd", dataLoan.sTaxTableU4DueDLckd);

            SetResult("sTaxTableRealEtxPaidD", dataLoan.sTaxTableRealEtxPaidD_rep);
            SetResult("sTaxTableSchoolPaidD", dataLoan.sTaxTableSchoolPaidD_rep);
            SetResult("sTaxTable1008PaidD", dataLoan.sTaxTable1008PaidD_rep);
            SetResult("sTaxTable1009PaidD", dataLoan.sTaxTable1009PaidD_rep);
            SetResult("sTaxTableU3PaidD", dataLoan.sTaxTableU3PaidD_rep);
            SetResult("sTaxTableU4PaidD", dataLoan.sTaxTableU4PaidD_rep);

            SetResult("sSpAddr",  dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);

            SetResult("sInvSchedDueD1", dataLoan.sInvSchedDueD1_rep);
            SetResult("sIsNextPaymentBefore1stPaymentDueInvestor", dataLoan.sIsNextPaymentBefore1stPaymentDueInvestor_rep);

            SetResult("sServicingNotes", dataLoan.sServicingNotes);

            SetResult("sPaymentStatementDelinquencyNotice", dataLoan.sPaymentStatementDelinquencyNotice);
            SetResult("sPaymentStatementSuspenseAccountNotice", dataLoan.sPaymentStatementSuspenseAccountNotice);
            SetResult("sPaymentStatementOtherNotices", dataLoan.sPaymentStatementOtherNotices);

            var paymentStatementServicer = dataLoan.GetPreparerOfForm(E_PreparerFormT.PaymentStatementServicer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("PaymentStatementServicerCompanyNm", paymentStatementServicer.CompanyName);
            SetResult("PaymentStatementServicerStreetAddr", paymentStatementServicer.StreetAddr);
            SetResult("PaymentStatementServicerCity", paymentStatementServicer.City);
            SetResult("PaymentStatementServicerState", paymentStatementServicer.State);
            SetResult("PaymentStatementServicerZip", paymentStatementServicer.Zip);
            SetResult("PaymentStatementServicerPhoneOfCompany", paymentStatementServicer.PhoneOfCompany);
            SetResult("PaymentStatementServicerEmailAddr", paymentStatementServicer.EmailAddr);

            SetResult("s1098FilingYear", dataLoan.s1098FilingYear);
            SetResult("s1098FilingYearLckd", dataLoan.s1098FilingYearLckd);
            SetResult("s1098CurrentYearInterestAndPoints", dataLoan.s1098CurrentYearInterestAndPoints_rep);
            SetResult("s1098PreviousYearInterestAndPoints", dataLoan.s1098PreviousYearInterestAndPoints_rep);
        }
    }
    public partial class ServicingService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new ServicingServiceItem());
        }
    }


}
