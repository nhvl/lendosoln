﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class WindstormInsurancePolicy : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }

        private void BindSettlementChargeDDL(CPageData dataLoan)
        {
            sWindInsSettlementChargeT.Items.Add(Tools.CreateEnumListItem("<- Select a settlement charge ->", E_sInsSettlementChargeT.None));
            sWindInsSettlementChargeT.Items.Add(Tools.CreateEnumListItem(dataLoan.s1006ProHExpDesc, E_sInsSettlementChargeT.Line1008));
            sWindInsSettlementChargeT.Items.Add(Tools.CreateEnumListItem(dataLoan.s1007ProHExpDesc, E_sInsSettlementChargeT.Line1009));
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                sWindInsSettlementChargeT.Items.Add(Tools.CreateEnumListItem(dataLoan.sU3RsrvDesc, E_sInsSettlementChargeT.Line1010));
                sWindInsSettlementChargeT.Items.Add(Tools.CreateEnumListItem(dataLoan.sU4RsrvDesc, E_sInsSettlementChargeT.Line1011));
            }
        }

        private void BindPolicyPaymentTypeDDL(CPageData dataLoan)
        {
            sWindInsPolicyPaymentTypeT.Items.Add(Tools.CreateEnumListItem("Escrowed", E_InsPolicyPaymentTypeT.Escrowed));
            sWindInsPolicyPaymentTypeT.Items.Add(Tools.CreateEnumListItem("Non-Escrowed", E_InsPolicyPaymentTypeT.NonEscrowed));
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(WindstormInsurancePolicy));
            dataLoan.InitLoad();

            BindSettlementChargeDDL(dataLoan);
            BindPolicyPaymentTypeDDL(dataLoan);

            sWindInsCompanyNm.Text = dataLoan.sWindInsCompanyNm;
            sWindInsCompanyId.Text = dataLoan.sWindInsCompanyId;
            sWindInsPolicyNum.Text = dataLoan.sWindInsPolicyNum;
            sWindInsPolicyPayeeCode.Text = dataLoan.sWindInsPolicyPayeeCode;//new

            Tools.SetDropDownListValue(sWindInsSettlementChargeT, dataLoan.sWindInsSettlementChargeT);
            Tools.SetDropDownListValue(sWindInsPaidByT, dataLoan.sWindInsPaidByT);
            Tools.SetDropDownListValue(sWindInsPolicyPaymentTypeT, dataLoan.sWindInsPolicyPaymentTypeT);//new
        
            // Money
            sWindInsCoverageAmt.Text = dataLoan.sWindInsCoverageAmt_rep;
            sWindInsDeductibleAmt.Text = dataLoan.sWindInsDeductibleAmt_rep;
            sWindInsPaymentDueAmtLckd.Checked = dataLoan.sWindInsPaymentDueAmtLckd;
            sWindInsPaymentDueAmt.Text = dataLoan.sWindInsPaymentDueAmt_rep;

            // Dates
            sWindInsPolicyActivationD.Text = dataLoan.sWindInsPolicyActivationD_rep;
            sWindInsPaymentDueD.Text = dataLoan.sWindInsPaymentDueD_rep;
            sWindInsPaymentMadeD.Text = dataLoan.sWindInsPaymentMadeD_rep;
            sWindInsPaidThroughD.Text = dataLoan.sWindInsPaidThroughD_rep;
            sWindInsPolicyExpirationD.Text = dataLoan.sWindInsPolicyExpirationD_rep;//new
            sWindInsPolicyLossPayeeTransD.Text = dataLoan.sWindInsPolicyLossPayeeTransD_rep;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "WindstormInsurancePolicy";
            this.PageID = "WindstormInsurancePolicy";

            Tools.Bind_sInsPaidByWithHOA(sWindInsPaidByT);
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
