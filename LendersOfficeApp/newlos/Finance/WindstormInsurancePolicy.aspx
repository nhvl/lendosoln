﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WindstormInsurancePolicy.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.WindstormInsurancePolicy" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<script language="javascript">
  function _init() {
    lockField(<%= AspxTools.JsGetElementById(sWindInsPaymentDueAmtLckd) %>, 'sWindInsPaymentDueAmt');
  }
  
    function clearCompanyId()
  {
    document.getElementById("sWindInsCompanyId").value = "";
  }
</script>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Windstorm Insurance Policy</title>  
</head>

<body bgcolor="gainsboro">
    <form id="WindstormPolicy" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="MainRightHeader" noWrap>
                    Windstorm Insurance Policy
                </td>
            </tr>            
            <tr>
                <td>
                    <table class="FieldLabel">
                        <tr>
                            <td></td>
                            <td>
                                <uc:CFM id="CFM" runat="server" CompanyNameField="sWindInsCompanyNm" CompanyIdField="sWindInsCompanyId" DisableAddTo="true"></uc:CFM>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Insurance Company
                            </td>
                            <td>
                                <asp:TextBox onchange="clearCompanyId()" ID="sWindInsCompanyNm" Width="150" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Policy number
                            </td>
                            <td>
                                <asp:TextBox ID="sWindInsPolicyNum" Width="150" runat="server" ></asp:TextBox>
                            </td>                            
                        </tr>
                        <tr>
                            <td>
                                Policy activation date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sWindInsPolicyActivationD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Policy expiration date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sWindInsPolicyExpirationD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Paid by
                            </td>
                            <td>
                                <asp:DropDownList ID="sWindInsPaidByT" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Coverage amount
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sWindInsCoverageAmt" runat="server" CssClass="mask" preset="money" width="110"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Deductible amount
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sWindInsDeductibleAmt" runat="server" CssClass="mask" preset="money" Width="110"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Due
                            </td>
                            <td>
                                <asp:DropDownList ID="sWindInsSettlementChargeT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                                <ml:MoneyTextBox ID="sWindInsPaymentDueAmt" runat="server" CssClass="mask" preset="money" ReadOnly="true" Width="110"></ml:MoneyTextBox>
                                <asp:CheckBox ID="sWindInsPaymentDueAmtLckd" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Type
                            </td>
                            <td>
                                <asp:DropDownList ID="sWindInsPolicyPaymentTypeT" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Due Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sWindInsPaymentDueD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Made Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sWindInsPaymentMadeD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Paid Through Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sWindInsPaidThroughD" runat="server" width="75"/>
                            </td>
                        </tr>   
                        <tr>
                            <td>
                                Payee Code
                            </td>
                            <td>
                                <asp:TextBox ID="sWindInsPolicyPayeeCode" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Loss Payee Transfer Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sWindInsPolicyLossPayeeTransD" runat="server" Width="75" />
                            </td>
                        </tr>          
                        <tr>
                            <td>Company Id</td>
                            <td><asp:TextBox runat="server" ID="sWindInsCompanyId" Text=""></asp:TextBox></td>
                        </tr>           
                    </table>
                </td>
            </tr>
        </table>
    </form> 
</body>
</html>