﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class PayoffStatement : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite, Permission.AllowCloserRead };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantRead, Permission.AllowCloserRead };
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(PayoffStatement));
            dataLoan.InitLoad();

            sPayoffStatementPayoffD.Text = dataLoan.sPayoffStatementPayoffD_rep;
            sServicingLastPmtDueD.Text = dataLoan.sServicingLastPmtDueD_rep;
            sServicingNextPmtDue.Text = dataLoan.sServicingNextPmtDue_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sServicingUnpaidPrincipalBalance.Text = dataLoan.sServicingUnpaidPrincipalBalance_rep;
            sDaysInYr.Text = dataLoan.sDaysInYr_rep;
            sSettlementIPerDay.Text = dataLoan.sSettlementIPerDay_rep;
            sPayoffStatementInterestWholeMonths.Text = dataLoan.sPayoffStatementInterestWholeMonths_rep;
            sPayoffStatementInterestOddDays.Text = dataLoan.sPayoffStatementInterestOddDays_rep;
            sPayoffStatementTotalInterestDueAmt.Text = dataLoan.sPayoffStatementTotalInterestDueAmt_rep;
            sServicingUnpaidPrincipalBalance_col2.Text = dataLoan.sServicingUnpaidPrincipalBalance_rep;
            sPayoffStatementTotalInterestDueAmt_col2.Text = dataLoan.sPayoffStatementTotalInterestDueAmt_rep;
            sPayoffStatementMonthlyMiDueAmt.Text = dataLoan.sPayoffStatementMonthlyMiDueAmt_rep;
            sServicingTotalDueFunds_Escrow.Text = dataLoan.sServicingTotalDueFunds_Escrow_rep;
            sServicingTotalDueFunds_LateFees.Text = dataLoan.sServicingTotalDueFunds_LateFees_rep;
            sPayoffStatementTotalDueAmt.Text = dataLoan.sPayoffStatementTotalDueAmt_rep;
            sPayoffStatementTotalDueAmt_2.Text = dataLoan.sPayoffStatementTotalDueAmt_rep;
            sPayoffStatementNotesToRecipient.Text = dataLoan.sPayoffStatementNotesToRecipient;

            sPayoffStatementRecipientAddressee.Text = dataLoan.sPayoffStatementRecipientAddressee;
            sPayoffStatementRecipientAttention.Text = dataLoan.sPayoffStatementRecipientAttention;
            sPayoffStatementRecipientStreetAddress.Text = dataLoan.sPayoffStatementRecipientStreetAddress;
            sPayoffStatementRecipientCity.Text = dataLoan.sPayoffStatementRecipientCity;
            sPayoffStatementRecipientState.Value = dataLoan.sPayoffStatementRecipientState;
            sPayoffStatementRecipientZip.Text = dataLoan.sPayoffStatementRecipientZip;

            Tools.SetDropDownListValue(sPayoffStatementInterestCalcT, dataLoan.sPayoffStatementInterestCalcT);
            Tools.SetDropDownListValue(sPayoffStatementRecipientT, dataLoan.sPayoffStatementRecipientT);
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "PayoffStatement";
            this.PageID = "PayoffStatement";

            Tools.Bind_sPayoffStatementInterestCalcT(sPayoffStatementInterestCalcT);
            Tools.Bind_sPayoffStatementRecipientT(sPayoffStatementRecipientT);

            sPayoffStatementRecipientZip.SmartZipcode(sPayoffStatementRecipientCity, sPayoffStatementRecipientState);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            EnableJqueryMigrate = false;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
