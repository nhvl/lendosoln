﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Finance
{
    public class TransactionsServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TransactionsServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sTransactions = ObsoleteSerializationHelper.JsonDeserialize<List<CTransactionFields>>(GetString("sTransactions"));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            foreach (CTransactionFields transaction in dataLoan.sTransactions)
            {
                int rowNum = transaction.RowNum;
                SetResult("ItemDescription" + rowNum, transaction.ItemDescription);
                SetResult("TransactionD" + rowNum, transaction.TransactionD_rep);
                SetResult("TransactionAmt" + rowNum, transaction.TransactionAmt_rep);
                SetResult("PmtDir" + rowNum, transaction.PmtDir_rep);
                SetResult("Entity" + rowNum, transaction.Entity);
                SetResult("TransNum" + rowNum, transaction.TransNum);
                SetResult("PmtMade" + rowNum, transaction.PmtMade);
                SetResult("Notes" + rowNum, transaction.Notes);
            }

            SetResult("sTransactionsTotPmtsRecvd", dataLoan.sTransactionsTotPmtsRecvd_rep);
            SetResult("sTransactionsTotPmtsMade", dataLoan.sTransactionsTotPmtsMade_rep);
            SetResult("sTransactionsNetPmtsToDate", dataLoan.sTransactionsNetPmtsToDate_rep);
            SetResult("sTransactionsTotPmtsReceivable", dataLoan.sTransactionsTotPmtsReceivable_rep);
            SetResult("sTransactionsTotPmtsPayable", dataLoan.sTransactionsTotPmtsPayable_rep);
            SetResult("sTransactionsNetPmtsDue", dataLoan.sTransactionsNetPmtsDue_rep);
        }
    }
    public partial class TransactionsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new TransactionsServiceItem());
        }
    }


}
