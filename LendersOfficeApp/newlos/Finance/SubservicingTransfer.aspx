﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubservicingTransfer.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.SubservicingTransfer" %>
<%@ Register TagPrefix="uc" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Servicer Info</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        table { display: inline-block; }
        label { display: inline-block; font-weight: bold; padding-left: 5px; }
        .ContactInfo label { width: 110px; }
        .SubservicerInfo { padding-top: 20px; }
        .SubservicerInfo label { width: 200px; }
        .HeaderContainer { width: 350px; }
        .MsrHeaderContainer { width: 400px; }
        .MyHeaderLabel { display: inline-block; font-weight: bold; padding-left: 5px; width: 140px; float: left; position: relative; top: 5px; }
        .MyHeaderLabel.Msr { width: 190px; }
        .MyHeaderPicker { display: inline-block; float: left; }
        .wide { width: 232px; }
        #ExpandedSubservicerContainer fieldset table, #form1 fieldset { width: 700px; }
        #form1 fieldset, #form1 table.last  { margin-top: 10px; display: block;}
        #form1 fieldset table { margin-left: 7px; margin-top: 5px;  }
        #form1 table.first { margin-left: 20px; table-layout: fixed; }
        .MainRightHeader { margin-bottom: 10px; }
        legend { font-weight: bolder;  }
        input.address, input.attention, input.company { width: 250px; }
        input.zip { width: 70px; }
        input.email { width: 150px; }
        input.contactname {width: 225px; }
        input.investor { width: 200px; }
        input.mers, input.seller { width: 95px; }
        span.usecb input { margin-left: -3px; position: relative;}
        .MsrDates { padding-top: 20px; }
        .PadBottom { padding-bottom: 20px; }
        .MsrDates label { width: 200px; }
        label.EmailLabel { width: 40px; }
    </style>
    
    <script type="text/javascript">
        $(function() {
            $('#sSubservicerRolodexEntries').change(function() {
                callWebMethodAsync({
                    type: "POST",
                    url: "SubservicingTransfer.aspx/GetInvestorInfo",
                    data: '{ id :' + $(this).val() + '}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(msg) {
                        $('#SellerId').val(msg.d.SellerId);
                        $('#MersOrganizationId').val(msg.d.MERSOrganizationId);
                        $('#CompanyName').val(msg.d.CompanyName);
                        $('#MainContactName').val(msg.d.MainContactName);
                        $('#MainAttention').val(msg.d.MainAttention);
                        $('#MainEmail').val(msg.d.MainEmail);
                        $('#MainAddress').val(msg.d.MainAddress);
                        $('#MainCity').val(msg.d.MainCity);
                        $('#MainState').val(msg.d.MainState);
                        $('#MainZip').val(msg.d.MainZip);
                        $('#MainPhone').val(msg.d.MainPhone);
                        $('#MainFax').val(msg.d.MainFax);
                        $('#NoteShipToAttention').val(msg.d.NoteShipToAttention);
                        $('#NoteShipToContactName').val(msg.d.NoteShipToContactName);
                        $('#NoteShipToAddress').val(msg.d.NoteShipToAddress);
                        $('#NoteShipToCity').val(msg.d.NoteShipToCity);
                        $('#NoteShipToState').val(msg.d.NoteShipToState);
                        $('#NoteShipToZip').val(msg.d.NoteShipToZip);
                        $('#NoteShipToEmail').val(msg.d.NoteShipToEmail);
                        $('#NoteShipToPhone').val(msg.d.NoteShipToPhone);
                        $('#NoteShipToFax').val(msg.d.NoteShipToFax);
                        $('#PaymentToAttention').val(msg.d.PaymentToAttention);
                        $('#PaymentToContactName').val(msg.d.PaymentToContactName);
                        $('#PaymentToAddress').val(msg.d.PaymentToAddress);
                        $('#PaymentToCity').val(msg.d.PaymentToCity);
                        $('#PaymentToState').val(msg.d.PaymentToState);
                        $('#PaymentToZip').val(msg.d.PaymentToZip);
                        $('#PaymentToEmail').val(msg.d.PaymentToEmail);
                        $('#PaymentToPhone').val(msg.d.PaymentToPhone);
                        $('#PaymentToFax').val(msg.d.PaymentToFax);
                        $('#LossPayeeAttention').val(msg.d.LossPayeeAttention);
                        $('#LossPayeeContactName').val(msg.d.LossPayeeContactName);
                        $('#LossPayeeAddress').val(msg.d.LossPayeeAddress);
                        $('#LossPayeeCity').val(msg.d.LossPayeeCity);
                        $('#LossPayeeState').val(msg.d.LossPayeeState);
                        $('#LossPayeeZip').val(msg.d.LossPayeeZip);
                        $('#LossPayeeEmail').val(msg.d.LossPayeeEmail);
                        $('#LossPayeePhone').val(msg.d.LossPayeePhone);
                        $('#LossPayeeFax').val(msg.d.LossPayeeFax);
                        $('#sHmdaPurchaser2004T').val(msg.d.HmdaPurchaser2004T);
                        $('#MERSOrganizationID').val(msg.d.MERSOrganizationId);
                        $('#SellerID').val(msg.d.SellerId);
                    },
                    error: function() {
                        alert('Could not fetch investor data.');
                    }
                });
            });
        });
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="MainRightHeader">Subservicing Transfer</div>
    <div>
        <div class="MsrHeaderContainer">
            <div class="MyHeaderLabel Msr">Mortgage Servicing Rights Sold To</div>
            <div class="MyHeaderPicker">
                <uc:cfm runat="server" ID="MsrCfm" 
                    CompanyNameField="MortgageServicingRightsHolderCompanyNm"
                    StreetAddressField="MortgageServicingRightsHolderStreetAddr"
                    CityField="MortgageServicingRightsHolderCity"
                    StateField="MortgageServicingRightsHolderState"
                    ZipField="MortgageServicingRightsHolderZip"
                    CompanyPhoneField="MortgageServicingRightsHolderPhoneOfCompany"
                    EmailField="MortgageServicingRightsHolderEmailAddr" />
            </div>
        </div>
        <div style="clear: both;"></div>
        <div class="ContactInfo">
            <div><label for="MortgageServicingRightsHolderCompanyNm">Company Name</label><asp:TextBox runat="server" ID="MortgageServicingRightsHolderCompanyNm" class="wide"></asp:TextBox></div>
            <div><label for="MortgageServicingRightsHolderStreetAddr">Address</label><asp:TextBox runat="server" ID="MortgageServicingRightsHolderStreetAddr" class="wide"></asp:TextBox></div>
            <div>
                <label></label>
                <asp:TextBox runat="server" ID="MortgageServicingRightsHolderCity"></asp:TextBox>
                <ml:StateDropDownList runat="server" ID="MortgageServicingRightsHolderState" />
                <ml:ZipcodeTextBox runat="server" ID="MortgageServicingRightsHolderZip"></ml:ZipcodeTextBox>
            </div>
            <div>
                <label for="MortgageServicingRightsHolderPhoneOfCompany">Company Phone</label><asp:Textbox runat="server" ID="MortgageServicingRightsHolderPhoneOfCompany" preset="phone"></asp:Textbox>
                <label class="EmailLabel" for="MortgageServicingRightsHolderEmailAddr">Email</label><asp:TextBox runat="server" ID="MortgageServicingRightsHolderEmailAddr" class="wide"></asp:TextBox>
            </div>
        </div>
    </div>
        
    <div id="MsrDates" runat="server" class="MsrDates">
        <div><label for="sMortgageServicingRightsSalesDate">MSR Sales Date</label><ml:DateTextBox runat="server" ID="sMortgageServicingRightsSalesDate"></ml:DateTextBox></div>
        <div><label for="sMortgageServicingRightsTransferDate">MSR Transfer Date</label><ml:DateTextBox runat="server" ID="sMortgageServicingRightsTransferDate"></ml:DateTextBox></div>
        <div><label for="sMortgageServicingRightsHolderFirstPaymentDueDate">First Payment Due to MSR Holder</label><ml:DateTextBox runat="server" ID="sMortgageServicingRightsHolderFirstPaymentDueDate"></ml:DateTextBox></div>
    </div>

    <div runat="server" id="NormalSubservicerContactContainer">
        <div class="HeaderContainer">
            <div class="MyHeaderLabel">Subservicer Contact Info</div>
            <div class="MyHeaderPicker">
                <uc:cfm runat="server" ID="CFM" 
                    CompanyNameField="SubservicerCompanyNm"
                    StreetAddressField="SubservicerStreetAddr"
                    CityField="SubservicerCity"
                    StateField="SubservicerState"
                    ZipField="SubservicerZip"
                    CompanyPhoneField="SubservicerPhoneOfCompany"
                    EmailField="SubservicerEmailAddr" />
            </div>
        </div>
        <div style="clear: both;"></div>
        <div class="ContactInfo">
            <div><label for="SubservicerCompanyNm">Company Name</label><asp:TextBox runat="server" ID="SubservicerCompanyNm" class="wide"></asp:TextBox></div>
            <div><label for="SubservicerStreetAddr">Address</label><asp:TextBox runat="server" ID="SubservicerStreetAddr" class="wide"></asp:TextBox></div>
            <div>
                <label></label>
                <asp:TextBox runat="server" ID="SubservicerCity"></asp:TextBox>
                <ml:StateDropDownList runat="server" ID="SubservicerState" />
                <ml:ZipcodeTextBox runat="server" ID="SubservicerZip"></ml:ZipcodeTextBox>
            </div>
            <div>
                <label for="SubservicerPhoneOfCompany">Company Phone</label><asp:Textbox runat="server" ID="SubservicerPhoneOfCompany" preset="phone"></asp:Textbox>
                <label class="EmailLabel" for="SubservicerEmailAddr">Email</label><asp:TextBox runat="server" ID="SubservicerEmailAddr" class="wide"></asp:TextBox>
            </div>
        </div>
    </div>
    
    <div class="SubservicerInfo">
        <div><label for="sSubservicerLoanNm">Subservicer loan number</label><asp:TextBox runat="server" ID="sSubservicerLoanNm" class="wide"></asp:TextBox></div>
        <div runat="server" id="sSubservicerMersIdContainer"><label for="sSubservicerMersId">Subservicer MERS ID</label><asp:TextBox runat="server" ID="sSubservicerMersId" MaxLength="7" Width="75px"></asp:TextBox></div>
        <div><label for="sGLServTransEffD">Subservicing Transfer Effective Date</label><ml:DateTextBox runat="server" ID="sGLServTransEffD"></ml:DateTextBox></div>
    </div>
    
    <div id="ExpandedSubservicerContainer" runat="server" style="display:none;">
    <table>
        <tr>
            <td><label for="sSubservicerLoanNm">Subservicer</label></td>
            <td><asp:DropDownList runat="server" ID="sSubservicerRolodexEntries"></asp:DropDownList></td>
        </tr>
        <tr>
            <td><label for="SellerID">Seller ID</label></td>
            <td><asp:TextBox ReadOnly="true" ID="SellerID" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td><label for="MERSOrganizationID">MERS Organization ID</label></td>
            <td><asp:TextBox ReadOnly="true" ID="MERSOrganizationID" runat="server"></asp:TextBox></td>
        </tr>
    </table>
        
        
        <fieldset style="margin-left: 10px">
        <legend>Main Address</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label5" runat="server" AssociatedControlID="CompanyName">Company Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="CompanyName" ReadOnly="true" CssClass="company"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label6" runat="server" AssociatedControlID="MainContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label7" runat="server" AssociatedControlID="MainAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label8" runat="server" AssociatedControlID="MainEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label9" runat="server" AssociatedControlID="MainAddress" >Address</ml:EncodedLabel>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="MainAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="MainCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="MainState" CssClass="state" BackColor="Gainsboro" ReadOnly="true" Enabled="false" />
                    <asp:TextBox runat="server" ID="MainZip" preset="longzipcode" CssClass="zip" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label10" runat="server" AssociatedControlID="MainPhone" >Phone</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="MainPhone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label11" runat="server" AssociatedControlID="MainFax">Fax</ml:EncodedLabel>
                </td>
                <td>
                    <ml:PhoneTextBox runat="server" ID="MainFax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Note Ship To</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label12" runat="server" AssociatedControlID="NoteShipToAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="NoteShipToAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label13" runat="server" AssociatedControlID="NoteShipToContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="NoteShipToContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
          
       
                <td>
                    <ml:EncodedLabel ID="Label16" runat="server" AssociatedControlID="NoteShipToAddress">Address</ml:EncodedLabel>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="NoteShipToAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
                    <td>
                    <ml:EncodedLabel ID="Label15" runat="server" AssociatedControlID="NoteShipToEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="NoteShipToEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="NoteShipToCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="NoteShipToState"  CssClass="state" Enabled="false"/>
                    <asp:TextBox runat="server" ID="NoteShipToZip" preset="longzipcode" CssClass="zip" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label17" runat="server" AssociatedControlID="NoteShipToPhone">Phone</ml:EncodedLabel>
                </td>
                <td>
                    <ml:PhoneTextBox runat="server" ID="NoteShipToPhone" CssClass="phone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label18" runat="server" AssociatedControlID="NoteShipToFax">Fax</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="NoteShipToFax" CssClass="fax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Payment To</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label19" runat="server" AssociatedControlID="PaymentToAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PaymentToAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label20" runat="server" AssociatedControlID="PaymentToContactName" >Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PaymentToContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
      
        
     
                <td>
                    <ml:EncodedLabel ID="Label23" runat="server" AssociatedControlID="PaymentToAddress">Address</ml:EncodedLabel>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="PaymentToAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
             <td>
                    <ml:EncodedLabel ID="Label22" runat="server" AssociatedControlID="PaymentToEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PaymentToEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
                
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="PaymentToCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="PaymentToState" CssClass="state" Enabled="false" />
                    <asp:TextBox runat="server" ID="PaymentToZip" preset="longzipcode" CssClass="zip" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label24" runat="server" AssociatedControlID="PaymentToPhone" >Phone</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="PaymentToPhone" CssClass="phone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label25" runat="server" AssociatedControlID="PaymentToFax">Fax</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="PaymentToFax" CssClass="fax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Loss Payee</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label26" runat="server" AssociatedControlID="LossPayeeAttention" >Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label27" runat="server" AssociatedControlID="LossPayeeContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label30" runat="server" AssociatedControlID="LossPayeeAddress">Address</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
             <td>
                    <ml:EncodedLabel ID="Label29" runat="server" AssociatedControlID="LossPayeeEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="LossPayeeCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="LossPayeeState"  CssClass="state" Enabled="false"/>
                    <asp:TextBox runat="server" ID="LossPayeeZip" preset="longzipcode" CssClass="zip" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label31" runat="server" AssociatedControlID="LossPayeePhone">Phone</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="LossPayeePhone" CssClass="phone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label32" runat="server" AssociatedControlID="LossPayeeFax">Fax</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="LossPayeeFax" CssClass="fax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    </div>
    </form>
</body>
</html>
