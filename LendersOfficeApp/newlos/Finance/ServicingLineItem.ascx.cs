﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class ServicingLineItem : System.Web.UI.UserControl
    {
        public string Text
        {
            set { Value.Text = value; }
        }

        public string Class
        {
            set { row.Attributes.Add("class", value); }
        }

        public void setData(string principal, string interest, string escrow, string other, string lateFees)
        {
            Principal.Text = principal;
            Interest.Text = interest;
            Escrow.Text = escrow;
            Other.Text = other;
            LateFees.Text = lateFees;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}