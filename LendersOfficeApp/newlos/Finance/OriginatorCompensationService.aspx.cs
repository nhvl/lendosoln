﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;


namespace LendersOfficeApp.newlos.Finance
{
    public class OriginatorCompensationServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(OriginatorCompensationServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SetPlan":
                    SetPlan();
                    break;
                case "ClearPlan":
                    ClearPlan();
                    break;
                case "ValidateMinMaxAmount":
                    ValidateMinMaxAmount();
                    break;
            }
        }

        /// <summary>
        /// Check if there's an error regarding minimum or maximum.
        /// </summary>
        private void ValidateMinMaxAmount()
        {
            var losConvert = new LosConvert();
            string errMsg = "";
            string minAmountStr = GetString("sOriginatorCompensationMinAmount");
            string maxAmountStr = GetString("sOriginatorCompensationMaxAmount");
            bool minCheckbox = GetBool("sOriginatorCompensationHasMinAmount");
            bool maxCheckbox = GetBool("sOriginatorCompensationHasMaxAmount");
            decimal minAmount = losConvert.ToMoney(minAmountStr);
            decimal maxAmount = losConvert.ToMoney(maxAmountStr);

            if (maxCheckbox && minCheckbox && !string.IsNullOrEmpty(minAmountStr) && !string.IsNullOrEmpty(maxAmountStr) && minAmount > maxAmount)
            {
                errMsg = ErrorMessages.OriginatorCompensationMaxAmountLessThanMinAmount;
            }

            if (minCheckbox && (string.IsNullOrEmpty(minAmountStr) || minAmount <= 0.0M))
            {
                errMsg = ErrorMessages.OriginatorCompensationMinAmountRequired;
            }
            
            if (maxCheckbox && (string.IsNullOrEmpty(maxAmountStr) || maxAmount <= 0.0M))
            {
                errMsg = ErrorMessages.OriginatorCompensationMaxAmountRequired;
            }

            SetResult("err", !string.IsNullOrEmpty(errMsg));
            SetResult("errMsg", errMsg);
        }

        private void ClearPlan()
        {
            Guid sLId = GetGuid("LoanId");
            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.ClearCompensationPlan();
            dataLoan.sOriginatorCompensationPlanT = E_sOriginatorCompensationPlanT.FromEmployee;

            dataLoan.Save(); // 2/21/2011 dd - Always save to loan file when user apply compensatin plan. The save
            // section is only deal with the compensation information.

            LoadCompensationPlanData(dataLoan);
            SetResult("sFileVersion", dataLoan.sFileVersion);

        }

        private void SetPlan()
        {

            int plan = GetInt("plan"); // 0 - Manual, 1 - From Employee.
            Guid sLId = GetGuid("LoanId");

            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (plan == 0)
            {
                dataLoan.ApplyCompensationManually(
                    GetString("sOriginatorCompensationPlanAppliedD"),
                    GetString("sOriginatorCompensationEffectiveD"),
                    GetString("sOriginatorCompensationPercent"),
                    (E_PercentBaseT)GetInt("sOriginatorCompensationBaseT", (int)E_PercentBaseT.LoanAmount),
                    GetString("sOriginatorCompensationMinAmount"),
                    GetString("sOriginatorCompensationMaxAmount"),
                    GetString("sOriginatorCompensationFixedAmount"),
                    GetString("sOriginatorCompensationNotes"),
                    GetBool("sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo")
                );
                dataLoan.sOriginatorCompensationPlanT = E_sOriginatorCompensationPlanT.Manual;
                SetResult("PlanSourceExplanation", "Set manually by ");
                SetResult("PlanSourceEmployeeId", "");
            }
            else if (plan == 1)
            {
                dataLoan.ApplyCompensationFromCurrentOriginator();
                dataLoan.sOriginatorCompensationPlanT = E_sOriginatorCompensationPlanT.FromEmployee;
                SetResult("PlanSourceExplanation", "Set from the employee record of ");
                
                // Check what kind of employee it is
                Guid employeeId = dataLoan.sOriginatorCompensationPlanSourceEmployeeId;
                EmployeeDB employee = new EmployeeDB(employeeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                employee.Retrieve();
                if (employee.UserType == 'P' && BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanAdministrateExternalUsers))
                {
                    SetResult("PlanSourceEmployeeId", dataLoan.sOriginatorCompensationPlanSourceEmployeeId);
                }
                else if (BrokerUserPrincipal.CurrentPrincipal.HasRole(E_RoleT.Administrator))
                {
                    SetResult("PlanSourceEmployeeId", dataLoan.sOriginatorCompensationPlanSourceEmployeeId);
                }
                else
                {
                    SetResult("PlanSourceEmployeeId", "");
                }
                SetResult("PlanSourceEmployeeUserType", employee.UserType.ToString());
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Unhandled plan=" + plan);
            }
            dataLoan.Save(); // 2/21/2011 dd - Always save to loan file when user apply compensatin plan. The save
            // section is only deal with the compensation information.
            // The ideal solution is to only save when the user explicitly saves, but a previous conversation
            //   suggested that that would require significantly more effort.

            LoadCompensationPlanData(dataLoan); // Load data to update fields
            SetResult("sFileVersion", dataLoan.sFileVersion);

        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {

            dataLoan.sOriginatorCompensationPaymentSourceT = (E_sOriginatorCompensationPaymentSourceT)GetInt("sOriginatorCompensationPaymentSourceT");

            if (dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid)
            {
                // Breaking with convention in order to cleanly support different inputs that change the same value
                dataLoan.sOriginatorCompensationBorrPaidPc_rep = GetString("sOriginatorCompensationBorrPaidPc");
                dataLoan.sOriginatorCompensationBorrPaidBaseT = (E_PercentBaseT)GetInt("sOriginatorCompensationBorrPaidBaseT");
                dataLoan.sOriginatorCompensationBorrPaidMb_rep = GetString("sOriginatorCompensationBorrPaidMb");
            }
            else
            {
                dataLoan.sOriginatorCompensationBorrPaidPc_rep = GetString("sOriginatorCompensationUnspPaidPc");
                dataLoan.sOriginatorCompensationBorrPaidBaseT = (E_PercentBaseT)GetInt("sOriginatorCompensationUnspPaidBaseT");
                dataLoan.sOriginatorCompensationBorrPaidMb_rep = GetString("sOriginatorCompensationUnspPaidMb");
            }
            
            // calculated sOriginatorCompensationBorrTotalAmount

            dataLoan.sIsIncludeOriginatorCompensationToCommission = GetBool("sIsIncludeOriginatorCompensationToCommission");
            dataLoan.sOriginatorCompensationLenderFeeOptionT = (E_sOriginatorCompensationLenderFeeOptionT)GetInt("sOriginatorCompensationLenderFeeOptionT");

            dataLoan.sOriginatorCompensationPlanT = (E_sOriginatorCompensationPlanT)GetInt("sOriginatorCompensationPlanT");

            if (dataLoan.sOriginatorCompensationPlanT == E_sOriginatorCompensationPlanT.Manual)
            {
                dataLoan.ApplyCompensationManually(
                    GetString("sOriginatorCompensationPlanAppliedD"),
                    GetString("sOriginatorCompensationEffectiveD"),
                    GetString("sOriginatorCompensationPercent"),
                    (E_PercentBaseT)GetInt("sOriginatorCompensationBaseT"),
                    GetString("sOriginatorCompensationMinAmount"),
                    GetString("sOriginatorCompensationMaxAmount"),
                    GetString("sOriginatorCompensationFixedAmount"),
                    GetString("sOriginatorCompensationNotes"),
                    GetBool("sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo")
                );
            }

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sOriginatorCompensationBorrPaidPc", dataLoan.sOriginatorCompensationBorrPaidPc_rep);
            if (dataLoan.sOriginatorCompensationBorrPaidBaseT == E_PercentBaseT.AllYSP)
            {
                SetResult("sOriginatorCompensationBorrPaidBaseT", E_PercentBaseT.LoanAmount);
            }
            else
            {
                SetResult("sOriginatorCompensationBorrPaidBaseT", dataLoan.sOriginatorCompensationBorrPaidBaseT);
            }
            SetResult("sOriginatorCompensationBorrPaidMb", dataLoan.sOriginatorCompensationBorrPaidMb_rep);
            SetResult("sOriginatorCompensationBorrTotalAmount", dataLoan.sOriginatorCompensationBorrTotalAmount_rep);

            // UnspecifiedPaid also uses the BorrPaid fields
            SetResult("sOriginatorCompensationUnspPaidPc", dataLoan.sOriginatorCompensationBorrPaidPc_rep);
            SetResult("sOriginatorCompensationUnspPaidBaseT", dataLoan.sOriginatorCompensationBorrPaidBaseT);
            SetResult("sOriginatorCompensationUnspPaidMb", dataLoan.sOriginatorCompensationBorrPaidMb_rep);
            SetResult("sOriginatorCompensationUnspTotalAmount", dataLoan.sOriginatorCompensationBorrTotalAmount_rep);

            SetResult("sOriginatorCompensationLenderFeeOptionT", dataLoan.sOriginatorCompensationLenderFeeOptionT);
            SetResult("sIsIncludeOriginatorCompensationToCommission", dataLoan.sIsIncludeOriginatorCompensationToCommission);

            LoadCompensationPlanData(dataLoan);

        }

        private void LoadCompensationPlanData(CPageData dataLoan)
        {
            string zeroStr = "$0.00";
            SetResult("sOriginatorCompensationAuditD", dataLoan.sOriginatorCompensationAuditD_rep);
            if (dataLoan.sOriginatorCompensationPlanT == E_sOriginatorCompensationPlanT.Manual)
            {
                SetResult("sOriginatorCompensationPlanSourceEmployeeName", dataLoan.sOriginatorCompensationAppliedBy);

                if (dataLoan.sOriginatorCompensationMinAmount_rep != zeroStr)
                {
                    SetResult("sOriginatorCompensationMinAmount", dataLoan.sOriginatorCompensationMinAmount_rep);
                }
                if (dataLoan.sOriginatorCompensationMaxAmount_rep != zeroStr)
                {
                    SetResult("sOriginatorCompensationMaxAmount", dataLoan.sOriginatorCompensationMaxAmount_rep);
                }
                //SetResult("sOriginatorCompensationHasMinAmount", dataLoan.sOriginatorCompensationHasMinAmount);
                //SetResult("sOriginatorCompensationHasMaxAmount", dataLoan.sOriginatorCompensationHasMaxAmount);
            }
            else
            {
                SetResult("sOriginatorCompensationPlanSourceEmployeeName", dataLoan.sOriginatorCompensationPlanSourceEmployeeName);
                bool hasMinAmount = dataLoan.sOriginatorCompensationHasMinAmount;
                bool hasMaxAmount = dataLoan.sOriginatorCompensationHasMaxAmount;
                SetResult("sOriginatorCompensationHasMinAmount", hasMinAmount);
                SetResult("sOriginatorCompensationHasMaxAmount", hasMaxAmount);
                if (hasMinAmount)
                {
                    SetResult("sOriginatorCompensationMinAmount", dataLoan.sOriginatorCompensationMinAmount_rep);
                }
                else
                {
                    SetResult("sOriginatorCompensationMinAmount", "");
                }
                if (hasMaxAmount)
                {
                    SetResult("sOriginatorCompensationMaxAmount", dataLoan.sOriginatorCompensationMaxAmount_rep);
                }
                else
                {
                    SetResult("sOriginatorCompensationMaxAmount", "");
                }
                
            }

            SetResult("sOriginatorCompensationPlanAppliedD", dataLoan.sOriginatorCompensationPlanAppliedD_rep);
            SetResult("sOriginatorCompensationEffectiveD", dataLoan.sOriginatorCompensationEffectiveD_rep);
            SetResult("sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo", dataLoan.sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo);
            SetResult("sOriginatorCompensationPercent", dataLoan.sOriginatorCompensationPercent_rep);
            SetResult("sOriginatorCompensationBaseT", dataLoan.sOriginatorCompensationBaseT);
            SetResult("sOriginatorCompensationAdjBaseAmount", dataLoan.sOriginatorCompensationAdjBaseAmount_rep);
            SetResult("sOriginatorCompensationFixedAmount", dataLoan.sOriginatorCompensationFixedAmount_rep);
            SetResult("sOriginatorCompensationTotalAmount", dataLoan.sOriginatorCompensationTotalAmount_rep);

            SetResult("sOriginatorCompensationNotes", dataLoan.sOriginatorCompensationNotes);
        }

    }
    public partial class OriginatorCompensationService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override bool PerformResultOptimization
        {
            get
            {
                return true;
            }
        }
        protected override void Initialize()
        {
            AddBackgroundItem("", new OriginatorCompensationServiceItem());
        }
    }

}
