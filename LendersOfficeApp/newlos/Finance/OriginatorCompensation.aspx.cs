﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOfficeApp.los.LegalForm;
using System.Data.SqlClient;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Finance
{
    /// <summary>
    /// How does this page work? I am almost certain that I will completely forget.
    /// This page is unique in that in hides some fields until the user selects one of the
    /// radio buttons.
    /// Stuff to remember:
    /// The first time you load this page, the lender paid div will be empty.
    /// Hide PaymentSourceUnspecifiedDiv when sIsOriginationCompensationSourceRequired.
    /// If you try to assign a nonexistent loan officer's plan, weird stuff will happen.
    /// Those lines with the percent, drop down list, and fixed amount for unspecified/borrower?
    ///     Those are two divs that contribute to the same data fields.
    /// This class will set some entries to be read by the javascript in the aspx file.
    /// That javascript must only run when the page is ready, which is why we had to put it in
    ///     the jquery ready function in the body statement.
    /// </summary>
    public partial class OriginatorCompensation : BaseLoanPage
    {
        // 2/21/2011 dd - Based from the specs we are restrict the write access to users with finance write permission.
        // We only need to enforce at the page level.
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite };
            }
        }

        protected string _PaymentSourceDiv = "";
        protected int _PlanT = -1; // This -1 will only happen if no plan is set.
        protected int _LenderDivHidden = 0;

        private void PaymentSourceTSelect(E_sOriginatorCompensationPaymentSourceT paymentSource)
        {
            switch (paymentSource)
            {
                // The $j(document).ready function in the .aspx file will handle these
                // The document needs to be ready before we can move around the payment information
                case E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified:
                    PaymentSourceSelectNotSpecified.Checked = true;
                    _PaymentSourceDiv = "PaymentSourceUnspecifiedLoanAmount";
                    break;
                case E_sOriginatorCompensationPaymentSourceT.BorrowerPaid:
                    PaymentSourceSelectBorrowerPaid.Checked = true;
                    _PaymentSourceDiv = "PaymentSourceBorrowerLoanAmount";
                    break;
                case E_sOriginatorCompensationPaymentSourceT.LenderPaid:
                    PaymentSourceSelectLenderPaid.Checked = true;
                    _PaymentSourceDiv = "PaymentSourceLenderLoanAmount";
                    break;
                default:
                    throw new UnhandledEnumException(paymentSource);
            }
        }

        private void LenderFeeOptionSelect(E_sOriginatorCompensationLenderFeeOptionT lenderFeeOption )
        {
            switch (lenderFeeOption)
            {
                case E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees:
                    LenderFeesAddition.Checked = true;
                    break;
                case E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees:
                    LenderFeesIncluded.Checked = true;
                    break;
                default:
                    throw new UnhandledEnumException(lenderFeeOption);
            }
        }

        private void PlanSelect(E_sOriginatorCompensationPlanT plan, string appliedBy, Guid sourceEmployeeId, string sourceEmployeeName)
        {
            // _PlanT will be used when the page is ready to select the plan
            switch (plan) {
                case E_sOriginatorCompensationPlanT.Manual:
                    _PlanT = 0;
                    PlanManual.Checked = true;
                    sOriginatorCompensationPlanSourceEmployeeName.Text = AspxTools.HtmlString(appliedBy);
                    PlanSourceExplanation.Text = "Set manually by ";
                    break;
                case E_sOriginatorCompensationPlanT.FromEmployee:
                    _PlanT = 1;
                    PlanFromEmployee.Checked = true;
                    // Display a link to the employee if the user can access the employee page
                    if (sourceEmployeeName == "")
                    {
                        // This situation should be RARE
                        PlanSourceExplanation.Text = "Set from the employee record, ";
                    }
                    else
                    {
                        PlanSourceExplanation.Text = "Set from the employee record of ";
                    }

                    // Check what kind of employee it is
                    Guid employeeId = sourceEmployeeId;
                    EmployeeDB employee = new EmployeeDB(employeeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                    employee.Retrieve();
                    string editorLink;
                    if (employee.UserType == 'P' && BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanAdministrateExternalUsers))
                    {
                        editorLink = "/los/BrokerAdmin/EditPmlUser.aspx?cmd=edit&employeeId=";
                        sOriginatorCompensationPlanSourceEmployeeName.Text =
                            string.Format("<a href=\"#\" onclick=\"{0}\">{1}</a>",
                                "showModal('" + editorLink + sourceEmployeeId.ToString() + "', null, null, null, null,{width:890, hideCloseButton: true});",
                                AspxTools.JsStringUnquoted(sourceEmployeeName));
                    }
                    else if (employee.UserType != 'P' && BrokerUser.HasRole(E_RoleT.Administrator))
                    {
                        editorLink = "/los/admin/EditEmployee.aspx?cmd=edit&employeeId=";
                        sOriginatorCompensationPlanSourceEmployeeName.Text =
                            string.Format("<a href=\"#\" onclick=\"{0}\">{1}</a>",
                                "showModal('" + editorLink + sourceEmployeeId.ToString() + "', null, null, null, null,{width:1000, hideCloseButton: true});",
                                AspxTools.JsStringUnquoted(sourceEmployeeName));
                    }
                    else
                    {
                        sOriginatorCompensationPlanSourceEmployeeName.Text = AspxTools.HtmlString(sourceEmployeeName);
                    }
                    break;
                default:
                    throw new UnhandledEnumException(plan);
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(OriginatorCompensation));
            dataLoan.InitLoad();

            // Unspecified / BorrowerPaid / LenderPaid
            PaymentSourceTSelect(dataLoan.sOriginatorCompensationPaymentSourceT);

            // Breaking with convention in order to cleanly support different inputs that change the same value
            sOriginatorCompensationBorrPaidPc.Text = dataLoan.sOriginatorCompensationBorrPaidPc_rep;
            Tools.Bind_PercentBaseLoanAmountsT(sOriginatorCompensationBorrPaidBaseT);
            Tools.SetDropDownListValue(sOriginatorCompensationBorrPaidBaseT, dataLoan.sOriginatorCompensationBorrPaidBaseT);
            // if the DDL is set to allYSP, what happens?
            sOriginatorCompensationBorrPaidMb.Text = dataLoan.sOriginatorCompensationBorrPaidMb_rep;
            sOriginatorCompensationBorrTotalAmount.Text = dataLoan.sOriginatorCompensationBorrTotalAmount_rep;

            sOriginatorCompensationUnspPaidPc.Text = dataLoan.sOriginatorCompensationBorrPaidPc_rep;
            Tools.Bind_sGfeOriginatorCompFBaseT(sOriginatorCompensationUnspPaidBaseT);
            Tools.SetDropDownListValue(sOriginatorCompensationUnspPaidBaseT, dataLoan.sOriginatorCompensationBorrPaidBaseT);
            if (dataLoan.sOriginatorCompensationBorrPaidBaseT == E_PercentBaseT.AllYSP)
            {
                sOriginatorCompensationUnspPaidPc.Attributes.Add("readonly", "readonly");
                Tools.SetDropDownListValue(sOriginatorCompensationBorrPaidBaseT, E_PercentBaseT.LoanAmount);
            }
            sOriginatorCompensationUnspPaidMb.Text = dataLoan.sOriginatorCompensationBorrPaidMb_rep;
            sOriginatorCompensationUnspTotalAmount.Text = dataLoan.sOriginatorCompensationBorrTotalAmount_rep;
            
            // Included in lender fees / In addition to lender fees
            LenderFeeOptionSelect(dataLoan.sOriginatorCompensationLenderFeeOptionT);

            // Hide PaymentSourceUnspecifiedDiv when sIsOriginationCompensationSourceRequired
            if (dataLoan.sIsOriginationCompensationSourceRequired)
            {
                PaymentSourceUnspecifiedDiv.Style.Value = "display:none";
            }

            // Include compensation on the commissions page
            sIsIncludeOriginatorCompensationToCommission.Checked = dataLoan.sIsIncludeOriginatorCompensationToCommission;

            // Manual / From employee
            // Employee ID
            E_sOriginatorCompensationPlanT plan;
            if (!dataLoan.sHasOriginatorCompensationPlan)
            {
                // This happens on the first load ever
                _LenderDivHidden = 1;
                _PlanT = -1;
                sOriginatorCompensationAuditD.Text = "";
            }
            else
            {
                plan = dataLoan.sOriginatorCompensationPlanT;
                PlanSelect(plan,
                    dataLoan.sOriginatorCompensationAppliedBy,
                    dataLoan.sOriginatorCompensationPlanSourceEmployeeId,
                    dataLoan.sOriginatorCompensationPlanSourceEmployeeName);

                // Audit date - the datetime when compensation plan was applied
                sOriginatorCompensationAuditD.Text = dataLoan.sOriginatorCompensationAuditD_rep;
            }
            
            

            // Set date
            sOriginatorCompensationPlanAppliedD.Text = dataLoan.sOriginatorCompensationPlanAppliedD_rep;
            // Effective date
            sOriginatorCompensationEffectiveD.Text = dataLoan.sOriginatorCompensationEffectiveD_rep;

            sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo.Checked = dataLoan.sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo;

            sOriginatorCompensationPercent.Text = dataLoan.sOriginatorCompensationPercent_rep;
            // Loan Amount Type
            Tools.Bind_PercentBaseLoanAmountsT(sOriginatorCompensationBaseT);
            Tools.SetDropDownListValue(sOriginatorCompensationBaseT, dataLoan.sOriginatorCompensationBaseT);

            sOriginatorCompensationAdjBaseAmount.Text = dataLoan.sOriginatorCompensationAdjBaseAmount_rep;

            sOriginatorCompensationMinAmount.Text = dataLoan.sOriginatorCompensationMinAmount_rep;
            sOriginatorCompensationMaxAmount.Text = dataLoan.sOriginatorCompensationMaxAmount_rep;
            sOriginatorCompensationHasMinAmount.Checked = dataLoan.sOriginatorCompensationHasMinAmount;
            if (sOriginatorCompensationHasMinAmount.Checked)
            {
                sOriginatorCompensationMinAmount.Text = dataLoan.sOriginatorCompensationMinAmount_rep;
            }
            else
            {
                sOriginatorCompensationMinAmount.Text = "";
            }
            sOriginatorCompensationHasMaxAmount.Checked = dataLoan.sOriginatorCompensationHasMaxAmount;
            if (sOriginatorCompensationHasMaxAmount.Checked)
            {
                sOriginatorCompensationMaxAmount.Text = dataLoan.sOriginatorCompensationMaxAmount_rep;
            }
            else
            {
                sOriginatorCompensationMaxAmount.Text = "";
            }

            sOriginatorCompensationFixedAmount.Text = dataLoan.sOriginatorCompensationFixedAmount_rep;
            sOriginatorCompensationTotalAmount.Text = dataLoan.sOriginatorCompensationTotalAmount_rep;

            sOriginatorCompensationNotes.Text = dataLoan.sOriginatorCompensationNotes;

            IsComboSecond.Value = (dataLoan.sLienPosT == E_sLienPosT.Second
                && !dataLoan.sIsStandAlone2ndLien) ? "1" : "0";
        }

        protected override void PostData()
        {
            LoadData();
            base.PostData();
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
            this.PageTitle = "Originator Compensation";
            this.PageID = "OriginatorCompensation";
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
