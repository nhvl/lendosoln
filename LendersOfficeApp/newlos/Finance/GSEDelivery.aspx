﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GSEDelivery.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.GSEDelivery" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GSE Delivery</title>
    <script type="text/javascript" src="../../inc/utilities.js" ></script>
</head>
<body bgcolor="gainsboro">
    <script language="javascript">
        var initRanFirstTime = false;
        
        function Page_ClientValidate() {
            var obj = document.getElementById('<%= AspxTools.ClientId(sGseDeliveryComments) %>');
            if( obj ) {
                var count = parseInt( obj.getAttribute('maxcharactercount') );
                var val = obj.value.length - count; 
                if( obj.value.length > count ) {
                    var fail = confirm("Your Comments to GSE are too long. You will not be able to save unless they are shortened by "+ val +" character(s). \n\r\n\rWould you like to automaticaly shorten?"); 
                    if( fail ) {
                        shortenTextArea(obj);
                        return true;
                    } 
                    return false; 
                }
            }
            return true;
        }

        function toggleFreddieMacProductTVisibility(refreshCalc) {
            if (refreshCalc) {
                refreshCalculation()
            }
            var targetDDL = document.getElementById("sGseDeliveryTargetT");
            var productDDL = document.getElementById("sGseFreddieMacProductT");
            var productLabel = document.getElementById("sGseFreddieMacProductTLabel");
            if (targetDDL.value == 1) {
                productDDL.style.display = '';
                productLabel.style.display = '';
            }
            else {
                productDDL.style.display = 'none';
                productLabel.style.display = 'none';
            }
        }
        
        function _init() {
            if( !initRanFirstTime ) {
                TextareaUtilities.LengthCheckInit();
                initRanFirstTime = true;
            }
            
            lockField(<%=AspxTools.JsGetElementById(sGseDeliveryServicerIdLckd)%>, 'sGseDeliveryServicerId');
            lockField(<%=AspxTools.JsGetElementById(sGseDeliveryPayeeIdLckd)%>, 'sGseDeliveryPayeeId');
            lockField(<%=AspxTools.JsGetElementById(sGseDeliveryGuaranteeFLckd)%>, 'sGseDeliveryGuaranteeFPc');
            
            var deliveryTypeDDL = <%= AspxTools.JsGetElementById(sGseDeliveryLoanDeliveryT) %>;
            var deliveryType = deliveryTypeDDL.options[deliveryTypeDDL.selectedIndex].value;
            var displayForMBSFields = '';//deliveryType == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseDeliveryLoanDeliveryT.MBS)) %> ? '' : 'none';
            
            reoPartyDDL.style.display = displayForMBSFields;
            reoPartyLbl.style.display = displayForMBSFields;
            lossPartyDDL.style.display = displayForMBSFields;
            lossPartyLbl.style.display = displayForMBSFields;
            upbAmtTb.style.display = displayForMBSFields;
            upbAmtLbl.style.display = displayForMBSFields;
            issueDTb.style.display = displayForMBSFields;
            issueDLbl.style.display = displayForMBSFields;
            remittanceTb.style.display = displayForMBSFields;
            remittanceLbl.style.display = displayForMBSFields;
            guaranteeTb.style.display = displayForMBSFields;
            guaranteeLbl.style.display = displayForMBSFields;
            buyTb.style.display = displayForMBSFields;
            buyLbl.style.display = displayForMBSFields;
            baseGuaranteeTb.style.display = displayForMBSFields;
            baseGuaranteeLbl.style.display = displayForMBSFields;
            
            // sGseDeliveryRemittanceNumOfDays can only be in the range of 1 - 31 or blank
            var remittanceDaysTb = <%= AspxTools.JsGetElementById(sGseDeliveryRemittanceNumOfDays) %>;
            if (remittanceDaysTb.value == '0') {
                remittanceDaysTb.value = '';
            }
            
            toggleFreddieMacProductTVisibility(false);
        }
  
        function shortenTextArea(obj) 
        {
            obj.value = obj.value.substring(0, obj.getAttribute('maxcharactercount')); 
            obj.onkeyup(); 
        }

        function doAfterDateFormat(e)
        {
            refreshCalculation();
        }
    </script>
    <form id="form1" runat="server">
        <table cellSpacing="0" cellPadding="0" width="100%">
			<tr>
				<td class="MainRightHeader" noWrap>
				    GSE Delivery
				</td>
			</tr>
			<tr>
				<td>
				    <table class="FieldLabel">
				        <tr>
				            <td>
				                Target GSE
				            </td>
				            <td>
				                <asp:DropDownList ID="sGseDeliveryTargetT" runat="server" onchange="toggleFreddieMacProductTVisibility(true);"></asp:DropDownList>
				            </td>
				            <td colspan=2></td>
				        </tr>
                        <tr>
                            <td>
                                GSE Loan Number
                            </td>
                            <td>
                                <input type="text" id="sGseInvestorLoanIdentifier" runat="server" />
                            </td>
                        </tr>
				        <tr>
				            <td>
    				            GSE Remittance Type
				            </td>
				            <td>
				                <asp:DropDownList ID="sGseDeliveryRemittanceT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
				            </td>
				            <td>
				                Loan Delivery Type
				            </td>
				            <td>
				                <asp:DropDownList ID="sGseDeliveryLoanDeliveryT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            GSE Ownership %
				            </td>
				            <td>
								<ml:percenttextbox id="sGseDeliveryOwnershipPc" Width="60px" runat="server" onchange="refreshCalculation();" decimalDigits="4"></ml:percenttextbox>
				            </td>
				            <td id="baseGuaranteeLbl">
				                Base Guarantee Fee 
				            </td>
				            <td id="baseGuaranteeTb">
				                <asp:TextBox ID="sGseDeliveryBaseGuaranteeFPc" Width="60px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            GSE Product Plan ID
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryPlanId" Width="80px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				            <td id="buyLbl">
				                Buy up (+)/Buy down (-) 
				            </td>
				            <td id="buyTb">
				                <asp:TextBox ID="sGseDeliveryBuyFPc" Width="60px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            GSE Commitment ID
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryCommitmentId" Width="60px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				            <td id="guaranteeLbl">
				                Guarantee Fee 
				            </td>
				            <td id="guaranteeTb">
				                <asp:TextBox ID="sGseDeliveryGuaranteeFPc" Width="60px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				                <asp:CheckBox ID="sGseDeliveryGuaranteeFLckd" runat="server" onclick="refreshCalculation();" />
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            GSE Contract ID
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryContractId" Width="60px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				            <td>
				                Servicing Fee
				            </td>
				            <td>
				                <ml:PercentTextBox ID="sGseDeliveryServicingFPc" Width="60px" runat="server"></ml:PercentTextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            Lender ID
				            </td>
				            <td>
				                <asp:TextBox ID="sLenNum" runat="server" Width="80px" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				            <td>
				                Servicing Fee (if flat amt)
				            </td>
				            <td>
				                <ml:MoneyTextBox ID="sGseDeliveryServicingF" Width="60px" runat="server"></ml:MoneyTextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            Servicer ID
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryServicerId" Width="80px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				                <asp:CheckBox ID="sGseDeliveryServicerIdLckd" runat="server" onclick="refreshCalculation();" />
				            </td>
				            <td id="remittanceLbl">
				                Remittance Day
				            </td>
				            <td id="remittanceTb">
				                <asp:TextBox ID="sGseDeliveryRemittanceNumOfDays" Width="30px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                Payee
				            </td>
				            <td>
				                <asp:DropDownList id="sGseDeliveryPayeeT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
				            </td>
				            <td id="issueDLbl">
				                Issue Date
				            </td>
				            <td id="issueDTb">
    	                        <ml:DateTextBox ID="sGseDeliveryIssueD" runat="server" width="75" onchange="date_onblur(null, this);" />
				            </td>				           
				        </tr>
				        <tr>
				            <td>
				                Payee ID
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryPayeeId" Width="80px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				                <asp:CheckBox ID="sGseDeliveryPayeeIdLckd" runat="server" onclick="refreshCalculation();" />				                
				            </td>
				             <td id="upbAmtLbl">
				                Scheduled UPB Amount <br/>
				                on Issue Date
				            </td>
				            <td id="upbAmtTb">
				                <asp:TextBox ID="sGseDeliveryScheduleUPBAmt" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            Document Custodian Name
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryDocumentCustodianName" Width="100px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				            <td id="lossPartyLbl">
				                Loan Default Loss Party Type
				            </td>
				            <td id="lossPartyDDL">
				                <asp:DropDownList ID="sGseDeliveryLoanDefaultLossPartyT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            Document Custodian ID
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryDocumentCustodianId" Width="80px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				            <td id="reoPartyLbl">
				                REO Marketing Party Type
				            </td>
				            <td id="reoPartyDDL">
				                <asp:DropDownList ID="sGseDeliveryREOMarketingPartyT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
				            </td>
				        </tr>
				        <tr>
				            <td style="padding-top:25px">
				                <span id="sGseFreddieMacProductTLabel">Freddie Mac Product</span>
				            </td>
				            <td style="padding-top:25px">
				                <asp:DropDownList ID="sGseFreddieMacProductT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            GSE Feature ID 1
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryFeature1Id" Width="40px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>

                            <td>
    				            GSE Feature ID 6
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryFeature6Id" Width="40px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            GSE Feature ID 2
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryFeature2Id" Width="40px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>

                            <td>
    				            GSE Feature ID 7
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryFeature7Id" Width="40px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            GSE Feature ID 3
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryFeature3Id" Width="40px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>

                            <td>
    				            GSE Feature ID 8
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryFeature8Id" Width="40px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            GSE Feature ID 4
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryFeature4Id" Width="40px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>

                            <td>
    				            GSE Feature ID 9
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryFeature9Id" Width="40px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
    				            GSE Feature ID 5
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryFeature5Id" Width="40px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>

                            <td>
    				            GSE Feature ID 10
				            </td>
				            <td>
				                <asp:TextBox ID="sGseDeliveryFeature10Id" Width="40px" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td valign="top">
				                Comments to GSE
				            </td>
				            <td colspan="3">
				                <asp:TextBox maxcharactercount="60" MaxLength="60" Width="200px" Rows="3" runat="server" ID="sGseDeliveryComments" TextMode="MultiLine" />
				            </td>
				        </tr>
				    </table>
				</td>
			</tr>
		</table>
    </form>
</body>
</html>
