﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Transactions.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.Transactions" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Transactions</title>
</head>
<body bgcolor="gainsboro">

    <style>
	    .ButtonStyle { overflow: visible; width: auto; padding: 2px;}
        .moneyField { width: 80px; }
        .dateField { width: 60px; }
        .notesField { width: 500px; }
	    .descriptionField { width: 160px; }
	    .textField { width: 100px; }
    </style>
    
    <script type="text/javascript" src="../../inc/json.js"></script>
    <script type="text/javascript" src="../../inc/AdjustmentTable.js"></script>
    <script type="text/javascript">
    <!--    
        var TransactionTable = {

            currentIndex: 0,

            tableData: null,

            tableName: 'TransactionTable',

            deleteRowBtn: 'btn_delete',

            rowsPerItem: 2,

            selectAllCheckbox: 'm_deleteTransactionsCheckbox',

            deleteRow: function(input) {
                var row = input.parent().parent();
                row.next().remove();
                row.remove();
            },

            __createAndAppendRow: function(index, data, newRow) {
                var oTBody, oTR, oTD, oInput;
                oTBody = this.tableData;

                var name_cb = 'DeleteEntry' + index;
                oTR = DynamicTable.__createAndAppendElement(oTBody, 'tr', {
                    'ItemIndex': index
                });
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_cb,
                    'name': name_cb,
                    'type': 'checkbox',
                    'DeleteEntry': index,
                    'NotForEdit': 'true'
                });
                    addEventHandler(oInput, 'click', function() { DynamicTable.uncheckDeleteAllCheckbox(TransactionTable); }, false);

                var name_des = 'ItemDescription' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_des,
                    'name': name_des,
                    'value': newRow ? '' : data.ItemDescription,
                    'type': 'text',
                    'className': 'descriptionField'
                });

                var name_transDate = 'TransactionD' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_transDate,
                    'name': name_transDate,
                    'type': 'text',
                    'value': newRow ? '' : data.TransactionD_rep,
                    'preset': 'date',
                    'class': 'mask',
                    'className': 'dateField'
                });
                oInput = DynamicTable.__createAndAppendElement(oTD, 'a', {
                    'href': '#'
                });
                oImg = DynamicTable.__createAndAppendElement(oInput, 'img', DynamicTable.calImg);
                $(oInput).on('click', function() {
                    return displayCalendar(name_transDate);
                });

                var name_transAmt = 'TransactionAmt' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_transAmt,
                    'name': name_transAmt,
                    'value': newRow ? '' : data.TransactionAmt_rep,
                    'type': 'text',
                    'preset': 'money',
                    'className': 'moneyField'
                });
                    addEventHandler(oInput, 'change', update, false);

                var name_pmtDir = 'PmtDir' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'select', {
                    'id': name_pmtDir,
                    'name': name_pmtDir
                });
                    addEventHandler(oInput, 'change', update, false);
                for (var i = 0; i < PmtDirOptions.length; i++) {
                    var val = PmtDirOptions[i];
                    var oOpt = DynamicTable.__createAndAppendElement(oInput, 'option', {
                        'value': val,
                        'innerText': val
                    });
                    if (!newRow && val == data.PmtDir_rep)
                        oOpt.setAttribute('selected', 'selected');
                }

                var name_entity = 'Entity' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_entity,
                    'name': name_entity,
                    'value': newRow ? '' : data.Entity,
                    'type': 'text',
                    'className': 'textField'
                });

                var name_trans = 'TransNum' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_trans,
                    'name': name_trans,
                    'value': newRow ? '' : data.TransNum,
                    'type': 'text',
                    'className': 'textField'
                });

                var name_pmtMade = 'PmtMade' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {});
                oTD.style.whiteSpace = 'nowrap';
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_pmtMade,
                    'name': name_pmtMade,
                    'type': 'checkbox'
                });
                    addEventHandler(oInput, 'click', update, false);
                if (!newRow && data.PmtMade)
                    oInput.setAttribute('checked', 'checked');
                oInput = DynamicTable.__createAndAppendElement(oTD, 'span', {
                    'innerHTML': 'Pmt made?'
                });

                oTR = DynamicTable.__createAndAppendElement(oTBody, 'tr', {});
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {
                    'colSpan': '2',
                    'align': 'right',
                    'innerText': 'Notes:'
                });

                var name_notes = 'Notes' + index;
                oTD = DynamicTable.__createAndAppendElement(oTR, 'td', {
                    'colSpan': '6'
                });
                oInput = DynamicTable.__createAndAppendElement(oTD, 'input', {
                    'id': name_notes,
                    'name': name_notes,
                    'value': newRow ? '' : data.Notes,
                    'type': 'text',
                    'className': 'notesField'
                });
            },

            serialize: function(index) {
                return {
                    RowNum: AdjustmentTable.__IE8NullFix(index),
                    ItemDescription: AdjustmentTable.__IE8NullFix($('#ItemDescription' + index).val()),
                    TransactionD_rep: AdjustmentTable.__IE8NullFix($('#TransactionD' + index).val()),
                    TransactionAmt_rep: AdjustmentTable.__IE8NullFix($('#TransactionAmt' + index).val()),
                    PmtDir_rep: AdjustmentTable.__IE8NullFix($('#PmtDir' + index).val()),
                    Entity: AdjustmentTable.__IE8NullFix($('#Entity' + index).val()),
                    TransNum: AdjustmentTable.__IE8NullFix($('#TransNum' + index).val()),
                    PmtMade: AdjustmentTable.__IE8NullFix($('#PmtMade' + index).prop('checked')),
                    Notes: AdjustmentTable.__IE8NullFix($('#Notes' + index).val())
                };
            }
        };
        var g_bIsInit = false;
        function _init()
        {
          if (g_bIsInit == false)
          {
            DynamicTable.instantiateTable(TransactionTable, sTransactions);
            clearDirty();
          }
          g_bIsInit = true;
        }

        function addTransaction() {
            DynamicTable.__createAndAppendRow(null, true, TransactionTable);
            updateDirtyBit();
        }
        function update() {
          refreshCalculation();
            updateDirtyBit();
        }
        function _postGetAllFormValues(args)
        {
            args.sTransactions = DynamicTable.serialize(TransactionTable);
          }
    //-->
    </script>
    
    <form id="Transactions" runat="server" onreset="return false;">
    	<table cellSpacing="0" cellPadding="0">
			<tr>
				<td class="MainRightHeader" noWrap>
				    Transactions
				</td>
			</tr>
			<tr>
				<td style="padding:4px">
					<table cellpadding="2" cellspacing="0" class="FieldLabel">
					    <tr>
					        <td>
					            Total of payments received
                            </td>
                            <td>
                            </td>
					        <td>
						        <ml:MoneyTextBox ID="sTransactionsTotPmtsRecvd" ReadOnly="true" runat="server" Width="90" />
					        </td>
					        <td>
					            Total of payments receivable
                            </td>
                            <td>
                            </td>
					        <td>
						        <ml:MoneyTextBox ID="sTransactionsTotPmtsReceivable" ReadOnly="true" runat="server" Width="90" />
					        </td>
					    </tr>
					    <tr>
					        <td>
					            Total of payments made
                            </td>
                            <td>
                                -
                            </td>
					        <td>
						        <ml:MoneyTextBox ID="sTransactionsTotPmtsMade" ReadOnly="true" runat="server" Width="90" />
					        </td>
					        <td>
					            Total of payments payable
                            </td>
                            <td>
                                -
                            </td>
					        <td>
						        <ml:MoneyTextBox ID="sTransactionsTotPmtsPayable" ReadOnly="true" runat="server" Width="90" />
					        </td>
					    </tr>
					    <tr>
					        <td>
					            Net payments to date
                            </td>
                            <td>
                                =
                            </td>
					        <td>
						        <ml:MoneyTextBox ID="sTransactionsNetPmtsToDate" ReadOnly="true" runat="server" Width="90" />
					        </td>
					        <td>
					            Net payments due
                            </td>
                            <td>
                                =
                            </td>
					        <td>
						        <ml:MoneyTextBox ID="sTransactionsNetPmtsDue" ReadOnly="true" runat="server" Width="90" />
					        </td>
					    </tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="padding:4px">
					<table id="TransactionTable" cellspacing="0" cellpadding="4" class="FieldLabel">
					    <thead>
					        <tr class="GridHeader">
					            <td>
			                        <input type="checkbox" id="m_deleteTransactionsCheckbox" onclick="DynamicTable.setDeleteCheckboxes(TransactionTable);" NotForEdit />
					            </td>
					            <td class="descriptionField">
					                Item Description
					            </td>
					            <td style="width:83px">
					                Date
					            </td>
					            <td class="moneyField">
					                Amount
					            </td>
					            <td style="width:97px">
					            </td>
					            <td class="textField">
					                Entity
					            </td>
					            <td class="textField">
					                Chk#/Trans#
					            </td>
					            <td style="width:90px">
					            </td>
					        </tr>
					    </thead>
					    <tbody>
					    </tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td style="padding:4px">
					<input type="button" id="btn_add" value="Add new transaction" onclick="addTransaction();" NoHighlight class="ButtonStyle" />
					<input type="button" id="btn_delete" value="Delete transaction" onclick="DynamicTable.deleteSelected(TransactionTable);" disabled="disabled" NoHighlight class="ButtonStyle" />
				</td>
			</tr>
		</table>
		<%-- Do not delete the following DateTextBox. The dynamically added DateTextBoxes need it to import the necessary javascript in order to work as expected --%>
        <ml:DateTextBox ID="DateTextBox1" runat="server" Visible="false"></ml:DateTextBox>
    </form>
</body>
</html>
