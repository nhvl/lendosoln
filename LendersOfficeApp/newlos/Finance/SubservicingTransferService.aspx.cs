﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class SubservicingTransferService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new SubservicingTransferServiceItem());
        }
    }

    public class SubservicingTransferServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(SubservicingTransferServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var servicingRightsHolder = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageServicingRightsHolder, E_ReturnOptionIfNotExist.CreateNew);
            servicingRightsHolder.CompanyName = this.GetString("MortgageServicingRightsHolderCompanyNm");
            servicingRightsHolder.StreetAddr = this.GetString("MortgageServicingRightsHolderStreetAddr");
            servicingRightsHolder.City = this.GetString("MortgageServicingRightsHolderCity");
            servicingRightsHolder.State = this.GetString("MortgageServicingRightsHolderState");
            servicingRightsHolder.Zip = this.GetString("MortgageServicingRightsHolderZip");
            servicingRightsHolder.PhoneOfCompany = this.GetString("MortgageServicingRightsHolderPhoneOfCompany");
            servicingRightsHolder.EmailAddr = this.GetString("MortgageServicingRightsHolderEmailAddr");
            servicingRightsHolder.Update();

            dataLoan.sMortgageServicingRightsSalesDate_rep = this.GetString("sMortgageServicingRightsSalesDate");
            dataLoan.sMortgageServicingRightsTransferDate_rep = this.GetString("sMortgageServicingRightsTransferDate");
            dataLoan.sMortgageServicingRightsHolderFirstPaymentDueDate_rep = this.GetString("sMortgageServicingRightsHolderFirstPaymentDueDate");

            dataLoan.sSubservicerLoanNm = GetString("sSubservicerLoanNm");
            dataLoan.sGLServTransEffD_rep = GetString("sGLServTransEffD");

            BrokerDB broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
            if (!broker.ExpandSubservicerContacts)
            {
                var subservicer = dataLoan.GetAgentOfRole(E_AgentRoleT.Subservicer, E_ReturnOptionIfNotExist.CreateNew);
                subservicer.CompanyName = GetString("SubservicerCompanyNm");
                subservicer.StreetAddr = GetString("SubservicerStreetAddr");
                subservicer.City = GetString("SubservicerCity");
                subservicer.State = GetString("SubservicerState");
                subservicer.Zip = GetString("SubservicerZip");
                subservicer.PhoneOfCompany = GetString("SubservicerPhoneOfCompany");
                subservicer.EmailAddr = GetString("SubservicerEmailAddr");
                subservicer.Update();
                dataLoan.sSubservicerMersId = GetString("sSubservicerMersId");
            }
            else
            {
                dataLoan.sSubservicerMersId = GetString("MERSOrganizationID");

                dataLoan.sSubservicerRolodexId_rep = GetString("sSubservicerRolodexEntries");
                InvestorRolodexEntry subservicerInfo = dataLoan.sSubservicerInfo;
                subservicerInfo.SellerId = GetString("SellerID");
            }  
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            var subservicer = dataLoan.GetAgentOfRole(E_AgentRoleT.Subservicer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("SubservicerCompanyNm", subservicer.CompanyName);
            SetResult("SubservicerStreetAddr", subservicer.StreetAddr);
            SetResult("SubservicerCity", subservicer.City);
            SetResult("SubservicerState", subservicer.State);
            SetResult("SubservicerZip", subservicer.Zip);
            SetResult("SubservicerPhoneOfCompany", subservicer.PhoneOfCompany );
            SetResult("SubservicerEmailAddr", subservicer.EmailAddr);

            SetResult("sSubservicerLoanNm", dataLoan.sSubservicerLoanNm);
            SetResult("sSubservicerMersId", dataLoan.sSubservicerMersId);
            SetResult("sGLServTransEffD", dataLoan.sGLServTransEffD_rep);
            SetResult("sSubservicerRolodexEntries", dataLoan.sSubservicerRolodexId_rep);

            var servicingRightsHolder = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageServicingRightsHolder, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("MortgageServicingRightsHolderCompanyNm", servicingRightsHolder.CompanyName);
            SetResult("MortgageServicingRightsHolderStreetAddr", servicingRightsHolder.StreetAddr);
            SetResult("MortgageServicingRightsHolderCity", servicingRightsHolder.City);
            SetResult("MortgageServicingRightsHolderState", servicingRightsHolder.State);
            SetResult("MortgageServicingRightsHolderZip", servicingRightsHolder.Zip);
            SetResult("MortgageServicingRightsHolderPhoneOfCompany", servicingRightsHolder.PhoneOfCompany);
            SetResult("MortgageServicingRightsHolderEmailAddr", servicingRightsHolder.EmailAddr);

            SetResult("sMortgageServicingRightsSalesDate", dataLoan.sMortgageServicingRightsSalesDate_rep);
            SetResult("sMortgageServicingRightsTransferDate", dataLoan.sMortgageServicingRightsTransferDate_rep);
            SetResult("sMortgageServicingRightsHolderFirstPaymentDueDate", dataLoan.sMortgageServicingRightsHolderFirstPaymentDueDate_rep);
        }
    }
}
