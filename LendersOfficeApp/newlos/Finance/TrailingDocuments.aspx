﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrailingDocuments.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.TrailingDocuments" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Trailing Documents</title>
    <style type="text/css">
        .FormTableSubHeader { background-color:#C0C0C0; color:#000080; font-weight:bold; }
        .dateCol { width: 100px; }
        .docCol { width: 170px; }
        .methodCol { width: 153px; }
        .trackingCol { width: 133px; }
        .commentsCol { width: 133px; }
    </style>
</head>
<body bgcolor="gainsboro">


<script type="text/javascript">

    function _init() {
      onShowDLLChange();
    }

    function lockMersTos() 
    {
      <%= AspxTools.JsGetElementById(sMersTosD)%>.readOnly = !<%= AspxTools.JsGetElementById(sMersTosDLckd) %>.checked;
      changeTosD();
      }
    
    function changeTosD()
    {
        if(!<%= AspxTools.JsGetElementById(sMersTosDLckd) %>.checked)
            <%= AspxTools.JsGetElementById(sMersTosD) %>.value = <%= AspxTools.JsGetElementById(sMersTobD) %>.value;
    
    }
    
    function onShowDLLChange()
    {
        //OnlyRequiredDocs is selected
        if(<%= AspxTools.JsGetElementById(sShownDocumentTypes) %>.selectedIndex == 0)
        {
               document.getElementById("sLoanPackageRow1").style.display = "";
               document.getElementById("sLoanPackageRow2").style.display = "";
               document.getElementById("sMortgageDOTRow1").style.display = "";
               document.getElementById("sMortgageDOTRow2").style.display = "";
               document.getElementById("sNoteRow1").style.display = "";
               document.getElementById("sNoteRow2").style.display = "";
               document.getElementById("sFinalHUD1SttlmtStmtRow1").style.display = "";
               document.getElementById("sFinalHUD1SttlmtStmtRow2").style.display = "";
               document.getElementById("sTitleInsPolicyRow1").style.display = "";
               document.getElementById("sTitleInsPolicyRow2").style.display = "";
               document.getElementById("sMiCertRow1").style.display = "";
               document.getElementById("sMiCertRow2").style.display = "";
               document.getElementById("sFinalHazInsPolicyRow1").style.display = "";
               document.getElementById("sFinalHazInsPolicyRow2").style.display = "";
               document.getElementById("sFinalFloodInsPolicyRow1").style.display = "";
               document.getElementById("sFinalFloodInsPolicyRow2").style.display = "";
               document.getElementById("sCustomTrailingDoc1Row1").style.display = "";
               document.getElementById("sCustomTrailingDoc1Row2").style.display = "";
               document.getElementById("sCustomTrailingDoc2Row1").style.display = "";
               document.getElementById("sCustomTrailingDoc2Row2").style.display = "";
               document.getElementById("sCustomTrailingDoc3Row1").style.display = "";
               document.getElementById("sCustomTrailingDoc3Row2").style.display = "";
               document.getElementById("sCustomTrailingDoc4Row1").style.display = "";
               document.getElementById("sCustomTrailingDoc4Row2").style.display = "";
        }
        else
        {
            if(!<%= AspxTools.JsGetElementById(sLoanPackageRequired) %>.checked)
            {
               document.getElementById("sLoanPackageRow1").style.display = "none";
               document.getElementById("sLoanPackageRow2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sMortgageDOTRequired) %>.checked)
            {
               document.getElementById("sMortgageDOTRow1").style.display = "none";
               document.getElementById("sMortgageDOTRow2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sNoteRequired) %>.checked)
            {
               document.getElementById("sNoteRow1").style.display = "none";
               document.getElementById("sNoteRow2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sFinalHUD1SttlmtStmtRequired) %>.checked)
            {
               document.getElementById("sFinalHUD1SttlmtStmtRow1").style.display = "none";
               document.getElementById("sFinalHUD1SttlmtStmtRow2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sTitleInsPolicyRequired) %>.checked)
            {
               document.getElementById("sTitleInsPolicyRow1").style.display = "none";
               document.getElementById("sTitleInsPolicyRow2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sMiCertRequired) %>.checked)
            {
               document.getElementById("sMiCertRow1").style.display = "none";
               document.getElementById("sMiCertRow2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sFinalHazInsPolicyRequired) %>.checked)
            {
               document.getElementById("sFinalHazInsPolicyRow1").style.display = "none";
               document.getElementById("sFinalHazInsPolicyRow2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sFinalFloodInsPolicyRequired) %>.checked)
            {
               document.getElementById("sFinalFloodInsPolicyRow1").style.display = "none";
               document.getElementById("sFinalFloodInsPolicyRow2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sCustomTrailingDoc1Required) %>.checked)
            {
               document.getElementById("sCustomTrailingDoc1Row1").style.display = "none";
               document.getElementById("sCustomTrailingDoc1Row2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sCustomTrailingDoc2Required) %>.checked)
            {
               document.getElementById("sCustomTrailingDoc2Row1").style.display = "none";
               document.getElementById("sCustomTrailingDoc2Row2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sCustomTrailingDoc3Required) %>.checked)
            {
               document.getElementById("sCustomTrailingDoc3Row1").style.display = "none";
               document.getElementById("sCustomTrailingDoc3Row2").style.display = "none";
            }
            if(!<%= AspxTools.JsGetElementById(sCustomTrailingDoc4Required) %>.checked)
            {
               document.getElementById("sCustomTrailingDoc4Row1").style.display = "none";
               document.getElementById("sCustomTrailingDoc4Row2").style.display = "none";
            }
        }
    }
    
    function showDateChangeHistory()
    {
        var path = "/newlos/AuditTrail.aspx";
        var url = path
        + '?category=trailingDocs&loanid=' + <%= AspxTools.JsString(LoanID) %>
        showModal(url);
    }
    </script> 

    <form id="form1" runat="server">
    
    <div>
        <table class="FormTable" width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td nowrap="noWrap" class="MainRightHeader">Trailing Documents</td>
				</tr>
				<tr>
				    <td>
				    <table cellspacing="0" cellpadding="5" border="0">
				        <tr> 
				            <td class="FieldLabel" nowrap="noWrap">Recorded</td>
				            <td nowrap="noWrap"><ml:datetextbox id="sRecordedD" runat="server" CssClass="mask" width="75" preset="date"></ml:datetextbox></td>
				            <td class="FieldLabel" nowrap="noWrap">MERS Registration Date</td>
				            <td></td>
				            <td nowrap="noWrap"><ml:datetextbox id="sMersRegistrationD" runat="server" CssClass="mask" width="75" preset="date"></ml:datetextbox></td>
				            <td class="FieldLabel" nowrap="noWrap">Investor</td><td><asp:DropDownList ReadOnly="true" disabled="true" id="sInvestorRolodexEntries" runat="server"></asp:DropDownList></td>
				        </tr>
				        <tr>
				            <td class="FieldLabel" nowrap="noWrap">Instrument #</td><td><asp:TextBox id="sRecordingInstrumentNum" runat="server"></asp:TextBox></td>
				            <td class="FieldLabel"  nowrap="noWrap">MERS TOB Date</td>
				            <td></td><td nowrap="noWrap"><ml:datetextbox onchange="date_onblur(); changeTosD();" id="sMersTobD" runat="server" CssClass="mask" width="75" preset="date"></ml:datetextbox></td>
				            <td class="FieldLabel" nowrap="noWrap">Investor Loan Number</td><td><asp:TextBox id="sInvestorLockLoanNum" ReadOnly="true"  runat="server"></asp:TextBox></td>
				        </tr>
				        <tr>
				        <td class="FieldLabel" nowrap="noWrap">Volume #</td>
				        <td>
				            <table cellspacing="0" cellpadding="0" border="0">
				            <tr>
				                <td><asp:TextBox id="sRecordingVolumeNum" width="75" runat="server"></asp:TextBox></td>
				                <td class="FieldLabel" nowrap="noWrap">&nbsp Book # &nbsp</td><td><asp:TextBox id="sRecordingBookNum" width="75" runat="server"></asp:TextBox></td>
				                <td class="FieldLabel" nowrap="noWrap">&nbsp Page # &nbsp</td><td><asp:TextBox id="sRecordingPageNum" width="75" runat="server"></asp:TextBox></td>
				            </tr>
				            </table>
				            </td>
				        <td class="FieldLabel" nowrap="noWrap">MERS TOS Date</td>
				        <td><asp:CheckBox ID="sMersTosDLckd" runat="server" onclick="lockMersTos();"/></td>
				        <td><ml:datetextbox id="sMersTosD" runat="server"></ml:datetextbox></td>
				        </tr>
				        <tr>
				            <td></td>
				            <td></td>
				            <td class="FieldLabel" nowrap="noWrap">MERS Min #</td>
				            <td></td>
				            <td><asp:TextBox id="sMersMin" runat="server"></asp:TextBox></td>
				        </tr>
				        <tr>
				            <td class="FieldLabel" nowrap="noWrap">Show</td>
				            <td><asp:DropDownList id="sShownDocumentTypes" onChange="onShowDLLChange()" runat="server">
				                <asp:ListItem Selected="True" Text = "All Documents" Value="0"/>
				                <asp:ListItem Text = "Only Required Documents" Value="1"/>
				            </asp:DropDownList></td>
				            <td></td><td></td><td></td><td></td>
				            <td><a href="#" onclick="showDateChangeHistory()">view date change history</a></td>
				        </tr>
				    </table>
				    </td>
				</tr>
				<tr>
				<td>
				<table class="FormTable" cellspacing="0" cellpadding="5" border="0">
				    <tr class="docRow">
				        <td nowrap="noWrap" class="FormTableSubHeader">Required?</td>
				        <td nowrap="noWrap" class="FormTableSubHeader">Document</td>
				        <td nowrap="noWrap" class="FormTableSubHeader">Ordered</td>
				        <td nowrap="noWrap" class="FormTableSubHeader">Document Date</td>
				        <td nowrap="noWrap" class="FormTableSubHeader">Received</td>
				        <td nowrap="noWrap" class="FormTableSubHeader">Shipped</td>
				        <td nowrap="noWrap" class="FormTableSubHeader">Method</td>
				        <td nowrap="noWrap" class="FormTableSubHeader">Tracking</td>
				        <td nowrap="noWrap" class="FormTableSubHeader">Reviewed</td>
				    </tr>
				    <tr class="docRow" id="sLoanPackageRow1">
				        <td nowrap="noWrap"><asp:CheckBox  ID="sLoanPackageRequired" runat="server" ></asp:CheckBox></td>
				        <td nowrap="noWrap">Loan Package</td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sLoanPackageOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
						<td nowrap="noWrap"><ml:DateTextBox ID="sLoanPackageDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
						<td nowrap="noWrap"><ml:DateTextBox ID="sLoanPackageReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
						<td nowrap="noWrap"><ml:DateTextBox ID="sLoanPackageShippedD" runat="server" preset="date"></ml:DateTextBox></td>
						<td nowrap="noWrap"><ml:ComboBox ID="sLoanPackageMethod" runat="server"></ml:ComboBox></td>
						<td nowrap="noWrap"><asp:TextBox ID="sLoanPackageTracking" runat="server"></asp:TextBox></td>
						<td nowrap="noWrap"><ml:DateTextBox ID="sLoanPackageReviewedD" runat="server" preset="date"></ml:DateTextBox></td>
				    </tr >
				    <tr class="docRow" id="sLoanPackageRow2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sLoanPackageN"/>
				        </td>
				    </tr>
				    <tr class="docRow" id = "sMortgageDOTRow1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sMortgageDOTRequired" runat="server" /></td>
				        <td nowrap="noWrap" class="fieldLabel">Mortgage / Deed of Trust</td>
						<td nowrap="noWrap"><ml:DateTextBox ID="sMortgageDOTOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
						<td nowrap="noWrap"><ml:DateTextBox ID="sMortgageDOTDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
						<td nowrap="noWrap"><ml:DateTextBox ID="sMortgageDOTReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
						<td nowrap="noWrap"><ml:DateTextBox ID="sMortgageDOTShippedD" runat="server" preset="date"></ml:DateTextBox></td>
						<td nowrap="noWrap"><ml:ComboBox ID="sMortgageDOTMethod" runat="server"></ml:ComboBox></td>
						<td nowrap="noWrap"><asp:TextBox ID="sMortgageDOTTracking" runat="server"></asp:TextBox></td>
						<td nowrap="noWrap"><ml:DateTextBox ID="sMortgageDOTReviewedD" runat="server" preset="date"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sMortgageDOTRow2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sMortgageDOTN"/>
				        </td>
				    </tr>
				    <tr class="docRow" id = "sNoteRow1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sNoteRequired" runat="server" /></td>
				        <td nowrap="noWrap" class="fieldLabel">Note</td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sNoteOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sDocumentNoteD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sNoteReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sNoteShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sNoteMethod" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sNoteTracking" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sNoteReviewedD" runat="server"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sNoteRow2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sNoteN"/>
				        </td>
				    </tr>
				    <tr class="docRow" id = "sFinalHUD1SttlmtStmtRow1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sFinalHUD1SttlmtStmtRequired" runat="server" /></td>
				        <td nowrap="noWrap" class="fieldLabel">Final HUD-1 Settlement Stmt</td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalHUD1SttlmtStmtOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalHUD1SttlmtStmtDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalHUD1SttlmtStmtReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalHUD1SttlmtStmtShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sFinalHUD1SttlmtStmtMethod" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sFinalHUD1SttlmtStmtTracking" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalHUD1SttlmtStmtReviewedD" runat="server"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sFinalHUD1SttlmtStmtRow2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sFinalHUD1SttlmtStmtN"/>
				        </td>
				    </tr>
				    <tr class="docRow" id = "sTitleInsPolicyRow1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sTitleInsPolicyRequired" runat="server" /></td>
				        <td nowrap="noWrap" class="fieldLabel">Title Insurance Policy</td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sTitleInsPolicyOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sTitleInsPolicyDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sTitleInsPolicyReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sTitleInsPolicyShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sTitleInsPolicyMethod" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sTitleInsPolicyTracking" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sTitleInsPolicyReviewedD" runat="server"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sTitleInsPolicyRow2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sTitleInsPolicyN"/>
				        </td>
				    </tr>
				    <tr class="docRow" id = "sMiCertRow1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sMiCertRequired" runat="server" /></td>
				        <td nowrap="noWrap" class="fieldLabel">Mortgage Ins Certificate</td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sMiCertOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sMiCertIssuedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sMiCertReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sMiCertShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sMiCertMethod" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sMiCertTracking" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sMiCertReviewedD" runat="server"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sMiCertRow2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sMiCertN"/>
				        </td>
				    </tr>
				    <tr class="docRow" id = "sFinalHazInsPolicyRow1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sFinalHazInsPolicyRequired" runat="server" /></td>
				        <td nowrap="noWrap" class="fieldLabel">Final Hazard Ins Policy</td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalHazInsPolicyOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalHazInsPolicyDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalHazInsPolicyReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalHazInsPolicyShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sFinalHazInsPolicyMethod" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sFinalHazInsPolicyTracking" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalHazInsPolicyReviewedD" runat="server"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sFinalHazInsPolicyRow2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sFinalHazInsPolicyN"/>
				        </td>
				    </tr>
				    <tr class="docRow" id = "sFinalFloodInsPolicyRow1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sFinalFloodInsPolicyRequired" runat="server" /></td>
				        <td nowrap="noWrap" class="fieldLabel">Final Flood Ins Policy</td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalFloodInsPolicyOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalFloodInsPolicyDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalFloodInsPolicyReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalFloodInsPolicyShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sFinalFloodInsPolicyMethod" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sFinalFloodInsPolicyTracking" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sFinalFloodInsPolicyReviewedD" runat="server" preset="date"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sFinalFloodInsPolicyRow2"> 
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sFinalFloodInsPolicyN"/>
				        </td>
				    </tr>
				     <tr class="docRow" id = "sCustomTrailingDoc1Row1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sCustomTrailingDoc1Required" runat="server" /></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc1Desc" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc1OrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc1DocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc1ReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc1ShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sCustomTrailingDoc1Method" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc1Tracking" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc1ReviewedD" runat="server" preset="date"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sCustomTrailingDoc1Row2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sCustomTrailingDoc1N"/>
				        </td>
				    </tr>
				    <tr class="docRow" id = "sCustomTrailingDoc2Row1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sCustomTrailingDoc2Required" runat="server" /></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc2Desc" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc2OrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc2DocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc2ReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc2ShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sCustomTrailingDoc2Method" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc2Tracking" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc2ReviewedD" runat="server" preset="date"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sCustomTrailingDoc2Row2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sCustomTrailingDoc2N"/>
				        </td>
				    </tr>
				    <tr class="docRow" id = "sCustomTrailingDoc3Row1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sCustomTrailingDoc3Required" runat="server" /></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc3Desc" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc3OrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc3DocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc3ReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc3ShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sCustomTrailingDoc3Method" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc3Tracking" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc3ReviewedD" runat="server" preset="date"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sCustomTrailingDoc3Row2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sCustomTrailingDoc3N"/>
				        </td>
				    </tr>
				    <tr class="docRow" id = "sCustomTrailingDoc4Row1">
				        <td nowrap="noWrap"><asp:CheckBox ID="sCustomTrailingDoc4Required" runat="server" /></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc4Desc" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc4OrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc4DocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc4ReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc4ShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sCustomTrailingDoc4Method" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc4Tracking" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc4ReviewedD" runat="server" preset="date"></ml:DateTextBox></td>
				    </tr>
				    <tr class="docRow" id = "sCustomTrailingDoc4Row2">
				        <td></td><td></td>
				        <td colspan="5"  class="FieldLabel" nowrap="noWrap">Comments &nbsp;
				        <asp:TextBox Width="520" runat="server" ID="sCustomTrailingDoc4N"/>
				        </td>
				    </tr>
				</table>
				</td>
				</tr>
				</table>
    
    </div>
    </form>
</body>
</html>
