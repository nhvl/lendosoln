﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using System.Web.Services;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class SubservicingTransfer : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Subservicing Transfer";
            this.PageID = "SubservicingTransfer";

            SubservicerZip.SmartZipcode(SubservicerCity, SubservicerState);
            this.MortgageServicingRightsHolderZip.SmartZipcode(
            this.MortgageServicingRightsHolderCity,
            this.MortgageServicingRightsHolderState);
            this.RegisterJsScript("LQBPopup.js");
        }

        protected override void LoadData()
        {
            EnableJqueryMigrate = false;
            var dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SubservicingTransfer));
            dataLoan.InitLoad();

            this.sMortgageServicingRightsSalesDate.Text = dataLoan.sMortgageServicingRightsSalesDate_rep;
            this.sMortgageServicingRightsTransferDate.Text = dataLoan.sMortgageServicingRightsTransferDate_rep;
            this.sMortgageServicingRightsHolderFirstPaymentDueDate.Text = dataLoan.sMortgageServicingRightsHolderFirstPaymentDueDate_rep;

            var servicingRightsHolder = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageServicingRightsHolder, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            this.MortgageServicingRightsHolderCompanyNm.Text = servicingRightsHolder.CompanyName;
            this.MortgageServicingRightsHolderStreetAddr.Text = servicingRightsHolder.StreetAddr;
            this.MortgageServicingRightsHolderCity.Text = servicingRightsHolder.City;
            this.MortgageServicingRightsHolderState.Value = servicingRightsHolder.State;
            this.MortgageServicingRightsHolderZip.Text = servicingRightsHolder.Zip;
            this.MortgageServicingRightsHolderPhoneOfCompany.Text = servicingRightsHolder.PhoneOfCompany;
            this.MortgageServicingRightsHolderEmailAddr.Text = servicingRightsHolder.EmailAddr;

            sSubservicerLoanNm.Text = dataLoan.sSubservicerLoanNm;
            sGLServTransEffD.Text = dataLoan.sGLServTransEffD_rep;
            if (Broker.ExpandSubservicerContacts)
            {
                NormalSubservicerContactContainer.Style.Add("display", "none");
                sSubservicerMersIdContainer.Style.Add("display", "none");
                ExpandedSubservicerContainer.Style.Add("display", "");

                InvestorRolodexEntry subservicerInfo = dataLoan.GetSubservicerInfo();

                if (subservicerInfo.Id != null)
                    BindSubservicerNames((int)subservicerInfo.Id);
                else
                    BindSubservicerNames(-1);

                MERSOrganizationID.Text = subservicerInfo.MERSOrganizationId;
                SellerID.Text = subservicerInfo.SellerId;

                CompanyName.Text = subservicerInfo.CompanyName;
                MainContactName.Text = subservicerInfo.MainContactName;
                MainAttention.Text = subservicerInfo.MainAttention;
                MainEmail.Text = subservicerInfo.MainEmail;
                MainAddress.Text = subservicerInfo.MainAddress;
                MainCity.Text = subservicerInfo.MainCity;
                MainState.Value = subservicerInfo.MainState;
                MainZip.Text = subservicerInfo.MainZip;
                MainPhone.Text = subservicerInfo.MainPhone;
                MainFax.Text = subservicerInfo.MainFax;


                NoteShipToAttention.Text = subservicerInfo.NoteShipToAttention;
                NoteShipToContactName.Text = subservicerInfo.NoteShipToContactName;
                NoteShipToAddress.Text = subservicerInfo.NoteShipToAddress;
                NoteShipToCity.Text = subservicerInfo.NoteShipToCity;
                NoteShipToState.Value = subservicerInfo.NoteShipToState;
                NoteShipToZip.Text = subservicerInfo.NoteShipToZip;
                NoteShipToEmail.Text = subservicerInfo.NoteShipToEmail;
                NoteShipToPhone.Text = subservicerInfo.NoteShipToPhone;
                NoteShipToFax.Text = subservicerInfo.NoteShipToFax;


                PaymentToAttention.Text = subservicerInfo.PaymentToAttention;
                PaymentToContactName.Text = subservicerInfo.PaymentToContactName;
                PaymentToAddress.Text = subservicerInfo.PaymentToAddress;
                PaymentToCity.Text = subservicerInfo.PaymentToCity;
                PaymentToState.Value = subservicerInfo.PaymentToState;
                PaymentToZip.Text = subservicerInfo.PaymentToZip;
                PaymentToEmail.Text = subservicerInfo.PaymentToEmail;
                PaymentToPhone.Text = subservicerInfo.PaymentToPhone;
                PaymentToFax.Text = subservicerInfo.PaymentToFax;


                LossPayeeAttention.Text = subservicerInfo.LossPayeeAttention;
                LossPayeeContactName.Text = subservicerInfo.LossPayeeContactName;
                LossPayeeAddress.Text = subservicerInfo.LossPayeeAddress;
                LossPayeeCity.Text = subservicerInfo.LossPayeeCity;
                LossPayeeState.Value = subservicerInfo.LossPayeeState;
                LossPayeeZip.Text = subservicerInfo.LossPayeeZip;
                LossPayeeEmail.Text = subservicerInfo.LossPayeeEmail;
                LossPayeePhone.Text = subservicerInfo.LossPayeePhone;
                LossPayeeFax.Text = subservicerInfo.LossPayeeFax;
            }
            else
            {
                this.MsrDates.Attributes["class"] += " PadBottom";

                var subservicer = dataLoan.GetAgentOfRole(E_AgentRoleT.Subservicer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                SubservicerCompanyNm.Text = subservicer.CompanyName;
                SubservicerStreetAddr.Text = subservicer.StreetAddr;
                SubservicerCity.Text = subservicer.City;
                SubservicerState.Value = subservicer.State;
                SubservicerZip.Text = subservicer.Zip;
                SubservicerPhoneOfCompany.Text = subservicer.PhoneOfCompany;
                SubservicerEmailAddr.Text = subservicer.EmailAddr;

                sSubservicerMersId.Text = dataLoan.sSubservicerMersId;

                base.LoadData();
            }
        }

        public void BindSubservicerNames(int selected)
        {
            List<InvestorRolodexEntry> entries = InvestorRolodexEntry.GetAll(Broker.BrokerID, null).ToList();
            sSubservicerRolodexEntries.DataSource = entries.Where(p => (p.Status == E_InvestorStatus.Active && ( p.InvestorRolodexType == E_InvestorRolodexType.Subservicer)) || p.Id == selected);
            sSubservicerRolodexEntries.DataTextField = "InvestorName";
            sSubservicerRolodexEntries.DataValueField = "Id";
            sSubservicerRolodexEntries.DataBind();

            sSubservicerRolodexEntries.Items.Insert(0, new ListItem("<-- Select a Subservicer -->", "-1"));
            Tools.SetDropDownListValue(sSubservicerRolodexEntries, selected);
        }

        [WebMethod]
        public static object GetInvestorInfo(int id)
        {
            return InvestorRolodexEntry.Get(PrincipalFactory.CurrentPrincipal.BrokerId, id);
        }
    }
}
