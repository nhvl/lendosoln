﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class PayoffStatementServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PayoffStatementServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sPayoffStatementPayoffD_rep = GetString("sPayoffStatementPayoffD");
            dataLoan.sPayoffStatementNotesToRecipient = GetString("sPayoffStatementNotesToRecipient");
            dataLoan.sPayoffStatementInterestCalcT = (E_sPayoffStatementInterestCalcT)GetInt("sPayoffStatementInterestCalcT");
            dataLoan.sPayoffStatementRecipientT = (E_sPayoffStatementRecipientT)GetInt("sPayoffStatementRecipientT");

            dataLoan.sPayoffStatementRecipientAddressee = GetString("sPayoffStatementRecipientAddressee");
            dataLoan.sPayoffStatementRecipientAttention = GetString("sPayoffStatementRecipientAttention");
            dataLoan.sPayoffStatementRecipientStreetAddress = GetString("sPayoffStatementRecipientStreetAddress");
            dataLoan.sPayoffStatementRecipientCity = GetString("sPayoffStatementRecipientCity");
            dataLoan.sPayoffStatementRecipientState = GetString("sPayoffStatementRecipientState");
            dataLoan.sPayoffStatementRecipientZip = GetString("sPayoffStatementRecipientZip");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sPayoffStatementPayoffD", dataLoan.sPayoffStatementPayoffD_rep);
            SetResult("sServicingLastPmtDueD", dataLoan.sServicingLastPmtDueD_rep);
            SetResult("sServicingNextPmtDue", dataLoan.sServicingNextPmtDue_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sServicingUnpaidPrincipalBalance", dataLoan.sServicingUnpaidPrincipalBalance_rep);
            SetResult("sDaysInYr", dataLoan.sDaysInYr_rep);
            SetResult("sSettlementIPerDay", dataLoan.sSettlementIPerDay_rep);
            SetResult("sPayoffStatementInterestWholeMonths", dataLoan.sPayoffStatementInterestWholeMonths_rep);
            SetResult("sPayoffStatementInterestOddDays", dataLoan.sPayoffStatementInterestOddDays_rep);
            SetResult("sPayoffStatementTotalInterestDueAmt", dataLoan.sPayoffStatementTotalInterestDueAmt_rep);
            SetResult("sServicingUnpaidPrincipalBalance_col2", dataLoan.sServicingUnpaidPrincipalBalance_rep);
            SetResult("sPayoffStatementTotalInterestDueAmt_col2", dataLoan.sPayoffStatementTotalInterestDueAmt_rep);
            SetResult("sPayoffStatementMonthlyMiDueAmt", dataLoan.sPayoffStatementMonthlyMiDueAmt_rep);
            SetResult("sServicingTotalDueFunds_Escrow", dataLoan.sServicingTotalDueFunds_Escrow_rep);
            SetResult("sServicingTotalDueFunds_LateFees", dataLoan.sServicingTotalDueFunds_LateFees_rep);
            SetResult("sPayoffStatementTotalDueAmt", dataLoan.sPayoffStatementTotalDueAmt_rep);
            SetResult("sPayoffStatementTotalDueAmt_2", dataLoan.sPayoffStatementTotalDueAmt_rep);
            SetResult("sPayoffStatementNotesToRecipient", dataLoan.sPayoffStatementNotesToRecipient);
            SetResult("sPayoffStatementInterestCalcT", dataLoan.sPayoffStatementInterestCalcT);
            SetResult("sPayoffStatementRecipientT", dataLoan.sPayoffStatementRecipientT);

            SetResult("sPayoffStatementRecipientAddressee", dataLoan.sPayoffStatementRecipientAddressee ?? "");
            SetResult("sPayoffStatementRecipientAttention", dataLoan.sPayoffStatementRecipientAttention ?? "");
            SetResult("sPayoffStatementRecipientStreetAddress", dataLoan.sPayoffStatementRecipientStreetAddress ?? "");
            SetResult("sPayoffStatementRecipientCity", dataLoan.sPayoffStatementRecipientCity ?? "");
            SetResult("sPayoffStatementRecipientState", dataLoan.sPayoffStatementRecipientState ?? "");
            SetResult("sPayoffStatementRecipientZip", dataLoan.sPayoffStatementRecipientZip ?? "");
        }
    }

    public partial class PayoffStatementService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new PayoffStatementServiceItem());
        }
    }
}
