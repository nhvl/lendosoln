﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServicingPayeeDlg.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.ServicingPayeeDlg" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<LINK href="../../css/stylesheet.css" type=text/css rel=stylesheet >

<head runat="server">
    <title>Pick a Payee</title>
</head>

<body bgcolor="gainsboro">
	<h4 class="page-header">Pick a Payee</h4>
    <form id="form1" runat="server" onload="HighlightFilter">
        <table cellspacing=1 cellpadding=3 width="100%" border=0>
			<tr>
				<td class="FormTableHeader" colspan="2">
				    Payees
				</td>
			</tr>
			<tr>
			    <td width="50px">
			        &nbsp; Payee: 
			    </td>
			    <td>
			        <asp:TextBox runat="server" id="m_payeeFilter"></asp:TextBox>
			    </td>
			</tr>
			<tr>
			    <td>
			    </td>
			    <td>
			        <asp:Button runat="server" Text="Search"/>
			    </td>
			</tr>
			<tr>
			    <td colspan="2">
			        <table cellspacing="0" style="width:100%">
			            <tr class="GridHeader" style="width:100%">
			                <td style="width:60%">Tax Payee Name</td>
			                <td style="width:25%">City</td>
			                <td style="width:10%">State</td>
			            </tr>
			        </table>
			    </td>
			</tr>
			<tr>
			    <td colspan="2">
			        <div style="height:300px; overflow:auto;">
		                <ml:CommonDataGrid runat="server" ID="PayeeList" AutoGenerateColumns="false" ShowHeader="false">
		                    <HeaderStyle CssClass="GridHeader" />
		                    <Columns>
		                        <asp:TemplateColumn HeaderText="Tax Payee Name" ItemStyle-Width="60%">
		                            <ItemTemplate>
		                                <asp:LinkButton ID="Link" runat="server" OnCommand='SavePayee' CommandArgument='<%# AspxTools.HtmlString(Container.ItemIndex.ToString()) %>'>
                                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PayeeName").ToString()) %>
		                                </asp:LinkButton>
		                                <asp:HiddenField runat="server" ID="ID" Value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PayeeId").ToString()) %>' />
		                            </ItemTemplate>
		                        </asp:TemplateColumn>
		                        <asp:BoundColumn DataField="City" HeaderText="City" ItemStyle-Width="25%"></asp:BoundColumn>
		                        <asp:BoundColumn DataField="State" HeaderText="State" ItemStyle-Width="10%"></asp:BoundColumn>
		                    </Columns>
		                </ml:CommonDataGrid>
		            </div>
			    </td>
			</tr>
			<tr>
			    <td colspan="2" style="text-align:center;">
    		        <input type="button" onclick="onClosePopup();" value="Cancel"/>
			    </td>
			</tr>
		</table>
    </form>
</body>
</html>
