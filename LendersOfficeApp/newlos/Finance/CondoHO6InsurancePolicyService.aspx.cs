﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class CondoHO6InsurancePolicyServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(CondoHO6InsurancePolicyServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sCondoHO6InsCompanyNm = GetString("sCondoHO6InsCompanyNm");
            dataLoan.sCondoHO6InsPolicyNum = GetString("sCondoHO6InsPolicyNum");
            dataLoan.sCondoHO6InsPolicyActivationD_rep = GetString("sCondoHO6InsPolicyActivationD");
            dataLoan.sCondoHO6InsSettlementChargeT = (E_sInsSettlementChargeT)GetInt("sCondoHO6InsSettlementChargeT");
            dataLoan.sCondoHO6InsPaidByT = (E_sInsPaidByT)GetInt("sCondoHO6InsPaidByT");
            dataLoan.sCondoHO6InsCoverageAmt_rep = GetString("sCondoHO6InsCoverageAmt");
            dataLoan.sCondoHO6InsDeductibleAmt_rep = GetString("sCondoHO6InsDeductibleAmt");
            dataLoan.sCondoHO6InsPaymentDueAmt_rep = GetString("sCondoHO6InsPaymentDueAmt");
            dataLoan.sCondoHO6InsPaymentDueAmtLckd = GetBool("sCondoHO6InsPaymentDueAmtLckd");
            dataLoan.sCondoHO6InsPaymentDueD_rep = GetString("sCondoHO6InsPaymentDueD");
            dataLoan.sCondoHO6InsPaymentMadeD_rep = GetString("sCondoHO6InsPaymentMadeD");
            dataLoan.sCondoHO6InsPaidThroughD_rep = GetString("sCondoHO6InsPaidThroughD");
            dataLoan.sCondoHO6InsPolicyPayeeCode = GetString("sCondoHO6InsPolicyPayeeCode");
            dataLoan.sCondoHO6InsPolicyExpirationD_rep = GetString("sCondoHO6InsPolicyExpirationD");
            dataLoan.sCondoHO6InsPolicyPaymentTypeT = (E_InsPolicyPaymentTypeT)GetInt("sCondoHO6InsPolicyPaymentTypeT");
            dataLoan.sCondoHO6InsCompanyId = GetString("sCondoHO6InsCompanyId");

            dataLoan.sCondoHO6InsPolicyLossPayeeTransD_rep = GetString("sCondoHO6InsPolicyLossPayeeTransD");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sCondoHO6InsCompanyNm", dataLoan.sCondoHO6InsCompanyNm);
            SetResult("sCondoHO6InsPolicyNum", dataLoan.sCondoHO6InsPolicyNum);
            SetResult("sCondoHO6InsPolicyActivationD", dataLoan.sCondoHO6InsPolicyActivationD_rep);
            SetResult("sCondoHO6InsSettlementChargeT", dataLoan.sCondoHO6InsSettlementChargeT);
            SetResult("sCondoHO6InsPaidByT", dataLoan.sCondoHO6InsPaidByT);
            SetResult("sCondoHO6InsCoverageAmt", dataLoan.sCondoHO6InsCoverageAmt_rep);
            SetResult("sCondoHO6InsDeductibleAmt", dataLoan.sCondoHO6InsDeductibleAmt_rep);
            SetResult("sCondoHO6InsPaymentDueAmt", dataLoan.sCondoHO6InsPaymentDueAmt_rep);
            SetResult("sCondoHO6InsPaymentDueAmtLckd", dataLoan.sCondoHO6InsPaymentDueAmtLckd);
            SetResult("sCondoHO6InsPaymentDueD", dataLoan.sCondoHO6InsPaymentDueD_rep);
            SetResult("sCondoHO6InsPaymentMadeD", dataLoan.sCondoHO6InsPaymentMadeD_rep);
            SetResult("sCondoHO6InsPaidThroughD", dataLoan.sCondoHO6InsPaidThroughD_rep);
            SetResult("sCondoHO6InsPolicyPayeeCode", dataLoan.sCondoHO6InsPolicyPayeeCode);
            SetResult("sCondoHO6InsPolicyExpirationD", dataLoan.sCondoHO6InsPolicyExpirationD_rep);
            SetResult("sCondoHO6InsPolicyPaymentTypeT", dataLoan.sCondoHO6InsPolicyPaymentTypeT);
            SetResult("sCondoHO6InsCompanyId", dataLoan.sCondoHO6InsCompanyId);

            SetResult("sCondoHO6InsPolicyLossPayeeTransD", dataLoan.sCondoHO6InsPolicyLossPayeeTransD_rep);
        }
    }
    public partial class CondoHO6InsurancePolicyService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new CondoHO6InsurancePolicyServiceItem());
        }
    }
}
