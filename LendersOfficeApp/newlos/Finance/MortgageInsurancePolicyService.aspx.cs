﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{
    public class MortgageInsurancePolicyServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(MortgageInsurancePolicyServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sMiLenderPaidAdj_rep = GetString("sMiLenderPaidAdj");
            dataLoan.sMiLenderPaidCoverage_rep = GetString("sMiLenderPaidCoverage");
            dataLoan.sMiCertId = GetString("sMiCertId");
            dataLoan.sMiPmtDueD_rep = GetString("sMiPmtDueD");
            dataLoan.sMiPmtMadeD_rep = GetString("sMiPmtMadeD");
            dataLoan.sMiCertOrderedD_rep = GetString("sMiCertOrderedD");
            dataLoan.sMiCertIssuedD_rep = GetString("sMiCertIssuedD");
            dataLoan.sMiCertReceivedD_rep = GetString("sMiCertReceivedD");
            dataLoan.sMiCommitmentRequestedD_rep = GetString("sMiCommitmentRequestedD");
            dataLoan.sMiCommitmentReceivedD_rep = GetString("sMiCommitmentReceivedD");
            dataLoan.sMiCommitmentExpirationD_rep = GetString("sMiCommitmentExpirationD");
            dataLoan.sMiInsuranceT = (E_sMiInsuranceT)GetInt("sMiInsuranceT");
            dataLoan.sMiReasonForAbsenceT = (E_sMiReasonForAbsenceT)GetInt("sMiReasonForAbsenceT");
            dataLoan.sMiCompanyNmT = (E_sMiCompanyNmT)GetInt("sMiCompanyNmT");
            dataLoan.sMiPaidThruD_rep = GetString("sMiPaidThruD");
            dataLoan.sMiFirstProrataT = (E_MiProrata) GetInt("sMiFirstProrataT");
            dataLoan.sMiLastProrataT = (E_MiProrata) GetInt("sMiLastProrataT");
            dataLoan.sMiIsNoReserve = GetBool("sMiIsNoReserve");
            dataLoan.sMiPmtCheckNumber = GetString("sMiPmtCheckNumber");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sMiInsuranceT", dataLoan.sMiInsuranceT);
            SetResult("sMiReasonForAbsenceT", dataLoan.sMiReasonForAbsenceT);
            SetResult("sMiLenderPaidAdj", dataLoan.sMiLenderPaidAdj_rep);
            SetResult("sMiLenderPaidCoverage", dataLoan.sMiLenderPaidCoverage_rep);
            SetResult("sMiCompanyNmT", dataLoan.sMiCompanyNmT);
            SetResult("sMiCertId", dataLoan.sMiCertId);
            SetResult("sMiPmtDueD", dataLoan.sMiPmtDueD);
            SetResult("sMiPmtMadeD", dataLoan.sMiPmtMadeD);
            SetResult("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            SetResult("sMiCertOrderedD", dataLoan.sMiCertOrderedD);
            SetResult("sMiCertIssuedD", dataLoan.sMiCertIssuedD);
            SetResult("sMiCertReceivedD", dataLoan.sMiCertReceivedD);
            SetResult("sMiCommitmentRequestedD", dataLoan.sMiCommitmentRequestedD);
            SetResult("sMiCommitmentReceivedD", dataLoan.sMiCommitmentReceivedD);
            SetResult("sMiCommitmentExpirationD", dataLoan.sMiCommitmentExpirationD);
            SetResult("sMiPaidThruD", dataLoan.sMiPaidThruD_rep);
            SetResult("sMiFirstProrataT", dataLoan.sMiFirstProrataT);
            SetResult("sMiLastProrataT", dataLoan.sMiLastProrataT);
            SetResult("sMiIsNoReserve", dataLoan.sMiIsNoReserve);
            SetResult("sMiPmtCheckNumber", dataLoan.sMiPmtCheckNumber);
        }
    }
    public partial class MortgageInsurancePolicyService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new MortgageInsurancePolicyServiceItem());
        }
    }
}
