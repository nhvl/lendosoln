﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SecondaryStatus.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.SecondaryStatus" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Secondary Status</title>
    <style type="text/css">
        .FormTableSubHeader { background-color:#C0C0C0; color:#000080; font-weight:bold; }
        .dateCol { width: 100px; }
        .docCol { width: 170px; }
        .methodCol { width: 165px; }
        .trackingCol { width: 133px; }
        .commentsCol { width: 148px; }
    </style>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <div>
        <table class="FormTable" width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td class="MainRightHeader" nowrap="noWrap">Secondary Status</td>
				</tr>
				<tr>
				    <td style="padding-left:10px;">
				        <table>
				            <tr>
					            <td nowrap="noWrap">
					                <table cellspacing="0" cellpadding="0" border="0">
				                        <tr>
					                        <td class="FieldLabel" nowrap="noWrap" style="width:170px;">
					                            File Status&nbsp;&nbsp;<asp:CheckBox id="sSecondStatusLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox>
					                        </td>
					                        <td>
						                        <asp:textbox id="sSecondStatus" runat="server"  ReadOnly="True"></asp:textbox>
						                    </td>
					                        <td>
					                            <input type="button" value="Change Status" style="WIDTH: 98px" onclick="displayStatusLink();" id="btnChangeStatus"/>&nbsp;&nbsp;
					                            <input id="sSecondStatusT" type="hidden" name="sSecondStatusT" runat="server" />
					                        </td>
				                        </tr>
				                        
				                        <tr>
                                          <td class="FieldLabel" colspan="3" nowrap="noWrap">&nbsp;</td>
                                        </tr>
				                        
				                        <tr>
								            <td class="FieldLabel" nowrap="noWrap">Investor Rate Lock</td>
								            <td nowrap="noWrap"><ml:datetextbox id="sInvRLckdD" runat="server" CssClass="mask" width="75" preset="date"></ml:datetextbox></td>
								            <td></td>
							            </tr>
							            <tr>
								            <td class="FieldLabel" nowrap="noWrap">Investor Rate Lock Expiration</td>
								            <td nowrap="noWrap"><ml:datetextbox id="sInvRLckdExpiredD" runat="server" CssClass="mask" width="75" preset="date"></ml:datetextbox></td>
								            <td></td>
							            </tr>
							            <tr>
								            <td class="FieldLabel" nowrap="noWrap">Investor Delivery Expiration</td>
								            <td nowrap="noWrap"><ml:datetextbox id="sInvDelvExpD" runat="server" CssClass="mask" width="75" preset="date"></ml:datetextbox></td>
								            <td></td>
							            </tr>
					                </table>
					            </td>
				            </tr>
            				
				            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            
				            <tr>
					            <td nowrap="noWrap">
						            <table id="Table2" cellspacing="0" cellpadding="0" border="0">
							            <tr>
								            <td class="FormTableSubHeader" nowrap="noWrap" style="width:170px;">Event</td>
								            <td class="FormTableSubHeader"></td>
								            <td class="FormTableSubHeader" style="width:58px;">Date</td>
								            <td class="FormTableSubHeader"></td>
							            </tr>
				                        <tr>
					                        <td class="FieldLabel" nowrap="noWrap">Funded</td>
					                        <td></td>
					                        <td nowrap="noWrap"><ml:datetextbox id="sFundD" runat="server" CssClass="mask" width="75" preset="date" onchange="refreshCalculation();"></ml:datetextbox></td>
					                        <td></td>
				                        </tr>
				                        <tr>
					                        <td class="FieldLabel" nowrap="noWrap">Loan Sold</td>
					                        <td></td>
					                        <td nowrap="noWrap"><ml:datetextbox id="sLPurchaseD" runat="server" CssClass="mask" width="75" preset="date" onchange="refreshCalculation();"></ml:datetextbox></td>
					                        <td></td>
				                        </tr>
				                        <tr>
					                        <td class="FieldLabel" nowrap="noWrap">Investor funds disbursement</td>
					                        <td></td>
					                        <td nowrap="noWrap"><ml:datetextbox id="sDisbursementD" runat="server" CssClass="mask" width="75" preset="date" onchange="refreshCalculation();"></ml:datetextbox></td>
					                        <td></td>
				                        </tr>
				                        <tr>
					                        <td class="FieldLabel" nowrap="noWrap">Loan reconciled</td>
					                        <td></td>
					                        <td nowrap="noWrap"><ml:datetextbox id="sReconciledD" runat="server" CssClass="mask" width="75" preset="date" onchange="refreshCalculation();"></ml:datetextbox></td>
					                        <td></td>
				                        </tr>
                        				
                                        <tr>
                                          <td class="FieldLabel" colspan="4" nowrap="noWrap">&nbsp;</td>
                                        </tr>
                                		
		                                <tr>
                                          <td class=FieldLabel nowrap="noWrap">Shipped to warehouse</td>
                                          <td></td>
                                          <td noWrap><ml:datetextbox id="sShippedToWarehouseD" runat="server" preset="date" width="75" CssClass="mask"></ml:datetextbox></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td class=FieldLabel nowrap="noWrap">Loan Shipped</td>
                                          <td></td>
                                          <td nowrap="noWrap"><ml:datetextbox id="sShippedToInvestorD" runat="server" preset="date" width="75" CssClass="mask"></ml:datetextbox></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td class="FieldLabel" nowrap="noWrap">Investor Conditions</td>
                                          <td></td>
                                          <td nowrap="noWrap"><ml:DateTextBox ID="sSuspendedByInvestorD" runat="server" preset="date" Width="75" CssClass="mask"></ml:DateTextBox></td>
                                        </tr>
                                        <tr>
                                          <td class="FieldLabel" nowrap="noWrap">Investor Conditions Sent</td>
                                          <td></td>
                                          <td nowrap="noWrap"><ml:DateTextBox ID="sCondSentToInvestorD" runat="server" preset="date" Width="75" CssClass="mask"></ml:DateTextBox></ml:DateTextBox></td>
                                        </tr>
                                        <tr>
                                          <td class="FieldLabel" nowrap="noWrap">Additional conditions sent</td>
                                          <td></td>
                                          <td nowrap="noWrap"><ml:DateTextBox ID="sAdditionalCondSentD" runat="server" preset="date" Width="75" CssClass="mask"></ml:DateTextBox></td>
                                        </tr>
                                        <tr>
                                          <td class="FieldLabel" nowrap="noWrap">"Good-bye" letter date</td>
                                          <td></td>
                                          <td nowrap="noWrap"><ml:DateTextBox ID="sGoodByLetterD" runat="server" preset="date" Width="75" CssClass="mask"></ml:DateTextBox></td>
                                        </tr>
                                        
                                        <tr>
                                          <td class="FieldLabel" colspan="4" nowrap="noWrap">&nbsp;</td>
                                        </tr>
            
                                        <tr>
					                        <td class="FieldLabel" nowrap="noWrap">MERS Registration</td>
					                        <td></td>
					                        <td nowrap="noWrap"><ml:DateTextBox ID="sMersRegistrationD" runat="server" preset="date" Width="75" CssClass="mask" onchange="refreshCalculation();"></ml:DateTextBox></td>
					                    </tr>
					                    <tr>
					                        <td class="FieldLabel" nowrap="noWrap">MERS TOB</td>
					                        <td></td>
					                        <td nowrap="noWrap"><ml:DateTextBox ID="sMersTobD" runat="server" preset="date" Width="75" CssClass="mask" onchange="refreshCalculation();"></ml:DateTextBox></td>
					                    </tr>
					                    <tr>
					                        <td class="FieldLabel" nowrap="noWrap">MERS TOS</td>
					                        <td><asp:CheckBox ID="sMersTosDLckd" runat="server" onclick="refreshCalculation();" /></td>
					                        <td><ml:DateTextBox ID="sMersTosD" runat="server" preset="date" Width="75" CssClass="mask" onchange="refreshCalculation();"></ml:DateTextBox></td>
					                    </tr>
                                        
                                        <tr>
                                          <td class="FieldLabel" colspan="4" nowrap="noWrap">&nbsp;</td>
                                        </tr>
                                        
                                        <tr>
			                                <td class="FieldLabel" nowrap="noWrap">Servicing started</td>
			                                <td></td>
			                                <td nowrap="noWrap"><ml:datetextbox id="sServicingStartD" runat="server" CssClass="mask" width="75" preset="date" ></ml:datetextbox></td>
			                                <td></td>
		                                </tr>
		                                <tr>
			                                <td class="FieldLabel" nowrap>1st payment due</td>
			                                <td>
                                                <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" onclick="lockField(this, 'sSchedDueD1'); refreshCalculation();" />
			                                </td>
			                                <td nowrap="noWrap"><ml:datetextbox id="sSchedDueD1" runat="server" CssClass="mask" width="75" preset="date" ></ml:datetextbox></td>
			                                <td></td>
		                                </tr>
		                                <tr>
			                                <td class="FieldLabel" nowrap>1st payment due investor</td>
			                                <td></td>
			                                <td nowrap="nowrap="noWrap""><ml:datetextbox id="sInvSchedDueD1" runat="server" CssClass="mask" width="75" preset="date" ></ml:datetextbox></td>
			                                <td></td>
		                                </tr>
		                                <tr>
                                          <td class="FieldLabel" colspan="3" nowrap="noWrap">&nbsp;</td>
                                        </tr>
						            </table>
					            </td>
				            </tr>
				            <tr>
				                <td nowrap="noWrap">
						            <table id="Table1" cellspacing="0" cellpadding="0" border="0">
					                    <tr>
					                        <td class="FormTableSubHeader docCol">Document</td>
						                    <td class="FormTableSubHeader dateCol">Ordered</td>
						                    <td class="FormTableSubHeader dateCol">Document Date</td>
						                    <td class="FormTableSubHeader dateCol">Received</td>
						                    <td class="FormTableSubHeader commentsCol">Comments</td>
						                    <td class="FormTableSubHeader dateCol">Shipped</td>
						                    <td class="FormTableSubHeader methodCol">Method</td>
						                    <td class="FormTableSubHeader trackingCol">Tracking</td>
					                    </tr>
							            <tr>
							                <td class="fieldLabel">Loan Package</td>
							                <td><ml:DateTextBox ID="sLoanPackageOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sLoanPackageDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sLoanPackageReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><asp:TextBox ID="sLoanPackageN" runat="server"></asp:TextBox></td>
							                <td><ml:DateTextBox ID="sLoanPackageShippedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:ComboBox ID="sLoanPackageMethod" runat="server"></ml:ComboBox></td>
							                <td><asp:TextBox ID="sLoanPackageTracking" runat="server"></asp:TextBox></td>
							            </tr>
							            <tr>
							                <td class="fieldLabel">Mortgage / Deed of Trust</td>
							                <td><ml:DateTextBox ID="sMortgageDOTOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sMortgageDOTDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sMortgageDOTReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><asp:TextBox ID="sMortgageDOTN" runat="server"></asp:TextBox></td>
							                <td><ml:DateTextBox ID="sMortgageDOTShippedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:ComboBox ID="sMortgageDOTMethod" runat="server"></ml:ComboBox></td>
							                <td><asp:TextBox ID="sMortgageDOTTracking" runat="server"></asp:TextBox></td>
							            </tr>
							            <tr>
							                <td class="fieldLabel">Note</td>
							                <td><ml:DateTextBox ID="sNoteOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sDocumentNoteD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sNoteReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><asp:TextBox ID="sNoteN" runat="server"></asp:TextBox></td>
							                <td><ml:DateTextBox ID="sNoteShippedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:ComboBox ID="sNoteMethod" runat="server"></ml:ComboBox></td>
							                <td><asp:TextBox ID="sNoteTracking" runat="server"></asp:TextBox></td>
							            </tr>
							            <tr>
							                <td class="fieldLabel">Final HUD-1 Settlement Stmt</td>
							                <td><ml:DateTextBox ID="sFinalHUD1SttlmtStmtOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sFinalHUD1SttlmtStmtDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sFinalHUD1SttlmtStmtReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><asp:TextBox ID="sFinalHUD1SttlmtStmtN" runat="server"></asp:TextBox></td>
							                <td><ml:DateTextBox ID="sFinalHUD1SttlmtStmtShippedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:ComboBox ID="sFinalHUD1SttlmtStmtMethod" runat="server"></ml:ComboBox></td>
							                <td><asp:TextBox ID="sFinalHUD1SttlmtStmtTracking" runat="server"></asp:TextBox></td>
							            </tr>
							            <tr>
							                <td class="fieldLabel">Title Insurance Policy</td>
							                <td><ml:DateTextBox ID="sTitleInsPolicyOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sTitleInsPolicyDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sTitleInsPolicyReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><asp:TextBox ID="sTitleInsPolicyN" runat="server"></asp:TextBox></td>
							                <td><ml:DateTextBox ID="sTitleInsPolicyShippedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:ComboBox ID="sTitleInsPolicyMethod" runat="server"></ml:ComboBox></td>
							                <td><asp:TextBox ID="sTitleInsPolicyTracking" runat="server"></asp:TextBox></td>
							            </tr>
							            <tr>
							                <td class="fieldLabel">Mortgage Ins Certificate</td>
							                <td><ml:DateTextBox ID="sMiCertOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sMiCertIssuedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sMiCertReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><asp:TextBox ID="sMiCertN" runat="server"></asp:TextBox></td>
							                <td><ml:DateTextBox ID="sMiCertShippedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:ComboBox ID="sMiCertMethod" runat="server"></ml:ComboBox></td>
							                <td><asp:TextBox ID="sMiCertTracking" runat="server"></asp:TextBox></td>
							            </tr>
							            <tr>
							                <td class="fieldLabel">Final Hazard Ins Policy</td>
							                <td><ml:DateTextBox ID="sFinalHazInsPolicyOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sFinalHazInsPolicyDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sFinalHazInsPolicyReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><asp:TextBox ID="sFinalHazInsPolicyN" runat="server"></asp:TextBox></td>
							                <td><ml:DateTextBox ID="sFinalHazInsPolicyShippedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:ComboBox ID="sFinalHazInsPolicyMethod" runat="server"></ml:ComboBox></td>
							                <td><asp:TextBox ID="sFinalHazInsPolicyTracking" runat="server"></asp:TextBox></td>
							            </tr>
							            <tr>
							                <td class="fieldLabel">Final Flood Ins Policy</td>
							                <td><ml:DateTextBox ID="sFinalFloodInsPolicyOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sFinalFloodInsPolicyDocumentD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:DateTextBox ID="sFinalFloodInsPolicyReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><asp:TextBox ID="sFinalFloodInsPolicyN" runat="server"></asp:TextBox></td>
							                <td><ml:DateTextBox ID="sFinalFloodInsPolicyShippedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:ComboBox ID="sFinalFloodInsPolicyMethod" runat="server"></ml:ComboBox></td>
							                <td><asp:TextBox ID="sFinalFloodInsPolicyTracking" runat="server"></asp:TextBox></td>
							            </tr>
					<tr class="docRow" id = "sCustomTrailingDoc1Row1">
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc1Desc" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc1OrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc1DocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc1ReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><asp:TextBox runat="server" ID="sCustomTrailingDoc1N"/></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc1ShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sCustomTrailingDoc1Method" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc1Tracking" runat="server"></asp:TextBox></td>				       
				    </tr>

				    <tr class="docRow" id = "sCustomTrailingDoc2Row1">
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc2Desc" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc2OrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc2DocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc2ReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><asp:TextBox runat="server" ID="sCustomTrailingDoc2N"/></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc2ShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sCustomTrailingDoc2Method" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc2Tracking" runat="server"></asp:TextBox></td>
				    </tr>

				    <tr class="docRow" id = "sCustomTrailingDoc3Row1">
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc3Desc" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc3OrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc3DocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc3ReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><asp:TextBox runat="server" ID="sCustomTrailingDoc3N"/></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc3ShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sCustomTrailingDoc3Method" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc3Tracking" runat="server"></asp:TextBox></td>
				    </tr>
				    
				    <tr class="docRow" id = "sCustomTrailingDoc4Row1">
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc4Desc" runat="server"></asp:TextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc4OrderedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc4DocumentD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc4ReceivedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><asp:TextBox runat="server" ID="sCustomTrailingDoc4N"/></td>
				        <td nowrap="noWrap"><ml:DateTextBox ID="sCustomTrailingDoc4ShippedD" runat="server" preset="date"></ml:DateTextBox></td>
				        <td nowrap="noWrap"><ml:ComboBox ID="sCustomTrailingDoc4Method" runat="server"></ml:ComboBox></td>
				        <td nowrap="noWrap"><asp:TextBox ID="sCustomTrailingDoc4Tracking" runat="server"></asp:TextBox></td>
				    </tr>
							            <tr>
                                          <td class="FieldLabel" colspan="3" nowrap="noWrap">&nbsp;</td>
                                        </tr>
							        </table>
							    </td>
				            </tr>
				            <tr>
				                <td nowrap="noWrap">
						            <table id="Table3" cellspacing="0" cellpadding="0" border="0">
							            <tr>
								            <td class="FormTableSubHeader docCol">Warehouse release</td>
								            <td class="FormTableSubHeader dateCol">Ordered</td>
								            <td class="FormTableSubHeader dateCol"></td>
								            <td class="FormTableSubHeader dateCol"></td>
								            <td class="FormTableSubHeader commentsCol"></td>
								            <td class="FormTableSubHeader dateCol">Shipped</td>
								            <td class="FormTableSubHeader methodCol">Method</td>
								            <td class="FormTableSubHeader trackingCol">Tracking</td>
							            </tr>
							            <tr>
							                <td class="fieldLabel">Collateral Package</td>
							                <td><ml:DateTextBox ID="sCollateralPkgOrderedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td></td>
							                <td></td>
							                <td></td>
							                <td><ml:DateTextBox ID="sCollateralPkgShippedD" runat="server" preset="date"></ml:DateTextBox></td>
							                <td><ml:ComboBox ID="sCollateralPkgMethod" runat="server"></ml:ComboBox></td>
							                <td><asp:TextBox ID="sCollateralPkgTracking" runat="server"></asp:TextBox></td>
							            </tr>
							        </table>
							    </td>
				            </tr>
				            <tr>
                                <td class="FieldLabel" colspan="3" nowrap="noWrap">&nbsp;</td>
                            </tr>
				            <tr>
				                <td nowrap="noWrap">
						            <table id="Table4" cellspacing="0" cellpadding="0" border="0">
							            <tr>
								            <td class="FormTableSubHeader docCol">Documents archived</td>
								            <td class="FormTableSubHeader dateCol">Archived</td>
								            <td class="FormTableSubHeader dateCol"></td>
								            <td class="FormTableSubHeader dateCol"></td>
								            <td class="FormTableSubHeader commentsCol"></td>
								            <td class="FormTableSubHeader dateCol"></td>
								            <td class="FormTableSubHeader methodCol"></td>
								            <td class="FormTableSubHeader trackingCol"></td>
							            </tr>
							            <tr>
							                <td class="fieldLabel">Loan Documents</td>
							                <td><ml:DateTextBox ID="sLoanDocsArchiveD" runat="server" preset="date"></ml:DateTextBox></td>
							            </tr>
							            <tr>
							                <td class="fieldLabel">Trailing Documents</td>
							                <td><ml:DateTextBox ID="sTrailingDocsArchiveD" runat="server" preset="date"></ml:DateTextBox></td>
							            </tr>
							        </table>
							    </td>
				            </tr>
				        </table>
				    </td>
				</tr>
		</table>
    
    </div>
    </form>
    <script type="text/javascript">
    function _init() {
      lockField(<%= AspxTools.JsGetElementById(sMersTosDLckd) %>, 'sMersTosD');
      document.getElementById("btnChangeStatus").disabled = !<%= AspxTools.JsGetElementById(sSecondStatusLckd) %>.checked;
      lockField(<%=AspxTools.JsGetElementById(sSchedDueD1Lckd)%>, 'sSchedDueD1');
    }
    
    function displayStatusLink() {
    
        showModal('/newlos/Finance/LoanSecondaryStatusList.aspx', null, null, null, function(arg){ 
			if (arg.OK) {
				updateDirtyBit();    
				<%= AspxTools.JsGetElementById(sSecondStatus) %>.value = arg.Status;
				<%= AspxTools.JsGetElementById(sSecondStatusT) %>.value = arg.StatusCode;
				
				var today = new Date();
				var dateString = (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear();
				var status = parseInt(arg.StatusCode);
				if (status == 1) { //sFundD
					<%= AspxTools.JsGetElementById(sFundD) %>.value = dateString;
				} else if (status == 2) { // sLPurchaseD
					<%= AspxTools.JsGetElementById(sLPurchaseD) %>.value = dateString;
				} else if (status == 3) { // sDisbursementD
					<%= AspxTools.JsGetElementById(sDisbursementD) %>.value = dateString;
				} else if (status == 4) { // sReconciledD
					<%= AspxTools.JsGetElementById(sReconciledD) %>.value = dateString;
				}
			}
			return arg;
		},{ hideCloseButton: true, width: 300, height: 200});
    }
    </script> 
    <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
</body>
</html>
