﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicingLineItem.ascx.cs" Inherits="LendersOfficeApp.newlos.Finance.ServicingLineItem" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<tr id="row" runat="server">
    <td><ml:EncodedLabel ID="Value" runat="server"></ml:EncodedLabel></td>
    <td align="right"><ml:EncodedLabel ID="Principal" runat="server"></ml:EncodedLabel></td>
    <td align="right"><ml:EncodedLabel ID="Interest" runat="server"></ml:EncodedLabel></td>
    <td align="right"><ml:EncodedLabel ID="Escrow" runat="server"></ml:EncodedLabel></td>
    <td align="right"><ml:EncodedLabel ID="LateFees" runat="server"></ml:EncodedLabel></td>
    <td align="right"><ml:EncodedLabel ID="Other" runat="server"></ml:EncodedLabel></td>

</tr>