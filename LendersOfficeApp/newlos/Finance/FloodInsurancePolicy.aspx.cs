﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class FloodInsurancePolicy : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FloodInsurancePolicy));
            dataLoan.InitLoad();

            sFloodInsCompanyNm.Text = dataLoan.sFloodInsCompanyNm;
            sFloodInsCompanyId.Text = dataLoan.sFloodInsCompanyId;
            sFloodInsPolicyNum.Text = dataLoan.sFloodInsPolicyNum;
            sFloodInsPolicyPayeeCode.Text = dataLoan.sFloodInsPolicyPayeeCode;

            Tools.SetDropDownListValue(sFloodInsPaidByT, dataLoan.sFloodInsPaidByT);
            Tools.SetDropDownListValue(sFloodInsPolicyPaymentTypeT, dataLoan.sFloodInsPolicyPaymentTypeT);

            // Money
            sFloodInsCoverageAmt.Text = dataLoan.sFloodInsCoverageAmt_rep;
            sFloodInsDeductibleAmt.Text = dataLoan.sFloodInsDeductibleAmt_rep;
            sFloodInsPaymentDueAmtLckd.Checked = dataLoan.sFloodInsPaymentDueAmtLckd;
            sFloodInsPaymentDueAmt.Text = dataLoan.sFloodInsPaymentDueAmt_rep;

            // Dates
            sFloodInsPolicyActivationD.Text = dataLoan.sFloodInsPolicyActivationD_rep;
            sFloodInsPaymentDueD.Text = dataLoan.sFloodInsPaymentDueD_rep;
            sFloodInsPaymentMadeD.Text = dataLoan.sFloodInsPaymentMadeD_rep;
            sFloodInsPaidThroughD.Text = dataLoan.sFloodInsPaidThroughD_rep;
            sFloodInsPolicyExpirationD.Text = dataLoan.sFloodInsPolicyExpirationD_rep;
            sFloodInsPolicyLossPayeeTransD.Text = dataLoan.sFloodInsPolicyLossPayeeTransD_rep;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "FloodInsurancePolicy";
            this.PageID = "FloodInsurancePolicy";

            Tools.Bind_sInsPaidByWithHOA(sFloodInsPaidByT);
            Tools.Bind_sInsPolicyPaymentTypeT(sFloodInsPolicyPaymentTypeT);
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
