﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MortgageInsurancePolicy.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.MortgageInsurancePolicy" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess"%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Mortgage Insurance Policy</title>
</head>
<body bgcolor="gainsboro">
    <script language="javascript">
        function doAfterDateFormat(e) 
        {
            refreshCalculation();
        }

        function toggleMIAbsenceBlankVisbility()
        {
            var miAbsentDDL = $("#sMiReasonForAbsenceT");

            var blankOption = $(miAbsentDDL).find("option[value=<%=AspxTools.JsString(E_sMiReasonForAbsenceT.None)%>]");

            if ($("#sMiInsuranceT").find("option:selected").val() === <%=AspxTools.JsString(E_sMiInsuranceT.None)%>) 
            {
                if ($(miAbsentDDL).val() === <%=AspxTools.JsString(E_sMiReasonForAbsenceT.None)%>) 
                {
                    // If the reason for absence is 'None' when the insurance type is set to 'None', 
                    // force the dropdown to display the first non-'None' option.
                    $(miAbsentDDL).val($(blankOption).next('option').val());
                }

                $(blankOption).remove();
            }
            else if (blankOption.length === 0) 
            {
                // Insert the blank option into the dropdown list but preserve the selected entry.
                var selectedValue = $(miAbsentDDL).val();

                $(miAbsentDDL)[0].options.add(new Option("", <%=AspxTools.JsString(E_sMiReasonForAbsenceT.None)%>), 0);

                $(miAbsentDDL).val(selectedValue);
            }
        }
        
        $(function () 
        {
            $("#sMiInsuranceT").change(function () 
            {
                toggleMIAbsenceBlankVisbility();

                refreshCalculation();
            });
        });
    </script>
    <style type="text/css">
        img
        {
            vertical-align: bottom;
        }
    </style>
    <form id="form1" runat="server">
        <table cellSpacing="0" cellPadding="0" width="100%" >
			<tr>
				<td class="MainRightHeader" noWrap>
				    Mortgage Insurance Policy
				</td>
			</tr>
			<tr>
				<td>
				    <table class="FieldLabel" >
				        <tr>
				            <td>
    				            Mortgage Insurance type
				            </td>
				            <td>
				                <asp:DropDownList ID="sMiInsuranceT" runat="server"></asp:DropDownList>
				            </td>
				            <td id="miAbsenseLbl">
				                Primary reason for MI absence
				            </td>
				            <td id="miAbsenseDDL">
				                <asp:DropDownList ID="sMiReasonForAbsenceT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>				            
				            </td>
				        </tr>
				        <tbody id="MISection">
				        <tr id="sMiLenderPaidAdjRow">
				            <td>
				                Lender paid MI rate adj.
				            </td>
				            <td>
								<ml:percenttextbox id="sMiLenderPaidAdj" runat="server" onchange="refreshCalculation();"></ml:percenttextbox>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                MI coverage %
				            </td>
				            <td>
								<ml:percenttextbox id="sMiLenderPaidCoverage" runat="server" onchange="refreshCalculation();" decimalDigits="4"></ml:percenttextbox>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                MI provider
				            </td>
				            <td>
				                <asp:DropDownList ID="sMiCompanyNmT" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                MI commitment requested date
				            </td>
				            <td>
    	                        <ml:DateTextBox ID="sMiCommitmentRequestedD" runat="server" width="75" CssClass="mask" preset="date"/>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                MI commitment received date
				            </td>
				            <td>
    	                        <ml:DateTextBox ID="sMiCommitmentReceivedD" runat="server" width="75" CssClass="mask" preset="date"/>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                MI commitment expiration date
				            </td>
				            <td>
    	                        <ml:DateTextBox ID="sMiCommitmentExpirationD" runat="server" width="75" CssClass="mask" preset="date"/>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                UFMIP / FF amount
				            </td>
				            <td>
				                <asp:TextBox ID="sFfUfmip1003" runat="server" ReadOnly="true"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                Payment due date
				            </td>
				            <td>
    	                        <ml:DateTextBox ID="sMiPmtDueD" runat="server" width="75" CssClass="mask" preset="date"/>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                Payment made date
				            </td>
				            <td >
    	                        <ml:DateTextBox ID="sMiPmtMadeD" runat="server" width="75" CssClass="mask" preset="date"/>
    	     
				            </td>
				            <td>
				                Check #
				            </td>
				            <td>
    	                         <asp:TextBox runat="server" ID="sMiPmtCheckNumber" MaxLength="25" ></asp:TextBox> 
				            </td>
				        </tr>
				        <tr>
				            <td>
				                Paid through date
				            </td>
				            <td>
    	                        <ml:DateTextBox ID="sMiPaidThruD" runat="server" width="75" CssClass="mask" preset="date"/>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                MI certificate ID
				            </td>
				            <td>
				                <asp:TextBox ID="sMiCertId" runat="server" onchange="refreshCalculation();"></asp:TextBox>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                MI certificate ordered date
				            </td>
				            <td>
    	                        <ml:DateTextBox ID="sMiCertOrderedD" runat="server" width="75" CssClass="mask" preset="date"/>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                MI certificate issued date
				            </td>
				            <td>
    	                        <ml:DateTextBox ID="sMiCertIssuedD" runat="server" width="75" CssClass="mask" preset="date"/>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                MI certificate received date
				            </td>
				            <td>
    	                        <ml:DateTextBox ID="sMiCertReceivedD" runat="server" width="75" CssClass="mask" preset="date"/>
				            </td>
				        </tr>
				        <tr>
				            <td>
				                First month prorata
				            </td>
				            <td>
    	                        <asp:DropDownList runat="server" ID="sMiFirstProrataT">
    	                        </asp:DropDownList>  
				            </td>
				        </tr>
				        <tr>
				            <td>
				                Last month prorata
				            </td>
				            <td>
    	                        <asp:DropDownList runat="server" ID="sMiLastProrataT">
    	                        </asp:DropDownList>    
    	                    </td>
				        </tr>
				        <tr>
				            <td>
				                Reserves may be held in escrow
				            </td>
				            <td>
    	                        <asp:RadioButtonList ID="sMiIsNoReserve" runat="server" RepeatDirection="Horizontal">
    	                            <asp:ListItem Selected="True" Value="True" Text="Yes" />
    	                            <asp:ListItem Value="False" Text="No" />
    	                        </asp:RadioButtonList>
				            </td>
				        </tr>
				        </tbody>		        
				    </table>
				</td>
			</tr>
		</table>
    </form>
</body>
</html>
