﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneratePaymentStatement.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.GeneratePaymentStatement" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Register TagPrefix="uc" TagName="LeftRightPicker" Src="~/los/LeftRightPicker.ascx" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title>Generate Payment Statement</title>
    <style>
        body {
            background-color: gainsboro;
        }

        .ContentContainer {
            width: 795px;
        }

        .FormTableHeader {
            line-height: 20px;
            text-align: center;
        }

        div.Table {
            margin-top: 10px;
            display: table;
            width: 600px;
            margin-left: auto;
            margin-right: auto;
        }

            div.Table > div {
                display: table-row;
            }

                div.Table > div > div {
                    display: table-cell;
                }

        .ButtonContainer {
            text-align: center;
        }

            .ButtonContainer input {
                margin-top: 10px;
                margin-left: 5px;
                margin-right: 5px;
            }
    </style>
</head>
<body>
    <h4 class="page-header">Generate Payment Statement PDF</h4>
    <form id="form1" runat="server">
        <asp:HiddenField runat="server" ID="SelectedFormIds" />
        <div class="ContentContainer">
            <div class="Table">
                <div>
                    <div>
                        <input type="checkbox" id="includeDelinquencyNotice" />
                    </div>
                    <div class="FieldLabel">
                        Include Delinquency Notice
                    </div>
                    <div class="FieldLabel">
                        Previous Statement Period Start
                    </div>
                    <div>
                        <ml:DateTextBox runat="server" ID="periodStart"></ml:DateTextBox>
                        <asp:RequiredFieldValidator runat="server" ID="periodStartValidator" ControlToValidate="periodStart" Display="Dynamic"><img runat="server" src="../../images/error_icon.gif" alt="This is a required field."/></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div>
                    <div>
                        <input type="checkbox" id="includeSuspenseAccountNotice" />
                    </div>
                    <div class="FieldLabel">
                        Include Suspense Account Notice
                    </div>
                    <div class="FieldLabel">
                        Previous Statement Period End
                    </div>
                    <div>
                        <ml:DateTextBox runat="server" ID="periodEnd"></ml:DateTextBox>
                        <asp:RequiredFieldValidator runat="server" ID="periodEndValidator" ControlToValidate="periodEnd" Display="Dynamic"><img runat="server" src="../../images/error_icon.gif" alt="This is a required field."/></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div>
                    <div>
                        <input type="checkbox" id="includeOtherNotices" />
                    </div>
                    <div class="FieldLabel">
                        Include Other Notices
                    </div>
                    <div class="FieldLabel">
                        Potential Late Fee
                    </div>
                    <div>
                        <ml:MoneyTextBox runat="server" ID="potentialLateFee"></ml:MoneyTextBox>
                        <asp:RequiredFieldValidator runat="server" ID="potentialLateFeeValidator" ControlToValidate="potentialLateFee" Display="Dynamic"><img runat="server" src="../../images/error_icon.gif" alt="This is a required field."/></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div>
                    <div>
                        <input type="checkbox" id="useLegalPageSize" />
                    </div>
                    <div class="FieldLabel">
                        Use Legal Page Size
                    </div>
                    <div class="FieldLabel">
                        Late Fee Date
                    </div>
                    <div class="FieldLabel">
                        <ml:DateTextBox runat="server" ID="lateFeeDate"></ml:DateTextBox>
                        <asp:RequiredFieldValidator runat="server" ID="lateFeeDateValidator" ControlToValidate="lateFeeDate" Display="Dynamic"><img runat="server" src="../../images/error_icon.gif" alt="This is a required field."/></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div>
                    <div>
                        <input type="checkbox" id="appendCustomPdfs" />
                    </div>
                    <div class="FieldLabel">
                        Append Custom PDF(s)
                    </div>
                    <div class="FieldLabel"></div>
                    <div class="FieldLabel"></div>
                </div>
            </div>
            <div id="PickerContainer">
                <uc:LeftRightPicker runat="server" ID="CustomPdfForms" AvailableHeader="Available PDFs" AssignedHeader="Included PDFs" EnableModalBehavior="false" />
            </div>
            <div class="ButtonContainer">
                <input type="button" value="Generate and Save" id="Generate" />
                <input type="button" value="Preview" id="Preview" />
                <input type="button" value="Close" id="ClosePaymentStatement" />
            </div>
        </div>
    </form>
    <script>
        function doAfterDateFormat() {
            Page_ClientValidate();
        }

        jQuery(function ($) {
            Page_ClientValidate();

            $('#periodStart, #periodEnd, #potentialLateFee, #lateFeeDate').change(function() {
                Page_ClientValidate();
            });

            function generatePaymentStatementSynchronously(url, options, callback) {
                var downloadSettings = {
                    url: url + '&cmd=submit&jobType=2&' + ML.SynchronousPdfFallbackQueryStringParameterName + '=true',
                    data: JSON.stringify(options),
                    displayOverlay: true,
                    error: function () { alert('System error. Please contact us if this happens again.'); },
                    success: function (result) {
                        if (typeof callback === 'function') {
                            callback(result.FileDbKey);
                        }
                        else {
                            fetchFileAsync(url + '&cmd=retrieve&jobType=2&FileDbKey=' + result.FileDbKey, 'application/pdf');
                        }
                    }
                };

                callWebMethodAsync(downloadSettings);
            }

            $('#Generate').click(function () {
                if (!Page_ClientValidate()) {
                    return;
                }

                var url = gVirtualRoot + "/pdf/PaymentStatementGeneratePdf.aspx?crack=" + new Date();
                var options = getPaymentStatementData();

                if (ML.UseSynchronousPdfGenerationFallback) {
                    generatePaymentStatementSynchronously(url, options, SavePaymentStatementToEdocs);
                }
                else {
                    LqbAsyncPdfDownloadHelper.SubmitPaymentStatementGenerate(url, options, SavePaymentStatementToEdocs);
                }
            });

            function SavePaymentStatementToEdocs(fileDbKey) {
                var args =
                {
                    LoanId: ML.sLId,
                    ApplicationId: ML.aAppId,
                    FileDbKey: fileDbKey,
                    PaymentIndex: <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("index")) %>
                };

                var result = gService.loanedit.call('SavePaymentStatementToEdocs', args);

                if (result.error) {
                    var message = result.UserMessage || 'An unexpected error occurred. Please try again.';
                    alert(message);
                    window.parent.LQBPopup.Return();
                    return;
                }

                window.parent.LQBPopup.Return({
                    documentId: result.value.DocumentId
                });
            }

            $('#Preview').click(function () {
                if (!Page_ClientValidate()) {
                    return;
                }

                var url = gVirtualRoot + "/pdf/PaymentStatementPreviewPdf.aspx?crack=" + new Date();
                var options = getPaymentStatementData();

                if (ML.UseSynchronousPdfGenerationFallback) {
                    generatePaymentStatementSynchronously(url, options);
                }
                else {
                    LqbAsyncPdfDownloadHelper.SubmitPaymentStatementDownload(url, options);
                }
            });

            $('#ClosePaymentStatement').click(function () {
                window.parent.LQBPopup.Hide();
            });
        });

        function getPaymentStatementData() {
            return {
                BrokerId: ML.BrokerId,
                IncludeDelinquencyNotice: $j('#includeDelinquencyNotice').is(':checked'),
                IncludeSuspenseAccountNotice: $j('#includeSuspenseAccountNotice').is(':checked'),
                IncludeOtherNotices: $j('#includeOtherNotices').is(':checked'),
                AppendCustomPdfs: $j('#appendCustomPdfs').is(':checked'),
                PeriodStart: $j('#periodStart').val(),
                PeriodEnd: $j('#periodEnd').val(),
                PotentialLateFee: $j('#potentialLateFee').val().replace(/[^0-9.-]+/g,""),
                LateFeeDate: $j('#lateFeeDate').val(),
                LoanId: ML.sLId,
                SelectedCustomPdfIds: <%= AspxTools.JsStringUnquoted(this.CustomPdfForms.ClientID) %>_GetSelectedItems(),
                PaymentIndex: <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("index")) %>,
                UseLegalPageSize: $j('#useLegalPageSize').is(':checked'),
                ApplicationId: ML.aAppId
            };
        }
    </script>
</body>
</html>
