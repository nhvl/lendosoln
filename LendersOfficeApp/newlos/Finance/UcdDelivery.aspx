﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UcdDelivery.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.UcdDelivery" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Integration.UcdDelivery" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" ng-app="UcdDelivery">
    <head runat="server">
        <title>UCD Delivery</title>
    </head>
    <body>
        <form id="form1" runat="server">
            <div class="MainRightHeader" runat="server" id="PageHeader">UCD Delivery</div>

            <div id="DeliveriesTableControllerDiv" ng-controller="DeliveriesTableController" ng-form="DeliveriesTableForm">
                <table class="DeliveriesTable">
                    <thead>
                        <tr class="GridHeader">
                            <th class="CaseFileIdColumn">Casefile ID</th>
                            <th class="DeliveredToColumn">Delivered To</th>
                            <th class="UcdFileColumn">UCD File</th>
                            <th class="DateOfDeliveryColumn">Date of Delivery</th>
                            <th class="IncludeInUlddColumn">Include Casefile<br />ID in ULDD</th>
                            <th ng-if="hasNonManual">Batch ID</th>
                            <th ng-if="hasNonManual">Status</th>
                            <th class="FindingsColumn">Findings</th>
                            <th class="NoteColumn">Note</th>
                            <th class="RemoveColumn"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="GridAutoItem" ng-repeat="delivery in deliveries" ng-if="!delivery.isDeleted">
                            <td>
                                <span class="spanCaseFileId" ng-if="delivery.UcdDeliveryType != <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>">{{delivery.CaseFileId}}</span>
                                <input type="text" class="CaseFileId" maxlength="50" ng-change="markDirty($index)" ng-model="delivery.CaseFileId" ng-if="delivery.UcdDeliveryType == <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>" required auto-wire-input />
                            </td>
                            <td>
                                <span class="spanDeliveredTo" ng-if="delivery.UcdDeliveryType != <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>">{{delivery.DeliveredTo_rep}}</span>
                                <select class="DeliveredTo" ng-change="changeDeliveredTo($index)" ng-model="delivery.DeliveredTo" convert-to-number ng-if="delivery.UcdDeliveryType == <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>" must-be-non-zero auto-wire-input>
                                    <option value=<%= AspxTools.HtmlAttribute(UcdDeliveredToEntity.Blank.ToString("d")) %>></option>
                                    <option value=<%= AspxTools.HtmlAttribute(UcdDeliveredToEntity.FannieMae.ToString("d")) %>>Fannie Mae</option>
                                    <option value=<%= AspxTools.HtmlAttribute(UcdDeliveredToEntity.FreddieMac.ToString("d")) %>>Freddie Mac</option>
                                </select>
                            </td>
                            <td>
                                <div class="ucdFileDiv" ng-model="delivery.FileDeliveredDocumentId" must-have-edoc>
                                    <input type="checkbox" class="IsUcdDocumentLinked" disabled="disabled" ng-checked="delivery.FileDeliveredDocumentId"/>
                                    <span class="UnlinkedUcdOptionsId" ng-if="!delivery.FileDeliveredDocumentId">
                                        <a ng-click="uploadUcdDocument($index)">upload</a>
                                        &nbsp;
                                        <a ng-click="associateUcdDocument($index)">associate</a>
                                    </span>
                                    <span class="LinkedUcdOptionsId" ng-if="delivery.FileDeliveredDocumentId">
                                        <a ng-click="viewUcdDocument($index)" ng-disabled="!delivery.CanViewFileDeliveredDocument">view</a>
                                        &nbsp;
                                        <a ng-click="downloadUcdDocument($index)" ng-disabled="!delivery.CanViewFileDeliveredDocument">download</a>
                                        {{delivery.UcdDeliveryType == <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %> ? "&nbsp;" : ""}}
                                        <a ng-click="clearUcdDocument($index)" ng-if="delivery.UcdDeliveryType == <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>">clear</a>
                                        &nbsp;
                                        <img ng-if="delivery.FileDeliveredDocumentMetadataJSON" ng-attr-data-docmetadata="{{delivery.FileDeliveredDocumentMetadataJSON}}" alt="Info" src="../../images/edocs/view.png" />
                                    </span>
                                </div>
                            </td>
                            <td>
                                <span class="spanDateOfDelivery" ng-if="delivery.UcdDeliveryType != <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>">{{delivery.DateOfDelivery}}</span>
                                <span ng-if="delivery.UcdDeliveryType == <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>">
                                    <ml:DateTextBox ID="DateOfDelivery" CssClass="DateOfDelivery" runat="server" preset="date" ng-change="markDirty($index)" ng-model="delivery.DateOfDelivery" required auto-wire-calendar></ml:DateTextBox>
                                </span>
                            </td>
                            <td class="tdIncludeInUldd">
                                <input ng-if="delivery.DeliveredTo !== <%= AspxTools.JsNumeric(UcdDeliveredToEntity.FreddieMac) %>" type="checkbox" class="IncludeInUldd" id="IncludeInUlddId" ng-model="delivery.IncludeInUldd" ng-click="fixUpUlddCheckboxes($index)" auto-wire-input />
                            </td>
                            <td ng-if="hasNonManual">
                                <span ng-if="delivery.UcdDeliveryType != <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>">{{delivery.BatchId}}</span>
                            </td>
                            <td ng-if="hasNonManual">
                                <span ng-if="delivery.UcdDeliveryType != <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>">{{delivery.Status}}</span>
                            </td>
                            <td>
                                <span ng-if="delivery.UcdDeliveryType != <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>">
                                    <a ng-click="viewFindings($index)">view</a>
                                </span>
                                &nbsp;
                                <span ng-if="checkCanRetrieve($index)">
                                    <a class="DisabledLink" ng-if="IsNewOrDirty($index)" title="Please save this record to retrieve findings.">retrieve</a>
                                    <a ng-if="!IsNewOrDirty($index)" ng-click="retrieveFindings($index)">retrieve</a>
                                </span>
                            </td>
                            <td>
                                <textarea class="Note" rows="3" maxlength="500" ng-change="markDirty($index)" ng-model="delivery.Note" auto-wire-input></textarea>
                            </td>
                            <td>
                                <input type="button" value="-" ng-click="removeDelivery($index)" ng-if="delivery.UcdDeliveryType == <%= AspxTools.JsNumeric(UcdDeliveryType.Manual) %>" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <input type="button" value="Add Manual Delivery Record" ng-click="addDelivery()" />
                <input type="button" value="Generate DocMagic UCD" ng-click="DocMagicGenerateUCD()" ng-if="HasDocMagicVendor" />
                <input type="button" value="Generate DocMagic UCD and Deliver" ng-click="DocMagicGenerateAndDeliverUCD()" ng-if="HasDocMagicVendor && DocMagicDeliveryEnabled" />
                <input type="button" value="Deliver Existing UCD" ng-click="DeliverExistingUCD()" ng-if="DirectIntegrationEnabled" />
            </div>
        </form>
        <script type="text/javascript">
            var oldSaveMe = window.saveMe;
            window.saveMe = function (bRefreshScreen) {
                if (SaveUcdDeliveries()) {
                    return oldSaveMe(bRefreshScreen);
                }

                return false;
            }

            function SaveUcdDeliveries() {
                var $deliveriesTableScope = getScope("#DeliveriesTableControllerDiv");

                if ($deliveriesTableScope.DeliveriesTableForm.$invalid)
                {
                    alert("Cannot save. Please ensure that all required fields are filled out.");
                    return false;
                }

                if (!$deliveriesTableScope.saveDeliveries()) {
                    return false;
                }

                return true;
            }
        </script>
    </body>
</html>
