﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class PurchaseAdvice : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantRead };
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(PurchaseAdvice));
            dataLoan.InitLoad();

            BindInvestorNames(dataLoan);

            sPurchaseAdviceUnpaidPBal2.Text = dataLoan.sPurchaseAdviceUnpaidPBal_rep;
            sPurchaseAdviceTotalPrice_Field22.Text = dataLoan.sPurchaseAdviceTotalPrice_Field2_rep;
            sPurchaseAdviceInterestTotalAdj2.Text = dataLoan.sPurchaseAdviceInterestTotalAdj_rep;
            sPurchaseAdviceEscrowAmtDueInv2.Text = dataLoan.sPurchaseAdviceEscrowAmtDueInv_rep;
            sPurchaseAdviceSummaryOtherAdjDesc.Text = dataLoan.sPurchaseAdviceSummaryOtherAdjDesc;
            sPurchaseAdviceSummaryOtherAdjVal.Text = dataLoan.sPurchaseAdviceSummaryOtherAdjVal_rep;
            sPurchaseAdviceSummaryInvNm.Text = dataLoan.sPurchaseAdviceSummaryInvNm;
            sPurchaseAdviceSummaryInvId.Value = dataLoan.sPurchaseAdviceSummaryInvId_rep;
            sPurchaseAdviceSummaryInvPgNm.Text = dataLoan.sPurchaseAdviceSummaryInvPgNm;
            sPurchaseAdviceSummaryInvLoanNm.Text = dataLoan.sPurchaseAdviceSummaryInvLoanNm;
            sPurchaseAdviceFeesTotal2.Text = dataLoan.sPurchaseAdviceFeesTotal_rep;
            sPurchaseAdviceSummarySubtotal.Text = dataLoan.sPurchaseAdviceSummarySubtotal_rep;
            sPurchaseAdviceSummaryTotalDueSeller.Text = dataLoan.sPurchaseAdviceSummaryTotalDueSeller_rep;
            sClosedD.Text = dataLoan.sClosedD_rep;
            sLPurchaseD.Text = dataLoan.sLPurchaseD_rep;
            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sInvSchedDueD1.Text = dataLoan.sInvSchedDueD1_rep;
            Tools.SetDropDownListValue(sPurchaseAdviceSummaryServicingStatus, dataLoan.sPurchaseAdviceSummaryServicingStatus);

            switch (dataLoan.sPurchaseAdviceBasePrice_AmountPriceCalcMode)
            {
                case E_AmountPriceCalcMode.Amount:
                    sPurchaseAdviceBasePrice_AmountPriceCalcMode_Amount.Checked = true;
                    break;
                case E_AmountPriceCalcMode.Price:
                    sPurchaseAdviceBasePrice_AmountPriceCalcMode_Price.Checked = true;
                    break;
                default:

                    throw new UnhandledEnumException(dataLoan.sPurchaseAdviceBasePrice_AmountPriceCalcMode);
            }

            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sPurchaseAdviceAmortCurtail.Text = dataLoan.sPurchaseAdviceAmortCurtail_rep;
            sPurchaseAdviceUnpaidPBal.Text = dataLoan.sPurchaseAdviceUnpaidPBal_rep;
            sPurchaseAdviceBasePrice_Field1.Text = dataLoan.sPurchaseAdviceBasePrice_Field1_rep;
            sPurchaseAdviceBasePrice_Field2.Text = dataLoan.sPurchaseAdviceBasePrice_Field2_rep;
            sPurchaseAdviceBasePrice_Field3.Text = dataLoan.sPurchaseAdviceBasePrice_Field3_rep;
            sPurchaseAdviceAdjustments_Field2.Text = dataLoan.sPurchaseAdviceAdjustments_Field2_rep;
            sPurchaseAdviceAdjustments_Field3.Text = dataLoan.sPurchaseAdviceAdjustments_Field3_rep;
            sPurchaseAdviceNetPrice_Field1.Text = dataLoan.sPurchaseAdviceNetPrice_Field1_rep;
            sPurchaseAdviceNetPrice_Field2.Text = dataLoan.sPurchaseAdviceNetPrice_Field2_rep;
            sPurchaseAdviceNetPrice_Field3.Text = dataLoan.sPurchaseAdviceNetPrice_Field3_rep;
            sPurchaseAdviceSRP_Field2.Text = dataLoan.sPurchaseAdviceSRP_Field2_rep;
            sPurchaseAdviceSRP_Field3.Text = dataLoan.sPurchaseAdviceSRP_Field3_rep;
            sPurchaseAdviceTotalPrice_Field1.Text = dataLoan.sPurchaseAdviceTotalPrice_Field1_rep;
            sPurchaseAdviceTotalPrice_Field2.Text = dataLoan.sPurchaseAdviceTotalPrice_Field2_rep;
            sPurchaseAdviceTotalPrice_Field3.Text = dataLoan.sPurchaseAdviceTotalPrice_Field3_rep;

            sPurchaseAdviceInterestDueSellerOrInvestor.Text = dataLoan.sPurchaseAdviceInterestDueSellerOrInvestor_rep;
            sPurchaseAdviceInterestReimbursement.Text = dataLoan.sPurchaseAdviceInterestReimbursement_rep;
            sPurchaseAdviceInterestTotalAdj.Text = dataLoan.sPurchaseAdviceInterestTotalAdj_rep;
            
            sPurchaseAdviceEscrowTotCollAtClosing.Text = dataLoan.sPurchaseAdviceEscrowTotCollAtClosing_rep;
            sPurchaseAdviceEscrowDisbursements.Text = dataLoan.sPurchaseAdviceEscrowDisbursements_rep;
            sPurchaseAdviceEscrowDepDue.Text = dataLoan.sPurchaseAdviceEscrowDepDue_rep;
            sPurchaseAdviceEscrowAmtDueInv.Text = dataLoan.sPurchaseAdviceEscrowAmtDueInv_rep;

            RegisterJsObject("Adjustments", dataLoan.sPurchaseAdviceAdjustments);
            RegisterJsObject("Fees", dataLoan.sPurchaseAdviceFees);
            sPurchaseAdviceFeesTotal.Text = dataLoan.sPurchaseAdviceFeesTotal_rep;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "PurchaseAdvice";
            this.PageID = "PurchaseAdvice";

            RegisterJsScript("DynamicTable.js");

            if (!(BrokerUser.HasPermission(Permission.AllowLockDeskRead) &&
                BrokerUser.HasPermission(Permission.AllowCloserRead)))
                m_pullLoanDetailsBtn.Disabled = true;

            Tools.Bind_PurchaseAdviceServicingStatus(sPurchaseAdviceSummaryServicingStatus);

        }

        private void BindInvestorNames(CPageData dataLoan)
        {
            List<InvestorRolodexEntry> entries = InvestorRolodexEntry.GetAll(PrincipalFactory.CurrentPrincipal.BrokerId, null).ToList();

            var nameToIdMap = new Dictionary<string, string>();
            int selected = dataLoan.sPurchaseAdviceSummaryInvId;
            
            foreach (var entry in entries.Where(p => p.Status == E_InvestorStatus.Active || p.Id == selected))
            {
                sPurchaseAdviceSummaryInvNm.Items.Add(entry.InvestorName);
                if (!nameToIdMap.ContainsKey(entry.InvestorName))
                {
                    nameToIdMap.Add(entry.InvestorName, entry.Id.ToString());
                }
            }

            var serializedMap = ObsoleteSerializationHelper.JavascriptJsonSerialize(nameToIdMap);
            ClientScript.RegisterStartupScript(typeof(PurchaseAdvice), "investorNameMap", "var investorNameMap = " + serializedMap, true);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
