﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class CondoHO6InsurancePolicy : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }

        private void BindSettlementChargeDDL(CPageData dataLoan)
        {
            sCondoHO6InsSettlementChargeT.Items.Add(Tools.CreateEnumListItem("<- Select a settlement charge ->", E_sInsSettlementChargeT.None));
            sCondoHO6InsSettlementChargeT.Items.Add(Tools.CreateEnumListItem(dataLoan.s1006ProHExpDesc, E_sInsSettlementChargeT.Line1008));
            sCondoHO6InsSettlementChargeT.Items.Add(Tools.CreateEnumListItem(dataLoan.s1007ProHExpDesc, E_sInsSettlementChargeT.Line1009));
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                sCondoHO6InsSettlementChargeT.Items.Add(Tools.CreateEnumListItem(dataLoan.sU3RsrvDesc, E_sInsSettlementChargeT.Line1010));
                sCondoHO6InsSettlementChargeT.Items.Add(Tools.CreateEnumListItem(dataLoan.sU4RsrvDesc, E_sInsSettlementChargeT.Line1011));
            }
        }
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(CondoHO6InsurancePolicy));
            dataLoan.InitLoad();

            BindSettlementChargeDDL(dataLoan);

            sCondoHO6InsCompanyNm.Text = dataLoan.sCondoHO6InsCompanyNm;
            sCondoHO6InsCompanyId.Text = dataLoan.sCondoHO6InsCompanyId;
            sCondoHO6InsPolicyNum.Text = dataLoan.sCondoHO6InsPolicyNum;
            sCondoHO6InsPolicyPayeeCode.Text = dataLoan.sCondoHO6InsPolicyPayeeCode;

            Tools.SetDropDownListValue(sCondoHO6InsSettlementChargeT, dataLoan.sCondoHO6InsSettlementChargeT);
            Tools.SetDropDownListValue(sCondoHO6InsPaidByT, dataLoan.sCondoHO6InsPaidByT);
            Tools.SetDropDownListValue(sCondoHO6InsPolicyPaymentTypeT, dataLoan.sCondoHO6InsPolicyPaymentTypeT);

            // Money
            sCondoHO6InsCoverageAmt.Text = dataLoan.sCondoHO6InsCoverageAmt_rep;
            sCondoHO6InsDeductibleAmt.Text = dataLoan.sCondoHO6InsDeductibleAmt_rep;
            sCondoHO6InsPaymentDueAmtLckd.Checked = dataLoan.sCondoHO6InsPaymentDueAmtLckd;
            sCondoHO6InsPaymentDueAmt.Text = dataLoan.sCondoHO6InsPaymentDueAmt_rep;

            // Dates
            sCondoHO6InsPolicyActivationD.Text = dataLoan.sCondoHO6InsPolicyActivationD_rep;
            sCondoHO6InsPaymentDueD.Text = dataLoan.sCondoHO6InsPaymentDueD_rep;
            sCondoHO6InsPaymentMadeD.Text = dataLoan.sCondoHO6InsPaymentMadeD_rep;
            sCondoHO6InsPaidThroughD.Text = dataLoan.sCondoHO6InsPaidThroughD_rep;
            sCondoHO6InsPolicyExpirationD.Text = dataLoan.sCondoHO6InsPolicyExpirationD_rep;
            sCondoHO6InsPolicyLossPayeeTransD.Text = dataLoan.sCondoHO6InsPolicyLossPayeeTransD_rep;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "CondoHO6InsurancePolicy";
            this.PageID = "CondoHO6InsurancePolicy";

            Tools.Bind_sInsPaidBy(sCondoHO6InsPaidByT);
            Tools.Bind_sInsPolicyPaymentTypeT(sCondoHO6InsPolicyPaymentTypeT);
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
