﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{
    public class DisbursementServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(DisbursementServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sDisbursementWarehouseLineIntLckd = GetBool("sDisbursementWarehouseLineIntLckd");
            dataLoan.sDisbursementWarehouseLineInt_rep = GetString("sDisbursementWarehouseLineInt");
            dataLoan.sDisbursementOther_rep = GetString("sDisbursementOther");
            dataLoan.sDisbursementWarehouseLineFees_rep = GetString("sDisbursementWarehouseLineFees");
            dataLoan.sDisbursementWarehousePerDiemInterest_rep = GetString("sDisbursementWarehousePerDiemInterest");
            dataLoan.sDisbursementDaysOfInterestLckd = GetBool("sDisbursementDaysOfInterestLckd");
            dataLoan.sDisbursementDaysOfInterest_rep = GetString("sDisbursementDaysOfInterest");
            dataLoan.sDisbursementNotes = GetString("sDisbursementNotes");
            dataLoan.sDisbursementD_rep = GetString("sDisbursementD");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sDisbursementWarehouseLineIntLckd", dataLoan.sDisbursementWarehouseLineIntLckd);
            SetResult("sDisbursementWarehouseLineInt", dataLoan.sDisbursementWarehouseLineInt_rep);
            SetResult("sDisbursementOther", dataLoan.sDisbursementOther_rep);
            SetResult("sDisbursementWarehouseLineFees", dataLoan.sDisbursementWarehouseLineFees_rep);
            SetResult("sDisbursementWarehousePerDiemInterest", dataLoan.sDisbursementWarehousePerDiemInterest_rep);
            SetResult("sDisbursementDaysOfInterestLckd", dataLoan.sDisbursementDaysOfInterestLckd);
            SetResult("sDisbursementDaysOfInterest", dataLoan.sDisbursementDaysOfInterest_rep);
            SetResult("sDisbursementNotes", dataLoan.sDisbursementNotes);
            SetResult("sDisbursementD", dataLoan.sDisbursementD_rep);
            SetResult("sDisbursementAmtFundFromWarehouseLine", dataLoan.sDisbursementAmtFundFromWarehouseLine_rep);
            SetResult("sDisbursementNetReceived", dataLoan.sDisbursementNetReceived_rep);
            SetResult("sPurchaseAdviceSummaryTotalDueSeller", dataLoan.sPurchaseAdviceSummaryTotalDueSeller_rep);
            SetResult("sFundD", dataLoan.sFundD_rep);
        }
    }
    public partial class DisbursementService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new DisbursementServiceItem());
        }
    }


}
