﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class MortgageInsurancePolicy : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite, Permission.AllowAccountantRead };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead, Permission.AllowAccountantRead };
            }
        }
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(MortgageInsurancePolicy));
            dataLoan.InitLoad();

            Tools.Bind_sMiCompanyNmT(sMiCompanyNmT, dataLoan.sLT);
            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sMiLenderPaidAdj.Text = dataLoan.sMiLenderPaidAdj_rep;
            sMiLenderPaidCoverage.Text = dataLoan.sMiLenderPaidCoverage_rep;
            sMiCertId.Text = dataLoan.sMiCertId;
            sMiPmtDueD.Text = dataLoan.sMiPmtDueD_rep;
            sMiPmtMadeD.Text = dataLoan.sMiPmtMadeD_rep;
            sMiPmtCheckNumber.Text = dataLoan.sMiPmtCheckNumber;

            sMiCertOrderedD.Text = dataLoan.sMiCertOrderedD_rep;
            sMiCertIssuedD.Text = dataLoan.sMiCertIssuedD_rep;
            sMiCertReceivedD.Text = dataLoan.sMiCertReceivedD_rep;
            sMiCommitmentRequestedD.Text = dataLoan.sMiCommitmentRequestedD_rep;
            sMiCommitmentReceivedD.Text = dataLoan.sMiCommitmentReceivedD_rep;
            sMiCommitmentExpirationD.Text = dataLoan.sMiCommitmentExpirationD_rep;

            Tools.SetDropDownListValue(sMiInsuranceT, dataLoan.sMiInsuranceT);
            Tools.SetDropDownListValue(sMiCompanyNmT, dataLoan.sMiCompanyNmT);

            sMiPaidThruD.Text = dataLoan.sMiPaidThruD_rep;
            sMiIsNoReserve.SelectedValue = dataLoan.sMiIsNoReserve.ToString();
            Tools.SetDropDownListValue(sMiFirstProrataT, dataLoan.sMiFirstProrataT);
            Tools.SetDropDownListValue(sMiLastProrataT, dataLoan.sMiLastProrataT);

            if (dataLoan.sLT == E_sLT.UsdaRural
                || dataLoan.sLT == E_sLT.VA
                || dataLoan.sLT == E_sLT.FHA)
            {
                sMiCompanyNmT.Enabled = false;
            }

            Tools.Bind_sMiReasonForAbsenceT(sMiReasonForAbsenceT, includeBlankOption: dataLoan.sMiInsuranceT != E_sMiInsuranceT.None);
            sMiReasonForAbsenceT.Enabled = !dataLoan.sIsMiNotRequiredUnderNyLaw;
            Tools.SetDropDownListValue(sMiReasonForAbsenceT, dataLoan.sMiReasonForAbsenceT);
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "MortgageInsurancePolicy";
            this.PageID = "MortgageInsurancePolicy";

            this.EnableJqueryMigrate = false;

            Tools.Bind_sMiInsuranceT(sMiInsuranceT);

            Tools.Bind_sMiProrata(sMiFirstProrataT);
            Tools.Bind_sMiProrata(sMiLastProrataT);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
