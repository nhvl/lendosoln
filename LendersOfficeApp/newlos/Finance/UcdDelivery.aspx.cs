﻿#region Generated Code
namespace LendersOfficeApp.newlos.Finance
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.UcdDelivery;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// UCD Delivery Page.
    /// </summary>
    public partial class UcdDelivery : BaseLoanPage
    {
        /// <summary>
        /// OnInit function.
        /// </summary>
        /// <param name="e">System event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.PageTitle = "UCD Delivery";
            this.PageID = "UcdDelivery";

            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;

            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterJsScript("widgets/jquery.docmetadatatooltip.js");

            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("SimplePopups.js");
            this.RegisterJsScript("CredentialsPopup.js");
            this.RegisterJsScript("LQBPopup.js");

            this.RegisterCSS("Ucd/UcdDelivery.css");
            this.RegisterJsScript("Ucd/UcdDelivery.js");

            this.RegisterJsScript("angular-1.5.5.min.js");

            base.OnInit(e);
        }

        /// <summary>
        /// Handles Page_Load event.
        /// </summary>
        /// <param name="sender">Object that caused this event.</param>
        /// <param name="e">Event Arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsGlobalVariables("BrokerId", this.BrokerID);
            this.RegisterServiceCredentialData();
        }

        /// <summary>
        /// Registers service credential data.
        /// </summary>
        private void RegisterServiceCredentialData()
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(UcdDelivery));
            dataLoan.InitLoad();
            var principal = PrincipalFactory.CurrentPrincipal;

            var ucdDeliveryCredentials = ServiceCredential.ListAvailableServiceCredentials(principal, dataLoan.sBranchId, ServiceCredentialService.UcdDelivery);
            var credentialDataMap = new Dictionary<int, bool>();

            foreach (UcdDeliveryTargetOption deliveredToEntity in Enum.GetValues(typeof(UcdDeliveryTargetOption)))
            {
                var hasAutoCredentials = ucdDeliveryCredentials.FirstOrDefault((cred) => cred.UcdDeliveryTarget == deliveredToEntity) != null;
                credentialDataMap.Add((int)deliveredToEntity, hasAutoCredentials);
            }

            this.RegisterJsObjectWithJsonNetSerializer("CredentialDataMap", credentialDataMap);
        }
    }
}