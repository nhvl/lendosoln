﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;
using LendersOffice.ObjLib.MortgagePool;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class PoolAssignment : BaseLoanPage
    {       
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(PoolAssignment));
            dataLoan.InitLoad();
            if (dataLoan.sMortgagePoolId.HasValue)
            {
                NoPool.Visible = false;
                MortgagePool mp = new MortgagePool(dataLoan.sMortgagePoolId.Value);
                sPoolNumber.Text = mp.PoolNumberByAgency;
                sPoolAgency.Text = mp.AgencyT_rep;
                sPoolAmortizationType.Text = mp.AmortizationT_rep;
                sPoolSecurityRate.Text = mp.SecurityR_rep;
            }
            else
            {
                PoolDetails.Visible = false;
            }

            if (!BrokerUser.HasPermission(Permission.AllowCapitalMarketsAccess))
            {
                sChangePoolAssignment.Attributes["disabled"] = "disabled";
            }
           
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
            this.PageTitle = "Pool Assignment";
            this.PageID = "PoolAssignment";
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = false;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
