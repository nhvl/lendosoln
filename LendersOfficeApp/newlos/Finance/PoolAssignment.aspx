﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PoolAssignment.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Finance.PoolAssignment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Pool Assignment</title>
</head>

<script type="text/javascript">
    function onclick_ChangePoolAssignment() {
        showModal("/newlos/Finance/PoolAssignmentPopUp.aspx?LoanId=" + ML.sLId, null, null, null, function(args){
            window.location.reload()
        }, {width: 440, height: 160});
    }
</script>

<body bgcolor="gainsboro">
    <form id="PoolAssignment" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="MainRightHeader" nowrap>
                Pool Assignment
            </td>
        </tr>
        <tr>
            <td>
                <table class="InsetBorder FieldLabel" cellspacing="0" cellpadding="0" width="50%"
                    border="0">
                    <asp:PlaceHolder ID="NoPool" runat="server">
                        <tr>
                            <td>
                                Loan not currently assigned to a pool
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="PoolDetails" runat="server">
                        <tr>
                            <td>
                                Pool Number
                            </td>
                            <td>
                                <ml:EncodedLabel ID="sPoolNumber" runat="server"></ml:EncodedLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Pool Agency
                            </td>
                            <td>
                                <ml:EncodedLabel ID="sPoolAgency" runat="server"></ml:EncodedLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Pool Amortization Type
                            </td>
                            <td>
                                <ml:EncodedLiteral ID="sPoolAmortizationType" runat="server"></ml:EncodedLiteral>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Pool Security Rate
                            </td>
                            <td>
                                <ml:EncodedLiteral ID="sPoolSecurityRate" runat="server"></ml:EncodedLiteral>
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                    <tr>
                        <td>
                            <input type="button" class="FieldLabel" id="sChangePoolAssignment" onclick="onclick_ChangePoolAssignment();"
                                value="Change Pool Assignment..." runat="server"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
