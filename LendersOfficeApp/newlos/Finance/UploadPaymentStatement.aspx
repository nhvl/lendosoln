﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadPaymentStatement.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.UploadPaymentStatement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload Payment Statement</title>
    <script>
        jQuery(function ($) {
            function readFileCallback(file) {
                var args = {
                    loanid: ML.sLId,
                    aAppId: ML.aAppId,
                };

                triggerOverlayImmediately();
                DocumentUploadHelper.Upload('UploadPaymentStatement', file, args, function (result) {
                    removeOverlay();

                    if (result.error) {
                        LQBPopup.ShowString(result.UserMessage);
                        return;
                    }

                    window.parent.LQBPopup.Return({
                        documentId: result.value['DocumentId']
                    });
                });
            }

            $('#Cancel').click(function () {
                window.parent.LQBPopup.Return({});
            });

            $('#Upload').click(function () {
                if (getFilesCount("DragDropZone") != 1) {
                    LQBPopup.ShowString('Please select a file to upload.');
                    return;
                }

                applyCallbackFileObjects("DragDropZone", readFileCallback);
            });

            var dragDropSettings = {
                blockUpload: !ML.CanUploadEdocs,
                blockUploadMsg: ML.UploadEdocsFailureMessage
            };

            registerDragDropUpload(document.getElementById("DragDropZone"), dragDropSettings);
        });
    </script>
    <style>
        body { background-color: gainsboro; }
        .UploadContainer { text-align: center; }
        .ButtonContainer { padding-top: 10px; text-align: center; }
        form {padding-top: 10px;}
    </style>
</head>
<body>
    <h4 class="page-header">Upload Payment Statement PDF</h4>
    <form id="form1" runat="server">
    <div>
        <div class="UploadContainer">
            <div id="DragDropZoneContainer" class="InsetBorder full-width">
                <div id="DragDropZone" class="drag-drop-container" limit="1" extensions=".pdf"></div>
            </div>
        </div>

        <div class="ButtonContainer">
            <input type="button" id="Upload" value="Upload" />
            <input type="button" id="Cancel" value="Cancel" />
        </div>
    </div>
    </form>
</body>
</html>
