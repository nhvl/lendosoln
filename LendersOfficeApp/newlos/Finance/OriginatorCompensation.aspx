﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="OriginatorCompensation.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Finance.OriginatorCompensation" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Originator Compensation</title>
    <script type="text/javascript">
    function Page_ClientValidate() {
        // call the service again and check if the values are valid, if not, return false.
        var lenderPaidCB = document.getElementById('PaymentSourceSelectLenderPaid').checked;
        if (!lenderPaidCB) {
            return true;
        }
        var minCheckbox = document.getElementById('sOriginatorCompensationHasMinAmount').checked;
        var maxCheckbox = document.getElementById('sOriginatorCompensationHasMaxAmount').checked;
        var minAmount = document.getElementById('sOriginatorCompensationMinAmount').value;
        var maxAmount = document.getElementById('sOriginatorCompensationMaxAmount').value;
        var errMsg = '';
        var args = {
            sOriginatorCompensationMinAmount: minAmount,
            sOriginatorCompensationMaxAmount: maxAmount,
            sOriginatorCompensationHasMinAmount : minCheckbox,
            sOriginatorCompensationHasMaxAmount : maxCheckbox
        }
        var result = gService.loanedit.call('ValidateMinMaxAmount', args);
        if (!result.error) {
            if (result.value['err'] == "True") {
                alert(result.value['errMsg']);
                return false;
            }
        }
        return true;
    }

    function showPaymentSourceUnspLine() {
        document.getElementById('PaymentSourceUnspLine').style.display = '';
        document.getElementById('PaymentSourceBorrLine').style.display = 'none';
        document.getElementById('PaymentSourceLenderLoanAmount').style.display = 'none';
    }
    function showPaymentSourceBorrLine() {
        document.getElementById('PaymentSourceUnspLine').style.display = 'none';
        document.getElementById('PaymentSourceBorrLine').style.display = '';
        document.getElementById('PaymentSourceLenderLoanAmount').style.display = 'none';
    }
    function showPaymentSourceLenderLoanAmount() {
        document.getElementById('PaymentSourceUnspLine').style.display = 'none';
        document.getElementById('PaymentSourceBorrLine').style.display = 'none';
        document.getElementById('PaymentSourceLenderLoanAmount').style.display = '';
    }

    function setUnspPaidBaseT() {
        var selected = $('#sOriginatorCompensationUnspPaidBaseT option:selected').text();
        $('#sOriginatorCompensationUnspPaidPc').prop('readonly', selected == 'All YSP');
    }

    function changeMinMaxAmountCheckbox(whatChanged, minCheckbox, maxCheckbox) {
        // Now correct the results if necessary
        minCheckbox = document.getElementById('sOriginatorCompensationHasMinAmount').checked;
        maxCheckbox = document.getElementById('sOriginatorCompensationHasMaxAmount').checked;

        if (!minCheckbox) {
            $('#sOriginatorCompensationMinAmount').prop("readonly", true);
            $('#sOriginatorCompensationMinAmount').val("");
        } else {
            $('#sOriginatorCompensationMinAmount').prop("readonly", false);
        }
        if (!maxCheckbox) {
            $('#sOriginatorCompensationMaxAmount').prop("readonly", true);
            $('#sOriginatorCompensationMaxAmount').val("");
        } else {
            $('#sOriginatorCompensationMaxAmount').prop("readonly", false);
        }
        changeMinMaxAmount(whatChanged);
    }

    // whatChanged is a string saying whether min or max changed (up for refactoring)
    function changeMinMaxAmount(whatChanged) {
        var minCheckbox = document.getElementById('sOriginatorCompensationHasMinAmount').checked;
        var maxCheckbox = document.getElementById('sOriginatorCompensationHasMaxAmount').checked;
        var minAmount = document.getElementById('sOriginatorCompensationMinAmount').value;
        var maxAmount = document.getElementById('sOriginatorCompensationMaxAmount').value;
        var errMsg = '';
        var args = {
            changedVal : whatChanged,
            sOriginatorCompensationMinAmount: minAmount,
            sOriginatorCompensationMaxAmount: maxAmount,
            sOriginatorCompensationHasMinAmount : minCheckbox,
            sOriginatorCompensationHasMaxAmount : maxCheckbox
        }
        var result = gService.loanedit.call('ValidateMinMaxAmount', args);
        if (!result.error) {
            displayMinMaxErrorMessage(result.value['errMsg']);
        } else if (null != result.UserMessage) {
            alert(result.UserMessage);
        }

        refreshCalculation();
    }

    function displayMinMaxErrorMessage(msg) {
        document.getElementById('MinMaxErrorMessage').innerText = msg;
    }

    var setPlanFields = function($minCB, $maxCB, $min, $max, enable) { // this function is only relevant here, so we define it in here
        $('.OriginatorCompensation_AutoDisable').prop('readonly', !enable);
        disableDDL(document.getElementById('sOriginatorCompensationBaseT'), !enable);
        $('#sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo').prop('disabled', !enable);
        $('#setPlanManual').prop('disabled', enable);
        $minCB.prop('disabled', !enable);
        $maxCB.prop('disabled', !enable);
        $min.prop('readonly', !enable || !$maxCB.prop('checked'));
        $max.prop('readonly', !enable || !$minCB.prop('checked'));
        if (!$minCB.prop('checked')) {
            $min.val('');
        }
        if (!$maxCB.prop('checked')) {
            $max.val('');
        }
    }

    // planNum: 0 for manual, 1 for fromEmployee
    // firstLoad: true if the document is loading for the first time
    function setPlan(planNum, firstLoad) {
        $minCB = $('#sOriginatorCompensationHasMinAmount');
        $maxCB = $('#sOriginatorCompensationHasMaxAmount');
        $min = $('#sOriginatorCompensationMinAmount');
        $max = $('#sOriginatorCompensationMaxAmount');
        var radioToCheck;
        if (planNum < 0) { // no plan is set
            setPlanFields($minCB, $maxCB, $min, $max, false);
            radioToCheck = 'PlanFromEmployee'; // Eh, why not. Not setting the radio causes problems.
            $('#'+radioToCheck).prop('checked', true);
            return;
        }
        if (firstLoad == false) {
            var args = {
                plan: planNum,
                LoanId: ML.sLId,
                sOriginatorCompensationPlanAppliedD:                    document.getElementById('sOriginatorCompensationPlanAppliedD').value,
                sOriginatorCompensationEffectiveD:                      document.getElementById('sOriginatorCompensationEffectiveD').value,
                sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo:   document.getElementById('sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo').checked,
                sOriginatorCompensationPercent:                         document.getElementById('sOriginatorCompensationPercent').value,
                sOriginatorCompensationBaseT:                           document.getElementById('sOriginatorCompensationBaseT').value,
                sOriginatorCompensationMinAmount:                       document.getElementById('sOriginatorCompensationMinAmount').value,
                sOriginatorCompensationMaxAmount:                       document.getElementById('sOriginatorCompensationMaxAmount').value,
                sOriginatorCompensationFixedAmount:                     document.getElementById('sOriginatorCompensationFixedAmount').value,
                sOriginatorCompensationNotes:                           document.getElementById('sOriginatorCompensationNotes').value
            }
            var result = gService.loanedit.call('SetPlan', args);
            if (!result.error) {
                populateForm(result.value);
                document.getElementById('PlanSourceExplanation').innerText = result.value['PlanSourceExplanation'];

                // EMPLOYEE LINK
                if (result.value['PlanSourceEmployeeId']) {
                    // If the PlanSourceEmployeeId is empty, then the plan is either manual or the user doesn't
                    //   have permission to access that employee's page
                    document.getElementById('sOriginatorCompensationPlanSourceEmployeeName').innerText = "";
                    var editorLink;
                    var lqbSettings;
                    if (result.value['PlanSourceEmployeeUserType'] == 'B') {
                        // Put in the link to the broker employee
                        editorLink = '/los/admin/EditEmployee.aspx?cmd=edit&employeeId=';
                        lqbSettings = '{width:1000, hideCloseButton: true}';
                    } else if (result.value['PlanSourceEmployeeUserType'] == 'P') {
                        // Put in the link to the PML employee
                        editorLink = '/los/BrokerAdmin/EditPmlUser.aspx?cmd=edit&employeeId=';
                        lqbSettings = '{width:890, hideCloseButton: true}';
                    }
                    var link = '<a href="#" '
                       + 'onclick="showModal(\'' + editorLink + result.value['PlanSourceEmployeeId'] + '\', null, null, null, null,'+lqbSettings+');" >'
                       + result.value['sOriginatorCompensationPlanSourceEmployeeName']
                       + '</a>';
                    $(link).appendTo('#sOriginatorCompensationPlanSourceEmployeeName');
                } else {
                    document.getElementById('sOriginatorCompensationPlanSourceEmployeeName').innerText = result.value['sOriginatorCompensationPlanSourceEmployeeName'];
                }

                document.getElementById('sOriginatorCompensationAuditD').innerText = result.value['sOriginatorCompensationAuditD'];
            } else {
                var errMsg = 'Unable to set compensation. Please try again.';

                if (null != result.UserMessage)
                    errMsg = result.UserMessage;
                alert(errMsg);
            }
        }

        if (planNum) {
            setPlanFields($minCB, $maxCB, $min, $max, false);
            displayMinMaxErrorMessage(''); // clear the error message if it exists
            radioToCheck = 'PlanFromEmployee';
        } else {
            if (firstLoad) { // spec 4.2.1: fields should be disabled on firstLoad
                setPlanFields($minCB, $maxCB, $min, $max, false);
            } else {
                setPlanFields($minCB, $maxCB, $min, $max, true);
            }
            onclick_OnlyPaidForFirstLienOfCombo();
            radioToCheck = 'PlanManual';
        }
        $('#'+radioToCheck).prop('checked', true);

    }

    function clearPlanFields() {
        $('.OriginatorCompensation_AutoDisable').val('');
        $('#sOriginatorCompensationTotalAmount').val('');
        $('#sOriginatorCompensationAdjBaseAmount').val('');
        $('#sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo').prop('checked', false);
    }

    function onclick_OnlyPaidForFirstLienOfCombo()
    {
        var combo2ndWithCompOn1st = document.getElementById('IsComboSecond').value == "1";
        var comboCheckbox = $('#sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo');
        var isChecked = comboCheckbox.prop('checked');

        if (combo2ndWithCompOn1st)
        {
            if (isChecked)
            {
                var percent = $('#sOriginatorCompensationPercent');
                var total = $('#sOriginatorCompensationTotalAmount');
                var base = $('#sOriginatorCompensationAdjBaseAmount');
                var fixed = $('#sOriginatorCompensationFixedAmount');

                percent.val('0.000%');
                total.val('$0.00');
                base.val('$0.00');
                fixed.val('$0.00');
                $min.val('');
                $max.val('');

                $minCB.prop('checked', false);
                $maxCB.prop('checked', false);

                percent.prop('readonly', true);
                total.prop('readonly', true);
                base.prop('readonly', true);
                fixed.prop('readonly', true);
                $min.prop('readonly', true);
                $max.prop('readonly', true);
                $minCB.prop('disabled', true);
                $maxCB.prop('disabled', true);
            }
            else
            {
                setPlanFields($minCB, $maxCB, $min, $max, true);
            }
        }
    }

    function onclick_clearPlan() {
        var args = {
            LoanId: ML.sLId
        };
        var result = gService.loanedit.call('ClearPlan', args);
        if (!result.error) {
            populateForm(result.value);
        } else {
            if (null != result.UserMessage)
                var errMsg = result.UserMessage;
            alert(errMsg);
        }
        setPlanFields($minCB, $maxCB, $min, $max, false);
        clearPlanFields();
        $('#PlanFromEmployee').prop('checked', true);
        document.getElementById('PlanSourceExplanation').innerText = 'No compensation plan set';
        document.getElementById('sOriginatorCompensationPlanSourceEmployeeName').innerText = '';
        document.getElementById('sOriginatorCompensationAuditD').innerText = '';
    }
    </script>

    <style type="text/css">
        .OriginatorCompensation_TotalRowLabel
        {
            text-align: right;
        }
        .OriginatorCompensation_LenderPaidCompensation
        {
            margin-top: 0.5em;
            margin-bottom: 1em;
            width: 32em;
            padding: 5px;
        }
        .OriginatorCompensation_InnerInfoTable
        {
            border: 1px solid black;
            width: 100%;
            margin-top: 0.5em;
            margin-bottom: 1em;
        }
        .OriginatorCompensation_InnerInfoTable td
        {
            padding: 0.2em 1em 0.2em 1em;
        }
        .OriginatorCompensation_SpacerMinMax
        {
            width: 2em;
        }
        .OriginatorCompensation_HiddenLoanAmount
        {
            padding: 0em 1em 0em 1em;
        }
        .OriginatorCompensation_AutoDisable
        {
        }
        .PaymentSourceArea
        {
        	margin: 2px 0px 2px 0px;
        }
        .LenderOptionArea
        {
        	margin: 2px 0px 2px 0px;
        }
        #setPlanFromEmployee
        {
            width: 20em;
        }
        #setPlanManual
        {
            width: 17em;
        }
        #clearPlan
        {

        }
        #MinMaxErrorMessage
        {
            color: red;
        }
        .Disabled
        {
            background-color: #f7f7f7;
        }
        .Hidden
        {
            display: none;
        }
        #sOriginatorCompensationPlanSourceEmployeeName A:link
        {
        	color: White;
        }
        #sOriginatorCompensationPlanSourceEmployeeName A:hover
        {
        	color: Red;
        }
        #sOriginatorCompensationPlanSourceEmployeeName A:visited
        {
        	color: White;
        }
    </style>
</head>
<body bgcolor="gainsboro">

    <script type="text/javascript">
    <!--
    $(function() {
        // These actions need to be taken once the document is ready
        var paymentSource = <%= AspxTools.JsString(_PaymentSourceDiv) %>;
        if (paymentSource == "PaymentSourceUnspecifiedLoanAmount") {
            showPaymentSourceUnspLine();
        } else if (paymentSource == "PaymentSourceBorrowerLoanAmount") {
            showPaymentSourceBorrLine();
        } else if (paymentSource == "PaymentSourceLenderLoanAmount") {
            showPaymentSourceLenderLoanAmount();
        }
        setPlan(<%= AspxTools.JsNumeric(_PlanT) %>, true);
        if (<%= AspxTools.JsNumeric(_LenderDivHidden) %> === 1) {
            $('.OriginatorCompensation_AutoDisable').val('');
            $('#sOriginatorCompensationBaseT').val('          ');
            $('#sOriginatorCompensationTotalAmount').val('');
            $('#sOriginatorCompensationTotalAmount').val('');
            $('#sOriginatorCompensationAdjBaseAmount').val('');
        }
    });
    //-->
    </script>

    <form id="OriginatorCompensation" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td class="MainRightHeader" nowrap="true">
                Originator Compensation
            </td>
        </tr>
        <tr>
            <td style="padding: 4px">
                <!-- Will need to use RegisterHiddenField to put it in a hidden input -->
                <input type="radio" class="Hidden" name="sOriginatorCompensationPlanT" id="PlanManual"
                    runat="server" value="0" />
                <input type="radio" class="Hidden" name="sOriginatorCompensationPlanT" id="PlanFromEmployee"
                    runat="server" value="1" />
                <!-- PaymentSourceUnspecifiedDiv is hidden when CPageData.sIsOriginationCompensationSourceRequired == true -->
                <div id="PaymentSourceUnspecifiedDiv" class="PaymentSourceArea" runat="server">
                    <input type="radio" name="sOriginatorCompensationPaymentSourceT" id="PaymentSourceSelectNotSpecified"
                        value="0" runat="server" onclick="showPaymentSourceUnspLine(); refreshCalculation();" />
                    <label for="PaymentSourceSelectNotSpecified" class="FieldLabel" >Payment source not specified</label>
                    <br />
                    <div id="PaymentSourceUnspLine" class="FieldLabel" style="display: none" runat="server">
                        <ml:PercentTextBox ID="sOriginatorCompensationUnspPaidPc" onchange="updateDirtyBit(event); refreshCalculation();"
                            runat="server" preset="percent"></ml:PercentTextBox>
                        of the
                        <asp:DropDownList ID="sOriginatorCompensationUnspPaidBaseT" onchange="setUnspPaidBaseT(); updateDirtyBit(event); refreshCalculation();"
                            runat="server">
                        </asp:DropDownList>
                        +
                        <ml:MoneyTextBox ID="sOriginatorCompensationUnspPaidMb" onchange="updateDirtyBit(event); refreshCalculation();"
                            runat="server"></ml:MoneyTextBox>
                        =
                        <ml:MoneyTextBox ReadOnly="true" ID="sOriginatorCompensationUnspTotalAmount" onchange="refreshCalculation();"
                            runat="server"></ml:MoneyTextBox>
                    </div>
                </div>
                <div id="PaymentSourceBorrowerArea" class="PaymentSourceArea">
                    <input type="radio" name="sOriginatorCompensationPaymentSourceT" id="PaymentSourceSelectBorrowerPaid"
                        value="1" runat="server" onclick="showPaymentSourceBorrLine(); refreshCalculation();" />
                    <label for="PaymentSourceSelectBorrowerPaid" class="FieldLabel" >Borrower paid</label>
                    <br />
                    <div id="PaymentSourceBorrLine" class="FieldLabel" style="display: none" runat="server">
                        <ml:PercentTextBox ID="sOriginatorCompensationBorrPaidPc" onchange="updateDirtyBit(event); refreshCalculation();"
                            runat="server" preset="percent"></ml:PercentTextBox>
                        of the
                        <asp:DropDownList ID="sOriginatorCompensationBorrPaidBaseT" onchange="updateDirtyBit(event); refreshCalculation();"
                            runat="server">
                        </asp:DropDownList>
                        +
                        <ml:MoneyTextBox ID="sOriginatorCompensationBorrPaidMb" onchange="updateDirtyBit(event); refreshCalculation();"
                            runat="server"></ml:MoneyTextBox>
                        =
                        <ml:MoneyTextBox ReadOnly="true" ID="sOriginatorCompensationBorrTotalAmount" onchange="refreshCalculation();"
                            runat="server"></ml:MoneyTextBox>
                    </div>
                </div>
                <div id="PaymentSourceLenderArea" class="PaymentSourceArea">
                    <input type="radio" name="sOriginatorCompensationPaymentSourceT" id="PaymentSourceSelectLenderPaid"
                        value="2" runat="server" onclick="showPaymentSourceLenderLoanAmount(); refreshCalculation();" />
                    <label for="PaymentSourceSelectLenderPaid" class="FieldLabel" >Lender paid</label>
                    <br />
                    <!-- Lender paid div -->
                    <div class="OriginatorCompensation_HiddenLoanAmount" id="PaymentSourceLenderLoanAmount"
                        style="display: none;" runat="server">
                        <div class="LenderOptionArea">
                            <input type="radio" name="sOriginatorCompensationLenderFeeOptionT" id="LenderFeesIncluded"
                                runat="server" value="0" />
                            <label for="LenderFeesIncluded" class="FieldLabel" >Originator compensation is included in lender fees</label>
                        </div>
                        <div class="LenderOptionArea">
                            <input type="radio" name="sOriginatorCompensationLenderFeeOptionT" id="LenderFeesAddition"
                                runat="server" value="1" />
                            <label for="LenderFeesAddition" class="FieldLabel" >Originator compensation is in addition to lender fees</label>
                        </div>
                        <div class="LenderOptionArea">
                            <asp:CheckBox runat="server" CssClass="FieldLabel" ID="sIsIncludeOriginatorCompensationToCommission" Text=" Include compensation on the commissions page" />
                        </div>
                        <table class="OriginatorCompensation_LenderPaidCompensation" cellspacing="0">
                            <tr>
                                <td colspan="3">
                                    <!-- These should change sOriginatorCompensationPlanT -->
                                    <!-- NoHighlight is case-sensitive. Ctrl-k Ctrl-d will break NoHighlight -->
                                    <input type="button" NoHighlight="true" class="FieldLabel" id="setPlanFromEmployee" onclick="setPlan(1, false);"
                                        value="Set compensation using current plan" />
                                    <input type="button" NoHighlight="true" class="FieldLabel" id="setPlanManual" onclick="setPlan(0, false);"
                                        value="Set compensation manually" />
                                    <input type="button" NoHighlight="true" class="FieldLabel" id="clearPlan" onclick="onclick_clearPlan();"
                                        value="Clear plan" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="OriginatorCompensation_InnerInfoTable" cellspacing="0">
                                        <tr>
                                            <td class="FormTableHeader" colspan="4">
                                                <ml:EncodedLabel ID="PlanSourceExplanation" runat="server" Text="No compensation plan set"></ml:EncodedLabel>
                                                <ml:PassThroughLabel ID="sOriginatorCompensationPlanSourceEmployeeName" runat="server"></ml:PassThroughLabel>
                                                &nbsp<ml:EncodedLabel ID="sOriginatorCompensationAuditD" runat="server"></ml:EncodedLabel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" colspan="2">
                                                Originator compensation set
                                            </td>
                                            <td>
                                                <ml:DateTextBox CssClass="OriginatorCompensation_AutoDisable" ID="sOriginatorCompensationPlanAppliedD"
                                                    runat="server"></ml:DateTextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" colspan="2">
                                                Effective date of compensation plan
                                            </td>
                                            <td>
                                                <ml:DateTextBox CssClass="OriginatorCompensation_AutoDisable" ID="sOriginatorCompensationEffectiveD"
                                                    runat="server"></ml:DateTextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
				                            <td class="FieldLabel" colspan="3">
                                                <asp:CheckBox id="sOriginatorCompensationIsOnlyPaidForFirstLienOfCombo"
                                                    runat="server" Text="Compensation only paid for the 1st lien of a combo" onchange="onclick_OnlyPaidForFirstLienOfCombo();" />
                                                <input type="hidden" id="IsComboSecond" value="0" runat="server" />
				                            </td>
                                            <td>
                                            </td>
				                        </tr>
                                        <tr>
                                            <td class="FieldLabel" colspan="3">
                                                <ml:PercentTextBox CssClass="OriginatorCompensation_AutoDisable" ID="sOriginatorCompensationPercent"
                                                    onchange="refreshCalculation();" runat="server" preset="percent"></ml:PercentTextBox>
                                                of the
                                                <asp:DropDownList CssClass="OriginatorCompensation_AutoDisable" ID="sOriginatorCompensationBaseT"
                                                    onchange="refreshCalculation();" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <ml:MoneyTextBox ReadOnly="true" ID="sOriginatorCompensationAdjBaseAmount" onchange="refreshCalculation();"
                                                    runat="server" preset="money"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" style="width:140px">
                                                <span class="OriginatorCompensation_SpacerMinMax"></span>
                                                <input class="Disabled" type="checkbox" runat="server" id="sOriginatorCompensationHasMinAmount"
                                                    disabled="disabled" onclick="changeMinMaxAmountCheckbox('min');" />
                                                Minimum
                                            </td>
                                            <td>
                                                <ml:MoneyTextBox CssClass="OriginatorCompensation_AutoDisable" ID="sOriginatorCompensationMinAmount"
                                                    onchange="changeMinMaxAmount('min');" runat="server" preset="money"></ml:MoneyTextBox>
                                            </td>
                                            <td colspan="2" rowspan="2">
                                                <span id="MinMaxErrorMessage" runat="server"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" >
                                                <span class="OriginatorCompensation_SpacerMinMax"></span>
                                                <input class="Disabled" type="checkbox" runat="server" id="sOriginatorCompensationHasMaxAmount"
                                                    disabled="disabled" onclick="changeMinMaxAmountCheckbox('max');" />
                                                Maximum
                                            </td>
                                            <td>
                                                <ml:MoneyTextBox CssClass="OriginatorCompensation_AutoDisable" ID="sOriginatorCompensationMaxAmount"
                                                    onchange="changeMinMaxAmount('max');" runat="server" preset="money"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" colspan="3">
                                                Fixed amount
                                            </td>
                                            <td>
                                                <ml:MoneyTextBox CssClass="OriginatorCompensation_AutoDisable" ID="sOriginatorCompensationFixedAmount"
                                                    onchange="refreshCalculation();" runat="server" preset="money"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr style="height: 1px; color: black;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" colspan="3" class="OriginatorCompensation_TotalRowLabel">
                                                Total lender-paid compensation
                                            </td>
                                            <td>
                                                <ml:MoneyTextBox ReadOnly="true" ID="sOriginatorCompensationTotalAmount" onchange="refreshCalculation();"
                                                    runat="server" preset="money"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" colspan="4">
                                    Compensation notes
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:TextBox CssClass="OriginatorCompensation_AutoDisable" ID="sOriginatorCompensationNotes"
                                        onkeyup="TextAreaMaxLength(this, 200);" runat="server" Width="100%" TextMode="MultiLine"
                                        Wrap="true" Rows="3"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <uc1:cModalDlg ID="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cModalDlg>
    </form>
</body>
</html>
