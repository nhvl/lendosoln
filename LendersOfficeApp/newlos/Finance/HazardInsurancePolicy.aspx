﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HazardInsurancePolicy.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.HazardInsurancePolicy" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<script language="javascript">
  function _init() {
    if(<%= AspxTools.JsGetElementById(sHazInsPaymentDueAmtLckd) %>.checked) {
      <%= AspxTools.JsGetElementById(sHazInsPaymentDueAmt) %>.readOnly = false;
	} else {
	  <%= AspxTools.JsGetElementById(sHazInsPaymentDueAmt) %>.readOnly = true;
	}
  }
  
    function clearCompanyId()
  {
    document.getElementById("sHazInsCompanyId").value = "";
  }
</script>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Hazard Insurance Policy</title>  
</head>

<body bgcolor="gainsboro">
    <form id="HazardPolicy" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td class="MainRightHeader" noWrap>
                    Hazard Insurance Policy
                </td>
            </tr>            
            <tr>
                <td>
                    <table class="FieldLabel">
                        <tr>
                            <td></td>
                            <td>
                                <uc:CFM id="CFM" runat="server" CompanyNameField="sHazInsCompanyNm" CompanyIdField="sHazInsCompanyId" DisableAddTo="true"></uc:CFM>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Insurance Company
                            </td>
                            <td>
                                <asp:TextBox onchange="clearCompanyId()" ID="sHazInsCompanyNm" width="150" runat="server"></asp:TextBox>                                
                   
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Policy Number
                            </td>
                            <td>
                                <asp:TextBox ID="sHazInsPolicyNum" Width="150" runat="server" ></asp:TextBox>
                            </td>                            
                        </tr>
                        <tr>
                            <td>
                                Policy Activation Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sHazInsPolicyActivationD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Policy Expiration Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sHazInsPolicyExpirationD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Paid By
                            </td>
                            <td>
                                <asp:DropDownList ID="sHazInsPaidByT" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Coverage Amount
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sHazInsCoverageAmt" runat="server" CssClass="mask" preset="money" width="110"></ml:moneytextbox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Deductible Amount
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sHazInsDeductibleAmt" runat="server" CssClass="mask" preset="money" Width="110"></ml:MoneyTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Due
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="sHazInsPaymentDueAmt" runat="server" CssClass="mask" preset="money" ReadOnly="true" Width="110"></ml:MoneyTextBox>
                                <asp:CheckBox ID="sHazInsPaymentDueAmtLckd" runat="server" onclick="refreshCalculation();"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Type
                            </td>
                            <td>
                                <asp:DropDownList ID="sHazInsPolicyPaymentTypeT" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Due Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sHazInsPaymentDueD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment Made Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sHazInsPaymentMadeD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Paid Through Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sHazInsPaidThroughD" runat="server" width="75"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payee Code
                            </td>
                            <td>
                                <asp:TextBox ID="sHazInsPolicyPayeeCode" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Loss Payee Transfer Date
                            </td>
                            <td>
                                <ml:DateTextBox ID="sHazInsPolicyLossPayeeTransD" runat="server" Width="75" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Company Id 
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="sHazInsCompanyId" value=""></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form> 
</body>
</html>