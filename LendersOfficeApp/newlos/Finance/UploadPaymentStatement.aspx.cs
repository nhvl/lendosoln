﻿#region Generated Code -- A lie to dupe the merciless StyleCop.
namespace LendersOfficeApp.newlos.Finance
#endregion
{
    using System;
    using LendersOffice.ConfigSystem.Operations;

    /// <summary>
    /// Provides UI for uploading a payment statment directly to a payment.
    /// </summary>
    public partial class UploadPaymentStatement : BaseLoanPage
    {
        /// <summary>
        /// Gets the extra workflow operations to check.
        /// </summary>
        /// <returns>The extra workflow operations to check.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[]
            {
                WorkflowOperations.UploadEDocs
            };
        }

        /// <summary>
        /// Gets the reasons for privileges that aren't allowed.
        /// </summary>
        /// <returns>The reasons for privileges that aren't allowed.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return this.GetExtraOpsToCheck();
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsGlobalVariables("CanUploadEdocs", this.UserHasWorkflowPrivilege(WorkflowOperations.UploadEDocs));
            this.RegisterJsGlobalVariables("UploadEdocsFailureMessage", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.UploadEDocs));
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.DisplayCopyRight = false;
            this.EnableJqueryMigrate = false;
            this.UseNewFramework = true;

            this.RegisterJsScript("DragDropUpload.js");
            this.RegisterJsScript("DocumentUploadHelper.js");

            this.RegisterCSS("DragDropUpload.css");

            base.OnInit(e);
        }
    }
}