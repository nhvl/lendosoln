﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using LendersOffice.CustomPmlFields;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Web.UI.HtmlControls;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class LoanOfficerPricingPolicy : BaseLoanPage
    {

        // 1/22/14 gf - opm 130492 want same permissions as originator comp page.
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite, Permission.AllowCloserRead };
            }
        }

        protected override void LoadData()
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LoanOfficerPricingPolicy));
            dataLoan.InitLoad();

            PopulateLoanOfficerPricingPolicyTable(BrokerID, dataLoan);

            if (dataLoan.sIsCustomPricingPolicySet)
            {
                sCustomPricingPolicyAuditD.Text = dataLoan.sCustomPricingPolicyAuditD_rep;

                if (dataLoan.sCustomPricingPolicySource == E_sCustomPricingPolicySourceT.Manual)
                {
                    // Handle Manual
                    PolicySourceExplanation.Text = "Set manually by ";
                    sCustomPricingPolicySourceEmployeeNm.Text = AspxTools.HtmlString(dataLoan.sCustomPricingPolicyAppliedBy);
                }
                else if (dataLoan.sCustomPricingPolicySource == E_sCustomPricingPolicySourceT.LoanOfficer)
                {
                    PolicySourceExplanation.Text = string.IsNullOrEmpty(dataLoan.sCustomPricingPolicySourceEmployeeNm) ?
                        "Set from the employee record, " : "Set from the employee record of ";

                    Guid employeeId = dataLoan.sCustomPricingPolicySourceEmployeeId;
                    var employee = new EmployeeDB(employeeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                    employee.Retrieve();
                    string editorLink;
                    if (employee.UserType == 'P' && BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanAdministrateExternalUsers))
                    {
                        editorLink = "/los/BrokerAdmin/EditPmlUser.aspx?cmd=edit&employeeId=";
                        sCustomPricingPolicySourceEmployeeNm.Text =
                            string.Format("<a href=\"#\" onclick=\"{0}\">{1}</a>",
                                "showModal('" + editorLink + dataLoan.sCustomPricingPolicySourceEmployeeId.ToString() + "', null, null, null, null,{width:890, hideCloseButton: true});",
                                AspxTools.JsStringUnquoted(dataLoan.sCustomPricingPolicySourceEmployeeNm));
                    }
                    else if (employee.UserType != 'P' && BrokerUser.HasRole(E_RoleT.Administrator))
                    {
                        editorLink = "/los/admin/EditEmployee.aspx?cmd=edit&employeeId=";
                        sCustomPricingPolicySourceEmployeeNm.Text =
                            string.Format("<a href=\"#\" onclick=\"{0}\">{1}</a>",
                                "showModal('" + editorLink + dataLoan.sCustomPricingPolicySourceEmployeeId.ToString() + "', null, null, null, null,{width:1000, hideCloseButton: true});",
                                AspxTools.JsStringUnquoted(dataLoan.sCustomPricingPolicySourceEmployeeNm));
                    }
                    else
                    {
                        sCustomPricingPolicySourceEmployeeNm.Text = AspxTools.HtmlString(dataLoan.sCustomPricingPolicySourceEmployeeNm);
                    }
                }
                else if (dataLoan.sCustomPricingPolicySource == E_sCustomPricingPolicySourceT.OriginatingCompany)
                {
                    sCustomPricingPolicySourceEmployeeNm.Text = AspxTools.HtmlString(dataLoan.sCustomPricingPolicySourceOCNm);
                    PolicySourceExplanation.Text =
                        "Could not find an assigned loan officer. Policy set from " +
                        "the originating company record of ";
                }
                else if (dataLoan.sCustomPricingPolicySource == E_sCustomPricingPolicySourceT.Branch)
                {
                    this.sCustomPricingPolicySourceEmployeeNm.Text = AspxTools.HtmlString(dataLoan.sCustomPricingPolicySourceBranchNm);
                    this.PolicySourceExplanation.Text =
                        "Could not find an assigned loan officer. Policy set from " +
                        "the originating company record of ";
                }
            }
        }

        public void PopulateLoanOfficerPricingPolicyTable(Guid brokerId, CPageData dataLoan)
        {
            var brokerDB = this.Broker;
            var customFields = brokerDB.CustomPricingPolicyFieldList;

            foreach (var field in brokerDB.CustomPricingPolicyFieldList.ValidFieldsSortedByRank)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell labelCell = new HtmlTableCell();
                labelCell.Attributes["class"] = "FieldLabel CustomLOPPFieldLabel";
                HtmlTableCell fieldCell = new HtmlTableCell();

                Literal label = new Literal();
                label.Text = field.Description;
                labelCell.Controls.Add(label);

                WebControl control = CustomPmlFieldUtilities.GetControlForFieldType(field.Type);
                control.ID = String.Format("sCustomPricingPolicyField{0}", field.KeywordNum);
                control.Attributes["class"] = control.ID;
                CustomPmlFieldUtilities.SetControlAttributesForFieldType(control, field.Type);

                if (control is DropDownList)
                {
                    CustomPmlFieldUtilities.BindCustomFieldDDL(control as DropDownList, field);
                }

                string fieldValue = dataLoan.GetCustomPricingPolicyField(field.KeywordNum);
                CustomPmlFieldUtilities.SetControlValue(control, fieldValue);

                fieldCell.Controls.Add(control);

                label = new Literal();
                label.Text = field.VisibilityType_rep;
                row.Controls.Add(labelCell);
                row.Controls.Add(fieldCell);

                CustomLOPricingPolicyTable.Rows.Add(row);
            }
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Loan Officer Pricing Policy";
            this.PageID = "LoanOfficerPricingPolicy";
            EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");
            RegisterJsScript("mask.js");
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
