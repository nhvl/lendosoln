﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{

    public class HazardInsurancePolicyServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(HazardInsurancePolicyServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sHazInsCompanyNm = GetString("sHazInsCompanyNm");
            dataLoan.sHazInsPolicyNum = GetString("sHazInsPolicyNum");
            dataLoan.sHazInsPolicyActivationD_rep = GetString("sHazInsPolicyActivationD");
            dataLoan.sHazInsPaidByT = (E_sInsPaidByT)GetInt("sHazInsPaidByT");
            dataLoan.sHazInsCoverageAmt_rep = GetString("sHazInsCoverageAmt");
            dataLoan.sHazInsDeductibleAmt_rep = GetString("sHazInsDeductibleAmt");
            dataLoan.sHazInsPaymentDueAmt_rep = GetString("sHazInsPaymentDueAmt");
            dataLoan.sHazInsPaymentDueAmtLckd = GetBool("sHazInsPaymentDueAmtLckd");            
            dataLoan.sHazInsPaymentDueD_rep = GetString("sHazInsPaymentDueD");
            dataLoan.sHazInsPaymentMadeD_rep = GetString("sHazInsPaymentMadeD");
            dataLoan.sHazInsPaidThroughD_rep = GetString("sHazInsPaidThroughD");
            dataLoan.sHazInsPolicyPayeeCode = GetString("sHazInsPolicyPayeeCode");
            dataLoan.sHazInsPolicyExpirationD_rep = GetString("sHazInsPolicyExpirationD");
            dataLoan.sHazInsPolicyPaymentTypeT = (E_InsPolicyPaymentTypeT)GetInt("sHazInsPolicyPaymentTypeT");
            dataLoan.sHazInsCompanyId = GetString("sHazInsCompanyId");

            dataLoan.sHazInsPolicyLossPayeeTransD_rep = GetString("sHazInsPolicyLossPayeeTransD");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sHazInsCompanyNm", dataLoan.sHazInsCompanyNm);
            SetResult("sHazInsPolicyNum", dataLoan.sHazInsPolicyNum);
            SetResult("sHazInsPolicyActivationD", dataLoan.sHazInsPolicyActivationD_rep);
            SetResult("sHazInsPaidByT", dataLoan.sHazInsPaidByT);
            SetResult("sHazInsCoverageAmt", dataLoan.sHazInsCoverageAmt_rep);
            SetResult("sHazInsDeductibleAmt", dataLoan.sHazInsDeductibleAmt_rep);
            SetResult("sHazInsPaymentDueAmt", dataLoan.sHazInsPaymentDueAmt_rep);
            SetResult("sHazInsPaymentDueAmtLckd", dataLoan.sHazInsPaymentDueAmtLckd);
            SetResult("sHazInsPaymentDueD", dataLoan.sHazInsPaymentDueD_rep);
            SetResult("sHazInsPaymentMadeD", dataLoan.sHazInsPaymentMadeD_rep);
            SetResult("sHazInsPaidThroughD", dataLoan.sHazInsPaidThroughD_rep);
            SetResult("sHazInsPolicyPayeeCode", dataLoan.sHazInsPolicyPayeeCode);
            SetResult("sHazInsPolicyExpirationD", dataLoan.sHazInsPolicyExpirationD_rep);
            SetResult("sHazInsPolicyPaymentTypeT", dataLoan.sHazInsPolicyPaymentTypeT);
            SetResult("sHazInsCompanyId", dataLoan.sHazInsCompanyId);

            SetResult("sHazInsPolicyLossPayeeTransD", dataLoan.sHazInsPolicyLossPayeeTransD_rep);
        }
    }
    public partial class HazardInsurancePolicyService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new HazardInsurancePolicyServiceItem());
        }
    }
}
