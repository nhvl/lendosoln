﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using LendersOffice.Constants;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class TrailingDocuments : BaseLoanPage
    {
        private ComboBox[] m_comboBoxes = null;

        protected override Permission[]  RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite, Permission.AllowAccountantRead };
            }
        }

        protected override Permission[]  RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantRead, Permission.AllowViewingInvestorInformation };
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TrailingDocuments));
            dataLoan.InitLoad();
            sRecordedD.Text = dataLoan.sRecordedD_rep;
            BindInvestorNames(dataLoan.sInvestorRolodexId);

            sRecordingInstrumentNum.Text = dataLoan.sRecordingInstrumentNum;
            sRecordingVolumeNum.Text = dataLoan.sRecordingVolumeNum;
            sRecordingBookNum.Text = dataLoan.sRecordingBookNum;
            sRecordingPageNum.Text = dataLoan.sRecordingPageNum;

            dataLoan.ByPassFieldSecurityCheck = true;
            sMersRegistrationD.Text = dataLoan.sMersRegistrationD_rep;
            sMersMin.Text = dataLoan.sMersMin;
            sMersTobD.Text = dataLoan.sMersTobD_rep;
            sMersTosD.Text = dataLoan.sMersTosD_rep;
            sMersTosDLckd.Checked = dataLoan.sMersTosDLckd;
            sMersTosD.ReadOnly = !sMersTosDLckd.Checked;
            dataLoan.ByPassFieldSecurityCheck = false;

            if (dataLoan.sPurchaseAdviceSummaryInvLoanNm != "")
            {
                sInvestorLockLoanNum.Text = dataLoan.sPurchaseAdviceSummaryInvLoanNm;
                sInvestorLockLoanNum.Attributes["data-field-id"] = nameof(CPageData.sPurchaseAdviceSummaryInvLoanNm);
            }
            else
            {
                sInvestorLockLoanNum.Text = dataLoan.sInvestorLockLoanNum;
                sInvestorLockLoanNum.Attributes["data-field-id"] = nameof(CPageData.sInvestorLockLoanNum);
            }

            sLoanPackageOrderedD.Text = dataLoan.sLoanPackageOrderedD_rep;
            sLoanPackageDocumentD.Text = dataLoan.sLoanPackageDocumentD_rep;
            sLoanPackageReceivedD.Text = dataLoan.sLoanPackageReceivedD_rep;
            sLoanPackageShippedD.Text = dataLoan.sLoanPackageShippedD_rep;
            sLoanPackageMethod.Text = dataLoan.sLoanPackageMethod;
            sLoanPackageTracking.Text = dataLoan.sLoanPackageTracking;
            sLoanPackageN.Text = dataLoan.sLoanPackageN;
            sLoanPackageRequired.Checked = dataLoan.sLoanPackageRequired;
            sLoanPackageReviewedD.Text = dataLoan.sLoanPackageReviewedD_rep;

            sMortgageDOTOrderedD.Text = dataLoan.sMortgageDOTOrderedD_rep;
            sMortgageDOTDocumentD.Text = dataLoan.sMortgageDOTDocumentD_rep;
            sMortgageDOTReceivedD.Text = dataLoan.sMortgageDOTReceivedD_rep;
            sMortgageDOTShippedD.Text = dataLoan.sMortgageDOTShippedD_rep;
            sMortgageDOTMethod.Text = dataLoan.sMortgageDOTMethod;
            sMortgageDOTTracking.Text = dataLoan.sMortgageDOTTracking;
            sMortgageDOTN.Text = dataLoan.sMortgageDOTN;
            sMortgageDOTRequired.Checked = dataLoan.sMortgageDOTRequired;
            sMortgageDOTReviewedD.Text = dataLoan.sMortgageDOTReviewedD_rep;

            sNoteOrderedD.Text = dataLoan.sNoteOrderedD_rep;
            sDocumentNoteD.Text = dataLoan.sDocumentNoteD_rep;
            sNoteReceivedD.Text = dataLoan.sNoteReceivedD_rep;
            sNoteShippedD.Text = dataLoan.sNoteShippedD_rep;
            sNoteMethod.Text = dataLoan.sNoteMethod;
            sNoteTracking.Text = dataLoan.sNoteTracking;
            sNoteN.Text = dataLoan.sNoteN;
            sNoteRequired.Checked = dataLoan.sNoteRequired;
            sNoteReviewedD.Text = dataLoan.sNoteReviewedD_rep;

            sFinalHUD1SttlmtStmtOrderedD.Text = dataLoan.sFinalHUD1SttlmtStmtOrderedD_rep;
            sFinalHUD1SttlmtStmtDocumentD.Text = dataLoan.sFinalHUD1SttlmtStmtDocumentD_rep;
            sFinalHUD1SttlmtStmtReceivedD.Text = dataLoan.sFinalHUD1SttlmtStmtReceivedD_rep;
            sFinalHUD1SttlmtStmtShippedD.Text = dataLoan.sFinalHUD1SttlmtStmtShippedD_rep;
            sFinalHUD1SttlmtStmtMethod.Text = dataLoan.sFinalHUD1SttlmtStmtMethod;
            sFinalHUD1SttlmtStmtTracking.Text = dataLoan.sFinalHUD1SttlmtStmtTracking;
            sFinalHUD1SttlmtStmtN.Text = dataLoan.sFinalHUD1SttlmtStmtN;
            sFinalHUD1SttlmtStmtRequired.Checked = dataLoan.sFinalHUD1SttlmtStmtRequired;
            sFinalHUD1SttlmtStmtReviewedD.Text = dataLoan.sFinalHUD1SttlmtStmtReviewedD_rep;

            sTitleInsPolicyOrderedD.Text = dataLoan.sTitleInsPolicyOrderedD_rep;
            sTitleInsPolicyDocumentD.Text = dataLoan.sTitleInsPolicyDocumentD_rep;
            sTitleInsPolicyReceivedD.Text = dataLoan.sTitleInsPolicyReceivedD_rep;
            sTitleInsPolicyShippedD.Text = dataLoan.sTitleInsPolicyShippedD_rep;
            sTitleInsPolicyMethod.Text = dataLoan.sTitleInsPolicyMethod;
            sTitleInsPolicyTracking.Text = dataLoan.sTitleInsPolicyTracking;
            sTitleInsPolicyN.Text = dataLoan.sTitleInsPolicyN;
            sTitleInsPolicyRequired.Checked = dataLoan.sTitleInsPolicyRequired;
            sTitleInsPolicyReviewedD.Text = dataLoan.sTitleInsPolicyReviewedD_rep;

            sMiCertOrderedD.Text = dataLoan.sMiCertOrderedD_rep;
            sMiCertIssuedD.Text = dataLoan.sMiCertIssuedD_rep;
            sMiCertReceivedD.Text = dataLoan.sMiCertReceivedD_rep;
            sMiCertShippedD.Text = dataLoan.sMiCertShippedD_rep;
            sMiCertMethod.Text = dataLoan.sMiCertMethod;
            sMiCertTracking.Text = dataLoan.sMiCertTracking;
            sMiCertN.Text = dataLoan.sMiCertN;
            sMiCertRequired.Checked = dataLoan.sMiCertRequired;
            sMiCertReviewedD.Text = dataLoan.sMiCertReviewedD_rep;

            sFinalHazInsPolicyOrderedD.Text = dataLoan.sFinalHazInsPolicyOrderedD_rep;
            sFinalHazInsPolicyDocumentD.Text = dataLoan.sFinalHazInsPolicyDocumentD_rep;
            sFinalHazInsPolicyReceivedD.Text = dataLoan.sFinalHazInsPolicyReceivedD_rep;
            sFinalHazInsPolicyShippedD.Text = dataLoan.sFinalHazInsPolicyShippedD_rep;
            sFinalHazInsPolicyMethod.Text = dataLoan.sFinalHazInsPolicyMethod;
            sFinalHazInsPolicyTracking.Text = dataLoan.sFinalHazInsPolicyTracking;
            sFinalHazInsPolicyN.Text = dataLoan.sFinalHazInsPolicyN;
            sFinalHazInsPolicyRequired.Checked = dataLoan.sFinalHazInsPolicyRequired;
            sFinalHazInsPolicyReviewedD.Text = dataLoan.sFinalHazInsPolicyReviewedD_rep;

            sFinalFloodInsPolicyOrderedD.Text = dataLoan.sFinalFloodInsPolicyOrderedD_rep;
            sFinalFloodInsPolicyDocumentD.Text = dataLoan.sFinalFloodInsPolicyDocumentD_rep;
            sFinalFloodInsPolicyReceivedD.Text = dataLoan.sFinalFloodInsPolicyReceivedD_rep;
            sFinalFloodInsPolicyShippedD.Text = dataLoan.sFinalFloodInsPolicyShippedD_rep;
            sFinalFloodInsPolicyMethod.Text = dataLoan.sFinalFloodInsPolicyMethod;
            sFinalFloodInsPolicyTracking.Text = dataLoan.sFinalFloodInsPolicyTracking;
            sFinalFloodInsPolicyN.Text = dataLoan.sFinalFloodInsPolicyN;
            sFinalFloodInsPolicyRequired.Checked = dataLoan.sFinalFloodInsPolicyRequired;
            sFinalFloodInsPolicyReviewedD.Text = dataLoan.sFinalFloodInsPolicyReviewedD_rep;

            sCustomTrailingDoc1Desc.Text = dataLoan.sCustomTrailingDoc1Desc;
            sCustomTrailingDoc1OrderedD.Text = dataLoan.sCustomTrailingDoc1OrderedD_rep;
            sCustomTrailingDoc1DocumentD.Text = dataLoan.sCustomTrailingDoc1DocumentD_rep;
            sCustomTrailingDoc1ReceivedD.Text = dataLoan.sCustomTrailingDoc1ReceivedD_rep;
            sCustomTrailingDoc1ShippedD.Text = dataLoan.sCustomTrailingDoc1ShippedD_rep;
            sCustomTrailingDoc1Method.Text = dataLoan.sCustomTrailingDoc1Method;
            sCustomTrailingDoc1Tracking.Text = dataLoan.sCustomTrailingDoc1Tracking;
            sCustomTrailingDoc1Required.Checked = dataLoan.sCustomTrailingDoc1Required;
            sCustomTrailingDoc1ReviewedD.Text = dataLoan.sCustomTrailingDoc1ReviewedD_rep;
            sCustomTrailingDoc1N.Text = dataLoan.sCustomTrailingDoc1N;

            sCustomTrailingDoc2Desc.Text = dataLoan.sCustomTrailingDoc2Desc;
            sCustomTrailingDoc2OrderedD.Text = dataLoan.sCustomTrailingDoc2OrderedD_rep;
            sCustomTrailingDoc2DocumentD.Text = dataLoan.sCustomTrailingDoc2DocumentD_rep;
            sCustomTrailingDoc2ReceivedD.Text = dataLoan.sCustomTrailingDoc2ReceivedD_rep;
            sCustomTrailingDoc2ShippedD.Text = dataLoan.sCustomTrailingDoc2ShippedD_rep;
            sCustomTrailingDoc2Method.Text = dataLoan.sCustomTrailingDoc2Method;
            sCustomTrailingDoc2Tracking.Text = dataLoan.sCustomTrailingDoc2Tracking;
            sCustomTrailingDoc2Required.Checked = dataLoan.sCustomTrailingDoc2Required;
            sCustomTrailingDoc2ReviewedD.Text = dataLoan.sCustomTrailingDoc2ReviewedD_rep;
            sCustomTrailingDoc2N.Text = dataLoan.sCustomTrailingDoc2N;

            sCustomTrailingDoc3Desc.Text = dataLoan.sCustomTrailingDoc3Desc;
            sCustomTrailingDoc3OrderedD.Text = dataLoan.sCustomTrailingDoc3OrderedD_rep;
            sCustomTrailingDoc3DocumentD.Text = dataLoan.sCustomTrailingDoc3DocumentD_rep;
            sCustomTrailingDoc3ReceivedD.Text = dataLoan.sCustomTrailingDoc3ReceivedD_rep;
            sCustomTrailingDoc3ShippedD.Text = dataLoan.sCustomTrailingDoc3ShippedD_rep;
            sCustomTrailingDoc3Method.Text = dataLoan.sCustomTrailingDoc3Method;
            sCustomTrailingDoc3Tracking.Text = dataLoan.sCustomTrailingDoc3Tracking;
            sCustomTrailingDoc3Required.Checked = dataLoan.sCustomTrailingDoc3Required;
            sCustomTrailingDoc3ReviewedD.Text = dataLoan.sCustomTrailingDoc3ReviewedD_rep;
            sCustomTrailingDoc3N.Text = dataLoan.sCustomTrailingDoc3N;

            sCustomTrailingDoc4Desc.Text = dataLoan.sCustomTrailingDoc4Desc;
            sCustomTrailingDoc4OrderedD.Text = dataLoan.sCustomTrailingDoc4OrderedD_rep;
            sCustomTrailingDoc4DocumentD.Text = dataLoan.sCustomTrailingDoc4DocumentD_rep;
            sCustomTrailingDoc4ReceivedD.Text = dataLoan.sCustomTrailingDoc4ReceivedD_rep;
            sCustomTrailingDoc4ShippedD.Text = dataLoan.sCustomTrailingDoc4ShippedD_rep;
            sCustomTrailingDoc4Method.Text = dataLoan.sCustomTrailingDoc4Method;
            sCustomTrailingDoc4Tracking.Text = dataLoan.sCustomTrailingDoc4Tracking;
            sCustomTrailingDoc4Required.Checked = dataLoan.sCustomTrailingDoc4Required;
            sCustomTrailingDoc4ReviewedD.Text = dataLoan.sCustomTrailingDoc4ReviewedD_rep;
            sCustomTrailingDoc4N.Text = dataLoan.sCustomTrailingDoc4N;

            if (sLoanPackageRequired.Checked || sMortgageDOTRequired.Checked || sNoteRequired.Checked
                || sFinalHUD1SttlmtStmtRequired.Checked || sTitleInsPolicyRequired.Checked ||
                sMiCertRequired.Checked || sFinalHazInsPolicyRequired.Checked || sFinalHazInsPolicyRequired.Checked
                || sCustomTrailingDoc1Required.Checked || sCustomTrailingDoc2Required.Checked
                || sCustomTrailingDoc3Required.Checked || sCustomTrailingDoc4Required.Checked)
            {
                sShownDocumentTypes.SelectedIndex = 1;
            }
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
            this.PageTitle = "Trailing Documents";
            this.PageID = "TrailingDocuments";

            m_comboBoxes = new ComboBox[] { sLoanPackageMethod, sMortgageDOTMethod, sNoteMethod, sFinalHUD1SttlmtStmtMethod,
                sTitleInsPolicyMethod, sMiCertMethod, sFinalHazInsPolicyMethod, sFinalFloodInsPolicyMethod, sCustomTrailingDoc1Method,
            sCustomTrailingDoc2Method, sCustomTrailingDoc3Method, sCustomTrailingDoc4Method};
            for (int i = 0; i < m_comboBoxes.Length; i++)
            {
                Tools.Bind_ShippingMethod(m_comboBoxes[i]);
            }
        }

        public void BindInvestorNames(int selected)
        {
            List<InvestorRolodexEntry> entries = InvestorRolodexEntry.GetAll(PrincipalFactory.CurrentPrincipal.BrokerId, null).ToList();
            sInvestorRolodexEntries.DataSource = entries.Where(p => p.Status == E_InvestorStatus.Active || p.Id == selected);
            sInvestorRolodexEntries.DataTextField = "InvestorName";
            sInvestorRolodexEntries.DataValueField = "Id";
            sInvestorRolodexEntries.DataBind();

            sInvestorRolodexEntries.Items.Insert(0, new ListItem("<-- Select an Investor -->", "-1"));
            Tools.SetDropDownListValue(sInvestorRolodexEntries, selected);
            
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
