﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.AntiXss;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class ServicingPayeeDlg : LendersOffice.Common.BasePage
    {
        protected void PageLoad(object sender, EventArgs args)
        {
            DataSet ds = new DataSet();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@state", Request["state"].ToString()));
            string filter = m_payeeFilter.Text.Trim();
            if (filter.Length > 0)
            {
                SqlParameter param = new SqlParameter("@filter", SqlDbType.VarChar);
                param.Value = filter;
                parameters.Add(param);
            }
            try
            {
                DataSetHelper.Fill(ds, DataSrc.LOShare, "ListTaxPayees", parameters);
                PayeeList.DataSource = ds.Tables[0].DefaultView;
                PayeeList.DataBind();
            }
            catch (SqlException e)
            {
                Tools.LogError("ServicingPayeeDlg.PageLoad - Error filling DataSet");
                Tools.LogError(e);
            }

        }
        protected void SavePayee(object sender, CommandEventArgs args)
        {
            //Grid Index passed via args.CommandArgument
            //Use this to create startup script that closes the modalDlg and returns PayeeId and PayeeName

            int index = int.Parse(args.CommandArgument.ToString());
            DataRowView row = ((DataView)PayeeList.DataSource)[index];
            string id = row[0].ToString();
            string name = row[1].ToString();
            string script = "window.dialogArguments.returnValue = "+
                "{'PayeeId':" + AspxTools.JsString(id) + ", 'PayeeName':" + AspxTools.JsString(name) + "};"+
                "onClosePopup();";
            ClientScript.RegisterStartupScript(GetType(), "SavePayee", script, true);
        }

        //Called on load, highlights matching text if a filter is used
        protected void HighlightFilter(object sender, EventArgs args)
        {
            string filterText = m_payeeFilter.Text.Trim();
            if (filterText.Length > 0)
            {
                foreach (DataGridItem item in PayeeList.Items)
                {
                    DataRowView row = ((DataView)PayeeList.DataSource)[item.ItemIndex];
                    foreach (Control c in item.Cells[0].Controls)
                    {
                        if (c is LinkButton)
                        {
                            LinkButton btn = (LinkButton)c;
                            btn.Text = string.Empty;
                            foreach (Control highlightControl in SplitIntoHighlightedControls(row["PayeeName"].ToString(), filterText))
                            {
                                btn.Controls.Add(highlightControl);
                            }

                            break;
                        }
                    }

                    TableCell cityCell = item.Cells[1];
                    cityCell.Text = string.Empty;
                    foreach (Control highlightControl in SplitIntoHighlightedControls(row["City"].ToString(), filterText))
                    {
                        cityCell.Controls.Add(highlightControl);
                    }
                }
            }
        }

        /// <summary>
        /// Adds Controls for highlighting matching text (only first match per cell).
        /// </summary>
        /// <param name="initialText">The initial text of the cell.</param>
        /// <param name="highlightTarget">The text to highlight.</param>
        /// <returns>A set of controls with the first occurrence of <paramref name="highlightTarget"/> highlighted.</returns>
        private IEnumerable<Control> SplitIntoHighlightedControls(string initialText, string highlightTarget)
        {
            int leftIndex = initialText.IndexOf(highlightTarget, StringComparison.OrdinalIgnoreCase);
            int rightIndex = leftIndex + highlightTarget.Length;
            if (leftIndex < 0)
            {
                return new[] { new EncodedLiteral() { Text = initialText } };
            }

            var controls = new List<Control>();
            if (leftIndex > 0)
            {
                controls.Add(new EncodedLiteral() { Text = initialText.Remove(leftIndex) });
            }

            var highlight = new System.Web.UI.HtmlControls.HtmlGenericControl("span");
            highlight.Style.Add("background-color", "yellow");
            highlight.InnerText = initialText.Substring(leftIndex, highlightTarget.Length);
            controls.Add(highlight);
            if (rightIndex < initialText.Length)
            {
                controls.Add(new EncodedLiteral() { Text = initialText.Substring(rightIndex) });
            }

            return controls;
        }

        protected override void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.PageLoad);
            base.OnInit(e);
        }
    }
}
