﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class GSEDelivery : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantWrite, Permission.AllowAccountantRead };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowAccountantRead };
            }
        }
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(GSEDelivery));
            dataLoan.InitLoad();

            sLenNum.Text = dataLoan.sLenNum;
            sGseDeliveryOwnershipPc.Text = dataLoan.sGseDeliveryOwnershipPc_rep;
            sGseDeliveryPlanId.Text = dataLoan.sGseDeliveryPlanId;
            sGseDeliveryCommitmentId.Text = dataLoan.sGseDeliveryCommitmentId;
            sGseDeliveryContractId.Text = dataLoan.sGseDeliveryContractId;
            sGseDeliveryServicerId.Text = dataLoan.sGseDeliveryServicerId;
            sGseDeliveryServicerIdLckd.Checked = dataLoan.sGseDeliveryServicerIdLckd;
            sGseDeliveryPayeeId.Text = dataLoan.sGseDeliveryPayeeId;
            sGseDeliveryPayeeIdLckd.Checked = dataLoan.sGseDeliveryPayeeIdLckd;
            sGseDeliveryDocumentCustodianName.Text = dataLoan.sGseDeliveryDocumentCustodianName;
            sGseDeliveryDocumentCustodianId.Text = dataLoan.sGseDeliveryDocumentCustodianId;
            sGseDeliveryBaseGuaranteeFPc.Text = dataLoan.sGseDeliveryBaseGuaranteeFPc_rep;
            sGseDeliveryBuyFPc.Text = dataLoan.sGseDeliveryBuyFPc_rep;
            sGseDeliveryGuaranteeFPc.Text = dataLoan.sGseDeliveryGuaranteeFPc_rep;
            sGseDeliveryGuaranteeFLckd.Checked = dataLoan.sGseDeliveryGuaranteeFLckd;
            sGseDeliveryRemittanceNumOfDays.Text = dataLoan.sGseDeliveryRemittanceNumOfDays_rep;
            sGseDeliveryIssueD.Text = dataLoan.sGseDeliveryIssueD_rep;
            sGseDeliveryScheduleUPBAmt.Text = dataLoan.sGseDeliveryScheduleUPBAmt_rep;
            sGseDeliveryFeature1Id.Text = dataLoan.sGseDeliveryFeature1Id;
            sGseDeliveryFeature2Id.Text = dataLoan.sGseDeliveryFeature2Id;
            sGseDeliveryFeature3Id.Text = dataLoan.sGseDeliveryFeature3Id;
            sGseDeliveryFeature4Id.Text = dataLoan.sGseDeliveryFeature4Id;
            sGseDeliveryFeature5Id.Text = dataLoan.sGseDeliveryFeature5Id;
            sGseDeliveryFeature6Id.Text = dataLoan.sGseDeliveryFeature6Id;
            sGseDeliveryFeature7Id.Text = dataLoan.sGseDeliveryFeature7Id;
            sGseDeliveryFeature8Id.Text = dataLoan.sGseDeliveryFeature8Id;
            sGseDeliveryFeature9Id.Text = dataLoan.sGseDeliveryFeature9Id;
            sGseDeliveryFeature10Id.Text = dataLoan.sGseDeliveryFeature10Id;
            sGseDeliveryComments.Text = dataLoan.sGseDeliveryComments;
            sGseDeliveryServicingFPc.Text = dataLoan.sGseDeliveryServicingFPc_rep;
            sGseDeliveryServicingF.Text = dataLoan.sGseDeliveryServicingF_rep;
            sGseInvestorLoanIdentifier.Value = dataLoan.sGseInvestorLoanIdentifier;

            Tools.SetDropDownListValue(sGseDeliveryRemittanceT, dataLoan.sGseDeliveryRemittanceT);
            Tools.SetDropDownListValue(sGseDeliveryLoanDeliveryT, dataLoan.sGseDeliveryLoanDeliveryT);
            Tools.SetDropDownListValue(sGseDeliveryLoanDefaultLossPartyT, dataLoan.sGseDeliveryLoanDefaultLossPartyT);
            Tools.SetDropDownListValue(sGseDeliveryREOMarketingPartyT, dataLoan.sGseDeliveryREOMarketingPartyT);
            Tools.SetDropDownListValue(sGseDeliveryTargetT, dataLoan.sGseDeliveryTargetT);
            RemoveObsoleteFreddieMacProducts(dataLoan.sGseFreddieMacProductT);
            Tools.SetDropDownListValue(sGseFreddieMacProductT, dataLoan.sGseFreddieMacProductT);
            Tools.SetDropDownListValue(sGseDeliveryPayeeT, dataLoan.sGseDeliveryPayeeT);
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "GSEDelivery";
            this.PageID = "GSEDelivery";

            Tools.Bind_sGseDeliveryRemittanceT(sGseDeliveryRemittanceT);
            Tools.Bind_sGseDeliveryLoanDeliveryT(sGseDeliveryLoanDeliveryT);
            Tools.Bind_sGseDeliveryREOMarketingPartyT(sGseDeliveryREOMarketingPartyT);
            Tools.Bind_sGseDeliveryLoanDefaultLossPartyT(sGseDeliveryLoanDefaultLossPartyT);
            Tools.Bind_sGseDeliveryTargetT(sGseDeliveryTargetT);
            Tools.Bind_sGseFreddieMacProductT(sGseFreddieMacProductT);
            Tools.Bind_sGseDeliveryPayeeT(sGseDeliveryPayeeT);
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        /// <summary>
        /// Remove all obsolete Freddie Mac loan products from the UI unless the loan file
        /// is already set to an obsolete product, in which case we leave that one bound.
        /// </summary>
        /// <param name="selectedProduct">The selected product for this loan.</param>
        protected void RemoveObsoleteFreddieMacProducts(E_sGseFreddieMacProductT selectedProduct)
        {
            if (!Enum.Equals(selectedProduct, E_sGseFreddieMacProductT.HomePossible97))
            {
                sGseFreddieMacProductT.Items.Remove(Tools.CreateEnumListItem("Home Possible 97 (Obsolete)", E_sGseFreddieMacProductT.HomePossible97));
            }

            if (!Enum.Equals(selectedProduct, E_sGseFreddieMacProductT.HomePossibleNeighborhoodSolution97))
            {
                sGseFreddieMacProductT.Items.Remove(Tools.CreateEnumListItem("Home Possible Neighborhood Solution 97 (Obsolete)", E_sGseFreddieMacProductT.HomePossibleNeighborhoodSolution97));
            }
        }
    }
}
