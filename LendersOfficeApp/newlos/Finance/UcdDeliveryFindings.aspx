﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UcdDeliveryFindings.aspx.cs" Inherits="LendersOfficeApp.newlos.Finance.UcdDeliveryFindings" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>UCD Delivery Findings</title>
    <style type="text/css">
        div.message {
            text-align: center;
            margin-top: 25%;
        }
    </style>
</head>
<body>
    <h4 class="page-header">UCD Delivery Findings</h4>
    <form id="form1" runat="server">
    <div class="message">
        No findings to display.
    </div>
    </form>
</body>
</html>
