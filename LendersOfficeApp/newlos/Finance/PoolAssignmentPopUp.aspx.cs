﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;
using System.Web.Services;
using LendersOffice.ObjLib.MortgagePool;
using System.Data;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class PoolAssignmentPopUp : BaseLoanPage
    {
        
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(PoolAssignmentPopUp));
            dataLoan.InitLoad();

            Tools.Bind_Generic(sPoolAgencyT, E_MortgagePoolAgencyT.FannieMae, Enum.GetNames(typeof(E_MortgagePoolAgencyT)).Where(a => a != "LeaveBlank"));
        }

        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;
            DisplayCopyRight = false;
            base.OnInit(e);
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowCapitalMarketsAccess)) throw new AccessDenied();
            if (Page.IsPostBack)
            {
                return;
            }           
        }

        [WebMethod]
        public static object FindPool(string PoolName, E_MortgagePoolAgencyT Agency)
        {
            PoolName = PoolName.TrimWhitespaceAndBOM();
            var dt = MortgagePool.FindMortgagePools(PoolName, false);
            var matchesAgency = new List<DataRow>();
            foreach (DataRow row in dt.Rows)
            {
                if (row[3].ToString().Replace(" ", "") == Enum.GetName(typeof(E_MortgagePoolAgencyT), Agency))
                {
                    matchesAgency.Add(row);
                }
            }
            if (matchesAgency.Count > 1)
            {
                return new
                {
                    FoundMatch = true,
                    MultipleResults = true
                };
            }
            else if (matchesAgency.Count == 1)
            {
                return new
                {
                    FoundMatch = true,
                    MultipleResults = false,
                    PoolId = matchesAgency.First()[0]
                };
            }
            else
            {
                return new
                {
                    FoundMatch = false
                };
            }
        }

        [WebMethod]
        public static void AssignLoan(Guid LoanId, long PoolId)
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowCapitalMarketsAccess)) return;

            CPageData loan = new CPageData(LoanId, new[]{"sMortgagePoolId"});
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
            
            //Doing this will verify that user can access the mortgage pool
            MortgagePool mp = new MortgagePool(PoolId);

            loan.sMortgagePoolId = PoolId;
            loan.Save();
        }
    }
}
