﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LendersOfficeApp.newlos.Finance {
    
    
    public partial class PurchaseAdvice {
        
        /// <summary>
        /// sPurchaseAdviceUnpaidPBal2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceUnpaidPBal2;
        
        /// <summary>
        /// sClosedD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sClosedD;
        
        /// <summary>
        /// sPurchaseAdviceTotalPrice_Field22 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceTotalPrice_Field22;
        
        /// <summary>
        /// sLPurchaseD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sLPurchaseD;
        
        /// <summary>
        /// sPurchaseAdviceInterestTotalAdj2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceInterestTotalAdj2;
        
        /// <summary>
        /// sSchedDueD1Lckd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sSchedDueD1Lckd;
        
        /// <summary>
        /// sSchedDueD1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sSchedDueD1;
        
        /// <summary>
        /// sPurchaseAdviceEscrowAmtDueInv2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceEscrowAmtDueInv2;
        
        /// <summary>
        /// sInvSchedDueD1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sInvSchedDueD1;
        
        /// <summary>
        /// sPurchaseAdviceFeesTotal2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceFeesTotal2;
        
        /// <summary>
        /// sPurchaseAdviceSummaryInvNm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.ComboBox sPurchaseAdviceSummaryInvNm;
        
        /// <summary>
        /// sPurchaseAdviceSummaryInvId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField sPurchaseAdviceSummaryInvId;
        
        /// <summary>
        /// sPurchaseAdviceSummarySubtotal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceSummarySubtotal;
        
        /// <summary>
        /// sPurchaseAdviceSummaryInvPgNm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sPurchaseAdviceSummaryInvPgNm;
        
        /// <summary>
        /// sPurchaseAdviceSummaryOtherAdjDesc control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sPurchaseAdviceSummaryOtherAdjDesc;
        
        /// <summary>
        /// sPurchaseAdviceSummaryOtherAdjVal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceSummaryOtherAdjVal;
        
        /// <summary>
        /// sPurchaseAdviceSummaryInvLoanNm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sPurchaseAdviceSummaryInvLoanNm;
        
        /// <summary>
        /// sPurchaseAdviceSummaryTotalDueSeller control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceSummaryTotalDueSeller;
        
        /// <summary>
        /// sPurchaseAdviceSummaryServicingStatus control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sPurchaseAdviceSummaryServicingStatus;
        
        /// <summary>
        /// m_pullLoanDetailsBtn control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputButton m_pullLoanDetailsBtn;
        
        /// <summary>
        /// sFinalLAmt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sFinalLAmt;
        
        /// <summary>
        /// sPurchaseAdviceInterestDueSellerOrInvestor control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceInterestDueSellerOrInvestor;
        
        /// <summary>
        /// sPurchaseAdviceAmortCurtail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceAmortCurtail;
        
        /// <summary>
        /// sPurchaseAdviceInterestReimbursement control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceInterestReimbursement;
        
        /// <summary>
        /// sPurchaseAdviceUnpaidPBal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceUnpaidPBal;
        
        /// <summary>
        /// sPurchaseAdviceInterestTotalAdj control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceInterestTotalAdj;
        
        /// <summary>
        /// sPurchaseAdviceBasePrice_AmountPriceCalcMode_Price control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton sPurchaseAdviceBasePrice_AmountPriceCalcMode_Price;
        
        /// <summary>
        /// sPurchaseAdviceBasePrice_AmountPriceCalcMode_Amount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButton sPurchaseAdviceBasePrice_AmountPriceCalcMode_Amount;
        
        /// <summary>
        /// sPurchaseAdviceBasePrice_Field1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sPurchaseAdviceBasePrice_Field1;
        
        /// <summary>
        /// sPurchaseAdviceBasePrice_Field2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceBasePrice_Field2;
        
        /// <summary>
        /// sPurchaseAdviceBasePrice_Field3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sPurchaseAdviceBasePrice_Field3;
        
        /// <summary>
        /// sPurchaseAdviceAdjustments_Field2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceAdjustments_Field2;
        
        /// <summary>
        /// sPurchaseAdviceAdjustments_Field3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sPurchaseAdviceAdjustments_Field3;
        
        /// <summary>
        /// sPurchaseAdviceEscrowTotCollAtClosing control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceEscrowTotCollAtClosing;
        
        /// <summary>
        /// sPurchaseAdviceNetPrice_Field1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sPurchaseAdviceNetPrice_Field1;
        
        /// <summary>
        /// sPurchaseAdviceNetPrice_Field2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceNetPrice_Field2;
        
        /// <summary>
        /// sPurchaseAdviceNetPrice_Field3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sPurchaseAdviceNetPrice_Field3;
        
        /// <summary>
        /// sPurchaseAdviceEscrowDepDue control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceEscrowDepDue;
        
        /// <summary>
        /// sPurchaseAdviceSRP_Field2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceSRP_Field2;
        
        /// <summary>
        /// sPurchaseAdviceSRP_Field3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sPurchaseAdviceSRP_Field3;
        
        /// <summary>
        /// sPurchaseAdviceEscrowDisbursements control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceEscrowDisbursements;
        
        /// <summary>
        /// sPurchaseAdviceTotalPrice_Field1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sPurchaseAdviceTotalPrice_Field1;
        
        /// <summary>
        /// sPurchaseAdviceTotalPrice_Field2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceTotalPrice_Field2;
        
        /// <summary>
        /// sPurchaseAdviceTotalPrice_Field3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PercentTextBox sPurchaseAdviceTotalPrice_Field3;
        
        /// <summary>
        /// sPurchaseAdviceEscrowAmtDueInv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceEscrowAmtDueInv;
        
        /// <summary>
        /// sPurchaseAdviceFeesTotal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sPurchaseAdviceFeesTotal;
        
        /// <summary>
        /// CModalDlg1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.common.ModalDlg.cModalDlg CModalDlg1;
    }
}
