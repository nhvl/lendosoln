﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class LoanOfficerPricingPolicyServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanOfficerPricingPolicyServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SetPolicyFromLORecord":
                    SetPolicyFromLORecord();
                    break;
                case "SetPolicyManually":
                    SetPolicyManually();
                    break;
                case "ClearPolicy":
                    ClearPolicy();
                    break;
            }
        }

        private void SetPolicyFromLORecord()
        {
            Guid sLId = GetGuid("LoanId");
            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.SetLoanOfficerPricingPolicyFromLORecord(PrincipalFactory.CurrentPrincipal.DisplayName);

            dataLoan.Save();

            LoadLoanOfficerPricingPolicyData(dataLoan);

            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        private void SetPolicyManually()
        {
            Guid sLId = GetGuid("LoanId");
            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            SetPolicyManuallyImpl(dataLoan);

            dataLoan.Save();

            LoadLoanOfficerPricingPolicyData(dataLoan);
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        private void SetPolicyManuallyImpl(CPageData dataLoan)
        {
            var customPricingPolicyFieldList = dataLoan.BrokerDB.CustomPricingPolicyFieldList;
            Func<int, bool> isFieldDefined = (keywordNum) => customPricingPolicyFieldList.IsFieldDefined(keywordNum);

            dataLoan.SetLoanOfficerPricingPolicyManually(PrincipalFactory.CurrentPrincipal.DisplayName,
                isFieldDefined(1) ? GetString("sCustomPricingPolicyField1") : "",
                isFieldDefined(2) ? GetString("sCustomPricingPolicyField2") : "",
                isFieldDefined(3) ? GetString("sCustomPricingPolicyField3") : "",
                isFieldDefined(4) ? GetString("sCustomPricingPolicyField4") : "",
                isFieldDefined(5) ? GetString("sCustomPricingPolicyField5") : "");
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // If the user triggers a save, we only want to write the policy
            // when it was not taken from an employee.
            if (dataLoan.sCustomPricingPolicySource == E_sCustomPricingPolicySourceT.Manual ||
                dataLoan.sCustomPricingPolicySource == E_sCustomPricingPolicySourceT.NotSet)
            {
                SetPolicyManuallyImpl(dataLoan);
            }
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            LoadLoanOfficerPricingPolicyData(dataLoan);
        }

        private void LoadLoanOfficerPricingPolicyData(CPageData dataLoan)
        {
            if (dataLoan.sIsCustomPricingPolicySet)
            {
                if (dataLoan.sCustomPricingPolicySource == E_sCustomPricingPolicySourceT.Manual)
                {
                    SetResult("PolicySourceExplanation", "Set manually by ");
                    SetResult("sCustomPricingPolicySourceEmployeeNm", dataLoan.sCustomPricingPolicyAppliedBy);
                    SetResult("sCustomPricingPolicySourceEmployeeId", "");
                }
                else if (dataLoan.sCustomPricingPolicySource == E_sCustomPricingPolicySourceT.LoanOfficer)
                {
                    Guid employeeId = dataLoan.sCustomPricingPolicySourceEmployeeId;
                    var employee = new EmployeeDB(employeeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                    employee.Retrieve();
                    if (employee.UserType == 'P' && BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanAdministrateExternalUsers))
                    {
                        SetResult("sCustomPricingPolicySourceEmployeeId", dataLoan.sCustomPricingPolicySourceEmployeeId);
                    }
                    else if (BrokerUserPrincipal.CurrentPrincipal.HasRole(E_RoleT.Administrator))
                    {
                        SetResult("sCustomPricingPolicySourceEmployeeId", dataLoan.sCustomPricingPolicySourceEmployeeId);
                    }
                    else
                    {
                        SetResult("sCustomPricingPolicySourceEmployeeId", "");
                    }
                    SetResult("sCustomPricingPolicySourceEmployeeNm", dataLoan.sCustomPricingPolicySourceEmployeeNm);
                    SetResult("CustomPricingPolicySourceUserType", employee.UserType.ToString());
                    SetResult("PolicySourceExplanation", "Set from the employee record of ");
                }
                else if (dataLoan.sCustomPricingPolicySource == E_sCustomPricingPolicySourceT.OriginatingCompany)
                {
                    SetResult("sCustomPricingPolicySourceEmployeeId", "");
                    SetResult("sCustomPricingPolicySourceEmployeeNm", dataLoan.sCustomPricingPolicySourceOCNm);
                    SetResult(
                        "PolicySourceExplanation",
                        "Could not find an assigned loan officer. Policy set from the originating company record of ");
                }
                else if (dataLoan.sCustomPricingPolicySource == E_sCustomPricingPolicySourceT.Branch)
                {
                    SetResult("sCustomPricingPolicySourceEmployeeId", "");
                    SetResult("sCustomPricingPolicySourceEmployeeNm", dataLoan.sCustomPricingPolicySourceBranchNm);
                    SetResult(
                        "PolicySourceExplanation",
                        "Could not find an assigned loan officer. Policy set from the branch record of ");
                }

                SetResult("sCustomPricingPolicyAuditD", dataLoan.sCustomPricingPolicyAuditD_rep);
            }

            SetResult("sCustomPricingPolicyField1", dataLoan.sCustomPricingPolicyField1);
            SetResult("sCustomPricingPolicyField2", dataLoan.sCustomPricingPolicyField2);
            SetResult("sCustomPricingPolicyField3", dataLoan.sCustomPricingPolicyField3);
            SetResult("sCustomPricingPolicyField4", dataLoan.sCustomPricingPolicyField4);
            SetResult("sCustomPricingPolicyField5", dataLoan.sCustomPricingPolicyField5);
        }

        private void ClearPolicy()
        {
            Guid sLId = GetGuid("LoanId");
            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.ClearLoanOfficerPricingPolicy();
            dataLoan.Save();
            LoadLoanOfficerPricingPolicyData(dataLoan);
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }
    }
    public partial class LoanOfficerPricingPolicyService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new LoanOfficerPricingPolicyServiceItem());
        }
    }
}
