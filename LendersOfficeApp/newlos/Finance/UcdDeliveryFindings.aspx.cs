﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.Finance
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.UcdDelivery;
    using LendersOffice.Integration.UcdDelivery.FannieMae;
    using LendersOffice.Integration.UcdDelivery.FreddieMac;

    /// <summary>
    /// Contains UCD Delivery Findings HTML for end-user consumption.
    /// </summary>
    public partial class UcdDeliveryFindings : BasePage
    {
        /// <summary>
        /// Handles the Page object's Load event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Guid loanId = RequestHelper.LoanID;
            int requestedOrderId = RequestHelper.GetInt("deliveryId", -1);
            if (loanId == Guid.Empty || requestedOrderId == -1)
            {
                return;
            }

            var dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(UcdDeliveryFindings));
            dataLoan.InitLoad();

            var deliveryItems = dataLoan.sUcdDeliveryCollection;
            var chosenItem = deliveryItems.Get(requestedOrderId);
            if (chosenItem == null)
            {
                return;
            }

            string html;
            if (chosenItem.DeliveredTo == UcdDeliveredToEntity.FannieMae)
            {
                var fannieData = chosenItem as FannieMaeUcdDelivery;
                html = fannieData.FindingsHtml;
            }
            else if (chosenItem.DeliveredTo == UcdDeliveredToEntity.FreddieMac)
            {
                var freddieData = chosenItem as FreddieMacUcdDelivery;
                html = freddieData.FindingsHtml;
            }
            else
            {
                Tools.LogError($"Uhandled enum: {chosenItem.DeliveredTo}");
                return;
            }

            Response.ClearContent();
            Response.Write(html);
            Response.End();
        }
    }
}
