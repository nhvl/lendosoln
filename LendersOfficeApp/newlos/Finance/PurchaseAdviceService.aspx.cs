﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Finance
{
    public class PurchaseAdviceServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(PurchaseAdviceServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "PullDetailsFromLoanFile":
                    PullDetailsFromLoanFile();
                    break;
                default:
                    break;
            }
        }

        private void PullDetailsFromLoanFile()
        {
            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitLoad();
            dataLoan.ApplyLoanDataToPurchaseAdvicePage();
            LoadData(dataLoan, null);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {

            dataLoan.sPurchaseAdviceSummaryOtherAdjDesc = GetString("sPurchaseAdviceSummaryOtherAdjDesc");
            dataLoan.sPurchaseAdviceSummaryOtherAdjVal_rep = GetString("sPurchaseAdviceSummaryOtherAdjVal");
            dataLoan.sPurchaseAdviceSummaryInvNm = GetString("sPurchaseAdviceSummaryInvNm");
            dataLoan.sPurchaseAdviceSummaryInvId_rep = GetString("sPurchaseAdviceSummaryInvId");
            dataLoan.sPurchaseAdviceSummaryInvPgNm = GetString("sPurchaseAdviceSummaryInvPgNm");
            dataLoan.sPurchaseAdviceSummaryInvLoanNm = GetString("sPurchaseAdviceSummaryInvLoanNm");
            dataLoan.sPurchaseAdviceSummaryServicingStatus = GetString("sPurchaseAdviceSummaryServicingStatus");
            dataLoan.sClosedD_rep = GetString("sClosedD");
            dataLoan.sLPurchaseD_rep = GetString("sLPurchaseD");
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");
            dataLoan.sInvSchedDueD1_rep = GetString("sInvSchedDueD1");
            
            dataLoan.sPurchaseAdviceAmortCurtail_rep = GetString("sPurchaseAdviceAmortCurtail");
            //dataLoan.CalculateBasePriceFields(GetInt("numFixedField"), GetString("sPurchaseAdviceBasePrice_Field1"), GetString("sPurchaseAdviceBasePrice_Field2"), GetString("sPurchaseAdviceBasePrice_Field3"));
            dataLoan.sPurchaseAdviceBasePrice_AmountPriceCalcMode = (E_AmountPriceCalcMode)GetInt("sPurchaseAdviceBasePrice_AmountPriceCalcMode");
            dataLoan.sPurchaseAdviceBasePrice_Field1_rep = GetString("sPurchaseAdviceBasePrice_Field1");
            dataLoan.sPurchaseAdviceBasePrice_Field2_rep = GetString("sPurchaseAdviceBasePrice_Field2");
            dataLoan.sPurchaseAdviceInterestDueSellerOrInvestor_rep = GetString("sPurchaseAdviceInterestDueSellerOrInvestor");
            dataLoan.sPurchaseAdviceInterestReimbursement_rep = GetString("sPurchaseAdviceInterestReimbursement");

            dataLoan.sPurchaseAdviceEscrowTotCollAtClosing_rep = GetString("sPurchaseAdviceEscrowTotCollAtClosing");
            dataLoan.sPurchaseAdviceEscrowDisbursements_rep = GetString("sPurchaseAdviceEscrowDisbursements");
            dataLoan.sPurchaseAdviceEscrowDepDue_rep = GetString("sPurchaseAdviceEscrowDepDue");

            // 9/3/2013 AV - 64100 Purchase Advice Base Loan Amount -- This needs to be set last because it depends on the fields above 
            dataLoan.sPurchaseAdviceFees = ObsoleteSerializationHelper.JsonDeserialize<List<CPurchaseAdviceFeesFields>>(GetString("sFees"));
            dataLoan.sPurchaseAdviceAdjustments = ObsoleteSerializationHelper.JsonDeserialize<List<CPurchaseAdviceAdjustmentsFields>>(GetString("sAdjustments"));

        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            foreach (CPurchaseAdviceFeesFields fee in dataLoan.sPurchaseAdviceFees)
            {
                int rowNum = fee.RowNum;
                SetResult("FeeDesc" + rowNum, fee.FeeDesc);
                SetResult("FeeAmt" + rowNum, fee.FeeAmt_rep);
            }
            
            foreach (CPurchaseAdviceAdjustmentsFields adjustment in dataLoan.sPurchaseAdviceAdjustments)
            {
                int rowNum = adjustment.RowNum;
                SetResult("AdjDesc" + rowNum, adjustment.AdjDesc);
                SetResult("AdjPc" + rowNum, adjustment.AdjPc_rep);
                SetResult("AdjAmt" + rowNum, adjustment.AdjAmt_rep);
                SetResult("IsSRP" + rowNum, adjustment.IsSRP);
            }
            SetResult("adjustmentTableSize", dataLoan.sPurchaseAdviceAdjustments.Count);
            
            SetResult("sPurchaseAdviceUnpaidPBal2", dataLoan.sPurchaseAdviceUnpaidPBal_rep);
            SetResult("sPurchaseAdviceTotalPrice_Field22", dataLoan.sPurchaseAdviceTotalPrice_Field2_rep);
            SetResult("sPurchaseAdviceInterestTotalAdj2", dataLoan.sPurchaseAdviceInterestTotalAdj_rep);
            SetResult("sPurchaseAdviceEscrowAmtDueInv2", dataLoan.sPurchaseAdviceEscrowAmtDueInv_rep);
            SetResult("sPurchaseAdviceSummaryOtherAdjDesc", dataLoan.sPurchaseAdviceSummaryOtherAdjDesc);
            SetResult("sPurchaseAdviceSummaryOtherAdjVal", dataLoan.sPurchaseAdviceSummaryOtherAdjVal_rep);
            SetResult("sPurchaseAdviceSummaryInvNm", dataLoan.sPurchaseAdviceSummaryInvNm);
            SetResult("sPurchaseAdviceSummaryInvId", dataLoan.sPurchaseAdviceSummaryInvId_rep);
            SetResult("sPurchaseAdviceSummaryInvPgNm", dataLoan.sPurchaseAdviceSummaryInvPgNm);
            SetResult("sPurchaseAdviceSummaryInvLoanNm", dataLoan.sPurchaseAdviceSummaryInvLoanNm);
            SetResult("sPurchaseAdviceSummaryServicingStatus", dataLoan.sPurchaseAdviceSummaryServicingStatus);
            SetResult("sPurchaseAdviceFeesTotal2", dataLoan.sPurchaseAdviceFeesTotal_rep);
            SetResult("sPurchaseAdviceSummarySubtotal", dataLoan.sPurchaseAdviceSummarySubtotal_rep);
            SetResult("sPurchaseAdviceSummaryTotalDueSeller", dataLoan.sPurchaseAdviceSummaryTotalDueSeller_rep);
            SetResult("sClosedD", dataLoan.sClosedD_rep);
            SetResult("sLPurchaseD", dataLoan.sLPurchaseD_rep);
            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
            SetResult("sInvSchedDueD1", dataLoan.sInvSchedDueD1_rep);

            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sPurchaseAdviceAmortCurtail", dataLoan.sPurchaseAdviceAmortCurtail_rep);
            SetResult("sPurchaseAdviceUnpaidPBal", dataLoan.sPurchaseAdviceUnpaidPBal_rep);
            SetResult("sPurchaseAdviceBasePrice_Field1", dataLoan.sPurchaseAdviceBasePrice_Field1_rep);
            SetResult("sPurchaseAdviceBasePrice_Field2", dataLoan.sPurchaseAdviceBasePrice_Field2_rep);
            SetResult("sPurchaseAdviceBasePrice_Field3", dataLoan.sPurchaseAdviceBasePrice_Field3_rep);
            SetResult("sPurchaseAdviceAdjustments_Field2", dataLoan.sPurchaseAdviceAdjustments_Field2_rep);
            SetResult("sPurchaseAdviceAdjustments_Field3", dataLoan.sPurchaseAdviceAdjustments_Field3_rep);
            SetResult("sPurchaseAdviceNetPrice_Field1", dataLoan.sPurchaseAdviceNetPrice_Field1_rep);
            SetResult("sPurchaseAdviceNetPrice_Field2", dataLoan.sPurchaseAdviceNetPrice_Field2_rep);
            SetResult("sPurchaseAdviceNetPrice_Field3", dataLoan.sPurchaseAdviceNetPrice_Field3_rep);
            SetResult("sPurchaseAdviceSRP_Field2", dataLoan.sPurchaseAdviceSRP_Field2_rep);
            SetResult("sPurchaseAdviceSRP_Field3", dataLoan.sPurchaseAdviceSRP_Field3_rep);
            SetResult("sPurchaseAdviceTotalPrice_Field1", dataLoan.sPurchaseAdviceTotalPrice_Field1_rep);
            SetResult("sPurchaseAdviceTotalPrice_Field2", dataLoan.sPurchaseAdviceTotalPrice_Field2_rep);
            SetResult("sPurchaseAdviceTotalPrice_Field3", dataLoan.sPurchaseAdviceTotalPrice_Field3_rep);

            SetResult("sPurchaseAdviceInterestDueSellerOrInvestor", dataLoan.sPurchaseAdviceInterestDueSellerOrInvestor_rep);
            SetResult("sPurchaseAdviceInterestReimbursement", dataLoan.sPurchaseAdviceInterestReimbursement_rep);
            SetResult("sPurchaseAdviceInterestTotalAdj", dataLoan.sPurchaseAdviceInterestTotalAdj_rep);

            SetResult("sPurchaseAdviceEscrowTotCollAtClosing", dataLoan.sPurchaseAdviceEscrowTotCollAtClosing_rep);
            SetResult("sPurchaseAdviceEscrowDisbursements", dataLoan.sPurchaseAdviceEscrowDisbursements_rep);
            SetResult("sPurchaseAdviceEscrowDepDue", dataLoan.sPurchaseAdviceEscrowDepDue_rep);
            SetResult("sPurchaseAdviceEscrowAmtDueInv", dataLoan.sPurchaseAdviceEscrowAmtDueInv_rep);
            
            SetResult("sPurchaseAdviceFeesTotal", dataLoan.sPurchaseAdviceFeesTotal_rep);
        }
    }
    public partial class PurchaseAdviceService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new PurchaseAdviceServiceItem());
        }
    }


}
