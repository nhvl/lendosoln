﻿#region Generated Code -- A lie to dupe the merciless StyleCop.
namespace LendersOfficeApp.newlos.Finance
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.PdfForm;

    /// <summary>
    /// This page provides extra data points and allows generating the payment statement.
    /// </summary>
    public partial class GeneratePaymentStatement : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            this.DisplayCopyRight = false;

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            this.RegisterJsGlobalVariables("BrokerId", this.BrokerID);

            this.CustomPdfForms.AvailableSrc = PdfForm.RetrieveCustomFormsOfCurrentBroker();
            this.CustomPdfForms.TextField = nameof(PdfForm.Description);
            this.CustomPdfForms.ValueField = nameof(PdfForm.FormId);
            this.CustomPdfForms.DataBind();

            base.OnInit(e);
        }

        /// <summary>
        /// Handles page load.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.periodStart.Text = CDateTime.Create(RequestHelper.GetSafeQueryString("start"), null).ToString();
            this.periodEnd.Text = CDateTime.Create(RequestHelper.GetSafeQueryString("end"), null).ToString();
        }
    }
}