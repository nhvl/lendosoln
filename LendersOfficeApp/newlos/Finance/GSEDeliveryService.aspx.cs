﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{
    public class GSEDeliveryServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(GSEDeliveryServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sLenNum = GetString("sLenNum");
            dataLoan.sGseDeliveryOwnershipPc_rep = GetString("sGseDeliveryOwnershipPc");
            dataLoan.sGseDeliveryPlanId = GetString("sGseDeliveryPlanId");
            dataLoan.sGseDeliveryCommitmentId = GetString("sGseDeliveryCommitmentId");
            dataLoan.sGseDeliveryContractId = GetString("sGseDeliveryContractId");
            dataLoan.sGseDeliveryServicerId = GetString("sGseDeliveryServicerId");
            dataLoan.sGseDeliveryServicerIdLckd = GetBool("sGseDeliveryServicerIdLckd");
            dataLoan.sGseDeliveryPayeeId = GetString("sGseDeliveryPayeeId");
            dataLoan.sGseDeliveryPayeeIdLckd = GetBool("sGseDeliveryPayeeIdLckd");
            dataLoan.sGseDeliveryDocumentCustodianName = GetString("sGseDeliveryDocumentCustodianName");
            dataLoan.sGseDeliveryDocumentCustodianId = GetString("sGseDeliveryDocumentCustodianId");
            dataLoan.sGseDeliveryBaseGuaranteeFPc_rep = GetString("sGseDeliveryBaseGuaranteeFPc");
            dataLoan.sGseDeliveryBuyFPc_rep = GetString("sGseDeliveryBuyFPc");
            dataLoan.sGseDeliveryGuaranteeFPc_rep = GetString("sGseDeliveryGuaranteeFPc");
            dataLoan.sGseDeliveryGuaranteeFLckd = GetBool("sGseDeliveryGuaranteeFLckd");
            dataLoan.sGseDeliveryRemittanceNumOfDays_rep = GetString("sGseDeliveryRemittanceNumOfDays");
            dataLoan.sGseDeliveryIssueD_rep = GetString("sGseDeliveryIssueD");
            dataLoan.sGseDeliveryScheduleUPBAmt_rep = GetString("sGseDeliveryScheduleUPBAmt");
            dataLoan.sGseDeliveryFeature1Id = GetString("sGseDeliveryFeature1Id");
            dataLoan.sGseDeliveryFeature2Id = GetString("sGseDeliveryFeature2Id");
            dataLoan.sGseDeliveryFeature3Id = GetString("sGseDeliveryFeature3Id");
            dataLoan.sGseDeliveryFeature4Id = GetString("sGseDeliveryFeature4Id");
            dataLoan.sGseDeliveryFeature5Id = GetString("sGseDeliveryFeature5Id");
            dataLoan.sGseDeliveryFeature6Id = GetString("sGseDeliveryFeature6Id");
            dataLoan.sGseDeliveryFeature7Id = GetString("sGseDeliveryFeature7Id");
            dataLoan.sGseDeliveryFeature8Id = GetString("sGseDeliveryFeature8Id");
            dataLoan.sGseDeliveryFeature9Id = GetString("sGseDeliveryFeature9Id");
            dataLoan.sGseDeliveryFeature10Id = GetString("sGseDeliveryFeature10Id");
            dataLoan.sGseDeliveryComments = GetString("sGseDeliveryComments");
            dataLoan.sGseInvestorLoanIdentifier = GetString("sGseInvestorLoanIdentifier");
            dataLoan.sGseDeliveryRemittanceT = (E_sGseDeliveryRemittanceT)GetInt("sGseDeliveryRemittanceT");
            dataLoan.sGseDeliveryLoanDeliveryT = (E_sGseDeliveryLoanDeliveryT)GetInt("sGseDeliveryLoanDeliveryT");
            dataLoan.sGseDeliveryREOMarketingPartyT = (E_sGseDeliveryREOMarketingPartyT)GetInt("sGseDeliveryREOMarketingPartyT");
            dataLoan.sGseDeliveryTargetT = (E_sGseDeliveryTargetT)GetInt("sGseDeliveryTargetT");
            dataLoan.sGseDeliveryLoanDefaultLossPartyT = (E_sGseDeliveryLoanDefaultLossPartyT)GetInt("sGseDeliveryLoanDefaultLossPartyT");
            dataLoan.sGseDeliveryServicingFPc_rep = GetString("sGseDeliveryServicingFPc");
            dataLoan.sGseDeliveryServicingF_rep = GetString("sGseDeliveryServicingF");
            dataLoan.sGseFreddieMacProductT = (E_sGseFreddieMacProductT)GetInt("sGseFreddieMacProductT");
            dataLoan.sGseDeliveryPayeeT = (E_sGseDeliveryPayeeT)GetInt("sGseDeliveryPayeeT");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sLenNum", dataLoan.sLenNum);
            SetResult("sGseDeliveryRemittanceT", dataLoan.sGseDeliveryRemittanceT);
            SetResult("sGseDeliveryOwnershipPc", dataLoan.sGseDeliveryOwnershipPc_rep);
            SetResult("sGseDeliveryPlanId", dataLoan.sGseDeliveryPlanId);
            SetResult("sGseDeliveryCommitmentId", dataLoan.sGseDeliveryCommitmentId);
            SetResult("sGseDeliveryContractId", dataLoan.sGseDeliveryContractId);
            SetResult("sGseDeliveryServicerId", dataLoan.sGseDeliveryServicerId);
            SetResult("sGseDeliveryServicerIdLckd", dataLoan.sGseDeliveryServicerIdLckd);
            SetResult("sGseDeliveryPayeeId", dataLoan.sGseDeliveryPayeeId);
            SetResult("sGseDeliveryPayeeIdLckd", dataLoan.sGseDeliveryPayeeIdLckd);
            SetResult("sGseDeliveryDocumentCustodianName", dataLoan.sGseDeliveryDocumentCustodianName);
            SetResult("sGseDeliveryDocumentCustodianId", dataLoan.sGseDeliveryDocumentCustodianId);
            SetResult("sGseDeliveryLoanDeliveryT", dataLoan.sGseDeliveryLoanDeliveryT);
            SetResult("sGseDeliveryTargetT", dataLoan.sGseDeliveryTargetT);
            SetResult("sGseDeliveryBaseGuaranteeFPc", dataLoan.sGseDeliveryBaseGuaranteeFPc_rep);
            SetResult("sGseDeliveryBuyFPc", dataLoan.sGseDeliveryBuyFPc_rep);
            SetResult("sGseDeliveryGuaranteeFPc", dataLoan.sGseDeliveryGuaranteeFPc_rep);
            SetResult("sGseDeliveryGuaranteeFLckd", dataLoan.sGseDeliveryGuaranteeFLckd);
            SetResult("sGseDeliveryRemittanceNumOfDays", dataLoan.sGseDeliveryRemittanceNumOfDays_rep);
            SetResult("sGseDeliveryIssueD", dataLoan.sGseDeliveryIssueD);
            SetResult("sGseDeliveryScheduleUPBAmt", dataLoan.sGseDeliveryScheduleUPBAmt_rep);
            SetResult("sGseDeliveryLoanDefaultLossPartyT", dataLoan.sGseDeliveryLoanDefaultLossPartyT);
            SetResult("sGseDeliveryREOMarketingPartyT", dataLoan.sGseDeliveryREOMarketingPartyT);
            SetResult("sGseDeliveryFeature1Id", dataLoan.sGseDeliveryFeature1Id);
            SetResult("sGseDeliveryFeature2Id", dataLoan.sGseDeliveryFeature2Id);
            SetResult("sGseDeliveryFeature3Id", dataLoan.sGseDeliveryFeature3Id);
            SetResult("sGseDeliveryFeature4Id", dataLoan.sGseDeliveryFeature4Id);
            SetResult("sGseDeliveryFeature5Id", dataLoan.sGseDeliveryFeature5Id);
            SetResult("sGseDeliveryFeature6Id", dataLoan.sGseDeliveryFeature6Id);
            SetResult("sGseDeliveryFeature7Id", dataLoan.sGseDeliveryFeature7Id);
            SetResult("sGseDeliveryFeature8Id", dataLoan.sGseDeliveryFeature8Id);
            SetResult("sGseDeliveryFeature9Id", dataLoan.sGseDeliveryFeature9Id);
            SetResult("sGseDeliveryFeature10Id", dataLoan.sGseDeliveryFeature10Id);
            SetResult("sGseDeliveryComments", dataLoan.sGseDeliveryComments);
            SetResult("sGseDeliveryServicingFPc", dataLoan.sGseDeliveryServicingFPc_rep);
            SetResult("sGseDeliveryServicingF", dataLoan.sGseDeliveryServicingF_rep);
            SetResult("sGseFreddieMacProductT", dataLoan.sGseFreddieMacProductT);
            SetResult("sGseDeliveryPayeeT", dataLoan.sGseDeliveryPayeeT);
        }
    }
    public partial class GSEDeliveryService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new GSEDeliveryServiceItem());
        }
    }
}
