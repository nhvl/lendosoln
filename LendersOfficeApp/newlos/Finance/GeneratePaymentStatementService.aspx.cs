﻿#region Generated Code -- A lie to dupe the merciless StyleCop.
namespace LendersOfficeApp.newlos.Finance
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LendersOffice.Pdf;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// The class containing the service methods for the Generate Payment Statement page.
    /// </summary>
    public partial class GeneratePaymentStatementServiceItem : AbstractBackgroundServiceItem
    {
        /// <summary>
        /// Executes the method associated with the given name.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.SavePaymentStatementToEdocs):
                    this.SavePaymentStatementToEdocs();
                    break;
                default:
                    throw new CBaseException("Unhandled service method.", "Unhandled service method.");
            }
        }

        /// <summary>
        /// Creates a CPageData object for this class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The CPageData object.</returns>
        protected override CPageData ConstructPageDataClass(Guid loanId)
        {
            return CPageData.CreateUsingSmartDependency(loanId, typeof(GeneratePaymentStatementServiceItem));
        }

        /// <summary>
        /// Saves payment statement pdf to EDocs, and returns the document id.
        /// </summary>
        protected void SavePaymentStatementToEdocs()
        {
            var loanId = this.sLId;
            var appId = this.aAppId;
            string fileDbKey = GetString("FileDbKey");
            int paymentIndex = GetInt("PaymentIndex");

            // We expect this doc type to always save. If somehow it incorrectly
            // does not have a value the user will get a system error.
            Guid documentId = FileDBTools.UseFile(E_FileDB.Normal, fileDbKey, (fileInfo) => AutoSaveDocTypeFactory.SavePdfDoc(
                E_AutoSavePage.PaymentStatement,
                fileInfo.FullName,
                PrincipalFactory.CurrentPrincipal.BrokerId,
                loanId,
                appId,
                userId: PrincipalFactory.CurrentPrincipal.UserId,
                description: "")).Value;

            this.SaveDocumentIdToPayment(paymentIndex, documentId);

            SetResult("DocumentId", documentId);
        }

        private void SaveDocumentIdToPayment(int paymentIndex, Guid documentId)
        {
            var loan = CPageData.CreateUsingSmartDependency(
                this.sLId,
                typeof(GeneratePaymentStatementServiceItem));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Due to how the servicing payments collection works, we will keep the entire collection
            // in memory, update a single record, and then set the entire collection back to the file.
            var payments = loan.sServicingPayments;
            var targetPayment = payments.ElementAtOrDefault(paymentIndex);
            if (targetPayment == null)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Unable to locate payment by index when attempting to save generated doc id to payment.");
            }

            targetPayment.PaymentStatementDocumentId = documentId;
            loan.sServicingPayments = payments;
            loan.Save();
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        /// <param name="dataApp">The application.</param>
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
        }

        /// <summary>
        /// Does nothing.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        /// <param name="dataApp">The application.</param>
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    /// <summary>
    /// The class to add the service item.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "I'm following the convention for adding service pages.")]
    public partial class GeneratePaymentStatementService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Adds the service item.
        /// </summary>
        protected override void Initialize()
        {
            this.AddBackgroundItem(string.Empty, new GeneratePaymentStatementServiceItem());
        }
    }
}