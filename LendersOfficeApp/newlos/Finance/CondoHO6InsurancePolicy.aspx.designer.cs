﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.5485
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LendersOfficeApp.newlos.Finance {
    
    
    public partial class CondoHO6InsurancePolicy {
        
        /// <summary>
        /// Head1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlHead Head1;
        
        /// <summary>
        /// CondoHO6Policy control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm CondoHO6Policy;
        
        /// <summary>
        /// CFM control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.newlos.Status.ContactFieldMapper CFM;
        
        /// <summary>
        /// sCondoHO6InsCompanyNm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sCondoHO6InsCompanyNm;
        
        /// <summary>
        /// sCondoHO6InsPolicyNum control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sCondoHO6InsPolicyNum;
        
        /// <summary>
        /// sCondoHO6InsPolicyActivationD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sCondoHO6InsPolicyActivationD;
        
        /// <summary>
        /// sCondoHO6InsPolicyExpirationD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sCondoHO6InsPolicyExpirationD;
        
        /// <summary>
        /// sCondoHO6InsPaidByT control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCondoHO6InsPaidByT;
        
        /// <summary>
        /// sCondoHO6InsCoverageAmt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCondoHO6InsCoverageAmt;
        
        /// <summary>
        /// sCondoHO6InsDeductibleAmt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCondoHO6InsDeductibleAmt;
        
        /// <summary>
        /// sCondoHO6InsSettlementChargeT control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCondoHO6InsSettlementChargeT;
        
        /// <summary>
        /// sCondoHO6InsPaymentDueAmt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sCondoHO6InsPaymentDueAmt;
        
        /// <summary>
        /// sCondoHO6InsPaymentDueAmtLckd control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sCondoHO6InsPaymentDueAmtLckd;
        
        /// <summary>
        /// sCondoHO6InsPolicyPaymentTypeT control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList sCondoHO6InsPolicyPaymentTypeT;
        
        /// <summary>
        /// sCondoHO6InsPaymentDueD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sCondoHO6InsPaymentDueD;
        
        /// <summary>
        /// sCondoHO6InsPaymentMadeD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sCondoHO6InsPaymentMadeD;
        
        /// <summary>
        /// sCondoHO6InsPaidThroughD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sCondoHO6InsPaidThroughD;
        
        /// <summary>
        /// sCondoHO6InsPolicyPayeeCode control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sCondoHO6InsPolicyPayeeCode;
        
        /// <summary>
        /// sCondoHO6InsPolicyLossPayeeTransD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sCondoHO6InsPolicyLossPayeeTransD;
        
        /// <summary>
        /// sCondoHO6InsCompanyId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sCondoHO6InsCompanyId;
    }
}
