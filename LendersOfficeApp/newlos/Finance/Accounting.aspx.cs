﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;

namespace LendersOfficeApp.newlos.Finance
{
    public partial class Accounting : BaseLoanPage
    {
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Accounting));
            dataLoan.InitLoad();

            //Funding
            sFundingCash.Text = dataLoan.sFundingCash_rep;
            sFundingPayables.Text = dataLoan.sFundingPayables_rep;
            sFundingReceivables.Text = dataLoan.sFundingReceivables_rep;
            sFundingLHS.Text = dataLoan.sFundingLHS_rep;
            
            //Servicing
            sServicingCash.Text = dataLoan.sServicingCash_rep;
            sServicingPayables.Text = dataLoan.sServicingPayables_rep;
            sServicingReceivables.Text = dataLoan.sServicingReceivables_rep;
            sServicingLHS.Text = dataLoan.sServicingLHS_rep;
            
            //Transactions            
            sTransactionsCash.Text = dataLoan.sTransactionsNetPmtsToDate_rep;            
            sTransactionsPayables.Text = dataLoan.sTransactionsTotPmtsPayable_rep;            
            sTransactionsReceivables.Text = dataLoan.sTransactionsTotPmtsReceivable_rep;
            sTransactionsLHS.Text = dataLoan.sTransactionsLHS_rep;

            //Commissions            
            sCommissionsCash.Text = dataLoan.sCommissionsCash_rep;
            sCommissionsPayables.Text = dataLoan.sCommissionsPayables_rep;
            sCommissionsReceivables.Text = dataLoan.sCommissionsReceivables_rep;
            sCommissionsLHS.Text = dataLoan.sCommissionsLHS_rep;
            
            //Trust Accounts            
            sTrustAccountCash.Text = dataLoan.sTrustBalance_rep;
            sTrustAccountPayables.Text = dataLoan.sTrustPayableTotal_rep;
            sTrustAccountReceivables.Text = dataLoan.sTrustReceivableTotal_rep;
            sTrustAccountLHS.Text = dataLoan.sTrustAccountLHS_rep;
            

            //Disbursement
            sDisbursementCash.Text = dataLoan.sDisbursementCash_rep;
            sDisbursementPayables.Text = dataLoan.sDisbursementPayables_rep;
            sDisbursementReceivables.Text = dataLoan.sDisbursementReceivables_rep;
            sDisbursementLHS.Text = dataLoan.sDisbursementLHS_rep;
            
            sTotalCash.Text = dataLoan.sTotalCash_rep;
            sTotalPayables.Text = dataLoan.sTotalPayables_rep;
            sTotalReceivables.Text = dataLoan.sTotalReceivables_rep;
            sTotalLHS.Text = dataLoan.sTotalLHS_rep;
            sTotalGainPercent.Text = dataLoan.sTotalGainPercent_rep;

            sInvestorLockProjectedProfit.Text = dataLoan.sInvestorLockProjectedProfit_rep;
            sInvestorLockProjectedProfitAmt.Text = dataLoan.sInvestorLockProjectedProfitAmt_rep;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Accounting";
            this.PageID = "Accounting";

            this.RegisterJsScript("loanframework2.js"); // Since UseNewFramework=false and we use the linkme js function, we need this
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = false; // There is no service page, so set this to false so that the smoke test won't choke on this page
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
