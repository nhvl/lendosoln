<%@ Page language="c#" Codebehind="FannieWaitingMessage.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FannieWaitingMessage" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>FannieWaitingMessage</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
  <body MS_POSITIONING="FlowLayout" onload="f_init();" bgcolor="gainsboro">
	<script language=javascript>
<!--
var g_iPollingInterval = 1000;
function f_init() {
  var args = getModalArgs() || {};
  var win = args.win;
  
  var sDuDoTitle = args.bIsDo ? "DO" : "DU";
  document.getElementById("MessageLabel").innerText = "LendingQB is waiting for the " + sDuDoTitle + " window to be closed to proceed.";
  if (win != null && !win.closed)
    win.focus();
  resize(300, 150);
  f_isWinOpen();
}
function f_isWinOpen() {
  var args = getModalArgs() || {};
  var win = args.win;
  if (null == win) {
    onClosePopup();
  } else {
    if (win.closed) {
      var arg = {};
      arg.OK = true;
      onClosePopup(arg);
    }
  }
  window.setTimeout(f_isWinOpen, g_iPollingInterval);
}
//-->
</script>

    <form id="FannieWaitingMessage" method="post" runat="server">
      <table>
        <tr><td style="font-weight:bold"><div id="MessageLabel"></div></td></tr>
      </table>
        <uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
     </form>
	
  </body>
</HTML>
