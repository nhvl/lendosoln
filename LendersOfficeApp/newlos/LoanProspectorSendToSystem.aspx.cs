namespace LendersOfficeApp.newlos
{
    using System;
    using global::DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Conversions.LoanProspector;
    using LendersOffice.Security;

    public partial class LoanProspectorSendToSystem : BasePage
    {
        private const string LpFormKey = "responseData";

        /// <remarks>
        /// 2017-02-11 TJ - This page is using Edge mode, which will cause typeof(document.all) === 'undefined', however, our
        /// IE only check requires typeof(document.all) === 'object'.  Per the HTML spec, however, any modern browser will
        /// return 'undefined'.  This property should be safe to remove after we've enabled IE in non-compatibility mode.
        /// (Tentatively planned for Spring/Summer 2017)
        /// </remarks>
        protected override bool EnableIEOnlyCheck
        {
            get { return false; }
        }

        [System.Web.Services.WebMethod]
        public static object Import(bool import1003Data, bool importLpFeedback, bool importCreditReport, string responseData)
        {
            try
            {
                // 2017-01-09 If the large request size here becomes an issue, we could easily switch responseData to load from the
                // auto-expire text cache.  This will also have the side effect that there will be a time limit on how long the page
                // can be used to import the data from LP, so Brian and I decided to hold off on this decision for now.
                var parsedServiceOrder = LoanProspectorResponseParser.Parse(responseData); // assume successful parsing, since the page load already checked that. If user manually changes it, in their face.
                LoanProspectorImporter importer = LoanProspectorImporter.CreateImporterForNonseamless(parsedServiceOrder, isImport1003: import1003Data, isImportFeedback: importLpFeedback, isImportCreditReport: importCreditReport, loanId: null); 
                importer.Import();
                return new { IsSuccessful = true };
            }
            catch (Exception exc)
            {
                if (exc is PageDataAccessDenied || exc is LoanFieldWritePermissionDenied)
                {
                    Tools.LogWarning(exc);
                }
                else
                {
                    Tools.LogError(exc);
                }

                return new { IsSuccessful = false, UserMessage = (exc as CBaseException)?.UserMessage ?? "Import failed. Please contact your LendingQB system administrator." };
            }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (this.Request.HttpMethod != "POST")
            {
                this.RegisterJsGlobalVariables("AutoClose", true);
                return;
            }

            string xml = this.Request.Form[LpFormKey];
            var loadedData = LoanProspectorHelper.LoadAssessmentData(xml);
            if (loadedData.IsSuccessful)
            {
                this.AssessmentSummaryData.DataSource = loadedData.AssessmentSummaryData;
                this.AssessmentSummaryData.DataBind();
                this.LpResponseData.Value = xml;// the footprint of a hidden field holding xml is less than the JS variable due to all the escaped characters
            }
            else
            {
                this.RegisterJsGlobalVariables("GeneralErrorMessage", loadedData.UserErrorMessage);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
