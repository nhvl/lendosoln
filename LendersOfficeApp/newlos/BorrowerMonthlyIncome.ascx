<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BorrowerMonthlyIncome.ascx.cs" Inherits="LendersOfficeApp.newlos.BorrowerMonthlyIncome" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@Import Namespace="LendersOffice.AntiXss"%>
<script type="text/javascript">
<!--
var bIsFirstLoad = true;
function _init() 
{
  if (bIsFirstLoad) 
  {
    <%= AspxTools.JsGetElementById(aBBaseI)%>.focus();
  }
  bIsFirstLoad = false;
  handleRentalILock();
  bindOtherIncomeTable();
}
function _postRefreshCalculation(results) {
  handleRentalILock();
}

function handleRentalILock()
{
  var b = <%= AspxTools.JsGetElementById(aNetRentI1003Lckd) %>.checked;

  <%= AspxTools.JsGetElementById(aBNetRentI1003) %>.readOnly = !b;
  <%= AspxTools.JsGetElementById(aCNetRentI1003) %>.readOnly = !b;
}
function f_incomeCalc(BClientId, CClientId, IncomeDesc) {
  var args = {
                aBName : <%= AspxTools.JsString(m_sBName)%>,
                aCName : <%= AspxTools.JsString(m_sCName)%>,
                aBBaseI : document.getElementById(BClientId).value,
                aCBaseI : document.getElementById(CClientId).value,
                aDescription : 'Monthly Income: ' + IncomeDesc
             };
  
  showModal('/newlos/Forms/IncomeCalculator.aspx', args, null, null, function(wargs){ 
	if (wargs.OK)
	{
		if (wargs.aBBaseI != null)
		{
			document.getElementById(BClientId).value = wargs.aBBaseI;
			document.getElementById(BClientId).focus();
		}
		if (wargs.aCBaseI != null)
		{
			document.getElementById(CClientId).value = wargs.aCBaseI;
			document.getElementById(CClientId).focus();
		}
		document.getElementById(BClientId).focus();
		
		if (typeof(refreshCalculation) == 'function')
		refreshCalculation();
		
		updateDirtyBit();
	}
  },{ hideCloseButton: true });
}


function checkForSubsequentBlanks()
{
    // 8/20/2013 EM - If there are only 3 rows, and a row has all subsequent rows as blanks, disable the delete
    var jsonStr = document.getElementById('<%=AspxTools.ClientId(OtherIncomeJson)%>');
    var data = $j.parseJSON( jsonStr.value);
    
    if(data.length == 3)
    {
        //work backwards from the last index
        for(var i = 2; i >= 0; i--)
        {
            if(data[i].Desc == "" && (data[i].Amount == "" || data[i].Amount == "0.00"))
            {
				var deleteA = document.getElementById("delete_"+i);
				if (deleteA != null) {
					deleteA.removeAttribute("href");
					setDisabledAttr(deleteA, true);
				}
            }
            else
                break;
        }
    }
}


function bindOtherIncomeTable()
{
    var isReadOnly = <%=AspxTools.JsBool(IsReadOnly) %>;
    var data = $j.parseJSON( $j('#<%=AspxTools.ClientId(OtherIncomeJson)%>').val());
	var h = hypescriptDom;
	
    $j("#OtherIncomeContent").empty().append(
		$j.map(data, function(item, i) {
			return h("tr", {className:"IncomeRow"}, [
				h("td", {}, h("select", {id:("s_"+i), onchange:onChangeOtherIncome, disabled:isReadOnly}, [
					h("option", {value:"B", selected:!item.IsForCoBorrower}, "B"),
					h("option", {value:"C", selected: item.IsForCoBorrower}, "C")
				])),
				h("td", {}, [
					h("input", {type:"text", id:("d_"+i), value:item.Desc, style:"width:430px", maxLength:100, 
								onkeydown:function(event){onComboboxKeypress(this, event)}, 
								onchange:onChangeOtherIncome, readOnly:isReadOnly }),
					h("img", {id:("i_"+i), src:(VRoot + "/images/dropdown_arrow.gif"), className:"combobox_img", align:"absbottom"})
				]),
				h("td", {}, h("input", {type:"text", id:("a_"+i), value:item.Amount, attrs:{preset:"money"}, style:"width:90px", onchange:onChangeOtherIncome, readOnly:isReadOnly})),
				h("td", {}, ((item.Desc == "" && (item.Amount == "" || item.Amount == "0.00"))
					? h("a", {id:("delete_"+i), href:"javascript:void(0);", onClick:function(){onDeleteClick(i)}}, " delete ")
					: h("a", {id:("delete_"+i), attrs:{disabled:"disabled"}}, " delete "))),
			])
		})
	);
    
    $j("#OtherIncomeContent").find("select").each( function() { 
		attachCommonEvents(this, false); 

		if ( isReadOnly )
			disableDDL(this, true);
    });
    
    $j("#OtherIncomeContent").find("input").each( function() { 
        attachCommonEvents(this, false); 
        
        if( $j(this).attr("preset") == "money")
        {
            format_money(this);
            _initMask(this);  
        }
       if ( isReadOnly )
        formatReadonlyField(this); 
    });
    
    $j('#<%=AspxTools.ClientId(OtherIncomeCombo)%>_cbl').attr('shared_combo', 'shared_combo');
    $j("#OtherIncomeContent").find(".combobox_img").each( function() { 
    
        attachCommonEvents(this, false);
        
        if ( isReadOnly )
            formatReadonlyField(this);
        else
        {
            $j(this).bind("click", function() { 
            
                var thisId = $j(this).attr('id');
                thisId = thisId.replace('i','d'); 
                $j('table[shared_combo]').attr('id', thisId + '_cbl');
                onSelect( thisId );  
            }); 
        }
    });
    checkForSubsequentBlanks()
}

function onDeleteClick(index)
{
    var jsonStr = document.getElementById('<%=AspxTools.ClientId(OtherIncomeJson)%>');
    var data = $j.parseJSON( jsonStr.value);
    var dataNew = Array();
    for(var i = 0; i < data.length; i++)
    {
        if(i != index)
            dataNew.push(data[i]);
    }
    jsonStr.value = JSON.stringify(dataNew);
    bindOtherIncomeTable();
    updateDirtyBit();
    //subtract by to ensure you have atleast three since you're deleting an entry as well
    if(data.length - 4 < 0)
        onAddMoreIncomeClick();
    
}

function onAddMoreIncomeClick()
{
    calculateOtherIncomeJson();

    var jsonStr = document.getElementById('<%=AspxTools.ClientId(OtherIncomeJson)%>');
    var data = $j.parseJSON( jsonStr.value);
    data.push({"Amount" : 0.00, "Desc":"", "IsForCoBorrower":false});
    jsonStr.value = JSON.stringify(data);
    bindOtherIncomeTable();
    updateDirtyBit();
}

function calculateOtherIncomeJson()
{
    var jsonStr = document.getElementById('<%=AspxTools.ClientId(OtherIncomeJson)%>');
    var data = new Array();

    var count = 0;
    $j("#OtherIncomeContent").children("tr").each( function() {
    
    var isForCoborrower = $j('#s_' + count).val() == 'C';
    var desc =  $j('#d_' + count).val();
    
    format_money( $j('#a_' + count)[0] );
    var amt =  $j('#a_' + count).val().replace('$','').replace(/,/g,'');
    if (amt == '') amt = '0';
    if (amt.indexOf( "(",0) == 0)
    {
        amt = amt.replace("(","-");
        amt = amt.replace(")","");
    }
    
    data.push({"Amount" : amt, "Desc":desc, "IsForCoBorrower":isForCoborrower});
    
    count++;
    });
    
    jsonStr.value = JSON.stringify(data);
}

function onChangeOtherIncome()
{
    calculateOtherIncomeJson();
    refreshCalculation();
}


//-->
</script>
<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0" class="InsetBorder">
	<TR>
		<TD>
			<TABLE id="Table2" cellSpacing="0" cellPadding="3" border="0">
				<TR>
					<TD noWrap></TD>
					<TD class="FieldLabel" noWrap>Borrower</TD>
					<TD class="FieldLabel" noWrap>Co-borrower</TD>
					<TD class="FieldLabel" noWrap>Total</TD>
				</TR>
				<TR>
				    <td class="FieldLabel" noWrap><a href="#" onclick="f_incomeCalc(<%= AspxTools.JsString(aBBaseI.ClientID)%>, <%= AspxTools.JsString(aCBaseI.ClientID)%>, 'Base Income');">Base Income</a></td>					
					<TD noWrap><ml:moneytextbox id="aBBaseI" runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD>
					<TD noWrap><ml:moneytextbox id="aCBaseI" runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD>
					<TD noWrap><ml:moneytextbox id="aTotBaseI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
				</TR>
				<TR>					
					<td class="FieldLabel" noWrap><a href="#" onclick="f_incomeCalc(<%= AspxTools.JsString(aBOvertimeI.ClientID)%>, <%=AspxTools.JsString(aCOvertimeI.ClientID)%>, 'Overtime');">Overtime</a></td>
					<TD noWrap><ml:moneytextbox id="aBOvertimeI" runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD>
					<TD noWrap><ml:moneytextbox id="aCOvertimeI" runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD>
					<TD noWrap><ml:moneytextbox id="aTotOvertimeI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
				</TR>
				<TR>					
					<td class="FieldLabel" noWrap><a href="#" onclick="f_incomeCalc(<%= AspxTools.JsString(aBBonusesI.ClientID)%>, <%= AspxTools.JsString(aCBonusesI.ClientID)%>, 'Bonuses');">Bonuses</a></td>
					<TD noWrap><ml:moneytextbox id="aBBonusesI" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
					<TD noWrap><ml:moneytextbox id="aCBonusesI" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
					<TD noWrap><ml:moneytextbox id="aTotBonusesI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
				</TR>
				<TR>					
					<TD class="FieldLabel" noWrap><a href="#" onclick="f_incomeCalc(<%= AspxTools.JsString(aBCommisionI.ClientID)%>, <%= AspxTools.JsString(aCCommisionI.ClientID)%>, 'Commission');">Commission</a></TD>
					<TD noWrap><ml:moneytextbox id="aBCommisionI" runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD>
					<TD noWrap><ml:moneytextbox id="aCCommisionI" runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD>
					<TD noWrap><ml:moneytextbox id="aTotCommisionI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
				</TR>
				<TR>					
					<TD class="FieldLabel" noWrap><a href="#" onclick="f_incomeCalc(<%= AspxTools.JsString(aBDividendI.ClientID)%>, <%= AspxTools.JsString(aCDividendI.ClientID)%>, 'Dividends/Interest');">Dividends/Interest</a></TD>
					<TD noWrap><ml:moneytextbox id="aBDividendI" runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD>
					<TD noWrap><ml:moneytextbox id="aCDividendI" runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD>
					<TD noWrap><ml:moneytextbox id="aTotDividendI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
				</TR>
				<TR>
					<TD class="FieldLabel" noWrap>
					    <A onclick="redirectToUladPage('REO');" title="Go to Real Estate Schedule">Net Rent:</A>
					    &nbsp;&nbsp;&nbsp;&nbsp;
						<asp:checkbox id="aNetRentI1003Lckd" onclick="refreshCalculation();" runat="server" Text="lock"></asp:checkbox></TD>
					<TD noWrap><ml:moneytextbox id="aBNetRentI1003" runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD>
					<TD noWrap><ml:moneytextbox id="aCNetRentI1003" runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD>
					<TD noWrap><ml:moneytextbox id="aTotNetRentI1003" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
				</TR>
				<tr>
				    <td class="FieldLabel" noWrap>Subject Net Cash</td>
				    <TD noWrap><ml:moneytextbox id="aBSpPosCf" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
					<TD noWrap><ml:moneytextbox id="aCSpPosCf" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
					<TD noWrap><ml:moneytextbox id="aTotSpPosCf" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
				</tr>
				<TR title="(see 'Describe Other Income' below)">
					<TD class="FieldLabel" noWrap>Other Income:</TD>
					<TD noWrap><ml:moneytextbox id="aBTotOI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
					<TD noWrap><ml:moneytextbox id="aCTotOI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
					<TD noWrap><ml:moneytextbox id="aTotOI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
				</TR>
				<TR>
					<TD class="FieldLabel" noWrap>Application Total:</TD>
					<TD noWrap><ml:moneytextbox id="aBTotI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
					<TD noWrap><ml:moneytextbox id="aCTotI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
					<TD noWrap><ml:moneytextbox id="aTotI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
	
	
	<TR>
		<TD class="FormTableSubheader">Describe Other Income</TD>
	</TR>
	<TR>
		<TD><input type="hidden" id="OtherIncomeJson" value="" runat ="server" />

						<TABLE id="Table13" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel">B/C</TD>
								<TD class="FieldLabel">Description</TD>
								<TD class="FieldLabel" noWrap>Monthly Income</TD>
							</TR>
							<tbody id="OtherIncomeContent">
							</tbody>
							<tr><td colspan='3'><input type=button onclick="onAddMoreIncomeClick()" value="Add More" /></td></tr>
						</TABLE>
						<span style="display:none" ><ml:ComboBox runat="server" id="OtherIncomeCombo" style="display:none" /></span>
		

		</TD>
	</TR>
	
	
	
	<TR>
		<TD class="FieldLabel">Total monthly income of all borrowers
			<ml:moneytextbox id="sLTotI" runat="server" preset="money" width="90" ReadOnly="True" /></TD>
	</TR>
	<TR>
		<TD></TD>
	</TR>
</TABLE>


