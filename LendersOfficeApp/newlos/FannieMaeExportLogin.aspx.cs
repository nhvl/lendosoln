using System;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib.ServiceCredential;

namespace LendersOfficeApp.newlos
{

    public partial class FannieMaeExportLogin : LendersOfficeApp.newlos.BaseLoanPage
    {
        protected string m_doDuHeader
        {
            get { return m_isDo ? "Desktop Originator" : "Desktop Underwriter"; }
        }
        protected bool m_isDo
        {
            get { return RequestHelper.GetSafeQueryString("isdo") == "t"; }
        }
        protected bool m_hasAutoLoginInformation
        {
            get;
            set;
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FannieMaeExportLogin));
            dataLoan.InitLoad();
            sDuCaseId.Text = dataLoan.sDuCaseId;
            sDuLenderInstitutionId.Text = dataLoan.sDuLenderInstitutionId;

            if (m_isDo)
            {
                this.m_hasAutoLoginInformation = BrokerUser.HasDoAutoLogin;
                FannieMaeMORNETUserID.Text = BrokerUser.HasDoAutoLogin ? BrokerUser.DoAutoLoginNm : BrokerUser.DoLastLoginNm;
            }
            else
            {
                if (BrokerUser.BrokerDB.UsingLegacyDUCredentials)
                {
                    this.m_hasAutoLoginInformation = BrokerUser.HasDuAutoLogin;
                    FannieMaeMORNETUserID.Text = BrokerUser.HasDuAutoLogin ? BrokerUser.DuAutoLoginNm : BrokerUser.DuLastLoginNm;
                }
                else
                {
                    var savedCredential = ServiceCredential.ListAvailableServiceCredentials(BrokerUser, dataLoan.sBranchId, ServiceCredentialService.AusSubmission)
                                            .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter && credential.IsEnabledForNonSeamlessDu);
                    if (savedCredential != null)
                    {
                        this.m_hasAutoLoginInformation = true;
                        FannieMaeMORNETUserID.Text = savedCredential.UserName;
                    }
                }
            }

            m_rememberLoginCB.Visible = BrokerUser.BrokerDB.UsingLegacyDUCredentials;
            m_rememberLoginCB.Checked = FannieMaeMORNETUserID.Text != "";
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.DisplayCopyRight = false;
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
