namespace LendersOfficeApp.newlos
{
using System;
    using System.Collections.Generic;
    using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
    using LendersOffice.Drivers.FileSystem;
    using LendersOffice.Drivers.OpenXmlDocument;
    using LendersOffice.ObjLib.CustomMergeFields;
using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Downloads a custom form.
    /// </summary>
    public partial class DownloadCustomForm : BasePage
    {
        /// <summary>
        /// The current user downloading the form.
        /// </summary>
        private BrokerUserPrincipal CurrentUser => BrokerUserPrincipal.CurrentPrincipal;

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is unused.
        /// </param>
        /// <param name="e">
        /// The parameter is unused.
        /// </param>
        protected void PageLoad(object sender, EventArgs e)
        {
            var customFormId = RequestHelper.GetGuid("id");
            var fileDbKeyTitlePair = Tools.GetCustomFormInfo(this.CurrentUser.BrokerId, customFormId);

            var tempFilePath = FileDBTools.CreateCopy(E_FileDB.Normal, fileDbKeyTitlePair.Key.ToString());
            var localFilePath = LocalFilePath.Create(tempFilePath).ForceValue();

            this.SetValuesForDocument(localFilePath);
            this.DownloadFile(localFilePath, fileDbKeyTitlePair.Value);
        }

        /// <summary>
        /// Sets values for the document to download.
        /// </summary>
        /// <param name="localFilePath">
        /// The path to the document.
        /// </param>
        private void SetValuesForDocument(LocalFilePath localFilePath)
        {
            var useLiveData = RequestHelper.GetInt("printoptions") == 0;

            var fieldIdsByWordCode = CustomMergeFieldRepository.FieldIdsByWordCode;
            var fieldNameValueDict = new Dictionary<string, string>();

            if (useLiveData)
        {
                var loan = new CCustomFormData(RequestHelper.LoanID);
                loan.InitLoad();
            
                var app = loan.GetAppData(RequestHelper.GetGuid("appid"));

                // 10/20/2004 dd - Reason I need to retrieve Loan Officer info here is because
                // I need to get Address,City,State,Zip,ActualBrokerName for PML User Info.
                EmployeeDB loanOfficer = null;
                if (loan.sEmployeeLoanRepId != Guid.Empty)
            {
                    loanOfficer = EmployeeDB.RetrieveById(loan.sBrokerId, loan.sEmployeeLoanRepId);
            }

                foreach (var formField in fieldIdsByWordCode)
            {
                    fieldNameValueDict[formField.Key] = this.GetData(formField.Value, loan, app, loanOfficer);
            } 
            }
            else 
            {
                foreach (var formField in fieldIdsByWordCode)
                {
                    fieldNameValueDict[formField.Key] = string.Empty;
            }
            }

            OpenXmlDocumentDriverHelper.SetTextFieldsOnDocument(localFilePath, fieldNameValueDict);
        }

        /// <summary>
        /// Downloads the file, sending it to the client.
        /// </summary>
        private void DownloadFile(LocalFilePath localFilePath, string title)
        {
            var escapedTitle = $"\"{Tools.EscapeInvalidFilenameCharacters(title)}.docx\"";
            RequestHelper.SendFileToClient(this.Context, localFilePath.Value, "application/octet-stream", escapedTitle);
            this.Context.Response.SuppressContent = true;
            this.Context.ApplicationInstance.CompleteRequest();
            FileOperationHelper.Delete(localFilePath);
            }

        /// <summary>
        /// Gets data for the specified custom form code.
        /// </summary>
        /// <param name="code">
        /// The custom form code.
        /// </param>
        /// <param name="dataLoan">
        /// The loan to load data.
        /// </param>
        /// <param name="loanOfficer">
        /// 
        /// </param>
        /// <returns></returns>
        private string GetData(string code, CPageData dataLoan, CAppData dataApp, EmployeeDB loanOfficer)
        {
            if (!LoanFieldSecurityManager.CanReadField(code))
            {
                // 3/22/2010 dd - If user does not have permission return restricted string.
                return "Restricted";
            }

            string ret = null, part = null;
            var lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            var borEmp = dataApp.aBEmpCollection.GetPrimaryEmp(false);
            var coBorEmp = dataApp.aCEmpCollection.GetPrimaryEmp(false);

            switch (code) 
            {
                case "aBFirstNm": ret = dataApp.aBFirstNm; break;
                case "aBMidNm": ret = dataApp.aBMidNm; break;
                case "aBLastNm": ret = dataApp.aBLastNm; break;
                case "aBSuffix": ret = dataApp.aBSuffix; break;
                case "aBNm": ret = dataApp.aBNm; break;
                case "aBSsn": ret = dataApp.aBSsn; break;
                case "aBDob": ret = dataApp.aBDob_rep; break;
                case "aBHPhone": ret = dataApp.aBHPhone; break;
                case "aBBusPhone": ret = dataApp.aBBusPhone; break;
                case "aBCellPhone": ret = dataApp.aBCellPhone; break;
                case "aBEmail": ret = dataApp.aBEmail; break;
                case "aBAddr": ret = dataApp.aBAddr; break;
                case "aBCity": ret = dataApp.aBCity; break;
                case "aBState": ret = dataApp.aBState; break;
                case "aBZip": ret = dataApp.aBZip; break;

                case "aCFirstNm": ret = dataApp.aCFirstNm; break;
                case "aCMidNm": ret = dataApp.aCMidNm; break;
                case "aCLastNm": ret = dataApp.aCLastNm; break;
                case "aCSuffix": ret = dataApp.aCSuffix; break;
                case "aCNm": ret = dataApp.aCNm; break;
                case "aCSsn": ret = dataApp.aCSsn; break;
                case "aCDob": ret = dataApp.aCDob_rep; break;
                case "aCHPhone": ret = dataApp.aCHPhone; break;
                case "aCBusPhone": ret = dataApp.aCBusPhone; break;
                case "aCCellPhone": ret = dataApp.aCCellPhone; break;
                case "aCEmail": ret = dataApp.aCEmail; break;

                case "sSpAddr": ret = dataLoan.sSpAddr; break;
                case "sSpCity": ret = dataLoan.sSpCity; break;
                case "sSpState": ret = dataLoan.sSpState; break;
                case "sSpZip": ret = dataLoan.sSpZip; break;
                case "sSpCounty": ret = dataLoan.sSpCounty; break;
                case "sLNm": ret = dataLoan.sLNm; break;
                case "sLpTemplateNm": ret = dataLoan.sLpTemplateNm; break;

                case "sLOrigFPc": ret = dataLoan.sLOrigFPc_rep; break;
                case "sLOrigFMb": ret = dataLoan.sLOrigFMb_rep; break;
                case "sLOrigF": ret = dataLoan.sLOrigF_rep; break;
                case "sLDiscntPc": ret = dataLoan.sLDiscntPc_rep; break;
                case "sLDiscnt": ret = dataLoan.sLDiscnt_rep; break;
                case "sLDiscntFMb": ret = dataLoan.sLDiscntFMb_rep; break;

                case "sLPurposeT": ret = dataLoan.sLPurposeT_rep; break;
                case "sLienPosT": ret = dataLoan.sLienPosT.ToString(); break; 
                case "sLT": ret = dataLoan.sLT_rep; break;
                case "sApprVal": ret = dataLoan.sApprVal_rep; break;
                case "sPurchPrice": ret = dataLoan.sPurchPrice_rep; break;
                case "sEquity": ret = dataLoan.sEquityCalc_rep; break;
                case "sLAmt": ret = dataLoan.sLAmtCalc_rep; break;
				case "sFinalLAmt": ret = dataLoan.sFinalLAmt_rep; break;
                case "sTerm": ret = dataLoan.sTerm_rep; break;
                case "sNoteIR": ret = dataLoan.sNoteIR_rep; break;
                case "sQualIR": ret = dataLoan.sQualIR_rep; break;
                case "sDue": ret = dataLoan.sDue_rep; break;
                case "sGseSpT": 
                    E_sGseSpT e = dataLoan.sGseSpT;
                switch (e) 
                {
                    case E_sGseSpT.Attached:
                    case E_sGseSpT.Condominium:
                    case E_sGseSpT.Cooperative:
                    case E_sGseSpT.Detached:
                    case E_sGseSpT.Modular:
                    case E_sGseSpT.PUD:
                        ret = e.ToString();
                        break;
                    case E_sGseSpT.DetachedCondominium:
                        ret = "Detached Condominium";
                        break;
                    case E_sGseSpT.HighRiseCondominium:
                        ret = "High Rise Condominium";
                        break;
                    case E_sGseSpT.ManufacturedHomeCondominium:
                        ret = "Manufactured Home Condominium";
                        break;
                    case E_sGseSpT.ManufacturedHomeMultiwide:
                        ret = "Manufactured Home Multiwide";
                        break;
                    case E_sGseSpT.ManufacturedHousing:
                        ret = "Manufactured Housing";
                        break;
                    case E_sGseSpT.ManufacturedHousingSingleWide:
                        ret = "Manufactured Housing Single wide";
                        break;
                    default:
                        ret = "";
                        break;
                };
                    break;
                case "sLtvR": ret = dataLoan.sLtvR_rep; break;
                case "sCltvR": ret = dataLoan.sCltvR_rep; break;
                case "sHcltvR": ret = dataLoan.sHcltvR_rep; break;
                case "sQualTopR": ret = dataLoan.sQualTopR_rep; break;
                case "sQualBottomR": ret = dataLoan.sQualBottomR_rep; break;
                case "sMonthlyPmt": ret = dataLoan.sMonthlyPmt_rep; break;

                case "sEmployeeLoanRepFirstName": ret = dataLoan.sEmployeeLoanRep.FirstName; break; // 10/20/2004 dd - Added
                case "sEmployeeLoanRepLastName": ret = dataLoan.sEmployeeLoanRep.LastName; break; // 10/20/2004 dd - Added
                case "sEmployeeLoanRepName": ret = dataLoan.sEmployeeLoanRepName; break;
                case "sEmployeeLoanRepEmail": ret = dataLoan.sEmployeeLoanRepEmail; break;
                case "sEmployeeLoanRepPhone": ret = dataLoan.sEmployeeLoanRepPhone; break;
                case "sEmployeeLoanRepFax": ret = dataLoan.sEmployeeLoanRep.Fax; break; // 10/20/2004 dd - Added
                case "sEmployeeLoanRepAddress": // 10/20/2004 dd - Added
                    if (null != loanOfficer)
                        ret = loanOfficer.Address.StreetAddress;
                    else
                        ret = "";
                    break;
                case "sEmployeeLoanRepCity": // 10/20/2004 dd - Added
                    if (null != loanOfficer)
                        ret = loanOfficer.Address.City;
                    else
                        ret = "";
                    break;
                case "sEmployeeLoanRepState": // 10/20/2004 dd - Added
                    if (null != loanOfficer)
                        ret = loanOfficer.Address.State;
                    else
                        ret = "";
                    break;
                case "sEmployeeLoanRepZipcode": // 10/20/2004 dd - Added
                    if (null != loanOfficer)
                        ret = loanOfficer.Address.Zipcode;
                    else
                        ret = "";
                    break;
                case "sEmployeeLoanRepActualBrokerName": // 10/20/2004 dd - Added
                    if (null != loanOfficer)
                        ret = dataLoan.sEmployeeLoanRepPmlUserBrokerNm;
                    else
                        ret = "";
                    break;
                case "sEmployeeProcessorName": ret = dataLoan.sEmployeeProcessorName; break;
                case "sEmployeeProcessorEmail": ret = dataLoan.sEmployeeProcessorEmail; break;
                case "sEmployeeProcessorPhone": ret = dataLoan.sEmployeeProcessorPhone; break;
                case "sEmployeeProcessorFax": ret = dataLoan.sEmployeeProcessor.Fax; break;
                case "sEmployeeLenderAccExecName": ret = dataLoan.sEmployeeLenderAccExecName; break;
                case "sEmployeeLenderAccExecEmail": ret = dataLoan.sEmployeeLenderAccExecEmail; break;
                case "sEmployeeLenderAccExecPhone": ret = dataLoan.sEmployeeLenderAccExecPhone; break;
                case "sEmployeeLenderAccExecFax": ret = dataLoan.sEmployeeLenderAccExec.Fax; break;

                case "sOpenedD": ret = dataLoan.sOpenedD_rep; break;
                case "sEstCloseD": ret = dataLoan.sEstCloseD_rep; break;
                case "sClosedD": ret = dataLoan.sClosedD_rep; break;
                case "sRLckdD": ret = dataLoan.sRLckdD_rep; break;
                case "sRLckdExpiredD": ret = dataLoan.sRLckdExpiredD_rep; break;
                case "sAgentLenderName": ret = lender.CompanyName; break;
                case "sAgentLenderAddress": ret = lender.StreetAddr; break;
                case "sAgentLenderCity": ret = lender.City; break;
                case "sAgentLenderState": ret = lender.State; break;
                case "sAgentLenderZipcode": ret = lender.Zip; break;
                case "sAgentLenderPhone": ret = lender.PhoneOfCompany; break;

                case "sUnitsNum": ret = dataLoan.sUnitsNum_rep; break;
                case "sYrBuilt": ret = dataLoan.sYrBuilt; break;
                case "sProfitRebate": ret = dataLoan.sProfitRebate_rep; break;
                case "sGrossProfit": ret = dataLoan.sGrossProfit_rep; break;
                case "sCommissionTotal": ret = dataLoan.sCommissionTotal_rep; break;
                case "sNetProfit": ret = dataLoan.sNetProfit_rep; break;
                case "sPrelimRprtOd": ret = dataLoan.sPrelimRprtOd_rep; break;   
                case "sApprRprtOd": ret = dataLoan.sApprRprtOd_rep; break;
                case "sPreQualD": ret = dataLoan.sPreQualD_rep; break;
                case "sPreApprovD": ret = dataLoan.sPreApprovD_rep; break;
                case "sSubmitD": ret = dataLoan.sSubmitD_rep; break;
                case "sApprovD": ret = dataLoan.sApprovD_rep; break;
                case "sDocsD": ret = dataLoan.sDocsD_rep; break;
                case "sFundD": ret = dataLoan.sFundD_rep; break;
                case "sRecordedD": ret = dataLoan.sRecordedD_rep; break;
                case "sOnHoldD": ret = dataLoan.sOnHoldD_rep; break;
                case "sCanceledD": ret = dataLoan.sCanceledD_rep; break;
                case "sRejectD": ret = dataLoan.sRejectD_rep; break;
                case "sSuspendedD": ret = dataLoan.sSuspendedD_rep; break;

                case "sDemandYieldSpreadPremiumLenderAmt": ret = dataLoan.sDemandYieldSpreadPremiumLenderAmt_rep; break;
                case "sDemandYieldSpreadPremiumBrokerAmt": ret = dataLoan.sDemandYieldSpreadPremiumBrokerAmt_rep; break;
                    #region OPM 2722 - Added 8/23 
                case "sApprF": ret = dataLoan.sApprF_rep; break;
                case "sCrF": ret = dataLoan.sCrF_rep; break;
                case "sMBrokF": ret = dataLoan.sMBrokF_rep; break;
                case "sProcF": ret = dataLoan.sProcF_rep; break;
                case "sUwF": ret = dataLoan.sUwF_rep; break;
                case "s800U1F": ret = dataLoan.s800U1F_rep; break;
                case "s800U2F": ret = dataLoan.s800U2F_rep; break;
                case "s800U3F": ret = dataLoan.s800U3F_rep; break;
                case "s800U4F": ret = dataLoan.s800U4F_rep; break;
                case "s800U5F": ret = dataLoan.s800U5F_rep; break;
                case "sU1Sc": ret = dataLoan.sU1Sc_rep; break;
                case "sU2Sc": ret = dataLoan.sU2Sc_rep; break;                
                case "sU3Sc": ret = dataLoan.sU3Sc_rep; break;
                case "sU4Sc": ret = dataLoan.sU4Sc_rep; break;
                case "sU5Sc": ret = dataLoan.sU5Sc_rep; break;
                case "sTransNetCash": ret = dataLoan.sTransNetCash_rep; break;
                case "sRAdjMarginR": ret = dataLoan.sRAdjMarginR_rep; break;
                case "sBrokComp1": ret = dataLoan.sBrokComp1_rep; break;
                case "aOccT": 
                    ret = "";
                    switch (dataApp.aOccT) 
                    {
                        case E_aOccT.PrimaryResidence:
                            ret = "Primary Residence"; break;
                        case E_aOccT.SecondaryResidence:
                            ret = "Secondary Residence"; break;
                        case E_aOccT.Investment:
                            ret = "Investment"; break;
                    }
                    break;

                    #endregion


                #region Loan Related Companies Merge Fields
                case "Appraiser_Company_Name":
                case "Appraiser_Company_Address":
                case "Appraiser_Company_City":
                case "Appraiser_Company_State":
                case "Appraiser_Company_Zip":
                case "Appraiser_Company_Phone":
                case "Appraiser_Company_Fax":
                case "Appraiser_Contact_Name":
                case "Appraiser_Contact_Phone":
                case "Appraiser_Case_Number":
                    part = code.Substring(10);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
                case "ClosingAgent_Company_Name":
                case "ClosingAgent_Company_Address":
                case "ClosingAgent_Company_City":
                case "ClosingAgent_Company_State":
                case "ClosingAgent_Company_Zip":
                case "ClosingAgent_Company_Phone":
                case "ClosingAgent_Company_Fax":
                case "ClosingAgent_Contact_Name":
                case "ClosingAgent_Contact_Phone":
                case "ClosingAgent_Case_Number":
                    part = code.Substring(13);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.ClosingAgent, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
                case "Escrow_Company_Name":
                case "Escrow_Company_Address":
                case "Escrow_Company_City":
                case "Escrow_Company_State":
                case "Escrow_Company_Zip":
                case "Escrow_Company_Phone":
                case "Escrow_Company_Fax":
                case "Escrow_Contact_Name":
                case "Escrow_Contact_Phone":
                case "Escrow_Case_Number":
                    part = code.Substring(7);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.Escrow, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
                case "Title_Company_Name":
                case "Title_Company_Address":
                case "Title_Company_City":
                case "Title_Company_State":
                case "Title_Company_Zip":
                case "Title_Company_Phone":
                case "Title_Company_Fax":
                case "Title_Contact_Name":
                case "Title_Contact_Phone":
                case "Title_Case_Number":
                    part = code.Substring(6);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
                case "ListingAgent_Company_Name":
                case "ListingAgent_Company_Address":
                case "ListingAgent_Company_City":
                case "ListingAgent_Company_State":
                case "ListingAgent_Company_Zip":
                case "ListingAgent_Company_Phone":
                case "ListingAgent_Company_Fax":
                case "ListingAgent_Contact_Name":
                case "ListingAgent_Contact_Phone":
                case "ListingAgent_Case_Number":
                    part = code.Substring(13);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.ListingAgent, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
                case "Servicing_Company_Name":
                case "Servicing_Company_Address":
                case "Servicing_Company_City":
                case "Servicing_Company_State":
                case "Servicing_Company_Zip":
                case "Servicing_Company_Phone":
                case "Servicing_Company_Fax":
                case "Servicing_Contact_Name":
                case "Servicing_Contact_Phone":
                case "Servicing_Case_Number":
                    part = code.Substring(10);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.Servicing, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
                case "HazardInsurance_Company_Name":
                case "HazardInsurance_Company_Address":
                case "HazardInsurance_Company_City":
                case "HazardInsurance_Company_State":
                case "HazardInsurance_Company_Zip":
                case "HazardInsurance_Company_Phone":
                case "HazardInsurance_Company_Fax":
                case "HazardInsurance_Contact_Name":
                case "HazardInsurance_Contact_Phone":
                case "HazardInsurance_Case_Number":
                    part = code.Substring(16);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.HazardInsurance, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;

                case "Realtor_Company_Name":
                case "Realtor_Company_Address":
                case "Realtor_Company_City":
                case "Realtor_Company_State":
                case "Realtor_Company_Zip":
                case "Realtor_Company_Phone":
                case "Realtor_Company_Fax":
                case "Realtor_Contact_Name":
                case "Realtor_Contact_Phone":
                case "Realtor_Case_Number":
                    part = code.Substring(8);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.Realtor, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
                case "Seller_Company_Name":
                case "Seller_Company_Address":
                case "Seller_Company_City":
                case "Seller_Company_State":
                case "Seller_Company_Zip":
                case "Seller_Company_Phone":
                case "Seller_Company_Fax":
                case "Seller_Contact_Name":
                case "Seller_Contact_Phone":
                case "Seller_Case_Number":
                    part = code.Substring(7);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.Seller, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
                case "SellingAgent_Company_Name":
                case "SellingAgent_Company_Address":
                case "SellingAgent_Company_City":
                case "SellingAgent_Company_State":
                case "SellingAgent_Company_Zip":
                case "SellingAgent_Company_Phone":
                case "SellingAgent_Company_Fax":
                case "SellingAgent_Contact_Name":
                case "SellingAgent_Contact_Phone":
                case "SellingAgent_Case_Number":
                    part = code.Substring(13);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.SellingAgent, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
                case "Broker_Company_Name":
                case "Broker_Company_Address":
                case "Broker_Company_City":
                case "Broker_Company_State":
                case "Broker_Company_Zip":
                case "Broker_Company_Phone":
                case "Broker_Company_Fax":
                case "Broker_Contact_Name":
                case "Broker_Contact_Phone":
                case "Broker_Case_Number":
                    part = code.Substring(7);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;

                case "Investor_Company_Name":
                case "Investor_Company_Address":
                case "Investor_Company_City":
                case "Investor_Company_State":
                case "Investor_Company_Zip":
                case "Investor_Company_Phone":
                case "Investor_Company_Fax":
                case "Investor_Contact_Name":
                case "Investor_Contact_Phone":
                case "Investor_Case_Number":
                    part = code.Substring(9);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.Investor, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
                case "OtherAgent_Company_Name":
                case "OtherAgent_Company_Address":
                case "OtherAgent_Company_City":
                case "OtherAgent_Company_State":
                case "OtherAgent_Company_Zip":
                case "OtherAgent_Company_Phone":
                case "OtherAgent_Company_Fax":
                case "OtherAgent_Contact_Name":
                case "OtherAgent_Contact_Phone":
                case "OtherAgent_Case_Number":
                    part = code.Substring(11);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.Other, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
                    break;
				case "Mortgagee_Company_Name":
				case "Mortgagee_Company_Address":
				case "Mortgagee_Company_City":
				case "Mortgagee_Company_State":
				case "Mortgagee_Company_Zip":
				case "Mortgagee_Company_Phone":
				case "Mortgagee_Company_Fax":
				case "Mortgagee_Contact_Name":
				case "Mortgagee_Contact_Phone":
				case "Mortgagee_Case_Number":
					part = code.Substring(10);
                    ret = GetAgentDatapoint(dataLoan.GetAgentOfRole(E_AgentRoleT.Mortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject), part);
					break;
                    #endregion


                    #region OPM 3924. Added 02/22/2006
                case "sProThisMPmt": ret = dataLoan.sProThisMPmt_rep; break;
                case "sProRealETx": ret = dataLoan.sProRealETx_rep; break;
                case "sProMIns": ret = dataLoan.sProMIns_rep; break;
                case "sProHazIns": ret = dataLoan.sProHazIns_rep; break;
                case "sRAdj1stCapR": ret = dataLoan.sRAdj1stCapR_rep; break;
                case "sRAdj1stCapMon": ret = dataLoan.sRAdj1stCapMon_rep; break;
                case "sRAdjCapR": ret = dataLoan.sRAdjCapR_rep; break;
                case "sRAdjCapMon": ret = dataLoan.sRAdjCapMon_rep; break;
                case "sRAdjLifeCapR": ret = dataLoan.sRAdjLifeCapR_rep; break;
                case "sRAdjIndexR": ret = dataLoan.sRAdjIndexR_rep; break;

                case "aBExperianScore": ret = dataApp.aBExperianScore_rep; break;
                case "aBTransUnionScore": ret = dataApp.aBTransUnionScore_rep; break;
                case "aBEquifaxScore": ret = dataApp.aBEquifaxScore_rep; break;
                case "aCExperianScore": ret = dataApp.aCExperianScore_rep; break;
                case "aCTransUnionScore": ret = dataApp.aCTransUnionScore_rep; break;
                case "aCEquifaxScore": ret = dataApp.aCEquifaxScore_rep; break;
                case "sCreditScoreType1": ret = dataLoan.sCreditScoreType1_rep; break;
                case "sCreditScoreType2": ret = dataLoan.sCreditScoreType2_rep; break;
                case "sProdDocT": ret = dataLoan.sProdDocT_rep; break;
                case "sProdCashoutAmt": ret = dataLoan.sProdCashoutAmt_rep; break;
                case "sProdImpound": ret = dataLoan.sProdImpound ? "Yes" : "No"; break;
                    #endregion

                    #region OPM 4191. Added 3/3/2006
                case "sIOnlyMon": ret = dataLoan.sIOnlyMon_rep; break;
                case "sProdAvailReserveMonths": ret = dataLoan.sProdAvailReserveMonths_rep; break;
                case "sIsIOnly": ret = dataLoan.sIsIOnly ? "Yes" : "No"; break;
                    #endregion

                    #region OPM 4399. Added 3/17/2006
                case "sArmIndexT":
                switch (dataLoan.sArmIndexT) 
                {
                    case E_sArmIndexT.LeaveBlank: ret = ""; break;
                    case E_sArmIndexT.WeeklyAvgCMT: ret = "Weekly Average CMT"; break;
                    case E_sArmIndexT.MonthlyAvgCMT: ret = "Monthly Average CMT"; break;
                    case E_sArmIndexT.WeeklyAvgTAAI: ret = "Weekly Average TAAI"; break;
                    case E_sArmIndexT.WeeklyAvgTAABD: ret = "Weekly Average TAABD"; break;
                    case E_sArmIndexT.WeeklyAvgSMTI: ret = "Weekly Average SMTI"; break;
                    case E_sArmIndexT.DailyCDRate: ret = "Daily CD Rate"; break;
                    case E_sArmIndexT.WeeklyAvgCDRate: ret = "Weekly Average CD Rate"; break;
                    case E_sArmIndexT.WeeklyAvgPrimeRate: ret = "Weekly Average Prime Rate"; break;
                    case E_sArmIndexT.TBillDailyValue: ret = "T-Bill Daily Value"; break;
                    case E_sArmIndexT.EleventhDistrictCOF: ret = "11th District COF"; break;
                    case E_sArmIndexT.NationalMonthlyMedianCostOfFunds: ret = "National Monthly Median Cost of Funds"; break;
                    case E_sArmIndexT.WallStreetJournalLIBOR: ret = "Wall Street Journal LIBOR"; break;
                    case E_sArmIndexT.FannieMaeLIBOR: ret = "Fannie Mae LIBOR"; break;
                    default:
                        Tools.LogBug("DownLoadCustomForm: Unsupported value " + dataLoan.sArmIndexT + " for sArmIndexT");
                        break;

                };
                    break;
                case "sFreddieArmIndexT":
                switch (dataLoan.sFreddieArmIndexT) 
                {
                    case E_sFreddieArmIndexT.LeaveBlank: ret = ""; break;
                    case E_sFreddieArmIndexT.OneYearTreasury: ret = "One Year Treasury"; break;
                    case E_sFreddieArmIndexT.ThreeYearTreasury: ret = "Three Year Treasury"; break;
                    case E_sFreddieArmIndexT.SixMonthTreasury: ret = "Six Month Treasury"; break;
                    case E_sFreddieArmIndexT.EleventhDistrictCostOfFunds: ret = "11th District Cost Of Funds"; break;
                    case E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds: ret = "National Monthly Median Cost Of Funds"; break;
                    case E_sFreddieArmIndexT.LIBOR: ret = "LIBOR"; break;
                    case E_sFreddieArmIndexT.Other: ret = "Other"; break;
                    default:
                        Tools.LogBug("DownLoadCustomForm: Unsupport value " + dataLoan.sFreddieArmIndexT + " for sFreddieArmIndexT");
                        break;                
                };
                    break;
                    #endregion
                case "sProfitGrossLender":
                    // 8/31/2005 dd - We no longer support this field. 
                    ret = ""; break;

                    #region OPM 4748. Added 4/28/2006
                case "sBrokComp1Pc": ret = dataLoan.sBrokComp1Pc_rep; break;
                    #endregion

					#region OPM 4826. Added 5/12/2006
				case "sSubFin": ret = dataLoan.sSubFin_rep; break;
				case "sSubFinTerm": ret = dataLoan.sSubFinTerm_rep; break;
				case "sSubFinIR": ret = dataLoan.sSubFinIR_rep; break;
				case "sSubFinPmt": ret = dataLoan.sSubFinPmt_rep; break;
                    #endregion

					#region OPM 4191. Added 5/12/2006
				case "aProdBCitizenT":
					E_aProdCitizenT en = dataApp.aProdBCitizenT;
					//int en = (int) dataApp.aProdBCitizenT;
					switch (en) 
					{
						case E_aProdCitizenT.USCitizen: ret = "US Citizen"; break;
						case E_aProdCitizenT.PermanentResident: ret = "Permanent Resident"; break;
						case E_aProdCitizenT.NonpermanentResident: ret = "Non-permanent Resident"; break;
                        case E_aProdCitizenT.ForeignNational: ret = "Non-Resident Alien (Foreign National)"; break;
						default: ret = ""; break;
					}; break;
					#endregion

                case "sSchedDueD1": ret = dataLoan.sSchedDueD1_rep; break;
				case "sProHoAssocDues": ret = dataLoan.sProHoAssocDues_rep; break;
				case "sProOHExp": ret = dataLoan.sProOHExp_rep; break;

					#region OPM 8401. Added 2/1/2007
				case "aBAddrMail": ret = dataApp.aBAddrMail; break;
				case "aBCityMail": ret = dataApp.aBCityMail; break;
				case "aBStateMail": ret = dataApp.aBStateMail; break;
				case "aBZipMail": ret = dataApp.aBZipMail; break;
				case "sInvestLNum": ret = dataLoan.sInvestLNum; break;
				case "sGoodByLetterD": ret = dataLoan.sGoodByLetterD_rep; break;
				case "sLPurchaseD": ret = dataLoan.sLPurchaseD_rep; break;
				case "sLenContractD": ret = dataLoan.sLenContractD_rep; break;
                    #endregion

				// 2/1/2007 nw - OPM 5814 - Add Branch Name field to Custom Forms
				case "sBranchName":
                    ret = dataLoan.BranchNm;
					break;

				// 2/2/2007 nw - OPM 6027 - Add "Prepared Date" field to Custom Forms
				case "gfeTilPrepareDate":
					IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject );
					ret = gfeTil.PrepareDate_rep;
					break;

				// 2/26/2007 nw - OPM 9545 - Add "Servicing Start Date" field to Custom Forms
				case "sServicingStartD":
					ret = dataLoan.sServicingStartD_rep;
					break;

				// 3/21/2007 nw - OPM 10871 - Add Amortization Type and Amortization Description fields
				case "sFinMethT":
					ret = dataLoan.sFinMethT_rep;
					break;

				case "sFinMethDesc":
					ret = dataLoan.sFinMethDesc;
					break;

				// 6/12/2007 nw - OPM 12921 - Add borrower and coborrower's current employment info
				#region Borrower's current employment info
				case "Borrower_Current_Employment_Name":
					if( null != borEmp )
						ret = borEmp.EmplrNm;
					break;

				case "Borrower_Current_Employment_Position":
					if( null != borEmp )
						ret = borEmp.JobTitle;
					break;

				case "Borrower_Current_Employment_Years":
					if( null != borEmp )
						ret = borEmp.EmplmtLenInYrs_rep;
					break;

				case "Borrower_Current_Employment_Phone":
					if( null != borEmp )
						ret = borEmp.EmplrBusPhone;
					break;

				case "Borrower_Current_Employment_Address":
					if( null != borEmp )
						ret = borEmp.EmplrAddr;
					break;

				case "Borrower_Current_Employment_City":
					if( null != borEmp )
						ret = borEmp.EmplrCity;
					break;

				case "Borrower_Current_Employment_State":
					if( null != borEmp )
						ret = borEmp.EmplrState;
					break;

				case "Borrower_Current_Employment_Zipcode":
					if( null != borEmp )
						ret = borEmp.EmplrZip;
					break;

                case "Borrower_Current_Employment_SelfEmpl": // OPM 20827
                    if (null != borEmp)
                        ret = (borEmp.IsSelfEmplmt)?"Yes":"No";
                    break;
				#endregion Borrower's current employment info

				#region CoBorrower's current employment info
				case "CoBorrower_Current_Employment_Name":
					if( null != coBorEmp )
						ret = coBorEmp.EmplrNm;
					break;

				case "CoBorrower_Current_Employment_Position":
					if( null != coBorEmp )
						ret = coBorEmp.JobTitle;
					break;

				case "CoBorrower_Current_Employment_Years":
					if( null != coBorEmp )
						ret = coBorEmp.EmplmtLenInYrs_rep;
					break;

				case "CoBorrower_Current_Employment_Phone":
					if( null != coBorEmp )
						ret = coBorEmp.EmplrBusPhone;
					break;

				case "CoBorrower_Current_Employment_Address":
					if( null != coBorEmp )
						ret = coBorEmp.EmplrAddr;
					break;

				case "CoBorrower_Current_Employment_City":
					if( null != coBorEmp )
						ret = coBorEmp.EmplrCity;
					break;

				case "CoBorrower_Current_Employment_State":
					if( null != coBorEmp )
						ret = coBorEmp.EmplrState;
					break;

				case "CoBorrower_Current_Employment_Zipcode":
					if( null != coBorEmp )
						ret = coBorEmp.EmplrZip;
					break;
                case "CoBorrower_Current_Employment_SelfEmpl": // OPM 20827
                    if (null != coBorEmp)
                        ret = (coBorEmp.IsSelfEmplmt) ? "Yes" : "No";
                    break;
				#endregion Borrower's current employment info

				// 6/21/2007 nw - OPM 16578 - Add Loan Status
				case "sStatus":
					ret = dataLoan.sStatusT_rep;
					break;

                // 4/13/2009 db - OPM 16556 - Add Appraisal Comments field
                case "sApprInfo":
                    ret = dataLoan.sApprInfo;
                    break;

                // 4/13/2009 db - OPM 24786 - Add Broker Fee Percent field
                case "sMBrokFPc":
                    ret = dataLoan.sMBrokFPc_rep;
                    break;

                #region OPM 20827 (There are also a couple in the borr and coborr employment section for this case)
                case "sLTotI": ret = dataLoan.sLTotI_rep; break;
                case "sPresLTotPersistentHExp": ret = dataLoan.sPresLTotPersistentHExp_rep; break;
                case "sUnderwritingD": ret = dataLoan.sUnderwritingD_rep; break;
                case "sFannieDocT": ret = dataLoan.sFannieDocT_rep; break;
                case "sPpmtPenaltyMon": ret = dataLoan.sPpmtPenaltyMon_rep; break;
                case "sAusRecommendation": ret = dataLoan.sAusRecommendation; break;
                case "sWillEscrowBeWaived": ret = (dataLoan.sWillEscrowBeWaived) ? "Yes" : "No"; break;
                case "aTransmOMonPmt": ret = dataApp.aTransmOMonPmt_rep; break;
                #endregion

                // 11/1/07 db - OPM 18245 - Add Custom Fields
				#region Custom Field 1 Set
				case "sCustomField1Desc":
					ret = dataLoan.sCustomField1Desc;
					break;

				case "sCustomField1D":
					ret = dataLoan.sCustomField1D_rep;
					break;

				case "sCustomField1Money":
					ret = dataLoan.sCustomField1Money_rep;
					break;

				case "sCustomField1Pc":
					ret = dataLoan.sCustomField1Pc_rep;
					break;

				case "sCustomField1Bit":
					ret = dataLoan.sCustomField1Bit ? "Yes" : "No"; 
					break;

				case "sCustomField1Notes":
					ret = dataLoan.sCustomField1Notes;
					break;
				#endregion

				#region Custom Field 2 Set
				case "sCustomField2Desc":
					ret = dataLoan.sCustomField2Desc;
					break;

				case "sCustomField2D":
					ret = dataLoan.sCustomField2D_rep;
					break;

				case "sCustomField2Money":
					ret = dataLoan.sCustomField2Money_rep;
					break;

				case "sCustomField2Pc":
					ret = dataLoan.sCustomField2Pc_rep;
					break;

				case "sCustomField2Bit":
					ret = dataLoan.sCustomField2Bit ? "Yes" : "No"; 
					break;

				case "sCustomField2Notes":
					ret = dataLoan.sCustomField2Notes;
					break;
				#endregion

				#region Custom Field 3 Set
				case "sCustomField3Desc":
					ret = dataLoan.sCustomField3Desc;
					break;

				case "sCustomField3D":
					ret = dataLoan.sCustomField3D_rep;
					break;

				case "sCustomField3Money":
					ret = dataLoan.sCustomField3Money_rep;
					break;

				case "sCustomField3Pc":
					ret = dataLoan.sCustomField3Pc_rep;
					break;

				case "sCustomField3Bit":
					ret = dataLoan.sCustomField3Bit ? "Yes" : "No"; 
					break;

				case "sCustomField3Notes":
					ret = dataLoan.sCustomField3Notes;
					break;
				#endregion

				#region Custom Field 4 Set
				case "sCustomField4Desc":
					ret = dataLoan.sCustomField4Desc;
					break;

				case "sCustomField4D":
					ret = dataLoan.sCustomField4D_rep;
					break;

				case "sCustomField4Money":
					ret = dataLoan.sCustomField4Money_rep;
					break;

				case "sCustomField4Pc":
					ret = dataLoan.sCustomField4Pc_rep;
					break;

				case "sCustomField4Bit":
					ret = dataLoan.sCustomField4Bit ? "Yes" : "No"; 
					break;

				case "sCustomField4Notes":
					ret = dataLoan.sCustomField4Notes;
					break;
				#endregion

				#region Custom Field 5 Set
				case "sCustomField5Desc":
					ret = dataLoan.sCustomField5Desc;
					break;

				case "sCustomField5D":
					ret = dataLoan.sCustomField5D_rep;
					break;

				case "sCustomField5Money":
					ret = dataLoan.sCustomField5Money_rep;
					break;

				case "sCustomField5Pc":
					ret = dataLoan.sCustomField5Pc_rep;
					break;

				case "sCustomField5Bit":
					ret = dataLoan.sCustomField5Bit ? "Yes" : "No"; 
					break;

				case "sCustomField5Notes":
					ret = dataLoan.sCustomField5Notes;
					break;
				#endregion

				#region Custom Field 6 Set
				case "sCustomField6Desc":
					ret = dataLoan.sCustomField6Desc;
					break;

				case "sCustomField6D":
					ret = dataLoan.sCustomField6D_rep;
					break;

				case "sCustomField6Money":
					ret = dataLoan.sCustomField6Money_rep;
					break;

				case "sCustomField6Pc":
					ret = dataLoan.sCustomField6Pc_rep;
					break;

				case "sCustomField6Bit":
					ret = dataLoan.sCustomField6Bit ? "Yes" : "No"; 
					break;

				case "sCustomField6Notes":
					ret = dataLoan.sCustomField6Notes;
					break;
				#endregion

				#region Custom Field 7 Set
				case "sCustomField7Desc":
					ret = dataLoan.sCustomField7Desc;
					break;

				case "sCustomField7D":
					ret = dataLoan.sCustomField7D_rep;
					break;

				case "sCustomField7Money":
					ret = dataLoan.sCustomField7Money_rep;
					break;

				case "sCustomField7Pc":
					ret = dataLoan.sCustomField7Pc_rep;
					break;

				case "sCustomField7Bit":
					ret = dataLoan.sCustomField7Bit ? "Yes" : "No"; 
					break;

				case "sCustomField7Notes":
					ret = dataLoan.sCustomField7Notes;
					break;
				#endregion

				#region Custom Field 8 Set
				case "sCustomField8Desc":
					ret = dataLoan.sCustomField8Desc;
					break;

				case "sCustomField8D":
					ret = dataLoan.sCustomField8D_rep;
					break;

				case "sCustomField8Money":
					ret = dataLoan.sCustomField8Money_rep;
					break;

				case "sCustomField8Pc":
					ret = dataLoan.sCustomField8Pc_rep;
					break;

				case "sCustomField8Bit":
					ret = dataLoan.sCustomField8Bit ? "Yes" : "No"; 
					break;

				case "sCustomField8Notes":
					ret = dataLoan.sCustomField8Notes;
					break;
				#endregion

				#region Custom Field 9 Set
				case "sCustomField9Desc":
					ret = dataLoan.sCustomField9Desc;
					break;

				case "sCustomField9D":
					ret = dataLoan.sCustomField9D_rep;
					break;

				case "sCustomField9Money":
					ret = dataLoan.sCustomField9Money_rep;
					break;

				case "sCustomField9Pc":
					ret = dataLoan.sCustomField9Pc_rep;
					break;

				case "sCustomField9Bit":
					ret = dataLoan.sCustomField9Bit ? "Yes" : "No"; 
					break;

				case "sCustomField9Notes":
					ret = dataLoan.sCustomField9Notes;
					break;
				#endregion

				#region Custom Field 10 Set
				case "sCustomField10Desc":
					ret = dataLoan.sCustomField10Desc;
					break;

				case "sCustomField10D":
					ret = dataLoan.sCustomField10D_rep;
					break;

				case "sCustomField10Money":
					ret = dataLoan.sCustomField10Money_rep;
					break;

				case "sCustomField10Pc":
					ret = dataLoan.sCustomField10Pc_rep;
					break;

				case "sCustomField10Bit":
					ret = dataLoan.sCustomField10Bit ? "Yes" : "No"; 
					break;

				case "sCustomField10Notes":
					ret = dataLoan.sCustomField10Notes;
					break;
				#endregion

				#region Custom Field 11 Set
				case "sCustomField11Desc":
					ret = dataLoan.sCustomField11Desc;
					break;

				case "sCustomField11D":
					ret = dataLoan.sCustomField11D_rep;
					break;

				case "sCustomField11Money":
					ret = dataLoan.sCustomField11Money_rep;
					break;

				case "sCustomField11Pc":
					ret = dataLoan.sCustomField11Pc_rep;
					break;

				case "sCustomField11Bit":
					ret = dataLoan.sCustomField11Bit ? "Yes" : "No"; 
					break;

				case "sCustomField11Notes":
					ret = dataLoan.sCustomField11Notes;
					break;
				#endregion

				#region Custom Field 12 Set
				case "sCustomField12Desc":
					ret = dataLoan.sCustomField12Desc;
					break;

				case "sCustomField12D":
					ret = dataLoan.sCustomField12D_rep;
					break;

				case "sCustomField12Money":
					ret = dataLoan.sCustomField12Money_rep;
					break;

				case "sCustomField12Pc":
					ret = dataLoan.sCustomField12Pc_rep;
					break;

				case "sCustomField12Bit":
					ret = dataLoan.sCustomField12Bit ? "Yes" : "No"; 
					break;

				case "sCustomField12Notes":
					ret = dataLoan.sCustomField12Notes;
					break;
				#endregion

				#region Custom Field 13 Set
				case "sCustomField13Desc":
					ret = dataLoan.sCustomField13Desc;
					break;

				case "sCustomField13D":
					ret = dataLoan.sCustomField13D_rep;
					break;

				case "sCustomField13Money":
					ret = dataLoan.sCustomField13Money_rep;
					break;

				case "sCustomField13Pc":
					ret = dataLoan.sCustomField13Pc_rep;
					break;

				case "sCustomField13Bit":
					ret = dataLoan.sCustomField13Bit ? "Yes" : "No"; 
					break;

				case "sCustomField13Notes":
					ret = dataLoan.sCustomField13Notes;
					break;
				#endregion

				#region Custom Field 14 Set
				case "sCustomField14Desc":
					ret = dataLoan.sCustomField14Desc;
					break;

				case "sCustomField14D":
					ret = dataLoan.sCustomField14D_rep;
					break;

				case "sCustomField14Money":
					ret = dataLoan.sCustomField14Money_rep;
					break;

				case "sCustomField14Pc":
					ret = dataLoan.sCustomField14Pc_rep;
					break;

				case "sCustomField14Bit":
					ret = dataLoan.sCustomField14Bit ? "Yes" : "No"; 
					break;

				case "sCustomField14Notes":
					ret = dataLoan.sCustomField14Notes;
					break;
				#endregion

				#region Custom Field 15 Set
				case "sCustomField15Desc":
					ret = dataLoan.sCustomField15Desc;
					break;

				case "sCustomField15D":
					ret = dataLoan.sCustomField15D_rep;
					break;

				case "sCustomField15Money":
					ret = dataLoan.sCustomField15Money_rep;
					break;

				case "sCustomField15Pc":
					ret = dataLoan.sCustomField15Pc_rep;
					break;

				case "sCustomField15Bit":
					ret = dataLoan.sCustomField15Bit ? "Yes" : "No"; 
					break;

				case "sCustomField15Notes":
					ret = dataLoan.sCustomField15Notes;
					break;
				#endregion

				#region Custom Field 16 Set
				case "sCustomField16Desc":
					ret = dataLoan.sCustomField16Desc;
					break;

				case "sCustomField16D":
					ret = dataLoan.sCustomField16D_rep;
					break;

				case "sCustomField16Money":
					ret = dataLoan.sCustomField16Money_rep;
					break;

				case "sCustomField16Pc":
					ret = dataLoan.sCustomField16Pc_rep;
					break;

				case "sCustomField16Bit":
					ret = dataLoan.sCustomField16Bit ? "Yes" : "No"; 
					break;

				case "sCustomField16Notes":
					ret = dataLoan.sCustomField16Notes;
					break;
				#endregion

				#region Custom Field 17 Set
				case "sCustomField17Desc":
					ret = dataLoan.sCustomField17Desc;
					break;

				case "sCustomField17D":
					ret = dataLoan.sCustomField17D_rep;
					break;

				case "sCustomField17Money":
					ret = dataLoan.sCustomField17Money_rep;
					break;

				case "sCustomField17Pc":
					ret = dataLoan.sCustomField17Pc_rep;
					break;

				case "sCustomField17Bit":
					ret = dataLoan.sCustomField17Bit ? "Yes" : "No"; 
					break;

				case "sCustomField17Notes":
					ret = dataLoan.sCustomField17Notes;
					break;
				#endregion

				#region Custom Field 18 Set
				case "sCustomField18Desc":
					ret = dataLoan.sCustomField18Desc;
					break;

				case "sCustomField18D":
					ret = dataLoan.sCustomField18D_rep;
					break;

				case "sCustomField18Money":
					ret = dataLoan.sCustomField18Money_rep;
					break;

				case "sCustomField18Pc":
					ret = dataLoan.sCustomField18Pc_rep;
					break;

				case "sCustomField18Bit":
					ret = dataLoan.sCustomField18Bit ? "Yes" : "No"; 
					break;

				case "sCustomField18Notes":
					ret = dataLoan.sCustomField18Notes;
					break;
				#endregion

				#region Custom Field 19 Set
				case "sCustomField19Desc":
					ret = dataLoan.sCustomField19Desc;
					break;

				case "sCustomField19D":
					ret = dataLoan.sCustomField19D_rep;
					break;

				case "sCustomField19Money":
					ret = dataLoan.sCustomField19Money_rep;
					break;

				case "sCustomField19Pc":
					ret = dataLoan.sCustomField19Pc_rep;
					break;

				case "sCustomField19Bit":
					ret = dataLoan.sCustomField19Bit ? "Yes" : "No"; 
					break;

				case "sCustomField19Notes":
					ret = dataLoan.sCustomField19Notes;
					break;
				#endregion

				#region Custom Field 20 Set
				case "sCustomField20Desc":
					ret = dataLoan.sCustomField20Desc;
					break;

				case "sCustomField20D":
					ret = dataLoan.sCustomField20D_rep;
					break;

				case "sCustomField20Money":
					ret = dataLoan.sCustomField20Money_rep;
					break;

				case "sCustomField20Pc":
					ret = dataLoan.sCustomField20Pc_rep;
					break;

				case "sCustomField20Bit":
					ret = dataLoan.sCustomField20Bit ? "Yes" : "No"; 
					break;

				case "sCustomField20Notes":
					ret = dataLoan.sCustomField20Notes;
					break;
				#endregion

                #region Custom Field 21 Set
                case "sCustomField21Desc":
                    ret = dataLoan.sCustomField21Desc;
                    break;

                case "sCustomField21D":
                    ret = dataLoan.sCustomField21D_rep;
                    break;

                case "sCustomField21Money":
                    ret = dataLoan.sCustomField21Money_rep;
                    break;

                case "sCustomField21Pc":
                    ret = dataLoan.sCustomField21Pc_rep;
                    break;

                case "sCustomField21Bit":
                    ret = dataLoan.sCustomField21Bit ? "Yes" : "No";
                    break;

                case "sCustomField21Notes":
                    ret = dataLoan.sCustomField21Notes;
                    break;
                #endregion

                #region Custom Field 22 Set
                case "sCustomField22Desc":
                    ret = dataLoan.sCustomField22Desc;
                    break;

                case "sCustomField22D":
                    ret = dataLoan.sCustomField22D_rep;
                    break;

                case "sCustomField22Money":
                    ret = dataLoan.sCustomField22Money_rep;
                    break;

                case "sCustomField22Pc":
                    ret = dataLoan.sCustomField22Pc_rep;
                    break;

                case "sCustomField22Bit":
                    ret = dataLoan.sCustomField22Bit ? "Yes" : "No";
                    break;

                case "sCustomField22Notes":
                    ret = dataLoan.sCustomField22Notes;
                    break;
                #endregion

                #region Custom Field 23 Set
                case "sCustomField23Desc":
                    ret = dataLoan.sCustomField23Desc;
                    break;

                case "sCustomField23D":
                    ret = dataLoan.sCustomField23D_rep;
                    break;

                case "sCustomField23Money":
                    ret = dataLoan.sCustomField23Money_rep;
                    break;

                case "sCustomField23Pc":
                    ret = dataLoan.sCustomField23Pc_rep;
                    break;

                case "sCustomField23Bit":
                    ret = dataLoan.sCustomField23Bit ? "Yes" : "No";
                    break;

                case "sCustomField23Notes":
                    ret = dataLoan.sCustomField23Notes;
                    break;
                #endregion

                #region Custom Field 24 Set
                case "sCustomField24Desc":
                    ret = dataLoan.sCustomField24Desc;
                    break;

                case "sCustomField24D":
                    ret = dataLoan.sCustomField24D_rep;
                    break;

                case "sCustomField24Money":
                    ret = dataLoan.sCustomField24Money_rep;
                    break;

                case "sCustomField24Pc":
                    ret = dataLoan.sCustomField24Pc_rep;
                    break;

                case "sCustomField24Bit":
                    ret = dataLoan.sCustomField24Bit ? "Yes" : "No";
                    break;

                case "sCustomField24Notes":
                    ret = dataLoan.sCustomField24Notes;
                    break;
                #endregion

                #region Custom Field 25 Set
                case "sCustomField25Desc":
                    ret = dataLoan.sCustomField25Desc;
                    break;

                case "sCustomField25D":
                    ret = dataLoan.sCustomField25D_rep;
                    break;

                case "sCustomField25Money":
                    ret = dataLoan.sCustomField25Money_rep;
                    break;

                case "sCustomField25Pc":
                    ret = dataLoan.sCustomField25Pc_rep;
                    break;

                case "sCustomField25Bit":
                    ret = dataLoan.sCustomField25Bit ? "Yes" : "No";
                    break;

                case "sCustomField25Notes":
                    ret = dataLoan.sCustomField25Notes;
                    break;
                #endregion

                #region Custom Field 26 Set
                case "sCustomField26Desc":
                    ret = dataLoan.sCustomField26Desc;
                    break;

                case "sCustomField26D":
                    ret = dataLoan.sCustomField26D_rep;
                    break;

                case "sCustomField26Money":
                    ret = dataLoan.sCustomField26Money_rep;
                    break;

                case "sCustomField26Pc":
                    ret = dataLoan.sCustomField26Pc_rep;
                    break;

                case "sCustomField26Bit":
                    ret = dataLoan.sCustomField26Bit ? "Yes" : "No";
                    break;

                case "sCustomField26Notes":
                    ret = dataLoan.sCustomField26Notes;
                    break;
                #endregion

                #region Custom Field 27 Set
                case "sCustomField27Desc":
                    ret = dataLoan.sCustomField27Desc;
                    break;

                case "sCustomField27D":
                    ret = dataLoan.sCustomField27D_rep;
                    break;

                case "sCustomField27Money":
                    ret = dataLoan.sCustomField27Money_rep;
                    break;

                case "sCustomField27Pc":
                    ret = dataLoan.sCustomField27Pc_rep;
                    break;

                case "sCustomField27Bit":
                    ret = dataLoan.sCustomField27Bit ? "Yes" : "No";
                    break;

                case "sCustomField27Notes":
                    ret = dataLoan.sCustomField27Notes;
                    break;
                #endregion

                #region Custom Field 28 Set
                case "sCustomField28Desc":
                    ret = dataLoan.sCustomField28Desc;
                    break;

                case "sCustomField28D":
                    ret = dataLoan.sCustomField28D_rep;
                    break;

                case "sCustomField28Money":
                    ret = dataLoan.sCustomField28Money_rep;
                    break;

                case "sCustomField28Pc":
                    ret = dataLoan.sCustomField28Pc_rep;
                    break;

                case "sCustomField28Bit":
                    ret = dataLoan.sCustomField28Bit ? "Yes" : "No";
                    break;

                case "sCustomField28Notes":
                    ret = dataLoan.sCustomField28Notes;
                    break;
                #endregion

                #region Custom Field 29 Set
                case "sCustomField29Desc":
                    ret = dataLoan.sCustomField29Desc;
                    break;

                case "sCustomField29D":
                    ret = dataLoan.sCustomField29D_rep;
                    break;

                case "sCustomField29Money":
                    ret = dataLoan.sCustomField29Money_rep;
                    break;

                case "sCustomField29Pc":
                    ret = dataLoan.sCustomField29Pc_rep;
                    break;

                case "sCustomField29Bit":
                    ret = dataLoan.sCustomField29Bit ? "Yes" : "No";
                    break;

                case "sCustomField29Notes":
                    ret = dataLoan.sCustomField29Notes;
                    break;
                #endregion

                #region Custom Field 30 Set
                case "sCustomField30Desc":
                    ret = dataLoan.sCustomField30Desc;
                    break;

                case "sCustomField30D":
                    ret = dataLoan.sCustomField30D_rep;
                    break;

                case "sCustomField30Money":
                    ret = dataLoan.sCustomField30Money_rep;
                    break;

                case "sCustomField30Pc":
                    ret = dataLoan.sCustomField30Pc_rep;
                    break;

                case "sCustomField30Bit":
                    ret = dataLoan.sCustomField30Bit ? "Yes" : "No";
                    break;

                case "sCustomField30Notes":
                    ret = dataLoan.sCustomField30Notes;
                    break;
                #endregion

                #region Custom Field 31 Set
                case "sCustomField31Desc":
                    ret = dataLoan.sCustomField31Desc;
                    break;

                case "sCustomField31D":
                    ret = dataLoan.sCustomField31D_rep;
                    break;

                case "sCustomField31Money":
                    ret = dataLoan.sCustomField31Money_rep;
                    break;

                case "sCustomField31Pc":
                    ret = dataLoan.sCustomField31Pc_rep;
                    break;

                case "sCustomField31Bit":
                    ret = dataLoan.sCustomField31Bit ? "Yes" : "No";
                    break;

                case "sCustomField31Notes":
                    ret = dataLoan.sCustomField31Notes;
                    break;
                #endregion

                #region Custom Field 32 Set
                case "sCustomField32Desc":
                    ret = dataLoan.sCustomField32Desc;
                    break;

                case "sCustomField32D":
                    ret = dataLoan.sCustomField32D_rep;
                    break;

                case "sCustomField32Money":
                    ret = dataLoan.sCustomField32Money_rep;
                    break;

                case "sCustomField32Pc":
                    ret = dataLoan.sCustomField32Pc_rep;
                    break;

                case "sCustomField32Bit":
                    ret = dataLoan.sCustomField32Bit ? "Yes" : "No";
                    break;

                case "sCustomField32Notes":
                    ret = dataLoan.sCustomField32Notes;
                    break;
                #endregion

                #region Custom Field 33 Set
                case "sCustomField33Desc":
                    ret = dataLoan.sCustomField33Desc;
                    break;

                case "sCustomField33D":
                    ret = dataLoan.sCustomField33D_rep;
                    break;

                case "sCustomField33Money":
                    ret = dataLoan.sCustomField33Money_rep;
                    break;

                case "sCustomField33Pc":
                    ret = dataLoan.sCustomField33Pc_rep;
                    break;

                case "sCustomField33Bit":
                    ret = dataLoan.sCustomField33Bit ? "Yes" : "No";
                    break;

                case "sCustomField33Notes":
                    ret = dataLoan.sCustomField33Notes;
                    break;
                #endregion

                #region Custom Field 34 Set
                case "sCustomField34Desc":
                    ret = dataLoan.sCustomField34Desc;
                    break;

                case "sCustomField34D":
                    ret = dataLoan.sCustomField34D_rep;
                    break;

                case "sCustomField34Money":
                    ret = dataLoan.sCustomField34Money_rep;
                    break;

                case "sCustomField34Pc":
                    ret = dataLoan.sCustomField34Pc_rep;
                    break;

                case "sCustomField34Bit":
                    ret = dataLoan.sCustomField34Bit ? "Yes" : "No";
                    break;

                case "sCustomField34Notes":
                    ret = dataLoan.sCustomField34Notes;
                    break;
                #endregion

                #region Custom Field 35 Set
                case "sCustomField35Desc":
                    ret = dataLoan.sCustomField35Desc;
                    break;

                case "sCustomField35D":
                    ret = dataLoan.sCustomField35D_rep;
                    break;

                case "sCustomField35Money":
                    ret = dataLoan.sCustomField35Money_rep;
                    break;

                case "sCustomField35Pc":
                    ret = dataLoan.sCustomField35Pc_rep;
                    break;

                case "sCustomField35Bit":
                    ret = dataLoan.sCustomField35Bit ? "Yes" : "No";
                    break;

                case "sCustomField35Notes":
                    ret = dataLoan.sCustomField35Notes;
                    break;
                #endregion

                #region Custom Field 36 Set
                case "sCustomField36Desc":
                    ret = dataLoan.sCustomField36Desc;
                    break;

                case "sCustomField36D":
                    ret = dataLoan.sCustomField36D_rep;
                    break;

                case "sCustomField36Money":
                    ret = dataLoan.sCustomField36Money_rep;
                    break;

                case "sCustomField36Pc":
                    ret = dataLoan.sCustomField36Pc_rep;
                    break;

                case "sCustomField36Bit":
                    ret = dataLoan.sCustomField36Bit ? "Yes" : "No";
                    break;

                case "sCustomField36Notes":
                    ret = dataLoan.sCustomField36Notes;
                    break;
                #endregion

                #region Custom Field 37 Set
                case "sCustomField37Desc":
                    ret = dataLoan.sCustomField37Desc;
                    break;

                case "sCustomField37D":
                    ret = dataLoan.sCustomField37D_rep;
                    break;

                case "sCustomField37Money":
                    ret = dataLoan.sCustomField37Money_rep;
                    break;

                case "sCustomField37Pc":
                    ret = dataLoan.sCustomField37Pc_rep;
                    break;

                case "sCustomField37Bit":
                    ret = dataLoan.sCustomField37Bit ? "Yes" : "No";
                    break;

                case "sCustomField37Notes":
                    ret = dataLoan.sCustomField37Notes;
                    break;
                #endregion

                #region Custom Field 38 Set
                case "sCustomField38Desc":
                    ret = dataLoan.sCustomField38Desc;
                    break;

                case "sCustomField38D":
                    ret = dataLoan.sCustomField38D_rep;
                    break;

                case "sCustomField38Money":
                    ret = dataLoan.sCustomField38Money_rep;
                    break;

                case "sCustomField38Pc":
                    ret = dataLoan.sCustomField38Pc_rep;
                    break;

                case "sCustomField38Bit":
                    ret = dataLoan.sCustomField38Bit ? "Yes" : "No";
                    break;

                case "sCustomField38Notes":
                    ret = dataLoan.sCustomField38Notes;
                    break;
                #endregion

                #region Custom Field 39 Set
                case "sCustomField39Desc":
                    ret = dataLoan.sCustomField39Desc;
                    break;

                case "sCustomField39D":
                    ret = dataLoan.sCustomField39D_rep;
                    break;

                case "sCustomField39Money":
                    ret = dataLoan.sCustomField39Money_rep;
                    break;

                case "sCustomField39Pc":
                    ret = dataLoan.sCustomField39Pc_rep;
                    break;

                case "sCustomField39Bit":
                    ret = dataLoan.sCustomField39Bit ? "Yes" : "No";
                    break;

                case "sCustomField39Notes":
                    ret = dataLoan.sCustomField39Notes;
                    break;
                #endregion

                #region Custom Field 40 Set
                case "sCustomField40Desc":
                    ret = dataLoan.sCustomField40Desc;
                    break;

                case "sCustomField40D":
                    ret = dataLoan.sCustomField40D_rep;
                    break;

                case "sCustomField40Money":
                    ret = dataLoan.sCustomField40Money_rep;
                    break;

                case "sCustomField40Pc":
                    ret = dataLoan.sCustomField40Pc_rep;
                    break;

                case "sCustomField40Bit":
                    ret = dataLoan.sCustomField40Bit ? "Yes" : "No";
                    break;

                case "sCustomField40Notes":
                    ret = dataLoan.sCustomField40Notes;
                    break;
                #endregion

                #region Custom Field 41 Set
                case "sCustomField41Desc":
                    ret = dataLoan.sCustomField41Desc;
                    break;

                case "sCustomField41D":
                    ret = dataLoan.sCustomField41D_rep;
                    break;

                case "sCustomField41Money":
                    ret = dataLoan.sCustomField41Money_rep;
                    break;

                case "sCustomField41Pc":
                    ret = dataLoan.sCustomField41Pc_rep;
                    break;

                case "sCustomField41Bit":
                    ret = dataLoan.sCustomField41Bit ? "Yes" : "No";
                    break;

                case "sCustomField41Notes":
                    ret = dataLoan.sCustomField41Notes;
                    break;
                #endregion

                #region Custom Field 42 Set
                case "sCustomField42Desc":
                    ret = dataLoan.sCustomField42Desc;
                    break;

                case "sCustomField42D":
                    ret = dataLoan.sCustomField42D_rep;
                    break;

                case "sCustomField42Money":
                    ret = dataLoan.sCustomField42Money_rep;
                    break;

                case "sCustomField42Pc":
                    ret = dataLoan.sCustomField42Pc_rep;
                    break;

                case "sCustomField42Bit":
                    ret = dataLoan.sCustomField42Bit ? "Yes" : "No";
                    break;

                case "sCustomField42Notes":
                    ret = dataLoan.sCustomField42Notes;
                    break;
                #endregion

                #region Custom Field 43 Set
                case "sCustomField43Desc":
                    ret = dataLoan.sCustomField43Desc;
                    break;

                case "sCustomField43D":
                    ret = dataLoan.sCustomField43D_rep;
                    break;

                case "sCustomField43Money":
                    ret = dataLoan.sCustomField43Money_rep;
                    break;

                case "sCustomField43Pc":
                    ret = dataLoan.sCustomField43Pc_rep;
                    break;

                case "sCustomField43Bit":
                    ret = dataLoan.sCustomField43Bit ? "Yes" : "No";
                    break;

                case "sCustomField43Notes":
                    ret = dataLoan.sCustomField43Notes;
                    break;
                #endregion

                #region Custom Field 44 Set
                case "sCustomField44Desc":
                    ret = dataLoan.sCustomField44Desc;
                    break;

                case "sCustomField44D":
                    ret = dataLoan.sCustomField44D_rep;
                    break;

                case "sCustomField44Money":
                    ret = dataLoan.sCustomField44Money_rep;
                    break;

                case "sCustomField44Pc":
                    ret = dataLoan.sCustomField44Pc_rep;
                    break;

                case "sCustomField44Bit":
                    ret = dataLoan.sCustomField44Bit ? "Yes" : "No";
                    break;

                case "sCustomField44Notes":
                    ret = dataLoan.sCustomField44Notes;
                    break;
                #endregion

                #region Custom Field 45 Set
                case "sCustomField45Desc":
                    ret = dataLoan.sCustomField45Desc;
                    break;

                case "sCustomField45D":
                    ret = dataLoan.sCustomField45D_rep;
                    break;

                case "sCustomField45Money":
                    ret = dataLoan.sCustomField45Money_rep;
                    break;

                case "sCustomField45Pc":
                    ret = dataLoan.sCustomField45Pc_rep;
                    break;

                case "sCustomField45Bit":
                    ret = dataLoan.sCustomField45Bit ? "Yes" : "No";
                    break;

                case "sCustomField45Notes":
                    ret = dataLoan.sCustomField45Notes;
                    break;
                #endregion

                #region Custom Field 46 Set
                case "sCustomField46Desc":
                    ret = dataLoan.sCustomField46Desc;
                    break;

                case "sCustomField46D":
                    ret = dataLoan.sCustomField46D_rep;
                    break;

                case "sCustomField46Money":
                    ret = dataLoan.sCustomField46Money_rep;
                    break;

                case "sCustomField46Pc":
                    ret = dataLoan.sCustomField46Pc_rep;
                    break;

                case "sCustomField46Bit":
                    ret = dataLoan.sCustomField46Bit ? "Yes" : "No";
                    break;

                case "sCustomField46Notes":
                    ret = dataLoan.sCustomField46Notes;
                    break;
                #endregion

                #region Custom Field 47 Set
                case "sCustomField47Desc":
                    ret = dataLoan.sCustomField47Desc;
                    break;

                case "sCustomField47D":
                    ret = dataLoan.sCustomField47D_rep;
                    break;

                case "sCustomField47Money":
                    ret = dataLoan.sCustomField47Money_rep;
                    break;

                case "sCustomField47Pc":
                    ret = dataLoan.sCustomField47Pc_rep;
                    break;

                case "sCustomField47Bit":
                    ret = dataLoan.sCustomField47Bit ? "Yes" : "No";
                    break;

                case "sCustomField47Notes":
                    ret = dataLoan.sCustomField47Notes;
                    break;
                #endregion

                #region Custom Field 48 Set
                case "sCustomField48Desc":
                    ret = dataLoan.sCustomField48Desc;
                    break;

                case "sCustomField48D":
                    ret = dataLoan.sCustomField48D_rep;
                    break;

                case "sCustomField48Money":
                    ret = dataLoan.sCustomField48Money_rep;
                    break;

                case "sCustomField48Pc":
                    ret = dataLoan.sCustomField48Pc_rep;
                    break;

                case "sCustomField48Bit":
                    ret = dataLoan.sCustomField48Bit ? "Yes" : "No";
                    break;

                case "sCustomField48Notes":
                    ret = dataLoan.sCustomField48Notes;
                    break;
                #endregion

                #region Custom Field 49 Set
                case "sCustomField49Desc":
                    ret = dataLoan.sCustomField49Desc;
                    break;

                case "sCustomField49D":
                    ret = dataLoan.sCustomField49D_rep;
                    break;

                case "sCustomField49Money":
                    ret = dataLoan.sCustomField49Money_rep;
                    break;

                case "sCustomField49Pc":
                    ret = dataLoan.sCustomField49Pc_rep;
                    break;

                case "sCustomField49Bit":
                    ret = dataLoan.sCustomField49Bit ? "Yes" : "No";
                    break;

                case "sCustomField49Notes":
                    ret = dataLoan.sCustomField49Notes;
                    break;
                #endregion

                #region Custom Field 50 Set
                case "sCustomField50Desc":
                    ret = dataLoan.sCustomField50Desc;
                    break;

                case "sCustomField50D":
                    ret = dataLoan.sCustomField50D_rep;
                    break;

                case "sCustomField50Money":
                    ret = dataLoan.sCustomField50Money_rep;
                    break;

                case "sCustomField50Pc":
                    ret = dataLoan.sCustomField50Pc_rep;
                    break;

                case "sCustomField50Bit":
                    ret = dataLoan.sCustomField50Bit ? "Yes" : "No";
                    break;

                case "sCustomField50Notes":
                    ret = dataLoan.sCustomField50Notes;
                    break;
                #endregion

                #region Custom Field 51 Set
                case "sCustomField51Desc":
                    ret = dataLoan.sCustomField51Desc;
                    break;

                case "sCustomField51D":
                    ret = dataLoan.sCustomField51D_rep;
                    break;

                case "sCustomField51Money":
                    ret = dataLoan.sCustomField51Money_rep;
                    break;

                case "sCustomField51Pc":
                    ret = dataLoan.sCustomField51Pc_rep;
                    break;

                case "sCustomField51Bit":
                    ret = dataLoan.sCustomField51Bit ? "Yes" : "No";
                    break;

                case "sCustomField51Notes":
                    ret = dataLoan.sCustomField51Notes;
                    break;
                #endregion

                #region Custom Field 52 Set
                case "sCustomField52Desc":
                    ret = dataLoan.sCustomField52Desc;
                    break;

                case "sCustomField52D":
                    ret = dataLoan.sCustomField52D_rep;
                    break;

                case "sCustomField52Money":
                    ret = dataLoan.sCustomField52Money_rep;
                    break;

                case "sCustomField52Pc":
                    ret = dataLoan.sCustomField52Pc_rep;
                    break;

                case "sCustomField52Bit":
                    ret = dataLoan.sCustomField52Bit ? "Yes" : "No";
                    break;

                case "sCustomField52Notes":
                    ret = dataLoan.sCustomField52Notes;
                    break;
                #endregion

                #region Custom Field 53 Set
                case "sCustomField53Desc":
                    ret = dataLoan.sCustomField53Desc;
                    break;

                case "sCustomField53D":
                    ret = dataLoan.sCustomField53D_rep;
                    break;

                case "sCustomField53Money":
                    ret = dataLoan.sCustomField53Money_rep;
                    break;

                case "sCustomField53Pc":
                    ret = dataLoan.sCustomField53Pc_rep;
                    break;

                case "sCustomField53Bit":
                    ret = dataLoan.sCustomField53Bit ? "Yes" : "No";
                    break;

                case "sCustomField53Notes":
                    ret = dataLoan.sCustomField53Notes;
                    break;
                #endregion

                #region Custom Field 54 Set
                case "sCustomField54Desc":
                    ret = dataLoan.sCustomField54Desc;
                    break;

                case "sCustomField54D":
                    ret = dataLoan.sCustomField54D_rep;
                    break;

                case "sCustomField54Money":
                    ret = dataLoan.sCustomField54Money_rep;
                    break;

                case "sCustomField54Pc":
                    ret = dataLoan.sCustomField54Pc_rep;
                    break;

                case "sCustomField54Bit":
                    ret = dataLoan.sCustomField54Bit ? "Yes" : "No";
                    break;

                case "sCustomField54Notes":
                    ret = dataLoan.sCustomField54Notes;
                    break;
                #endregion

                #region Custom Field 55 Set
                case "sCustomField55Desc":
                    ret = dataLoan.sCustomField55Desc;
                    break;

                case "sCustomField55D":
                    ret = dataLoan.sCustomField55D_rep;
                    break;

                case "sCustomField55Money":
                    ret = dataLoan.sCustomField55Money_rep;
                    break;

                case "sCustomField55Pc":
                    ret = dataLoan.sCustomField55Pc_rep;
                    break;

                case "sCustomField55Bit":
                    ret = dataLoan.sCustomField55Bit ? "Yes" : "No";
                    break;

                case "sCustomField55Notes":
                    ret = dataLoan.sCustomField55Notes;
                    break;
                #endregion

                #region Custom Field 56 Set
                case "sCustomField56Desc":
                    ret = dataLoan.sCustomField56Desc;
                    break;

                case "sCustomField56D":
                    ret = dataLoan.sCustomField56D_rep;
                    break;

                case "sCustomField56Money":
                    ret = dataLoan.sCustomField56Money_rep;
                    break;

                case "sCustomField56Pc":
                    ret = dataLoan.sCustomField56Pc_rep;
                    break;

                case "sCustomField56Bit":
                    ret = dataLoan.sCustomField56Bit ? "Yes" : "No";
                    break;

                case "sCustomField56Notes":
                    ret = dataLoan.sCustomField56Notes;
                    break;
                #endregion

                #region Custom Field 57 Set
                case "sCustomField57Desc":
                    ret = dataLoan.sCustomField57Desc;
                    break;

                case "sCustomField57D":
                    ret = dataLoan.sCustomField57D_rep;
                    break;

                case "sCustomField57Money":
                    ret = dataLoan.sCustomField57Money_rep;
                    break;

                case "sCustomField57Pc":
                    ret = dataLoan.sCustomField57Pc_rep;
                    break;

                case "sCustomField57Bit":
                    ret = dataLoan.sCustomField57Bit ? "Yes" : "No";
                    break;

                case "sCustomField57Notes":
                    ret = dataLoan.sCustomField57Notes;
                    break;
                #endregion

                #region Custom Field 58 Set
                case "sCustomField58Desc":
                    ret = dataLoan.sCustomField58Desc;
                    break;

                case "sCustomField58D":
                    ret = dataLoan.sCustomField58D_rep;
                    break;

                case "sCustomField58Money":
                    ret = dataLoan.sCustomField58Money_rep;
                    break;

                case "sCustomField58Pc":
                    ret = dataLoan.sCustomField58Pc_rep;
                    break;

                case "sCustomField58Bit":
                    ret = dataLoan.sCustomField58Bit ? "Yes" : "No";
                    break;

                case "sCustomField58Notes":
                    ret = dataLoan.sCustomField58Notes;
                    break;
                #endregion

                #region Custom Field 59 Set
                case "sCustomField59Desc":
                    ret = dataLoan.sCustomField59Desc;
                    break;

                case "sCustomField59D":
                    ret = dataLoan.sCustomField59D_rep;
                    break;

                case "sCustomField59Money":
                    ret = dataLoan.sCustomField59Money_rep;
                    break;

                case "sCustomField59Pc":
                    ret = dataLoan.sCustomField59Pc_rep;
                    break;

                case "sCustomField59Bit":
                    ret = dataLoan.sCustomField59Bit ? "Yes" : "No";
                    break;

                case "sCustomField59Notes":
                    ret = dataLoan.sCustomField59Notes;
                    break;
                #endregion

                #region Custom Field 60 Set
                case "sCustomField60Desc":
                    ret = dataLoan.sCustomField60Desc;
                    break;

                case "sCustomField60D":
                    ret = dataLoan.sCustomField60D_rep;
                    break;

                case "sCustomField60Money":
                    ret = dataLoan.sCustomField60Money_rep;
                    break;

                case "sCustomField60Pc":
                    ret = dataLoan.sCustomField60Pc_rep;
                    break;

                case "sCustomField60Bit":
                    ret = dataLoan.sCustomField60Bit ? "Yes" : "No";
                    break;

                case "sCustomField60Notes":
                    ret = dataLoan.sCustomField60Notes;
                    break;
                #endregion

				//OPM 18292 jk - Add current date
				case "currentDate":
					ret = DateTime.Now.ToShortDateString();
					break;

				default:
                    Tools.LogErrorWithCriticalTracking("Missing " + code + " for custom form.");
                    ret = string.Empty;
                    break;
            }

            return ret;
        }

        /// <summary>
        /// Gets agent data for the specified datapoint.
        /// </summary>
        /// <param name="agent">
        /// The agent.
        /// </param>
        /// <param name="datapoint">
        /// The datapoint.
        /// </param>
        /// <returns>
        /// Data for the specified datapoint.
        /// </returns>
        private string GetAgentDatapoint(CAgentFields agent, string datapoint)
        {
            switch (datapoint)
            {
                case "Company_Name":
                    return agent.CompanyName;
                case "Company_Address":
                    return agent.StreetAddr;
                case "Company_City":
                    return agent.City;
                case "Company_State":
                    return agent.State;
                case "Company_Zip":
                    return agent.Zip;
                case "Company_Phone":
                    return agent.PhoneOfCompany;
                case "Company_Fax":
                    return agent.FaxOfCompany;
                case "Contact_Name":
                    return agent.AgentName;
                case "Contact_Phone":
                    return agent.Phone;
                case "Case_Number":
                    return agent.CaseNum;
                default:
                    Tools.LogErrorWithCriticalTracking(datapoint + " is not recognized as valid part for CAgentFields info.");
                    return string.Empty;
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
        }
		#endregion
    }
}
