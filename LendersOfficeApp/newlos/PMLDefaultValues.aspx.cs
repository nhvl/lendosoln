namespace LendersOfficeApp.newlos
{
    using System;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.CustomPmlFields;
    using LendersOffice.Security;

    public partial class PMLDefaultValues : LendersOfficeApp.newlos.BaseLoanPage
	{
        protected System.Web.UI.WebControls.CheckBox aBHasSpouse;
		protected bool m_isShowsProdEstimatedResidualI = false;
        protected bool m_isAutoPmiOrNewPml = false;

        protected bool EnableRenovationUi => this.Broker.IsEnableRenovationCheckboxInPML || this.Broker.EnableRenovationLoanSupport;
    
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageID = "PMLDefaultValues";
            BindLateDatesDropDown(sProdCrManual30MortLateCount);
            BindLateDatesDropDown(sProdCrManual60MortLateCount);
            BindLateDatesDropDown(sProdCrManual90MortLateCount);
            BindLateDatesDropDown(sProdCrManual120MortLateCount);
            BindLateDatesDropDown(sProdCrManual150MortLateCount);
            BindLateDatesDropDown(sProdCrManualNonRolling30MortLateCount);
			BindLateDatesDropDown(sProdCrManualRolling60MortLateCount);
			BindLateDatesDropDown(sProdCrManualRolling90MortLateCount);

            BindMonthDropDown(sProdCrManualBk13RecentFileMon);
            BindMonthDropDown(sProdCrManualBk13RecentSatisfiedMon);
            BindMonthDropDown(sProdCrManualBk7RecentFileMon);
            BindMonthDropDown(sProdCrManualBk7RecentSatisfiedMon);
            BindMonthDropDown(sProdCrManualForeclosureRecentFileMon);
            BindMonthDropDown(sProdCrManualForeclosureRecentSatisfiedMon);

            BindYearDropDown(sProdCrManualBk13RecentFileYr);
            BindYearDropDown(sProdCrManualBk13RecentSatisfiedYr);
            BindYearDropDown(sProdCrManualBk7RecentFileYr);
            BindYearDropDown(sProdCrManualBk7RecentSatisfiedYr);
            BindYearDropDown(sProdCrManualForeclosureRecentFileYr);
            BindYearDropDown(sProdCrManualForeclosureRecentSatisfiedYr);

            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualBk13RecentStatusT);
            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualBk7RecentStatusT);
            Tools.Bind_sProdCrManualDerogRecentStatusT(sProdCrManualForeclosureRecentStatusT);
            Tools.Bind_sProdAvailReserveMonths(sProdAvailReserveMonths);

            Tools.Bind_sProdSpT(sProdSpT);
            Tools.Bind_sProdSpStructureT(sProdSpStructureT);
            Tools.Bind_sLPurposeT(sLPurposeTPe);
            Tools.Bind_sProd3rdPartyUwResultT(sProd3rdPartyUwResultT);
            Tools.Bind_aOccT(sOccTPe);
			Tools.Bind_sProdMIOptionT(sProdMIOptionT);
            Tools.Bind_sProdConvMIOptionT(sProdConvMIOptionT, Broker.EnabledBPMISinglePremium_Case115836, Broker.EnableBPMISplitPremium_Case115836);
			m_isShowsProdEstimatedResidualI = Broker.PmlRequireEstResidualIncome;

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            Tools.Bind_sConvSplitMIRT(sConvSplitMIRT);

            Tools.Bind_sLenderFeeBuyoutRequestedT(sLenderFeeBuyoutRequestedT);
        }
        private void BindLateDatesDropDown(DropDownList ddl) 
        {
            for (int i = 0; i < 13; i++)
                ddl.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        private void BindMonthDropDown(DropDownList ddl) 
        {
            ddl.Items.Add(new ListItem("", ""));
            for (int i = 1; i < 13; i++) 
            {
                ddl.Items.Add(new ListItem(string.Format("{0:0#}", i), i.ToString()));
            }
        }
        private void BindYearDropDown(DropDownList ddl) 
        {
            ddl.Items.Add(new ListItem("", ""));
            // 7/1/2005 dd - Drop down list will be from 1996 to current year.
            int currentYear = DateTime.Now.Year;
            for (int i = 1996; i <= currentYear; i++) 
            {
                ddl.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(LoanID, typeof(PMLDefaultValues));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(0);

            this.RegisterJsGlobalVariables("IsTargeting2019Ulad", dataLoan.sIsTargeting2019Ulad);

            var borrCitizenshipValues = dataApp.GetApplicableCitizenshipValues(E_BorrowerModeT.Borrower);
            Tools.Bind_aProdCitizenT(aProdBCitizenT, borrCitizenshipValues);

            Tools.Bind_sProdDocT(sProdDocT, this.Broker, dataLoan.sProdDocT);

            LenderFeeBuyoutPlaceHolder.Visible = Broker.IsLenderFeeBuyoutEnabledForBranchChannel(dataLoan.sBranchChannelT);

            m_isAutoPmiOrNewPml = Broker.HasEnabledPMI || dataLoan.IsNewPMLEnabled;

            // Step 1 - Credit Information
            sCreditScoreEstimatePe.Text = dataLoan.sCreditScoreEstimatePe_rep;
            aBExperianScorePe.Text = dataApp.aBExperianScorePe_rep;
            aBTransUnionScorePe.Text = dataApp.aBTransUnionScorePe_rep;
            aBEquifaxScorePe.Text = dataApp.aBEquifaxScorePe_rep;
            aCExperianScorePe.Text = dataApp.aCExperianScorePe_rep;
            aCTransUnionScorePe.Text = dataApp.aCTransUnionScorePe_rep;
            aCEquifaxScorePe.Text = dataApp.aCEquifaxScorePe_rep;
            sTransmOMonPmtPe.Text = dataLoan.sTransmOMonPmtPe_rep;
            Tools.SetDropDownListValue(sProdCrManualNonRolling30MortLateCount, dataLoan.sProdCrManualNonRolling30MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual60MortLateCount, dataLoan.sProdCrManual60MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual90MortLateCount, dataLoan.sProdCrManual90MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual120MortLateCount, dataLoan.sProdCrManual120MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual150MortLateCount, dataLoan.sProdCrManual150MortLateCount_rep);
            Tools.SetDropDownListValue(sProdCrManual30MortLateCount, dataLoan.sProdCrManual30MortLateCount_rep);
			Tools.SetDropDownListValue(sProdCrManualRolling60MortLateCount, dataLoan.sProdCrManualRolling60MortLateCount_rep);
			Tools.SetDropDownListValue(sProdCrManualRolling90MortLateCount, dataLoan.sProdCrManualRolling90MortLateCount_rep);
            sProdCrManualForeclosureHas.Checked= dataLoan.sProdCrManualForeclosureHas;
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentFileMon, dataLoan.sProdCrManualForeclosureRecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentFileYr, dataLoan.sProdCrManualForeclosureRecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentStatusT, dataLoan.sProdCrManualForeclosureRecentStatusT);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentSatisfiedMon, dataLoan.sProdCrManualForeclosureRecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualForeclosureRecentSatisfiedYr, dataLoan.sProdCrManualForeclosureRecentSatisfiedYr_rep);
            sProdCrManualBk7Has.Checked = dataLoan.sProdCrManualBk7Has;
            Tools.SetDropDownListValue(sProdCrManualBk7RecentFileMon, dataLoan.sProdCrManualBk7RecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentFileYr, dataLoan.sProdCrManualBk7RecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentStatusT, dataLoan.sProdCrManualBk7RecentStatusT);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentSatisfiedMon, dataLoan.sProdCrManualBk7RecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk7RecentSatisfiedYr, dataLoan.sProdCrManualBk7RecentSatisfiedYr_rep);
            sProdCrManualBk13Has.Checked = dataLoan.sProdCrManualBk13Has;
            Tools.SetDropDownListValue(sProdCrManualBk13RecentFileMon, dataLoan.sProdCrManualBk13RecentFileMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentFileYr, dataLoan.sProdCrManualBk13RecentFileYr_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentStatusT, dataLoan.sProdCrManualBk13RecentStatusT);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentSatisfiedMon, dataLoan.sProdCrManualBk13RecentSatisfiedMon_rep);
            Tools.SetDropDownListValue(sProdCrManualBk13RecentSatisfiedYr, dataLoan.sProdCrManualBk13RecentSatisfiedYr_rep);

            // Step 2 - Applicant Information
            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBSsn.Text = dataApp.aBSsn;
            Tools.SetDropDownListValue(aProdBCitizenT, dataApp.aProdBCitizenT);
            aCFirstNm.Text = dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCSsn.Text = dataApp.aCSsn;
            aIsBorrSpousePrimaryWageEarner.Checked = dataApp.aIsBorrSpousePrimaryWageEarner;
            sIsSelfEmployed.Checked = dataLoan.sIsSelfEmployed;
            aLiquidAssetsPe.Text = dataApp.aLiquidAssetsPe_rep;
            sOpNegCfPe.Text = dataLoan.sOpNegCfPe_rep;

            // Step 3 - Property & Loan
            sSpStatePe.Value = dataLoan.sSpStatePe;
            sProRealETxPe.Text = dataLoan.sProRealETxPe_rep;
            sProOHExpPe.Text = dataLoan.sProOHExpPe_rep;
            sOccRPe.Text = dataLoan.sOccRPe_rep;
            sSpGrossRentPe.Text = dataLoan.sSpGrossRentPe_rep;
            sProdCondoStories.Text = dataLoan.sProdCondoStories_rep;
            sProdIsSpInRuralArea.Checked = dataLoan.sProdIsSpInRuralArea;
            sProdIsCondotel.Checked = dataLoan.sProdIsCondotel;
            sProdIsNonwarrantableProj.Checked = dataLoan.sProdIsNonwarrantableProj;
            aPresOHExpPe.Text = dataApp.aPresOHExpPe_rep;
            sProdCashoutAmt.Text = dataLoan.sProdCashoutAmt_rep;
            sHas1stTimeBuyerPe.Checked = dataLoan.sHas1stTimeBuyerPe;
            sNumFinancedProperties.Text = dataLoan.sNumFinancedProperties_rep;
            sProdImpound.Checked = dataLoan.sProdImpound;
            sPrimAppTotNonspIPe.Text = dataLoan.sPrimAppTotNonspIPe_rep;
            if (this.Broker.IsEnableRenovationCheckboxInPML || this.Broker.EnableRenovationLoanSupport)
            {
                sIsRenovationLoan.Checked = dataLoan.sIsRenovationLoan;
                sTotalRenovationCosts.Text = dataLoan.sTotalRenovationCosts_rep;
                sTotalRenovationCostsLckd.Checked = dataLoan.sTotalRenovationCostsLckd;
            }
            sProdRLckdDays.Text = dataLoan.sProdRLckdDays_rep;
            Tools.SetDropDownListValue(sProdAvailReserveMonths, dataLoan.sProdAvailReserveMonths_rep);
            Tools.SetDropDownListValue(sProdDocT, dataLoan.sProdDocT);
            Tools.SetDropDownListValue(sLPurposeTPe, dataLoan.sLPurposeTPe);
            Tools.SetDropDownListValue(sProdSpT, dataLoan.sProdSpT);
            this.sIsNotPermanentlyAffixed.Checked = dataLoan.sIsNotPermanentlyAffixed;
            Tools.SetDropDownListValue(sProdSpStructureT, dataLoan.sProdSpStructureT);

            Tools.SetDropDownListValue(sOccTPe, dataLoan.sOccTPe);

            Tools.SetDropDownListValue(sProd3rdPartyUwResultT, dataLoan.sProd3rdPartyUwResultT);

            Tools.SetDropDownListValue(sProdConvMIOptionT, dataLoan.sProdConvMIOptionT);
            Tools.SetDropDownListValue(sProdMIOptionT, dataLoan.sProdMIOptionT);
            Tools.SetDropDownListValue(sConvSplitMIRT, dataLoan.sConvSplitMIRT);

            Tools.SetDropDownListValue(sLenderFeeBuyoutRequestedT, dataLoan.sLenderFeeBuyoutRequestedT);

            sIsStudentLoanCashoutRefi.Checked = dataLoan.sIsStudentLoanCashoutRefi;

            sProdEstimatedResidualI.Text = dataLoan.sProdEstimatedResidualI_rep;

            sProdFilterDue10Yrs.Checked = dataLoan.sProdFilterDue10Yrs;
			sProdFilterDue15Yrs.Checked = dataLoan.sProdFilterDue15Yrs;
            sProdFilterDue20Yrs.Checked = dataLoan.sProdFilterDue20Yrs;
            sProdFilterDue25Yrs.Checked = dataLoan.sProdFilterDue25Yrs;
            sProdFilterDue30Yrs.Checked = dataLoan.sProdFilterDue30Yrs;			
			sProdFilterDueOther.Checked = dataLoan.sProdFilterDueOther;
			sProdFilterFinMethFixed.Checked = dataLoan.sProdFilterFinMethFixed;
			//sProdFilterFinMethOptionArm.Checked = dataLoan.sProdFilterFinMethOptionArm; //opm 22068 05/29/08 fs			
			sProdFilterFinMeth3YrsArm.Checked = dataLoan.sProdFilterFinMeth3YrsArm;
			sProdFilterFinMeth5YrsArm.Checked = dataLoan.sProdFilterFinMeth5YrsArm;
            sProdFilterFinMeth7YrsArm.Checked = dataLoan.sProdFilterFinMeth7YrsArm;
            sProdFilterFinMeth10YrsArm.Checked = dataLoan.sProdFilterFinMeth10YrsArm;
			sProdFilterFinMethOther.Checked = dataLoan.sProdFilterFinMethOther;
            sProdFilterPmtTPI.Checked = dataLoan.sProdFilterPmtTPI;
            sProdFilterPmtTIOnly.Checked = dataLoan.sProdFilterPmtTIOnly;
            sProdIncludeMyCommunityProc.Checked = dataLoan.sProdIncludeMyCommunityProc;
            sProdIncludeHomePossibleProc.Checked = dataLoan.sProdIncludeHomePossibleProc;
            sProdIncludeNormalProc.Checked = dataLoan.sProdIncludeNormalProc;
            sProdIncludeFHATotalProc.Checked = dataLoan.sProdIncludeFHATotalProc;
            sProdIncludeUSDARuralProc.Checked = dataLoan.sProdIncludeUSDARuralProc;
            sProdIncludeVAProc.Checked = dataLoan.sProdIncludeVAProc;

            bool isAdvancedFiltersEnabled = Broker.IsAdvancedFilterOptionsForPricingEnabled(PrincipalFactory.CurrentPrincipal);
            advancedFilterBtn.Visible = isAdvancedFiltersEnabled;
            ProductCodeFilterPopup.Visible = isAdvancedFiltersEnabled;
            ProductCodeFilterPopup.EnableAdvancedFilterOptionsForPriceEngineResults = isAdvancedFiltersEnabled;
            if (isAdvancedFiltersEnabled)
            {
                ProductCodeFilterPopup.SelectedProductCodes = dataLoan.sSelectedProductCodeFilter;
                ProductCodeFilterPopup.AvailableProductCodes = dataLoan.sAvailableProductCodeFilter;
                ProductCodeFilterPopup.ProductCodesByFileType = dataLoan.sProductCodesByFileType;
            }

            sIsCreditQualifying.Checked = dataLoan.sIsCreditQualifying;

            PopulateCustomPmlFieldTable(dataLoan);

            this.RegisterJsGlobalVariables("isLoanTypeFHA", dataLoan.sLT == E_sLT.FHA);
            this.RegisterJsGlobalVariables("IsEnableRenovationCheckboxInPML", this.Broker.IsEnableRenovationCheckboxInPML);
            this.RegisterJsGlobalVariables("EnableRenovationLoanSupport", this.Broker.EnableRenovationLoanSupport);
        }

        private void PopulateCustomPmlFieldTable(CPageData dataLoan)
        {
            var list = Broker.CustomPmlFieldList;

            foreach (CustomPmlField field in list.FieldsSortedByRank)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell labelCell = new HtmlTableCell();
                labelCell.Attributes["class"] = "FieldLabel CustomPmlFieldLabel";
                HtmlTableCell fieldCell = new HtmlTableCell();
                HtmlTableCell visibilityCell = new HtmlTableCell();

                if (field.IsValid)
                {
                    Literal label = new Literal();
                    label.Text = field.Description;
                    labelCell.Controls.Add(label);

                    WebControl control = CustomPmlFieldUtilities.GetControlForFieldType(field.Type);
                    control.ID = String.Format("sCustomPMLField{0}", field.KeywordNum);
                    CustomPmlFieldUtilities.SetControlAttributesForFieldType(control, field.Type);

                    if (control is DropDownList)
                    {
                        CustomPmlFieldUtilities.BindCustomFieldDDL(control as DropDownList, field);
                    }

                    string fieldValue = dataLoan.GetCustomPMLFieldRep(field.KeywordNum);
                    CustomPmlFieldUtilities.SetControlValue(control, fieldValue);

                    fieldCell.Controls.Add(control);

                    label = new Literal();
                    label.Text = field.VisibilityType_rep;
                    visibilityCell.Controls.Add(label);
                }
                else
                {
                    // Add a row with the blank description.
                    Literal label = new Literal();
                    label.Text = "--Unused Custom PML Field--";
                    labelCell.Controls.Add(label);

                    label = new Literal();
                    label.Text = "Please contact support to setup and enable";
                    fieldCell.Controls.Add(label);

                    label = new Literal();
                    label.Text = "N/A";
                    visibilityCell.Controls.Add(label);
                }

                row.Controls.Add(labelCell);
                row.Controls.Add(fieldCell);
                row.Controls.Add(visibilityCell);
                CustomPMLFieldsTable.Rows.Add(row);
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            this.EnableJqueryMigrate = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
