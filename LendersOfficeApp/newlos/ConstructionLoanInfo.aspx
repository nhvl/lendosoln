﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConstructionLoanInfo.aspx.cs" Inherits="LendersOfficeApp.newlos.ConstructionLoanInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="Rfp" TagName="RateFloorPopup" Src="~/newlos/Forms/RateFloorPopup.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style>
        input[preset="date"]:read-only {
            box-sizing: border-box;
            height: 18px;
            vertical-align: middle;
        }

        #DrawScheduleTable td {
            border: 1px solid black;
            padding-left: 2px;
            padding-right: 2px;
        }

        .constructionDataPoints {
            padding-top: 5px;
        }

        .layout-grid {
            display: flex;
            display: -ms-flexbox;
            -ms-flex-wrap: wrap;
            flex-wrap:wrap;
            max-width: 1000px;
            
        }

            .layout-grid > div {
                flex: 0 0 50%;
                -ms-flex: 0 0 50%;
            }

        .split-half {
            display: flex;
            display: -ms-flexbox;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            padding-left: 5px;
            padding-bottom: 3px;
        }
        
            .split-half label, .split-half > div {
                flex: 0 0 50%;
                -ms-flex: 0 0 50%;
                display: inline-block;
            }

        .arm-section {
            border: 1px solid black;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .arm-section .FormTableSubheader {
            border-bottom: 1px solid black;
            padding: 5px;
            margin-bottom: 10px;
        }

        label {
            font-weight: bold;
        }

        .ui-dialog
        {
            border: solid 1px black !important;
        }

        .ui-dialog-titlebar
        {
            background-color:Maroon !important;
        }

        #AprDebugDiv
        {
            display: none;
        }

        #sAprDebugResult
        {
            white-space:pre;
            height: 100%;
            width: 100%;
            overflow: auto;
            text-align: left;
            padding: 5px;
            box-sizing: border-box;
        }

        #dateWarningPopup {
            display: none;
        }

        #dateWarningPopupInnerDiv {
            text-align: left;
            box-sizing: border-box;
            padding: 5px;
        }
    </style>
    
 
    
</head>
<body style="background-color: gainsboro" onload="init();">

   <script>
   
   var maxId = -1;
   
   function __IE8NullFix(v) {
        return v === "" ? "" : v; //http://blogs.msdn.com/jscript/archive/2009/06/23/serializing-the-value-of-empty-dom-elements-using-native-json-in-ie8.aspx
    }
    
            function f_save() {
                f_saveMe();
                return true;
            }
            
            function saveMe() {
                f_saveMe();
                return true;
            }
            function f_saveMe() {
                var data = getAllFormValuesWithPostCallbacks("", true);
                
                data.LoanId = document.getElementById( 'loanid' ).value;
                data.sFileVersion = document.getElementById('sFileVersion').value;
                data.Draws = serializeDraws(data); 
                
                var result = gService.loanedit.call('SaveData', data);
                clearDirty();
                return !result.error;      
            }
   
   function serializeDraws(data)
   {
        var rows = DrawScheduleTable.rows;
        var draws = [];
        for(var i = 1; i < rows.length; i++)
        {
            var index = rows[i].getAttribute("index");
            var draw = {
                Condition: this.__IE8NullFix(document.getElementById('condition' + index).innerText),
                Timing: this.__IE8NullFix(document.getElementById('timing' + index).value),
                Percent: this.__IE8NullFix(document.getElementById('percent' + index).value),
                Basis: this.__IE8NullFix(document.getElementById('basis' + index).value),
                FixedAmount: this.__IE8NullFix(document.getElementById('fixedAmount' + index).value),
                Total: this.__IE8NullFix(document.getElementById('total' + index).value)
            };
            draws.push(draw);
        }
        
        return JSON.stringify(draws);
   }
   
   
   function recalculateTotal(id)
   {
        var fixedAmountElem = document.getElementById("fixedAmount" + id);
        var percentElem = document.getElementById("percent" + id);
        
        fixedAmountElem.blur();
        percentElem.blur();
   
        var fixedAmount = fixedAmountElem.value.replace('$', '').replace(',','');
        var percent = percentElem.value.replace('%', '').replace(',','');
        var basis=document.getElementById("basis" + id).value;
        
        var args = new Object();
        args["fixedAmount"] = fixedAmount;
        args["percent"] = percent;
        args["basis"] = basis;
        args["LoanId"] = <%=AspxTools.JsString(LoanID) %>;
        
        var result = gService.loanedit.call("calculateTotal", args);
        
        var total = document.getElementById("total" + id);
        total.value = result.value["total"];
        total.blur();
   }
   
   
   
        function deleteAdjustment()
        {
            var rows = DrawScheduleTable.rows;
            var i = 1;
            while( i < rows.length)
            {
                var index = rows[i].getAttribute("index");
                var checkbox = document.getElementById("deleteCheckbox" + index);
                if(checkbox.checked)
                    DrawScheduleTable.deleteRow(i);
                else   
                    i++;
            }
            
            document.getElementById("deleteAll").checked = false;
        }
   
        function addAdjustment() {
            //generate a new row
            var rowIndex = DrawScheduleTable.rows.length;
            var index = maxId;
            var row = DrawScheduleTable.insertRow(rowIndex);
            row.setAttribute("index", index);
            if (index % 2 == 0)
                row.className = "GridAlternatingItem";
            else
                row.className = "GridItem";
                
                maxId++;
            
            var cell = row.insertCell(0);
            addInput(cell, "money", "total" + index, true, index);
            
            cell = row.insertCell(0);
            addInput(cell, "money", "fixedAmount" + index, false, index);

            cell = row.insertCell(0);
            ddl = document.createElement("select");
            ddl.setAttribute("SkipMe", "");
            ddl.id = "basis" + index;
            addOption(ddl, "Loan Amount",<%=AspxTools.JsNumeric(E_PercentBaseT.LoanAmount)%>);
            addOption(ddl, "Purchase Price", <%=AspxTools.JsNumeric(E_PercentBaseT.SalesPrice) %>);
            addOption(ddl, "Appraisal Value", <%=AspxTools.JsNumeric(E_PercentBaseT.AppraisalValue)%>);
            addOption(ddl, "Original Cost", <%=AspxTools.JsNumeric(E_PercentBaseT.OriginalCost)%>);
            addOption(ddl, "Total Loan Amount", <%=AspxTools.JsNumeric(E_PercentBaseT.TotalLoanAmount)%>);
            _initDynamicInput(ddl);
            
                    addEventHandler(ddl, "change", function(){recalculateTotal(index);}, true);
            cell.appendChild(ddl);
             
            cell = row.insertCell(0);
            addInput(cell, "percent", "percent" + index, false, index);

            cell = row.insertCell(0);
            ddl = document.createElement("select");
            ddl.setAttribute("SkipMe", "");
            ddl.id = "basis" + index;
            ddl.id = "timing" + index;
            addOption(ddl, "", <%=AspxTools.JsNumeric(E_Timing.Blank) %>);
            addOption(ddl, "Before Closing",<%=AspxTools.JsNumeric(E_Timing.Before_Closing)%>);
            addOption(ddl, "At Closing", <%=AspxTools.JsNumeric(E_Timing.At_Closing) %>);
            addOption(ddl, "After Closing", <%=AspxTools.JsNumeric(E_Timing.After_Closing )%>);
            _initDynamicInput(ddl);
            cell.appendChild(ddl);
            
            cell = row.insertCell(0);
            addInput(cell, "text", "condition" + index, false, index);
            
            cell = row.insertCell(0);
            addInput(cell, "checkbox", "deleteCheckbox" + index, false, index);
            
            
            updateDirtyBit();
        }

        function addOption(ddl, name, value) {
            var option = document.createElement("option");
            option.value = value;
            option.innerText = name;
            ddl.appendChild(option);
        }

        function addInput(cell, type, id, isReadOnly, index) {
            var input = document.createElement("input");
            input.id = id;
            switch (type) {
                case "checkbox":
                    input.type = "checkbox";
                    break;
                case "text":
                    var textArea = document.createElement("textarea");
                    textArea.style.width = "400px";
                    textArea.id = id;
                    cell.appendChild(textArea);
                    _initDynamicInput(textArea);
                    return;
                case "percent":
                    input.type = "text";
                    input.setAttribute("preset", "percent");
                    input.style.width = "70px";
                    break;
                case "money":
                    input.type = "text";
                    input.setAttribute("preset", "money");
                    input.style.width = "90px";
                    
                    break;
            }
            if(id==="percent" + index || id==="fixedAmount" + index )
            {
                    addEventHandler(input, "change", function(){recalculateTotal(index);}, true);
            }
            input.readOnly = isReadOnly;
            _initDynamicInput(input);
            input.setAttribute("SkipMe", "");
            cell.appendChild(input);
            
           
        }

        function selectAll() {
            var allCheckbox = document.getElementById("deleteAll");
            var inputs = document.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type === "checkbox")
                    inputs[i].checked = allCheckbox.checked;
            }
        }

        var $borrowerOption;
        var $borrowerSibling;
       
        function _init()
        {
            lockField(<%= AspxTools.JsGetElementById(sConstructionLoanDLckd) %>, 'sConstructionLoanD');
            lockField(<%= AspxTools.JsGetElementById(sConstructionIntAccrualDLckd) %>, 'sConstructionIntAccrualD');
            lockField(<%= AspxTools.JsGetElementById(sConstructionFirstPaymentDLckd) %>, 'sConstructionFirstPaymentD');

            if (typeof (RateFloorPopupInit) == "function")
            {
                RateFloorPopupInit();
            }
        }

        function init()
        {
            maxId = DrawScheduleTable.rows.length - 1;
            $("#sConstructionAmortT").on("change", function(){ 
                $(".arm-section").toggle(this.value == ML.ConstructionAmortTAdjustable); });
            $("#sConstructionAmortT").change();
            $("#sConstructionPurposeT").on("change", function(){ 
                isConstructionAndPurchase = this.value == ML.ConstructionPurposeConstructionAndLotPurchase;
                if(!$borrowerOption)
                {
                    $borrowerOption = $(".borrower-item");
                    $borrowerOptionSibling = $borrowerOption.next();
                }

                isConstructionAndPurchase ? $borrowerOption.remove() : $borrowerOptionSibling.before($borrowerOption);
                
                $("#sLotOwnerT").prop("disabled", !isConstructionAndPurchase); 
                $(".land-cost").toggle(isConstructionAndPurchase);
                $(".land-value").toggle(!isConstructionAndPurchase);
            });
            $("#sConstructionPurposeT").change();

            $(".layout-grid").find("input, select").not("input[preset='date']").on("change", refreshCalculation);
            $(".layout-grid").find("input[type='date']").on("blur", refreshCalculation);

            $(".constructionDataPoints").toggle(ML.AreConstructionLoanDataPointsMigrated);
        }

        function openAprDebug() {
            var data = getAllFormValuesWithPostCallbacks("", true);
            var result = gService.loanedit.callAsyncSimple("GetAprDebugText", data, openAprDebug_callback);
        }

        function openAprDebug_callback(result) {
            if (result.error) {
                alert(result.UserMessage);
                return;
            }

            $("#sAprDebugResult").text(result.value.sAprDebugResult);
            LQBPopup.ShowElement($("#AprDebugDiv"), { width: 500, height: 400 });
       }

       function checkInterestAccrualDate() {
           refreshCalculation();

           var dateWarning = $("#dateWarning").val();
           
           if (dateWarning) {
               $("#dateWarningPopupInnerDiv").text(dateWarning);
               LQBPopup.ShowElement($("#dateWarningPopup"), { width: 400, height: 50 });
           }
       }
    </script>


    <form id="form1" runat="server" style="width: 100%;">
        <h4 class="page-header">Construction Loan Info</h4>

        <input type="hidden" id="dateWarning" />

        <div id="dateWarningPopup">
            <h4 class="page-header">Warning</h4>
            <div id="dateWarningPopupInnerDiv"></div>
        </div>

        <div id="AprDebugDiv">
            <h4 class="page-header">APR Details</h4>
            <div id="sAprDebugResult"></div>
        </div>

        <div class="layout-grid constructionDataPoints">
            <div>
                <div class="split-half">
                    <label for="sConstructionPurposeT">Loan Purpose</label>
                    <asp:DropDownList Enabled="false" ID="sLPurposeT" runat="server"></asp:DropDownList>
                </div>
                <div class="split-half">
                    <label for="sConstructionPurposeT">Construction Purpose</label>
                    <asp:DropDownList runat="server" ID="sConstructionPurposeT"></asp:DropDownList>
                </div>
                <div class="split-half">
                    <label for="sLotOwnerT">Lot Owner</label>
                    <asp:DropDownList runat="server" ID="sLotOwnerT"></asp:DropDownList>
                </div>
                <div class="split-half">
                    <label for="sBuildingStatusT">Construction Status</label>
                    <asp:DropDownList runat="server" ID="sBuildingStatusT"></asp:DropDownList>
                </div>
                <div class="split-half">
                    <label for="sConstructionMethodT">Construction Method</label>
                    <asp:DropDownList runat="server" ID="sConstructionMethodT"></asp:DropDownList>
                </div>
                <div class="split-half">
                    <label for="sConstructionPeriodMon">Construction Term (mths)</label>
                    <input preset="numeric" type="text" runat="server" ID="sConstructionPeriodMon"></input>
                </div>
                <div class="split-half">
                    <label for="sConstructionPeriodIR">Interest Rate</label>
                    <ml:PercentTextBox runat="server" ID="sConstructionPeriodIR"></ml:PercentTextBox>
                </div>
                <div class="split-half">
                    <label for="sConstructionPhaseIntAccrualT">Interest Accrual</label>
                    <asp:DropDownList runat="server" ID="sConstructionPhaseIntAccrualT"></asp:DropDownList>
                </div>
                <div class="split-half">
                    <label for="sConstructionIntCalcT">Interest Calculation</label>
                    <asp:DropDownList runat="server" ID="sConstructionIntCalcT"></asp:DropDownList>
                </div>
                <div class="split-half">
                    <label for="sConstructionAmortT">Amortization Type</label>
                    <asp:DropDownList runat="server" ID="sConstructionAmortT"></asp:DropDownList>
                </div>
            </div>
            <div>
                <div class="split-half">
                    <label for="sLotImprovC">Construction Cost</label>
                    <ml:MoneyTextBox type="text" preset="money" runat="server" ID="sLotImprovC" />
                </div>
                <div class="split-half land-cost">
                    <label for="sLandCost">Land Cost</label>
                    <ml:MoneyTextBox type="text" preset="money" runat="server" ID="sLandCost" />
                </div>
                <div class="split-half land-value">
                    <label for="sLotVal">Land Value</label>
                    <ml:MoneyTextBox type="text" preset="money" runat="server" ID="sLotVal" />
                </div>
                <div class="split-half">
                    <label for="sSubsequentlyPaidFinanceChargeAmt">Subsequently Paid Finance Charges</label>
                    <ml:MoneyTextBox type="text" preset="money" runat="server" ID="sSubsequentlyPaidFinanceChargeAmt" />
                </div>
                <div class="split-half">
                    <label for="sConstructionInitialAdvanceAmt">Initial Advance</label>
                    <ml:MoneyTextBox type="text" preset="money" runat="server" ID="sConstructionInitialAdvanceAmt"/>
                </div>
                <div class="split-half">
                    <label for="sConstructionLoanD">Loan Date</label>
                    <span>
                        <ml:DateTextbox type="text" preset="date" runat="server" ID="sConstructionLoanD" onchange="checkInterestAccrualDate();"/>
                        <input type="checkbox" runat="server" id="sConstructionLoanDLckd" />
                        <label for="sConstructionLoanDLckd">Lock</label>
                    </span>
                </div>
                <div class="split-half">
                    <label for="sConstructionIntAccrualD">Interest Accrual Date</label>
                    <span>
                        <ml:DateTextbox type="text" preset="date" runat="server" ID="sConstructionIntAccrualD" onchange="checkInterestAccrualDate();"/>
                        <input type="checkbox" runat="server" id="sConstructionIntAccrualDLckd" />
                        <label for="sConstructionIntAccrualDLckd">Lock</label>
                    </span>
                </div>
                <div class="split-half">
                    <label for="sConstructionFirstPaymentD">First Payment Date</label>
                    <span>
                        <ml:DateTextbox type="text" preset="date" runat="server" ID="sConstructionFirstPaymentD" onchange="refreshCalculation();"/>
                        <input type="checkbox" runat="server" id="sConstructionFirstPaymentDLckd" />
                        <label for="sConstructionFirstPaymentDLckd">Lock</label>
                    </span>
                </div>
                <div class="split-half">
                    <label for="sIsIntReserveRequired">Is Interest Reserve Required</label>
                    <input type="checkbox" runat="server" ID="sIsIntReserveRequired"/>
                </div>
                <div class="split-half">
                    <label for="sIntReserveAmt">Interest Reserve Amount</label>
                    <ml:MoneyTextBox type="text" preset="money" runat="server" ID="sIntReserveAmt" onchange="refreshCalculation();"/>
                </div>
            </div>

            <div class="arm-section">
                <div class="FormTableSubheader">Adjustable Rate Mortgage</div>
                <div class="split-half">
                    <label for="sConstructionRAdj1stCapR">1st Adj Cap</label>
                    <input type="text" runat="server" id="sConstructionRAdj1stCapR" />
                </div>
                <div class="split-half">
                    <label for="sConstructionRAdj1stCapMon">1st Change</label>
                    <div>
                    <input type="text" runat="server" id="sConstructionRAdj1stCapMon" />
                     mths
                    </div>
                </div>
                <div class="split-half">
                    <label for="sConstructionRAdjCapR">Adj Cap</label>
                    <input type="text" runat="server" id="sConstructionRAdjCapR" />
                </div>
                <div class="split-half">
                    <label for="sConstructionRAdjCapMon">Adj Period</label>
                    <div>
                        <input type="text" runat="server" id="sConstructionRAdjCapMon" />
                        mths
                    </div>
                </div>
                <div class="split-half">
                    <label for="sConstructionRAdjLifeCapR">Life Adj Cap</label>
                    <input type="text" runat="server" id="sConstructionRAdjLifeCapR" />
                </div>
                <div class="split-half">
                    <label for="sConstructionRAdjMarginR">Margin</label>
                    <input type="text" runat="server" id="sConstructionRAdjMarginR" />
                </div>
                <div class="split-half">
                    <label for="sConstructionArmIndexNameVstr">Index</label>
                </div>
                <div class="split-half">
                    <div>
                        <input type="text" runat="server" id="sConstructionArmIndexNameVstr" />
                    </div>
                    <input type="text" runat="server" id="sConstructionRAdjIndexR" />
                </div>
                <div class="split-half">
                    <div>
                        <label for="sConstructionRAdjFloorR">Rate Floor</label>
                        <a id="RateFloorCalcLink">calculate</a>
                    </div>
                    <input type="text" runat="server" id="sConstructionRAdjFloorR" readonly="readonly" />
                </div>
                <div class="split-half">
                    <div>
                        <label>Round</label>
                        <asp:DropDownList runat="server" ID="sConstructionRAdjRoundT" />
                    </div>
                    <input type="text" runat="server" id="sConstructionRAdjRoundToR" />
                </div>
            </div>
        </div>

        <div class="layout-grid constructionDataPoints">
            <div>
                <div class="split-half">
                    <label>Interest Amount</label>
                    <ml:MoneyTextBox runat="server" ID="sConstructionIntAmount" Enabled="false"></ml:MoneyTextBox>
                </div>
            </div>
        </div>

        <div class="layout-grid constructionDataPoints">
            <div>
                <div class="split-half">
                    <label>APR</label>
                    <span>
                        <ml:MoneyTextBox runat="server" ID="sApr" Enabled="false"></ml:MoneyTextBox>
                        &nbsp;<a onclick="openAprDebug();">View Details</a>
                    </span>
                </div>
            </div>
        </div>


        <p class="FormTableSubheader" style="padding-top: 5px; padding-bottom: 5px; padding-left: 5px;">Construction Draw Schedule</p>

        <div>
            <table id="DrawScheduleTable" runat="server" style="border: 1px solid black; border-collapse: collapse; text-align: center;">
                <tr class="GridHeader">
                    <td style="width: 20px">
                        <input type="checkbox" id="deleteAll" onclick="selectAll()" /></td>
                    <td style="width: 420px">Condition</td>
                    <td style="width: 110px">Timing</td>
                    <td style="width: 85px">Percent</td>
                    <td style="width: 130px">Base</td>
                    <td style="width: 105px">Fixed Amount</td>
                    <td style="width: 100px">Total</td>
                </tr>
            </table>

            <input type="button" style="margin-top: 2px;" value="Add new draw" onclick="addAdjustment();" />
            <input type="button" style="margin-top: 2px;" value="Delete selected draws" onclick="deleteAdjustment()" />
        </div>

    <Rfp:RateFloorPopup runat="server" ID="RateFloorPopup" />
    </form>
</body>
</html>
