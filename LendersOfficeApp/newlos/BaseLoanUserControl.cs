using System;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos
{
	public class BaseLoanUserControl : System.Web.UI.UserControl
	{
        protected Guid LoanID 
        {
            get { return RequestHelper.LoanID; }
        }

        protected int sFileVersion
        {
            get { return ((LendersOfficeApp.newlos.BaseLoanPage)this.Page).sFileVersion; }
            set { ((LendersOfficeApp.newlos.BaseLoanPage)this.Page).sFileVersion = value; }
        }

        protected Guid ApplicationID 
        {
            get { return RequestHelper.GetGuid("appid"); }
        }

        protected bool IsReadOnly 
        {
            get { return ((LendersOfficeApp.newlos.BaseLoanPage) this.Page).IsReadOnly; }
        }

        ////protected AccessBinding AccessBinding 
        ////{
        ////    get { return ((LendersOfficeApp.newlos.BaseLoanPage) this.Page).AccessBinding; }
        ////}

        /// <summary>
        /// Return application id from last request.
        /// This is useful in SaveData() where we need to save the previous ApplicationID not application id
        /// from request query string.
        /// </summary>
        protected Guid OldApplicationID 
        {
            get { return (Guid) ViewState["oldappid"]; }
        }

        protected override void OnPreRender(System.EventArgs e) 
        {
            // Store application id to old application id variable.
            ViewState["oldappid"] = RequestHelper.GetGuid("appid", Guid.Empty); // Not all control required application id.
            base.OnPreRender(e);
        }

        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
        }


        private BrokerDB x_brokerDB = null;

        protected BrokerDB BrokerDB
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = this.BrokerUser.BrokerDB;
                }

                return x_brokerDB;
            }
        }

        protected bool IsOnLeadPage
        {
            get { return RequestHelper.GetSafeQueryString("isLead") == "t"; } //! may need to check for exceptions
        }
	}
}
