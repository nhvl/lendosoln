﻿namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendersOffice.Security;

    public class BigLoanInfoServiceItem : AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CalculateAge":
                    CalculateAge();
                    break;
                case "GenerateNewLoanNumber":
                    GenerateNewLoanNumber();
                    break;
            }
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(BigLoanInfoServiceItem));
        }

        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        #region Borrower Info

        private void BindBorrowerInfo(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aBAliases = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("aBAlias"));
            dataApp.aCAliases = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("aCAlias"));
            dataApp.aBPowerOfAttorneyNm = GetString("aBPowerOfAttorneyNm");
            dataApp.aCPowerOfAttorneyNm = GetString("aCPowerOfAttorneyNm");

            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBPreferredNm = GetString("aBPreferredNm");
            dataApp.aBPreferredNmLckd = GetBool("aBPreferredNmLckd");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aCFirstNm = GetString("aCFirstNm");
            dataApp.aCPreferredNm = GetString("aCPreferredNm");
            dataApp.aCPreferredNmLckd = GetBool("aCPreferredNmLckd");
            dataApp.aCMidNm = GetString("aCMidNm");
            dataApp.aCLastNm = GetString("aCLastNm");
            dataApp.aCSuffix = GetString("aCSuffix");
            dataApp.aBSsn = GetString("aBSsn");
            dataApp.aCSsn = GetString("aCSsn");
            dataApp.aBDob_rep = GetString("aBDob");
            dataApp.aCDob_rep = GetString("aCDob");
            dataApp.aBHPhone = GetString("aBHPhone");
            dataApp.aCHPhone = GetString("aCHPhone");
            dataApp.aBCellPhone = GetString("aBCellphone");
            dataApp.aCCellPhone = GetString("aCCellphone");
            dataApp.aBBusPhone = GetString("aBBusPhone");
            dataApp.aCBusPhone = GetString("aCBusPhone");
            dataApp.aBEmail = GetString("aBEmail");
            dataApp.aCEmail = GetString("aCEmail");
            dataApp.aBAddr = GetString("aBAddr");
            dataApp.aCAddr = GetString("aCAddr");
            dataApp.aBCity = GetString("aBCity");
            dataApp.aBState = GetString("aBState");
            dataApp.aBZip = GetString("aBZip");
            dataApp.aBAddrYrs = GetString("aBAddrYrs");
            dataApp.aCAddrYrs = GetString("aCAddrYrs");
            dataApp.aCZip = GetString("aCZip");
            dataApp.aCState = GetString("aCState");
            dataApp.aCCity = GetString("aCCity");
            dataApp.aBAddrMail = GetString("aBAddrMail");
            dataApp.aCAddrMail = GetString("aCAddrMail");
            dataApp.aBCityMail = GetString("aBCityMail");
            dataApp.aCCityMail = GetString("aCCityMail");
            dataApp.aBStateMail = GetString("aBStateMail");
            dataApp.aCStateMail = GetString("aCStateMail");
            dataApp.aBZipMail = GetString("aBZipMail");
            dataApp.aCZipMail = GetString("aCZipMail");
            dataApp.aBAddrMailSourceT = (E_aAddrMailSourceT)GetInt("aBAddrMailSourceT");
            dataApp.aCAddrMailSourceT = (E_aAddrMailSourceT)GetInt("aCAddrMailSourceT");
            dataApp.aBFax = GetString("aBFax");
            dataApp.aCFax = GetString("aCFax");
            dataApp.aBAddrT = (E_aBAddrT)GetInt("aBAddrT");
            dataApp.aCAddrT = (E_aCAddrT)GetInt("aCAddrT");
        }

        private void LoadBorrowerInfo(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBPreferredNm", dataApp.aBPreferredNm);
            SetResult("aBPreferredNmLckd", dataApp.aBPreferredNmLckd);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCPreferredNm", dataApp.aCPreferredNm);
            SetResult("aCPreferredNmLckd", dataApp.aCPreferredNmLckd);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aBSsn", dataApp.aBSsn);
            SetResult("aCSsn", dataApp.aCSsn);
            SetResult("aBDob", dataApp.aBDob_rep);
            SetResult("aCDob", dataApp.aCDob_rep);
            SetResult("aBHPhone", dataApp.aBHPhone);
            SetResult("aCHPhone", dataApp.aCHPhone);
            SetResult("aBCellphone", dataApp.aBCellPhone);
            SetResult("aCCellphone", dataApp.aCCellPhone);
            SetResult("aBBusPhone", dataApp.aBBusPhone);
            SetResult("aCBusPhone", dataApp.aCBusPhone);
            SetResult("aBEmail", dataApp.aBEmail);
            SetResult("aCEmail", dataApp.aCEmail);
            SetResult("aBAddr", dataApp.aBAddr);
            SetResult("aCAddr", dataApp.aCAddr);
            SetResult("aBCity", dataApp.aBCity);
            SetResult("aBState", dataApp.aBState);
            SetResult("aBZip", dataApp.aBZip);
            SetResult("aBAddrYrs", dataApp.aBAddrYrs);
            SetResult("aCAddrYrs", dataApp.aCAddrYrs);
            SetResult("aCZip", dataApp.aCZip);
            SetResult("aCState", dataApp.aCState);
            SetResult("aCCity", dataApp.aCCity);
            SetResult("aBAddrMail", dataApp.aBAddrMail);
            SetResult("aCAddrMail", dataApp.aCAddrMail);
            SetResult("aBCityMail", dataApp.aBCityMail);
            SetResult("aCCityMail", dataApp.aCCityMail);
            SetResult("aBStateMail", dataApp.aBStateMail);
            SetResult("aCStateMail", dataApp.aCStateMail);
            SetResult("aBZipMail", dataApp.aBZipMail);
            SetResult("aCZipMail", dataApp.aCZipMail);
            SetResult("aBAddrMailSourceT", dataApp.aBAddrMailSourceT);
            SetResult("aCAddrMailSourceT", dataApp.aCAddrMailSourceT);
            SetResult("aBFax", dataApp.aBFax);
            SetResult("aCFax", dataApp.aCFax);
            SetResult("aBAddrT", dataApp.aBAddrT);
            SetResult("aCAddrT", dataApp.aCAddrT);
        }

        private void CalculateAge()
        {
            string dob = GetString("dob");
            string currentAge = GetString("age");

            DateTime dt = DateTime.MinValue;
            if (DateTime.TryParse(dob, out dt) == true)
            {
                try
                {
                    SetResult("age", Tools.CalcAgeForToday(dt).ToString());
                }
                catch (CBaseException)
                {
                    SetResult("age", currentAge);
                }
            }
            else
            {
                SetResult("age", currentAge);
            }
        }

        #endregion

        #region Loan Info
        
        private void BindLoanInfo(CPageData dataLoan, CAppData dataApp)
        {
            if (dataLoan.BrokerDB.ShowLoanProductIdentifier)
            {
                dataLoan.sLoanProductIdentifier = GetString("sLoanProductIdentifier");
            }

            dataLoan.sLpTemplateNm = GetString("sLpTemplateNm");
            dataLoan.sCcTemplateNm = GetString("sCcTemplateNm");
            dataLoan.sLT = (E_sLT)GetInt("sLT");
            dataLoan.sLienPosT = (E_sLienPosT)GetInt("sLienPosT");
            dataLoan.sLPurposeT = (E_sLPurposeT)GetInt("sLPurposeT");
            dataLoan.sIsStudentLoanCashoutRefi = GetBool("sIsStudentLoanCashoutRefi");
            dataLoan.sFinMethT = (E_sFinMethT)GetInt("sFinMethT");
            dataLoan.sFinMethDesc = GetString("sFinMethDesc");
            dataLoan.sPurchPrice_rep = GetString("sPurchPrice");
            dataLoan.sApprVal_rep = GetString("sApprVal");
            dataLoan.sEquityCalc_rep = GetString("sEquityCalc");
            dataLoan.sLAmtCalc_rep = GetString("sLAmtCalc");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sQualIR_rep = GetString("sQualIR");
            dataLoan.sQualIRLckd = this.GetBool(nameof(dataLoan.sQualIRLckd));
            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sDue_rep = GetString("sDue");
            dataLoan.sIsOptionArm = GetBool("sIsOptionArm");
            dataLoan.sOptionArmTeaserR_rep = GetString("sOptionArmTeaserR");

            if (!dataLoan.sRealEstateTaxExpenseInDisbMode)
            {
                dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb");
                dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
                dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
            }

            dataLoan.sProHoAssocDues_rep = GetString("sProHoAssocDues");
            dataLoan.sProOHExp_rep = GetString("sProOHExp");
            dataLoan.sProOHExpLckd = GetBool("sProOHExpLckd");
            //// 2/11/2005 dd - Relate to OPM case 1064. If user just calculate data then ignore sLNm, even if it blank.
            if (dataLoan.DataState == E_DataState.InitSave)
            {
                dataLoan.sLNm = GetString("sLNm");
            }
            dataLoan.sCrmNowLeadId = GetString("sCrmNowLeadId");
            if (!dataLoan.sHazardExpenseInDisbursementMode)
            {
                dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
                dataLoan.sProHazInsR_rep = GetString("sProHazInsR");
                dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb");
            }
            dataLoan.sProMIns_rep = GetString("sProMIns");
            dataLoan.sProMInsLckd = GetBool("sProMInsLckd");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sLAmtLckd = GetBool("sLAmtLckd");
            dataApp.aOccT = (E_aOccT)GetInt("aOccT");
            dataLoan.sGseSpT = (E_sGseSpT)GetInt("sGseSpT");
            dataLoan.sHomeIsMhAdvantageTri = this.GetEnum<E_TriState>("sHomeIsMhAdvantageTri");
            dataLoan.sIsQRateIOnly = GetBool("sIsQRateIOnly");
            dataLoan.sOriginalAppraisedValue_rep = GetString("sOriginalAppraisedValue");
            dataLoan.sHighPricedMortgageTLckd = GetBool("sHighPricedMortgageTLckd");
            dataLoan.sHighPricedMortgageT = GetEnum<E_HighPricedMortgageT>("sHighPricedMortgageT");
            dataLoan.sIsEmployeeLoan = GetBool("sIsEmployeeLoan");
            dataLoan.sIsNewConstruction = GetBool("sIsNewConstruction");
            dataLoan.sIsLineOfCredit = GetBool("sIsLineOfCredit");
            dataLoan.sCreditLineAmt_rep = GetString("sCreditLineAmt");
            dataLoan.sLeadSrcDesc = GetString("sLeadSrcDesc");

            dataLoan.sIsRenovationLoan = GetBool("sIsRenovationLoan", dataLoan.sIsRenovationLoan);
            dataLoan.sFHASpAsIsVal_rep = GetString("sFHASpAsIsVal");
            dataLoan.sInducementPurchPrice_rep = GetString("sInducementPurchPrice");
            dataLoan.sTotalRenovationCostsLckd = GetBool("sTotalRenovationCostsLckd");
            dataLoan.sTotalRenovationCosts_rep = GetString("sTotalRenovationCosts");

            dataLoan.sQualTermCalculationType = (QualTermCalculationType)GetInt("sQualTermCalculationType");
            dataLoan.sQualTerm_rep = GetString("sQualTerm");

            if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges))
            {
                dataLoan.sConstructionPeriodMon_rep = GetString("sConstructionPeriodMon");
                dataLoan.sConstructionPeriodIR_rep = GetString("sConstructionPeriodIR");
                dataLoan.sConstructionImprovementAmt_rep = GetString("sConstructionImprovementAmt");
                dataLoan.sLandCost_rep = GetString("sLandCost");
            }
        }

        private void LoadLoanInfo(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aOccT", dataApp.aOccT);
            SetResult("sApprVal", dataLoan.sApprVal_rep);
            SetResult("sCcTemplateNm", dataLoan.sCcTemplateNm);
            SetResult("sLPurposeT", dataLoan.sLPurposeT);
            SetResult("sIsStudentLoanCashoutRefi", dataLoan.sIsStudentLoanCashoutRefi);
            SetResult("sCltvR", dataLoan.sCltvR_rep);
            SetResult("sDownPmtPc", dataLoan.sDownPmtPc_rep);
            SetResult("sDue", dataLoan.sDue_rep);
            SetResult("sEquityCalc", dataLoan.sEquityCalc_rep);
            SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            SetResult("sFinMethDesc", dataLoan.sFinMethDesc);
            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sLAmtLckd", dataLoan.sLAmtLckd);
            SetResult("sHighPricedMortgageT", dataLoan.sHighPricedMortgageT);
            SetResult("sHighPricedMortgageTLckd", dataLoan.sHighPricedMortgageTLckd);
            SetResult("sIsEmployeeLoan", dataLoan.sIsEmployeeLoan);
            SetResult("sIsNewConstruction", dataLoan.sIsNewConstruction);

            if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges))
            {
                SetResult("sConstructionPeriodMon", dataLoan.sConstructionPeriodMon_rep);
                SetResult("sConstructionPeriodIR", dataLoan.sConstructionPeriodIR_rep);
                SetResult("sConstructionImprovementAmt", dataLoan.sConstructionImprovementAmt_rep);
                SetResult("sLandCost", dataLoan.sLandCost_rep);
            }

            // 7/25/2005 dd - OPM 1064. Since we don't set sLNm to dataobject therefore don't replace the current value in UI.
            if (dataLoan.DataState == E_DataState.InitSave)
            {
                SetResult("sLNm", dataLoan.sLNm);
            }

            SetResult("sCrmNowLeadId", dataLoan.sCrmNowLeadId);
            SetResult("sLT", dataLoan.sLT);
            SetResult("sLTotI", dataLoan.sLTotI_rep);
            SetResult("sLiaMonLTot", dataLoan.sLiaMonLTot_rep);
            SetResult("sLienPosT", dataLoan.sLienPosT);
            SetResult("sLpTemplateNm", dataLoan.sLpTemplateNm);
            SetResult("sLoanProductIdentifier", dataLoan.sLoanProductIdentifier);
            SetResult("sLtvR", dataLoan.sLtvR_rep);
            SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sPresLTotPersistentHExp", dataLoan.sPresLTotPersistentHExp_rep);
            SetResult("sProHazIns", dataLoan.sProHazIns_rep);
            SetResult("sProHazInsBaseAmt", dataLoan.sProHazInsBaseAmt_rep);
            SetResult("sProHazInsMb", dataLoan.sProHazInsMb_rep);
            SetResult("sProHazInsR", dataLoan.sProHazInsR_rep);
            SetResult("sProHazInsT", dataLoan.sProHazInsT);
            SetResult("sProHoAssocDues", dataLoan.sProHoAssocDues_rep);
            SetResult("sProMIns", dataLoan.sProMIns_rep);
            SetResult("sProMInsLckd", dataLoan.sProMInsLckd);
            SetResult("sProOFinPmt", dataLoan.sProOFinPmt_rep);
            SetResult("sProOHExp", dataLoan.sProOHExp_rep);
            SetResult("sProOHExpLckd", dataLoan.sProOHExpLckd);
            SetResult("sProRealETx", dataLoan.sProRealETx_rep);
            SetResult("sProRealETxBaseAmt", dataLoan.sProRealETxBaseAmt_rep);
            SetResult("sProRealETxR", dataLoan.sProRealETxR_rep);
            SetResult("sProRealETxT", dataLoan.sProRealETxT);
            SetResult("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            SetResult("sPurchPrice", dataLoan.sPurchPrice_rep);
            SetResult("sQualBottomR", dataLoan.sQualBottomR_rep);
            SetResult("sQualIR", dataLoan.sQualIR_rep);
            this.SetResult(nameof(dataLoan.sQualIRLckd), dataLoan.sQualIRLckd);
            SetResult("sQualTopR", dataLoan.sQualTopR_rep);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sOptionArmTeaserR", dataLoan.sOptionArmTeaserR_rep);
            SetResult("sIsOptionArm", dataLoan.sIsOptionArm);
            SetResult("sIsQRateIOnly", dataLoan.sIsQRateIOnly);
            SetResult("sOriginalAppraisedValue", dataLoan.sOriginalAppraisedValue_rep);
            SetResult("sFHAPurposeIsStreamlineRefiWithoutAppr", dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr ? "1" : "0");
            SetResult("sIsLineOfCredit", dataLoan.sIsLineOfCredit);
            SetResult("sCreditLineAmt", dataLoan.sCreditLineAmt_rep);

            SetResult("sTransmProTotHExp_pop", dataLoan.sTransmProTotHExp_rep);
            SetResult("sLTotI_pop", dataLoan.sLTotI_rep);
            SetResult("sQualTopR_pop", dataLoan.sQualTopR_rep);

            SetResult("sMonthlyPmt_pop", dataLoan.sMonthlyPmt_rep);
            SetResult("sPresLTotPersistentHExp_pop", dataLoan.sPresLTotPersistentHExp_rep);
            SetResult("sLiaMonLTot_pop", dataLoan.sLiaMonLTot_rep);
            SetResult("sLTotI_pop2", dataLoan.sLTotI_rep);
            SetResult("sQualBottomR_pop", dataLoan.sQualBottomR_rep);

            SetResult("sIsRenovationLoan", dataLoan.sIsRenovationLoan);
            SetResult("sFHASpAsIsVal", dataLoan.sFHASpAsIsVal_rep);
            SetResult("sTotalRenovationCosts", dataLoan.sTotalRenovationCosts_rep);
            SetResult("sInducementPurchPrice", dataLoan.sInducementPurchPrice_rep);
            SetResult("sTotalRenovationCostsLckd", dataLoan.sTotalRenovationCostsLckd);

            SetResult("sQualTermCalculationType", dataLoan.sQualTermCalculationType);
            SetResult("sQualTerm", dataLoan.sQualTerm_rep);

            this.SetResult("sHomeIsMhAdvantageTri", dataLoan.sHomeIsMhAdvantageTri, forDropdown: true);
        }

        private void GenerateNewLoanNumber()
        {
            // 1/26/2005 kb - Get loan namer and get an auto name.
            // The namer will query the broker's bits to determine
            // if a sequential, counter-based name is used, or a
            // friendly id is used.
            //
            // We also get the loan number passed back, so we should
            // do a security check, to determine if they have permission
            // to get a new name for a loan.  Yes, the loan editor will
            // enforce saving, but this call has the side effect of
            // incrementing the broker's naming counter.  Hmmm...
            //
            // 1/28/2005 kb - Loop until a valid number is served up.
            CLoanFileNamer loanNamer = new CLoanFileNamer();
            try
            {
                String newLoanName = "";
                string sMersMin = string.Empty;
                string sLRefNm = string.Empty;
                do
                {

                    newLoanName = loanNamer.GetLoanAutoName(BrokerUser.BrokerId, BrokerUser.BranchId, false, false, false /* enableMersGeneration*/, out sMersMin, out sLRefNm);
                }
                while (CLoanFileNamer.CheckIfLoanNameExists(BrokerUser.BrokerId, sLId, newLoanName) == true);

                SetResult("NewLoanName", newLoanName);
            }
            catch (CBaseException e)
            {
                SetResult("ErrMsg", "Failed to generate a new loan number.");

                Tools.LogError("Failed to generate a new loan number.", e);
            }
        }

        #endregion

        #region Monthly Income

        private void BindMonthlyIncome(CPageData dataLoan, CAppData dataApp)
        {
            if (!dataLoan.sIsIncomeCollectionEnabled)
            {
                dataApp.aBBaseI_rep = GetString("aBBaseI");
                dataApp.aBOvertimeI_rep = GetString("aBOvertimeI");
                dataApp.aBBonusesI_rep = GetString("aBBonusesI");
                dataApp.aBCommisionI_rep = GetString("aBCommisionI");
                dataApp.aBDividendI_rep = GetString("aBDividendI");
                dataApp.aCBaseI_rep = GetString("aCBaseI");
                dataApp.aCOvertimeI_rep = GetString("aCOvertimeI");
                dataApp.aCBonusesI_rep = GetString("aCBonusesI");
                dataApp.aCCommisionI_rep = GetString("aCCommisionI");
                dataApp.aCDividendI_rep = GetString("aCDividendI");
            }

            dataApp.aBNetRentI1003_rep = GetString("aBNetRentI1003");
            dataApp.aCNetRentI1003_rep = GetString("aCNetRentI1003");
            dataApp.aNetRentI1003Lckd = GetBool("aNetRentI1003Lckd");
        }

        private void LoadMonthlyIncome(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aBBaseI", dataApp.aBBaseI_rep);
            SetResult("aBOvertimeI", dataApp.aBOvertimeI_rep);
            SetResult("aBBonusesI", dataApp.aBBonusesI_rep);
            SetResult("aBCommisionI", dataApp.aBCommisionI_rep);
            SetResult("aBDividendI", dataApp.aBDividendI_rep);
            SetResult("aBNetRentI1003", dataApp.aBNetRentI1003_rep);
            SetResult("aCBaseI", dataApp.aCBaseI_rep);
            SetResult("aCOvertimeI", dataApp.aCOvertimeI_rep);
            SetResult("aCBonusesI", dataApp.aCBonusesI_rep);
            SetResult("aCCommisionI", dataApp.aCCommisionI_rep);
            SetResult("aCDividendI", dataApp.aCDividendI_rep);
            SetResult("aCNetRentI1003", dataApp.aCNetRentI1003_rep);
            SetResult("aNetRentI1003Lckd", dataApp.aNetRentI1003Lckd);
            SetResult("aBSpPosCf", dataApp.aBSpPosCf_rep);
            SetResult("aCSpPosCf", dataApp.aCSpPosCf_rep);
            SetResult("aTotSpPosCf", dataApp.aTotSpPosCf_rep);

            SetResult("aBTotI", dataApp.aBTotI_rep);
            SetResult("aCTotI", dataApp.aCTotI_rep);

            SetResult("aTotI", dataApp.aTotI_rep);
            SetResult("aBTotOI", dataApp.aBTotOI_rep);
            SetResult("aCTotOI", dataApp.aCTotOI_rep);
            SetResult("aTotOI", dataApp.aTotOI_rep);

            SetResult("aTotNetRentI1003", dataApp.aTotNetRentI1003_rep);
            SetResult("aTotDividendI", dataApp.aTotDividendI_rep);
            SetResult("aTotCommisionI", dataApp.aTotCommisionI_rep);
            SetResult("aTotBonusesI", dataApp.aTotBonusesI_rep);
            SetResult("aTotBaseI", dataApp.aTotBaseI_rep);
            SetResult("aTotOvertimeI", dataApp.aTotOvertimeI_rep);

            SetResult("sLTotI_mon", dataLoan.sLTotI_rep);
        }

        #endregion

        #region Credit Score

        private void BindCreditScore(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aBExperianScore_rep = GetString("aBExperianScore");
            dataApp.aBTransUnionScore_rep = GetString("aBTransUnionScore");
            dataApp.aBEquifaxScore_rep = GetString("aBEquifaxScore");
            dataApp.aCExperianScore_rep = GetString("aCExperianScore");
            dataApp.aCTransUnionScore_rep = GetString("aCTransUnionScore");
            dataApp.aCEquifaxScore_rep = GetString("aCEquifaxScore");

            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckd");
            dataLoan.sEstCloseD_rep = GetString("sEstCloseD");
        }

        private void LoadCreditScore(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aBExperianScore", dataApp.aBExperianScore_rep);
            SetResult("aBTransUnionScore", dataApp.aBTransUnionScore_rep);
            SetResult("aBEquifaxScore", dataApp.aBEquifaxScore_rep);
            SetResult("aCExperianScore", dataApp.aCExperianScore_rep);
            SetResult("aCTransUnionScore", dataApp.aCTransUnionScore_rep);
            SetResult("aCEquifaxScore", dataApp.aCEquifaxScore_rep);

            SetResult("sEstCloseDLckd", dataLoan.sEstCloseDLckd);
            SetResult("sEstCloseD", dataLoan.sEstCloseD_rep);
        }

        #endregion

        #region Subj Prop Info

        private void BindSubjPropInfo(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sSpCity = GetString("sSpCity_spi");
            dataLoan.sSpState = GetString("sSpState_spi");
            dataLoan.sSpZip = GetString("sSpZip_spi");
            dataLoan.sSpAddr = GetString("sSpAddr_spi");
        }

        #endregion

        #region MIP

        private void BindMIP(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sProMInsT = (E_PercentBaseT)GetInt("sProMInsT"); // opm 146309
            dataLoan.sFfUfmipR_rep = GetString("sFfUfmipR");
            dataLoan.sFfUfmip1003_rep = GetString("sFfUfmip1003");
            dataLoan.sFfUfmip1003Lckd = GetBool("sFfUfmip1003Lckd");
            dataLoan.sMipFrequency = (E_MipFrequency)GetInt("sMipFrequency");

            if (GetBool("m_sFfUfMipIsBeingFinancedChanged"))
                dataLoan.OnChangesFfUfMipIsBeingFinanced(GetBool("sFfUfMipIsBeingFinanced"));
            else
                dataLoan.sFfUfMipIsBeingFinanced = GetBool("sFfUfMipIsBeingFinanced");

            dataLoan.sMipPiaMon_rep = GetString("sMipPiaMon");
            dataLoan.sUfCashPdLckd = GetBool("sUfCashPdLckd");
            dataLoan.sUfCashPd_rep = GetString("sUfCashPd");

            dataLoan.sProMInsR_rep = GetString("sProMInsR");
            dataLoan.sProMInsMb_rep = GetString("sProMInsMb");
            dataLoan.sProMIns_rep = GetString("sProMIns_mip");
            dataLoan.sProMInsLckd = GetBool("sProMInsLckd_mip");

            dataLoan.sMiLenderPaidCoverage_rep = GetString("sMiLenderPaidCoverage");
            dataLoan.sMiCommitmentRequestedD_rep = GetString("sMiCommitmentRequestedD");
            dataLoan.sMiCommitmentReceivedD_rep = GetString("sMiCommitmentReceivedD");
            dataLoan.sMiCommitmentExpirationD_rep = GetString("sMiCommitmentExpirationD");
            dataLoan.sMiCertId = GetString("sMiCertId");
            dataLoan.sProMInsMon_rep = GetString("sProMInsMon");
            dataLoan.sProMIns2Mon_rep = GetString("sProMIns2Mon");
            dataLoan.sProMInsR2_rep = GetString("sProMInsR2");
            dataLoan.sProMInsCancelLtv_rep = GetString("sProMInsCancelLtv");
            dataLoan.sProMInsCancelAppraisalLtv_rep = GetString("sProMInsCancelAppraisalLtv");
            dataLoan.sProMInsCancelMinPmts_rep = GetString("sProMInsCancelMinPmts");
            dataLoan.sProMInsMidptCancel = GetBool("sProMInsMidptCancel");
            dataLoan.sMiInsuranceT = (E_sMiInsuranceT)GetInt("sMiInsuranceT");
            dataLoan.sMiCompanyNmT = (E_sMiCompanyNmT)GetInt("sMiCompanyNmT");
            dataLoan.sLenderUfmipR_rep = GetString("sLenderUfmipR");
            dataLoan.sLenderUfmip_rep = GetString("sLenderUfmip");
            dataLoan.sLenderUfmipLckd = GetBool("sLenderUfmipLckd");
            dataLoan.sUfmipIsRefundableOnProRataBasis = GetBool("sUfmipIsRefundableOnProRataBasis");
        }

        private void LoadMIP(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sFfUfmipR", dataLoan.sFfUfmipR_rep);
            SetResult("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            SetResult("sFfUfmip1003Lckd", dataLoan.sFfUfmip1003Lckd);
            SetResult("sMipFrequency", dataLoan.sMipFrequency);
            SetResult("sFfUfMipIsBeingFinanced", dataLoan.sFfUfMipIsBeingFinanced);
            SetResult("sMipPiaMon", dataLoan.sMipPiaMon_rep);
            SetResult("sUfCashPd", dataLoan.sUfCashPd_rep);
            SetResult("sFfUfmipFinanced_mip", dataLoan.sFfUfmipFinanced_rep);
            SetResult("sUfCashPdLckd", dataLoan.sUfCashPdLckd);

            SetResult("sProMInsT", dataLoan.sProMInsT);
            SetResult("sProMInsR", dataLoan.sProMInsR_rep);
            SetResult("sProMInsBaseAmt", dataLoan.sProMInsBaseAmt_rep);
            SetResult("sProMInsBaseMonthlyPremium", dataLoan.sProMInsBaseMonthlyPremium_rep);
            SetResult("sProMInsMb", dataLoan.sProMInsMb_rep);
            SetResult("sProMIns_mip", dataLoan.sProMIns_rep);
            SetResult("sProMInsLckd_mip", dataLoan.sProMInsLckd);

            SetResult("sMiCompanyNmT", dataLoan.sMiCompanyNmT);
            SetResult("sMiInsuranceT", dataLoan.sMiInsuranceT);
            SetResult("sProMInsMidptCancel", dataLoan.sProMInsMidptCancel);
            SetResult("sProMInsCancelMinPmts", dataLoan.sProMInsCancelMinPmts_rep);
            SetResult("sProMInsCancelLtv", dataLoan.sProMInsCancelLtv_rep);
            SetResult("sProMInsCancelAppraisalLtv", dataLoan.sProMInsCancelAppraisalLtv_rep);
            SetResult("sProMIns2", dataLoan.sProMIns2_rep);
            SetResult("sProMInsR2", dataLoan.sProMInsR2_rep);
            SetResult("sProMIns2Mon", dataLoan.sProMIns2Mon_rep);
            SetResult("sProMInsMon", dataLoan.sProMInsMon_rep);
            SetResult("sMiCertId", dataLoan.sMiCertId);
            SetResult("sMiCommitmentExpirationD", dataLoan.sMiCommitmentExpirationD_rep);
            SetResult("sMiCommitmentReceivedD", dataLoan.sMiCommitmentReceivedD_rep);
            SetResult("sMiCommitmentRequestedD", dataLoan.sMiCommitmentRequestedD_rep);
            SetResult("sMiLenderPaidCoverage", dataLoan.sMiLenderPaidCoverage_rep);
            SetResult("sLenderUfmipR", dataLoan.sLenderUfmipR_rep);
            SetResult("sLenderUfmip", dataLoan.sLenderUfmip_rep);
            SetResult("sLenderUfmipLckd", dataLoan.sLenderUfmipLckd);
            SetResult("sUfmipIsRefundableOnProRataBasis", dataLoan.sUfmipIsRefundableOnProRataBasis);
        }

        #endregion

        #region Misc

        private void BindMisc(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum");
            dataLoan.sCaseAssignmentD_rep = GetString("sCaseAssignmentD");
            dataLoan.sFHAHousingActSection = GetString("sFHAHousingActSection");

            dataApp.aVaEntitleCode = GetString("aVaEntitleCode");
            dataApp.aVaEntitleAmt_rep = GetString("aVaEntitleAmt");
            dataApp.aVaServiceBranchT = (E_aVaServiceBranchT)GetInt("aVaServiceBranchT");
            dataApp.aVaMilitaryStatT = (E_aVaMilitaryStatT)GetInt("aVaMilitaryStatT");
        }

        #endregion

        #region Footer

        private void BindFooter(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sAltCostLckd = GetBool("sAltCostLckd");
            dataLoan.sAltCost_rep = GetString("sAltCost");
            dataLoan.sFfUfmip1003Lckd = GetBool("sFfUfmip1003Lckd_foot");
            dataLoan.sFfUfmip1003_rep = GetString("sFfUfmip1003_foot");
            dataLoan.sLDiscnt1003Lckd = GetBool("sLDiscnt1003Lckd");
            dataLoan.sLDiscnt1003_rep = GetString("sLDiscnt1003");
            dataLoan.sLandCost_rep = GetString("sLandCost");
            dataLoan.sOCredit1Amt_rep = GetString("sOCredit1Amt");
            dataLoan.sOCredit1Desc = GetString("footer_sOCredit1Desc");
            dataLoan.sOCredit1Lckd = GetBool("sOCredit1Lckd");

            if (false == dataLoan.sLoads1003LineLFromAdjustments)
            {
                dataLoan.sOCredit2Amt_rep = GetString("sOCredit2Amt");
                dataLoan.sOCredit2Desc = GetString("footer_sOCredit2Desc");
                dataLoan.sOCredit3Amt_rep = GetString("sOCredit3Amt");
                dataLoan.sOCredit3Desc = GetString("footer_sOCredit3Desc");
                dataLoan.sOCredit4Amt_rep = GetString("sOCredit4Amt");
                dataLoan.sOCredit4Desc = GetString("footer_sOCredit4Desc");
            }

            dataLoan.sPurchPrice_rep = GetString("sPurchPrice_foot");
            dataLoan.sRefPdOffAmt1003Lckd = GetBool("sRefPdOffAmt1003Lckd");
            dataLoan.sRefPdOffAmt1003_rep = GetString("sRefPdOffAmt1003");
            dataLoan.sTotCcPbsLocked = GetBool("sTotCcPbsLocked");
            dataLoan.sTotCcPbs_rep = GetString("sTotCcPbs");
            dataLoan.sTotEstCc1003Lckd = GetBool("sTotEstCc1003Lckd");
            dataLoan.sTotEstCcNoDiscnt1003_rep = GetString("sTotEstCcNoDiscnt1003");
            dataLoan.sTotEstPp1003Lckd = GetBool("sTotEstPp1003Lckd");
            dataLoan.sTotEstPp1003_rep = GetString("sTotEstPp1003");
            dataLoan.sTransNetCashLckd = GetBool("sTransNetCashLckd");
            dataLoan.sTransNetCash_rep = GetString("sTransNetCash");

            if (!dataLoan.sIsIncludeONewFinCcInTotEstCc)
            {
                dataLoan.sONewFinCc_rep = GetString("sONewFinCc");
            }
            else
            {
                dataLoan.sONewFinCc_rep = GetString("sONewFinCc2");
            }

            dataLoan.sLAmtCalc_rep = GetString("sLAmtCalc_foot");
            dataLoan.sLAmtLckd = GetBool("sLAmtLckd_foot");
        }

        private void LoadFooter(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sAltCostLckd", dataLoan.sAltCostLckd);
            SetResult("sAltCost", dataLoan.sAltCost_rep);
            SetResult("sFfUfmip1003_foot", dataLoan.sFfUfmip1003_rep);
            SetResult("sFfUfmip1003Lckd_foot", dataLoan.sFfUfmip1003Lckd);
            SetResult("sFfUfmipFinanced_foot", dataLoan.sFfUfmipFinanced_rep);
            SetResult("sFinalLAmt_foot", dataLoan.sFinalLAmt_rep);
            SetResult("sLAmt1003", dataLoan.sLAmt1003_rep);
            SetResult("sLDiscnt1003", dataLoan.sLDiscnt1003_rep);
            SetResult("sLDiscnt1003Lckd", dataLoan.sLDiscnt1003Lckd);
            SetResult("sLandCost", dataLoan.sLandCost_rep);
            SetResult("sOCredit1Amt", dataLoan.sOCredit1Amt_rep);
            SetResult("footer_sOCredit1Desc", dataLoan.sOCredit1Desc);
            SetResult("sOCredit1Lckd", dataLoan.sOCredit1Lckd);
            SetResult("sOCredit2Amt", dataLoan.sOCredit2Amt_rep);
            SetResult("footer_sOCredit2Desc", dataLoan.sOCredit2Desc);
            SetResult("sOCredit3Amt", dataLoan.sOCredit3Amt_rep);
            SetResult("footer_sOCredit3Desc", dataLoan.sOCredit3Desc);
            SetResult("sOCredit4Amt", dataLoan.sOCredit4Amt_rep);
            SetResult("footer_sOCredit4Desc", dataLoan.sOCredit4Desc);
            SetResult("sOCredit5Amt", dataLoan.sOCredit5Amt_rep);
            SetResult("sONewFinBal", dataLoan.sONewFinBal_rep);
            SetResult("sPurchPrice_foot", dataLoan.sPurchPrice_rep);
            SetResult("sRefPdOffAmt1003", dataLoan.sRefPdOffAmt1003_rep);
            SetResult("sRefPdOffAmt1003Lckd", dataLoan.sRefPdOffAmt1003Lckd);
            SetResult("sTotCcPbs", dataLoan.sTotCcPbs_rep);
            SetResult("sTotCcPbsLocked", dataLoan.sTotCcPbsLocked);
            SetResult("sTotEstCc1003Lckd", dataLoan.sTotEstCc1003Lckd);
            SetResult("sTotEstCcNoDiscnt1003", dataLoan.sTotEstCcNoDiscnt1003_rep);
            SetResult("sTotEstPp1003", dataLoan.sTotEstPp1003_rep);
            SetResult("sTotEstPp1003Lckd", dataLoan.sTotEstPp1003Lckd);
            SetResult("sTotTransC", dataLoan.sTotTransC_rep);
            SetResult("sTransNetCash", dataLoan.sTransNetCash_rep);
            SetResult("sTransNetCashLckd", dataLoan.sTransNetCashLckd);
            SetResult("sONewFinCc", dataLoan.sONewFinCc_rep);
            SetResult("sONewFinCc2", dataLoan.sONewFinCc_rep);
            SetResult("sLAmtCalc_foot", dataLoan.sLAmtCalc_rep);
            SetResult("sLAmtLckd_foot", dataLoan.sLAmtLckd);
            SetResult("sTotEstCcNoDiscnt", dataLoan.sTotEstCcNoDiscnt_rep);
            SetResult("sTotEstPp", dataLoan.sTotEstPp_rep);
            SetResult("sTotalBorrowerPaidProrations", dataLoan.sTotalBorrowerPaidProrations_rep);
        }

        #endregion

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            BindBorrowerInfo(dataLoan, dataApp);

            BindLoanInfo(dataLoan, dataApp);

            BindMonthlyIncome(dataLoan, dataApp);

            BindCreditScore(dataLoan, dataApp);

            BindSubjPropInfo(dataLoan, dataApp);

            BindMIP(dataLoan, dataApp);

            BindMisc(dataLoan, dataApp);

            if (dataLoan.sGseTargetApplicationT == GseTargetApplicationT.Ulad2019)
            {
                QualifyingBorrower.Bind_QualifyingBorrower(this, dataLoan);
            }
            else
            {
                BindFooter(dataLoan, dataApp);
            }

            BigLoanInfo.BindDataFromControls(dataLoan, dataApp, this);

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            LoadBorrowerInfo(dataLoan, dataApp);

            LoadLoanInfo(dataLoan, dataApp);

            LoadMonthlyIncome(dataLoan, dataApp);

            LoadCreditScore(dataLoan, dataApp);

            LoadMIP(dataLoan, dataApp);


            if (dataLoan.sGseTargetApplicationT == GseTargetApplicationT.Ulad2019)
            {
                QualifyingBorrower.Load_QualifyingBorrower(this, dataLoan);
            }
            else
            {
                LoadFooter(dataLoan, dataApp);
            }

            BigLoanInfo.LoadDataForControls(dataLoan, dataApp, this);
        }
    }
    public partial class BigLoanInfoService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new BigLoanInfoServiceItem());
        }
    }
}
