<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="LockInConfirmation.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LockInConfirmation" %>
<%@ Import namespace="DataAccess"%>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../newlos/Status/ContactFieldMapper.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>LockInConfirmation</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	<script language=javascript>
  <!--
  var oRolodex;  
  function _init() {
    oRolodex = new cRolodex();
  }
  
	// OPM 17652 - Ethan
	//
	// this javascript has now been centralized in a single control: ContactFieldMapper
	// location newlos/Status/ContactFieldMapper.ascx
	// and all instances of the "Pick from Contacts" and "Add to Contacts" links have been replaced with it
	
  /*function pickAndPopulate() {
    var lenderType =  <%=AspxTools.JsString(E_AgentRoleT.Lender)%>;
    var args = oRolodex.chooseFromRolodex(lenderType);
    if (args.OK) {
      <%= AspxTools.JsGetElementById(LockInConfirmationCompanyName) %>.value = args.AgentCompanyName;
      <%= AspxTools.JsGetElementById(LockInConfirmationStreetAddr) %>.value = args.AgentStreetAddr;
      <%= AspxTools.JsGetElementById(LockInConfirmationCity) %>.value = args.AgentCity;      
      <%= AspxTools.JsGetElementById(LockInConfirmationState) %>.value = args.AgentState;
      <%= AspxTools.JsGetElementById(LockInConfirmationZip) %>.value = args.AgentZip;
      <%= AspxTools.JsGetElementById(LockInConfirmationPhoneOfCompany) %>.value = args.PhoneOfCompany;
      updateDirtyBit();
    }
  }
  function addToRolodex() {
    var lenderType = <%=AspxTools.JsString(E_AgentRoleT.Lender)%>;
    var args = new Object();
    args.AgentType = lenderType;
    args.AgentCompanyName = <%= AspxTools.JsGetElementById(LockInConfirmationCompanyName) %>.value;
    args.AgentStreetAddr = <%= AspxTools.JsGetElementById(LockInConfirmationStreetAddr) %>.value;
    args.AgentCity = <%= AspxTools.JsGetElementById(LockInConfirmationCity) %>.value;      
    args.AgentState = <%= AspxTools.JsGetElementById(LockInConfirmationState) %>.value;
    args.AgentZip = <%= AspxTools.JsGetElementById(LockInConfirmationZip) %>.value;
    args.PhoneOfCompany = <%= AspxTools.JsGetElementById(LockInConfirmationPhoneOfCompany) %>.value;
    
    oRolodex.addToRolodex(args);
    
  }*/

  //-->
  </script>

    <form id="LockInConfirmation" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 border=0>
  <tr>
    <td class=MainRightHeader nowrap>Lock-In Confirmation</td></tr>
  <tr>
    <td nowrap>
      <table id=Table2 cellspacing=0 cellpadding=0 border=0 class=InsetBorder width="98%">
        <tr>
          <td class=FieldLabel nowrap>Lender</td>
          <td nowrap>
			<uc:CFM id="CFM" runat="server" Type="21" CompanyNameField="LockInConfirmationCompanyName" StreetAddressField="LockInConfirmationStreetAddr" CityField="LockInConfirmationCity" StateField="LockInConfirmationState" ZipField="LockInConfirmationZip" CompanyPhoneField="LockInConfirmationPhoneOfCompany"></uc:CFM>
          </td></tr>
        <tr>
          <td nowrap class=FieldLabel>Name</td>
          <td nowrap><asp:TextBox id=LockInConfirmationCompanyName runat="server" Width="304px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Address</td>
          <td nowrap><asp:TextBox id=LockInConfirmationStreetAddr runat="server" Width="304px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap></td>
          <td nowrap><asp:TextBox id=LockInConfirmationCity runat="server" Width="204px"></asp:TextBox><ml:StateDropDownList id=LockInConfirmationState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=LockInConfirmationZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Phone</td>
          <td nowrap><ml:PhoneTextBox id=LockInConfirmationPhoneOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
        <tr>
          <td nowrap class=FieldLabel>Prepared Date</td>
          <td nowrap><ml:DateTextBox id=LockInConfirmationPreparedDate runat="server" width="75" preset="date"></ml:DateTextBox></td></tr></table></td></tr>
  <tr>
    <td nowrap>
      <table id=Table3 cellspacing=0 cellpadding=0 border=0 class=InsetBorder width="98%">
        <tr>
          <td class=FieldLabel nowrap>Loan Type</td>
          <td nowrap><asp:DropDownList id=sLT runat="server"></asp:DropDownList></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Loan Program</td>
          <td nowrap><asp:TextBox id=sLpTemplateNm runat="server" Width="215px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Loan Amount</td>
          <td nowrap><ml:MoneyTextBox id=sLAmt runat="server" width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Term / Due</td>
          <td nowrap><asp:TextBox id=sTerm runat="server" Width="58px"></asp:TextBox> / <asp:TextBox id=sDue runat="server" Width="58px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Interest Rate</td>
          <td nowrap><ml:PercentTextBox id=sNoteIR runat="server" width="70" preset="percent"></ml:PercentTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap title="(Percentage + Fee)">Discount Points</td>
          <td nowrap><ml:PercentTextBox id=sLDiscntPc runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:PercentTextBox>&nbsp;+ 
<ml:MoneyTextBox id=sLDiscntFMb runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>&nbsp;= 
<ml:MoneyTextBox id=sLDiscnt runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap title="(Percentage + Fee)">Origination Fees</td>
          <td nowrap><ml:PercentTextBox id=sLOrigFPc runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:PercentTextBox>&nbsp;+ 
<ml:MoneyTextBox id=sLOrigFMb runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>&nbsp;= 
<ml:MoneyTextBox id=sLOrigF runat="server" preset="money" width="90" ReadOnly="True"></ml:MoneyTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap><%= AspxTools.HtmlControl(GenerateExpirationDateLink()) %></td>
          <td nowrap><ml:DateTextBox id=sRLckdExpiredD runat="server" preset="date" width="75"></ml:DateTextBox></td></tr></table></td></tr></table>

     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</html>
