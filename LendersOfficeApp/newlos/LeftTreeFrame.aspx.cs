namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DataRetrievalFramework;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.GenericFramework;
    using LendersOffice.ObjLib.TitleProvider;
    using LendersOffice.ObjLib.DynaTree;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using LendersOffice.ObjLib.CustomFavoriteFolder;

    public partial class LeftTreeFrame : LendersOffice.Common.BaseServicePage
	{
		protected bool m_isLON = false;
        private readonly List<string> x_disabledLpqItems = new List<string>() { "Duplicate this file", "Create subfinancing loan file", "Create template from this file" };   //opm 30792 av disable links for lpq
        private readonly string x_sLpqQuery = RequestHelper.GetSafeQueryString("lpq");
        private CPageData m_pageData;

        // OPM 231742, 12/14/2015, ML
        // Some pages have not implemented the methods required for the 
        // refreshCalculation() call made by the CTRL+ALT+G test (i.e. BindData
        // and LoadData) or cannot be accessed using an incomplete loan file.
        // We skip those pages by adding them to this list.
        private readonly HashSet<string> skippableTestPageIds = new HashSet<string>()
        {
            "1008_04",
            "Accounting",
            "AdjustmentsAndOtherCredits",
            "Batch_Editor",
            "BorrowerEmployments",
            "CARE885_1",
            "CARE885_2",
            "CARE885_3",
            "CARE885_4",
            "CheckEligibilityV2",
            "CoborrowerEmployments",
            "ConversationLog",
            "DataLayerMigration",
            "EmployeeResources",
            "Fax_Documents",
            "FeeAudit",
            "FHATransmittal",
            "LoanEstimate",
            "MismoClosing30",
            "PropHousingExpenses",
            "QualifiedLoanProgramSearch",
            "QualifiedLoanProgramSearchBeta",
            "RunInternalPricingV2",
            "Servicing",
            "Ship_Documents",
            "StatusAgents",
            "Upload_Documents",
            "VerbalVoe",
            "VerificationDashboard",

            // This page is handled automatically by the test.
            "Print"
        };

        protected bool IsInternalUser
        {
            get { return this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms); } // Internal Admin user.
        }

        private bool IsDeveloper
        {
            get
            {
                return ConstAppDavid.CurrentServerLocation == ServerLocation.Development ||
                    ConstAppDavid.CurrentServerLocation == ServerLocation.LocalHost ||
                    IsInternalUser; 
            }
        }

        /// <summary>
        /// The full base url, including the virtual root, to use for document generation.
        /// </summary>
        protected string DocumentGenerationUrlRoot
        {
            get { return ConstStage.UseEDocsSiteForDocumentGeneration ? Tools.GetEDocsLink(VirtualRoot) : VirtualRoot; }
        }

        protected E_sLT sLT
        {
            get { return m_pageData.sLT; }
        }
        protected string sLNm
        {
            get { return m_pageData.sLNm; }
        }
        protected E_sLPurposeT sLPurposeT
        {
            get { return m_pageData.sLPurposeT; }
        }
        protected bool IsLeadInFullEditor
        {
            get { return Broker.IsEditLeadsInFullLoanEditor && Tools.IsStatusLead(m_pageData.sStatusT); }
        }

        private BrokerDB m_brokerDB = null;
        private BrokerDB Broker
        {
            get 
            {
                if (m_brokerDB == null)
                {
                    m_brokerDB = this.BrokerUser.BrokerDB;
                }

                return m_brokerDB;
            }
        }

        protected BrokerUserPrincipal BrokerUser 
        {
            get { return BrokerUserPrincipal.CurrentPrincipal;  }
        }

        public static DynaTreeNode CreateDynaNodesFromFavorites(CustomFavoriteNode favoriteNode, Dictionary<string, List<DynaTreeNode>> dynaTreeDictionary, bool isUserFavorite, LendersOffice.Common.UI.Tree tree, bool isRoot, Dictionary<string, List<string>> associatedPages)
        {
            if(favoriteNode is CustomFavoriteFolderNode)
            {
                var folderNode = favoriteNode as CustomFavoriteFolderNode;
                isUserFavorite = folderNode.Folder.EmployeeId != null;
                var isUserFavoriteRoot = folderNode.Folder.ParentFolderId == null && folderNode.Folder.EmployeeId != null;
                if(!folderNode.IsVisible)
                {
                    return null;
                }

                var dynaNode = new DynaTreeNode()
                {
                    title = string.IsNullOrEmpty(folderNode.Folder.ExternalName) ?
                    folderNode.Folder.InternalName
                    : folderNode.Folder.ExternalName,
                    key = folderNode.Folder.FolderId.ToString(),
                    isFolder = true,
                    addClass = "PageContainer",
                    onCreate = "initializeNodes",
                    expand = true
                };

                dynaNode.customAttributes.Add("path", folderNode.Folder.FolderId.ToString());
                tree.DetermineFolderCollapsed(dynaNode);

                if(isUserFavoriteRoot)
                {
                    dynaNode.customAttributes.Add("isUserFavoriteFolder", "true");
                }
                else if (isUserFavorite)
                {
                    dynaNode.customAttributes.Add("isUserSubFavoriteFolder", "true");
                }

                foreach(CustomFavoriteNode child in folderNode.Children)
                {
                    var childDynaNode = CreateDynaNodesFromFavorites(child, dynaTreeDictionary, isUserFavoriteRoot, tree, false, associatedPages);
                    if(childDynaNode != null)
                    {
                        dynaNode.children.Add(childDynaNode);
                    }

                    List<string> associatedPageIds;
                    if (child is CustomFavoritePageNode && associatedPages.TryGetValue((child as CustomFavoritePageNode).PageId, out associatedPageIds))
                    {
                        foreach (var associatedPageId in associatedPageIds)
                        {
                            var associatedPageNode = new CustomFavoritePageNode(associatedPageId, child.SortOrder);
                            var associatedDynaNode = CreateDynaNodesFromFavorites(associatedPageNode, dynaTreeDictionary, isUserFavoriteRoot, tree, false, associatedPages);
                            if (associatedDynaNode != null)
                            {
                                dynaNode.children.Add(associatedDynaNode);
                            }
                        }
                    }
                }

                return dynaNode.children.Any() || isRoot ? dynaNode : null;
            }
            else
            {
                var pageNode = favoriteNode as CustomFavoritePageNode;
                List<DynaTreeNode> dynaNodeList;

                if (dynaTreeDictionary.TryGetValue(pageNode.PageId, out dynaNodeList))
                {
                    foreach (var dynaNode in dynaNodeList)
                    {

                        if (isUserFavorite && !dynaNode.customAttributes.ContainsKey("isUserFavorite"))
                        {
                            dynaNode.customAttributes.Add("isUserFavorite", "true");
                        }
                    }

                    return dynaNodeList.First();
                }

                return null;
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            // 5/10/2005 dd - Retrieve user loan edit menu tree state
            string loanEditorMenuTreeStateXmlContent = "";
            
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserID", BrokerUser.UserId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.ConnectionInfo, "RetrieveLoanEditorMenuTreeState", parameters )) 
            {
                if (reader.Read())
                    loanEditorMenuTreeStateXmlContent = (string) reader["LoanEditorMenuTreeStateXmlContent"];
            }
            m_pageData = CPageData.CreateUsingSmartDependency(RequestHelper.LoanID, typeof(LeftTreeFrame));
            m_pageData.InitLoad();

            RegisterJsGlobalVariables("isFHA", m_pageData.sLT == E_sLT.FHA);
            RegisterJsGlobalVariables("isVA", m_pageData.sLT == E_sLT.VA);
            RegisterJsGlobalVariables("isConstruction", m_pageData.sLPurposeT == E_sLPurposeT.Construct || m_pageData.sLPurposeT == E_sLPurposeT.ConstructPerm); ;
            RegisterService("folderNavigation", "/los/admin/CustomFolderNavigationService.aspx");
            
            var a = Broker.DocuTechInfo;
            if (a != null)
            {
                RegisterService("Docutech", "/newlos/services/DocutechService.aspx");
            }

            DocListUrl.Value = Tools.GetEDocsLink( Tools.VRoot + "/newlos/ElectronicDocs/DocList.aspx?loanid=" + RequestHelper.LoanID);

            LendersOffice.Common.UI.Tree tree = new LendersOffice.Common.UI.Tree(this);
            tree.SpecialTypeNode += new LendersOffice.Common.UI.SpecialTypeNodeHandler(this.SpecialTreeNode);
            tree.OnNodeCreate += new LendersOffice.Common.UI.NodeCreateHandler(this.TreeNode_Create);

            tree.MenuTreeStateXmlContent = loanEditorMenuTreeStateXmlContent;
            ClientScript.RegisterHiddenField("loanEditorMenuTreeStateXmlContent", loanEditorMenuTreeStateXmlContent);

            var configFileName = "LoanNavigationNew.xml.config";

            var favorites = CustomFavoriteFolderHelper.RetrieveCustomFavoriteFolderTreeByBrokerId(
                PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId, true );

            CustomFavoriteFolderNode root;

            root = new CustomFavoriteFolderNode();
            root.Folder = new CustomFavoriteFolder(PrincipalFactory.CurrentPrincipal.BrokerId,  PrincipalFactory.CurrentPrincipal.EmployeeId);
            root.Children = favorites;

            root.IsVisible = true;
            var groupIds = GroupDB.ListInclusiveGroupForEmployee(this.Broker.BrokerID, PrincipalFactory.CurrentPrincipal.EmployeeId).Select(group => group.Id);
            var roleIds = LendersOffice.Security.PrincipalFactory.CurrentPrincipal.GetRoles().Select(roleT => Role.Get(roleT).Id);

            var landingPageId = EmployeeDB.RetrieveLandingPageIdByEmployeeId(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId);
            var filter = new CustomFavoriteFolderFilter() {
                EmployeeId = PrincipalFactory.CurrentPrincipal.EmployeeId,
                LeadLoanStatus = Tools.IsStatusLead(m_pageData.sStatusT) ? LeadLoanStatusT.Lead : LeadLoanStatusT.Loan,
                GroupIds = groupIds,
                RoleIds = roleIds,
                LoanPurposes = new List<E_sLPurposeT>() { m_pageData.sLPurposeT },
                LoanTypes = new List<E_sLT>() { m_pageData.sLT }
            };
            CustomFavoriteFolderHelper.CheckCustomFavoriteFolderNodePermissionRecursive(root, filter);
            tree.Load(Tools.GetServerMapPath(configFileName));
            
            var dynaNode = tree.Render(false);
            var flatDynaNodeDict = CustomFavoriteFolderHelper.FlattenDynaNodePages(dynaNode, new Dictionary<string, List<DynaTreeNode>>());
            List<DynaTreeNode> landingPageList;
            if(flatDynaNodeDict.TryGetValue(landingPageId, out landingPageList))
            {
                landingPageList.First().customAttributes.Add("landingpage", "true");
            }

            var dynaNodeRoot = CreateDynaNodesFromFavorites(root, flatDynaNodeDict, false, tree, true, CustomFavoriteFolderHelper.AssociatedPages);
            dynaNodeRoot.children = dynaNodeRoot.children.Concat(dynaNode.children).ToList();

            m_tree.Text = "<div id='pageNavigation'></div>";
            this.RegisterJsGlobalVariables("navigationModel", SerializationHelper.JsonNetSerialize(dynaNodeRoot));

            if (tree.MenuTreeStateWidth > 0)
            {
                string script = "f_changeFrameSize(" + tree.MenuTreeStateWidth.ToString() + ");";
                ClientScript.RegisterStartupScript(this.GetType(), "CHANGE_FRAME_SIZE", script, true);
            }
            
            XmlDocument doc = new XmlDocument();
            doc.Load(Tools.GetServerMapPath(configFileName));

            XmlNodeList list = doc.SelectNodes("//item[@pageid]");
            var pages = new HashSet<string>();
            foreach (XmlElement el in list)
            {
                var pageId = el.GetAttribute("pageid");

                if (!this.skippableTestPageIds.Contains(pageId))
                {
                    pages.Add(string.Format("'{0}'", pageId));
                }
            }

            ClientScript.RegisterArrayDeclaration("testpages", string.Join(",", pages));
            
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("dynatree/jquery.dynatree.js");
            this.RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");
            this.RegisterJsScript("json.js");
            this.RegisterJsScript("Favorites.js");
            this.RegisterCSS("Favorites.css");

            this.RegisterJsGlobalVariables("sBorrowerApplicationCollectionT", (int)this.m_pageData.sBorrowerApplicationCollectionT);
            this.RegisterJsGlobalVariables("IsTargeting2019Ulad", this.m_pageData.sIsTargeting2019Ulad);

            if (PrincipalFactory.CurrentPrincipal.HasConversationLogQuickPostingEnabled)
            {
               this.RegisterJsScript("quickReplyInjector.js");
            }
            else
            {
                this.LoadsQuickReplyInjector.Visible = false;
            }
        }
        
        protected override bool EnableJQueryInitCode
        {
            get { return true; }
        }

        public static void SpecialTreeNode_UserConfig(LendersOffice.Common.UI.TreeNode node, string code, LendersOffice.Common.UI.Tree tree)
        {
            SpecialTreeNode_AlmostNonLoanDependent(node, code, tree, PrincipalFactory.CurrentPrincipal.BrokerDB, PrincipalFactory.CurrentPrincipal, false, null, false);
        }
        public static void SpecialTreeNode_AdminConfig(LendersOffice.Common.UI.TreeNode node, string code, LendersOffice.Common.UI.Tree tree)
        {
            SpecialTreeNode_AlmostNonLoanDependent(node, code, tree, PrincipalFactory.CurrentPrincipal.BrokerDB, PrincipalFactory.CurrentPrincipal, false, null, false);
        }

        public static void SpecialTreeNode_AdminConfig(LendersOffice.Common.UI.TreeNode node, string code, LendersOffice.Common.UI.Tree tree, BrokerDB broker, AbstractUserPrincipal brokerUser, bool isLeftTreeFrame, CPageData m_pageData)
        {
            SpecialTreeNode_AlmostNonLoanDependent(node, code, tree, PrincipalFactory.CurrentPrincipal.BrokerDB, PrincipalFactory.CurrentPrincipal, false, null, true);
        }

        public static void SpecialTreeNode_UserDependent(LendersOffice.Common.UI.TreeNode node, string code, LendersOffice.Common.UI.Tree tree, BrokerDB broker, AbstractUserPrincipal brokerUser, bool isLeftTreeFrame, CPageData m_pageData)
        {
            LendersOffice.Common.UI.TreeFolder folder;
            switch (code)
            {
                case "LON":
                    node.IsVisible = (brokerUser as BrokerUserPrincipal).HasLONIntegration;
                    break;
                case "DavidOnly":
                    node.IsVisible = brokerUser.LoginNm == "david_acc" || brokerUser.LoginNm == "antoniov"||  brokerUser.LoginNm.ToLower() == "lester";
                    break;
                case "CreateTemplate":
                    node.IsVisible = brokerUser.LoginNm.TrimWhitespaceAndBOM().Equals("PML0204", StringComparison.InvariantCultureIgnoreCase);
                    break;
                case "PoolAssignment":
                    if (brokerUser.HasPermission(Permission.AllowCapitalMarketsAccess))
                        node.IsVisible = true;
                    break;
                case "QualityControl":
                    if (!brokerUser.HasPermission(Permission.AllowQualityControlAccess))
                        node.IsVisible = false;
                    break;
                case "GlobalDMSAppraisals":
                    if (!(brokerUser.HasPermission(Permission.AllowOrderingGlobalDMSAppraisals) &&
                           broker.IsAllowOrderingGlobalDmsAppraisals))
                        node.IsVisible = false;
                    break;
                case "SubmitCoreSpecial":
                    node.IsVisible = brokerUser.IsInEmployeeGroup(ConstAppDavid.CoreExportEmployeeGroup) &&
                        (broker.CustomerCode == "PML0223" // PML0223 - First Tech
                        || brokerUser.LoginNm == "david_acc");
                    break;
                case "MiserSpecial":

                    node.IsVisible = brokerUser.IsInEmployeeGroup(ConstAppDavid.CoreExportEmployeeGroup) &&
                        (broker.CustomerCode == "PML0215" || brokerUser.LoginNm == "david_acc");
                    break;
                case "SubmitToThirdPartySpecial":
                    node.IsVisible = brokerUser.IsInEmployeeGroup(ConstAppDavid.ThirdPartyExportEmployeeGroup) && (DataRetrievalPartner.CountPartnersByBrokerId(brokerUser.BrokerId) > 0);
                    break;
                case "MULTI_DOC_VENDORS":
                    node.IsVisible = false;
                    folder = node.Parent as LendersOffice.Common.UI.TreeFolder;
                    if (folder == null) break;

                    foreach (DocumentVendorBrokerSettings settings in broker.GetDocumentVendors(isLeftTreeFrame && m_pageData.sLoanFileT == E_sLoanFileT.Test, isLeftTreeFrame))
                    {
                        if (isLeftTreeFrame)
                        {

                            if (settings.VendorId == Guid.Empty)
                            {
                                if (m_pageData.sLoanRequiresTridDisclosures)
                                {
                                    continue;
                                }
                            }
                            else if (!m_pageData.sLoanRequiresTridDisclosures)
                            {
                                var vendor = VendorConfig.Retrieve(settings.VendorId);
                                if (vendor.PlatformType == E_DocumentVendor.DocMagic)
                                {
                                    continue;
                                }
                            }

                            var login = VendorCredentials.GetUserCredentialsOnly(brokerUser.UserId, broker.BrokerID, settings.VendorId);
                            if (!login.UserEnabled)
                            {
                                continue;
                            }
                        }

                        var item = new LendersOffice.Common.UI.TreeNode(settings.ServicesLinkNm);
                        item.Attributes["id"] = "MultiDocVendor" + (settings.VendorId);
                        item.Attributes["class"] = "TreeNodeLink";
                        item.Attributes["onclick"] = "return f_viewMultiDocGeneration('" + settings.VendorId + "');";
                        item.Attributes["pageid"] = "DocMagicDocGeneration";
                        item.Attributes["navDupInd"] = node.Attributes["navDupInd"];
                        folder.AddChild(item);
                    }
                    break;
                case "SpecialVendorPortal":
                    node.IsVisible = false;
                    folder = node.Parent as LendersOffice.Common.UI.TreeFolder;
                    if (folder == null)
                        break;

                    foreach (DocumentVendorBrokerSettings settings in broker.GetDocumentVendors(isLeftTreeFrame && m_pageData.sLoanFileT == E_sLoanFileT.Test, isLeftTreeFrame)
                        .Where(vendor => vendor.VendorId != Guid.Empty && VendorConfig.Retrieve(vendor.VendorId).PlatformType == E_DocumentVendor.IDS))
                    {
                        var login = VendorCredentials.GetUserCredentialsOnly(brokerUser.UserId, broker.BrokerID, settings.VendorId);
                        if (!login.UserEnabled)
                        {
                            continue;
                        }

                        var item = new LendersOffice.Common.UI.TreeNode($"Document Portal - {settings.ServicesLinkNm}");
                        item.Attributes["id"] = "DocVendorPort" + (settings.VendorId);
                        item.Attributes["class"] = "TreeNodeLink";
                        item.Attributes["onclick"] = "return f_getDocumentVendorPortalUrl('" + settings.VendorId + "');";
                        item.Attributes["pageid"] = "DocVendorPort";
                        item.Attributes["navDupInd"] = node.Attributes["navDupInd"];
                        folder.AddChild(item);
                    }

                    break;
            }
        }

        public static void SpecialTreeNode_AlmostNonLoanDependent(LendersOffice.Common.UI.TreeNode node, string code, LendersOffice.Common.UI.Tree tree, BrokerDB broker, AbstractUserPrincipal brokerUser, bool isLeftTreeFrame, CPageData m_pageData, bool isAdminConfig)
        {
            LendersOffice.Common.UI.TreeFolder folder;
            switch(code)
            {
				case "DATATRAC":
                    node.IsVisible = broker.IsDataTracIntegrationEnabled;
					break;
                case "HideIfEmpty":
                    folder = node as LendersOffice.Common.UI.TreeFolder;
                    if (folder == null) break;
                    
                    folder.IsVisible = folder.HasVisibleChildren;
                    break;
                case "ComplianceEagle":
                    node.IsVisible = broker.IsEnableComplianceEagleIntegration;
                    break;
                case "ComplianceEase":
                    node.IsVisible = broker.IsEnableComplianceEaseIntegration;
                    break;
                case "Generic_Framework_Vendors":
                    node.IsVisible = false;
                    folder = node.Parent as LendersOffice.Common.UI.TreeFolder;
                    if (folder == null) break;

                    foreach (GenericFrameworkVendor vendor in GenericFrameworkVendor.LoadVendors(broker.BrokerID)
                        .Where(vendor => !isLeftTreeFrame || vendor.LaunchLinkConfig.IsDisplayLinkForLoanAndUser(RequestHelper.LoanID, LinkLocation.LoanEditor)))
                    {
                        var item = new LendersOffice.Common.UI.TreeNode(vendor.LaunchLinkConfig.LinkDisplayName(LinkLocation.LoanEditor));
                        item.Attributes["id"] = "GenericFrameworkVendor_" + vendor.ProviderID;
                        item.Attributes["class"] = "TreeNodeLink";
                        item.Attributes["onclick"] = "return f_viewGenericFramework('" + vendor.ProviderID + "');";
                        folder.AddChild(item);
                    }

                    break;
            }

            if(!isAdminConfig)
            {
                SpecialTreeNode_UserDependent(node, code, tree, broker, brokerUser, isLeftTreeFrame, m_pageData);
            }
        }

        private void SpecialTreeNode(LendersOffice.Common.UI.TreeNode node, string code, LendersOffice.Common.UI.Tree tree) 
        {
            switch (code)
            {
				case "TemplateOnly":
					node.IsVisible = m_isTemplate;
					break;
            }

            SpecialTreeNode_AlmostNonLoanDependent(node, code, tree, this.Broker, this.BrokerUser, true, m_pageData, false);
        }

        private bool m_isTemplate
        {
            get { return m_pageData.IsTemplate; }
        }
        public static void TreeNode_Create_AdminConfig(LendersOffice.Common.UI.TreeNode item)
        {
            TreeNode_Create_NonInternal_NonLoanDependent(item, true);
        }

        public static void TreeNode_Create_UserConfig(LendersOffice.Common.UI.TreeNode item)
        {
            TreeNode_Create_NonInternal_NonLoanDependent(item, false);
        }

        public static void TreeNode_Create_NonInternal_NonLoanDependent(LendersOffice.Common.UI.TreeNode item, bool isAdminConfig)
        {
            var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
            var brokerUser = PrincipalFactory.CurrentPrincipal;
            var internalOnlyPages = new List<string>() { "Page_SellerClosingDisclosureArchive", "Page_SellerClosingCostsArchive" };

            item.IsVisible = !internalOnlyPages.Contains(item.ID) || brokerUser.HasPermission(Permission.CanModifyLoanPrograms) && !isAdminConfig;
            
            TreeNode_Create_NonLoanDependent(item, broker, brokerUser, isAdminConfig);
        }

        public static void TreeNode_Create_UserDependent(LendersOffice.Common.UI.TreeNode item, BrokerDB broker, AbstractUserPrincipal brokerUser)
        {
              
            if (item.ID == "FHATotalAudit" || item.ID == "FHATOTALFindings")
            {
                item.IsVisible = broker.IsTotalScorecardEnabled && brokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) && brokerUser.HasPermission(Permission.CanAccessTotalScorecard);
            }
            else if (item.ID == "Folder_Underwriting")
            {
                item.IsVisible = broker.HasLenderDefaultFeatures && brokerUser.HasPermission(Permission.AllowUnderwritingAccess);
            }
            else if (item.ID == "Folder_PriceMyLoan" || item.ID == "Page_Certificate" || item.ID == "Page_PmlReviewInfo" || item.ID == "Page_ViewbrokerNotes" || item.ID == "Page_QualifiedLoanProgramSearch" || item.ID == "Page_QualifiedLoanProgramSearchBeta")
            {
                item.IsVisible = brokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);

                if (item.IsVisible && item.ID == "Page_QualifiedLoanProgramSearch")
                {
                    if (broker.IsForcePML3)
                    {
                        item.IsVisible = false;
                    }
                }

                if (item.IsVisible && item.ID == "Page_QualifiedLoanProgramSearchBeta")
                {
                    item.IsVisible = broker.IsEnablePmlResults3Ui(PrincipalFactory.CurrentPrincipal);

                    if (broker.IsForcePML3)
                    {
                        item.Name = "Run PriceMyLoan/Lock Request";
                    }
                }
            }
            else if (item.ID == "InvestorRolodexInfo")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowViewingInvestorInformation);
            }
            else if (item.ID == "Page_TrailingDocuments")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowAccountantRead) && brokerUser.HasPermission(Permission.AllowViewingInvestorInformation);
            }
            else if (item.ID == "Folder_Closer")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowCloserRead);
            }
            else if (item.ID == "Folder_Finance" || item.ID == "Folder_Shipping" || item.ID == "Folder_Servicing")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowAccountantRead);
            }
            else if (item.ID == "Folder_LockDesk")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowLockDeskRead);
            }
            else if (item.ID == "Page_RunInternalPricing")
            {
                item.IsVisible = !brokerUser.UseInternalPricerV2;
            }
            else if (item.ID == "Page_RunInternalPricingV2")
            {
                item.IsVisible = brokerUser.UseInternalPricerV2;
            }
            else if (item.ID == "Page_Disbursement" || item.ID == "Page_Disbursement_Purchasing")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowCloserRead) && brokerUser.HasPermission(Permission.AllowAccountantRead);
            }
            else if (item.ID == "Page_Accounting")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowLockDeskRead) && brokerUser.HasPermission(Permission.AllowCloserRead) && brokerUser.HasPermission(Permission.AllowAccountantRead);
            }
            else if (item.ID == "Page_Servicing")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowCloserRead) && brokerUser.HasPermission(Permission.AllowAccountantRead);
            }
            else if (item.ID == "Page_SecondaryStatus")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowCloserRead) && brokerUser.HasPermission(Permission.AllowAccountantRead) && brokerUser.HasPermission(Permission.AllowLockDeskRead);
            }
            else if (item.ID == "Page_StatusCommissions")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowCloserRead) && brokerUser.HasPermission(Permission.AllowAccountantRead);
            }
            else if (item.ID == "Page_LendingStaffNotes") //opm 50434 fs 07/30/10
            {
                item.IsVisible = brokerUser.HasPermission(Permission.CanAccessLendingStaffNotes);
            }
            else if (item.ID == "Page_CheckEligibility")
            {
                item.IsVisible = !brokerUser.UseInternalPricerV2
                    && (brokerUser.HasRole(E_RoleT.Underwriter) || brokerUser.HasRole(E_RoleT.JuniorUnderwriter) || brokerUser.IsInEmployeeGroup(ConstAppDavid.CheckEligibilityEmployeeGroup));
            }
            else if (item.ID == "Page_CheckEligibilityV2")
            {
                item.IsVisible = brokerUser.UseInternalPricerV2
                    && (brokerUser.HasRole(E_RoleT.Underwriter) || brokerUser.HasRole(E_RoleT.JuniorUnderwriter) || brokerUser.IsInEmployeeGroup(ConstAppDavid.CheckEligibilityEmployeeGroup));
            }
            else if (item.ID == "Page_StatusConditions")
            {
                item.IsVisible = brokerUser.HasLenderDefaultFeatures == false && broker.IsUseNewTaskSystem == false && brokerUser.HasPermission(Permission.AllowUnderwritingAccess);
            }
            else if (item.ID == "Page_NewStatusConditions")
            {
                item.IsVisible = brokerUser.HasLenderDefaultFeatures == false && broker.IsUseNewTaskSystem == true && brokerUser.HasPermission(Permission.AllowUnderwritingAccess);
            }
            else if (item.ID == "Page_UnderwritingConditions")
            {
                item.IsVisible = brokerUser.HasLenderDefaultFeatures == true && broker.IsUseNewTaskSystem == false
                    && (brokerUser.HasPermission(Permission.AllowUnderwritingAccess) || brokerUser.HasPermission(Permission.AllowCloserRead));
            }
            else if (item.ID == "Page_UnderwritingConditionsForTask" || item.ID == "Page_ConditionSignoff")
            {
                item.IsVisible = brokerUser.HasLenderDefaultFeatures == true && broker.IsUseNewTaskSystem == true
                    && (brokerUser.HasPermission(Permission.AllowUnderwritingAccess) || brokerUser.HasPermission(Permission.AllowCloserRead));
            }
            else if (item.ID == "Folder_Funding" || item.ID == "Page_Funding")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowCloserRead);
            }
            else if (item.ID == "Page_TitleQuoteRequest")
            {
                item.IsVisible = false;
                if (brokerUser.HasPermission(Permission.AllowOrderingTitleServices))
                {
                    int count = TitleProvider.GetAssociations(broker.BrokerID).Count;
                    item.IsVisible = count > 0;
                }
            }
            else if (item.ID == "Page_Order4056T")
            {
                item.IsVisible = broker.Has4506TIntegration && brokerUser.HasPermission(Permission.AllowOrder4506T);
            }
            else if (item.ID == "Page_InternalSupportPage")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.CanModifyLoanPrograms); // Internal Admin user.
            }
            else if (item.ID == "Page_ManageNewFeatures" || item.ID == "Page_DataLayerMigration")
            {
                item.IsVisible = brokerUser.HasPermission(Permission.AllowManageNewFeatures);
            }
            else if (item.ID == "Capture_Audit")
            {
                item.IsVisible = broker.EnableKtaIntegration && brokerUser.HasPermission(Permission.AllowAccessingLoanEditorDocumentCaptureAuditPage);
            }
            else if (item.ID == "Capture_Review")
            {
                item.IsVisible = broker.EnableKtaIntegration && brokerUser.HasPermission(Permission.AllowAccessingLoanEditorDocumentCaptureReviewPage);
            }
        }

        public static void TreeNode_Create_NonLoanDependent(LendersOffice.Common.UI.TreeNode item, BrokerDB broker, AbstractUserPrincipal brokerUser, bool isAdminConfig)
        {
            var m_HasAutoDocutechEnabled = broker.DocuTechInfo != null;
            if (item.ID == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Node " + item.Name + " does not have ID");
            }

            else if (item.ID == "Page_ExportLPQ")
            {
                item.IsVisible = broker.HasLPQExportEnabled;
            }
            else if (item.ID == "Page_Funding")
            {
                item.IsVisible = broker.HasLenderDefaultFeatures;
            }
            else if (item.ID == "Page_DocuTech")
            {
                item.IsVisible = (broker.IsDocuTechEnabled || broker.IsDocuTechStageEnabled) && !m_HasAutoDocutechEnabled;
            }
            else if (item.ID == "Page_DocuTech_1")
            {
                item.IsVisible = (broker.IsDocuTechEnabled || broker.IsDocuTechStageEnabled) && m_HasAutoDocutechEnabled;
            }
            else if (item.ID == "Page_NewTaskList")
            {
                item.IsVisible = broker.IsUseNewTaskSystem;
            }
            else if (item.ID == "Page_CreateNewTask") // It should not be visible if the broker is using the new task system
            {
                item.IsVisible = !broker.IsUseNewTaskSystem;
            }
            else if (item.ID == "Page_TaskList")
            {
                item.IsVisible = !broker.IsUseNewTaskSystem;
            }
            else if (item.ID == "Page_DocMagic")
            {
                item.IsVisible = (broker.BillingVersion == E_BrokerBillingVersion.PerTransaction
                    && broker.IsEnablePTMDocMagicOnlineInterface)
                    || broker.BillingVersion != E_BrokerBillingVersion.PerTransaction;
            }
            else if (item.ID == "Page_FHAConnection")
            {
                item.IsVisible = broker.IsEnableFhaConnection;
            }
            else if (item.ID == "Page_FHAConnectionResults")
            {
                item.IsVisible = broker.IsEnableFhaConnection;
            }
            else if (item.ID == "Page_DataVerifyDRIVE")
            {
                item.IsVisible = broker.IsEnableDriveIntegration;
            }
            else if (item.ID == "Page_LoanRepurchase")
            {
                item.IsVisible = broker.EnableLoanRepurchasePage;
            }
            else if (item.ID == "Page_FreddieLoanQualityAdvisorExport" ||
                item.ID == "Page_FHLMCLQA")
            {
                item.IsVisible = broker.IsEnableFreddieMacLoanQualityAdvisor;
            }
            else if (item.ID == "Page_FannieMaeEarlyCheck")
            {
                item.IsVisible = broker.IsEnableFannieMaeEarlyCheck;
            }
            else if (item.ID == "Page_DUSubmission")
            {
                item.IsVisible = broker.IsEnableSeamlessDu;
            }
            else if (item.ID == "Page_LoanOfficerPricingPolicy")
            {
                item.IsVisible = broker.IsCustomPricingPolicyFieldEnabled;
            }
            else if (item.ID == "Page_ConversationLog")
            {
                item.IsVisible = broker.ActuallyEnableConversationLogForLoans;
            }
            else if (item.ID == "Page_BigLoanInformation")
            {
                item.IsVisible = broker.IsEnableBigLoanPage;
            }
            else if (item.ID == "Page_LoanInformation")
            {
                item.IsVisible = !broker.IsEnableBigLoanPage;
            }
            else if (item.ID == "Page_ExportDataToCore")
            {
                item.IsVisible = LendersOffice.Integration.Kiva.KivaExportProcessor.IsExportEnabled(broker);
            }
            else if (item.ID == "Page_StatusEvents")
            {
                item.IsVisible = !ConstStage.DisableStatusEventFeature;
            }
            else if (item.ID == "Document_Capture")
            {
                item.IsVisible = broker.EnableKtaIntegration;
            }

            if (!isAdminConfig)
            {
                TreeNode_Create_UserDependent(item, broker, brokerUser);
            }
        }

        private void TreeNode_Create(LendersOffice.Common.UI.TreeNode item) 
        {
            TreeNode_Create_NonLoanDependent(item, this.Broker, this.BrokerUser, false);

            if (item.ID == null)
                throw new CBaseException(ErrorMessages.Generic, "Node " + item.Name + " does not have ID");

                        //opm 30792 av disable links  for Lpq
            if (x_sLpqQuery == "1" && x_disabledLpqItems.Contains(item.Name))
            {
                item.IsVisible = false;
            }
            else if (item.ID == "ConstructionLoanInfo")
            {
                item.IsVisible = m_pageData.sLPurposeT == E_sLPurposeT.Construct || m_pageData.sLPurposeT == E_sLPurposeT.ConstructPerm;
            }
            else if (item.ID == "QM")
            {
                item.IsVisible = item.Parent.ID.Equals("Folder_ObsoleteDisclosuresGFETILlegacy") || item.Parent.ID.Equals("Folder_Favorites") || m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.ClosingCostFee2015;
            }
            else if (item.ID == "QM2015")
            {
                item.IsVisible = m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;

                if (item.IsVisible && m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    item.Name = "QM";
                }
            }
            else if (item.ID == "Folder_DisclosuresGFETILlegacy")
            {
                if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    item.Name = "Disclosures";
                    item.IsVisible = true;
                }
                else if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated)
                {
                    item.IsVisible = true;
                }
                else if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    item.IsVisible = false;
                }
            }
            else if (item.ID == "Folder_DisclosuresTRIDnew")
            {
                if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy || m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
                {
                    item.IsVisible = false;
                }
                else if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated)
                {
                    item.IsVisible = true;
                }
                else if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    item.Name = "Disclosures TRID";
                    item.IsVisible = true;
                }
            }
            else if (item.ID == "Folder_DisclosuresGFETILnew")
            {
                if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy || m_pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                {
                    item.IsVisible = false;
                }
                else if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated)
                {
                    item.IsVisible = true;
                }
                else if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    item.Name = "Disclosures GFE/TIL";
                    item.IsVisible = true;
                }
            }
            else if (item.ID == "Folder_ObsoleteDisclosuresGFETILlegacy")
            {
                if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    item.IsVisible = true;
                }
                else
                {
                    item.IsVisible = false;
                }
            }
            //else if (item.ID == "Folder_Disclosures")
            //{
            //    // Hide for now until Disclosure folders are settled.
            //    item.IsVisible = false;
            //}
            if (item.ID == "Page_PMLDefaultValues")
            {
                item.IsVisible = m_isTemplate && BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
            }
            else if (item.ID == "Page_TitleFrameworkDashboard")
            {
                item.IsVisible = !this.Broker.DisableNewTitleFramework
                    && LendersOffice.Integration.TitleFramework.TitleOrder.LoanHasOrdersOrLenderServices(RequestHelper.LoanID, this.Broker.BrokerID);
            }
            else if (item.ID == "Page_VerificationDashboard")
            {
                item.IsVisible = Broker.IsEnableVOXConfiguration && LendersOffice.Integration.VOXFramework.AbstractVOXOrder.LoanHasOrdersOrLenderServices(RequestHelper.LoanID, Broker.BrokerID);
            }
            else if (item.ID == "Electronic_Docs")   //hide if all edoc permissions are off
            {
                // 6/10/2014 dd - Hide eDocs if loan is sandbox loan.
                item.IsVisible = Broker.IsEDocsEnabled && BrokerUser.HasPermission(Permission.CanViewEDocs) && (m_pageData.sLoanFileT == E_sLoanFileT.Loan || m_pageData.sLoanFileT == E_sLoanFileT.Test);
            }
            else if (item.ID == "Folder_Purchasing")
            {
                item.IsVisible = m_pageData.sBranchChannelT == E_BranchChannelT.Correspondent && BrokerUser.HasPermission(Permission.AllowAccountantRead);
            }
            else if (item.ID == "Page_Duplicate")
            {
                item.IsVisible = BrokerUser.HasPermission(Permission.CanDuplicateLoans) && (m_pageData.sLoanFileT == E_sLoanFileT.Loan || m_pageData.sLoanFileT == E_sLoanFileT.Test);
            }
            else if (item.ID == "Page_CreateTest")
            {
                item.IsVisible = BrokerUser.HasPermission(Permission.AllowCreatingTestLoans) && m_pageData.sLoanFileT == E_sLoanFileT.Loan;
            }
            else if (item.ID == "Page_TestConfig")
            {
                item.IsVisible = m_pageData.sLoanFileT == E_sLoanFileT.Test;
            }
            else if (item.ID == "Page_Sandbox")
            {

                item.IsVisible = false; // 6/11/2014 dd - Let disable this page for now.
            }
            else if (item.ID == "Page_SandboxList")
            {
                item.IsVisible = Broker.IsSandboxEnabled && !m_isTemplate && m_pageData.sLoanFileT == E_sLoanFileT.Loan;
            }
            else if (item.ID == "Page_CreateSecondLoan")
            {
                // 6/13/2014 dd - Only allow to create subordinate loan on actual loan and not sandbox or test file.
                item.IsVisible = m_pageData.sLoanFileT == E_sLoanFileT.Loan || m_pageData.sLoanFileT == E_sLoanFileT.Test;
            }
            else if (item.ID == "Page_2010GFE")
            {
                item.IsVisible = (item.Parent != null && (item.Parent.ID.Equals("Folder_ObsoleteDisclosuresGFETILlegacy") || item.Parent.ID.Equals("Folder_Favorites") || item.Parent.ID.Equals("Folder_DisclosuresGFETILlegacy"))) ||
                    (m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.ClosingCostFee2015 && m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID);
            }
            else if (item.ID == "Page_FeeAudit")
            {
                item.IsVisible = m_pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            }
            else if (item.ID == "Page_2010GFEArchive")
            {
                item.IsVisible = Broker.IsGFEandCoCVersioningEnabled &&
                    (item.Parent.ID.Equals("Folder_ObsoleteDisclosuresGFETILlegacy") || item.Parent.ID.Equals("Folder_Favorites") ||
                     m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy);

                if (item.IsVisible && (m_pageData.sLastDisclosedGFEArchiveD_rep == ""  // don't hit the filedb just yet.
                    || m_pageData.sDoesGfeArchivesDataExist == false))
                {
                    item.Attributes["onclick"] = "alert('No GFE Archive on file.  "
                    + "Please create one before attempting to access the archives.');"
                    + "return false;";
                }
            }
            else if (item.ID == "Page_ChangeOfCircumstances")
            {
                item.IsVisible = item.Parent.ID.Equals("Folder_ObsoleteDisclosuresGFETILlegacy") || item.Parent.ID.Equals("Folder_Favorites") ||
                    m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy;

                if (Broker.IsGFEandCoCVersioningEnabled)
                {
                    if (m_pageData.sLastDisclosedGFEArchiveD_rep == "" || m_pageData.sDoesGfeArchivesDataExist == false) // don't hit the filedb just yet.
                    {
                        item.Attributes["onclick"] = "alert('No GFE Archive on file.  "
                        + "Please create one before attempting to apply a Change of Circumstance.');"
                        + "return false;";
                    }
                    else
                    {
                        item.Attributes["onclick"] = "return f_viewChangeOfCircumstancesForArchives();";
                    }
                }
            }
            else if (item.ID == "Page_ChangeOfCircumstancesNew")
            {
                item.IsVisible = m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                    && Broker.IsGFEandCoCVersioningEnabled;

                if (item.IsVisible && m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    item.Name = "Change of Circumstances";
                }

                if (item.IsVisible)
                {
                    if (!m_pageData.sHasLoanEstimateArchive && !m_pageData.sHasGfe2015Archive)
                    {
                        item.Attributes["onclick"] = "alert('No Archive on file.  "
                        + "Please create one before attempting to apply a Change of Circumstance.');"
                        + "return false;";
                    }
                    else
                    {
                        item.Attributes["onclick"] = "return f_viewChangeOfCircumstancesForCcArchive();";
                    }
                }
            }
            else if (item.ID == "Page_OrderMIPolicy")
            {
                item.IsVisible = this.Broker.HasMIVendorIntegration && this.BrokerUser.HasPermission(Permission.AllowOrderingPMIPolicies)
                    && (this.m_pageData.sLoanFileT == E_sLoanFileT.Loan || this.m_pageData.sLoanFileT == E_sLoanFileT.Test) && !this.m_pageData.IsTemplate;
            }
            else if (item.ID == "Page_AdditionalHELOC")
            {
                item.IsVisible = Broker.IsEnableHELOC && (m_pageData.IsTemplate || m_pageData.sIsLineOfCredit);
            }
            else if (item.ID == "Page_SettlementCharges")
            {
                item.IsVisible = m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.ClosingCostFee2015;
            }
            else if (item.ID == "Page_AdditionalHUD1Data")
            {
                item.IsVisible = m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID;
            }
            else if (item.ID == "Page_SettlementChargesMigration")
            {
                item.IsVisible = !m_pageData.IsTemplate && Broker.IsGFEandCoCVersioningEnabled
                    && (!m_pageData.sUseGFEDataForSCFields || m_pageData.sHasClosingCostMigrationArchive);

                if (item.IsVisible && item.Parent != null)
                {
                    // Per OPM 226882, Legacy/LegacyMigrated -> Funding Folder.  2015 Fee migrated -> Obsolete folder.
                    item.IsVisible = (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015) ?
                                              item.Parent.ID.Equals("Folder_Obsolete")
                                              : item.Parent.ID.Equals("Folder_Funding");
                }
            }
            else if (item.ID == "RActiveNewGfe")
            {
                item.IsVisible = m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID;

                if (item.IsVisible && m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    item.Name = "GFE";
                }
            }
            else if (item.ID == "RActiveNewGfeArchive")
            {
                item.IsVisible = Broker.IsGFEandCoCVersioningEnabled && m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                    && m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID;

                if (item.IsVisible && m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    item.Name = "GFE Archive";
                }

                if (item.IsVisible && !m_pageData.sHasGfe2015Archive)
                {
                    item.Attributes["onclick"] = "alert('No GFE Archive on file.  "
                    + "Please create one before attempting to access the archives.');"
                    + "return false;";
                }
            }
            else if (item.ID == "Page_SellerClosingDisclosure")
            {
                item.IsVisible = m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && m_pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            }
            else if (item.ID == "Page_SellerClosingDisclosureArchive")
            {
                item.IsVisible = Broker.IsGFEandCoCVersioningEnabled && this.IsInternalUser &&
                    m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && m_pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

                if (item.IsVisible && m_pageData.sClosingCostArchive.Count() == 0)
                {
                    item.Attributes["onclick"] = "alert('No Closing Cost Archive on file.  "
                    + "Please create one before attempting to access the archives.');"
                    + "return false;";
                }
            }
            else if (item.ID == "Page_SellerClosingCosts")
            {
                item.IsVisible = m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID;
            }
            else if (item.ID == "Page_SellerClosingCostsArchive")
            {
                item.IsVisible = Broker.IsGFEandCoCVersioningEnabled && this.IsInternalUser &&
                    m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID;

                if (item.IsVisible && m_pageData.sClosingCostArchive.Count() == 0)
                {
                    item.Attributes["onclick"] = "alert('No Closing Cost Archive on file.  "
                    + "Please create one before attempting to access the archives.');"
                    + "return false;";
                }
            }
            else if (item.ID == "Page_LoanEstimate" || item.ID == "Page_ClosingDisclosure" || item.ID == "Page_LoanTerms"
                || item.ID == "Page_BorrowerClosingCosts" || item.ID == "Page_AdjustmentsAndOtherCredits" || item.ID == "Page_ToleranceCure")
            {
                item.IsVisible = m_pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            }
            else if (item.ID == "Page_LenderCredits")
            {
                item.IsVisible = m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy ||
                    Broker.EnableLenderCreditsWhenInLegacyClosingCostMode;
            }
            else if (item.ID == "Page_PropHousingExpenses")
            {
                item.IsVisible = m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;
            }
            else if (item.ID == "Page_LoanEstimateArchive")
            {
                item.IsVisible = m_pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID
                    && Broker.IsGFEandCoCVersioningEnabled && m_pageData.sHasLoanEstimateArchive;
            }
            else if (item.ID == "Page_ClosingDisclosureArchive")
            {
                item.IsVisible = m_pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID
                    && Broker.IsGFEandCoCVersioningEnabled && m_pageData.sHasClosingDisclosureArchive;

                if (item.IsVisible && m_pageData.sHasClosingDisclosureArchive == false)
                {
                    item.Attributes["onclick"] = "alert('No Closing Disclosure Archive on file.  "
                    + "Please create one before attempting to access the archives.');"
                    + "return false;";
                }
            }
            else if (item.ID == "Page_TIL")
            {
                // OPM 212203 Remove TIL from Forms folder when in TRID 2015 mode.
                item.IsVisible = (item.Parent != null && !item.Parent.ID.Equals("Folder_Disclosure")) || m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID;
            }
            else if (item.ID == "Page_StatusGeneral")
            {
                // OPM 222987.  If this is a lead in loan editor, Status/General is not to be present.
                // It is replaced by the Assignment & Status page
                item.IsVisible = !Broker.IsEditLeadsInFullLoanEditor || !Tools.IsStatusLead(m_pageData.sStatusT);
            }
            else if (item.ID == "Page_StatusAgents")
            {
                // OPM 222987.  If this is a lead in loan editor, Status/Agents is not to be present.
                // It is replaced by the Assignment & Status page
                item.IsVisible = !Broker.IsEditLeadsInFullLoanEditor || !Tools.IsStatusLead(m_pageData.sStatusT);
            }
            else if (item.ID == "Page_LeadAssignment")
            {
                // OPM 222987. Assignment & Status page replaces the General/Agents page in leads.
                item.IsVisible = Broker.IsEditLeadsInFullLoanEditor && Tools.IsStatusLead(m_pageData.sStatusT);
            }
            else if (item.ID == "Folder_Favorites")
            {
                // OPM 222987. To give users further clue that these are not loan favorites,
                // when looking at them in the full editor of a lead, we want to rename it.
                if (Broker.IsEditLeadsInFullLoanEditor && Tools.IsStatusLead(m_pageData.sStatusT))
                {
                    item.Name = "Lead Favorites";
                }
            }
            else if (item.ID == "Page_MismoClosing33" || item.ID == "MismoClosing34")
            {
                item.IsVisible = m_pageData.sIsInTRID2015Mode && !m_pageData.sIsLegacyClosingCostVersion;
            }
            else if (item.ID == "Page_ImportFromSymitar")
            {
                item.IsVisible = this.Broker.GetEnabledSymitarLenderConfig(this.m_pageData.sLoanFileT)?.AllowImportFromSymitar ?? false;
            }
            else if (item.ID == "Page_ExportToSymitar")
            {
                item.IsVisible = this.Broker.GetEnabledSymitarLenderConfig(this.m_pageData.sLoanFileT)?.AllowExportToSymitar ?? false;
            }
            else if (item.ID == "Page_FHA203kMaximumMortgage")
            {
                item.IsVisible = Broker.EnableRenovationLoanSupport && m_pageData.sIsRenovationLoan;
            }
            else if (item.ID == "Page_FannieMaeHomeStyleMaximumMortgage")
            {
                item.IsVisible = Broker.EnableRenovationLoanSupport && m_pageData.sIsRenovationLoan;
            }
            else if (item.ID == "Folder_Renovation")
            {
                item.IsVisible = Broker.EnableRenovationLoanSupport && m_pageData.sIsRenovationLoan;
            }
            else if (item.ID == "Page_EmployeeResources")
            {
                item.IsVisible = !Broker.EmployeeResourcesUrl.Equals(string.Empty);
                ClientScript.RegisterHiddenField("EmployeeResourcesUrl", Broker.EmployeeResourcesUrl);
            }
            else if (item.ID == "Page_AppraisalTracking")
            {
                item.IsVisible = !this.m_isTemplate;
            }
            else if (item.ID == "Page_HomeownerCounseling")
            {
                item.IsVisible = this.m_pageData.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy;
            }
            else if (item.ID == "Page_DetailsofTransaction")
            {
                item.IsVisible = item.Parent.ID == "Folder_Obsolete" ? this.m_pageData.sIsTargeting2019Ulad : !this.m_pageData.sIsTargeting2019Ulad;
            }
            else if (item.ID == "Page_QualifyingBorrower")
            {
                item.IsVisible = this.m_pageData.sIsTargeting2019Ulad;
            }
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterService("main", "/newlos/LeftTreeFrameService.aspx");
            this.RegisterService("edoc", "/newlos/ElectronicDocs/EDocsUtilitiesService.aspx");
            this.RegisterVbsScript("common.vbs");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("ModelessDlg.js");
            this.RegisterJsGlobalVariables("sLId", RequestHelper.LoanID);
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

            this.Init += new System.EventHandler(this.PageInit);


        }
        #endregion

        public bool HasPermit()
        {
            string reason;
            return BrokerUser.CanUserViewLoanEditorTestPage(out reason);
        }
	}
}
