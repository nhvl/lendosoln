<%@ Import namespace="LendersOffice.Common" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="SubjPropInvestment" Src="SubjPropInvestment.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PropertyInfo" Src="PropertyInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PropertyDetails" Src="PropertyDetails.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Page language="c#" Codebehind="PropertyInfo.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.PropertyInfo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
		<title>PropertyInfo</title>
      <style type="text/css">
          .appraisal-section:not(.fha) .fha-version{
              display: none;
          }

      </style>
  </head>
	<body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" scroll="yes">
    <script type="text/javascript">
      function _init()
      {
        if (typeof (f_initControl) != "undefined")
        {
          f_initControl();
        }
      }
    </script>
		<form id="PropertyInfo" method="post" runat="server">
		  <table border=0 cellpadding=0 cellspacing=0 width="100%">
		    <tr>
		    <td nowrap class="TabRow">
                <uc1:Tabs runat="server" ID="Tabs" /> 
            </td>
            </tr>
		    <tr>
		        <td style="PADDING-LEFT:5px">
		            <uc1:PropertyInfo id=PropertyInfoUserControl runat="server"></uc1:PropertyInfo>
		            <uc1:PropertyDetails id=PropertyDetailsUserControl runat="server"></uc1:PropertyDetails>
		            <uc1:SubjPropInvestment id=SubjPropInvestment runat="server"></uc1:SubjPropInvestment>
		        </td>
		    </tr>
		  </table>

		</form>
	</body>
</html>
