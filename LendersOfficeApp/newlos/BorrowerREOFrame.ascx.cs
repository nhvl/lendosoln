using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.AntiXss;
namespace LendersOfficeApp.newlos
{
	public partial  class BorrowerREOFrame : BaseLoanUserControl, IAutoLoadUserControl
	{
        protected string ListUrl
        {
            get { return "BorrowerREOList.aspx?loanid=" + LoanID + "&appid=" + ApplicationID; }
        }
        protected string EditUrl
        {
            get { return "REORecord.aspx?loanid=" + LoanID + "&appid=" + ApplicationID; }
        }
        public void SaveData() 
        {
        }

        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(BorrowerREOFrame));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            aReTotVal.Text = dataApp.aReTotVal_rep;
            aReTotMAmt.Text = dataApp.aReTotMAmt_rep;
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            // Only add this javascript when this usercontrol is visible, else borrower info, monthly income tab will not work.
            if (this.Visible) 
            {
                Page.ClientScript.RegisterHiddenField("ListLocation", "BorrowerInfo.aspx?loanid=" + LoanID + "&pg=6");

                ((BasePage)this.Page).RegisterJsScript("singleedit2.js");
                ((BasePage)this.Page).RegisterJsScript("LQBPrintFix.js");
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
