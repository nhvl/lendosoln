<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="BorrowerAssetFrame.ascx.cs" Inherits="LendersOfficeApp.newlos.BorrowerAssetFrame" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %><%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %><%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript">
<!--
function _init() {
  <% if (IsReadOnly) { %>
    document.getElementById("btnPrev").disabled = true;
    document.getElementById("btnNext").disabled = true;
    document.getElementById("btnInsert").disabled = true;
    document.getElementById("btnAdd").disabled = true;
    document.getElementById("btnSave").disabled = true;
    document.getElementById("btnUp").disabled = true;    
    document.getElementById("btnDown").disabled = true;
    document.getElementById("btnDelete").disabled = true;    
    document.getElementById("btnEditVoe").disabled = true;
  <% } %>

    var $cashDepositCell = $(".cashDepositCell");

    $cashDepositCell.toggle(ML.IsTrid2017);
    $cashDepositCell.find("select").prop("disabled", !ML.HasNonZeroNewOtherFinancing);
}

    function refreshCalculation() {
     callFrameMethod(window, "SingleIFrame", "refreshCalculation");
    }

function saveMe() {
    var ret = false;
    var single_saveMe = retrieveFrameProperty(window, "SingleIFrame", "single_saveMe");
    if (single_saveMe) {
        var result = single_saveMe();        
        if(ret = !result.error)
        {            
            callFrameMethod(parent, "info", "f_refreshInfo");
        }
        else
        {
            clearDirty();
        }
    }
    return ret;
}

function runListIFrameMethod(methodName, arg) {
    callFrameMethod(window, "ListIFrame", methodName, [arg]);
}

function getAsstLiaCompletedNotJointly() { return <%= AspxTools.JsGetElementById(aAsstLiaCompletedNotJointly) %>.checked; }

function getCashDeposit1_Desc() { return <%= AspxTools.JsGetElementById(CashDeposit1_Desc) %>.value; }
function setCashDeposit1_Desc(value) { <%= AspxTools.JsGetElementById(CashDeposit1_Desc) %>.value = value; }  

function getCashDeposit1_Val() { return <%= AspxTools.JsGetElementById(CashDeposit1_Val) %>.value; }
function setCashDeposit1_Val(value) { <%= AspxTools.JsGetElementById(CashDeposit1_Val) %>.value = value; }

function getCashDeposit2_Desc() { return <%= AspxTools.JsGetElementById(CashDeposit2_Desc) %>.value; }
function setCashDeposit2_Desc(value) { <%= AspxTools.JsGetElementById(CashDeposit2_Desc) %>.value = value; }  

function getCashDeposit2_Val() { return <%= AspxTools.JsGetElementById(CashDeposit2_Val) %>.value; }
function setCashDeposit2_Val(value) { <%= AspxTools.JsGetElementById(CashDeposit2_Val) %>.value = value; }

function getLifeInsurance_FaceVal() { return <%= AspxTools.JsGetElementById(LifeInsurance_FaceVal) %>.value; }
function setLifeInsurance_FaceVal(value) { <%= AspxTools.JsGetElementById(LifeInsurance_FaceVal) %>.value = value; }

function getLifeInsurance_Val() { return <%= AspxTools.JsGetElementById(LifeInsurance_Val) %>.value; }
function setLifeInsurance_Val(value) { <%= AspxTools.JsGetElementById(LifeInsurance_Val) %>.value = value; }

function getRetirement_Val() { return <%= AspxTools.JsGetElementById(Retirement_Val) %>.value; }
function setRetirement_Val(value) { <%= AspxTools.JsGetElementById(Retirement_Val) %>.value = value; }

function getBusiness_Val() { return <%= AspxTools.JsGetElementById(Business_Val) %>.value; }
function setBusiness_Val(value) { <%= AspxTools.JsGetElementById(Business_Val) %>.value = value; }

function getLienToIncludeCashDepositInTridDisclosures() { return <%= AspxTools.JsGetElementById(sLienToIncludeCashDepositInTridDisclosures) %>.value; }
function setLienToIncludeCashDepositInTridDisclosures(value) { if (value == null) return; <%= AspxTools.JsGetElementById(sLienToIncludeCashDepositInTridDisclosures) %>.value = value; }

function updateDirtyBit_callback() { parent_disableSaveBtn(false); }
       
function updateTotal(aAsstLiqTot, aReTotVal, aAsstNonReSolidTot, aAsstValTot) {
  <%= AspxTools.JsGetElementById(aAsstLiqTot) %>.value = aAsstLiqTot;
  <%= AspxTools.JsGetElementById(aReTotVal) %>.value = aReTotVal;
  <%= AspxTools.JsGetElementById(aAsstNonReSolidTot) %>.value = aAsstNonReSolidTot;
  <%= AspxTools.JsGetElementById(aAsstValTot) %>.value = aAsstValTot;
  
}
  function parent_setVODStatus(enable){
    if( isReadOnly())  {
        return;
    }
    
    document.getElementById('btnEditVOD').disabled = !enable;
  }
  
  function parent_editVOD() {
  if (isDirty()) {
    var ret = confirm('Save is required before proceeding'); 
    if (ret ) saveMe();
    else { return; }
  }

  var recordid = callFrameMethod(window, "ListIFrame", "list_getRecordID", [parent_iCurrentIndex]);
  linkMe('Verifications/VODRecord.aspx', 'recordid=' + recordid );
}

//-->
</script>

<table width="99%">
  <tr>
    <td>
    <table style="width:100%;">
        <tr><td>
              <table id="Table1" cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td class="FieldLabel w-98" nowrap title="Cash deposit toward purchase held by:">Cash Deposit</td>
                  <td nowrap>
                    <asp:TextBox ID="CashDeposit1_Desc" runat="server" TabIndex="1" Width="100px" /></td>
                  <td nowrap><ml:MoneyTextBox ID="CashDeposit1_Val" runat="server" preset="money" Width="80px" TabIndex="1" onchange="refreshCalculation();" /></td>
                  <td class="FieldLabel" nowrap style="padding-left: 10px" title="Vested interest in retirement fund ($)">Retir. Funds</td>
                  <td nowrap><ml:MoneyTextBox ID="Retirement_Val" runat="server" preset="money" Width="80px" TabIndex="5" onchange="refreshCalculation();" /></td>
                  <td class="FieldLabel" nowrap style="padding-left: 10px">Subtotal Liquid </td>
                  <td nowrap><ml:MoneyTextBox ID="aAsstLiqTot" runat="server" Width="80px" preset="money" ReadOnly="True" /></td>
                  <td nowrap></td>
                </tr>
                <tr>
                  <td nowrap></td>
                  <td nowrap>
                    <asp:TextBox ID="CashDeposit2_Desc" runat="server" TabIndex="1" Width="100px" /></td>
                  <td nowrap><ml:MoneyTextBox ID="CashDeposit2_Val" runat="server" preset="money" Width="80px" TabIndex="1" onchange="refreshCalculation();" /></td>
                  <td class="FieldLabel" nowrap style="padding-left: 10px" title="Net worth of business(es) owned ($)">Business</td>
                  <td nowrap><ml:MoneyTextBox ID="Business_Val" runat="server" preset="money" Width="80px" TabIndex="5" onchange="refreshCalculation();" /></td>
                  <td class="FieldLabel" nowrap style="padding-left: 10px">Subtotal Other</td>
                  <td nowrap><ml:MoneyTextBox ID="aAsstNonReSolidTot" runat="server" Width="80px" preset="money" ReadOnly="True" /></td>
                  <td nowrap></td>
                </tr>
                <tr>
                  <td nowrap colspan="2">
                      <span class="FieldLabel cashDepositCell">Include Cash Deposit In Disclosures For:</span>
                  </td>
                  <td nowrap>
                      <span class="cashDepositCell">
                        <asp:dropdownlist ID="sLienToIncludeCashDepositInTridDisclosures" runat="server"></asp:dropdownlist>
                      </span>
                  </td>
                  <td nowrap class="FieldLabel" style="padding-left: 10px"><a onclick="redirectToUladPage('REO');" title="Go to Real Estate Schedule">REO</a></td>
                  <td nowrap><ml:MoneyTextBox ID="aReTotVal" runat="server" Width="80px" preset="money" ReadOnly="True" /></td>
                  <td class="FieldLabel" nowrap style="padding-left: 10px">Borrower Total</td>
                  <td nowrap><ml:MoneyTextBox ID="aAsstValTot" runat="server" Width="80px" preset="money" ReadOnly="True" /></td>
                </tr>
                  <tr>
                    <td class="FieldLabel" nowrap colspan="2">
                        <span title="Life insurance face amount and net cash value" class="FloatLeft">Life Ins. Face Amt</span>
                        <ml:MoneyTextBox ID="LifeInsurance_FaceVal" runat="server" preset="money" Width="80px" TabIndex="1" class="FloatRight" />
                    </td>
                    <td nowrap>
                        <ml:MoneyTextBox ID="LifeInsurance_Val" runat="server" preset="money" Width="80px" TabIndex="1" onchange="refreshCalculation();" />
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
              </table>
          </td>
          <td valign="bottom" style="text-align:right">
                <label class="FieldLabel">Assets and Liabilities Completed: </label>
                <asp:RadioButton GroupName="aAsstLiaCompleted" ID="aAsstLiaCompletedJointly" runat="server" /><label class="FieldLabel" for="aAsstLiaCompletedJointly">Jointly</label>
                <asp:RadioButton GroupName="aAsstLiaCompleted" ID="aAsstLiaCompletedNotJointly" runat="server" /><label class="FieldLabel" for="aAsstLiaCompletedNotJointly">Not Jointly</label>
          </td>
          </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <iframe id="ListIFrame" src=<%= AspxTools.SafeUrl(ListUrl) %> width="100%" height="100" tabindex="-1"></iframe>
    </td>
  </tr>
  <tr>
    <td>
      <input id="btnPrev" onclick="runListIFrameMethod('list_go', -1);" type="button" value="<< Prev" tabindex="10" accesskey="P" nohighlight />
      <input id="btnNext" onclick="runListIFrameMethod('list_go', 1);" type="button" value="Next >>" tabindex="10" accesskey="N" nohighlight />
      <input id="btnInsert" onclick="parent_onInsert();" type="button" value="Insert" tabindex="10" nohighlight />
      <input id="btnAdd" type="button" value="Add" onclick="parent_onAdd();" tabindex="10" nohighlight />
      <input id="btnSave" onclick="saveMe();" type="button" value="Save" tabindex="10" nohighlight />
      <input id="btnUp" onclick="runListIFrameMethod('list_move', 1);" type="button" value="Move Up" tabindex="10" nohighlight />
      <input id="btnDown" onclick="runListIFrameMethod('list_move', -1);" type="button" value="Move Down" tabindex="10" nohighlight />
      <input id="btnDelete" onclick="runListIFrameMethod('list_deleteRecord');" type="button" value="Delete" tabindex="10" nohighlight />
      <input id="btnEditVOD" onclick="parent_editVOD();" type="button" value="Edit VOD" tabindex="10" nohighlight />
    </td>
  </tr>
  <tr>
    <td valign="top">
      <iframe id="SingleIFrame" src=<%= AspxTools.SafeUrl(EditUrl) %> width="100%" height="300" tabindex="100"></iframe>
    </td>
  </tr>
</table>
