﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisclosureAudit.aspx.cs" Inherits="LendersOfficeApp.newlos.DisclosureAudit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Disclosure Audit</title>
    <style>
        body { background-color: gainsboro; }
        #DisclosureContainer 
        {
            height: 400px;
            border: solid 1px gray;
            overflow-y: auto;
        }
        #ButtonContainer 
        {
            padding-top: 10px;
            text-align: center;
        }
        th { text-align: left; }
        .Header 
        {
            padding: 2px 5px;
            background-color: #999999;
            color: white;
        }
        .FirstLine { float: left; }
        .SecondLine { float: left; clear: left; }
        .FormTableHeader
        {
            padding: 5px;
        }
        #HistoryHeader
        {
            margin-top: 5px;
        }
        #BorrowersContainer
        {
            width: 100%;
            max-height: 150px;
            overflow-y: auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="FormTableHeader">Borrowers</div>
        <div id="BorrowersContainer">
            <table id="Borrowers" class="FormTable" cellpadding="2" cellspacing="1" style="width: 100%;">
                <thead>
                    <tr class="Header">
                        <th style="width: 50%;"><span class="FirstLine">Name</span><span class="SecondLine">Email</span></th>
                        <th style="width: 25%;">E-Consent Received</th>
                        <th style="width: 25%;">Declined E-Consent</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="BorrowerEntries" OnItemDataBound="BorrowerEntries_OnItemDataBound">
                        <ItemTemplate>
                            <tr runat="server" id="BorrRow">
                                <td>
                                    <span class="FirstLine"><ml:EncodedLiteral runat="server" ID="Name"></ml:EncodedLiteral></span>
                                    <span class="SecondLine"><a runat="server" id="Email" href="#"></a></span>
                                </td>
                                <td>
                                    <span><ml:EncodedLiteral runat="server" ID="EConsentReceivedD"></ml:EncodedLiteral></span>
                                </td>
                                <td>
                                    <span><ml:EncodedLiteral runat="server" ID="EConsentDeclinedD"></ml:EncodedLiteral></span>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
        
        <div id="HistoryHeader" class="FormTableHeader">Disclosure History</div>
        <div id="DisclosureContainer">
        <table id="DisclosureHistory" class="FormTable" cellpadding="2" cellspacing="1" style="width: 100%;">
            <thead>
                <tr class="Header">
                    <th style="width: 33.33%;">Source</th>
                    <th style="width: 66.66%;">Event</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater runat="server" ID="DisclosureHistoryEntries" OnItemDataBound="DisclosureHistoryEntries_OnItemDataBound">
                    <ItemTemplate>
                        <tr runat="server" id="AuditRow">
                            <td>
                                <span class="FirstLine"><ml:EncodedLiteral runat="server" ID="Username"></ml:EncodedLiteral></span>
                                <span class="SecondLine"><ml:EncodedLiteral runat="server" ID="TimestampDescription"></ml:EncodedLiteral></span>
                            </td>
                            <td>
                                <ml:EncodedLiteral runat="server" ID="Description"></ml:EncodedLiteral>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        </div>
        <div id="ButtonContainer">
            <input type="button" value="Close" onclick="onClosePopup();" />
        </div>
    </div>
    </form>
</body>
</html>
