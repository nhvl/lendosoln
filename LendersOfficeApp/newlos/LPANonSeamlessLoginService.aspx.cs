﻿#region Auto-generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using System.Data;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.LoanProductAdvisor;
    using LendersOffice.ObjLib.Conversions.Aus;

    /// <summary>
    /// Service page for LPA non-seamless login page.
    /// </summary>
    public partial class LPANonSeamlessLoginService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Initialize the page.
        /// </summary>
        protected override void Initialize()
        {
            this.AddBackgroundItem(string.Empty, new LpaNonSeamlessLoginServiceItem());
        }

        /// <summary>
        /// Service item class for handling loan framework service requests to this page.
        /// </summary>
        private class LpaNonSeamlessLoginServiceItem : AbstractBackgroundServiceItem
        {
            /// <summary>
            /// Binds data from the UI to the loan file.
            /// </summary>
            /// <param name="dataLoan">Loan file to save to. Prepped for saving.</param>
            /// <param name="dataApp">The app data object to save to.</param>
            protected override void BindData(CPageData dataLoan, CAppData dataApp)
            {
                AusCreditOrderingInfo tempCraInfo = new AusCreditOrderingInfo
                {
                    CraProviderId = this.GetString("FreddieCrcDDL", string.Empty),
                    CreditReportOption = this.GetString("CreditChoice", string.Empty) == "Merged" ? (GetBool("cbRequestReorderCredit") ? CreditReportType.Reissue : CreditReportType.OrderNew) : CreditReportType.UsePrevious,
                    Applications = dataLoan.Apps.Select(app => this.GetAppCreditData(app))
                };
                LoanSubmitRequest.SaveTemporaryCRAInformation(dataLoan.sLId, tempCraInfo);

                dataLoan.sFreddieLoanId = this.GetString("sFreddieLoanId");
                dataLoan.sFreddieTransactionId = this.GetString("sFreddieTransactionId");
                dataLoan.sLpAusKey = this.GetString("sLpAusKey");
                dataLoan.sFreddieSellerNum = this.GetString("sFreddieSellerNum", dataLoan.sFreddieSellerNum);
                if (this.GetString("sFreddieLpPassword", string.Empty) != string.Empty)
                {
                    dataLoan.sFreddieLpPassword = this.GetString("sFreddieLpPassword");
                }

                dataLoan.sFreddieTpoNum = this.GetString("sFreddieTpoNum");
                dataLoan.sFreddieNotpNum = this.GetString("sFreddieNotpNum");
                dataLoan.sFredProcPointT = (E_sFredProcPointT)this.GetInt("sFredProcPointT");
                dataLoan.sFreddieLenderBranchId = this.GetString("sFreddieLenderBranchId");
            }

            /// <summary>
            /// Constructs the page data class for loading/saving data from.
            /// </summary>
            /// <param name="loanId">The loan ID.</param>
            /// <returns>A newly created but uninitialized data object with the proper dependencies for this page.</returns>
            protected override CPageData ConstructPageDataClass(Guid loanId)
            {
                return CPageData.CreateUsingSmartDependency(loanId, typeof(LpaNonSeamlessLoginServiceItem));
            }

            /// <summary>
            /// Loads data to populate back to the page after saving.
            /// There is no data to send back; This is a saving-only page.
            /// </summary>
            /// <param name="dataLoan">The loan data object to load from.</param>
            /// <param name="dataApp">The app data object to load from.</param>
            protected override void LoadData(CPageData dataLoan, CAppData dataApp)
            {
            }

            /// <summary>
            /// Generates a credit requesting data object for an application, based on service arguments.
            /// </summary>
            /// <param name="appData">The application to build a credit data object for.</param>
            /// <returns>The credit reference number to add to credit ordering info.</returns>
            private AusCreditOrderingInfo.AusCreditAppData GetAppCreditData(CAppData appData)
            {
                string key = "B" + appData.aAppId.ToString("N");
                string reference = this.GetString(key, string.Empty);

                string key2 = "ReorderB" + appData.aAppId.ToString("N");
                bool reorder = this.GetBool(key2, false);

                return new AusCreditOrderingInfo.AusCreditAppData(appData.aBNm, appData.aCNm, reference, appData.aAppId, reorder);
            }
        }
    }
}