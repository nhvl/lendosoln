using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOfficeApp.los.admin;
using DataAccess;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos
{
	public partial class HeaderFrame : LendersOffice.Common.BaseServicePage
	{
        protected string m_serverID = "";

        protected string m_userName = "";
        protected string m_brokerName = "";
		protected string m_userMail = "";
    
		protected void PageLoad(object sender, System.EventArgs e)
		{
            ServerLocation server = ConstAppDavid.CurrentServerLocation;
            switch (server) 
            {
                case ServerLocation.LocalHost:
                    m_serverID = "local_";
                    break;
                case ServerLocation.Demo:
                    m_serverID = "demo_";
                    break;
                case ServerLocation.Development:
                    m_serverID = "dev_";
                    break;
                case ServerLocation.DevCopy:
                    m_serverID = "devcopy_";
                    break;
            }

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal; //Page.User as BrokerUserPrincipal;
            if (null != principal) 
            {
                string pml_description = "";
                if (principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
                    pml_description = " (PML)";

                m_welcomeLabel.Text = string.Format("Welcome, {0}!", principal.DisplayName);
                m_userName = principal.DisplayName;
                m_brokerName = principal.BrokerName + pml_description;
                // opm 473272 jk 8/17/18
                m_userMail = EmployeeDB.RetrieveEmployeeEmailById(principal.BrokerId, principal.EmployeeId);

                m_NewSupportLink.Title = m_NewSupportLink.InnerText = ConstStage.SupportCenterLinkText;
            }
            #region opm 30792 Hide main link leading to pipeline and loan duplication link in editor
            m_HomeLink.Visible = RequestHelper.GetSafeQueryString("lpq") != "1";
            #endregion
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
			//OPM 4134 Ethan - required by gService call in f_chat (HeaderFrame.aspx)
            RegisterService("lookupRecord", "/common/RecordsLookupService.aspx");

            this.RegisterVbsScript("common.vbs");
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

            this.Init += new System.EventHandler(this.PageInit);


        }
		#endregion

	}
}
