﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.DU;
using DataAccess;
using LendersOffice.Common;
using System.Data.SqlClient;
using LendersOffice.Security;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos
{
    public partial class FannieMaeCasefileStatusService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CasefileStatus":
                    GetCasefileStatus();
                    break;
            }
        }

        private void GetCasefileStatus()
        {
            string status = "ERROR";
            string errorMessage = "";

            Guid sLId = GetGuid("LoanID");
            bool isDo = GetBool("IsDo");
            string sDuCaseId = GetString("sDuCaseId", "");



            try
            {
                CPageData data = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMaeCasefileStatusService));
                data.InitSave(ConstAppDavid.SkipVersionCheck);
                data.sDuCaseId = sDuCaseId;
                data.Save();
            }
            catch
            {
              
                //its okay if we cant save the ducase id.
            }

            bool rememberLastLogin = GetBool("RememberLastLogin");
            string rememberLoginNm = rememberLastLogin ? GetString("FannieMaeMORNETUserID") : "";
            string parameterName = isDo ? "@DOLastLoginNm" : "@DULastLoginNm";
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", BrokerUserPrincipal.CurrentPrincipal.UserId),
                                            new SqlParameter(parameterName, rememberLoginNm) 
                                        };
            StoredProcedureHelper.ExecuteNonQuery(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "UpdateLastDoDuLpLoginNm", 3, parameters);


            FnmaDwebCasefileStatusRequest request = new FnmaDwebCasefileStatusRequest(isDo, sLId);

            request.FannieMaeMORNETUserID = GetString("FannieMaeMORNETUserID");
            request.FannieMaeMORNETPassword = GetString("FannieMaeMORNETPassword");
            request.MornetPlusCasefileIdentifier = sDuCaseId;



            try
            {
                FnmaDwebCasefileStatusResponse response = (FnmaDwebCasefileStatusResponse)DUServer.Submit(request, suppressAusResultLogging: true);
                if (response.IsReady)
                {
                    
                    if (response.HasError)
                    {
                        status = "ERROR";
                        errorMessage = response.ErrorMessage;
                    }
                    else if (response.HasBusinessError)
                    {
                        status = "ERROR";
                        errorMessage = response.BusinessErrorMessage + Environment.NewLine + response.FnmaStatusLog;
                    }
                    else
                    {
                        status = "DONE";
                        SetResult("LastUnderwritingDate", response.LastUnderwritingDate);
                        SetResult("UnderwritingRecommendation", response.UnderwritingRecommendationDescription);
                        SetResult("UnderwritingStatus", response.UnderwritingStatusDescription);
                        SetResult("UnderwritingSubmissionType", response.UnderwritingSubmissionType);
                        SetResult("OriginatorInstitutionName", response.OriginatorInstitutionName);
                        SetResult("OriginatorInstitutionIdentifier", response.OriginatorInstitutionIdentifier);
                        SetResult("LenderInstitutionName", response.LenderInstitutionName);
                        SetResult("LenderInstitutionIdentifier", response.LenderInstitutionIdentifier);

                    }
                };
            }
            catch (CBaseException exc)
            {
                status = "ERROR";
                errorMessage = exc.UserMessage;
                Tools.LogErrorWithCriticalTracking("Unable to export file to DU", exc);
            }
            catch (Exception exc)
            {
                status = "ERROR";
                errorMessage = ErrorMessages.Generic;
                Tools.LogErrorWithCriticalTracking("Unable to export file to DU", exc);
            }

            SetResult("Status", status);
            SetResult("ErrorMessage", errorMessage);
        }
    }
}
