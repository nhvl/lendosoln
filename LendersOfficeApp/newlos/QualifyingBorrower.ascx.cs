﻿#region Generated code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using System.Web.UI;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.QualifyingBorrower;

    /// <summary>
    /// The qualifying borrower user control.  This control should be re-used wherever the Qualifying Borrower UI should appear.
    /// </summary>
    public partial class QualifyingBorrower : UserControl
    {
        /// <summary>
        /// Updates the loan from the data given to the service item.
        /// </summary>
        /// <param name="serviceItem">A service item from a service page.</param>
        /// <param name="dataLoan">The loan file to update.</param>
        public static void Bind_QualifyingBorrower(AbstractBackgroundServiceItem serviceItem, CPageData dataLoan)
        {
            dataLoan.sAltCostLckd = serviceItem.GetBool("sAltCostLckd");
            dataLoan.sTotEstPp1003Lckd = serviceItem.GetBool("sTotEstPp1003Lckd");
            dataLoan.sTotCcPbsLocked = serviceItem.GetBool("sTotCcPbsLocked");
            dataLoan.sLDiscnt1003Lckd = serviceItem.GetBool("sLDiscnt1003Lckd");
            dataLoan.sTotEstCc1003Lckd = serviceItem.GetBool("sTotEstCc1003Lckd");
            dataLoan.sAltCost_rep = serviceItem.GetString("sAltCost");
            dataLoan.sTotEstCcNoDiscnt1003_rep = serviceItem.GetString("sTotEstCcNoDiscnt1003");
            dataLoan.sTotEstPp1003_rep = serviceItem.GetString("sTotEstPp1003");
            dataLoan.sTotCcPbs_rep = serviceItem.GetString("sTotCcPbs");
            dataLoan.sLDiscnt1003_rep = serviceItem.GetString("sLDiscnt1003");
        }

        /// <summary>
        /// Takes the data from the given loan and updates the results with the necessary values.
        /// </summary>
        /// <param name="serviceItem">A service item belonging to a service page.</param>
        /// <param name="dataLoan">The loan file to retrieve the data from.</param>
        public static void Load_QualifyingBorrower(AbstractBackgroundServiceItem serviceItem, CPageData dataLoan)
        {
            serviceItem.SetResult("sPurchasePrice1003", dataLoan.sPurchasePrice1003_rep);
            serviceItem.SetResult("sAltCost", dataLoan.sAltCost_rep);
            serviceItem.SetResult("sAltCostLckd", dataLoan.sAltCostLckd);
            serviceItem.SetResult("sLandIfAcquiredSeparately1003", dataLoan.sLandIfAcquiredSeparately1003_rep);
            serviceItem.SetResult("sRefTransMortgageBalancePayoffAmt", dataLoan.sRefTransMortgageBalancePayoffAmt_rep);
            serviceItem.SetResult("sRefNotTransMortgageBalancePayoffAmt", dataLoan.sRefNotTransMortgageBalancePayoffAmt_rep);
            serviceItem.SetResult("sTotEstBorrCostUlad", dataLoan.sTotEstBorrCostUlad_rep);
            serviceItem.SetResult("sLDiscnt1003", dataLoan.sLDiscnt1003_rep);
            serviceItem.SetResult("sLDiscnt1003Lckd", dataLoan.sLDiscnt1003Lckd);
            serviceItem.SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            serviceItem.SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            serviceItem.SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            serviceItem.SetResult("sONewFinBal", dataLoan.sONewFinBal_rep);
            serviceItem.SetResult("sTotMortLAmtUlad", dataLoan.sTotMortLAmtUlad_rep);
            serviceItem.SetResult("sSellerCreditsUlad", dataLoan.sSellerCreditsUlad_rep);
            serviceItem.SetResult("sTotOtherCreditsUlad", dataLoan.sTotOtherCreditsUlad_rep);
            serviceItem.SetResult("sTotCreditsUlad", dataLoan.sTotCreditsUlad_rep);
            serviceItem.SetResult("sTotTransCUlad", dataLoan.sTotTransCUlad_rep);
            serviceItem.SetResult("sTotMortLTotCreditUlad", dataLoan.sTotMortLTotCreditUlad_rep);
            serviceItem.SetResult("sTransNetCashUlad", dataLoan.sTransNetCashUlad_rep);
            serviceItem.SetResult("sTotEstCcNoDiscnt1003", dataLoan.sTotEstCcNoDiscnt1003_rep);
            serviceItem.SetResult("sTotEstCc1003Lckd", dataLoan.sTotEstCc1003Lckd);
            serviceItem.SetResult("sTotEstPp1003", dataLoan.sTotEstPp1003_rep);
            serviceItem.SetResult("sTotEstPp1003Lckd", dataLoan.sTotEstPp1003Lckd);
            serviceItem.SetResult("sTotCcPbs", dataLoan.sTotCcPbs_rep);
            serviceItem.SetResult("sTotCcPbsLocked", dataLoan.sTotCcPbsLocked);
            serviceItem.SetResult("sTRIDSellerCredits", dataLoan.sTRIDSellerCredits_rep);

            var isConstruction = dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            bool areConstructionLoanDataPointsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints);

            serviceItem.SetResult("sAltCostLckdDisabled", !dataLoan.sIsRenovationLoan || (areConstructionLoanDataPointsMigrated && isConstruction));
        }

        /// <summary>
        /// Updates a loan with the data provided to the service page.
        /// </summary>
        /// <param name="servicePage">The service page to retrieve the data from.</param>
        /// <param name="dataLoan">The loan to be updated.</param>
        public static void Bind_QualifyingBorrower(BaseSimpleServiceXmlPage servicePage, CPageData dataLoan)
        {
            dataLoan.sAltCostLckd = servicePage.GetBool("sAltCostLckd");
            dataLoan.sTotEstPp1003Lckd = servicePage.GetBool("sTotEstPp1003Lckd");
            dataLoan.sTotCcPbsLocked = servicePage.GetBool("sTotCcPbsLocked");
            dataLoan.sLDiscnt1003Lckd = servicePage.GetBool("sLDiscnt1003Lckd");
            dataLoan.sTotEstCc1003Lckd = servicePage.GetBool("sTotEstCc1003Lckd");
            dataLoan.sAltCost_rep = servicePage.GetString("sAltCost");
            dataLoan.sTotEstCcNoDiscnt1003_rep = servicePage.GetString("sTotEstCcNoDiscnt1003");
            dataLoan.sTotEstPp1003_rep = servicePage.GetString("sTotEstPp1003");
            dataLoan.sTotCcPbs_rep = servicePage.GetString("sTotCcPbs");
            dataLoan.sLDiscnt1003_rep = servicePage.GetString("sLDiscnt1003");
        }

        /// <summary>
        /// Updates the service page's result with the loan data.
        /// </summary>
        /// <param name="servicePage">The service page whose result will be updated.</param>
        /// <param name="dataLoan">The loan to retrieve the data from.</param>
        public static void Load_QualifyingBorrower(BaseSimpleServiceXmlPage servicePage, CPageData dataLoan)
        {
            servicePage.SetResult("sPurchasePrice1003", dataLoan.sPurchasePrice1003_rep);
            servicePage.SetResult("sAltCost", dataLoan.sAltCost_rep);
            servicePage.SetResult("sAltCostLckd", dataLoan.sAltCostLckd);
            servicePage.SetResult("sLandIfAcquiredSeparately1003", dataLoan.sLandIfAcquiredSeparately1003_rep);
            servicePage.SetResult("sRefTransMortgageBalancePayoffAmt", dataLoan.sRefTransMortgageBalancePayoffAmt_rep);
            servicePage.SetResult("sRefNotTransMortgageBalancePayoffAmt", dataLoan.sRefNotTransMortgageBalancePayoffAmt_rep);
            servicePage.SetResult("sTotEstBorrCostUlad", dataLoan.sTotEstBorrCostUlad_rep);
            servicePage.SetResult("sLDiscnt1003", dataLoan.sLDiscnt1003_rep);
            servicePage.SetResult("sLDiscnt1003Lckd", dataLoan.sLDiscnt1003Lckd);
            servicePage.SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            servicePage.SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            servicePage.SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            servicePage.SetResult("sONewFinBal", dataLoan.sONewFinBal_rep);
            servicePage.SetResult("sTotMortLAmtUlad", dataLoan.sTotMortLAmtUlad_rep);
            servicePage.SetResult("sSellerCreditsUlad", dataLoan.sSellerCreditsUlad_rep);
            servicePage.SetResult("sTotOtherCreditsUlad", dataLoan.sTotOtherCreditsUlad_rep);
            servicePage.SetResult("sTotCreditsUlad", dataLoan.sTotCreditsUlad_rep);
            servicePage.SetResult("sTotTransCUlad", dataLoan.sTotTransCUlad_rep);
            servicePage.SetResult("sTotMortLTotCreditUlad", dataLoan.sTotMortLTotCreditUlad_rep);
            servicePage.SetResult("sTransNetCashUlad", dataLoan.sTransNetCashUlad_rep);
            servicePage.SetResult("sTotEstCcNoDiscnt1003", dataLoan.sTotEstCcNoDiscnt1003_rep);
            servicePage.SetResult("sTotEstCc1003Lckd", dataLoan.sTotEstCc1003Lckd);
            servicePage.SetResult("sTotEstPp1003", dataLoan.sTotEstPp1003_rep);
            servicePage.SetResult("sTotEstPp1003Lckd", dataLoan.sTotEstPp1003Lckd);
            servicePage.SetResult("sTotCcPbs", dataLoan.sTotCcPbs_rep);
            servicePage.SetResult("sTotCcPbsLocked", dataLoan.sTotCcPbsLocked);
            servicePage.SetResult("sTRIDSellerCredits", dataLoan.sTRIDSellerCredits_rep);

            var isConstruction = dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            bool areConstructionLoanDataPointsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints);

            servicePage.SetResult("sAltCostLckdDisabled", !dataLoan.sIsRenovationLoan || (areConstructionLoanDataPointsMigrated && isConstruction));
        }

        /// <summary>
        /// Generates a QualifyingBorrowerMOdel from the given loan data.
        /// </summary>
        /// <param name="dataLoan">The loan to retrieve the data from.</param>
        /// <returns>A model containing all the data necessary for the qualifying borrower ui.</returns>
        public static QualifyingBorrowerModel RetrieveQualifyingBorrowerModel(CPageData dataLoan)
        {
            var isConstruction = dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            bool areConstructionLoanDataPointsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints);

            return new QualifyingBorrowerModel()
            {
                sPurchasePrice1003 = dataLoan.sPurchasePrice1003_rep,
                sAltCost = dataLoan.sAltCost_rep,
                sLandIfAcquiredSeparately1003 = dataLoan.sLandIfAcquiredSeparately1003_rep,
                sRefTransMortgageBalancePayoffAmt = dataLoan.sRefTransMortgageBalancePayoffAmt_rep,
                sRefNotTransMortgageBalancePayoffAmt = dataLoan.sRefNotTransMortgageBalancePayoffAmt_rep,
                sTotEstBorrCostUlad = dataLoan.sTotEstBorrCostUlad_rep,
                sLDiscnt1003 = dataLoan.sLDiscnt1003_rep,
                sLDiscnt1003Lckd = dataLoan.sLDiscnt1003Lckd,
                sTotTransCUlad = dataLoan.sTotTransCUlad_rep,
                sLAmtCalc = dataLoan.sLAmtCalc_rep,
                sFfUfmipFinanced = dataLoan.sFfUfmipFinanced_rep,
                sFinalLAmt = dataLoan.sFinalLAmt_rep,
                sONewFinBal = dataLoan.sONewFinBal_rep,
                sTotMortLAmtUlad = dataLoan.sTotMortLAmtUlad_rep,
                sSellerCreditsUlad = dataLoan.sSellerCreditsUlad_rep,
                sTotOtherCreditsUlad = dataLoan.sTotOtherCreditsUlad_rep,
                sTotCreditsUlad = dataLoan.sTotCreditsUlad_rep,
                sTotMortLTotCreditUlad = dataLoan.sTotMortLTotCreditUlad_rep,
                sTransNetCashUlad = dataLoan.sTransNetCashUlad_rep,
                sAltCostLckd = dataLoan.sAltCostLckd,
                sTotEstCcNoDiscnt1003 = dataLoan.sTotEstCcNoDiscnt1003_rep,
                sTotEstCc1003Lckd = dataLoan.sTotEstCc1003Lckd,
                sTotEstPp1003 = dataLoan.sTotEstPp1003_rep,
                sTotEstPp1003Lckd = dataLoan.sTotEstPp1003Lckd,
                sTotCcPbs = dataLoan.sTotCcPbs_rep,
                sTotCcPbsLocked = dataLoan.sTotCcPbsLocked,
                sTRIDSellerCredits = dataLoan.sTRIDSellerCredits_rep,
                sAltCostLckdDisabled = !dataLoan.sIsRenovationLoan || (areConstructionLoanDataPointsMigrated && isConstruction)
            };
        }

        /// <summary>
        /// Updates a loan from the given QualifyingBorrowerModel.
        /// </summary>
        /// <param name="dataLoan">The loan to be updated.</param>
        /// <param name="model">The model to retrieve the data from.</param>
        public static void UpdateLoanFromQualifyingBorrowerModel(CPageData dataLoan, QualifyingBorrowerModel model)
        {
            dataLoan.sAltCost_rep = model.sAltCost;
            dataLoan.sLDiscnt1003_rep = model.sLDiscnt1003;
            dataLoan.sLDiscnt1003Lckd = model.sLDiscnt1003Lckd;
            dataLoan.sLAmtCalc_rep = model.sLAmtCalc;
            dataLoan.sAltCostLckd = model.sAltCostLckd;
            dataLoan.sTotEstCcNoDiscnt1003_rep = model.sTotEstCcNoDiscnt1003;
            dataLoan.sTotEstCc1003Lckd = model.sTotEstCc1003Lckd;
            dataLoan.sTotEstPp1003_rep = model.sTotEstPp1003;
            dataLoan.sTotEstPp1003Lckd = model.sTotEstPp1003Lckd;
            dataLoan.sTotCcPbs_rep = model.sTotCcPbs;
            dataLoan.sTotCcPbsLocked = model.sTotCcPbsLocked;
        }

        /// <summary>
        /// Generates the qualifying borrower model on page load for BasePage pages.
        /// </summary>
        /// <param name="page">The page that is loading the qualifying borrower data.</param>
        /// <param name="dataLoan">The loan to retrieve the data from.</param>
        public static void Init_QualifyingBorrower(BasePage page, CPageData dataLoan)
        {
            page.RegisterJsGlobalVariables("qualifyingBorrowerModel", SerializationHelper.JsonNetSerialize(RetrieveQualifyingBorrowerModel(dataLoan)));
        }

        /// <summary>
        /// Loads the user control.
        /// </summary>
        /// <param name="sender">The object sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            (Page as BasePage).RegisterCSS("material-icons.css");
            (Page as BasePage).RegisterJsScript("QualifyingBorrower.js");
            (Page as BasePage).RegisterJsScript("mask.js");
            (Page as BasePage).RegisterCSS("QualifyingBorrower.css");
        }
    }
}