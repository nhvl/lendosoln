﻿using System;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib.BatchOperationError;

namespace LendersOfficeApp.newlos
{
    public partial class BatchOperationErrorSummary : LendersOffice.Common.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cacheId = RequestHelper.GetSafeQueryString("id");

            if (string.IsNullOrEmpty(cacheId))
            {
                throw CBaseException.GenericException("Loaded BatchOperationErrorSummary without cache id.");
            }

            BatchOperationError<LoanError> batchOperationError = BatchOperationError.LoadFromCache<LoanError>(cacheId);

            HeaderMessage.Text = batchOperationError.Description;

            ErrorList.DataSource = batchOperationError.Errors;
            ErrorList.DataBind();
        }
    }
}
