﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalSupportPage.aspx.cs" Inherits="LendersOfficeApp.newlos.InternalSupportPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Internal Support Page</title>

    <style type="text/css">
        .popup {
            display: none;
        }

        .description {
            display: block;
        }

        #TouchResetOkBtn, #TouchResetCancelBtn {
            margin-top: 10px;
        }

        #LQBPopupElement {
            background-color: gainsboro;
            font-size: 13px;
            padding: 5px;
        }

        .sLegalEntityIdentifier, #sBorrowerApplicationCollectionT {
            width: 200px;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript">
    function _init() {
        lockField(document.getElementById('sRespa6DLckd'), 'sRespa6D');
        $("#TouchReset").click(ConfirmTouchReset);
    }

    function ConfirmTouchReset() {
        var settings = {
            onReturn: ConfirmTouchOk,
            hideCloseButton: true,
            height: 75
        };

        LQBPopup.ShowElement($("#ResetTouchConfirmationDialog"), settings);
    }

    function ConfirmTouchOk()
    {
        var args = {
            loanid:ML.sLId
        };
        var result = gService.loanedit.call('ResetTouchEvents', args);
        if(result.error)
        {
            alert(result.UserMessage)
        }
        else {
            alert("Touch Events reset.");
        }
    }

    function migrate(version) {
        var args = {
            loanid:ML.sLId,
            version:version
        };

        var result = gService.loanedit.call('Migrate', args);

        if (!result.error)
        {
            document.getElementById("sIsHousingExpenseMigrated").checked = result.value["sIsHousingExpenseMigrated"] == 'True';
            document.getElementById("sClosingCostFeeVersionT").value = result.value["sClosingCostFeeVersionT"];
            alert('Succesfully migrated both data sets.');
            window.parent.treeview.location = window.parent.treeview.location;
        } 
        else {
            alert(result.UserMessage);
        }
        return false;
    }

    function createClosingCostDisclosure(version) {
        var args = {
            loanid: ML.sLId
        };

        var result = gService.loanedit.call('CreateClosingDisclosureArchive', args);

        if (!result.error) {
            alert(result.value["ResultMsg"]);
            window.parent.treeview.location = window.parent.treeview.location;
        }
        else {
            alert(result.UserMessage);
        }
        return false;
    }

    function runAutoProcess(mode) {
        var args = {
            loanid: ML.sLId,
            action: mode
        };

        var result = gService.loanedit.call('RunAutoPriceEligibilityProcess', args);

        if (!result.error) {
            alert(result.value["ResultMsg"]);
        }
        else {
            alert(result.UserMessage);
        }
        return false;
    }

    var oldSaveMe = window.saveMe;
    window.saveMe = function(bRefreshScreen)
    {
        var saveResult = oldSaveMe(bRefreshScreen);

        if (saveResult)
        {
            // Refresh left tree frame to reload disclosure folders.
            window.parent.treeview.location = window.parent.treeview.location;
        }
        return saveResult;
    }

    function FixLEAndCDDates()
    {
        var args = {
            loanid: ML.sLId
        };

        var result = gService.loanedit.call('FixLEAndCDDates', args);

        if(!result.error)
        {
            alert("Loan Estimate and Closing Disclosure Dates fixed.");
        }
        else
        {
            alert(result.UserMessage);
        }

        return false;
    }

    function PerformRollback()
    {
        var args = {
            loanid: ML.sLId,
            rollbackTarget: document.getElementById("rollbackTarget").value
        };

        var result = gService.loanedit.call("RollbackLoanVersion", args);
        if (!result.error)
        {
            var resultStatus = result.value["IsSuccessful"];
            if (resultStatus == 'True') {
                alert("Rollback successful.");
            }
            else
            {
                alert(result.value["ErrorMessage"]);
            }
        }
        else
        {
            alert(result.UserMessage);
        }
    }

    function migrateToLatestCollectionVersion() {
        var args = {
            loanid: ML.sLId
        };
        gService.loanedit.callAsyncSimple('MigrateToLatestUladDataLayer', args, function (result) {
            if (!result.error) {
                document.getElementById("sBorrowerApplicationCollectionT").value = result.value["sBorrowerApplicationCollectionT"];
                $('#MigrateToLatestUladDataLayer').hide();
                $('#IsMigratedToLatestUladDataLayer').prop('checked', true);
                alert('Succesfully migrated to latest ULAD data layer.');
                window.parent.treeview.location = window.parent.treeview.location;
            } else {
                alert(result.UserMessage);
            }
        });
    }

    </script>
    <form id="form1" runat="server">
    <div>
        <table width="980px">
            <tr><td class="MainRightHeader">Internal Support Page</td></tr>
            <tr>
                <td>
                    <table cellspacing="2" cellpading="0" width="98%" border="0">
                    <tr>
                        <td class="FieldLabel">sIsAllowExcludeToleranceCure</td>
                        <td><asp:CheckBox ID="sIsAllowExcludeToleranceCure" runat="server" /></td>
                        <td style="width:70%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">sIsManuallySetThirdPartyAffiliateProps</td>
                        <td><asp:CheckBox ID="sIsManuallySetThirdPartyAffiliateProps" runat="server" /></td>
                        <td style="width:70%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">sClosingCostFeeVersionT</td>
                        <td><asp:DropDownList ID="sClosingCostFeeVersionT" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Closing Cost Fee AND Housing Expense Migration</td>
                        <td><input type="button"  value="Migrate to LegacyButMigrated" onclick="return migrate(1);" /></td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">sIsHousingExpenseMigrated</td>
                        <td>
                            <asp:CheckBox runat="server" ID="sIsHousingExpenseMigrated" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Create Closing Cost Disclosure</td>
                        <td>
                            <input type="button" value="Create Closing Disclosure Archive" onclick="return createClosingCostDisclosure();" />
                            This will create a CD Archive and mark it as disclosed. Please ask the Lender to review.
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Populate Line L of 1003 from Adjustments?</td>
                        <td>
                            <asp:CheckBox runat="server" ID="sLoads1003LineLFromAdjustments" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">sLoanFileT</td>
                        <td><asp:DropDownList ID="sLoanFileT" runat="server" Enabled="false"/></td>
                    </tr>
                    <tr>
                        <td class="FieldLable">Add IDs to Loan Estimate and Closing Disclosure Dates</td>
                        <td>
                            <input type="button" value="Add Ids to LE and CD Dates." onclick="return FixLEAndCDDates();" />
                        </td>
                    </tr>
                    <tr id="tr_sTRIDLoanEstimateCashToCloseCalcMethodT" runat="server" visible="false">
                        <td class="FieldLabel">Cash to Close Calculation Method</td> 
                        <td>
                            <asp:RadioButtonList runat="server" ID="sTRIDLoanEstimateCashToCloseCalcMethodT" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Standard" Value="0" />
                                <asp:ListItem Text="Alternative" Value="1" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Rollback sLoanVersionT</td>
                        <td>
                            <input type="text" id="rollbackTarget" style="width: 20px;" />
                            <input type="button" value="Rollback" onclick="PerformRollback();" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Use 2016 Combo and Proration Updates to 1003 Details</td>
                        <td>
                            <asp:CheckBox runat="server" ID="sUse2016ComboAndProrationUpdatesTo1003Details" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Prevent CEID from being sent to ComplianceEase</td>
                        <td>
                            <asp:CheckBox runat="server" ID="sBlockComplianceEaseIdFromRequest" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">RESPA 6 Date</td>
                        <td>
                            <ml:DateTextBox runat="server" ID="sRespa6D"></ml:DateTextBox>
                            <asp:CheckBox runat="server" ID="sRespa6DLckd" onchange="refreshCalculation();"/> Lckd
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">1003 Interview Date (from Preparer)</td>
                        <td>
                            <ml:DateTextBox runat="server" ID="App1003PrepareDate"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">Calculate sInitAPR and sLastDiscAPR?</td>
                        <td><asp:CheckBox runat="server" ID="sCalculateInitAprAndLastDiscApr" /></td>
                    </tr>
                     <tr>
                        <td class="FieldLabel">Run Overnight Process</td>
                        <td>
                            <hr />
                            <input type="button" id="AutoProcessBtn" value="Run Process On This Loan Now" runat="server" onclick="return runAutoProcess('runnow');" /><br />
                            This will run the "overnight" process for pricing and eligibilty on this loan right now.
                            <hr />
                            <input type="button" id="EnqueueLoanBtn" value="Enqueue This Loan For Automatic Process" runat="server" onclick="return runAutoProcess('enqueueloan');" /><br />
                            This will add this loan to the queue for processing.
                            <hr />
                            <input type="button" id="EnqueueLenderBtn" value="Enqueue All Applicable Loans At This Lender" runat="server" onclick="return runAutoProcess('enqueuebroker');" /><br />
                            This will add all applicable loans at this lender for processing.
                        </td>
                    </tr>
                        <tr>
                            <td class="FieldLabel">Reset the Touch Events</td>
                            <td>
                                <button type="button" id="TouchReset">Reset Touch Events</button>
                                <span class="description">This sets the field sStatusEventMigrationVersion to "Not migrated", and removes the loan's Status Event entries from the DB.</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Legal Entity Identifier</td>
                            <td>
                                <asp:TextBox ID="sLegalEntityIdentifier" runat="server" MaxLength="20" CssClass="sLegalEntityIdentifier"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="LegalEntityIdentifierValidator" runat="server" ControlToValidate="sLegalEntityIdentifier" Display="Dynamic" ValidationGroup="LegalEntityIdentifierValidationGroup" />
                                <asp:ValidationSummary ID="LegalEntityIdentifierValidationSummary" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="LegalEntityIdentifierValidationGroup" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">LQB Collections Feature</td>
                            <td><asp:TextBox ID="sBorrowerApplicationCollectionT" runat="server" Enabled="false" /></td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Latest Version of ULAD Compatible Application Information?</td>
                            <td>
                                <input type="checkbox" runat="server" id="IsMigratedToLatestUladDataLayer" disabled />
                                <input type="button" runat="server" id="MigrateToLatestUladDataLayer" value="Migrate to Latest ULAD Compatible Application Information" onclick="return migrateToLatestCollectionVersion()"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

        <div class="popup" id="ResetTouchConfirmationDialog">
            Are you sure you want to reset the touch events?  This will result in Data Loss.
            <br />
            <button type="button" onclick="LQBPopup.Return()" id="TouchResetOkBtn">OK</button>
            <button type="button" onclick="LQBPopup.Hide()" id="TouchResetCancelBtn">Cancel</button>
        </div>
    </form>
</body>
</html>
