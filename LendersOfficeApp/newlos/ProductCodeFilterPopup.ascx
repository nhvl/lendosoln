﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductCodeFilterPopup.ascx.cs" Inherits="LendersOfficeApp.newlos.ProductCodeFilterPopup" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script type="text/javascript">
    var availableCodesJsonId = '#<%= AspxTools.ClientId(sAvailableProductCodeFilter) %>';
    var selectedCodesJsonId = '#<%= AspxTools.ClientId(sSelectedProductCodeFilter) %>';
    var productCodeFileTypeJsonId = '#<%= AspxTools.ClientId(sProductCodesByFileType) %>'; 
</script>
<style type="text/css">
.Hidden
{
    display: none;
}
.Header
{
    text-align: center;
    border: 1px solid black;
    padding: 2px;
    margin: 0;
    color: white;
    background-color: maroon;
    font-weight: bold;
}
.Bolded
{
    font-weight: bold;
    color: black;
}
.TableCell
{
    padding: 10px;
    display: table-cell;
    vertical-align: middle;
}
.ProductCodeFilters .ui-dialog-titlebar-close
{
    display: none;
}
.ui-dialog-titlebar
{
    background-color:Maroon;
}
.ui-widget-content a
{
    color: blue;
}
.ui-widget-content a:hover
{
    color: orange;
}
.PickerSection
{
    overflow: auto;
    width: 100%;
    height: 100%;
    display: table;
}
select.Picker
{
    width: 300px;
}
select.AvailableCodesPicker
{
    height: 433px;
}
select.SelectedCodesPicker
{
    height: 450px;
}
.ActionButtonContainer
{
    text-align: center;
}
.ActionButtonList
{
    list-style-type: none;
    padding: 0;
    margin: 0;
}
.PickerButton
{
    width: 120px;
}
.FinalizeButtons
{
    text-align: center;
}
#availableCodeSearchBox
{
    vertical-align: middle;
    width: 300px;
    height: 18px;
    padding: 2px;
    margin: 0;
    border: 1px solid black;
}
.TooltipList
{
    list-style-type: none;
    padding: 2px;
    margin: 0;
}
#SelectedCodesPopup
{
    position: absolute;
    width: auto;
    background-color: rgb(255, 255, 202);
    border: 1px solid;
    z-index: 9999;
}
#SelectedCodesPopup td
{
    padding: 0 5px;
    text-indent: -3px;
}
</style>
<div id="SelectedCodesPopup" class="Hidden">
    <table>
        <tr>
            <td class="Bolded" colspan="5">Product Codes</td>
        </tr>
        <tr id="ProductCodesRow">
            <td id="NoCodesCol">
                <span>
                    No product codes are selected. Select product codes for more refined pricing results.
                </span>
            </td>
            <td>
                <ul class="TooltipList" id="ConvCodes"></ul>
            </td>
            <td>
                <ul class="TooltipList" id="FhaCodes"></ul>
            </td>
            <td>
                <ul class="TooltipList" id="VaCodes"></ul>
            </td>
            <td>
                <ul class="TooltipList" id="UsdaCodes"></ul>
            </td>
            <td>
                <ul class="TooltipList" id="OtherCodes"></ul>
            </td>
        </tr>
    </table>
</div>
<div id="AdvancedFilterPopup" class="Hidden">
    <input type="hidden" id="sSelectedProductCodeFilter" runat="server" />
    <input type="hidden" id="sAvailableProductCodeFilter" runat="server"/>
    <input type="hidden" id="sProductCodesByFileType" runat="server" />
    <div class="PickerSection">
        <div class="TableCell">
            <div class="Header">
                Available Product Codes
            </div>
            <div>
                <input type="text" placeholder="Search" id="availableCodeSearchBox" class="NoPricing" NoHighlight="true" />
            </div>
            <div class="PickerContainer">
                <select multiple="true" id="availableProductCodes" class="Picker AvailableCodesPicker NoPricing" NoHighlight="true"></select>
            </div>
        </div>
        <div class="TableCell ActionButtonContainer">
            <ul class="ActionButtonList">
                <li>
                    <input class="PickerButton" type="button" id="addAllBtn" value="Add All >>">
                </li>
                <li>
                    <input class="PickerButton" type="button" id="addBtn" value="Add >">
                </li>
                <li>
                    <input class="PickerButton" type="button" id="removeBtn" value="< Remove">
                </li>
                <li>
                    <input class="PickerButton" type="button" id="removeAllBtn" value="<< Remove All">
                </li>
                <li>
                    <a id="selectNoneLink">Select none</a>
                </li>
            </ul>
        </div>
        <div class="TableCell">
            <div class="Header">
                Selected Product Codes
            </div>
            <div class="PickerContainer">
                <select multiple="true" id="selectedProductCodes" class="Picker NoPricing SelectedCodesPicker" NoHighlight="true"></select>
            </div>
        </div>
    </div>
    <div class="FinalizeButtons">
        <input type="button" id="applyBtn" value="Apply" />
        <input type="button" id="cancelBtn" value="Cancel" />
    </div>
</div>