namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Xml;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions.ComplianceEagleIntegration;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Security;
    using LendersOffice.HttpModule;

    public partial class LeftSummaryFrame : LendersOffice.Common.BaseServicePage
	{
        // 1/29/2004 dd - Number of times to try reload invalid loan before display error.
        // Reason loan could not be found during the first attempt is loan creating happens in different process such
        // as from MessageQueue (NHC integration). Continue to poll until the number of tries exceed the threshold.
        private const int MAX_FAILED_ATTEMPTS = 6; 
        
        protected string m_title = "";
        protected string m_aBLastNm = "";
        protected bool m_hasError = false;
        protected bool m_isPolling = false;
        private List<IDocumentVendor> DocumentVendorSkin = DocumentVendorFactory.CurrentVendors();
        
        public bool m_IsEnablePTMDocMagicSeamlessInterface = false;

        
        protected bool m_hasRecentModification = false;

        private BrokerUserPrincipal BrokerUser 
        {
            get 
            {
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

                if (null == principal)
                    RequestHelper.Signout();
                return principal;
            }
        }
        
		protected Guid LoanID 
        {
            get { return RequestHelper.LoanID; }
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("ModelessDlg.js");
            this.EnableJqueryMigrate = false;
            RegisterService("LeftSummaryFrame", "/newlos/LeftSummaryFrameService.aspx");
        }

		protected void PageLoad(object sender, System.EventArgs a)
		{
            BrokerDB brokerDB = this.BrokerUser.BrokerDB;

            RegisterJsGlobalVariables("IsEnforceVersionCheck", brokerDB.IsEnforceVersionCheck);
            // 8/2/2010 dd - Detect if any loan modification in past X minutes and display warning to user.
            List<string> userNames = CPageData.GetRecentModificationUserName(LoanID, BrokerUser);
            m_hasRecentModification = userNames.Count > 0;

            ExtraTds.Visible = BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
            RateLockInfoTds.Visible = BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);

            this.RegisterVbsScript("common.vbs");
            RegisterJsGlobalVariables("sLId", LoanID);
            RegisterJsGlobalVariables("CheckDirtyInMinutes", ConstApp.LoanRecentMOdificationPendingDirtyCheckInMinutes);
            RegisterJsGlobalVariables("MinimumSecondsBetweenGfeToleranceRefreshes", ConstStage.MinimumSecondsBetweenGfeToleranceRefreshes);

			try 
			{
				// 11/8/2004 kb - We now check access control in the init call
				// to prevent improper loading when user can't read.

				CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LeftSummaryFrame));
				dataLoan.InitLoad();

                this.SetInitialLandingPage(brokerDB, dataLoan);

                dataLoan.LoadAppNames(m_applicantsDDL);

                ToleranceHeader0.InnerText = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE ? "GFE" : "TRID";
                ToleranceHeader10.InnerText = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE ? "GFE" : "TRID";

                RegisterJsGlobalVariables("isTrid", dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID);

                sLNm.Text = dataLoan.sLNm;
				sStatusT.Text = dataLoan.sStatusT_rep;                

				LoanOfficer.Text = dataLoan.sEmployeeLoanRepName; 

				CAppData dataApp = dataLoan.GetAppData(0);
                m_aBLastNm = Utilities.SafeJsString(dataApp.aBLastNm);

				string serverID = "";
				ServerLocation server = ConstAppDavid.CurrentServerLocation;
				switch (server) 
				{
					case ServerLocation.LocalHost:
					serverID = "(LOCALHOST)  (LOCALHOST)  (LOCALHOST)";
					break;
					case ServerLocation.Demo:
					serverID = "(DEMO)  (DEMO)  (DEMO)";
					break;
					case ServerLocation.Development:
					serverID = "(DEVELOPMENT)  (DEVELOPMENT)";
					break;
				}

				m_title = string.Format("{0}, {1} - {2} {3}", dataApp.aBLastNm, dataApp.aBFirstNm, dataLoan.sLNm, serverID); //.Replace("'", @"\'");
                RegisterJsGlobalVariables("Title", m_title);
				if( dataLoan.CanWrite == false && dataLoan.CanRead == true )
				{
					// 11/8/2004 kb - We had previously performed a
					// security check and cached the result.  We now
					// perform the check inside the loan load.  The
					// read only marker is visible iff we can't write,
					// but can read.

					panelReadOnly.Visible = true;
                    panelReadOnlyText.Text += "READ-ONLY ";
				}
                if (dataLoan.sLoanFileT == E_sLoanFileT.Sandbox)
                {
                    // 6/10/2014 dd - Display short status to indicate the loan is sandbox loan.
                    panelReadOnly.Visible = true;
                    panelReadOnlyText.Text += "SANDBOX FILE ";
                }
                if (dataLoan.sLoanFileT == E_sLoanFileT.Test)
                {
                    // 7/27/2015 je - Display short status to indicate the loan is a test loan.
                    panelReadOnly.Visible = true;
                    panelReadOnlyText.Text += "TEST FILE ";
                }

                var ComplianceIgnoreLoanStatuses = dataLoan.BrokerDB.ComplianceEaseExportConfig.IgnoredLoanStatuses.Select(o => CPageBase.sStatusT_map_rep(o)).ToList();
                RegisterJsObject("complianceDisabledStatuses", ComplianceIgnoreLoanStatuses);

                ComplianceEagleAuditConfiguration ceagleAuditConfig = dataLoan.BrokerDB.ComplianceEagleAuditConfiguration;

                var complianceEagleIgnoreLoanStatuses = ceagleAuditConfig.IgnoreLoanStatuses.Select(o => CPageBase.sStatusT_map_rep(o)).ToList();
                RegisterJsObject("complianceEagleDisabledStatuses", complianceEagleIgnoreLoanStatuses);

                //If the current loan is a template or qp loan, don't show the compliance audit.
                if (dataLoan.IsTemplate || dataLoan.sIsQuickPricerLoan || (dataLoan.sLoanFileT != E_sLoanFileT.Loan))
                {
                    Compliance.Visible = false;
                    ComplianceEagleDiv.Visible = false;
                }

                //Also don't show it unless the broker is on per-transaction billing
                if (brokerDB.BillingVersion != E_BrokerBillingVersion.PerTransaction)
                {
                    Compliance.Visible = false;
                    ComplianceEagleDiv.Visible = false;
                }

                //Also don't show it unless the broker has it enabled
                if (!brokerDB.IsEnablePTMComplianceEaseIndicator)
                {
                    Compliance.Visible = false;
                }

                if (!brokerDB.IsEnablePTMComplianceEagleAuditIndicator)
                {
                    ComplianceEagleDiv.Visible = false;
                }

                if (DocumentVendorSkin.Any(v => v.Skin.AutomaticPlanCodes))
                {
                    m_IsEnablePTMDocMagicSeamlessInterface = brokerDB.BillingVersion == E_BrokerBillingVersion.PerTransaction && brokerDB.IsEnablePTMDocMagicSeamlessInterface;
                }
                else
                {
                    m_IsEnablePTMDocMagicSeamlessInterface = false;
                }
            } 
			catch (LoanNotFoundException exc) 
			{
				// 1/29/2004 dd - If loan is not found then attempts to look up in 6 tries before return error message.
				// Workaround solution for NHC when there are the delay between creating new loan from MessageQueue and
				// get to loan edit.
				//
				// 11/8/2004 kb - The init load call will now throw
				// 'loan not found' when we are unable to establish
				// a valid access control check because the loan
				// could not be found.

				int count = 0;
				try 
				{
					count = int.Parse(Request.Form["numOfTries"]);
				} 
				catch {}
				if (count < MAX_FAILED_ATTEMPTS) 
				{
					m_isPolling = true;
					ClientScript.RegisterHiddenField("numOfTries", (++count).ToString());
				} 
				else 
				{
                    ErrorUtilities.DisplayErrorPage(exc, false, BrokerUser.BrokerId, BrokerUser.EmployeeId);
				}

			}
            catch (PageDataAccessDenied exc) 
            {
                this.AddInitScriptFunctionWithArgs("f_accessDenied", "'" + Utilities.SafeJsString(exc.UserMessage) + "'");
                return;
            }
            catch (CBaseException exc) 
            {
                ErrorUtilities.DisplayErrorPage(exc, true, BrokerUser.BrokerId, BrokerUser.EmployeeId);
            }
			catch( Exception exc )
			{
				ErrorUtilities.DisplayErrorPage( exc, true, BrokerUser.BrokerId, BrokerUser.EmployeeId );
			}
		}

        /// <summary>
        /// Sets the initial landing page for the editor.
        /// </summary>
        /// <param name="brokerDB">
        /// The broker for the user.
        /// </param>
        /// <param name="dataLoan">
        /// The loan being edited.
        /// </param>
        private void SetInitialLandingPage(BrokerDB brokerDB, CPageData dataLoan)
        {
            string initialUrl = null;
            var disableInitialPageSelectionFromFavorites = true;

            if (dataLoan.IsTemplate)
            {
                // 6/23/2005 dd - If it is a template then initial page will be the template settings.
                initialUrl = "LoanTemplateSettings.aspx?loanid=" + LoanID;
            }
            else if (string.IsNullOrWhiteSpace(RequestHelper.GetSafeQueryString("body_url")))
            {
                disableInitialPageSelectionFromFavorites = ConstStage.DisableInitialPageSelectionFromFavorites;
                initialUrl = this.GetInitialLoanEditorUrl();                
            }
            else
            {
                //this is where we get the address from the url Option

                try
                {
                    int urlOption;
                    urlOption = Int32.Parse(Request.QueryString["body_url"]);

                    initialUrl = DataAccess.Tools.UrlOptionToUrl((E_UrlOption)urlOption);

                    if (initialUrl.Equals(""))
                    {
                        String msg = @"The body_url has been given a value that does not match a value in the method UrlOptionToUrl in LosUtils.  This could be 
caused by having an Enum value missing from E_UrlOption, or by not including the Enum in UrlOptionToUrl() in LosUtils to return the page's location.";
                        DataAccess.Tools.LogErrorWithCriticalTracking(msg);
                        ErrorUtilities.DisplayErrorPage(msg, false, brokerDB.BrokerID, BrokerUser.EmployeeId);
                    }


                    if (initialUrl.Contains("?"))
                        initialUrl += "&loanid=" + LoanID;
                    else
                        initialUrl += "?loanid=" + LoanID;
                }
                catch (System.FormatException e)
                {
                    string value = RequestHelper.GetSafeQueryString("body_url");
                    String msg = @"The parameter 'body_url' is given the value '" + value + @"', which cannot be parsed into int.  If '" + value + @"' is an url, this can be solved
by adding its pageId, which can be found in LoanNavigationNew.xml.config, as an Enum value for E_urlOption.  Then add the url to the method UrlOptionToUrl() in LosUtils.";

                    DataAccess.Tools.LogErrorWithCriticalTracking(msg, e);
                    ErrorUtilities.DisplayErrorPage(msg, false, brokerDB.BrokerID, BrokerUser.EmployeeId);
                }
            }

            this.RegisterJsGlobalVariables("DisableInitialPageSelectionFromFavorites", disableInitialPageSelectionFromFavorites);
            this.RegisterJsGlobalVariables("InitialUrl", initialUrl);
        }

        /// <summary>
        /// Gets the initial URL for the loan editor.
        /// </summary>
        /// <param name="user">
        /// The user accessing the loan editor.
        /// </param>
        /// <param name="loanId">
        /// The ID of the loan.
        /// </param>
        /// <returns>
        /// The initial URL.
        /// </returns>
        private string GetInitialLoanEditorUrl()
        {
            var hasFeatureForRateLockPage = this.BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
            var hasRolesForRateLockPage = this.BrokerUser.HasAtLeastOneRole(new E_RoleT[]
            {
                E_RoleT.Underwriter,
                E_RoleT.LenderAccountExecutive,
                E_RoleT.LockDesk
            });

            if (hasFeatureForRateLockPage && hasRolesForRateLockPage)
            {
                return "LockDesk/BrokerRateLock.aspx?loanid=" + this.LoanID;
            }

            if (this.BrokerUser.BrokerDB.IsEnableBigLoanPage)
            {
                return "BigLoanInfo.aspx?loanid=" + this.LoanID;
            }

            return "LoanInfo.aspx?loanid=" + this.LoanID;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            RegisterVbsScript("common.vbs");
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
