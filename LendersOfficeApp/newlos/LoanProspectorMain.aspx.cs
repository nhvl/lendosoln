/// Author: David Dao

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Audit;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Conversions.LoanProspector;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos
{

	public partial class LoanProspectorMain : LoanProspectorMainEntryPage
	{
        protected bool m_canExportLoan = false;

        protected void PageLoad(object sender, System.EventArgs e)
		{
            m_canExportLoan = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanExportLoan);
            m_noExportPermissionTable.Visible = !m_canExportLoan;
            m_waitMessageTable.Visible = m_canExportLoan;
            if (!m_canExportLoan) 
            {
                return;
            }

            LoadData();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
