﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOfficeApp.los.LegalForm;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class SettlementCharges : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserWrite };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                return IsClosingCostMigrationArchivePage || base.IsReadOnly;
            }
        }

        protected bool IsClosingCostMigrationArchivePage
        {
            get
            {
                return Broker.IsGFEandCoCVersioningEnabled
                    && RequestHelper.GetSafeQueryString("IsClosingCostMigrationArchivePage") == "true";
            }
        }

        protected bool showFooter
        {
            get
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SettlementCharges));
                dataLoan.InitLoad();

                return !IsClosingCostMigrationArchivePage && dataLoan.sUseGFEDataForSCFields;
            }
        }

        private void SetByPassBgCalcForGfeAsDefault()
        {
            bool m_byPassBgCalcForGfeAsDefault = false;
            SqlParameter[] parameters = { new SqlParameter("@UserId", BrokerUser.UserId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "RetrieveByPassBgCalcForGfeAsDefaultByUserId", parameters))
            {
                if (reader.Read())
                    m_byPassBgCalcForGfeAsDefault = (bool)reader["ByPassBgCalcForGfeAsDefault"];
            }

            ByPassBgCalcForGfeAsDefault_0.Checked = !m_byPassBgCalcForGfeAsDefault;
            ByPassBgCalcForGfeAsDefault_1.Checked = m_byPassBgCalcForGfeAsDefault;
        }

        protected override void LoadData()
        {
            CPageData dataLoan;
            if (IsClosingCostMigrationArchivePage)
            {
                dataLoan = new CPageData(LoanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(SettlementCharges)).Union(
                    CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release));
            }
            else
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SettlementCharges));
            }
            dataLoan.InitLoad();
            if (IsClosingCostMigrationArchivePage)
            {
                dataLoan.TemporarilyUseSCDataForSCFields();
            }

            if(dataLoan.sUseGFEDataForSCFields && dataLoan.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock)
            {
                settlementLDiscntCalc.Style["display"] = "none";
            }

            bool heMigrated = dataLoan.sIsHousingExpenseMigrated;
            SetByPassBgCalcForGfeAsDefault();

            hfsLPurposeT.Value = dataLoan.sLPurposeT.ToString("d");
            hfsGfeUsePaidToFromOfficialContact.Value = LendersOffice.AntiXss.AspxTools.JsBool(dataLoan.sGfeUsePaidToFromOfficialContact);

            sSchedDueD1Lckd.Checked = dataLoan.sSchedDueD1Lckd;
            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sSchedDueD1.ToolTip = "Hint:  Enter 't' for today's date.";
            sSchedFundD.Text = dataLoan.sSchedFundD_rep;
            sSchedFundD.ToolTip = "Hint:  Enter 't' for today's date.";
            sDue.Text = dataLoan.sDue_rep;
            sTerm.Text = dataLoan.sTerm_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sDaysInYr.Text = dataLoan.sDaysInYr_rep;
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);

            sSettlement800U5F.Text = dataLoan.sSettlement800U5F_rep;
            sSettlement800U5FDesc.Text = dataLoan.sSettlement800U5FDesc;
            sSettlement800U4F.Text = dataLoan.sSettlement800U4F_rep;
            sSettlement800U4FDesc.Text = dataLoan.sSettlement800U4FDesc;
            sSettlement800U3F.Text = dataLoan.sSettlement800U3F_rep;
            sSettlement800U3FDesc.Text = dataLoan.sSettlement800U3FDesc;
            sSettlement800U2F.Text = dataLoan.sSettlement800U2F_rep;
            sSettlement800U2FDesc.Text = dataLoan.sSettlement800U2FDesc;
            sSettlement800U1F.Text = dataLoan.sSettlement800U1F_rep;
            sSettlement800U1FDesc.Text = dataLoan.sSettlement800U1FDesc;
            sSettlementWireF.Text = dataLoan.sSettlementWireF_rep;
            sSettlementUwF.Text = dataLoan.sSettlementUwF_rep;
            sSettlementProcF.Text = dataLoan.sSettlementProcF_rep;
            sSettlementProcFProps_ctrl.Paid = dataLoan.sSettlementProcFPaid;
            sSettlementTxServF.Text = dataLoan.sSettlementTxServF_rep;
            Tools.SetDropDownListValue(sSettlementFloodCertificationDeterminationT, dataLoan.sSettlementFloodCertificationDeterminationT);
            sSettlementFloodCertificationF.Text = dataLoan.sSettlementFloodCertificationF_rep;
            sSettlementMBrokF.Text = dataLoan.sSettlementMBrokF_rep;
            sSettlementMBrokFMb.Text = dataLoan.sSettlementMBrokFMb_rep;
            sSettlementMBrokFPc.Text = dataLoan.sSettlementMBrokFPc_rep;
            Tools.SetDropDownListValue(sSettlementMBrokFBaseT, dataLoan.sSettlementMBrokFBaseT);
            sSettlementInspectF.Text = dataLoan.sSettlementInspectF_rep;
            sSettlementCrF.Text = dataLoan.sSettlementCrF_rep;
            sSettlementCrFProps_ctrl.Paid = dataLoan.sSettlementCrFPaid;
            sSettlementApprF.Text = dataLoan.sSettlementApprF_rep;
            sSettlementApprFProps_ctrl.Paid = dataLoan.sSettlementApprFPaid;

            // Line 802
            // Credit or charge
            sSettlementLDiscnt.Text = dataLoan.sSettlementLDiscnt_rep;
            sSettlementLDiscntFMb.Text = dataLoan.sSettlementLDiscntFMb_rep;
            sSettlementLDiscntPc.Text = dataLoan.sSettlementLDiscntPc_rep;
            Tools.SetDropDownListValue(sSettlementLDiscntBaseT, dataLoan.sSettlementLDiscntBaseT);
            //InitItemProps(sSettlementLDiscntProps_ctrl, dataLoan.sSettlementLDiscntProps); // Removed for case 75442

            // Credit for lender paid fees
            Tools.SetDropDownListValue(sSettlementCreditLenderPaidItemT, dataLoan.sSettlementCreditLenderPaidItemT);
            sSettlementCreditLenderPaidItemF.Text = dataLoan.sSettlementCreditLenderPaidItemF_rep;

            // General lender credit
            sSettlementLenderCreditFPc.Text = dataLoan.sSettlementLenderCreditFPc_rep;
            sSettlementLenderCreditF.Text = dataLoan.sSettlementLenderCreditF_rep;
            //InitItemProps(sSettlementLenderCreditFProps_ctrl, dataLoan.sSettlementLenderCreditFProps); // nonexistent

            // Discount points
            sSettlementDiscountPointFPc.Text = dataLoan.sSettlementDiscountPointFPc_rep;
            sSettlementDiscountPointF.Text = dataLoan.sSettlementDiscountPointF_rep;
            InitItemProps(sSettlementDiscountPointFProps_ctrl, dataLoan.sSettlementDiscountPointFProps);
            if (sSettlementDiscountPointFProps_ctrl.PdByT_DDL.SelectedValue == LosConvert.LENDER_PAID.ToString()) // Lender paid doesn't make sense. Change it to borrower.
            {
                sSettlementDiscountPointFProps_ctrl.PdByT_DDL.SelectedValue = LosConvert.BORR_PAID_OUTOFPOCKET.ToString(); ;
            }
            var sSettlementDiscountPointFProps_ctrlPdByT_DDL_LenderPaid = sSettlementDiscountPointFProps_ctrl.PdByT_DDL.Items.FindByValue(LosConvert.LENDER_PAID.ToString());
            sSettlementDiscountPointFProps_ctrl.PdByT_DDL.Items.Remove(sSettlementDiscountPointFProps_ctrlPdByT_DDL_LenderPaid);

            sSettlementLOrigF.Text = dataLoan.sSettlementLOrigF_rep;
            sSettlementLOrigFMb.Text = dataLoan.sSettlementLOrigFMb_rep;
            sSettlementLOrigFPc.Text = dataLoan.sSettlementLOrigFPc_rep;
            sSettlement900U1Pia.Text = dataLoan.sSettlement900U1Pia_rep;
            sSettlement900U1PiaDesc.Text = dataLoan.sSettlement900U1PiaDesc;
            sVaFf.Text = dataLoan.sVaFf_rep;
            sSettlement904Pia.Text = dataLoan.sSettlement904Pia_rep;
            sSettlement904PiaDesc.Text = dataLoan.sSettlement904PiaDesc;
            sSettlementHazInsPia.Text = dataLoan.sSettlementHazInsPia_rep;
            sSettlementHazInsPiaMon.Text = dataLoan.sSettlementHazInsPiaMon_rep;
            Tools.SetDropDownListValue(sProHazInsT, dataLoan.sProHazInsT);
            sProHazInsR.Text = dataLoan.sProHazInsR_rep;
            sProHazInsMb.Text = dataLoan.sProHazInsMb_rep;
            sProHazInsHolder.Visible = !dataLoan.sHazardExpenseInDisbursementMode;
            sMipPia.Text = dataLoan.sMipPia_rep;

            sSettlementIPia.Text = dataLoan.sSettlementIPia_rep;
            sSettlementIPiaDy.Text = dataLoan.sSettlementIPiaDy_rep;
            sSettlementIPiaDyLckd.Checked = dataLoan.sSettlementIPiaDyLckd;
            sSettlementIPerDayLckd.Checked = dataLoan.sSettlementIPerDayLckd;
            sSettlementIPerDay.Text = dataLoan.sSettlementIPerDay_rep;
            sSettlementAggregateAdjRsrv.Text = dataLoan.sSettlementAggregateAdjRsrv_rep;
            sSettlementAggregateAdjRsrvLckd.Checked = dataLoan.sSettlementAggregateAdjRsrvLckd;
            sSettlement1009Rsrv.Text = dataLoan.sSettlement1009Rsrv_rep;
            if (heMigrated && dataLoan.sLine1009Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                s1007ProHExp.Text = dataLoan.sLine1009Expense.MonthlyAmtServicing_rep;
            }
            else
            {
                s1007ProHExp.Text = dataLoan.s1007ProHExp_rep;
            }
            s1007ProHExp.ReadOnly = (heMigrated && dataLoan.sLine1009Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            sSettlement1009RsrvMon.Text = dataLoan.sSettlement1009RsrvMon_rep;
            s1007ProHExpDesc.Text = dataLoan.s1007ProHExpDesc;
            sSettlement1008Rsrv.Text = dataLoan.sSettlement1008Rsrv_rep;
            if (heMigrated && dataLoan.sLine1008Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                s1006ProHExp.Text = dataLoan.sLine1008Expense.MonthlyAmtServicing_rep;
            }
            else
            {
                s1006ProHExp.Text = dataLoan.s1006ProHExp_rep;
            }
            s1006ProHExp.ReadOnly = (heMigrated && dataLoan.sLine1008Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            sSettlement1008RsrvMon.Text = dataLoan.sSettlement1008RsrvMon_rep;
            s1006ProHExpDesc.Text = dataLoan.s1006ProHExpDesc;
            sSettlementU3Rsrv.Text = dataLoan.sSettlementU3Rsrv_rep;
            if (heMigrated && dataLoan.sLine1010Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                sProU3Rsrv.Text = dataLoan.sLine1010Expense.MonthlyAmtServicing_rep;
            }
            else
            {
                sProU3Rsrv.Text = dataLoan.sProU3Rsrv_rep;
            }
            sProU3Rsrv.ReadOnly = (heMigrated && dataLoan.sLine1010Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            sSettlementU3RsrvMon.Text = dataLoan.sSettlementU3RsrvMon_rep;
            sU3RsrvDesc.Text = dataLoan.sU3RsrvDesc;
            sSettlementU4Rsrv.Text = dataLoan.sSettlementU4Rsrv_rep;
            if (heMigrated && dataLoan.sLine1011Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                sProU4Rsrv.Text = dataLoan.sLine1011Expense.MonthlyAmtServicing_rep;
            }
            else
            {
                sProU4Rsrv.Text = dataLoan.sProU4Rsrv_rep;
            }
            sProU4Rsrv.ReadOnly = (heMigrated && dataLoan.sLine1011Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            sSettlementU4RsrvMon.Text = dataLoan.sSettlementU4RsrvMon_rep;
            sU4RsrvDesc.Text = dataLoan.sU4RsrvDesc;
            sSettlementFloodInsRsrv.Text = dataLoan.sSettlementFloodInsRsrv_rep;
            if (heMigrated && dataLoan.sFloodExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                sProFloodIns.Text = dataLoan.sFloodExpense.MonthlyAmtServicing_rep;
            }
            else
            {
                sProFloodIns.Text = dataLoan.sProFloodIns_rep;
            }
            sProFloodIns.ReadOnly = (heMigrated && dataLoan.sFloodExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            sSettlementFloodInsRsrvMon.Text = dataLoan.sSettlementFloodInsRsrvMon_rep;
            sSettlementRealETxRsrv.Text = dataLoan.sSettlementRealETxRsrv_rep;
            Tools.SetDropDownListValue(sProRealETxT, dataLoan.sProRealETxT);
            sProRealETxMb.Text = dataLoan.sProRealETxMb_rep;
            sProRealETxR.Text = dataLoan.sProRealETxR_rep;
            sProRealETxHolder.Visible = !dataLoan.sRealEstateTaxExpenseInDisbMode;
            sSettlementRealETxRsrvMon.Text = dataLoan.sSettlementRealETxRsrvMon_rep;
            sSettlementSchoolTxRsrv.Text = dataLoan.sSettlementSchoolTxRsrv_rep;
            if (heMigrated && dataLoan.sSchoolTaxExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                sProSchoolTx.Text = dataLoan.sSchoolTaxExpense.MonthlyAmtServicing_rep;
            }
            else
            {
                sProSchoolTx.Text = dataLoan.sProSchoolTx_rep;
            }
            sProSchoolTx.ReadOnly = (heMigrated && dataLoan.sSchoolTaxExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            sSettlementSchoolTxRsrvMon.Text = dataLoan.sSettlementSchoolTxRsrvMon_rep;
            sSettlementMInsRsrv.Text = dataLoan.sSettlementMInsRsrv_rep;
            sProMIns.Text = dataLoan.sProMIns_rep;
            sSettlementMInsRsrvMon.Text = dataLoan.sSettlementMInsRsrvMon_rep;
            sSettlementHazInsRsrv.Text = dataLoan.sSettlementHazInsRsrv_rep;
            if (heMigrated && dataLoan.sHazardExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                sProHazIns.Text = dataLoan.sHazardExpense.MonthlyAmtServicing_rep;
            }
            else
            {
                sProHazIns.Text = dataLoan.sProHazIns_rep;
            }
            sSettlementHazInsRsrvMon.Text = dataLoan.sSettlementHazInsRsrvMon_rep;
            sSettlementU4Tc.Text = dataLoan.sSettlementU4Tc_rep;
            sSettlementU4TcDesc.Text = dataLoan.sSettlementU4TcDesc;
            sSettlementU3Tc.Text = dataLoan.sSettlementU3Tc_rep;
            sSettlementU3TcDesc.Text = dataLoan.sSettlementU3TcDesc;
            sSettlementU2Tc.Text = dataLoan.sSettlementU2Tc_rep;
            sSettlementU2TcDesc.Text = dataLoan.sSettlementU2TcDesc;
            sSettlementU1Tc.Text = dataLoan.sSettlementU1Tc_rep;
            sSettlementU1TcDesc.Text = dataLoan.sSettlementU1TcDesc;
            sSettlementTitleInsF.Text = dataLoan.sSettlementTitleInsF_rep;
            sSettlementAttorneyF.Text = dataLoan.sSettlementAttorneyF_rep;
            sSettlementNotaryF.Text = dataLoan.sSettlementNotaryF_rep;
            sSettlementDocPrepF.Text = dataLoan.sSettlementDocPrepF_rep;
            sSettlementEscrowF.Text = dataLoan.sSettlementEscrowF_rep;
            sSettlementOwnerTitleInsF.Text = dataLoan.sSettlementOwnerTitleInsF_rep;
            sSettlementU3GovRtc.Text = dataLoan.sSettlementU3GovRtc_rep;
            sSettlementU3GovRtcMb.Text = dataLoan.sSettlementU3GovRtcMb_rep;
            Tools.SetDropDownListValue(sSettlementU3GovRtcBaseT, dataLoan.sSettlementU3GovRtcBaseT);
            sSettlementU3GovRtcPc.Text = dataLoan.sSettlementU3GovRtcPc_rep;
            sSettlementU3GovRtcDesc.Text = dataLoan.sSettlementU3GovRtcDesc;
            sSettlementU2GovRtc.Text = dataLoan.sSettlementU2GovRtc_rep;
            sSettlementU2GovRtcMb.Text = dataLoan.sSettlementU2GovRtcMb_rep;
            Tools.SetDropDownListValue(sSettlementU2GovRtcBaseT, dataLoan.sSettlementU2GovRtcBaseT);
            sSettlementU2GovRtcPc.Text = dataLoan.sSettlementU2GovRtcPc_rep;
            sSettlementU2GovRtcDesc.Text = dataLoan.sSettlementU2GovRtcDesc;
            sSettlementU1GovRtc.Text = dataLoan.sSettlementU1GovRtc_rep;
            sSettlementU1GovRtcMb.Text = dataLoan.sSettlementU1GovRtcMb_rep;
            Tools.SetDropDownListValue(sSettlementU1GovRtcBaseT, dataLoan.sSettlementU1GovRtcBaseT);
            sSettlementU1GovRtcPc.Text = dataLoan.sSettlementU1GovRtcPc_rep;
            sSettlementU1GovRtcDesc.Text = dataLoan.sSettlementU1GovRtcDesc;
            sSettlementStateRtc.Text = dataLoan.sSettlementStateRtc_rep;
            sSettlementStateRtcMb.Text = dataLoan.sSettlementStateRtcMb_rep;
            Tools.SetDropDownListValue(sSettlementStateRtcBaseT, dataLoan.sSettlementStateRtcBaseT);
            sSettlementStateRtcPc.Text = dataLoan.sSettlementStateRtcPc_rep;
            sSettlementCountyRtc.Text = dataLoan.sSettlementCountyRtc_rep;
            sSettlementCountyRtcMb.Text = dataLoan.sSettlementCountyRtcMb_rep;
            Tools.SetDropDownListValue(sSettlementCountyRtcBaseT, dataLoan.sSettlementCountyRtcBaseT);
            sSettlementCountyRtcPc.Text = dataLoan.sSettlementCountyRtcPc_rep;
            sSettlementRecF.Text = dataLoan.sSettlementRecF_rep;
            sSettlementRecFMb.Text = dataLoan.sSettlementRecFMb_rep;
            Tools.SetDropDownListValue(sSettlementRecBaseT, dataLoan.sSettlementRecBaseT);
            sSettlementRecFPc.Text = dataLoan.sSettlementRecFPc_rep;
            sSettlementRecFLckd.Checked = dataLoan.sSettlementRecFLckd;
            sSettlementRecDeed.Text = dataLoan.sSettlementRecDeed_rep;
            sSettlementRecMortgage.Text = dataLoan.sSettlementRecMortgage_rep;
            sSettlementRecRelease.Text = dataLoan.sSettlementRecRelease_rep;
            sSettlementU5Sc.Text = dataLoan.sSettlementU5Sc_rep;
            sSettlementU5ScDesc.Text = dataLoan.sSettlementU5ScDesc;
            sSettlementU4Sc.Text = dataLoan.sSettlementU4Sc_rep;
            sSettlementU4ScDesc.Text = dataLoan.sSettlementU4ScDesc;
            sSettlementU3Sc.Text = dataLoan.sSettlementU3Sc_rep;
            sSettlementU3ScDesc.Text = dataLoan.sSettlementU3ScDesc;
            sSettlementU2Sc.Text = dataLoan.sSettlementU2Sc_rep;
            sSettlementU2ScDesc.Text = dataLoan.sSettlementU2ScDesc;
            sSettlementU1Sc.Text = dataLoan.sSettlementU1Sc_rep;
            sSettlementU1ScDesc.Text = dataLoan.sSettlementU1ScDesc;
            sSettlementPestInspectF.Text = dataLoan.sSettlementPestInspectF_rep;
            sSettlementTotalEstimateSettlementCharge.Text = dataLoan.sSettlementTotalEstimateSettlementCharge_rep;

            InitItemProps(sSettlementLOrigFProps_ctrl, dataLoan.sSettlementLOrigFProps);
            InitItemProps(sSettlementApprFProps_ctrl, dataLoan.sSettlementApprFProps);
            InitItemProps(sSettlementCrFProps_ctrl, dataLoan.sSettlementCrFProps);
            InitItemProps(sSettlementInspectFProps_ctrl, dataLoan.sSettlementInspectFProps);
            InitItemProps(sSettlementMBrokFProps_ctrl, dataLoan.sSettlementMBrokFProps);
            InitItemProps(sSettlementTxServFProps_ctrl, dataLoan.sSettlementTxServFProps);
            InitItemProps(sSettlementFloodCertificationFProps_ctrl, dataLoan.sSettlementFloodCertificationFProps);
            InitItemProps(sSettlementProcFProps_ctrl, dataLoan.sSettlementProcFProps);
            InitItemProps(sSettlementUwFProps_ctrl, dataLoan.sSettlementUwFProps);
            InitItemProps(sSettlementWireFProps_ctrl, dataLoan.sSettlementWireFProps);
            InitItemProps(sSettlement800U1FProps_ctrl, dataLoan.sSettlement800U1FProps);
            InitItemProps(sSettlement800U2FProps_ctrl, dataLoan.sSettlement800U2FProps);
            InitItemProps(sSettlement800U3FProps_ctrl, dataLoan.sSettlement800U3FProps);
            InitItemProps(sSettlement800U4FProps_ctrl, dataLoan.sSettlement800U4FProps);
            InitItemProps(sSettlement800U5FProps_ctrl, dataLoan.sSettlement800U5FProps);
            InitItemProps(sSettlementIPiaProps_ctrl, dataLoan.sSettlementIPiaProps);
            InitItemProps(sMipPiaProps_ctrl, dataLoan.sMipPiaProps);
            InitItemProps(sSettlementHazInsPiaProps_ctrl, dataLoan.sSettlementHazInsPiaProps);
            InitItemProps(sSettlement904PiaProps_ctrl, dataLoan.sSettlement904PiaProps);
            InitItemProps(sVaFfProps_ctrl, dataLoan.sVaFfProps);
            InitItemProps(sSettlement900U1PiaProps_ctrl, dataLoan.sSettlement900U1PiaProps);
            InitItemProps(sSettlementHazInsRsrvProps_ctrl, dataLoan.sSettlementHazInsRsrvProps);
            InitItemProps(sSettlementMInsRsrvProps_ctrl, dataLoan.sSettlementMInsRsrvProps);
            InitItemProps(sSettlementSchoolTxRsrvProps_ctrl, dataLoan.sSettlementSchoolTxRsrvProps);
            InitItemProps(sSettlementRealETxRsrvProps_ctrl, dataLoan.sSettlementRealETxRsrvProps);
            InitItemProps(sSettlementFloodInsRsrvProps_ctrl, dataLoan.sSettlementFloodInsRsrvProps);
            InitItemProps(sSettlement1008RsrvProps_ctrl, dataLoan.sSettlement1008RsrvProps);
            InitItemProps(sSettlement1009RsrvProps_ctrl, dataLoan.sSettlement1009RsrvProps);
            InitItemProps(sSettlementU3RsrvProps_ctrl, dataLoan.sSettlementU3RsrvProps);
            InitItemProps(sSettlementU4RsrvProps_ctrl, dataLoan.sSettlementU4RsrvProps);
            InitItemProps(sSettlementAggregateAdjRsrvProps_ctrl, dataLoan.sSettlementAggregateAdjRsrvProps);
            InitItemProps(sSettlementEscrowFProps_ctrl, dataLoan.sSettlementEscrowFProps);
            InitItemProps(sSettlementOwnerTitleInsFProps_ctrl, dataLoan.sSettlementOwnerTitleInsFProps);
            InitItemProps(sSettlementDocPrepFProps_ctrl, dataLoan.sSettlementDocPrepFProps);
            InitItemProps(sSettlementNotaryFProps_ctrl, dataLoan.sSettlementNotaryFProps);
            InitItemProps(sSettlementAttorneyFProps_ctrl, dataLoan.sSettlementAttorneyFProps);
            InitItemProps(sSettlementTitleInsFProps_ctrl, dataLoan.sSettlementTitleInsFProps);
            InitItemProps(sSettlementU1TcProps_ctrl, dataLoan.sSettlementU1TcProps);
            InitItemProps(sSettlementU2TcProps_ctrl, dataLoan.sSettlementU2TcProps);
            InitItemProps(sSettlementU3TcProps_ctrl, dataLoan.sSettlementU3TcProps);
            InitItemProps(sSettlementU4TcProps_ctrl, dataLoan.sSettlementU4TcProps);
            InitItemProps(sSettlementRecFProps_ctrl, dataLoan.sSettlementRecFProps);
            InitItemProps(sSettlementCountyRtcProps_ctrl, dataLoan.sSettlementCountyRtcProps);
            InitItemProps(sSettlementStateRtcProps_ctrl, dataLoan.sSettlementStateRtcProps);
            InitItemProps(sSettlementU1GovRtcProps_ctrl, dataLoan.sSettlementU1GovRtcProps);
            InitItemProps(sSettlementU2GovRtcProps_ctrl, dataLoan.sSettlementU2GovRtcProps);
            InitItemProps(sSettlementU3GovRtcProps_ctrl, dataLoan.sSettlementU3GovRtcProps);
            InitItemProps(sSettlementPestInspectFProps_ctrl, dataLoan.sSettlementPestInspectFProps);
            InitItemProps(sSettlementU1ScProps_ctrl, dataLoan.sSettlementU1ScProps);
            InitItemProps(sSettlementU2ScProps_ctrl, dataLoan.sSettlementU2ScProps);
            InitItemProps(sSettlementU3ScProps_ctrl, dataLoan.sSettlementU3ScProps);
            InitItemProps(sSettlementU4ScProps_ctrl, dataLoan.sSettlementU4ScProps);
            InitItemProps(sSettlementU5ScProps_ctrl, dataLoan.sSettlementU5ScProps);

            // OPM 144673 - Add Paid To fields to Settlement Charges
            sSettlementApprFProps_ctrl.SetPaidTo(()=> dataLoan.sApprFPaidTo);
            sSettlementCrFProps_ctrl.SetPaidTo(()=> dataLoan.sCrFPaidTo);
            sSettlementTxServFProps_ctrl.SetPaidTo(()=> dataLoan.sTxServFPaidTo);
            sSettlementFloodCertificationFProps_ctrl.SetPaidTo(()=> dataLoan.sFloodCertificationFPaidTo);
            sSettlementInspectFProps_ctrl.SetPaidTo(()=> dataLoan.sInspectFPaidTo);
            sSettlementProcFProps_ctrl.SetPaidTo(()=> dataLoan.sProcFPaidTo);
            sSettlementUwFProps_ctrl.SetPaidTo(()=> dataLoan.sUwFPaidTo);
            sSettlementWireFProps_ctrl.SetPaidTo(()=> dataLoan.sWireFPaidTo);
            sSettlement800U1FProps_ctrl.SetPaidTo(()=> dataLoan.s800U1FPaidTo);
            sSettlement800U2FProps_ctrl.SetPaidTo(()=> dataLoan.s800U2FPaidTo);
            sSettlement800U3FProps_ctrl.SetPaidTo(()=> dataLoan.s800U3FPaidTo);
            sSettlement800U4FProps_ctrl.SetPaidTo(()=> dataLoan.s800U4FPaidTo);
            sSettlement800U5FProps_ctrl.SetPaidTo(()=> dataLoan.s800U5FPaidTo);
            sSettlementOwnerTitleInsFProps_ctrl.SetPaidTo(()=> dataLoan.sOwnerTitleInsPaidTo);
            sSettlementDocPrepFProps_ctrl.SetPaidTo(()=> dataLoan.sDocPrepFPaidTo);
            sSettlementNotaryFProps_ctrl.SetPaidTo(()=> dataLoan.sNotaryFPaidTo);
            sSettlementAttorneyFProps_ctrl.SetPaidTo(()=> dataLoan.sAttorneyFPaidTo);
            sSettlementU1TcProps_ctrl.SetPaidTo(()=> dataLoan.sU1TcPaidTo);
            sSettlementU2TcProps_ctrl.SetPaidTo(()=> dataLoan.sU2TcPaidTo);
            sSettlementU3TcProps_ctrl.SetPaidTo(()=> dataLoan.sU3TcPaidTo);
            sSettlementU4TcProps_ctrl.SetPaidTo(()=> dataLoan.sU4TcPaidTo);
            sSettlementU1GovRtcProps_ctrl.SetPaidTo(()=> dataLoan.sU1GovRtcPaidTo);
            sSettlementU2GovRtcProps_ctrl.SetPaidTo(()=> dataLoan.sU2GovRtcPaidTo);
            sSettlementU3GovRtcProps_ctrl.SetPaidTo(()=> dataLoan.sU3GovRtcPaidTo);
            sSettlementPestInspectFProps_ctrl.SetPaidTo(()=> dataLoan.sPestInspectPaidTo);
            sSettlementU1ScProps_ctrl.SetPaidTo(()=> dataLoan.sU1ScPaidTo);
            sSettlementU2ScProps_ctrl.SetPaidTo(()=> dataLoan.sU2ScPaidTo);
            sSettlementU3ScProps_ctrl.SetPaidTo(()=> dataLoan.sU3ScPaidTo);
            sSettlementU4ScProps_ctrl.SetPaidTo(()=> dataLoan.sU4ScPaidTo);
            sSettlementU5ScProps_ctrl.SetPaidTo(()=> dataLoan.sU5ScPaidTo);
            sSettlementHazInsPiaProps_ctrl.SetPaidTo(()=> dataLoan.sHazInsPiaPaidTo);
            sMipPiaProps_ctrl.SetPaidTo(()=> dataLoan.sMipPiaPaidTo);
            sVaFfProps_ctrl.SetPaidTo(()=> dataLoan.sVaFfPaidTo);
            sSettlementTitleInsFProps_ctrl.SetPaidTo(()=> dataLoan.sTitleInsFTable);
            sSettlementEscrowFProps_ctrl.SetPaidTo(()=> dataLoan.sEscrowFTable);
            sSettlementStateRtcProps_ctrl.SetPaidTo(()=> dataLoan.sStateRtcDesc);
            sSettlementCountyRtcProps_ctrl.SetPaidTo(()=> dataLoan.sCountyRtcDesc);
            sSettlementRecFProps_ctrl.SetPaidTo(()=> dataLoan.sRecFDesc);

            sSettlementApprFProps_ctrl.ToBrok = dataLoan.sApprFProps_ToBroker;
            sSettlementCrFProps_ctrl.ToBrok = dataLoan.sCrFProps_ToBroker;
            sSettlementTxServFProps_ctrl.ToBrok = dataLoan.sTxServFProps_ToBroker;
            sSettlementFloodCertificationFProps_ctrl.ToBrok = dataLoan.sFloodCertificationFProps_ToBroker;
            sSettlementInspectFProps_ctrl.ToBrok = dataLoan.sInspectFProps_ToBroker;
            sSettlementProcFProps_ctrl.ToBrok = dataLoan.sProcFProps_ToBroker;
            sSettlementUwFProps_ctrl.ToBrok = dataLoan.sUwFProps_ToBroker;
            sSettlementWireFProps_ctrl.ToBrok = dataLoan.sWireFProps_ToBroker;
            sSettlement800U1FProps_ctrl.ToBrok = dataLoan.s800U1FProps_ToBroker;
            sSettlement800U2FProps_ctrl.ToBrok = dataLoan.s800U2FProps_ToBroker;
            sSettlement800U3FProps_ctrl.ToBrok = dataLoan.s800U3FProps_ToBroker;
            sSettlement800U4FProps_ctrl.ToBrok = dataLoan.s800U4FProps_ToBroker;
            sSettlement800U5FProps_ctrl.ToBrok = dataLoan.s800U5FProps_ToBroker;
            sSettlementOwnerTitleInsFProps_ctrl.ToBrok = dataLoan.sOwnerTitleInsProps_ToBroker;
            sSettlementDocPrepFProps_ctrl.ToBrok = dataLoan.sDocPrepFProps_ToBroker;
            sSettlementNotaryFProps_ctrl.ToBrok = dataLoan.sNotaryFProps_ToBroker;
            sSettlementAttorneyFProps_ctrl.ToBrok = dataLoan.sAttorneyFProps_ToBroker;
            sSettlementU1TcProps_ctrl.ToBrok = dataLoan.sU1TcProps_ToBroker;
            sSettlementU2TcProps_ctrl.ToBrok = dataLoan.sU2TcProps_ToBroker;
            sSettlementU3TcProps_ctrl.ToBrok = dataLoan.sU3TcProps_ToBroker;
            sSettlementU4TcProps_ctrl.ToBrok = dataLoan.sU4TcProps_ToBroker;
            //sSettlementU1GovRtcProps_ctrl.ToBrok = dataLoan.sU1GovRtcProps_ToBroker;
            //sSettlementU2GovRtcProps_ctrl.ToBrok = dataLoan.sU2GovRtcProps_ToBroker;
            //sSettlementU3GovRtcProps_ctrl.ToBrok = dataLoan.sU3GovRtcProps_ToBroker;
            sSettlementPestInspectFProps_ctrl.ToBrok = dataLoan.sPestInspectFProps_ToBroker;
            sSettlementU1ScProps_ctrl.ToBrok = dataLoan.sU1ScProps_ToBroker;
            sSettlementU2ScProps_ctrl.ToBrok = dataLoan.sU2ScProps_ToBroker;
            sSettlementU3ScProps_ctrl.ToBrok = dataLoan.sU3ScProps_ToBroker;
            sSettlementU4ScProps_ctrl.ToBrok = dataLoan.sU4ScProps_ToBroker;
            sSettlementU5ScProps_ctrl.ToBrok = dataLoan.sU5ScProps_ToBroker;
            //sSettlementHazInsPiaProps_ctrl.ToBrok = dataLoan.sHazInsPiaProps_ToBroker;
            sSettlementTitleInsFProps_ctrl.ToBrok = dataLoan.sTitleInsFProps_ToBroker;
            sSettlementEscrowFProps_ctrl.ToBrok = dataLoan.sEscrowFProps_ToBroker;
            //sSettlementStateRtcProps_ctrl.ToBrok = dataLoan.sStateRProps_ToBroker;
            //sSettlementCountyRtcProps_ctrl.ToBrok = dataLoan.sCountyRtcProps_ToBroker;
            //sSettlementRecFProps_ctrl.ToBrok = dataLoan.sRecFProps_ToBroker;
            sSettlementLOrigFProps_ctrl.ToBrok = dataLoan.sLOrigFProps_ToBroker;
            sSettlementMBrokFProps_ctrl.ToBrok = dataLoan.sMBrokFProps_ToBroker;

            sSettlement800U1FProps_ctrl.Page2Selection = dataLoan.s800U1FSettlementSection;
            sSettlement800U2FProps_ctrl.Page2Selection = dataLoan.s800U2FSettlementSection;
            sSettlement800U3FProps_ctrl.Page2Selection = dataLoan.s800U3FSettlementSection;
            sSettlement800U4FProps_ctrl.Page2Selection = dataLoan.s800U4FSettlementSection;
            sSettlement800U5FProps_ctrl.Page2Selection = dataLoan.s800U5FSettlementSection;
            sSettlementEscrowFProps_ctrl.Page2Selection = dataLoan.sEscrowFSettlementSection;
            sSettlementDocPrepFProps_ctrl.Page2Selection = dataLoan.sDocPrepFSettlementSection;
            sSettlementNotaryFProps_ctrl.Page2Selection = dataLoan.sNotaryFSettlementSection;
            sSettlementAttorneyFProps_ctrl.Page2Selection = dataLoan.sAttorneyFSettlementSection;
        //    sSettlementTitleInsFProps_ctrl.Page2Selection = dataLoan.sTitleInsFSettlementSection;
            sSettlementU1TcProps_ctrl.Page2Selection = dataLoan.sU1TcSettlementSection;
            sSettlementU2TcProps_ctrl.Page2Selection = dataLoan.sU2TcSettlementSection;
            sSettlementU3TcProps_ctrl.Page2Selection = dataLoan.sU3TcSettlementSection;
            sSettlementU4TcProps_ctrl.Page2Selection = dataLoan.sU4TcSettlementSection;
            sSettlementU1GovRtcProps_ctrl.Page2Selection = dataLoan.sU1GovRtcSettlementSection;
            sSettlementU2GovRtcProps_ctrl.Page2Selection = dataLoan.sU2GovRtcSettlementSection;
            sSettlementU3GovRtcProps_ctrl.Page2Selection = dataLoan.sU3GovRtcSettlementSection;
            sSettlementU1ScProps_ctrl.Page2Selection = dataLoan.sU1ScSettlementSection;
            sSettlementU2ScProps_ctrl.Page2Selection = dataLoan.sU2ScSettlementSection;
            sSettlementU3ScProps_ctrl.Page2Selection = dataLoan.sU3ScSettlementSection;
            sSettlementU4ScProps_ctrl.Page2Selection = dataLoan.sU4ScSettlementSection;
            sSettlementU5ScProps_ctrl.Page2Selection = dataLoan.sU5ScSettlementSection;
            sSettlement904PiaProps_ctrl.Page2Selection = dataLoan.s904PiaSettlementSection;
            sSettlement900U1PiaProps_ctrl.Page2Selection = dataLoan.s900U1PiaSettlementSection;

            if (Broker.IsB4AndB6DisabledIn1100And1300OfGfe)
                hideB4andB6();

            sGfeIsTPOTransaction.Checked = dataLoan.sGfeIsTPOTransaction;

            if (dataLoan.sGfeIsTPOTransactionIsCalculated
                && (dataLoan.sBranchChannelT == E_BranchChannelT.Broker
                    || dataLoan.sBranchChannelT == E_BranchChannelT.Retail
                    || dataLoan.sBranchChannelT == E_BranchChannelT.Wholesale))
            {
                // OPM 198345. It is calculated and readonly.
                sGfeIsTPOTransaction.Enabled = false;
            }

            sSettlementTotalDedFromLoanProc.Text = dataLoan.sSettlementTotalDedFromLoanProc_rep;
            sSettlementTotalFundByLenderAtClosing.Text = dataLoan.sSettlementTotalFundByLenderAtClosing_rep;


            Tools.SetRadioButtonListValue(sOriginatorCompensationPaymentSourceT, dataLoan.sOriginatorCompensationPaymentSourceT);

            sGfeOriginatorCompF.Text = dataLoan.sGfeOriginatorCompF_rep;
            sGfeOriginatorCompFPc.Text = dataLoan.sGfeOriginatorCompFPc_rep;
            sGfeOriginatorCompFMb.Text = dataLoan.sGfeOriginatorCompFMb_rep;
            Tools.SetDropDownListValue(sGfeOriginatorCompFBaseT, dataLoan.sGfeOriginatorCompFBaseT);
            InitItemProps(sGfeOriginatorCompFProps_ctrl, dataLoan.sGfeOriginatorCompFProps);
            sGfeOriginatorCompFProps_ctrl.ToBrok = LosConvert.GfeItemProps_ToBr(dataLoan.sGfeOriginatorCompFProps);
            if (dataLoan.sUseGFEDataForSCFields)
            {
                sSettlementHazInsRsrvMon.ReadOnly = (false == dataLoan.sHazInsRsrvMonLckd) || IsReadOnly;
                sSettlementMInsRsrvMon.ReadOnly = (false == dataLoan.sMInsRsrvMonLckd) || IsReadOnly;
                sSettlementRealETxRsrvMon.ReadOnly = (false == dataLoan.sRealETxRsrvMonLckd) || IsReadOnly;
                sSettlementSchoolTxRsrvMon.ReadOnly = (false == dataLoan.sSchoolTxRsrvMonLckd) || IsReadOnly;
                sSettlementFloodInsRsrvMon.ReadOnly = (false == dataLoan.sFloodInsRsrvMonLckd) || IsReadOnly;
                sSettlement1008RsrvMon.ReadOnly = (false == dataLoan.s1006RsrvMonLckd) || IsReadOnly;
                sSettlement1009RsrvMon.ReadOnly = (false == dataLoan.s1007RsrvMonLckd) || IsReadOnly;
                sSettlementU3RsrvMon.ReadOnly = (false == dataLoan.sU3RsrvMonLckd) || IsReadOnly;
                sSettlementU4RsrvMon.ReadOnly = (false == dataLoan.sU4RsrvMonLckd) || IsReadOnly;
            }
            // 3/15/2012 dd - Per OPM 59454 - We are switch sSettlementChargesExportSource when user copy
            // GFE values to Settlement Charges page.
            ClientScript.RegisterHiddenField("sSettlementChargesExportSource", dataLoan.sSettlementChargesExportSource.ToString("D"));

            ReadonlifyAdditionalControlsAsNeeded();
        }

        private void ReadonlifyAdditionalControlsAsNeeded()
        {
            if (IsReadOnly)
            {
                var gfercs = Form.Controls.OfType<GoodFaithEstimate2010RightColumn>();

                foreach (var gferc in gfercs)
                {
                    gferc.Enabled = !IsReadOnly;
                }
            }
        }

        private void hideB4andB6()
        {
            List<GoodFaithEstimate2010RightColumn> charges1100 = new List<GoodFaithEstimate2010RightColumn>();
            charges1100.Add(sSettlementEscrowFProps_ctrl);
            charges1100.Add(sSettlementOwnerTitleInsFProps_ctrl);
            charges1100.Add(sSettlementTitleInsFProps_ctrl);
            charges1100.Add(sSettlementDocPrepFProps_ctrl);
            charges1100.Add(sSettlementNotaryFProps_ctrl);
            charges1100.Add(sSettlementAttorneyFProps_ctrl);
            charges1100.Add(sSettlementU1TcProps_ctrl);
            charges1100.Add(sSettlementU2TcProps_ctrl);
            charges1100.Add(sSettlementU3TcProps_ctrl);
            charges1100.Add(sSettlementU4TcProps_ctrl);

            foreach (GoodFaithEstimate2010RightColumn rightCol in charges1100)
            {
                switch (rightCol.getIndexOfRadioButton("B6"))
                {
                    case 1:
                        rightCol.Page2Option1Text = "";
                        rightCol.Page2A_rbVisible = false;
                        break;
                    case 2:
                        rightCol.Page2Option2Text = "";
                        rightCol.Page2B_rbVisible = false;
                        break;
                    case 3:
                        rightCol.Page2Option3Text = "";
                        rightCol.Page2C_rbVisible = false;
                        break;
                }
            }


            List<GoodFaithEstimate2010RightColumn> charges1300 = new List<GoodFaithEstimate2010RightColumn>();
            charges1300.Add(sSettlementU1ScProps_ctrl);
            charges1300.Add(sSettlementU2ScProps_ctrl);
            charges1300.Add(sSettlementU3ScProps_ctrl);
            charges1300.Add(sSettlementU4ScProps_ctrl);
            charges1300.Add(sSettlementU5ScProps_ctrl);

            foreach (GoodFaithEstimate2010RightColumn rightCol in charges1300)
            {
                switch (rightCol.getIndexOfRadioButton("B4"))
                {
                    case 1:
                        rightCol.Page2Option1Text = "";
                        rightCol.Page2A_rbVisible = false;
                        break;
                    case 2:
                        rightCol.Page2Option2Text = "";
                        rightCol.Page2B_rbVisible = false;
                        break;
                    case 3:
                        rightCol.Page2Option3Text = "";
                        rightCol.Page2C_rbVisible = false;
                        break;
                }
            }
        }

        private void InitItemProps(GoodFaithEstimate2010RightColumn ctrl, int props)
        {
            ctrl.PdByT = LosConvert.GfeItemProps_Payer(props);
            ctrl.Borr = LosConvert.GfeItemProps_Borr(props);
            ctrl.Poc = LosConvert.GfeItemProps_Poc(props);
            ctrl.Dflp = LosConvert.GfeItemProps_Dflp(props);
            ctrl.Apr_Hidden = LosConvert.GfeItemProps_Apr(props);
            ctrl.ThirdParty_Hidden = LosConvert.GfeItemProps_PaidToThirdParty(props);
            ctrl.Affiliate_Hidden = LosConvert.GfeItemProps_ThisPartyIsAffiliate(props);
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");

            this.EnableJqueryMigrate = false;

            // OPM 170146 - Manually insert copyright notice.
            DisplayCopyRight = false;

            this.PageTitle = "Settlement Charges";
            if (IsClosingCostMigrationArchivePage)
            {
                this.PageID = "SettlementChargesClosingCostMigrationArchive";
            }
            else
            {
                this.PageID = "SettlementCharges";
            }
            //this.PDFPrintClass = typeof(PDFPrintClass);

            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sProRealETxT(sProRealETxT);
            Tools.Bind_PercentBaseT(sProHazInsT);
            Tools.Bind_PercentBaseT(sSettlementRecBaseT);
            Tools.Bind_PercentBaseT(sSettlementCountyRtcBaseT);
            Tools.Bind_PercentBaseT(sSettlementStateRtcBaseT);
            Tools.Bind_PercentBaseT(sSettlementU1GovRtcBaseT);
            Tools.Bind_PercentBaseT(sSettlementU2GovRtcBaseT);
            Tools.Bind_PercentBaseT(sSettlementU3GovRtcBaseT);
            Tools.Bind_PercentBaseLoanAmountsT(sSettlementLDiscntBaseT);
            Tools.Bind_FloodCertificationDeterminationT(sSettlementFloodCertificationDeterminationT);
            Tools.Bind_PercentBaseLoanAmountsT(sSettlementMBrokFBaseT);

            Tools.Bind_sGfeOriginatorCompFBaseT(sGfeOriginatorCompFBaseT);
            Tools.Bind_sOriginatorCompensationPaymentSourceT(sOriginatorCompensationPaymentSourceT);
            Tools.Bind_sGfeCreditLenderPaidItemT(sSettlementCreditLenderPaidItemT);

            if (IsClosingCostMigrationArchivePage)
            {
                hfIsClosingCostMigrationArchivePage.Value = "true";
                TitleLiteral.Text = string.Format("{0} Settlement Charges Data", RequestHelper.GetSafeQueryString("MigratedOrDiscarded"));
            }
            phFieldsBefore800.Visible = !IsClosingCostMigrationArchivePage;

            phAdditionalSection1000CustomFees.Visible = Broker.EnableAdditionalSection1000CustomFees;
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
