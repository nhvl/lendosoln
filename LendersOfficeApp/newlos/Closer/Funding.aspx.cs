﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;
using LendersOffice.ObjLib.Rolodex;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class Funding : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserWrite };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Funding));
            dataLoan.InitLoad();

            // OPM 214147 - Remove links to settlement charges page.
            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                lnkSettlementChargesDedFromLoanProc.Visible = true;
                lnkSettlementTotalFundByLenderAtClosing.Visible = true;
                lblSettlementChargesDedFromLoanProc.Visible = false;
                lblSettlementTotalFundByLenderAtClosing.Visible = false;
            }
            else
            {
                lnkSettlementChargesDedFromLoanProc.Visible = false;
                lnkSettlementTotalFundByLenderAtClosing.Visible = false;
                lblSettlementChargesDedFromLoanProc.Visible = true;
                lblSettlementTotalFundByLenderAtClosing.Visible = true;
            }

            if ((dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated && dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID) 
                || dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
            {
                this.PDFPrintClass = typeof(LendersOffice.Pdf.CFundingWorksheet2015);
            }
            else
            {
                this.PDFPrintClass = typeof(LendersOffice.Pdf.CStandardFundingWorksheetPDF);
            }

            this.AdjustmentsOtherCreditsFields.Visible = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

            var lenderInfo = WarehouseLenderRolodexEntry.Get(PrincipalFactory.CurrentPrincipal.BrokerId, dataLoan.sWarehouseLenderRolodexId);
            BindWarehouseLenderNames(dataLoan.sWarehouseLenderRolodexId);
            BindAdvanceClassifications(lenderInfo.AdvanceClassifications, dataLoan.sWarehouseAdvanceClassification);

            sWarehouseFunderDesc.Text = dataLoan.sWarehouseFunderDesc;
            Tools.SetDropDownListValue(sFundingDisbursementMethodT, dataLoan.sFundingDisbursementMethodT);
            sWarehouseMaxFundPc.Text = dataLoan.sWarehouseMaxFundPc_rep;
            sWarehouseMaxFundAmt.Text = dataLoan.sWarehouseMaxFundAmt_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sSettlementChargesDedFromLoanProc.Text = dataLoan.sSettlementChargesDedFromLoanProc_rep;
            sSettlementTotalFundByLenderAtClosing.Text = dataLoan.sSettlementTotalFundByLenderAtClosing_rep;
            sAdjustmentsOtherCreditsDeductedFromLoanProceeds.Text = dataLoan.sAdjustmentsOtherCreditsDeductedFromLoanProceeds_rep;
            sAdjustmentsOtherCreditsFundedByLenderAtClosing.Text = dataLoan.sAdjustmentsOtherCreditsFundedByLenderAtClosing_rep;
            sOtherFundAdj.Text = dataLoan.sOtherFundAdj_rep;
            sAmtReqToFund.Text = dataLoan.sAmtReqToFund_rep;
            
            sAmtFundFromWarehouseLine.Text = dataLoan.sAmtFundFromWarehouseLine_rep;
            sAmtFundFromWarehouseLineLckd.Checked = dataLoan.sAmtFundFromWarehouseLineLckd;
            sFundReqForShortfall.Text = dataLoan.sFundReqForShortfall_rep;
            sChkDueFromClosingRcvd.Checked = dataLoan.sChkDueFromClosingRcvd;
            sChkDueFromClosingRcvd.Enabled = IsReadOnly ? false : !dataLoan.sChkDueFromClosingIsZero;
            sChkDueFromClosing.Text = dataLoan.sChkDueFromClosing_rep;

            sSchedFundD.Text = dataLoan.sSchedFundD_rep;
            sFundsOrderedD.Text = dataLoan.sFundsOrderedD_rep;
            sFundD.Text = dataLoan.sFundD_rep;
            sFundN.Text = dataLoan.sFundN;
            sShippedToWarehouseD.Text = dataLoan.sShippedToWarehouseD_rep;
            sShippedToWarehouseN.Text = dataLoan.sShippedToWarehouseN;
            sTrackingN.Text = dataLoan.sTrackingN;

            Tools.SetDropDownListValue(sLoanProceedsToT, dataLoan.sLoanProceedsToT);

            sFundNotes.Text = dataLoan.sFundNotes;

            CFM.IsLocked = dataLoan.sFundingCfmIsLocked;
            CFM.AgentRoleT = dataLoan.sFundingCfmAgentRoleT;

            sFundingCompanyName.Text = dataLoan.sFundingCompanyName;
            sFundingBranchName.Text = dataLoan.sFundingBranchName;
            sFundingContactName.Text = dataLoan.sFundingContactName;
            sFundingCompanyAddr.Text = dataLoan.sFundingCompanyAddr;
            sFundingCompanyCity.Text = dataLoan.sFundingCompanyCity;
            sFundingCompanyState.Value = dataLoan.sFundingCompanyState;
            sFundingCompanyZip.Text = dataLoan.sFundingCompanyZip;
            sFundingCompanyPhone.Text = dataLoan.sFundingCompanyPhone;
            sFundingCompanyFax.Text = dataLoan.sFundingCompanyFax;
            
            sFundingBankName.Value = dataLoan.sFundingBankName;
            sFundingBankCityState.Value = dataLoan.sFundingBankCityState;
            sFundingABANumber.Value = dataLoan.sFundingABANumber.Value;
            sFundingAccountNumber.Value = dataLoan.sFundingAccountNumber.Value;
            sFundingAccountName.Value = dataLoan.sFundingAccountName;

            sFundingFurtherCreditToAccountName.Text = dataLoan.sFundingFurtherCreditToAccountName;
            sFundingFurtherCreditToAccountNumber.Text = dataLoan.sFundingFurtherCreditToAccountNumber.Value;
            sFundingRequestBatchNumber.Text = dataLoan.sFundingRequestBatchNumber;

            sFundingAdditionalInstructionsLine1.Value = dataLoan.sFundingAdditionalInstructionsLine1.Value;
            sFundingAdditionalInstructionsLine2.Value = dataLoan.sFundingAdditionalInstructionsLine2.Value;

            sConsummationD.Text = dataLoan.sConsummationD_rep;
            sConsummationDLckd.Checked = dataLoan.sConsummationDLckd;
        }
        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Funding";
            this.PageID = "FundingCloser";

            this.EnableJqueryMigrate = false;

            sFundingCompanyZip.SmartZipcode(sFundingCompanyCity, sFundingCompanyState);
            
            Tools.Bind_sLoanProceedsToT(sLoanProceedsToT);
            Tools.Bind_sFundingDisbursementMethodT(sFundingDisbursementMethodT);

            InitContactFieldMapper();
            this.RegisterJsScript("LQBPopup.js");
        }
        private void BindWarehouseLenderNames(int selected)
        {
            List<WarehouseLenderRolodexEntry> entries = WarehouseLenderRolodexEntry.GetAll(PrincipalFactory.CurrentPrincipal.BrokerId, null).ToList();
            sWarehouseLenderRolodexId.DataSource = entries.Where(p => p.Status == E_WarehouseLenderStatus.Active || p.Id == selected);
            sWarehouseLenderRolodexId.DataTextField = "WarehouseLenderName";
            sWarehouseLenderRolodexId.DataValueField = "Id";
            sWarehouseLenderRolodexId.DataBind();

            sWarehouseLenderRolodexId.Items.Insert(0, new ListItem("<-- Select a Warehouse Lender -->", "-1"));
            // 8/22/2013 GF - If the value for "Other" changes, ensure StandardFundingWorksheetPDF.cs
            // still loads the lender name correctly. OPM 135072
            sWarehouseLenderRolodexId.Items.Add(new ListItem("Other", "-2"));
            Tools.SetDropDownListValue(sWarehouseLenderRolodexId, selected);
        }

        private void BindAdvanceClassifications(List<WarehouseLenderAdvanceClassification> entries, string selected)
        {
            sWarehouseAdvanceClassification.DataSource = entries.Select(
                    item => item.Description + " - " + item.MaxFundingPercent_rep
                );
            sWarehouseAdvanceClassification.DataBind();
            sWarehouseAdvanceClassification.Items.Insert(0, new ListItem("", ""));
            sWarehouseAdvanceClassification.SelectedValue = selected;
        }
        private void InitContactFieldMapper()
        {
            CFM.IsAllowLockableFeature = true;
            CFM.Type = "19";
            CFM.AgentNameField = sFundingContactName.ClientID;
            CFM.BranchNameField = sFundingBranchName.ClientID;
            CFM.StreetAddressField = sFundingCompanyAddr.ClientID;
            CFM.CityField = sFundingCompanyCity.ClientID;
            CFM.StateField = sFundingCompanyState.ClientID;
            CFM.ZipField = sFundingCompanyZip.ClientID;
            CFM.CompanyNameField = sFundingCompanyName.ClientID;
            CFM.CompanyPhoneField = sFundingCompanyPhone.ClientID;
            CFM.CompanyFaxField = sFundingCompanyFax.ClientID;
            CFM.PayToBankNameField = sFundingBankName.ClientID;
            CFM.PayToBankCityStateField = sFundingBankCityState.ClientID;
            CFM.PayToABANumberField = sFundingABANumber.ClientID;
            CFM.PayToAccountNameField = sFundingAccountName.ClientID;
            CFM.PayToAccountNumberField = sFundingAccountNumber.ClientID;
            CFM.FurtherCreditToAccountNumberField = sFundingFurtherCreditToAccountNumber.ClientID;
            CFM.FurtherCreditToAccountNameField = sFundingFurtherCreditToAccountName.ClientID;           
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
