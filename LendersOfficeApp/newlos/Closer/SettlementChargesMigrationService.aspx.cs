﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Closer
{
    public class SettlementChargesMigrationServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return new CPageData(sLId, "SettlementMigration", CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(SettlementChargesMigrationServiceItem)).Union(
                CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Migrate":
                    Migrate();
                    break;
                case "UndoMigration":
                    UndoMigration();
                    break;
            }
        }

        protected void Migrate()
        {
            var sFileVersion = GetInt("sFileVersion");
            var currentClosingCostSource = (E_CurrentClosingCostSourceT)GetInt("CurrentClosingCostSource");
            var lastDisclosedClosingCostSource = (E_LastDisclosedClosingCostSourceT)GetInt("LastDisclosedClosingCostSource");
            var dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(sFileVersion);
            dataLoan.MigrateCurrentClosingCostDataToGFE(currentClosingCostSource, lastDisclosedClosingCostSource);
            dataLoan.Save();

            SetResult("sFileVersion", dataLoan.sFileVersion_rep);
            LoadData(dataLoan, null);
        }

        protected void UndoMigration()
        {
            var sFileVersion = GetInt("sFileVersion");
            var dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(sFileVersion);
            dataLoan.RestoreClosingCostDataFromMigrationArchive();
            dataLoan.Save();

            SetResult("sFileVersion", dataLoan.sFileVersion_rep);
            LoadData(dataLoan, null);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sCurrentClosingCostStructure", dataLoan.sCurrentClosingCostStructure);
            SetResult("sUseGFEDataForSCFields", dataLoan.sUseGFEDataForSCFields);
            SetResult("sClosingCostMigrationCurrentClosingCostSource", dataLoan.sClosingCostMigrationCurrentClosingCostSource);
            SetResult("sClosingCostMigrationLastDisclosedClosingCostSource", dataLoan.sClosingCostMigrationLastDisclosedClosingCostSource);
        }
    }
    public partial class SettlementChargesMigrationService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new SettlementChargesMigrationServiceItem());
        }
    }
}
