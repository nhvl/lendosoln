﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="TitleAndVesting.aspx.cs" Inherits="LendersOfficeApp.newlos.Closer.TitleAndVesting" %>

<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="DataAccess.Trust" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Title And Vesting</title>
    <style type="text/css">
        #Lock
        {
            float: right;
            width: 6em;
        }
        .MostlyFull
        {
            width: 28em;
        }
        .FullWidth
        {
            width: 34em;
        }
        div.FieldItem input, div.FieldItem select
        {
            margin-left: 0;
        }
        .TitleWidth
        {
            width: 15em;
        }
        span.TitleWidth, .Title
        {
            padding: 3px;
            font-weight: bold;
        }
        #TitleBorrowers {
            margin-left: 3px;
            margin-top: 3px;
            border: groove thin;
            width: 671px;
        }
        #TitleBorrowers div.FormTableSubHeader  {
            width: 665px;
        }
        .Title
        {
            margin-top: 5px;
        }
        #TrustInformation
        {
            margin-left: 3px;
            border: groove thin;
            width: 671px;
        }
        div.FormTableSubHeader
        {
            padding: 3px;
            width: 665px;
        }
        div.FieldItem
        {
            margin-top: 3px;
            margin-bottom: 3px;
        }
        #TrustorsSettlors, #CurrentTrustees
        {
            margin-top: 10px;
        }
        #TrustInformation div span
        {
            padding: 3px;
        }
        .Action
        {
            color: Blue;
            cursor: pointer;
            text-decoration: underline;
        }
        .Action.Disabled
        {
            cursor: auto;
        }
        .Cloneable
        {
            list-style-type: none;
            margin: 0px;
            padding: 3px;
            border: 0px;
        }
        li
        {
            margin-bottom: 5px;
        }
        #TrusteeList li
        {
            margin-top: 5px;
            margin-bottom: 15px;
        }
        #TrusteeList li:last-child
        {
            margin-bottom: 0px;
        }
        .City
        {
            width: 25.4em;
        }
        .ZipCode
        {
            width: 4em;
        }
        .Phone
        {
            width: 10em;
        }
        .SignatureVerbiage
        {
            vertical-align: top;
        }
        .Left 
        {
            float: left;
        }
        .Clear
        {
            clear: both;
        }
        .hidden { display: none;}
        #TitleOnylBorrowerTable { list-style-type: none; margin-left: 5px; margin-top: 0; padding: 0; }
        #TitleOnylBorrowerTable li {
            margin: 0;
            padding-left: 0;
        }
        .w-680 { width: 680; }
        .AddTitleOnlyBorrower { padding: 3px; }
        .email-error
        {
            color: red;
            display: none;
        }
    </style>
</head>
<body bgcolor="gainsboro">

    <script type="text/javascript">
        var numTitleOnlyBorrowerClicked = 0;

function onEditBorrAliasesClick(index)
{
    editAliasesPOA('B', index);
}

function onEditCoborrAliasesClick(index)
{
    editAliasesPOA('C', index);
}

function editAliasesPOA(borrMode, index)
{
    var aliasPOAData = {
        alias : $('input[id$=a' + borrMode + 'Aliases'+index+']').val(),
        powerOfAttorney : $('input[id$=a' + borrMode + 'PowerOfAttorneyNm'+index+']').val(),
        index: index
    };
    var args = {
        data : JSON.stringify(aliasPOAData)
    };
    
    var path = '/newlos/Borrower/EditAliasesPOA.aspx';
    var url = path
        + '?loanid=' + <%= AspxTools.JsString(LoanID) %>
        + '&borrmode=' + borrMode;
    showModal(url, args, null, null, function(modalResult){
        if (modalResult.OK) {
            var data = JSON.parse(modalResult.data);
            $('input[id$=a' + borrMode + 'Aliases'+index+']').val(data.alias);
            $('input[id$=a' + borrMode + 'PowerOfAttorneyNm'+index+']').val(data.powerOfAttorney);
            // Mark page as dirty
            updateDirtyBit();
        }
    },{ hideCloseButton: true });
}

    function getId() {
        var result = gService.utils.call('UniqueId', {});
        return result.value.Id;;
    }
        function _init() {
            for (var i = 0; document.getElementById("Relationships_ctl0" + i + "_aBRelationshipTitleT") != null; i += 2) {                
                var borrDDL = document.getElementById("Relationships_ctl0" + i + "_aBRelationshipTitleT");
                var cobrDDL = document.getElementById("Relationships_ctl0" + i + "_aCRelationshipTitleT");
                
                var borrTb = document.getElementById("Relationships_ctl0" + i + "_aBRelationshipTitleOtherDesc");
                var cobrTb = document.getElementById("Relationships_ctl0" + i + "_aCRelationshipTitleOtherDesc");
                var otrVal = <%= AspxTools.HtmlString(E_aRelationshipTitleT.Other.ToString("d")) %>;
                
                // If the DDL is set to "other", show the textbox
                if (borrDDL.value == otrVal) {
                    borrTb.readOnly = false;
                    borrTb.style.display = "block";
                } else {
                    borrTb.style.display = "none";
                }
                if (cobrTb != null) {
                    if (cobrTb == otrVal) {
                        cobrTb.readOnly = false;
                        cobrTb.style.display = "block";
                    } else {
                        cobrTb.style.display = "none";
                    }
                }
                borrTb.readOnly = borrDDL.value != otrVal;
                if (cobrTb != null)
                    cobrTb.readOnly = cobrDDL.value != otrVal;
            }
            
            if ( <%= AspxTools.JsBool(useNewNonPurchaseBorrower) %> )
            {
                $('.AddTitleOnlyBorrower').hide();
                if (<%= AspxTools.JsBool(TitleOnlyBorrowers.Items.Count == 0) %>)
                {
                    $('#TitleBorrowers').hide();
                }
            }
            lockField(<%= AspxTools.JsGetElementById(sVestingToReadLckd) %>, <%= AspxTools.JsString(sVestingToRead.ClientID) %>);

            $('.ulad-data').toggle(ML.IsTargeting2019Ulad);
        }

        function updateTitleLockBoxDisplay(checkbox) {
            $(checkbox).prev().prop('disabled', !checkbox.checked);
        }

        function validateEmailAddress(input) {
            var isValid = true;
            if (input.value !== '') {
                var emailRegex = new RegExp(ML.EmailValidationExpression);
                isValid = emailRegex.test(input.value);
            }
        
            return isValid;
        }
        
        jQuery(function($){
            //Stuff the contents of our trustor/settor and trustee lists into an input before save
            var oldSaveMe = saveMe;
            saveMe = function(bRefresh)
            {
                var isValid = true;
                $('#TitleOnylBorrowerTable .TitleEmail').each(function (index, input) {
                    if (!validateEmailAddress(input)) {
                        isValid = false;
                        return false;
                    }
                });

                if (!isValid) {
                    return false;
                }

                var trustors = [];
                $('#TrustorList li input').each(function(){
                    trustors.push({'Name': $(this).val()});
                });
                
                var trustees = [];
                $('#TrusteeList li').each(function(){
                    
                    var name = $(this).find('.Name').val();
                    var phone = $(this).find('.Phone').val();
                    
                    var streetAddr = $(this).find('.StreetAddress').val();
                    var city = $(this).find('.City').val();
                    var state = $(this).find('.State').val();
                    var zip = $(this).find('.ZipCode').val();
                    var certTrust = $(this).find('.CanSignCertOfTrust').is(':checked');
                    var loanDocs = $(this).find('.CanSignLoanDocs').is(':checked');
                    var signatureVerb = $(this).find('.SignatureVerbiage').val();
                    
                    var address = 
                    { 
                        'StreetAddress': streetAddr,
                        'City': city,
                        'State': state,
                        'PostalCode': zip
                    }
                    
                    var current = {
                        'Name': name,
                        'Phone': phone,
                        'Address': address,
                        'CanSignCertOfTrust': certTrust,
                        'CanSignLoanDocs': loanDocs,
                        'SignatureVerbiage': signatureVerb
                    };
                    
                    trustees.push(current);
                });
                
                var result = {
                        'Trustors': trustors,
                        'Trustees': trustees
                    };
                
                
                $('#TrustInfoHolder').val(JSON.stringify(result));
                
                return oldSaveMe(bRefresh);
            }

            $('#TitleOnylBorrowerTable .title-borrower-lock').each(function (index, checkbox) {
	            updateTitleLockBoxDisplay(checkbox);
            });

            $('#TitleOnylBorrowerTable').on('change', '.title-borrower-lock', function (event) {
                var target = retrieveEventTarget(event);
                updateTitleLockBoxDisplay(target);
                refreshCalculation();
            });

            $('#TitleOnylBorrowerTable').on('blur', '.TitleEmail', function (event) {
                var target = retrieveEventTarget(event);
                var isValid = validateEmailAddress(target);
                $(target).parent().find('span.email-error').toggle(!isValid);
            });

            $('#TitleOnylBorrowerTable').on('click', 'a.EditAliasPOAClick', function() {
                var parent = $(this).parents('li');
                var aliasPOAData = {
                    alias : parent.find('input[id$=TitleAliases]').val(),
                    powerOfAttorney : parent.find('input.TitlePOA').val()
                }; 
                var args = {
                    data : JSON.stringify(aliasPOAData)
                };
           
                var path = '/newlos/Borrower/EditAliasesPOA.aspx';
                var url = path
                    + '?loanid=' + <%= AspxTools.JsString(LoanID) %>
                    + '&borrmode=B';   
                showModal(url, args, null, null, function(modalResult){
                    if (modalResult.OK) {
                        var data = JSON.parse(modalResult.data);
                        parent.find('input[id$=TitleAliases]').val(data.alias);
                        parent.find('input.TitlePOA').val(data.powerOfAttorney);
                        // Mark page as dirty
                        updateDirtyBit();
                    } 
                },{hideCloseButton:true});    
                return false;
          });
            
            $('.Cloneable').each(function(){SetRemoveState(this);});
        
            $('#sVestingToReadLckd').change(function(){
                if($(this).is(':checked'))
                {
                    $('#sVestingToRead').removeAttr('readonly');
                }
                else
                {
                    $('#sVestingToRead').attr('readonly', 'readonly');
                    refreshCalculation();
                }
                
            });//.change();
            
            $('.CloneSibling').click(function(){
                if($(this).data('disabled')) return;
                
                CloneEmptyFirstElement($(this).siblings('.Cloneable').first());
                SetRemoveState($(this).siblings('.Cloneable'));
                updateDirtyBit();
                
            });
            
            $('.Cloneable').on('click', 'a.Remove', function(){
                if (hasDisabledAttr($(this))) {
                    return;
                }

                var li = $(this).closest('li');
                var ul = li.parent();
                li.remove();
                updateDirtyBit();
                SetRemoveState(ul);
            });

            $('.Cloneable,#TitleOnylBorrowerTable').on('keydown, change', 'input', function(event){
                if(typeof(updateDirtyBit) != 'undefined') updateDirtyBit(event);
            });
            
            $(document).on('change', 'select.TitleRelationshipTitleT', function(){
                if ($(this).val() == 53) {
                    $(this).parents('div').siblings('div.tilteotherdesc').removeClass('hidden');
                }
                else {
                    $(this).parents('div').siblings('div.tilteotherdesc').addClass('hidden');
                    $(this).parents('div').siblings('div.tilteotherdesc').val('');
                }   
            });

            $(document).on('focus', '.TitleDOB', function(){
                this.oldValue = this.value;   
            });

            $(document).on('blur', '#TitleOnylBorrowerTable input.TitleZip', function(event){
                var parent = $(this).parents('li');
                smartZipcode(
                    this,
                    parent.find('input.TitleCity').get(0),
                    parent.find('select.TitleState').get(0),
                    null,
                    event);
            });
            $('a.AddTitleOnlyBorrower').click(function(){
                numTitleOnlyBorrowerClicked++;

                var parent = $('#TitleOnylBorrowerTable');
                var cloneRow =  $('#TitleRowTempl').clone().removeClass('hidden');
                cloneRow.removeAttr('id');

                // To clone the calendar
                var createdCalendar = cloneRow.find('.TitleDOB');
                var newId = $(createdCalendar).prop('id')+numTitleOnlyBorrowerClicked;
                $(createdCalendar).prop('id', newId);
                $(createdCalendar).siblings('a').attr('onclick', "return displayCalendar('"+newId+"')");
                _initMask(createdCalendar.get(0));
                _initDynamicInput(createdCalendar.get(0));

                parent.append(cloneRow);
                _initMask(cloneRow.find('input.TitleSSN').get(0));
                _initMask(cloneRow.find('input.TitleHPhone').get(0));
                _initMask(cloneRow.find('input.TitleZip').get(0));

                var id = getId();
                cloneRow.attr('data-title-only-borrower-id', id).find('input.TitleId').val(id);
                cloneRow.find('input[id$=TitleAliases]').val(JSON.stringify([]));
                updateDirtyBit();
                
                return false;
            });
            $('#TitleOnylBorrowerTable').on('click', 'a.RemoveTitleRow', function(){
                $(this).parents('li').remove();
                updateDirtyBit();
                return false;
            });
            
            function SetRemoveState(ul)
            {
                if($(ul).find('.Remove').length == 1)
                {
                    $(ul).find('.Remove').attr('disabled', 'disabled').addClass('disabled').data('disabled', true);
                }
                else
                {
                    $(ul).find('.Remove').removeAttr('disabled').removeClass('disabled').data('disabled', false);
                }
            }
            
            function CloneEmptyFirstElement(list)
            {
                var elem = $(list).children('li:first').clone();
                elem.appendTo(list);
                $(elem).find('input').each(function(){
                    $(this).find('option').filter(':selected').removeAttr('selected');
                    $(this).find('option').first().attr('selected', 'selected');
                    $(this).val('');
                    $(this).removeAttr('checked');
                    if($(this).is('.mask')) _initMask(this);
                });
                $(elem).find('textarea').each(function(){
                    $(this).val('');
                });
            }
            
            $('select.TitleRelationshipTitleT').change();
            
        });

        function doAfterDateFormat(e)
        {
            if(e.id === "sPrelimRprtOd" ||
                e.id === "sPrelimRprtDueD" ||
                e.id === "sPrelimRprtDocumentD" ||
                e.id === "sPrelimRprtRd")
            {
                refreshCalculation();
            }
        }
        
    </script>
    
    
    <%--
    Chrome does NOT like being told to disable autofill. This attribute value was suggested by MDN
    and must be applied at both the form and the input level.
    https://developer.mozilla.org/en-US/docs/Web/Security/Securing_your_site/Turning_off_form_autocompletion#Disabling_autocompletion
    --%>
    <form id="form1" runat="server" autocomplete="nope">
    <table cellspacing="0" cellpadding="0" class="w-680">
        <tr>
            <td class="MainRightHeader" nowrap>
                Title And Vesting
            </td>
        </tr>
        <tr>
            <td>
                <table class="InsetBorder FieldLabel" cellspacing="0" cellpadding="0" width="99%"
                    border="0">
                    <tr>
                        <td colspan="6" class="FormTableSubHeader">
                            Title Information
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            Ordered
                        </td>
                        <td>
                            Due
                        </td>
                        <td>
                            Document Date
                        </td>
                        <td>
                            Received
                        </td>
                        <td>
                            Comments
                        </td>
                    </tr>
                    <tr>
                        <td class="TitleWidth">
                            Preliminary Title Report
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPrelimRprtOd" runat="server" Width="70" preset="date" CssClass="mask" onchange="date_onblur(null, this);"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPrelimRprtDueD" runat="server" Width="70" preset="date" CssClass="mask" onchange="date_onblur(null, this);"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPrelimRprtDocumentD" runat="server" Width="70" preset="date" CssClass="mask" onchange="date_onblur(null, this);"></ml:DateTextBox>
                        </td>
                        <td>
                            <ml:DateTextBox ID="sPrelimRprtRd" runat="server" Width="70" preset="date" CssClass="mask" onchange="date_onblur(null, this);"></ml:DateTextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="sPrelimRprtN" Width="70" runat="server" onchange="refreshCalculation();"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="TitleWidth">
                            Assessors Parcel ID
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="sAssessorsParcelId" CssClass="FullWidth" runat="server" onchange="refreshCalculation();"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="TitleWidth">
                            Property Tax Status Message
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="sPropertyTaxMessageDescription" CssClass="FullWidth" runat="server"
                                onchange="refreshCalculation();"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="TitleWidth">
                            Required Endorsements
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="sRequiredEndorsements" CssClass="FullWidth" runat="server" TextMode="MultiLine"
                                Rows="3" onchange="refreshCalculation();"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="TitleWidth">
                            Approved Items
                        </td>
                        <td colspan="5">
                            <asp:TextBox ID="sTitleReportItemsDescription" CssClass="FullWidth" runat="server"
                                TextMode="MultiLine" Rows="3" onchange="refreshCalculation();"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table class="InsetBorder FieldLabel" cellspacing="0" cellpadding="0" width="99%"
                    border="0">
                    <tr>
                        <td colspan="3" class="FormTableSubHeader">
                            Vesting Information
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="2">
                            Relationship Title Type
                        </td>
                    </tr>
                    <asp:Repeater runat="server" ID="Relationships" OnItemDataBound="Relationships_OnItemDataBound">
                        <ItemTemplate>
                            <tr>
                                <td class="TitleWidth">
                                    <ml:EncodedLabel ID="aTitleNm1" runat="server"></ml:EncodedLabel>
                                </td>
                                <td>
                                    <asp:DropDownList ID="aBRelationshipTitleT" CssClass="FullWidth" runat="server" onchange="refreshCalculation()">
                                    </asp:DropDownList>
                                    <ml:EncodedLabel ID="aBTypeT_isTitle" runat="server">Non-Title Spouse</ml:EncodedLabel>
                                    <a ID="editBAliases" runat="server">Edit Aliases/POA</a>
                                </td>
                                <td>
                                    <asp:TextBox ID="aBRelationshipTitleOtherDesc" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="coborrowerRow" runat="server">
                                <td class="TitleWidth">
                                    <ml:EncodedLabel ID="aTitleNm2" runat="server"></ml:EncodedLabel>
                                </td>
                                <td>
                                    <asp:DropDownList ID="aCRelationshipTitleT" CssClass="FullWidth" runat="server" onchange="refreshCalculation()">
                                    </asp:DropDownList>
                                    <ml:EncodedLabel ID="aCTypeT_isTitle" runat="server">Non-Title Spouse</ml:EncodedLabel>
                                    <a ID="editCAliases" runat="server">Edit Aliases/POA</a>
                                </td>
                                <td>
                                    <asp:TextBox ID="aCRelationshipTitleOtherDesc" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <tr>
                                <td colspan="3">
                                    <hr />
                                </td>
                            </tr>
                        </SeparatorTemplate>
                    </asp:Repeater>
                    <tr>
                        <td class="TitleWidth">
                            Final Relation
                        </td>
                        <td>
                            <ml:ComboBox ID="aManner" runat="server" CssClass="FullWidth" onchange="refreshCalculation();"></ml:ComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="TitleWidth">
                            Vesting to Read
                        </td>
                        <td>
                            <label id="Lock">
                                <input type="checkbox" runat="server" id="sVestingToReadLckd" onclick="refreshCalculation();"/>Locked</label>
                            <asp:TextBox ID="sVestingToRead" runat="server" CssClass="MostlyFull" TextMode="MultiLine"
                                Rows="4" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="TrustInformation" class="FullWidth">
        <input type="hidden" runat="server" id="TrustInfoHolder" />
        <div class="FormTableSubHeader FullWidth" >
            Trust Information</div>
        <div class="FieldItem">
            <span class="TitleWidth Left">Trust Name </span>
            <input type="text" id="sTrustName" class="FullWidth" runat="server"/>
        </div>
        <div class="Clear"></div>
        <div class="FieldItem">
            <span class="TitleWidth Left">For the Benefit of </span>
            <input type="text" id="sTrustForBenefitOf" class="FullWidth" runat="server"/>
        </div>
        <div class="Clear"></div>
        <div class="FieldItem">
            <span class="TitleWidth Left">Date of Trust Agreement </span>
            <ml:DateTextBox ID="sTrustAgreementD" runat="server" Width="75" preset="date" CssClass="mask"></ml:DateTextBox>
        </div>
        <div class="Clear"></div>
        <div class="FieldItem">
            <span class="TitleWidth Left">Trust State </span>
            <ml:StateDropDownList ID="sTrustState" runat="server"></ml:StateDropDownList>
        </div>
        <div class="Clear"></div>
        <div class="FieldItem">
            <span class="TitleWidth Left">Trust ID </span>
            <input type="text" id="sTrustId" class="FullWidth" runat="server" />
        </div>
        <div class="Clear ulad-data"></div>
        <div class="FieldItem ulad-data">
            <span class="TitleWidth Left">Trust Classification Type </span>
            <asp:DropDownList runat="server" ID="sTrustClassificationT"></asp:DropDownList>
        </div>
        <div class="Clear"></div>
        <div id="TrustorsSettlors">
            <span class="Title">Name of Trustor(s)/Settlor(s) </span>
            <ul id="TrustorList" class="Cloneable">
                    <asp:Repeater ID="Trustors" runat="server">
                    <ItemTemplate>
                    <li>
                        <input type="text" value="<%#AspxTools.HtmlString(((TrustCollection.Trustor)Container.DataItem).Name)%>" />
                        <a class="Action Remove">remove</a>
                    </li>
                    </ItemTemplate>
                    </asp:Repeater>
            </ul>
            <span class="Action CloneSibling">add Trustor/Settlor</span>
        </div>
        <div id="CurrentTrustees">
            <span class="Title">Current Trustees </span>
            <ul id="TrusteeList" class="Cloneable">
                <asp:Repeater ID="Trustees" runat="server">
                <ItemTemplate>
                <li>
                    <div class="FieldItem">
                        <span class="TitleWidth Left">Name</span>
                        <input class="FullWidth Name" type="text" value="<%#AspxTools.HtmlString(((TrustCollection.Trustee)Container.DataItem).Name)%>" />
                    </div>
                    <div class="Clear"></div>
                    <div class="AddressParts">
                        <div class="FieldItem">
                            <span class="TitleWidth Left">Address</span>
                            <input class="FullWidth StreetAddress" type="text" autocomplete="nope" value="<%#AspxTools.HtmlString(((TrustCollection.Trustee)Container.DataItem).Address.StreetAddress)%>"/>
                        </div>
                        <div class="FieldItem">
                            <span class="TitleWidth Left">&nbsp;</span>
                            <input type="text" class="City" autocomplete="nope" value="<%#AspxTools.HtmlString(((TrustCollection.Trustee)Container.DataItem).Address.City)%>" />
                            <ml:StateDropDownList runat="server" autocomplete="nope" class="State" ID="State" /><%//This gets set in the codebehind %>
                            <input type="text" class="ZipCode mask" autocomplete="nope" preset="zipcode" value="<%#AspxTools.HtmlString(((TrustCollection.Trustee)Container.DataItem).Address.PostalCode)%>" />
                        </div>
                    </div>
                    <div class="Clear"></div>
                    <div class="FieldItem">
                        <span class="TitleWidth Left">Phone</span>
                        <input type="text" class="Phone mask" preset="phone" value="<%#AspxTools.HtmlString(((TrustCollection.Trustee)Container.DataItem).Phone)%>" />
                    </div>
                    <div class="Clear"></div>
                    <div class="FieldItem">
                        <span class="TitleWidth Left">Signature Verbiage</span>
                        <asp:TextBox ID="SignatureVerbiage" runat="server" CssClass="MostlyFull SignatureVerbiage" TextMode="MultiLine" Rows="4"></asp:TextBox>
                    </div>
                    <div class="Clear"></div>
                    <div class="FieldItem">
                        <input type="checkbox" class="CanSignCertOfTrust" id="SignCertOfTrust" runat="server"/> <%//These two too%>
                        Signing Certification of Trust
                        <input type="checkbox" class="CanSignLoanDocs" id="SignLoanDocs" runat="server"/>
                        Signing Loan Documents <a class="Action Remove">remove</a>
                    </div>
                </li>
                </ItemTemplate>
                </asp:Repeater>
            </ul>
            <span class="Action CloneSibling">add Trustee</span>
        </div>
    </div>
        <div id="TitleBorrowers" class="FullWidth">
            <input type="hidden" runat="server" id="TitleOnlyBorrowerJSON" />
        
             <div class="FormTableSubHeader" class="FullWidth">Non-Obligate Borrowers</div>
             
            <asp:Repeater runat="server" ID="TitleOnlyBorrowers" OnItemDataBound="TitleOnlyBorrowers_OnItemDataBound">
                <ItemTemplate>
                    <li data-title-only-borrower-id=<%# AspxTools.HtmlAttribute( ((TitleBorrower)Container.DataItem).Id.ToString()) %>>
                        <input type="hidden" class="TitleId" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).Id) %>" />
                    <div class="FieldItem">
                            <span class="TitleWidth Left">First Name</span>
                            <input type="text" class="TitleFirstNm FullWidth" onchange="refreshCalculation();" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).FirstNm) %>" />
                        </div>
                        <div class="Clear"></div>                               
                    <div class="FieldItem">
                            <span class="TitleWidth Left">Middle Name</span>
                            <input type="text" class="TitleMidNm" onchange="refreshCalculation();" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).MidNm) %>" />
                        </div>
                        <div class="Clear"></div>   
                    <div class="FieldItem">
                            <span class="TitleWidth Left">Last Name</span>
                            <input type="text" class="TitleLastNm FullWidth" onchange="refreshCalculation();"  value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).LastNm) %>" />
                            <a href="#" class="EditAliasPOAClick">Edit Aliases/POA</a>
                            <input type="hidden" class="TitlePOA" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).POA) %>" />
                            <asp:HiddenField ID="TitleAliases" runat="server" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).Aliases) %>" />
                        </div>
                        <div class="Clear"></div>   
                    <div class="FieldItem">
                            <span class="TitleWidth Left">SSN</span>
                            <input type="text" class="TitleSSN mask"  preset="ssn" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).SSN) %>" />
                        </div>
                        <div class="Clear"></div>   
                    <div class="FieldItem">
                            <span class="TitleWidth Left">Date of Birth</span>
                            <ml:DateTextBox runat="server" ID="TitleDob" class="TitleDOB mask" preset="date" Width="75"></ml:DateTextBox>
                        </div>
                        <div class="Clear"></div>   
                    <div class="FieldItem">
                            <span class="TitleWidth Left">Home Phone</span>
                            <input type="text" class="TitleHPhone mask" preset="phone" width="120" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).HPhone) %>" />
                        </div>
                        <div class="Clear"></div>   
                    <div class="FieldItem">
                            <span class="TitleWidth Left">Email</span>
                            <input type="text" class="TitleEmail FullWidth" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).Email) %>" />
                            <span class="email-error">Invalid Email</span>
                       </div>
                       <div class="Clear"></div>   
                       <div class="AddressParts">
                        <div class="FieldItem">
                                <span class="TitleWidth Left">Address</span>
                                <input type="text" class="TitleAddress FullWidth StreetAddress" autocomplete="nope" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).Address) %>" />
                            </div>
                        <div class="FieldItem">
                                <span class="TitleWidth Left">&nbsp;</span>
                                <input type="text" class="TitleCity City" autocomplete="nope" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).City) %>" />
                                <ml:StateDropDownList ID="StateDropDownList1" autocomplete="nope" Value='<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).State) %>' CssClass="TitleState" runat="server"></ml:StateDropDownList>
                                <input type="text" class="TitleZip  mask ZipCode" autocomplete="nope" preset="zipcode" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).Zip) %>" />
                            </div>
                       </div>
                       <div class="Clear"></div>   
                   <div class="FieldItem">
                            <span class="TitleWidth Left">Title Type</span>
                            <asp:DropDownList runat="server" ID="TitleRelationshipTitleT" CssClass="TitleRelationshipTitleT FullWidth"></asp:DropDownList>
                       </div>
                       <div class="Clear"></div>   
                       <div class="hidden tilteotherdesc">
                        
                            <span class="TitleWidth Left">Title Other Desc</span>
                            <input type="text" class="TitleRelationshipTitleTOtherDesc" value="<%# AspxTools.HtmlString( ((TitleBorrower)Container.DataItem).RelationshipTitleTOtherDesc) %>" />
                       </div>
                       <div class="Clear"></div>   
                       <div>
                           <span class="TitleWidth Left">Associated Application</span>
                           <asp:DropDownList runat="server" ID="TitleAssociatedApplicationId" CssClass="TitleAssociatedApplicationId FullWidth"></asp:DropDownList>
                       </div>
                       <div class="Clear"></div>   
                       <div class="ulad-data">
                           <span class="TitleWidth Left">Currently Holds Title</span>
                           <input type="checkbox" class="TitleCurrentlyHoldsTitle" <%# AspxTools.HtmlString(((TitleBorrower)Container.DataItem).CurrentlyHoldsTitle ? "checked" : string.Empty) %> />
                       </div>
                       <div class="Clear ulad-data"></div>   
                       <div class="ulad-data">
                           <span class="TitleWidth Left">Name on Current Title</span>
                           <input type="text" class="TitleNameOnCurrentTitle FullWidth" maxlength="128" value=<%# AspxTools.HtmlAttribute(((TitleBorrower)Container.DataItem).NameOnCurrentTitle) %> />
                           <input type="checkbox" class="TitleNameOnCurrentTitleLckd title-borrower-lock" <%# AspxTools.HtmlString(((TitleBorrower)Container.DataItem).NameOnCurrentTitleLckd ? "checked" : string.Empty) %> />
                       </div>
                       <div class="Clear ulad-data"></div>   
                       <div class="ulad-data">
                           <span class="TitleWidth Left">Will Hold Title</span>
                           <input type="checkbox" class="TitleWillHoldTitle" <%# AspxTools.HtmlString(((TitleBorrower)Container.DataItem).WillHoldTitle ? "checked" : string.Empty) %> />
                       </div>
                       <div class="Clear ulad-data"></div>   
                       <div class="ulad-data">
                           <span class="TitleWidth Left">Title will be Held in What Name</span>
                           <input type="text" class="TitleWillBeHeldInWhatName FullWidth" maxlength="128" value=<%# AspxTools.HtmlAttribute(((TitleBorrower)Container.DataItem).TitleWillBeHeldInWhatName) %> />
                           <input type="checkbox" class="TitleWillBeHeldInWhatNameLckd title-borrower-lock" <%# AspxTools.HtmlString(((TitleBorrower)Container.DataItem).TitleWillBeHeldInWhatNameLckd ? "checked" : string.Empty) %> />
                       </div>
                        <div class="Clear ulad-data"></div>   
                        <a href="#" class="RemoveTitleRow" >remove</a>
                    </li>                   
                </ItemTemplate>
                <HeaderTemplate>
                    <ul id="TitleOnylBorrowerTable">
                        <li  class="hidden " id="TitleRowTempl">                       
                             <input type="hidden" class="TitleId"  />
                        
                            <div>  
                                <span class="TitleWidth Left">First Name</span>
                                <input type="text" class="TitleFirstNm FullWidth" onchange="refreshCalculation();"  />
                            </div>
                            <div class="Clear"></div>   
                            <div>  
                                <span class="TitleWidth Left">Middle Name</span>
                                <input type="text" class="TitleMidNm" onchange="refreshCalculation();"  />
                            </div>
                            <div class="Clear"></div>   
                            <div>
                                <span class="TitleWidth Left">Last Name</span>
                                <input type="text" class="TitleLastNm FullWidth" onchange="refreshCalculation();" />
                                <a href="#" class="EditAliasPOAClick">Edit Aliases/POA</a>
                                <input type="hidden" class="TitlePOA" />
                                <asp:HiddenField ID="TitleAliases" runat="server" />
                            </div>
                            <div class="Clear"></div>   
                            <div>
                                <span class="TitleWidth Left">SSN</span>
                                <input type="text" class="TitleSSN mask"  preset="ssn" />
                            </div>
                            <div class="Clear"></div>   
                            <div>
                                <span class="TitleWidth Left">Date of Birth</span>
                                <ml:DateTextBox runat="server" ID="TitleDob" class="TitleDOB mask" preset="date" Width="75"></ml:DateTextBox>
                            </div>
                            <div class="Clear"></div>   
                            <div>
                                <span class="TitleWidth Left">Home Phone</span>
                                <ml:PhoneTextBox runat="server" class="TitleHPhone mask" preset="phone" Width="120"></ml:PhoneTextBox>
                            </div>
                            <div class="Clear"></div>   
                            <div>
                                <span class="TitleWidth Left">Email</span>
                                <input type="text" class="TitleEmail FullWidth" />
                                <span class="email-error">Invalid Email</span>
                           </div>
                           <div class="Clear"></div>   
                           <div class="AddressParts">
                                <div>
                                    <span class="TitleWidth Left">Address</span>
                                    <input type="text" class="TitleAddress FullWidth StreetAddress" autocomplete="nope"  />
                                </div>
                                <div>
                                    <span class="TitleWidth Left">&nbsp;</span>
                                    <input type="text" class="TitleCity City" autocomplete="nope" />
                                    <ml:StateDropDownList ID="StateDropDownList1" autocomplete="nope"  CssClass="TitleState" runat="server"></ml:StateDropDownList>
                                    <input type="text" class="TitleZip  mask ZipCode" autocomplete="nope" preset="zipcode"  />
                                </div>
                           </div>
                           <div class="Clear"></div>   
                           <div>
                                <span class="TitleWidth Left">Title Type</span>
                                <asp:DropDownList runat="server" ID="TitleRelationshipTitleT" CssClass="TitleRelationshipTitleT FullWidth"></asp:DropDownList>
                           </div>
                           <div class="Clear"></div>   
                           <div class="hidden tilteotherdesc">
                        
                                <span class="TitleWidth Left">Title Other Desc</span>
                                <input type="text" class="TitleRelationshipTitleTOtherDesc" />
                            </div>
                            <div class="Clear"></div>   
                           <div>
                               <span class="TitleWidth Left">Associated Application</span>
                               <asp:DropDownList runat="server" ID="TitleAssociatedApplicationId" CssClass="TitleAssociatedApplicationId FullWidth"></asp:DropDownList>
                           </div>
                            <div class="Clear ulad-data"></div>   
                           <div class="ulad-data">
                               <span class="TitleWidth Left">Currently Holds Title</span>
                               <input type="checkbox" class="TitleCurrentlyHoldsTitle" />
                           </div>
                            <div class="Clear ulad-data"></div>   
                           <div class="ulad-data">
                               <span class="TitleWidth Left">Name on Current Title</span>
                               <input type="text" class="TitleNameOnCurrentTitle FullWidth" maxlength="128" />
                               <input type="checkbox" class="TitleNameOnCurrentTitleLckd title-borrower-lock" />
                           </div>
                            <div class="Clear ulad-data"></div>   
                           <div class="ulad-data">
                               <span class="TitleWidth Left">Will Hold Title</span>
                               <input type="checkbox" class="TitleWillHoldTitle" />
                           </div>
                            <div class="Clear ulad-data"></div>   
                           <div class="ulad-data">
                               <span class="TitleWidth Left">Title will be Held in What Name</span>
                               <input type="text" class="TitleWillBeHeldInWhatName FullWidth" maxlength="128" />
                               <input type="checkbox" class="TitleWillBeHeldInWhatNameLckd title-borrower-lock" />
                           </div>
                           <div class="Clear"></div>   
                           <a href="#" class="RemoveTitleRow" >remove</a>       
                        </li>    
                </HeaderTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>
             <a href="#" class="AddTitleOnlyBorrower" >add Non-Obligate Borrower</a>
        </div>
    </form>
</body>
</html>
