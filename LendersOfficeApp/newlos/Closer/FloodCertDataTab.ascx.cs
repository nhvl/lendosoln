﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class FloodCertDataTab : BaseLoanUserControl, IAutoLoadUserControl
    {
        public void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FloodCertDataTab));
            dataLoan.InitLoad();

            sFloodHazardCommunityDesc.Text = dataLoan.sFloodHazardCommunityDesc;
            sSpCounty.Text = dataLoan.sSpCounty;
            sSpState.Text = dataLoan.sSpState;

            sFloodCertificationCommunityNum.Text = dataLoan.sFloodCertificationCommunityNum;

            sFloodCertificationMapNum.Text = dataLoan.sFloodCertificationMapNum;
            sFloodCertificationPanelNums.Text = dataLoan.sFloodCertificationPanelNums;
            sFloodCertificationPanelSuffix.Text = dataLoan.sFloodCertificationPanelSuffix;
            sFloodCertificationMapD.Text = dataLoan.sFloodCertificationMapD_rep;
            sFloodCertificationLOMChangedD.Text = dataLoan.sFloodCertificationLOMChangedD_rep;
            sNfipFloodZoneId.Text = dataLoan.sNfipFloodZoneId;
            sFloodCertificationIsNoMap.Checked = dataLoan.sFloodCertificationIsNoMap;
            

            // Todo: Use sFloodHazardFedInsAvail and sFloodHazardFedInsNotAvail
            if ((dataLoan.sFloodCertificationParticipationStatus == "N") || (dataLoan.sFloodCertificationParticipationStatus == "S"))
            {
                sIsAvailable_0.Checked = false;
                sIsAvailable_1.Checked = true;
                sSuspended.Checked = (dataLoan.sFloodCertificationParticipationStatus == "S");
            }
            else
            {
                sIsAvailable_0.Checked = true;
                sIsAvailable_1.Checked = false;
                sEmergencyProgramOfNFIP.Checked = (dataLoan.sFloodCertificationParticipationStatus == "E");
                sRegularProgram.Checked = (dataLoan.sFloodCertificationParticipationStatus == "R");
                sProbation.Checked = (dataLoan.sFloodCertificationParticipationStatus == "P");
            }
            sFloodCertificationDesignationD.Text = dataLoan.sFloodCertificationDesignationD_rep;
            sFloodCertificationIsCBRAorOPA.Checked = dataLoan.sFloodCertificationIsCBRAorOPA;

            sFloodCertificationIsInSpecialArea.SelectedValue = dataLoan.sFloodCertificationIsInSpecialArea.ToString();

            Tools.SetDropDownListValue(sFloodCertificationPreparerT, dataLoan.sFloodCertificationPreparerT);
            if (dataLoan.sFloodCertificationPreparerT == E_FloodCertificationPreparerT.Other)
                sFloodCertificationPreparerOtherName.Text = dataLoan.sFloodCertificationPreparerOtherName;

            sFloodCertificationDeterminationD.Text = dataLoan.sFloodCertificationDeterminationD_rep;

            if (dataLoan.sFloodCertificationDeterminationD != null && dataLoan.sFloodCertificationDeterminationD.IsValid)
            {
                DateTime dt = dataLoan.sFloodCertificationDeterminationD.DateTimeForComputationWithTime;
                sHourOfDetermination.Text = "" + (dt.Hour % 12 + (dt.Hour % 12 == 0 ? 12 : 0));
                sMinuteOfDetermination.Text = dt.Minute.ToString("00");
                sAmPm.SelectedValue = (dt.Hour < 12 ? "AM" : "PM");
            }

            Tools.SetDropDownListValue(sFloodCertificationDeterminationDTimeZoneT, dataLoan.sFloodCertificationDeterminationDTimeZoneT);
            sFloodCertId.Text = dataLoan.sFloodCertId;
            sFloodCertificationIsLOLUpgraded.SelectedValue = dataLoan.sFloodCertificationIsLOLUpgraded.ToString();
            sFloodCertificationLOLUpgradeD.Text = dataLoan.sFloodCertificationLOLUpgradeD_rep;

            sFloodCertificationCancellationReceivedD.Text = dataLoan.sFloodCertificationCancellationReceivedD_rep;
            sFloodCertificationCancellationOrderedD.Text = dataLoan.sFloodCertificationCancellationOrderedD_rep;

            Tools.SetDropDownListValue(sLomaLomrT, dataLoan.sLomaLomrT);
            sNfipIsPartialZone.Checked = dataLoan.sNfipIsPartialZone;
            sNfipMappingCompany.Text = dataLoan.sNfipMappingCompany;
            sFfiaDateCommunityEnteredProgramD.Text = dataLoan.sFfiaDateCommunityEnteredProgramD_rep;
        }

        public void SaveData()
        {
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            Tools.Bind_sTimeZoneT(sFloodCertificationDeterminationDTimeZoneT);
            Tools.Bind_sFloodCertificationPreparerT(sFloodCertificationPreparerT);
            Tools.Bind_sLomaLomrT(sLomaLomrT);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}