﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Funding.aspx.cs" Inherits="LendersOfficeApp.newlos.Closer.Funding" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>Funding</title>
    <style type="text/css">
        .BorderTop{border-top:thin groove;}
        .BorderLeft{border-left:thin groove;}
        .BorderRight{border-right:thin groove;}
        .BorderBottom{border-bottom:thin groove;}
        .InvalidTextareaLength { color: Red;  font-weight: bold; }
        .counter { font-weight: normal; font-size: 10px; }
        .Col { float: left;}
        .Wide { width: 270px;}
        
        .insetLabel { padding-left: 5px; width: 90px; }
        .insetLabel2 { padding-left: 5px; width: 100px; }
        .insetLabel3 { padding-left: 5px; width: 40px; }
        .padBottom { padding-bottom: .15em; }
        #sFundingBranchNameLabel {  padding-left: 5px; width: 90px; }
        #sFundingCompanyAddr { width: 230px; }
        .phone { width: 120px; }
        .right-margin-50 { margin-right: 50px; }
    </style>
    <script type="text/javascript" src="../../inc/utilities.js" ></script>
</head>
<body bgcolor="gainsboro">
    <script language="javascript">
        var initRanFirstTime = false;
        
        function Page_ClientValidate() {
            var obj = document.getElementById('<%= AspxTools.ClientId(sFundNotes) %>');
            if( obj ) {
                var count = parseInt( obj.getAttribute('maxcharactercount') );
                var val = obj.value.length - count; 
                if( obj.value.length > count ) {
                    var fail = confirm("Your Funding notes are too long. You will not be able to save unless they are shortened by "+ val +" character(s). \n\r\n\rWould you like to automaticaly shorten?"); 
                    if( fail ) {
                        shortenTextArea(obj);
                        return true;
                    } 
                    return false; 
                }
            }
            return true;
        }
        // Function for attaching change events with some basic
        // cross-browser support. If additional functionality is 
        // required in the future, should probably switch to using 
        // jQuery.
        function attachChangeEvent(element, handler) {
                addEventHandler(element, 'change', handler, false);
        }
        
        function attachClickEvent(element, handler) {
                addEventHandler(element, 'click', handler, false);
        }
        
        function _init() {
            if( !initRanFirstTime ) {
                TextareaUtilities.LengthCheckInit();
                initRanFirstTime = true;
                
                var rbUseOfficialContact = <%=AspxTools.JsGetElementById(CFM.FindControl("m_rbUseOfficialContact"))%>;
                attachClickEvent(rbUseOfficialContact, f_setLoanProceedsToVisibility);
                
                var rbManualOverride = <%=AspxTools.JsGetElementById(CFM.FindControl("m_rbManualOverride"))%>;
                attachClickEvent(rbManualOverride, f_setLoanProceedsToVisibility);

                var officialContactListDDL = <%=AspxTools.JsGetElementById(CFM.FindControl("m_officialContactList"))%>;
                attachChangeEvent(officialContactListDDL, f_setLoanProceedsToVisibility);
                
                f_setLoanProceedsToVisibility();
                
                var advanceClassificationDDL = document.getElementById('sWarehouseAdvanceClassification');
                attachChangeEvent(advanceClassificationDDL, f_onchange_sWarehouseAdvanceClassification);
                f_onchange_sWarehouseLenderRolodexId(true);
            }
            if(document.getElementById('_ReadOnly').value != 'True')
                lockField(<%=AspxTools.JsGetElementById(sAmtFundFromWarehouseLineLckd)%>, 'sAmtFundFromWarehouseLine');

            lockField(<%=AspxTools.JsGetElementById(sConsummationDLckd) %>, 'sConsummationD');
        }
        
        function _postRefreshCalculation(results) {
            var cb = document.getElementById('sChkDueFromClosingRcvd');
            cb.disabled = results['sChkDueFromClosingIsZero'] == 'True';
            cb.parentElement.disabled = cb.disabled;
        }
  
        function shortenTextArea(obj) 
        {
            
            obj.value = obj.value.substring(0, obj.getAttribute('maxcharactercount')); 
            obj.onkeyup(); 
        }
        
        function f_onchange_sWarehouseLenderRolodexId( bIsInit ) {
            var warehouseLenderId = $('#sWarehouseLenderRolodexId').val();
            
            if(!bIsInit)
            {
                // Clear out text box to prevent lender name from being saved to lender other description
                $('#sWarehouseFunderDesc').val('');
            }
            
            if(warehouseLenderId == "-2") //Other
            {
                document.getElementById('WarehouseLenderAdvanceClassificationPanel').style.display = 'none';
                document.getElementById('WarehouseLenderOtherDescPanel').style.display = '';
            }
            else
            {
                document.getElementById('WarehouseLenderAdvanceClassificationPanel').style.display = '';
                document.getElementById('WarehouseLenderOtherDescPanel').style.display = 'none';
                
                if(!bIsInit)
                {
                    //Load Advance Classifications for Warehouse Lender
                    var args = {
                        warehouseLenderId: warehouseLenderId
                    };
                    var results = gService.loanedit.call('LoadAdvanceClassifications', args);
                    var advanceClassificationItems = JSON.parse(results.value.AdvanceClassifications);    
                    $('#sWarehouseAdvanceClassification').empty().append("<option></option>");
                    $(advanceClassificationItems).each(function(index, item) {
                        var value = item.Description + " - " + item.MaxFundingPercent_rep;
                        $('#sWarehouseAdvanceClassification').append(
                            $("<option></option>").val(value).text(value)
                        );
                    });
                }
            }
        }
        
        function f_onchange_sWarehouseAdvanceClassification(event) {
            var input = retrieveEventTarget(event);
            var regex = /\d+(\.\d+)?%(\s+)?$/
            var arr = regex.exec(input.value);
            if (arr != null) {
                var maxFundingPc = document.getElementById('sWarehouseMaxFundPc');
                maxFundingPc.value = arr[0];
                maxFundingPc.onchange();
            }
        }
        
        var TITLE_COMPANY_AGENT_TYPES = <%=AspxTools.JsArray(E_AgentRoleT.Title)%>;
        var CLOSING_COMPANY_AGENT_TYPES = <%=AspxTools.JsArray(E_AgentRoleT.Escrow, E_AgentRoleT.ClosingAgent)%>;
        
        function f_setLoanProceedsToVisibility()
        {
            var $trLoanProceedsTo = $('#trLoanProceedsTo');
            
            // Always show if "Set Manually" is selected
            var rbManualOverride = <%=AspxTools.JsGetElementById(CFM.FindControl("m_rbManualOverride"))%>;
            if ( rbManualOverride.checked )
            {
                $trLoanProceedsTo.show();
                return;
            }
            
            // Otherwise, show only if Agent type does not map to Loan Proceeds To type
            var loanProceedsToDDL = document.getElementById('sLoanProceedsToT');
            var officialContactListDDL = <%=AspxTools.JsGetElementById(CFM.FindControl("m_officialContactList"))%>;
            if( $.inArray(officialContactListDDL.value, TITLE_COMPANY_AGENT_TYPES) != -1 )
            {
                loanProceedsToDDL.value = <%=AspxTools.JsString(E_LoanProceedsToT.TitleCompany)%>;
                $trLoanProceedsTo.hide();
            }
            else if( $.inArray(officialContactListDDL.value, CLOSING_COMPANY_AGENT_TYPES) != -1 )
            {
                loanProceedsToDDL.value = <%=AspxTools.JsString(E_LoanProceedsToT.ClosingCompany)%>;
                $trLoanProceedsTo.hide();
            }
            else
            {
                $trLoanProceedsTo.show();
            }
        }
        
    </script>
    <form id="FundingCloser" runat="server">
		<table cellSpacing="0" cellPadding="0">
			<tr>
				<td class="MainRightHeader" noWrap>
				    Funding
				</td>
			</tr>
			<tr>
				<td style="padding:4px">
					<table cellpadding="2" cellspacing="0" class="FieldLabel Col right-margin-50">
					    <tr>
					        <td class="FieldLabel">
                                <ml:EncodedLabel ID="Label1" Text="Warehouse lender" AssociatedControlID="sWarehouseLenderRolodexId" runat="server"></ml:EncodedLabel>
					        </td>
					        <td colspan="3" align="right">
					            <asp:DropDownList runat="server" ID="sWarehouseLenderRolodexId" onchange="f_onchange_sWarehouseLenderRolodexId(false);"></asp:DropDownList>
					        </td>
					    </tr>
					    <tr id="WarehouseLenderAdvanceClassificationPanel">
					        <td class="FieldLabel">
                                <ml:EncodedLabel ID="Label6" Text="Advance classification" AssociatedControlID="sWarehouseAdvanceClassification" runat="server"></ml:EncodedLabel>
					        </td>
					        <td colspan="3" align="right">
					            <asp:DropDownList runat="server" ID="sWarehouseAdvanceClassification"></asp:DropDownList>
					        </td>
					    </tr>
					    <tr id="WarehouseLenderOtherDescPanel">
					        <td colspan="2" class="FieldLabel">
                                <ml:EncodedLabel ID="Label15" Text="Warehouse lender other desc" AssociatedControlID="sWarehouseFunderDesc" runat="server"></ml:EncodedLabel>
					        </td>
					        <td colspan="2" align="right">
					            <asp:TextBox runat="server" ID="sWarehouseFunderDesc"></asp:TextBox>
					        </td>
					    </tr>
					    <tr>
					        <td class="FieldLabel">
					            <ml:EncodedLabel ID="DisbursementLabel" Text="Disbursement Method" runat="server" AssociatedControlID="sFundingDisbursementMethodT"></ml:EncodedLabel>
					        </td>
					        <td colspan="3" align="right">
					            <asp:DropDownList ID="sFundingDisbursementMethodT" runat="server"></asp:DropDownList>
					        </td>
					    </tr>
					    <tr>
					        <td colspan="3">
                                <ml:EncodedLabel ID="Label3" Text="Warehouse line max funding %" AssociatedControlID="sWarehouseMaxFundPc" runat="server"></ml:EncodedLabel>
					        </td>
					        <td>
						        <ml:PercentTextBox ID="sWarehouseMaxFundPc" onchange="refreshCalculation();" Width="90px" runat="server" />
					        </td>
					    </tr>
					    <tr>
					        <td colspan="3">
                                <ml:EncodedLabel ID="Label5" Text="Warehouse line max funding amount" AssociatedControlID="sWarehouseMaxFundAmt" runat="server"></ml:EncodedLabel>
					        </td>
					        <td>
						        <ml:MoneyTextBox ID="sWarehouseMaxFundAmt" ReadOnly="true" runat="server"/>
					        </td>
					    </tr>
					    <tr>
					        <td>
                            </td>
                            <td style="width:100px">
                            </td>
					        <td colspan="2">
					        </td>

					    </tr>
					    <tr>
					        <td colspan="3">
                                <ml:EncodedLabel ID="Label7" Text="Total loan amount" AssociatedControlID="sFinalLAmt" runat="server"></ml:EncodedLabel>
					        </td>
					        <td>
						        <ml:MoneyTextBox ID="sFinalLAmt" ReadOnly="true" runat="server"/>
					        </td>
					    </tr>
					    <tr>
					        <td colspan="3">
                                <a href="javascript:linkMe('SettlementCharges.aspx');" id="lnkSettlementChargesDedFromLoanProc" runat="server">Closing costs deducted from loan proceeds</a>
                                <ml:EncodedLabel id="lblSettlementChargesDedFromLoanProc" runat="server">Closing costs deducted from loan proceeds</ml:EncodedLabel>
					        </td>
					        <td>
						        <ml:MoneyTextBox ID="sSettlementChargesDedFromLoanProc" ReadOnly="true" runat="server"/>
					        </td>
					    </tr>
					    <tr>
					        <td colspan="3">
                                <a href="javascript:linkMe('SettlementCharges.aspx');" id="lnkSettlementTotalFundByLenderAtClosing" runat="server">Closing costs funded by lender at closing</a>
                                <ml:EncodedLabel id="lblSettlementTotalFundByLenderAtClosing" runat="server">Closing costs funded by lender at closing</ml:EncodedLabel>
                            </td>
                            <td>
						        <ml:MoneyTextBox ID="sSettlementTotalFundByLenderAtClosing" ReadOnly="true" runat="server"/>
                            </td>
					    </tr>
                        <asp:PlaceHolder runat="server" ID="AdjustmentsOtherCreditsFields">
					    <tr>
					        <td colspan="3">
                                Other costs deducted from loan proceeds
                            </td>
                            <td>
						        <ml:MoneyTextBox ID="sAdjustmentsOtherCreditsDeductedFromLoanProceeds" ReadOnly="true" runat="server"/>
                            </td>
					    </tr>
					    <tr>
					        <td colspan="3">
                                Other costs funded by lender at closing
                            </td>
                            <td>
						        <ml:MoneyTextBox ID="sAdjustmentsOtherCreditsFundedByLenderAtClosing" ReadOnly="true" runat="server"/>
                            </td>
					    </tr>
                        </asp:PlaceHolder>
					    <tr>
					        <td colspan="3">
                                <ml:EncodedLabel ID="Label10" Text="Other funding adjustments" AssociatedControlID="sOtherFundAdj" runat="server"></ml:EncodedLabel>
                            </td>
					        <td>
						        <ml:MoneyTextBox ID="sOtherFundAdj" onchange="refreshCalculation();" runat="server"/>
					        </td>
					    </tr>
					    <tr>
					        <td colspan="5">&nbsp;</td>
					    </tr>
					    <tr>
					        <td colspan="3" class="BorderLeft BorderTop">
                                <ml:EncodedLabel ID="Label11" Text="Amount required to fund" AssociatedControlID="sAmtReqToFund" runat="server"></ml:EncodedLabel>
                            </td>
					        <td class="BorderTop BorderRight">
						        <ml:MoneyTextBox ID="sAmtReqToFund" ReadOnly="true" runat="server"/>
					        </td>
					    </tr>
					    <tr>
					        <td colspan="2" class="BorderLeft">
                                <ml:EncodedLabel ID="Label12" Text="Amount funded from warehouse line" AssociatedControlID="sAmtFundFromWarehouseLine" runat="server"></ml:EncodedLabel>
                            </td>
                            <td>
                                <asp:CheckBox ID="sAmtFundFromWarehouseLineLckd" onclick="lockField(this, 'sAmtFundFromWarehouseLine'); refreshCalculation();" Text="Locked" runat="server" />
                            </td>
					        <td class="BorderRight">
						        <ml:MoneyTextBox ID="sAmtFundFromWarehouseLine" onchange="refreshCalculation();" ReadOnly="true" runat="server"/>
					        </td>
					    </tr>
					    <tr>
					        <td class="BorderLeft" colspan="3">
                                <ml:EncodedLabel ID="Label13" Text="Funds required for shortfall" AssociatedControlID="sFundReqForShortfall" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderRight">
						        <ml:MoneyTextBox ID="sFundReqForShortfall" ReadOnly="true" runat="server"/>
					        </td>
					    </tr>
					    <tr>
					        <td colspan="2" class="BorderBottom BorderLeft">
                                <ml:EncodedLabel ID="Label14" Text="Check due from closing" AssociatedControlID="sChkDueFromClosing" runat="server"></ml:EncodedLabel>
                            </td>
                            <td class="BorderBottom">
                                <asp:CheckBox ID="sChkDueFromClosingRcvd" onclick="refreshCalculation();" Text="Rcv'd" runat="server" />
                            </td>
					        <td class="BorderBottom BorderRight">
						        <ml:MoneyTextBox ID="sChkDueFromClosing" ReadOnly="true" runat="server"/>
					        </td>
					    </tr>
					</table>
					<table cellpadding="2" cellspacing="0" class="FieldLabel Col">
                        <tr>
                            <td class="FieldLabel" width="150">
                                <ml:EncodedLabel ID="sConsummationDLabel" Text="Per-diem Interest Start Date" AssociatedControlID="sConsummationD" runat="server" onchange="refreshCalculation();"></ml:EncodedLabel>
                            </td>
                            <td>
                                <ml:DateTextBox ID="sConsummationD" Width="76" preset="date" CssClass="mask" runat="server"></ml:DateTextBox>
                                <asp:CheckBox ID="sConsummationDLckd" runat="server" onclick="refreshCalculation();" /> Locked
                            </td>
                        </tr>
					    <tr>
                            <td class="FieldLabel">
                                <ml:EncodedLabel ID="sSchedFundDLabel" Text="Scheduled Funding Date" AssociatedControlID="sSchedFundD" runat="server"></ml:EncodedLabel>
                            </td>
                            <td>
                                <ml:DateTextBox ID="sSchedFundD" Width="76" preset="date" CssClass="mask" runat="server"></ml:DateTextBox>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="FieldLabel">
                                <ml:EncodedLabel ID="sFundsOrderedDLabel" Text="Funds Ordered Date" AssociatedControlID="sFundsOrderedD" runat="server"></ml:EncodedLabel>
                            </td>
                            <td>
                                <ml:DateTextBox ID="sFundsOrderedD" Width="76" preset="date" CssClass="mask" runat="server"></ml:DateTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <ml:EncodedLabel ID="Label2" Text="Funded" AssociatedControlID="sFundD" runat="server"></ml:EncodedLabel>
                            </td>
                            <td nowrap>
                                <ml:DateTextBox ID="sFundD" runat="server" Width="76" preset="date" CssClass="mask"></ml:DateTextBox>
                                <asp:TextBox ID="sFundN" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 130px">
                                <ml:EncodedLabel ID="Label4" Text="Shipped to warehouse" AssociatedControlID="sShippedToWarehouseD"
                                    runat="server"></ml:EncodedLabel>
                            </td>
                            <td nowrap>
                                <ml:DateTextBox ID="sShippedToWarehouseD" runat="server" Width="75" preset="date"
                                    CssClass="mask"></ml:DateTextBox>
                                <asp:TextBox ID="sShippedToWarehouseN" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <ml:EncodedLabel ID="Label8" Text="Tracking number" AssociatedControlID="sTrackingN" runat="server"></ml:EncodedLabel>
                            </td>
                            <td>
                                <asp:TextBox ID="sTrackingN" Width="229px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="FormTableSubheader" colspan="2" id="ToBeCompletedByLoanOriginator">Loan Proceeds To</td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" nowrap colspan="2"><uc:CFM ID="CFM" runat="server" /></td>
                        </tr>
                        <tr id="trLoanProceedsTo">
                            <td>
                                Loan Proceeds To
                            </td>
                            <td>
                                <asp:DropDownList ID="sLoanProceedsToT" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="sFundingCompanyName">Company Name</label></td>
                            <td nowrap>
                                <asp:TextBox ID="sFundingCompanyName" runat="server"></asp:TextBox>
                                <label id="sFundingBranchNameLabel" for="sFundingBranchName" class="padBottom">Branch Name</label>
                                <asp:TextBox ID="sFundingBranchName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="sFundingContactName">Contact Name</label></td>
                            <td>
                                <asp:TextBox ID="sFundingContactName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="sFundingCompanyAddr">Address</label></td>
                            <td>
                                <asp:TextBox ID="sFundingCompanyAddr" Width="230px" runat="server"></asp:TextBox>
                                <label for="sFundingCompanyPhone" class="insetLabel3 padBottom">Phone</label>
                                <asp:TextBox ID="sFundingCompanyPhone" class="phone" preset="phone" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td nowrap>
                                <asp:TextBox ID="sFundingCompanyCity" runat="server"></asp:TextBox>
                                <ml:StateDropDownList ID="sFundingCompanyState" runat="server" />
                                <ml:ZipcodeTextBox ID="sFundingCompanyZip" runat="server"></ml:ZipcodeTextBox>
                                <label for="sFundingCompanyFax" class="insetLabel3 padBottom">Fax</label>
                                <asp:TextBox ID="sFundingCompanyFax" class="phone" preset="phone" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>First Credit To</td>
                            <td nowrap>
                                <label for="sFundingBankName" class="insetLabel padBottom">Bank Name</label>
                                <input type="text" id="sFundingBankName" runat="server" />
                                <label for="sFundingBankCityState" class="insetLabel2 padBottom">Bank City/State</label>
                                <input type="text" id="sFundingBankCityState" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <label for="sFundingABANumber" class="insetLabel padBottom">ABA Number</label>
                                <input type="text" id="sFundingABANumber" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <label for="sFundingAccountName" class="insetLabel padBottom">Account Name</label>
                                <input type="text" id="sFundingAccountName" runat="server" />
                                <label for="sFundingAccountNumber" class="insetLabel2 padBottom">Account Number</label>
                                <input type="text" id="sFundingAccountNumber" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>Further Credit To</td>
                            <td nowrap>
                                <label for="sFundingFurtherCreditToAccountName" class="insetLabel padBottom">Account Name</label>
                                <asp:TextBox ID="sFundingFurtherCreditToAccountName" runat="server"></asp:TextBox>
                                <label for="sFundingFurtherCreditToAccountNumber" class="insetLabel2 padBottom">Account Number</label>
                                <asp:TextBox ID="sFundingFurtherCreditToAccountNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap><label for="sFundingRequestBatchNumber">Funding Request Batch #</label></td>
                            <td>
                                <asp:TextBox ID="sFundingRequestBatchNumber" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Additional Instructions
                            </td>
                            <td>
                                <input type="text" id="sFundingAdditionalInstructionsLine1" class="Wide" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input type="text" id="sFundingAdditionalInstructionsLine2" class="Wide" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td valign=top>
                                <ml:EncodedLabel ID="Label9" Text="Funding notes" AssociatedControlID="sFundNotes" runat="server"></ml:EncodedLabel>
                            </td>
                            <td rowspan="5">
                                <asp:TextBox ID="sFundNotes" Width="270px" maxcharactercount="1000" MaxLength="1000"
                                    TextMode="MultiLine" Rows="7" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
				</td>
			</tr>
		</table>
	    <uc1:cmodaldlg id="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cmodaldlg>
    </form>
</body>
</html>
