﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FloodCertification.aspx.cs" Inherits="LendersOfficeApp.newlos.Closer.FloodCertification" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FloodOrderTab" Src="FloodOrderTab.ascx" %>
<%@ Register TagPrefix="uc1" TagName="FloodCertDataTab" Src="FloodCertDataTab.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Flood Certification</title>
</head>
<body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" scroll="yes">
    <form id="FloodCertification" method="post" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td nowrap class="TabRow">
                <uc1:Tabs runat="server" ID="Tabs" />
            </td>
        </tr>
        <tr>
            <td nowrap style="PADDING-LEFT:5px">
                <uc1:FloodOrderTab ID="FloodOrderTab" runat="server"></uc1:FloodOrderTab>
                <uc1:FloodCertDataTab ID="FloodCertDataTab" runat="server"></uc1:FloodCertDataTab>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
