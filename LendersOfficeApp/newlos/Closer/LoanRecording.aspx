﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="LoanRecording.aspx.cs" Inherits="LendersOfficeApp.newlos.Closer.LoanRecording" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        .verticalAlign { padding-bottom: .15em; width: 46px; margin-left: 7px;}
    </style>
</head>
<script type="text/javascript">
    function GenerateMersMin_OnClick() {
        args = {
            sLNm: ML.sLNm
        };
        var results = gService.loanedit.call('GenerateMersMin', args);
        if (results.value.Error) {
            alert(results.value.Error);
            return;
        }
        if (!results.value.sMersMin) {
            alert("An unspecified error occurred when generating your Mers MIN. Please try again later.");
            return;
        }
        
        sMersMinTextBox = document.getElementById('sMersMin');
        sMersMinTextBox.value = results.value.sMersMin;
        updateDirtyBit();
    }
    
    function _init() {
        lockField(<%= AspxTools.JsGetElementById(sMersOriginatingOrgIdLckd) %>, 'sMersOriginatingOrgId');
        lockField(<%= AspxTools.JsGetElementById(sMersTosDLckd) %>, 'sMersTosD'); 
    }
</script>
<body bgcolor="gainsboro">
    <form id="LoanRecording" runat="server">
    		<table cellSpacing="0" cellPadding="0">
			<tr>
				<td class="MainRightHeader" noWrap>
				    Loan Recording
				</td>
			</tr>
			<tr>
				<td style="padding:4px">
					<table cellpadding="2" cellspacing="0" class="FieldLabel">
					    <tr>
					        <td colspan="2">
					            State
					            <ml:statedropdownlist id="sSpState" runat="server" IncludeTerritories="false"></ml:statedropdownlist>
					            County
					            <asp:DropDownList ID="sSpCounty" runat="server" onchange="refreshCalculation();"></asp:DropDownList>					            
					        </td>
					    </tr>
					    <tr>
					        <td>
                                <ml:EncodedLabel ID="Label1" Text="Recorded" AssociatedControlID="sRecordedD" runat="server"></ml:EncodedLabel>
					        </td>
					        <td>
                                <ml:datetextbox id="sRecordedD" runat="server" width="75" preset="date" CssClass="mask" onchange="date_onblur(event);refreshCalculation();"></ml:datetextbox>
					            <asp:TextBox ID="sRecordedN" runat="server" onchange="refreshCalculation();"></asp:TextBox>
					        </td>
					    </tr>
					    <tr>
					        <td>
    					        Instrument #
					        </td>			
					        <td>
					            <asp:TextBox ID="sRecordingInstrumentNum" MaxLength="40" Width="229px" runat="server"></asp:TextBox>
					        </td>
					    </tr>
					    <tr>
					        <td>
					            <label>Volume #</label>
					        </td>
					        <td>
					            <asp:TextBox ID="sRecordingVolumeNum" MaxLength="20" Width="85px" runat="server"></asp:TextBox>
					            <label class="verticalAlign">Book #</label>
					            <asp:TextBox ID="sRecordingBookNum" MaxLength="20" Width="85px" runat="server"></asp:TextBox>
					            <label class="verticalAlign">Page #</label>
					            <asp:TextBox ID="sRecordingPageNum" MaxLength="20" Width="85px" runat="server"></asp:TextBox>
					        </td>
					    </tr>
                        <tr>
                            <td>
                                <label for="sHasENote">
                                    Has ENote
                                </label>
                            </td>
                            <td>
                                <input type="checkbox" id="sHasENote" runat="server" />
                            </td>
                        </tr>
					    <tr>
					    </tr>
					 </table>
					 <br />
					 <table cellpadding="2" cellspacing="0" class="FieldLabel">
					    <tr>
					        <td>
                                <ml:EncodedLabel ID="Label2" Text="MERS MIN" AssociatedControlID="sMersMin" runat="server"></ml:EncodedLabel>
					        </td>
					        <td></td>
					        <td>
					            <asp:TextBox ID="sMersMin" Width="120px" MaxLength="18" runat="server" onchange="refreshCalculation();"></asp:TextBox>
					            <input type="button" id="GenerateMersMin" runat="server" onclick="GenerateMersMin_OnClick();refreshCalculation();" value="Generate" />
					        </td>
					    </tr>
					    <tr>
					        <td>
                                <ml:EncodedLabel ID="Label3" Text="MERS Originating Org ID" AssociatedControlID="sMersOriginatingOrgId" runat="server"></ml:EncodedLabel>
					        </td>
					        <td>
					            <asp:CheckBox ID="sMersOriginatingOrgIdLckd" runat="server" onclick="refreshCalculation();" />
					        </td>
					        <td>
					            <asp:TextBox ID="sMersOriginatingOrgId" Width="75px" MaxLength="7" runat="server" ></asp:TextBox>
					        </td>
					    </tr>
					    <tr>
					        <td>
                                <ml:EncodedLabel ID="Label4" Text="MERS Registration" AssociatedControlID="sMersRegistrationD" runat="server"></ml:EncodedLabel>
					        </td>
					        <td></td>
					        <td>
					            <ml:DateTextBox ID="sMersRegistrationD" runat="server" preset="date" Width="75" CssClass="mask" onchange="date_onblur(event); refreshCalculation();"></ml:DateTextBox>
					            <asp:CheckBox ID="sMersIsOriginalMortgagee" runat="server" />
					            <ml:EncodedLabel ID="sMersIsOriginalMortgageeLabel" Text="MERS is original mortgagee" runat="server"></ml:EncodedLabel>
					        </td>
					    </tr>
					    <tr>
					        <td>
                                <ml:EncodedLabel ID="Label5" Text="MERS TOB" AssociatedControlID="sMersTobD" runat="server"></ml:EncodedLabel>
					        </td>
					        <td></td>
					        <td>
					            <ml:DateTextBox ID="sMersTobD" runat="server" preset="date" Width="75" CssClass="mask" onchange="date_onblur(event); refreshCalculation();"></ml:DateTextBox>
					        </td>
					    </tr>
					    <tr>
					        <td>
                                <ml:EncodedLabel ID="Label6" Text="MERS TOS" AssociatedControlID="sMersTosD" runat="server"></ml:EncodedLabel>
					        </td>
					        <td>
					            <asp:CheckBox ID="sMersTosDLckd" runat="server" onclick="refreshCalculation();" />
					        </td>
					        <td>
					            <ml:DateTextBox ID="sMersTosD" runat="server" preset="date" Width="75" CssClass="mask" onchange="date_onblur(event); refreshCalculation();"></ml:DateTextBox>
					        </td>
					    </tr>
					</table>
				</td>
			</tr>
		</table>
    </form>
</body>
</html>
