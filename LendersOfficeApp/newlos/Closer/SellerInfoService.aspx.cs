﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using DataAccess.Sellers;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class SellerInfoServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(SellerInfoServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var jsonSerializedCollection = GetString("SellerInfoHolder");
            SellerCollection sellColl = ObsoleteSerializationHelper.JavascriptJsonDeserializer<SellerCollection>(jsonSerializedCollection);
            if (sellColl != null) dataLoan.sSellerCollection = sellColl;

            dataLoan.sSellerVesting = GetString("sSellerVesting");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public partial class SellerInfoService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new SellerInfoServiceItem());
        }
    }
}
