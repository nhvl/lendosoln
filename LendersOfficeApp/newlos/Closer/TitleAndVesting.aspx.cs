﻿namespace LendersOfficeApp.newlos.Closer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.Trust;
    using LendersOffice.Common;
    using MeridianLink.CommonControls;
    using LendersOffice.AntiXss;

    public partial class TitleAndVesting : BaseLoanPage
    {
        protected bool useNewNonPurchaseBorrower = false;

        private CPageData dataLoan;

        protected override void LoadData()
        {
            this.EnableJqueryMigrate = false;

            dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(TitleAndVesting));
            dataLoan.InitLoad();

            this.RegisterJsGlobalVariables("IsTargeting2019Ulad", dataLoan.sIsTargeting2019Ulad);

            List<AppRelationshipData> apps = new List<AppRelationshipData>(dataLoan.nApps);
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData currentApp = dataLoan.GetAppData(i);
                AppRelationshipData appForBind = new AppRelationshipData();

                var borrowerTitleName = currentApp.aBTypeT == E_aTypeT.CurrentTitleOnly
                    ? currentApp.aBCurrentTitleName
                    : currentApp.aTitleNm1;

                var coborrowerTitleName = currentApp.aCTypeT == E_aTypeT.CurrentTitleOnly
                    ? currentApp.aCCurrentTitleName
                    : currentApp.aTitleNm2;

                appForBind.aTitleNm1 = borrowerTitleName;
                appForBind.aTitleNm2 = coborrowerTitleName;
                appForBind.aBRelationshipTitleT = currentApp.aBRelationshipTitleT;
                appForBind.aCRelationshipTitleT = currentApp.aCRelationshipTitleT;
                appForBind.hasCoborrower = currentApp.aCIsValidNameSsn;
                appForBind.aBRelationshipTitleOtherDesc = currentApp.aBRelationshipTitleOtherDesc;
                appForBind.aCRelationshipTitleOtherDesc = currentApp.aCRelationshipTitleOtherDesc;
                appForBind.aBTypeT = currentApp.aBTypeT;
                appForBind.aCTypeT = currentApp.aCTypeT;
                apps.Add(appForBind);

                var bAliases = new List<string>(currentApp.aBAliases);
                var cAliases = new List<string>(currentApp.aCAliases);

                ClientScript.RegisterHiddenField("aBAliases" + i, ObsoleteSerializationHelper.JavascriptJsonSerialize(bAliases));
                ClientScript.RegisterHiddenField("aCAliases" + i, ObsoleteSerializationHelper.JavascriptJsonSerialize(cAliases));
                ClientScript.RegisterHiddenField("aBPowerOfAttorneyNm" + i,currentApp.aBPowerOfAttorneyNm);
                ClientScript.RegisterHiddenField("aCPowerOfAttorneyNm" + i,currentApp.aCPowerOfAttorneyNm);

            }

            Relationships.DataSource = apps;
            Relationships.DataBind();
            ClientScript.RegisterHiddenField("RelationshipsId", Relationships.ClientID);

            aManner.Text = dataLoan.GetAppData(0).aManner;

            sTitleReportItemsDescription.Text = dataLoan.sTitleReportItemsDescription;
            sRequiredEndorsements.Text = dataLoan.sRequiredEndorsements;
            sPropertyTaxMessageDescription.Text = dataLoan.sPropertyTaxMessageDescription;
            sAssessorsParcelId.Text = dataLoan.sAssessorsParcelId;

            sPrelimRprtN.Text = dataLoan.sPrelimRprtN;
            sPrelimRprtRd.Text = dataLoan.sPrelimRprtRd_rep;
            sPrelimRprtDueD.Text = dataLoan.sPrelimRprtDueD_rep;
            sPrelimRprtDocumentD.Text = dataLoan.sPrelimRprtDocumentD_rep;
            sPrelimRprtOd.Text = dataLoan.sPrelimRprtOd_rep;

            sVestingToRead.Text = dataLoan.sVestingToRead;
            sVestingToReadLckd.Checked = dataLoan.sVestingToReadLckd;

            sTrustId.Value = dataLoan.sTrustId;
            sTrustName.Value = dataLoan.sTrustName;
            sTrustForBenefitOf.Value = dataLoan.sTrustForBenefitOf;
            sTrustAgreementD.Text = dataLoan.sTrustAgreementD_rep;
            sTrustState.Value = dataLoan.sTrustState;
            Tools.SetDropDownListValue(this.sTrustClassificationT, dataLoan.sTrustClassificationT);

            var tvc = dataLoan.sTrustCollection;
            
            var trustorSource = tvc.Trustors;
            if (!trustorSource.Any()) trustorSource = new List<TrustCollection.Trustor>() { new TrustCollection.Trustor() };

            Trustors.DataSource = trustorSource;
            Trustors.DataBind();
            
            var trusteeSource = tvc.Trustees;
            if (!trusteeSource.Any()) trusteeSource = new List<TrustCollection.Trustee>() { new TrustCollection.Trustee() };
            Trustees.ItemDataBound += new RepeaterItemEventHandler(Trustees_ItemDataBound);
            Trustees.DataSource = trusteeSource;
            Trustees.DataBind();

            List<TitleBorrower> borrowers = dataLoan.sTitleBorrowers;
            TitleOnlyBorrowers.DataSource = borrowers;
            TitleOnlyBorrowers.DataBind();
            TitleOnlyBorrowerJSON.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sTitleBorrowers);

            useNewNonPurchaseBorrower = dataLoan.BrokerDB.IsUseNewNonPurchaseSpouseFeature;
        }

        void Trustees_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var dataItem = ((TrustCollection.Trustee)e.Item.DataItem);
            var StateDDL = (StateDropDownList)e.Item.FindControl("State");
            StateDDL.Value = dataItem.Address.State;

            var SignCertOfTrust = (HtmlInputCheckBox)e.Item.FindControl("SignCertOfTrust");
            SignCertOfTrust.Checked = dataItem.CanSignCertOfTrust;

            var SignLoanDocs = (HtmlInputCheckBox)e.Item.FindControl("SignLoanDocs");
            SignLoanDocs.Checked = dataItem.CanSignLoanDocs;

            var SignatureVerbiage = (TextBox)e.Item.FindControl("SignatureVerbiage");
            SignatureVerbiage.Text = dataItem.SignatureVerbiage;
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsGlobalVariables("EmailValidationExpression", LendersOffice.Constants.ConstApp.EmailValidationExpression);

            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("TitleOnlyBorrowerUtils.js");

            Tools.BindComboBox_aManner(aManner);
            Tools.Bind_sTrustClassificationT(this.sTrustClassificationT);

            this.PageTitle = "Title And Vesting";
            this.PageID = "TitleAndVesting";
        }

        protected void TitleOnlyBorrowers_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Item  ||
                args.Item.ItemType == ListItemType.AlternatingItem ||
                args.Item.ItemType == ListItemType.Header)
            {
                TitleBorrower borrower = (TitleBorrower)args.Item.DataItem;
                DropDownList titleRelationshipDDL = args.Item.FindControl("TitleRelationshipTitleT") as DropDownList;
                Tools.Bind_aBRelationshipTitleT(titleRelationshipDDL);

                DropDownList associatedApplicationDDL = args.Item.FindControl("TitleAssociatedApplicationId") as DropDownList;
                dataLoan.LoadAppNames(associatedApplicationDDL);
                associatedApplicationDDL.Items.Insert(0, new ListItem("-- NONE --", Guid.Empty.ToString()));

                if (args.Item.ItemType != ListItemType.Header)
                {
                    DateTextBox dob = args.Item.FindControl("TitleDob") as DateTextBox;
                    dob.Text = borrower.Dob_rep;

                    Tools.SetDropDownListValue(titleRelationshipDDL, borrower.RelationshipTitleT);

                    if (borrower.AssociatedApplicationId != Guid.Empty)
                    {
                        Tools.SetDropDownListValue(associatedApplicationDDL, borrower.AssociatedApplicationId.ToString());
                    }

                    HiddenField aliases = (HiddenField)args.Item.FindControl("TitleAliases");
                    aliases.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(borrower.Aliases);
                }
            }
        }

        protected void Relationships_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Separator)
            {
                return;
            }

            AppRelationshipData data = args.Item.DataItem as AppRelationshipData;
            EncodedLabel aTitleNm1 = args.Item.FindControl("aTitleNm1") as EncodedLabel;
            EncodedLabel aTitleNm2 = args.Item.FindControl("aTitleNm2") as EncodedLabel;
            EncodedLabel aBTypeT_isTitle = args.Item.FindControl("aBTypeT_isTitle") as EncodedLabel;
            EncodedLabel aCTypeT_isTitle = args.Item.FindControl("aCTypeT_isTitle") as EncodedLabel;
            TextBox aBRelationshipTitleOtherDesc = args.Item.FindControl("aBRelationshipTitleOtherDesc") as TextBox;
            TextBox aCRelationshipTitleOtherDesc = args.Item.FindControl("aCRelationshipTitleOtherDesc") as TextBox;
            DropDownList aBRelationshipTitleT = args.Item.FindControl("aBRelationshipTitleT") as DropDownList;
            DropDownList aCRelationshipTitleT = args.Item.FindControl("aCRelationshipTitleT") as DropDownList;
            HtmlTableRow coborrowerRow = args.Item.FindControl("coborrowerRow") as HtmlTableRow;
            HtmlAnchor editBAliases = args.Item.FindControl("editBAliases") as HtmlAnchor;
            HtmlAnchor editCAliases = args.Item.FindControl("editCAliases") as HtmlAnchor;

            editBAliases.Attributes.Add("onclick", "onEditBorrAliasesClick(" + AspxTools.JsNumeric(args.Item.ItemIndex) + ")");
            editCAliases.Attributes.Add("onclick", "onEditCoborrAliasesClick(" + AspxTools.JsNumeric(args.Item.ItemIndex) + ")");

            aTitleNm1.Text = data.aTitleNm1;
            if (data.aBTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly))
            {
                aBTypeT_isTitle.Visible = true;
                aBTypeT_isTitle.Text = data.aBTypeT == E_aTypeT.NonTitleSpouse ? "Non Title Spouse" : "Current Title Only";
                aBRelationshipTitleT.Visible = false;
                aBRelationshipTitleOtherDesc.Visible = false;
            }
            else
            {
                aBTypeT_isTitle.Visible = false;
                aBRelationshipTitleT.Visible = true;
                Tools.Bind_aBRelationshipTitleT(aBRelationshipTitleT);
                aBRelationshipTitleOtherDesc.Visible = true;
            }
            Tools.SetDropDownListValue(aBRelationshipTitleT, data.aBRelationshipTitleT);
            aBRelationshipTitleOtherDesc.Text = data.aBRelationshipTitleOtherDesc;

            if (data.hasCoborrower)
            {
                aTitleNm2.Text = data.aTitleNm2;
                if (data.aCTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly))
                {
                    aCTypeT_isTitle.Visible = true;
                    aCTypeT_isTitle.Text = data.aCTypeT == E_aTypeT.NonTitleSpouse ? "Non Title Spouse" : "Current Title Only";
                    aCRelationshipTitleT.Visible = false;
                    aCRelationshipTitleOtherDesc.Visible = false;
                }
                else
                {
                    aCTypeT_isTitle.Visible = false;
                    aCRelationshipTitleT.Visible = true;
                    Tools.Bind_aBRelationshipTitleT(aCRelationshipTitleT);
                    aCRelationshipTitleOtherDesc.Visible = true;
                }
                Tools.SetDropDownListValue(aCRelationshipTitleT, data.aCRelationshipTitleT);
                aCRelationshipTitleOtherDesc.Text = data.aCRelationshipTitleOtherDesc;
            }
            else
            {
                coborrowerRow.Visible = false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        protected class AppRelationshipData
        {
            public string aTitleNm1 { get; set; }
            public string aTitleNm2 { get; set; }
            public E_aRelationshipTitleT aBRelationshipTitleT { get; set; }
            public E_aRelationshipTitleT aCRelationshipTitleT { get; set; }
            public string aBRelationshipTitleOtherDesc { get; set; }
            public string aCRelationshipTitleOtherDesc { get; set; }
            public bool hasCoborrower { get; set; }
            public E_aTypeT aBTypeT { get; set; }
            public E_aTypeT aCTypeT { get; set; }

            public AppRelationshipData() { }
        }
    }
}
