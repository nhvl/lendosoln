﻿namespace LendersOfficeApp.newlos.Closer
{
    using System;
    using DataAccess;

    public class SecurityInstrumentServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(SecurityInstrumentServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sExecutionLocationSourceT = (E_sExecutionLocationSourceT)GetInt("sExecutionLocationSourceT_SelectedValue");
            dataLoan.sExecutionLocationAgentT = (E_AgentRoleT)GetInt("sExecutionLocationAgentT");
            dataLoan.sExecutionLocationAddress = GetString("sExecutionLocationAddress");
            dataLoan.sExecutionLocationCity = GetString("sExecutionLocationCity");
            dataLoan.sExecutionLocationState = GetString("sExecutionLocationState");
            dataLoan.sExecutionLocationZip = GetString("sExecutionLocationZip");
            dataLoan.sExecutionLocationCounty = GetString("sExecutionLocationCounty");

            dataLoan.sTxSecurityInstrumentPar27T = (E_sTxSecurityInstrumentPar27T)GetInt("sTxSecurityInstrumentPar27T");
            dataLoan.sNyPropertyStatementT = (E_sNyPropertyStatementT)GetInt("sNyPropertyStatementT");
            dataLoan.sAcknowledgeCashAdvanceForNonHomestead = GetBool("sAcknowledgeCashAdvanceForNonHomestead");
            dataLoan.sSecInstrAttorneyFeesPc_rep = GetString("sSecInstrAttorneyFeesPc");
            dataLoan.sTrusteeFeePc_rep = GetString("sTrusteeFeePc");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sExecutionLocationSourceT_LeaveBlank", dataLoan.sExecutionLocationSourceT == E_sExecutionLocationSourceT.LeaveBlank);
            SetResult("sExecutionLocationSourceT_PopulateFromContact", dataLoan.sExecutionLocationSourceT == E_sExecutionLocationSourceT.PopulateFromContact);
            SetResult("sExecutionLocationSourceT_SubjectPropertyAddress", dataLoan.sExecutionLocationSourceT == E_sExecutionLocationSourceT.SubjectPropertyAddress);
            SetResult("sExecutionLocationSourceT_SetManually", dataLoan.sExecutionLocationSourceT == E_sExecutionLocationSourceT.SetManually);
            SetResult("sExecutionLocationAgentT", dataLoan.sExecutionLocationAgentT);
            SetResult("sExecutionLocationAddress", dataLoan.sExecutionLocationAddress);
            SetResult("sExecutionLocationCity", dataLoan.sExecutionLocationCity);
            SetResult("sExecutionLocationState", dataLoan.sExecutionLocationState);
            SetResult("sExecutionLocationZip", dataLoan.sExecutionLocationZip);
            SetResult("sExecutionLocationCounty", dataLoan.sExecutionLocationCounty);

            SetResult("sTxSecurityInstrumentPar27T", dataLoan.sTxSecurityInstrumentPar27T);
            SetResult("sNyPropertyStatementT", dataLoan.sNyPropertyStatementT);
            SetResult("sAcknowledgeCashAdvanceForNonHomestead", dataLoan.sAcknowledgeCashAdvanceForNonHomestead);
            SetResult("sSecInstrAttorneyFeesPc", dataLoan.sSecInstrAttorneyFeesPc_rep);
            SetResult("sTrusteeFeePc", dataLoan.sTrusteeFeePc_rep);
        }
    }

    public partial class SecurityInstrumentService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new SecurityInstrumentServiceItem());
        }
    }
}
