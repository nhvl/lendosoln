<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdditionalHUD1Data.aspx.cs"
    Inherits="LendersOfficeApp.newlos.Closer.AdditionalHUD1Data" %>

<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Security" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="x-ua-compatible" content="IE=8" />
    <title></title>


    <style type="text/css">
        *
        {
            margin: 0px;
            padding: 0px;
        }
        input
        {
            width: 90px;
        }
        input[type='checkbox']
        {
            width: 13px;
            height: 13px;
            margin-left: 2px;
            vertical-align: bottom;
            position: relative;
            top: -1px;
            *overflow: hidden;
        }
        .LineNumber
        {
            display: inline-block;
            width: 45px;
            text-align: right;
            font-weight: bold;
        }
        select.LineNumber
        {
            width: 50px;
            background-image: none;
            border-width: thin;
        }
        .Error
        {
            background-color: #FF0000;
        }
        .Desc
        {
            display: inline-block;
            width: 220px;
        }
        .Line
        {
            margin-bottom: 3px;
        }
        .Block
        {
            margin-right: 5px;
            width: 600px;
        }
        .Section
        {
            margin-bottom: 20px;
            position: relative;
            left: 10px;
            
            width: 900px;
        }
        span.Desc
        {
            margin-right: 3px;
            font-weight: bold;
        }
        span.FullWidth
        {
            width:100%;
        }
        #Block_100
        {
            float: left;
            width: 390px;
        }
        #Block_100 .Desc
        {
            width: 240px;
        }
        #Block_400
        {
            float: left;
            width: 480px;
        }
        #Section_100And400
        {
            height: 125px;
        }
        #Block_200
        {
            float: left;
            width: 425px;
        }
        #Block_500
        {
            float: left;
            width: 500px;
        }
        #Section_200And500
        {
            height: 185px;
            width: 1000px;
        }
        #Section_200And500 span.Desc
        {
            margin-left: 5px;
        }
        #Section_200And500 input.Desc
        {
            width: 224px;
        }
        #Block_700
        {
            width: 550px;
        }
        #Block_700 table
        {
            display: inline;
            height: 20px;
            position: relative;
            bottom: 10px;
        }
        #Block_700 div.Line_701
        {
            height: 20px;
        }
        #Block_700 div.Line_702
        {
            height: 20px;
        }
        #Block_700 div.Line_703 input.Money
        {
            float: right;
            height: 13px;
        }
        #Block_700 div.Line_703 input#sRealEstateBrokerFeesCommissionPdAtSettlementFFromBorr
        {
            position: relative;
            right: 13px;
        }
        #Block_700 div.Line_704 input.Money
        {
            float: right;
            height: 13px;
        }
        #Block_700 div.Line_704 input#sRealEstateBrokerFeesU1FFromBorr
        {
            position: relative;
            right: 13px;
        }
        #Block_700 div.PaidFromBorr.PaidFromSeller
        {
            width: 200px;
            height: 10px;
            float: right;
        }
        #Block_700 div.PaidFromBorr.PaidFromSeller span.PaidFromSeller
        {
            width: 90px;
            height: 10px;
            float: right;
        }
        #Block_700 div.PaidFromBorr.PaidFromSeller span.PaidFromBorr
        {
            width: 100px;
            height: 10px;
            float: left;
        }
        #Block_Column
        {
            width: 374px;
            height: 275px;
        }
        #Block_Column div.Line_Total
        {
            float: right;
            height: 20px;
        }
        #Block_Column div.LoanFormHeader span.Desc
        {
            margin-left: 5px;
        }
        .LoanFormHeader
        {
            width: 850px;
            position: relative;
            left: -5px;
            font-weight: bold;
            height: 15px;
            margin-bottom: 5px;
        }
        .LoanFormHeader span.Desc
        {
            width: 80%;
        }
        .Line_500.LoanFormHeader
        {
            width: 400px;
        }
        .Line_400.LoanFormHeader
        {
            width: 400px;
        }
        .Line_510, .Line_511
        {
            /*Hide these for now*/
            display:none;
        }
        input.ShowHide
        {
            float: left;
        }
        .Section.Collapsed
        {
            height: 20px !important;
        }
        .Line.Collapsed
        {
            display: none;
        }
        .Line.LoanFormHeader.Collapsed
        {
            display: block;
        }
        #Main
        {
            overflow: hidden;
        }
        img
        {
            vertical-align: bottom;
        }
        .Line_104
        {
            visibility:hidden;
        }
        span.Collapsed
        {
            display:none;
        }
        #Block_200 label
        {
            font-weight: bold;
            margin-left: 5px;
        }
    </style>

    <script type="text/javascript">
        jQuery(function($) {

            //Hide the add to contacts links
            $('#Block_700 [id$="linkAddToContact"]').hide();

            //Copy 102 into 402 in the UI
            $('#sGrossDueFromBorrPersonalProperty').change(MirrorField($('#GrossDueFromBorrPersonalPropertyS'),
                                                                          $('#sGrossDueFromBorrPersonalProperty')));

            //Apply the show/hide state, it's a string of seven 0's or 1's. 1 means open.
            var InitiallyOpen = $('#OpenSections').val();
            var sections = $('.Section');
            $.each(InitiallyOpen.split(''), function(idx, value) {
                if (idx >= sections.length) return;
                sections.eq(idx).find('.ShowHide')[0].checked = value == '1';
            });

            //Show/hide sections
            $('.ShowHide').change(function() {
                var showHide = '1'; // 1 means show
                var parent = $(this).parents('.Section');
                if ($(this).is(':checked')) {
                    parent.removeClass('Collapsed')
                          .children('.Block')
                          .children('.Line')
                          .removeClass('Collapsed');
                }
                else {
                    showHide = '0';
                    parent.addClass('Collapsed')
                          .children('.Block')
                          .children('.Line')
                          .addClass('Collapsed');
                }

                //We have to track our state, so modify whatever's in the hidden field.
                var showHideState = $('#OpenSections').val();
                //We have to go through this whole split thing because accessing characters directly
                //(e.g, showHideState[3] = '1') is non-standard and unsupported in IE 7.
                var temp = showHideState.split('');
                //parent.index() is the DOM index, so it's 1-based.
                temp[parent.index() - 1] = showHide;
                showHideState = temp.join('');
                $('#OpenSections').val(showHideState);

                //IE 7 hack - need to toggle position: relative, otherwise half the sections won't display. 
                // It also needs to happen outside this function.
                window.setTimeout(function() {
                    parent.toggleClass('Section').toggleClass('Section');
                }, 1);
            });

            $('.ShowHide').trigger('change');

            //Setup the charges to seller box - it's the sum of these inputs.
            var ChargesToSellerDependencies = $(
              '#sRealEstateBrokerFeesU1FFromSeller,' +
              '#sRealEstateBrokerFeesCommissionPdAtSettlementFFromSeller,' +
              '#Total_800');

            ChargesToSellerDependencies.change(function() {
                var sumInCents = 0;
                ChargesToSellerDependencies.each(function() {
                    sumInCents += Math.round(ParseFromMoneyFormat($(this).val()) * 100)
                });
                $('#ChargesToSeller').val(sumInCents / 100);
                //Manually format it using the fxn in mask.js, since it doesn't get called automatically
                //when we change the value w/ jQuery
                format_money($('#ChargesToSeller')[0]);
                $('#ChargesToSeller').change();
            });

            ChargesToSellerDependencies.change()

            function MirrorField(copyInTo, copyOutOf) {
                return function() {
                    //Handle checkboxes
                    if ($(copyOutOf).is("input[type='checkbox']")) {
                        if ($(copyOutOf).is(':checked')) $(copyInTo).attr('checked', 'checked');
                        else $(copyInTo).removeAttr('checked');
                        return;
                    }

                    //Handle money fields - they won't be nicely formatted until after this function executes,
                    //so wait a bit and copy it over.
                    //This also handles other text inputs.
                    window.setTimeout(function() {
                        $(copyInTo).val($(copyOutOf).val());
                        $(copyInTo).trigger('change');
                    }, 10);
                };
            }


            //The HUDlines in section 200&500 are all text fields for consistency,
            //but what they really need to display as are literals (in the future, we're going to make which value they are 
            //user-selectable). Hide the default HUDlines and set up the fancy literal stuff here.
            $('#Section_200And500').find('.HUDLine')
            .hide()
            .after(Make200_500HUDline);

            $('#Section_200And500').find('.HUDLine').trigger("Autoassign");

            //Set up the Settlement Charges column
            $('#Block_Column .Line').each(function() {
                $(this).children('.Desc').attr('readonly', 'readonly');
                $(this).children('.LineNumber')
                    .hide()
                    .after(MakeColumnHUDline);
            });

            //Set up the Settlement Charges Total box
            $('#Block_Column .Line input.Money').each(function() {
                var total = $('#Total_800');
                $(this).change(function() {
                    var sumInCents = 0;
                    $('#Block_Column .Line input.Money').each(function() {
                        sumInCents += Math.round(ParseFromMoneyFormat($(this).val()) * 100);
                    });
                    total.val(sumInCents / 100);
                    //jQuery refuses to trigger events created with .attachElement,
                    //so manually call the function in mask.js.
                    format_money(total[0]);
                    total.change();
                });
                $(this).change();
            });

            var getMinorLineNumberFromClass = /Line_\d(\d+)/;
            //Note that ASP.Net wraps the actual input[type='checkbox'] in a span (or label sometimes)
            $('.SameAsBorr').children('input[type="checkbox"]').change(function() {
                var lineNumberMinor = getMinorLineNumberFromClass.exec($(this).parents('.Line').attr('class'))[1];
                var seller = $(this).parent().siblings('.Money, .Desc');
                var borrower = $(this).parents('.Section') //Go back up to the section
                    .children('.Borrower') //Go down the borrower block
                    .children('.Line[class$=' + lineNumberMinor + ']') //Find the line we're linked to
                    .children('.Money, .Desc'); //And take their money (field)!

                if ($(this).is(':checked')) {
                    for (var i = 0; i < seller.length; i++) {
                        borrower.bind('change.MatchSellerValue', MirrorField(seller[i], borrower[i]));
                    }
                    borrower.change();
                    seller.change()
                    seller.attr('disabled', 'disabled');
                }
                else {
                    seller.removeAttr('disabled');
                    borrower.unbind('change.MatchSellerValue');
                    seller.trigger('change.NoRestore');
                }

            });
            //And then initialize things
            $('.SameAsBorr').children('input[type="checkbox"]').change();

            function Make200_500HUDline() {

                var section = $(this).parents('#Block_200').length > 0 ? 200 : 500;
                var start = 506;
                var end = 509;
                if (section == 200) {
                    start = 204;
                    end = 209;
                }

                var elem = $('<span>').addClass($(this).attr('class'));

                $(elem).bind("Autoassign", function() {
                    AutoassignNextHudline(elem, section, start, end);
                    $(this).prev().val($(this).text());
                });

                return elem;
            }

            function ParseFromMoneyFormat(value) {
                var strippedValue = value.replace(',', '').replace('$', '');

                if (strippedValue.indexOf('(') == -1) {
                    return (parseFloat(strippedValue) || 0)
                }
                else {
                    return -(parseFloat(strippedValue.replace('(', '')) || 0)
                }
            }

            function MakeColumnHUDline() {
                //This function gets snuck in by the codebehind.
                var SettlementCharges = GetSettlementCharges();
                var range = [''];
                $.each(SettlementCharges, function() {
                    range.push(this.LineNumber);
                });

                var elem = CreateSelect(this, range);
                $(elem).bind('change.ColumnHUDline', function(event) {
                    if ($.trim($(this).val()) == '') {
                        $(this).next().val('');
                        $(this).next().next().val('');
                    }
                    var charges = SettlementCharges[$(this).val()] || { Description: '', Value: 0 };
                    var valueField = $(this).next().next();
                    var descField = $(this).next();

                    if (descField.val() != charges.Description) {
                        descField.val(charges.Description);
                        updateDirtyBit(event);
                    }
                });

                return elem;

            }

            function CreateSelect(original, range) {
                //Copy all the original's css classes over, so we don't have to worry too much about styling it.
                var select = $('<select />').addClass($(original).attr('class'));

                var options = [];
                var selected = 'selected = "selected"';
                var originalVal = $(original).val();
                //If the original had a non-zero value, initialize to it and un-disable us

                //Set up the options for the new selects
                $.each(range, function() {
                    if (originalVal == this) {
                        options.push('<option value="' + this + '" selected="selected">' + this + '</option>');
                    }
                    else {
                        options.push('<option value="' + this + '">' + this + '</option>');
                    }
                });

                select.append(options.join(''));

                //And when we change, make sure the original changes too
                $(select).change(function(event) {
                    $(this).prev().val($(this).val());
                    updateDirtyBit(event);
                });

                return select;
            }

            function GetRelatedHudlines(element) {
                return $(element).parents('.Block').find('span.HUDLine').not(':disabled').not(element);
            }

            function AutoassignNextHudline(element, section, start, end) {
                var numberIsAvailable = {};
                for (var i = start; i <= end; i++) {
                    numberIsAvailable[i] = true;
                }
                GetRelatedHudlines(element).each(function() {
                    numberIsAvailable[$(this).text()] = false;
                });
                //The last one (509 or 209) is always considered available.
                numberIsAvailable[section + 9] = true;
                //The blank default value is never available
                numberIsAvailable[''] = false;

                var nextAvailable;
                for (nextAvailable = start; nextAvailable <= end; nextAvailable++) {
                    if (numberIsAvailable[nextAvailable]) {
                        break;
                    }
                }
                $(element).text(nextAvailable);
            }
        });
    </script>
</head>
<body bgcolor='gainsboro'>
    <form id="form1" runat="server">
    <div id='Main'>
        <div class='MainRightHeader'>
            Additional HUD-1 Data</div>
        <div id='Section_100And400' class='Section Collapsed'>
            <div id='Block_100' class='Block Borrower'>
                <div class='Line Line_100 BlockHeader LoanFormHeader'>
                    <input type="checkbox" class='ShowHide' id='Section_100And400ShowHide' />
                    <span class='LineNumber '>100</span> <span class='Desc '>Gross Amount Due from Borrower</span>
                </div>
                <div class='Line Collapsed Line_101'>
                    <span class='LineNumber '>101</span> <span class='Desc '>Contract sales price</span>
                    <ml:MoneyTextBox ID='sPurchPriceB' class='sPurchPriceB Const ' runat='server' ReadOnly='true'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_102'>
                    <span class='LineNumber '>102</span> <span class='Desc '>Personal property</span>
                    <ml:MoneyTextBox ID='sGrossDueFromBorrPersonalProperty' class='GrossDueFromBorrPersonalProperty_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_103'>
                    <span class='LineNumber '>103</span> <span class='Desc '>Settlement charges to borrower
                        (line 1400)</span>
                    <ml:MoneyTextBox ID='sSettlementTotalEstimateSettlementCharge' class='sSettlementTotalEstimateSettlementCharge Const '
                        runat='server' ReadOnly='true'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_104'>
                    <span class='LineNumber '>104</span>
                    <asp:TextBox ID='sGrossDueFromBorrU1FDesc' class='GrossDueFromBorr Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sGrossDueFromBorrU1F' class='GrossDueFromBorr Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_105'>
                    <span class='LineNumber '>105</span>
                    <asp:TextBox ID='sGrossDueFromBorrU2FDesc' class='GrossDueFromBorr Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sGrossDueFromBorrU2F' class='GrossDueFromBorr Money ' runat='server'></ml:MoneyTextBox>
                </div>
            </div>
            <div id='Block_400' class='Block Seller'>
                <div class='Line Line_400 BlockHeader LoanFormHeader'>
                    <span class='LineNumber '>400</span> <span class='Desc '>Gross Amount Due to Seller</span>
                </div>
                <div class='Line Collapsed Line_401'>
                    <span class='LineNumber '>401</span> <span class='Desc '>Contract sales price</span>
                    <ml:MoneyTextBox ID='sPurchPriceS' class='sPurchPriceS Const ' runat='server' ReadOnly='true'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_402'>
                    <span class='LineNumber '>402</span> <span class='Desc '>Personal property</span>
                    <ml:MoneyTextBox ID='GrossDueFromBorrPersonalPropertyS' class='GrossDueFromBorrPersonalPropertyS Const '
                        runat='server' ReadOnly='true'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_403'>
                    <span class='LineNumber '>403</span>
                    <asp:TextBox ID='sGrossDueToSellerU1FDesc' class='GrossDueToSeller Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sGrossDueToSellerU1F' class='GrossDueToSeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_404'>
                    <span class='LineNumber '>404</span>
                    <asp:TextBox ID='sGrossDueToSellerU2FDesc' class='GrossDueToSeller Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sGrossDueToSellerU2F' class='GrossDueToSeller Money ' runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID='sGrossDueToSellerU2FSameAsBorr' class='GrossDueToSeller SameAsBorr Collapsed'
                        runat='server' Text='Same as borrower'></asp:CheckBox>
                </div>
                <div class='Line Collapsed Line_405'>
                    <span class='LineNumber '>405</span>
                    <asp:TextBox ID='sGrossDueToSellerU3FDesc' class='GrossDueToSeller Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sGrossDueToSellerU3F' class='GrossDueToSeller Money ' runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID='sGrossDueToSellerU3FSameAsBorr' class='GrossDueToSeller SameAsBorr '
                        runat='server' Text='Same as borrower'></asp:CheckBox>
                </div>
            </div>
        </div>
        <div id='Section_100-400' class='Section Collapsed'>
            <div id='Block_100-400' class='Block'>
                <div class='Line Line_100-400 BlockHeader LoanFormHeader'>
                    <input type="checkbox" class='ShowHide' id='Section_100-400ShowHide' />
                    <span class='LineNumber '>100/400</span> <span class='Desc '>Adjustment for items paid
                        by seller in advance</span>
                </div>
                <div class='Line Collapsed Line_106-406'>
                    <span class='LineNumber '>106/406</span> <span class='Desc '>City/town taxes</span>
                    <ml:DateTextBox ID='sPdBySellerCityTaxFStartD' class='PdBySellerCityTaxFStartD_rep '
                        runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sPdBySellerCityTaxFEndD' class='PdBySellerCityTaxFEndD_rep '
                        runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sPdBySellerCityTaxF' class='PdBySellerCityTaxF_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_107-407'>
                    <span class='LineNumber '>107/407</span> <span class='Desc '>County taxes</span>
                    <ml:DateTextBox ID='sPdBySellerCountyTaxFStartD' class='PdBySellerCountyTaxFStartD_rep '
                        runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sPdBySellerCountyTaxFEndD' class='PdBySellerCountyTaxFEndD_rep '
                        runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sPdBySellerCountyTaxF' class='PdBySellerCountyTaxF_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_108-408'>
                    <span class='LineNumber '>108/408</span> <span class='Desc '>Assessments</span>
                    <ml:DateTextBox ID='sPdBySellerAssessmentsTaxFStartD' class='PdBySellerAssessmentsTaxFStartD_rep '
                        runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sPdBySellerAssessmentsTaxFEndD' class='PdBySellerAssessmentsTaxFEndD_rep '
                        runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sPdBySellerAssessmentsTaxF' class='PdBySellerAssessmentsTaxF_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_109-409'>
                    <span class='LineNumber '>109/409</span>
                    <asp:TextBox ID='sPdBySellerU1FDesc' class='PdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sPdBySellerU1FStartD' class='PdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sPdBySellerU1FEndD' class='PdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sPdBySellerU1F' class='PdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_110-410'>
                    <span class='LineNumber '>110/410</span>
                    <asp:TextBox ID='sPdBySellerU2FDesc' class='PdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sPdBySellerU2FStartD' class='PdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sPdBySellerU2FEndD' class='PdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sPdBySellerU2F' class='PdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_111-411'>
                    <span class='LineNumber '>111/411</span>
                    <asp:TextBox ID='sPdBySellerU3FDesc' class='PdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sPdBySellerU3FStartD' class='PdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sPdBySellerU3FEndD' class='PdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sPdBySellerU3F' class='PdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_112-412'>
                    <span class='LineNumber '>112/412</span>
                    <asp:TextBox ID='sPdBySellerU4FDesc' class='PdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sPdBySellerU4FStartD' class='PdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sPdBySellerU4FEndD' class='PdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sPdBySellerU4F' class='PdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
            </div>
        </div>
        <div id='Section_200And500' class='Section Collapsed'>
            <div id='Block_200' class='Block Borrower'>
                <div class='Line Line_200 BlockHeader LoanFormHeader'>
                    <input type="checkbox" class='ShowHide' id='Section_200And500ShowHide' />
                    <span class='LineNumber '>200</span> <span class='Desc '>Amount Paid by or in Behalf
                        of Borrower</span>
                </div>
                <div class='Line Collapsed Line_201'>
                    <span class='LineNumber '>201</span> <span class='Desc '>Deposit or earnest money</span>
                    <ml:MoneyTextBox ID='sTotCashDeposit' class='sTotCashDeposit Const ' runat='server'
                        ReadOnly='true'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_202'>
                    <span class='LineNumber '>202</span> <span class='Desc '>Principal amount of new loan(s)</span>
                    <ml:MoneyTextBox ID='sFinalLAmt' class='sFinalLAmt Const ' runat='server' ReadOnly='true'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_204'>
                    <asp:TextBox ID='sPdByBorrowerU1FHudline' class='PdByBorrower HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sPdByBorrowerU1FDesc' class='PdByBorrower Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sPdByBorrowerU1F' class='PdByBorrower Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_205'>
                    <asp:TextBox ID='sPdByBorrowerU2FHudline' class='PdByBorrower HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sPdByBorrowerU2FDesc' class='PdByBorrower Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sPdByBorrowerU2F' class='PdByBorrower Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_206'>
                    <asp:TextBox ID='sPdByBorrowerU3FHudline' class='PdByBorrower HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sPdByBorrowerU3FDesc' class='PdByBorrower Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sPdByBorrowerU3F' class='PdByBorrower Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_207'>
                    <asp:TextBox ID='sPdByBorrowerU4FHudline' class='PdByBorrower HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sPdByBorrowerU4FDesc' class='PdByBorrower Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sPdByBorrowerU4F' class='PdByBorrower Money ' runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID="sPdByBorrowerU4FRowLckd" class="PdByBorrower Lckd " Text="Lock" onclick="lockField(this,'sPdByBorrowerU4FHudline');lockField(this,'sPdByBorrowerU4FDesc');lockField(this,'sPdByBorrowerU4F');refreshCalculation();" runat="server" />
                </div>
                <div class='Line Collapsed Line_208'>
                    <asp:TextBox ID='sPdByBorrowerU5FHudline' class='PdByBorrower HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sPdByBorrowerU5FDesc' class='PdByBorrower Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sPdByBorrowerU5F' class='PdByBorrower Money ' runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID="sPdByBorrowerU5FRowLckd" class="PdByBorrower Lckd " Text="Lock" onclick="lockField(this,'sPdByBorrowerU5FHudline');lockField(this,'sPdByBorrowerU5FDesc');lockField(this,'sPdByBorrowerU5F');refreshCalculation();" runat="server" />
                </div>
                <div class='Line Collapsed Line_209'>
                    <asp:TextBox ID='sPdByBorrowerU6FHudline' class='PdByBorrower HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sPdByBorrowerU6FDesc' class='PdByBorrower Desc ' runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sPdByBorrowerU6F' class='PdByBorrower Money ' runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID="sPdByBorrowerU6FRowLckd" class="PdByBorrower Lckd " Text="Lock" onclick="lockField(this,'sPdByBorrowerU6FHudline');lockField(this,'sPdByBorrowerU6FDesc');lockField(this,'sPdByBorrowerU6F');refreshCalculation();" runat="server" />
                </div>
            </div>
            <div id='Block_500' class='Block Seller'>
                <div class='Line Line_500 BlockHeader LoanFormHeader'>
                    <span class='LineNumber '>500</span> <span class='Desc '>Reductions In Amount Due to
                        seller</span>
                </div>
                <div class='Line Collapsed Line_501'>
                    <span class='LineNumber '>501</span> <span class='Desc '>Excess deposit</span>
                    <ml:MoneyTextBox ID='sReductionsDueToSellerExcessDeposit' class='ReductionsDueToSellerExcessDeposit_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_502'>
                    <span class='LineNumber '>502</span> <span class='Desc '>Settlement charges to seller</span>
                    <ml:MoneyTextBox ID='ChargesToSeller' class='calc Const ' runat='server' ReadOnly='true'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_504'>
                    <span class='LineNumber '>504</span> <span class='Desc '>Payoff of first mortgage loan</span>
                    <ml:MoneyTextBox ID='sReductionsDueToSellerPayoffOf1stMtgLoan' class='ReductionsDueToSellerPayoffOf1stMtgLoan_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_505'>
                    <span class='LineNumber '>505</span> <span class='Desc '>Payoff of second mortgage loan</span>
                    <ml:MoneyTextBox ID='sReductionsDueToSellerPayoffOf2ndMtgLoan' class='ReductionsDueToSellerPayoffOf2ndMtgLoan_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_506'>
                    <asp:TextBox ID='sReductionsDueToSellerU1FHudline' class='ReductionsDueToSeller HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sReductionsDueToSellerU1FDesc' class='ReductionsDueToSeller Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sReductionsDueToSellerU1F' class='ReductionsDueToSeller Money '
                        runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID='sReductionsDueToSellerU1SameAsBorr' class='ReductionsDueToSeller SameAsBorr '
                        runat='server' Text='Same as borrower'></asp:CheckBox>
                </div>
                <div class='Line Collapsed Line_507'>
                    <asp:TextBox ID='sReductionsDueToSellerU2FHudline' class='ReductionsDueToSeller HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sReductionsDueToSellerU2FDesc' class='ReductionsDueToSeller Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sReductionsDueToSellerU2F' class='ReductionsDueToSeller Money '
                        runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID='sReductionsDueToSellerU2SameAsBorr' class='ReductionsDueToSeller SameAsBorr '
                        runat='server' Text='Same as borrower'></asp:CheckBox>
                </div>
                <div class='Line Collapsed Line_508'>
                    <asp:TextBox ID='sReductionsDueToSellerU3FHudline' class='ReductionsDueToSeller HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sReductionsDueToSellerU3FDesc' class='ReductionsDueToSeller Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sReductionsDueToSellerU3F' class='ReductionsDueToSeller Money '
                        runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID='sReductionsDueToSellerU3SameAsBorr' class='ReductionsDueToSeller SameAsBorr '
                        runat='server' Text='Same as borrower'></asp:CheckBox>
                </div>
                <div class='Line Collapsed Line_509'>
                    <asp:TextBox ID='sReductionsDueToSellerU4FHudline' class='ReductionsDueToSeller HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sReductionsDueToSellerU4FDesc' class='ReductionsDueToSeller Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sReductionsDueToSellerU4F' class='ReductionsDueToSeller Money '
                        runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID='sReductionsDueToSellerU4SameAsBorr' class='ReductionsDueToSeller SameAsBorr '
                        runat='server' Text='Same as borrower'></asp:CheckBox>
                </div>
                <div class='Line Collapsed Line_510'>
                    <asp:TextBox ID='sReductionsDueToSellerU5FHudline' class='ReductionsDueToSeller HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sReductionsDueToSellerU5FDesc' class='ReductionsDueToSeller Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sReductionsDueToSellerU5F' class='ReductionsDueToSeller Money '
                        runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID='sReductionsDueToSellerU5SameAsBorr' class='ReductionsDueToSeller SameAsBorr '
                        runat='server' Text='Same as borrower'></asp:CheckBox>
                </div>
                <div class='Line Collapsed Line_511'>
                    <asp:TextBox ID='sReductionsDueToSellerU6FHudline' class='ReductionsDueToSeller HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sReductionsDueToSellerU6FDesc' class='ReductionsDueToSeller Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sReductionsDueToSellerU6F' class='ReductionsDueToSeller Money '
                        runat='server'></ml:MoneyTextBox>
                    <asp:CheckBox ID='sReductionsDueToSellerU6SameAsBorr' class='ReductionsDueToSeller SameAsBorr '
                        runat='server' Text='Same as borrower'></asp:CheckBox>
                </div>
            </div>
        </div>
        <div id='Section_200-500' class='Section Collapsed'>
            <div id='Block_200-500' class='Block'>
                <div class='Line Line_200-500 BlockHeader LoanFormHeader'>
                    <input type="checkbox" class='ShowHide' id='Section_200-500ShowHide' />
                    <span class='LineNumber '>200/500</span> <span class='Desc '>Adjustments for items unpaid
                        by seller</span>
                </div>
                <div class='Line Collapsed Line_210-510'>
                    <span class='LineNumber '>210/510</span> <span class='Desc '>City/town taxes</span>
                    <ml:DateTextBox ID='sUnPdBySellerCityTaxFStartD' class='UnPdBySellerCityTaxFStartD_rep '
                        runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sUnPdBySellerCityTaxFEndD' class='UnPdBySellerCityTaxFEndD_rep '
                        runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sUnPdBySellerCityTaxF' class='UnPdBySellerCityTaxF_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_211-511'>
                    <span class='LineNumber '>211/511</span> <span class='Desc '>County taxes</span>
                    <ml:DateTextBox ID='sUnPdBySellerCountyTaxFStartD' class='UnPdBySellerCountyTaxFStartD_rep '
                        runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sUnPdBySellerCountyTaxFEndD' class='UnPdBySellerCountyTaxFEndD_rep '
                        runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sUnPdBySellerCountyTaxF' class='UnPdBySellerCountyTaxF_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_212-512'>
                    <span class='LineNumber '>212/512</span> <span class='Desc '>Assessments</span>
                    <ml:DateTextBox ID='sUnPdBySellerAssessmentsTaxFStartD' class='UnPdBySellerAssessmentsTaxFStartD_rep '
                        runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sUnPdBySellerAssessmentsTaxFEndD' class='UnPdBySellerAssessmentsTaxFEndD_rep '
                        runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sUnPdBySellerAssessmentsTaxF' class='UnPdBySellerAssessmentsTaxF_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_213-513'>
                    <span class='LineNumber '>213/513</span>
                    <asp:TextBox ID='sUnPdBySellerU1FDesc' class='UnPdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sUnPdBySellerU1FStartD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sUnPdBySellerU1FEndD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sUnPdBySellerU1F' class='UnPdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_214-514'>
                    <span class='LineNumber '>214/514</span>
                    <asp:TextBox ID='sUnPdBySellerU2FDesc' class='UnPdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sUnPdBySellerU2FStartD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sUnPdBySellerU2FEndD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sUnPdBySellerU2F' class='UnPdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_215-515'>
                    <span class='LineNumber '>215/515</span>
                    <asp:TextBox ID='sUnPdBySellerU3FDesc' class='UnPdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sUnPdBySellerU3FStartD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sUnPdBySellerU3FEndD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sUnPdBySellerU3F' class='UnPdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_216-516'>
                    <span class='LineNumber '>216/516</span>
                    <asp:TextBox ID='sUnPdBySellerU4FDesc' class='UnPdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sUnPdBySellerU4FStartD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sUnPdBySellerU4FEndD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sUnPdBySellerU4F' class='UnPdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_217-517'>
                    <span class='LineNumber '>217/517</span>
                    <asp:TextBox ID='sUnPdBySellerU5FDesc' class='UnPdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sUnPdBySellerU5FStartD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sUnPdBySellerU5FEndD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sUnPdBySellerU5F' class='UnPdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_218-518'>
                    <span class='LineNumber '>218/518</span>
                    <asp:TextBox ID='sUnPdBySellerU6FDesc' class='UnPdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sUnPdBySellerU6FStartD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sUnPdBySellerU6FEndD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sUnPdBySellerU6F' class='UnPdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_219-519'>
                    <span class='LineNumber '>219/519</span>
                    <asp:TextBox ID='sUnPdBySellerU7FDesc' class='UnPdBySeller Desc ' runat='server'></asp:TextBox>
                    <ml:DateTextBox ID='sUnPdBySellerU7FStartD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <span class="DateTo">to</span>
                    <ml:DateTextBox ID='sUnPdBySellerU7FEndD' class='UnPdBySeller ' runat='server'></ml:DateTextBox>
                    <ml:MoneyTextBox ID='sUnPdBySellerU7F' class='UnPdBySeller Money ' runat='server'></ml:MoneyTextBox>
                </div>
            </div>
        </div>
        <div id='Section_700' class='Section Collapsed'>
            <div id='Block_700' class='Block'>
                <div class='Line Line_700 BlockHeader LoanFormHeader'>
                    <input type="checkbox" class='ShowHide' id='Section_700ShowHide' />
                    <span class='LineNumber '>700</span> <span class='Desc '>Total Real Estate Broker Fees</span>
                </div>
                <div class='Line Collapsed Desc PaidFromBorr Desc PaidFromSeller'>
                    <span class='Desc PaidFromBorr '>Paid from borrower's funds at settlement</span>
                    <span class='Desc PaidFromSeller '>Paid from seller's funds at settlement</span>
                </div>
                <div class='Line Collapsed Line_701'>
                    <span class='LineNumber '>701</span>
                    <ml:MoneyTextBox ID='sRealEstateBrokerFees1F' class='RealEstateBrokerFees1F_rep Money '
                        runat='server'></ml:MoneyTextBox>
                    to
                    <asp:TextBox ID='sRealEstateBrokerFees1FAgentName' class='RealEstateBrokerFees1FAgentName Agent '
                        runat='server'></asp:TextBox>
                    <UC:CFM ID="BuyerAgent" Name="BuyerAgent" runat="server" AgentNameField="sRealEstateBrokerFees1FAgentName"
                        Type="32" DisableAddTo="true"></UC:CFM>
                </div>
                <div class='Line Collapsed Line_702'>
                    <span class='LineNumber '>702</span>
                    <ml:MoneyTextBox ID='sRealEstateBrokerFees2F' class='RealEstateBrokerFees2F_rep Money '
                        runat='server'></ml:MoneyTextBox>
                    to
                    <asp:TextBox ID='sRealEstateBrokerFees2FAgentName' class='RealEstateBrokerFees2FAgentName Agent '
                        runat='server'></asp:TextBox>
                    <UC:CFM ID="SellerAgent" Name="SellerAgent" runat="server" AgentNameField="sRealEstateBrokerFees2FAgentName"
                        Type="7"></UC:CFM>
                </div>
                <div class='Line Collapsed Line_703'>
                    <span class='LineNumber '>703</span> <span class='Desc '>Commission paid at settlement</span>
                    <ml:MoneyTextBox ID='sRealEstateBrokerFeesCommissionPdAtSettlementFFromSeller'
                        class='RealEstateBrokerFeesCommissionPdAtSettlementFFromSeller_rep Money ' runat='server'></ml:MoneyTextBox>
                    <ml:MoneyTextBox ID='sRealEstateBrokerFeesCommissionPdAtSettlementFFromBorr' class='RealEstateBrokerFeesCommissionPdAtSettlementFFromBorr_rep Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_704'>
                    <span class='LineNumber '>704</span>
                    <asp:TextBox ID='sRealEstateBrokerFeesU1FDesc' class='RealEstateBrokerFees Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sRealEstateBrokerFeesU1FFromSeller' class='RealEstateBrokerFees Money '
                        runat='server'></ml:MoneyTextBox>
                    <ml:MoneyTextBox ID='sRealEstateBrokerFeesU1FFromBorr' class='RealEstateBrokerFees Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
            </div>
        </div>
        <div id='SectionColumn' class='Section Collapsed' runat="server">
            <div id='Block_Column' class='Block'>
                <div class='Line Desc BlockHeader LoanFormHeader'>
                    <input type="checkbox" class='ShowHide' id='Section_ColumnShowHide' />
                    <span class='Desc '>Seller's Column Settlement Charges</span>
                </div>
                <div class='Line Collapsed Line_801'>
                    <asp:TextBox ID='sSellerSettlementChargesU01FHudline' class='SellerSettlementCharges HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sSellerSettlementChargesU01FDesc' class='SellerSettlementCharges Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sSellerSettlementChargesU01F' class='SellerSettlementCharges Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_802'>
                    <asp:TextBox ID='sSellerSettlementChargesU02FHudline' class='SellerSettlementCharges HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sSellerSettlementChargesU02FDesc' class='SellerSettlementCharges Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sSellerSettlementChargesU02F' class='SellerSettlementCharges Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_803'>
                    <asp:TextBox ID='sSellerSettlementChargesU03FHudline' class='SellerSettlementCharges HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sSellerSettlementChargesU03FDesc' class='SellerSettlementCharges Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sSellerSettlementChargesU03F' class='SellerSettlementCharges Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_804'>
                    <asp:TextBox ID='sSellerSettlementChargesU04FHudline' class='SellerSettlementCharges HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sSellerSettlementChargesU04FDesc' class='SellerSettlementCharges Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sSellerSettlementChargesU04F' class='SellerSettlementCharges Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_805'>
                    <asp:TextBox ID='sSellerSettlementChargesU05FHudline' class='SellerSettlementCharges HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sSellerSettlementChargesU05FDesc' class='SellerSettlementCharges Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sSellerSettlementChargesU05F' class='SellerSettlementCharges Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_806'>
                    <asp:TextBox ID='sSellerSettlementChargesU06FHudline' class='SellerSettlementCharges HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sSellerSettlementChargesU06FDesc' class='SellerSettlementCharges Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sSellerSettlementChargesU06F' class='SellerSettlementCharges Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_807'>
                    <asp:TextBox ID='sSellerSettlementChargesU07FHudline' class='SellerSettlementCharges HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sSellerSettlementChargesU07FDesc' class='SellerSettlementCharges Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sSellerSettlementChargesU07F' class='SellerSettlementCharges Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_808'>
                    <asp:TextBox ID='sSellerSettlementChargesU08FHudline' class='SellerSettlementCharges HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sSellerSettlementChargesU08FDesc' class='SellerSettlementCharges Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sSellerSettlementChargesU08F' class='SellerSettlementCharges Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_809'>
                    <asp:TextBox ID='sSellerSettlementChargesU09FHudline' class='SellerSettlementCharges HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sSellerSettlementChargesU09FDesc' class='SellerSettlementCharges Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sSellerSettlementChargesU09F' class='SellerSettlementCharges Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_810'>
                    <asp:TextBox ID='sSellerSettlementChargesU10FHudline' class='SellerSettlementCharges HUDLine LineNumber'
                        runat='server'></asp:TextBox>
                    <asp:TextBox ID='sSellerSettlementChargesU10FDesc' class='SellerSettlementCharges Desc '
                        runat='server'></asp:TextBox>
                    <ml:MoneyTextBox ID='sSellerSettlementChargesU10F' class='SellerSettlementCharges Money '
                        runat='server'></ml:MoneyTextBox>
                </div>
                <div class='Line Collapsed Line_Total'>
                    Total:
                    <ml:MoneyTextBox ID='Total_800' runat='server' ReadOnly='true'></ml:MoneyTextBox>
                </div>
            </div>
        </div>
        <asp:Panel runat="server" id='Section_FHA' class='Section Collapsed' Visible="false">
            <div id='Block_FHA' class='Block'>
                <div class='Line Desc BlockHeader LoanFormHeader'>
                    <input type="checkbox" class='ShowHide' id='Section_FHAShowHide' />
                    <span class='Desc '>FHA Addendum to HUD-1</span>
                </div>
                <div class="Line Collapsed">
                    <span class="Desc FullWidth">Source of funds other than from borrower</span>
                </div>
                <div class="Line Collapsed">
                    <asp:TextBox runat="server" ID="sFhaAddendToHud1NonBorrSrcFunds" TextMode="MultiLine" Rows="4" Width="100%">
                    </asp:TextBox>
                    <asp:RegularExpressionValidator ID="rev_sFhaAddendToHud1NonBorrSrcFunds_LengthValidator"
                                ControlToValidate="sFhaAddendToHud1NonBorrSrcFunds" ErrorMessage="Source of funds can not exceed 250 characters"
                                ValidationExpression="^[\s\S]{0,250}$" runat="server" Display="Dynamic" SetFocusOnError="true" />
                    
                </div>
            </div>
        </asp:Panel>
        
    </div>
    </form>
</body>
</html>
