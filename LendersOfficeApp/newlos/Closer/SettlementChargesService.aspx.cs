﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
using System.Data.SqlClient;

namespace LendersOfficeApp.newlos.Closer
{
    public class SettlementChargesServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(SettlementChargesServiceItem));
        }

        protected BrokerDB Broker
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerDB; }
        }

        protected bool IsClosingCostMigrationArchivePage
        {
            get 
            {
                return this.Broker.IsGFEandCoCVersioningEnabled
                    && (GetString("hfIsClosingCostMigrationArchivePage") == "true"); 
            }
        }


        // 2/26/14 gf - opm 150695, NOTE: the To Broker checkbox does exist on the SC page,
        // but it's value is always set to the GFE props field.
        private int RetrieveItemProps(LosConvert convert, string name, int originalProps)
        {
            bool fhaAllow = LosConvert.GfeItemProps_FhaAllow(originalProps);
            bool toBr = LosConvert.GfeItemProps_ToBr(originalProps);
            bool bf = LosConvert.GfeItemProps_BF(originalProps);
            bool gbf = LosConvert.GfeItemProps_GBF(originalProps);
            int payer = GetInt(name + "_ctrl_PdByT_dd", 0);
            bool poc = GetBool(name + "_ctrl_Poc_chk", false);
            bool dflp = GetBool(name + "_ctrl_Dflp_chk", false);
            bool borr = GetBool(name + "_ctrl_Borr_chk", false);

            bool apr = GetString(name + "_ctrl_Apr_hdn", "N") == "Y";
            bool trdPty = GetString(name + "_ctrl_TrdPty_hdn", "N") == "Y";
            bool aff = GetString(name + "_ctrl_Aff_hdn", "N") == "Y";
            bool qmWarn = GetString(name + "_ctrl_QmWarn_hdn", "N") == "Y";

            return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, dflp, gbf, bf, borr, trdPty, aff, qmWarn);
        }

        // 2/26/14 gf - opm 150695, Since it is now possible to combine the GFE/SC pages, we
        // do not want to stomp the FHA value from the original props.
        //private int RetrieveItemProps(LosConvert convert, string name)
        //{
        //    bool fhaAllow = false;
        //    bool toBr = false;
        //    int payer = GetInt(name + "_ctrl_PdByT_dd", 0);
        //    bool poc = GetBool(name + "_ctrl_Poc_chk", false);
        //    bool dflp = GetBool(name + "_ctrl_Dflp_chk", false);
        //    bool borr = GetBool(name + "_ctrl_Borr_chk", false);

        //    bool apr = GetString(name + "_ctrl_Apr_hdn", "N") == "Y";
        //    bool trdPty = GetString(name + "_ctrl_TrdPty_hdn", "N") == "Y";
        //    bool aff = GetString(name + "_ctrl_Aff_hdn", "N") == "Y";
        //    bool qmWarn = GetString(name + "_ctrl_QmWarn_hdn", "N") == "Y";

        //    return LosConvert.GfeItemProps_Pack(apr, toBr, payer, fhaAllow, poc, dflp, false, false, borr, trdPty, aff, qmWarn);
        //}

        private void InitItemProps(string name, int props)
        {
            SetResult(name + "_ctrl_PdByT_dd", LosConvert.GfeItemProps_Payer(props));
            SetResult(name + "_ctrl_Poc_chk", LosConvert.GfeItemProps_Poc(props));
            SetResult(name + "_ctrl_Dflp_chk", LosConvert.GfeItemProps_Dflp(props));
            SetResult(name + "_ctrl_Borr_chk", LosConvert.GfeItemProps_Borr(props));

            SetResult(name + "_ctrl_Apr_hdn", LosConvert.GfeItemProps_Apr(props) ? "Y" : "N");
            SetResult(name + "_ctrl_TrdPty_hdn", LosConvert.GfeItemProps_PaidToThirdParty(props) ? "Y" : "N");
            SetResult(name + "_ctrl_Aff_hdn", LosConvert.GfeItemProps_ThisPartyIsAffiliate(props) ? "Y" : "N");
            SetResult(name + "_ctrl_QmWarn_hdn", LosConvert.GfeItemProps_ShowQmWarning(props) ? "Y" : "N");
        }

        protected override void AfterSaveAndLoadPageDataCallback(CPageData data)
        {
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            bool byPassBgCalcForGfeAsDefault = GetString("ByPassBgCalcForGfeAsDefault", "0") == "1";
            SqlParameter[] parameters = {
                                            new SqlParameter("@ByPassBgCalcForGfeAsDefault", byPassBgCalcForGfeAsDefault)
                                            , new SqlParameter("@UserId", principal.UserId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "SetPassBgCalcForGfeAsDefaultByUserId", 3, parameters);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            if (IsClosingCostMigrationArchivePage)
            {
                Tools.LogError("Programming Error: Should not call services on archive page.");
                return;
            }

            bool heMigrated = dataLoan.sIsHousingExpenseMigrated;
            dataLoan.sSchedDueD1Lckd = GetBool("sSchedDueD1Lckd");
            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");
            dataLoan.sSchedFundD_rep = GetString("sSchedFundD");
            dataLoan.sDaysInYr_rep = GetString("sDaysInYr");

            dataLoan.sSettlement800U5F_rep = GetString("sSettlement800U5F");
            dataLoan.sSettlement800U5FDesc = GetString("sSettlement800U5FDesc");
            dataLoan.sSettlement800U4F_rep = GetString("sSettlement800U4F");
			dataLoan.sSettlement800U4FDesc = GetString("sSettlement800U4FDesc");
			dataLoan.sSettlement800U3F_rep = GetString("sSettlement800U3F");
			dataLoan.sSettlement800U3FDesc = GetString("sSettlement800U3FDesc");
			dataLoan.sSettlement800U2F_rep = GetString("sSettlement800U2F");
			dataLoan.sSettlement800U2FDesc = GetString("sSettlement800U2FDesc");
			dataLoan.sSettlement800U1F_rep = GetString("sSettlement800U1F");
			dataLoan.sSettlement800U1FDesc = GetString("sSettlement800U1FDesc");
			dataLoan.sSettlementWireF_rep = GetString("sSettlementWireF");
			dataLoan.sSettlementUwF_rep = GetString("sSettlementUwF");
			dataLoan.sSettlementProcF_rep = GetString("sSettlementProcF");
			dataLoan.sSettlementProcFPaid = GetBool("sSettlementProcFProps_ctrl_Paid_chk");
			dataLoan.sSettlementTxServF_rep = GetString("sSettlementTxServF");
			dataLoan.sSettlementMBrokFMb_rep = GetString("sSettlementMBrokFMb");
			dataLoan.sSettlementMBrokFPc_rep = GetString("sSettlementMBrokFPc");
            dataLoan.sSettlementMBrokFBaseT = (E_PercentBaseT)GetInt("sSettlementMBrokFBaseT");
            dataLoan.sSettlementInspectF_rep = GetString("sSettlementInspectF");
			dataLoan.sSettlementCrF_rep = GetString("sSettlementCrF");
            dataLoan.sSettlementCrFPaid = GetBool("sSettlementCrFProps_ctrl_Paid_chk");
			dataLoan.sSettlementApprF_rep = GetString("sSettlementApprF");
            dataLoan.sSettlementApprFPaid = GetBool("sSettlementApprFProps_ctrl_Paid_chk");
            dataLoan.sSettlementFloodCertificationF_rep = GetString("sSettlementFloodCertificationF");
			dataLoan.sSettlementLDiscntFMb_rep = GetString("sSettlementLDiscntFMb");
            dataLoan.sSettlementLDiscntPc_rep = GetString("sSettlementLDiscntPc");
            dataLoan.sSettlementLDiscntBaseT = (E_PercentBaseT)GetInt("sSettlementLDiscntBaseT");
            dataLoan.sSettlementCreditLenderPaidItemT = (E_CreditLenderPaidItemT)GetInt("sSettlementCreditLenderPaidItemT");
            dataLoan.sSettlementLOrigFMb_rep = GetString("sSettlementLOrigFMb");
			dataLoan.sSettlementLOrigFPc_rep = GetString("sSettlementLOrigFPc");
			dataLoan.sSettlement900U1Pia_rep = GetString("sSettlement900U1Pia");
			dataLoan.sSettlement900U1PiaDesc = GetString("sSettlement900U1PiaDesc");
			dataLoan.sSettlement904PiaDesc = GetString("sSettlement904PiaDesc");
			dataLoan.sSettlement904Pia_rep = GetString("sSettlement904Pia");
			dataLoan.sSettlementHazInsPiaMon_rep = GetString("sSettlementHazInsPiaMon");
            if (!dataLoan.sHazardExpenseInDisbursementMode)
            {
                dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
                dataLoan.sProHazInsR_rep = GetString("sProHazInsR");
                dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb");
            }
            dataLoan.sSettlementIPiaDy_rep = GetString("sSettlementIPiaDy");
            dataLoan.sSettlementIPiaDyLckd = GetBool("sSettlementIPiaDyLckd");
            dataLoan.sSettlementIPerDayLckd = GetBool("sSettlementIPerDayLckd");
			dataLoan.sSettlementIPerDay_rep = GetString("sSettlementIPerDay");
			dataLoan.sSettlementAggregateAdjRsrv_rep = GetString("sSettlementAggregateAdjRsrv");
            dataLoan.sSettlementAggregateAdjRsrvLckd = GetBool("sSettlementAggregateAdjRsrvLckd");
            if (!heMigrated || dataLoan.sLine1009Expense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
            {
                dataLoan.s1007ProHExp_rep = GetString("s1007ProHExp");
            }
            if (false == dataLoan.sUseGFEDataForSCFields || dataLoan.s1007RsrvMonLckd)
            {
                dataLoan.sSettlement1009RsrvMon_rep = GetString("sSettlement1009RsrvMon");
            }
			dataLoan.s1007ProHExpDesc = GetString("s1007ProHExpDesc");
            if (!heMigrated || dataLoan.sLine1008Expense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
            {
                dataLoan.s1006ProHExp_rep = GetString("s1006ProHExp");
            }
            if (false == dataLoan.sUseGFEDataForSCFields || dataLoan.s1006RsrvMonLckd)
            {
                dataLoan.sSettlement1008RsrvMon_rep = GetString("sSettlement1008RsrvMon");
            }
			dataLoan.s1006ProHExpDesc = GetString("s1006ProHExpDesc");
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                if (!heMigrated || dataLoan.sLine1010Expense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
                {
                    dataLoan.sProU3Rsrv_rep = GetString("sProU3Rsrv");
                }
                if (false == dataLoan.sUseGFEDataForSCFields || dataLoan.sU3RsrvMonLckd)
                {
                    dataLoan.sSettlementU3RsrvMon_rep = GetString("sSettlementU3RsrvMon");
                }
                dataLoan.sU3RsrvDesc = GetString("sU3RsrvDesc");
                if (!heMigrated || dataLoan.sLine1011Expense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
                {
                    dataLoan.sProU4Rsrv_rep = GetString("sProU4Rsrv");
                }
                if (false == dataLoan.sUseGFEDataForSCFields || dataLoan.sU4RsrvMonLckd)
                {
                    dataLoan.sSettlementU4RsrvMon_rep = GetString("sSettlementU4RsrvMon");
                }
                dataLoan.sU4RsrvDesc = GetString("sU4RsrvDesc");
            }
            if (!heMigrated || dataLoan.sFloodExpense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
            {
                dataLoan.sProFloodIns_rep = GetString("sProFloodIns");
            }
            if (false == dataLoan.sUseGFEDataForSCFields || dataLoan.sFloodInsRsrvMonLckd)
            {
                dataLoan.sSettlementFloodInsRsrvMon_rep = GetString("sSettlementFloodInsRsrvMon");
            }
            if (!dataLoan.sRealEstateTaxExpenseInDisbMode)
            {
                dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb");
                dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
                dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
            }
			
            if (false == dataLoan.sUseGFEDataForSCFields || dataLoan.sRealETxRsrvMonLckd)
            {
                dataLoan.sSettlementRealETxRsrvMon_rep = GetString("sSettlementRealETxRsrvMon");
            }
            if (!heMigrated || dataLoan.sSchoolTaxExpense.AnnualAmtCalcType != E_AnnualAmtCalcTypeT.Disbursements)
            {
                dataLoan.sProSchoolTx_rep = GetString("sProSchoolTx");
            }
            if (false == dataLoan.sUseGFEDataForSCFields || dataLoan.sSchoolTxRsrvMonLckd)
            {
                dataLoan.sSettlementSchoolTxRsrvMon_rep = GetString("sSettlementSchoolTxRsrvMon");
            }
            if (false == dataLoan.sUseGFEDataForSCFields || dataLoan.sMInsRsrvMonLckd)
            {
                dataLoan.sSettlementMInsRsrvMon_rep = GetString("sSettlementMInsRsrvMon");
            }
            if (false == dataLoan.sUseGFEDataForSCFields || dataLoan.sHazInsRsrvMonLckd)
            {
                dataLoan.sSettlementHazInsRsrvMon_rep = GetString("sSettlementHazInsRsrvMon");
            }
			dataLoan.sSettlementU4Tc_rep = GetString("sSettlementU4Tc");
			dataLoan.sSettlementU4TcDesc = GetString("sSettlementU4TcDesc");
			dataLoan.sSettlementU3Tc_rep = GetString("sSettlementU3Tc");
			dataLoan.sSettlementU3TcDesc = GetString("sSettlementU3TcDesc");
			dataLoan.sSettlementU2Tc_rep = GetString("sSettlementU2Tc");
			dataLoan.sSettlementU2TcDesc = GetString("sSettlementU2TcDesc");
			dataLoan.sSettlementU1Tc_rep = GetString("sSettlementU1Tc");
			dataLoan.sSettlementU1TcDesc = GetString("sSettlementU1TcDesc");
            dataLoan.sSettlementOwnerTitleInsF_rep = GetString("sSettlementOwnerTitleInsF");
            dataLoan.sSettlementTitleInsF_rep = GetString("sSettlementTitleInsF");
			dataLoan.sSettlementAttorneyF_rep = GetString("sSettlementAttorneyF");
			dataLoan.sSettlementNotaryF_rep = GetString("sSettlementNotaryF");
            dataLoan.sSettlementDocPrepF_rep = GetString("sSettlementDocPrepF");
			dataLoan.sSettlementEscrowF_rep = GetString("sSettlementEscrowF");
			dataLoan.sSettlementU3GovRtcMb_rep = GetString("sSettlementU3GovRtcMb");
            dataLoan.sSettlementU3GovRtcBaseT = (E_PercentBaseT)GetInt("sSettlementU3GovRtcBaseT");
			dataLoan.sSettlementU3GovRtcPc_rep = GetString("sSettlementU3GovRtcPc");
			dataLoan.sSettlementU3GovRtcDesc = GetString("sSettlementU3GovRtcDesc");
			dataLoan.sSettlementU2GovRtcMb_rep = GetString("sSettlementU2GovRtcMb");
            dataLoan.sSettlementU2GovRtcBaseT = (E_PercentBaseT)GetInt("sSettlementU2GovRtcBaseT");
			dataLoan.sSettlementU2GovRtcPc_rep = GetString("sSettlementU2GovRtcPc");
			dataLoan.sSettlementU2GovRtcDesc = GetString("sSettlementU2GovRtcDesc");
			dataLoan.sSettlementU1GovRtcMb_rep = GetString("sSettlementU1GovRtcMb");
            dataLoan.sSettlementU1GovRtcBaseT = (E_PercentBaseT)GetInt("sSettlementU1GovRtcBaseT");
			dataLoan.sSettlementU1GovRtcPc_rep = GetString("sSettlementU1GovRtcPc");
			dataLoan.sSettlementU1GovRtcDesc = GetString("sSettlementU1GovRtcDesc");
			dataLoan.sSettlementStateRtcMb_rep = GetString("sSettlementStateRtcMb");
            dataLoan.sSettlementStateRtcBaseT = (E_PercentBaseT)GetInt("sSettlementStateRtcBaseT");
			dataLoan.sSettlementStateRtcPc_rep = GetString("sSettlementStateRtcPc");
			dataLoan.sSettlementCountyRtcMb_rep = GetString("sSettlementCountyRtcMb");
            dataLoan.sSettlementCountyRtcBaseT = (E_PercentBaseT)GetInt("sSettlementCountyRtcBaseT");
			dataLoan.sSettlementCountyRtcPc_rep = GetString("sSettlementCountyRtcPc");
			dataLoan.sSettlementRecFMb_rep = GetString("sSettlementRecFMb");
            dataLoan.sSettlementRecBaseT = (E_PercentBaseT)GetInt("sSettlementRecBaseT");
			dataLoan.sSettlementRecFPc_rep = GetString("sSettlementRecFPc");
            dataLoan.sSettlementRecFLckd = GetBool("sSettlementRecFLckd");
            dataLoan.sSettlementRecDeed_rep = GetString("sSettlementRecDeed");
            dataLoan.sSettlementRecMortgage_rep = GetString("sSettlementRecMortgage");
            dataLoan.sSettlementRecRelease_rep = GetString("sSettlementRecRelease");
			dataLoan.sSettlementU5Sc_rep = GetString("sSettlementU5Sc");
			dataLoan.sSettlementU5ScDesc = GetString("sSettlementU5ScDesc");
			dataLoan.sSettlementU4Sc_rep = GetString("sSettlementU4Sc");
			dataLoan.sSettlementU4ScDesc = GetString("sSettlementU4ScDesc");
			dataLoan.sSettlementU3Sc_rep = GetString("sSettlementU3Sc");
			dataLoan.sSettlementU3ScDesc = GetString("sSettlementU3ScDesc");
			dataLoan.sSettlementU2Sc_rep = GetString("sSettlementU2Sc");
			dataLoan.sSettlementU2ScDesc = GetString("sSettlementU2ScDesc");
			dataLoan.sSettlementU1Sc_rep = GetString("sSettlementU1Sc");
			dataLoan.sSettlementU1ScDesc = GetString("sSettlementU1ScDesc");
			dataLoan.sSettlementPestInspectF_rep = GetString("sSettlementPestInspectF");

            dataLoan.sSettlementLOrigFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementLOrigFProps", dataLoan.sSettlementLOrigFProps);
            dataLoan.sSettlementBrokerCompProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementBrokerCompProps", dataLoan.sSettlementBrokerCompProps); // eh?
            dataLoan.sSettlementLOrigBrokerCreditFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementLOrigBrokerCreditFProps", dataLoan.sSettlementLOrigBrokerCreditFProps); // wot?
            dataLoan.sGfeOriginatorCompFProps = RetrieveItemProps(dataLoan.m_convertLos, "sGfeOriginatorCompFProps", dataLoan.sGfeOriginatorCompFProps);
            dataLoan.sSettlementLDiscntProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementLDiscntProps", dataLoan.sSettlementLDiscntProps);
            dataLoan.sSettlementDiscountPointFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementDiscountPointFProps", dataLoan.sSettlementDiscountPointFProps);
            dataLoan.sSettlementApprFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementApprFProps", dataLoan.sSettlementApprFProps);
            dataLoan.sSettlementCrFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementCrFProps", dataLoan.sSettlementCrFProps);
            dataLoan.sSettlementInspectFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementInspectFProps", dataLoan.sSettlementInspectFProps);
            dataLoan.sSettlementMBrokFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementMBrokFProps", dataLoan.sSettlementMBrokFProps);
            dataLoan.sSettlementTxServFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementTxServFProps", dataLoan.sSettlementTxServFProps);
            dataLoan.sSettlementFloodCertificationFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementFloodCertificationFProps", dataLoan.sSettlementFloodCertificationFProps);
            dataLoan.sSettlementProcFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementProcFProps", dataLoan.sSettlementProcFProps);
            dataLoan.sSettlementUwFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementUwFProps", dataLoan.sSettlementUwFProps);
            dataLoan.sSettlementWireFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementWireFProps", dataLoan.sSettlementWireFProps);
            dataLoan.sSettlement800U1FProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlement800U1FProps", dataLoan.sSettlement800U1FProps);
            dataLoan.sSettlement800U2FProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlement800U2FProps", dataLoan.sSettlement800U2FProps);
            dataLoan.sSettlement800U3FProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlement800U3FProps", dataLoan.sSettlement800U3FProps);
            dataLoan.sSettlement800U4FProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlement800U4FProps", dataLoan.sSettlement800U4FProps);
            dataLoan.sSettlement800U5FProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlement800U5FProps", dataLoan.sSettlement800U5FProps);
            dataLoan.sSettlementIPiaProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementIPiaProps", dataLoan.sSettlementIPiaProps);
            dataLoan.sMipPiaProps = RetrieveItemProps(dataLoan.m_convertLos, "sMipPiaProps", dataLoan.sMipPiaProps);
            dataLoan.sSettlementHazInsPiaProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementHazInsPiaProps", dataLoan.sSettlementHazInsPiaProps);
            dataLoan.sSettlement904PiaProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlement904PiaProps", dataLoan.sSettlement904PiaProps);
            dataLoan.sVaFfProps = RetrieveItemProps(dataLoan.m_convertLos, "sVaFfProps", dataLoan.sVaFfProps);
            dataLoan.sSettlement900U1PiaProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlement900U1PiaProps", dataLoan.sSettlement900U1PiaProps);
            dataLoan.sSettlementHazInsRsrvProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementHazInsRsrvProps", dataLoan.sSettlementHazInsRsrvProps);
            dataLoan.sSettlementMInsRsrvProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementMInsRsrvProps", dataLoan.sSettlementMInsRsrvProps);
            dataLoan.sSettlementSchoolTxRsrvProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementSchoolTxRsrvProps", dataLoan.sSettlementSchoolTxRsrvProps);
            dataLoan.sSettlementRealETxRsrvProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementRealETxRsrvProps", dataLoan.sSettlementRealETxRsrvProps);
            dataLoan.sSettlementFloodInsRsrvProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementFloodInsRsrvProps", dataLoan.sSettlementFloodInsRsrvProps);
            dataLoan.sSettlement1008RsrvProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlement1008RsrvProps", dataLoan.sSettlement1008RsrvProps);
            dataLoan.sSettlement1009RsrvProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlement1009RsrvProps", dataLoan.sSettlement1009RsrvProps);
            if (Broker.EnableAdditionalSection1000CustomFees)
            {
                dataLoan.sSettlementU3RsrvProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU3RsrvProps", dataLoan.sSettlementU3RsrvProps);
                dataLoan.sSettlementU4RsrvProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU4RsrvProps", dataLoan.sSettlementU4RsrvProps);
            }
            dataLoan.sSettlementAggregateAdjRsrvProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementAggregateAdjRsrvProps", dataLoan.sSettlementAggregateAdjRsrvProps);
            dataLoan.sSettlementEscrowFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementEscrowFProps", dataLoan.sSettlementEscrowFProps);
            dataLoan.sSettlementOwnerTitleInsFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementOwnerTitleInsFProps", dataLoan.sSettlementOwnerTitleInsFProps);
            dataLoan.sSettlementDocPrepFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementDocPrepFProps", dataLoan.sSettlementDocPrepFProps);
            dataLoan.sSettlementNotaryFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementNotaryFProps", dataLoan.sSettlementNotaryFProps);
            dataLoan.sSettlementAttorneyFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementAttorneyFProps", dataLoan.sSettlementAttorneyFProps);
            dataLoan.sSettlementTitleInsFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementTitleInsFProps", dataLoan.sSettlementTitleInsFProps);
            dataLoan.sSettlementU1TcProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU1TcProps", dataLoan.sSettlementU1TcProps);
            dataLoan.sSettlementU2TcProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU2TcProps", dataLoan.sSettlementU2TcProps);
            dataLoan.sSettlementU3TcProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU3TcProps", dataLoan.sSettlementU3TcProps);
            dataLoan.sSettlementU4TcProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU4TcProps", dataLoan.sSettlementU4TcProps);
            dataLoan.sSettlementRecFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementRecFProps", dataLoan.sSettlementRecFProps);
            dataLoan.sSettlementCountyRtcProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementCountyRtcProps", dataLoan.sSettlementCountyRtcProps);
            dataLoan.sSettlementStateRtcProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementStateRtcProps", dataLoan.sSettlementStateRtcProps);
            dataLoan.sSettlementU1GovRtcProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU1GovRtcProps", dataLoan.sSettlementU1GovRtcProps);
            dataLoan.sSettlementU2GovRtcProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU2GovRtcProps", dataLoan.sSettlementU2GovRtcProps);
            dataLoan.sSettlementU3GovRtcProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU3GovRtcProps", dataLoan.sSettlementU3GovRtcProps);
            dataLoan.sSettlementPestInspectFProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementPestInspectFProps", dataLoan.sSettlementPestInspectFProps);
            dataLoan.sSettlementU1ScProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU1ScProps", dataLoan.sSettlementU1ScProps);
            dataLoan.sSettlementU2ScProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU2ScProps", dataLoan.sSettlementU2ScProps);
            dataLoan.sSettlementU3ScProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU3ScProps", dataLoan.sSettlementU3ScProps);
            dataLoan.sSettlementU4ScProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU4ScProps", dataLoan.sSettlementU4ScProps);
            dataLoan.sSettlementU5ScProps = RetrieveItemProps(dataLoan.m_convertLos, "sSettlementU5ScProps", dataLoan.sSettlementU5ScProps);


            // OPM 144673 - Add Paid To fields to Settlement Charges
            dataLoan.sApprFPaidTo = GetString("sSettlementApprFProps_ctrl_PaidTo_tb");
            dataLoan.sCrFPaidTo = GetString("sSettlementCrFProps_ctrl_PaidTo_tb");
            dataLoan.sTxServFPaidTo = GetString("sSettlementTxServFProps_ctrl_PaidTo_tb");
            dataLoan.sFloodCertificationFPaidTo = GetString("sSettlementFloodCertificationFProps_ctrl_PaidTo_tb");
            dataLoan.sInspectFPaidTo = GetString("sSettlementInspectFProps_ctrl_PaidTo_tb");
            dataLoan.sProcFPaidTo = GetString("sSettlementProcFProps_ctrl_PaidTo_tb");
            dataLoan.sUwFPaidTo = GetString("sSettlementUwFProps_ctrl_PaidTo_tb");
            dataLoan.sWireFPaidTo = GetString("sSettlementWireFProps_ctrl_PaidTo_tb");
            dataLoan.s800U1FPaidTo = GetString("sSettlement800U1FProps_ctrl_PaidTo_tb");
            dataLoan.s800U2FPaidTo = GetString("sSettlement800U2FProps_ctrl_PaidTo_tb");
            dataLoan.s800U3FPaidTo = GetString("sSettlement800U3FProps_ctrl_PaidTo_tb");
            dataLoan.s800U4FPaidTo = GetString("sSettlement800U4FProps_ctrl_PaidTo_tb");
            dataLoan.s800U5FPaidTo = GetString("sSettlement800U5FProps_ctrl_PaidTo_tb");
            dataLoan.sOwnerTitleInsPaidTo = GetString("sSettlementOwnerTitleInsFProps_ctrl_PaidTo_tb");
            dataLoan.sDocPrepFPaidTo = GetString("sSettlementDocPrepFProps_ctrl_PaidTo_tb");
            dataLoan.sNotaryFPaidTo = GetString("sSettlementNotaryFProps_ctrl_PaidTo_tb");
            dataLoan.sAttorneyFPaidTo = GetString("sSettlementAttorneyFProps_ctrl_PaidTo_tb");
            dataLoan.sU1TcPaidTo = GetString("sSettlementU1TcProps_ctrl_PaidTo_tb");
            dataLoan.sU2TcPaidTo = GetString("sSettlementU2TcProps_ctrl_PaidTo_tb");
            dataLoan.sU3TcPaidTo = GetString("sSettlementU3TcProps_ctrl_PaidTo_tb");
            dataLoan.sU4TcPaidTo = GetString("sSettlementU4TcProps_ctrl_PaidTo_tb");
            dataLoan.sU1GovRtcPaidTo = GetString("sSettlementU1GovRtcProps_ctrl_PaidTo_tb");
            dataLoan.sU2GovRtcPaidTo = GetString("sSettlementU2GovRtcProps_ctrl_PaidTo_tb");
            dataLoan.sU3GovRtcPaidTo = GetString("sSettlementU3GovRtcProps_ctrl_PaidTo_tb");
            dataLoan.sPestInspectPaidTo = GetString("sSettlementPestInspectFProps_ctrl_PaidTo_tb");
            dataLoan.sU1ScPaidTo = GetString("sSettlementU1ScProps_ctrl_PaidTo_tb");
            dataLoan.sU2ScPaidTo = GetString("sSettlementU2ScProps_ctrl_PaidTo_tb");
            dataLoan.sU3ScPaidTo = GetString("sSettlementU3ScProps_ctrl_PaidTo_tb");
            dataLoan.sU4ScPaidTo = GetString("sSettlementU4ScProps_ctrl_PaidTo_tb");
            dataLoan.sU5ScPaidTo = GetString("sSettlementU5ScProps_ctrl_PaidTo_tb");
            dataLoan.sHazInsPiaPaidTo = GetString("sSettlementHazInsPiaProps_ctrl_PaidTo_tb");
            dataLoan.sMipPiaPaidTo = GetString("sMipPiaProps_ctrl_PaidTo_tb");
            dataLoan.sVaFfPaidTo = GetString("sVaFfProps_ctrl_PaidTo_tb");

            dataLoan.sTitleInsFTable = GetString("sSettlementTitleInsFProps_ctrl_PaidTo_tb");
            dataLoan.sEscrowFTable = GetString("sSettlementEscrowFProps_ctrl_PaidTo_tb");
            dataLoan.sStateRtcDesc = GetString("sSettlementStateRtcProps_ctrl_PaidTo_tb");
            dataLoan.sCountyRtcDesc = GetString("sSettlementCountyRtcProps_ctrl_PaidTo_tb");
            dataLoan.sRecFDesc = GetString("sSettlementRecFProps_ctrl_PaidTo_tb");

            dataLoan.sApprFProps_ToBroker = GetBool("sSettlementApprFProps_ctrl_ToBrok_chk");
            dataLoan.sCrFProps_ToBroker = GetBool("sSettlementCrFProps_ctrl_ToBrok_chk");
            dataLoan.sTxServFProps_ToBroker = GetBool("sSettlementTxServFProps_ctrl_ToBrok_chk");
            dataLoan.sFloodCertificationFProps_ToBroker = GetBool("sSettlementFloodCertificationFProps_ctrl_ToBrok_chk");
            dataLoan.sInspectFProps_ToBroker = GetBool("sSettlementInspectFProps_ctrl_ToBrok_chk");
            dataLoan.sProcFProps_ToBroker = GetBool("sSettlementProcFProps_ctrl_ToBrok_chk");
            dataLoan.sUwFProps_ToBroker = GetBool("sSettlementUwFProps_ctrl_ToBrok_chk");
            dataLoan.sWireFProps_ToBroker = GetBool("sSettlementWireFProps_ctrl_ToBrok_chk");
            dataLoan.s800U1FProps_ToBroker = GetBool("sSettlement800U1FProps_ctrl_ToBrok_chk");
            dataLoan.s800U2FProps_ToBroker = GetBool("sSettlement800U2FProps_ctrl_ToBrok_chk");
            dataLoan.s800U3FProps_ToBroker = GetBool("sSettlement800U3FProps_ctrl_ToBrok_chk");
            dataLoan.s800U4FProps_ToBroker = GetBool("sSettlement800U4FProps_ctrl_ToBrok_chk");
            dataLoan.s800U5FProps_ToBroker = GetBool("sSettlement800U5FProps_ctrl_ToBrok_chk");
            dataLoan.sOwnerTitleInsProps_ToBroker = GetBool("sSettlementOwnerTitleInsFProps_ctrl_ToBrok_chk");
            dataLoan.sDocPrepFProps_ToBroker = GetBool("sSettlementDocPrepFProps_ctrl_ToBrok_chk");
            dataLoan.sNotaryFProps_ToBroker = GetBool("sSettlementNotaryFProps_ctrl_ToBrok_chk");
            dataLoan.sAttorneyFProps_ToBroker = GetBool("sSettlementAttorneyFProps_ctrl_ToBrok_chk");
            dataLoan.sU1TcProps_ToBroker = GetBool("sSettlementU1TcProps_ctrl_ToBrok_chk");
            dataLoan.sU2TcProps_ToBroker = GetBool("sSettlementU2TcProps_ctrl_ToBrok_chk");
            dataLoan.sU3TcProps_ToBroker = GetBool("sSettlementU3TcProps_ctrl_ToBrok_chk");
            dataLoan.sU4TcProps_ToBroker = GetBool("sSettlementU4TcProps_ctrl_ToBrok_chk");
            dataLoan.sGfeOriginatorCompFProps_ToBroker = GetBool("sGfeOriginatorCompFProps_ctrl_ToBrok_chk");
            //dataLoan.sU1GovRtcProps_ToBroker = GetBool("sSettlementU1GovRtcProps_ctrl_ToBrok_chk");
            //dataLoan.sU2GovRtcProps_ToBroker = GetBool("sSettlementU2GovRtcProps_ctrl_ToBrok_chk");
            //dataLoan.sU3GovRtcProps_ToBroker = GetBool("sSettlementU3GovRtcProps_ctrl_ToBrok_chk");
            dataLoan.sPestInspectFProps_ToBroker = GetBool("sSettlementPestInspectFProps_ctrl_ToBrok_chk");
            dataLoan.sU1ScProps_ToBroker = GetBool("sSettlementU1ScProps_ctrl_ToBrok_chk");
            dataLoan.sU2ScProps_ToBroker = GetBool("sSettlementU2ScProps_ctrl_ToBrok_chk");
            dataLoan.sU3ScProps_ToBroker = GetBool("sSettlementU3ScProps_ctrl_ToBrok_chk");
            dataLoan.sU4ScProps_ToBroker = GetBool("sSettlementU4ScProps_ctrl_ToBrok_chk");
            dataLoan.sU5ScProps_ToBroker = GetBool("sSettlementU5ScProps_ctrl_ToBrok_chk");
            //dataLoan.sHazInsPiaProps_ToBroker = GetBool("sSettlementHazInsPiaProps_ctrl_ToBrok_chk");
            dataLoan.sTitleInsFProps_ToBroker = GetBool("sSettlementTitleInsFProps_ctrl_ToBrok_chk");
            dataLoan.sEscrowFProps_ToBroker = GetBool("sSettlementEscrowFProps_ctrl_ToBrok_chk");
            //dataLoan.sStateRtcProps_ToBroker = GetBool("sSettlementStateRtcProps_ctrl_ToBrok_chk");
            //dataLoan.sCountyRtcProps_ToBroker = GetBool("sSettlementCountyRtcProps_ctrl_ToBrok_chk");
            //dataLoan.sRecFProps_ToBroker = GetBool("sSettlementRecFProps_ctrl_ToBrok_chk");
            dataLoan.sLOrigFProps_ToBroker = GetBool("sSettlementLOrigFProps_ctrl_ToBrok_chk");
            dataLoan.sMBrokFProps_ToBroker = GetBool("sSettlementMBrokFProps_ctrl_ToBrok_chk");

            dataLoan.s800U1FSettlementSection = (E_GfeSectionT)GetInt("sSettlement800U1FProps_ctrl_Page2");
            dataLoan.s800U2FSettlementSection = (E_GfeSectionT)GetInt("sSettlement800U2FProps_ctrl_Page2");
            dataLoan.s800U3FSettlementSection = (E_GfeSectionT)GetInt("sSettlement800U3FProps_ctrl_Page2");
            dataLoan.s800U4FSettlementSection = (E_GfeSectionT)GetInt("sSettlement800U4FProps_ctrl_Page2");
            dataLoan.s800U5FSettlementSection = (E_GfeSectionT)GetInt("sSettlement800U5FProps_ctrl_Page2");
            dataLoan.sEscrowFSettlementSection = (E_GfeSectionT)GetInt("sSettlementEscrowFProps_ctrl_Page2");
            //dataLoan.sTitleInsFSettlementSection = (E_GfeSectionT)GetInt("sSettlementTitleInsFProps_ctrl_Page2");
            dataLoan.sDocPrepFSettlementSection = (E_GfeSectionT)GetInt("sSettlementDocPrepFProps_ctrl_Page2");
            dataLoan.sNotaryFSettlementSection = (E_GfeSectionT)GetInt("sSettlementNotaryFProps_ctrl_Page2");
            dataLoan.sAttorneyFSettlementSection = (E_GfeSectionT)GetInt("sSettlementAttorneyFProps_ctrl_Page2");
            dataLoan.sU1TcSettlementSection = (E_GfeSectionT)GetInt("sSettlementU1TcProps_ctrl_Page2");
            dataLoan.sU2TcSettlementSection = (E_GfeSectionT)GetInt("sSettlementU2TcProps_ctrl_Page2");
            dataLoan.sU3TcSettlementSection = (E_GfeSectionT)GetInt("sSettlementU3TcProps_ctrl_Page2");
            dataLoan.sU4TcSettlementSection = (E_GfeSectionT)GetInt("sSettlementU4TcProps_ctrl_Page2");
            dataLoan.sU1GovRtcSettlementSection = (E_GfeSectionT)GetInt("sSettlementU1GovRtcProps_ctrl_Page2");
            dataLoan.sU2GovRtcSettlementSection = (E_GfeSectionT)GetInt("sSettlementU2GovRtcProps_ctrl_Page2");
            dataLoan.sU3GovRtcSettlementSection = (E_GfeSectionT)GetInt("sSettlementU3GovRtcProps_ctrl_Page2");
            dataLoan.sU1ScSettlementSection = (E_GfeSectionT)GetInt("sSettlementU1ScProps_ctrl_Page2");
            dataLoan.sU2ScSettlementSection = (E_GfeSectionT)GetInt("sSettlementU2ScProps_ctrl_Page2");
            dataLoan.sU3ScSettlementSection = (E_GfeSectionT)GetInt("sSettlementU3ScProps_ctrl_Page2");
            dataLoan.sU4ScSettlementSection = (E_GfeSectionT)GetInt("sSettlementU4ScProps_ctrl_Page2");
            dataLoan.sU5ScSettlementSection = (E_GfeSectionT)GetInt("sSettlementU5ScProps_ctrl_Page2");
            dataLoan.s904PiaSettlementSection = (E_GfeSectionT)GetInt("sSettlement904PiaProps_ctrl_Page2");
            dataLoan.s900U1PiaSettlementSection = (E_GfeSectionT)GetInt("sSettlement900U1PiaProps_ctrl_Page2");
            dataLoan.sSettlementChargesExportSource = (E_SettlementChargesExportSource)GetInt("sSettlementChargesExportSource");

            dataLoan.sGfeIsTPOTransaction = GetBool("sGfeIsTPOTransaction");

            E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT = (E_sOriginatorCompensationPaymentSourceT)GetInt("sOriginatorCompensationPaymentSourceT");

            string sGfeOriginatorCompFPc_rep = GetString("sGfeOriginatorCompFPc");
            string sGfeOriginatorCompFMb_rep = GetString("sGfeOriginatorCompFMb");
            E_PercentBaseT sGfeOriginatorCompFBaseT = (E_PercentBaseT)GetInt("sGfeOriginatorCompFBaseT");

            dataLoan.SetOriginatorCompensation(sOriginatorCompensationPaymentSourceT, sGfeOriginatorCompFPc_rep, sGfeOriginatorCompFBaseT, sGfeOriginatorCompFMb_rep);

            if (GetString("copyFromGFE") == "true")
                dataLoan.ApplyGfeDataToSettlementPage();

            // OPM 170146 - Footer, Details of Transaction tab
            if (!IsClosingCostMigrationArchivePage && dataLoan.sUseGFEDataForSCFields)
            {
                dataLoan.sAltCostLckd = GetBool("Gfe2010Footer_dot_sAltCostLckd");
                dataLoan.sAltCost_rep = GetString("Gfe2010Footer_dot_sAltCost");
                dataLoan.sFfUfmip1003Lckd = GetBool("Gfe2010Footer_dot_sFfUfmip1003Lckd");
                dataLoan.sFfUfmip1003_rep = GetString("Gfe2010Footer_dot_sFfUfmip1003");
                dataLoan.sLDiscnt1003Lckd = GetBool("Gfe2010Footer_dot_sLDiscnt1003Lckd");
                dataLoan.sLDiscnt1003_rep = GetString("Gfe2010Footer_dot_sLDiscnt1003");
                dataLoan.sLandCost_rep = GetString("Gfe2010Footer_dot_sLandCost");
                dataLoan.sOCredit1Amt_rep = GetString("Gfe2010Footer_dot_sOCredit1Amt");
                dataLoan.sOCredit1Desc = GetString("Gfe2010Footer_dot_sOCredit1Desc");
                dataLoan.sOCredit1Lckd = GetBool("Gfe2010Footer_dot_sOCredit1Lckd");

                if (false == dataLoan.sLoads1003LineLFromAdjustments)
                {
                    dataLoan.sOCredit2Amt_rep = GetString("Gfe2010Footer_dot_sOCredit2Amt");
                    dataLoan.sOCredit2Desc = GetString("Gfe2010Footer_dot_sOCredit2Desc");
                    dataLoan.sOCredit3Amt_rep = GetString("Gfe2010Footer_dot_sOCredit3Amt");
                    dataLoan.sOCredit3Desc = GetString("Gfe2010Footer_dot_sOCredit3Desc");
                    dataLoan.sOCredit4Amt_rep = GetString("Gfe2010Footer_dot_sOCredit4Amt");
                    dataLoan.sOCredit4Desc = GetString("Gfe2010Footer_dot_sOCredit4Desc"); 
                }

                dataLoan.sPurchPrice_rep = GetString("Gfe2010Footer_dot_sPurchPrice");
                dataLoan.sRefPdOffAmt1003Lckd = GetBool("Gfe2010Footer_dot_sRefPdOffAmt1003Lckd");
                dataLoan.sRefPdOffAmt1003_rep = GetString("Gfe2010Footer_dot_sRefPdOffAmt1003");
                dataLoan.sTotCcPbsLocked = GetBool("Gfe2010Footer_dot_sTotCcPbsLocked");
                dataLoan.sTotCcPbs_rep = GetString("Gfe2010Footer_dot_sTotCcPbs");
                dataLoan.sTotEstCc1003Lckd = GetBool("Gfe2010Footer_dot_sTotEstCc1003Lckd");
                dataLoan.sTotEstCcNoDiscnt1003_rep = GetString("Gfe2010Footer_dot_sTotEstCcNoDiscnt1003");
                dataLoan.sTotEstPp1003Lckd = GetBool("Gfe2010Footer_dot_sTotEstPp1003Lckd");
                dataLoan.sTotEstPp1003_rep = GetString("Gfe2010Footer_dot_sTotEstPp1003");
                dataLoan.sTransNetCashLckd = GetBool("Gfe2010Footer_dot_sTransNetCashLckd");
                dataLoan.sTransNetCash_rep = GetString("Gfe2010Footer_dot_sTransNetCash");

                if (!dataLoan.sIsIncludeONewFinCcInTotEstCc)
                {
                    dataLoan.sONewFinCc_rep = GetString("Gfe2010Footer_dot_sONewFinCc");
                }
                else
                {
                    dataLoan.sONewFinCc_rep = GetString("Gfe2010Footer_dot_sONewFinCc2");
                }

                dataLoan.sLAmtCalc_rep = GetString("Gfe2010Footer_dot_sLAmtCalc");
                dataLoan.sLAmtLckd = GetBool("Gfe2010Footer_dot_sLAmtLckd");
            }
        }
        
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            if (IsClosingCostMigrationArchivePage)
            {
                Tools.LogError("Programming Error: Should not call services on archive page.");
                return;
            }
            bool heMigrated = dataLoan.sIsHousingExpenseMigrated;

            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sSchedDueD1Lckd", dataLoan.sSchedDueD1Lckd);
            SetResult("sSchedDueD1", dataLoan.sSchedDueD1_rep);
            SetResult("sSchedFundD", dataLoan.sSchedFundD_rep);
            SetResult("sDue", dataLoan.sDue_rep);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sDaysInYr", dataLoan.sDaysInYr_rep);
            
            SetResult("sSettlement800U5F", dataLoan.sSettlement800U5F_rep);
            SetResult("sSettlement800U5FDesc", dataLoan.sSettlement800U5FDesc);
            SetResult("sSettlement800U4F", dataLoan.sSettlement800U4F_rep);
            SetResult("sSettlement800U4FDesc", dataLoan.sSettlement800U4FDesc);
            SetResult("sSettlement800U3F", dataLoan.sSettlement800U3F_rep);
            SetResult("sSettlement800U3FDesc", dataLoan.sSettlement800U3FDesc);
            SetResult("sSettlement800U2F", dataLoan.sSettlement800U2F_rep);
            SetResult("sSettlement800U2FDesc", dataLoan.sSettlement800U2FDesc);
            SetResult("sSettlement800U1F", dataLoan.sSettlement800U1F_rep);
            SetResult("sSettlement800U1FDesc", dataLoan.sSettlement800U1FDesc);
            SetResult("sSettlementWireF", dataLoan.sSettlementWireF_rep);
            SetResult("sSettlementUwF", dataLoan.sSettlementUwF_rep);
            SetResult("sSettlementProcF", dataLoan.sSettlementProcF_rep);
            SetResult("sSettlementTxServF", dataLoan.sSettlementTxServF_rep);
            SetResult("sSettlementFloodCertificationF", dataLoan.sSettlementFloodCertificationF_rep);
            SetResult("sSettlementMBrokFMb", dataLoan.sSettlementMBrokFMb_rep);
            SetResult("sSettlementMBrokFPc", dataLoan.sSettlementMBrokFPc_rep);
            SetResult("sSettlementMBrokFBaseT", dataLoan.sSettlementMBrokFBaseT);
            SetResult("sSettlementCrF", dataLoan.sSettlementCrF_rep);
            SetResult("sSettlementApprF", dataLoan.sSettlementApprF_rep);
            SetResult("sSettlementLDiscntFMb", dataLoan.sSettlementLDiscntFMb_rep);
            SetResult("sSettlementLDiscntPc", dataLoan.sSettlementLDiscntPc_rep);
            SetResult("sSettlementLDiscntBaseT", dataLoan.sSettlementLDiscntBaseT);
            SetResult("sSettlementLOrigFMb", dataLoan.sSettlementLOrigFMb_rep);
            SetResult("sSettlementLOrigFPc", dataLoan.sSettlementLOrigFPc_rep);
            SetResult("sSettlementCreditLenderPaidItemT", dataLoan.sSettlementCreditLenderPaidItemT);
            SetResult("sSettlementCreditLenderPaidItemF", dataLoan.sSettlementCreditLenderPaidItemF_rep);
            SetResult("sSettlementLenderCreditFPc", dataLoan.sSettlementLenderCreditFPc_rep);
            SetResult("sSettlementLenderCreditF", dataLoan.sSettlementLenderCreditF_rep);
            SetResult("sSettlementDiscountPointFPc", dataLoan.sSettlementDiscountPointFPc_rep);
            SetResult("sSettlementDiscountPointF", dataLoan.sSettlementDiscountPointF_rep);

            SetResult("sSettlementMBrokF", dataLoan.sSettlementMBrokF_rep);
            SetResult("sSettlementInspectF", dataLoan.sSettlementInspectF_rep);
            SetResult("sSettlementLDiscnt", dataLoan.sSettlementLDiscnt_rep);
            SetResult("sSettlementLOrigF", dataLoan.sSettlementLOrigF_rep);
            SetResult("sSettlement900U1Pia", dataLoan.sSettlement900U1Pia_rep);
            SetResult("sSettlement900U1PiaDesc", dataLoan.sSettlement900U1PiaDesc);
            SetResult("sVaFf", dataLoan.sVaFf_rep);
            SetResult("sSettlement904Pia", dataLoan.sSettlement904Pia_rep);
            SetResult("sSettlement904PiaDesc", dataLoan.sSettlement904PiaDesc);
            SetResult("sSettlementHazInsPia", dataLoan.sSettlementHazInsPia_rep);
            SetResult("sSettlementHazInsPiaMon", dataLoan.sSettlementHazInsPiaMon_rep);
            SetResult("sProHazInsT", dataLoan.sProHazInsT);
            SetResult("sProHazInsR", dataLoan.sProHazInsR_rep);
            SetResult("sProHazInsMb", dataLoan.sProHazInsMb_rep);
            SetResult("sMipPia", dataLoan.sMipPia_rep);

            SetResult("sSettlementIPia", dataLoan.sSettlementIPia_rep);
            SetResult("sSettlementIPiaDy", dataLoan.sSettlementIPiaDy_rep);
            SetResult("sSettlementIPiaDyLckd", dataLoan.sSettlementIPiaDyLckd);
            SetResult("sSettlementIPerDayLckd", dataLoan.sSettlementIPerDayLckd);
            SetResult("sSettlementIPerDay", dataLoan.sSettlementIPerDay_rep);
            SetResult("sSettlementAggregateAdjRsrv", dataLoan.sSettlementAggregateAdjRsrv_rep);
            SetResult("sSettlementAggregateAdjRsrvLckd", dataLoan.sSettlementAggregateAdjRsrvLckd);
            SetResult("sSettlement1009Rsrv", dataLoan.sSettlement1009Rsrv_rep);
            if (heMigrated && dataLoan.sLine1009Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("s1007ProHExp", dataLoan.sLine1009Expense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("s1007ProHExp", dataLoan.s1007ProHExp_rep);
            }
            SetResult("sSettlement1009RsrvMon", dataLoan.sSettlement1009RsrvMon_rep);
            SetResult("s1007ProHExpDesc", dataLoan.s1007ProHExpDesc);
            SetResult("sSettlement1008Rsrv", dataLoan.sSettlement1008Rsrv_rep);
            if (heMigrated && dataLoan.sLine1008Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("s1006ProHExp", dataLoan.sLine1008Expense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("s1006ProHExp", dataLoan.s1006ProHExp_rep);
            }
            SetResult("sSettlement1008RsrvMon", dataLoan.sSettlement1008RsrvMon_rep);
            SetResult("s1006ProHExpDesc", dataLoan.s1006ProHExpDesc);
            SetResult("sSettlementU3Rsrv", dataLoan.sSettlementU3Rsrv_rep);
            if (heMigrated && dataLoan.sLine1010Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("sProU3Rsrv", dataLoan.sLine1010Expense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("sProU3Rsrv", dataLoan.sProU3Rsrv_rep);
            }
            SetResult("sSettlementU3RsrvMon", dataLoan.sSettlementU3RsrvMon_rep);
            SetResult("sU3RsrvDesc", dataLoan.sU3RsrvDesc);
            SetResult("sSettlementU4Rsrv", dataLoan.sSettlementU4Rsrv_rep);
            if (heMigrated && dataLoan.sLine1011Expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("sProU4Rsrv", dataLoan.sLine1011Expense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("sProU4Rsrv", dataLoan.sProU4Rsrv_rep);
            }
            SetResult("sSettlementU4RsrvMon", dataLoan.sSettlementU4RsrvMon_rep);
            SetResult("sU4RsrvDesc", dataLoan.sU4RsrvDesc);
            SetResult("sSettlementFloodInsRsrv", dataLoan.sSettlementFloodInsRsrv_rep);
            if (heMigrated && dataLoan.sFloodExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("sProFloodIns", dataLoan.sFloodExpense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("sProFloodIns", dataLoan.sProFloodIns_rep);
            }
            SetResult("sSettlementFloodInsRsrvMon", dataLoan.sSettlementFloodInsRsrvMon_rep);
            SetResult("sSettlementRealETxRsrv", dataLoan.sSettlementRealETxRsrv_rep);
            SetResult("sProRealETxT", dataLoan.sProRealETxT);
            SetResult("sProRealETxMb", dataLoan.sProRealETxMb_rep);
            SetResult("sProRealETxR", dataLoan.sProRealETxR_rep);
            SetResult("sSettlementProRealETx", dataLoan.sProRealETx_rep);
            SetResult("sSettlementRealETxRsrvMon", dataLoan.sSettlementRealETxRsrvMon_rep);
            SetResult("sSettlementSchoolTxRsrv", dataLoan.sSettlementSchoolTxRsrv_rep);
            if (heMigrated && dataLoan.sSchoolTaxExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("sProSchoolTx", dataLoan.sSchoolTaxExpense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("sProSchoolTx", dataLoan.sProSchoolTx_rep);
            }
            SetResult("sSettlementSchoolTxRsrvMon", dataLoan.sSettlementSchoolTxRsrvMon_rep);
            SetResult("sSettlementMInsRsrv", dataLoan.sSettlementMInsRsrv_rep);
            SetResult("sProMIns", dataLoan.sProMIns_rep);
            SetResult("sSettlementMInsRsrvMon", dataLoan.sSettlementMInsRsrvMon_rep);
            SetResult("sSettlementHazInsRsrv", dataLoan.sSettlementHazInsRsrv_rep);
            if (heMigrated && dataLoan.sHazardExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                SetResult("sProHazIns", dataLoan.sHazardExpense.MonthlyAmtServicing_rep);
            }
            else
            {
                SetResult("sProHazIns", dataLoan.sProHazIns_rep);
            }
            SetResult("sSettlementHazInsRsrvMon", dataLoan.sSettlementHazInsRsrvMon_rep);
            SetResult("sSettlementTitleInsF", dataLoan.sSettlementTitleInsF_rep);
            SetResult("sSettlementAttorneyF", dataLoan.sSettlementAttorneyF_rep);
            SetResult("sSettlementNotaryF", dataLoan.sSettlementNotaryF_rep);
            SetResult("sSettlementDocPrepF", dataLoan.sSettlementDocPrepF_rep);
            SetResult("sSettlementEscrowF", dataLoan.sSettlementEscrowF_rep);
            SetResult("sSettlementU3GovRtc", dataLoan.sSettlementU3GovRtc_rep);
            SetResult("sSettlementU2GovRtc", dataLoan.sSettlementU2GovRtc_rep);
            SetResult("sSettlementU1GovRtc", dataLoan.sSettlementU1GovRtc_rep);
            SetResult("sSettlementStateRtc", dataLoan.sSettlementStateRtc_rep);
            SetResult("sSettlementCountyRtc", dataLoan.sSettlementCountyRtc_rep);
            SetResult("sSettlementRecF", dataLoan.sSettlementRecF_rep);
            SetResult("sSettlementPestInspectF", dataLoan.sSettlementPestInspectF_rep);

            SetResult("sSettlementU4Tc", dataLoan.sSettlementU4Tc_rep);
            SetResult("sSettlementU4TcDesc", dataLoan.sSettlementU4TcDesc);
            SetResult("sSettlementU3Tc", dataLoan.sSettlementU3Tc_rep);
            SetResult("sSettlementU3TcDesc", dataLoan.sSettlementU3TcDesc);
            SetResult("sSettlementU2Tc", dataLoan.sSettlementU2Tc_rep);
            SetResult("sSettlementU2TcDesc", dataLoan.sSettlementU2TcDesc);
            SetResult("sSettlementU1Tc", dataLoan.sSettlementU1Tc_rep);
            SetResult("sSettlementU1TcDesc", dataLoan.sSettlementU1TcDesc);
            SetResult("sSettlementOwnerTitleInsF", dataLoan.sSettlementOwnerTitleInsF_rep);

            SetResult("sSettlementU3GovRtcMb", dataLoan.sSettlementU3GovRtcMb_rep);
            SetResult("sSettlementU3GovRtcBaseT", dataLoan.sSettlementU3GovRtcBaseT);
            SetResult("sSettlementU3GovRtcPc", dataLoan.sSettlementU3GovRtcPc_rep);
            SetResult("sSettlementU3GovRtcDesc", dataLoan.sSettlementU3GovRtcDesc);
            SetResult("sSettlementU2GovRtcMb", dataLoan.sSettlementU2GovRtcMb_rep);
            SetResult("sSettlementU2GovRtcBaseT", dataLoan.sSettlementU2GovRtcBaseT);
            SetResult("sSettlementU2GovRtcPc", dataLoan.sSettlementU2GovRtcPc_rep);
            SetResult("sSettlementU2GovRtcDesc", dataLoan.sSettlementU2GovRtcDesc);
            SetResult("sSettlementU1GovRtcMb", dataLoan.sSettlementU1GovRtcMb_rep);
            SetResult("sSettlementU1GovRtcBaseT", dataLoan.sSettlementU1GovRtcBaseT);
            SetResult("sSettlementU1GovRtcPc", dataLoan.sSettlementU1GovRtcPc_rep);
            SetResult("sSettlementU1GovRtcDesc", dataLoan.sSettlementU1GovRtcDesc);
            SetResult("sSettlementStateRtcMb", dataLoan.sSettlementStateRtcMb_rep);
            SetResult("sSettlementStateRtcBaseT", dataLoan.sSettlementStateRtcBaseT);
            SetResult("sSettlementStateRtcPc", dataLoan.sSettlementStateRtcPc_rep);
            SetResult("sSettlementCountyRtcMb", dataLoan.sSettlementCountyRtcMb_rep);
            SetResult("sSettlementCountyRtcBaseT", dataLoan.sSettlementCountyRtcBaseT);
            SetResult("sSettlementCountyRtcPc", dataLoan.sSettlementCountyRtcPc_rep);
            SetResult("sSettlementRecFMb", dataLoan.sSettlementRecFMb_rep);
            SetResult("sSettlementRecBaseT", dataLoan.sSettlementRecBaseT);
            SetResult("sSettlementRecFPc", dataLoan.sSettlementRecFPc_rep);
            SetResult("sSettlementRecDeed", dataLoan.sSettlementRecDeed_rep);
            SetResult("sSettlementRecMortgage", dataLoan.sSettlementRecMortgage_rep);
            SetResult("sSettlementRecRelease", dataLoan.sSettlementRecRelease_rep);
            SetResult("sSettlementRecFLckd", dataLoan.sSettlementRecFLckd);

            SetResult("sSettlementU5Sc", dataLoan.sSettlementU5Sc_rep);
            SetResult("sSettlementU5ScDesc", dataLoan.sSettlementU5ScDesc);
            SetResult("sSettlementU4Sc", dataLoan.sSettlementU4Sc_rep);
            SetResult("sSettlementU4ScDesc", dataLoan.sSettlementU4ScDesc);
            SetResult("sSettlementU3Sc", dataLoan.sSettlementU3Sc_rep);
            SetResult("sSettlementU3ScDesc", dataLoan.sSettlementU3ScDesc);
            SetResult("sSettlementU2Sc", dataLoan.sSettlementU2Sc_rep);
            SetResult("sSettlementU2ScDesc", dataLoan.sSettlementU2ScDesc);
            SetResult("sSettlementU1Sc", dataLoan.sSettlementU1Sc_rep);
            SetResult("sSettlementU1ScDesc", dataLoan.sSettlementU1ScDesc);

            SetResult("sSettlementProcFProps_ctrl_Paid_chk", dataLoan.sSettlementProcFPaid);
            SetResult("sSettlementCrFProps_ctrl_Paid_chk", dataLoan.sSettlementCrFPaid);
            SetResult("sSettlementApprFProps_ctrl_Paid_chk", dataLoan.sSettlementApprFPaid);


            SetResult("sGfeIsTPOTransaction", dataLoan.sGfeIsTPOTransaction);
            SetResult("sSettlementTotalEstimateSettlementCharge", dataLoan.sSettlementTotalEstimateSettlementCharge_rep);
            SetResult("sSettlementTotalDedFromLoanProc", dataLoan.sSettlementTotalDedFromLoanProc_rep);
            SetResult("sSettlementTotalFundByLenderAtClosing", dataLoan.sSettlementTotalFundByLenderAtClosing_rep);

            // 800 - Items payable in connection with loan
            InitItemProps("sSettlementLOrigFProps", dataLoan.sSettlementLOrigFProps);
            //InitItemProps("sSettlementBrokerCompProps", dataLoan.sSettlementBrokerCompProps); // eh?
            //InitItemProps("sSettlementLOrigBrokerCreditFProps", dataLoan.sSettlementLOrigBrokerCreditFProps); // wot?
            InitItemProps("sGfeOriginatorCompFProps", dataLoan.sGfeOriginatorCompFProps);
            //InitItemProps("sSettlementLDiscntProps", dataLoan.sSettlementLDiscntProps); // Not used anymore
            //InitItemProps("sGfeLenderCreditFProps", dataLoan.sSettlementLenderCreditFProps);
            InitItemProps("sSettlementDiscountPointFProps", dataLoan.sSettlementDiscountPointFProps);
            InitItemProps("sSettlementApprFProps", dataLoan.sSettlementApprFProps);
            InitItemProps("sSettlementCrFProps", dataLoan.sSettlementCrFProps);
            InitItemProps("sSettlementInspectFProps", dataLoan.sSettlementInspectFProps);
            InitItemProps("sSettlementMBrokFProps", dataLoan.sSettlementMBrokFProps);
            InitItemProps("sSettlementTxServFProps", dataLoan.sSettlementTxServFProps);
            InitItemProps("sSettlementFloodCertificationFProps", dataLoan.sSettlementFloodCertificationFProps);
            InitItemProps("sSettlementProcFProps", dataLoan.sSettlementProcFProps);
            InitItemProps("sSettlementUwFProps", dataLoan.sSettlementUwFProps);
            InitItemProps("sSettlementWireFProps", dataLoan.sSettlementWireFProps);
            InitItemProps("sSettlement800U1FProps", dataLoan.sSettlement800U1FProps);
            InitItemProps("sSettlement800U2FProps", dataLoan.sSettlement800U2FProps);
            InitItemProps("sSettlement800U3FProps", dataLoan.sSettlement800U3FProps);
            InitItemProps("sSettlement800U4FProps", dataLoan.sSettlement800U4FProps);
            InitItemProps("sSettlement800U5FProps", dataLoan.sSettlement800U5FProps);
            InitItemProps("sSettlementIPiaProps", dataLoan.sSettlementIPiaProps);
            InitItemProps("sMipPiaProps", dataLoan.sMipPiaProps);
            InitItemProps("sSettlementHazInsPiaProps", dataLoan.sSettlementHazInsPiaProps);
            InitItemProps("sSettlement904PiaProps", dataLoan.sSettlement904PiaProps);
            InitItemProps("sVaFfProps", dataLoan.sVaFfProps);
            InitItemProps("sSettlement900U1PiaProps", dataLoan.sSettlement900U1PiaProps);
            InitItemProps("sSettlementHazInsRsrvProps", dataLoan.sSettlementHazInsRsrvProps);
            InitItemProps("sSettlementMInsRsrvProps", dataLoan.sSettlementMInsRsrvProps);
            InitItemProps("sSettlementSchoolTxRsrvProps", dataLoan.sSettlementSchoolTxRsrvProps);
            InitItemProps("sSettlementRealETxRsrvProps", dataLoan.sSettlementRealETxRsrvProps);
            InitItemProps("sSettlementFloodInsRsrvProps", dataLoan.sSettlementFloodInsRsrvProps);
            InitItemProps("sSettlement1008RsrvProps", dataLoan.sSettlement1008RsrvProps);
            InitItemProps("sSettlement1009RsrvProps", dataLoan.sSettlement1009RsrvProps);
            InitItemProps("sSettlementU3RsrvProps", dataLoan.sSettlementU3RsrvProps);
            InitItemProps("sSettlementU4RsrvProps", dataLoan.sSettlementU4RsrvProps);
            InitItemProps("sSettlementAggregateAdjRsrvProps", dataLoan.sSettlementAggregateAdjRsrvProps);
            InitItemProps("sSettlementEscrowFProps", dataLoan.sSettlementEscrowFProps);
            InitItemProps("sSettlementOwnerTitleInsFProps", dataLoan.sSettlementOwnerTitleInsFProps);
            InitItemProps("sSettlementDocPrepFProps", dataLoan.sSettlementDocPrepFProps);
            InitItemProps("sSettlementNotaryFProps", dataLoan.sSettlementNotaryFProps);
            InitItemProps("sSettlementAttorneyFProps", dataLoan.sSettlementAttorneyFProps);
            InitItemProps("sSettlementTitleInsFProps", dataLoan.sSettlementTitleInsFProps);
            InitItemProps("sSettlementU1TcProps", dataLoan.sSettlementU1TcProps);
            InitItemProps("sSettlementU2TcProps", dataLoan.sSettlementU2TcProps);
            InitItemProps("sSettlementU3TcProps", dataLoan.sSettlementU3TcProps);
            InitItemProps("sSettlementU4TcProps", dataLoan.sSettlementU4TcProps);
            InitItemProps("sSettlementRecFProps", dataLoan.sSettlementRecFProps);
            InitItemProps("sSettlementCountyRtcProps", dataLoan.sSettlementCountyRtcProps);
            InitItemProps("sSettlementStateRtcProps", dataLoan.sSettlementStateRtcProps);
            InitItemProps("sSettlementU1GovRtcProps", dataLoan.sSettlementU1GovRtcProps);
            InitItemProps("sSettlementU2GovRtcProps", dataLoan.sSettlementU2GovRtcProps);
            InitItemProps("sSettlementU3GovRtcProps", dataLoan.sSettlementU3GovRtcProps);
            InitItemProps("sSettlementPestInspectFProps", dataLoan.sSettlementPestInspectFProps);
            InitItemProps("sSettlementU1ScProps", dataLoan.sSettlementU1ScProps);
            InitItemProps("sSettlementU2ScProps", dataLoan.sSettlementU2ScProps);
            InitItemProps("sSettlementU3ScProps", dataLoan.sSettlementU3ScProps);
            InitItemProps("sSettlementU4ScProps", dataLoan.sSettlementU4ScProps);
            InitItemProps("sSettlementU5ScProps", dataLoan.sSettlementU5ScProps);

            SetResult("sSettlement800U1FProps_ctrl_Page2", dataLoan.s800U1FSettlementSection);
            SetResult("sSettlement800U2FProps_ctrl_Page2", dataLoan.s800U2FSettlementSection);
            SetResult("sSettlement800U3FProps_ctrl_Page2", dataLoan.s800U3FSettlementSection);
            SetResult("sSettlement800U4FProps_ctrl_Page2", dataLoan.s800U4FSettlementSection);
            SetResult("sSettlement800U5FProps_ctrl_Page2", dataLoan.s800U5FSettlementSection);
            SetResult("sSettlementEscrowFProps_ctrl_Page2", dataLoan.sEscrowFSettlementSection);
            SetResult("sSettlementTitleInsFProps_ctrl_Page2", dataLoan.sTitleInsFSettlementSection);
            SetResult("sSettlementDocPrepFProps_ctrl_Page2", dataLoan.sDocPrepFSettlementSection);
            SetResult("sSettlementNotaryFProps_ctrl_Page2", dataLoan.sNotaryFSettlementSection);
            SetResult("sSettlementAttorneyFProps_ctrl_Page2", dataLoan.sAttorneyFSettlementSection);
            SetResult("sSettlementU1TcProps_ctrl_Page2", dataLoan.sU1TcSettlementSection);
            SetResult("sSettlementU2TcProps_ctrl_Page2", dataLoan.sU2TcSettlementSection);
            SetResult("sSettlementU3TcProps_ctrl_Page2", dataLoan.sU3TcSettlementSection);
            SetResult("sSettlementU4TcProps_ctrl_Page2", dataLoan.sU4TcSettlementSection);
            SetResult("sSettlementU1GovRtcProps_ctrl_Page2", dataLoan.sU1GovRtcSettlementSection);
            SetResult("sSettlementU2GovRtcProps_ctrl_Page2", dataLoan.sU2GovRtcSettlementSection);
            SetResult("sSettlementU3GovRtcProps_ctrl_Page2", dataLoan.sU3GovRtcSettlementSection);
            SetResult("sSettlementU1ScProps_ctrl_Page2", dataLoan.sU1ScSettlementSection);
            SetResult("sSettlementU2ScProps_ctrl_Page2", dataLoan.sU2ScSettlementSection);
            SetResult("sSettlementU3ScProps_ctrl_Page2", dataLoan.sU3ScSettlementSection);
            SetResult("sSettlementU4ScProps_ctrl_Page2", dataLoan.sU4ScSettlementSection);
            SetResult("sSettlementU5ScProps_ctrl_Page2", dataLoan.sU5ScSettlementSection);
            SetResult("sSettlement904PiaProps_ctrl_Page2", dataLoan.s904PiaSettlementSection);
            SetResult("sSettlement900U1PiaProps_ctrl_Page2", dataLoan.s900U1PiaSettlementSection);

            SetResult("ccTemplateMissing", dataLoan.IsGfeToSettlementCCTemplateMissing ? "1" : "0");
            SetResult("sGfeOriginatorCompF", dataLoan.sGfeOriginatorCompF_rep);
            SetResult("sOriginatorCompensationPaymentSourceT", dataLoan.sOriginatorCompensationPaymentSourceT);

            SetResult("sGfeOriginatorCompFPc", dataLoan.sGfeOriginatorCompFPc_rep);
            SetResult("sGfeOriginatorCompFMb", dataLoan.sGfeOriginatorCompFMb_rep);
            SetResult("sGfeOriginatorCompFBaseT", dataLoan.sGfeOriginatorCompFBaseT);

            SetResult("sSettlementChargesExportSource", dataLoan.sSettlementChargesExportSource);

            // OPM 144673 - Add Paid To fields to Settlement Charges
            SetResult("sApprFProps_ctrl_PaidTo_tb", dataLoan.sApprFPaidTo);
            SetResult("sCrFProps_ctrl_PaidTo_tb", dataLoan.sCrFPaidTo);
            SetResult("sTxServFProps_ctrl_PaidTo_tb", dataLoan.sTxServFPaidTo);
            SetResult("sFloodCertificationFProps_ctrl_PaidTo_tb", dataLoan.sFloodCertificationFPaidTo);
            SetResult("sInspectFProps_ctrl_PaidTo_tb", dataLoan.sInspectFPaidTo);
            SetResult("sProcFProps_ctrl_PaidTo_tb", dataLoan.sProcFPaidTo);
            SetResult("sUwFProps_ctrl_PaidTo_tb", dataLoan.sUwFPaidTo);
            SetResult("sWireFProps_ctrl_PaidTo_tb", dataLoan.sWireFPaidTo);
            SetResult("s800U1FProps_ctrl_PaidTo_tb", dataLoan.s800U1FPaidTo);
            SetResult("s800U2FProps_ctrl_PaidTo_tb", dataLoan.s800U2FPaidTo);
            SetResult("s800U3FProps_ctrl_PaidTo_tb", dataLoan.s800U3FPaidTo);
            SetResult("s800U4FProps_ctrl_PaidTo_tb", dataLoan.s800U4FPaidTo);
            SetResult("s800U5FProps_ctrl_PaidTo_tb", dataLoan.s800U5FPaidTo);
            SetResult("sEscrowFProps_ctrl_PaidTo_tb", dataLoan.sEscrowFTable);
            SetResult("sTitleInsFProps_ctrl_PaidTo_tb", dataLoan.sTitleInsFTable);
            SetResult("sOwnerTitleInsFProps_ctrl_PaidTo_tb", dataLoan.sOwnerTitleInsPaidTo);
            SetResult("sDocPrepFProps_ctrl_PaidTo_tb", dataLoan.sDocPrepFPaidTo);
            SetResult("sNotaryFProps_ctrl_PaidTo_tb", dataLoan.sNotaryFPaidTo);
            SetResult("sAttorneyFProps_ctrl_PaidTo_tb", dataLoan.sAttorneyFPaidTo);
            SetResult("sU1TcProps_ctrl_PaidTo_tb", dataLoan.sU1TcPaidTo);
            SetResult("sU2TcProps_ctrl_PaidTo_tb", dataLoan.sU2TcPaidTo);
            SetResult("sU3TcProps_ctrl_PaidTo_tb", dataLoan.sU3TcPaidTo);
            SetResult("sU4TcProps_ctrl_PaidTo_tb", dataLoan.sU4TcPaidTo);
            SetResult("sRecFProps_ctrl_PaidTo_tb", dataLoan.sRecFDesc);
            SetResult("sCountyRtcProps_ctrl_PaidTo_tb", dataLoan.sCountyRtcDesc);
            SetResult("sStateRtcProps_ctrl_PaidTo_tb", dataLoan.sStateRtcDesc);
            SetResult("sU1GovRtcProps_ctrl_PaidTo_tb", dataLoan.sU1GovRtcPaidTo);
            SetResult("sU2GovRtcProps_ctrl_PaidTo_tb", dataLoan.sU2GovRtcPaidTo);
            SetResult("sU3GovRtcProps_ctrl_PaidTo_tb", dataLoan.sU3GovRtcPaidTo);
            SetResult("sPestInspectFProps_ctrl_PaidTo_tb", dataLoan.sPestInspectPaidTo);
            SetResult("sU1ScProps_ctrl_PaidTo_tb", dataLoan.sU1ScPaidTo);
            SetResult("sU2ScProps_ctrl_PaidTo_tb", dataLoan.sU2ScPaidTo);
            SetResult("sU3ScProps_ctrl_PaidTo_tb", dataLoan.sU3ScPaidTo);
            SetResult("sU4ScProps_ctrl_PaidTo_tb", dataLoan.sU4ScPaidTo);
            SetResult("sU5ScProps_ctrl_PaidTo_tb", dataLoan.sU5ScPaidTo);
            SetResult("sHazInsPiaProps_ctrl_PaidTo_tb", dataLoan.sHazInsPiaPaidTo);
            SetResult("sMipPiaProps_ctrl_PaidTo_tb", dataLoan.sMipPiaPaidTo);
            SetResult("sVaFfProps_ctrl_PaidTo_tb", dataLoan.sVaFfPaidTo);

            SetResult("sApprFProps_ctrl_ToBrok_chk", dataLoan.sApprFProps_ToBroker);
            SetResult("sCrFProps_ctrl_ToBrok_chk", dataLoan.sCrFProps_ToBroker);
            SetResult("sTxServFProps_ctrl_ToBrok_chk", dataLoan.sTxServFProps_ToBroker);
            SetResult("sFloodCertificationFProps_ctrl_ToBrok_chk", dataLoan.sFloodCertificationFProps_ToBroker);
            SetResult("sInspectFProps_ctrl_ToBrok_chk", dataLoan.sInspectFProps_ToBroker);
            SetResult("sProcFProps_ctrl_ToBrok_chk", dataLoan.sProcFProps_ToBroker);
            SetResult("sUwFProps_ctrl_ToBrok_chk", dataLoan.sUwFProps_ToBroker);
            SetResult("sWireFProps_ctrl_ToBrok_chk", dataLoan.sWireFProps_ToBroker);
            SetResult("s800U1FProps_ctrl_ToBrok_chk", dataLoan.s800U1FProps_ToBroker);
            SetResult("s800U2FProps_ctrl_ToBrok_chk", dataLoan.s800U2FProps_ToBroker);
            SetResult("s800U3FProps_ctrl_ToBrok_chk", dataLoan.s800U3FProps_ToBroker);
            SetResult("s800U4FProps_ctrl_ToBrok_chk", dataLoan.s800U4FProps_ToBroker);
            SetResult("s800U5FProps_ctrl_ToBrok_chk", dataLoan.s800U5FProps_ToBroker);
            SetResult("sEscrowFProps_ctrl_ToBrok_chk", dataLoan.sEscrowFProps_ToBroker);
            SetResult("sTitleInsFProps_ctrl_ToBrok_chk", dataLoan.sTitleInsFProps_ToBroker);
            SetResult("sOwnerTitleInsFProps_ctrl_ToBrok_chk", dataLoan.sOwnerTitleInsProps_ToBroker);
            SetResult("sDocPrepFProps_ctrl_ToBrok_chk", dataLoan.sDocPrepFProps_ToBroker);
            SetResult("sNotaryFProps_ctrl_ToBrok_chk", dataLoan.sNotaryFProps_ToBroker);
            SetResult("sAttorneyFProps_ctrl_ToBrok_chk", dataLoan.sAttorneyFProps_ToBroker);
            SetResult("sU1TcProps_ctrl_ToBrok_chk", dataLoan.sU1TcProps_ToBroker);
            SetResult("sU2TcProps_ctrl_ToBrok_chk", dataLoan.sU2TcProps_ToBroker);
            SetResult("sU3TcProps_ctrl_ToBrok_chk", dataLoan.sU3TcProps_ToBroker);
            SetResult("sU4TcProps_ctrl_ToBrok_chk", dataLoan.sU4TcProps_ToBroker);
            //SetResult("sRecFProps_ctrl_ToBrok_chk", dataLoan.sRecFProps_ToBroker);
            //SetResult("sCountyRtcProps_ctrl_ToBrok_chk", dataLoan.sCountyRtcProps_ToBroker);
            //SetResult("sStateRtcProps_ctrl_ToBrok_chk", dataLoan.sStateRtcProps_ToBroker);
            //SetResult("sU1GovRtcProps_ctrl_ToBrok_chk", dataLoan.sU1GovRtcProps_ToBroker);
            //SetResult("sU2GovRtcProps_ctrl_ToBrok_chk", dataLoan.sU2GovRtcProps_ToBroker);
            //SetResult("sU3GovRtcProps_ctrl_ToBrok_chk", dataLoan.sU3GovRtcProps_ToBroker);
            SetResult("sPestInspectFProps_ctrl_ToBrok_chk", dataLoan.sPestInspectFProps_ToBroker);
            SetResult("sU1ScProps_ctrl_ToBrok_chk", dataLoan.sU1ScProps_ToBroker);
            SetResult("sU2ScProps_ctrl_ToBrok_chk", dataLoan.sU2ScProps_ToBroker);
            SetResult("sU3ScProps_ctrl_ToBrok_chk", dataLoan.sU3ScProps_ToBroker);
            SetResult("sU4ScProps_ctrl_ToBrok_chk", dataLoan.sU4ScProps_ToBroker);
            SetResult("sU5ScProps_ctrl_ToBrok_chk", dataLoan.sU5ScProps_ToBroker);
            //SetResult("sHazInsPiaProps_ctrl_ToBrok_chk", dataLoan.sHazInsPiaProps_ToBroker);
            SetResult("sSettlementLOrigFProps_ctrl_ToBrok_chk", dataLoan.sLOrigFProps_ToBroker);
            SetResult("sSettlementMBrokFProps_ctrl_ToBrok_chk", dataLoan.sMBrokFProps_ToBroker);


            if (!IsClosingCostMigrationArchivePage && dataLoan.sUseGFEDataForSCFields)
            {
                // OPM 170146 - Footer, Lender and Seller paid totals tab
                SetResult("Gfe2010Footer_sGfeTotalFundByLender", dataLoan.sGfeTotalFundByLender_rep);
                SetResult("Gfe2010Footer_sGfeTotalFundBySeller", dataLoan.sGfeTotalFundBySeller_rep);

                // OPM 170146 - Footer, Details of Transaction tab
                SetResult("Gfe2010Footer_dot_sAltCost", dataLoan.sAltCost_rep);
                SetResult("Gfe2010Footer_dot_sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
                SetResult("Gfe2010Footer_dot_sFfUfmip1003Lckd", dataLoan.sFfUfmip1003Lckd);
                SetResult("Gfe2010Footer_dot_sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
                SetResult("Gfe2010Footer_dot_sFinalLAmt", dataLoan.sFinalLAmt_rep);
                SetResult("Gfe2010Footer_dot_sLAmt1003", dataLoan.sLAmt1003_rep);
                SetResult("Gfe2010Footer_dot_sLDiscnt1003", dataLoan.sLDiscnt1003_rep);
                SetResult("Gfe2010Footer_dot_sLDiscnt1003Lckd", dataLoan.sLDiscnt1003Lckd);
                SetResult("Gfe2010Footer_dot_sLandCost", dataLoan.sLandCost_rep);
                SetResult("Gfe2010Footer_dot_sOCredit1Amt", dataLoan.sOCredit1Amt_rep);
                SetResult("Gfe2010Footer_dot_sOCredit1Desc", dataLoan.sOCredit1Desc);
                SetResult("Gfe2010Footer_dot_sOCredit1Lckd", dataLoan.sOCredit1Lckd);
                SetResult("Gfe2010Footer_dot_sOCredit2Amt", dataLoan.sOCredit2Amt_rep);
                SetResult("Gfe2010Footer_dot_sOCredit2Desc", dataLoan.sOCredit2Desc);
                SetResult("Gfe2010Footer_dot_sOCredit3Amt", dataLoan.sOCredit3Amt_rep);
                SetResult("Gfe2010Footer_dot_sOCredit3Desc", dataLoan.sOCredit3Desc);
                SetResult("Gfe2010Footer_dot_sOCredit4Amt", dataLoan.sOCredit4Amt_rep);
                SetResult("Gfe2010Footer_dot_sOCredit4Desc", dataLoan.sOCredit4Desc);
                SetResult("Gfe2010Footer_dot_sOCredit5Amt", dataLoan.sOCredit5Amt_rep);
                SetResult("Gfe2010Footer_dot_sONewFinBal", dataLoan.sONewFinBal_rep);
                SetResult("Gfe2010Footer_dot_sPurchPrice", dataLoan.sPurchPrice_rep);
                SetResult("Gfe2010Footer_dot_sRefPdOffAmt1003", dataLoan.sRefPdOffAmt1003_rep);
                SetResult("Gfe2010Footer_dot_sRefPdOffAmt1003Lckd", dataLoan.sRefPdOffAmt1003Lckd);
                SetResult("Gfe2010Footer_dot_sTotCcPbs", dataLoan.sTotCcPbs_rep);
                SetResult("Gfe2010Footer_dot_sTotCcPbsLocked", dataLoan.sTotCcPbsLocked);
                SetResult("Gfe2010Footer_dot_sTotEstCc1003Lckd", dataLoan.sTotEstCc1003Lckd);
                SetResult("Gfe2010Footer_dot_sTotEstCcNoDiscnt1003", dataLoan.sTotEstCcNoDiscnt1003_rep);
                SetResult("Gfe2010Footer_dot_sTotEstPp1003", dataLoan.sTotEstPp1003_rep);
                SetResult("Gfe2010Footer_dot_sTotEstPp1003Lckd", dataLoan.sTotEstPp1003Lckd);
                SetResult("Gfe2010Footer_dot_sTotTransC", dataLoan.sTotTransC_rep);
                SetResult("Gfe2010Footer_dot_sTransNetCash", dataLoan.sTransNetCash_rep);
                SetResult("Gfe2010Footer_dot_sTransNetCashLckd", dataLoan.sTransNetCashLckd);
                SetResult("Gfe2010Footer_dot_sONewFinCc", dataLoan.sONewFinCc_rep);
                SetResult("Gfe2010Footer_dot_sONewFinCc2", dataLoan.sONewFinCc_rep);
                SetResult("Gfe2010Footer_dot_sLAmtCalc", dataLoan.sLAmtCalc_rep);
                SetResult("Gfe2010Footer_dot_sLAmtLckd", dataLoan.sLAmtLckd);
                SetResult("Gfe2010Footer_dot_sTotEstCcNoDiscnt", dataLoan.sTotEstCcNoDiscnt_rep);
                SetResult("Gfe2010Footer_dot_sTotEstPp", dataLoan.sTotEstPp_rep);
                SetResult("Gfe2010Footer_dot_sTotalBorrowerPaidProrations", dataLoan.sTotalBorrowerPaidProrations_rep);
            }
        }
    }

    public partial class SettlementChargesService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override bool PerformResultOptimization
        {
            get
            {
                return true;
            }
        }
        protected override void Initialize()
        {
            AddBackgroundItem("", new SettlementChargesServiceItem());
        }
    }
}