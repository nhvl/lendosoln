﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Closer
{
    public class FundingServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadAdvanceClassifications":
                    LoadAdvanceClassifications();
                    break;
            }
        }
        private void LoadAdvanceClassifications()
        {
            int warehouseLenderRolodexId = GetInt("warehouseLenderId");
            var lenderInfo = WarehouseLenderRolodexEntry.Get(PrincipalFactory.CurrentPrincipal.BrokerId, warehouseLenderRolodexId);
            string jsonList = ObsoleteSerializationHelper.JsonSerialize(lenderInfo.AdvanceClassifications);
            SetResult("AdvanceClassifications", jsonList);
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FundingServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sWarehouseLenderRolodexId = GetInt("sWarehouseLenderRolodexId");
            dataLoan.sWarehouseAdvanceClassification = GetString("sWarehouseAdvanceClassification");
            dataLoan.sWarehouseFunderDesc = GetString("sWarehouseFunderDesc");
            dataLoan.sFundingDisbursementMethodT = (E_sDisbursementMethodT)GetInt("sFundingDisbursementMethodT");
            dataLoan.sWarehouseMaxFundPc_rep = GetString("sWarehouseMaxFundPc");
            dataLoan.sOtherFundAdj_rep = GetString("sOtherFundAdj");
            dataLoan.sAmtFundFromWarehouseLine_rep = GetString("sAmtFundFromWarehouseLine");
            dataLoan.sAmtFundFromWarehouseLineLckd = GetBool("sAmtFundFromWarehouseLineLckd");
            dataLoan.sChkDueFromClosingRcvd = GetBool("sChkDueFromClosingRcvd");
            dataLoan.sSchedFundD_rep = GetString("sSchedFundD");
            dataLoan.sFundsOrderedD_rep = GetString("sFundsOrderedD");
            dataLoan.sFundD_rep = GetString("sFundD");
            dataLoan.sFundN = GetString("sFundN");
            dataLoan.sShippedToWarehouseD_rep = GetString("sShippedToWarehouseD");
            dataLoan.sShippedToWarehouseN = GetString("sShippedToWarehouseN");
            dataLoan.sTrackingN = GetString("sTrackingN");
            dataLoan.sLoanProceedsToT = (E_LoanProceedsToT)GetInt("sLoanProceedsToT");
            dataLoan.sFundNotes = GetString("sFundNotes");

            dataLoan.sFundingCfmAgentRoleT = (E_AgentRoleT)GetInt("CFM_AgentRoleT");
            dataLoan.sFundingCfmIsLocked = GetBool("CFM_IsLocked");

            dataLoan.sFundingCompanyName = GetString("sFundingCompanyName");
            dataLoan.sFundingBranchName = GetString("sFundingBranchName");
            dataLoan.sFundingContactName = GetString("sFundingContactName");
            dataLoan.sFundingCompanyAddr = GetString("sFundingCompanyAddr");
            dataLoan.sFundingCompanyCity = GetString("sFundingCompanyCity");
            dataLoan.sFundingCompanyState = GetString("sFundingCompanyState");
            dataLoan.sFundingCompanyZip = GetString("sFundingCompanyZip");
            dataLoan.sFundingCompanyPhone = GetString("sFundingCompanyPhone");
            dataLoan.sFundingCompanyFax = GetString("sFundingCompanyFax");

            dataLoan.sFundingBankName = GetString("sFundingBankName");
            dataLoan.sFundingBankCityState = GetString("sFundingBankCityState");
            dataLoan.sFundingABANumber = GetString("sFundingABANumber");
            dataLoan.sFundingAccountNumber = GetString("sFundingAccountNumber");
            dataLoan.sFundingAccountName = GetString("sFundingAccountName");

            dataLoan.sFundingFurtherCreditToAccountName = GetString("sFundingFurtherCreditToAccountName");
            dataLoan.sFundingFurtherCreditToAccountNumber = GetString("sFundingFurtherCreditToAccountNumber");
            dataLoan.sFundingRequestBatchNumber = GetString("sFundingRequestBatchNumber");

            dataLoan.sFundingAdditionalInstructionsLine1 = GetString("sFundingAdditionalInstructionsLine1");
            dataLoan.sFundingAdditionalInstructionsLine2 = GetString("sFundingAdditionalInstructionsLine2");

            dataLoan.sConsummationD_rep = GetString("sConsummationD");
            dataLoan.sConsummationDLckd = GetBool("sConsummationDLckd");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sWarehouseLenderRolodexId", dataLoan.sWarehouseLenderRolodexId);
            SetResult("sWarehouseAdvanceClassification", dataLoan.sWarehouseAdvanceClassification);
            SetResult("sFundingDisbursementMethodT", dataLoan.sFundingDisbursementMethodT);
            SetResult("sWarehouseFunderDesc", dataLoan.sWarehouseFunderDesc);
            SetResult("sWarehouseMaxFundPc", dataLoan.sWarehouseMaxFundPc_rep);
            SetResult("sWarehouseMaxFundAmt", dataLoan.sWarehouseMaxFundAmt_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sSettlementChargesDedFromLoanProc", dataLoan.sSettlementChargesDedFromLoanProc_rep);
            SetResult("sSettlementTotalFundByLenderAtClosing", dataLoan.sSettlementTotalFundByLenderAtClosing_rep);
            SetResult("sOtherFundAdj", dataLoan.sOtherFundAdj_rep);
            SetResult("sAmtReqToFund", dataLoan.sAmtReqToFund_rep);
            SetResult("sAmtFundFromWarehouseLine", dataLoan.sAmtFundFromWarehouseLine_rep);
            SetResult("sAmtFundFromWarehouseLineLckd", dataLoan.sAmtFundFromWarehouseLineLckd);
            SetResult("sFundReqForShortfall", dataLoan.sFundReqForShortfall_rep);
            SetResult("sChkDueFromClosingRcvd", dataLoan.sChkDueFromClosingRcvd);
            SetResult("sChkDueFromClosing", dataLoan.sChkDueFromClosing_rep);
            SetResult("sSchedFundD", dataLoan.sSchedFundD);
            SetResult("sFundsOrderedD", dataLoan.sFundsOrderedD);
            SetResult("sFundD", dataLoan.sFundD_rep);
            SetResult("sFundN", dataLoan.sFundN);
            SetResult("sShippedToWarehouseD", dataLoan.sShippedToWarehouseD_rep);
            SetResult("sShippedToWarehouseN", dataLoan.sShippedToWarehouseN);
            SetResult("sTrackingN", dataLoan.sTrackingN);
            SetResult("sLoanProceedsToT", dataLoan.sLoanProceedsToT);
            SetResult("sFundNotes", dataLoan.sFundNotes);
            SetResult("sChkDueFromClosingIsZero", dataLoan.sChkDueFromClosingIsZero);

            SetResult("sFundingCompanyName", dataLoan.sFundingCompanyName);
            SetResult("sFundingBranchName", dataLoan.sFundingBranchName);
            SetResult("sFundingContactName", dataLoan.sFundingContactName);
            SetResult("sFundingCompanyAddr", dataLoan.sFundingCompanyAddr);
            SetResult("sFundingCompanyCity", dataLoan.sFundingCompanyCity);
            SetResult("sFundingCompanyState", dataLoan.sFundingCompanyState);
            SetResult("sFundingCompanyZip", dataLoan.sFundingCompanyZip);
            SetResult("sFundingCompanyPhone", dataLoan.sFundingCompanyPhone);
            SetResult("sFundingCompanyFax", dataLoan.sFundingCompanyFax);

            SetResult("sFundingBankName", dataLoan.sFundingBankName);
            SetResult("sFundingBankCityState", dataLoan.sFundingBankCityState);
            SetResult("sFundingABANumber", dataLoan.sFundingABANumber.Value);
            SetResult("sFundingAccountNumber", dataLoan.sFundingAccountNumber.Value);
            SetResult("sFundingAccountName", dataLoan.sFundingAccountName);

            SetResult("sFundingFurtherCreditToAccountName", dataLoan.sFundingFurtherCreditToAccountName);
            SetResult("sFundingFurtherCreditToAccountNumber", dataLoan.sFundingFurtherCreditToAccountNumber.Value);
            SetResult("sFundingRequestBatchNumber", dataLoan.sFundingRequestBatchNumber);

            SetResult("sFundingAdditionalInstructionsLine1", dataLoan.sFundingAdditionalInstructionsLine1.Value);
            SetResult("sFundingAdditionalInstructionsLine2", dataLoan.sFundingAdditionalInstructionsLine2.Value);

            SetResult("sConsummationD", dataLoan.sConsummationD_rep);
            SetResult("sConsummationDLckd", dataLoan.sConsummationDLckd);
        }
    }
    public partial class FundingService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new FundingServiceItem());
        }
    }


}
