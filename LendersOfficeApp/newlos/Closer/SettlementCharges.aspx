﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="SettlementCharges.aspx.cs" Inherits="LendersOfficeApp.newlos.Closer.SettlementCharges" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimate2010RightColumn" Src="../../los/LegalForm/GoodFaithEstimate2010RightColumn.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimate2010Footer" Src="~/newlos/Forms/GoodFaithEstimate2010Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Settlement Charges</title>
    
    <style type="text/css">
		    /* Adding "_" before attribute name makes those attributes valid only in IE 6 or quirks (IE 5.5) mode */
		    html, body{
	            _height: 100%;
	            _overflow: hidden;
            }

            #wrapper {
	            _width: 100%;
	            _height: 100%;
	            _overflow: auto;
	            _position: relative;
            }
            
		    #footerWrapper 
		    {
		        position: fixed;
		        left: 0px;
		        bottom: 0px;
		        width: 100%;
		        
	            _position: absolute; /* Overrides position attribute in quirks mode */
            }
            
            #footer {
	            _margin-right: 17px; /* for right scroll bar in quirks mode */
	            
	            /**
	            /* Following properties needed to keep mouse clicks from falling through to underlying divs.
	            /* Placed at #footer level instead of #footerWrapper so that user can still click on #wrapper's scrollbar
	            **/
	            background: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7); /* background image set to 1x1 transparent GIF. */
	            width: 100%; /* needs to be set to 100%, or #footer width will match width of active tab */
            }
        </style>
</head>
<body bgcolor="gainsboro">
    <script type="text/javascript">
      <!--      
      var linkedPaidTo = new Array("sSettlementApprFProps_ctrl_PaidTo_tb"
                              ,"sSettlementCrFProps_ctrl_PaidTo_tb"
                              ,"sSettlementFloodCertificationFProps_ctrl_PaidTo_tb"
                              ,"sSettlementEscrowFProps_ctrl_PaidTo_tb"
                              ,"sSettlementTitleInsFProps_ctrl_PaidTo_tb"
                              ,"sSettlementAttorneyFProps_ctrl_PaidTo_tb"
                              );
      
      function _init() {
        <%=AspxTools.JsGetElementById(copyFromGFE)%>.value = 'false';
        if(document.getElementById('_ReadOnly').value != 'True')
        {
          updateOriginatorCompensationUI();
          lockField(<%=AspxTools.JsGetElementById(sSchedDueD1Lckd)%>, 'sSchedDueD1');
          lockField(<%=AspxTools.JsGetElementById(sSettlementAggregateAdjRsrvLckd)%>, 'sSettlementAggregateAdjRsrv');
          lockField(<%=AspxTools.JsGetElementById(sSettlementIPerDayLckd)%>, 'sSettlementIPerDay');
          lockField(<%=AspxTools.JsGetElementById(sSettlementIPiaDyLckd)%>, 'sSettlementIPiaDy');
          
          // OPM 122786 - If it is a purchase loan then we want to have line
          // 1103 mimic line 1104
          if (<%= AspxTools.JsGetElementById(hfsLPurposeT) %>.value === 
                  <%= AspxTools.JsString(E_sLPurposeT.Purchase) %>) {
              linkedPaidTo.push('sSettlementOwnerTitleInsFProps_ctrl_PaidTo_tb');
          }
          verifyPaidToTBStatus();
          verifyLinkedPaidToTBStatus();
    
          bIsAutoCalculate = document.getElementById("ByPassBgCalcForGfeAsDefault_0").checked;
          document.getElementById("btnCalculate").disabled = bIsAutoCalculate;
    
          var elements = document.forms[0];
          for (var j = 0; j < elements.length; j++) {
            var elementName = elements[j].name;
            clientID = elementName.substring(0, elementName.indexOf(':Poc_chk'));
            if(clientID != '' && typeof(enableDisableDflp) == 'function')
              enableDisableDflp(clientID);
          }
        }
        
        toggleRecFFields();
        
        // Set combobox item lists in footer to fixed position
        if (document.documentMode >= 8) {
            $(".combobox").filter("[id^=Gfe2010Footer_dot_]").css("position", "fixed");
        }
        
        if (typeof _initControl === 'function') {
            _initControl();
        }
      }
      
      function toggleRecFFields()
      {
          var sSettlementRecFLckd = <%= AspxTools.JsGetElementById(sSettlementRecFLckd)%>;
          var $sp1201 = $("#sp1201");
          var sSettlementRecDeed = <%= AspxTools.JsGetElementById(sSettlementRecDeed)%>;
          var sSettlementRecMortgage = <%= AspxTools.JsGetElementById(sSettlementRecMortgage)%>;
          var sSettlementRecRelease = <%= AspxTools.JsGetElementById(sSettlementRecRelease)%>;
          
          if( sSettlementRecFLckd.checked )
          {
            $sp1201.show();
            sSettlementRecDeed.readOnly = true;
            sSettlementRecMortgage.readOnly = true;
            sSettlementRecRelease.readOnly = true;
          }
          else
          {
            $sp1201.hide();
            sSettlementRecDeed.readOnly = false;
            sSettlementRecMortgage.readOnly = false;
            sSettlementRecRelease.readOnly = false;
          }
      }
      
      function OnCopyFromGfeClicked() {
        updateDirtyBit();
        <%=AspxTools.JsGetElementById(copyFromGFE)%>.value = 'true';
        backgroundCalculation();
        if ( document.getElementById('ccTemplateMissing').value == "1") <%-- // OPM 57095 --%>
            alert('The original CC template applied to this loan file no longer exists. The DFLP checkboxes may need to be set manually.');
      }
      
      function _postRefreshCalculation(results) {
        for(entry in results) {
          if(entry.indexOf('Props_ctrl_Page2') == -1)
            continue;
          
          value = results[entry];          
          checkRB(entry + 'A_rb', value);
          checkRB(entry + 'B_rb', value);
          checkRB(entry + 'C_rb', value);
        }
      }
      
      function checkRB(rbID, value) {
        var rb = document.getElementById(rbID);
        if (rb != null) 
          rb.checked = rb.value == value;      
      }
  function getPaymentSource()
  {
    // THere are 3 options.
    for (var i = 0; i < 3; i++)
    {
      var o = document.getElementById("sOriginatorCompensationPaymentSourceT_" + i);
      if (null != o && o.checked)
      {
        return o.value;
      }
    }
    return "";
  }      
  function updateOriginatorCompensationUI()
  {
    <% if (IsReadOnly == false) { %>
    
    var paidBy = getPaymentSource(); // 0 - not specified, 1 - borrower, 2 - lender
    
    var sGfeOriginatorCompFPcDisabled = paidBy == 2 || (paidBy == 0 && <%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>.value == '6');
    
    <%= AspxTools.JsGetElementById(sGfeOriginatorCompFPc) %>.readOnly =  sGfeOriginatorCompFPcDisabled;
    disableDDL(<%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>, paidBy == 2);
    <%= AspxTools.JsGetElementById(sGfeOriginatorCompFMb) %>.readOnly = paidBy == 2;
    
    document.getElementById("of_label").style.visibility = <%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>.value == '6' ? 'hidden' : 'visible';
    if (paidBy === "0" || paidBy === "1")
    {
      var sGfeOriginatorCompFBaseT = <%= AspxTools.JsGetElementById(sGfeOriginatorCompFBaseT) %>;
      if (paidBy === "0")
      {
        var bHasAllYsp = false;
        for (var i = 0; i < sGfeOriginatorCompFBaseT.options.length; i++)
        {
          if (sGfeOriginatorCompFBaseT.options[i].value === '6')
          {
            bHasAllYsp = true;
            break;
          }
        }
        
        if (bHasAllYsp === false)
        {
          var opt = document.createElement("option");
          opt.text = "All YSP";
          opt.value = "6";
          sGfeOriginatorCompFBaseT.options.add(opt);
        }
      }
      else if (paidBy === "1")
      {
        for (var i = 0; i < sGfeOriginatorCompFBaseT.options.length; i++)
        {
          var opt = sGfeOriginatorCompFBaseT.options[i]
          if (opt.value === '6')
          {
            sGfeOriginatorCompFBaseT.options.remove(i);
            break;
          }
        }
      }
    }
    <% } %>
  }
  
  function verifyPaidToTBStatus()
  {
    unlockPaidTo('sSettlementTxServFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementInspectFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementProcFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementUwFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementWireFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlement800U1FProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlement800U2FProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlement800U3FProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlement800U4FProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlement800U5FProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementOwnerTitleInsFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementDocPrepFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementNotaryFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementU1TcProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementU2TcProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementU3TcProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementU4TcProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementPestInspectFProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementU1ScProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementU2ScProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementU3ScProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementU4ScProps_ctrl_ToBrok_chk');
    unlockPaidTo('sSettlementU5ScProps_ctrl_ToBrok_chk');
  }
  
  function verifyLinkedPaidToTBStatus() {
    var i, id, tb, tbID;
    for (i in linkedPaidTo) {
      tbID = linkedPaidTo[i];
      tb = document.getElementById(tbID);
      id = tbID.substring(0, tbID.indexOf('_PaidTo_tb'));
      Brok_cb = document.getElementById(id + '_ToBrok_chk');
      
      if (Brok_cb.checked)
        tb.readOnly = 'true';
      else
        tb.readOnly = <%= AspxTools.JsGetElementById(hfsGfeUsePaidToFromOfficialContact) %>.value === "true";
    }
  }
  
  function unlockPaidTo(id) {
    tbID = id.substring(0, id.indexOf('_ToBrok_chk')) + '_PaidTo_tb';
    var LinkCbChecked = <%= AspxTools.JsGetElementById(hfsGfeUsePaidToFromOfficialContact) %>.value === "true";
    if(!LinkCbChecked || !checkLinkedPaidTo(tbID))
      document.getElementById(tbID).readOnly = document.getElementById(id).checked;
  }

  function checkLinkedPaidTo(tbID) {
    var i = "";
    for (i in linkedPaidTo) {
      if (tbID == linkedPaidTo[i])
        return true;
    }
    return false;
  }

  function doAfterDateFormat(e)
  {
      refreshCalculation();
  }
  
  <%if(IsClosingCostMigrationArchivePage){ %>
  refreshCalculation = function(){};
  saveMe = function(){};
  backgroundCalculation = function(){};
  <%} %>
    //-->
	</script>
	<form id="SettlementCharges" runat="server">
        <div id="wrapper">
	    <asp:HiddenField ID="copyFromGFE" runat="server" Visible="true" />
	    <asp:HiddenField runat="server" ID="hfsLPurposeT" />
	    <asp:HiddenField runat="server" ID="hfsGfeUsePaidToFromOfficialContact" />
	    <asp:HiddenField runat="server" ID="hfIsClosingCostMigrationArchivePage" />
	    
	    <input type=hidden id="ccTemplateMissing" value="0" />
		<TABLE cellSpacing="0" cellPadding="0" border="0">
			<TR>
				<TD class="MainRightHeader" noWrap><ml:EncodedLiteral runat="server" ID="TitleLiteral" Text="Settlement Charges"></ml:EncodedLiteral></TD>
			</TR>
			<TR>
				<TD style="padding:4px">
					<TABLE cellSpacing="0" cellPadding="0" border="0">
                        <asp:PlaceHolder runat="server" ID="phFieldsBefore800">
						<tr>
						    <td colspan="12">
					    		<TABLE class="FieldLabel">
					                <tr>
							            <td class="FieldLabel" nowrap colspan="4">
							                Perform calculations:
							                <input type="radio" name="ByPassBgCalcForGfeAsDefault" ID="ByPassBgCalcForGfeAsDefault_0" value="0" runat="server" onclick="backgroundCalculation();" />automatically
                                            <input type="radio" name="ByPassBgCalcForGfeAsDefault" ID="ByPassBgCalcForGfeAsDefault_1" value="1" runat="server" onclick="backgroundCalculation();" />manually
                                            <input disabled="disabled" accesskey="c" onclick="backgroundCalculation();" tabindex="-1" type="button" value="Recalculate  (Alt + C)" name="btnCalculate" id="btnCalculate">
							            </td>
						            </tr>
						            <TR>
				                        <TD>
				                            Total Loan Amt
				                        </TD>
                                        <TD>
                                            <ml:moneytextbox id="sFinalLAmt" runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox>
                                        </TD>
                                        <TD>
                                            1st Payment Date
                                            <asp:CheckBox runat="server" ID="sSchedDueD1Lckd" Text="Lock" onclick="lockField(this, 'sSchedDueD1'); refreshCalculation();"/>
                                        </TD>
                                        <TD>
                                            <ml:datetextbox id="sSchedDueD1" runat="server" width="75" preset="date" CssClass="mask" onchange="date_onblur(null, this);" ></ml:datetextbox>
                                        </TD>
				                    </TR>
				                    <TR>
				                        <TD>
				                            Interest Rate
				                        </TD>
                                        <TD>
                                            <ml:percenttextbox id="sNoteIR" runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox>
                                        </TD>
    					                <TD>
    					                    Scheduled Funding
    					                </TD>
                                        <TD>
                                            <ml:datetextbox id="sSchedFundD" runat="server" width="75" preset="date" CssClass="mask" onchange="date_onblur(null, this);" ></ml:datetextbox>
                                        </TD>
				                    </TR>
				                    <TR>
				                        <TD>
				                            Term/Due
				                        </TD>
                                        <TD>
                                            <asp:textbox id="sTerm" runat="server" Width="38px" ReadOnly="True" MaxLength="3"></asp:textbox> / <asp:textbox id="sDue" runat="server" Width="38px" ReadOnly="True" MaxLength="3"></asp:textbox>
                                        </TD>
				                    </TR>
				                    <TR>
				                        <TD>
				                            Amort Type
				                        </TD>
                                        <TD>
                                            <asp:dropdownlist id="sFinMethT" runat="server" Enabled="false"></asp:dropdownlist>
                                        </TD>
                                        <TD>
                                            Days In Year
                                        </TD>
                                        <TD>
                                            <asp:textbox id="sDaysInYr" runat="server" Width="38px" MaxLength="3"></asp:textbox>
                                        </TD>
				                    </TR>
				                </TABLE>
						    </td>
						</tr>
						<TR class="FieldLabel">
					        <TD colspan="14" align="right">
					            <input type="button" value="Copy values from GFE" onclick="OnCopyFromGfeClicked();" />
					        </TD>
					    </TR>
									    <tr class="FormTableHeader">
									      <td></td>
									      <td colspan="11">Loan originator compensation source</td>
									    </tr>
									    <tr>
									      <td></td>
									      <td colspan="13" class="FieldLabel" style="padding-top:5px;padding-bottom:5px">Loan originator is paid by
									      <asp:RadioButtonList ID="sOriginatorCompensationPaymentSourceT" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" onclick="refreshCalculation();"/>
									      </td>
									    </tr>
                        </asp:PlaceHolder>
						<TR>
							<TD class="FormTableHeader" colSpan="14" align="center">
							    POC = Paid Outside of Closing&nbsp;&nbsp;&nbsp;
							    PD = Paid&nbsp;&nbsp;&nbsp;
							    DFLP = Deducted From Loan Proceeds
							    B = Paid to broker</TD>
							</TD>
						</TR>
						<TR>
							<TD class="FormTableHeader"></TD>
							<TD class="FormTableHeader">Description of Charge</TD>
							<TD class="FormTableHeader"></TD>
							<TD class="FormTableHeader">Amount</TD>
							<TD class="FormTableHeader">GFE</TD>
							<TD class="FormTableHeader">Paid By</TD>
							<TD class="FormTableHeader" colspan="5"></TD>
							<TD class="FormTableHeader">Paid To</TD>
							<td></td>
						</TR>
						<TR>
							<TD class="FormTableSubheader">800</TD>
							<TD class="FormTableSubheader" colSpan="13">
								ITEMS PAYABLE IN CONNECTION WITH LOAN</TD>
						</TR>
						<TR>
							<TD class="FieldLabel">801</TD>
							<TD class="FieldLabel">Loan origination fee</TD>
							<TD class="FieldLabel"><ml:percenttextbox id="sSettlementLOrigFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" EnableViewState="False"></ml:percenttextbox>
								+
								<ml:moneytextbox id="sSettlementLOrigFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="59" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
							<TD><ml:moneytextbox id="sSettlementLOrigF" runat="server" preset="money" width="77" ReadOnly="True" EnableViewState="False"></ml:moneytextbox></TD>
							<uc1:goodfaithestimate2010rightcolumn id="sSettlementLOrigFProps_ctrl" Page2Value="A1" SettlementMode="true" PaidToTBVisible="false" PaidToBCBVisible="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<tr>
                            <td></td>
                            <td class="FieldLabel">Originator compensation</td>
                            <td class="FieldLabel"><ml:PercentTextBox ID="sGfeOriginatorCompFPc" runat="server" preset="percent" Width="70" onchange="refreshCalculation();" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" />
                                <span id="of_label">of</span>
                                <asp:DropDownList ID="sGfeOriginatorCompFBaseT" runat="server" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onchange="refreshCalculation();" />
                                +
                                <ml:MoneyTextBox ID="sGfeOriginatorCompFMb" runat="server" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" preset="money" Width="59" onchange="refreshCalculation();" />
                            </td>
                            <td><ml:MoneyTextBox ID="sGfeOriginatorCompF" runat="server" ReadOnly="true" Width="77"/></td>
                            <uc1:GoodFaithEstimate2010RightColumn ID="sGfeOriginatorCompFProps_ctrl" Page2Value="A1" SettlementMode="True" PocCBVisible="true" PaidToTBVisible="false" PaidToBCBVisible="true" runat="server" />
						</tr>

                        <%-- <LINE 802> --%>
						<tr>
							<td class="FieldLabel">802</td>
							<td class="FieldLabel">Credit (-) or Charge (+)</td>
							<td class="FieldLabel" noWrap>
                                <div runat="server" id="settlementLDiscntCalc">
                                    <ml:percenttextbox id="sSettlementLDiscntPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" EnableViewState="False"></ml:percenttextbox>&nbsp;of
								    <asp:dropdownlist id="sSettlementLDiscntBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
								    +
								    <ml:moneytextbox id="sSettlementLDiscntFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="59" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox>
                                </div>        
                            </td>
							<td><ml:moneytextbox id="sSettlementLDiscnt" runat="server" preset="money" width="76px" ReadOnly="True" EnableViewState="False"></ml:moneytextbox></td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td class="FieldLabel">Credit for lender paid fees</td>
						  <td align="right" style="padding-right:7px">
						    <asp:DropDownList ID="sSettlementCreditLenderPaidItemT" runat="server" onchange="refreshCalculation();"/>
						    <ml:MoneyTextBox ID="sSettlementCreditLenderPaidItemF" width="76px" runat="server" ReadOnly="true" />
						  </td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td class="FieldLabel">General lender credit</td>
						  <td align="right" style="padding-right:7px">
						    <ml:PercentTextBox ID="sSettlementLenderCreditFPc" runat="server" preset="percent" Width="76" ReadOnly="true" />
						    <ml:MoneyTextBox ID="sSettlementLenderCreditF" runat="server" preset="money" Width="76px" ReadOnly="true" /></td>
						  <td>&nbsp;</td>
						</tr>
						<tr>
						  <td>&nbsp;</td>
						  <td class="FieldLabel">Discount points</td>
						  <td align="right" style="padding-right:7px">
						    <ml:PercentTextBox ID="sSettlementDiscountPointFPc" runat="server" preset="percent" Width="76" ReadOnly="true" />
						    <ml:MoneyTextBox ID="sSettlementDiscountPointF" runat="server" preset="money" Width="76px" ReadOnly="true" /></td>
						  <td>&nbsp;</td>
						  <uc1:GoodFaithEstimate2010RightColumn ID="sSettlementDiscountPointFProps_ctrl"  Page2A_rbVisible="false" runat="server" SettlementMode="True" PocCBVisible="false" PaidToTBVisible="false" />
						</tr>
						<%-- </LINE 802> --%>
						
						<tr>
						  <td></td>
						  <td colspan="2" class="FieldLabel"><asp:CheckBox ID="sGfeIsTPOTransaction" runat="server" Text="This transaction involves a TPO"/></td>
						</tr>
						<TR>
							<TD class="FieldLabel">804</TD>
							<TD class="FieldLabel">Appraisal fee</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementApprF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementApprFProps_ctrl" Page2Value="B3" SettlementMode="true" runat="server" PaidCBVisible="true"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">805</TD>
							<TD class="FieldLabel">Credit report</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementCrF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementCrFProps_ctrl" Page2Value="B3" SettlementMode="true" runat="server" PaidCBVisible="true"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">806</TD>
							<TD class="FieldLabel">Tax service fee</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementTxServF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementTxServFProps_ctrl" Page2Value="B3" SettlementMode="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
						    <TD class="FieldLabel">807</TD>
							<TD class="FieldLabel">Flood Certification</TD>
							<TD><asp:dropdownlist id="sSettlementFloodCertificationDeterminationT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist></TD>
							<TD><ml:moneytextbox id="sSettlementFloodCertificationF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementFloodCertificationFProps_ctrl" Page2Value="B3" SettlementMode="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">808</TD>
							<TD class="FieldLabel">Mortgage broker fee</TD>
							<TD class="FieldLabel"><ml:percenttextbox id="sSettlementMBrokFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" EnableViewState="False"></ml:percenttextbox>&nbsp;of
								<asp:dropdownlist id="sSettlementMBrokFBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
								+
								<ml:moneytextbox id="sSettlementMBrokFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="59" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
							<TD><ml:moneytextbox id="sSettlementMBrokF" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementMBrokFProps_ctrl" Page2Value="A1" SettlementMode="true" PaidToTBVisible="false" PaidToBCBVisible="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">809</TD>
							<TD class="FieldLabel">Lender's inspection fee</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementInspectF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();" EnableViewState="False"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementInspectFProps_ctrl" Page2Value="A1" SettlementMode="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">810</TD>
							<TD class="FieldLabel">Processing fee</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementProcF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementProcFProps_ctrl" Page2Value="A1" SettlementMode="true" PaidCBVisible="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">811</TD>
							<TD class="FieldLabel">Underwriting fee</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementUwF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementUwFProps_ctrl" Page2Value="A1" SettlementMode="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">812</TD>
							<TD class="FieldLabel">Wire transfer</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementWireF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementWireFProps_ctrl" Page2Value="A1" SettlementMode="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">813</TD>
							<TD colSpan="2"><asp:textbox id="sSettlement800U1FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlement800U1F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlement800U1FProps_ctrl" Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">814</TD>
							<TD colSpan="2"><asp:textbox id="sSettlement800U2FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlement800U2F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlement800U2FProps_ctrl" Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">815</TD>
							<TD colSpan="2"><asp:textbox id="sSettlement800U3FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlement800U3F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlement800U3FProps_ctrl" Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">816</TD>
							<TD colSpan="2"><asp:textbox id="sSettlement800U4FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlement800U4F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlement800U4FProps_ctrl" Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FieldLabel">817</TD>
							<TD colSpan="2"><asp:textbox id="sSettlement800U5FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlement800U5F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlement800U5FProps_ctrl" Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
						</TR>
						<TR>
							<TD class="FormTableSubheader"><A name="900"></A>900</TD>
							<TD class="FormTableSubheader" colSpan="13">ITEMS REQUIRED BY LENDER TO BE PAID IN ADVANCE</TD>
						</TR>
						<TR>
							<TD class="FieldLabel">901</TD>
							<TD class="FieldLabel">Interest for&nbsp;&nbsp; <asp:CheckBox ID="sSettlementIPiaDyLckd" onclick="lockField(this, 'sSettlementIPiaDy'); refreshCalculation();" Text="Lock" runat="server" /> </TD>
							<TD class="FieldLabel" noWrap><asp:textbox id="sSettlementIPiaDy" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="40px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;days 
								@
								<asp:CheckBox id="sSettlementIPerDayLckd" runat="server" onclick="lockField(this, 'sSettlementIPerDay'); refreshCalculation();" Text="Lock"></asp:CheckBox>
								<ml:moneytextbox id="sSettlementIPerDay" runat="server" preset="money" onchange="refreshCalculation();" decimalDigits="6"></ml:moneytextbox>
								per day</TD>
							<TD><ml:moneytextbox id="sSettlementIPia" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementIPiaProps_ctrl" Page2Value="B10" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">902</TD>
							<TD class="FieldLabel" noWrap colSpan="2" rowSpan="1">
							    <A href="javascript:linkMe('../LoanInfo.aspx?pg=1');">Mortgage Insurance Premium</A>
							</TD>
							<TD><ml:moneytextbox id="sMipPia" runat="server" preset="money" width="76px" readonly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sMipPiaProps_ctrl" Page2Value="B3" SettlementMode="true" PaidToBCBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">903</TD>
							<TD class="FieldLabel" noWrap colSpan="2">
                                Haz Ins. @
                                <asp:PlaceHolder runat="server" id="sProHazInsHolder">
                                    <ml:percenttextbox id="sProHazInsR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();" DESIGNTIMEDRAGDROP="236"></ml:percenttextbox>
								    of
								    <asp:dropdownlist id="sProHazInsT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
								    +
								    <ml:moneytextbox id="sProHazInsMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="60px" onchange="refreshCalculation();" decimalDigits="4"></ml:moneytextbox>
								    for
                                </asp:PlaceHolder>
                                <asp:textbox id="sSettlementHazInsPiaMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="30px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>
								mths
							</TD>
							<TD><ml:moneytextbox id="sSettlementHazInsPia" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementHazInsPiaProps_ctrl" Page2Value="B11" SettlementMode="true" PaidToBCBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">904</TD>
							<TD colSpan="2"><asp:textbox id="sSettlement904PiaDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlement904Pia" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlement904PiaProps_ctrl" Page2Option1Text="B3" Page2Option1Value="B3" Page2Option2Text="B11" Page2Option2Value="B11" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">905</TD>
							<TD class="FieldLabel"><A href="javascript:linkMe('../LoanInfo.aspx?pg=1');">VA Funding 
									Fee</A></TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sVaFf" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" ReadOnly="True" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sVaFfProps_ctrl" Page2Value="B3" SettlementMode="true" PaidToBCBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">906</asp:textbox></TD>
							<TD colSpan="2"><asp:textbox id="sSettlement900U1PiaDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlement900U1Pia" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlement900U1PiaProps_ctrl" Page2Option1Text="B3" Page2Option1Value="B3" Page2Option2Text="B11" Page2Option2Value="B11" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FormTableSubheader"><A name="1000"></A>1000</TD>
							<TD class="FormTableSubheader" colSpan="13">RESERVES DEPOSITED WITH LENDER</TD>
						</TR>
						<TR>
							<TD class="FieldLabel">1002</TD>
							<TD class="FieldLabel">Haz ins. reserve</TD>
							<TD class="FieldLabel"><asp:textbox id="sSettlementHazInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
								@
								<ml:moneytextbox id="sProHazIns" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>
								/ month</TD>
							<TD><ml:moneytextbox id="sSettlementHazInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementHazInsRsrvProps_ctrl" Page2Value="B9" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1003</TD>
							<TD class="FieldLabel" colSpan="2"><A href="javascript:linkMe('../LoanInfo.aspx?pg=1');">Mtg ins.</A> 
								reserve&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = 
								<ml:moneytextbox id="sProMIns" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>&nbsp;&nbsp;for
								<asp:textbox id="sSettlementMInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>
								mths</TD>
							<TD colSpan="1"><ml:moneytextbox id="sSettlementMInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementMInsRsrvProps_ctrl" Page2Value="B9" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1004</TD>
							<TD class="FieldLabel" colSpan="2" noWrap>
                                Tax resrv @
								<asp:PlaceHolder runat="server" ID="sProRealETxHolder">
                                    <ml:percenttextbox id="sProRealETxR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox>&nbsp;of
								    <asp:dropdownlist id="sProRealETxT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
								    +
								    <ml:moneytextbox id="sProRealETxMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="59px" onchange="refreshCalculation();"></ml:moneytextbox>
								    for
								</asp:PlaceHolder>
                                <asp:textbox id="sSettlementRealETxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="31px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>
								mths
							</TD>
							<TD><ml:moneytextbox id="sSettlementRealETxRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementRealETxRsrvProps_ctrl" Page2Value="B9" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1005</TD>
							<TD class="FieldLabel">School taxes</TD>
							<TD class="FieldLabel"><asp:textbox id="sSettlementSchoolTxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
								@
								<ml:moneytextbox id="sProSchoolTx" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
								/ month</TD>
							<TD><ml:moneytextbox id="sSettlementSchoolTxRsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementSchoolTxRsrvProps_ctrl" Page2Value="B9" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1006</TD>
							<TD class="FieldLabel">Flood ins. reserve</TD>
							<TD class="FieldLabel"><asp:textbox id="sSettlementFloodInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
								@
								<ml:moneytextbox id="sProFloodIns" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
								/ month</TD>
							<TD><ml:moneytextbox id="sSettlementFloodInsRsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementFloodInsRsrvProps_ctrl" Page2Value="B9" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1007</TD>
							<TD class="FieldLabel">Aggregate adjustment</TD>
							<TD align="right"><INPUT type="button" value="Aggregate Escrow" onclick="linkMe('../Forms/AggregateEscrowDisclosure.aspx');">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:CheckBox id="sSettlementAggregateAdjRsrvLckd" runat="server" onclick="lockField(this, 'sSettlementAggregateAdjRsrv'); refreshCalculation();" Text="Locked" CssClass="FieldLabel"></asp:CheckBox></TD>
							<TD><ml:moneytextbox id="sSettlementAggregateAdjRsrv" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementAggregateAdjRsrvProps_ctrl" Page2Value="B9" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1008</TD>
							<TD><asp:textbox id="s1006ProHExpDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="135" MaxLength="21"></asp:textbox></TD>
							<TD class="FieldLabel"><asp:textbox id="sSettlement1008RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
								@
								<ml:moneytextbox id="s1006ProHExp" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
								/ month</TD>
							<TD><ml:moneytextbox id="sSettlement1008Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlement1008RsrvProps_ctrl" Page2Value="B9" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1009</TD>
							<TD><asp:textbox id="s1007ProHExpDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="135" MaxLength="21"></asp:textbox></TD>
							<TD class="FieldLabel"><asp:textbox id="sSettlement1009RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
								@
								<ml:moneytextbox id="s1007ProHExp" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
								/ month</TD>
							<TD><ml:moneytextbox id="sSettlement1009Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlement1009RsrvProps_ctrl" Page2Value="B9" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<asp:PlaceHolder runat="server" ID="phAdditionalSection1000CustomFees">
    						<tr>
    							<td class="FieldLabel">1010</td>
    							<td><asp:textbox id="sU3RsrvDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="135" MaxLength="21"></asp:textbox></td>
    							<td class="FieldLabel"><asp:textbox id="sSettlementU3RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
    								@
    								<ml:moneytextbox id="sProU3Rsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
    								/ month</td>
    							<td><ml:moneytextbox id="sSettlementU3Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></td>
    							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU3RsrvProps_ctrl" Page2Value="B9" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
    						</tr>
    						<tr>
    							<td class="FieldLabel">1011</td>
    							<td><asp:textbox id="sU4RsrvDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="135" MaxLength="21"></asp:textbox></td>
    							<td class="FieldLabel"><asp:textbox id="sSettlementU4RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="41px" onchange="refreshCalculation();" MaxLength="4"></asp:textbox>&nbsp;mths 
    								@
    								<ml:moneytextbox id="sProU4Rsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox>
    								/ month</td>
    							<td><ml:moneytextbox id="sSettlementU4Rsrv" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></td>
    							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU4RsrvProps_ctrl" Page2Value="B9" SettlementMode="true" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
    						</tr>
						</asp:PlaceHolder>
						<TR>
							<TD class="FormTableSubheader">1100</TD>
							<TD class="FormTableSubheader" colSpan="13">TITLE CHARGES</TD>
						</TR>
						<TR>
							<TD class="FieldLabel">1102</TD>
							<TD class="FieldLabel">Closing/Escrow Fee</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementEscrowF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementEscrowFProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1103</TD>
							<TD class="FieldLabel">Owner's title Insurance</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementOwnerTitleInsF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementOwnerTitleInsFProps_ctrl" BorrVisible="true" Page2Value="B5" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1104</TD>
							<TD class="FieldLabel">Lender's title Insurance</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementTitleInsF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementTitleInsFProps_ctrl" BorrVisible="true" Page2Value="B4"  SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1109</TD>
							<TD class="FieldLabel">Doc preparation fee</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementDocPrepF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementDocPrepFProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1110</TD>
							<TD class="FieldLabel">Notary fees</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementNotaryF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementNotaryFProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1111</TD>
							<TD class="FieldLabel">Attorney fees</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementAttorneyF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementAttorneyFProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1112</TD>
							<TD colSpan="2"><asp:textbox id="sSettlementU1TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlementU1Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU1TcProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1113</TD>
							<TD colSpan="2"><asp:textbox id="sSettlementU2TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlementU2Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU2TcProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1114</TD>
							<TD colSpan="2"><asp:textbox id="sSettlementU3TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlementU3Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU3TcProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1115</TD>
							<TD colSpan="2"><asp:textbox id="sSettlementU4TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlementU4Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU4TcProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FormTableSubheader">1200</TD>
							<TD class="FormTableSubheader" noWrap colSpan="13">GOVERNMENT RECORDING &amp; 
								TRANSFER CHARGES</TD>
						</TR>
						<TR>
							<TD class="FieldLabel">1201&nbsp;&nbsp;</TD>
							<TD class="FieldLabel" noWrap>Recording fees</TD>
							<TD class="FieldLabel" noWrap>
							    <span id="sp1201" style="float:left">
							        <ml:percenttextbox id="sSettlementRecFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51" onchange="refreshCalculation();"></ml:percenttextbox>
								    of
								    <asp:dropdownlist id="sSettlementRecBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
								    +
								    <ml:moneytextbox id="sSettlementRecFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" onchange="refreshCalculation();"></ml:moneytextbox>
								</span>
								<asp:CheckBox ID="sSettlementRecFLckd" runat="server" style="float:right" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onclick="toggleRecFFields(); refreshCalculation();"/>
							</TD>
							<TD><ml:moneytextbox id="sSettlementRecF" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementRecFProps_ctrl" Page2Value="B7" SettlementMode="true" PaidToBCBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1202&nbsp;&nbsp;</TD>
							<TD class="FieldLabel" noWrap>Deed <ml:moneytextbox id="sSettlementRecDeed" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<TD class="FieldLabel" noWrap>
							    <span style="float:left">Mortgage <ml:moneytextbox id="sSettlementRecMortgage" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" onchange="refreshCalculation();"></ml:moneytextbox></span>
							    <span style="float:right; margin-right:25px">Release <ml:moneytextbox id="sSettlementRecRelease" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" onchange="refreshCalculation();"></ml:moneytextbox></span>
							</TD>
						</TR>
						<TR>
							<TD class="FieldLabel">1204</TD>
							<TD class="FieldLabel" noWrap>City/County tax stamps</TD>
							<TD class="FieldLabel" noWrap><ml:percenttextbox id="sSettlementCountyRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
								of
								<asp:dropdownlist id="sSettlementCountyRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
								+
								<ml:moneytextbox id="sSettlementCountyRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<TD><ml:moneytextbox id="sSettlementCountyRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementCountyRtcProps_ctrl" Page2Value="B8" SettlementMode="true" PaidToBCBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1205</TD>
							<TD class="FieldLabel" noWrap>State tax/stamps</TD>
							<TD class="FieldLabel" noWrap><ml:percenttextbox id="sSettlementStateRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
								of
								<asp:dropdownlist id="sSettlementStateRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
								+
								<ml:moneytextbox id="sSettlementStateRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<TD><ml:moneytextbox id="sSettlementStateRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementStateRtcProps_ctrl" Page2Value="B8" SettlementMode="true" PaidToBCBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1206</TD>
							<TD><asp:textbox id="sSettlementU1GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="104" MaxLength="21"></asp:textbox></TD>
							<TD class="FieldLabel" noWrap><ml:percenttextbox id="sSettlementU1GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
								of
								<asp:dropdownlist id="sSettlementU1GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
								+
								<ml:moneytextbox id="sSettlementU1GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<TD><ml:moneytextbox id="sSettlementU1GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU1GovRtcProps_ctrl" Page2Option1Text="B7" Page2Option1Value="B7" Page2Option2Text="B8" Page2Option2Value="B8" SettlementMode="true" PaidToBCBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1207</TD>
							<TD><asp:textbox id="sSettlementU2GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="104px" MaxLength="21"></asp:textbox></TD>
							<TD class="FieldLabel" noWrap><ml:percenttextbox id="sSettlementU2GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
								of
								<asp:dropdownlist id="sSettlementU2GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
								+
								<ml:moneytextbox id="sSettlementU2GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<TD><ml:moneytextbox id="sSettlementU2GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU2GovRtcProps_ctrl" Page2Option1Text="B7" Page2Option1Value="B7" Page2Option2Text="B8" Page2Option2Value="B8" SettlementMode="true" PaidToBCBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1208</TD>
							<TD><asp:textbox id="sSettlementU3GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="104px" MaxLength="21"></asp:textbox></TD>
							<TD class="FieldLabel" noWrap><ml:percenttextbox id="sSettlementU3GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="percent" width="51px" onchange="refreshCalculation();"></ml:percenttextbox>
								of
								<asp:dropdownlist id="sSettlementU3GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" onchange="refreshCalculation();"></asp:dropdownlist>
								+
								<ml:moneytextbox id="sSettlementU3GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<TD><ml:moneytextbox id="sSettlementU3GovRtc" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU3GovRtcProps_ctrl" Page2Option1Text="B7" Page2Option1Value="B7" Page2Option2Text="B8" Page2Option2Value="B8" SettlementMode="true" PaidToBCBVisible="false" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FormTableSubheader">1300</TD>
							<TD class="FormTableSubheader" colSpan="13">ADDITIONAL SETTLEMENT CHARGES</TD>
						</TR>
						<TR>
							<TD class="FieldLabel">1302</TD>
							<TD class="FieldLabel">Pest Inspection</TD>
							<TD></TD>
							<TD><ml:moneytextbox id="sSettlementPestInspectF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementPestInspectFProps_ctrl" BorrVisible="true" Page2Value="B6" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1303</TD>
							<TD colSpan="2"><asp:textbox id="sSettlementU1ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlementU1Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU1ScProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1304</TD>
							<TD colSpan="2"><asp:textbox id="sSettlementU2ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlementU2Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU2ScProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1305</TD>
							<TD colSpan="2"><asp:textbox id="sSettlementU3ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlementU3Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU3ScProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1306</TD>
							<TD colSpan="2"><asp:textbox id="sSettlementU4ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlementU4Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU4ScProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel">1307</TD>
							<TD colSpan="2"><asp:textbox id="sSettlementU5ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" Width="95%" MaxLength="100"></asp:textbox></TD>
							<TD><ml:moneytextbox id="sSettlementU5Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="76px" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							<uc1:GoodFaithEstimate2010RightColumn id="sSettlementU5ScProps_ctrl" BorrVisible="true" Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6" Page2Option3Text="N/A" Page2Option3Value="NotApplicable" SettlementMode="true" runat="server"></uc1:goodfaithestimate2010rightcolumn>
						</TR>
						<TR>
							<TD class="FieldLabel" colSpan="3">TOTAL SETTLEMENT CHARGES</TD>
							<TD><ml:moneytextbox id="sSettlementTotalEstimateSettlementCharge" tabIndex="201" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
						</TR>
						<TR>
							<TD class="FieldLabel" colSpan="3">TOTAL DEDUCTED FROM LOAN PROCEEDS</TD>
							<TD><ml:moneytextbox id="sSettlementTotalDedFromLoanProc" tabIndex="202" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
						</TR>
						<TR>
							<TD class="FieldLabel" colSpan="3">TOTAL ITEMS FUNDED BY LENDER AT CLOSING</TD>
							<TD><ml:moneytextbox id="sSettlementTotalFundByLenderAtClosing" tabIndex="203" runat="server" preset="money" width="76px" ReadOnly="True"></ml:moneytextbox></TD>
						</TR>				
					</TABLE>
				</td>
			</tr>
		</TABLE>
	    <uc1:cmodaldlg id="CModalDlg1" runat="server" DESIGNTIMEDRAGDROP="5482"></uc1:cmodaldlg>
	    
		<%-- OPM 170146 - Copywright notice must be placed here or it will be hidden by footer.--%>
		<br />
		<hr>
		<table>
		    <tr>
		        <td nowrap style='font-size:11px'><%=AspxTools.HtmlString(ConstAppDavid.CopyrightMessage)%></td>
		    </tr>
		</table>
	</div>
<%if (showFooter)
  { %>
    <div id="footerWrapper">
        <div id="footer">
            <uc1:GoodFaithEstimate2010Footer ID="Gfe2010Footer" runat="server" />
        </div>
    </div>
	        
    <script type="text/javascript">
        var $wrapper = $("#wrapper");
        var $footerWrapper = $("#footerWrapper");
        var lastWidth = 0;

        // Polling function to catch zoom/resize event
        function pollZoomFireEvent() {
            // jQuery does not support quirks mode. As such, we have
            // to grab the client width to determine the width of
            // the window.
            var widthNow = document.body.clientWidth;
            if (lastWidth == widthNow) return;
            lastWidth = widthNow;
            // Length changed, user must have zoomed or resized. Reset foorter bottom
            setFooterBottom();
        }

        // Moves footer out of the way of horizontal scrollbar, if it's showing
        // Only needed for rendering in quirks mode
        function setFooterBottom() {
            // OPM 220126, ML, 10/19/2015, 
            // Checking the scroll width against the inner width is a more reliable
            // way of determining if there is a scrollbar on the page, as it includes
            // instances where (like in case 220126) a page is opened outside of the
            // loan editor.
            if ($wrapper.get(0).scrollWidth > $wrapper.innerWidth())
                $footerWrapper.css("bottom", "17px");
            else
                $footerWrapper.css("bottom", "0px");
        }

        function setWrapperPadding() {
            $wrapper.css("padding-bottom", $footerWrapper.height() + "px");
        }

        // This should only run in Quirks mode on IE9 or below
        // NOTE: IE10 quirks mode is not the same as in previous versions of IE.
        if (document.documentMode < 8) {
            setFooterBottom();
            setInterval(pollZoomFireEvent, 100);
        }

        setWrapperPadding();
        $.event.add(this, "footer/switchTab", setWrapperPadding);
    </script>
<% } %>
	
	
	
    </form>
</body>
</html>
