﻿#region Generated code
namespace LendersOfficeApp.newlos.Closer
#endregion
{
    using System;
    using DataAccess;
    using LendersOffice.Rolodex;

    /// <summary>
    /// The code behind for the Security Instrument page.
    /// </summary>
    public partial class SecurityInstrument : BaseLoanPage
    {
        /// <summary>
        /// The initialization process for the page.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The sending object.</param>
        /// <param name="e">Event arguments.</param>
        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Security Instrument";
            this.PageID = "SecurityInstrument";

            this.EnableJqueryMigrate = false;

            RolodexDB.PopulateAgentTypeDropDownList(this.sExecutionLocationAgentT);
            this.sExecutionLocationZip.SmartZipcode(this.sExecutionLocationCity, this.sExecutionLocationState, this.sExecutionLocationCountyDropdown);
            this.sExecutionLocationState.Attributes.Add("onchange", "javascript:UpdateCounties(this,document.getElementById('" + this.sExecutionLocationCountyDropdown.ClientID + "'), event);");

            this.sExecutionLocationSourceT_LeaveBlank.Attributes.Add("value", E_sExecutionLocationSourceT.LeaveBlank.ToString("D"));
            this.sExecutionLocationSourceT_PopulateFromContact.Attributes.Add("value", E_sExecutionLocationSourceT.PopulateFromContact.ToString("D"));
            this.sExecutionLocationSourceT_SetManually.Attributes.Add("value", E_sExecutionLocationSourceT.SetManually.ToString("D"));
            this.sExecutionLocationSourceT_SubjectPropertyAddress.Attributes.Add("value", E_sExecutionLocationSourceT.SubjectPropertyAddress.ToString("D"));

            Tools.Bind_sTxSecurityInstrumentPar27T(sTxSecurityInstrumentPar27T);
            Tools.Bind_sNyPropertyStatementT(sNyPropertyStatementT);
        }

        /// <summary>
        /// Loads data to the page from a loan object.
        /// </summary>
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SecurityInstrument));
            dataLoan.InitLoad();

            this.SetExecutionLocationSource(dataLoan.sExecutionLocationSourceT);
            this.sExecutionLocationSourceT_SelectedValue.Value = dataLoan.sExecutionLocationSourceT.ToString("D");
            Tools.SetDropDownListValue(this.sExecutionLocationAgentT, dataLoan.sExecutionLocationAgentT);
            this.sExecutionLocationAddress.Text = dataLoan.sExecutionLocationAddress;
            this.sExecutionLocationCity.Text = dataLoan.sExecutionLocationCity;
            this.sExecutionLocationState.Value = dataLoan.sExecutionLocationState;
            this.sExecutionLocationZip.Text = dataLoan.sExecutionLocationZip;
            this.BindCounty(dataLoan.sExecutionLocationState, dataLoan.sExecutionLocationCounty);

            Tools.SetRadioButtonListValue(sTxSecurityInstrumentPar27T, dataLoan.sTxSecurityInstrumentPar27T);
            Tools.SetRadioButtonListValue(sNyPropertyStatementT, dataLoan.sNyPropertyStatementT);
            sAcknowledgeCashAdvanceForNonHomestead.Checked = dataLoan.sAcknowledgeCashAdvanceForNonHomestead;
            sSecInstrAttorneyFeesPc.Text = dataLoan.sSecInstrAttorneyFeesPc_rep;
            sTrusteeFeePc.Text = dataLoan.sTrusteeFeePc_rep;
        }

        /// <summary>
        /// Binds the county dropdown. This must be done during page load as it requires
        /// the currently selected state and county.
        /// </summary>
        /// <param name="state">The currently selected state.</param>
        /// <param name="county">The currently selected county.</param>
        private void BindCounty(string state, string county)
        {
            Tools.Bind_sSpCounty(state, this.sExecutionLocationCountyDropdown, true);
            Tools.SetDropDownListCaseInsensitive(this.sExecutionLocationCountyDropdown, county);
            this.sExecutionLocationCounty.Value = county;
        }

        /// <summary>
        /// Sets the correct radio button based on the loan's execution location source.
        /// </summary>
        /// <param name="source">The execution location source.</param>
        private void SetExecutionLocationSource(E_sExecutionLocationSourceT source)
        {
            switch (source)
            {
                case E_sExecutionLocationSourceT.LeaveBlank:
                    this.sExecutionLocationSourceT_LeaveBlank.Checked = true;
                    break;
                case E_sExecutionLocationSourceT.PopulateFromContact:
                    this.sExecutionLocationSourceT_PopulateFromContact.Checked = true;
                    break;
                case E_sExecutionLocationSourceT.SetManually:
                    this.sExecutionLocationSourceT_SetManually.Checked = true;
                    break;
                case E_sExecutionLocationSourceT.SubjectPropertyAddress:
                    this.sExecutionLocationSourceT_SubjectPropertyAddress.Checked = true;
                    break;
                default:
                    throw new UnhandledEnumException(source);
            }
        }
    }
}
