﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class FloodCertification : BaseLoanPage
    {
        protected FloodOrderTab FloodOrderTab;
        protected FloodCertDataTab FloodCertDataTab;

        protected void PageInit(object sender, System.EventArgs e)
        {
            RegisterJsScript("utilities.js");
            Tabs.RegisterControl("Flood Order", FloodOrderTab);
            Tabs.RegisterControl("Flood Certification Data", FloodCertDataTab);
            Tabs.AddToQueryString("loanid", LoanID.ToString());

            this.PageTitle = "Flood Certification";
            this.PageID = "FloodCertification";
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            EnableJquery = true;
            UseNewFramework = true;
            IsAppSpecific = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
