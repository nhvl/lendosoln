﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class SettlementChargesMigration : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;

            this.EnableJqueryMigrate = false;

            BindRadioButtonLists();
            this.PageID = "SettlementChargesMigration";
            base.OnInit(e);
        }

        private void BindRadioButtonLists()
        {
            m_currentClosingCosts.Items.AddRange(new ListItem[]
            {
                new ListItem("GFE", E_CurrentClosingCostSourceT.GFE.ToString("D")),
                new ListItem("Settlement Charges", E_CurrentClosingCostSourceT.SettlementCharges.ToString("D"))
            });

            m_lastDisclosedClosingCosts.Items.AddRange(new ListItem[]
            {
                new ListItem("GFE Archive", E_LastDisclosedClosingCostSourceT.GFEArchive.ToString("D")),
                new ListItem("GFE", E_LastDisclosedClosingCostSourceT.GFE.ToString("D")),
                new ListItem("None", E_LastDisclosedClosingCostSourceT.None.ToString("D"))
            });
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SettlementChargesMigration));
            dataLoan.InitLoad();

            sUseGFEDataForSCFields.Value = dataLoan.sUseGFEDataForSCFields.ToString();
            sCurrentClosingCostStructure.Value = dataLoan.sCurrentClosingCostStructure;
            sClosingCostMigrationCurrentClosingCostSource.Value = dataLoan.sClosingCostMigrationCurrentClosingCostSource.ToString("D");
            sClosingCostMigrationLastDisclosedClosingCostSource.Value = dataLoan.sClosingCostMigrationLastDisclosedClosingCostSource.ToString("D");

            // The dictionaries can't have enum keys if we are going to serialize them.
            m_futureMigrationActionsHF.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(x_serializableFutureMigrationActions);
            m_pastMigrationActionsHF.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(x_serializablePastMigrationActions);
        }

        #region Future Migration Actions
        private static Dictionary<E_CurrentClosingCostSourceT, Dictionary<E_LastDisclosedClosingCostSourceT, List<String>>> x_futureMigrationActions =
            new Dictionary<E_CurrentClosingCostSourceT, Dictionary<E_LastDisclosedClosingCostSourceT, List<string>>>()
        {
            { 
                E_CurrentClosingCostSourceT.GFE, new Dictionary<E_LastDisclosedClosingCostSourceT, List<string>>()
                {
                    { 
                        E_LastDisclosedClosingCostSourceT.GFEArchive, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages will use the closing costs that are currently on the GFE page",
                            "The closing costs on the Settlement Charges page will not be used"
                        }
                    },
                    {
                        E_LastDisclosedClosingCostSourceT.GFE, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages will use the closing costs that are currently on the GFE page",
                            "GFE data will be saved to the GFE Archive",
                            "The closing costs on the Settlement Charges page will not be used"
                        }
                    },
                    {
                        E_LastDisclosedClosingCostSourceT.None, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages will use the closing costs that are currently on the GFE page",
                            "The closing costs on the Settlement Charges page will not be used"
                        }
                    }
                }
            },
            {
                E_CurrentClosingCostSourceT.SettlementCharges, new Dictionary<E_LastDisclosedClosingCostSourceT, List<string>>()
                {
                    { 
                        E_LastDisclosedClosingCostSourceT.GFEArchive, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages will use the closing costs that are currently on the Settlement Charges page",
                            "The closing costs on the GFE page will not be used"
                        }
                    },
                    {
                        E_LastDisclosedClosingCostSourceT.GFE, new List<string>()
                        {
                            "GFE data will be saved to the GFE Archive",
                            "Both the GFE and Settlement Charges pages will use the closing costs that are currently on the Settlement Charges page"
                        }
                    },
                    {
                        E_LastDisclosedClosingCostSourceT.None, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages will use the closing costs that are currently on the Settlement Charges page",
                            "The closing costs on the GFE page will not be used"
                        }
                    }
                }
            }
        };

        private static Dictionary<string, Dictionary<string, List<string>>> x_serializableFutureMigrationActions = x_futureMigrationActions.ToDictionary(
            (kvp) => kvp.Key.ToString("D"),
            (kvp) => kvp.Value.ToDictionary(
                (kvp2) => kvp2.Key.ToString("D"),
                (kvp2) => kvp2.Value));

        #endregion

        #region Past Migration Actions
        private static Dictionary<E_CurrentClosingCostSourceT, Dictionary<E_LastDisclosedClosingCostSourceT, List<String>>> x_pastMigrationActions =
            new Dictionary<E_CurrentClosingCostSourceT, Dictionary<E_LastDisclosedClosingCostSourceT, List<string>>>()
        {
            { 
                E_CurrentClosingCostSourceT.GFE, new Dictionary<E_LastDisclosedClosingCostSourceT, List<string>>()
                {
                    { 
                        E_LastDisclosedClosingCostSourceT.GFEArchive, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages used the closing costs that were on the GFE page [view migrated GFE data]",
                            "The closing costs on the Settlement Charges page were not used [view discarded SC data]"
                        }
                    },
                    {
                        E_LastDisclosedClosingCostSourceT.GFE, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages used the closing costs that were on the GFE page [view migrated GFE data]",
                            "GFE data was saved to the GFE Archive",
                            "The closing costs on the Settlement Charges page were not used [view discarded SC data]"
                        }
                    },
                    {
                        E_LastDisclosedClosingCostSourceT.None, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages used the closing costs that were on the GFE page [view migrated GFE data]",
                            "The closing costs on the Settlement Charges page were not used [view discarded SC data]"
                        }
                    }
                }
            },
            {
                E_CurrentClosingCostSourceT.SettlementCharges, new Dictionary<E_LastDisclosedClosingCostSourceT, List<string>>()
                {
                    { 
                        E_LastDisclosedClosingCostSourceT.GFEArchive, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages used the closing costs that were on the Settlement Charges page [view migrated SC data]",
                            "The closing costs on the GFE page were not used [view discarded GFE data]",
                        }
                    },
                    {
                        E_LastDisclosedClosingCostSourceT.GFE, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages used the closing costs that were on the Settlement Charges page [view migrated SC data]",
                            "GFE data was saved to the GFE Archive [view migrated GFE data]"
                        }
                    },
                    {
                        E_LastDisclosedClosingCostSourceT.None, new List<string>()
                        {
                            "Both the GFE and Settlement Charges pages used the closing costs that were on the Settlement Charges page [view migrated SC data]",
                            "The closing costs on the GFE page were not used [view discarded GFE data]"
                        }
                    }
                }
            }
        };

        private static Dictionary<string, Dictionary<string, List<string>>> x_serializablePastMigrationActions = x_pastMigrationActions.ToDictionary(
            (kvp) => kvp.Key.ToString("D"),
            (kvp) => kvp.Value.ToDictionary(
                (kvp2) => kvp2.Key.ToString("D"),
                (kvp2) => kvp2.Value));
        #endregion
    }
}