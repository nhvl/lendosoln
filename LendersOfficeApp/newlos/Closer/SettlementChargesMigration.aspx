﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SettlementChargesMigration.aspx.cs" Inherits="LendersOfficeApp.newlos.Closer.SettlementChargesMigration" %>
<%@ Import Namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Settlement Charges Migration</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        div { padding-top: 5px; padding-left: 5px; }
        input[type="button"] { margin-left: 5px; }
        ul { margin-top: 5px; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="MainRightHeader">Settlement Charges Migration</div>
    <div>
        Current closing cost structure : <span runat="server" id="sCurrentClosingCostStructureLabel"></span>
    </div>
    
    <div id="MigrationOptions">
        Select migration options:
        <div>
            Which page contains the most current closing cost information for this file?
            <div id="CurrentClosingCosts">
                <asp:RadioButtonList runat="server" ID="m_currentClosingCosts" RepeatLayout="Flow" RepeatDirection="Vertical"></asp:RadioButtonList>
            </div>
        </div>
        <div>
            Which page contains the last disclosed closing costs?
            <div id="LastDisclosedClosingCosts">
                <asp:RadioButtonList runat="server" ID="m_lastDisclosedClosingCosts" RepeatLayout="Flow" RepeatDirection="Vertical"></asp:RadioButtonList>
            </div>
        </div>
    </div>
    
    <div id="MigrationDesc"></div>
    <ul id="MigrationActions">
    </ul>
    
    <input type="button" id="Migrate" value="Migrate" NotForEdit="true" />
    <input type="button" id="UndoMigration" value="Undo migration" NotForEdit="true" />
    
    <asp:HiddenField runat="server" ID="sUseGFEDataForSCFields" />
    <asp:HiddenField runat="server" ID="sCurrentClosingCostStructure" />
    <asp:HiddenField runat="server" ID="sClosingCostMigrationCurrentClosingCostSource" />
    <asp:HiddenField runat="server" ID="sClosingCostMigrationLastDisclosedClosingCostSource" />
    <asp:HiddenField runat="server" ID="m_futureMigrationActionsHF" />
    <asp:HiddenField runat="server" ID="m_pastMigrationActionsHF" />
    </form>
    <script type="text/javascript">
        $(document).ready(function() {
            var futureMigrationActions = $.parseJSON($('#m_futureMigrationActionsHF').val());
            var pastMigrationActions = $.parseJSON($('#m_pastMigrationActionsHF').val());
            var E_LastDisclosedClosingCostSourceT_GFEArchive = <%= AspxTools.JsString(E_LastDisclosedClosingCostSourceT.GFEArchive) %>;
            var E_LastDisclosedClosingCostSourceT_GFE = <%= AspxTools.JsString(E_LastDisclosedClosingCostSourceT.GFE) %>;

            function getMigrationActions(currentClosingCosts, lastDisclosedClosingCosts) {
                var usePastMigratonActions = $('#sUseGFEDataForSCFields').val() === 'True';
                var migrationActions = usePastMigratonActions ? pastMigrationActions : futureMigrationActions;

                return migrationActions[currentClosingCosts][lastDisclosedClosingCosts];
            }

            function updateMigrationActions() {
                var fileHasBeenMigrated = $('#sUseGFEDataForSCFields').val() === 'True';
                var currentClosingCosts = fileHasBeenMigrated ? $('#sClosingCostMigrationCurrentClosingCostSource').val() : $('#CurrentClosingCosts input[type="radio"]:checked').val();
                var lastDisclosedClosingCosts = fileHasBeenMigrated ? $('#sClosingCostMigrationLastDisclosedClosingCostSource').val() : $('#LastDisclosedClosingCosts input[type="radio"]:checked').val();
                var $migrationActions = $('#MigrationActions');
                var migrationActionsArray = getMigrationActions(currentClosingCosts, lastDisclosedClosingCosts);
                var migrationAction;
                var regex = /\[view (migrated|discarded) (GFE|SC) data\]/;
                var i;

                $migrationActions.empty();
                for (i = 0; i < migrationActionsArray.length; i++) {
                    migrationAction = migrationActionsArray[i].replace(regex, '<a href="#" class="Action View$2 $1">view $1 $2 data</a>');
                    $migrationActions.append('<li>' + migrationAction + '</li>');
                }
            }

            function initializeUI() {
                var fileHasBeenMigrated = $('#sUseGFEDataForSCFields').val() === 'True';
                var lastDisclosedClosingCostSource = $('#sClosingCostMigrationLastDisclosedClosingCostSource').val();

                $('#sCurrentClosingCostStructureLabel').text($('#sCurrentClosingCostStructure').val());
                
                $('#CurrentClosingCosts input:radio[value="' + $('#sClosingCostMigrationCurrentClosingCostSource').val() + '"]').prop('checked', true);
                
                $('#LastDisclosedClosingCosts label, #LastDisclosedClosingCosts input:radio').prop('disabled', false);
                if (lastDisclosedClosingCostSource === E_LastDisclosedClosingCostSourceT_GFEArchive) {
                    $('#LastDisclosedClosingCosts input:radio[value="' + E_LastDisclosedClosingCostSourceT_GFEArchive + '"]').prop('checked', true)
                    $('#LastDisclosedClosingCosts input:radio[value!="' + E_LastDisclosedClosingCostSourceT_GFEArchive + '"]').prop('disabled', true)
                        .next('label')
                        .prop('disabled', true);
                }
                else {
                    $('#LastDisclosedClosingCosts input:radio[value="' + E_LastDisclosedClosingCostSourceT_GFE + '"]').prop('checked', true);
                    $('#LastDisclosedClosingCosts input:radio[value="' + E_LastDisclosedClosingCostSourceT_GFEArchive + '"]').prop('disabled', true)
                        .next('label')
                        .prop('disabled', true);
                }
                
                $('#MigrationDesc').text(fileHasBeenMigrated ? 'Migration that was run:' : 'Migration actions:');
                $('#MigrationOptions').toggle(!fileHasBeenMigrated);
                $('#Migrate').toggle(!fileHasBeenMigrated);
                $('#UndoMigration').toggle(fileHasBeenMigrated);

                updateMigrationActions();
            }
            
            function refreshNavigation() {
                if (parent.treeview) {
                    parent.treeview.location.reload();
                    window.setTimeout(function() {
                        if (parent.treeview && typeof parent.treeview.selectPageID === 'function') {
                            parent.treeview.selectPageID(<%= AspxTools.JsString(this.PageID) %>);
                        }
                    }, 500);
                }
            }

            function migrate() {
                var args = {
                    loanid: ML.sLId,
                    sFileVersion: $('#sFileVersion').val(),
                    CurrentClosingCostSource: $('#CurrentClosingCosts input[type="radio"]:checked').val(),
                    LastDisclosedClosingCostSource: $('#LastDisclosedClosingCosts input[type="radio"]:checked').val()
                };
                var bRefreshNavigation = args.LastDisclosedClosingCostSource === E_LastDisclosedClosingCostSourceT_GFE;
                var result = gService.loanedit.call('Migrate', args);

                if (!result.error) {
                    populateForm(result.value, null);
                    initializeUI();
                    if (bRefreshNavigation) {
                        refreshNavigation();
                    }
                }
                else {
                    var errMsg = 'Unable to migrate closing cost data. Please try again.';
                    if (typeof result.UserMessage === 'string') {
                        errMsg = result.UserMessage;
                    }
                    alert(errMsg);
                }
            }

            function undoMigration() {
                var confirmMsg = 'Undoing the closing cost migration will permanently discard any changes ' +
                    'that have been made to closing costs since the migration was run. This action cannot ' +
                    'be undone.';
                var args;
                if (confirm(confirmMsg)) {
                    args = {
                        loanid: ML.sLId,
                        sFileVersion: $('#sFileVersion').val()
                    };
                    var bRefreshNavigation = $('#sClosingCostMigrationLastDisclosedClosingCostSource').val() === E_LastDisclosedClosingCostSourceT_GFE;
                    var result = gService.loanedit.call('UndoMigration', args);

                    if (!result.error) {
                        populateForm(result.value, null);
                        initializeUI();
                        if (bRefreshNavigation) {
                            refreshNavigation();
                        }
                    }
                    else {
                        var errMsg = 'Unable to undo closing cost migration. Please try again.';
                        if (typeof result.UserMessage === 'string') {
                            errMsg = result.UserMessage;
                        }
                        alert(errMsg);
                    }
                }
            }

            $('input[type="radio"]').on('click', updateMigrationActions)
                .each(function() {
                    removeEventHandler(this, 'onclick', updateDirtyBit);
                });
            $('#Migrate').on('click', migrate);
            $('#UndoMigration').on('click', undoMigration);
            $('#MigrationActions').on('click', 'a.ViewGFE', function() {
                var migratedOrDiscarded = $(this).hasClass('migrated') ? 'Migrated' : 'Discarded';
                if (parent && parent.treeview && typeof parent.treeview.load === 'function') {
                    return parent.treeview.load('Forms/GoodFaithEstimate2010.aspx?IsClosingCostMigrationArchivePage=true&MigratedOrDiscarded=' + migratedOrDiscarded);
                }
            });
            $('#MigrationActions').on('click', 'a.ViewSC', function() {
                var migratedOrDiscarded = $(this).hasClass('migrated') ? 'Migrated' : 'Discarded';
                if (parent && parent.treeview && typeof parent.treeview.load === 'function') {
                    return parent.treeview.load('Closer/SettlementCharges.aspx?IsClosingCostMigrationArchivePage=true&MigratedOrDiscarded=' + migratedOrDiscarded);
                }
            });

            initializeUI();
        });
    </script>
</body>
</html>
