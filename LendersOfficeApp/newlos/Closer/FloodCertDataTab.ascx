﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FloodCertDataTab.ascx.cs" Inherits="LendersOfficeApp.newlos.Closer.FloodCertDataTab" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<script type="text/javascript">
    function _init()
    {
        toggleProgramCheckboxes();
        toggleDesignationDTextBox();
        toggleUpgradeDTextBox();
        toggleCustomPayeeTextBox();
    }

    function toggleProgramCheckboxes()
    {
        var availButtonChecked = <%= AspxTools.JsGetElementById(sIsAvailable_0) %>.checked;
        var cbR = <%= AspxTools.JsGetElementById(sRegularProgram)%>;
        var cbE = <%= AspxTools.JsGetElementById(sEmergencyProgramOfNFIP)%>;
        var cbP = <%= AspxTools.JsGetElementById(sProbation)%>;
        var cbS = <%= AspxTools.JsGetElementById(sSuspended)%>;
        
        cbR.disabled = !availButtonChecked;
        cbE.disabled = !availButtonChecked;
        cbP.disabled = !availButtonChecked;
        cbS.disabled = availButtonChecked;
        <%= AspxTools.JsGetElementById(sFfiaDateCommunityEnteredProgramD)%>.readOnly = !availButtonChecked;
        
        if(!availButtonChecked)
        {
            cbR.checked = false;
            cbE.checked = false;
            cbP.checked = false;
            <%= AspxTools.JsGetElementById(sFfiaDateCommunityEnteredProgramD)%>.value = "";
        } else {
            cbR.checked = !cbE.checked && !cbP.checked;
            cbS.checked = false;
        }
    }
    function toggleDesignationDTextBox()
    {
        var designatedCBRA_OPA = <%= AspxTools.JsGetElementById(sFloodCertificationIsCBRAorOPA) %>.checked;
        <%= AspxTools.JsGetElementById(sFloodCertificationDesignationD)%>.readOnly = 
            !designatedCBRA_OPA;
        if(!designatedCBRA_OPA)
        {
            <%= AspxTools.JsGetElementById(sFloodCertificationDesignationD)%>.value = "";
        }
        
    }
    function toggleUpgradeDTextBox()
    {
        var upgradedButtonChecked = <%= AspxTools.JsGetElementById(sFloodCertificationIsLOLUpgraded) %>.getElementsByTagName("input")[0].checked;
        <%= AspxTools.JsGetElementById(sFloodCertificationLOLUpgradeD) %>.disabled = !upgradedButtonChecked;
        if(!upgradedButtonChecked)
        {
            <%= AspxTools.JsGetElementById(sFloodCertificationLOLUpgradeD) %>.value = "";
        }
    }
    function toggleCustomPayeeTextBox()
    {
        if(<%= AspxTools.JsGetElementById(sFloodCertificationPreparerT) %>.value === "0")
        {
            <%= AspxTools.JsGetElementById(sFloodCertificationPreparerOtherName) %>.style.visibility = "visible";
        }
        else
        {
            <%= AspxTools.JsGetElementById(sFloodCertificationPreparerOtherName) %>.style.visibility = "hidden";
        }
    }
    function excludeOtherNFIPProgram(checkbox)
    {
        if(checkbox.checked)
        {
            var cbE = <%= AspxTools.JsGetElementById(sEmergencyProgramOfNFIP) %>;
            var cbR = <%= AspxTools.JsGetElementById(sRegularProgram) %>;
            var cbS = <%= AspxTools.JsGetElementById(sSuspended) %>;
            var cbP = <%= AspxTools.JsGetElementById(sProbation) %>;
            cbE.checked = cbE == checkbox;
            cbR.checked = cbR == checkbox;
            cbS.checked = cbS == checkbox;
            cbP.checked = cbP == checkbox;
        } else {
            checkbox.checked = true;
        }
    }

    function doAfterDateFormat(e)
    {
        if(e.id.indexOf("sFloodCertificationDeterminationD") !== -1)
        {
            refreshCalculation();
        }
    }
</script>
<div id="FloodCertificationDataTab">
	<table cellSpacing="0" cellPadding="0" border="0">
		<tr>
			<td class="MainRightHeader" noWrap>
			    Flood Certification Data
			</td>
		</tr>
		
		<tr>
		    <td width="99%">
		        <table class="InsetBorder" cellSpacing="0" cellPadding="0" width="99%" border="0">
                <tr>
                    <td colspan="4" class="FormTableSubheader">
                        National Flood Insurance Program (NFIP) Community Jurisdiction
                    </td>
                </tr>
                <tr class="FieldLabel">                        
                    <td>
                        NFIP Community Name
                    </td>
                    <td>
                        County
                    </td>
                    <td>
                        State
                    </td>
                    <td>
                        NFIP Community Number
                    </td>
                </tr>
                <tr>                        
                    <td>
                        <asp:TextBox ID="sFloodHazardCommunityDesc" Width="200" runat="server" MaxLength="50"></asp:TextBox>
                    </td>
                    <td>                            
                        <ml:EncodedLiteral ID="sSpCounty" runat="server" ></ml:EncodedLiteral>
                    </td>
                    <td>
                        <ml:EncodedLiteral ID="sSpState" runat="server" ></ml:EncodedLiteral>
                    </td>
                    <td>
                        <asp:TextBox ID="sFloodCertificationCommunityNum" Width="100" runat="server" MaxLength="6" ></asp:TextBox>
                    </td>
                </tr>
                
            </table>
            
            <table class="InsetBorder" cellSpacing="0" cellPadding="0" width="99%" border="0">
                <tr>
                    <td colspan="13" class="FormTableSubheader">
                        National Flood Insurance Program (NFIP) Data Affecting Building/Mobile Home
                    </td>
                </tr>
                <tr class="FieldLabel" >                        
                    <td> NFIP Map Number | Panel Number | Panel Suffix</td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        NFIP Map Panel<br>Effective/Revised Date
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        LOMA/LOMR
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        LOMA/LOMR Date
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        Flood<br>Zone
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        No NFIP<br>Map
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        Partial<br>Zone
                    </td>
                </tr>
                <tr >                        
                    <td>
                        <asp:TextBox ID="sFloodCertificationMapNum" Width="100" runat="server" MaxLength="6" ></asp:TextBox>
                        <asp:TextBox ID="sFloodCertificationPanelNums" Width="70" runat="server" MaxLength="5"></asp:TextBox>
                        <asp:TextBox ID="sFloodCertificationPanelSuffix" Width="40" runat="server" MaxLength="2"></asp:TextBox>
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        <ml:DateTextBox ID="sFloodCertificationMapD" runat="server" preset="date"></ml:DateTextBox>
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        <asp:DropDownList ID="sLomaLomrT" runat="server"></asp:DropDownList>
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        <ml:DateTextBox ID="sFloodCertificationLOMChangedD" runat="server" preset="date"></ml:DateTextBox>
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        <asp:TextBox ID="sNfipFloodZoneId" Width="50" runat="server" MaxLength="50"></asp:TextBox>
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        <asp:CheckBox ID="sFloodCertificationIsNoMap" Width="10" runat="server" ></asp:CheckBox>
                    </td>
                    <td class="ColumnSpacing"></td>
                    <td>
                        <asp:CheckBox ID="sNfipIsPartialZone" Width="10" runat="server" ></asp:CheckBox>                            
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel" colspan="2">
                        Mapping Company
                        <asp:TextBox ID="sNfipMappingCompany" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
            
            <table class="InsetBorder" cellSpacing="0" cellPadding="0" width="99%" border="0">
                <tr>
                    <td colspan="2" class="FormTableSubheader">
                        Federal Flood Insurance Availability
                    </td>
                </tr>
                <tr> 
                    <td colspan="2"> 
                        <table>
                            <tr>
                                <td colspan="2">
                                    <input type="radio" runat='server' name="sIsAvailable" value="True" ID='sIsAvailable_0' onclick="toggleProgramCheckboxes();" /> <label class="FieldLabel">Available (community participates in the NFIP)</label>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap class="FieldLabel" style="padding-left:15px">
                                    <asp:CheckBox ID="sRegularProgram" Text="Regular Program" runat="server" value="R" onclick="excludeOtherNFIPProgram(this);"></asp:CheckBox>
                                    <asp:CheckBox ID="sEmergencyProgramOfNFIP" Text="Emergency Program of NFIP" runat="server" value="E" onclick="excludeOtherNFIPProgram(this);"></asp:CheckBox>
                                    <asp:CheckBox ID="sProbation" Text="Probation" runat="server" value="P" onclick="excludeOtherNFIPProgram(this);"></asp:CheckBox>
                                </td>
                                <td class="FieldLabel" style="padding-left:30px">
                                    Date Community Entered Program
                                    <ml:DateTextBox ID="sFfiaDateCommunityEnteredProgramD" runat="server" preset="date"></ml:DateTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type='radio' runat='server' name="sIsAvailable" value="False" ID='sIsAvailable_1' onclick="toggleProgramCheckboxes();" /> <label class="FieldLabel">Not available (community does not participate)</label>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap class="FieldLabel" style="padding-left:15px">
                                    <asp:CheckBox ID="sSuspended" Text="Suspended" runat="server" value="S"></asp:CheckBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>                        
                    <td>
                        <div style="width:450px; white-space:normal;">
                            <asp:CheckBox ID="sFloodCertificationIsCBRAorOPA" runat="server" onclick="toggleDesignationDTextBox();"></asp:CheckBox> 
                            May not be available. Building/Mobile Home is in a Coastal Barrier Resources Area (CBRA) or Otherwise Protected Area (OPA). 
                        </div>                            
                    </td>
                    <td nowrap>
                        <label class="FieldLabel">CBRA/OPA Designation Date</label>
                        <ml:DateTextBox ID="sFloodCertificationDesignationD" runat="server" preset="date"></ml:DateTextBox>
                    </td>
                </tr>
                
            </table>
            
            <table class="InsetBorder" cellSpacing="0" cellPadding="0" width="99%" border="0">
                <tr>
                    <td colspan="2" class="FormTableSubheader">
                        Determination
                    </td>
                </tr>
                <tr>                                                
                    <td>
                        <label class="FieldLabel" style="float:left;margin-top:5px;">Building is in Special Flood Hazard Area (Zones containing letters "A" or "V")</label>
                        <asp:RadioButtonList runat="server" ID="sFloodCertificationIsInSpecialArea" RepeatDirection="Horizontal" style="float:left">
                            <asp:ListItem Value="True">Yes</asp:ListItem>
                            <asp:ListItem Value="False">No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>                                 
            </table>
            
            <table class="InsetBorder FieldLabel" cellSpacing="0" cellPadding="0" width="99%" border="0">
                <tr>
                    <td colspan="4" class="FormTableSubheader">
                        Preparer's Information
                    </td>
                </tr>
                <tr>                                                
                    <td>
                        Name
                    </td>
                    <td colspan="2">
                        Date of Determination
                    </td>
                    <td>
                        Flood Cert Number
                    </td>
                </tr>                                 
                <tr>
                    <td rowspan="2">
                       <asp:DropDownList onchange="toggleCustomPayeeTextBox();" ID="sFloodCertificationPreparerT" width="15em" style="vertical-align:top;" runat="server">
                       </asp:DropDownList>
                       <br />
                       <asp:TextBox runat='server' ID="sFloodCertificationPreparerOtherName" MaxLength="50"></asp:TextBox>
                    </td>
                    <td colspan="2">
                        <ml:DateTextBox ID="sFloodCertificationDeterminationD" runat="server" preset="date" onchange="date_onblur(null, this);" ></ml:DateTextBox>
                        &nbsp;
                        <asp:TextBox ID="sHourOfDetermination" Width="30" runat="server" MaxLength="2" ></asp:TextBox>:
                        <asp:TextBox ID="sMinuteOfDetermination" Width="30" runat="server" MaxLength="2" ></asp:TextBox>
                        <asp:DropDownList ID="sAmPm" runat="server">
                            <asp:ListItem Value="AM">AM</asp:ListItem>
                            <asp:ListItem Value="PM">PM</asp:ListItem>
                        </asp:DropDownList>                           
                        &nbsp;
                        <asp:DropDownList ID="sFloodCertificationDeterminationDTimeZoneT" Width="100" runat="server" ></asp:DropDownList>                                                           
                    </td>
                    <td>
                        <asp:TextBox ID="sFloodCertId" Width="100" runat="server" MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td> Life Of Loan Upgraded</td>
                    <td> LOL Upgrade Date</td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:RadioButtonList ID="sFloodCertificationIsLOLUpgraded" runat='server' RepeatDirection="Horizontal">
                            <asp:ListItem Value="True" onclick="toggleUpgradeDTextBox();">Yes</asp:ListItem>
                            <asp:ListItem Value="False" onclick="toggleUpgradeDTextBox();">No</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td> 
                        <ml:DateTextBox ID="sFloodCertificationLOLUpgradeD" runat="server" preset="date"></ml:DateTextBox>                         
                    </td>
                    <td></td>
                </tr>
            </table>
           
            <table class="InsetBorder FieldLabel" cellSpacing="0" cellPadding="0" width="99%" border="0">
                <tr>
                    <td colspan="2" class="FormTableSubheader">
                        Cancellation
                    </td>
                </tr>
                <tr>                                                
                    <td>
                        Cancellation Ordered Date
                    </td>
                    <td>
                        Cancellation Confirmation Received Date
                    </td>
                </tr>                                 
                <tr>                                                
                    <td>
                       <ml:DateTextBox ID="sFloodCertificationCancellationOrderedD" runat="server" preset="date"></ml:DateTextBox>
                    </td>
                    <td>
                       <ml:DateTextBox ID="sFloodCertificationCancellationReceivedD" runat="server" preset="date"></ml:DateTextBox>
                    </td>
                </tr> 
            </table> 
		    </td>
		</tr>			
	</table>
</div>