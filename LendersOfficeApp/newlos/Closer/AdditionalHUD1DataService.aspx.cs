using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class AdditionalHUD1DataService : LendersOffice.Common.BaseSimpleServiceXmlPage 
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new AddlHUD1ServiceItem());
        }
    }

    public class AddlHUD1ServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            // 100
            dataLoan.sGrossDueFromBorrPersonalProperty_rep = GetString("sGrossDueFromBorrPersonalProperty");
            dataLoan.sGrossDueFromBorrU1FDesc = GetString("sGrossDueFromBorrU1FDesc");
            dataLoan.sGrossDueFromBorrU1F_rep = GetString("sGrossDueFromBorrU1F");
            dataLoan.sGrossDueFromBorrU2FDesc = GetString("sGrossDueFromBorrU2FDesc");
            dataLoan.sGrossDueFromBorrU2F_rep = GetString("sGrossDueFromBorrU2F");
            // 400
            dataLoan.sGrossDueToSellerU1FDesc = GetString("sGrossDueToSellerU1FDesc");
            dataLoan.sGrossDueToSellerU1F_rep = GetString("sGrossDueToSellerU1F");
            dataLoan.sGrossDueToSellerU2FDesc = GetString("sGrossDueToSellerU2FDesc");
            dataLoan.sGrossDueToSellerU2F_rep = GetString("sGrossDueToSellerU2F");
            dataLoan.sGrossDueToSellerU2FSameAsBorr = GetBool("sGrossDueToSellerU2FSameAsBorr");
            dataLoan.sGrossDueToSellerU3FDesc = GetString("sGrossDueToSellerU3FDesc");
            dataLoan.sGrossDueToSellerU3F_rep = GetString("sGrossDueToSellerU3F");
            dataLoan.sGrossDueToSellerU3FSameAsBorr = GetBool("sGrossDueToSellerU3FSameAsBorr");
            // 100-400
            dataLoan.sPdBySellerCityTaxFStartD_rep = GetString("sPdBySellerCityTaxFStartD");
            dataLoan.sPdBySellerCityTaxFEndD_rep = GetString("sPdBySellerCityTaxFEndD");
            dataLoan.sPdBySellerCityTaxF_rep = GetString("sPdBySellerCityTaxF");
            dataLoan.sPdBySellerCountyTaxFStartD_rep = GetString("sPdBySellerCountyTaxFStartD");
            dataLoan.sPdBySellerCountyTaxFEndD_rep = GetString("sPdBySellerCountyTaxFEndD");
            dataLoan.sPdBySellerCountyTaxF_rep = GetString("sPdBySellerCountyTaxF");
            dataLoan.sPdBySellerAssessmentsTaxFStartD_rep = GetString("sPdBySellerAssessmentsTaxFStartD");
            dataLoan.sPdBySellerAssessmentsTaxFEndD_rep = GetString("sPdBySellerAssessmentsTaxFEndD");
            dataLoan.sPdBySellerAssessmentsTaxF_rep = GetString("sPdBySellerAssessmentsTaxF");
            dataLoan.sPdBySellerU1FDesc = GetString("sPdBySellerU1FDesc");
            dataLoan.sPdBySellerU1FStartD_rep = GetString("sPdBySellerU1FStartD");
            dataLoan.sPdBySellerU1FEndD_rep = GetString("sPdBySellerU1FEndD");
            dataLoan.sPdBySellerU1F_rep = GetString("sPdBySellerU1F");
            dataLoan.sPdBySellerU2FDesc = GetString("sPdBySellerU2FDesc");
            dataLoan.sPdBySellerU2FStartD_rep = GetString("sPdBySellerU2FStartD");
            dataLoan.sPdBySellerU2FEndD_rep = GetString("sPdBySellerU2FEndD");
            dataLoan.sPdBySellerU2F_rep = GetString("sPdBySellerU2F");
            dataLoan.sPdBySellerU3FDesc = GetString("sPdBySellerU3FDesc");
            dataLoan.sPdBySellerU3FStartD_rep = GetString("sPdBySellerU3FStartD");
            dataLoan.sPdBySellerU3FEndD_rep = GetString("sPdBySellerU3FEndD");
            dataLoan.sPdBySellerU3F_rep = GetString("sPdBySellerU3F");
            dataLoan.sPdBySellerU4FDesc = GetString("sPdBySellerU4FDesc");
            dataLoan.sPdBySellerU4FStartD_rep = GetString("sPdBySellerU4FStartD");
            dataLoan.sPdBySellerU4FEndD_rep = GetString("sPdBySellerU4FEndD");
            dataLoan.sPdBySellerU4F_rep = GetString("sPdBySellerU4F");
            // 200
            dataLoan.sPdByBorrowerU1FHudline_rep = GetString("sPdByBorrowerU1FHudline");
            dataLoan.sPdByBorrowerU1FDesc = GetString("sPdByBorrowerU1FDesc");
            dataLoan.sPdByBorrowerU1F_rep = GetString("sPdByBorrowerU1F");
            dataLoan.sPdByBorrowerU2FHudline_rep = GetString("sPdByBorrowerU2FHudline");
            dataLoan.sPdByBorrowerU2FDesc = GetString("sPdByBorrowerU2FDesc");
            dataLoan.sPdByBorrowerU2F_rep = GetString("sPdByBorrowerU2F");
            dataLoan.sPdByBorrowerU3FHudline_rep = GetString("sPdByBorrowerU3FHudline");
            dataLoan.sPdByBorrowerU3FDesc = GetString("sPdByBorrowerU3FDesc");
            dataLoan.sPdByBorrowerU3F_rep = GetString("sPdByBorrowerU3F");
            dataLoan.sPdByBorrowerU4FHudline_rep = GetString("sPdByBorrowerU4FHudline");
            dataLoan.sPdByBorrowerU4FDesc = GetString("sPdByBorrowerU4FDesc");
            dataLoan.sPdByBorrowerU4F_rep = GetString("sPdByBorrowerU4F");
            dataLoan.sPdByBorrowerU4FRowLckd = GetBool("sPdByBorrowerU4FRowLckd");

            if (dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled && false == dataLoan.sIsLineOfCredit)
            {
                dataLoan.sPdByBorrowerU5FRowLckd = GetBool("sPdByBorrowerU5FRowLckd");
                if (dataLoan.sPdByBorrowerU5FRowLckd)
                {
                    dataLoan.sPdByBorrowerU5FHudline_rep = GetString("sPdByBorrowerU5FHudline");
                    dataLoan.sPdByBorrowerU5FDesc = GetString("sPdByBorrowerU5FDesc");
                    dataLoan.sPdByBorrowerU5F_rep = GetString("sPdByBorrowerU5F");
                }
                dataLoan.sPdByBorrowerU6FRowLckd = GetBool("sPdByBorrowerU6FRowLckd");
                if (dataLoan.sPdByBorrowerU6FRowLckd)
                {
                    dataLoan.sPdByBorrowerU6FHudline_rep = GetString("sPdByBorrowerU6FHudline");
                    dataLoan.sPdByBorrowerU6FDesc = GetString("sPdByBorrowerU6FDesc");
                    dataLoan.sPdByBorrowerU6F_rep = GetString("sPdByBorrowerU6F");
                }
            }
            else
            {
                dataLoan.sPdByBorrowerU5FHudline_rep = GetString("sPdByBorrowerU5FHudline");
                dataLoan.sPdByBorrowerU5FDesc = GetString("sPdByBorrowerU5FDesc");
                dataLoan.sPdByBorrowerU5F_rep = GetString("sPdByBorrowerU5F");
                dataLoan.sPdByBorrowerU6FHudline_rep = GetString("sPdByBorrowerU6FHudline");
                dataLoan.sPdByBorrowerU6FDesc = GetString("sPdByBorrowerU6FDesc");
                dataLoan.sPdByBorrowerU6F_rep = GetString("sPdByBorrowerU6F");
            }
            // 500
            dataLoan.sReductionsDueToSellerExcessDeposit_rep = GetString("sReductionsDueToSellerExcessDeposit");
            dataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep = GetString("sReductionsDueToSellerPayoffOf1stMtgLoan");
            dataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep = GetString("sReductionsDueToSellerPayoffOf2ndMtgLoan");
            dataLoan.sReductionsDueToSellerU1FHudline_rep = GetString("sReductionsDueToSellerU1FHudline");
            dataLoan.sReductionsDueToSellerU1FDesc = GetString("sReductionsDueToSellerU1FDesc");
            dataLoan.sReductionsDueToSellerU1F_rep = GetString("sReductionsDueToSellerU1F");
            dataLoan.sReductionsDueToSellerU1SameAsBorr = GetBool("sReductionsDueToSellerU1SameAsBorr");
            dataLoan.sReductionsDueToSellerU2FHudline_rep = GetString("sReductionsDueToSellerU2FHudline");
            dataLoan.sReductionsDueToSellerU2FDesc = GetString("sReductionsDueToSellerU2FDesc");
            dataLoan.sReductionsDueToSellerU2F_rep = GetString("sReductionsDueToSellerU2F");
            dataLoan.sReductionsDueToSellerU2SameAsBorr = GetBool("sReductionsDueToSellerU2SameAsBorr");
            dataLoan.sReductionsDueToSellerU3FHudline_rep = GetString("sReductionsDueToSellerU3FHudline");
            dataLoan.sReductionsDueToSellerU3FDesc = GetString("sReductionsDueToSellerU3FDesc");
            dataLoan.sReductionsDueToSellerU3F_rep = GetString("sReductionsDueToSellerU3F");
            dataLoan.sReductionsDueToSellerU3SameAsBorr = GetBool("sReductionsDueToSellerU3SameAsBorr");
            dataLoan.sReductionsDueToSellerU4FHudline_rep = GetString("sReductionsDueToSellerU4FHudline");
            dataLoan.sReductionsDueToSellerU4FDesc = GetString("sReductionsDueToSellerU4FDesc");
            dataLoan.sReductionsDueToSellerU4F_rep = GetString("sReductionsDueToSellerU4F");
            dataLoan.sReductionsDueToSellerU4SameAsBorr = GetBool("sReductionsDueToSellerU4SameAsBorr");
            dataLoan.sReductionsDueToSellerU5FHudline_rep = GetString("sReductionsDueToSellerU5FHudline");
            dataLoan.sReductionsDueToSellerU5FDesc = GetString("sReductionsDueToSellerU5FDesc");
            dataLoan.sReductionsDueToSellerU5F_rep = GetString("sReductionsDueToSellerU5F");
            dataLoan.sReductionsDueToSellerU5SameAsBorr = GetBool("sReductionsDueToSellerU5SameAsBorr");
            dataLoan.sReductionsDueToSellerU6FHudline_rep = GetString("sReductionsDueToSellerU6FHudline");
            dataLoan.sReductionsDueToSellerU6FDesc = GetString("sReductionsDueToSellerU6FDesc");
            dataLoan.sReductionsDueToSellerU6F_rep = GetString("sReductionsDueToSellerU6F");
            dataLoan.sReductionsDueToSellerU6SameAsBorr = GetBool("sReductionsDueToSellerU6SameAsBorr");
            // 200-500
            dataLoan.sUnPdBySellerCityTaxFStartD_rep = GetString("sUnPdBySellerCityTaxFStartD");
            dataLoan.sUnPdBySellerCityTaxFEndD_rep = GetString("sUnPdBySellerCityTaxFEndD");
            dataLoan.sUnPdBySellerCityTaxF_rep = GetString("sUnPdBySellerCityTaxF");
            dataLoan.sUnPdBySellerCountyTaxFStartD_rep = GetString("sUnPdBySellerCountyTaxFStartD");
            dataLoan.sUnPdBySellerCountyTaxFEndD_rep = GetString("sUnPdBySellerCountyTaxFEndD");
            dataLoan.sUnPdBySellerCountyTaxF_rep = GetString("sUnPdBySellerCountyTaxF");
            dataLoan.sUnPdBySellerAssessmentsTaxFStartD_rep = GetString("sUnPdBySellerAssessmentsTaxFStartD");
            dataLoan.sUnPdBySellerAssessmentsTaxFEndD_rep = GetString("sUnPdBySellerAssessmentsTaxFEndD");
            dataLoan.sUnPdBySellerAssessmentsTaxF_rep = GetString("sUnPdBySellerAssessmentsTaxF");
            dataLoan.sUnPdBySellerU1FDesc = GetString("sUnPdBySellerU1FDesc");
            dataLoan.sUnPdBySellerU1FStartD_rep = GetString("sUnPdBySellerU1FStartD");
            dataLoan.sUnPdBySellerU1FEndD_rep = GetString("sUnPdBySellerU1FEndD");
            dataLoan.sUnPdBySellerU1F_rep = GetString("sUnPdBySellerU1F");
            dataLoan.sUnPdBySellerU2FDesc = GetString("sUnPdBySellerU2FDesc");
            dataLoan.sUnPdBySellerU2FStartD_rep = GetString("sUnPdBySellerU2FStartD");
            dataLoan.sUnPdBySellerU2FEndD_rep = GetString("sUnPdBySellerU2FEndD");
            dataLoan.sUnPdBySellerU2F_rep = GetString("sUnPdBySellerU2F");
            dataLoan.sUnPdBySellerU3FDesc = GetString("sUnPdBySellerU3FDesc");
            dataLoan.sUnPdBySellerU3FStartD_rep = GetString("sUnPdBySellerU3FStartD");
            dataLoan.sUnPdBySellerU3FEndD_rep = GetString("sUnPdBySellerU3FEndD");
            dataLoan.sUnPdBySellerU3F_rep = GetString("sUnPdBySellerU3F");
            dataLoan.sUnPdBySellerU4FDesc = GetString("sUnPdBySellerU4FDesc");
            dataLoan.sUnPdBySellerU4FStartD_rep = GetString("sUnPdBySellerU4FStartD");
            dataLoan.sUnPdBySellerU4FEndD_rep = GetString("sUnPdBySellerU4FEndD");
            dataLoan.sUnPdBySellerU4F_rep = GetString("sUnPdBySellerU4F");
            dataLoan.sUnPdBySellerU5FDesc = GetString("sUnPdBySellerU5FDesc");
            dataLoan.sUnPdBySellerU5FStartD_rep = GetString("sUnPdBySellerU5FStartD");
            dataLoan.sUnPdBySellerU5FEndD_rep = GetString("sUnPdBySellerU5FEndD");
            dataLoan.sUnPdBySellerU5F_rep = GetString("sUnPdBySellerU5F");
            dataLoan.sUnPdBySellerU6FDesc = GetString("sUnPdBySellerU6FDesc");
            dataLoan.sUnPdBySellerU6FStartD_rep = GetString("sUnPdBySellerU6FStartD");
            dataLoan.sUnPdBySellerU6FEndD_rep = GetString("sUnPdBySellerU6FEndD");
            dataLoan.sUnPdBySellerU6F_rep = GetString("sUnPdBySellerU6F");
            dataLoan.sUnPdBySellerU7FDesc = GetString("sUnPdBySellerU7FDesc");
            dataLoan.sUnPdBySellerU7FStartD_rep = GetString("sUnPdBySellerU7FStartD");
            dataLoan.sUnPdBySellerU7FEndD_rep = GetString("sUnPdBySellerU7FEndD");
            dataLoan.sUnPdBySellerU7F_rep = GetString("sUnPdBySellerU7F");
            // 700
            dataLoan.sRealEstateBrokerFees1F_rep = GetString("sRealEstateBrokerFees1F");
            dataLoan.sRealEstateBrokerFees1FAgentName = GetString("sRealEstateBrokerFees1FAgentName");
            dataLoan.sRealEstateBrokerFees2F_rep = GetString("sRealEstateBrokerFees2F");
            dataLoan.sRealEstateBrokerFees2FAgentName = GetString("sRealEstateBrokerFees2FAgentName");
            dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementFFromSeller_rep = GetString("sRealEstateBrokerFeesCommissionPdAtSettlementFFromSeller");
            dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementFFromBorr_rep = GetString("sRealEstateBrokerFeesCommissionPdAtSettlementFFromBorr");
            dataLoan.sRealEstateBrokerFeesU1FDesc = GetString("sRealEstateBrokerFeesU1FDesc");
            dataLoan.sRealEstateBrokerFeesU1FFromSeller_rep = GetString("sRealEstateBrokerFeesU1FFromSeller");
            dataLoan.sRealEstateBrokerFeesU1FFromBorr_rep = GetString("sRealEstateBrokerFeesU1FFromBorr");

            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                // Column
                dataLoan.sSellerSettlementChargesU01FHudline_rep = GetString("sSellerSettlementChargesU01FHudline");
                dataLoan.sSellerSettlementChargesU01FDesc = GetString("sSellerSettlementChargesU01FDesc");
                dataLoan.sSellerSettlementChargesU01F_rep = GetString("sSellerSettlementChargesU01F");
                dataLoan.sSellerSettlementChargesU02FHudline_rep = GetString("sSellerSettlementChargesU02FHudline");
                dataLoan.sSellerSettlementChargesU02FDesc = GetString("sSellerSettlementChargesU02FDesc");
                dataLoan.sSellerSettlementChargesU02F_rep = GetString("sSellerSettlementChargesU02F");
                dataLoan.sSellerSettlementChargesU03FHudline_rep = GetString("sSellerSettlementChargesU03FHudline");
                dataLoan.sSellerSettlementChargesU03FDesc = GetString("sSellerSettlementChargesU03FDesc");
                dataLoan.sSellerSettlementChargesU03F_rep = GetString("sSellerSettlementChargesU03F");
                dataLoan.sSellerSettlementChargesU04FHudline_rep = GetString("sSellerSettlementChargesU04FHudline");
                dataLoan.sSellerSettlementChargesU04FDesc = GetString("sSellerSettlementChargesU04FDesc");
                dataLoan.sSellerSettlementChargesU04F_rep = GetString("sSellerSettlementChargesU04F");
                dataLoan.sSellerSettlementChargesU05FHudline_rep = GetString("sSellerSettlementChargesU05FHudline");
                dataLoan.sSellerSettlementChargesU05FDesc = GetString("sSellerSettlementChargesU05FDesc");
                dataLoan.sSellerSettlementChargesU05F_rep = GetString("sSellerSettlementChargesU05F");
                dataLoan.sSellerSettlementChargesU06FHudline_rep = GetString("sSellerSettlementChargesU06FHudline");
                dataLoan.sSellerSettlementChargesU06FDesc = GetString("sSellerSettlementChargesU06FDesc");
                dataLoan.sSellerSettlementChargesU06F_rep = GetString("sSellerSettlementChargesU06F");
                dataLoan.sSellerSettlementChargesU07FHudline_rep = GetString("sSellerSettlementChargesU07FHudline");
                dataLoan.sSellerSettlementChargesU07FDesc = GetString("sSellerSettlementChargesU07FDesc");
                dataLoan.sSellerSettlementChargesU07F_rep = GetString("sSellerSettlementChargesU07F");
                dataLoan.sSellerSettlementChargesU08FHudline_rep = GetString("sSellerSettlementChargesU08FHudline");
                dataLoan.sSellerSettlementChargesU08FDesc = GetString("sSellerSettlementChargesU08FDesc");
                dataLoan.sSellerSettlementChargesU08F_rep = GetString("sSellerSettlementChargesU08F");
                dataLoan.sSellerSettlementChargesU09FHudline_rep = GetString("sSellerSettlementChargesU09FHudline");
                dataLoan.sSellerSettlementChargesU09FDesc = GetString("sSellerSettlementChargesU09FDesc");
                dataLoan.sSellerSettlementChargesU09F_rep = GetString("sSellerSettlementChargesU09F");
                dataLoan.sSellerSettlementChargesU10FHudline_rep = GetString("sSellerSettlementChargesU10FHudline");
                dataLoan.sSellerSettlementChargesU10FDesc = GetString("sSellerSettlementChargesU10FDesc");
                dataLoan.sSellerSettlementChargesU10F_rep = GetString("sSellerSettlementChargesU10F");
            }
            else if (GetString("sSellerSettlementChargesU01FHudline", null) != null)
            {
                throw new CBaseException("This page is no longer valid. Please refresh and renter your information.", "User loaded hud1 before migration.");
            }

            if (dataLoan.sLT == E_sLT.FHA)
            {
                dataLoan.sFhaAddendToHud1NonBorrSrcFunds = GetString("sFhaAddendToHud1NonBorrSrcFunds");
            }

            //Section show/hide state
            dataLoan.sHUD1DataOpenSections = GetString("OpenSections");
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, this.GetType());
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sPdByBorrowerU5FHudline",dataLoan.sPdByBorrowerU5FHudline_rep);
            SetResult("sPdByBorrowerU5FDesc",dataLoan.sPdByBorrowerU5FDesc);
            SetResult("sPdByBorrowerU5F",dataLoan.sPdByBorrowerU5F_rep);
            SetResult("sPdByBorrowerU6FHudline", dataLoan.sPdByBorrowerU6FHudline_rep);
            SetResult("sPdByBorrowerU6FDesc", dataLoan.sPdByBorrowerU6FDesc);
            SetResult("sPdByBorrowerU6F", dataLoan.sPdByBorrowerU6F_rep);
            SetResult("sPdByBorrowerU4FDesc", dataLoan.sPdByBorrowerU4FDesc);
            SetResult("sPdByBorrowerU4F", dataLoan.sPdByBorrowerU4F_rep);
        }
    }
}