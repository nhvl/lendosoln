﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Conversions.FloodOrder;
using LendersOffice.Conversions.FloodOrder.Request;
using LendersOffice.Conversions.FloodOrder.Response;
using System.Net;
using System.Text;
using LendersOffice.Admin;
using LendersOffice.Audit;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class FloodOrderTab : BaseLoanUserControl, IAutoLoadUserControl
    {
        #region Variables
        private BrokerDB m_lender;
        private string m_sResponseXML;
        private FloodSavedAuthentication m_auth;
        private string m_sMessage = "";
        private E_FloodStatus m_status = E_FloodStatus.None;
        private Guid m_EdocID = Guid.Empty;

        protected bool IsIntegrationEnabled
        {
            get { return m_lender.IsEnableFloodIntegration; }
        }

        protected string UserMessage
        {
            get { return m_sMessage; }
        }

        protected E_FloodStatus FloodStatus
        {
            get { return m_status; }
        }

        protected Guid EDocID
        {
            get { return m_EdocID; }
        }
        #endregion

        public void LoadData()
        {
            GetLoginInfoFromDB();
            MergeUIAndDBLoginInfo();

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FloodOrderTab));
            dataLoan.InitLoad();

            LoadOrderType(dataLoan.sFloodOrderType);
            sFloodCertId.Text = dataLoan.sFloodCertId;
            sFloodOrderQueryID.Text = dataLoan.sFloodOrderQueryID;

            sIsFloodOrderLOL.Checked = dataLoan.sIsFloodOrderLOL;
            sIsFloodOrderHMDA.Checked = dataLoan.sIsFloodOrderHMDA;
            sIsFloodOrderRushOrder.Checked = dataLoan.sIsFloodOrderRushOrder;
            sIsFloodOrderPayByCreditCard.Checked = dataLoan.sIsFloodOrderPayByCreditCard;

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            aBFirstNm.Text = dataApp.aBFirstNm;
            aBLastNm.Text = dataApp.aBLastNm;

            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip.TrimWhitespaceAndBOM();
            sSpLegalDesc.Text = dataLoan.sSpLegalDesc;
            sAssessorsParcelId.Text = dataLoan.sAssessorsParcelId;
        }

        public void SaveData()
        {
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            m_lender = this.BrokerDB;
            sSpZip.SmartZipcode(sSpCity, sSpState);
        }

        protected void OnSubmitClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                SubmitFloodOrder();
                if (m_status == E_FloodStatus.Completed)
                {
                    RefreshOrderID();
                }
            }
            SaveLoginInfoToDB();
        }

        private void GetLoginInfoFromDB()
        {
            m_auth = new FloodSavedAuthentication(BrokerUser.BrokerId, BrokerUser.UserId);
        }

        private void HandleMessageToUser(string sMessage) 
        {
            switch(m_status)
            {
                case E_FloodStatus.Completed:
                    m_sMessage = "Order complete. Flood Certificate uploaded to EDocs. View now?";
                    break;
                case E_FloodStatus.ERROR:
                case E_FloodStatus.Manual:
                    m_sMessage = sMessage;
                    break;
                case E_FloodStatus.Pending:
                case E_FloodStatus.Duplicate:
                case E_FloodStatus.None:
                default:
                    m_status = E_FloodStatus.ERROR;
                    HandleMessageToUser(ErrorMessages.Flood.UnknownError);
                    break;
            }
        }

        private void HandleWebException(WebException exc)
        {
            LogWebException(exc);
            m_status = E_FloodStatus.ERROR;
            m_sMessage = ErrorMessages.Flood.UnknownError;
        }

        private void LoadOrderType(E_FloodActionType sFloodOrderType)
        {
            switch(sFloodOrderType)
            {
                case E_FloodActionType.None:
                case E_FloodActionType.Original:
                    NewReport.Checked = true;
                    break;
                case E_FloodActionType.Reissue:
                    Reissue.Checked = true;
                    break;
                case E_FloodActionType.StatusQuery:
                    Query.Checked = true;
                    break;
                case E_FloodActionType.Upgrade:
                    Upgrade.Checked = true;
                    break;
                default:
                    throw new UnhandledEnumException(sFloodOrderType);
            }
        }

        private void MergeUIAndDBLoginInfo()
        {
            bool bRememberedLogin = !string.IsNullOrEmpty(m_auth.UserName);

            if (!IsPostBack)
            {
                m_loginName.Text = m_auth.UserName;
                m_rememberLogin.Checked = bRememberedLogin;
            }
            else
            {
                m_auth.UserName = m_loginName.Text;
                if (m_password.Text != ConstAppDavid.FakePasswordDisplay)
                {
                    m_auth.Password = m_password.Text;
                }
            }

            if (m_rememberLogin.Checked)
            {
                m_password.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
            }
        }

        private void RecordAudit(string sXML)
        {
            if (!String.IsNullOrEmpty(sXML))
                m_sResponseXML = sXML;

            AbstractAuditItem xmlAuditItem = new FloodSubmissionXmlAuditItem(PrincipalFactory.CurrentPrincipal, m_sResponseXML);
            AuditManager.RecordAudit(LoanID, xmlAuditItem);
        }

        private void RefreshOrderID()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FloodOrderTab));
            dataLoan.InitLoad();

            sFloodCertId.Text = dataLoan.sFloodCertId;
            sFloodOrderQueryID.Text = dataLoan.sFloodOrderQueryID;
        }

        private void SaveLoginInfoToDB()
        {
            if (IsPostBack)
            {
                if (!m_rememberLogin.Checked)
                {
                    m_auth.UserName = "";
                    m_auth.Password = "";
                    m_loginName.Text = "";                
                }
            }
            m_auth.Save();
        }

        private void SubmitFloodOrder()
        {
            FloodRequest request = FloodServer.CreateRequest(m_auth.UserName, m_auth.Password, LoanID);
            FloodResponse response = null;
            E_FloodCertificationPreparerT floodProvider = (E_FloodCertificationPreparerT)m_lender.FloodProviderID;

            try
            {
                response = FloodServer.Submit(request, floodProvider, out m_sResponseXML, m_lender.BrokerID);
            }
            catch (WebException exc)
            {
                HandleWebException(exc);
                return;
            }

            FloodProcessor processor = new FloodProcessor(response, LoanID, floodProvider, m_lender.IsEDocsEnabled, m_lender.FloodDocTypeID, m_lender.BrokerID);
            try
            {
                processor.ProcessFloodResponse(m_auth.UserName, m_auth.Password);
            }
            catch (WebException exc)
            {
                HandleWebException(exc);
                return;
            }

            RecordAudit(processor.ResponseXML);

            m_status = processor.Status;
            m_EdocID = processor.EDocID;
            HandleMessageToUser(processor.Message);
        }

        private void LogWebException(WebException exc)
        {
            StringBuilder debugMsg = new StringBuilder();
            debugMsg.AppendLine(ErrorMessages.Flood.NoResponse);
            debugMsg.AppendLine(exc.Message + Environment.NewLine + exc.StackTrace);
            Tools.LogInfo(debugMsg.ToString());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}