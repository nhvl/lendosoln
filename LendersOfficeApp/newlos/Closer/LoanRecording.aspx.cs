﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class LoanRecording : BaseLoanPage
    {
        protected override Permission[] RequiredWritePermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserWrite };
            }
        }

        protected override Permission[] RequiredReadPermissions
        {
            get
            {
                return new Permission[] { Permission.AllowCloserRead };
            }
        }
        
        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LoanRecording));
            dataLoan.InitLoad();
            
            sSpState.Value = dataLoan.sSpState;
            Tools.Bind_sSpCounty(dataLoan.sSpState, sSpCounty, true);
            Tools.SetDropDownListCaseInsensitive(sSpCounty, dataLoan.sSpCounty);
            sRecordedD.Text = dataLoan.sRecordedD_rep;
            sRecordedN.Text = dataLoan.sRecordedN;
            sRecordingInstrumentNum.Text = dataLoan.sRecordingInstrumentNum;
            sRecordingVolumeNum.Text = dataLoan.sRecordingVolumeNum;
            sRecordingBookNum.Text = dataLoan.sRecordingBookNum;
            sRecordingPageNum.Text = dataLoan.sRecordingPageNum;
            sMersMin.Text = dataLoan.sMersMin;
            sMersIsOriginalMortgagee.Checked = dataLoan.sMersIsOriginalMortgagee;
            sMersOriginatingOrgId.Text = dataLoan.sMersOriginatingOrgId;
            sMersOriginatingOrgIdLckd.Checked = dataLoan.sMersOriginatingOrgIdLckd;
            sMersRegistrationD.Text = dataLoan.sMersRegistrationD_rep;
            sMersTobD.Text = dataLoan.sMersTobD_rep;
            sMersTosD.Text = dataLoan.sMersTosD_rep;
            sMersTosDLckd.Checked = dataLoan.sMersTosDLckd;
            sHasENote.Checked = dataLoan.sHasENote;
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Loan Recording";
            this.PageID = "LoanRecording";

            this.sSpState.Attributes.Add("onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event)");

            this.GenerateMersMin.Visible = Broker.IsAutoGenerateMersMin;
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
