﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LendersOffice.Common;
using System.Text.RegularExpressions;
using System.Text;

namespace LendersOfficeApp.newlos.Closer
{
    public class LoanRecordingServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName) {
                case "GenerateMersMin":
                    GenerateMersMin();
                    break;

            }
        }

        private void GenerateMersMin()
        {
            var currentPrincipal = PrincipalFactory.CurrentPrincipal;
            string sLNm = GetString("sLNm", "");

            string sMersMinVal;
            string errorReasons;

            if (!Tools.TryGenerateMersMin(currentPrincipal, sLNm, out sMersMinVal, out errorReasons))
            {
                SetResult("Error", errorReasons);
                return;
            }

            SetResult("sMersMin", sMersMinVal);
        }

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LoanRecordingServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sSpCounty = GetString("sSpCounty");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sRecordedD_rep = GetString("sRecordedD");
            dataLoan.sRecordedN = GetString("sRecordedN");
            dataLoan.sRecordingInstrumentNum = GetString("sRecordingInstrumentNum");
            dataLoan.sRecordingVolumeNum = GetString("sRecordingVolumeNum");
            dataLoan.sRecordingBookNum = GetString("sRecordingBookNum");
            dataLoan.sRecordingPageNum = GetString("sRecordingPageNum");
            dataLoan.sMersMin = GetString("sMersMin");
            dataLoan.sMersIsOriginalMortgagee = GetBool("sMersIsOriginalMortgagee");
            dataLoan.sMersOriginatingOrgId = GetString("sMersOriginatingOrgId");
            dataLoan.sMersOriginatingOrgIdLckd = GetBool("sMersOriginatingOrgIdLckd");
            dataLoan.sMersRegistrationD_rep = GetString("sMersRegistrationD");
            dataLoan.sMersTobD_rep = GetString("sMersTobD");
            dataLoan.sMersTosD_rep = GetString("sMersTosD");
            dataLoan.sMersTosDLckd = GetBool("sMersTosDLckd");
            dataLoan.sHasENote = GetBool("sHasENote");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sSpCounty", dataLoan.sSpCounty);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sRecordedD", dataLoan.sRecordedD_rep);
            SetResult("sRecordedN", dataLoan.sRecordedN);
            SetResult("sRecordingInstrumentNum", dataLoan.sRecordingInstrumentNum);
            SetResult("sRecordingVolumeNum", dataLoan.sRecordingVolumeNum);
            SetResult("sRecordingBookNum", dataLoan.sRecordingBookNum);
            SetResult("sRecordingPageNum", dataLoan.sRecordingPageNum);
            SetResult("sMersMin", dataLoan.sMersMin);
            SetResult("sMersIsOriginalMortgagee", dataLoan.sMersIsOriginalMortgagee);
            SetResult("sMersOriginatingOrgId", dataLoan.sMersOriginatingOrgId);
            SetResult("sMersOriginatingOrgIdLckd", dataLoan.sMersOriginatingOrgIdLckd);
            SetResult("sMersRegistrationD", dataLoan.sMersRegistrationD_rep);
            SetResult("sMersTobD", dataLoan.sMersTobD_rep);
            SetResult("sMersTosD", dataLoan.sMersTosD_rep);
            SetResult("sMersTosDLckd", dataLoan.sMersTosDLckd);
        }
    }
    public partial class LoanRecordingService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new LoanRecordingServiceItem());
        }
    }


}
