﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using DataAccess.Sellers;
using LendersOffice.Common;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class SellerInfo : BaseLoanPage
    {
        private ListItem[] SellerTypes = new ListItem[]
        {
            new ListItem("Individual", E_SellerType.Individual.ToString()),
            new ListItem("Entity", E_SellerType.Entity.ToString())
        };
        private ListItem[] SellerEntityTypes = new ListItem[]
        {
            new ListItem("-- Select Entity Type --", E_SellerEntityType.Undefined.ToString()),
            new ListItem("Company", E_SellerEntityType.Company.ToString()),
            new ListItem("Corporation", E_SellerEntityType.Corporation.ToString()),
            new ListItem("General Partnership", E_SellerEntityType.GeneralPartnership.ToString()),
            new ListItem("Limited Liability Company", E_SellerEntityType.LimitedLiabilityCompany.ToString()),
            new ListItem("Limited Liability Corporation", E_SellerEntityType.LimitedLiabilityCorporation.ToString()),
            new ListItem("Limited Partnership", E_SellerEntityType.LimitedPartnership.ToString()),
            new ListItem("Sole Proprietor", E_SellerEntityType.SoleProprietor.ToString())
        };
        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SellerInfo));
            dataLoan.InitLoad();
            sSellerVesting.Text = dataLoan.sSellerVesting;
            if (dataLoan.sSellerCollection.IsEmpty())
            {
                NumberOfSellers.SelectedValue = "0";
            }
            else
            {
                NumberOfSellers.SelectedValue = dataLoan.sSellerCollection.ListOfSellers.Count.ToString();
                Sellers.DataSource = dataLoan.sSellerCollection.ListOfSellers;
                Sellers.DataBind();
            }

            SellerCollection sellersFromAgents = new SellerCollection();
            sellersFromAgents.PopulateFromAgents(dataLoan.GetAgentsOfRole(E_AgentRoleT.Seller, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            RegisterJsObjectArray("AgentsOfRoleSeller", sellersFromAgents.IsEmpty() ? null : sellersFromAgents.ListOfSellers);
        }

        protected void PageInit(object sender, EventArgs e)
        {
            this.PageTitle = "Seller Info";
            this.PageID = "SellerInfo";

            this.EnableJqueryMigrate = false;

            for (int i = 0; i <= 10; i++)
                NumberOfSellers.Items.Add(i.ToString());

            SellerTemplateSellerType.Items.AddRange(SellerTypes);
            SellerTemplateSellerEntityType.Items.AddRange(SellerEntityTypes);
        }

        protected void SellerList_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            var seller = (Seller)args.Item.DataItem;
            TextBox name = (TextBox)args.Item.FindControl("Name");
            HtmlInputHidden agentId = (HtmlInputHidden)args.Item.FindControl("AgentId");
            TextBox streetAddress = (TextBox)args.Item.FindControl("StreetAddress");
            TextBox city = (TextBox)args.Item.FindControl("City");
            StateDropDownList state = (StateDropDownList)args.Item.FindControl("State");
            TextBox zipcode = (TextBox)args.Item.FindControl("Zipcode");
            DropDownList sellerType = (DropDownList)args.Item.FindControl("SellerType");
            DropDownList sellerEntityType = (DropDownList)args.Item.FindControl("SellerEntityType");

            name.Text = seller.Name;
            agentId.Value = seller.AgentId.ToString();
            streetAddress.Text = seller.Address.StreetAddress;
            city.Text = seller.Address.City;
            state.Value = seller.Address.State;
            zipcode.Text = seller.Address.PostalCode;

            sellerType.Items.AddRange(SellerTypes);
            sellerType.SelectedValue = seller.Type.ToString();

            sellerEntityType.Items.AddRange(SellerEntityTypes);
            if (seller.Type == E_SellerType.Entity)
            {
                sellerEntityType.SelectedValue = seller.EntityType.ToString();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
