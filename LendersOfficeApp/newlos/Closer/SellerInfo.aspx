﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SellerInfo.aspx.cs" Inherits="LendersOfficeApp.newlos.Closer.SellerInfo" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Seller Info</title>
    <style type="text/css">
        #SellerInfo
        {
            padding-bottom: 3px;
        }
        .MostlyFull
        {
            width: 28em;
        }
        .FullWidth
        {
            width: 34em;
        }
        .TitleWidth
        {
            vertical-align: top;
            width: 15em;
            padding: 3px;
            font-weight: bold;
            display: inline-block;
        }
        #SellerList
        {
            list-style-type: none;
            margin: 0px;
            padding: 3px 0px;
            border: 0px;
        }
        #SellerList li
        {
            margin-bottom: 20px;
        }
        div.FieldItem
        {
            margin: 5px 0px;
        }
        .City
        {
            width: 25.4em;
        }
        .Zipcode
        {
            width: 4em;
        }
        .SellerType
        {
            width: 7.7em;
        }
        .SellerEntityType
        {
            width: 26em;
        }
    </style>

</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <div>
    <table cellspacing="0" cellpadding="0" width="700">
        <tr>
            <td class="MainRightHeader" nowrap>
                Seller Info
            </td>
        </tr>
        <tr>
            <td>
                <div id="SellerInfo" class="InsetBorder">
                    <input type="hidden" id="SellerInfoHolder" name="SellerInfoHolder" value="" />
                    <div class="FieldItem">
                        <span class="FieldLabel TitleWidth">Number of Sellers</span>
                        <asp:DropDownList ID="NumberOfSellers" runat="server"></asp:DropDownList>
                        <input type="button" value="Populate from Agents" id="PopulateSellersFromAgentsBtn" />
                    </div>
                    <ul id="SellerList">
                        <asp:Repeater ID="Sellers" runat="server" OnItemDataBound="SellerList_OnItemDataBound">
                        <ItemTemplate>
                            <li class="SellerListItem">
                                <div>
                                    <span class="TitleWidth FieldLabel">Seller Name</span>
                                    <asp:TextBox ID="Name" CssClass="Name FullWidth" runat="server" />
                                    <input type="hidden" ID="AgentId" runat="server" class="AgentId" />
                                </div>
                                <div class="AddressParts">
                                    <div>
                                        <span class="TitleWidth FieldLabel">Seller Address</span>
                                        <asp:TextBox ID="StreetAddress" CssClass="StreetAddress FullWidth" runat="server" />
                                    </div>
                                    <div>
                                        <span class="TitleWidth FieldLabel">&nbsp;</span>
                                        <asp:TextBox ID="City" CssClass="City" runat="server" />
                                        <ml:StateDropDownList runat="server" class="State" ID="State" />
                                        <asp:TextBox ID="Zipcode" CssClass="Zipcode" runat="server" />
                                    </div>
                                </div>
                                <div>
                                    <span class="TitleWidth FieldLabel">&nbsp;</span>
                                    <asp:DropDownList ID="SellerType" CssClass="SellerType" runat="server">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="SellerEntityType" CssClass="SellerEntityType" runat=server>
                                    </asp:DropDownList>
                                </div>
                            </li>
                        </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="FieldItem">
                        <span class="TitleWidth FieldLabel">Seller Vesting </span>
                        <asp:TextBox ID="sSellerVesting" runat="server" CssClass="MostlyFull" TextMode="MultiLine"
                            Rows="4"></asp:TextBox>
                    </div>
                </div>
            </td>
        </tr>
        <tr style="display:none;">
            <td>
                <%--Create new seller form elements using this template.--%>
                <ul id="SellerTemplate" style="display: none;">
                    <li class="SellerListItem">
                        <div>
                            <span class="TitleWidth FieldLabel">Seller Name</span>
                            <input type="text" class="FullWidth Name" />
                            <input type="hidden" class="AgentId" value="<%=AspxTools.HtmlString(Guid.Empty) %>" />
                        </div>
                        <div class="AddressParts">
                            <div>
                                <span class="TitleWidth FieldLabel">Seller Address</span>
                                <input type="text" class="FullWidth StreetAddress" />
                            </div>
                            <div>
                                <span class="TitleWidth FieldLabel">&nbsp;</span>
                                <input type="text" class="City" />
                                <ml:StateDropDownList runat="server" class="State" ID="SellerTemplateState" />
                                <input type="text" class="Zipcode" />
                            </div>
                        </div>
                        <div>
                            <span class="TitleWidth FieldLabel">&nbsp;</span>
                            <asp:DropDownList ID="SellerTemplateSellerType" CssClass="SellerType" runat="server"></asp:DropDownList>
                            <asp:DropDownList ID="SellerTemplateSellerEntityType" CssClass="SellerEntityType" runat=server></asp:DropDownList>
                        </div>
                    </li>
                </ul>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
<script language="javascript" type="text/javascript">
    function PopulateSellersFromAgentsBtn_click() {
        var i, agentCount;
        if (AgentsOfRoleSeller == null) {
            $('#NumberOfSellers').val('0'); return;
        }
        $('#SellerList').children().remove();
        agentCount = AgentsOfRoleSeller.length;
        $('#NumberOfSellers').val(agentCount);
        for (i = 0; i < agentCount; ++i)
            $('#SellerList').append(GeneratePopulatedSeller(AgentsOfRoleSeller[i]));
        if(typeof(updateDirtyBit) != 'undefined')
            updateDirtyBit();
    }

    function GeneratePopulatedSeller(SellerObject) {
        var newSeller = FreshSeller();
        $(newSeller).find('.Name').first().val(SellerObject.Name);
        $(newSeller).find('.AgentId').first().val(SellerObject.AgentId);
        $(newSeller).find('.StreetAddress').first().val(SellerObject.Address.StreetAddress);
        $(newSeller).find('.City').first().val(SellerObject.Address.City);
        $(newSeller).find('.State').first().val(SellerObject.Address.State);
        $(newSeller).find('.Zipcode').first().val(SellerObject.Address.PostalCode);
        return newSeller;
    }

    function FreshSeller() {
        return $('#SellerTemplate').children('li').first().clone();
    }

    function disableSellerEntityType($sellerType) {
        var IsNotEntity = ($sellerType.val() != 'Entity');
        var sellerEntityType = $sellerType.next();
        if (IsNotEntity) sellerEntityType.val('Undefined');
        sellerEntityType.prop('disabled', IsNotEntity);
    }

    window.onload = function() {
      disableSellerEntityType($('.SellerType'));
      $('#PopulateSellersFromAgentsBtn').click(PopulateSellersFromAgentsBtn_click);

      $('#SellerList').on('change', '.SellerType', function () {
          disableSellerEntityType($(this));
      });

      $('#SellerList').on('keydown', 'input', function (event) {
          if (typeof (updateDirtyBit) != 'undefined') updateDirtyBit(event);
      });

      $('#SellerList').on('change', 'select', function (event) {
          if (typeof (updateDirtyBit) != 'undefined') updateDirtyBit(event);
      });

      $('.Name').change(function () {
          $listItem = $(this).closest('.SellerListItem');
          $listItem.find('.AgentId').val(<%=AspxTools.JsString(Guid.Empty) %>);
      });

      $(document).on('blur', '.Zipcode', function (event) {
          var parent = $(this).parents('li');
          smartZipcode(
                  this,
                  parent.find('.City').get(0),
                  parent.find('.State').get(0),
                  null, event);
      });

      $(document).on('focus', '.Zipcode', function () {
          this.oldValue = this.value;
      });

      $('#NumberOfSellers').change(function() {
          var selectedNum = $(this).val();
          var currentNum = $('#SellerList').children('li').length;
          while (selectedNum < currentNum) {
              $('#SellerList').children('li').last().remove();
              --currentNum;
          }
          while (selectedNum > currentNum) {
              $('#SellerList').append(FreshSeller());
              ++currentNum;
          }
      });
    };

    jQuery(function($){
        //Stuff the contents of our seller list into an input before save
        var oldSaveMe = saveMe;
        saveMe = function(bRefresh)
        {
            var sellerList = [];
            $('#SellerList li').each(function(){
                var current, address = 
                { 
                    'StreetAddress': $(this).find('.StreetAddress').val(),
                    'City': $(this).find('.City').val(),
                    'State': $(this).find('.State').val(),
                    'PostalCode': $(this).find('.Zipcode').val()
                };
                current = {
                    'Name': $(this).find('.Name').val(),
                    'AgentId': $(this).find('.AgentId').val(),
                    'Address': address,
                    'Type': $(this).find('.SellerType').val(),
                    'EntityType': $(this).find('.SellerEntityType').val()
                };
                sellerList.push(current);
            });

            var result = {
                    'ListOfSellers': sellerList
                };
            var saveVal = JSON.stringify(result);
            $('#SellerInfoHolder').val(saveVal);
            return oldSaveMe(bRefresh);
        }
    });
</script>
</html>
