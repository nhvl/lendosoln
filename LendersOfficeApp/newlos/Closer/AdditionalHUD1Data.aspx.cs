using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Closer
{
    public partial class AdditionalHUD1Data : BaseLoanPage
    {
        private CPageData dataLoan;
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;

            this.EnableJqueryMigrate = false;

            this.PageTitle = "Additional HUD-1 Data";
            this.PageID = "AdditionalHUD1Data";
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            dataLoan = CPageData.CreateUsingSmartDependency(LoanID, this.GetType().BaseType);
            dataLoan.InitLoad();
            BindData();
            BindSettlementChargeList();
            RegisterSectionState(dataLoan.sHUD1DataOpenSections);
            SectionColumn.Visible = dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy;
            this.RegisterJsScript("LQBPopup.js");
        }

        /// <summary>
        /// Registers a field which controls which sections are open on the client.
        /// </summary>
        /// <param name="OpenSections">This is a string of six 0's and 1's that maps to the sections in top-down order; the first index maps to 100&400, the last maps to the charges column.</param>
        private void RegisterSectionState(string OpenSections)
        {
            //We default to having the second and last columns open per spec.
            OpenSections = string.IsNullOrEmpty(OpenSections) ? "0010010" : OpenSections;
            ClientScript.RegisterHiddenField("OpenSections", OpenSections);
        }

        private bool ParseChar(char c)
        {
            return c == '1';
        }

        private class SettlementCharge
        {
            public int LineNumber { get; private set;}
            public string Description { get; private set; }
            public string Value { get; private set; }

            public SettlementCharge(int ln, string desc, string val)
            {
                LineNumber = ln;
                Description = desc;
                Value = val;
            }
        }

        private void BindSettlementChargeList()
        {
            List<SettlementCharge> charges = new List<SettlementCharge>()
            {
                new SettlementCharge(801, "Loan origination fee", dataLoan.sSettlementLOrigF_rep),
                new SettlementCharge(802, "Credit (-) or Charge (+)", dataLoan.sSettlementLDiscnt_rep),
                new SettlementCharge(804, "Appraisal fee", dataLoan.sSettlementApprF_rep),
                new SettlementCharge(805, "Credit report", dataLoan.sSettlementCrF_rep),
                new SettlementCharge(806, "Tax service fee", dataLoan.sSettlementTxServF_rep),
                new SettlementCharge(807, "Flood Certification", dataLoan.sSettlementFloodCertificationF_rep),
                new SettlementCharge(808, "Mortgage broker fee", dataLoan.sSettlementMBrokF_rep),
                new SettlementCharge(809, "Lender's inspection fee", dataLoan.sSettlementInspectF_rep),
                new SettlementCharge(810, "Processing fee", dataLoan.sSettlementProcF_rep),
                new SettlementCharge(811, "Underwriting fee", dataLoan.sSettlementUwF_rep),
                new SettlementCharge(812, "Wire transfer", dataLoan.sSettlementWireF_rep),
                new SettlementCharge(813, dataLoan.sSettlement800U1FDesc, dataLoan.sSettlement800U1F_rep),
                new SettlementCharge(814, dataLoan.sSettlement800U2FDesc, dataLoan.sSettlement800U2F_rep),
                new SettlementCharge(815, dataLoan.sSettlement800U3FDesc, dataLoan.sSettlement800U3F_rep),
                new SettlementCharge(816, dataLoan.sSettlement800U4FDesc, dataLoan.sSettlement800U4F_rep),
                new SettlementCharge(817, dataLoan.sSettlement800U5FDesc, dataLoan.sSettlement800U5F_rep),
                //new SettlementCharge(901, "Interest", dataLoan.sSettlementIPia_rep),
                new SettlementCharge(902, "Mortgage Insurance Premium", dataLoan.sMipPia_rep),
                new SettlementCharge(903, "Hazard Insurance", dataLoan.sSettlementHazInsPia_rep),
                new SettlementCharge(904, dataLoan.sSettlement904PiaDesc, dataLoan.sSettlement904Pia_rep),
                new SettlementCharge(905, "VA Funding Fee", dataLoan.sVaFf_rep),
                new SettlementCharge(906, dataLoan.sSettlement900U1PiaDesc, dataLoan.sSettlement900U1Pia_rep),
                //new SettlementCharge(1002, "Hazard insurance reserve", dataLoan.sSettlementHazInsRsrv_rep),
                //new SettlementCharge(1003, "Mortgage insurance reserve", dataLoan.sSettlementMInsRsrv_rep),
                //new SettlementCharge(1004, "Tax reserve", dataLoan.sSettlementRealETxRsrv_rep),
                //new SettlementCharge(1005, "School taxes", dataLoan.sSettlementSchoolTxRsrv_rep),
                //new SettlementCharge(1006, "Flood ins. reserve", dataLoan.sSettlementFloodInsRsrv_rep),
                //new SettlementCharge(1007, "Aggregate adjustment", dataLoan.sSettlementAggregateAdjRsrv_rep),
                //new SettlementCharge(1008, dataLoan.s1006ProHExpDesc, dataLoan.sSettlement1008Rsrv_rep),
                //new SettlementCharge(1009, dataLoan.s1007ProHExpDesc, dataLoan.sSettlement1009Rsrv_rep),
                new SettlementCharge(1102, "Closing/Escrow Fee", dataLoan.sSettlementEscrowF_rep),
                new SettlementCharge(1103, "Owner's title Insurance", dataLoan.sSettlementOwnerTitleInsF_rep),
                new SettlementCharge(1104, "Lender's title Insurance", dataLoan.sSettlementTitleInsF_rep),
                new SettlementCharge(1109, "Doc preparation fee", dataLoan.sSettlementDocPrepF_rep),
                new SettlementCharge(1110, "Notary fees", dataLoan.sSettlementNotaryF_rep),
                new SettlementCharge(1111, "Attorney fees", dataLoan.sSettlementAttorneyF_rep),
                new SettlementCharge(1112, dataLoan.sSettlementU1TcDesc, dataLoan.sSettlementU1Tc_rep),
                new SettlementCharge(1113, dataLoan.sSettlementU2TcDesc, dataLoan.sSettlementU2Tc_rep),
                new SettlementCharge(1114, dataLoan.sSettlementU3TcDesc, dataLoan.sSettlementU3Tc_rep),
                new SettlementCharge(1115, dataLoan.sSettlementU4TcDesc, dataLoan.sSettlementU4Tc_rep),
                new SettlementCharge(1201, "Recording fees", dataLoan.sSettlementRecF_rep),
                new SettlementCharge(1204, "City/County tax stamps", dataLoan.sSettlementCountyRtc_rep),
                new SettlementCharge(1205, "State tax/stamps", dataLoan.sSettlementStateRtc_rep),
                new SettlementCharge(1206, dataLoan.sSettlementU1GovRtcDesc, dataLoan.sSettlementU1GovRtc_rep),
                new SettlementCharge(1207, dataLoan.sSettlementU2GovRtcDesc, dataLoan.sSettlementU2GovRtc_rep),
                new SettlementCharge(1208, dataLoan.sSettlementU3GovRtcDesc, dataLoan.sSettlementU3GovRtc_rep),
                new SettlementCharge(1302, "Pest Inspection", dataLoan.sSettlementPestInspectF_rep),
                new SettlementCharge(1303, dataLoan.sSettlementU1ScDesc, dataLoan.sSettlementU1Sc_rep),
                new SettlementCharge(1304, dataLoan.sSettlementU2ScDesc, dataLoan.sSettlementU2Sc_rep),
                new SettlementCharge(1305, dataLoan.sSettlementU3ScDesc, dataLoan.sSettlementU3Sc_rep),
                new SettlementCharge(1306, dataLoan.sSettlementU4ScDesc, dataLoan.sSettlementU4Sc_rep),
                new SettlementCharge(1307, dataLoan.sSettlementU5ScDesc, dataLoan.sSettlementU5Sc_rep),

            };

            var asDictionary = charges.ToDictionary((a) => a.LineNumber.ToString());

            ClientScript.RegisterClientScriptBlock(this.GetType(), "ChargesDictionary", "var SettlementCharges = " + ObsoleteSerializationHelper.JavascriptJsonSerialize(asDictionary) + "; function GetSettlementCharges(){return SettlementCharges;}", true);
        }

        private void BindData()
        {
            // 100
            sPurchPriceB.Text = dataLoan.sPurchPrice_rep;
            sGrossDueFromBorrPersonalProperty.Text = dataLoan.sGrossDueFromBorrPersonalProperty_rep;
            sSettlementTotalEstimateSettlementCharge.Text = dataLoan.sSettlementTotalEstimateSettlementCharge_rep;
            sGrossDueFromBorrU1FDesc.Text = dataLoan.sGrossDueFromBorrU1FDesc;
            sGrossDueFromBorrU1F.Text = dataLoan.sGrossDueFromBorrU1F_rep;
            sGrossDueFromBorrU2FDesc.Text = dataLoan.sGrossDueFromBorrU2FDesc;
            sGrossDueFromBorrU2F.Text = dataLoan.sGrossDueFromBorrU2F_rep;
            // 400
            sPurchPriceS.Text = dataLoan.sPurchPrice_rep;
            GrossDueFromBorrPersonalPropertyS.Text = dataLoan.sGrossDueFromBorrPersonalProperty_rep;
            sGrossDueToSellerU1FDesc.Text = dataLoan.sGrossDueToSellerU1FDesc;
            sGrossDueToSellerU1F.Text = dataLoan.sGrossDueToSellerU1F_rep;
            sGrossDueToSellerU2FDesc.Text = dataLoan.sGrossDueToSellerU2FDesc;
            sGrossDueToSellerU2F.Text = dataLoan.sGrossDueToSellerU2F_rep;
            sGrossDueToSellerU2FSameAsBorr.Checked = dataLoan.sGrossDueToSellerU2FSameAsBorr;
            sGrossDueToSellerU3FDesc.Text = dataLoan.sGrossDueToSellerU3FDesc;
            sGrossDueToSellerU3F.Text = dataLoan.sGrossDueToSellerU3F_rep;
            sGrossDueToSellerU3FSameAsBorr.Checked = dataLoan.sGrossDueToSellerU3FSameAsBorr;
            // 100-400
            sPdBySellerCityTaxFStartD.Text = dataLoan.sPdBySellerCityTaxFStartD_rep;
            sPdBySellerCityTaxFEndD.Text = dataLoan.sPdBySellerCityTaxFEndD_rep;
            sPdBySellerCityTaxF.Text = dataLoan.sPdBySellerCityTaxF_rep;
            sPdBySellerCountyTaxFStartD.Text = dataLoan.sPdBySellerCountyTaxFStartD_rep;
            sPdBySellerCountyTaxFEndD.Text = dataLoan.sPdBySellerCountyTaxFEndD_rep;
            sPdBySellerCountyTaxF.Text = dataLoan.sPdBySellerCountyTaxF_rep;
            sPdBySellerAssessmentsTaxFStartD.Text = dataLoan.sPdBySellerAssessmentsTaxFStartD_rep;
            sPdBySellerAssessmentsTaxFEndD.Text = dataLoan.sPdBySellerAssessmentsTaxFEndD_rep;
            sPdBySellerAssessmentsTaxF.Text = dataLoan.sPdBySellerAssessmentsTaxF_rep;
            sPdBySellerU1FDesc.Text = dataLoan.sPdBySellerU1FDesc;
            sPdBySellerU1FStartD.Text = dataLoan.sPdBySellerU1FStartD_rep;
            sPdBySellerU1FEndD.Text = dataLoan.sPdBySellerU1FEndD_rep;
            sPdBySellerU1F.Text = dataLoan.sPdBySellerU1F_rep;
            sPdBySellerU2FDesc.Text = dataLoan.sPdBySellerU2FDesc;
            sPdBySellerU2FStartD.Text = dataLoan.sPdBySellerU2FStartD_rep;
            sPdBySellerU2FEndD.Text = dataLoan.sPdBySellerU2FEndD_rep;
            sPdBySellerU2F.Text = dataLoan.sPdBySellerU2F_rep;
            sPdBySellerU3FDesc.Text = dataLoan.sPdBySellerU3FDesc;
            sPdBySellerU3FStartD.Text = dataLoan.sPdBySellerU3FStartD_rep;
            sPdBySellerU3FEndD.Text = dataLoan.sPdBySellerU3FEndD_rep;
            sPdBySellerU3F.Text = dataLoan.sPdBySellerU3F_rep;
            sPdBySellerU4FDesc.Text = dataLoan.sPdBySellerU4FDesc;
            sPdBySellerU4FStartD.Text = dataLoan.sPdBySellerU4FStartD_rep;
            sPdBySellerU4FEndD.Text = dataLoan.sPdBySellerU4FEndD_rep;
            sPdBySellerU4F.Text = dataLoan.sPdBySellerU4F_rep;
            // 200
            sTotCashDeposit.Text = dataLoan.sTotCashDeposit.ToString();
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sPdByBorrowerU1FHudline.Text = dataLoan.sPdByBorrowerU1FHudline_rep;
            sPdByBorrowerU1FDesc.Text = dataLoan.sPdByBorrowerU1FDesc;
            sPdByBorrowerU1F.Text = dataLoan.sPdByBorrowerU1F_rep;
            sPdByBorrowerU2FHudline.Text = dataLoan.sPdByBorrowerU2FHudline_rep;
            sPdByBorrowerU2FDesc.Text = dataLoan.sPdByBorrowerU2FDesc;
            sPdByBorrowerU2F.Text = dataLoan.sPdByBorrowerU2F_rep;
            sPdByBorrowerU3FHudline.Text = dataLoan.sPdByBorrowerU3FHudline_rep;
            sPdByBorrowerU3FDesc.Text = dataLoan.sPdByBorrowerU3FDesc;
            sPdByBorrowerU3F.Text = dataLoan.sPdByBorrowerU3F_rep;
            sPdByBorrowerU4FHudline.Text = dataLoan.sPdByBorrowerU4FHudline_rep;
            sPdByBorrowerU4FDesc.Text = dataLoan.sPdByBorrowerU4FDesc;
            sPdByBorrowerU4FRowLckd.Checked = dataLoan.sPdByBorrowerU4FRowLckd;
            sPdByBorrowerU4F.Text = dataLoan.sPdByBorrowerU4F_rep;

            sPdByBorrowerU4FDesc.ReadOnly = !dataLoan.sPdByBorrowerU4FRowLckd;
            sPdByBorrowerU4F.ReadOnly = !dataLoan.sPdByBorrowerU4FRowLckd;

            sPdByBorrowerU5FHudline.Text = dataLoan.sPdByBorrowerU5FHudline_rep;
            sPdByBorrowerU5FDesc.Text = dataLoan.sPdByBorrowerU5FDesc;
            sPdByBorrowerU5F.Text = dataLoan.sPdByBorrowerU5F_rep;
            if (dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled && false == dataLoan.sIsLineOfCredit)
            {
                sPdByBorrowerU5FRowLckd.Checked = dataLoan.sPdByBorrowerU5FRowLckd;
                sPdByBorrowerU5FHudline.ReadOnly = !dataLoan.sPdByBorrowerU5FRowLckd;
                sPdByBorrowerU5FDesc.ReadOnly = !dataLoan.sPdByBorrowerU5FRowLckd;
                sPdByBorrowerU5F.ReadOnly = !dataLoan.sPdByBorrowerU5FRowLckd;
            }
            else
            {
                sPdByBorrowerU5FRowLckd.Visible = false;
            }
            sPdByBorrowerU6FHudline.Text = dataLoan.sPdByBorrowerU6FHudline_rep;
            sPdByBorrowerU6FDesc.Text = dataLoan.sPdByBorrowerU6FDesc;
            sPdByBorrowerU6F.Text = dataLoan.sPdByBorrowerU6F_rep;
            if (dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled && false == dataLoan.sIsLineOfCredit)
            {
                sPdByBorrowerU6FRowLckd.Checked = dataLoan.sPdByBorrowerU6FRowLckd;
                sPdByBorrowerU6FHudline.ReadOnly = !dataLoan.sPdByBorrowerU6FRowLckd;
                sPdByBorrowerU6FDesc.ReadOnly = !dataLoan.sPdByBorrowerU6FRowLckd;
                sPdByBorrowerU6F.ReadOnly = !dataLoan.sPdByBorrowerU6FRowLckd;
            }
            else
            {
                sPdByBorrowerU6FRowLckd.Visible = false;
            }

            // 500
            sReductionsDueToSellerExcessDeposit.Text = dataLoan.sReductionsDueToSellerExcessDeposit_rep;
            sReductionsDueToSellerPayoffOf1stMtgLoan.Text = dataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep;
            sReductionsDueToSellerPayoffOf2ndMtgLoan.Text = dataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep;
            sReductionsDueToSellerU1FHudline.Text = dataLoan.sReductionsDueToSellerU1FHudline_rep;
            sReductionsDueToSellerU1FDesc.Text = dataLoan.sReductionsDueToSellerU1FDesc;
            sReductionsDueToSellerU1F.Text = dataLoan.sReductionsDueToSellerU1F_rep;
            sReductionsDueToSellerU1SameAsBorr.Checked = dataLoan.sReductionsDueToSellerU1SameAsBorr;
            sReductionsDueToSellerU2FHudline.Text = dataLoan.sReductionsDueToSellerU2FHudline_rep;
            sReductionsDueToSellerU2FDesc.Text = dataLoan.sReductionsDueToSellerU2FDesc;
            sReductionsDueToSellerU2F.Text = dataLoan.sReductionsDueToSellerU2F_rep;
            sReductionsDueToSellerU2SameAsBorr.Checked = dataLoan.sReductionsDueToSellerU2SameAsBorr;
            sReductionsDueToSellerU3FHudline.Text = dataLoan.sReductionsDueToSellerU3FHudline_rep;
            sReductionsDueToSellerU3FDesc.Text = dataLoan.sReductionsDueToSellerU3FDesc;
            sReductionsDueToSellerU3F.Text = dataLoan.sReductionsDueToSellerU3F_rep;
            sReductionsDueToSellerU3SameAsBorr.Checked = dataLoan.sReductionsDueToSellerU3SameAsBorr;
            sReductionsDueToSellerU4FHudline.Text = dataLoan.sReductionsDueToSellerU4FHudline_rep;
            sReductionsDueToSellerU4FDesc.Text = dataLoan.sReductionsDueToSellerU4FDesc;
            sReductionsDueToSellerU4F.Text = dataLoan.sReductionsDueToSellerU4F_rep;
            sReductionsDueToSellerU4SameAsBorr.Checked = dataLoan.sReductionsDueToSellerU4SameAsBorr;
            sReductionsDueToSellerU5FHudline.Text = dataLoan.sReductionsDueToSellerU5FHudline_rep;
            sReductionsDueToSellerU5FDesc.Text = dataLoan.sReductionsDueToSellerU5FDesc;
            sReductionsDueToSellerU5F.Text = dataLoan.sReductionsDueToSellerU5F_rep;
            sReductionsDueToSellerU5SameAsBorr.Checked = dataLoan.sReductionsDueToSellerU5SameAsBorr;
            sReductionsDueToSellerU6FHudline.Text = dataLoan.sReductionsDueToSellerU6FHudline_rep;
            sReductionsDueToSellerU6FDesc.Text = dataLoan.sReductionsDueToSellerU6FDesc;
            sReductionsDueToSellerU6F.Text = dataLoan.sReductionsDueToSellerU6F_rep;
            sReductionsDueToSellerU6SameAsBorr.Checked = dataLoan.sReductionsDueToSellerU6SameAsBorr;
            // 200-500
            sUnPdBySellerCityTaxFStartD.Text = dataLoan.sUnPdBySellerCityTaxFStartD_rep;
            sUnPdBySellerCityTaxFEndD.Text = dataLoan.sUnPdBySellerCityTaxFEndD_rep;
            sUnPdBySellerCityTaxF.Text = dataLoan.sUnPdBySellerCityTaxF_rep;
            sUnPdBySellerCountyTaxFStartD.Text = dataLoan.sUnPdBySellerCountyTaxFStartD_rep;
            sUnPdBySellerCountyTaxFEndD.Text = dataLoan.sUnPdBySellerCountyTaxFEndD_rep;
            sUnPdBySellerCountyTaxF.Text = dataLoan.sUnPdBySellerCountyTaxF_rep;
            sUnPdBySellerAssessmentsTaxFStartD.Text = dataLoan.sUnPdBySellerAssessmentsTaxFStartD_rep;
            sUnPdBySellerAssessmentsTaxFEndD.Text = dataLoan.sUnPdBySellerAssessmentsTaxFEndD_rep;
            sUnPdBySellerAssessmentsTaxF.Text = dataLoan.sUnPdBySellerAssessmentsTaxF_rep;
            sUnPdBySellerU1FDesc.Text = dataLoan.sUnPdBySellerU1FDesc;
            sUnPdBySellerU1FStartD.Text = dataLoan.sUnPdBySellerU1FStartD_rep;
            sUnPdBySellerU1FEndD.Text = dataLoan.sUnPdBySellerU1FEndD_rep;
            sUnPdBySellerU1F.Text = dataLoan.sUnPdBySellerU1F_rep;
            sUnPdBySellerU2FDesc.Text = dataLoan.sUnPdBySellerU2FDesc;
            sUnPdBySellerU2FStartD.Text = dataLoan.sUnPdBySellerU2FStartD_rep;
            sUnPdBySellerU2FEndD.Text = dataLoan.sUnPdBySellerU2FEndD_rep;
            sUnPdBySellerU2F.Text = dataLoan.sUnPdBySellerU2F_rep;
            sUnPdBySellerU3FDesc.Text = dataLoan.sUnPdBySellerU3FDesc;
            sUnPdBySellerU3FStartD.Text = dataLoan.sUnPdBySellerU3FStartD_rep;
            sUnPdBySellerU3FEndD.Text = dataLoan.sUnPdBySellerU3FEndD_rep;
            sUnPdBySellerU3F.Text = dataLoan.sUnPdBySellerU3F_rep;
            sUnPdBySellerU4FDesc.Text = dataLoan.sUnPdBySellerU4FDesc;
            sUnPdBySellerU4FStartD.Text = dataLoan.sUnPdBySellerU4FStartD_rep;
            sUnPdBySellerU4FEndD.Text = dataLoan.sUnPdBySellerU4FEndD_rep;
            sUnPdBySellerU4F.Text = dataLoan.sUnPdBySellerU4F_rep;
            sUnPdBySellerU5FDesc.Text = dataLoan.sUnPdBySellerU5FDesc;
            sUnPdBySellerU5FStartD.Text = dataLoan.sUnPdBySellerU5FStartD_rep;
            sUnPdBySellerU5FEndD.Text = dataLoan.sUnPdBySellerU5FEndD_rep;
            sUnPdBySellerU5F.Text = dataLoan.sUnPdBySellerU5F_rep;
            sUnPdBySellerU6FDesc.Text = dataLoan.sUnPdBySellerU6FDesc;
            sUnPdBySellerU6FStartD.Text = dataLoan.sUnPdBySellerU6FStartD_rep;
            sUnPdBySellerU6FEndD.Text = dataLoan.sUnPdBySellerU6FEndD_rep;
            sUnPdBySellerU6F.Text = dataLoan.sUnPdBySellerU6F_rep;
            sUnPdBySellerU7FDesc.Text = dataLoan.sUnPdBySellerU7FDesc;
            sUnPdBySellerU7FStartD.Text = dataLoan.sUnPdBySellerU7FStartD_rep;
            sUnPdBySellerU7FEndD.Text = dataLoan.sUnPdBySellerU7FEndD_rep;
            sUnPdBySellerU7F.Text = dataLoan.sUnPdBySellerU7F_rep;
            // 700
            sRealEstateBrokerFees1F.Text = dataLoan.sRealEstateBrokerFees1F_rep;
            sRealEstateBrokerFees1FAgentName.Text = dataLoan.sRealEstateBrokerFees1FAgentName;
            sRealEstateBrokerFees2F.Text = dataLoan.sRealEstateBrokerFees2F_rep;
            sRealEstateBrokerFees2FAgentName.Text = dataLoan.sRealEstateBrokerFees2FAgentName;
            sRealEstateBrokerFeesCommissionPdAtSettlementFFromSeller.Text = dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementFFromSeller_rep;
            sRealEstateBrokerFeesCommissionPdAtSettlementFFromBorr.Text = dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementFFromBorr_rep;
            sRealEstateBrokerFeesU1FDesc.Text = dataLoan.sRealEstateBrokerFeesU1FDesc;
            sRealEstateBrokerFeesU1FFromSeller.Text = dataLoan.sRealEstateBrokerFeesU1FFromSeller_rep;
            sRealEstateBrokerFeesU1FFromBorr.Text = dataLoan.sRealEstateBrokerFeesU1FFromBorr_rep;
            // Column
            sSellerSettlementChargesU01FHudline.Text = dataLoan.sSellerSettlementChargesU01FHudline_rep;
            sSellerSettlementChargesU01FDesc.Text = dataLoan.sSellerSettlementChargesU01FDesc;
            sSellerSettlementChargesU01F.Text = dataLoan.sSellerSettlementChargesU01F_rep;
            sSellerSettlementChargesU02FHudline.Text = dataLoan.sSellerSettlementChargesU02FHudline_rep;
            sSellerSettlementChargesU02FDesc.Text = dataLoan.sSellerSettlementChargesU02FDesc;
            sSellerSettlementChargesU02F.Text = dataLoan.sSellerSettlementChargesU02F_rep;
            sSellerSettlementChargesU03FHudline.Text = dataLoan.sSellerSettlementChargesU03FHudline_rep;
            sSellerSettlementChargesU03FDesc.Text = dataLoan.sSellerSettlementChargesU03FDesc;
            sSellerSettlementChargesU03F.Text = dataLoan.sSellerSettlementChargesU03F_rep;
            sSellerSettlementChargesU04FHudline.Text = dataLoan.sSellerSettlementChargesU04FHudline_rep;
            sSellerSettlementChargesU04FDesc.Text = dataLoan.sSellerSettlementChargesU04FDesc;
            sSellerSettlementChargesU04F.Text = dataLoan.sSellerSettlementChargesU04F_rep;
            sSellerSettlementChargesU05FHudline.Text = dataLoan.sSellerSettlementChargesU05FHudline_rep;
            sSellerSettlementChargesU05FDesc.Text = dataLoan.sSellerSettlementChargesU05FDesc;
            sSellerSettlementChargesU05F.Text = dataLoan.sSellerSettlementChargesU05F_rep;
            sSellerSettlementChargesU06FHudline.Text = dataLoan.sSellerSettlementChargesU06FHudline_rep;
            sSellerSettlementChargesU06FDesc.Text = dataLoan.sSellerSettlementChargesU06FDesc;
            sSellerSettlementChargesU06F.Text = dataLoan.sSellerSettlementChargesU06F_rep;
            sSellerSettlementChargesU07FHudline.Text = dataLoan.sSellerSettlementChargesU07FHudline_rep;
            sSellerSettlementChargesU07FDesc.Text = dataLoan.sSellerSettlementChargesU07FDesc;
            sSellerSettlementChargesU07F.Text = dataLoan.sSellerSettlementChargesU07F_rep;
            sSellerSettlementChargesU08FHudline.Text = dataLoan.sSellerSettlementChargesU08FHudline_rep;
            sSellerSettlementChargesU08FDesc.Text = dataLoan.sSellerSettlementChargesU08FDesc;
            sSellerSettlementChargesU08F.Text = dataLoan.sSellerSettlementChargesU08F_rep;
            sSellerSettlementChargesU09FHudline.Text = dataLoan.sSellerSettlementChargesU09FHudline_rep;
            sSellerSettlementChargesU09FDesc.Text = dataLoan.sSellerSettlementChargesU09FDesc;
            sSellerSettlementChargesU09F.Text = dataLoan.sSellerSettlementChargesU09F_rep;
            sSellerSettlementChargesU10FHudline.Text = dataLoan.sSellerSettlementChargesU10FHudline_rep;
            sSellerSettlementChargesU10FDesc.Text = dataLoan.sSellerSettlementChargesU10FDesc;
            sSellerSettlementChargesU10F.Text = dataLoan.sSellerSettlementChargesU10F_rep;

            if (dataLoan.sLT == E_sLT.FHA)  // opm 143928
            {
                Section_FHA.Visible = true;
                sFhaAddendToHud1NonBorrSrcFunds.Text = dataLoan.sFhaAddendToHud1NonBorrSrcFunds;
            }
           
        }
    }
}

