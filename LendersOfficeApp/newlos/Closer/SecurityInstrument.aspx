﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SecurityInstrument.aspx.cs" Inherits="LendersOfficeApp.newlos.Closer.SecurityInstrument" %>
<%@ Register TagPrefix="cc1" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Security Instrument</title>
    <style type="text/css">
        .StateSpecificFieldSet 
        {
            margin-top: 10px;
            padding: 5px;
        }

        .FieldLabel
        {
            display: inline-block;
            margin-top: 5px;
            padding-left: 5px;
        }

        .PercentField
        {
            margin-left: 15px;
        }

        .SectionHeader
        {
            padding: 5px;
            font-weight: bold;
	        font-size: 11px;
	        color: white ;
	        font-family: Arial, Helvetica, sans-serif;
            background-color: navy;
        }

        .ClosingLocationTable
        {
            table-layout:fixed;
            padding: 0;
            border-spacing: 0;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <div class="MainRightHeader">Security Instrument</div>

        <div class="StateSpecificFieldSet FormTableSubheader">Execution</div>
        <div class="SectionHeader">Closing Location</div>
        <table class="ClosingLocationTable">
            <colgroup>
                <col width="0px" />
                <col />
            </colgroup>
            <tr>
                <td colspan="2">
                    <asp:HiddenField runat="server" ID="sExecutionLocationSourceT_SelectedValue" />
                    <asp:RadioButton runat="server" onchange="onPopulateFromContactSelected(); return false;" GroupName="sExecutionLocationSourceT" ID="sExecutionLocationSourceT_PopulateFromContact"></asp:RadioButton>
                    <label runat="server" class="FieldLabel">Populate from Official Contact</label>
                    &nbsp;&nbsp;
                    <asp:DropDownList runat="server" onchange="onPopulateFromContactSelected(); return false;" ID="sExecutionLocationAgentT"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton runat="server" onchange="onSubjectPropertyAddressSelected(); return false;" GroupName="sExecutionLocationSourceT" ID="sExecutionLocationSourceT_SubjectPropertyAddress"></asp:RadioButton>
                    <label class="FieldLabel">Subject Property Address</label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton runat="server" onchange="onLeaveBlankSelected(); return false;" GroupName="sExecutionLocationSourceT" ID="sExecutionLocationSourceT_LeaveBlank"></asp:RadioButton>
                    <label class="FieldLabel">Leave Blank (use document vendor settings)</label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:RadioButton runat="server" onchange="onSetManuallySelected(); return false;" GroupName="sExecutionLocationSourceT" ID="sExecutionLocationSourceT_SetManually"></asp:RadioButton>
                    <label class="FieldLabel">Set Manually</label>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="FieldLabel">Address</label>
                </td>
                <td colspan="2">
                    <asp:TextBox runat="server" Width="253" ID="sExecutionLocationAddress"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">
                    <asp:TextBox runat="server" Width="150" ID="sExecutionLocationCity"></asp:TextBox>
                    <cc1:StateDropDownList runat="server" ID="sExecutionLocationState"></cc1:StateDropDownList>
                    <cc1:ZipcodeTextBox runat="server" ID="sExecutionLocationZip" onchange="onCountyChanged();"></cc1:ZipcodeTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="FieldLabel">County</label>
                </td>
                <td colspan="2">
                    <asp:DropDownList runat="server" Width="100" ID="sExecutionLocationCountyDropdown" onchange="onCountyChanged();"></asp:DropDownList>
                    <asp:HiddenField runat="server" ID="sExecutionLocationCounty" />
                </td>
            </tr>
        </table>

    <div class="StateSpecificFieldSet FormTableSubheader">State Specific Fields - Texas</div>
    <label class="FieldLabel">Security Instrument Paragraph 27</label>
    <asp:RadioButtonList runat="server" 
        ID="sTxSecurityInstrumentPar27T" 
        RepeatDirection="Vertical">
    </asp:RadioButtonList>
    <div>
    <label class="FieldLabel">Acknowledgement of Cash Advanced Against Non-Homestead Property</label>
    <div style="width: 70%"><asp:CheckBox style="float:left" runat="server" ID="sAcknowledgeCashAdvanceForNonHomestead" /> 
    <div>Borrower states that Borrower does not now and does not intend ever to reside on, use in any manner, or claim the Property secured by this Security Instrument as a business or residential homestead. </div>   
    </div></div>

    <div class="StateSpecificFieldSet FormTableSubheader">State Specific Fields - New York</div>
    <div style="width: 70%">
    <label class="FieldLabel">Borrower's Statement Regarding the Property</label>
    <asp:RadioButtonList runat="server"
        ID="sNyPropertyStatementT"
        RepeatDirection="Vertical">
        </asp:RadioButtonList>
        </div>
        
    <div class="StateSpecificFieldSet FormTableSubheader">State Specific Fields - Louisiana / Delaware</div>
    <div>
        <label class="FieldLabel">Attorney Fees (Percent)<ml:PercentTextBox runat="server" ID="sSecInstrAttorneyFeesPc" class="PercentField"></ml:PercentTextBox></label>
    </div>
    
    <div class="StateSpecificFieldSet FormTableSubheader">State Specific Fields - Maryland / North Carolina / Nebraska / Virginia / District of Columbia</div>
    <div>
        <label class="FieldLabel">Trustee Fee (Percent)<ml:PercentTextBox runat="server" ID="sTrusteeFeePc" class="PercentField"></ml:PercentTextBox></label>
    </div>
    </form>
    
    <script type="text/javascript">
        function _init() {
            if (!$('#sExecutionLocationSourceT_SetManually').is(':checked'))
            {
                toggleClosingLocationAddressEnabled(false);
            }

            if (!$('#sExecutionLocationSourceT_PopulateFromContact').is(':checked'))
            {
                toggleAgentPicker(false);
            }
        }

        function toggleClosingLocationAddressEnabled(enable) {
            $('#sExecutionLocationAddress').prop('disabled', !enable);
            $('#sExecutionLocationCity').prop('disabled', !enable);
            $('#sExecutionLocationState').prop('disabled', !enable);
            $('#sExecutionLocationZip').prop('disabled', !enable);
            $('#sExecutionLocationCountyDropdown').prop('disabled', !enable);
        }

        function toggleAgentPicker(enable) {
            $('#sExecutionLocationAgentT').prop('disabled', !enable);
        }

        function onNewExecutionLocationSourceSelected(radioSelector, enableAddress, enableAgentPicker, selectedValue) {
            if (!$(radioSelector).is(':checked'))
            {
                return;
            }

            toggleClosingLocationAddressEnabled(enableAddress);
            toggleAgentPicker(enableAgentPicker);

            $('#sExecutionLocationSourceT_SelectedValue').val(selectedValue);

            refreshCalculation();

            // Rebind the county dropdown.
            $('#sExecutionLocationState').change();
            var currentCounty = $('#sExecutionLocationCounty').val();
            $('#sExecutionLocationCountyDropdown').val(currentCounty);
        }

        function onPopulateFromContactSelected() {
            var radioSelector = '#sExecutionLocationSourceT_PopulateFromContact';
            var selectedValue = <%= AspxTools.JsString(E_sExecutionLocationSourceT.PopulateFromContact)%>;
            onNewExecutionLocationSourceSelected(radioSelector, false, true, selectedValue);
        }

        function onSubjectPropertyAddressSelected() {
            var radioSelector = '#sExecutionLocationSourceT_SubjectPropertyAddress';
            var selectedValue = <%= AspxTools.JsString(E_sExecutionLocationSourceT.SubjectPropertyAddress)%>;
            onNewExecutionLocationSourceSelected(radioSelector, false, false, selectedValue);
        }

        function onLeaveBlankSelected() {
            var radioSelector = '#sExecutionLocationSourceT_LeaveBlank';
            var selectedValue = <%= AspxTools.JsString(E_sExecutionLocationSourceT.LeaveBlank)%>;
            onNewExecutionLocationSourceSelected(radioSelector, false, false, selectedValue);
        }

        function onSetManuallySelected() {
            var radioSelector = '#sExecutionLocationSourceT_SetManually';
            var selectedValue = <%= AspxTools.JsString(E_sExecutionLocationSourceT.SetManually)%>;
            onNewExecutionLocationSourceSelected(radioSelector, true, false, selectedValue);
        }

        function onCountyChanged() {
            var selectedValue = $('#sExecutionLocationCountyDropdown').val();
            $('#sExecutionLocationCounty').val(selectedValue);
        }
    </script>
</body>
</html>
