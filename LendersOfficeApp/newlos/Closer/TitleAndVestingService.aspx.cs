﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using DataAccess.Trust;
using System.IO;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Closer
{
    public class TitleAndVestingServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(TitleAndVestingServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sPrelimRprtOd_rep = GetString("sPrelimRprtOd");
            dataLoan.sPrelimRprtDueD_rep = GetString("sPrelimRprtDueD");
            dataLoan.sPrelimRprtDocumentD_rep = GetString("sPrelimRprtDocumentD");
            dataLoan.sPrelimRprtRd_rep = GetString("sPrelimRprtRd");
            dataLoan.sPrelimRprtN = GetString("sPrelimRprtN");
            dataLoan.sAssessorsParcelId = GetString("sAssessorsParcelId");
            dataLoan.sPropertyTaxMessageDescription = GetString("sPropertyTaxMessageDescription");
            dataLoan.sRequiredEndorsements = GetString("sRequiredEndorsements");
            dataLoan.sTitleReportItemsDescription = GetString("sTitleReportItemsDescription");
            dataLoan.GetAppData(0).aManner = GetString("aManner");

            string repeaterId = GetString("RelationshipsId");
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                string rowName = string.Format("{0}_ctl0{1}_", repeaterId, i * 2);
                CAppData currentApp = dataLoan.GetAppData(i);
                if (!currentApp.aBTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly))
                {
                    currentApp.aBRelationshipTitleT = (E_aRelationshipTitleT)GetInt(rowName + "aBRelationshipTitleT");
                    currentApp.aBRelationshipTitleOtherDesc = GetString(rowName + "aBRelationshipTitleOtherDesc");
                }
                if (!currentApp.aCTypeT.EqualsOneOf(E_aTypeT.NonTitleSpouse, E_aTypeT.CurrentTitleOnly))
                {
                    currentApp.aCRelationshipTitleT = (E_aRelationshipTitleT)GetInt(rowName + "aCRelationshipTitleT", Convert.ToInt32(E_aRelationshipTitleT.LeaveBlank));
                    currentApp.aCRelationshipTitleOtherDesc = GetString(rowName + "aCRelationshipTitleOtherDesc", "");
                }

                currentApp.aBAliases = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("aBAliases" + i));
                currentApp.aCAliases = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("aCAliases" + i));
                currentApp.aBPowerOfAttorneyNm = GetString("aBPowerOfAttorneyNm" + i);
                currentApp.aCPowerOfAttorneyNm = GetString("aCPowerOfAttorneyNm" + i);

            }
            dataLoan.sVestingToReadLckd = GetBool("sVestingToReadLckd");
            dataLoan.sVestingToRead = GetString("sVestingToRead");

            dataLoan.sTrustName = GetString("sTrustName");
            dataLoan.sTrustAgreementD_rep = GetString("sTrustAgreementD");
            dataLoan.sTrustState = GetString("sTrustState");
            dataLoan.sTrustId = GetString("sTrustId");
            dataLoan.sTrustForBenefitOf = GetString("sTrustForBenefitOf");
            dataLoan.sTrustClassificationT = this.GetEnum<TrustClassificationT>("sTrustClassificationT");

            var jsonSerializedTitleOnlyBorrower = GetString("TitleOnlyBorrowerJSON");
            var titleBorrowers = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<TitleBorrower>>(jsonSerializedTitleOnlyBorrower);

            List<TitleBorrower> toRemvoe = new List<TitleBorrower>();
            titleBorrowers.ForEach(p => {
                if (p.Id == Guid.Empty)
                {
                    throw CBaseException.GenericException("Something is blanking out the ID.");
                }
                if (string.IsNullOrEmpty(p.Email) && string.IsNullOrEmpty(p.FirstNm) && string.IsNullOrEmpty(p.MidNm) && string.IsNullOrEmpty(p.SSN))
                {
                    toRemvoe.Add(p);
                }
            });

            toRemvoe.ForEach(p => titleBorrowers.Remove(p));
            dataLoan.sTitleBorrowers = titleBorrowers; 

            var jsonSerializedCollection = GetString("TrustInfoHolder");
            TrustCollection tvc = ObsoleteSerializationHelper.JavascriptJsonDeserializer<TrustCollection>(jsonSerializedCollection);
            if(tvc != null) dataLoan.sTrustCollection = tvc;
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sPrelimRprtOd", dataLoan.sPrelimRprtOd);
            SetResult("sPrelimRprtDueD", dataLoan.sPrelimRprtDueD);
            SetResult("sPrelimRprtDocumentD", dataLoan.sPrelimRprtDocumentD_rep);
            SetResult("sPrelimRprtRd", dataLoan.sPrelimRprtRd);
            SetResult("sPrelimRprtN", dataLoan.sPrelimRprtN);
            SetResult("sAssessorsParcelId", dataLoan.sAssessorsParcelId);
            SetResult("sPropertyTaxMessageDescription", dataLoan.sPropertyTaxMessageDescription);
            SetResult("sRequiredEndorsements", dataLoan.sRequiredEndorsements);
            SetResult("sTitleReportItemsDescription", dataLoan.sTitleReportItemsDescription);
            SetResult("aManner", dataLoan.GetAppData(0).aManner);
            SetResult("TitleOnlyBorrowerJSON", ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sTitleBorrowers));

            string repeaterId = GetString("RelationshipsId");
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                string rowName = string.Format("{0}__ctl{1}_", repeaterId, i * 2);
                CAppData currentApp = dataLoan.GetAppData(i);

                SetResult(rowName + "aBRelationshipTitleOtherDesc", currentApp.aBRelationshipTitleOtherDesc);
                SetResult(rowName + "aCRelationshipTitleOtherDesc", currentApp.aCRelationshipTitleOtherDesc);

                SetResult(rowName + "aBRelationshipTitleT", currentApp.aBRelationshipTitleT);
                SetResult(rowName + "aCRelationshipTitleT", currentApp.aCRelationshipTitleT);
            }

            SetResult("sVestingToReadLckd", dataLoan.sVestingToReadLckd);
            SetResult("sVestingToRead", dataLoan.sVestingToRead);

            SetResult("sTrustName", dataLoan.sTrustName);
            SetResult("sTrustAgreementD", dataLoan.sTrustAgreementD_rep);
            SetResult("sTrustState", dataLoan.sTrustState);
            SetResult("sTrustId", dataLoan.sTrustId);
            SetResult("sTrustForBenefitOf", dataLoan.sTrustForBenefitOf);

            SetResult("TrustInfoHolder", ObsoleteSerializationHelper.JavascriptJsonSerialize(dataLoan.sTrustCollection));
        }
    }
    public partial class TitleAndVestingService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new TitleAndVestingServiceItem());
        }
    }


}
