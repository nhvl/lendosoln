﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Conversions.FloodOrder.Request;
using LendersOffice.Security;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos.Closer
{
    public class FloodCertificationServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FloodCertificationServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {            
            dataLoan.sFloodHazardCommunityDesc = GetString("sFloodHazardCommunityDesc");
            dataLoan.sFloodCertificationCommunityNum = GetString("sFloodCertificationCommunityNum");

            dataLoan.sFloodCertificationMapNum = GetString("sFloodCertificationMapNum");
            dataLoan.sFloodCertificationPanelNums = GetString("sFloodCertificationPanelNums");
            dataLoan.sFloodCertificationPanelSuffix = GetString("sFloodCertificationPanelSuffix");
            dataLoan.sNfipFloodZoneId = GetString("sNfipFloodZoneId");
            dataLoan.sFloodCertificationMapD_rep = GetString("sFloodCertificationMapD");
            dataLoan.sFloodCertificationLOMChangedD_rep = GetString("sFloodCertificationLOMChangedD");
            dataLoan.sFloodCertId = GetString("sFloodCertId");
            dataLoan.sFloodCertificationIsNoMap = GetBool("sFloodCertificationIsNoMap");

            if (GetBool("sIsAvailable"))
            {
                if (GetBool("sEmergencyProgramOfNFIP"))
                    dataLoan.sFloodCertificationParticipationStatus = "E";
                else if (GetBool("sProbation"))
                    dataLoan.sFloodCertificationParticipationStatus = "P";
                else
                    dataLoan.sFloodCertificationParticipationStatus = "R";
            }
            else
            {
                if (GetBool("sSuspended"))
                    dataLoan.sFloodCertificationParticipationStatus = "S";
                else
                    dataLoan.sFloodCertificationParticipationStatus = "N";
            }
            dataLoan.sFloodCertificationIsCBRAorOPA = GetBool("sFloodCertificationIsCBRAorOPA");
            dataLoan.sFloodCertificationDesignationD_rep = GetString("sFloodCertificationDesignationD");

            dataLoan.sFloodCertificationIsInSpecialArea = GetBool("sFloodCertificationIsInSpecialArea");

            dataLoan.sFloodCertificationPreparerT = (E_FloodCertificationPreparerT)GetInt("sFloodCertificationPreparerT");
            if (dataLoan.sFloodCertificationPreparerT == E_FloodCertificationPreparerT.Other)
            {
                dataLoan.sFloodCertificationPreparerOtherName = GetString("sFloodCertificationPreparerOtherName");
            }
            
            try
            {
                DateTime determinationD = DateTime.Parse(GetString("sFloodCertificationDeterminationD"));

                int hours;
                int minutes;

                try
                {
                    hours = GetInt("sHourOfDetermination");
                    minutes = GetInt("sMinuteOfDetermination");


                    if (hours < 1 || hours > 11)
                        hours = 0;
                    if (minutes < 0 || minutes > 59)
                        minutes = 0;
                }
                catch (GenericUserErrorMessageException)
                {
                    hours = 0;
                    minutes = 0;
                }

                if (GetString("sAmPm") == "PM")
                    hours += 12;

                dataLoan.sFloodCertificationDeterminationD = CDateTime.Create(determinationD.AddMinutes(hours*60 + minutes));
            }
            catch (FormatException)
            {
                dataLoan.sFloodCertificationDeterminationD_rep = "";
            }

            dataLoan.sFloodCertificationDeterminationDTimeZoneT = (E_sTimeZoneT)GetInt("sFloodCertificationDeterminationDTimeZoneT");
            dataLoan.sFloodCertId = GetString("sFloodCertId");
            dataLoan.sFloodCertificationIsLOLUpgraded = GetBool("sFloodCertificationIsLOLUpgraded");
            dataLoan.sFloodCertificationLOLUpgradeD_rep = GetString("sFloodCertificationLOLUpgradeD");

            dataLoan.sFloodCertificationCancellationReceivedD_rep = GetString("sFloodCertificationCancellationReceivedD");
            dataLoan.sFloodCertificationCancellationOrderedD_rep = GetString("sFloodCertificationCancellationOrderedD");

            dataLoan.sLomaLomrT = (E_LomaLomrT)GetInt("sLomaLomrT");
            dataLoan.sNfipIsPartialZone = GetBool("sNfipIsPartialZone");
            dataLoan.sNfipMappingCompany = GetString("sNfipMappingCompany");
            dataLoan.sFfiaDateCommunityEnteredProgramD_rep = GetString("sFfiaDateCommunityEnteredProgramD");

        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sFloodHazardCommunityDesc", dataLoan.sFloodHazardCommunityDesc);
            SetResult("sFloodCertificationCommunityNum", dataLoan.sFloodCertificationCommunityNum);

            SetResult("sFloodCertificationMapNum", dataLoan.sFloodCertificationMapNum);
            SetResult("sFloodCertificationPanelNums", dataLoan.sFloodCertificationPanelNums);
            SetResult("sFloodCertificationPanelSuffix", dataLoan.sFloodCertificationPanelSuffix);
            SetResult("sNfipFloodZoneId", dataLoan.sNfipFloodZoneId);
            SetResult("sFloodCertificationMapD", dataLoan.sFloodCertificationMapD_rep);
            SetResult("sFloodCertificationLOMChangedD", dataLoan.sFloodCertificationLOMChangedD_rep);
            SetResult("sFloodCertificationIsNoMap", dataLoan.sFloodCertificationIsNoMap);
            
            if ((dataLoan.sFloodCertificationParticipationStatus == "N") || (dataLoan.sFloodCertificationParticipationStatus == "S"))
            {
                SetResult("sIsAvailable", false);
                SetResult("sSuspended", (dataLoan.sFloodCertificationParticipationStatus == "S"));
            }
            else
            {
                SetResult("sIsAvailable", true);
                SetResult("sEmergencyProgramOfNFIP", (dataLoan.sFloodCertificationParticipationStatus == "E"));
                SetResult("sRegularProgram", (dataLoan.sFloodCertificationParticipationStatus == "R"));
                SetResult("sProbation", (dataLoan.sFloodCertificationParticipationStatus == "P"));
            }
            SetResult("sFloodCertificationDesignationD", dataLoan.sFloodCertificationDesignationD_rep);
            SetResult("sFloodCertificationIsCBRAorOPA", dataLoan.sFloodCertificationIsCBRAorOPA);

            SetResult("sFloodCertificationIsInSpecialArea", dataLoan.sFloodCertificationIsInSpecialArea);

            SetResult("sFloodCertificationPreparerT", dataLoan.sFloodCertificationPreparerT);
            SetResult("sFloodCertificationPreparerOtherName", dataLoan.sFloodCertificationPreparerOtherName);

            SetResult("sFloodCertificationDeterminationD", dataLoan.sFloodCertificationDeterminationD_rep);
                
            if (dataLoan.sFloodCertificationDeterminationD != null && dataLoan.sFloodCertificationDeterminationD.IsValid)
            { 
                DateTime dt = dataLoan.sFloodCertificationDeterminationD.DateTimeForComputationWithTime;
                SetResult("sHourOfDetermination", "" + (dt.Hour % 12 + (dt.Hour % 12 == 0 ? 12 : 0)));
                SetResult("sMinuteOfDetermination", dt.Minute.ToString("00"));
                SetResult("sAmPm", (dt.Hour < 12 ? "AM" : "PM"));
            }
            else
            {
                SetResult("sHourOfDetermination", "");
                SetResult("sMinuteOfDetermination", "");
                SetResult("sAmPm", "AM");
            }

            SetResult("sFloodCertificationDeterminationDTimeZoneT", dataLoan.sFloodCertificationDeterminationDTimeZoneT);
            SetResult("sFloodCertId", dataLoan.sFloodCertId);
            SetResult("sFloodCertificationIsLOLUpgraded", dataLoan.sFloodCertificationIsLOLUpgraded);
            SetResult("sFloodCertificationLOLUpgradeD", dataLoan.sFloodCertificationLOLUpgradeD_rep);

            SetResult("sFloodCertificationCancellationReceivedD", dataLoan.sFloodCertificationCancellationReceivedD_rep);
            SetResult("sFloodCertificationCancellationOrderedD", dataLoan.sFloodCertificationCancellationOrderedD_rep);

            SetResult("sLomaLomrT", dataLoan.sLomaLomrT);
            SetResult("sNfipIsPartialZone", dataLoan.sNfipIsPartialZone);
            SetResult("sNfipMappingCompany", dataLoan.sNfipMappingCompany);
            SetResult("sFfiaDateCommunityEnteredProgramD", dataLoan.sFfiaDateCommunityEnteredProgramD_rep);
        }
    }
    
    public class FloodOrderServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FloodOrderServiceItem));
        }
        
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            switch(GetString("sFloodOrderType"))
            {
                case "NewReport":
                    dataLoan.sFloodOrderType = E_FloodActionType.Original;
                    break;
                case "Reissue":
                    dataLoan.sFloodOrderType = E_FloodActionType.Reissue;
                    break;
                case "Upgrade":
                    dataLoan.sFloodOrderType = E_FloodActionType.Upgrade;
                    break;
                case "Query":
                    dataLoan.sFloodOrderType = E_FloodActionType.StatusQuery;
                    break;
                default:
                    dataLoan.sFloodOrderType = E_FloodActionType.None;
                    break;
            }

            dataLoan.sFloodCertId = GetString("sFloodCertId");
            dataLoan.sFloodOrderQueryID = GetString("sFloodOrderQueryID");

            dataLoan.sIsFloodOrderLOL = GetBool("sIsFloodOrderLOL");
            dataLoan.sIsFloodOrderHMDA = GetBool("sIsFloodOrderHMDA");
            dataLoan.sIsFloodOrderRushOrder = GetBool("sIsFloodOrderRushOrder");
            dataLoan.sIsFloodOrderPayByCreditCard = GetBool("sIsFloodOrderPayByCreditCard");

            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBLastNm = GetString("aBLastNm");

            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sSpLegalDesc = GetString("sSpLegalDesc");
            dataLoan.sAssessorsParcelId = GetString("sAssessorsParcelId");
        }
        
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sFloodOrderType", FloodOrderType(dataLoan.sFloodOrderType));

            SetResult("sFloodCertId", dataLoan.sFloodCertId);
            SetResult("sFloodOrderQueryID", dataLoan.sFloodOrderQueryID);

            SetResult("sIsFloodOrderLOL", dataLoan.sIsFloodOrderLOL);
            SetResult("sIsFloodOrderHMDA", dataLoan.sIsFloodOrderHMDA);
            SetResult("sIsFloodOrderRushOrder", dataLoan.sIsFloodOrderRushOrder);
            SetResult("sIsFloodOrderPayByCreditCard", dataLoan.sIsFloodOrderPayByCreditCard);

            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBLastNm = GetString("aBLastNm");

            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip.TrimWhitespaceAndBOM());
            SetResult("sSpLegalDesc", dataLoan.sSpLegalDesc);
            SetResult("sAssessorsParcelId", dataLoan.sAssessorsParcelId);
        }

        private string FloodOrderType(E_FloodActionType sFloodOrderType)
        {
            string sType;
            switch(sFloodOrderType)
            {
                case E_FloodActionType.None:
                case E_FloodActionType.Original:
                    sType = "NewReport";
                    break;
                case E_FloodActionType.Reissue:
                    sType = "Reissue";
                    break;
                case E_FloodActionType.StatusQuery:
                    sType = "Query";
                    break;
                case E_FloodActionType.Upgrade:
                    sType = "Upgrade";
                    break;
                default:
                    throw new UnhandledEnumException(sFloodOrderType);
            }
            return sType;
        }
    }
    
    public partial class FloodCertificationService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("FloodCertDataTab", new FloodCertificationServiceItem());
            AddBackgroundItem("FloodOrderTab", new FloodOrderServiceItem());
        }
    }
}
