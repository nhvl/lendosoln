﻿<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FloodOrderTab.ascx.cs" Inherits="LendersOfficeApp.newlos.Closer.FloodOrderTab" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Conversions.FloodOrder.Response" %>
<%@ Import Namespace="DataAccess" %>

<div id="FloodOrderTab">
	<div class="MainRightHeader" >
		Flood Certification Order
	</div>
	<table cellSpacing="0" cellPadding="0" border="0">
        <tr>
            <td>
            <table id="FloodOrderDetails" class="InsetBorder" cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding: 5px">
                <tr>
	                <td class="FormTableSubheader">Flood Order Details</td>
	            </tr>
	            <tr>
	                <td>
	                <table id="OrderTypeMenu" class="OptionsMenu" >
	                    <tr>
	                        <td class="FieldLabel">
	                        <ml:EncodedLabel ID="OrderTypeLabel" runat="server" >Order Type:</ml:EncodedLabel> 
	                        </td>
	                    </tr>
	                    <tr>
		                    <td>
			                <asp:radiobutton id="NewReport" onclick="f_refreshUI();" runat="server" GroupName="sFloodOrderType" Text="New Report" />
		                    </td>
		                </tr>
		                <tr>
		                    <td>
			                <asp:radiobutton id="Reissue" onclick="f_refreshUI();" runat="server" GroupName="sFloodOrderType" Text="Re-issue Existing" />
		                    </td>
		                </tr>
		                <tr>
		                    <td>
			                <asp:radiobutton id="Upgrade" onclick="f_refreshUI();" runat="server" GroupName="sFloodOrderType" Text="Upgrade Existing" />
		                    </td>
		                </tr>
		                <tr>
		                    <td>
			                <asp:radiobutton id="Query" onclick="f_refreshUI();" runat="server" GroupName="sFloodOrderType" Text="Query for Manual Report" />
		                    </td>
		                </tr>
		            </table>
	                </td>
	            </tr>
		        <tr>
			        <td>
			        <table id="FloodCertIDTable" style="DISPLAY:none;">
			            <tr>
			                <td class="FieldLabel">
			                <ml:EncodedLabel ID="FloodCertNumLabel" runat="server" AssociatedControlID="sFloodCertId" >Flood Cert Number</ml:EncodedLabel>
			                </td>
			            </tr>
		                <tr>
			                <td noWrap>
			                <asp:textbox id="sFloodCertId" runat="server" enableviewstate="False" Width="97px" maxlength="21" onchange="f_RecoverSubmitButton();"></asp:textbox>
			                <asp:RequiredFieldValidator ID="FloodCertIDTableValidator" runat="server" ControlToValidate="sFloodCertId" Display="Dynamic" Enabled="false">
                                <img runat="server" src="../../images/error_icon.gif">
			                </asp:RequiredFieldValidator>
			                </td>
		                </tr>
		            </table>
		            <table id="QueryIDTable" style="DISPLAY:none;">
		                <tr>
			                <td class="FieldLabel">
			                <ml:EncodedLabel ID="QueryIDLabel" runat="server" AssociatedControlID="sFloodOrderQueryID" >Query ID</ml:EncodedLabel>
			                </td>
		                </tr>
		                <tr>
			                <td noWrap>
			                <asp:textbox id="sFloodOrderQueryID" runat="server" enableviewstate="False" Width="97px" maxlength="21" onchange="f_RecoverSubmitButton();"></asp:textbox>
			                <asp:RequiredFieldValidator ID="QueryIDTableValidator" runat="server" ControlToValidate="sFloodOrderQueryID" Display="Dynamic" Enabled="false">
                                <img runat="server" src="../../images/error_icon.gif">
			                </asp:RequiredFieldValidator>
			                </td>
			            </tr>
			        </table>
			        </td>
		        </tr>
		        <tr>
	                <td>
	                <table id="OrderAddonsMenu" class="OptionsMenu" style="margin: 0; padding: 5px 0 0 0">
	                    <tr>
	                        <td class="FieldLabel">
	                        <ml:EncodedLabel ID="OrderAddonsLabel" runat="server" >Options:</ml:EncodedLabel> 
	                        </td>
	                    </tr>
		                <tr ID="showLifeOfLoanOption">
			                <td>
				            <asp:checkbox id="sIsFloodOrderLOL" runat="server" Text="Life of Loan" />
			                </td>
		                </tr>
		                <tr ID="showHMDAOption">
			                <td>
				            <asp:checkbox id="sIsFloodOrderHMDA" runat="server" Text="HMDA" />
			                </td>
		                </tr>
		                <tr ID="showRushOrderOption">
			                <td>
				            <asp:checkbox id="sIsFloodOrderRushOrder" runat="server" Text="Rush Order" />
			                </td>
		                </tr>
		                <tr ID="showPayByCreditCardOption" style="DISPLAY:none;"><!--Placeholder for CC pmt feature-->
			                <td>
				            <asp:checkbox id="sIsFloodOrderPayByCreditCard" runat="server" Text="Pay by credit card" />
			                </td>
		                </tr>
	                </table>
	                </td>
	            </tr>
            </table>
            <table id="LoginCredentials" class="InsetBorder" cellSpacing="0" cellPadding="0" width="100%" border="0" >
	            <tr>
	                <td class="FormTableSubheader" colspan="2">Login Credentials</td>
	            </tr>
	            <tr>
	                <td class="FieldLabel">
	                <ml:EncodedLabel runat="server" ID="LoginLabel" AssociatedControlID="m_loginName">Login</ml:EncodedLabel>
	                </td>
	                <td noWrap>
		            <asp:textbox id="m_loginName" runat="server" Width="137px" onchange="f_RecoverSubmitButton();"></asp:textbox>
		            <asp:requiredfieldvalidator id="LoginNameValidator" runat="server" ControlToValidate="m_loginName" Display="Dynamic">
                        <img runat="server" src="../../images/error_icon.gif">
		            </asp:requiredfieldvalidator>
		            </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                    <ml:EncodedLabel ID="PasswordLabel" runat="server" AssociatedControlID="m_password">Password</ml:EncodedLabel>
                    </td>
                    <td noWrap>
		            <asp:textbox id="m_password" runat="server" Width="137px" textmode="Password" onchange="f_RecoverSubmitButton();"></asp:textbox>
		            <asp:requiredfieldvalidator id="PasswordValidator" runat="server" ControlToValidate="m_password" Display="Dynamic">
                        <img runat="server" src="../../images/error_icon.gif">
		            </asp:requiredfieldvalidator>
		            </td>
	            </tr>
	            <tr>
	                <td colspan="2">
	                <asp:checkbox id="m_rememberLogin" runat="server" ></asp:checkbox>
	                <ml:EncodedLabel class="FieldLabel" runat="server" ID="RememberLabel" AssociatedControlID="m_rememberLogin">Remember Login Credentials</ml:EncodedLabel>
	                </td>
	            </tr>
	        </table>
            </td>
            <td valign="top">
	        <table id="LoanInformation" class="InsetBorder" cellSpacing="0" cellPadding="0" border="0" style="padding: 5px">
	            <tr>
	                <td class="FormTableSubheader">Loan Information</td>
	            </tr>
	            <tr>
	                <td>
			        <table id="LoanInfoTable" cellSpacing="0" cellPadding="0" border="0">
				        <tr>
					        <td class="FieldLabel"><ml:EncodedLabel ID="FnameLabel" runat="server" AssociatedControlID="aBFirstNm" >Borrower First Name</ml:EncodedLabel></td>
					        <td noWrap>
					        <asp:textbox id="aBFirstNm" runat="server" enableviewstate="False" Width="127px" maxlength="50" onchange="f_RecoverSubmitButton();">
					        </asp:textbox>
                                <asp:requiredfieldvalidator id="aBFirstNmValidator" runat="server" ControlToValidate="aBFirstNm" Display="Dynamic">
                                    <img runat="server" src="../../images/error_icon.gif">
                                </asp:requiredfieldvalidator>&nbsp;
					        </td>
					    </tr>
				        <tr>
					        <td class="FieldLabel"><ml:EncodedLabel ID="LnameLabel" runat="server" AssociatedControlID="aBLastNm" >Last Name</ml:EncodedLabel></td>
					        <td noWrap>
					        <asp:textbox id="aBLastNm" runat="server" enableviewstate="False" Width="127px" maxlength="50" onchange="f_RecoverSubmitButton();"></asp:textbox>
					        <asp:requiredfieldvalidator id="aBLastNmValidator" runat="server" ControlToValidate="aBLastNm" Display="Dynamic">
                                <img runat="server" src="../../images/error_icon.gif">
					        </asp:requiredfieldvalidator>&nbsp;
					        </td>
				        </tr>
				        <tr>
					        <td class="FieldLabel" noWrap><ml:EncodedLabel ID="SubjPropStreetLabel" runat="server" AssociatedControlID="sSpAddr" >Subject Property Street</ml:EncodedLabel></td>
					        <td noWrap>
					        <asp:textbox id="sSpAddr" runat="server" Width="359px" MaxLength="60" onchange="f_RecoverSubmitButton();"></asp:textbox>
					        <asp:requiredfieldvalidator id="sSpAddrRequiredValidator" runat="server" ControlToValidate="sSpAddr">
                                <img runat="server" src="../../images/error_icon.gif">
					        </asp:requiredfieldvalidator>
					        </td>
				        </tr>
				        <tr>
					        <td class="FieldLabel"><ml:EncodedLabel ID="CityLabel" runat="server" AssociatedControlID="sSpCity" >City / State / Zip</ml:EncodedLabel></td>
					        <td noWrap>
					        <asp:textbox id="sSpCity" runat="server" enableviewstate="False" Width="258px" MaxLength="72" onchange="f_RecoverSubmitButton();"></asp:textbox>
					        <asp:requiredfieldvalidator id="sSpCityValidator" runat="server" Display="Dynamic" ControlToValidate="sSpCity">
                                <img runat="server" src="../../images/error_icon.gif">
					        </asp:requiredfieldvalidator>&nbsp;
					        <ml:statedropdownlist id="sSpState" runat="server" onblur="ValidateStateDD();" enableviewstate="False"></ml:statedropdownlist>
					        <asp:customvalidator id="sSpStateValidator" runat="server" Display="Dynamic" ClientValidationFunction="ValidateStateDropDown">
                                <img runat="server" src="../../images/error_icon.gif">
					        </asp:customvalidator>&nbsp;
					        <ml:zipcodetextbox id="sSpZip" runat="server" onchange="ValidateCityState();" CssClass="mask" preset="zipcode" width="50"></ml:zipcodetextbox>
					        <asp:requiredfieldvalidator id="sSpZipValidator" runat="server" Display="Dynamic" ControlToValidate="sSpZip">
                                <img runat="server" src="../../images/error_icon.gif">
					        </asp:requiredfieldvalidator>
					        </td>
				        </tr>
				        <tr>
                            <td class="FieldLabel" nowrap>Legal Description</td>
                            <td noWrap>
                            <asp:TextBox ID="sSpLegalDesc" Width="401px" runat="server" TextMode="MultiLine" Columns="20" Rows="3" Height="92px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" noWrap>Assessors Parcel ID</td>
                            <td noWrap>
                            <asp:TextBox ID="sAssessorsParcelId" Width="280px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
			        </table>
	                </td>
	            </tr>
	        </table>             
            </td>
        </tr>
        <tr>
            <td>
	        <ml:NoDoubleClickButton ID="m_bSubmit" runat="server" Text="Submit Flood Order"  OnClientClick="return f_onSubmitClick();" OnClick="OnSubmitClick"></ml:NoDoubleClickButton>
            </td>
        </tr>
	</table>
</div>
<div id="IntegrationDisabledMsg" style="DISPLAY:none; font-size: 12px"><font color="red"><br><b>&nbsp;&nbsp;The flood integration is disabled.</font><br>&nbsp;&nbsp;Contact your LendingQB system administrator to enable, or click the Flood Certification Data tab to fill out the data manually.</b></div>
<div id="PageInvalidMsg" style="DISPLAY:none; font-size: 12px"><font color="red"><br><b>&nbsp;&nbsp;Please fill out all of the required fields above.</font></b></div>
<div id="WaitingPanel" style="display:none">
    <div>
        <span style="font-weight: bold; font-size: 12px">Please wait ...</span> <br /><br /><img id="WaitImage" src="~/images/status.gif" runat="server" />
    </div>
</div>
<div id="DownloadProgress" style="display: none; background-color: Gray; position:absolute"> 
    <table>
        <tbody>
            <tr>
                <td>
                    Please wait while we prepare your document
                </td>
            </tr>
            <tr>
                <td>
                    <img src="../../images/status.gif" alt="Loading" />
                </td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    function _init(event)
	{
		<% if (! IsIntegrationEnabled) { %>   

			document.getElementById("FloodOrderTab").style.display = "none";
			document.getElementById("IntegrationDisabledMsg").style.display="";
			return;

        <% } %>

		f_refreshUI();
		document.getElementById('<%= AspxTools.ClientId(sSpStateValidator)%>').enabled = true;
		
		<% if (IsReadOnly) { %> 
            document.getElementById('<%= AspxTools.ClientId(m_bSubmit)%>').disabled = true;
        <% } %>
		
		<% if(FloodStatus == E_FloodStatus.Completed) { %>
		    if(confirm(<%= AspxTools.JsString(UserMessage) %>))
		    {
		        View(event);
		    }
		<% } else if(FloodStatus == E_FloodStatus.Manual) { %>
                alert(<%= AspxTools.JsString(UserMessage) %>);
		    <% } else if(FloodStatus == E_FloodStatus.ERROR) { %>
                alert(<%= AspxTools.JsString(UserMessage) %>);
		    <% } %>
    }
    
    function fnStartInit(event)
    {
            Modal.Hide();
            progressingDownload = false;
    }
    
    function f_onSubmitClick() 
    {
        if (isDirty()) {
            if (typeof (saveMe) == 'function' || typeof (saveMe) == 'object') {
                saveMe(false);
            }
        }

        if(ValidatePage())
        {
            document.getElementById('WaitingPanel').style.display = 'block';
        }
        return true;
    }
    
    function f_RecoverSubmitButton()
    {
        if(document.getElementById('<%= AspxTools.ClientId(m_bSubmit)%>').disabled == true)
            ValidatePage();
    }
    
    function f_refreshUI() 
    {
        if(document.getElementById('<%= AspxTools.ClientId(NewReport)%>').checked)
        {
            ToggleAddOnMenu(true);
            ToggleFloodCertID(false);
            ToggleQueryID(false);
        }
        else if(document.getElementById('<%= AspxTools.ClientId(Reissue)%>').checked)
        {
            ToggleAddOnMenu(false);
            ToggleFloodCertID(true);
            ToggleQueryID(false);
        }
        else if(document.getElementById('<%= AspxTools.ClientId(Upgrade)%>').checked)
        {
            ToggleAddOnMenuOptions(true, true);
            ToggleFloodCertID(true);
            ToggleQueryID(false);
        }
        else if(document.getElementById('<%= AspxTools.ClientId(Query)%>').checked)
        {
            ToggleAddOnMenu(false);
            ToggleFloodCertID(false);
            ToggleQueryID(true);
        }
        
        f_RecoverSubmitButton();
    }
    
    function ToggleAddOnMenu(bDisplay)
    {
        ToggleAddOnMenuOptions(bDisplay, false);
    }
    
    function ToggleAddOnMenuOptions(bDisplay, bEnableOnlyLOL)
    {
        var display = (bDisplay) ? "" : "none";
        var displayHMDAandRush = (bEnableOnlyLOL) ? "none" : "";
        
        document.getElementById("OrderAddonsMenu").style.display = display;
        if(bEnableOnlyLOL)
        {
            document.getElementById('<%= AspxTools.ClientId(sIsFloodOrderLOL)%>').checked = true;
        }
        document.getElementById('<%= AspxTools.ClientId(sIsFloodOrderLOL)%>').disabled = bEnableOnlyLOL;
        document.getElementById("showHMDAOption").style.display = displayHMDAandRush;
        document.getElementById("showRushOrderOption").style.display = displayHMDAandRush;
    }
    
    function ToggleFloodCertID(bDisplay)
    {
        ToggleOrderID(bDisplay, 0);
    }
    function ToggleQueryID(bDisplay)
    {
        ToggleOrderID(bDisplay, 1);
    }
    
    function ToggleOrderID(bDisplay, iOrderIDType)
    {
        var display = (bDisplay) ? "" : "none";
        
        switch(iOrderIDType)
        {
            case 0:
                document.getElementById("FloodCertIDTable").style.display = display;
                document.getElementById('<%= AspxTools.ClientId(FloodCertIDTableValidator)%>').enabled = bDisplay;
                break;
            case 1:
                document.getElementById("QueryIDTable").style.display = display;
                document.getElementById('<%= AspxTools.ClientId(QueryIDTableValidator)%>').enabled = bDisplay;
                break;
            default:
                break;    
        }
    }
    
    function ValidateCityState() {
        ValidateStateDD();
        var cityValidator = document.getElementById('<%= AspxTools.ClientId(sSpCityValidator)%>');
        if (cityValidator.enabled == true) {
            if (typeof (ValidatorValidate) == 'function')
                ValidatorValidate(cityValidator);
        }
    }

    function ValidateStateDD() {
        var stateValidator = document.getElementById('<%= AspxTools.ClientId(sSpStateValidator)%>');
        if (stateValidator.enabled == true) {
            if (typeof (ValidatorValidate) == 'function')
                ValidatorValidate(stateValidator);
        }
        f_RecoverSubmitButton();
    }

    function ValidateStateDropDown(source, args) {
        args.IsValid = document.getElementById('<%= AspxTools.ClientId(sSpState)%>').selectedIndex != 0;
    }
    
    function ValidatePage() {
        Page_ClientValidate();
        if(Page_IsValid)
        {
            document.getElementById("PageInvalidMsg").style.display = "none";
            document.getElementById('<%= AspxTools.ClientId(m_bSubmit)%>').disabled = false;
        }
        else
        {
            document.getElementById("PageInvalidMsg").style.display = "";
            document.getElementById('<%= AspxTools.ClientId(m_bSubmit)%>').disabled = true;
        }
        return Page_IsValid;
    }
    
    function View(event) {
        Modal.ShowPopup('DownloadProgress', null, event);
        var url = <%= AspxTools.SafeUrl(Tools.GetEDocsLink(Page.ResolveUrl("~/newlos/ElectronicDocs/ViewEdocPdf.aspx"))) %> + '?docid=' + <%= AspxTools.JsString(EDocID) %>;
        fetchFileAsync(url, 'application/pdf', fnStartInit);
    }
</script>
