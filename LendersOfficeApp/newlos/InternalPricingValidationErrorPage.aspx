﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InternalPricingValidationErrorPage.aspx.cs" Inherits="LendersOfficeApp.newlos.InternalPricingValidationErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="color:Red; font-weight:bold;margin:5px">
    <div>Unable to proceed with pricing due to the following errors</div>
    <br />
    <asp:PlaceHolder ID="ErrorMessagePlaceHolder" runat="server" EnableViewState="false" />
    </div>
    </form>
</body>
</html>
