﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RunBrowserXtVerificationCheck.aspx.cs" Inherits="LendersOfficeApp.newlos.RunBrowserXtVerificationCheck" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BrowserXT Detection</title>
    <style type="text/css">
        .center-div {
            width: 40%;
            margin: 10px auto;
        }
    </style>
    <script type="text/javascript">
        function logOut() {
            location.href = VRoot + '/logout.aspx';
        }

        function openBrowserXTInfo() {
            window.open(VRoot + '/common/AppDownload.aspx?key=' + ML.BrowserXtInfoPdfDownloadKey);
        }

        function openBrowserXtInstructions() {
            window.open(VRoot + '/common/AppDownload.aspx?key=' + ML.BrowserXtUninstallDirectionsDownloadKey);
        }

        function downloadBrowserXtUninstaller() {
            window.open(VRoot + '/common/AppDownload.aspx?key=' + ML.BrowserXtUninstallerDownloadKey);
        }

        function clientHasBrowserXtInstalled() {
            try {
                var version = new ActiveXObject("BrowserXT.VersionInfo");
                return true;
            }
            catch (e) {
                return false;
            }
        }

        function callWebMethod(methodName) {
            try {
                var args = {
                    type: 'POST',
                    url: 'RunBrowserXtVerificationCheck.aspx/' + methodName,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: true,
                };

                $.ajax(args);
            } catch (e) {
                // No-op
            }
        }

        function onBrowserXtDetected() {
            callWebMethod('AddInterceptLog');

            $('.initial-text').hide();
            $('.warning-label, #ContinueButton, #LogOutLink').show();

            $('.soft-stop').toggle(!ML.HardStopIfDetected);
            $('.hard-stop').toggle(ML.HardStopIfDetected);

            if (ML.HardStopIfDetected) {
                $('#ContinueButton').remove();
            }
        }

        function runBrowserXtDetection() {
            if (clientHasBrowserXtInstalled()) {
                onBrowserXtDetected();
            }
            else {
                document.getElementById("ContinueButton").click();
            }
        }

        $(runBrowserXtDetection);
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="RightAlign PaddingLeftRight5 center-div">
            <a id="LogOutLink" onclick="logOut();" class="Hidden">Log Out</a>
        </div>
        <div class="center-div">
            <div class="initial-text align-center">
                Running BrowserXT check...
            </div>

            <div class="Hidden little-indent warning-label">
                <label class="FieldLabel">BrowserXT Detected</label>
            </div>

            <div class="Hidden little-indent soft-stop">
                <p>
                    We have detected an active installation of the <a onclick="openBrowserXTInfo();">BrowserXT</a>
                    application on this computer. The current version of LendingQB no longer requires this software 
                    to function properly and we are requiring that all users remove it from their system to avoid any 
                    future security vulnerabilities with this add-on. To uninstall BrowserXT, please unzip and run the 
                    uninstaller that will download when you click this link: <a onclick="downloadBrowserXtUninstaller();">click here</a>. 
                    Alternatively, you may uninstall manually by following the instructions contained in this PDF: 
                    <a onclick="openBrowserXtInstructions();">click here</a>.
                </p>
                <p>
                    Please complete one of these these methods prior to <span runat="server" id="HardStopDate"></span>
                    to ensure that your access to LendingQB will continue without interruption.
                </p>
            </div>

            <div class="Hidden little-indent hard-stop">
                <p>
                    We have detected an active installation of the <a onclick="openBrowserXTInfo();">BrowserXT</a>
                    application on this computer. The current version of LendingQB no longer requires this software 
                    to function properly and we are requiring that all users remove it from their system to avoid any 
                    future security vulnerabilities with this add-on. To uninstall BrowserXT, please unzip and run the 
                    uninstaller that will download when you click this link: <a onclick="downloadBrowserXtUninstaller();">click here</a>. 
                    Alternatively, you may uninstall manually by following the instructions contained in this PDF: 
                    <a onclick="openBrowserXtInstructions();">click here</a>.
                </p>
                <p>
                    Please complete these steps and restart this computer to restore your access to LendingQB.
                </p>
            </div>
        </div>

        <div class="center-div align-center">
            <asp:Button 
                runat="server"
                ID="ContinueButton"
                OnClientClick="return !(ML.HardStopIfDetected && clientHasBrowserXtInstalled());"
                OnClick="ContinueButton_Click" 
                Text="Continue" 
                CssClass="Hidden" />
        </div>
    </form>
</body>
</html>
