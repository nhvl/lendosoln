﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos
{
    public partial class LoanRecentModification : BaseLoanPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            this.DisplayCopyRight = false;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> list = CPageData.GetRecentModificationUserName(LoanID, BrokerUser);
            m_repeater.DataSource = list;
            m_repeater.DataBind();
        }
    }
}
