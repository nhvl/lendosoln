namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    public partial class SubjPropInvestment : BaseLoanUserControl, IAutoLoadUserControl
    {
        protected void PageInit(object sender, System.EventArgs e) 
        {
            Tools.Bind_aOccT(aOccT);
        }

        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SubjPropInvestment));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            sSpGrossRent.Text = dataLoan.sSpGrossRent_rep;
            sOccR.Text = dataLoan.sOccR_rep;
            sOccRLckd.Checked = dataLoan.sOccRLckd;
            sPrimResid.Checked = dataLoan.sPrimResid;
            sSpCountRentalIForPrimaryResidToo.Checked = dataLoan.sSpCountRentalIForPrimaryResidToo;
            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);
            aBNm.Text = dataApp.aBNm;

            var page = Page as BasePage;
            page?.RegisterJsGlobalVariables("isPrimaryResidence", IsPrimaryResidence(dataLoan));
        }

        public void SaveData() 
        {
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// This is similar to sOccT except the current application is not included.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        /// <remarks>
        /// We exclude the current application to prevent UI issues. For example, if we took sOccT
        /// and the current application was the only one set to "Primary Residence" then sOccT
        /// would indicate a Primary Residence and sPrimresid would stay checked even if the
        /// current application's aOccT was changed.
        /// </remarks>
        private bool IsPrimaryResidence(CPageData dataLoan)
        {
            for (int i = 0; i < dataLoan.nApps; ++i)
            {
                CAppData dataApp = dataLoan.GetAppData(i);

                if (dataApp.aAppId == ApplicationID)
                {
                    continue;
                }
                else if (dataApp.aOccT == E_aOccT.PrimaryResidence)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
