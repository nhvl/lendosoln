﻿#region Generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// A popup for the advanced product filters.
    /// </summary>
    public partial class ProductCodeFilterPopup : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets or sets the product codes that can be selected.
        /// </summary>
        /// <value>The product codes that can be selected.</value>
        public Dictionary<string, bool> AvailableProductCodes { get; set; }

        /// <summary>
        /// Gets or sets the product codes that have been selected.
        /// </summary>
        /// <value>The product codes that are selected.</value>
        public Dictionary<string, bool> SelectedProductCodes { get; set; }

        /// <summary>
        /// Gets or sets the mapping from file type to product codes.
        /// </summary>
        /// <value>Mapping from file type to product codes.</value>
        public Dictionary<E_sLT, HashSet<string>> ProductCodesByFileType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the broker has the advanced filter options enabled for pricing.
        /// </summary>
        /// <value>Whether the broker has the advanced options for pricing enabled.</value>
        public bool EnableAdvancedFilterOptionsForPriceEngineResults { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this control should automatically load Jquery UI.
        /// </summary>
        /// <value>Whether this control should load jquery.</value>
        public bool ShouldLoadJqueryUi { get; set; } = true;

        /// <summary>
        /// Binds the data using a service item.
        /// </summary>
        /// <param name="service">The service.</param>
        /// <param name="dataLoan">The loan to bind to.</param>
        /// <param name="principal">The principal to use.</param>
        public static void BindData(AbstractBackgroundServiceItem service, CPageData dataLoan, AbstractUserPrincipal principal)
        {
            if (dataLoan.BrokerDB.IsAdvancedFilterOptionsForPricingEnabled(principal))
            {
                dataLoan.sSelectedProductCodeFilter = SerializationHelper.JsonNetDeserialize<Dictionary<string, bool>>(service.GetString("sSelectedProductCodeFilter"));
            }
        }

        /// <summary>
        /// Loads the data using a service item.
        /// </summary>
        /// <param name="service">The service item.</param>
        /// <param name="dataLoan">The loan to load from.</param>
        /// <param name="principal">The principal to use.</param>
        public static void LoadData(AbstractBackgroundServiceItem service, CPageData dataLoan, AbstractUserPrincipal principal)
        {
            if (dataLoan.BrokerDB.IsAdvancedFilterOptionsForPricingEnabled(principal))
            {
                service.SetResult("sSelectedProductCodeFilter", SerializationHelper.JsonNetSerialize(dataLoan.sSelectedProductCodeFilter));
                service.SetResult("sAvailableProductCodeFilter", SerializationHelper.JsonNetSerialize(dataLoan.sAvailableProductCodeFilter));
                service.SetResult("sProductCodesByFileType", SerializationHelper.JsonNetSerialize(dataLoan.sProductCodesByFileType));
            }
        }

        /// <summary>
        /// Loads data for the popup.
        /// </summary>
        /// <param name="sender">Caller of this method.</param>
        /// <param name="e">Any event data.</param>
        protected void PageLoad(object sender, EventArgs e)
        {
            if (!this.EnableAdvancedFilterOptionsForPriceEngineResults)
            {
                return;
            }

            var page = this.Page as BaseLoanPage;
            page.RegisterJsScript("ProductCodeFilterPopup.js");
            if (this.ShouldLoadJqueryUi)
            {
                page.RegisterJsScript("jquery-ui-1.11.4.min.js");
                page.RegisterCSS("jquery-ui-1.11.custom.css");
            }

            if (this.SelectedProductCodes == null ||
                this.AvailableProductCodes == null ||
                this.ProductCodesByFileType == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Please specify the selected product codes and available product codes");
            }

            this.sAvailableProductCodeFilter.Value = SerializationHelper.JsonNetSerialize(this.AvailableProductCodes);
            this.sSelectedProductCodeFilter.Value = SerializationHelper.JsonNetSerialize(this.SelectedProductCodes);
            this.sProductCodesByFileType.Value = SerializationHelper.JsonNetSerialize(this.ProductCodesByFileType);
        }

        /// <summary>
        /// Initializes the popup.
        /// </summary>
        /// <param name="sender">Caller of this method.</param>
        /// <param name="e">Any event data.</param>
        protected void PageInit(object sender, EventArgs e)
        {
            var page = this.Page as BaseLoanPage;
            if (page == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "ProductCodeFilterPopup can only be used on a BaseLoanPage");
            }
        }

        #region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        ///	the contents of this method with the code editor.
        /// </summary>
        /// <param name="e">Event argument parameter.</param>
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///	Required method for Designer support - do not modify
        ///	the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}