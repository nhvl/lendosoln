<%@ Page language="c#" Codebehind="LeftSummaryFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LeftSummaryFrame" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD id="HEAD1" runat="server">
    <title>LeftSummaryFrame</title>
  </HEAD>
  <body class="HeaderBackground">
  <script type="text/javascript" src="../inc/ModelessDlg.js"> </script>
    <script type="text/javascript">
  var gHistoryIndex = -1;
  var gHistory = [];
  var gHistoryTitle = [];
  var gMaxHistoryIndex = 0;
  var gIsTreeCollapse = false;
  var currentClosingCostFeeVersionT;

	function f_verifyPendingDirtyBit() {
    var bIsDirty = false;
    if (parent.body != null && typeof(parent.body.isDirty) == 'function') {
      bIsDirty = parent.body.isDirty();
    }
    if (bIsDirty) 
    {
      var args = {};
      args["loanid"] = ML.sLId;
      var result = gService.LeftSummaryFrame.call("UpdatePendingDirtyBit", args);
    }
  }
function f_accessDenied(errmsg) {
  alert(errmsg);

      var winOpener = parent.window.opener;
      // MULTI-EDIT
      if (null != winOpener && !winOpener.closed) {
        if (typeof(winOpener.removeFromCurrentLoanList) != "undefined")
          winOpener.removeFromCurrentLoanList(ML.sLId);
      }  
      parent.close();
}
        
  function f_pushHistory(href, title) {
    if (gHistory[gHistoryIndex] != href) {
      // Don't insert if previous location is the same with current.
      gHistory[++gHistoryIndex] = href;
      gHistoryTitle[gHistoryIndex] = title;
      gMaxHistoryIndex = gHistoryIndex;
    }
    f_updateButtons();    
  }
  
  function f_goBack() {
    if (gHistoryIndex > 0)
      f_goToLocation(gHistory[--gHistoryIndex]);
      
  }
  
  function f_goForward() {
    if (gHistoryIndex < gMaxHistoryIndex)
      f_goToLocation(gHistory[++gHistoryIndex]);
  }
  
  function f_goToLocation(href) {
    try{
        parent.body.f_loadFromHistory(href);
        f_updateButtons();    
    }catch(e){}
  }
  
  function f_updateButtons() {
    var btnBack = document.getElementById("btnBack");
    btnBack.disabled = gHistoryIndex <= 0;
    btnBack.style.backgroundColor = "";
    if (!btnBack.disabled) {
      btnBack.title = "Back to " + gHistoryTitle[gHistoryIndex - 1];
    } else {
      btnBack.title = "";
    }
    var btnForward = document.getElementById("btnForward");
    btnForward.disabled = gHistoryIndex == gMaxHistoryIndex || gHistoryIndex == -1;
    btnForward.style.backgroundColor = "";
    if (!btnForward.disabled) {
      btnForward.title = "Forward to " + gHistoryTitle[gHistoryIndex + 1];
    } else {
      btnForward.title = "";
    }
  }  
  
  function f_enableSave(b) {
    // For some odd reason if the page does not get load completely then it will
    // not cause any javascript error. dd
    var btnSave = document.getElementById("btnSave");
    if (btnSave != null) {
      btnSave.disabled = !b;
      btnSave.style.backgroundColor = "";
    }
  }
  function f_save() {
    try{
        var bSuccess = parent.body.f_saveMe();
        f_enableSave(!bSuccess);
    }catch(e){}
  }
  function f_getCurrentApplicationID() {
    return <%= AspxTools.JsGetElementById(m_applicantsDDL) %>.value;
  }
  
  function f_setCurrentApplicationID(id) {
    return <%= AspxTools.JsGetElementById(m_applicantsDDL) %>.value = id;
  }
  function f_updateApplicantDDL(name, id) {
    var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;
    var count = ddl.options.length;
    var op = null;
    for (var i = 0; i < count; i++) {
      if (ddl.options[i].value == id) {
        op = ddl.options[i];
        break;
      }
    }
    if (op == null) {
      op = document.createElement("OPTION");
      ddl.options.add(op);
    }
    op.text = name;
    op.value = id;
    f_setCurrentApplicationID(id);
    
  }
  function f_swapPrimary(id) {
    var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;
    var count = ddl.options.length;
    var op = null;
    for (var i = 0; i < count; i++) {
      if (ddl.options[i].value == id) {
        op = ddl.options[i];
        ddl.options.remove(i);
        ddl.options.add(op, 0);
        break;
      }
    }

    f_setCurrentApplicationID(id);
    
  }
  function f_removeBorrower(id) {
    var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;
    var count = ddl.options.length;
    for (var i = 0; i < count; i++) {
      if (ddl.options[i].value == id) {
        
        ddl.options.remove(i);
        
        break;
      }
    }  
    
  }

        function navigateToInitialUrl() {
            if (!redirectToUladIfUladPage(ML.InitialUrl, true)) {
                parent.body.location = ML.InitialUrl + '&appid=' + encodeURIComponent(f_getCurrentApplicationID());
            }
        }

    function addNewAppToDropdown(appId) {
        var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;
        ddl.options.add(new Option(', ', appId));
    }

        function clickLandingPage() {
        
            var treeFrame = retrieveFrame(window.parent, "treeview");
            if (treeFrame.document.readyState !== "complete" || !treeFrame.clickLandingPage) {
                window.setTimeout(function () { clickLandingPage(); }, 150);
            }
            else {
                treeFrame.clickLandingPage();
            }
    }

    function setFavoritePage() {
        var favoriteLinks = getFavoriteLinks();

        // Certain pages that redirect to PML or other locations
        // on load are ignored.
        var ignorePageIds = [
            'Page_QualifiedLoanProgramSearch', 
            'Page_QualifiedLoanProgramSearchBeta', 
            'Page_RunInternalPricingV2'
        ];

        for (var i = 0; i < favoriteLinks.length && i < 5; ++i) {
            var favoriteLink = favoriteLinks.get(i);

            if (ignorePageIds.indexOf(favoriteLink.id) !== -1) {
                continue;
            }

            var calledFunction = favoriteLink.onclick.toString();

            // Only click a favorite link if it opens a page.
            // Popups and alerts are excluded.
            if (calledFunction.indexOf('.aspx') !== -1 || calledFunction.indexOf('loadUladPage') !== -1) {
                favoriteLink.click();
                return;
            }
        }

        navigateToInitialUrl();
    }

    function documentHasFavoritePageLink() {
        return getFavoriteLinks().length > 0;
    }

    function setFavoritePageById(tryNumber) {
        if (tryNumber > 4) {
            navigateToInitialUrl();
            return;
        }

        try {
            var treeFrame = retrieveFrame(window.parent, "treeview");
            if (treeFrame.document.readyState !== "complete" || !documentHasFavoritePageLink()) {
                window.setTimeout(function() { setFavoritePageById(tryNumber + 1); }, 150);
            }
            else {
                setFavoritePage();
            }
        } catch (e) {
            navigateToInitialUrl();
        }
    }

  function reloadPage() {
    document.forms[0].submit();
  }
        function _init() {
    <% if (m_isPolling)
        { %>
            setTimeout(reloadPage, 5000);
            return;
    <% } %>
            // After this page finished, load main page.
      <% if (!RequestHelper.GetBool("noinit") && !m_hasError)
        { %>

            if (!callFrameMethod(window, 'parent', "navigateToHighUrl")) {
                if (ML.DisableInitialPageSelectionFromFavorites) {
                    navigateToInitialUrl();
                }
                else {
                    clickLandingPage();
                }
            }
    <% } %>
    <% if (m_hasError) { %>
      parent.pageframeset.rows = "30, 0, *";
      parent.mainframeset.cols = "0, *";
      parent.body.location = 'loading.aspx?errcode=notfound';
    <% } %>
    parent.document.title = ML.Title;
    
    if (ML.IsEnforceVersionCheck)
    {
      setInterval(f_verifyPendingDirtyBit, ML.CheckDirtyInMinutes * 60000);
    }
    		    
    f_updateButtons();
    <% if (m_hasRecentModification) { %>
    showModal('/newlos/LoanRecentModification.aspx?loanid=' + ML.sLId, null, null, null, null,{ hideCloseButton: true });
    <% } %>
  }
  function onApplicantsChange() {
    // Notify body frame that applicant changed. 
    if (typeof(parent.body.switchApplicant) == "function")
      parent.body.switchApplicant();
  }

  function f_updateLoanNumber(sLNm) {
    parent.f_update_sLNm(sLNm + ' - <%=AspxTools.JsStringUnquoted(m_aBLastNm)%>');  
    document.getElementById('sLNm').innerText = sLNm;
  }
  function f_getLoanNumber() {
    return document.getElementById('sLNm').innerText;
  }
  function f_updateLoanStatus(name) {
    document.getElementById("sStatusT").innerText = name;
  }
  function f_updateLoanOfficer(name) {
    document.getElementById("LoanOfficer").innerText = name;
  }
  function f_updateFieldValue(name, value) {
    var item = document.getElementById(name);
    if(item){
        item.innerText = value == "" ? "N/A" : value;
    }
  }
      
  function UpdateLoanVersionTAlert(loanVersionUpToDate, currentVersion, latestVersion)
  {
      var wasVisible = jQuery("#LoanVersionTAlert").is(":visible");
      if(loanVersionUpToDate === "True")
      {
          jQuery("#LoanVersionTAlert").hide();
          return wasVisible;
      }
      else
      {
          jQuery("#LoanVersionTAlert").show();

          // In case we, for some strange reason, are unable to load the loan version from the cache.
          if(currentVersion === "-1")
          {
              if(jQuery("#LoanVersionAlertMoreInfo").is(":visible"))
              {
                  // The message is visible from a previous save but a page load couldn't load the value from the cache.
                  // Let's just keep displaying this message.
                  return false;
              }

              var noCacheMessage = "Unable to load version for alert message";
              jQuery("#LoanVersionTMessageShort").text(noCacheMessage);
              jQuery("#LoanVersionTMessageShort").show();
              jQuery("#LoanVersionTMessageLong").hide();
              jQuery("#LoanVersionAlertMoreInfo").hide();
              return true;
          }

          var shortMessage = "File calculation version: " + currentVersion + ", latest: " + latestVersion;
          var longMessage = "This file is using calculation version " + currentVersion + ". The latest version is " + latestVersion + " and migrating to this version may or may not be necessary. " +
                            "Please contact your system administrator to discuss your options and effects.";

          jQuery("#LoanVersionTMessageShort").text(shortMessage);
          jQuery("#LoanVersionTMessageLong").text(longMessage);
          jQuery("#LoanVersionAlertMoreInfo").show();

          var shortVisible = jQuery("#LoanVersionTMessageShort").is(":visible");
          if(shortVisible || !wasVisible)
          {
              jQuery("#LoanVersionTMessageShort").show();
              jQuery("#LoanVersionTMessageLong").hide();
              jQuery("#LoanVersionAlertMoreInfo").text("more info");
          }
          else
          {
              jQuery("#LoanVersionTMessageShort").hide();
              jQuery("#LoanVersionTMessageLong").show();
              jQuery("#LoanVersionAlertMoreInfo").text("less info");
          }

          return !wasVisible;
      }
  }

  function ToggleLoanVersionMessage()
  {
      var shortVisible = jQuery("#LoanVersionTMessageShort").is(":visible");
      if(shortVisible)
      {
          jQuery("#LoanVersionTMessageShort").hide();
          jQuery("#LoanVersionTMessageLong").show();
          jQuery("#LoanVersionAlertMoreInfo").text("less info");
      }
      else
      {
          jQuery("#LoanVersionTMessageShort").show();
          jQuery("#LoanVersionTMessageLong").hide();
          jQuery("#LoanVersionAlertMoreInfo").text("more info");
      }

      jQuery("#Notifications").trigger('Resize');
  }

    function f_postRefresh(data)
    {
        var auditNeedsResize = f_updateDocumentAudit(data.documentId);
        var complNeedsResize = f_updateCompliance(data.status);
        var complEagleNeedsResize = f_updateCEagleDiv(data.status);
        var aprNeedsResize = f_updateAPR(data.NeedsAPRAlert, data.APRDelta);
        var loanVersionNeedsResize = UpdateLoanVersionTAlert(data.LoanVersionTUpToDate, data.LoanVersionTCurrent, data.LoanVersionTLatest);
        if(auditNeedsResize || complNeedsResize || aprNeedsResize || complEagleNeedsResize || loanVersionNeedsResize) 
        {
            jQuery('#Notifications').trigger('Resize');
        }
        
        window.setTimeout(f_refreshGfeToleranceInfo, 50);
    }
    function f_updateGfeZero(needsNotification, redisclosingWillClear)
    {
        var $gfeZeroDiv = jQuery('#GfeZeroPercentAlert');
        var $spans = $gfeZeroDiv.find('span');
        var wasVisible = $gfeZeroDiv.is(":visible");
        if(!needsNotification) {
            if(wasVisible) $gfeZeroDiv.hide();
            return wasVisible;
        }
        if(!wasVisible && needsNotification)
        {
            needsResize = true;
            $gfeZeroDiv.show();
        }

        if (needsNotification) {
            $gfeZeroDiv.find('span.Redisclose').toggle(redisclosingWillClear);

            if (redisclosingWillClear) {
                $spans.removeClass('ErrorMsg')
                    .removeClass('RedisclosingWillClear')
                    .addClass('RedisclosingWillClear');
            } else {
                $spans.removeClass('ErrorMsg')
                    .removeClass('RedisclosingWillClear')
                    .addClass('ErrorMsg');
            }
        }

        return needsResize;
    }
    function f_updateGfeTen(needsNotification, redisclosingWillClear)
    {
        var $gfeTenDiv = jQuery('#GfeTenPercentAlert');
        var $spans = $gfeTenDiv.find('span');
        var wasVisible = $gfeTenDiv.is(":visible");
        if(!needsNotification) {
            if(wasVisible) $gfeTenDiv.hide();
            return wasVisible;
        }
        if(!wasVisible && needsNotification)
        {
            needsResize = true;
            $gfeTenDiv.show();
        }

        if (needsNotification) {
            $gfeTenDiv.find('span.Redisclose').toggle(redisclosingWillClear);

            if (redisclosingWillClear) {
                $spans.removeClass('ErrorMsg')
                    .removeClass('RedisclosingWillClear')
                    .addClass('RedisclosingWillClear');
            } else {
                $spans.removeClass('ErrorMsg')
                    .removeClass('RedisclosingWillClear')
                    .addClass('ErrorMsg');
            }
        }

        return needsResize;
    }
    
    function f_updateAPR(needsNotification, delta)
    {
        var $notificationDiv = jQuery('#APRRedisclosure');
        var wasVisible = $notificationDiv.is(":visible");
        if(!needsNotification) {
            if(wasVisible) $notificationDiv.hide();
            return wasVisible;
        }
        
        delta = Math.round(delta * 1000)/1000;
        if(delta.toString().indexOf("0") == 0) delta = delta.toString().substring(1);
        
        jQuery('#APRDelta').text(delta);
        var needsResize = false;
        if(!wasVisible)
        {
            needsResize = true;
            $notificationDiv.show();

            if (ML.isTrid) {
                alert("The APR disclosed on the most recent Closing Disclosure has changed by " + delta + " and has become inaccurate.\nRe-issue the Closing Disclosure to begin new three day waiting period per TRID regulations.");
            } else {
                alert("APR has changed by " + delta + " since last disclosed. \nRedisclosure is required under MDIA regulations.");
            }
        }
        
        return needsResize;
    }

    function f_updateCompliance(status)
    {
        var compliance = jQuery('#Compliance');
        var complianceDivVisible = compliance.is(':visible');

        showCompliance = jQuery.inArray(status, complianceDisabledStatuses) == -1;

        var needsResize = showCompliance != complianceDivVisible;
        
        if(!complianceDivVisible && showCompliance)
        {
            compliance.trigger('Enable');
        }
        
        if(complianceDivVisible && !showCompliance)
        {
            compliance.trigger('Disable');
        }
        
        if(typeof(parent.body.registerPostSaveMeCallback) == "function"){
            parent.body.registerPostSaveMeCallback(refreshCompliance); //Does nothing if it's already registered, so this is ok.
        }
        
        return needsResize;
    }

    function refreshCompliance()
    {
        jQuery('#Compliance').trigger('Refresh');
    }
    
    function f_updateCEagleDiv(status)
    {
        var compliance = jQuery('#ComplianceEagleDiv');
        var complianceDivVisible = compliance.is(':visible');

        showCompliance = jQuery.inArray(status, complianceEagleDisabledStatuses) == -1;

        var needsResize = showCompliance != complianceDivVisible;
        
        if(!complianceDivVisible && showCompliance)
        {
            compliance.trigger('Enable');
        }
        
        if(complianceDivVisible && !showCompliance)
        {
            compliance.trigger('Disable');
        }
        
        if(typeof(parent.body.registerPostSaveMeCallback) == "function"){
            parent.body.registerPostSaveMeCallback(refreshComplianceEagleDiv); //Does nothing if it's already registered, so this is ok.
        }
        
        return needsResize;
    }

    function refreshComplianceEagleDiv()
    {
        jQuery('#ComplianceEagleDiv').trigger('Refresh');
    }
  
  function f_updateDocumentAudit(documentId)
  {
    var bShowDocumentAudit = false;
    var oDocumentId = document.getElementById('sDocMagicPlanCodeId');
    var oDocumentNm = document.getElementById('sDocMagicPlanCodeNm');
    
    if (( documentId == '00000000-0000-0000-0000-000000000000' ) || <%=AspxTools.JsBool(!m_IsEnablePTMDocMagicSeamlessInterface)%>)
    {
      bShowDocumentAudit = false;
      oDocumentNm.innerHTML = ''
      oDocumentId.innerHTML = documentId;
    }
    else if ( documentId == <%=AspxTools.JsString(LendersOffice.ObjLib.DocMagicLib.DocMagicPlanCode.PendingUserChoice.ToString())%> )
    {
      bShowDocumentAudit = true;
      oDocumentNm.innerHTML = '<a href="#" onclick="parent.body.LQBPopup.Show(\''+ML.VirtualRoot+'/newlos/SelectDocMagicPlan.aspx?loanid=' + ML.sLId + '\',{onReturn:f_refreshInfoFromDB} );">select doc magic plan code</a>';
      oDocumentId.innerHTML = documentId;
    }
    
    var statusDiv = document.getElementById('DocumentStatusDiv');
    var statusDivVisible = statusDiv.style.display != 'none';
    statusDiv.style.display = ( bShowDocumentAudit ? 'block' : 'none');

    var needsResize = statusDivVisible != bShowDocumentAudit;
    
    return needsResize;
  }
  

  
  function f_update(sQualTopR, sQualBottomR, sCltvR, sLtvR, sHcltvR, sRateLockStatusT) {
    f_updateFieldValue("sQualTopR", sQualTopR);
    f_updateFieldValue("sQualBottomR", sQualBottomR);
    f_updateFieldValue("sCltvR", sCltvR);
    f_updateFieldValue("sLtvR", sLtvR);
    f_updateFieldValue("sHcltvR", sHcltvR);
  }
  
  function f_refreshInfo() {
    f_displayLoanSummary(true);
    
    var parentML = parent.frames['body'].ML;
    
    if (parentML != null)
    {
      f_updateLoanNumber(parentML.sLNm);
      f_updateLoanStatus(parentML.sStatusT);
      f_updateLoanOfficer(parentML.sEmployeeLoanRepName);
      f_updateFieldValue("sQualTopR", parentML.sQualTopR);
      f_updateFieldValue("sQualBottomR", parentML.sQualBottomR);
      f_updateFieldValue("sCltvR", parentML.sCltvR);
      f_updateFieldValue("sLtvR", parentML.sLtvR);
      f_updateFieldValue("sHcltvR", parentML.sHcltvR);
      f_updateFieldValue("sRateLockStatusT", parentML.sRateLockStatusT);
      f_updateFieldValue("sNoteIR", parentML.sNoteIR);
      f_updateFieldValue("sFinalLAmt", parentML.sFinalLAmt);
      f_updateFieldValue("sLT", parentML.sLT);
      
      var needsAPR = parentML.NeedsAPRAlert; //make sure it's a bool
      currentClosingCostFeeVersionT = parentML.sClosingCostFeeVersionT;
      
      var data = {
        status: parentML.sStatusT, 
        documentId: parentML.sDocMagicPlanCodeId,
        APRDelta: parentML.APRDelta,
        NeedsAPRAlert: needsAPR,
        LoanVersionTUpToDate: parentML.LoanVersionTUpToDate,
        LoanVersionTCurrent: parentML.LoanVersionTCurrent,
        LoanVersionTLatest: parentML.LoanVersionTLatest
      };
      f_postRefresh(data);
    }
  }
  
  var haveBeenAlertedOfGfeToleranceInfoFailure = false;
  var timestampOfLastGfeToleranceRefresh = null;
  var minimumSecondsBetweenGfeToleranceRefreshes = null;

  function f_refreshGfeToleranceInfo()
    {
        if (minimumSecondsBetweenGfeToleranceRefreshes === null
                && ML && ML.MinimumSecondsBetweenGfeToleranceRefreshes !== null
                && ML.MinimumSecondsBetweenGfeToleranceRefreshes >= 0) {
            minimumSecondsBetweenGfeToleranceRefreshes = ML.MinimumSecondsBetweenGfeToleranceRefreshes
        }

        if (timestampOfLastGfeToleranceRefresh !== null
                && minimumSecondsBetweenGfeToleranceRefreshes !== null
                && minimumSecondsBetweenGfeToleranceRefreshes >= 0) {
            var elapsedSeconds = (Date.now() - timestampOfLastGfeToleranceRefresh) / 1000;
            if (elapsedSeconds < minimumSecondsBetweenGfeToleranceRefreshes) {
                return;
            }
        }

        timestampOfLastGfeToleranceRefresh = Date.now();

        var DTO = 
        {
            sLId: ML.sLId
        };
        jQuery.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'LeftSummaryFrameService.aspx/GetGfeToleranceInfo',
            dataType: 'json',
            data: JSON.stringify(DTO),
            success: function(msg){
                var ret = msg.d;
                if (ret.MinimumSecondsBetweenGfeToleranceRefreshes !== undefined 
                        && ret.MinimumSecondsBetweenGfeToleranceRefreshes !== null
                        && ret.MinimumSecondsBetweenGfeToleranceRefreshes >= 0) {
                    minimumSecondsBetweenGfeToleranceRefreshes = ret.MinimumSecondsBetweenGfeToleranceRefreshes;
                }
                var gfeNeedsResize = f_updateGfeZero(ret.NeedsGfeZeroAlert, ret.RedisclosingPendingArchiveWillClearZeroPercentCure);
                gfeNeedsResize = f_updateGfeTen(ret.NeedsGfeTenAlert, ret.RedisclosingPendingArchiveWillClearTenPercentCure) || gfeNeedsResize;
                if(gfeNeedsResize)
                { jQuery('#Notifications').trigger('Resize');}
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){ 
                if(false === haveBeenAlertedOfGfeToleranceInfoFailure)
                {
                    haveBeenAlertedOfGfeToleranceInfoFailure = true;
                    alert('Unable to calculate GFE Tolerances.  Please refresh the loan.  If this error persists please notify LQB support.');
                }
            },
            complete: function(req){

            }
        });
    }

  function f_reloadNavigationFrameIfClosingCostFeeVersionChanged(newClosingCostFeeVersionT) {
      if (newClosingCostFeeVersionT !== currentClosingCostFeeVersionT) {
          parent.treeview.f_refreshAfterTimeout(1000);
          currentClosingCostFeeVersionT = newClosingCostFeeVersionT;
      }
  }
  
  function f_refreshInfoFromDB()
  {
    var args = new Object();
    args["loanid"] = ML.sLId;
    var result = gService.LeftSummaryFrame.call("RefreshInfo", args);
    if (!result.error) {
      f_updateLoanNumber(result.value["sLNm"]);
      f_updateLoanStatus(result.value["sStatusT"]);
      f_updateLoanOfficer(result.value["LoanOfficer"]);
      f_updateFieldValue("sQualTopR", result.value["sQualTopR"]);
      f_updateFieldValue("sQualBottomR", result.value["sQualBottomR"]);
      f_updateFieldValue("sCltvR", result.value["sCltvR"]);
      f_updateFieldValue("sLtvR", result.value["sLtvR"]);
      f_updateFieldValue("sHcltvR", result.value["sHcltvR"]);
      f_updateFieldValue("sRateLockStatusT", result.value["sRateLockStatusT"]);
      f_updateFieldValue("sNoteIR", result.value["sNoteIR"]);
      f_updateFieldValue("sFinalLAmt", result.value["sFinalLAmt"]);
      f_updateFieldValue("sLT", result.value["sLT"]);

      f_reloadNavigationFrameIfClosingCostFeeVersionChanged(result.value['sClosingCostFeeVersionT']);
      
      var data = {
          status: result.value["sStatusT"], 
          documentId: result.value["sDocMagicPlanCodeId"],
          APRDelta: result.value["APRDelta"],
          NeedsAPRAlert: result.value["NeedsAPRAlert"],
          LoanVersionTUpToDate: result.value["LoanVersionTUpToDate"],
          LoanVersionTCurrent: result.value["LoanVersionTCurrent"],
          LoanVersionTLatest: result.value["LoanVersionTLatest"]
      };
      f_postRefresh(data);
    }
  }

  //The old TreeStateXml is passed in since saving now merges the sets instead of just overwriting them.
  function f_updateLoanEditorMenuTreeState(xml, loanEditorMenuTreeStateXmlContent) {
    var args = new Object();
    args["xml"] = xml;
    args["loanEditorMenuTreeStateXmlContent"] = loanEditorMenuTreeStateXmlContent;
    var result = gService.LeftSummaryFrame.call("UpdateLoanEditorMenuTreeState", args);

    alert('Menu layout saved.');
  }

  g_bReload = false;
  __bIsReloading = false;
  function f_AppReload()
  {
    if (__bIsReloading)
        return true;
    
    if (g_bReload)
    {
        g_bReload = false;
        var oApps = f_refreshAppList();
        if (oApps != null && !f_IsActiveAppRemoved(oApps) )
        {
            f_updateDDL(oApps);
            return false;
        }
            
        __bIsReloading = true;
        var newUrl = f_parseUrl(parent.info.location);
        parent.info.location.href = newUrl;
        return true;
    }
    return false;
  }
  
  function f_updateDDL(oApps)
  {
    var hash = new Object();
    var ddlHash = new Object();   
    var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;
    
    for (var i=0; i < oApps.values.length; i++)
    {
        hash[oApps.values[i].id] = oApps.values[i].name;
    }
    
    for (var i = (ddl.options.length-1) ; i >= 0; i--) {
      if (typeof(hash[ddl.options[i].value]) == 'undefined')
      {
        ddl.options.remove(i);
      }
    }
    
    for (var i=0; i < ddl.options.length; i++)
    {
        ddlHash[ddl.options[i].value] = ddl.options[i].text;
    }
    
    for (var i=0; i < oApps.values.length; i++)
    {
        if (typeof(ddlHash[oApps.values[i].id]) == 'undefined')
        {
            var op = document.createElement("OPTION");
            op.text = oApps.values[i].name;
            op.value = oApps.values[i].id;
            ddl.options.add(op);
        }
    }
  }
  
  function f_IsActiveAppRemoved(oApps, currentGuid)
  {
    if (typeof(oApps) != 'object' || oApps == null)
        return true;
    
    var currentGuid = f_getCurrentApplicationID();
    var found = false;
    for (var i=0; i < oApps.values.length; i++)
    {
        if (oApps.values[i].id == currentGuid)
        {
            found = true;
            break;
        }
    }
    if (found)
        return false;
        
    return true;
  }
  
  function f_refreshAppList()
  {
    var oApps = null;
    var args = new Object();
    args["loanid"] = ML.sLId;
    
    var result = gService.LeftSummaryFrame.call("RefreshAppList", args);
    if (!result.error) {
      var data = result.value["appList"];
      try{
        oApps = eval(data);
      }
      catch(e){ oApps = null; }
    }
    return oApps;
  }

  function updateApplicantsForceRefresh()
  {
      var appList = f_refreshAppList();

      var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;

      for (var i = (ddl.options.length-1) ; i >= 0; i--) {
          ddl.options.remove(i);
      }

      for (var i = 0; i < appList.length; i++) {
          var option = document.createElement("OPTION");
          option.text = appList.values[i].name;
          option.value = appList.values[i].id;
          ddl.options.add(option);
      }
  }
  
  function f_parseUrl(location)
  {
    var href = location.href;
    var baseUrl = location.pathname;
    var param = '';
    var newParam = '';            
    var limit = href.indexOf('?');
            
    if (limit != -1)
    {
        param = href.substring(limit+1);
        newParam = f_parseUrlParameters(param);
        return baseUrl + '?' + newParam;
    }
    return href;  
  }
  
function f_parseUrlParameters(param)
{
    var newParam = '';
    var limit = param.indexOf("appid=");
    if (limit != -1)
    {
        if (limit==0)
        {
            limit = param.indexOf("&");
            if (limit != -1)
            {
                newParam = param.substring(limit+1);
            }
        }
        else
        {
            newParam = param.substring(0, limit-1);
            param = param.substring(limit);
            limit = param.indexOf("&");
            if (limit != -1)
            {
                newParam += param.substring(limit);
            }
        }
        return newParam;
    }
    return param;
}
  
  function f_displayLoanSummary(bVisible, reload) {
    document.getElementById("LoanInfoTablePanel").style.display = bVisible ? "" : "none";
    if (typeof(reload) != 'undefined' || bVisible)
        <%= AspxTools.JsGetElementById(m_applicantsDDL) %>.style.display = bVisible ? "" : "none";
    if (!g_bReload && typeof(reload) != 'undefined' && reload)
        g_bReload = true;
  }
  
  function f_print() {
    parent.treeview.load('PrintList.aspx');
  }
  
jQuery(function($){
    var readOnly = <%= AspxTools.JsGetElementById(panelReadOnly) %>
    var notifications = jQuery('#Notifications');
    notifications.bind('Resize', function(){
        var divs = jQuery(this).children('div:visible');
        var newsize = 24;
        divs.each(function() {
            newsize += $(this).height();
        });
        //var newsize = divs.length * 16 + 24;
        if(readOnly && (readOnly.style.visibility == "visible" || readOnly.style.visibility == ""))
            newsize += 16;
        
        if(typeof(parent.resizeSummary) == "function")
        {
            if(newsize > 40)
            {
                parent.resizeSummary(newsize);
            }
            else
            {
                parent.resizeSummary();
            }
        }
    });
    
    var compliance = $('#Compliance');
    var isComplianceEase = true;
    
    if(!compliance.length){
        compliance = $('#ComplianceEagleDiv');
        isComplianceEase = false;
        
        if(!compliance.length){
            return;
        }
    }
    
    compliance.one('Enable', enableComplianceRefresh);
    
    function enableComplianceRefresh(){
        if(!compliance.data('initialized')){
            if(isComplianceEase){
                initCompliance();
            }
            else{
                initComplianceEagleDiv();
            }
        }
        
        compliance.one('Disable', disableComplianceRefresh);
        
        compliance.trigger('Refresh');
        compliance.data('Enabled', true);
        compliance.show();
    }
        
    function disableComplianceRefresh()
    {
        compliance.hide();
        compliance.data('Enabled', false);
        if(RefreshTimeout)
        {
            window.clearTimeout(RefreshTimeout);
        }
        compliance.one('Enable', enableComplianceRefresh);
    }
    
    var RefreshTimeout = null;
    var lastXhr = null;
    
    function Refresh(){
        var updating = window.setTimeout(function(){
            compliance.find('span.Level').text("Updating...").attr('class', 'Level');
            compliance.find('a').hide();
        }, 500);
        
        var refreshTimeSeconds = 60;
        var stop = false;
        var DTO = 
        {
            sLId: ML.sLId,
            checkingError: false
        };
        lastXhr = $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'LeftSummaryFrameService.aspx/GetComplianceInfo',
            dataType: 'json',
            data: JSON.stringify(DTO),
            success: function(msg){
                refreshTimeSeconds = handleData(msg, updating);
                if(!refreshTimeSeconds) stop = true;
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){ 
                window.clearTimeout(updating);            
                compliance.find('span.Level').text("Error updating, please reopen file").attr('class', 'Level ErrorMsg');
                stop = true;
            },
            complete: function(req){
                if(req != lastXhr) return;
                if(stop) return;
                if(!compliance.data('Enabled')) return;
                
                RefreshTimeout = window.setTimeout(Refresh, refreshTimeSeconds * 1000);
            }
        });
    }
    
    function reallyErroring()
    {
        var DTO = 
        {
            sLId: ML.sLId,
            checkingError: true
        };
        var errorsExist = false;
        $.ajax({
            type: "POST",
            async: false,
            contentType: "application/json; charset=utf-8",
            url: 'LeftSummaryFrameService.aspx/GetComplianceInfo',
            dataType: 'json',
            data: JSON.stringify(DTO),
            success: function(msg){errorsExist = msg.d.IsError}
        });
        
        return errorsExist;
    }
    
    var complianceTimedOut = null;
    var complianceUpdateTimeout = null; 
    
    function refreshComplianceTimeout(){
        complianceTimedOut = false;
        window.clearTimeout(complianceUpdateTimeout);
        complianceUpdateTimeout = window.setTimeout(function(){
            complianceTimedOut = true;
        }, 1000 * 60 * 20 /*keep in mind that this gets restarted 
        on any sort of page activity, like saving or clicking a link*/);
    }
    
    function handleData(msg, updating){
        var data = msg.d;
        var refreshTimeSeconds = data.RefreshTimeSeconds;
        
        if(data.IsUpdating && !data.IsError)
        {
            if(complianceTimedOut) return false;
            else return refreshTimeSeconds;
        }
        
        refreshComplianceTimeout();
        window.clearTimeout(updating);
        compliance.find('a').hide();
        
        var ErrorWindow = TryGetChildWindow();
        
        if(data.IsError)
        {
            compliance.find('span.Level').text("ERRORS").attr('class', 'Level ErrorMsg');
            compliance.find('a.Errors').show();
            
            if(ErrorWindow)
            {
                ErrorWindow.window.location.reload(true);
            }
            
            return refreshTimeSeconds;
        }
        
        if(ErrorWindow)
        {
            ErrorWindow.onClosePopup();
        }
        
        compliance.find('span.Level').text(data.Status).attr('class', 'Level ' + data.StatusClass);
        compliance.find('a.Report').show();
        return refreshTimeSeconds;
    }
        
    function initCompliance()
    {
        if(compliance.data('initialized')) return;
    
        compliance.find('a.Errors').click(function(){
            if(reallyErroring())
            {
                var url = ML.VirtualRoot + '/newlos/services/ComplianceEaseErrorSummary.aspx?loanid=' + ML.sLId + '&appid=' + parent.info.f_getCurrentApplicationID();
                var opts = "dialogWidth: 600px; dialogHeight: 387px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;";
                
                window.parent.AuditWindow = showModeless(url, opts, null,  window.parent);
            }
            else
            {
                Refresh();
            }
        });
        
        compliance.find('a.Report').click(function(){
              window.open(ML.VirtualRoot + '/newlos/Services/ComplianceEaseReport.aspx?loanid=' + ML.sLId + '&useEdocs=t&rand=' + new Date(), null, "toolbar=no,menubar=no,location=no,status=no");
        });
        
        compliance.bind('keyup', function(e){
            if(e.ctrlKey && e.altKey && e.which == 70 /*F on IE8*/)
            {
                if(RefreshTimeout)
                {
                    window.clearTimeout(RefreshTimeout);
                }
                Refresh();
            }
        });
        
        refreshComplianceTimeout();
        compliance.bind("Refresh", Refresh);
        compliance.data('initialized', true);
    }
    
    function initComplianceEagleDiv()
    {
        if(compliance.data('initialized')) return;
    
        compliance.find('a').click(function(){
            var url = ML.VirtualRoot + '/newlos/services/ComplianceEagleResultSummary.aspx?loanid=' + ML.sLId + '&appid=' + parent.info.f_getCurrentApplicationID();
            var opts = "dialogWidth: 800px; dialogHeight: 387px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;";
                
            window.parent.AuditWindow = showModeless(url, opts, null,  window.parent);
        });
        
        compliance.bind('keyup', function(e){
            if(e.ctrlKey && e.altKey && e.which == 70 /*F on IE8*/)
            {
                if(RefreshTimeout)
                {
                    window.clearTimeout(RefreshTimeout);
                }
                Refresh();
            }
        });
        
        refreshComplianceTimeout();
        compliance.bind("Refresh", Refresh);
        compliance.data('initialized', true);
    }
    
    function TryGetChildWindow()
    {
        //This variable is set when you call showSoloModeless, it's the child window with the errors in it.
        //If it's not null, the user may or may not have it open; the only way to tell is to try.
        var child = g_CurrentModelessDlg;
        try 
        {
            if(child) child.window;
        }
        catch(e)
        {
            child = null;
            g_CurrentModelessDlg = null;
        }
        
        return child;
    }
    
    
});
    </script>
    <style type="text/css">
        #Notifications label, #Notifications span
        {
            font-weight: bold;
        }
        
        #Notifications a:link
        {
            color: #FFFFFF;
        }
        
        #Notifications a:visited 
        {
            color:#aaaaaa;
        }
        
        #Compliance, #ComplianceEagleDiv 
        {
            display:none;
        }
        
        #Compliance a, #ComplianceEagleDiv a
        {
            display:none;
        }
        
        #Compliance span.Minimal, #ComplianceEagleDiv span.Pass       
        {
            color:#00FF00;
        }
        
        #Compliance span.Elevated
        {
            color:Yellow;
        }
        
        #Compliance span.Moderate, #ComplianceEagleDiv span.Warning, #ComplianceEagleDiv span.Review
        {
            color:#ffcc00;
        }
        
        #Compliance span.Significant, #Compliance span.Critical, #Compliance span.ErrorMsg, #GfeZeroPercentAlert span.ErrorMsg, #GfeTenPercentAlert span.ErrorMsg, #ComplianceEagleDiv span.FailException, #ComplianceEagleDiv span.Fail, #ComplianceEagleDiv span.Alert, #ComplianceEagleDiv span.UserInput
        {
            color:#FF0000;
        }
        
        #APRRedisclosure span
        {
            color: Red;
        }
        #Notifications
        {
            padding-left:10px;
        }
        #GfeZeroPercentAlert span.RedisclosingWillClear, #GfeTenPercentAlert span.RedisclosingWillClear
        {
            color: #f2a900;
        }
        #LoanVersionTAlert
        {
            width: 255px;
            white-space: normal;
        }
    </style>
    <form id="LeftSummaryFrame" method="post" runat="server">
      <TABLE id=Table1 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD noWrap valign=bottom height=37>
            <asp:Panel id="panelReadOnly" runat="server" style="PADDING-LEFT:6px;FONT-WEIGHT:bold;FONT-SIZE:14px;COLOR:#ffcc00" Visible="false">
				<ml:EncodedLiteral ID="panelReadOnlyText" runat="server" />
			</asp:Panel>

			<div id="Notifications"> 
			    <div id="APRRedisclosure" style="display: none">
			        <span>Redisclosure Needed: APR changed by </span> <span id="APRDelta">.25</span>
			    </div>
		        <div id="DocumentStatusDiv" style="display:none">
			        <span>Document Audit:</span>
			        <span id="sDocMagicPlanCodeNm"></span>
			        <span id="sDocMagicPlanCodeId" style="display:none">00000000-0000-0000-0000-000000000000</span>
		        </div>
		        <div id="Compliance" runat="server" tabindex="1000">
		            <label class="Risk">Compliance Risk: 
		            <span class="Level">Updating...</span> </label>
		            <a class="Report" href="#">view report</a>
		            <a class="Errors" href="#">view error messages</a>
		        </div>
		        <div id="ComplianceEagleDiv" runat="server" tabindex="1000">
		            <label class="Risk">Compliance Status: 
		            <span class="Level">Updating...</span>
		            </label>
		            <a class="Report" href="#">view results</a>
		            <a class="Errors" href="#">view error messages</a>
		        </div>
		        <div id="GfeZeroPercentAlert" style="display:none">
		            <span class="ErrorMsg" runat="server" id="ToleranceHeader0"></span>
		            <span class="ErrorMsg">0% Tolerance Violation Detected</span>
                    <span class="Redisclose">-- Re-disclose to remove</span>
		        </div>
		        <div id="GfeTenPercentAlert" style="display:none">
		            <span class="ErrorMsg" runat="server" id="ToleranceHeader10"></span>
		            <span class="ErrorMsg">10% Tolerance Violation Detected</span>
                    <span class="Redisclose">-- Re-disclose to remove</span>
		        </div>
                <div id="LoanVersionTAlert" style="display: none">
                    <span id="LoanVersionTMessageShort" class="ErrorMsg"></span>
                    <span style="display: none;" id="LoanVersionTMessageLong" class="ErrorMsg"></span>&nbsp;
                    <a id="LoanVersionAlertMoreInfo" href="javascript:void(0);" onclick="ToggleLoanVersionMessage();" style="display: none;">more info</a>
                </div>
			</div>
            <INPUT id=btnBack type=button value=Back onclick="f_goBack();" tabIndex=-1>
            <INPUT id=btnForward type=button value=Forward onclick="f_goForward();" tabIndex=-1 NoHighlight>
            <INPUT id=btnSave type=button value=Save onclick="f_save();" tabIndex=-1 title="Save" NoHighlight>
            <input id=btnPrint type=button value="Print ..." onclick="f_print();" tabindex=-1 title="Print ..." NoHighlight>
            <% if (!m_hasError) { %>
            <asp:DropDownList id=m_applicantsDDL runat="server" onchange="onApplicantsChange();" tabIndex=-1 NotForEdit></asp:DropDownList>
            <% } %>
          </TD>
          <% if (!m_hasError) { %>
          <td nowrap valign=top height=37>
            <table cellspacing=0 cellpadding=0 border=0 class="LoanInfoTable" id="LoanInfoTablePanel">
              <tr>
                <td class="LoanInfoFieldLabel" nowrap>Loan Officer:</td>
                <td  nowrap><b><span id="LoanOfficer"><ml:EncodedLiteral id="LoanOfficer" runat="server" enableviewstate=false></ml:EncodedLiteral></span></b></td>
                <td class="LoanInfoFieldLabel" nowrap >Status:</td>
                <td  nowrap><b><span id="sStatusT"><ml:EncodedLiteral id="sStatusT" runat="server" enableviewstate=false></ml:EncodedLiteral></span></b></td>
                <td class="LoanInfoFieldLabel">Top:</td>
                <td><b><span id=sQualTopR><ml:EncodedLiteral id=sQualTopR runat="server"></ml:EncodedLiteral></span></b></td>                
                <td class="LoanInfoFieldLabel">LTV:</td>
                <td><b><span id="sLtvR"><ml:EncodedLiteral id=sLtvR runat="server"></ml:EncodedLiteral></span></b></td>    
                <td class="LoanInfoFieldLabel">Rate:</td>
                <td><b><span id="sNoteIR"><ml:EncodedLiteral id=sNoteIR runat="server"></ml:EncodedLiteral></span></b></td>    
                <td class="LoanInfoFieldLabel">Loan Type:</td>
                <td><b><span id="sLT"><ml:EncodedLiteral id=sLT runat="server"></ml:EncodedLiteral></span></b></td>    
                <asp:PlaceHolder runat="server" ID="ExtraTds">
                <td></td>              
                <td></td>
                </asp:PlaceHolder>
              </tr>
              <tr>
                <td class="LoanInfoFieldLabel" nowrap >Loan Num:</td>
                <td nowrap ><b><span id=sLNm><ml:EncodedLiteral id=sLNm runat="server" enableviewstate="false"></ml:EncodedLiteral></span></b></td>
                
                <asp:PlaceHolder ID="RateLockInfoTds" runat=server>
                <td class=LoanInfoFieldLabel nowrap>Rate Lock Status:</td>
                <td nowrap ><b><span id="sRateLockStatusT"><ml:EncodedLiteral id="sRateLockStatusT" runat="server" enableviewstate="False"></ml:EncodedLiteral></span></b></td>              
                </asp:PlaceHolder>
                
                <td class="LoanInfoFieldLabel">Bottom:</td>
                <td nowrap><b><span id="sQualBottomR"><ml:EncodedLiteral id=sQualBottomR runat="server"></ml:EncodedLiteral></span></b></td>
                <td class="LoanInfoFieldLabel">CLTV:</td>
                <td nowrap><b><span id="sCltvR"><ml:EncodedLiteral id=sCltvR runat="server"></ml:EncodedLiteral></span></b></td>        
                <td class=LoanInfoFieldLabel nowrap>HCLTV:</td>
                <td nowrap ><b><span id="sHcltvR"><ml:EncodedLiteral id="sHcltvR" runat="server" enableviewstate="False"></ml:EncodedLiteral></span></b></td>              
                <td class=LoanInfoFieldLabel nowrap>Total Loan Amt:</td>
                <td nowrap ><b><span id="sFinalLAmt"><ml:EncodedLiteral id="sFinalLAmt" runat="server" enableviewstate="False"></ml:EncodedLiteral></span></b></td>              
              </tr>
            </table>
          </td>
          <% } %>
        </TR>
      </TABLE>
    </form>
  </body>
</HTML>
