﻿using System;
using LendersOffice.Conversions.Closing231;
using DataAccess;

namespace LendersOfficeApp.newlos
{
    public partial class ExportToJetDocs : BaseLoanPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ExportToJetDocs));
            dataLoan.InitLoad();
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.BufferOutput = true;
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{dataLoan.sLNm}.xml\"");
            Closing231Exporter exporter = new Closing231Exporter(LoanID, "");
            Response.Write(exporter.Export());
            Response.Flush();
            Response.End();
        }
    }
}
