<%@ Page language="c#" Codebehind="PMLDefaultValues.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.PMLDefaultValues" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="PCF" TagName="ProductCodeFilterPopup" Src="~/newlos/ProductCodeFilterPopup.ascx" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>PMLDefaultValues</title>
    <style type="text/css">
        .CustomPMLFieldLabel
        {
            width: 300px;
        }
        #CustomPMLFieldsTable td
        {
            vertical-align: middle;
        }
        input.CustomPMLField
        {
            width: 90px;
        }
        .Underline
        {
            text-decoration: underline;
        }
        .CustomPmlFieldValue
        {
            width: 250px;
        }
    </style>
  </head>
  <body bgcolor="gainsboro">
  
  <script type="text/javascript">
  function _init() { 
      OnDocTypeChange(null);
      OnsLPurposeTPeChange(null);
    <%if (this.EnableRenovationUi) { %>
    renovationLoanInit();
    <%} %>
  }
  <% if (this.EnableRenovationUi) { %>
  function renovationLoanInit()
  {
    var cb = <%=AspxTools.JsGetElementById(sIsRenovationLoan) %>;
    $(cb).change(onRenovationLoanToggled);
    onRenovationLoanToggled();

    lockField(document.getElementById('sTotalRenovationCostsLckd'), 'sTotalRenovationCosts');
    $("#sTotalRenovationCostsLckd").prop('disabled', ML.IsEnableRenovationCheckboxInPML && !ML.EnableRenovationLoanSupport);
    $("#navigateToRenovLink").toggle(ML.EnableRenovationLoanSupport);
    $("#plainRenovText").toggle(ML.IsEnableRenovationCheckboxInPML && !ML.EnableRenovationLoanSupport);
  }
  function onRenovationLoanToggled()
  {
    var cb = <%=AspxTools.JsGetElementById(sIsRenovationLoan) %>;
    var cost = <%=AspxTools.JsGetElementById(sTotalRenovationCosts) %>;
    var totRenCostsRow = $('#totalRenovationCostsRow');
    totRenCostsRow.toggle($(cb).prop('checked'));
    if(false === $(cb).prop('checked'))
    {
        $(cost).val("$0.00");
    }
  }
  <%} %>
    <%--	OPM 15974 - intern Ethan
	called by custom validators to ensure client/user checks at least one box for each type of filter (Term | Amortization Type) 
	--%>
	function validateTermFilters(source, args) 
    {
		args.IsValid = <%= AspxTools.JsGetElementById(sProdFilterDue10Yrs) %>.checked == true ||
		               <%= AspxTools.JsGetElementById(sProdFilterDue15Yrs) %>.checked == true ||
					   <%= AspxTools.JsGetElementById(sProdFilterDue20Yrs) %>.checked == true ||
					   <%= AspxTools.JsGetElementById(sProdFilterDue25Yrs) %>.checked == true ||
					   <%= AspxTools.JsGetElementById(sProdFilterDue30Yrs) %>.checked == true ||					   
					   <%= AspxTools.JsGetElementById(sProdFilterDueOther) %>.checked == true;
    }
    
    function validateAmortTypeFilters(source, args)
    {
		args.IsValid = <%= AspxTools.JsGetElementById(sProdFilterFinMethFixed) %>.checked == true ||					   
					   <%= AspxTools.JsGetElementById(sProdFilterFinMeth3YrsArm) %>.checked == true ||
					   <%= AspxTools.JsGetElementById(sProdFilterFinMeth5YrsArm) %>.checked == true ||
					   <%= AspxTools.JsGetElementById(sProdFilterFinMeth7YrsArm) %>.checked == true ||
					   <%= AspxTools.JsGetElementById(sProdFilterFinMeth10YrsArm) %>.checked == true ||
					   <%= AspxTools.JsGetElementById(sProdFilterFinMethOther) %>.checked == true;
    }

    function validatePaymentTypeFilters(source, args)
    {
        args.IsValid = $(".PaymentTypeFilterCheck:checked").length > 0;
    }

function OnsLPurposeTPeChange(obj) {
    obj = obj || document.getElementById("sLPurposeTPe");
    if (obj.options[obj.selectedIndex].value == 2){
        $("#sIsStudentLoanCashoutRefi").removeAttr("disabled");
    }
    else {
        $("#sIsStudentLoanCashoutRefi").attr("disabled", true);
    }
}

function OnDocTypeChange(obj) {
    obj = obj || <%= AspxTools.JsGetElementById(sProdDocT) %>;
    if (obj.options[obj.selectedIndex].value == 11) {
        document.getElementById("reservesAvailableLabel").style.display = 'none';
        document.getElementById("reservesAvailableDD").style.display = 'none';
    }
    else {
        document.getElementById("reservesAvailableLabel").style.display = '';
        document.getElementById("reservesAvailableDD").style.display = '';  
    }
}

function goToDeclarations() {
    if (!ML.IsTargeting2019Ulad &&
        parent &&
        parent.treeview &&
        parent.treeview.load &&
        typeof parent.treeview.load === 'function') {
        parent.treeview.load('Declarations.aspx');
    }
}

function f_navigateToRenovLoanPage() {
    if (parent && parent.treeview && parent.treeview.load && typeof parent.treeview.load === 'function') {
        if (ML.isLoanTypeFHA) { // loan type is FHA
            parent.treeview.load('renovation/fha203kmaximummortgage.aspx');
        }    
        else {
            parent.treeview.load('renovation/fanniemaehomestylemaximummortgage.aspx');
        }
    }       
}
      
jQuery(function ($) {
    $("#advancedFilterBtn").click(function () {
        if(typeof OpenProductCodeFilterPopup === 'function')
        {
            OpenProductCodeFilterPopup();
        }
    });

    var timer;
    var delay = 500;
    $("#advancedFilterBtn").hover(
        function (e) {
            var parent = $('#PMLDefaultValues');
            timer = setTimeout(function () {
                if(typeof DisplayTooltip === 'function')
                {
                    DisplayTooltip(e, parent);
                }
            }, delay);
        },
        function () {
            clearTimeout(timer);
            if(typeof HideTooltip === 'function')
            {
                HideTooltip();
            }
        }
    );

    if (typeof (RegisterPostApplyProductCodesCallback) !== 'undefined')
    {
        RegisterPostApplyProductCodesCallback(refreshCalculation);
    }

    $('.see-declarations-item-j-k-container').toggle(!ML.IsTargeting2019Ulad);
});
</script>

	<form id="PMLDefaultValues" method="post" runat="server">
    	<PCF:ProductCodeFilterPopup ID="ProductCodeFilterPopup" runat="server" />
	    <table width="100%" cellspacing=0 cellpadding=0 border=0>
	        <tr>
	            <td nowrap class=MainRightHeader>PML Default Values</td>
            </tr>
      <tr>
        <td>
          <table width="98%" class="InsetBorder">
            <tr><td class="FormTableHeader">1. Enter Credit</td></tr>
            <tr><td class=FormTableSubheader>Credit Information</td></tr>
            <tr><td>
            <table id=Table1 cellSpacing=0 cellPadding=0 border=0>
                <tr>
                    <td class="FieldLabel">Estimated Credit Score</td>
                    <td><asp:TextBox runat="server" ID="sCreditScoreEstimatePe" Width="47px"></asp:TextBox></td>
                </tr>
              <tr>
                <td class="FieldLabel" noWrap>Borrower Credit&nbsp; Scores</td>
                <td noWrap class="FieldLabel">XP: <asp:TextBox id=aBExperianScorePe runat="server" Width="47px"></asp:TextBox></td>
                <td noWrap class="FieldLabel">TU: <asp:TextBox id=aBTransUnionScorePe runat="server" Width="47px"></asp:TextBox></td>
                <td noWrap class="FieldLabel">EF: <asp:TextBox id=aBEquifaxScorePe runat="server" Width="47px"></asp:TextBox>&nbsp;</td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Spouse Credit Scores</td>
                <td noWrap class="FieldLabel">XP: <asp:TextBox id=aCExperianScorePe runat="server" Width="47px"></asp:TextBox></td>
                <td noWrap class="FieldLabel">TU: <asp:TextBox id=aCTransUnionScorePe runat="server" Width="47px"></asp:TextBox></td>
                <td noWrap class="FieldLabel">EF: <asp:TextBox id=aCEquifaxScorePe runat="server" Width="47px"></asp:TextBox></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Total Non-mortgage Debt 
                  Payment&nbsp; </td>
                <td class="FieldLabel" noWrap colSpan=3><ml:MoneyTextBox id=sTransmOMonPmtPe runat="server" width="90" preset="money"></ml:MoneyTextBox>&nbsp;/ 
                  month</td></tr></table></td></tr>
            <tr><td class=FormTableSubheader>Mortgage Delinquency 12 Month Look-Back</td></tr>
            <tr><td>
            <table id=Table2 border=0>
              <tr>
                <td class="FieldLabel" noWrap>Lates (rolling not allowed):</td>
                <td class="FieldLabel" noWrap><asp:dropdownlist id=sProdCrManualNonRolling30MortLateCount runat="server"></asp:dropdownlist>&nbsp;x30</td>
                <td class="FieldLabel" noWrap><asp:dropdownlist id=sProdCrManual60MortLateCount runat="server"></asp:dropdownlist>&nbsp;x60</td>
                <td class="FieldLabel" noWrap><asp:dropdownlist id=sProdCrManual90MortLateCount runat="server"></asp:dropdownlist>&nbsp;x90</td>
                <td class="FieldLabel" noWrap><asp:dropdownlist id=sProdCrManual120MortLateCount runat="server"></asp:dropdownlist>&nbsp;x120</td>
                <td class="FieldLabel" noWrap><asp:dropdownlist id=sProdCrManual150MortLateCount runat="server"></asp:dropdownlist>&nbsp;x150</td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Lates (with rolling allowed):</td>
                <td class="FieldLabel" noWrap><asp:dropdownlist id=sProdCrManual30MortLateCount runat="server"></asp:dropdownlist>&nbsp;x30</td>
				<td class="FieldLabel" noWrap><asp:dropdownlist id="sProdCrManualRolling60MortLateCount" runat="server"></asp:dropdownlist>&nbsp;x60</td>
				<td class="FieldLabel" noWrap><asp:dropdownlist id="sProdCrManualRolling90MortLateCount" runat="server"></asp:dropdownlist>&nbsp;x90</td>                
                
                </tr></table></td></tr>
            <tr><td class=FormTableSubheader>Public Records</td></tr>
            <tr><td>Use the most recent public records 
        only</td></tr>
        <tr>
          <td>
            <table id=Table3 border=0>
              <tr>
                <td class="FieldLabel" noWrap colSpan=2>Check if applicable</td>
                <td class="FieldLabel" noWrap colSpan=2>File Date </td>
                <td class="FieldLabel" noWrap>Status</td>
                <td class="FieldLabel" noWrap colSpan=2>Satisfied Date </td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Foreclosure/NOD:</td>
                <td noWrap><asp:checkbox id=sProdCrManualForeclosureHas runat="server"></asp:checkbox></td>
                <td noWrap><asp:dropdownlist id=sProdCrManualForeclosureRecentFileMon runat="server"></asp:dropdownlist>&nbsp;/ 
<asp:dropdownlist id=sProdCrManualForeclosureRecentFileYr runat="server"></asp:dropdownlist></td>
                <td noWrap></td>
                <td noWrap><asp:dropdownlist id=sProdCrManualForeclosureRecentStatusT runat="server"></asp:dropdownlist></td>
                <td noWrap><asp:dropdownlist id=sProdCrManualForeclosureRecentSatisfiedMon runat="server"></asp:dropdownlist>&nbsp;/ 
<asp:dropdownlist id=sProdCrManualForeclosureRecentSatisfiedYr runat="server"></asp:dropdownlist></td>
                <td noWrap></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Chapter 7:</td>
                <td noWrap><asp:checkbox id=sProdCrManualBk7Has runat="server"></asp:checkbox></td>
                <td noWrap><asp:dropdownlist id=sProdCrManualBk7RecentFileMon runat="server"></asp:dropdownlist>&nbsp;/ 
<asp:dropdownlist id=sProdCrManualBk7RecentFileYr runat="server"></asp:dropdownlist></td>
                <td noWrap></td>
                <td noWrap><asp:dropdownlist id=sProdCrManualBk7RecentStatusT runat="server"></asp:dropdownlist></td>
                <td noWrap><asp:dropdownlist id=sProdCrManualBk7RecentSatisfiedMon runat="server"></asp:dropdownlist>&nbsp;/ 
<asp:dropdownlist id=sProdCrManualBk7RecentSatisfiedYr runat="server"></asp:dropdownlist></td>
                <td noWrap></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Chapter 13:</td>
                <td noWrap><asp:checkbox id=sProdCrManualBk13Has runat="server"></asp:checkbox></td>
                <td noWrap><asp:dropdownlist id=sProdCrManualBk13RecentFileMon runat="server"></asp:dropdownlist>&nbsp;/ 
<asp:dropdownlist id=sProdCrManualBk13RecentFileYr runat="server"></asp:dropdownlist></td>
                <td noWrap></td>
                <td noWrap><asp:dropdownlist id=sProdCrManualBk13RecentStatusT runat="server"></asp:dropdownlist></td>
                <td noWrap><asp:dropdownlist id=sProdCrManualBk13RecentSatisfiedMon runat="server"></asp:dropdownlist>&nbsp;/ 
<asp:dropdownlist id=sProdCrManualBk13RecentSatisfiedYr runat="server"></asp:dropdownlist></td>
                <td noWrap></td></tr></table></td></tr>
        <tr>
          <td></td></tr>
          </table>
        </td>
      </tr>
  <tr>
    <td>
      <table id=Table6 cellSpacing=0 cellPadding=0 width="98%" border=0 class=InsetBorder>
        <tr>
          <td class=FormTableHeader noWrap>2. Applicant</td></tr>
        <tr>
          <td noWrap>
            <table id=Table7 cellSpacing=0 cellPadding=5 width=700 border=0>
              <tr>
                <td colSpan=2>
                  <table id=Table8 cellSpacing=0 cellPadding=0 width=600 
                  border=0>
                    <tr>
                      <td vAlign=top noWrap>
                        <table id=Table9 cellSpacing=0 cellPadding=0>
                          <tr>
                            <td noWrap colSpan=2 class="FieldLabel">Applicant Info</td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>First Name</td>
                            <td noWrap><asp:textbox id=aBFirstNm runat="server" width="160px"></asp:textbox></td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>Middle 
                            Name&nbsp;&nbsp;</td>
                            <td noWrap><asp:textbox id=aBMidNm runat="server" width="160px"></asp:textbox></td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>Last Name</td>
                            <td noWrap><asp:textbox id=aBLastNm runat="server" width="160px"></asp:textbox></td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>Suffix</td>
                            <td noWrap><ml:ComboBox id=aBSuffix runat="server" width="160px"></ml:ComboBox></td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>SSN</td>
                            <td noWrap><ml:ssntextbox id=aBSsn runat="server" width="130px" preset="ssn"></ml:ssntextbox></td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>Citizenship</td>
                            <td class=FieldValue noWrap>
                                <asp:dropdownlist id=aProdBCitizenT runat="server"></asp:dropdownlist>
                                <span class="see-declarations-item-j-k-container">
                                    see <a href="#" onclick="goToDeclarations();">declarations</a> items j & k
                                </span>
                            </td></tr></table></td>
                      <td style="WIDTH: 22px" vAlign=top 
                        noWrap>&nbsp;&nbsp;&nbsp; </td>
                      <td vAlign=top noWrap>
                        <table id=Table10 cellSpacing=0 cellPadding=0>
                          <tbody id=SpousePanel>
                          <tr>
                            <td class="FieldLabel" noWrap colSpan=2>Co-Applicant 
                              Info</td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>First Name</td>
                            <td noWrap><asp:textbox id=aCFirstNm runat="server" width="160px"></asp:textbox></td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>Middle 
                            Name&nbsp;&nbsp;</td>
                            <td noWrap><asp:textbox id=aCMidNm runat="server" width="160px"></asp:textbox></td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>Last Name</td>
                            <td noWrap><asp:textbox id=aCLastNm  runat="server" width="160px"></asp:textbox></td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>Suffix</td>
                            <td noWrap><ml:ComboBox id=aCSuffix runat="server" width="160px"></ml:ComboBox></td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap>SSN</td>
                            <td noWrap><ml:ssntextbox id=aCSsn  runat="server" width="130px" preset="ssn"></ml:ssntextbox></td></tr>
                          <tr>
                            <td class="FieldLabel" noWrap colSpan=2>Spouse is 
                              Primary Wage Earner&nbsp;&nbsp; <asp:checkbox id=aIsBorrSpousePrimaryWageEarner runat="server" Text="Yes"></asp:checkbox></td></tr></TBODY></table></td></tr></table></td></tr>
              <tr>
                <td vAlign=top colSpan=2>
                  <table id=Table11 cellSpacing=0 cellPadding=0 border=0>
                    <tr>
                      <td class="FieldLabel" noWrap width=105>Self Employed</td>
                      <td class="FieldLabel" noWrap width="100%"><asp:checkbox id=sIsSelfEmployed runat="server" Text="Yes"></asp:checkbox></td></tr>
                    <tr>
                      <td class="FieldLabel" noWrap colSpan=2>Total Liquid Assets&nbsp;
                      <ml:moneytextbox id=aLiquidAssetsPe runat="server" width="90px" preset="money"></ml:moneytextbox></td></tr>
                    <tr>
                      <td class="FieldLabel" noWrap colSpan=2>Negative Cash Flow 
                        From Other Properties&nbsp;<ml:moneytextbox id=sOpNegCfPe runat="server" width="90px" preset="money"></ml:moneytextbox>&nbsp;/ 
                        month</td></tr></table></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table id=Table4 cellSpacing=0 cellPadding=0 width="98%" border=0 class=InsetBorder>
        <tr>
          <td noWrap class=FormTableHeader>3. Property 
            &amp; Loan</td></tr>
        <tr>
          <td noWrap>
            <table id=Table5 style="MARGIN-TOP: 5px" cellSpacing=0 cellPadding=0 
            border=0>
              <TBODY>
  <tr>
                <td class=FormTableSubheader noWrap colSpan=5>Property Info</td></tr>
              <tr>
                <td class="FieldLabel" noWrap width=180>Property State</td>
                <td noWrap><ml:statedropdownlist id=sSpStatePe runat="server" IncludeTerritories="false"></ml:statedropdownlist></td>
                <td class="FieldLabel" noWrap width=170 colSpan=2>Property 
                Type</td>
                <td noWrap width=200><asp:dropdownlist id=sProdSpT tabIndex=4 runat="server" width="110px"></asp:dropdownlist></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Property Use</td>
                <td noWrap width=160><asp:dropdownlist id=sOccTPe runat="server" Width="135px"></asp:dropdownlist></td>
                <td class="FieldLabel no-wrap" colspan="2">Not Permanently Affixed?</td>
                <td class="FieldLabel"><asp:CheckBox ID="sIsNotPermanentlyAffixed" runat="server" /></td>
              <tr>
                <td class="FieldLabel" noWrap>Proposed Property Tax</td>
                <td class="FieldLabel" noWrap width=160><ml:moneytextbox id=sProRealETxPe runat="server" width="90" preset="money"></ml:moneytextbox>&nbsp;/ 
                  month</td>
                <td class="FieldLabel" noWrap colSpan=2>Structure Type</td>
                <td class="FieldLabel" noWrap><asp:dropdownlist id=sProdSpStructureT tabIndex=4 runat="server" width="110px"></asp:dropdownlist></td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Other Proposed Housing 
                Expenses&nbsp;</td>
                <td class="FieldLabel" noWrap width=160><ml:moneytextbox id=sProOHExpPe runat="server" width="90px" preset="money"></ml:moneytextbox></td>
                <td class="FieldLabel" noWrap colSpan=2>Condo Stories</td>
                <td class="FieldLabel" noWrap><asp:textbox id=sProdCondoStories style="TEXT-ALIGN: right" tabIndex=4 runat="server" width="28px" maxlength="3"></asp:textbox></td></tr>
              <tr>
                <td class="FieldLabel" noWrap><SPAN 
                  id=InvestmentPropertyPanel_0>Occupancy Rate (%)</SPAN></td>
                <td noWrap><SPAN id=InvestmentPropertyPanel_1><ml:percenttextbox id=sOccRPe runat="server" width="90px" preset="percent"></ml:percenttextbox></SPAN></td>
                <td class="FieldLabel" noWrap colSpan=2>Is In Rural Area</td>
                <td class="FieldLabel" noWrap><asp:checkbox id=sProdIsSpInRuralArea tabIndex=4 runat="server" text="Yes"></asp:checkbox></td></tr>
              <tr>
                <td class="FieldLabel" noWrap><SPAN 
                  id=InvestmentPropertyPanel_2>Gross Rent</SPAN></td>
                <td noWrap><SPAN id=InvestmentPropertyPanel_3><ml:moneytextbox id=sSpGrossRentPe runat="server" width="90px" preset="money"></ml:moneytextbox></SPAN></td>
                <td class="FieldLabel" noWrap colSpan=2>Is Condotel</td>
                <td class="FieldLabel" noWrap><asp:checkbox id=sProdIsCondotel tabIndex=4 runat="server" text="Yes"></asp:checkbox></td></tr>
              <tr>
                <td class="FieldLabel" colspan="2" noWrap></td>
                <td class="FieldLabel" noWrap colSpan=2>Is Non-Warrantable Proj&nbsp;&nbsp;</td>
                <td class="FieldLabel"><asp:checkbox id=sProdIsNonwarrantableProj tabIndex=4 runat="server" text="Yes"></asp:checkbox></td></tr>
              <tr>
                <td class="FieldLabel" noWrap></td>
                <td noWrap></td>
                <td class="FieldLabel" noWrap colSpan=2></td>
                <td class="FieldLabel"></td>
              </tr>
              <TBODY id=HousingExpensePanel>
              <tr>
                <td colSpan=5><IMG height=12 src="../images/spacer.gif"></td>
              </tr>
              <tr>
                <td class=FormTableSubheader noWrap colSpan=5>Present Housing 
                Expense</td></tr>
              <tr>
                <td class="FieldLabel" noWrap>Present Housing 
                  Expense&nbsp;&nbsp;&nbsp;</td>
                <td noWrap><ml:moneytextbox id=aPresOHExpPe tabIndex=5 runat="server" width="90px" preset="money"></ml:moneytextbox></td>
                <td noWrap></td>
                <td noWrap></td>
                <td noWrap></td></tr></TBODY>
              <tr>
                <td colSpan=5><IMG height=12 src="../images/spacer.gif"></td></tr>
              <tr>
                <td class=FormTableSubheader noWrap colSpan=5>Loan Info</td></tr>
              <tr>
                <td class="FieldLabel" style="WIDTH: 140px" noWrap>Loan Purpose</td>
                <td noWrap><asp:dropdownlist onchange="OnsLPurposeTPeChange(this)" id=sLPurposeTPe tabIndex=7 runat="server" width="135px"></asp:dropdownlist></td>
                <td class="FieldLabel" noWrap>Doc Type</td>
                <td class="FieldLabel" noWrap colSpan=2><asp:dropdownlist onchange="OnDocTypeChange(this)" id=sProdDocT tabIndex=10 runat="server" width="290px"></asp:dropdownlist></td></tr>
              <tr>
                  <td class="FieldLabel">Student Loan Cashout?</td>
                  <td><asp:CheckBox ID="sIsStudentLoanCashoutRefi" runat="server"/></td>
              </tr>
              <tr>
                <td class="FieldLabel" noWrap>Cashout Amount</td>
                <td noWrap><ml:moneytextbox id=sProdCashoutAmt tabIndex=7 runat="server" width="90px" preset="money"></ml:moneytextbox></td>
                <td class="FieldLabel" noWrap>Total Income</td>
                <td class="FieldLabel" noWrap colSpan=2><ml:moneytextbox id=sPrimAppTotNonspIPe tabIndex=10 runat="server" width="90" preset="money"></ml:moneytextbox>&nbsp;/ 
                  month</td></tr>
                  <%if (this.EnableRenovationUi) { %>
              <tr>
                <td class="FieldLabel" noWrap>Is Renovation Loan?</td>
                <td class="FieldLabel" noWrap><asp:checkbox id=sIsRenovationLoan tabIndex=7 runat="server" Text="Yes"></asp:checkbox></td>
                <td colspan=2></td>
                <td colSpan=2></td></tr>
              <tr id="totalRenovationCostsRow" style="display:none">
                <td class="FieldLabel" noWrap>
                    <a id="navigateToRenovLink" href="#" onclick="return f_navigateToRenovLoanPage();">Total Renovation Costs</a>
                    <span id="plainRenovText">Total Renovation Costs</span>
                </td>
                <td noWrap>
                    <ml:moneytextbox id=sTotalRenovationCosts tabIndex=7 runat="server" width="90px" preset="money" onchange="refreshCalculation();"></ml:moneytextbox>
                    <asp:CheckBox ID="sTotalRenovationCostsLckd" runat="server" onchange="refreshCalculation()" onclick="refreshCalculation();" />
                </td>
                <td colspan=2></td>
                <td colSpan=2></td>
              </tr>
                <%} %>
              <tr>
                <td class="FieldLabel" style="HEIGHT: 17px" noWrap>First Time Home Buyer</td>
                <td class="FieldLabel" style="HEIGHT: 17px" noWrap><asp:checkbox id=sHas1stTimeBuyerPe disabled tabIndex=7 runat="server" Text="Yes"></asp:checkbox></td>
                <td class="FieldLabel" noWrap style="<%= AspxTools.HtmlString(m_isShowsProdEstimatedResidualI ? "DISPLAY:block" : "DISPLAY:none")%>">Estimated Residual Income</td>
                <td class="FieldLabel" noWrap colSpan=2 style="<%= AspxTools.HtmlString(m_isShowsProdEstimatedResidualI ? "DISPLAY:block" : "DISPLAY:none")%>"><ml:moneytextbox id="sProdEstimatedResidualI" tabIndex=10 runat="server" width="90" preset="money"></ml:moneytextbox>&nbsp;/ 
                  month</td>                
              </tr>
              <tr>
                <td class="FieldLabel" style="height: 17px" noWrap>Number of Financed Properties</td>
                <td class="FieldLabel" style="height: 17px" noWrap><asp:TextBox ID="sNumFinancedProperties" runat="server"></asp:TextBox></td>
                <td class="FieldLabel" style="height: 17px" noWrap></td>
                <td class="FieldLabel" style="height: 17px" noWrap></td>
              </tr>
              <tr>
                <td class="FieldLabel" noWrap>Impound</td>
                <td class="FieldLabel" noWrap><asp:checkbox id=sProdImpound tabIndex=7 runat="server" Text="Yes"></asp:checkbox></td>
                <td class="FieldLabel" noWrap>Rate Lock Period</td>
                <td class="FieldLabel" noWrap colSpan=2><asp:textbox id=sProdRLckdDays style="TEXT-ALIGN: right" tabIndex=10 runat="server" Width="44px"></asp:textbox>&nbsp;days</td>
              </tr>
              <tr>
                    <% if (m_isAutoPmiOrNewPml) { %>
                    <td class="FieldLabel" noWrap>Conv Loan PMI Type</td>
                    <td class="FieldLabel" noWrap><asp:dropdownlist id="sProdConvMIOptionT" runat="server" /></td>
                    <% } else { %>
                    <td class="FieldLabel" noWrap>Mortgage Insurance</td>
                    <td class="FieldLabel" noWrap><asp:dropdownlist id="sProdMIOptionT" runat="server" /></td>
                    <% } %>
                <td class="FieldLabel" noWrap><span id="reservesAvailableLabel">Reserves Available</span>&nbsp;</td>
                <td class="FieldLabel" noWrap colSpan=2><span id="reservesAvailableDD"><asp:DropDownList id=sProdAvailReserveMonths tabIndex=10 runat="server" Width="140px"></asp:DropDownList></span>&nbsp;</td>
			</tr>
			<% if (m_isAutoPmiOrNewPml) { %>
			<td class="FieldLabel">Split MI Upfront Premium</td>
			<td class="FieldLabel"><asp:DropDownList id="sConvSplitMIRT" tabIndex=10 runat="server" ></asp:DropDownList></td>
			<td colspan="2">&nbsp;</td>
			<% } %>
              <tr>
                <td class="FieldLabel">Is Credit Qualifying?</td>
                <td class="FieldLabel"><asp:CheckBox ID="sIsCreditQualifying" runat="server" Text="Yes" /></td>
                <td colspan="2">&nbsp;</td>
              </tr>
              <asp:PlaceHolder runat="server" ID="LenderFeeBuyoutPlaceHolder">
              <tr>
                <td colSpan=5><IMG height=12 src="../images/spacer.gif"></td></tr>
                <tr>
                    <td class="FormTableSubHeader" colspan="5">
                        Other Info
                    </td>  
                </tr>
                <tr>
                    <td class="FieldLabel"> Lender Fee Buyout </td>
                    <td colspan="4"> <asp:DropDownList id="sLenderFeeBuyoutRequestedT" runat="server"/>  </td>
                </tr>
               </asp:PlaceHolder>
              <tr>
                <td colSpan=5><IMG height=12 src="../images/spacer.gif"></td></tr>
              
              <tr>
                <td class=FormTableSubHeader colSpan=5>AUS Options</td></tr>
              <tr>
                <td class="FieldLabel">AUS Response</td><td colspan=4 class="FieldLabel"> <asp:DropDownList id=sProd3rdPartyUwResultT runat="server">
</asp:DropDownList></td></tr>
              <tr>
                <td class="FieldLabel">AUS Processing Type</td>
                <td colspan=4 class="FieldLabel">
                <asp:CheckBox ID="sProdIncludeNormalProc" Runat="server" Text="Conventional" TabIndex="12"></asp:CheckBox>&nbsp;&nbsp;
      <asp:CheckBox ID="sProdIncludeMyCommunityProc" Runat="server" Text="My Community / HomeReady" TabIndex="12"></asp:CheckBox>&nbsp;&nbsp;
      <asp:CheckBox ID="sProdIncludeHomePossibleProc" Runat="server" Text="Home Possible"  TabIndex="12"></asp:CheckBox>&nbsp;&nbsp;
      <asp:CheckBox ID="sProdIncludeFHATotalProc" Runat="server" Text="FHA Total"  TabIndex="12"></asp:CheckBox>&nbsp;&nbsp;
      <asp:CheckBox ID="sProdIncludeUSDARuralProc" Runat="server" Text="USDA"  TabIndex="12"></asp:CheckBox>
      <asp:CheckBox ID="sProdIncludeVAProc" Runat="server" Text="VA"  TabIndex="12"></asp:CheckBox>
                
                </td>
              </tr>
              <tr>
                <td colSpan=5><IMG height=12 src="../images/spacer.gif"></td></tr>
              <tr>
                <td colSpan=5></td></tr>
              <tr>
                <td colSpan=5><IMG height=12 src="../images/spacer.gif"></td></tr>
              <tr>
                <td class=FormTableSubHeader colSpan=5>Filters & Sort</td></tr>
			<tr>
			<td class="FieldLabel">Term&nbsp;<IMG id="m_TermReqIcon" src="../images/require_icon.gif"></td>
			<td class="FieldLabel" colspan="4">
			    <asp:CheckBox id="sProdFilterDue10Yrs" runat="server" Text="10yr" class="DueFilterCheck" />
				<asp:CheckBox id="sProdFilterDue15Yrs" runat="server" Text="15yr" class="DueFilterCheck" />
				<asp:CheckBox id="sProdFilterDue20Yrs" runat="server" Text="20yr" class="DueFilterCheck" />
				<asp:CheckBox id="sProdFilterDue25Yrs" runat="server" Text="25yr" class="DueFilterCheck" />
				<asp:CheckBox id="sProdFilterDue30Yrs" runat="server" Text="30yr" class="DueFilterCheck" />				
				<asp:CheckBox id="sProdFilterDueOther" runat="server" Text="Other" class="DueFilterCheck" />  
				<asp:customvalidator id=TermFiltersValidator runat="server" Display="Dynamic" ClientValidationFunction="validateTermFilters">
                    <span runat="server">
                        <img src="../images/error_icon.gif"> Select at least one
                    </span>
				</asp:customvalidator>
			</td>
			</tr>
			<tr>
			<td class="FieldLabel">Amortization Type&nbsp;<IMG id="m_AmorTypeReqIcon" src="../images/require_icon.gif" ></td>
			<td class="FieldLabel" colspan="4">
				<asp:CheckBox id="sProdFilterFinMethFixed" runat="server" Text="Fixed" class="AmortFilterCheck" />
				<asp:CheckBox id="sProdFilterFinMethOptionArm" runat="server" Text="Option ARM" class="AmortFilterCheck" Visible="False" />				
				<asp:CheckBox id="sProdFilterFinMeth3YrsArm" runat="server" Text="3Yrs ARM" class="AmortFilterCheck" />
				<asp:CheckBox id="sProdFilterFinMeth5YrsArm" runat="server" Text="5Yrs ARM" class="AmortFilterCheck" />
				<asp:CheckBox id="sProdFilterFinMeth7YrsArm" runat="server" Text="7Yrs ARM" class="AmortFilterCheck" />
				<asp:CheckBox id="sProdFilterFinMeth10YrsArm" runat="server" Text="10Yrs ARM" class="AmortFilterCheck" />
				<asp:CheckBox id="sProdFilterFinMethOther" runat="server" Text="Other" class="AmortFilterCheck" />
				<asp:customvalidator id="AmortTypeFitersValidator" runat="server" Display="Dynamic" ClientValidationFunction="validateAmortTypeFilters">
                    <span runat="server">
                        <img src="../images/error_icon.gif"> Select at least one
                    </span>
				</asp:customvalidator>
			</td>
			</tr>
            <tr>
			    <td class="FieldLabel">Payment Type&nbsp;<IMG id="m_PaymentTypeReqIcon" src="../images/require_icon.gif" ></td>
			    <td class="FieldLabel" colspan="4">
				    <input type="checkbox" id="sProdFilterPmtTPI" runat="server" class="PaymentTypeFilterCheck" /><label for="sProdFilterPmtTPI">P&I</label>
				    <input type="checkbox" id="sProdFilterPmtTIOnly" runat="server" class="PaymentTypeFilterCheck" /><label for="sProdFilterPmtTPI">I/O</label>

				    <asp:customvalidator id="PaymentTypeFitersValidator" runat="server" Display="Dynamic" ClientValidationFunction="validatePaymentTypeFilters">
                        <span runat="server">
                            <img src="../images/error_icon.gif"> Select at least one
                        </span>
				    </asp:customvalidator>
			    </td>
			</tr>
            <tr>
                <td>
                    <a runat="server" id="advancedFilterBtn">Advanced Filter Options</a>
                </td>
            </tr>
			<tr>
			    <td class="FormTableSubHeader" colspan="5">Custom PML Fields</td>
			</tr>
			<tr>
			    <td colspan="5">
			        <%-- This table is populated server-side based on the Broker's configuration. --%>
                    <table id="CustomPMLFieldsTable" runat="server">
                        <tr>
                            <td class="FieldLabel Underline">Description</td>
                            <td class="FieldLabel Underline CustomPmlFieldValue">Value</td>
                            <td class="FieldLabel Underline">Visible for:</td>
                        </tr>
                    </table>
			    </td>
			</tr>

                </table>
                
                </td></tr></table></td></tr>
    </table>
     </form>
	
  </body>
</HTML>
