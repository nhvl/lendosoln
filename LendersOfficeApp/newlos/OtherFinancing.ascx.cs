using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
namespace LendersOfficeApp.newlos
{

	public partial  class OtherFinancing : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
	{
        protected bool m_hasLinkedLoan = false;
        protected Guid m_linkedLoanID = Guid.Empty;
        protected E_sLienPosT m_sLienPosT;
        protected bool m_isSubFinCreditLine;
        protected bool m_isUseLayeredFinance = false;
        protected bool m_isPurchase = false;

		protected void PageInit(object sender, System.EventArgs e)
		{
            Tools.Bind_sFinMethT(sOtherLFinMethT);
            Tools.Bind_sFinMethT(sOtherLFinMethT_2);
		}
        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency( LoanID, typeof(OtherFinancing));
            dataLoan.InitLoad();
            m_sLienPosT = dataLoan.sLienPosT;
            m_isSubFinCreditLine = dataLoan.sSubFinT == E_sSubFinT.Heloc;
            m_isUseLayeredFinance = dataLoan.BrokerDB.IsUseLayeredFinancing;
            m_isPurchase = dataLoan.sLPurposeT == E_sLPurposeT.Purchase;

            m_hasLinkedLoan = dataLoan.sLinkedLoanInfo.IsLoanLinked;
            if (m_hasLinkedLoan) 
            {
                m_linkedLoanID = dataLoan.sLinkedLoanInfo.LinkedLId;
                sLinkedLoanInfo_LinkedLNm.Text = dataLoan.sLinkedLoanInfo.LinkedLNm;

                // OPM 217312 - Determin if linked loan is lead.
                CPageData linkedLoan = new CPageData(m_linkedLoanID, new string[] { "sStatusT" });

                bool isLinkedLead = false;
                try
                {
                    linkedLoan.InitLoad();
                    isLinkedLead = Tools.IsStatusLead(linkedLoan.sStatusT);
                }
                catch (PageDataLoadDenied)
                {
                    LinkedLoanButtonsPlaceHolder.Visible = false;
                }
                catch (AccessDenied)
                {
                    LinkedLoanButtonsPlaceHolder.Visible = false;
                }

                ((BasePage)Page).RegisterJsGlobalVariables("isLinkedLead", isLinkedLead );
            }


            if (dataLoan.sLienPosT == E_sLienPosT.First) 
            {
                sSubFin.Text       = dataLoan.sSubFin_rep;
                sConcurSubFin.Text = dataLoan.sConcurSubFin_rep;
                sSubFinPmt.Text    = dataLoan.sSubFinPmt_rep;
                sSubFinPmtLckd.Checked = dataLoan.sSubFinPmtLckd;
                sSubFinIR.Text     = dataLoan.sSubFinIR_rep;
                sSubFinTerm.Text   = dataLoan.sSubFinTerm_rep;
                sSubFinMb.Text     = dataLoan.sSubFinMb_rep;
                // Disable first mortgage related fields.
                s1stMtgOrigLAmt.ReadOnly = true;
                sRemain1stMBal.ReadOnly = true;
                sRemain1stMPmt.ReadOnly = true;
                sIsIOnlyForSubFin.Checked = dataLoan.sIsIOnlyForSubFin;
                Tools.SetDropDownListValue(sOtherLFinMethT, dataLoan.sOtherLFinMethT);
                sIsOFinNew.Checked = dataLoan.sIsOFinNew;
                sIsOFinCreditLineInDrawPeriod.Checked = dataLoan.sIsOFinCreditLineInDrawPeriod;
                sLayeredTotalForgivableBalance.Text = dataLoan.sLayeredTotalForgivableBalance_rep;

            } 
            else 
            {
                s1stMtgOrigLAmt.Text = dataLoan.s1stMtgOrigLAmt_rep;
                sRemain1stMBal.Text  = dataLoan.sRemain1stMBal_rep;
                sRemain1stMPmt.Text  = dataLoan.sRemain1stMPmt_rep;

                // Disable second mortgage related fields.
                sSubFin.ReadOnly = true;
                sConcurSubFin.ReadOnly = true;
                sSubFinPmt.ReadOnly = true;
                sSubFinPmtLckd.Enabled = false;
                sSubFinIR.ReadOnly = true;
                sSubFinTerm.ReadOnly = true;
                sSubFinMb.ReadOnly = true;
                sIsIOnlyForSubFin.Enabled = false;
                sIsIOnlyForSubFin_2.Checked = dataLoan.sIsIOnlyForSubFin;
                Tools.SetDropDownListValue(sOtherLFinMethT_2, dataLoan.sOtherLFinMethT);
                sIsOFinNew_2.Checked = dataLoan.sIsOFinNew;
                sLpIsNegAmortOtherLien.Checked = dataLoan.sLpIsNegAmortOtherLien;

            }

        }

        public void SaveData() 
        {
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
