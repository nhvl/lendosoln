﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using LendersOffice.Constants;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.newlos
{
    public partial class ContinuationData : BaseLoanPage
    {
       

        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ContinuationData));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            a1003ContEditSheet.Text = dataApp.a1003ContEditSheet;
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "ContinuationData";
            this.PageID = "ContinuationData";
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
