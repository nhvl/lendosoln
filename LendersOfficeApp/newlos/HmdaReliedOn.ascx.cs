﻿#region Generated Code
namespace LendersOfficeApp.newlos
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.Hmda;

    /// <summary>
    /// User control for Hmda Relied On.
    /// </summary>
    public partial class HmdaReliedOn : BaseLoanUserControl
    {
        /// <summary>
        /// Data Loan for the user control.
        /// </summary>
        private CPageData dataLoan;

        /// <summary>
        /// Loads the HMDA Relied On data.
        /// </summary>
        /// <param name="dataLoan">The loan to load from.</param>
        /// <param name="dataApp">The app to load from.</param>
        /// <param name="serviceItem">The background service item.</param>
        /// <param name="controlPrefix">The prefix that the control will have.</param>
        public static void LoadHmdaReliedOnData(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem, string controlPrefix)
        {
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnTotalIncome"), dataLoan.sReliedOnTotalIncome_rep);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnDebtRatio"), dataLoan.sReliedOnDebtRatio_rep);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCombinedLTVRatio"), dataLoan.sReliedOnCombinedLTVRatio_rep);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnPropertyValue"), dataLoan.sReliedOnPropertyValue_rep);

            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sHmdaIsIncomeReliedOn"), dataLoan.sHmdaIsIncomeReliedOn);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sHmdaIsDebtRatioReliedOn"), dataLoan.sHmdaIsDebtRatioReliedOn);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sHmdaIsCombinedRatioReliedOn"), dataLoan.sHmdaIsCombinedRatioReliedOn);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sHmdaIsPropertyValueReliedOn"), dataLoan.sHmdaIsPropertyValueReliedOn);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sHmdaIsCreditScoreReliedOn"), dataLoan.sHmdaIsCreditScoreReliedOn);

            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnTotalIncomeLckd"), dataLoan.sReliedOnTotalIncomeLckd);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnDebtRatioLckd"), dataLoan.sReliedOnDebtRatioLckd);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCombinedLTVRatioLckd"), dataLoan.sReliedOnCombinedLTVRatioLckd);
            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnPropertyValueLckd"), dataLoan.sReliedOnPropertyValueLckd);

            serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModeT"), dataLoan.sReliedOnCreditScoreModeT);

            if (dataLoan.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.SingleScore)
            {
                serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreSourceT"), dataLoan.sReliedOnCreditScoreSourceT);
                serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreSingleHmdaReportAsT"), dataLoan.sReliedOnCreditScoreSingleHmdaReportAsT);
                serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScore"), dataLoan.sReliedOnCreditScore_rep);

                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
                {
                    serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModelT"), dataLoan.sReliedOnCreditScoreModelT);
                    serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModelTOtherDescription"), dataLoan.sReliedOnCreditScoreModelTOtherDescription);
                    serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModelTLckd"), dataLoan.sReliedOnCreditScoreModelTLckd);
                }
                else
                {
                    serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModelName"), dataLoan.sReliedOnCreditScoreModelName);
                    serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModelNameLckd"), dataLoan.sReliedOnCreditScoreModelNameLckd);
                }

                serviceItem.SetResult(Tools.ConstructControlKey(controlPrefix, "HmdaReportAs"), dataLoan.GetDefaultReliedOnCreditScoreSingleHmdaReportAsT());
            }
        }

        /// <summary>
        /// Saves HMDA Relied On.
        /// </summary>
        /// <param name="dataLoan">The loan to save to.</param>
        /// <param name="dataApp">The app to save to.</param>
        /// <param name="serviceItem">The service page item.</param>
        /// <param name="controlPrefix">The prefix that the control will have.</param>
        public static void BindHmdaReliedOnData(CPageData dataLoan, CAppData dataApp, AbstractBackgroundServiceItem serviceItem, string controlPrefix)
        {
            dataLoan.sReliedOnTotalIncome_rep = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "sReliedOnTotalIncome"));
            dataLoan.sReliedOnDebtRatio_rep = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "sReliedOnDebtRatio"));
            dataLoan.sReliedOnCombinedLTVRatio_rep = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "sReliedOnCombinedLTVRatio"));
            dataLoan.sReliedOnPropertyValue_rep = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "sReliedOnPropertyValue"));

            dataLoan.sHmdaIsIncomeReliedOn = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sHmdaIsIncomeReliedOn"));
            dataLoan.sHmdaIsDebtRatioReliedOn = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sHmdaIsDebtRatioReliedOn"));
            dataLoan.sHmdaIsCombinedRatioReliedOn = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sHmdaIsCombinedRatioReliedOn"));
            dataLoan.sHmdaIsPropertyValueReliedOn = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sHmdaIsPropertyValueReliedOn"));
            dataLoan.sHmdaIsCreditScoreReliedOn = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sHmdaIsCreditScoreReliedOn"));

            dataLoan.sReliedOnTotalIncomeLckd = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sReliedOnTotalIncomeLckd"));
            dataLoan.sReliedOnDebtRatioLckd = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sReliedOnDebtRatioLckd"));
            dataLoan.sReliedOnCombinedLTVRatioLckd = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sReliedOnCombinedLTVRatioLckd"));
            dataLoan.sReliedOnPropertyValueLckd = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sReliedOnPropertyValueLckd"));

            dataLoan.sReliedOnCreditScoreModeT = serviceItem.GetEnum<ReliedOnCreditScoreMode>(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModeT"));

            if (dataLoan.sReliedOnCreditScoreModeT == ReliedOnCreditScoreMode.SingleScore)
            {
                dataLoan.sReliedOnCreditScoreSourceT = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreSourceT"));
                int reportAs = serviceItem.GetInt(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreSingleHmdaReportAsT"), 0);
                dataLoan.sReliedOnCreditScoreSingleHmdaReportAsT = (sHmdaReportCreditScoreAsT)reportAs;
                dataLoan.sReliedOnCreditScore_rep = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScore"));

                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
                {
                    dataLoan.sReliedOnCreditScoreModelT = (E_CreditScoreModelT)serviceItem.GetInt(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModelT"));
                    dataLoan.sReliedOnCreditScoreModelTOtherDescription = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModelTOtherDescription"));
                    dataLoan.sReliedOnCreditScoreModelTLckd = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModelTLckd"));
                }
                else
                {
                    dataLoan.sReliedOnCreditScoreModelName = serviceItem.GetString(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModelName"));
                    dataLoan.sReliedOnCreditScoreModelNameLckd = serviceItem.GetBool(Tools.ConstructControlKey(controlPrefix, "sReliedOnCreditScoreModelNameLckd"));
                }
            }
        }

        /// <summary>
        /// Initializes the control.
        /// </summary>
        /// <param name="sender">Caller of this method.</param>
        /// <param name="e">Any event data.</param>
        protected void PageInit(object sender, EventArgs e)
        {
            var page = this.Page as BaseLoanPage;
            if (page == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "HmdaReliedOn can only be used on a BaseLoanPage");
            }

            page.RegisterJsScript("HmdaReliedOn.js");
            page.RegisterJsGlobalVariables("prefix", $"{nameof(HmdaReliedOn)}_");

            page.RegisterJsGlobalVariables("OtherCreditSource", E_aDecisionCreditSourceT.Other.ToString("D"));
            page.RegisterJsGlobalVariables("CreditScoreReliedOnSourceOther", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Other]);
            page.RegisterJsGlobalVariables("CreditScoreReliedOnSourceBlank", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Blank]);
            page.RegisterJsGlobalVariables("CreditScoreReliedOnSourceType1", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type1]);
            page.RegisterJsGlobalVariables("CreditScoreReliedOnSourceType2", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2]);
            page.RegisterJsGlobalVariables("CreditScoreReliedOnSourceType2Soft", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type2Soft]);
            page.RegisterJsGlobalVariables("CreditScoreReliedOnSourceType3", HmdaReliedOnData.ReliedOnCreditScoreSourceToGuidMap[ReliedOnCreditScoreSource.Type3]);

            page.RegisterJsGlobalVariables("CreditScoreModelT_LeaveBlank", E_CreditScoreModelT.LeaveBlank.ToString("D"));
            page.RegisterJsGlobalVariables("CreditScoreModelT_Other", E_CreditScoreModelT.Other.ToString("D"));

            this.dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(HmdaReliedOn));
            this.dataLoan.InitLoad();

            List<CAppData> apps = this.dataLoan.Apps.ToList();

            for (int i = 0; i < this.dataLoan.nApps; i++)
            {
                if (apps[i].aAppId == this.ApplicationID)
                {
                    page.RegisterJsGlobalVariables("currentAppIndex", i.ToString("00"));
                }
            } 

            Tools.Bind_sReliedOnCreditScoreModeT(this.sReliedOnCreditScoreModeT);
            Tools.Bind_sReliedOnCreditScoreSourceT(this.sReliedOnCreditScoreSourceT, this.dataLoan.Apps);

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.dataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
            {
                Tools.Bind_CreditModelT(this.sReliedOnCreditScoreModelT);
            }
            else
            {
                Tools.BindComboBox_CreditModelName(this.sReliedOnCreditScoreModelName);
            }
        }

        /// <summary>
        /// Loads data for the control.
        /// </summary>
        /// <param name="sender">Caller of this method.</param>
        /// <param name="e">Any event data.</param>
        protected void PageLoad(object sender, EventArgs e)
        {
            Tools.SetDropDownListValue(this.sReliedOnCreditScoreModeT, this.dataLoan.sReliedOnCreditScoreModeT);
            Tools.SetDropDownListValue(this.sReliedOnCreditScoreSourceT, this.dataLoan.sReliedOnCreditScoreSourceT);
            Tools.SetRadioButtonListValue(this.sReliedOnCreditScoreSingleHmdaReportAsT, this.dataLoan.sReliedOnCreditScoreSingleHmdaReportAsT);

            this.sReliedOnTotalIncome.Text = this.dataLoan.sReliedOnTotalIncome_rep;
            this.sReliedOnDebtRatio.Text = this.dataLoan.sReliedOnDebtRatio_rep;
            this.sReliedOnCombinedLTVRatio.Text = this.dataLoan.sReliedOnCombinedLTVRatio_rep;
            this.sReliedOnPropertyValue.Text = this.dataLoan.sReliedOnPropertyValue_rep;

            this.sHmdaIsIncomeReliedOn.Checked = this.dataLoan.sHmdaIsIncomeReliedOn;
            this.sHmdaIsDebtRatioReliedOn.Checked = this.dataLoan.sHmdaIsDebtRatioReliedOn;
            this.sHmdaIsCombinedRatioReliedOn.Checked = this.dataLoan.sHmdaIsCombinedRatioReliedOn;
            this.sHmdaIsPropertyValueReliedOn.Checked = this.dataLoan.sHmdaIsPropertyValueReliedOn;
            this.sHmdaIsCreditScoreReliedOn.Checked = this.dataLoan.sHmdaIsCreditScoreReliedOn;

            this.sReliedOnTotalIncomeLckd.Checked = this.dataLoan.sReliedOnTotalIncomeLckd;
            this.sReliedOnDebtRatioLckd.Checked = this.dataLoan.sReliedOnDebtRatioLckd;
            this.sReliedOnCombinedLTVRatioLckd.Checked = this.dataLoan.sReliedOnCombinedLTVRatioLckd;
            this.sReliedOnPropertyValueLckd.Checked = this.dataLoan.sReliedOnPropertyValueLckd;

            this.sReliedOnCreditScore.Text = this.dataLoan.sReliedOnCreditScore_rep;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.dataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
            {
                this.sReliedOnCreditScoreModelLegacySection.Visible = false;
                Tools.SetDropDownListValue(this.sReliedOnCreditScoreModelT, this.dataLoan.sReliedOnCreditScoreModelT);
                this.sReliedOnCreditScoreModelTOtherDescription.Text = this.dataLoan.sReliedOnCreditScoreModelTOtherDescription;
                this.sReliedOnCreditScoreModelTLckd.Checked = this.dataLoan.sReliedOnCreditScoreModelTLckd;
            }
            else
            {
                this.sReliedOnCreditScoreModelLoanVersion26Section.Visible = false;
                this.sReliedOnCreditScoreModelName.Text = this.dataLoan.sReliedOnCreditScoreModelName;
                this.sReliedOnCreditScoreModelNameLckd.Checked = this.dataLoan.sReliedOnCreditScoreModelNameLckd;
            }

            HmdaReportAs.Value = this.dataLoan.GetDefaultReliedOnCreditScoreSingleHmdaReportAsT().ToString("D");

            var appRepeaterDataSource = this.dataLoan.Apps.Select((app, index) => new AppCreditScoreRepeaterData()
            {
                Index = index + 1,
                Application = app
            });

            this.ApplicantCreditScores.DataSource = appRepeaterDataSource;
            this.ApplicantCreditScores.DataBind();
        }

        /// <summary>
        /// Event handler for applicant credit scores repeater.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void ApplicantCreditScores_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var dataItem = e.Item.DataItem as AppCreditScoreRepeaterData;
            if (dataItem == null)
            {
                return;
            }

            var borrAppRow = e.Item.FindControl("AppCreditScoreBorrowerRow") as TableRow;
            var borrAppLabel = e.Item.FindControl("AppCreditScoreBorrowerLabel") as Literal;
            var borrowerDecisionCreditSourceTLckd = e.Item.FindControl("aBDecisionCreditSourceTLckd") as CheckBox;
            var borrowerDecisionCreditSourceT = e.Item.FindControl("aBDecisionCreditSourceT") as DropDownList;
            var borrowerDecisionCreditScore = e.Item.FindControl("aBDecisionCreditScore") as TextBox;
            var borrowerDecisionCreditModelName = e.Item.FindControl("aBDecisionCreditModelName") as TextBox;
            var borrowerIsDecisionCreditModelAValidHmdaModel = e.Item.FindControl("aBIsDecisionCreditModelAValidHmdaModel") as CheckBox;

            if (dataItem.Application.aBIsValidNameSsn)
            {
                borrAppLabel.Text = "App " + dataItem.Index + " Borrower's Decision Score";
                borrowerDecisionCreditSourceTLckd.Checked = dataItem.Application.aBDecisionCreditSourceTLckd;
                Tools.Bind_aDecisionCreditSourceT(borrowerDecisionCreditSourceT);
                Tools.SetDropDownListValue(borrowerDecisionCreditSourceT, dataItem.Application.aBDecisionCreditSourceT);
                borrowerDecisionCreditScore.Text = dataItem.Application.aBDecisionCreditScore_rep;

                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.dataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
                {
                    borrowerIsDecisionCreditModelAValidHmdaModel.Checked = dataItem.Application.aBIsDecisionCreditModelAValidHmdaModel;
                    borrowerDecisionCreditModelName.Text = dataItem.Application.aBDecisionCreditModelName;
                }
                else if (dataItem.Application.aBDecisionCreditSourceT == E_aDecisionCreditSourceT.Other)
                {
                    borrowerDecisionCreditModelName.Text = dataItem.Application.aBDecisionCreditModelName;
                }                
                else if (dataItem.Application.aAppId != this.ApplicationID)
                {
                    borrowerDecisionCreditModelName.Visible = false;
                }
            }            
            else
            {
                e.Item.FindControl("AppCreditScoreBorrowerRow").Visible = false;
            }

            var coborrAppRow = e.Item.FindControl("AppCreditScoreCoBorrowerRow") as TableRow;
            var coborrAppLabel = e.Item.FindControl("AppCreditScoreCoBorrowerLabel") as Literal;
            var coborrowerDecisionCreditSourceTLckd = e.Item.FindControl("aCDecisionCreditSourceTLckd") as CheckBox;
            var coborrowerDecisionCreditSourceT = e.Item.FindControl("aCDecisionCreditSourceT") as DropDownList;
            var coborrowerDecisionCreditScore = e.Item.FindControl("aCDecisionCreditScore") as TextBox;
            var coborrowerDecisionCreditModelName = e.Item.FindControl("aCDecisionCreditModelName") as TextBox;
            var coborrowerIsDecisionCreditModelAValidHmdaModel = e.Item.FindControl("aCIsDecisionCreditModelAValidHmdaModel") as CheckBox;

            if (dataItem.Application.aCIsValidNameSsn)
            {
                coborrAppLabel.Text = "App " + dataItem.Index + " Coborrower's Decision Score";
                coborrowerDecisionCreditSourceTLckd.Checked = dataItem.Application.aCDecisionCreditSourceTLckd;
                Tools.Bind_aDecisionCreditSourceT(coborrowerDecisionCreditSourceT);
                Tools.SetDropDownListValue(coborrowerDecisionCreditSourceT, dataItem.Application.aCDecisionCreditSourceT);
                coborrowerDecisionCreditScore.Text = dataItem.Application.aCDecisionCreditScore_rep;

                if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.dataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
                {                    
                    coborrowerIsDecisionCreditModelAValidHmdaModel.Checked = dataItem.Application.aCIsDecisionCreditModelAValidHmdaModel;
                    borrowerDecisionCreditModelName.Text = dataItem.Application.aCDecisionCreditModelName;
                }
                else if (dataItem.Application.aCDecisionCreditSourceT == E_aDecisionCreditSourceT.Other)
                {
                    borrowerDecisionCreditModelName.Text = dataItem.Application.aCDecisionCreditModelName;
                }
                else if (dataItem.Application.aAppId != this.ApplicationID)
                {
                    borrowerDecisionCreditModelName.Visible = false;
                }
            }
            else
            {
                e.Item.FindControl("AppCreditScoreCoBorrowerRow").Visible = false;
            }
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// <param name="e">Event argument parameter.</param>
        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }

        /// <summary>
        /// Data source for the application credit score data repeaters.
        /// </summary>
        private class AppCreditScoreRepeaterData
        {
            /// <summary>
            /// Gets or sets the 1-based index of the application.
            /// </summary>
            /// <value>The 1-based index of the application.</value>
            public int Index { get; set; }

            /// <summary>
            /// Gets or sets the loan application.
            /// </summary>
            public CAppData Application { get; set; }
        }
    }    
}