<%@ Import namespace="LendersOffice.Common" %>
<%@Import Namespace="LendersOffice.AntiXss"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="BorrowerLiabilityList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.BorrowerLiabilityList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>BorrowerLiabilityList</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout" scroll="yes" leftmargin=0 bgcolor="gainsboro" style="CURSOR: default">
	<script type="text/javascript">
  function _init() {
    list_oTable = document.getElementById("<%= AspxTools.ClientId(m_dg) %>");
    //get the index of the REO record from the table
    
    var continueSearching = true;
    var i = 0;
    
    if(<%=AspxTools.JsBool(fromREO) %>)
    while(continueSearching)
    {
          if (i >= list_oTable.rows.length) i= null;
          else
            if( list_oTable.rows[i].recordid == <%= AspxTools.JsString(recordIDREOMatch)%>)
                parent.parent_setRowIndex(i);
           if(i == null)
                break;
                i++;
    }
    parent.parent_initScreen(2);
  }
  function constructEmptyRow(tr) {
      // Construct empty row.
      var td = null;
      <% if (HasPmlEnabled) { %>
      td = tr.insertCell();
      td.innerText = " ";
        
      <% } %>      
      td = tr.insertCell();
      td.innerText = " ";
      td = tr.insertCell();
      td.innerText = " ";
      td = tr.insertCell();
      td.innerText = " ";
      td = tr.insertCell();
      td.setAttribute("align", "right");;
      td.innerText = " ";
      td = tr.insertCell();
      td.setAttribute("align", "right");;
      td.innerText = " ";
      td = tr.insertCell();
      td.innerText = " ";
      td = tr.insertCell();
      td.innerText = " ";

  }    
  </script>

    <form id="BorrowerLiabilityList" method="post" runat="server">
      <asp:DataGrid id=m_dg runat="server" DataKeyField="RecordId" BorderColor="Gainsboro" AutoGenerateColumns="False" Width="100%" enableviewstate="False">
<alternatingitemstyle cssclass="GridItem">
</AlternatingItemStyle>

<itemstyle cssclass="GridItem">
</ItemStyle>

<headerstyle wrap="False" cssclass="GridHeader">
</HeaderStyle>

<footerstyle cssclass="GridFooter">
</FooterStyle>

<columns>
<asp:TemplateColumn SortExpression="PmlAuditTrailXmlContent" HeaderText="Altered in PML">
<ItemTemplate>
<%#AspxTools.HtmlString(displayIsPmlAltered(DataBinder.Eval(Container.DataItem, "PmlAuditTrailXmlContent")))%>
</ItemTemplate>
<headerstyle width="82px">
</HeaderStyle>

<itemstyle width="80px">
</ItemStyle>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="OwnerT" HeaderText="Owner">
<headerstyle width="40px">
</HeaderStyle>

<itemstyle width="40px">
</ItemStyle>

<itemtemplate>
<%# AspxTools.HtmlString(displayOwnerType(DataBinder.Eval(Container.DataItem, "OwnerT").ToString())) %>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="DebtT" HeaderText="Debt Type">
<headerstyle width="70px">
</HeaderStyle>

<itemstyle width="70px">
</ItemStyle>

<itemtemplate>
<%# AspxTools.HtmlString(displayLiabilityType(DataBinder.Eval(Container.DataItem, "DebtT").ToString())) %>
</ItemTemplate>
</asp:TemplateColumn>
<asp:templatecolumn headertext="Company" sortexpression="ComNm">
  <itemtemplate>
    <%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "ComNm").ToString()) %>
  </itemtemplate>
</asp:templatecolumn>
<asp:TemplateColumn SortExpression="Bal" HeaderText="Balance">
<headerstyle width="80px">
</HeaderStyle>

<itemstyle horizontalalign="Right" width="80px">
</ItemStyle>

<itemtemplate>
<%# AspxTools.HtmlString(displayMoneyString(DataBinder.Eval(Container.DataItem, "Bal").ToString())) %>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="Pmt" HeaderText="Payment">
<headerstyle wrap="False" width="80px">
</HeaderStyle>

<itemstyle horizontalalign="Right" width="80px">
</ItemStyle>

<itemtemplate>
<%# AspxTools.HtmlString(displayMoneyString(DataBinder.Eval(Container.DataItem, "Pmt").ToString())) %>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="WillBePdOff" HeaderText="Pd Off">
<headerstyle width="40px">
</HeaderStyle>

<itemstyle width="40px">
</ItemStyle>

<itemtemplate>
<%# AspxTools.HtmlString(displayPayoffCheckbox(DataBinder.Eval(Container.DataItem, "WillBePdOff"), DataBinder.Eval(Container.DataItem, "RecordId"))) %>
</ItemTemplate>
</asp:TemplateColumn>
<asp:TemplateColumn SortExpression="NotUsedInRatio" HeaderText="Used in Ratio">
<headerstyle width="80px">
</HeaderStyle>

<itemstyle width="80px">
</ItemStyle>

<itemtemplate>
<%#AspxTools.HtmlString(displayUsedInRatiodCheckbox(DataBinder.Eval(Container.DataItem,"NotUsedInRatio"), DataBinder.Eval(Container.DataItem,"RecordId"), DataBinder.Eval(Container.DataItem, "DebtT"))) %>
</ItemTemplate>
</asp:TemplateColumn>


</Columns>
      </asp:DataGrid>
     </form>
	
  </body>
</HTML>
