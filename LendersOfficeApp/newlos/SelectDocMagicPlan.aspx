﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectDocMagicPlan.aspx.cs" Inherits="LendersOfficeApp.newlos.SelectDocMagicPlan" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    	<link href="~/css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body onload="onLoad();">
    <script type="text/javascript">
		<!--
        if (!window.dialogArguments) {
            window.dialogArguments = { callback: opener.AuditWindow.getChildArgs() };
        }
        function onLoad() {

            if (document.getElementById('m_commandToDo') != null) {
                if (document.getElementById('m_commandToDo').value == "Close") {
                    var arg = window.dialogArguments || {};
                    if(arg.callback) arg.callback(arg);
                    onClosePopup(arg);
                }
            }
            resizeForIE6And7(400, 200);
        }

        function closeFailure() {
            var arg = window.dialogArguments || {};
            arg.Action = 'Failure';
            if(arg.callback) arg.callback(arg);
            onClosePopup(arg);
        }
		//-->
	</script>
    <h4 class="page-header">Document Plan Code</h4>
    <form id="form1" runat="server">
    <div>
    		<table cellspacing="2" cellpadding="0" width="100%" height="100%">
		    <asp:MultiView id="m_selectionView" runat="server">
		        <asp:View id="selectPlanCode" runat="server">
		            <tr>
		                <td>
		                    Select the document plan code to use for this loan file:
		                </td>
		            </tr>
		            <tr>
		                <td>
		                    <asp:DropDownList ID="m_planCodes" runat="server"></asp:DropDownList>
		                    </td>
		            </tr>
		            <tr>
		                <td align="center">
		                    <asp:Button Text="Save" runat="server" ID="m_saveBtn" OnClick="SaveClick" />
		                    <input type="button" value="Cancel" onclick="closeFailure()" />
		                </td>
		            </tr>
		        </asp:View>
		        <asp:View ID="warnings" runat="server">
		            <tr>
		                <td>
		                    Cannot choose a document plan.  Please correct the following:
		                </td>
		            </tr>
		            <tr>
		                <td>
		                    <ul>
                            <asp:Repeater ID="m_warningList" runat="server">
                                <ItemTemplate>
                                    <li><%# AspxTools.HtmlString( (string) DataBinder.Eval(Container.DataItem, "Message" )) %></li>
                                </ItemTemplate>
                            </asp:Repeater>
                            </ul>
                        </td>
		            </tr>
		            <tr>
		                <td align="center">
		                    <input type="button" value=" OK " onclick="closeFailure()" />
		                </td>
		            </tr>		        
		        </asp:View>
		    </asp:MultiView>
		    </table>
    </div>
    </form>
</body>
</html>
