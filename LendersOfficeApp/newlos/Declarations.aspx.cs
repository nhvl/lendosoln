﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using LendersOffice.Constants;
using MeridianLink.CommonControls;
using LendersOffice.Admin;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos
{
    public partial class Declarations : BaseLoanPage
    {
        protected bool m_isPurchase = false;

        public String getDisplayStyle
        {
            get {
                return BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) ? "display: block" : "display: none";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(Declarations));
            dataLoan.InitLoad();

            // ****************************************************************
            // dataApp
            // ****************************************************************
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            aBDecJudgment.Text = dataApp.aBDecJudgment.ToUpper().TrimWhitespaceAndBOM();
            aCDecJudgment.Text = dataApp.aCDecJudgment.ToUpper().TrimWhitespaceAndBOM();
            aBDecBankrupt.Text = dataApp.aBDecBankrupt.ToUpper().TrimWhitespaceAndBOM();
            aBDecForeclosure.Text = dataApp.aBDecForeclosure.ToUpper().TrimWhitespaceAndBOM();
            aCDecForeclosure.Text = dataApp.aCDecForeclosure.ToUpper().TrimWhitespaceAndBOM();
            aBDecLawsuit.Text = dataApp.aBDecLawsuit.ToUpper().TrimWhitespaceAndBOM();
            aCDecLawsuit.Text = dataApp.aCDecLawsuit.ToUpper().TrimWhitespaceAndBOM();
            aCDecBankrupt.Text = dataApp.aCDecBankrupt.ToUpper().TrimWhitespaceAndBOM();
            aBDecDelinquent.Text = dataApp.aBDecDelinquent.ToUpper().TrimWhitespaceAndBOM();
            aBDecAlimony.Text = dataApp.aBDecAlimony.ToUpper().TrimWhitespaceAndBOM();
            aCDecAlimony.Text = dataApp.aCDecAlimony.ToUpper().TrimWhitespaceAndBOM();
            aBDecBorrowing.Text = dataApp.aBDecBorrowing.ToUpper().TrimWhitespaceAndBOM();
            aCDecBorrowing.Text = dataApp.aCDecBorrowing.ToUpper().TrimWhitespaceAndBOM();
            aCDecDelinquent.Text = dataApp.aCDecDelinquent.ToUpper().TrimWhitespaceAndBOM();
            aCDecObligated.Text = dataApp.aCDecObligated.ToUpper().TrimWhitespaceAndBOM();
            aBDecObligated.Text = dataApp.aBDecObligated.ToUpper().TrimWhitespaceAndBOM();
            aCDecPastOwnership.Text = dataApp.aCDecPastOwnership.ToUpper().TrimWhitespaceAndBOM();
            aBDecEndorser.Text = dataApp.aBDecEndorser.ToUpper().TrimWhitespaceAndBOM();
            aBDecCitizen.Text = dataApp.aBDecCitizen.ToUpper().TrimWhitespaceAndBOM();
            aCDecCitizen.Text = dataApp.aCDecCitizen.ToUpper().TrimWhitespaceAndBOM();
            aBDecResidency.Text = dataApp.aBDecResidency.ToUpper().TrimWhitespaceAndBOM();
            aCDecResidency.Text = dataApp.aCDecResidency.ToUpper().TrimWhitespaceAndBOM();
            aBDecOcc.Text = dataApp.aBDecOcc.ToUpper().TrimWhitespaceAndBOM();
            aCDecOcc.Text = dataApp.aCDecOcc.ToUpper().TrimWhitespaceAndBOM();
            aCDecEndorser.Text = dataApp.aCDecEndorser.ToUpper().TrimWhitespaceAndBOM();
            aBDecPastOwnership.Text = dataApp.aBDecPastOwnership.ToUpper().TrimWhitespaceAndBOM();
            aBDecForeignNational.Text = dataApp.aBDecForeignNational.ToUpper().TrimWhitespaceAndBOM();

            Tools.Bind_aDecPastOwnedPropT(aBDecPastOwnedPropT);
            Tools.Bind_aDecPastOwnedPropTitleT(aBDecPastOwnedPropTitleT);
            Tools.Bind_aDecPastOwnedPropT(aCDecPastOwnedPropT);
            Tools.Bind_aDecPastOwnedPropTitleT(aCDecPastOwnedPropTitleT);

            Tools.SetDropDownListValue(aBDecPastOwnedPropT, dataApp.aBDecPastOwnedPropT);
            Tools.SetDropDownListValue(aCDecPastOwnedPropT, dataApp.aCDecPastOwnedPropT);
            Tools.SetDropDownListValue(aBDecPastOwnedPropTitleT, dataApp.aBDecPastOwnedPropTitleT);
            Tools.SetDropDownListValue(aCDecPastOwnedPropTitleT, dataApp.aCDecPastOwnedPropTitleT);

            this.EnableJquery = true;
            this.RegisterJsScript("DeclarationExplanation.js");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("jquery.tmpl.js");

            var decExplnDataContainer = dataApp.ConstructDeclarationExplanationData();
            var serializedDecExplnData = SerializationHelper.JsonNetSerialize(decExplnDataContainer);
            this.Page.ClientScript.RegisterHiddenField("serializedDecExplnData", serializedDecExplnData);
        }

        private void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "Declarations";
            this.PageID = "Declarations";

        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Init += new System.EventHandler(this.PageInit);
            base.OnInit(e);
        }
    }
}
