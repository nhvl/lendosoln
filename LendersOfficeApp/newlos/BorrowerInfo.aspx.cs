using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOfficeApp.common;
using LendersOfficeApp.los.admin;

namespace LendersOfficeApp.newlos
{
	public partial class BorrowerInfo : BaseLoanPage
	{
        protected BorrowerInfoTab BorrowerInfoTab;
        protected BorrowerMonthlyIncome BorrowerMonthlyIncome;
        protected BorrowerEmploymentFrame BorrowerEmploymentFrame;
        protected BorrowerEmploymentFrame CoborrowerEmploymentFrame;

        protected BorrowerAssetFrame BorrowerAssetFrame;
        protected BorrowerLiabilityFrame BorrowerLiabilityFrame;
        protected BorrowerREOFrame BorrowerREOFrame;
        protected BorrowerHousingExpense BorrowerHousingExpense;

        protected String HeaderDisplay;

        public override bool IsReadOnly
        {
            get
            {
                // 7/13/2005 dd - OPM 2351: Protect liabilities from being edited by AE.
                if (Tabs.TabIndex== 4)
                    return base.IsReadOnly || BrokerUser.IsAccountExecutiveOnly;
                else
                    return base.IsReadOnly;
            }
        }

		protected void PageInit(object sender, System.EventArgs e)
		{
			//RegisterService("BorrowerInfo", "/newlos/BorrowerInfoService.aspx");


            this.CoborrowerEmploymentFrame.IsBorrower = false;

            Tabs.Visible = false;

            if (IsLeadPage)
            {
                Tabs.AddToQueryString("isLead", "t");
            }

            Tabs.RegisterControl("Borr. Info.", BorrowerInfoTab);
            Tabs.RegisterControl("Borr. Employ.", BorrowerEmploymentFrame);
            Tabs.RegisterControl("Coborr. Employ.", CoborrowerEmploymentFrame);
            Tabs.RegisterControl("Monthly Income", BorrowerMonthlyIncome);
            Tabs.RegisterControl("Liabilities", BorrowerLiabilityFrame);
            Tabs.RegisterControl("Assets", BorrowerAssetFrame);
            Tabs.RegisterControl("Reo", BorrowerREOFrame);
            Tabs.RegisterControl("Housing Expense", BorrowerHousingExpense);
            Tabs.AddToQueryString("loanid", LoanID.ToString());

		}
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }

        override protected void OnPreRender(EventArgs e)
        {
            if (RequestHelper.GetSafeQueryString("cmd") == "new" && !Page.IsPostBack)
            {
                this.AddInitScriptFunction("newApp");
            }

            int index = Tabs.TabIndex;

            if (index== 0)
            {
                this.PageTitle = "Borrower information";
                this.PageID = "BorrowerInformation";
                HeaderDisplay = "Borr. Info";
            }
            else if (index== 1)
            {
                this.PageTitle = "Borrower employments";
                this.PageID = "BorrowerEmployments";
                HeaderDisplay = "Borr. Employ.";
            }
            else if (index== 2)
            {
                this.PageTitle = "Coborrower employments";
                this.PageID = "CoborrowerEmployments";
                HeaderDisplay = "Coborr. Employ.";
            }
            else if (index== 3)
            {
                this.PageTitle = "Borrower monthly income";
                this.PageID = "BorrowerMonthlyIncome";
                HeaderDisplay = "Monthly Income";
            }
            else if (index== 4)
            {
                this.PageTitle = "Borrower liabilities";
                this.PageID = "BorrowerLiabilities";
                HeaderDisplay = "Liabilities";
            }
            else if (index== 5)
            {
                this.PageTitle = "Borrower assets";
                this.PageID = "BorrowerAssets";
                HeaderDisplay = "Assets";
            }
            else if (index== 6)
            {
                this.PageTitle = "Borrower REO";
                this.PageID = "BorrowerREO";
                HeaderDisplay = "REO";
            }
            else if (index== 7)
            {
                this.PageTitle = "Present housing expense";
                this.PageID = "HousingExpense";
                HeaderDisplay = "Pres House Expense";
            }
            base.OnPreRender(e);
        }
	}
}
