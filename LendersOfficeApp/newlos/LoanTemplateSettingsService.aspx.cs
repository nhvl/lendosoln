using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos
{
    public class LoanTemplateSettingsServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return new CLoanTemplateSettingsData(sLId);
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
			/*
            dataLoan.sTemplatePublishLevelT = (E_sTemplatePublishLevelT) GetInt("sTemplatePublishLevelT");

            dataLoan.sTemplateAssignLoanOfficerT = (E_sTemplateAssignT) GetInt("sTemplateAssignLoanOfficerT");
            dataLoan.sTemplateAssignProcessorT = (E_sTemplateAssignT) GetInt("sTemplateAssignProcessorT");
            dataLoan.sTemplateAssignManagerT = (E_sTemplateAssignT) GetInt("sTemplateAssignManagerT");
            dataLoan.sTemplateAssignCallCenterAgentT = (E_sTemplateAssignT) GetInt("sTemplateAssignCallCenterAgentT");
            dataLoan.sTemplateAssignSellingAgentT = (E_sTemplateAssignT) GetInt("sTemplateAssignSellingAgentT");
            dataLoan.sTemplateAssignLoanOpenerT = (E_sTemplateAssignT) GetInt("sTemplateAssignLoanOpenerT");
            dataLoan.sTemplateAssignUnderwriterT = (E_sTemplateAssignT) GetInt("sTemplateAssignUnderwriterT");
            dataLoan.sTemplateAssignLockDeskT = (E_sTemplateAssignT) GetInt("sTemplateAssignLockDeskT");
            dataLoan.sTemplateAssignAeT = (E_sTemplateAssignT) GetInt("sTemplateAssignAeT");
			*/

			dataLoan.sBranchIdForNewFileFromTemplate = GetGuid("sBranchIdForNewFileFromTemplate");
        }
    }

	public partial class LoanTemplateSettingsService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new LoanTemplateSettingsServiceItem());
        }
	}
}
