<%@ Import Namespace="DataAccess" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="OtherFinancing.ascx.cs" Inherits="LendersOfficeApp.newlos.OtherFinancing" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript">
  function _init() {
    <%= AspxTools.JsGetElementById(sRemain1stMBal)%>.readOnly = <%= AspxTools.JsGetElementById(s1stMtgOrigLAmt) %>.readOnly || <%= AspxTools.JsGetElementById(sIsOFinNew) %>.checked;
    <% if (!m_hasLinkedLoan) { %>
      document.getElementById("btnUnlinkLoan").disabled = true;
      document.getElementById("btnGotoLinkLoan").disabled = true;  
      document.getElementById("btnUpdateLinkLoan").disabled = true;    
    <% } %>

    <% if (m_sLienPosT == E_sLienPosT.First) { %>
        document.getElementById("FirstLienTable").style.display = "none";
                
        lockField(<%= AspxTools.JsGetElementById(sSubFinPmtLckd) %>, "sSubFinPmt");

        var sIsOFinNew = <%= AspxTools.JsGetElementById(sIsOFinNew)%>;
        var sConcurSubFinTb = <%= AspxTools.JsGetElementById(sConcurSubFin)%>;
        var sSubFinTb = <%= AspxTools.JsGetElementById(sSubFin)%>;
        var sIsOFinCreditLineInDrawPeriodCb = <%=AspxTools.JsGetElementById(sIsOFinCreditLineInDrawPeriod) %>;
        var sSubFinLabel = document.getElementById('sSubFinLabel');
        var linkedLoan = document.getElementById("btnGotoLinkLoan").disabled == false;
        
        
        if ( <%= AspxTools.JsBool( m_isPurchase )  %> )
        {
            sIsOFinNew.checked = true;
            sIsOFinNew.disabled = true;
        }
        else
        {
            if ( linkedLoan )
            {
                sIsOFinNew.checked = true;
            }
        }
        
        if(sIsOFinCreditLineInDrawPeriodCb.checked) 
        {
            sSubFinLabel.innerHTML = "Credit Line";
            sSubFinTb.readOnly = false;
        }
        else
        {
            sSubFinLabel.innerHTML = "Original Balance";        
        }
        
        if ( sIsOFinNew.checked == true && sIsOFinCreditLineInDrawPeriodCb.checked == false )
        {
            //sConcurSubFinTb.value = sSubFinTb.value;
            sSubFinTb.readOnly = true;
        }
        else 
        {
            sSubFinTb.readOnly = false;
        }
        
        <% if (!m_isUseLayeredFinance) { %>
            document.getElementById("LayeredFinancingTable").style.display = "none";
        <% } %>
        
    <% } else { %>
        document.getElementById("SecondLienTable").style.display = "none";    
        document.getElementById("LayeredFinancingTable").style.display = "none";
    <% } %>
  }
  function f_unlinkLoan() {
    if(confirm("Warning: This will permanently unlink the loan file currently linked to this loan file. Would you like to proceed?"))
    {
        var args = new Object();
        args["loanid"] = ML.sLId;
        args["_ClientID"] = ML._ClientID;
        var result = gService.loanedit.call(ML._ClientID + "_UnlinkLoan", args);
        if (!result.error) {
          // Clear Textbox
          <%= AspxTools.JsGetElementById(sLinkedLoanInfo_LinkedLNm) %>.value = "";
          document.getElementById("btnUnlinkLoan").disabled = true;
          document.getElementById("btnGotoLinkLoan").disabled = true;     
          document.getElementById("btnUpdateLinkLoan").disabled = true; 
          
        }
        else
        {
            window.alert("Unable to unlink loan. " + result.UserMessage);
        }
    }
  }
    function f_goToLinkLoan() {
        if (ML.isLinkedLead) {
            f_openLead(<%= AspxTools.JsString(m_linkedLoanID) %>, <%= AspxTools.JsGetElementById(sLinkedLoanInfo_LinkedLNm) %>.value);
        } else {
            f_openLoan(<%= AspxTools.JsString(m_linkedLoanID) %>, <%= AspxTools.JsGetElementById(sLinkedLoanInfo_LinkedLNm) %>.value);
        }
  }
  function f_updateLinkLoan()
  {
	var args = new Object();
	args["loanid"] = ML.sLId;
	args["_ClientID"] = ML._ClientID;
	var result = gService.loanedit.call(ML._ClientID + "_UpdateLinkLoan", args );
	if( result.value.MsgToUser != null && result.value.MsgToUser != "" )
		alert( result.value.MsgToUser );
	if( result.error ) 
	{
		alert( 'Error encountered during update.' );
	}	

	populateForm(result.value, ML._ClientID);
	_init();
  }
  
  function f_confirmsIsOFinNew()
  {
    if ( <%= AspxTools.JsBool( !m_isPurchase )  %> && <%= AspxTools.JsBool( m_hasLinkedLoan )  %> )
    {
        var sIsOFinNew = <%= AspxTools.JsGetElementById(sIsOFinNew)%>;
        if ( sIsOFinNew.checked == false )
        {
            if  ( confirm("Warning: Marking the other financing as existing will permanently unlink the loan file currently linked to this loan file. Would you like to proceed?" ) )
            {
                f_unlinkLoan();
            }
            else
            {
                return false;
            }
        }
    }
    
    refreshCalculation();
    return true;
  }
</script>

			<table id="Table4" cellspacing="1" cellpadding="3" border="0" class="FormTable" width=600>
  <tr>
    <td class=FieldLabel></td></tr>
  <TR>
    <td>
      <table class=InsetBorder id=Table2 cellspacing=0 cellpadding=0 width=600 border=0>
        <tr>
          <td class=FormTableSubHeader nowrap>Linked Loan</td></tr>
        <tr>
          <td class=FieldLabel nowrap>Loan Number <asp:TextBox id=sLinkedLoanInfo_LinkedLNm runat="server" ReadOnly="True"></asp:TextBox>

          <asp:PlaceHolder runat="server" ID="LinkedLoanButtonsPlaceHolder">
          <input class=ButtonStyle id=btnGotoLinkLoan type=button value="Go to" onclick="f_goToLinkLoan();" >&nbsp; 
          <input class=ButtonStyle id=btnUpdateLinkLoan type=button value="Update" onclick="f_updateLinkLoan();">&nbsp;
              
          <input type=button value="Permanently unlink" onclick="f_unlinkLoan();" id=btnUnlinkLoan class="ButtonStyle" style="width:150px"></td></tr></table></td></TR>
          </asp:PlaceHolder>

  <tr>
    <td>
      <table class=InsetBorder id="FirstLienTable" cellspacing=0 cellpadding=0 width="600" 
      border=0>
        <tr>
          <td class=FormTableSubHeader nowrap colspan=3>Additional 1st 
        Lien</td></tr>
        <TR>
          <TD class=FieldLabel noWrap colSpan=3><asp:CheckBox id=sIsOFinNew_2 onclick=refreshCalculation(); Text="Additional financing is a new loan." runat="server"></asp:CheckBox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap colSpan=3><asp:CheckBox id=sLpIsNegAmortOtherLien onclick=refreshCalculation(); Text="Additional financing has potential negative amortization." runat="server"></asp:CheckBox></TD></TR>          
        <TR>
          <TD class=FieldLabel noWrap colSpan=3><asp:CheckBox id=sIsIOnlyForSubFin_2 onclick=refreshCalculation(); Text="Interest only loan" runat="server"></asp:CheckBox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap colSpan=3>Amortization Method <asp:DropDownList id=sOtherLFinMethT_2 runat="server"></asp:DropDownList></TD></TR>
        <tr>
          <td class=FieldLabel nowrap>Original Balance</td>
          <td class=FieldLabel nowrap>Current Balance</td>
          <td class=FieldLabel nowrap width="100%">Monthly Payment</td></tr>
        <tr>
          <td nowrap><ml:moneytextbox id=s1stMtgOrigLAmt cssclass="mask" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox>&nbsp; 
          </td>
          <td nowrap><ml:moneytextbox id=sRemain1stMBal cssclass="mask" preset="money" width="90" runat="server"></ml:moneytextbox></td>
          <td nowrap><ml:moneytextbox id=sRemain1stMPmt cssclass="mask" preset="money" width="90" runat="server"></ml:moneytextbox></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id="SecondLienTable" cellspacing=0 cellpadding=0 width="600" 
      border=0>
        <tr>
          <td class=FormTableSubHeader nowrap colspan=6>Additional 2nd 
        Lien</td></tr>
        <TR>
          <TD class=FieldLabel noWrap colSpan=6><asp:CheckBox id=sIsOFinNew onclick="return f_confirmsIsOFinNew();" Text="Additional financing is a new loan." runat="server"></asp:CheckBox></TD></TR>
        <TR>
          <TD class=FieldLabel noWrap colSpan=6><asp:CheckBox id=sIsOFinCreditLineInDrawPeriod onclick=refreshCalculation(); Text="Additional financing is a line of credit in the draw period." runat="server"></asp:CheckBox></TD></TR>          
        <tr>
          <td class=FieldLabel nowrap colspan=6><asp:CheckBox id=sIsIOnlyForSubFin Text="Interest only loan" runat="server" onclick="refreshCalculation();"></asp:CheckBox></td></tr>
        <TR>
          <TD class=FieldLabel noWrap colSpan=6>Amortization Method&nbsp; <asp:DropDownList id=sOtherLFinMethT runat="server"></asp:DropDownList></TD></TR>
        <tr>
          <td class=FieldLabel nowrap id="sSubFinLabel"><% if (m_isSubFinCreditLine) { %> Credit Line <% } else { %> Original Balance <% } %></td>
          <td class=FieldLabel nowrap>Current Balance</td>
          <td class=FieldLabel nowrap>Rate</td>
          <td class=FieldLabel nowrap>Term (months)</td>
          <td class=FieldLabel nowrap align=right>Payment Base</td>
          <td class=FieldLabel nowrap>Monthly Payment</td></tr>
        <tr>
          <td nowrap><ml:moneytextbox id=sSubFin cssclass="mask" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></td>
          <td nowrap><ml:moneytextbox id=sConcurSubFin cssclass="mask" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></td>
          <td nowrap><ml:percenttextbox id=sSubFinIR cssclass="mask" preset="percent" width="70" runat="server" onchange="refreshCalculation();"></ml:percenttextbox></td>
          <td nowrap align=center><asp:textbox id=sSubFinTerm width="51px" runat="server" onchange="refreshCalculation();"></asp:textbox></td>
          <td nowrap>+<ml:moneytextbox id=sSubFinMb cssclass="mask" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></td>
          <td nowrap>=<ml:moneytextbox id=sSubFinPmt runat="server" width="90" preset="money" cssclass="mask" readonly="True"></ml:moneytextbox>
          <asp:CheckBox ID="sSubFinPmtLckd" runat="server" onclick="refreshCalculation();" />
          </td></tr></table></td></tr>
          <tr>
          <td>
      <table class=InsetBorder id="LayeredFinancingTable" cellspacing=0 cellpadding=0 width="600" 
      border=0>
        <tr>
          <td class=FormTableSubHeader nowrap colspan=2>Layered Financing</td></tr>
          <tr>
          <td class="FieldLabel" width="135px" >Total Forgivable Balance</td>
          <td align="left" ><ml:moneytextbox id="sLayeredTotalForgivableBalance" cssclass="mask" preset="money" width="90" runat="server" onchange="refreshCalculation();"></ml:moneytextbox></td>
          </tr>
          </table>
          </td></tr>
			</table>
