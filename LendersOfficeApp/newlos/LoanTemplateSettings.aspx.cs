using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.newlos
{
	public partial class LoanTemplateSettings : LendersOfficeApp.newlos.BaseLoanPage
	{

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Loan Template Settings";
            this.PageID = "LoanTemplateSettings";

            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", brokerId )
                                        };

			using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( brokerId, "ListBranchByBrokerID" , parameters ) ) 
			{
				sBranchIdForNewFileFromTemplate.DataSource = reader;
				sBranchIdForNewFileFromTemplate.DataValueField = "BranchID";
				sBranchIdForNewFileFromTemplate.DataTextField = "Name";
				sBranchIdForNewFileFromTemplate.DataBind();
				sBranchIdForNewFileFromTemplate.Items.Insert(0, new ListItem("<-- Creating user's (or user's AE's) own branch -->", Guid.Empty.ToString()));
			}


        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = new CLoanTemplateSettingsData(LoanID);
            dataLoan.InitLoad();

			Tools.SetDropDownListValue(sBranchIdForNewFileFromTemplate, dataLoan.sBranchIdForNewFileFromTemplate.ToString());
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
