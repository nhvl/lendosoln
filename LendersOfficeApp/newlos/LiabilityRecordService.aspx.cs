using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOfficeApp.los.Rolodex;
using LendersOffice.Constants;
using LendersOffice.Admin;
using LendersOffice.ObjLib;

namespace LendersOfficeApp.newlos
{

	public partial class LiabilityRecordService : AbstractSingleEditService2
	{
        protected override void CustomProcess(string methodName) 
        {
            switch (methodName) 
            {
                case "AddToReo":
                    AddToReo();
                    break;
            }
        }

        protected bool HasPmlEnabled
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan); }
        }
        protected override void SaveData(CPageData dataLoan, CAppData dataApp, IRecordCollection collection, ICollectionItemBase2 record) 
        {
            dataApp.aAsstLiaCompletedNotJointly = GetBool("aAsstLiaCompletedNotJointly");

            ILiaCollection recordList = (ILiaCollection) collection;
            ILiabilityRegular field = (ILiabilityRegular) record;
			if( null != field )
			{
				field.DebtT = (E_DebtRegularT) GetInt("DebtT");
				field.VerifExpD_rep = GetString("VerifExpD") ;
				field.VerifRecvD_rep = GetString("VerifRecvD") ;
				field.VerifSentD_rep = GetString("VerifSentD") ;
				field.VerifReorderedD_rep = GetString("VerifReorderedD");
				field.Bal_rep = GetString("Bal");
				field.Pmt_rep = GetString("Pmt");
				field.R_rep = GetString("Rate");
				field.AccNum = GetString("AccNum");
				field.ComAddr = GetString("ComAddr");
				field.ComCity = GetString("ComCity");
				field.ComState = GetString("ComState");
				field.ComZip = GetString("ComZip");
				field.Desc = GetString("Desc") ;
				field.ComPhone = GetString("ComPhone");
				field.ComFax = GetString("ComFax");
				field.AccNm = GetString("AccNm");
				field.ComNm = GetString("ComNm");
				field.OrigTerm_rep = GetString("OrigTerm");
				field.OwnerT = (E_LiaOwnerT) GetInt("OwnerT");
				field.Due_rep = GetString("Due");
				field.RemainMons_rep = GetString("RemainMons");
				field.WillBePdOff = GetBool("WillBePdOff");
				field.NotUsedInRatio = !GetBool("UsedInRatio");
				field.IsPiggyBack = GetBool("IsPiggyBack");
				field.Late30_rep = GetString("Late30");
				field.Late60_rep = GetString("Late60");
				field.Late90Plus_rep = GetString("Late90Plus");
				field.IncInReposession = GetBool("IncInReposession");
				field.IncInBankruptcy = GetBool("IncInBankruptcy");
				field.IncInForeclosure = GetBool("IncInForeclosure");

                // 8/2/2013 EM - Case 130395 - Non-underwriters can save now as well.
                // OPM 2758 - Only allow to save this bit if user has underwriter role. Temporary solution only.
               // if (BrokerUserPrincipal.CurrentPrincipal.HasRole(E_RoleT.Underwriter)) 
               // {
                    field.ExcFromUnderwriting = GetBool("ExcFromUnderwriting");
               // }

                // If sLT is FHA then handle FHA specific fields.
                if (dataLoan.sLT == E_sLT.FHA) 
                {
                    field.OrigDebtAmt_rep = GetString("OrigDebtAmt");
                    field.AutoYearMake = GetString("AutoYearMake");
                    field.IsForAuto = GetBool("IsForAuto");
                    field.IsMortFHAInsured = GetBool("IsMortFHAInsured");
                }

                field.MatchedReRecordId = GetGuid("MatchedReRecordId", field.MatchedReRecordId);
                field.PayoffAmtLckd = GetBool("PayoffAmtLckd");
                field.PayoffAmt_rep = GetString("PayoffAmt");
                field.PayoffTimingLckd = GetBool("PayoffTimingLckd");
                field.PayoffTiming = (E_Timing)GetInt("PayoffTiming");
			}

            // Save special record

            var alimony = recordList.GetAlimony(true);
            alimony.OwedTo = GetString("Alimony_ComNm");
            alimony.Pmt_rep = GetString("Alimony_Pmt");
            alimony.RemainMons_rep = GetString("Alimony_RemainMons");
            alimony.NotUsedInRatio = GetBool("Alimony_NotUsedInRatio");

            var childSupport = recordList.GetChildSupport(true);
            childSupport.OwedTo = GetString("ChildSupport_ComNm");
            childSupport.Pmt_rep = GetString("ChildSupport_Pmt");
            childSupport.RemainMons_rep = GetString("ChildSupport_RemainMons");
            childSupport.NotUsedInRatio = GetBool("ChildSupport_NotUsedInRatio");

            var jobRelated = recordList.GetJobRelated1(true);
            jobRelated.ExpenseDesc = GetString("JobRelated1_ComNm");
            jobRelated.Pmt_rep = GetString("JobRelated1_Pmt");
            jobRelated.NotUsedInRatio = GetBool("JobRelated1_NotUsedInRatio");

            jobRelated = recordList.GetJobRelated2(true);
            jobRelated.ExpenseDesc = GetString("JobRelated2_ComNm");
            jobRelated.Pmt_rep = GetString("JobRelated2_Pmt");
            jobRelated.NotUsedInRatio = GetBool("JobRelated2_NotUsedInRatio");

            dataLoan.sLienToPayoffTotDebt = GetEnum<ResponsibleLien>("sLienToPayoffTotDebt");
        }

        protected override CPageData DataLoan 
        {
            get { return new CLiaRecordData(LoanID); }
        }

        protected override IRecordCollection LoadRecordList(CPageData dataLoan, CAppData dataApp) 
        {
            return dataApp.aLiaCollection;
        }


        protected override void LoadData(CPageData dataLoan, CAppData dataApp, ICollectionItemBase2 record) 
        {
            NameValueCollection ret = new NameValueCollection();
            ILiabilityRegular field = (ILiabilityRegular) record;

            if (null != field) 
            {
                SetResult("RecordID", field.RecordId);

                SetResult("DebtT", field.DebtT);
                SetResult("DebtT_rep", CLiaRegular.DisplayStringOfDebtT(field.DebtT));
                SetResult("VerifExpD", field.VerifExpD);
                SetResult("VerifRecvD", field.VerifRecvD);
                SetResult("VerifSentD", field.VerifSentD);
                SetResult("VerifReorderedD", field.VerifReorderedD);
                SetResult("Bal", field.Bal_rep);
                SetResult("Pmt", field.Pmt_rep);
                SetResult("Rate", field.R_rep);
                SetResult("AccNum", field.AccNum.Value);
                SetResult("ComAddr", field.ComAddr);
                SetResult("ComCity", field.ComCity);
                SetResult("ComState", field.ComState);
                SetResult("ComZip", field.ComZip);
                SetResult("Desc", field.Desc);
                SetResult("ComPhone", field.ComPhone);
                SetResult("ComFax", field.ComFax);
                SetResult("AccNm", field.AccNm);
                SetResult("ComNm", field.ComNm);
                SetResult("OrigTerm", field.OrigTerm_rep);
                SetResult("OwnerT", field.OwnerT);
                SetResult("OwnerT_rep", CLiaFields.DisplayStringOfOwnerT(field.OwnerT));
                SetResult("Due", field.Due_rep);
                SetResult("RemainMons", field.RemainMons_raw);
                SetResult("WillBePdOff", field.WillBePdOff);
                SetResult("UsedInRatio", (!field.NotUsedInRatio));
                SetResult("IsPiggyBack", field.IsPiggyBack);
                SetResult("Late30", field.Late30_rep);
                SetResult("Late60", field.Late60_rep);
                SetResult("Late90Plus", field.Late90Plus_rep);
                SetResult("IncInReposession", field.IncInReposession);
                SetResult("IncInBankruptcy", field.IncInBankruptcy);
                SetResult("IncInForeclosure", field.IncInForeclosure);
                SetResult("ExcFromUnderwriting", field.ExcFromUnderwriting);
                SetResult("MatchedReRecordId", field.MatchedReRecordId);
                if (HasPmlEnabled) 
                {
                    SetResult("PmlAuditTrailXmlContent", BuildAuditHistoryTable(field.PmlAuditTrailXmlContent));
                }

                // If sLT is FHA then handle FHA specific fields.
                if (dataLoan.sLT == E_sLT.FHA) 
                {
                    SetResult("OrigDebtAmt", field.OrigDebtAmt_rep );
                    SetResult("AutoYearMake", field.AutoYearMake );
                    SetResult("IsForAuto", field.IsForAuto );
                    SetResult("IsMortFHAInsured", field.IsMortFHAInsured );
                }

                SetResult("PayoffAmtLckd", field.PayoffAmtLckd);
                SetResult("PayoffAmt", field.PayoffAmt_rep);
                SetResult("PayoffTimingLckd", field.PayoffTimingLckd);
                SetResult("PayoffTiming", field.PayoffTiming);
            }
            ILiaCollection recordList = dataApp.aLiaCollection;

            // Load liability's total value.
            SetResult("aLiaBalTot", dataApp.aLiaBalTot_rep);
            SetResult("aLiaMonTot", dataApp.aLiaMonTot_rep);
            SetResult("aLiaPdOffTot", dataApp.aLiaPdOffTot_rep);
            SetResult("sLiaBalLTot", dataLoan.sLiaBalLTot_rep);
            SetResult("sLiaMonLTot", dataLoan.sLiaMonLTot_rep);
            SetResult("sRefPdOffAmt", dataLoan.sRefPdOffAmt_rep);

            // Load special record value.
            var alimony = recordList.GetAlimony(false);
            if (null != alimony) 
            {
                SetResult("Alimony_ComNm", alimony.OwedTo);
                SetResult("Alimony_Pmt", alimony.Pmt_rep);
                SetResult("Alimony_RemainMons", alimony.RemainMons_rep);
                SetResult("Alimony_NotUsedInRatio", alimony.NotUsedInRatio);
            } 
            else 
            {
                SetResult("Alimony_ComNm", "");
                SetResult("Alimony_Pmt", "");
                SetResult("Alimony_RemainMons", "");
                SetResult("Alimony_NotUsedInRatio", false);

            }

            var childSupport = recordList.GetChildSupport(false);
            SetResult("ChildSupport_ComNm", childSupport?.OwedTo ?? string.Empty);
            SetResult("ChildSupport_Pmt", childSupport?.Pmt_rep ?? string.Empty);
            SetResult("ChildSupport_RemainMons", childSupport?.RemainMons_rep ?? string.Empty);
            SetResult("ChildSupport_NotUsedInRatio", childSupport?.NotUsedInRatio ?? false);

            var jobRelated = recordList.GetJobRelated1(false);
            if (null != jobRelated) 
            {
                SetResult("JobRelated1_Pmt", jobRelated.Pmt_rep);
                SetResult("JobRelated1_ComNm", jobRelated.ExpenseDesc);
                SetResult("JobRelated1_NotUsedInRatio", jobRelated.NotUsedInRatio);
            } 
            else 
            {
                SetResult("JobRelated1_Pmt", "");
                SetResult("JobRelated1_ComNm", "");
                SetResult("JobRelated1_NotUsedInRatio", false);

            }

            jobRelated = recordList.GetJobRelated2(false);
            if (null != jobRelated) 
            {
                SetResult("JobRelated2_ComNm", jobRelated.ExpenseDesc);
                SetResult("JobRelated2_Pmt", jobRelated.Pmt_rep);
                SetResult("JobRelated2_NotUsedInRatio", jobRelated.NotUsedInRatio);

            } 
            else 
            {
                SetResult("JobRelated2_ComNm", "");
                SetResult("JobRelated2_Pmt", "");
                SetResult("JobRelated2_NotUsedInRatio", false);

            }

        }

        private class AuditItem : IComparable 
        {
            public string EventDate = "";
            public string UserName = "";
            public string Field = "";
			public string Action = "";
            public string Value = "";
			public string LoginId = "";
            public int CompareTo(object o) 
            {
                AuditItem _o = (AuditItem) o;

                DateTime dt1 = DateTime.Parse(EventDate.Replace("PDT", "").Replace("PST", ""));
                DateTime dt2 = DateTime.Parse(_o.EventDate.Replace("PDT", "").Replace("PST", ""));
                

                return dt2.CompareTo(dt1); // 10/9/2004 dd - Compare in DESCENDING order
            }
        }
        private ArrayList SortAuditList(XmlNodeList list) 
        {
            ArrayList ret = new ArrayList();
            foreach (XmlElement el in list) 
            {
                AuditItem o = new AuditItem();
                o.EventDate = el.GetAttribute("EventDate");
                o.UserName = el.GetAttribute("UserName");
				o.LoginId = el.GetAttribute("LoginId");		//-jM OPM 17403
                o.Action = el.GetAttribute("Action");
				o.Field = el.GetAttribute("Field");
                o.Value = el.GetAttribute("Value");				
                ret.Add(o);

            }

            ret.Sort();
            return ret;
        }
        private string BuildAuditHistoryTable(string xml) 
        {
            string noneHtml = "<table width='100%'><tr><td align=center>None</td></tr></table>";
            if (null == xml || "" == xml)
                return noneHtml;

            StringBuilder sb = new StringBuilder();

            try 
            {
                sb.Append("<table cellpadding=0 cellspacing=0 border=1 rules=all class=AuditTable>");
                sb.Append("<tr class=AuditTableHeader><td>Date</td><td>User Name</td><td>Login Name</td><td>Action</td><td>Field</td><td>Value</td></tr>");
                
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                ArrayList list = SortAuditList(doc.SelectNodes("//History/Event"));

                bool isOdd = true;
				
				//-jM OPM 17403			
                foreach (AuditItem o in list) 
                {
                    string className = isOdd ? "AuditItem" : "AuditAlternatingItem";
                    sb.AppendFormat("<tr class={0}><td nowrap>{1}</td><td nowrap>{2}</td><td nowrap>{3}</td><td nowrap>{4}</td><td>{5}</td><td>{6}</td></tr>",
                        className, o.EventDate, o.UserName, o.LoginId, o.Action, o.Field, o.Value);
                    isOdd = !isOdd;

                }
                sb.Append("</table>");
                return sb.ToString();
            } 
            catch (Exception exc) 
            {
                Tools.LogErrorWithCriticalTracking("Unable to build Liability Audit Table", exc);
                return noneHtml;

            }
        }

        private void AddToReo() 
        {
            Guid reoId = GetGuid("ReoId");
            decimal bal = 0.0M;
            decimal payment = 0.0M;

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LiabilityRecordService));
            dataLoan.InitSave(sFileVersion);

            try 
            {
                bal = dataLoan.m_convertLos.ToMoney(GetString("Bal"));
            } 
            catch {}

            try 
            {
                payment = dataLoan.m_convertLos.ToMoney(GetString("Payment"));
            } 
            catch {}

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            var reFields = dataApp.aReCollection.GetRegRecordOf(reoId);
            reFields.MAmt += bal;
            reFields.MPmt += payment;
            reFields.Update();

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }
	}
}
