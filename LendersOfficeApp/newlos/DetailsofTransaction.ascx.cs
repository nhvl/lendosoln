﻿namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;

    public partial class DetailsofTransaction1 : BaseLoanUserControl
    {
        public bool hideHeaderAndBoarder { get; set; }

        protected bool IsRenovationLoan { get; set; }

        protected bool m_isPurchase = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(DetailsofTransaction1));
            dataLoan.InitLoad();

            var isConstruction = dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            bool areConstructionLoanDataPointsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V31_ConstructionLoans_ExposeNewDataPoints);

            ((BasePage)this.Page).RegisterJsGlobalVariables("IsConstruction", isConstruction);
            ((BasePage)this.Page).RegisterJsGlobalVariables("AreConstructionLoanDataPointsMigrated", areConstructionLoanDataPointsMigrated);

            sPurchPrice.Text = dataLoan.sPurchPrice_rep;

            if (areConstructionLoanDataPointsMigrated)
            {
                sPurchPrice.ReadOnly = dataLoan.sLPurposeT != E_sLPurposeT.Purchase;
            }
            else
            {
                sPurchPrice.ReadOnly = dataLoan.sLPurposeT != E_sLPurposeT.Purchase && dataLoan.sLPurposeT != E_sLPurposeT.Construct && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm;
            }

            sPurchasePrice1003.Text = dataLoan.sPurchasePrice1003_rep;
            m_isPurchase = dataLoan.sLPurposeT != E_sLPurposeT.Purchase && dataLoan.sLPurposeT != E_sLPurposeT.Construct && dataLoan.sLPurposeT != E_sLPurposeT.ConstructPerm;
            this.IsRenovationLoan = dataLoan.sIsRenovationLoan;
            sAltCost.Text = dataLoan.sAltCost_rep;
            sAltCost.ReadOnly = areConstructionLoanDataPointsMigrated && isConstruction;
            sAltCostLckd.Checked = dataLoan.sAltCostLckd;
            sAltCostLckd.Disabled = IsReadOnly || !dataLoan.sIsRenovationLoan || (areConstructionLoanDataPointsMigrated && isConstruction);
            sLandCost.Text = dataLoan.sLandCost_rep;
            sLandIfAcquiredSeparately1003.Text = dataLoan.sLandIfAcquiredSeparately1003_rep;
            sRefPdOffAmt1003.Text = dataLoan.sRefPdOffAmt1003_rep;
            sRefPdOffAmt1003.ReadOnly = areConstructionLoanDataPointsMigrated && isConstruction;
            sRefPdOffAmt1003Lckd.Checked = dataLoan.sRefPdOffAmt1003Lckd;
            sRefPdOffAmt1003Lckd.Enabled = !areConstructionLoanDataPointsMigrated || !isConstruction;
            sTotEstPp1003.Text = dataLoan.sTotEstPp1003_rep;

            if (dataLoan.sIsIncludeProrationsIn1003Details && dataLoan.sIsIncludeProrationsInTotPp)
            {
                sTotEstPp.Text = dataLoan.sTotEstPp_rep;
                sTotalBorrowerPaidProrations.Text = dataLoan.sTotalBorrowerPaidProrations_rep;
            }
            else
            {
                this.PrepaidsAndReservesLabel.Visible = false;
                this.sTotEstPp.Visible = false;
                this.ProrationsPaidByBorrowerLabel.Visible = false;
                this.sTotalBorrowerPaidProrations.Visible = false;
            }

            sTotEstPp1003Lckd.Checked = dataLoan.sTotEstPp1003Lckd;
            sTotEstCc1003Lckd.Checked = dataLoan.sTotEstCc1003Lckd;
            sTotEstCcNoDiscnt1003.Text = dataLoan.sTotEstCcNoDiscnt1003_rep;

            if (dataLoan.sIsIncludeONewFinCcInTotEstCc)
            {
                sTotEstCcNoDiscnt.Text = dataLoan.sTotEstCcNoDiscnt_rep;
                sONewFinCc2.Text = dataLoan.sONewFinCc_rep;
                this.HideIfOtherFinancingCcIncludedInCc.Visible = false;

                var cells = new List<System.Web.UI.HtmlControls.HtmlTableCell>
                {
                    this.LabelCellM,
                    this.LockedCellM,
                    this.AmountCellM
                };

                foreach (var cell in cells)
                {
                    cell.Attributes["rowspan"] = "3";
                    cell.Attributes["class"] = cell.Attributes["class"] + " align-bottom";
                }
            }
            else
            {
                this.DisplayIfOtherFinancingCcIncludedInCc.Visible = false;
            }

            sLDiscnt1003.Text = dataLoan.sLDiscnt1003_rep;
            sLDiscnt1003Lckd.Checked = dataLoan.sLDiscnt1003Lckd;
            sFfUfmip1003Lckd.Checked = dataLoan.sFfUfmip1003Lckd;
            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sTotTransC.Text = dataLoan.sTotTransC_rep;
            sONewFinBal.Text = dataLoan.sONewFinBal_rep;
            sTotCcPbs.Text = dataLoan.sTotCcPbs_rep;
            sTotCcPbsLocked.Checked = dataLoan.sTotCcPbsLocked;
            sOCredit1Lckd.Checked = dataLoan.sOCredit1Lckd;
            sOCredit1Amt.Text = dataLoan.sOCredit1Amt_rep;
            sOCredit1Desc.Text = dataLoan.sOCredit1Desc;
            sOCredit2Amt.Text = dataLoan.sOCredit2Amt_rep;
            sOCredit2Desc.Text = dataLoan.sOCredit2Desc;

            sOCredit3Amt.Text = dataLoan.sOCredit3Amt_rep;
            sOCredit3Desc.Text = dataLoan.sOCredit3Desc;
            sOCredit4Amt.Text = dataLoan.sOCredit4Amt_rep;
            sOCredit4Desc.Text = dataLoan.sOCredit4Desc;

            if (dataLoan.sLoads1003LineLFromAdjustments)
            {
                sOCredit1Lckd.Enabled = false;
                sOCredit2Amt.ReadOnly = true;
                sOCredit2Desc.ReadOnly = true;
                sOCredit3Amt.ReadOnly = true;
                sOCredit3Desc.ReadOnly = true;
                sOCredit4Amt.ReadOnly = true;
                sOCredit4Desc.ReadOnly = true;
            }

            sOCredit5Amt.Text = dataLoan.sOCredit5Amt_rep;
            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            sLAmtLckd.Checked = dataLoan.sLAmtLckd;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sTransNetCash.Text = dataLoan.sTransNetCash_rep;
            sTransNetCashLckd.Checked = dataLoan.sTransNetCashLckd;
            sONewFinCc.Text = dataLoan.sONewFinCc_rep;
            sLoads1003LineLFromAdjustments.Checked = dataLoan.sLoads1003LineLFromAdjustments;

            if (dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID || this.hideHeaderAndBoarder)
            {
                this.adjustmentsBracket.Visible = false;
                this.adjustmentsAction.Visible = false;
            }
            else
            {
                if (dataLoan.sLoads1003LineLFromAdjustments == false)
                {
                    this.syncAdjAndCredBtn.Style["display"] = "";
                    this.linkToAdjAndCred.Style["display"] = "none";
                }
                else
                {
                    this.syncAdjAndCredBtn.Style["display"] = "none";
                    this.linkToAdjAndCred.Style["display"] = "";
                }
            }
        }

        private void Page_Init(object sender, System.EventArgs e)
        {
            BindOtherCreditDescription(sOCredit1Desc);
            BindOtherCreditDescription(sOCredit2Desc);
            BindOtherCreditDescription(sOCredit3Desc);
            BindOtherCreditDescription(sOCredit4Desc);

            //Page.ClientScript.RegisterHiddenField("_ClientID", this.ClientID);
        }

        private void BindOtherCreditDescription(MeridianLink.CommonControls.ComboBox cb)
        {
            cb.Items.Add("Cash Deposit on sales contract");
            cb.Items.Add("Seller Credit");
            cb.Items.Add("Lender Credit");
            cb.Items.Add("Relocation Funds");
            cb.Items.Add("Employer Assisted Housing");
            cb.Items.Add("Lease Purchase Fund");
            cb.Items.Add("Borrower Paid Fees");
            cb.Items.Add("Paid Outside of Closing");
            cb.Items.Add("Broker Credit");

        }
    }
}