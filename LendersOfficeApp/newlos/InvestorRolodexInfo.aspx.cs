﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using System.Web.Services;

namespace LendersOfficeApp.newlos
{
    public partial class InvestorRolodexInfo : BaseLoanPage
    {
        protected override LendersOffice.Security.Permission[] RequiredReadPermissions
        {
            get
            {
                return new LendersOffice.Security.Permission[] {
                    LendersOffice.Security.Permission.AllowViewingInvestorInformation
                };
            }
        }

        protected override LendersOffice.Security.Permission[] RequiredWritePermissions
        {
            get
            {
                return new LendersOffice.Security.Permission[] {
                    LendersOffice.Security.Permission.AllowEditingInvestorInformation
                };
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageID = "InvestorRolodexInfo";
            CPageData data = CPageData.CreateUsingSmartDependency(LoanID, typeof(InvestorRolodexInfo));
            data.InitLoad();

            // 6/26/2014 AV - 185855 System Error when viewing Shipping > Investor Info page
            data.ByPassFieldSecurityCheck = true;
            if (data.sIsInvestorRateLocked)
            {
                // 6/12/2014 dd - Don't allow to change investor if loan has investor rate lock.
                sInvestorRolodexEntries.Enabled = false;
            }
            data.ByPassFieldSecurityCheck = false;

            var investorInfo = data.GetInvestorInfo();
            BindInvestorNames(data.sInvestorRolodexId);
            this.EnableJqueryMigrate = false;

            SellerId.Text = investorInfo.SellerId;
            MersOrganizationId.Text = investorInfo.MERSOrganizationId;
            CompanyName.Text = investorInfo.CompanyName;
            MainContactName.Text = investorInfo.MainContactName;
            MainAttention.Text = investorInfo.MainAttention;
            MainEmail.Text = investorInfo.MainEmail;
            MainAddress.Text = investorInfo.MainAddress;
            MainCity.Text = investorInfo.MainCity;
            MainState.Value = investorInfo.MainState;
            MainZip.Text = investorInfo.MainZip;
            MainPhone.Text = investorInfo.MainPhone;
            MainFax.Text = investorInfo.MainFax;


            NoteShipToAttention.Text = investorInfo.NoteShipToAttention;
            NoteShipToContactName.Text = investorInfo.NoteShipToContactName;
            NoteShipToAddress.Text = investorInfo.NoteShipToAddress;
            NoteShipToCity.Text = investorInfo.NoteShipToCity;
            NoteShipToState.Value = investorInfo.NoteShipToState;
            NoteShipToZip.Text = investorInfo.NoteShipToZip;
            NoteShipToEmail.Text = investorInfo.NoteShipToEmail;
            NoteShipToPhone.Text = investorInfo.NoteShipToPhone;
            NoteShipToFax.Text = investorInfo.NoteShipToFax;


            PaymentToAttention.Text = investorInfo.PaymentToAttention;
            PaymentToContactName.Text = investorInfo.PaymentToContactName;
            PaymentToAddress.Text = investorInfo.PaymentToAddress;
            PaymentToCity.Text = investorInfo.PaymentToCity;
            PaymentToState.Value = investorInfo.PaymentToState;
            PaymentToZip.Text = investorInfo.PaymentToZip;
            PaymentToEmail.Text = investorInfo.PaymentToEmail;
            PaymentToPhone.Text = investorInfo.PaymentToPhone;
            PaymentToFax.Text = investorInfo.PaymentToFax;


            LossPayeeAttention.Text = investorInfo.LossPayeeAttention;
            LossPayeeContactName.Text = investorInfo.LossPayeeContactName;
            LossPayeeAddress.Text = investorInfo.LossPayeeAddress;
            LossPayeeCity.Text = investorInfo.LossPayeeCity;
            LossPayeeState.Value = investorInfo.LossPayeeState;
            LossPayeeZip.Text = investorInfo.LossPayeeZip;
            LossPayeeEmail.Text = investorInfo.LossPayeeEmail;
            LossPayeePhone.Text = investorInfo.LossPayeePhone;
            LossPayeeFax.Text = investorInfo.LossPayeeFax;

            Tools.Bind_sHmdaPurchaser2015T(sHmdaPurchaser2015T, HmdaPurchaser2015T.LifeInsCreditUnion);
            Tools.SetDropDownListValue(sHmdaPurchaser2015T, investorInfo.HmdaPurchaser2015T);
        }

        public void BindInvestorNames(int selected)
        {
            List<InvestorRolodexEntry> entries = InvestorRolodexEntry.GetAll(PrincipalFactory.CurrentPrincipal.BrokerId, null).ToList();
            sInvestorRolodexEntries.DataSource = entries.Where(p => (p.Status == E_InvestorStatus.Active || p.Id == selected) && ( p.InvestorRolodexType == E_InvestorRolodexType.Investor));
            sInvestorRolodexEntries.DataTextField = "InvestorName";
            sInvestorRolodexEntries.DataValueField = "Id";
            sInvestorRolodexEntries.DataBind();

            sInvestorRolodexEntries.Items.Insert(0, new ListItem("<-- Select an Investor -->", "-1"));
            Tools.SetDropDownListValue(sInvestorRolodexEntries, selected);
        }

        [WebMethod]
        public static object GetInvestorInfo(int id)
        {
            return InvestorRolodexEntry.Get(PrincipalFactory.CurrentPrincipal.BrokerId, id);
        }


        [WebMethod]
        public static void Save(Guid sLId, int id, int fileVersion)
        {
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowEditingInvestorInformation))
            {
                throw new AccessDenied();
            }
            CPageData data = CPageData.CreateUsingSmartDependency(sLId, typeof(InvestorRolodexInfo));
            data.InitSave(fileVersion);
            data.sInvestorRolodexId = id;
            data.Save();
        }
    }
}
