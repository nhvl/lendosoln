using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos
{
    public partial class BorrowerList : BaseLoanPage
    {

        protected bool IsLoansPQFile = false;
        private int m_appCount;
        private Guid m_lastAppId = Guid.Empty;

        protected void PageLoad(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            if (Page.IsPostBack && Request.Form["action"] == "UpdatePrimary")
            {
                try
                {
                    UpdatePrimaryBorrower();
                }
                catch (LoanFieldWritePermissionDenied ex)
                {
                    AddInitScriptFunctionWithArgs("f_displayAlertMessage", AspxTools.JsString(ex.UserMessage));
                }
            }
        }

        protected override void LoadData() 
        {

            CPageData dataLoan = new CBorrowerInfoData(LoanID); 
            dataLoan.InitLoad();
            sFileVersion = dataLoan.sFileVersion;

            m_appCount = dataLoan.nApps;
            DataTable table = CreateDataTable();

            for (int i = 0; i < m_appCount; i++) 
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                AddRow(table, dataApp.aIsPrimary, dataApp.aAppId, Utilities.SafeHtmlString(dataApp.aBNm), Utilities.SafeHtmlString(dataApp.aCNm));
                if (i + 1 == m_appCount)
                {
                    m_lastAppId = dataApp.aAppId;
                }
            }


            IsLoansPQFile = dataLoan.sLpqLoanNumber != 0;

           
            m_dg.DataSource = table.DefaultView;
            m_dg.DataBind();
            ClientScript.RegisterHiddenField("action", "");
        }

        protected HtmlInputRadioButton DisplayRadioBox(bool isPrimary, Guid applicantID, string name) 
        {
            // <input value="391c93e0-a9cd-49aa-be1e-a736008f9937" name="rb" type="radio" id="391c93e0-a9cd-49aa-be1e-a736008f9937" checked="checked" onclick="confirmPrimaryBorrower(&#39;Test This Loan&#39;);" />
            var radio = new HtmlInputRadioButton();
            radio.ID = applicantID.ToString();
            radio.Value = applicantID.ToString();
            radio.Name = "rb";
            radio.Checked = isPrimary;
            radio.Attributes.Add("onclick", $"confirmPrimaryBorrower({AspxTools.JsString(name)});");
            return radio;
        }
        private void AddRow(DataTable table, bool isPrimary, Guid aAppId, string borrowerName, string coborrowerName) 
        {
            DataRow row = table.NewRow();
            row["IsPrimary"] = isPrimary;
            string ch = coborrowerName.TrimWhitespaceAndBOM() == "" ? "" : " & ";
            row["Name"] = borrowerName + ch + coborrowerName;
            row["aAppId"] = aAppId;

            table.Rows.Add(row);
        }
        private DataTable CreateDataTable() 
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("aAppId", typeof(Guid)));
            table.Columns.Add(new DataColumn("IsPrimary", typeof(bool)));
            table.Columns.Add(new DataColumn("Name", typeof(string)));

            return table;
        }


        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Multi borrowers";
            this.PageID = "BorrowerList";
        }
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.m_dg.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.m_dg_ItemCreated);
            this.m_dg.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.m_dg_ItemCommand);
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        private void UpdatePrimaryBorrower()
        {
            // Update primary borrower.
            Guid applicationID = new Guid(Request.Form["rb"]);
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(applicationID);
            dataApp.aIsPrimary = true;

            dataLoan.Save();

            AddInitScriptFunctionWithArgs("updateBorrowerPrimary", "'" + applicationID + "'");
            LoadData();

        }

        protected void m_addBtn_Click(object sender, System.EventArgs e)
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);

            int appIndex = dataLoan.AddNewApp();

            CAppData dataApp = dataLoan.GetAppData(appIndex);
            Response.Redirect("BorrowerInfo.aspx?loanid=" + LoanID + "&appid=" + dataApp.aAppId + "&cmd=new");
        }

        private void m_dg_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (IsReadOnly) 
                {
                    // Don't display anything.
                    e.Item.Cells[2].Controls.Clear();
                    e.Item.Cells[3].Controls.Clear();
                    e.Item.Cells[4].Controls.Clear();
                } 
                else 
                {
                    e.Item.Cells[2].Attributes.Add("onclick", "return confirm('Do you want to swap borrower & spouse?');");
                    e.Item.Cells[3].Attributes.Add("onclick", "return confirm('Do you want to delete all data of spouse?');");

                    if (m_appCount == 1) 
                    {
                        // DO NOT display delete Married Borrower & Coborrower link if there is only one application.
                        e.Item.Controls[4].Controls.Clear();
                    } 
                    else 
                    {
                        e.Item.Cells[4].Attributes.Add("onclick", @"return confirm('Please note: for loans with an IRS 4506-T order, removing a borrower will also remove that IRS order.\n\nDo you want to delete all data of borrower and spouse?');");
                    }

                    if (IsLoansPQFile)
                    {
                        foreach (Control ctrl in e.Item.Controls[2].Controls)
                        {
                            ctrl.Visible = false;
                        }
                        Guid applicationID = (Guid) m_dg.DataKeys[e.Item.ItemIndex];

                        if (m_lastAppId != applicationID)
                        {
                            foreach (Control ctrl in e.Item.Controls[4].Controls)
                            {
                                ctrl.Visible = false;
                            }
                        }
                
                    }
                }
            }
        }

        private void m_dg_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            Guid applicationID = (Guid) m_dg.DataKeys[e.Item.ItemIndex];

            try
            {
                switch (e.CommandName)
                {
                    case "swap":
                        SwapBorrowerCoborrower(applicationID);
                        break;
                    case "deletecoborrower":
                        DeleteCoborrower(applicationID);
                        break;
                    case "delete":
                        DeleteBorrowerCoborrower(applicationID);
                        break;
                }
            }
            catch (CBaseException exc)
            {
                AddInitScriptFunctionWithArgs("f_displayAlertMessage", AspxTools.JsString(exc.UserMessage));
            }
            LoadData();
        }

        private void DeleteBorrowerCoborrower(Guid applicationID)
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.DelApp(applicationID);
            AddInitScriptFunctionWithArgs("deleteBorrower", "'" + applicationID + "'");
        }

        private void SwapBorrowerCoborrower(Guid applicationID) 
        {
            CPageData dataLoan = new CSwapCoborrowerData(LoanID);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(applicationID);
            dataApp.SwapMarriedBorAndCobor();
            dataLoan.Save();
        }
        private void DeleteCoborrower(Guid applicationID) 
        {
            CPageData dataLoan = new CDeleteCoborrowerData(LoanID);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(applicationID);
            dataApp.DelMarriedCobor();
            dataLoan.Save();

        }


    }
}
