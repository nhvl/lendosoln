namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.TRID2;

    public partial  class BorrowerAssetFrame : BaseLoanUserControl, IAutoLoadUserControl
	{
        protected string ListUrl
        {
            get { return "BorrowerAssetList.aspx?loanid=" + LoanID + "&appid=" + ApplicationID; }
        }
        protected string EditUrl
        {
            get { return "AssetRecord.aspx?loanid=" + LoanID + "&appid=" + ApplicationID; }
        }
        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(BorrowerAssetFrame));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IAssetCollection recordList = dataApp.aAssetCollection;

            var business = recordList.GetBusinessWorth(false);
            if (null != business) 
            {
                Business_Val.Text = business.Val_rep;
            }

            var cashDeposit = recordList.GetCashDeposit1(false);
            if (null != cashDeposit) 
            {
                CashDeposit1_Desc.Text = cashDeposit.Desc;
                CashDeposit1_Val.Text = cashDeposit.Val_rep;
            }

            cashDeposit = recordList.GetCashDeposit2(false);
            if (null != cashDeposit) 
            {
                CashDeposit2_Desc.Text = cashDeposit.Desc;
                CashDeposit2_Val.Text = cashDeposit.Val_rep;
            }
			CashDeposit1_Desc.ToolTip = CashDeposit2_Desc.ToolTip = "Enter description";
			CashDeposit1_Val.ToolTip  = CashDeposit2_Val.ToolTip  = "Enter cash/market value ($)";

            var insurance = recordList.GetLifeInsurance(false);
            if (null != insurance) 
            {
                LifeInsurance_FaceVal.Text = insurance.FaceVal_rep;
                LifeInsurance_Val.Text = insurance.Val_rep;
            }
			LifeInsurance_FaceVal.ToolTip = "Life insurance face amount ($)";
			LifeInsurance_Val.ToolTip     = "Life insurance net cash value ($)";

            var retirement = recordList.GetRetirement(false);
            if (null != retirement) 
            {
                Retirement_Val.Text = retirement.Val_rep;
            }

            aAsstLiqTot.Text = dataApp.aAsstLiqTot_rep;
            aReTotVal.Text = dataApp.aReTotVal_rep;
            aAsstNonReSolidTot.Text = dataApp.aAsstNonReSolidTot_rep;
            aAsstValTot.Text = dataApp.aAsstValTot_rep;

            aAsstLiaCompletedJointly.Checked = !dataApp.aAsstLiaCompletedNotJointly;
            aAsstLiaCompletedNotJointly.Checked = dataApp.aAsstLiaCompletedNotJointly;

            Tools.Bind_ResponsibleLien(sLienToIncludeCashDepositInTridDisclosures);
            Tools.SetDropDownListValue(sLienToIncludeCashDepositInTridDisclosures, dataLoan.sLienToIncludeCashDepositInTridDisclosures);

            bool IsTrid2017 = dataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017;
            bool HasNonZeroNewOtherFinancing = (dataLoan.sLienPosT == E_sLienPosT.First && dataLoan.sIsOFinNew && dataLoan.sConcurSubFin > 0M) || dataLoan.sLienPosT == E_sLienPosT.Second;

            ((BasePage)this.Page).RegisterJsGlobalVariables("IsTrid2017", IsTrid2017);
            ((BasePage)this.Page).RegisterJsGlobalVariables("HasNonZeroNewOtherFinancing", HasNonZeroNewOtherFinancing);
        }

        public void SaveData() 
        {
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            // Only add this javascript when this usercontrol is visible, else borrower info, monthly income tab will not work.
            if (this.Visible) 
            {
                Page.ClientScript.RegisterHiddenField("ListLocation", "BorrowerInfo.aspx?loanid=" + LoanID + "&pg=5");
                ((BasePage)this.Page).RegisterJsScript("singleedit2.js");
                ((BasePage)this.Page).RegisterJsScript("LQBPrintFix.js");
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

	}
}
