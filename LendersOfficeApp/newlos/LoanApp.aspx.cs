using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.ObjLib.DocMagicLib;
using LendersOffice.Integration.DocumentVendor;
using System.Linq;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos
{
	public partial class LoanApp : BasePage
	{
        protected string m_sLNm = "";
        protected bool m_isLpqLoan = false;

        /// <summary>
        /// Returns a string, containing the mode that the document mode that the page should display as in IE.
        /// If the ConstStage bit "IsForceEdgeEverywhere" is true, the LoanEditor will display in edge.  Else, 
        /// it will attempt to display in the first valid mode it can from 10 to 8.
        /// </summary>
        protected string CompMetaValue
        {
            get
            {
                return ConstStage.IsForceEdgeEverywhere ? "IE=Edge;" : "IE=10; IE=9; IE=8;";
            }
        }

        protected string TreeViewSrc
        {
            get
            {
                return String.Format("LeftTreeFrame.aspx?loanid={0}&lpq={1}", RequestHelper.LoanID, m_isLpqLoan ? "1" : "0"); 
            }
        }

        protected string HeaderSrc
        {
            get
            {
                return String.Format("HeaderFrame.aspx?loanid={0}&lpq={1}", RequestHelper.LoanID, m_isLpqLoan ? "1" : "0");
            }
        }

        protected bool IsResize
        {
            get
            {
                return RequestHelper.GetBool("isResize");
            }
        }

        protected string highUrl
        {
            //+ '?'+<%=AspxTools.JsString(Request.QueryString.ToString()) %>);
            get
            {
                try
                {
                    string url = DataAccess.Tools.UrlOptionToUrl((E_UrlOption) Int32.Parse(Request.QueryString["highURL"]));

                    if (url.Equals(""))
                    {
                        String msg = @"The body_url has been given a value that does not match a value in the method UrlOptionToUrl in LosUtils.  This could be 
caused by having an Enum value missing from E_UrlOption, or by not including the Enum in UrlOptionToUrl() in LosUtils to return the page's location.";
                        DataAccess.Tools.LogErrorWithCriticalTracking(msg);

                    }
                    else
                    {
                        if (url.Contains("?"))
                        {
                            foreach(string key in Request.QueryString.Keys)
                            {
                                if(!(url.Contains($"?{key}=") || url.Contains($"&{key}=")))
                                {
                                    url += $"&{key}={Request.QueryString[key]}";
                                }
                            }

                        }
                        else
                        {
                            url += "?" + Request.QueryString.ToString();
                        }

                        var appId = RequestHelper.GetGuid("applicationId", Guid.Empty);
                        if(appId != Guid.Empty)
                        {
                            url += "&appid=" + appId.ToString(); 
                        }

                        return url;
                    }
                }
                 //this will never be hit, RequestHelper.GetInt will never throw an error...
                catch (System.FormatException e)
                {
                    string value = RequestHelper.GetSafeQueryString("highURL");
                    String msg = @" The parameter 'highURL' is given the value '" + value + @"', which cannot be parsed into int.  If '" + value + @"' is an actual url, this can be solved
by adding its pageId, which can be found in LoanNavigationNew.xml.config, as an Enum value for E_urlOption.  Then add the url to the method UrlOptionToUrl() in LosUtils.";

                    DataAccess.Tools.LogErrorWithCriticalTracking(msg, e);
                    
                }

                return "";
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
		{
            if (RequestHelper.GetSafeQueryString("view") == "summary")
            {
                Response.Redirect(String.Format( "~/los/view/PrintPreviewFrame.aspx?loanid={0}&display=t", RequestHelper.LoanID) , true);
            }

            if (RequestHelper.GetBool("isnew")) 
            {

                CPageData dataLoan = CPageData.CreateUsingSmartDependency(RequestHelper.LoanID, typeof(LoanApp));
                dataLoan.InitLoad();
                m_sLNm = dataLoan.sLNm;

            }

            //Needs this for 30792  I'll be cleaning this up after our next release. 
            CPageData lpqData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(RequestHelper.LoanID, typeof(LoanApp));
            lpqData.InitLoad();
            m_isLpqLoan = lpqData.sLpqLoanNumber != 0;

            //We can skip all of the doc magic plan code automatic calculation stuff if we're not automatically calculating plan codes.
            if (DocumentVendorFactory.CurrentVendors().Any(a => a.Skin.AutomaticPlanCodes)) CalculatePlanCode(lpqData);
		}

        private static void CalculatePlanCode(CPageData lpqData)
        {

            // OPM 13035 Piggybacking on this load to check if we need to re-calculate doc magic plan code for this loan.
            // Spec states that when a loan is opened that does not have plan code set, we should calculate it.
            lpqData.ByPassFieldSecurityCheck = true;
            if (lpqData.BrokerDB.BillingVersion == LendersOffice.Admin.E_BrokerBillingVersion.PerTransaction
                && lpqData.BrokerDB.IsEnablePTMDocMagicSeamlessInterface
                && lpqData.sLpInvestorNm != string.Empty        // Calculating plan code only makes sense when investor applied.
                && lpqData.sDocMagicPlanCodeId == Guid.Empty)
            {
                Guid planCode;
                
                var validationResultCollection = ValidateForRunningDocMagicPlanCode(lpqData);
                if (validationResultCollection.ErrorCount != 0)
                {
                    // OPM 83183.
                    // We cannot assign a plan code to this file--the loan data is not
                    // set up for keyword evaluation.  User will get Selection link to 
                    // show them the errors.
                    planCode = DocMagicPlanCode.PendingUserChoice;
                }
                else
                {
                    // OPM 84422.  If the lender does not allow investor plan codes, only run generic.
                    string sLpInvestorNm = lpqData.BrokerDB.AllowInvestorDocMagicPlanCodes ? lpqData.sLpInvestorNm : null;

                    // OPM 324029. This calcmode switch is here for perf so we can reuse the single object.
                    var oldCalcModeT = lpqData.CalcModeT;
                    lpqData.CalcModeT = E_CalcModeT.PriceMyLoan;
                    planCode = DocMagicPlanCodeEvaluator.CalculateDocMagicPlanCode(lpqData.BrokerDB, sLpInvestorNm, lpqData.sSymbolTableForPriceRule);
                    lpqData.CalcModeT = oldCalcModeT;
                }

                if (planCode != lpqData.sDocMagicPlanCodeId)
                {
                    DocMagicPlanCode.SetPlanCode(lpqData.BrokerDB, RequestHelper.LoanID, planCode);
                }
            }
            else if (lpqData.BrokerDB.BillingVersion == LendersOffice.Admin.E_BrokerBillingVersion.PerTransaction
                        && lpqData.BrokerDB.IsEnablePTMDocMagicSeamlessInterface
                        && lpqData.BrokerDB.AllowInvestorDocMagicPlanCodes == false
                        && lpqData.sDocMagicPlanCodeId != Guid.Empty
                        && lpqData.sDocMagicPlanCodeId != DocMagicPlanCode.PendingUserChoice)
            {
                // OPM 84422. This has to be an investor-free plan code.  If not, it should be rerun.
                DocMagicPlanCode currentCode;
                try
                {
                    currentCode = DocMagicPlanCode.Retrieve(lpqData.BrokerDB, lpqData.sDocMagicPlanCodeId);
                }
                catch (PlanCodeNotFoundException exc)
                {
                    // Plan code may not exist anymore. (deleted)
                    // Re-calc it.
                    Tools.LogError(exc);
                    currentCode = DocMagicPlanCode.Create();
                    currentCode.InvestorNm = "NOT FOUND";
                }

                if (currentCode.InvestorNm != "GENERIC")
                {
                    // Generic execution

                    // OPM 324029. This calcmode switch is here for perf so we can reuse the single object.
                    var oldCalcModeT = lpqData.CalcModeT;
                    lpqData.CalcModeT = E_CalcModeT.PriceMyLoan;
                    Guid planCode = DocMagicPlanCodeEvaluator.CalculateDocMagicPlanCode(lpqData.BrokerDB, null, lpqData.sSymbolTableForPriceRule);
                    lpqData.CalcModeT = oldCalcModeT;

                    DocMagicPlanCode.SetPlanCode(lpqData.BrokerDB, RequestHelper.LoanID, planCode);
                }

            }
        }

        private static DataAccess.LoanValidation.LoanValidationResultCollection ValidateForRunningDocMagicPlanCode(CPageData dataLoan)
        {
            // OPM 324029. This calcmode switch is here for perf so we can reuse the single object.
            var oldCalcModeT = dataLoan.CalcModeT;
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            var result = dataLoan.ValidateForRunningDocMagicPlanCode();
            dataLoan.CalcModeT = oldCalcModeT;
            return result;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            this.CompatTag.Attributes.Add("content", this.CompMetaValue);
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
