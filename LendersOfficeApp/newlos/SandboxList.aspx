﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SandboxList.aspx.cs" Inherits="LendersOfficeApp.newlos.SandboxList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Sandbox Loans</title>
</head>
<body class="RightBackground">
    <form id="form1" runat="server">
    <div  style="padding-left:10px;padding-top:5px">
    <input type="button" value="Create Sandbox file" onclick="parent.body.f_createSandbox();" AlwaysEnable="true" />
    <ml:CommonDataGrid ID="m_mainGrid" runat="server" AutoGenerateColumns="False" CssClass="DataGrid" DataKeyField="sLId" EnableViewState="true">
        <alternatingitemstyle cssclass="GridAlternatingItem" />
        <itemstyle cssclass="GridItem" />
        <headerstyle cssclass="GridHeader"/>

        <Columns>
            <asp:TemplateColumn>
            <ItemTemplate>
            <a href="#" onclick="f_openLoan(<%# AspxTools.JsString(Eval("sLId").ToString()) %>, <%# AspxTools.JsString(Eval("sLNm").ToString())%>); return false;">edit</a>
            </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="sLNm" HeaderText="Loan Number" />
            <asp:BoundColumn DataField="sCreatedD" HeaderText="Created Date" />
            <asp:ButtonColumn ButtonType="LinkButton" Text="delete" CommandName="delete" />
        </Columns>
    </ml:CommonDataGrid>
    </div>
    </form>
</body>
</html>
