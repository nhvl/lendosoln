<%@ Register TagPrefix="uc1" TagName="BorrowerMonthlyIncome" Src="BorrowerMonthlyIncome.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BorrowerInfoTab" Src="BorrowerInfoTab.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BorrowerAssetFrame" Src="BorrowerAssetFrame.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BorrowerEmploymentFrame" Src="BorrowerEmploymentFrame.ascx" %>
<%@ Page language="c#" Codebehind="BorrowerInfo.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.BorrowerInfo" %>
<%@ Register TagPrefix="uc1" TagName="BorrowerHousingExpense" Src="BorrowerHousingExpense.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BorrowerREOFrame" Src="BorrowerREOFrame.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BorrowerLiabilityFrame" Src="BorrowerLiabilityFrame.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>BorrowerInfo</title>
      <style>
          .FthbExplanation {
              display: block;
              font-weight: normal;
          }
          .w-98 { width: 98px; }
      </style>
  </head>
  <body bgcolor="gainsboro" scroll=yes onresize="if (typeof(f_onresize) == 'function') f_onresize();">
  <script type="text/javascript">
  function newApp() 
  {
    parent.info.f_updateApplicantDDL('<-- New Borrower -->', ML.aAppId);
  
  }
  </script>

    <form id="BorrowerInfo" method="post" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
      <tr>
        <td nowrap class="Tabs">
            <uc1:Tabs runat="server" ID="Tabs" />
        </td>
      </tr>
          <tr>
            <td class="MainRightHeader" nowrap>
                <%= AspxTools.HtmlString(HeaderDisplay)%>
            </td>
        </tr>
      <tr>
        <td nowrap style="padding-left: 5px">
          <uc1:BorrowerInfoTab ID="BorrowerInfoTab" runat="server" />
          <uc1:BorrowerEmploymentFrame ID="BorrowerEmploymentFrame" runat="server" />
          <uc1:BorrowerEmploymentFrame ID="CoborrowerEmploymentFrame" runat="server" />
          <uc1:BorrowerMonthlyIncome ID="BorrowerMonthlyIncome" runat="server" />
          <uc1:BorrowerAssetFrame ID="BorrowerAssetFrame" runat="server" />
          <uc1:BorrowerLiabilityFrame ID="BorrowerLiabilityFrame" runat="server" />
          <uc1:BorrowerREOFrame ID="BorrowerREOFrame" runat="server" />
          <uc1:BorrowerHousingExpense ID="BorrowerHousingExpense" runat="server" />
          <asp:PlaceHolder ID="MainPlaceHolder" runat="server"></asp:PlaceHolder>
        </td>
      </tr>
    </table>
    </form>
	
  </body>
</html>
