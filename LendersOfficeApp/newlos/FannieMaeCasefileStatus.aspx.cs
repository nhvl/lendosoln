﻿using System;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos
{
    public partial class FannieMaeCasefileStatus : BaseLoanPage
    {
        protected bool m_isDo
        {
            get { return RequestHelper.GetSafeQueryString("isdo") == "t"; }
        }

        private void PageLoad(object sender, EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FannieMaeCasefileStatus));
            dataLoan.InitLoad();
            sDuCaseId.Text = dataLoan.sDuCaseId;

            FannieMaeMORNETUserID.Text = m_isDo ? BrokerUser.DoLastLoginNm : BrokerUser.DuLastLoginNm;

            m_rememberLoginCB.Checked = FannieMaeMORNETUserID.Text != "";
        }

        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.Load += new EventHandler(this.PageLoad);
            this.Init += new EventHandler(this.PageInit);
            base.OnInit(e);
        }

        private void PageInit(object sender, EventArgs e)
        {
            this.DisplayCopyRight = false;

            string label = m_isDo ? "Desktop Originator" : "Desktop Underwriter";
            m_headerLabel.Text = "Get " + label + " Casefile Status";
            m_loginLabel.Text = label + " User ID";
            m_caseIdLabel.Text = label + " Case ID";
            m_passwordLabel.Text = label + " Password";
        }
    }
}
