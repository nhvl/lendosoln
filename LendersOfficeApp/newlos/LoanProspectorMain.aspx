<%@ Page language="c#" Codebehind="LoanProspectorMain.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LoanProspectorMain" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>LoanProspectorMain</title>
  </HEAD>
<body onload=f_submit();>
<script type="text/javascript">
function f_submit() {
  <% if (m_canExportLoan) { %>
  
    <% if (m_hasAutoLogin) { %>
      try
      {
        var oHttp = new XMLHttpRequest();
        oHttp.open("GET", <%= AspxTools.JsString(this.Form.Action) %>, false );
        oHttp.setRequestHeader('Authorization', <%= AspxTools.JsString(m_basicAuthentication) %>);
        oHttp.send();
      } catch (e) {
        document.getElementById("m_waitMessageTable").style.display = "none";
        document.getElementById("help").style.display = "";
        return;
      }
    <% } %>
  document.forms[0].submit();
  <% } %>
}
</script>


  <form name="dataForm" runat="server" method="post">
  <table width="100%" id="m_noExportPermissionTable" runat="server">
    <tr>
      <td style="font-weight: bold; color: red">You do not have permission to export loan to Loan Product Advisor</td>
    </tr>
  </table>
  <table height="100%" width="100%" id="m_waitMessageTable" runat="server">
    <tr>
      <td style="font-weight: bold" valign="middle" align="center">Please wait ...</td>
    </tr>
  </table>
        <iframe src="help/Q00015.html" width="100%" height="100%" id="help" style="display:none"></iframe>
  
  </form>
</body>
</HTML>
