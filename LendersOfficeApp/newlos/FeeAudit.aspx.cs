﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.UI.WebControls;
using DataAccess;
using DataAccess.GFE;
using LendersOffice.Common;
using LendersOffice.Common.SerializationTypes;
using LendersOffice.ObjLib.TRID2;
using LendersOffice.Rolodex;

namespace LendersOfficeApp.newlos
{
    public partial class FeeAudit : BaseLoanPage
    {
        [DataContract]
        public class FeeAuditViewModel
        {
            /// <summary>
            /// Note, this does not include all the closing cost sets, just the ones that are relevant at page load.
            /// </summary>
            [DataMember]
            public Dictionary<Guid, ClosingCostFeeWithSection[]> ClosingCostSetFeeViewsByArchiveID { get; set; }

            [DataMember]
            public ClosingCostFeeWithSection[] LiveClosingDisclosureFeeViews { get; set; }

            [DataMember]
            public ClosingCostFeeWithSection[] LiveLoanEstimateDisclosureFeeViews { get; set; }

            [DataMember]
            public Guid? InitialDisclosedArchiveID { get; set; }

            [DataMember]
            public Guid? LastDisclosedArchiveID { get; set; }

            [DataMember]
            public Dictionary<Guid, string> ArchiveDateByID;

            [DataMember]
            public Dictionary<ClosingCostArchive.E_ClosingCostArchiveType, Guid[]> ArchiveIDsByType;

            [DataMember]
            public Dictionary<int, string> SectionNameByIntegerValue = new Dictionary<int, string>();

            [DataMember]
            public Dictionary<Guid, List<string>> ToleranceBucket = new Dictionary<Guid, List<string>>();

        }

        protected override bool EnableJQueryInitCode => true;

        protected override void OnInit(EventArgs e)
        {
            this.PageID = "FeeAudit";
            this.EnableJquery = true; // Required for EnableJQueryInitCode
            this.EnableJqueryMigrate = false;
            this.UseNewFramework = true; // for registering the service page.
            RegisterJsScript("ThirdParty/ractive-legacy.min.js");

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var viewModel = new FeeAuditViewModel();
            var dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FeeAudit));
            dataLoan.InitLoad();

            var tridTargetVersion = dataLoan.sTridTargetRegulationVersionT;
            viewModel.ArchiveDateByID = dataLoan.sClosingCostArchive.ToDictionary(
                (archive) => archive.Id, 
                (archive) => this.GetArchiveDescription(archive, tridTargetVersion));

            viewModel.ArchiveIDsByType = dataLoan.sClosingCostArchive.GroupBy((archive) => archive.ClosingCostArchiveType).Select((grouping) => new
            {
                Type = grouping.Key,
                IDs = grouping.OrderBy((archive) => DateTime.Parse(archive.DateArchived)).Select((archive) => archive.Id)
            }).ToDictionary((anon) => anon.Type, (anon) => anon.IDs.ToArray());

            var initialEstimate = dataLoan.sLoanEstimateDatesInfo.InitialLoanEstimate;
            if (initialEstimate == null)
            {
                var firstLoanEstimateArchive = dataLoan.sClosingCostArchive.LastOrDefault((archive) => 
                    archive.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate &&
                    ClosingCostArchive.IsDisclosedDataArchive(tridTargetVersion, archive));

                if (firstLoanEstimateArchive != null)
                {
                    viewModel.InitialDisclosedArchiveID = firstLoanEstimateArchive.Id;
                }
                else
                {
                    viewModel.InitialDisclosedArchiveID = null;
                }
            }
            else
            {
                viewModel.InitialDisclosedArchiveID = initialEstimate.ArchiveId;
            }

            if (dataLoan.sLastDisclosedGFEArchiveId == Guid.Empty)
            {
                viewModel.LastDisclosedArchiveID = null;
            }
            else
            {
                viewModel.LastDisclosedArchiveID = dataLoan.sLastDisclosedGFEArchiveId;
            }

            var liveCDBorrowerFees = dataLoan.sClosingCostSet.GetViewBase(E_ClosingCostViewT.LoanClosingCost).SelectMany((closingCostFeeSection) => closingCostFeeSection.FilteredClosingCostFeeList.Select((fee) => new ClosingCostFeeWithSection() { Section = closingCostFeeSection.SectionType, Fee = (BorrowerClosingCostFee)fee.Clone() }));
            var liveCDSellerFees = dataLoan.sSellerResponsibleClosingCostSet.GetViewBase(E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate).SelectMany((closingCostFeeSection) => closingCostFeeSection.FilteredClosingCostFeeList.Select((fee) => new ClosingCostFeeWithSection() { Section = closingCostFeeSection.SectionType, Fee = (SellerClosingCostFee)fee.Clone() })).ToArray();
            foreach (var sellerFee in liveCDSellerFees)
            {
                sellerFee.Fee.GfeResponsiblePartyT = sellerFee.Fee.GfeResponsiblePartyT;
            }

            viewModel.LiveClosingDisclosureFeeViews = liveCDBorrowerFees.Union(liveCDSellerFees).OrderBy((fee) => fee.Section).ToArray();

            // Sending this on page load since it doesn't require reloading the loan, but yes it's not necessary if they don't toggle the current values dropdown.
            var liveLEBorrowerFees = dataLoan.sClosingCostSet.GetViewForSerialization(E_ClosingCostViewT.LoanClosingCost).SelectMany((closingCostFeeSection) => closingCostFeeSection.FilteredClosingCostFeeList.Select((fee) => new ClosingCostFeeWithSection() { Section = closingCostFeeSection.SectionType, Fee = (BorrowerClosingCostFee)fee.Clone() })).ToArray();
            foreach (var fee in liveLEBorrowerFees)
            {
                if (fee.Fee.DidShop)
                {
                    fee.Section = E_IntegratedDisclosureSectionT.SectionC;
                }
                else if (fee.Fee.CanShop)
                {
                    fee.Section = E_IntegratedDisclosureSectionT.SectionB;
                }
            }
            viewModel.LiveLoanEstimateDisclosureFeeViews = liveLEBorrowerFees.OrderBy((fee) => fee.Section).ToArray();

            viewModel.ClosingCostSetFeeViewsByArchiveID = new Dictionary<Guid, ClosingCostFeeWithSection[]>();

            SetupSectionNames(viewModel);

            RegisterJsEnumByName(typeof(E_GfeResponsiblePartyT));
            RegisterJsEnumByName(typeof(E_ClosingCostFeePaymentPaidByT));
            RegisterJsEnumByValue(typeof(E_ClosingCostFeePaymentPaidByT));
            RegisterJsEnumByValue(typeof(E_GfeResponsiblePartyT));
            RegisterJsObjectWithJsonNetSerializer("RoleDescriptionByType", RolodexDB.GetRoleDecriptionByRoleTypeValue());

            Dictionary<Guid, List<string>> toleranceBucket = new Dictionary<Guid, List<string>>();

            if (dataLoan.sLastDisclosedClosingCostArchive != null)
            {
                ClosingCostArchive archive = dataLoan.sLastDisclosedClosingCostArchiveLinkedToleranceArchive;
                BorrowerClosingCostSet archiveSet = new BorrowerClosingCostSet(archive);
                BorrowerClosingCostSet tenPercentArchiveSet = new BorrowerClosingCostSet(dataLoan.sClosingCostArchive.FirstOrDefault(p => p.Id.Equals(dataLoan.sTolerance10BasisLEArchiveId)));

                // Made from an archive. Filter out Orig Comp fee based on archive values.
                Func<BaseClosingCostFee, bool> archiveSetFilter = archiveFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)archiveFee, archiveSet.ClosingCostArchive);
                Func<BaseClosingCostFee, bool> tenPercFilter = tenPercFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)tenPercFee, tenPercentArchiveSet.ClosingCostArchive);

                foreach (LendersOffice.Common.SerializationTypes.ClosingCostFeeWithSection fee in liveCDBorrowerFees)
                {
                    if (fee.Fee.TotalAmount != 0)
                    {
                        if (GfeToleranceViolationCalculator.ConsiderFeeForTenToleranceInTRIDMode((BorrowerClosingCostFee)fee.Fee))
                        {
                            AddFeeToleranceBucket(fee.Fee, tenPercentArchiveSet, tenPercFilter, toleranceBucket, dataLoan, "10% Tolerance");
                        }
                        else if (GfeToleranceViolationCalculator.ConsiderFeeForZeroToleranceInTRIDMode((BorrowerClosingCostFee)fee.Fee))
                        {
                            AddFeeToleranceBucket(fee.Fee, archiveSet, archiveSetFilter, toleranceBucket, dataLoan, "0% Tolerance");
                        }
                        else if (GfeToleranceViolationCalculator.ConsiderFeeForUnlimitedToleranceInTRIDMode((BorrowerClosingCostFee)fee.Fee))
                        {
                            AddFeeToleranceBucket(fee.Fee, archiveSet, archiveSetFilter, toleranceBucket, dataLoan, "Unlimited Tolerance");
                        }
                    }
                }

                viewModel.ToleranceBucket = toleranceBucket;
            }

            RegisterJsObjectWithJsonNetSerializer("model", viewModel); // The dictionaries turn out ugly unless using Json NET.
        }

        /// <summary>
        /// Gets the description for the archive to display in the dropdown.
        /// </summary>
        /// <param name="archive">
        /// The archive.
        /// </param>
        /// <param name="tridTargetVersion">
        /// The TRID target regulation for the loan file.
        /// </param>
        /// <returns>
        /// The archive's description.
        /// </returns>
        private string GetArchiveDescription(ClosingCostArchive archive, TridTargetRegulationVersionT tridTargetVersion)
        {
            if (tridTargetVersion != TridTargetRegulationVersionT.TRID2017)
            {
                return archive.DateArchived;
            }

            string archiveDataType;
            if (ClosingCostArchive.IsDisclosedDataArchive(tridTargetVersion, archive))
            {
                archiveDataType = " (Disclosed)";
            }
            else
            {
                archiveDataType = " (Tolerance)";
            }

            return archive.DateArchived + archiveDataType;
        }

        private void AddFeeToleranceBucket(LoanClosingCostFee fee, BorrowerClosingCostSet archiveSet, Func<BaseClosingCostFee, bool> archivedSetFilter, Dictionary<Guid, List<string>> toleranceBucket, CPageData dataLoan, string toleranceDesc)
        {
            decimal amount = 0;
            decimal archiveAmount = 0;
            Func<BaseClosingCostFee, bool> finalizedFilter = archiveSetFee => archivedSetFilter(archiveSetFee) &&
                                                                           fee.ClosingCostFeeTypeId == archiveSetFee.ClosingCostFeeTypeId;

            BorrowerClosingCostFee archiveFee = archiveSet.GetFees(finalizedFilter).Cast<BorrowerClosingCostFee>().FirstOrDefault();

            if (archiveFee != null && archiveFee.IsValidFee())
            {
                archiveAmount = archiveFee.TotalAmount;
            }

            amount = fee.TotalAmount - archiveAmount;

            toleranceBucket.Add(fee.ClosingCostFeeTypeId, new List<string> { 
                         toleranceDesc,
                         dataLoan.m_convertLos.ToMoneyString(amount, FormatDirection.ToRep).Replace("(", "").Replace(")", ""),
                         (amount == 0).ToString().ToLower(),
                         (amount >= 0).ToString().ToLower()});

        }

        private void SetupSectionNames(FeeAuditViewModel model)
        {
            model.SectionNameByIntegerValue.Clear();
            foreach (E_IntegratedDisclosureSectionT section in Enum.GetValues(typeof(E_IntegratedDisclosureSectionT)))
            {
                model.SectionNameByIntegerValue.Add((int)section, Tools.GetIntegratedDisclosureFriendlyName(section));
            }
        }

    }
}
