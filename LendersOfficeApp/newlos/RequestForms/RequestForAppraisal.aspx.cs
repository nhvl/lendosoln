using System;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.RequestForms
{
	/// <summary>
	/// Summary description for RequestForAppraisal.
	/// </summary>
	public partial class RequestForAppraisal : BaseLoanPage
	{
        #region Protected member variables
        protected MeridianLink.CommonControls.StateDropDownList sSpStateDDL;


	

		#endregion


        protected string m_openedDate 
        {
            get { return (string) ViewState["OpenedDate"]; }
            set { ViewState["OpenedDate"] = value; }
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            Tools.Bind_aOccT(aOccT);
            Tools.Bind_sLT(sLT);
            Tools.Bind_sLienPosT(sLienPosT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sApprPmtMethod(sApprPmtMethodT);
            sSpZipcode.SmartZipcode(sSpCity, sSpState, sSpCounty);
            appraiserZipcode.SmartZipcode(appraiserCity, appraiserState);
            RequestOfAppraisalZip.SmartZipcode(RequestOfAppraisalCity, RequestOfAppraisalState);
            this.PageTitle = "Request for Appraisal";
            this.PageID = "RequestAppraisal";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CRequestForAppraisalPDF);
			// 01-14-08 av 18913
			sSpState.Attributes.Add( "onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event);" );
            RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency( RequestHelper.LoanID, typeof(RequestForAppraisal) );
            dataLoan.InitLoad();
            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.RequestOfAppraisal, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            RequestOfAppraisalPreparerName.Text = f.PreparerName;
            RequestOfAppraisalTitle.Text = f.Title;
            RequestOfAppraisalPrepareDate.Text = f.PrepareDate_rep;
			RequestOfAppraisalPrepareDate.ToolTip = "Hint:  Enter 't' for today's date.  Enter 'o' for opened date.";

            RequestOfAppraisalCompanyName.Text = f.CompanyName;
            RequestOfAppraisalStreetAddr.Text = f.StreetAddr;
            RequestOfAppraisalCity.Text = f.City;
            RequestOfAppraisalState.Value = f.State;
            RequestOfAppraisalZip.Text = f.Zip;
            RequestOfAppraisalPhone.Text = f.Phone;
            RequestOfAppraisalFaxNum.Text = f.FaxNum;
            RequestOfAppraisalEmailAddr.Text = f.EmailAddr;
            sIsDisplayLAmtApprValInRequestAppr.Checked = dataLoan.sIsDisplayLAmtApprValInRequestAppr;

            m_openedDate = dataLoan.sOpenedD_rep;

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            
			
			//OPM 3734
			sBorrowerNames.Text = dataApp.aBNm_aCNm;
			sBorrowersAddress.Text = dataApp.aBAddr;
			sBorrowersCity.Text = dataApp.aBCity;
			sBorrowersState.Value = dataApp.aBState;
			sBorrowersZipCode.Text = dataApp.aBZip;

			sBorrowerHomeNumber.Text = dataApp.aBHPhone;
			sBorrowerCellNumbe.Text = dataApp.aBCellPhone;
			sBorrowerEmailAddr.Text = dataApp.aBEmail;
			sBorrowerShowContactOnPrintOut.Checked = dataLoan.sIsPrintBorrPhoneOnRequestForAppraisal;

            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZipcode.Text = dataLoan.sSpZip;
	


			//01-10-08 av opm 18913 Populate the new dropdownlist with counties in the state and select the one that matches it. 
			Tools.Bind_sSpCounty( dataLoan.sSpState, sSpCounty, true) ; 
			Tools.SetDropDownListCaseInsensitive( sSpCounty, dataLoan.sSpCounty );
			
            sLAmt.Text = dataLoan.sFinalLAmt_rep;
            sApprVal.Text = dataLoan.sApprVal_rep;
            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sSpLegalDesc.Text = dataLoan.sSpLegalDesc;
            sUnitsNum.Text = dataLoan.sUnitsNum_rep;
            
            sApprFull.Checked = dataLoan.sApprFull;
            sApprDriveBy.Checked = dataLoan.sApprDriveBy;
            sApprMarketRentAnalysis.Checked = dataLoan.sApprMarketRentAnalysis;

            if (dataLoan.sGseRequestAppraisalT == E_GseRequestAppraisal.Unset)
            {
                    dataLoan.sGseRequestAppraisalT = E_GseRequestAppraisal.Other;
                    dataLoan.sGseRequestAppraisalOther = string.Empty;
            }
            string IdToSelect = dataLoan.sGseRequestAppraisalT.ToString();
            switch (IdToSelect)
            {
                case "Other":
                    Other.Checked = true;
                    sGseRequestAppraisalOther.Text = dataLoan.sGseRequestAppraisalOther;
                    break;

                case "Fannie216": Fannie216.Checked = true; break;
                case "Fannie1004": Fannie1004.Checked = true; break;
                case "Fannie1004C": Fannie1004C.Checked = true; break;
                case "Fannie1004D": Fannie1004D.Checked = true; break;
                case "Fannie1007": Fannie1007.Checked = true; break;
                case "Fannie1025": Fannie1025.Checked = true; break;
                case "Fannie1073": Fannie1073.Checked = true; break;
                case "Fannie1075": Fannie1075.Checked = true; break;
                case "Fannie2000": Fannie2000.Checked = true; break;
                case "Fannie2000A": Fannie2000A.Checked = true; break;
                case "Fannie2055": Fannie2055.Checked = true; break;
                case "Fannie2065": Fannie2065.Checked = true; break;
                case "Fannie2075": Fannie2075.Checked = true; break;
                case "Fannie2090": Fannie2090.Checked = true; break;
                case "Fannie2095": Fannie2095.Checked = true; break;

                case "Freddie70": Freddie70.Checked = true; break;
                case "Freddie70B": Freddie70B.Checked = true; break;
                case "Freddie72": Freddie72.Checked = true; break;
                case "Freddie442": Freddie442.Checked = true; break;
                case "Freddie465": Freddie465.Checked = true; break;
                case "Freddie466": Freddie466.Checked = true; break;
                case "Freddie998": Freddie998.Checked = true; break;
                case "Freddie1032": Freddie1032.Checked = true; break;
                case "Freddie1072": Freddie1072.Checked = true; break;
                case "Freddie2055": Freddie2055.Checked = true; break;
                case "Freddie2070": Freddie2070.Checked = true; break;

                case "VA26_1805": VA26_1805.Checked = true; break;
                case "VA26_8712": VA26_8712.Checked = true; break;

                default:
                    throw new UnhandledEnumException(dataLoan.sGseRequestAppraisalT);
            }

            sApprContactForEntry.Text = dataLoan.sApprContactForEntry;
            sApprInfo.Text = dataLoan.sApprInfo;
            sApprRprtDueD.Text = dataLoan.sApprRprtDueD_rep;
            sApprRprtDueD.ToolTip = "Hint:  Enter 't' for today's date.";
            sLenderCaseNum.Text = dataLoan.sLenderCaseNum;
            sLenderCaseNumLckd.Checked = dataLoan.sLenderCaseNumLckd;

            if (dataLoan.sApprPmtMethodT == E_sApprPmtMethodT.Bill || dataLoan.sApprPmtMethodT == E_sApprPmtMethodT.Other)
            {
                sApprPmtMethodDesc.Text = dataLoan.sApprPmtMethodDesc;
            }

            
            Tools.SetDropDownListValue(sSpT, dataLoan.sSpT);
            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            Tools.SetDropDownListValue(sLienPosT, dataLoan.sLienPosT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sApprPmtMethodT, dataLoan.sApprPmtMethodT);

            if (Tools.ShouldBeReadonly_sLT(dataLoan.sLPurposeT))
            {
                Tools.ReadonlifyDropDown(sLT);
            }

            CAgentFields agent = dataLoan.GetAppraiserForAppraisalRequest(); // Want to load the most recently used appraiser.
            appraiserName.Text    = agent.AgentName;
            appraiserCompany.Text = agent.CompanyName;
            appraiserAddress.Text = agent.StreetAddr;
            appraiserCity.Text    = agent.City;
            appraiserState.Value  = agent.State;
            appraiserZipcode.Text = agent.Zip;
            appraiserPhone.Text   = agent.Phone;
            appraiserFax.Text     = agent.FaxNum;
            appraiserCell.Text    = agent.CellPhone;
            appraiserEmail.Text   = agent.EmailAddr;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Escrow, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            escrowNumber.Text  = agent.CaseNum;
            escrowCompany.Text = agent.CompanyName;
            escrowName.Text    = agent.AgentName;
            escrowFax.Text     = agent.FaxNum;
            escrowPhone.Text   = agent.Phone;
            escrowCell.Text    = agent.CellPhone;
            escrowEmail.Text   = agent.EmailAddr;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            titleNumber.Text  = agent.CaseNum;
            titleCompany.Text = agent.CompanyName;
            titleName.Text    = agent.AgentName;
            titleFax.Text     = agent.FaxNum;
            titlePhone.Text   = agent.Phone;
            titleCell.Text    = agent.CellPhone;
            titleEmail.Text   = agent.EmailAddr;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ListingAgent, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            listingNumber.Text  = agent.CaseNum;
            listingCompany.Text = agent.CompanyName;
            listingName.Text    = agent.AgentName;
            listingFax.Text     = agent.FaxNum;
            listingPhone.Text   = agent.Phone;
            listingCell.Text    = agent.CellPhone;
            listingEmail.Text   = agent.EmailAddr;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.SellingAgent, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            sellingNumber.Text  = agent.CaseNum;
            sellingCompany.Text = agent.CompanyName;
            sellingName.Text    = agent.AgentName;
            sellingFax.Text     = agent.FaxNum;
            sellingPhone.Text   = agent.Phone;
            sellingCell.Text    = agent.CellPhone;
            sellingEmail.Text   = agent.EmailAddr;

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}