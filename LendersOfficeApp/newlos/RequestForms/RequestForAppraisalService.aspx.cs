using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.RequestForms
{
    public class RequestForAppraisalServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(RequestForAppraisalServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            IPreparerFields field = dataLoan.GetPreparerOfForm(E_PreparerFormT.RequestOfAppraisal, E_ReturnOptionIfNotExist.CreateNew);
            field.PreparerName    = GetString("RequestOfAppraisalPreparerName");
            field.Title           = GetString("RequestOfAppraisalTitle");
            field.PrepareDate_rep = GetString("RequestOfAppraisalPrepareDate");

            field.CompanyName     = GetString("RequestOfAppraisalCompanyName");
            field.StreetAddr      = GetString("RequestOfAppraisalStreetAddr");
            field.City            = GetString("RequestOfAppraisalCity");
            field.State           = GetString("RequestOfAppraisalState");
            field.Zip             = GetString("RequestOfAppraisalZip");
            field.Phone           = GetString("RequestOfAppraisalPhone");
            field.FaxNum          = GetString("RequestOfAppraisalFaxNum");
            field.EmailAddr       = GetString("RequestOfAppraisalEmailAddr");
            field.Update();
            dataLoan.sApprRprtDueD_rep = GetString("sApprRprtDueD");

            dataLoan.sSpAddr                 = GetString("sSpAddr");
            dataLoan.sSpCity                 = GetString("sSpCity");
            dataLoan.sSpState                = GetString("sSpState");
            dataLoan.sSpZip                  = GetString("sSpZipcode");
            dataLoan.sSpCounty               = GetString("sSpCounty");
            dataLoan.sApprVal_rep            = GetString("sApprVal");
            dataLoan.sPurchPrice_rep         = GetString("sPurchPrice");
            dataLoan.sSpLegalDesc            = GetString("sSpLegalDesc");
            dataLoan.sUnitsNum_rep           = GetString("sUnitsNum");
            dataLoan.sSpT                    = (E_sSpT) GetInt("sSpT");
            dataApp.aOccT                    = (E_aOccT) GetInt("aOccT");
            dataLoan.sLT                     = (E_sLT) GetInt("sLT");
            dataLoan.sLienPosT               = (E_sLienPosT) GetInt("sLienPosT");
            dataLoan.sLPurposeT              = (E_sLPurposeT) GetInt("sLPurposeT");
            dataLoan.sApprPmtMethodT         = (E_sApprPmtMethodT)GetInt("sApprPmtMethodT");
            dataLoan.sIsDisplayLAmtApprValInRequestAppr = GetBool("sIsDisplayLAmtApprValInRequestAppr");

            dataLoan.sApprFull               = GetBool("sApprFull");
            dataLoan.sApprDriveBy            = GetBool("sApprDriveBy");
            dataLoan.sApprMarketRentAnalysis = GetBool("sApprMarketRentAnalysis");

            dataLoan.sGseRequestAppraisalT = GetsGseRequestAppraisal(GetString("sGseRequestAppraisal"));
            if (dataLoan.sGseRequestAppraisalT == E_GseRequestAppraisal.Other)
                dataLoan.sGseRequestAppraisalOther = GetString("sGseRequestAppraisalOther");
            else
                dataLoan.sGseRequestAppraisalOther = string.Empty;

            dataLoan.sApprInfo               = GetString("sApprInfo");
            dataLoan.sApprContactForEntry    = GetString("sApprContactForEntry");
            dataLoan.sLenderCaseNum          = GetString("sLenderCaseNum");
            dataLoan.sLenderCaseNumLckd      = GetBool("sLenderCaseNumLckd");

            dataLoan.sApprPmtMethodDesc      = GetString("sApprPmtMethodDesc");

			dataLoan.sIsPrintBorrPhoneOnRequestForAppraisal   = GetBool("sBorrowerShowContactOnPrintOut"); //opm 3734 fs 09/25/08

            CAgentFields agent = dataLoan.GetAgentFields(-1);
            agent.AgentName   = GetString("appraiserName");
            agent.CompanyName = GetString("appraiserCompany");
            agent.StreetAddr  = GetString("appraiserAddress");
            agent.City        = GetString("appraiserCity");
            agent.State       = GetString("appraiserState");
            agent.Zip         = GetString("appraiserZipcode");
            agent.Phone       = GetString("appraiserPhone");
            agent.FaxNum      = GetString("appraiserFax");
            agent.CellPhone   = GetString("appraiserCell");
            agent.EmailAddr   = GetString("appraiserEmail");
            // Want to leave the original appraiser on file in tact and create 
            // a 'Second Appraiser' if needed. OPM 114685
            dataLoan.UpdateAppraiserForAppraisalRequest(agent);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Escrow, E_ReturnOptionIfNotExist.CreateNew);
            agent.CaseNum     = GetString("escrowNumber");
            agent.CompanyName = GetString("escrowCompany");
            agent.AgentName   = GetString("escrowName");
            agent.FaxNum      = GetString("escrowFax");
            agent.Phone       = GetString("escrowPhone");
            agent.CellPhone   = GetString("escrowCell");
            agent.EmailAddr   = GetString("escrowEmail");
            agent.Update();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.CreateNew);
            agent.CaseNum     = GetString("titleNumber");
            agent.CompanyName = GetString("titleCompany");
            agent.AgentName   = GetString("titleName");
            agent.FaxNum      = GetString("titleFax");
            agent.Phone       = GetString("titlePhone");
            agent.CellPhone   = GetString("titleCell");
            agent.EmailAddr   = GetString("titleEmail");
            agent.Update();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ListingAgent, E_ReturnOptionIfNotExist.CreateNew);
            agent.CaseNum     = GetString("listingNumber");
            agent.CompanyName = GetString("listingCompany");
            agent.AgentName   = GetString("listingName");
            agent.FaxNum      = GetString("listingFax");
            agent.Phone       = GetString("listingPhone");
            agent.CellPhone   = GetString("listingCell");
            agent.EmailAddr   = GetString("listingEmail");
            agent.Update();

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.SellingAgent, E_ReturnOptionIfNotExist.CreateNew);
            agent.CaseNum     = GetString("sellingNumber");
            agent.CompanyName = GetString("sellingCompany");
            agent.AgentName   = GetString("sellingName");
            agent.FaxNum      = GetString("sellingFax");
            agent.Phone       = GetString("sellingPhone");
            agent.CellPhone   = GetString("sellingCell");
            agent.EmailAddr   = GetString("sellingEmail");
            agent.Update();

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sLenderCaseNum", dataLoan.sLenderCaseNum);
        }
        private E_GseRequestAppraisal GetsGseRequestAppraisal(string sGseRequestAppraisalValue)
        {
            switch (sGseRequestAppraisalValue)
            {
                case "Other": return E_GseRequestAppraisal.Other;
                case "Fannie1004": return E_GseRequestAppraisal.Fannie1004;
                case "Fannie1004C": return E_GseRequestAppraisal.Fannie1004C;
                case "Fannie1004D": return E_GseRequestAppraisal.Fannie1004D;
                case "Fannie1007": return E_GseRequestAppraisal.Fannie1007;
                case "Fannie1025": return E_GseRequestAppraisal.Fannie1025;
                case "Fannie1073": return E_GseRequestAppraisal.Fannie1073;
                case "Fannie1075": return E_GseRequestAppraisal.Fannie1075;
                case "Fannie2000": return E_GseRequestAppraisal.Fannie2000;
                case "Fannie2000A": return E_GseRequestAppraisal.Fannie2000A;
                case "Fannie2055": return E_GseRequestAppraisal.Fannie2055;
                case "Fannie216": return E_GseRequestAppraisal.Fannie216;
                case "Fannie2075": return E_GseRequestAppraisal.Fannie2075;
                case "Fannie2065": return E_GseRequestAppraisal.Fannie2065;
                case "Fannie2090": return E_GseRequestAppraisal.Fannie2090;
                case "Fannie2095": return E_GseRequestAppraisal.Fannie2095;
                case "Freddie70": return E_GseRequestAppraisal.Freddie70;
                case "Freddie70B": return E_GseRequestAppraisal.Freddie70B;
                case "Freddie442": return E_GseRequestAppraisal.Freddie442;
                case "Freddie72": return E_GseRequestAppraisal.Freddie72;
                case "Freddie465": return E_GseRequestAppraisal.Freddie465;
                case "Freddie466": return E_GseRequestAppraisal.Freddie466;
                case "Freddie1032": return E_GseRequestAppraisal.Freddie1032;
                case "Freddie1072": return E_GseRequestAppraisal.Freddie1072;
                case "Freddie2055": return E_GseRequestAppraisal.Freddie2055;
                case "Freddie998": return E_GseRequestAppraisal.Freddie998;
                case "Freddie2070": return E_GseRequestAppraisal.Freddie2070;
                case "VA26_1805": return E_GseRequestAppraisal.VA26_1805;
                case "VA26_8712": return E_GseRequestAppraisal.VA26_8712;
            }
            return E_GseRequestAppraisal.Unset;
        }
    }
	/// <summary>
	/// Summary description for RequestForAppraisalService.
	/// </summary>
	public partial class RequestForAppraisalService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new RequestForAppraisalServiceItem());
        }
	}
}
