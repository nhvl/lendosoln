using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.RequestForms
{
    public class SurveyRequestServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(SurveyRequestServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);

            dataApp.aBHPhone = GetString("aBHPhone");
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sSpLegalDesc = GetString("sSpLegalDesc");

            dataLoan.sAttachedContractBit = GetBool("sAttachedContractBit");
            dataLoan.sAttachedSurveyBit = GetBool("sAttachedSurveyBit");
            dataLoan.sAttachedPlansBit = GetBool("sAttachedPlansBit");
            dataLoan.sAttachedCostsBit = GetBool("sAttachedCostsBit");
            dataLoan.sAttachedSpecsBit = GetBool("sAttachedSpecsBit");
            dataLoan.sSurveyRequestN = GetString("sSurveyRequestN");

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.SurveyRequest_To, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("SurveyRequest_ToCompanyName");
            preparer.StreetAddr = GetString("SurveyRequest_ToStreetAddr");
            preparer.City = GetString("SurveyRequest_ToCity");
            preparer.State = GetString("SurveyRequest_ToState");
            preparer.Zip = GetString("SurveyRequest_ToZip");
            preparer.Update();

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.SurveyRequest_From, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("SurveyRequest_FromCompanyName");
            preparer.StreetAddr = GetString("SurveyRequest_FromStreetAddr");
            preparer.City = GetString("SurveyRequest_FromCity");
            preparer.State = GetString("SurveyRequest_FromState");
            preparer.Zip = GetString("SurveyRequest_FromZip");
            preparer.PhoneOfCompany = GetString("SurveyRequest_FromPhoneOfCompany");
            preparer.FaxOfCompany = GetString("SurveyRequest_FromFaxOfCompany");
            preparer.PrepareDate_rep = GetString("SurveyRequest_FromPrepareDate");
            preparer.Update();

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.CreateNew);
            agent.CompanyName = GetString("TitleCompanyName");
            agent.StreetAddr = GetString("TitleStreetAddr");
            agent.City = GetString("TitleCity");
            agent.State = GetString("TitleState");
            agent.Zip = GetString("TitleZip");
            agent.PhoneOfCompany = GetString("TitlePhoneOfCompany");
            agent.FaxOfCompany = GetString("TitleFaxOfCompany");
            agent.Update();
            
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Seller, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = GetString("SellerAgentName");
            agent.Phone = GetString("SellerPhone");
            agent.Update();

            dataLoan.sSpT = (E_sSpT) GetInt("sSpT");
            dataLoan.sLPurposeT = (E_sLPurposeT) GetInt("sLPurposeT");

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
        }

    }
	/// <summary>
	/// Summary description for SurveyRequestService.
	/// </summary>
	public partial class SurveyRequestService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new SurveyRequestServiceItem());
        }
	}
}
