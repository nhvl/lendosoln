using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.RequestForms
{
    public class RequestForInsuranceServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "Toggle_AgentInfo":
                    Toggle_AgentInfo();
                    break;
            }
        }

        private void Toggle_AgentInfo() 
        {
            CPageData dataLoan = ConstructPageDataClass(GetGuid("LoanID"));
            dataLoan.InitLoad();

            CAgentFields agent = null;
            dataLoan.sUseLenderAgentForInsRequest = GetBool("sUseLenderAgentForInsRequest");
            if (dataLoan.sUseLenderAgentForInsRequest) 
                agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            else
                agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Mortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            SetResult("sAgentLenderCompanyName", agent.CompanyName);
            SetResult("sAgentLenderAgentName", agent.AgentName);
            SetResult("sAgentLenderAddress", agent.StreetAddr);
            SetResult("sAgentLenderCity", agent.City);
            SetResult("sAgentLenderState", agent.State);
            SetResult("sAgentLenderZip", agent.Zip);
            SetResult("sAgentLenderAgentPhone", agent.Phone);
            SetResult("sAgentLenderAgentFax", agent.FaxNum);
            SetResult("sAgentLenderAgentCell", agent.CellPhone);
            SetResult("sAgentLenderAgentEmail", agent.EmailAddr);
        }

        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(RequestForInsuranceServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.RequestOfInsurance, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("RequestOfInsurancePreparerName");
            f.Title = GetString("RequestOfInsuranceTitle");
            f.PrepareDate_rep = GetString("RequestOfInsurancePrepareDate");
            f.CompanyName = GetString("RequestOfInsuranceCompanyName");
            f.StreetAddr = GetString("RequestOfInsuranceStreetAddr");
            f.City = GetString("RequestOfInsuranceCity");
            f.State = GetString("RequestOfInsuranceState");
            f.Zip = GetString("RequestOfInsuranceZip");
            f.Phone = GetString("RequestOfInsurancePhone");
            f.FaxNum = GetString("RequestOfInsuranceFaxNum");
            f.EmailAddr = GetString("RequestOfInsuranceEmailAddr");

            f.Update();
           
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sSpCounty = GetString("sSpCounty");
            dataLoan.sPurchPrice_rep = GetString("sPurchPrice");
            dataLoan.sSpLegalDesc = GetString("sSpLegalDesc");
            dataLoan.sInsReqReplacement = GetString("sInsReqReplacement"); // Why is it not decimal ???
            dataLoan.sSpT = (E_sSpT) GetInt("sSpT");
            dataLoan.sLPurposeT = (E_sLPurposeT) GetInt("sLPurposeT");
            dataLoan.sLienPosT = (E_sLienPosT) GetInt("sLienPosT");

            CAgentFields agent = null;
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.HazardInsurance, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = GetString("InsuranceName");
            agent.Phone = GetString("InsurancePhone");
            agent.FaxNum = GetString("InsuranceFax");
            agent.CellPhone = GetString("InsuranceCell");
            agent.EmailAddr = GetString("InsuranceEmail");
            agent.CompanyName = GetString("InsuranceCompany");
            agent.StreetAddr = GetString("InsuranceAddress");
            agent.City = GetString("InsuranceCity");
            agent.State = GetString("InsuranceState");
            agent.Zip = GetString("InsuranceZip");
            agent.Update();

            dataLoan.sUseLenderAgentForInsRequest = GetBool("sUseLenderAgentForInsRequest");
            if (dataLoan.sUseLenderAgentForInsRequest) 
                agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.CreateNew);
            else
                agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Mortgagee, E_ReturnOptionIfNotExist.CreateNew);

            agent.CompanyName = GetString("sAgentLenderCompanyName");
            agent.AgentName = GetString("sAgentLenderAgentName");
            agent.StreetAddr = GetString("sAgentLenderAddress");
            agent.City = GetString("sAgentLenderCity");
            agent.State = GetString("sAgentLenderState");
            agent.Zip = GetString("sAgentLenderZip");
            agent.Phone = GetString("sAgentLenderAgentPhone");
            agent.FaxNum = GetString("sAgentLenderAgentFax");
            agent.CellPhone = GetString("sAgentLenderAgentCell");
            agent.EmailAddr = GetString("sAgentLenderAgentEmail");
            agent.Update();

            dataLoan.sInsReqFlood = GetBool("sInsReqFlood");
            dataLoan.sInsReqWind = GetBool("sInsReqWind");
            dataLoan.sInsReqHazard = GetBool("sInsReqHazard");
            dataLoan.sInsReqEscrow = GetBool("sInsReqEscrow");
            dataLoan.sInsReqComments = GetString("sInsReqComments");
            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckd");
            dataLoan.sEstCloseD_rep = GetString("sEstCloseD");
            dataLoan.sLenderCaseNum = GetString("sLenderCaseNum");
            dataLoan.sLenderCaseNumLckd = GetBool("sLenderCaseNumLckd");

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sLenderCaseNum", dataLoan.sLenderCaseNum);
            SetResult("sEstCloseD", dataLoan.sEstCloseD_rep);
        }
    }

	public partial class RequestForInsuranceService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new RequestForInsuranceServiceItem());
        }
	}
}
