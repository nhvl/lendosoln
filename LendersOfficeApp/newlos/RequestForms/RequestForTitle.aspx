<%@ Page language="c#" Codebehind="RequestForTitle.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.RequestForms.RequestForTitle" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
		<title>Request For Title</title>
  </head>
	<body MS_POSITIONING="FlowLayout" class="RightBackground">
		<script language="javascript">
    <!--
    var oRolodex;
    var openedDate = <%= AspxTools.JsString(m_openedDate) %>;                
    
    function _init() {
      oRolodex = new cRolodex();
      <% if ( !IsReadOnly) { %>
      onChangesLPurposeT(); 
      <% } %>
      lockField(document.getElementById('sEstCloseDLckd'), 'sEstCloseD');
      lockField(<%= AspxTools.JsGetElementById(sLenderCaseNumLckd) %>, 'sLenderCaseNum');
    }
    
    function onDateKeyUp(o, event) {
      if (event.keyCode == 79) {
        o.value = openedDate;
      }
    }
    
    
      function onChangesLPurposeT()
      {
        var sLPurposeT = <%= AspxTools.JsGetElementById(sLPurposeT) %>;
		var sPurchPrice = <%= AspxTools.JsGetElementById(sPurchPrice) %>;
		
		if (sLPurposeT.value != <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Purchase.ToString("D")) %> 
		        && sLPurposeT.value != <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Construct.ToString("D")) %> 
		        && sLPurposeT.value != <%= AspxTools.JsString(DataAccess.E_sLPurposeT.ConstructPerm.ToString("D")) %> 		
		)
		{
		  sPurchPrice.readOnly = true;
		  sPurchPrice.value = "$0.00";
		}
		else
		{
		  sPurchPrice.readOnly = false;
		}
      }


//-->
		</script>
		<form id="RequestForTitle" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD noWrap class="MainRightHeader">Request for Title Commitment</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder" id="Table2" cellSpacing="0" cellPadding="0" width="98%" border="0">
							<TR>
								<TD noWrap colSpan="3" class="LoanFormHeader" align="middle">I. REQUEST</TD>
							</TR>
							<TR>
								<TD vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD noWrap colSpan="2" class="FieldLabel">Title Company</TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap></TD>
											<td nowrap>
												<uc:CFM name=CFM1 runat="server" Type="4" AgentNameField="TitleName" PhoneField="TitlePhone" FaxField="TitleFax" CellPhoneField="TitleCell" EmailField="TitleEmail" CompanyNameField="TitleCompany" StreetAddressField="TitleAddress" CityField="TitleCity" StateField="TitleState" ZipField="TitleZip"></uc:CFM>
											</td>
										</TR>
										<TR>
											<TD noWrap class="FieldLabel">Contact</TD>
											<TD noWrap><asp:TextBox id="TitleName" runat="server" Width="257px"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD noWrap class="FieldLabel">Phone</TD>
											<TD noWrap class="FieldLabel"><ml:PhoneTextBox id="TitlePhone" runat="server" width="120" preset="phone"></ml:PhoneTextBox>&nbsp;Fax
												<ml:PhoneTextBox id="TitleFax" runat="server" width="120" preset="phone"></ml:PhoneTextBox></TD>
										</TR>
										<TR>
											<TD noWrap class="FieldLabel">Cell</TD>
											<TD noWrap><ml:PhoneTextBox id="TitleCell" runat="server" width="120" preset="phone"></ml:PhoneTextBox></TD>
										</TR>
										<TR>
											<TD noWrap class="FieldLabel">Email</TD>
											<TD noWrap><asp:TextBox id="TitleEmail" runat="server"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD noWrap class="FieldLabel">Company</TD>
											<TD noWrap><asp:TextBox id="TitleCompany" runat="server" Width="257px"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD noWrap class="FieldLabel">Address</TD>
											<TD noWrap><asp:TextBox id="TitleAddress" runat="server" Width="259px"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD noWrap></TD>
											<TD noWrap><asp:TextBox id="TitleCity" runat="server"></asp:TextBox><ml:StateDropDownList id="TitleState" runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id="TitleZip" runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></TD>
										</TR>
									</TABLE>
								</TD>
								<TD noWrap width="10"></TD>
								<TD vAlign="top" noWrap width="100%">
									<TABLE id="Table4" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD noWrap class="FieldLabel">From</TD>
											<TD noWrap></TD>
										</TR>
										<tr>
											<td class=FieldLabel nowrap></td>
											<td nowrap>
												<uc:CFM name=CFM2 runat="server" Type="22" AgentNameField="RequestOfTitlePreparerName" AgentTitleField="RequestOfTitleTitle" PhoneField="RequestOfTitlePhone" FaxField="RequestOfTitleFaxNum" EmailField="RequestOfTitleEmailAddr" CompanyNameField="RequestOfTitleCompanyName" StreetAddressField="RequestOfTitleStreetAddr" CityField="RequestOfTitleCity" StateField="RequestOfTitleState" ZipField="RequestOfTitleZip"></uc:CFM>
											</td>	
										</tr>
										<TR>
											<TD noWrap class="FieldLabel">Attn</TD>
											<TD noWrap><asp:TextBox id="RequestOfTitlePreparerName" runat="server"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD noWrap class="FieldLabel">Title</TD>
											<TD noWrap><asp:TextBox id="RequestOfTitleTitle" runat="server"></asp:TextBox></TD>
										</TR>
              <tr>
                <td class=FieldLabel nowrap>Phone</td>
                <td class=FieldLabel nowrap><ml:PhoneTextBox id=RequestOfTitlePhone runat="server" preset="phone" width="120"></ml:PhoneTextBox>&nbsp;Fax 
<ml:PhoneTextBox id=RequestOfTitleFaxNum runat="server" preset="phone" width="120"></ml:PhoneTextBox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>Email</td>
                <td class=FieldLabel nowrap><asp:TextBox id=RequestOfTitleEmailAddr runat="server"></asp:TextBox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>Company</td>
                <td nowrap><asp:TextBox id=RequestOfTitleCompanyName runat="server" Width="257px"></asp:TextBox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>Address</td>
                <td nowrap><asp:TextBox id=RequestOfTitleStreetAddr runat="server" Width="257px"></asp:TextBox></td></tr>
              <tr>
                <td class=FieldLabel nowrap></td>
                <td nowrap><asp:TextBox id=RequestOfTitleCity runat="server"></asp:TextBox><ml:StateDropDownList id=RequestOfTitleState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=RequestOfTitleZip runat="server" preset="zipcode" width="50"></ml:ZipcodeTextBox></td></tr>
										<TR>
											<TD noWrap class="FieldLabel">Date</TD>
											<TD noWrap><ml:DateTextBox id="RequestOfTitlePrepareDate" runat="server" width="75" preset="date" onkeyup="onDateKeyUp(this, event);"></ml:DateTextBox></TD>
										</TR>
										<TR>
											<TD noWrap colSpan="2" class="FieldLabel">Hint: Type 't' for today's date.</TD>
										</TR>
										<TR>
											<TD noWrap colSpan="2" class="FieldLabel">Type 'o' for loan opened date.</TD>
										</TR>
										<TR>
											<TD noWrap class="FieldLabel">Borr DOB</TD>
											<TD noWrap><ml:DateTextBox id="aBDOB" runat="server" width="75" preset="date"></ml:DateTextBox></TD>
										</TR>
										<TR>
											<TD noWrap class="FieldLabel">Coborr DOB</TD>
											<TD noWrap><ml:DateTextBox id="aCDOB" runat="server" width="75" preset="date"></ml:DateTextBox></TD>
										</TR>
              <tr>
                <td class=FieldLabel nowrap>Lender's Number</td>
                <td class=FieldLabel nowrap><asp:TextBox id=sLenderCaseNum runat="server"></asp:TextBox><asp:CheckBox ID="sLenderCaseNumLckd" runat="server" onclick="refreshCalculation();"/>Lock</td></tr>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder" id="Table5" cellSpacing="0" cellPadding="0" width="98%" border="0">
							<TR>
								<TD noWrap class="LoanFormHeader" align="middle">II. PROPERTY AND MORTGAGE 
									INFORMATION</TD>
							</TR>
							<TR>
								<TD noWrap class="FieldLabel">Property Type
									<asp:DropDownList id="sSpT" runat="server"></asp:DropDownList>&nbsp;Loan 
									Purpose
									<asp:DropDownList id="sLPurposeT" onchange="onChangesLPurposeT();" runat="server"></asp:DropDownList>&nbsp;Occupancy 
									Status<asp:DropDownList id="aOccT" runat="server"></asp:DropDownList></TD>
							</TR>
							<TR>
								<TD noWrap>
									<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD class="FieldLabel" noWrap colSpan="2">Property Address</TD>
											<TD noWrap width="10"></TD>
											<TD noWrap></TD>
											<TD noWrap></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Street</TD>
											<TD noWrap><asp:textbox id="sSpAddr" runat="server" Width="251px" MaxLength="60"></asp:textbox></TD>
											<TD class="FieldLabel" noWrap></TD>
											<TD class="FieldLabel" noWrap>Sales Price</TD>
											<TD noWrap><ml:moneytextbox id="sPurchPrice" runat="server" width="90" preset="money"></ml:moneytextbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>City</TD>
											<TD noWrap><asp:textbox id="sSpCity" runat="server"></asp:textbox><ml:statedropdownlist id="sSpState" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="sSpZip" runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></TD>
											<TD class="FieldLabel" noWrap></TD>
											<TD class="FieldLabel" noWrap>Total Loan Amount</TD>
											<TD noWrap><ml:moneytextbox id="sFinalLAmt" runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>County</TD>
											<TD noWrap><asp:DropDownList id="sSpCounty" runat="server"></asp:DropDownList></TD>
											<TD class="FieldLabel" noWrap></TD>
											<TD class="FieldLabel" noWrap></TD>
											<TD noWrap></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap colSpan="5">Legal Description</TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap colSpan="5"><asp:textbox id="sSpLegalDesc" runat="server" Width="504px" TextMode="MultiLine" Height="70px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD noWrap colSpan="5" class="FieldLabel">Owner Name
												<asp:TextBox id="sTitleReqOwnerNm" runat="server"></asp:TextBox>&nbsp;Owner 
												Phone
												<ml:PhoneTextBox id="sTitleReqOwnerPhone" runat="server" width="120" preset="phone"></ml:PhoneTextBox></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD noWrap>
									<TABLE id="Table7" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD vAlign="top" noWrap>
												<TABLE id="Table8" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<TD noWrap class="FieldLabel">Seller</TD>
														<TD noWrap></TD>
													</TR>
													<TR>
														<TD class="FieldLabel" noWrap></TD>
														<td nowrap>
															<uc:CFM name=CFM3 runat="server" Type="17" AgentNameField="SellerName" StreetAddressField="SellerAddress" CityField="SellerCity" StateField="SellerState" ZipField="SellerZip"></uc:CFM>
														</td>
													</TR>
													<TR>
														<TD noWrap class="FieldLabel">Name</TD>
														<TD noWrap><asp:TextBox id="SellerName" runat="server"></asp:TextBox></TD>
													</TR>
													<TR>
														<TD noWrap class="FieldLabel">Address</TD>
														<TD noWrap><asp:TextBox id="SellerAddress" runat="server" Width="214px"></asp:TextBox></TD>
													</TR>
													<TR>
														<TD noWrap></TD>
														<TD noWrap><asp:TextBox id="SellerCity" runat="server" Width="114px"></asp:TextBox><ml:StateDropDownList id="SellerState" runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id="SellerZip" runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></TD>
													</TR>
												</TABLE>
											</TD>
											<TD noWrap width="10"></TD>
											<TD vAlign="top" noWrap width="100%">
												<TABLE id="Table9" cellSpacing="0" cellPadding="0" border="0">
													<TR>
														<TD noWrap class="FieldLabel">Mortgagee</TD>
														<TD noWrap></TD>
													</TR>
													<TR>
														<TD class="FieldLabel" noWrap></TD>
														<td>
															<uc:CFM name=CFM4 runat="server" Type="30" AgentNameField="MortgageeName" CompanyNameField="MortgageeCompany" StreetAddressField="MortgageeAddress" CityField="MortgageeCity" StateField="MortgageeState" ZipField="MortgageeZip"></uc:CFM>
														</td>
													</TR>
													<TR>
														<TD noWrap class="FieldLabel">Name</TD>
														<TD noWrap><asp:TextBox id="MortgageeName" runat="server"></asp:TextBox></TD>
													</TR>
													<TR>
														<TD noWrap class="FieldLabel">Company Name</TD>
														<TD noWrap><asp:TextBox id="MortgageeCompany" runat="server"></asp:TextBox></TD>
													</TR>
													<TR>
														<TD noWrap class="FieldLabel">Address</TD>
														<TD noWrap><asp:TextBox id="MortgageeAddress" runat="server" Width="207px"></asp:TextBox></TD>
													</TR>
													<TR>
														<TD noWrap></TD>
														<TD noWrap><asp:TextBox id="MortgageeCity" runat="server" Width="108px"></asp:TextBox><ml:StateDropDownList id="MortgageeState" runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id="MortgageeZip" runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></TD>
													</TR>
												</TABLE>
											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD noWrap></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE id="Table10" cellSpacing="0" cellPadding="0" width="98%" border="0" class="InsetBorder">
							<TR>
								<TD noWrap class="LoanFormHeader" align="middle">III. REQUEST FOR TITLE COMMITMENT</TD>
							</TR>
							<TR>
								<TD noWrap class="FieldLabel">Attachment</TD>
							</TR>
							<TR>
								<TD noWrap class="FieldLabel"><asp:CheckBox id="sTitleReqPriorPolicy" runat="server" Text="Prior Title Policy"></asp:CheckBox><asp:CheckBox id="sTitleReqWarrantyDeed" runat="server" Text="Warranty Deed"></asp:CheckBox><asp:CheckBox id="sTitleReqInsRequirements" runat="server" Text="Title Ins. Requirements"></asp:CheckBox><asp:CheckBox id="sTitleReqSurvey" runat="server" Text="Survey"></asp:CheckBox><asp:CheckBox id="sTitleReqContract" runat="server" Text="Contract"></asp:CheckBox></TD>
							</TR>
							<TR>
								<TD noWrap class="FieldLabel">Type of Policy
									<asp:TextBox id="sTitleReqPolicyTypeDesc" runat="server" Width="259px"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD noWrap class="FieldLabel">Est. Closing Date
									<ml:DateTextBox id="sEstCloseD" runat="server" preset="date" width="75" />&nbsp;
                                    <input type="checkbox" runat="server" id="sEstCloseDLckd" onchange="lockField(this, 'sEstCloseD'); refreshCalculation();"/>
                                    Lock
                                    <asp:CheckBox id="sTitleReqMailAway" runat="server" Text="Mail Away"></asp:CheckBox>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder" id="Table11" cellSpacing="0" cellPadding="0" width="98%" border="0">
							<TR>
								<TD class="LoanFormHeader" noWrap align="middle">IV. SPECIAL INSTRUCTION</TD>
							</TR>
							<TR>
								<TD noWrap><asp:TextBox id="sTitleReqInstruction" runat="server" Width="530px" Height="83px" TextMode="MultiLine"></asp:TextBox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
	</body>
</html>
