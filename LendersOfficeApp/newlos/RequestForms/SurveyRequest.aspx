<%@ Page language="c#" Codebehind="SurveyRequest.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.RequestForms.SurveyRequest" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>SurveyRequest</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="EditBackground">
	<script language=javascript>
<!--
var oRolodex;

function _init() {
  oRolodex = new cRolodex();
}

	// OPM 17652 - Ethan
	//
	// this javascript has now been centralized in a single control: ContactFieldMapper
	// location newlos/Status/ContactFieldMapper.ascx
	// and all instances of the "Pick from Contacts" and "Add to Contacts" links have been replaced with it
	
/*function pickAndPopulate(prefix) {
  var type;
  if (prefix == "Title") type = "4";
  else if (prefix == "SurveyRequest_To") type = "18";

  // Populate screen. Override any current data.

  var args = oRolodex.chooseFromRolodex(type);

  if (args.OK) {
    if (prefix == "SurveyRequest_To") {
        document.getElementById(prefix + "CompanyName").value = args.AgentCompanyName;
        document.getElementById(prefix + "StreetAddr").value = args.AgentStreetAddr;
        document.getElementById(prefix + "City").value = args.AgentCity;        
        document.getElementById(prefix + "State").value = args.AgentState;        
        document.getElementById(prefix + "Zip").value = args.AgentZip;        

      } else if (prefix == "SurveyRequest_From" || prefix == "Title") {
        document.getElementById(prefix + "CompanyName").value = args.AgentCompanyName;
        document.getElementById(prefix + "StreetAddr").value = args.AgentStreetAddr;
        document.getElementById(prefix + "City").value = args.AgentCity;        
        document.getElementById(prefix + "State").value = args.AgentState;        
        document.getElementById(prefix + "Zip").value = args.AgentZip;        
        document.getElementById(prefix + "PhoneOfCompany").value = args.PhoneOfCompany;
        document.getElementById(prefix + "FaxOfCompany").value = args.FaxOfCompany;
      
      }
      updateDirtyBit()

    }      
  }
    function addToRolodex(prefix)
    {

        var args = new Object();
        if (prefix == "SurveyRequest_To") {
          args.AgentType = "18";
          args.AgentCompanyName = document.getElementById(prefix + "CompanyName").value;
          args.AgentStreetAddr = document.getElementById(prefix + "StreetAddr").value;
          args.AgentCity = document.getElementById(prefix + "City").value;
          args.AgentState = document.getElementById(prefix + "State").value;
          args.AgentZip = document.getElementById(prefix + "Zip").value;
        
        } else if (prefix == "SurveyRequest_From" || prefix == "Title") {
          args.AgentType = prefix == "SurveyRequest_From" ? "" : "4";
          args.AgentCompanyName = document.getElementById(prefix + "CompanyName").value;
          args.AgentStreetAddr = document.getElementById(prefix + "StreetAddr").value;
          args.AgentCity = document.getElementById(prefix + "City").value;
          args.AgentState = document.getElementById(prefix + "State").value;
          args.AgentZip = document.getElementById(prefix + "Zip").value;
          args.PhoneOfCompany = document.getElementById(prefix + "PhoneOfCompany").value;
          args.FaxOfCompany = document.getElementById(prefix + "FaxOfCompany").value;
        }              
        
        if (args.AgentCompanyName != null && args.AgentCompanyName.length > 0)
        {
            args.DisplayName = args.AgentCompanyName;
        }
        else
        {
            args.DisplayName = "Entry";
        }
        oRolodex.addToRolodex(args);
    }*/
//-->
</script>

    <form id="SurveyRequest" method="post" runat="server">
			<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td nowrap class="MainRightHeader">Survey Request</td>
				</tr>
  <tr>
    <td nowrap style="PADDING-LEFT: 5px">
      <table id=Table2 cellspacing=0 cellpadding=0 width="100%" border=0>
        <tr>
          <td valign=top>
            <table id=Table3 cellspacing=0 cellpadding=0 width=300 border=0 class=InsetBorder>
              <tr>
                <td class=FieldLabel>To</td>
                <td>
					<uc:CFM name=CFM1 runat="server" Type="18" CompanyNameField="SurveyRequest_ToCompanyName" StreetAddressField="SurveyRequest_ToStreetAddr" CityField="SurveyRequest_ToCity" StateField="SurveyRequest_ToState" ZipField="SurveyRequest_ToZip"></uc:CFM>
                </td></tr>
              <tr>
                <td class=FieldLabel>Name</td>
                <td><asp:TextBox id=SurveyRequest_ToCompanyName runat="server" Width="213px"></asp:TextBox></td></tr>
              <tr>
                <td class=FieldLabel>Address</td>
                <td><asp:TextBox id=SurveyRequest_ToStreetAddr runat="server" Width="213px"></asp:TextBox></td></tr>
              <tr>
                <td></td>
                <td><asp:TextBox id=SurveyRequest_ToCity runat="server" Width="120px"></asp:TextBox><ml:StateDropDownList id=SurveyRequest_ToState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=SurveyRequest_ToZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td></tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td></tr></table></td>
          <td valign=top width="100%" style="PADDING-LEFT: 5px">
            <table class=InsetBorder id=Table4 cellspacing=0 cellpadding=0 
            width=300 border=0>
              <tr>
                <td class=FieldLabel>From</td>
                <td>
					<uc:CFM name=CFM2 runat="server" CompanyNameField="SurveyRequest_FromCompanyName" StreetAddressField="SurveyRequest_FromStreetAddr" CityField="SurveyRequest_FromCity" StateField="SurveyRequest_FromState" ZipField="SurveyRequest_FromZip" CompanyPhoneField="SurveyRequest_FromPhoneOfCompany" CompanyFaxField="SurveyRequest_FromFaxOfCompany"></uc:CFM>
                </td></tr>
              <tr>
                <td class=FieldLabel>Company</td>
                <td><asp:TextBox id=SurveyRequest_FromCompanyName runat="server" Width="213px"></asp:TextBox></td></tr>
              <tr>
                <td class=FieldLabel>Address</td>
                <td><asp:TextBox id=SurveyRequest_FromStreetAddr runat="server" Width="213px"></asp:TextBox></td></tr>
              <tr>
                <td></td>
                <td><asp:TextBox id=SurveyRequest_FromCity runat="server" Width="120px"></asp:TextBox><ml:StateDropDownList id=SurveyRequest_FromState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=SurveyRequest_FromZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
              <tr>
                <td class=FieldLabel>Phone</td>
                <td><ml:PhoneTextBox id=SurveyRequest_FromPhoneOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
              <tr>
                <td class=FieldLabel>Fax</td>
                <td><ml:PhoneTextBox id=SurveyRequest_FromFaxOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr></table></td></tr></table>
      <table id=Table5 cellspacing=0 cellpadding=0 border=0 class=InsetBorder width=610>
        <tr>
          <td nowrap class=FieldLabel>Property Address</td>
          <td nowrap><asp:TextBox id=sSpAddr runat="server" Width="271px"></asp:TextBox></td></tr>
        <tr>
          <td nowrap></td>
          <td nowrap><asp:TextBox id=sSpCity runat="server" Width="170px"></asp:TextBox><ml:StateDropDownList id=sSpState runat="server" IncludeTerritories="false"></ml:StateDropDownList><ml:ZipcodeTextBox id=sSpZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td valign=top nowrap class=FieldLabel>Legal Description</td>
          <td nowrap><asp:TextBox id=sSpLegalDesc runat="server" Width="386px" TextMode="MultiLine" Height="92px"></asp:TextBox></td></tr></table></td></tr>
  <tr>
    <td nowrap style="PADDING-LEFT: 5px">
      <table id=Table6 cellspacing=0 cellpadding=0 width=610 border=0 class=InsetBorder>
        <tr>
          <td class=FieldLabel>Title Company  </td>
          <td>
			<uc:CFM name=CFM3 runat="server" Type="4" CompanyNameField="TitleCompanyName" StreetAddressField="TitleStreetAddr" CityField="TitleCity" StateField="TitleState" ZipField="TitleZip" CompanyPhoneField="TitlePhoneOfCompany" CompanyFaxField="TitleFaxOfCompany"></uc:CFM>
		  </td></tr>
        <tr>
          <td class=FieldLabel>Company</td>
          <td><asp:TextBox id=TitleCompanyName runat="server" Width="271px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel>Address</td>
          <td><asp:TextBox id=TitleStreetAddr runat="server" Width="271px"></asp:TextBox></td></tr>
        <tr>
          <td></td>
          <td><asp:TextBox id=TitleCity runat="server" Width="170px"></asp:TextBox><ml:StateDropDownList id=TitleState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=TitleZip runat="server" width="50" preset="zipcode"></ml:ZipcodeTextBox></td></tr>
        <tr>
          <td class=FieldLabel>Phone</td>
          <td><ml:PhoneTextBox id=TitlePhoneOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel>Fax</td>
          <td><ml:PhoneTextBox id=TitleFaxOfCompany runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr></table></td></tr>
  <tr>
    <td nowrap style="PADDING-LEFT: 5px">
      <table id=Table7 cellspacing=0 cellpadding=0 width=610 border=0 class=InsetBorder>
        <tr>
          <td class=FieldLabel nowrap>Access</td>
          <td class=FieldLabel nowrap>Name</td>
          <td class=FieldLabel nowrap>Phone</td></tr>
        <tr>
          <td class=FieldLabel nowrap>Seller</td>
          <td nowrap><asp:TextBox id=SellerAgentName runat="server" Width="271px"></asp:TextBox></td>
          <td nowrap><ml:PhoneTextBox id=SellerPhone runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Borrower</td>
          <td nowrap><asp:TextBox id=aBNm runat="server" Width="271px" ReadOnly="True"></asp:TextBox></td>
          <td nowrap><ml:PhoneTextBox id=aBHPhone runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr></table></td></tr>
  <tr>
    <td nowrap style="PADDING-LEFT: 5px">
      <table id=Table8 cellspacing=0 cellpadding=0 border=0 class=InsetBorder width=610>
        <tr>
          <td class=FieldLabel nowrap>Property Type</td>
          <td nowrap><asp:DropDownList id=sSpT runat="server"></asp:DropDownList></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Loan Purpose</td>
          <td nowrap><asp:DropDownList id=sLPurposeT runat="server"></asp:DropDownList></td></tr>
        <tr>
          <td class=FieldLabel nowrap valign=top>Attachment</td>
          <td nowrap>
            <table id=Table9 cellspacing=0 cellpadding=0 width=300 border=0>
              <tr>
                <td nowrap><asp:CheckBox id=sAttachedContractBit runat="server" Text="Contract"></asp:CheckBox></td>
                <td nowrap><asp:CheckBox id=sAttachedSurveyBit runat="server" Text="Survey"></asp:CheckBox></td>
                <td nowrap><asp:CheckBox id=sAttachedPlansBit runat="server" Text="Plans"></asp:CheckBox></td></tr>
              <tr>
                <td nowrap><asp:CheckBox id=sAttachedCostsBit runat="server" Text="Costs"></asp:CheckBox></td>
                <td nowrap><asp:CheckBox id=sAttachedSpecsBit runat="server" Text="Specs"></asp:CheckBox></td>
                <td nowrap></td></tr></table></td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap>Comments</td>
          <td nowrap><asp:TextBox id=sSurveyRequestN runat="server" Width="362px" TextMode="MultiLine" Height="80px"></asp:TextBox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>Due Date</td>
          <td nowrap><ml:DateTextBox id=SurveyRequest_FromPrepareDate runat="server" width="75" preset="date"></ml:DateTextBox></td></tr></table></td></tr>
			</table>
     </form><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
	
  </body>
</HTML>
