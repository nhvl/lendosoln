using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.RequestForms
{
	public partial class RequestForInsurance : BaseLoanPage
	{
        protected string m_openedDate 
        {
            get { return (string) ViewState["OpenedDate"]; }
            set { ViewState["OpenedDate"] = value; }
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            Tools.Bind_sSpT(sSpT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sLienPosT(sLienPosT);
            sSpZip.SmartZipcode(sSpCity, sSpState, sSpCounty);
            InsuranceZip.SmartZipcode(InsuranceCity, InsuranceState);
            RequestOfInsuranceZip.SmartZipcode(RequestOfInsuranceCity, RequestOfInsuranceState);
            sAgentLenderZip.SmartZipcode(sAgentLenderCity, sAgentLenderState);

            this.PageTitle = "Request for Insurance";
            this.PageID = "RequestInsurance";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CRequestForInsurancePDF);
		    // 01-30-08 av 18913
			sSpState.Attributes.Add( "onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event);" );

            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(RequestForInsurance)); 
            dataLoan.InitLoad();
            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.RequestOfInsurance, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            RequestOfInsurancePreparerName.Text = f.PreparerName;
            RequestOfInsuranceTitle.Text = f.Title;
            RequestOfInsurancePrepareDate.Text = f.PrepareDate_rep;
			RequestOfInsurancePrepareDate.ToolTip = "Hint:  Enter 't' for today's date.  Enter 'o' for opened date.";
            RequestOfInsuranceCompanyName.Text = f.CompanyName;
            RequestOfInsuranceStreetAddr.Text = f.StreetAddr;
            RequestOfInsuranceCity.Text = f.City;
            RequestOfInsuranceState.Value = f.State;
            RequestOfInsuranceZip.Text = f.Zip;
            RequestOfInsurancePhone.Text = f.Phone;
            RequestOfInsuranceFaxNum.Text = f.FaxNum;
            RequestOfInsuranceEmailAddr.Text = f.EmailAddr;

            m_openedDate = dataLoan.sOpenedD_rep;
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;
         
			//01-10-08 av opm 18913 Populate the new dropdownlist with counties in the state and select the one that matches it. 
			Tools.Bind_sSpCounty( dataLoan.sSpState, sSpCounty, true) ; 
			Tools.SetDropDownListCaseInsensitive( sSpCounty, dataLoan.sSpCounty );

            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep; //opm 27443 fs 02/10/09
            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sSpLegalDesc.Text = dataLoan.sSpLegalDesc;
            sInsReqReplacement.Text = dataLoan.sInsReqReplacement; // Why is it not decimal ???
            sLenderCaseNum.Text = dataLoan.sLenderCaseNum;
            sLenderCaseNumLckd.Checked = dataLoan.sLenderCaseNumLckd;
            Tools.SetDropDownListValue(sSpT, dataLoan.sSpT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sLienPosT, dataLoan.sLienPosT);

            CAgentFields agent = null;
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.HazardInsurance, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.IsValid) 
            {
                InsuranceName.Text = agent.AgentName;
                InsurancePhone.Text = agent.Phone;
                InsuranceFax.Text = agent.FaxNum;
                InsuranceCell.Text = agent.CellPhone;
                InsuranceEmail.Text = agent.EmailAddr;
                InsuranceCompany.Text = agent.CompanyName;
                InsuranceAddress.Text = agent.StreetAddr;
                InsuranceCity.Text = agent.City;
                InsuranceState.Value = agent.State;
                InsuranceZip.Text = agent.Zip;
            }

            sUseLenderAgentForInsRequest.SelectedIndex = dataLoan.sUseLenderAgentForInsRequest ? 0 : 1;
            // 4/4/2005 dd - OPM #1501
            if (dataLoan.sUseLenderAgentForInsRequest) 
                agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            else
                agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Mortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (agent.IsValid) 
            {
                sAgentLenderCompanyName.Text = agent.CompanyName;
                sAgentLenderAgentName.Text = agent.AgentName;
                sAgentLenderAddress.Text = agent.StreetAddr;
                sAgentLenderCity.Text = agent.City;
                sAgentLenderState.Value = agent.State;
                sAgentLenderZip.Text = agent.Zip;
                sAgentLenderAgentPhone.Text = agent.Phone;
                sAgentLenderAgentFax.Text = agent.FaxNum;
                sAgentLenderAgentCell.Text = agent.CellPhone;
                sAgentLenderAgentEmail.Text = agent.EmailAddr;
            }

            sInsReqFlood.Checked = dataLoan.sInsReqFlood;
            sInsReqWind.Checked = dataLoan.sInsReqWind;
            sInsReqHazard.Checked = dataLoan.sInsReqHazard;
            sInsReqEscrow.Checked = dataLoan.sInsReqEscrow;
            sInsReqComments.Text = dataLoan.sInsReqComments;
            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;
            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
			sEstCloseD.ToolTip = "Hint:  Enter 't' for today's date.";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
