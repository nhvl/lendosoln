<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<%@ Page Language="c#" CodeBehind="RequestForAppraisal.aspx.cs" AutoEventWireup="false"
    Inherits="LendersOfficeApp.newlos.RequestForms.RequestForAppraisal" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Request For Appraisal</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href=<%=AspxTools.SafeUrl(StyleSheet)%> type="text/css" rel="stylesheet">
    <style type="text/css">
    table#GovSponsored td
    {
        text-align:left;
        vertical-align:top;
    }
    .GovSponsoredSpacer
    {
        width:40px;
    }
    </style>
</head>
<body class="RightBackground" ms_positioning="FlowLayout">

    <script language="javascript">
    <!--
      var oRolodex;
      var openedDate = <%=AspxTools.JsString(m_openedDate)%>;
      function _init() {
        oRolodex = new cRolodex();
        <% if ( !IsReadOnly) { %>
        onChangesLPurposeT(); 
        <% } %>
        onChangesApprPmtMethodT();
        lockField(<%= AspxTools.JsGetElementById(sLenderCaseNumLckd) %>, 'sLenderCaseNum');
      }
      
      function onDateKeyUp(o, event) {
        if (event.keyCode == 79) {
          o.value = openedDate;
        }
      }
      
      function onChangesLPurposeT()
      {
        var sLPurposeT = <%= AspxTools.JsGetElementById(sLPurposeT) %>;
		var sPurchPrice = <%= AspxTools.JsGetElementById(sPurchPrice) %>;
		
		if (sLPurposeT.value != <%=AspxTools.JsString(DataAccess.E_sLPurposeT.Purchase)%>
		    && sLPurposeT.value != <%=AspxTools.JsString(DataAccess.E_sLPurposeT.Construct)%> 
		    && sLPurposeT.value != <%=AspxTools.JsString(DataAccess.E_sLPurposeT.ConstructPerm)%>
		    )
		{
		  sPurchPrice.readOnly = true;
		  sPurchPrice.value = "$0.00";
		}
		else
		{
		  sPurchPrice.readOnly = false;
		}
      }
      
      function onChangesApprPmtMethodT()
      {
        // Check what they selected
        var sApprPmtMethodT = <%=AspxTools.JsGetElementById(sApprPmtMethodT)%>;

        var sApprPmtMethodDesc = <%=AspxTools.JsGetElementById(sApprPmtMethodDesc)%>;
        if (sApprPmtMethodT.value != <%=AspxTools.JsString(DataAccess.E_sApprPmtMethodT.Bill) %>
            && sApprPmtMethodT.value != <%=AspxTools.JsString(DataAccess.E_sApprPmtMethodT.Other) %>) {
            // If it's not "Bill" and not "Other", disable the description textbox;
            sApprPmtMethodDesc.value = "";
            sApprPmtMethodDesc.readOnly = true;
        } else {
            sApprPmtMethodDesc.readOnly = false;
        }
      }
      //-->
    </script>

    <form id="RequestForAppraisal" method="post" runat="server">
    <table class="FormTable" id="Table1" cellspacing="0" cellpadding="0" width="100%"
        border="0">
        <tr>
            <td class="MainRightHeader" nowrap>
                Request For Appraisal<uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>
            </td>
        </tr>
        <tr>
            <td class="LoanFormHeader" nowrap>
                I. Request
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="FieldLabel">
                            Appraiser &nbsp;
                            <UC:CFM Name="CFM1" runat="server" Type="1" AgentNameField="appraiserName" PhoneField="appraiserPhone"
                                FaxField="appraiserFax" CellPhoneField="appraiserCell" EmailField="appraiserEmail"
                                CompanyNameField="appraiserCompany" StreetAddressField="appraiserAddress" CityField="appraiserCity"
                                StateField="appraiserState" ZipField="appraiserZipcode" ID="CFM1"></UC:CFM>
                        </td>
                        <td class="FieldLabel">
                            From &nbsp;
                            <UC:CFM Name="CFM2" runat="server" Type="22" AgentNameField="RequestOfAppraisalPreparerName"
                                AgentTitleField="RequestOfAppraisalTitle" PhoneField="RequestOfAppraisalPhone"
                                FaxField="RequestOfAppraisalFaxNum" EmailField="RequestOfAppraisalEmailAddr"
                                CompanyNameField="RequestOfAppraisalCompanyName" StreetAddressField="RequestOfAppraisalStreetAddr"
                                CityField="RequestOfAppraisalCity" StateField="RequestOfAppraisalState" ZipField="RequestOfAppraisalZip"
                                ID="CFM2"></UC:CFM>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table id="Table3" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Name
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="appraiserName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Phone
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <ml:PhoneTextBox ID="appraiserPhone" runat="server" CssClass="mask" preset="phone"
                                            Width="120"></ml:PhoneTextBox>&nbsp; Fax
                                        <ml:PhoneTextBox ID="appraiserFax" runat="server" CssClass="mask" preset="phone"
                                            Width="120"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Cell
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <ml:PhoneTextBox ID="appraiserCell" runat="server" Width="120" preset="phone"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Email
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <asp:TextBox ID="appraiserEmail" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Company&nbsp;&nbsp; &nbsp;
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="appraiserCompany" runat="server" Width="256px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Address
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="appraiserAddress" runat="server" Width="256px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="appraiserCity" runat="server"></asp:TextBox>
                                        <ml:StateDropDownList ID="appraiserState" runat="server"></ml:StateDropDownList>
                                        <ml:ZipcodeTextBox ID="appraiserZipcode" runat="server" CssClass="mask" preset="zipcode"
                                            Width="50"></ml:ZipcodeTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="FieldLabel">
                                        Attn
                                    </td>
                                    <td>
                                        <asp:TextBox ID="RequestOfAppraisalPreparerName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                        Title
                                    </td>
                                    <td>
                                        <asp:TextBox ID="RequestOfAppraisalTitle" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                        Phone
                                    </td>
                                    <td class="FieldLabel">
                                        <ml:PhoneTextBox ID="RequestOfAppraisalPhone" runat="server" Width="120" preset="phone">
                                        </ml:PhoneTextBox>&nbsp;Fax<ml:PhoneTextBox ID="RequestOfAppraisalFaxNum" runat="server"
                                            Width="120" preset="phone"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                        Email
                                    </td>
                                    <td class="FieldLabel">
                                        <asp:TextBox ID="RequestOfAppraisalEmailAddr" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                        Company
                                    </td>
                                    <td>
                                        <asp:TextBox ID="RequestOfAppraisalCompanyName" runat="server" Width="253px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                        Address
                                    </td>
                                    <td>
                                        <asp:TextBox ID="RequestOfAppraisalStreetAddr" runat="server" Width="254px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                    </td>
                                    <td>
                                        <asp:TextBox ID="RequestOfAppraisalCity" runat="server"></asp:TextBox><ml:StateDropDownList
                                            ID="RequestOfAppraisalState" runat="server"></ml:StateDropDownList>
                                        <ml:ZipcodeTextBox ID="RequestOfAppraisalZip" runat="server" Width="50" preset="zipcode"
                                            CssClass="mask"></ml:ZipcodeTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                        Date
                                    </td>
                                    <td class="FieldLabel">
                                        <ml:DateTextBox ID="RequestOfAppraisalPrepareDate" onkeyup="onDateKeyUp(this, event);" runat="server"
                                            CssClass="mask" preset="date" Width="75"></ml:DateTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Lender's Number
                                    </td>
                                    <td class="FieldLabel">
                                        <asp:TextBox ID="sLenderCaseNum" runat="server"></asp:TextBox>
                                        <asp:CheckBox ID="sLenderCaseNumLckd" runat="server" onclick="refreshCalculation();"/>Lock
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table id="Table3" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <a href="javascript:linkMe('../BorrowerInfo.aspx');" title="Go to borrower's info">Names
                                    of Borrowers(s)</a>&nbsp;
                            </td>
                            <td>
                                <asp:TextBox ID="sBorrowerNames" runat="server" ReadOnly="True" Width="300"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="javascript:linkMe('../BorrowerInfo.aspx');" title="Go to borrower's info">Present
                                    Address</a>&nbsp;
                            </td>
                            <td>
                                <asp:TextBox ID="sBorrowersAddress" runat="server" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:TextBox ID="sBorrowersCity" runat="server" ReadOnly="True"></asp:TextBox>
                                <ml:StateDropDownList ID="sBorrowersState" runat="server" disabled></ml:StateDropDownList>
                                <ml:ZipcodeTextBox ID="sBorrowersZipCode" runat="server" Width="50" ReadOnly="true"
                                    preset="zipcode" CssClass="mask"></ml:ZipcodeTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:CheckBox ID="sBorrowerShowContactOnPrintOut" runat="server" Text="Show borrower's phone number(s) and email on print-out">
                                </asp:CheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <span><a href="javascript:linkMe('../BorrowerInfo.aspx');" title="Go to borrower's info">
                                    Home</a>&nbsp;</span>
                                <ml:PhoneTextBox runat="server" Width="120" preset="phone" ID="sBorrowerHomeNumber"
                                    ReadOnly="True"></ml:PhoneTextBox>
                                <span><a href="javascript:linkMe('../BorrowerInfo.aspx');" title="Go to borrower's info">
                                    Cell</a>&nbsp;</span>
                                <ml:PhoneTextBox ID="sBorrowerCellNumbe" runat="server" Width="120" preset="phone"
                                    ReadOnly="True"></ml:PhoneTextBox>
                                <span><a href="javascript:linkMe('../BorrowerInfo.aspx');" title="Go to borrower's info">
                                    Email</a>&nbsp;</span>
                                <asp:TextBox ID="sBorrowerEmailAddr" runat="server" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td class="LoanFormHeader" nowrap>
                II. Property and Mortgage Information
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table5" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Property Type
                        </td>
                        <td nowrap>
                            <asp:DropDownList ID="sSpT" runat="server" Width="128px" DESIGNTIMEDRAGDROP="1871">
                                <asp:ListItem Value="0" Selected="True">Leave Blank</asp:ListItem>
                                <asp:ListItem Value="1">Detached Housing</asp:ListItem>
                                <asp:ListItem Value="2">Attached Housing</asp:ListItem>
                                <asp:ListItem Value="3">Condominium</asp:ListItem>
                                <asp:ListItem Value="4">PUD</asp:ListItem>
                                <asp:ListItem Value="5">CO-OP</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Occupancy Status&nbsp;
                        </td>
                        <td nowrap>
                            <asp:DropDownList ID="aOccT" runat="server" Width="131px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Type of Loan
                        </td>
                        <td nowrap>
                            <asp:DropDownList ID="sLT" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Lien Position
                        </td>
                        <td nowrap>
                            <asp:DropDownList ID="sLienPosT" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Loan Purpose
                        </td>
                        <td nowrap>
                            <asp:DropDownList ID="sLPurposeT" onchange="onChangesLPurposeT()" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Number of Units
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sUnitsNum" runat="server" Width="40px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table6" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="FieldLabel" nowrap colspan="4">
                            Property Address
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Address
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sSpAddr" runat="server" Width="223px" MaxLength="60"></asp:TextBox>
                        </td>
                        <td class="FieldLabel" nowrap style="padding-left: 10px">
                            Sales Price
                        </td>
                        <td nowrap>
                            <ml:MoneyTextBox ID="sPurchPrice" runat="server" CssClass="mask" preset="money" Width="90"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sSpCity" runat="server"></asp:TextBox><ml:StateDropDownList ID="sSpState"
                                runat="server" IncludeTerritories="false"></ml:StateDropDownList>
                            <ml:ZipcodeTextBox ID="sSpZipcode" runat="server" CssClass="mask" preset="zipcode"
                                Width="50"></ml:ZipcodeTextBox>
                        </td>
                        <td class="FieldLabel" nowrap style="padding-left: 10px">
                            Estimated Value
                        </td>
                        <td nowrap>
                            <ml:MoneyTextBox ID="sApprVal" runat="server" CssClass="mask" preset="money" Width="90"></ml:MoneyTextBox>
                            <asp:CheckBox runat="server" Style="vertical-align: baseline" ID="sIsDisplayLAmtApprValInRequestAppr" />
                            <ml:EncodedLabel ID="Label1" runat="server" AssociatedControlID="sIsDisplayLAmtApprValInRequestAppr"
                                CssClass="FieldLabel">Display value on form</ml:EncodedLabel>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            County
                        </td>
                        <td nowrap>
                            <asp:DropDownList ID="sSpCounty" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="FieldLabel" nowrap style="padding-left: 10px">
                            Loan Amount
                        </td>
                        <td nowrap>
                            <ml:MoneyTextBox ID="sLAmt" runat="server" CssClass="mask" preset="money" Width="90"
                                ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" nowrap>
                Legal Description
            </td>
        </tr>
        <tr>
            <td nowrap>
                <asp:TextBox ID="sSpLegalDesc" runat="server" Width="474px" Height="78px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table9" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td valign="top" nowrap align="left">
                            <table class="InsetBorder" id="Table10" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="FieldLabel" nowrap colspan="2">
                                        Escrow Company&nbsp;&nbsp;
                                        <UC:CFM Name="CFM3" runat="server" Type="2" AgentNameField="escrowName" CompanyNameField="escrowCompany"
                                            PhoneField="escrowPhone" FaxField="escrowFax" CellPhoneField="escrowCell" EmailField="escrowEmail"
                                            ID="CFM3"></UC:CFM>
                                    </td>
                                    <td nowrap>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Number
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="escrowNumber" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Officer
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="escrowName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Company
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="escrowCompany" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Phone
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <ml:PhoneTextBox ID="escrowPhone" runat="server" CssClass="mask" preset="phone" Width="120"></ml:PhoneTextBox>&nbsp;Fax<ml:PhoneTextBox
                                            ID="escrowFax" runat="server" CssClass="mask" preset="phone" Width="120"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Cell
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <ml:PhoneTextBox ID="escrowCell" runat="server" Width="120" preset="phone" CssClass="mask"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Email
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <asp:TextBox ID="escrowEmail" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" nowrap align="left">
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td valign="top" nowrap align="left">
                            <table class="InsetBorder" id="Table11" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="FieldLabel" nowrap colspan="2">
                                        Title Company&nbsp;&nbsp;
                                        <UC:CFM Name="CFM4" runat="server" Type="4" AgentNameField="titleName" CompanyNameField="titleCompany"
                                            PhoneField="titlePhone" FaxField="titleFax" CellPhoneField="titleCell" EmailField="titleEmail"
                                            ID="CFM4"></UC:CFM>
                                    </td>
                                    <td nowrap>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Number
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="titleNumber" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Officer
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="titleName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Company
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="titleCompany" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Phone
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <ml:PhoneTextBox ID="titlePhone" runat="server" CssClass="mask" preset="phone" Width="120"></ml:PhoneTextBox>&nbsp;Fax
                                        <ml:PhoneTextBox ID="titleFax" runat="server" CssClass="mask" preset="phone" Width="120"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Cell
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <ml:PhoneTextBox ID="titleCell" runat="server" Width="120" preset="phone" CssClass="mask"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Email
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <asp:TextBox ID="titleEmail" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" nowrap align="left">
                        </td>
                        <td valign="top" nowrap align="left">
                        </td>
                        <td valign="top" nowrap align="left">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" nowrap align="left">
                            <table class="InsetBorder" id="Table12" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="FieldLabel" nowrap colspan="2">
                                        Listing Agent&nbsp;&nbsp;
                                        <UC:CFM Name="CFM5" runat="server" Type="6" AgentNameField="listingName" CompanyNameField="listingCompany"
                                            PhoneField="listingPhone" FaxField="listingFax" CellPhoneField="listingCell"
                                            EmailField="listingEmail" ID="CFM5"></UC:CFM>
                                    </td>
                                    <td nowrap>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Number
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="listingNumber" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Officer
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="listingName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Company
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="listingCompany" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Phone
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <ml:PhoneTextBox ID="listingPhone" runat="server" CssClass="mask" preset="phone"
                                            Width="120"></ml:PhoneTextBox>&nbsp;Fax
                                        <ml:PhoneTextBox ID="listingFax" runat="server" CssClass="mask" preset="phone" Width="120"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Cell
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <ml:PhoneTextBox ID="listingCell" runat="server" Width="120" preset="phone" CssClass="mask"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Email
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <asp:TextBox ID="listingEmail" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" nowrap align="left">
                        </td>
                        <td valign="top" nowrap align="left">
                            <table class="InsetBorder" id="Table13" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td class="FieldLabel" nowrap colspan="2">
                                        Selling Agent&nbsp;&nbsp;
                                        <UC:CFM Name="CFM6" runat="server" Type="7" AgentNameField="sellingName" CompanyNameField="sellingCompany"
                                            PhoneField="sellingPhone" FaxField="sellingFax" CellPhoneField="sellingCell"
                                            EmailField="sellingEmail" ID="CFM6"></UC:CFM>
                                    </td>
                                    <td nowrap>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Number
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="sellingNumber" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Officer
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="sellingName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Company
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox ID="sellingCompany" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Phone
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <ml:PhoneTextBox ID="sellingPhone" runat="server" CssClass="mask" preset="phone"
                                            Width="120"></ml:PhoneTextBox>&nbsp;Fax
                                        <ml:PhoneTextBox ID="sellingFax" runat="server" CssClass="mask" preset="phone" Width="120"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Cell
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <ml:PhoneTextBox ID="sellingCell" runat="server" Width="120" preset="phone" CssClass="mask"></ml:PhoneTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel" nowrap>
                                        Email
                                    </td>
                                    <td class="FieldLabel" nowrap>
                                        <asp:TextBox ID="sellingEmail" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="LoanFormHeader" nowrap>
                III Appraisal Information
            </td>
        </tr>
        <tr>
            <td nowrap>
                <div id="Table8">
                    <div class="FieldLabel">
                        Due Date&nbsp;
                        <ml:DateTextBox ID="sApprRprtDueD" runat="server" CssClass="mask" preset="date" Width="75"></ml:DateTextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <span title="(if not same as borrower)">
                            Contact for entry:
                            <asp:TextBox ID="sApprContactForEntry" runat="server" MaxLength="30"></asp:TextBox>
                        </span>
                     </div>
                    <div class="InsetBorder">
                        <span class="FieldLabel">Type of Appraisal:</span>
                        <asp:CheckBox ID="sApprFull" runat="server" Text="Full"></asp:CheckBox>
                        <asp:CheckBox ID="sApprDriveBy" runat="server" Text="Drive By"></asp:CheckBox>
                        <asp:CheckBox ID="sApprMarketRentAnalysis" runat="server" Text="Market Rent Analysis"></asp:CheckBox>
                    </div>
                    <div class="InsetBorder">
                        <table id="GovSponsored"><tbody>
                            <tr>
                                <td colspan="5"><span class="FieldLabel" nowrap>Fannie Mae</span></td>
                                <td class="GovSponsoredSpacer"></td>
                                <td colspan="4"><span class="FieldLabel" nowrap>Freddie Mac</span></td>
                                <td class="GovSponsoredSpacer"></td>
                                <td colspan="1"><span class="FieldLabel" nowrap>VA</span></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="Fannie216" Text="216" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Fannie1004" Text="1004" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Fannie1004C" Text="1004C" GroupName="sGseRequestAppraisal" runat="server" />
                                </td>
                                <td>
                                    <asp:RadioButton ID="Fannie1004D" Text="1004D" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Fannie1007" Text="1007" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Fannie1025" Text="1025" GroupName="sGseRequestAppraisal" runat="server" />
                                </td>
                                <td>
                                    <asp:RadioButton ID="Fannie1073" Text="1073" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Fannie1075" Text="1075" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Fannie2000" Text="2000" GroupName="sGseRequestAppraisal" runat="server" />
                                </td>
                                <td>
                                    <asp:RadioButton ID="Fannie2000A" Text="2000A" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Fannie2055" Text="2055" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Fannie2065" Text="2065" GroupName="sGseRequestAppraisal" runat="server" />
                                </td>
                                <td>
                                    <asp:RadioButton ID="Fannie2075" Text="2075" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Fannie2090" Text="2090" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Fannie2095" Text="2095" GroupName="sGseRequestAppraisal" runat="server" />
                                </td>
                                <td class="GovSponsoredSpacer"></td>
                                <td>
                                    <asp:RadioButton ID="Freddie70" Text="70" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Freddie70B" Text="70B" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Freddie72" Text="72" GroupName="sGseRequestAppraisal" runat="server" />
                                </td>
                                <td>
                                    <asp:RadioButton ID="Freddie442" Text="442" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Freddie465" Text="465" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Freddie466" Text="466" GroupName="sGseRequestAppraisal" runat="server" />
                                </td>
                                <td>
                                    <asp:RadioButton ID="Freddie998" Text="998" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Freddie1032" Text="1032" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Freddie1072" Text="1072" GroupName="sGseRequestAppraisal" runat="server" />
                                </td>
                                <td>
                                    <asp:RadioButton ID="Freddie2055" Text="2055" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="Freddie2070" Text="2070" GroupName="sGseRequestAppraisal" runat="server" />
                                </td>
                                <td class="GovSponsoredSpacer"></td>
                                <td>
                                    <asp:RadioButton ID="VA26_1805" Text="26-1805" GroupName="sGseRequestAppraisal" runat="server" /><br />
                                    <asp:RadioButton ID="VA26_8712" Text="26-8712" GroupName="sGseRequestAppraisal" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <asp:RadioButton ID="Other" GroupName="sGseRequestAppraisal" runat="server" />
                                    <label for="Other">Other <asp:TextBox ID="sGseRequestAppraisalOther" runat="server"></asp:TextBox></label>
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                    <div class="InsetBorder">
                        <span class="FieldLabel">Payment Method</span>
                        <asp:DropDownList ID="sApprPmtMethodT" onchange="onChangesApprPmtMethodT()" runat="server"></asp:DropDownList>
                        <asp:TextBox ID="sApprPmtMethodDesc" onkeyup="TextAreaMaxLength(this, 200);" runat="server"></asp:TextBox>
                    </div>
                    <div class="FieldLabel">
                        Comments
                        <br />
                        <asp:TextBox ID="sApprInfo" onkeyup="TextAreaMaxLength(this, 400);" runat="server"
                            Width="338px" Height="104px" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td nowrap>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
