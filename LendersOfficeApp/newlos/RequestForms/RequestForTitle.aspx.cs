using System;
using DataAccess;

namespace LendersOfficeApp.newlos.RequestForms
{
	public partial class RequestForTitle : BaseLoanPage
	{
        protected string m_openedDate 
        {
            get { return (string) ViewState["OpenedDate"]; }
            set { ViewState["OpenedDate"] = value; }
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            Tools.Bind_aOccT(aOccT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sSpT(sSpT);

            sSpZip.SmartZipcode(sSpCity, sSpState, sSpCounty);
            SellerZip.SmartZipcode(SellerCity, SellerState);
            MortgageeZip.SmartZipcode(MortgageeCity, MortgageeState);
            TitleZip.SmartZipcode(TitleCity, TitleState);
            RequestOfTitleZip.SmartZipcode(RequestOfTitleCity, RequestOfTitleState);

            this.PageTitle = "Request for Title Commitment";
            this.PageID = "RequestTitle";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CRequestForTitlePDF);

		    // 01-30-08 av 18913
			sSpState.Attributes.Add( "onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event);" );

            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(RequestForTitle));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            m_openedDate = dataLoan.sOpenedD_rep;
            CAgentFields agent = null;

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.IsValid) 
            {
                TitleName.Text = agent.AgentName;
                TitleCompany.Text = agent.CompanyName;
                TitlePhone.Text = agent.Phone;
                TitleFax.Text = agent.FaxNum;
                TitleCell.Text = agent.CellPhone;
                TitleAddress.Text = agent.StreetAddr;
                TitleCity.Text = agent.City;
                TitleState.Value = agent.State;
                TitleZip.Text = agent.Zip;
                TitleEmail.Text = agent.EmailAddr;
            }

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Seller, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.IsValid) 
            {
                SellerName.Text = agent.AgentName;
                SellerAddress.Text = agent.StreetAddr;
                SellerCity.Text = agent.City;
                SellerState.Value = agent.State;
                SellerZip.Text = agent.Zip;
            }

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Mortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.IsValid) 
            {
                MortgageeName.Text = agent.AgentName ;
                MortgageeCompany.Text = agent.CompanyName ;
                MortgageeAddress.Text = agent.StreetAddr ;
                MortgageeCity.Text = agent.City ;
                MortgageeState.Value = agent.State ;
                MortgageeZip.Text = agent.Zip ;
            }

            Tools.SetDropDownListValue(sSpT, dataLoan.sSpT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.RequestOfTitle, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            RequestOfTitlePreparerName.Text = preparer.PreparerName;
            RequestOfTitleTitle.Text = preparer.Title;			
			RequestOfTitlePrepareDate.Text = preparer.PrepareDate_rep;
            RequestOfTitleCompanyName.Text = preparer.CompanyName;
            RequestOfTitleStreetAddr.Text = preparer.StreetAddr;
            RequestOfTitleCity.Text = preparer.City;
            RequestOfTitleState.Value = preparer.State;
            RequestOfTitleZip.Text = preparer.Zip;
            RequestOfTitlePhone.Text = preparer.Phone;
            RequestOfTitleFaxNum.Text = preparer.FaxNum;
            RequestOfTitleEmailAddr.Text = preparer.EmailAddr;

            sTitleReqOwnerNm.Text = dataLoan.sTitleReqOwnerNm;
            sTitleReqOwnerPhone.Text = dataLoan.sTitleReqOwnerPhone;

            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep; // 5/5/2009 dd - OPM 30205 Switch to final loan amount.
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;
			// 01-14-08 av 18913 
			Tools.Bind_sSpCounty( dataLoan.sSpState, sSpCounty, true) ; 
			Tools.SetDropDownListCaseInsensitive( sSpCounty, dataLoan.sSpCounty );
            sSpLegalDesc.Text = dataLoan.sSpLegalDesc;
            sTitleReqPriorPolicy.Checked = dataLoan.sTitleReqPriorPolicy;
            sTitleReqWarrantyDeed.Checked = dataLoan.sTitleReqWarrantyDeed;
            sTitleReqInsRequirements.Checked = dataLoan.sTitleReqInsRequirements;
            sTitleReqSurvey.Checked = dataLoan.sTitleReqSurvey;
            sTitleReqContract.Checked = dataLoan.sTitleReqContract;
            sTitleReqMailAway.Checked = dataLoan.sTitleReqMailAway;
            sTitleReqInstruction.Text = dataLoan.sTitleReqInstruction;
            sTitleReqPolicyTypeDesc.Text = dataLoan.sTitleReqPolicyTypeDesc;
            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;
            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
            aBDOB.Text = dataApp.aBDob_rep;
            aCDOB.Text = dataApp.aCDob_rep;
            sLenderCaseNum.Text = dataLoan.sLenderCaseNum;
            sLenderCaseNumLckd.Checked = dataLoan.sLenderCaseNumLckd;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
