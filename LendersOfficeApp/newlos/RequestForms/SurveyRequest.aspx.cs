using System;
using DataAccess;

namespace LendersOfficeApp.newlos.RequestForms
{
	public partial class SurveyRequest : LendersOfficeApp.newlos.BaseLoanPage
	{
        protected System.Web.UI.WebControls.CheckBox CheckBox4;
        protected System.Web.UI.WebControls.CheckBox CheckBox5;

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "Survey Request";
            this.PageID = "SurveyRequest";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CSurveyRequestPDF);

            Tools.Bind_sSpT(sSpT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            sSpZip.SmartZipcode(sSpCity, sSpState);
            SurveyRequest_FromZip.SmartZipcode(SurveyRequest_FromCity, SurveyRequest_FromState);
            SurveyRequest_ToZip.SmartZipcode(SurveyRequest_ToCity, SurveyRequest_ToState);
            TitleZip.SmartZipcode(TitleCity, TitleState);
            RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
        protected void PageLoad(object sender, System.EventArgs e)
        {
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(SurveyRequest));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            aBNm.Text = dataApp.aBNm;
            aBHPhone.Text = dataApp.aBHPhone;

            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;
            sSpLegalDesc.Text = dataLoan.sSpLegalDesc;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.SurveyRequest_To, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SurveyRequest_ToCompanyName.Text = preparer.CompanyName;
            SurveyRequest_ToStreetAddr.Text = preparer.StreetAddr;
            SurveyRequest_ToCity.Text = preparer.City;
            SurveyRequest_ToState.Value = preparer.State;
            SurveyRequest_ToZip.Text = preparer.Zip;

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.SurveyRequest_From, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SurveyRequest_FromCompanyName.Text = preparer.CompanyName;
            SurveyRequest_FromStreetAddr.Text = preparer.StreetAddr;
            SurveyRequest_FromCity.Text = preparer.City;
            SurveyRequest_FromState.Value = preparer.State;
            SurveyRequest_FromZip.Text = preparer.Zip;
            SurveyRequest_FromPhoneOfCompany.Text = preparer.PhoneOfCompany;
            SurveyRequest_FromFaxOfCompany.Text = preparer.FaxOfCompany;
            SurveyRequest_FromPrepareDate.Text = preparer.PrepareDate_rep;
			SurveyRequest_FromPrepareDate.ToolTip = "Hint:  Enter 't' for today's date.";

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            TitleCompanyName.Text    = agent.CompanyName;
            TitleStreetAddr.Text     = agent.StreetAddr;
            TitleCity.Text           = agent.City;
            TitleState.Value         = agent.State;
            TitleZip.Text            = agent.Zip;
            TitlePhoneOfCompany.Text = agent.PhoneOfCompany;
            TitleFaxOfCompany.Text   = agent.FaxOfCompany;
            
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Seller, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SellerAgentName.Text = agent.AgentName;
            SellerPhone.Text = agent.Phone;

            Tools.SetDropDownListValue(sSpT, dataLoan.sSpT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);

            sAttachedContractBit.Checked = dataLoan.sAttachedContractBit;
            sAttachedSurveyBit.Checked = dataLoan.sAttachedSurveyBit;
            sAttachedPlansBit.Checked = dataLoan.sAttachedPlansBit;
            sAttachedCostsBit.Checked = dataLoan.sAttachedCostsBit;
            sAttachedSpecsBit.Checked = dataLoan.sAttachedSpecsBit;
            sSurveyRequestN.Text = dataLoan.sSurveyRequestN;

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
