<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="RequestForInsurance.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.RequestForms.RequestForInsurance" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
		<title>Request For Insurance</title>
  </head>
	<body class="RightBackground" MS_POSITIONING="FlowLayout">
		<script language="javascript">
    <!--
      var oRolodex;
      var openedDate = <%= AspxTools.JsString(m_openedDate) %>;                
      
      function _init() {
        oRolodex = new cRolodex();
        <% if ( !IsReadOnly) { %>
        onChangesLPurposeT(); 
        <% } %>
        lockField(document.getElementById('sEstCloseDLckd'), 'sEstCloseD');
        lockField(<%= AspxTools.JsGetElementById(sLenderCaseNumLckd) %>, 'sLenderCaseNum');
      }
      function onDateKeyUp(o, event) {
        if (event.keyCode == 79) {
          o.value = openedDate;
        }
      }
      function f_toggleLenderMortgagee() {
        var args = new Object();
        args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
        args["sUseLenderAgentForInsRequest"] = f_pickFromRadioList("sUseLenderAgentForInsRequest");

        var result = gService.loanedit.call("Toggle_AgentInfo", args);
        if (!result.error) {
          populateForm(result.value, "");
        }
        
      }
      function f_pickFromRadioList(name) {
        var coll = document.forms[0][name];
        for (var i = 0; i < coll.length; i++)
          if (coll[i].checked) return coll[i].value;      
      }
 
    
      function onChangesLPurposeT()
      {
        var sLPurposeT = <%= AspxTools.JsGetElementById(sLPurposeT) %>;
		var sPurchPrice = <%= AspxTools.JsGetElementById(sPurchPrice) %>;
		
		if (sLPurposeT.value != <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Purchase.ToString("D")) %> 
		        && sLPurposeT.value != <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Construct.ToString("D")) %> 
		        && sLPurposeT.value != <%= AspxTools.JsString(DataAccess.E_sLPurposeT.ConstructPerm.ToString("D")) %> 
		        )
		{
		  sPurchPrice.readOnly = true;
		  sPurchPrice.value = "$0.00";
		}
		else
		{
		  sPurchPrice.readOnly = false;
		}
      }

  
//-->
        </script>

		<form id="RequestForInsurance" method="post" runat="server">


			<TABLE class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="MainRightHeader" noWrap>Request for Insurance</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder" id="Table2" cellSpacing="0" cellPadding="0" width="98%" border="0">
							<TR>
								<TD class="LoanFormHeader" vAlign="top" noWrap align="middle" colSpan="3">I. 
									REQUEST</TD>
							</TR>
							<TR>
								<TD vAlign="top" noWrap>
									<TABLE id="Table3" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD class="FieldLabel" noWrap colSpan="2">Insurance Company Agent</TD>
										</TR>
										<TR>
											<td class=FieldLabel nowrap></td>
											<TD noWrap>
												<uc:CFM name=CFM1 runat="server" Type="15" AgentNameField="InsuranceName" PhoneField="InsurancePhone" FaxField="InsuranceFax" CellPhoneField="InsuranceCell" EmailField="InsuranceEmail" CompanyNameField="InsuranceCompany" StreetAddressField="InsuranceAddress" CityField="InsuranceCity" StateField="InsuranceState" ZipField="InsuranceZip"></uc:CFM>
											</TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Name</TD>
											<TD noWrap><asp:textbox id="InsuranceName" runat="server" Width="279px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Phone</TD>
											<TD class="FieldLabel" noWrap><ml:phonetextbox id="InsurancePhone" runat="server" preset="phone" width="120"></ml:phonetextbox>&nbsp; 
												Fax:
												<ml:phonetextbox id="InsuranceFax" runat="server" preset="phone" width="120"></ml:phonetextbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Cell</TD>
											<TD noWrap><ml:phonetextbox id="InsuranceCell" runat="server" preset="phone" width="120"></ml:phonetextbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Email</TD>
											<TD noWrap><asp:textbox id="InsuranceEmail" runat="server"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Company</TD>
											<TD noWrap><asp:textbox id="InsuranceCompany" runat="server" Width="277px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Address</TD>
											<TD noWrap><asp:textbox id="InsuranceAddress" runat="server" Width="276px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD noWrap></TD>
											<TD noWrap><asp:textbox id="InsuranceCity" runat="server" Width="176px"></asp:textbox><ml:statedropdownlist id="InsuranceState" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="InsuranceZip" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></TD>
										</TR>
									</TABLE>
								</TD>
								<TD vAlign="top" noWrap width="10"></TD>
								<TD vAlign="top" noWrap width="100%">
									<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="FieldLabel" noWrap>From</TD>
											<TD noWrap></TD>
										</TR>
										<tr>
											<td class=FieldLabel nowrap></td>
											<td nowrap>
												<uc:CFM name=CFM2 runat="server" Type="22" AgentNameField="RequestOfInsurancePreparerName" AgentTitleField="RequestOfInsuranceTitle" PhoneField="RequestOfInsurancePhone" FaxField="RequestOfInsuranceFaxNum" CompanyNameField="RequestOfInsuranceCompanyName" EmailField="RequestOfInsuranceEmailAddr" StreetAddressField="RequestOfInsuranceStreetAddr" CityField="RequestOfInsuranceCity" StateField="RequestOfInsuranceState" ZipField="RequestOfInsuranceZip"></uc:CFM>
											</td></tr>
										<TR>
											<TD class="FieldLabel" noWrap>Attn</TD>
											<TD noWrap><asp:textbox id="RequestOfInsurancePreparerName" runat="server"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Title</TD>
											<TD noWrap><asp:textbox id="RequestOfInsuranceTitle" runat="server"></asp:textbox></TD>
										</TR>
              <tr>
                <td class=FieldLabel nowrap>Phone</td>
                <td class=FieldLabel nowrap><ml:phonetextbox id=RequestOfInsurancePhone runat="server" width="120" preset="phone"></ml:phonetextbox>&nbsp;Fax 
<ml:phonetextbox id=RequestOfInsuranceFaxNum runat="server" width="120" preset="phone"></ml:phonetextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>Email</td>
                <td class=FieldLabel nowrap><asp:TextBox id=RequestOfInsuranceEmailAddr runat="server"></asp:TextBox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>Company</td>
                <td class=FieldLabel nowrap><asp:textbox id=RequestOfInsuranceCompanyName runat="server" Width="277px"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>Address</td>
                <td class=FieldLabel nowrap><asp:textbox id=RequestOfInsuranceStreetAddr runat="server" Width="276px"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap></td>
                <td class=FieldLabel nowrap><asp:textbox id=RequestOfInsuranceCity runat="server" Width="176px"></asp:textbox><ml:statedropdownlist id=RequestOfInsuranceState runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=RequestOfInsuranceZip runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></td></tr>
										<TR>
											<TD class="FieldLabel" noWrap>Date</TD>
											<TD noWrap class=FieldLabel><ml:datetextbox id="RequestOfInsurancePrepareDate" onkeyup="onDateKeyUp(this, event);" runat="server" preset="date" width="75"></ml:datetextbox></TD>
										</TR>
              <tr>
                <td class=FieldLabel nowrap >Lender's Number</td>
                <td class=FieldLabel><asp:TextBox id=sLenderCaseNum runat="server" Width="202px"></asp:TextBox><asp:CheckBox ID="sLenderCaseNumLckd" runat="server" onclick="refreshCalculation();"/>Lock</td>
                </tr>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder" id="Table5" cellSpacing="0" cellPadding="0" width="98%" border="0">
							<TR>
								<TD class="LoanFormHeader" noWrap align="middle">II. PROPERTY AND MORTGAGE INFORMATION</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>Property Type
									<asp:dropdownlist id="sSpT" runat="server"></asp:dropdownlist>&nbsp;Loan 
									Purpose
									<asp:dropdownlist id="sLPurposeT" runat="server" onchange="onChangesLPurposeT();"></asp:dropdownlist>&nbsp;Lien 
									Position
									<asp:dropdownlist id="sLienPosT" runat="server"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD noWrap>
									<TABLE id="Table6" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD class="FieldLabel" noWrap colSpan="2">Property Address</TD>
											<TD noWrap width="10"></TD>
											<TD noWrap></TD>
											<TD noWrap></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Street</TD>
											<TD noWrap><asp:textbox id="sSpAddr" runat="server" Width="244px" MaxLength="60"></asp:textbox></TD>
											<TD class="FieldLabel" noWrap></TD>
											<TD class="FieldLabel" noWrap>Sales Price</TD>
											<TD noWrap><ml:moneytextbox id="sPurchPrice" runat="server" preset="money" width="90"></ml:moneytextbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>City</TD>
											<TD noWrap><asp:textbox id="sSpCity" runat="server"></asp:textbox><ml:statedropdownlist id="sSpState" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="sSpZip" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></TD>
											<TD class="FieldLabel" noWrap></TD>
											<TD class="FieldLabel" noWrap>Replacement Value</TD>
											<TD noWrap><asp:textbox id="sInsReqReplacement" runat="server" Width="90px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>County</TD>
											<TD noWrap><asp:DropDownList id="sSpCounty" runat="server"></asp:DropDownList></TD>
											<TD class="FieldLabel" noWrap></TD>
											<TD class="FieldLabel" noWrap>Total Loan Amount</TD>
											<TD noWrap><ml:moneytextbox id="sFinalLAmt" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap colSpan="5">Legal Description</TD>
										</TR>
										<TR>
											<TD noWrap colSpan="5"><asp:textbox id="sSpLegalDesc" runat="server" Width="504px" TextMode="MultiLine" Height="70px"></asp:textbox></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD noWrap>
									<TABLE id="Table7" cellSpacing="0" cellPadding="0" border="0">
										<TR>
											<TD class="FieldLabel" noWrap colspan=2>Lender (or Mortgagee):</TD>
										</TR>
              <tr>
                <td class=FieldLabel nowrap colspan=2><asp:RadioButtonList id=sUseLenderAgentForInsRequest runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" onclick="f_toggleLenderMortgagee();">
<asp:ListItem Value="True">Use lender information&nbsp;&nbsp;&nbsp;</asp:ListItem>
<asp:ListItem Value="False">Use mortgagee information</asp:ListItem>
</asp:RadioButtonList></td></tr>
										<TR>
											<td class=FieldLabel nowrap></td>
											<TD noWrap>
												<uc:CFM name=CFM3 runat="server" Type="21" AgentNameField="sAgentLenderAgentName" PhoneField="sAgentLenderAgentPhone" FaxField="sAgentLenderAgentFax" CellPhoneField="sAgentLenderAgentCell" EmailField="sAgentLenderAgentEmail" CompanyNameField="sAgentLenderCompanyName" StreetAddressField="sAgentLenderAddress" CityField="sAgentLenderCity" StateField="sAgentLenderState" ZipField="sAgentLenderZip"></uc:CFM>
											</TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Name</TD>
											<TD noWrap><asp:textbox id="sAgentLenderCompanyName" runat="server" Width="262px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Contact</TD>
											<TD noWrap><asp:textbox id="sAgentLenderAgentName" runat="server" Width="262px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Address</TD>
											<TD noWrap><asp:textbox id="sAgentLenderAddress" runat="server" Width="262px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD noWrap></TD>
											<TD noWrap><asp:textbox id="sAgentLenderCity" runat="server"></asp:textbox><ml:statedropdownlist id="sAgentLenderState" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="sAgentLenderZip" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Phone</TD>
											<TD class="FieldLabel" noWrap><ml:phonetextbox id="sAgentLenderAgentPhone" runat="server" preset="phone" width="120"></ml:phonetextbox>&nbsp;Fax:
												<ml:phonetextbox id="sAgentLenderAgentFax" runat="server" preset="phone" width="120"></ml:phonetextbox>&nbsp;Cellphone:&nbsp;<ml:phonetextbox id="sAgentLenderAgentCell" runat="server" preset="phone" width="120"></ml:phonetextbox>
											</TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Email</TD>
											<TD noWrap><asp:textbox id="sAgentLenderAgentEmail" runat="server" width="262px"></asp:textbox></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD noWrap></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder" id="Table8" cellSpacing="0" cellPadding="0" width="98%" border="0">
							<TR>
								<TD class="LoanFormHeader" noWrap align="middle">III. INSURANCE INFORMATION</TD>
							</TR>
							<TR>
								<TD noWrap>
									<TABLE id="Table9" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD class="FieldLabel" noWrap>Estimated Closing Date&nbsp;</TD>
											<TD noWrap width="90%" class="FieldLabel">
                                                <ml:datetextbox id="sEstCloseD" runat="server" preset="date" width="75"></ml:datetextbox>
                                                <input type="checkbox" runat="server" id="sEstCloseDLckd" onchange="lockField(this, 'sEstCloseD'); refreshCalculation();" />
                                                Lock
                                            </TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Type of Insurance</TD>
											<TD noWrap><asp:checkbox id="sInsReqFlood" runat="server" Text="Flood"></asp:checkbox><asp:checkbox id="sInsReqWind" runat="server" Text="Wind/Storm"></asp:checkbox><asp:checkbox id="sInsReqHazard" runat="server" Text="Hazard"></asp:checkbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap></TD>
											<TD noWrap><asp:checkbox id="sInsReqEscrow" runat="server" Text="Insurance Escrowed"></asp:checkbox></TD>
										</TR>
										<TR>
											<TD class="FieldLabel" noWrap>Comments</TD>
											<TD noWrap></TD>
										</TR>
										<TR>
											<TD noWrap colSpan="2"><asp:textbox id="sInsReqComments" runat="server" Width="455px" TextMode="MultiLine" Height="110px"></asp:textbox></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
		<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
	</body>
</html>
