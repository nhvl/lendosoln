using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.RequestForms
{
    public class RequestForTitleServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(RequestForTitleServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.CreateNew);
            
            agent.AgentName = GetString("TitleName");
            agent.CompanyName = GetString("TitleCompany");
            agent.Phone = GetString("TitlePhone");
            agent.FaxNum = GetString("TitleFax");
            agent.CellPhone = GetString("TitleCell");
            agent.StreetAddr = GetString("TitleAddress");
            agent.City = GetString("TitleCity");
            agent.State = GetString("TitleState");
            agent.Zip = GetString("TitleZip");
            agent.EmailAddr = GetString("TitleEmail");
            agent.Update();
            
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Seller, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = GetString("SellerName");
            agent.StreetAddr = GetString("SellerAddress");
            agent.City = GetString("SellerCity");
            agent.State = GetString("SellerState");
            agent.Zip = GetString("SellerZip");
            agent.Update();
            
            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Mortgagee, E_ReturnOptionIfNotExist.CreateNew);
            agent.AgentName = GetString("MortgageeName");
            agent.CompanyName = GetString("MortgageeCompany");
            agent.StreetAddr = GetString("MortgageeAddress");
            agent.City = GetString("MortgageeCity");
            agent.State = GetString("MortgageeState");
            agent.Zip = GetString("MortgageeZip");
            agent.Update();
            
            dataLoan.sSpT = (E_sSpT) GetInt("sSpT");
            dataLoan.sLPurposeT = (E_sLPurposeT) GetInt("sLPurposeT");
            dataApp.aOccT = (E_aOccT) GetInt("aOccT");
            
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.RequestOfTitle, E_ReturnOptionIfNotExist.CreateNew);
            preparer.PreparerName = GetString("RequestOfTitlePreparerName");
            preparer.Title = GetString("RequestOfTitleTitle");
            preparer.PrepareDate_rep = GetString("RequestOfTitlePrepareDate");
            preparer.CompanyName = GetString("RequestOfTitleCompanyName");
            preparer.StreetAddr = GetString("RequestOfTitleStreetAddr");
            preparer.City = GetString("RequestOfTitleCity");
            preparer.State = GetString("RequestOfTitleState");
            preparer.Zip = GetString("RequestOfTitleZip");
            preparer.Phone = GetString("RequestOfTitlePhone");
            preparer.FaxNum = GetString("RequestOfTitleFaxNum");
            preparer.EmailAddr = GetString("RequestOfTitleEmailAddr");
            preparer.Update();
            
            dataLoan.sPurchPrice_rep = GetString("sPurchPrice");
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sSpCounty = GetString("sSpCounty");
            dataLoan.sSpLegalDesc = GetString("sSpLegalDesc");
                        
            dataLoan.sTitleReqOwnerNm = GetString("sTitleReqOwnerNm");
            dataLoan.sTitleReqOwnerPhone = GetString("sTitleReqOwnerPhone");
            
            dataLoan.sTitleReqPriorPolicy = GetBool("sTitleReqPriorPolicy");
            dataLoan.sTitleReqWarrantyDeed = GetBool("sTitleReqWarrantyDeed");
            dataLoan.sTitleReqInsRequirements = GetBool("sTitleReqInsRequirements");
            dataLoan.sTitleReqSurvey = GetBool("sTitleReqSurvey");
            dataLoan.sTitleReqContract = GetBool("sTitleReqContract");
            dataLoan.sTitleReqMailAway = GetBool("sTitleReqMailAway");
            dataLoan.sTitleReqInstruction = GetString("sTitleReqInstruction");
            dataLoan.sTitleReqPolicyTypeDesc = GetString("sTitleReqPolicyTypeDesc");
            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckd");
            dataLoan.sEstCloseD_rep = GetString("sEstCloseD");
            dataLoan.sLenderCaseNum = GetString("sLenderCaseNum");
            dataLoan.sLenderCaseNumLckd = GetBool("sLenderCaseNumLckd");
            dataApp.aBDob_rep = GetString("aBDOB");
            dataApp.aCDob_rep = GetString("aCDOB");

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sLenderCaseNum", dataLoan.sLenderCaseNum);
            SetResult("sEstCloseD", dataLoan.sEstCloseD);
        }
    }

	public partial class RequestForTitleService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new RequestForTitleServiceItem());
        }
	}
}
