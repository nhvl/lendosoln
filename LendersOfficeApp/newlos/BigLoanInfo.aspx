﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="BigLoanInfo.aspx.cs" Inherits="LendersOfficeApp.newlos.BigLoanInfo" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimate2010Footer" Src="~/newlos/Forms/GoodFaithEstimate2010Footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="QualifyingBorrower" Src="~/newlos/QualifyingBorrower.ascx" %>
<%@ Register TagPrefix="QRP" TagName="QualRatePopup" Src="~/newlos/Forms/QualRateCalculationPopup.ascx" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html>
<head runat="server">
    <title>LoanInfo</title>
    <style type="text/css">
        .popup
        {
            position: absolute;
            width: 300px;
            background-color: white;
            border: 1px solid;
        }

        /* Adding "_" before attribute name makes those attributes valid only in IE 6 or quirks (IE 5.5) mode */
	    html, body{
            _height: 100%;
            _overflow: hidden;
        }

        #wrapper {
            _width: 100%;
            _height: 100%;
            _overflow: auto;
            _position: relative;
        }

	    #footerWrapper
	    {
	        position: fixed;
	        left: 0px;
	        bottom: 0px;
	        width: 100%;

            _position: absolute; /* Overrides position attribute in quirks mode */
        }

        #footer {
            _margin-right: 17px; /* for right scroll bar in quirks mode */
            max-height: 500px;

            /**
            /* Following properties needed to keep mouse clicks from falling through to underlying divs.
            /* Placed at #footer level instead of #footerWrapper so that user can still click on #wrapper's scrollbar
            **/
            background: url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7); /* background image set to 1x1 transparent GIF. */
            width: 100%; /* needs to be set to 100%, or #footer width will match width of active tab */
        }

        .tabContent {
            background-color: gainsboro;
            padding: 10px;
        }

        #Tabs {
            background-color: Transparent;
        }

        #closeIcon{
            padding: 10px;
            float:right;
        }

        #closeIcon a,
        #closeIcon a:hover
        {
            font-size:15px;
            color: Gray;
            text-decoration: none;
        }
        .dateCol { width: 100px; }
        .docCol { width: 170px; }
        .methodCol { width: 153px; }
        .trackingCol { width: 133px; }
        .commentsCol { width: 133px; }

        #pageBody:not(.FTFCU) .HideUnlessFTFCU
        {
            display:none;
        }
        .w-174 { width: 174px; }
        
        .income-collection-dependent label {
            display: none;
        }

        .is-income-collection-enabled .income-collection-dependent label {
            display: initial;
        }

        .is-income-collection-enabled .income-collection-dependent a {
            display: none;
        }

        #Dot .true-container {
            height: 450px;
        }
    </style>


</head>
<body runat="server" id="pageBody" bgcolor="gainsboro" ms_positioning="FlowLayout">

    <script type="text/javascript">
        function valueSync(source, destId)
        {
            var sourceVal = source.value;
            document.getElementById(destId).value = sourceVal;
        }

        function checkedSync(source, destId)
        {
            var sourceVal = source.checked;
            document.getElementById(destId).checked = sourceVal;
        }

        jQuery(function($)
        {
            // New popups on Loan Info
            $('#topPopClick').click(function(e)
            {
                var scrollTop = $(this).parents('#wrapper').scrollTop();
                $('#topPop').css({ left: e.pageX + 20, top: e.pageY + scrollTop }).show();
            });

            $('#botPopClick').click(function(e)
            {
                var scrollTop = $(this).parents('#wrapper').scrollTop();
                $('#botPop').css({ left: e.pageX + 20, top: e.pageY }).show();
            });

            $('.popupClose').click(function()
            {
                $(this).closest('.popup').hide();
            });

            // Borrower Info
            updateMailingAddressFields();

                addEventHandler(<%= AspxTools.JsGetElementById(aBState) %>, "change", syncMailingAddress, false);
                addEventHandler(<%= AspxTools.JsGetElementById(aCState) %>, "change", syncMailingAddress, false);
            enableVorBtn(true);
            enableVorBtn(false);

            Page_Validators = new Array(document.getElementById("RegularExpressionValidator1"));

            _init();
        });

        // merged _init
        function _init()
        {
            addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(RegularExpressionValidator1) %>);

            _initLoanInfo();
            _initMonIncome();
            _initMip();
            _initFooter();
        }
    </script>

    <script id="BorrowerInfoJS" type="text/javascript">

        if (!('trim' in String.prototype)) {
            String.prototype.trim = function() {
                return this.replace(/^\s+/, '').replace(/\s+$/, '');
            };
        }

        function onEditBorrAliasesClick()
        {
            editAliasesPOA('B');
        }

        function onEditCoborrAliasesClick()
        {
            editAliasesPOA('C');
        }

        function editAliasesPOA(borrMode)
        {
            var aliasPOAData = {
                alias : $('input[id$=a' + borrMode + 'Alias]').val(),
                powerOfAttorney : $('input[id$=a' + borrMode + 'PowerOfAttorneyNm]').val()
            };
            var args = {
                data : JSON.stringify(aliasPOAData)
            };

            var path = '/newlos/Borrower/EditAliasesPOA.aspx';
            var url = path
                + '?loanid=' + <%= AspxTools.JsString(LoanID) %>
                + '&borrmode=' + borrMode;
            showModal(url, args, null, null, function(modalResult){
                if (modalResult.OK) {
                    var data = JSON.parse(modalResult.data);
                    $('input[id$=a' + borrMode + 'Alias]').val(data.alias);
                    $('input[id$=a' + borrMode + 'PowerOfAttorneyNm]').val(data.powerOfAttorney);
                    // Mark page as dirty
                    updateDirtyBit();
                }
            },{ hideCloseButton: true });
        }

        function calculateAge(aDobId, aAgeId)
        {
            var args = {
                _ClientID : ML._ClientID,
                dob : document.getElementById(aDobId).value,
                age : document.getElementById(aAgeId).value
            };


            var result = gService.loanedit.call("CalculateAge", args);
            if (!result.error)
            {
                if (result.value["age"] != null && result.value["age"] != "")
                {
                    document.getElementById(aAgeId).value = result.value["age"];
                }
            }
        }

        function updateBorrowerName() {
            var name = <%= AspxTools.JsGetElementById(aBLastNm) %>.value + ", " + <%= AspxTools.JsGetElementById(aBFirstNm) %>.value;
            parent.info.f_updateApplicantDDL(name, ML.aAppId);

            //Borrower preferred name
            var borrPreferredNmSet = <%= AspxTools.JsGetElementById(aBPreferredNmLckd) %>.checked;
            if (!borrPreferredNmSet)
            {
                <%= AspxTools.JsGetElementById(aBPreferredNm) %>.value = <%= AspxTools.JsGetElementById(aBFirstNm) %>.value;
            }
        }

        function coBorrowerPreferredNameCalc() {
            //Co-borrower preferred name
            var coBorrPreferredNmSet = <%= AspxTools.JsGetElementById(aCPreferredNmLckd) %>.checked;
            if (!coBorrPreferredNmSet)
            {
                <%= AspxTools.JsGetElementById(aCPreferredNm) %>.value = <%= AspxTools.JsGetElementById(aCFirstNm) %>.value;
            }
        }

        function onCopyBorrowerClick() {
            if('' == <%= AspxTools.JsGetElementById(aCLastNm) %>.value)
                <%= AspxTools.JsGetElementById(aCLastNm) %>.value = <%= AspxTools.JsGetElementById(aBLastNm) %>.value;

            <%= AspxTools.JsGetElementById(aCHPhone) %>.value = <%= AspxTools.JsGetElementById(aBHPhone) %>.value;
            <%= AspxTools.JsGetElementById(aCAddr) %>.value = <%= AspxTools.JsGetElementById(aBAddr) %>.value;
            <%= AspxTools.JsGetElementById(aCCity) %>.value = <%= AspxTools.JsGetElementById(aBCity) %>.value;
            <%= AspxTools.JsGetElementById(aCState) %>.value = <%= AspxTools.JsGetElementById(aBState) %>.value;
            <%= AspxTools.JsGetElementById(aCZip) %>.value = <%= AspxTools.JsGetElementById(aBZip) %>.value;
            <%= AspxTools.JsGetElementById(aCAddrT) %>.value = <%= AspxTools.JsGetElementById(aBAddrT) %>.value;
            <%= AspxTools.JsGetElementById(aCAddrYrs) %>.value = <%= AspxTools.JsGetElementById(aBAddrYrs) %>.value;
            <%= AspxTools.JsGetElementById(aCAddrMailSourceT) %>.value = <%= AspxTools.JsGetElementById(aBAddrMailSourceT) %>.value;
            <%= AspxTools.JsGetElementById(aCAddrMail) %>.value = <%= AspxTools.JsGetElementById(aBAddrMail) %>.value;
            <%= AspxTools.JsGetElementById(aCCityMail) %>.value = <%= AspxTools.JsGetElementById(aBCityMail) %>.value;
            <%= AspxTools.JsGetElementById(aCStateMail) %>.value = <%= AspxTools.JsGetElementById(aBStateMail) %>.value;
            <%= AspxTools.JsGetElementById(aCZipMail) %>.value = <%= AspxTools.JsGetElementById(aBZipMail) %>.value;


            updateMailingAddressFields();
            updateDirtyBit();
        }

        function fillPresentAddress() {
            <%= AspxTools.JsGetElementById(aBAddr) %>.value = <%= AspxTools.JsGetElementById(sSpAddr_spi) %>.value;
            <%= AspxTools.JsGetElementById(aBCity) %>.value = <%= AspxTools.JsGetElementById(sSpCity_spi) %>.value;
            <%= AspxTools.JsGetElementById(aBZip) %>.value = <%= AspxTools.JsGetElementById(sSpZip_spi) %>.value;
            <%= AspxTools.JsGetElementById(aBState) %>.value = <%= AspxTools.JsGetElementById(sSpState_spi) %>.value;
            updateDirtyBit();
        }
        function syncMailingAddress() {
            var source = $(<%= AspxTools.JsGetElementById(aBAddrMailSourceT) %>).val();
            switch(source)
            {
                case "0":   // PresentAddress
                    <%= AspxTools.JsGetElementById(aBAddrMail) %>.value = <%= AspxTools.JsGetElementById(aBAddr) %>.value;
                    <%= AspxTools.JsGetElementById(aBCityMail) %>.value = <%= AspxTools.JsGetElementById(aBCity) %>.value;
                    <%= AspxTools.JsGetElementById(aBStateMail) %>.value = <%= AspxTools.JsGetElementById(aBState) %>.value;
                    <%= AspxTools.JsGetElementById(aBZipMail) %>.value = <%= AspxTools.JsGetElementById(aBZip) %>.value;
                    break;
                case "1":   // SubjectPropertyAddress
                    <%= AspxTools.JsGetElementById(aBAddrMail) %>.value = <%= AspxTools.JsGetElementById(sSpAddr_spi) %>.value;
                    <%= AspxTools.JsGetElementById(aBCityMail) %>.value = <%= AspxTools.JsGetElementById(sSpCity_spi) %>.value;
                    <%= AspxTools.JsGetElementById(aBStateMail) %>.value = <%= AspxTools.JsGetElementById(sSpState_spi) %>.value;
                    <%= AspxTools.JsGetElementById(aBZipMail) %>.value = <%= AspxTools.JsGetElementById(sSpZip_spi) %>.value;
                    break;
            }

            source = $(<%= AspxTools.JsGetElementById(aCAddrMailSourceT) %>).val();
            switch(source)
            {
                case "0":   // PresentAddress
                    <%= AspxTools.JsGetElementById(aCAddrMail) %>.value = <%= AspxTools.JsGetElementById(aCAddr) %>.value;
                    <%= AspxTools.JsGetElementById(aCCityMail) %>.value = <%= AspxTools.JsGetElementById(aCCity) %>.value;
                    <%= AspxTools.JsGetElementById(aCStateMail) %>.value = <%= AspxTools.JsGetElementById(aCState) %>.value;
                    <%= AspxTools.JsGetElementById(aCZipMail) %>.value = <%= AspxTools.JsGetElementById(aCZip) %>.value;
                    break;
                case "1":   // SubjectPropertyAddress
                    <%= AspxTools.JsGetElementById(aCAddrMail) %>.value = <%= AspxTools.JsGetElementById(sSpAddr_spi) %>.value;
                    <%= AspxTools.JsGetElementById(aCCityMail) %>.value = <%= AspxTools.JsGetElementById(sSpCity_spi) %>.value;
                    <%= AspxTools.JsGetElementById(aCStateMail) %>.value = <%= AspxTools.JsGetElementById(sSpState_spi) %>.value;
                    <%= AspxTools.JsGetElementById(aCZipMail) %>.value = <%= AspxTools.JsGetElementById(sSpZip_spi) %>.value;
                    break;
            }

            updateMailingAddressFields();
        }

        function updateMailingAddressFields() {

            var b = $(<%= AspxTools.JsGetElementById(aBAddrMailSourceT) %>).val() != "2";
            <%= AspxTools.JsGetElementById(aBAddrMail) %>.readOnly = b;
            <%= AspxTools.JsGetElementById(aBCityMail) %>.readOnly = b;
            disableDDL(<%= AspxTools.JsGetElementById(aBStateMail) %>, b);
            <%= AspxTools.JsGetElementById(aBZipMail) %>.readOnly = b;

            b = $(<%= AspxTools.JsGetElementById(aCAddrMailSourceT) %>).val() != "2";
            <%= AspxTools.JsGetElementById(aCAddrMail) %>.readOnly = b;
            <%= AspxTools.JsGetElementById(aCCityMail) %>.readOnly = b;
            disableDDL(<%= AspxTools.JsGetElementById(aCStateMail) %>, b);
            <%= AspxTools.JsGetElementById(aCZipMail) %>.readOnly = b;
        }
        function enableVorBtn(isborrower) {
            var readonly = document.getElementById('_ReadOnly');
            if( readonly.value === 'True') {
                return;
            }
            var fields = null;
            if( isborrower ) {
                fields = [
                    <%= AspxTools.JsGetElementById(aBAddr) %>.value,
                    <%= AspxTools.JsGetElementById(aBCity) %>.value,
                    <%= AspxTools.JsGetElementById(aBState) %>.value,
                    <%= AspxTools.JsGetElementById(aBZip) %>.value
                ];
            } else {
                fields = [
                    <%= AspxTools.JsGetElementById(aCAddr) %>.value,
                    <%= AspxTools.JsGetElementById(aCCity) %>.value,
                    <%= AspxTools.JsGetElementById(aCState) %>.value,
                    <%= AspxTools.JsGetElementById(aCZip) %>.value
                ];
            }
            var enable = false;
            var rent = isborrower ?  <%= AspxTools.JsGetElementById(aBAddrT) %> :   <%= AspxTools.JsGetElementById(aCAddrT) %>;
            var button = isborrower ? document.getElementById('EditVORB') : document.getElementById('EditVORC');

            for( var i = 0; i < fields.length; i++) {
                if( fields[i].trim().length > 0 ) {
                    enable = true;
                    break;
                }
            }

            if( rent.value !== "1" ) {
                enable = false;
            }

            if( enable ) {
                button.removeAttribute('disabled');
            }
            else {
                button.disabled = 'disabled';
            }
        }
        function EditVOR(isborrower) {
            var readonly = document.getElementById('_ReadOnly');
            if( readonly.value === 'True') {
                return;
            }
            if (isDirty()) {
                var ret = confirm('Save is required before proceeding');
                if (ret ) saveMe();
                else { return; }
            }

            var recordid = isborrower ? '11111111-1111-1111-1111-111111111111' : '44444444-4444-4444-4444-444444444444';

            linkMe('Verifications/VORRecord.aspx', 'recordid=' + recordid );
        }

        var Page_Validators;
    </script>

    <script id="LoanInfoJS" type="text/javascript">

        function _initLoanInfo() {
            f_onchange_sLT();

	        var bTemplate = <%= AspxTools.JsBool(m_isTemplate)%> ;
            var sLPurposeTValue = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value;
	        var isConstruction = sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Construct.ToString("D")) %>
						        || sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.ConstructPerm.ToString("D")) %>;

            var bPurchase = (sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Purchase) %>
					        || (isConstruction && (!ML.AreConstructionLoanCalcsMigrated || ML.IsConstructionAndLotPurchase)));

	        var bStreamlineRefi = sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.FhaStreamlinedRefinance) %>;
            var bCannotModifyLoanName = <%= AspxTools.JsBool(m_cannotModifyLoanName) %>;
            var isRenovationLoan = <%=AspxTools.JQuery(sIsRenovationLoan)%>.is(':checked');
	        
	        document.getElementById("btnGenerateNewLoanNumber").disabled = bCannotModifyLoanName || isReadOnly() || bTemplate;
	        if (bCannotModifyLoanName)
		        document.getElementById("btnGenerateNewLoanNumber").title = "You do not have permission to modify the loan number.";
	        <%= AspxTools.JsGetElementById(sPurchPrice) %>.readOnly = !bPurchase || isReadOnly();
	        <%= AspxTools.JsGetElementById(sLAmtLckd) %>.disabled = !bPurchase || isRenovationLoan;

            if (!ML.isUlad2019) {
                <%= AspxTools.JsGetElementById(sLAmtLckd_foot) %>.disabled = !bPurchase || isRenovationLoan;
            }

	        var OrigAppraisedVal = document.getElementById("OriginalAppraisedValueSpan");
	        var bIsWoAppraisal = <%= AspxTools.JsGetElementById(sFHAPurposeIsStreamlineRefiWithoutAppr) %>.value == "1";
	        OrigAppraisedVal.style.display = (bStreamlineRefi && bIsWoAppraisal) ? "inline" : "none";

	        var sLAmtLckdChecked = <%= AspxTools.JsGetElementById(sLAmtLckd) %>.checked;
	        <%= AspxTools.JsGetElementById(sDownPmtPc) %>.readOnly = sLAmtLckdChecked;
	        <%= AspxTools.JsGetElementById(sEquityCalc) %>.readOnly = sLAmtLckdChecked;
	        <%= AspxTools.JsGetElementById(sLAmtCalc) %>.readOnly = !sLAmtLckdChecked;
	        <%= AspxTools.JsGetElementById(sProMIns) %>.readOnly = !<%= AspxTools.JsGetElementById(sProMInsLckd) %>.checked;
	        <%= AspxTools.JsGetElementById(sProOHExp) %>.readOnly = !<%= AspxTools.JsGetElementById(sProOHExpLckd) %>.checked;
	        <%= AspxTools.JsGetElementById(sHighPricedMortgageT) %>.disabled = !<%= AspxTools.JsGetElementById(sHighPricedMortgageTLckd) %>.checked;

	        f_onchange_sQualIR( true );
	        f_onchange_sIsOptionArm( true );
	        f_onchange_sIsLineOfCredit( true );

	        lockField(<%=AspxTools.JsGetElementById(this.sQualIRLckd)%>, 'sQualIR');
            lockField(<%=AspxTools.JsGetElementById(this.sTotalRenovationCostsLckd)%>, 'sTotalRenovationCosts');

            document.getElementById('ConstructionPurposeData').style.display = (!ML.AreConstructionLoanCalcsMigrated && isConstruction) ? "" : "none";

            var isRefinance = sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.FhaStreamlinedRefinance.ToString("D")) %>
					        || sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.VaIrrrl.ToString("D")) %>
					        || sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Refin.ToString("D")) %>
					        || sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.RefinCashout.ToString("D")) %>
                            || (isConstruction && ML.AreConstructionLoanCalcsMigrated && ML.IsConstructionPurposeRefi);

            $("#sIsStudentLoanCashoutRefiRow").toggle(sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.RefinCashout.ToString("D")) %>);

            if (isRefinance)
	        {
		        <%= AspxTools.JsGetElementById(sDownOrEquityLabel) %>.innerText = "Equity";
		        <%= AspxTools.JsGetElementById(sDownOrEquityPcLabel) %>.innerText = "Equity (%)";
	        }
	        else
	        {
		        <%= AspxTools.JsGetElementById(sDownOrEquityLabel) %>.innerText = "Down Payment";
		        <%= AspxTools.JsGetElementById(sDownOrEquityPcLabel) %>.innerText = "Down Payment (%)";
	        }

            enableDisableLoanType();
            $("#asIsValueRow").toggle(isRenovationLoan);
            $("#navigateToRenovRow").toggle(isRenovationLoan);
            var loanPurpose = $("#sLPurposeT").val();
            $("#inducedPurchPriceRow").toggle(isRenovationLoan &&
                (loanPurpose == 0 || loanPurpose == 3 || loanPurpose == 4)); // loan purpose is purchase, construction, or construct perm

            if (isRenovationLoan) {
                document.getElementById("appraisedValueLabel").innerText = "As-Completed Value";
            }
            else {
                document.getElementById("appraisedValueLabel").innerText = "Appraised Value";
            }

            if (!ML.isUlad2019) {
                $("#sAltCostLckd").prop("disabled", !isRenovationLoan);
            }

            $("#sIsRenovationLoan").on("click", function () {
                var $this = $(this);
                var $sAltCostLckd = $("#sAltCostLckd");

                if ($this.prop("checked")) {
                    $sAltCostLckd.prop("checked", false);
                    lockFields();
                }
            });

            $("#navigateToRenovLink").toggle(ML.EnableRenovationLoanSupport);
            $("#plainRenovText").toggle(ML.IsEnableRenovationCheckboxInPML && !ML.EnableRenovationLoanSupport);

            $("#sTotalRenovationCostsLckd").prop('disabled', ML.IsEnableRenovationCheckboxInPML && !ML.EnableRenovationLoanSupport);

            $(".RenovationContent").toggle(ML.IsEnableRenovationCheckboxInPML || ML.EnableRenovationLoanSupport);

            SetQualTermReadonly();
            onPropertyTypeChange();
        }

        function SetQualTermReadonly() {
            var isManual = $(".sQualTermCalculationType").val() == '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Manual)%>';
            $(".sQualTerm").prop("readonly", !isManual);
        }

        function onPropertyTypeChange() {
            var manufacturedTypes = [
                <%=AspxTools.JsString(E_sGseSpT.ManufacturedHomeCondominium)%>,
                <%=AspxTools.JsString(E_sGseSpT.ManufacturedHomeMultiwide)%>,
                <%=AspxTools.JsString(E_sGseSpT.ManufacturedHousing)%>,
                <%=AspxTools.JsString(E_sGseSpT.ManufacturedHousingSingleWide)%>
            ];

            var propertyType = <%=AspxTools.JQuery(this.sGseSpT)%>.val();
            $('#HomeIsMhAdvantageRow').toggle(manufacturedTypes.indexOf(propertyType) !== -1);
        }

        function _onError(errMsg) {
	        if (errMsg == <%= AspxTools.JsString(LendersOffice.Common.JsMessages.LoanInfo_DuplicateLoanNumber) %>) {

		        var val = <%= AspxTools.JsGetElementById(sLNm_Validator) %>;
		        val.isvalid = false;
		        ValidatorUpdateDisplay(val);
	        }
	        alert(errMsg);
	        updateDirtyBit();
        }

        function f_checkBlank() {
	        var sLNm = <%= AspxTools.JsGetElementById(sLNm) %>.value;
	        if (sLNm == "") {
		        var val = <%= AspxTools.JsGetElementById(sLNm_Validator) %>;
		        val.isvalid = false;
		        ValidatorUpdateDisplay(val);
	        }
        }

        function f_fhaLimitedPage() {
	        window.open("https://entp.hud.gov/idapp/html/hicostlook.cfm", 'fhalimit', 'width=780,height=600,menu=no,status=yes,location=no,resizable=yes');
	        return false;
        }

        function f_navigateToRenovLoanPage() {
            if ($("#sLT").val() == 1) { // loan type is FHA
                linkMe('renovation/fha203kmaximummortgage.aspx');
            }
            else {
                linkMe('renovation/fanniemaehomestylemaximummortgage.aspx');
            }
        }

        function findCCTemplate() {
            PolyShouldShowConfirmSave(isDirty, function(confirmed){
                if (confirmed && !saveMe()){
                    return;
                }

                showModal(<%= AspxTools.JsString(ClosingCostPageUrl) %>, null, null, null, function(args){
                    if (args.OK) {
                        self.location = self.location; // Refresh;
                    }
                },{ hideCloseButton: true, height:370 });
            });
        }

        function generateNewLoanName()
        {
	        <% if (!m_cannotModifyLoanName) { %>
		        var res;
		        var args = {};

		        args[ "sLNm"   ] = <%= AspxTools.JsGetElementById(sLNm) %>.value;
		        args[ "loanId" ] = ML.sLId;
		        args["_ClientID"] = ML._ClientID;

		        res = gService.loanedit.call("GenerateNewLoanNumber", args);

		        if (res.error)
		        {
			        alert( "Failed to update loan number.  Please contact your system administrator if the problem persists." );
			        return;
		        }

		        if (res.value.ErrMsg == null)
		        {
			        <%= AspxTools.JsGetElementById(sLNm) %>.value = res.value.NewLoanName;
                    triggerEvent(<%= AspxTools.JsGetElementById(sLNm) %>,"change");
			        updateDirtyBit();
		        }
		        else
		        {
			        alert( res.value.ErrMsg );
		        }
	        <% } %>
        }

        function on_sDownPmtPc_change(event) {
	        var p = parseMoneyFloat(<%= AspxTools.JsGetElementById(sPurchPrice) %>.value);
            var sLPurposeTValue = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value;

            if (!ML.AreConstructionLoanCalcsMigrated
                && (sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Construct) %>
		            || sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.ConstructPerm) %> ))
	        {
                p = p + parseMoneyFloat(<%= AspxTools.JsGetElementById(sLandCost) %>.value);
	        }

	        var a = parseMoneyFloat(<%= AspxTools.JsGetElementById(sApprVal) %>.value);

	        var minPurchaseOrAppraisal = p;
	        if (a > 0 && a < p)
	        {
		        minPurchaseOrAppraisal = a;
	        }

	        var d = parseFloat(<%= AspxTools.JsGetElementById(sDownPmtPc) %>.value);
	        var e = ((d / 100.0) * minPurchaseOrAppraisal) + 0.005;
	        var ctl = <%= AspxTools.JsGetElementById(sEquityCalc) %>;
	        ctl.value = e;
	        format_money(ctl);
	        updateDirtyBit(event);
	        refreshCalculation();
        }

        <% if (m_isPmlEnabled) { %>
        function f_runLPE() {
	        linkMe('Template/RunEmbeddedPML.aspx?islpe=t');
        }
        <% } %>

        function f_runNonLPE() {
	        linkMe('Template/QualifiedLoanProgramsearchResultNew.aspx?islpe=f');
        }

        function f_onchange_sLT() {
	        var v = <%= AspxTools.JsGetElementById(sLT) %>.value;
	        var bFHA = v == <%= AspxTools.JsString(DataAccess.E_sLT.FHA.ToString("D")) %>;
	        document.getElementById("FHALimitPanel").style.display = bFHA ? "" : "none";
        }

        function f_onchange_sIsOptionArm( bIsInit )
        {
	        if ( ! bIsInit )
	        {
		        refreshCalculation();
	        }

	        var sOptionArmTeaserR = <%= AspxTools.JsGetElementById(sOptionArmTeaserR) %>;
	        var sIsOptionArm = <%= AspxTools.JsGetElementById(sIsOptionArm) %>;

	        sOptionArmTeaserR.readOnly = <%= AspxTools.JsBool((m_IsRateLocked)) %> || ( ! sIsOptionArm.checked );
        }

        function f_onchange_sQualIR( bIsInit )
        {
	        if ( ! bIsInit )
	        {
		        refreshCalculation();
	        }

	        var sQualIR = <%= AspxTools.JsGetElementById(sQualIR) %>;
            var sIsQRateIOnly = <%= AspxTools.JsGetElementById(sIsQRateIOnly) %>;
            var sUseQualRate = document.getElementById(QualRateFieldIds.ShouldUseQualRate).checked;
            var sQualIRLckd = <%= AspxTools.JsGetElementById(sQualIRLckd) %>.checked;

            sIsQRateIOnly.disabled = ( sQualIR.value == '0.000%' || sQualIR.value == '' || (!sUseQualRate && !sQualIRLckd));
        }

        function f_onchange_sIsLineOfCredit( bIsInit )
        {
	        //OPM 127698 : only show for FTFCU for now
	        if (<%= AspxTools.JsBool(!m_showCreditLineFields)%>)
	        {
		        return;
	        }
	        if ( ! bIsInit )
	        {
		        refreshCalculation();
	        }

	        var bIsLineOfCredit = <%= AspxTools.JsGetElementById(sIsLineOfCredit) %>.checked;
	        document.getElementById('CreditLineAmtPanel').style.display = bIsLineOfCredit ? 'table-row' : 'none';
        }
    </script>

    <script id="MonthlyIncomeJS" type="text/javascript">
        function _initMonIncome()
        {
          handleRentalILock();
        }

        function _postRefreshCalculation(results) {
          handleRentalILock();

          // The MI company name dropdown has special handling that requires
          // adding and removing options from the dropdown based on the loan
          // type. In some cases, the loan type can be updated by the background
          // calculation. In this case, we won't know which options to add/
          // remove until this point. We also need to manually set the value
          // again because the value of sMiCompanyNmT may not have been in the
          // dropdown before calling updateCompanyNm().
          updateCompanyNm();
          document.getElementById('sMiCompanyNmT').value = results.sMiCompanyNmT;

          enableDisableLoanType();
          updateQualRateFields(results);
          postPopulationQb(results);
      }

        function enableDisableLoanType() {
            var purpose = document.getElementById('sLPurposeT').value;
            var disableLoanType = purpose === <%= AspxTools.JsString(DataAccess.E_sLPurposeT.FhaStreamlinedRefinance.ToString("D")) %> ||
                purpose === <%= AspxTools.JsString(DataAccess.E_sLPurposeT.VaIrrrl.ToString("D")) %>;
            var loanTypeDropdown = document.getElementById('sLT');

            loanTypeDropdown.disabled = disableLoanType;
            loanTypeDropdown.style.backgroundColor = 'white';
        }

        function handleRentalILock()
        {
          var b = <%= AspxTools.JsGetElementById(aNetRentI1003Lckd) %>.checked;

          <%= AspxTools.JsGetElementById(aBNetRentI1003) %>.readOnly = !b;
          <%= AspxTools.JsGetElementById(aCNetRentI1003) %>.readOnly = !b;
        }
        function f_incomeCalc(BClientId, CClientId, IncomeDesc) {
          var args = {
                        aBName : <%= AspxTools.JsString(m_sBName)%>,
                        aCName : <%= AspxTools.JsString(m_sCName)%>,
                        aBBaseI : document.getElementById(BClientId).value,
                        aCBaseI : document.getElementById(CClientId).value,
                        aDescription : 'Monthly Income: ' + IncomeDesc
                     };

          showModal('/newlos/Forms/IncomeCalculator.aspx', args, null, null, function(wargs){
              if (wargs.OK)
              {
                if (wargs.aBBaseI != null)
                {
                    document.getElementById(BClientId).value = wargs.aBBaseI;
                    document.getElementById(BClientId).focus();
                }
                if (wargs.aCBaseI != null)
                {
                    document.getElementById(CClientId).value = wargs.aCBaseI;
                    document.getElementById(CClientId).focus();
                }
                document.getElementById(BClientId).focus();

                if (typeof(refreshCalculation) == 'function')
                  refreshCalculation();

                updateDirtyBit();
              }
          },{ hideCloseButton: true });
        }
    </script>

    <script id="MIPJS" type="text/javascript">
        var m_sFfUfMipIsBeingFinancedRecentValue;

        function _initMip() {
            var fhaLoan = isFHALoan();
            m_sFfUfMipIsBeingFinancedRecentValue = <%=AspxTools.JsGetElementById(sFfUfMipIsBeingFinanced_0)%>.checked;
            <%=AspxTools.JsGetElementById(m_sFfUfMipIsBeingFinancedChanged)%>.value = 'False';
            <%=AspxTools.JsGetElementById(sProMInsMb)%>.readOnly = fhaLoan;
            <%=AspxTools.JsGetElementById(sUfCashPdLckd)%>.disabled = fhaLoan || <%=AspxTools.JsGetElementById(sFfUfMipIsBeingFinanced_1)%>.checked;
            <%=AspxTools.JsGetElementById(sMipPiaMon)%>.readOnly = !<%=AspxTools.JsGetElementById(sMipFrequency_1)%>.checked;
            lockField(<%=AspxTools.JsGetElementById(sFfUfmip1003Lckd)%>, 'sFfUfmip1003');
            lockField(<%=AspxTools.JsGetElementById(sUfCashPdLckd)%>, 'sUfCashPd');
            lockField(<%=AspxTools.JsGetElementById(sProMInsLckd_mip)%>, 'sProMIns_mip');
            lockField(<%=AspxTools.JsGetElementById(sLenderUfmipLckd)%>, 'sLenderUfmip');
            disableEnableCompanyNm();
        }

        function isFHALoan() {
            return <%=AspxTools.JsGetElementById(sLT)%>.value == <%=AspxTools.JsString(DataAccess.E_sLT.FHA)%>;
        }

        function isVALoan() {
            return <%=AspxTools.JsGetElementById(sLT)%>.value == <%=AspxTools.JsString(DataAccess.E_sLT.VA)%>;
        }

        function isUSDALoan() {
            return <%=AspxTools.JsGetElementById(sLT)%>.value == <%=AspxTools.JsString(DataAccess.E_sLT.UsdaRural)%>;
        }

        function onChange_sLT() {
            if (isFHALoan()) {
                bindsProMInsT(MipCalcTypeFha_map);
            }
            else {
                bindsProMInsT(MipCalcTypeNonFha_map);
            }

            updateCompanyNm();
            refreshCalculation();
        }

        function updateCompanyNm() {
            var sMiCompanyNmT = <%=AspxTools.JsGetElementById(sMiCompanyNmT)%>;
            sMiCompanyNmT.disabled = false;

            for(var i = 0; i < sMiCompanyNmT.length; i++) {
                if(sMiCompanyNmT[i].value == <%=AspxTools.JsString(DataAccess.E_sMiCompanyNmT.FHA)%>
                    || sMiCompanyNmT[i].value == <%=AspxTools.JsString(DataAccess.E_sMiCompanyNmT.VA)%>
                    || sMiCompanyNmT[i].value == <%=AspxTools.JsString(DataAccess.E_sMiCompanyNmT.USDA)%>) {
                    sMiCompanyNmT.removeChild(sMiCompanyNmT[i]);
                    i--;
                }
            }

            if (isFHALoan()) {
                var child = document.createElement('option');
                child.value = <%=AspxTools.JsString(DataAccess.E_sMiCompanyNmT.FHA)%>;
                child.innerText = 'FHA';
                sMiCompanyNmT.appendChild(child);
            }

            if (isVALoan()) {
                var child = document.createElement('option');
                child.value = <%=AspxTools.JsString(DataAccess.E_sMiCompanyNmT.VA)%>;
                child.innerText = 'VA';
                sMiCompanyNmT.appendChild(child);
            }

            if (isUSDALoan()) {
                var child = document.createElement('option');
                child.value = <%=AspxTools.JsString(DataAccess.E_sMiCompanyNmT.USDA)%>;
                child.innerText = 'USDA';
                sMiCompanyNmT.appendChild(child);
            }

            disableEnableCompanyNm();
        }

        function disableEnableCompanyNm() {
            var sMiCompanyNmT = <%=AspxTools.JsGetElementById(sMiCompanyNmT)%>;
            if (isFHALoan() || isVALoan() || isUSDALoan()) {
                sMiCompanyNmT.disabled = true;
            }
            sMiCompanyNmT.style.backgroundColor = 'white';
        }

        function bindsProMInsT(list) {
            var sProMInsT = <%=AspxTools.JsGetElementById(sProMInsT)%>;
            var sProMInsTValue = sProMInsT.value;

            while(sProMInsT.hasChildNodes())
                sProMInsT.removeChild(sProMInsT.firstChild);

            for(var i = 0; i < list.length; i++){
                var child = document.createElement('option');
                child.value = list[i].Key;
                child.innerText = list[i].Value;
                if (child.value == sProMInsTValue)
                    child.setAttribute('selected', 'selected');
                sProMInsT.appendChild(child);
            }
        }

        function sFfUfMipIsBeingFinancedChanged() {
            if(m_sFfUfMipIsBeingFinancedRecentValue != <%=AspxTools.JsGetElementById(sFfUfMipIsBeingFinanced_0)%>.checked)
                <%=AspxTools.JsGetElementById(m_sFfUfMipIsBeingFinancedChanged)%>.value = 'True';
            refreshCalculation();
        }
    </script>

    <script id="footerJS" type="text/javascript">
        var bFirst = true;
        function _initFooter()
        {
            initComboboxes();

            if (typeof _initControl === 'function')
            {
                _initControl();
            }
        }

        function initComboboxes()
        {
            var $wrapper = $("#wrapper");
            var $footerCBs = $('.combobox').filter("[id^='footer_']");
            var $nonFooterCBs = $('.combobox').filter(":not([id^='footer_'])");

            // Set combobox item lists in footer to fixed position when running in IE 10 or
            // rendering in standards mode (not Quirks mode) on IE9 or below
            if (document.documentMode >= 8)
            {
                $footerCBs.css("position", "fixed");
            }
            else
            {
                $nonFooterCBs.appendTo($wrapper);
            }

            // Add blank element to top of drop down
            if(bFirst === true)
            {
                $nonFooterCBs.each(function()
                {
                    var oRow = this.insertRow(0);
                    oRow.onmouseover = function() { selectItem(this.rowIndex); };
                    oRow.onclick = function() { onItemClick(this); };

                    var oCell = oRow.insertCell();
                    oCell.style.width = "100%";

                    oCell.innerHTML = "&zwnj;"; // zero-width non-joiner. Invisible to browser. Unlikely input.
                });
                bFirst = false;
            }
        }

        function displayCalendar(field)
        {
            var el = document.getElementById(field);
            if (el.readOnly || el.disabled)
                return false;

            if (gCalendar != null)
            {
                gCalendar.hide();
            } else
            {
                var cal = new Calendar(false, null, selectedDate, closeCalHandler);
                gCalendar = cal;
                cal.setRange(1900, 2070);
                cal.setDateFormat('mm/dd/y');
                cal.create();

                // For IE6 and Quirks mode, the parent of the calendar needs to be wrapper
                // to scroll with the page to accomodate for the position:fixed workarounds.
                if (document.compatMode !== 'CSS1Compat' || navigator.appVersion.indexOf("MSIE 6.") != -1)
                {
                    document.getElementById("wrapper").appendChild(cal.element);
                }
            }
            gCalendar.sel = el;
            var dateVal = gCalendar.sel.value;
            if (dateVal != '')
            {
                gCalendar.sel.blur();
                gCalendar._init(false, new Date(dateVal));
            }
            gCalendar.showAtElement(el);
            return false;
        }

        function _initControl() {
            lockFields();
            var sLPurposeTValue = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value;
	        var isConstruction = sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Construct.ToString("D")) %>
						        || sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.ConstructPerm.ToString("D")) %>;
            var bPurchase = (sLPurposeTValue == <%= AspxTools.JsString(DataAccess.E_sLPurposeT.Purchase) %>
					        || (isConstruction && (!ML.AreConstructionLoanCalcsMigrated || ML.IsConstructionAndLotPurchase)));

            if (!ML.isUlad2019) {
                <%= AspxTools.JsGetElementById(sLAmtLckd_foot) %>.disabled = !bPurchase;
            }
        }

        function lockFields()
        {
            lockFieldByElement(<%= AspxTools.JsGetElementById(sEstCloseDLckd) %>, <%= AspxTools.JsGetElementById(sEstCloseD) %>);
            lockFieldByElement(<%= AspxTools.JsGetElementById(aBPreferredNmLckd) %>, <%= AspxTools.JsGetElementById(aBPreferredNm) %>);
            lockFieldByElement(<%= AspxTools.JsGetElementById(aCPreferredNmLckd) %>, <%= AspxTools.JsGetElementById(aCPreferredNm) %>);

            if (!ML.isUlad2019) {
                lockFieldByElement(<%= AspxTools.JsGetElementById(sTransNetCashLckd) %>, <%= AspxTools.JsGetElementById(sTransNetCash) %>);
                lockFieldByElement(<%= AspxTools.JsGetElementById(sOCredit1Lckd) %>, <%= AspxTools.JsGetElementById(footer_sOCredit1Desc) %>);
                lockFieldByElement(<%= AspxTools.JsGetElementById(sOCredit1Lckd) %>, <%= AspxTools.JsGetElementById(sOCredit1Amt) %>);
                lockFieldByElement(<%= AspxTools.JsGetElementById(sTotCcPbsLocked) %>, <%= AspxTools.JsGetElementById(sTotCcPbs) %>);
                lockFieldByElement(<%= AspxTools.JsGetElementById(sLDiscnt1003Lckd) %>, <%= AspxTools.JsGetElementById(sLDiscnt1003) %>);
                lockFieldByElement(<%= AspxTools.JsGetElementById(sTotEstCc1003Lckd) %>, <%= AspxTools.JsGetElementById(sTotEstCcNoDiscnt1003) %>);
                lockFieldByElement(<%= AspxTools.JsGetElementById(sTotEstPp1003Lckd) %>, <%= AspxTools.JsGetElementById(sTotEstPp1003) %>);
                lockFieldByElement(<%= AspxTools.JsGetElementById(sRefPdOffAmt1003Lckd) %>, <%= AspxTools.JsGetElementById(sRefPdOffAmt1003) %>);
                lockFieldByElement(<%= AspxTools.JsGetElementById(sLAmtLckd_foot) %>, <%= AspxTools.JsGetElementById(sLAmtCalc_foot) %>);
                lockFieldByElement(<%= AspxTools.JsGetElementById(sFfUfmip1003Lckd_foot) %>, <%= AspxTools.JsGetElementById(sFfUfmip1003_foot) %>);
                lockFieldByElement(<%= AspxTools.JsGetElementById(sAltCostLckd) %>, <%= AspxTools.JsGetElementById(sAltCost) %>);
            }
        }

        function lockFieldByElement(cb, tb) {
            tb.readOnly = !cb.checked;

            if (!cb.checked)
                tb.style.backgroundColor = "lightgrey";
            else
                tb.style.backgroundColor = "";
        }
    </script>

    <form id="form1" runat="server">
        <QRP:QualRatePopup ID="QualRatePopup" runat="server" />
        <div id="wrapper">
            <div class="popup" id="topPop" style="display: none;">
                <table style="border-collapse: collapse; margin: auto;">
                    <tr>
                        <td>
                            Total Primary Housing Expense
                        </td>
                        <td>
                            <ml:MoneyTextBox runat="server" ID="sTransmProTotHExp_pop" preset="money" ReadOnly="true"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr style="border-bottom: solid 1px; margin-bottom: 2px;">
                        <td>
                            Total monthly income of all borrowers
                        </td>
                        <td>
                            <ml:MoneyTextBox runat="server" ID="sLTotI_pop2" preset="money" ReadOnly='true'></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Top Ratio
                        </td>
                        <td>
                            <ml:PercentTextBox Width="90px" runat="server" ID="sQualTopR_pop" preset="percent"
                                ReadOnly="true"></ml:PercentTextBox>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            Note: Top Ratio is Calculated as
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            [Total Primary Housing Expense / Total Monthly Income]
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <input type="button" class="popupClose" value="Close" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="popup" id="botPop" style="display: none;">
                <table style="border-collapse: collapse; margin: auto;">
                    <tr>
                        <td>
                            PITI
                        </td>
                        <td>
                            <ml:MoneyTextBox runat="server" ID="sMonthlyPmt_pop" preset="money" ReadOnly="true"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Present Housing Expense
                        </td>
                        <td>
                            <ml:MoneyTextBox runat="server" ID="sPresLTotPersistentHExp_pop" preset="money" ReadOnly="true"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Liabilities
                        </td>
                        <td>
                            <ml:MoneyTextBox runat="server" ID="sLiaMonLTot_pop" preset="money" ReadOnly="true"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr style="border-bottom: solid 1px; margin-bottom: 2px;">
                        <td>
                            Total Monthly Income of All Borrowers
                        </td>
                        <td>
                            <ml:MoneyTextBox runat="server" ID="sLTotI_pop" preset="money" ReadOnly="true"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Bottom Ratio
                        </td>
                        <td>
                            <ml:PercentTextBox Width="90px" runat="server" ID="sQualBottomR_pop" preset="percent"
                                ReadOnly="true"></ml:PercentTextBox>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            Note: Bottom Ratio is calculated as
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            [(PITI + Liabilities + Pres House Exp.)/Total Monthly Income]
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <input type="button" class="popupClose" value="Close" />
                        </td>
                    </tr>
                </table>
            </div>
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td colspan="2" class="MainRightHeader" nowrap>
                        <%= AspxTools.HtmlString(HeaderDisplay)%>
                    </td>
                </tr>
                <tr id="BorrowerInfoRow">
                    <asp:HiddenField ID="aBAlias" runat="server" />
                    <asp:HiddenField ID="aBPowerOfAttorneyNm" runat="server" />
                    <asp:HiddenField ID="aCAlias" runat="server" />
                    <asp:HiddenField ID="aCPowerOfAttorneyNm" runat="server" />
                    <td colspan="2" nowrap style="padding-left: 5px;">
                        <table class="FormTable" id="BorrowerInfoTable" cellspacing="2" cellpadding="0" border="0">
                            <tr>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0" class="InsetBorder">
                                        <tr>
                                            <td>
                                                <table id="Table14" cellspacing="0" cellpadding="0" border="0">
                                                    <tr style="height: 21px">
                                                        <td class="FormTableSubHeader" colspan="2">
                                                            Borrower Information
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            First Name
                                                        </td>
                                                        <td class="FieldLabelNoPadding" nowrap>
                                                            <asp:TextBox ID="aBFirstNm" runat="server" MaxLength="21" Width="127px" onchange="updateBorrowerName();"></asp:TextBox>&nbsp;Middle
                                                            Name<asp:TextBox ID="aBMidNm" MaxLength="21" runat="server" Width="70px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            Preferred Name
                                                        </td>
                                                        <td class="FieldLabelNoPadding" nowrap colspan="3">
                                                            <asp:TextBox ID="aBPreferredNm" runat="server" MaxLength="21" Width="127px"></asp:TextBox>&nbsp;
                                                            <asp:CheckBox ID="aBPreferredNmLckd" runat="server" onclick="lockFields();refreshCalculation();" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            Last Name
                                                        </td>
                                                        <td class="FieldLabelNoPadding">
                                                            <asp:TextBox ID="aBLastNm" runat="server" MaxLength="21" Width="127px" onchange="updateBorrowerName();"></asp:TextBox>&nbsp;Suffix
                                                            <ml:ComboBox ID="aBSuffix" runat="server" Width="30px"></ml:ComboBox>
                                                            <a href="#" onclick="onEditBorrAliasesClick()">Edit Aliases/POA</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            SSN
                                                        </td>
                                                        <td class="FieldLabelNoPadding">
                                                            <ml:SSNTextBox ID="aBSsn" runat="server" Width="90" preset="ssn"></ml:SSNTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel" title="(MM/DD/YYYY)">
                                                            DOB
                                                        </td>
                                                        <td class="FieldLabelNoPadding">
                                                            <ml:DateTextBox ID="aBDob" runat="server" preset="date" Width="75"></ml:DateTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            Home Phone
                                                        </td>
                                                        <td class="FieldLabelNoPadding" nowrap>
                                                            <ml:PhoneTextBox ID="aBHPhone" runat="server" preset="phone" Width="120"></ml:PhoneTextBox>&nbsp;Cellphone&nbsp;<ml:PhoneTextBox
                                                                ID="aBCellphone" Width="120" runat="server" preset="phone"></ml:PhoneTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            Work Phone
                                                        </td>
                                                        <td class="FieldLabel">
                                                            <ml:PhoneTextBox ID="aBBusPhone" runat="server" preset="phone" Width="120"></ml:PhoneTextBox>&nbsp;Fax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                                            <ml:PhoneTextBox ID="aBFax" runat="server" Width="120" preset="phone"></ml:PhoneTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            Email
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="aBEmail" runat="server"></asp:TextBox><asp:RegularExpressionValidator
                                                                ID="RegularExpressionValidator1" runat="server" ControlToValidate="aBEmail" Display="Dynamic"
                                                                ErrorMessage=" Invalid Email"></asp:RegularExpressionValidator>&nbsp;<a onclick="window.open('mailto:' + document.getElementById(<%= AspxTools.JsString(aBEmail.ClientID) %>).value);"
                                                                    href="#" tabindex="-1">send email</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel" colspan="2">
                                                            <table id="Table6" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td class="FormTableSubHeader" colspan="2">
                                                                        Present Address
                                                                    </td>
                                                                    <td class="FormTableSubHeader" align="right" colspan="4">
                                                                        <input onclick="fillPresentAddress();" type="button" value="Copy from property address"
                                                                            style="width: 180px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabelNoPadding">
                                                                        Street
                                                                    </td>
                                                                    <td class="FieldLabel">
                                                                        City
                                                                    </td>
                                                                    <td class="FieldLabel">
                                                                        ST
                                                                    </td>
                                                                    <td class="FieldLabel">
                                                                        Zip
                                                                    </td>
                                                                    <td class="FieldLabel">
                                                                        Own/Rent
                                                                    </td>
                                                                    <td class="FieldLabel">
                                                                        # Yrs
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="aBAddr" runat="server" MaxLength="36" onchange="syncMailingAddress();enableVorBtn(true);"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="aBCity" runat="server" Width="92px" onchange="syncMailingAddress();enableVorBtn(true);"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <ml:StateDropDownList ID="aBState" runat="server" onchange="syncMailingAddress();enableVorBtn(true);">
                                                                        </ml:StateDropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <ml:ZipcodeTextBox ID="aBZip" runat="server" Width="50" preset="zipcode" onchange="syncMailingAddress();enableVorBtn(true);"></ml:ZipcodeTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="aBAddrT" runat="server" Width="59px" onchange="enableVorBtn(true);">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="aBAddrYrs" runat="server" MaxLength="3" Width="34px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabelNoPadding">
                                                                        Mailing Address
                                                                    </td>
                                                                    <td align="left" colspan="5" class="FieldLabel">
                                                                        <asp:DropDownList ID="aBAddrMailSourceT" runat="server" onchange="syncMailingAddress();" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabelNoPadding">
                                                                        <asp:TextBox ID="aBAddrMail" MaxLength="36" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="aBCityMail" runat="server" Width="92px"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <ml:StateDropDownList ID="aBStateMail" runat="server"></ml:StateDropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <ml:ZipcodeTextBox ID="aBZipMail" Width="50" runat="server" preset="zipcode"></ml:ZipcodeTextBox>
                                                                    </td>
                                                                    <td><input type="button" onclick="EditVOR(true);" id="EditVORB" value="Edit VOR" />
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0" class="insetborder">
                                        <tr>
                                            <td>
                                                <table id="Table5" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td class="FieldLabel" colspan="2">
                                                            <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td class="FormTableSubHeader">
                                                                        Co-borrower Information
                                                                    </td>
                                                                    <td class="FormTableSubHeader" align="right">
                                                                        <input onclick="onCopyBorrowerClick();" type="button" value="Copy from borrower">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            First Name
                                                        </td>
                                                        <td class="FieldLabelNoPadding" nowrap>
                                                            <asp:TextBox ID="aCFirstNm" runat="server" MaxLength="21" Width="127px" onchange="coBorrowerPreferredNameCalc();"></asp:TextBox>&nbsp;Middle
                                                            Name<asp:TextBox ID="aCMidNm" MaxLength="21" runat="server" Width="70px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            Preferred Name
                                                        </td>
                                                        <td class="FieldLabelNoPadding" nowrap colspan="3">
                                                            <asp:TextBox ID="aCPreferredNm" runat="server" MaxLength="21" Width="127px"></asp:TextBox>&nbsp;
                                                            <asp:CheckBox ID="aCPreferredNmLckd" runat="server" onclick="lockFields();refreshCalculation();" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            Last Name
                                                        </td>
                                                        <td nowrap class="FieldLabelNoPadding">
                                                            <asp:TextBox ID="aCLastNm" runat="server" MaxLength="21" Width="127px"></asp:TextBox>&nbsp;Suffix
                                                            <ml:ComboBox ID="aCSuffix" runat="server" Width="30px"></ml:ComboBox>
                                                            <a href="#" onclick="onEditCoborrAliasesClick()">Edit Aliases/POA</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            SSN
                                                        </td>
                                                        <td class="FieldLabelNoPadding">
                                                            <ml:SSNTextBox ID="aCSsn" runat="server" Width="90" preset="ssn"></ml:SSNTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel" title="(MM/DD/YYYY)">
                                                            DOB
                                                        </td>
                                                        <td class="FieldLabelNoPadding">
                                                            <ml:DateTextBox ID="aCDob" runat="server" preset="date" Width="75"></ml:DateTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            Home Phone
                                                        </td>
                                                        <td class="FieldLabelNoPadding" nowrap>
                                                            <ml:PhoneTextBox ID="aCHPhone" runat="server" Width="120" preset="phone"></ml:PhoneTextBox>&nbsp;Cellphone
                                                            <ml:PhoneTextBox ID="aCCellphone" Width="120" runat="server" preset="phone"></ml:PhoneTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            Work Phone
                                                        </td>
                                                        <td class="FieldLabel">
                                                            <ml:PhoneTextBox ID="aCBusPhone" runat="server" Width="120" preset="phone"></ml:PhoneTextBox>&nbsp;Fax&nbsp;&nbsp;&nbsp;
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                                            <ml:PhoneTextBox ID="aCFax" runat="server" Width="120" preset="phone"></ml:PhoneTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            Email
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="aCEmail" runat="server"></asp:TextBox>&nbsp;<a onclick="window.open('mailto:' + document.getElementById(<%= AspxTools.JsString(aCEmail.ClientID) %>).value);"
                                                                href="#" tabindex="-1">send email</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FormTableSubHeader" nowrap colspan="2" style="height: 21px">
                                                            Present Address
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel" colspan="2">
                                                            <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                <tr>
                                                                    <td class="FieldLabelNoPadding">
                                                                        Street
                                                                    </td>
                                                                    <td class="FieldLabel">
                                                                        City
                                                                    </td>
                                                                    <td class="FieldLabel">
                                                                        ST
                                                                    </td>
                                                                    <td class="FieldLabel">
                                                                        Zip
                                                                    </td>
                                                                    <td class="FieldLabel">
                                                                        Own/Rent
                                                                    </td>
                                                                    <td class="FieldLabel">
                                                                        # Yrs
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="aCAddr" runat="server" MaxLength="36" onchange="syncMailingAddress();enableVorBtn(false);"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="aCCity" runat="server" Width="92px" onchange="syncMailingAddress();enableVorBtn(false);"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <ml:StateDropDownList ID="aCState" runat="server" onchange="syncMailingAddress();enableVorBtn(false);">
                                                                        </ml:StateDropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <ml:ZipcodeTextBox ID="aCZip" runat="server" Width="50" preset="zipcode" onchange="syncMailingAddress();enableVorBtn(false);"></ml:ZipcodeTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="aCAddrT" runat="server" Width="59px" onchange="enableVorBtn(false);">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="aCAddrYrs" runat="server" MaxLength="3" Width="34px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabelNoPadding">
                                                                        Mailing Address
                                                                    </td>
                                                                    <td colspan="5" class="FieldLabel">
                                                                        <asp:DropDownList ID="aCAddrMailSourceT" runat="server" onchange="syncMailingAddress();" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="FieldLabelNoPadding">
                                                                        <asp:TextBox ID="aCAddrMail" MaxLength="36" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="aCCityMail" runat="server" Width="92px" />
                                                                    </td>
                                                                    <td>
                                                                        <ml:StateDropDownList ID="aCStateMail" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <ml:ZipcodeTextBox ID="aCZipMail" Width="50" runat="server" preset="zipcode" />
                                                                    </td>
                                                                    <td><input type="button" id="EditVORC" onclick="EditVOR(false);" value="Edit VOR" />
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="LoanInfoRow">
                    <td colspan="2" nowrap style="padding-left: 5px;">
                        <%--OPM 169478 : only show for brokers with CRMNow Integration--%>
                        <% if (!m_showCRMID) { %>
                        <style type="text/css">
                            .HideUnlessCRMNowEnabled
                            {
                                display: none;
                            }
                        </style>
                        <% } %>
                        <table id="Table2" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <tr>
                                    <td nowrap>
                                        <table class="InsetBorder" id="Table2" cellspacing="0" cellpadding="0" width="99%" border="0">
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    Loan Program
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="sLpTemplateNm" Style="padding-left: 4px" TabIndex="1" Width="249px" runat="server"></asp:TextBox>
                                                    <% if (m_isPmlEnabled) { %>
                                                    <input class="ButtonStyle" runat="server" skipme="SkipMe" alwaysenable="AlwaysEnable"
                                                        id="btnRunLPE" style="width: 80px" onclick="f_runLPE();" tabindex="1" type="button"
                                                        value="Run PML" />&nbsp;&nbsp;
                                                    <% } %>
                                                    <input class="ButtonStyle" id="btnSelectProgram" style="width: 110px" onclick="f_runNonLPE();"
                                                        type="button" value="Select Program" />
                                                </td>
                                            </tr>
                                            <tr id="sLoanProductIdentifierRow" runat="server">
                                                <td class="FieldLabel" nowrap>
                                                    Loan Product Identifier
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="sLoanProductIdentifier" Style="padding-left: 4px" Width="249px" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    Closing Cost Template
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="sCcTemplateNm" Style="padding-left: 4px" TabIndex="1" Width="249px" runat="server"></asp:TextBox>
                                                    <input class="ButtonStyle" style="width: 210px" onclick="findCCTemplate();" tabindex="1"
                                                        type="button" value="Find Closing Cost Template ..." />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    Loan Number
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="sLNm" Style="padding-left: 4px" TabIndex="1" Width="249px" runat="server"></asp:TextBox>
                                                    <input class="ButtonStyle" id="btnGenerateNewLoanNumber" style="width: 160px" onclick="if( !parentElement.disabled ) generateNewLoanName();"
                                                        tabindex="1" type="button" value="Generate new number">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:CustomValidator ID="sLNm_Validator" runat="server" ErrorMessage="Name Taken!"
                                                        Display="Dynamic" Font-Bold="True"></asp:CustomValidator>
                                                    <asp:RequiredFieldValidator ID="sLNm_RequiredValidator" runat="server" Font-Bold="True"
                                                        ControlToValidate="sLNm" Display="Dynamic" ErrorMessage=" * Loan number cannot be blank"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    Loan Reference Number
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="sLRefNm" Style="padding-left: 4px" TabIndex="1" Width="249px" runat="server" Enabled="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    <span class="HideUnlessCRMNowEnabled">CRM ID</span>
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="sCrmNowLeadId" Style="padding-left: 4px" TabIndex="1" Width="249px"
                                                        runat="server" CssClass="HideUnlessCRMNowEnabled"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        <table class="InsetBorder" id="Table9" cellspacing="0" cellpadding="0" width="99%" border="0">
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    Loan Purpose
                                                </td>
                                                <td nowrap>
                                                    <asp:DropDownList ID="sLPurposeT" TabIndex="1" Width="130px" runat="server" onchange="refreshCalculation();">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    Lien Position
                                                </td>
                                                <td nowrap>
                                                    <asp:DropDownList ID="sLienPosT" TabIndex="1" Width="90px" runat="server" onchange="refreshCalculation();">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    Loan Type
                                                </td>
                                                <td nowrap>
                                                    <asp:DropDownList ID="sLT" TabIndex="1" Width="120px" runat="server" onchange="onChange_sLT();refreshCalculation();">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="sIsStudentLoanCashoutRefiRow">
                                                <td class="FieldLabel" nowrap>
                                                    Student Loan Cashout?
                                                </td>
                                                <td nowrap>
                                                    <asp:CheckBox ID="sIsStudentLoanCashoutRefi" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    Subj Prop Occ.
                                                </td>
                                                <td nowrap>
                                                    <asp:DropDownList ID="aOccT" TabIndex="1" Width="130px" runat="server" onchange="refreshCalculation();">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    Amort. Type
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="sFinMethT" TabIndex="1" Width="90px" runat="server" onchange="refreshCalculation();">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    Amort. Desc
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="sFinMethDesc" TabIndex="1" Width="120px" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="ConstructionPurposeData">
                                    <td nowrap>
                                        <table class="InsetBorder" id="Table10" cellspacing="0" cellpadding="0" width="99%"
                                            border="0">
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    Construction Period (mths)
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="sConstructionPeriodMon" runat="server" Width="51px" onchange="refreshCalculation();"></asp:TextBox>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    Construction Improvements Cost
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sConstructionImprovementAmt" runat="server" Width="90" preset="money"
                                                        onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    Construction Period Interest Rate
                                                </td>
                                                <td nowrap>
                                                    <ml:PercentTextBox ID="sConstructionPeriodIR" runat="server" Width="70" preset="percent"
                                                        onchange="refreshCalculation();"></ml:PercentTextBox>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    Land (if acquired separately)
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sLandCost" runat="server" Width="90" preset="money" onchange="valueSync(this,'sLandCost_foot');refreshCalculation();"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        <table class="InsetBorder" id="Table11" cellspacing="0" cellpadding="0" width="99%"
                                            border="0">
                                            <tr>
                                                <td class="FieldLabel w-174" nowrap>
                                                    Purchase Price
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sPurchPrice" TabIndex="2" Width="90" runat="server" onchange="valueSync(this,'sPurchPrice_foot');refreshCalculation();"
                                                        preset="money"></ml:MoneyTextBox>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    <a title="Go to Upfront MIP/FF screen" tabindex="-1" href="javascript:linkMe('LoanInfo.aspx?pg=1');">
                                                        Upfront MIP / FF</a>
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sFfUfmipFinanced" TabIndex="1" Width="90" runat="server" preset="money"
                                                        ReadOnly="True"></ml:MoneyTextBox>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    <a tabindex="-1" onclick="redirectToUladPage('MonthlyIncome');">Monthly Income</a>
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sLTotI" TabIndex="1" Width="90" runat="server" preset="money"
                                                        ReadOnly="True"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr id="inducedPurchPriceRow">
                                                <td class="FieldLabel w-174">
                                                    Inducements to Purchase Price
                                                </td>
                                                <td>
                                                    <ml:MoneyTextBox ID="sInducementPurchPrice" runat="server"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel w-174" nowrap id="appraisedValueLabel">
                                                    Appraised Value
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sApprVal" TabIndex="2" Width="90" runat="server" onchange="refreshCalculation();"
                                                        preset="money" designtimedragdrop="247"></ml:MoneyTextBox>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    Total Loan Amt
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sFinalLAmt" TabIndex="1" Width="90" runat="server" preset="money"
                                                        ReadOnly="True"></ml:MoneyTextBox>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    <a tabindex="-1" onclick="redirectToUladPage('Liabilities');">Liabilities</a>
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sLiaMonLTot" TabIndex="1" Width="90" runat="server" preset="money"
                                                        ReadOnly="True"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr id="asIsValueRow">
                                                <td class="FieldLabel w-174">
                                                    As-Is Value
                                                </td>
                                                <td>
                                                    <ml:MoneyTextBox ID="sFHASpAsIsVal" runat="server"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel w-174" id="sDownOrEquityPcLabel" runat="server" nowrap title="Includes Other Financing (if any)">
                                                    Down Payment (%)
                                                </td>
                                                <td nowrap title="Includes Other Financing (if any)">
                                                    <ml:PercentTextBox ID="sDownPmtPc" TabIndex="2" Width="70" runat="server" onchange="on_sDownPmtPc_change(event);"
                                                        preset="percent"></ml:PercentTextBox>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    Note Rate
                                                </td>
                                                <td nowrap>
                                                    <ml:PercentTextBox ID="sNoteIR" TabIndex="5" Width="70" runat="server" onchange="refreshCalculation();"
                                                        preset="percent"></ml:PercentTextBox>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    <a href="javascript:linkMe('BorrowerInfo.aspx?pg=7');" title="Present Housing Expense Included in Ratios">
                                                        Pres House Exp.</a>
                                                </td>
                                                <td nowrap title="Amount included in ratios">
                                                    <ml:MoneyTextBox ID="sPresLTotPersistentHExp" TabIndex="1" Width="90" runat="server"
                                                        preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel w-174" id="sDownOrEquityLabel" runat="server" nowrap title="Includes Other Financing (if any)">
                                                    Down Payment
                                                </td>
                                                <td nowrap title="Includes Other Financing (if any)">
                                                    <ml:MoneyTextBox ID="sEquityCalc" TabIndex="2" Width="90" runat="server" onchange="refreshCalculation();"
                                                        preset="money"></ml:MoneyTextBox>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    Term / Due (mths)
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="sTerm" TabIndex="5" Width="37px" runat="server" onchange="refreshCalculation();"
                                                        MaxLength="3"></asp:TextBox>/<asp:TextBox ID="sDue" TabIndex="5" Width="37px" runat="server"
                                                            onchange="refreshCalculation();" MaxLength="3"></asp:TextBox>
                                                </td>
                                                <td valign="top" nowrap colspan="2" rowspan="2">
                                                    <table id="Table12" cellspacing="0" cellpadding="0" border="0">
                                                        <tr>
                                                            <td class="FieldLabel" nowrap>
                                                                <a href="javascript:void(0);" id="topPopClick">Top</a>
                                                            </td>
                                                            <td nowrap>
                                                                <asp:TextBox ID="sQualTopR" TabIndex="1" runat="server" Width="56px" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td class="FieldLabel" nowrap>
                                                                &nbsp;LTV
                                                            </td>
                                                            <td nowrap>
                                                                <asp:TextBox ID="sLtvR" TabIndex="1" runat="server" Width="56px" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="FieldLabel" nowrap>
                                                                <a href="javascript:void(0);" id="botPopClick">Bottom&nbsp;</a>
                                                            </td>
                                                            <td nowrap>
                                                                <asp:TextBox ID="sQualBottomR" TabIndex="1" Width="56px" runat="server" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                            <td class="FieldLabel" nowrap>
                                                                &nbsp;CLTV&nbsp;
                                                            </td>
                                                            <td nowrap>
                                                                <asp:TextBox ID="sCltvR" TabIndex="1" runat="server" Width="56px" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel w-174" nowrap>
                                                    Loan Amt
                                                    <asp:CheckBox ID="sLAmtLckd" onclick="checkedSync(this,'sLAmtLckd_foot');refreshCalculation();" runat="server" Text="Lock">
                                                    </asp:CheckBox>
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sLAmtCalc" TabIndex="2" runat="server" Width="90" onchange="valueSync(this, 'sLAmtCalc_foot');refreshCalculation();"
                                                        preset="money"></ml:MoneyTextBox>
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    <a id="QualRateCalcLink">Qual Rate</a>
                                                </td>
                                                <td nowrap>
                                                    <ml:PercentTextBox ID="sQualIR" TabIndex="5" runat="server" Width="70" onchange="refreshCalculation();" preset="percent"></ml:PercentTextBox>
                                                    <asp:CheckBox ID="sQualIRLckd" runat="server" onchange="refreshCalculation()" Text="Lock" onclick="refreshCalculation();" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel w-174">
                                                    <span class="RenovationContent">Is Renovation Loan</span>
                                                </td>
                                                <td id="sIsRenovationLoanCell">
                                                    <span class="RenovationContent">
                                                        <asp:CheckBox id="sIsRenovationLoan" runat="server" onclick="refreshCalculation();"/>
                                                    </span>
                                                </td>

                                                <td class="FieldLabel">
                                                    Qual Term
                                                </td>
                                                <td colspan="4">
                                                    <asp:DropDownList id="sQualTermCalculationType" runat="server" onchange="refreshCalculation();" CssClass="sQualTermCalculationType" />
                                                    &nbsp
                                                    <input type="text" id="sQualTerm" runat="server" preset="numeric" onchange="refreshCalculation();" class="sQualTerm" />
                                                </td>
                                            </tr>
                                            <tr id="navigateToRenovRow">
                                                <td class="FieldLabel w-174">
                                                    <a id="navigateToRenovLink" href="#" onclick="return f_navigateToRenovLoanPage();">Total Renovation Costs</a>
                                                    <span id="plainRenovText">Total Renovation Costs</span>
                                                </td>
                                                <td>
                                                    <ml:MoneyTextBox runat="server" ID="sTotalRenovationCosts" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                    <asp:CheckBox ID="sTotalRenovationCostsLckd" runat="server" onchange="refreshCalculation()" Text="Lock" onclick="refreshCalculation();" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel w-174" nowrap>
                                                    <span class="HideUnlessFTFCU">Is Line of Credit</span>
                                                </td>
                                                <td nowrap>
                                                    <asp:CheckBox runat="server" ID="sIsLineOfCredit" onclick="f_onchange_sIsLineOfCredit( false );"
                                                        CssClass="HideUnlessFTFCU" />
                                                </td>
                                                <td class="FieldLabel" nowrap <% if(!m_showCreditLineFields) { %> rowspan="3" <% } %>>
                                                    Qual Rate I/O
                                                </td>
                                                <td <% if(!m_showCreditLineFields) { %> rowspan="3" <% } %>>
                                                    <asp:CheckBox runat="server" TabIndex="5" ID="sIsQRateIOnly" onclick="f_onchange_sQualIR( false );">
                                                    </asp:CheckBox>
                                                </td>
                                                <td class="FieldLabel" nowrap <% if(!m_showCreditLineFields) { %> rowspan="3" <% } %>>
                                                    Option ARM
                                                </td>
                                                <td class="FieldLabel" <% if(!m_showCreditLineFields) { %> rowspan="3" <% } %>>
                                                    <asp:CheckBox TabIndex="5" ID="sIsOptionArm" onclick="f_onchange_sIsOptionArm( false );"
                                                        runat="server"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr id="CreditLineAmtPanel" style="display: <%= AspxTools.HtmlString(m_lineAmtDisplay) %>;">
                                                <td class="FieldLabel w-174" nowrap>
                                                    Line Amt
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sCreditLineAmt" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel w-174" nowrap <% if(m_showCreditLineFields) { %> rowspan="2" <% } %>>
                                                    Subj Prop State
                                                </td>
                                                <td nowrap <% if(m_showCreditLineFields) { %> rowspan="2" <% } %>>
                                                    <ml:StateDropDownList ID="sSpState" TabIndex="2" runat="server" onchange="valueSync(this, 'sSpState_spi');refreshCalculation();"
                                                        IncludeTerritories="false"></ml:StateDropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td <% if(m_showCreditLineFields) { %> colspan="2" <% } else { %> colspan="4" <% } %>>
                                                </td>
                                                <td class="FieldLabel" nowrap <% if(!m_showCreditLineFields) { %> rowspan="2" <% } %>>
                                                    Teaser Rate
                                                </td>
                                                <td nowrap <% if(!m_showCreditLineFields) { %> rowspan="2" <% } %>>
                                                    <ml:PercentTextBox ID="sOptionArmTeaserR" TabIndex="5" Width="70" runat="server"
                                                        onchange="refreshCalculation();" preset="percent"></ml:PercentTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel w-174" nowrap>
                                                    Property Type
                                                </td>
                                                <td class="FieldLabel" nowrap colspan="3">
                                                    <asp:DropDownList ID="sGseSpT" TabIndex="2" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="HomeIsMhAdvantageRow">
                                                <td class="FieldLabel">Is home MH Advantage?</td>
                                                <td colspan="3">
                                                    <asp:DropDownList runat="server" ID="sHomeIsMhAdvantageTri"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <div id="FHALimitPanel" class="FieldLabel">
                                                        <a href="#" onclick="return f_fhaLimitedPage();">See FHA Mortgage Limits Page ...</a>
                                                        <span id="OriginalAppraisedValueSpan" class="FieldLabel">&nbsp;Original Appraised Value&nbsp;
                                                            <asp:TextBox ID="sOriginalAppraisedValue" runat="server" onchange="refreshCalculation();"
                                                                Width="90px" preset="money"></asp:TextBox>
                                                            <input type="hidden" runat="server" id="sFHAPurposeIsStreamlineRefiWithoutAppr" value="0" />
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel w-174" nowrap>
                                                    Higher-priced indicator
                                                </td>
                                                <td nowrap>
                                                    <asp:DropDownList runat="server" ID="sHighPricedMortgageT">
                                                    </asp:DropDownList>
                                                    <asp:CheckBox runat="server" ID="sHighPricedMortgageTLckd" onclick="refreshCalculation();"
                                                        Text="Lock" />
                                                </td>
                                                <td class="FieldLabel" nowrap>
                                                    Employee Loan
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="sIsEmployeeLoan" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel w-174" nowrap>
                                                    New Construction
                                                </td>
                                                <td>
                                                    <input type="checkbox" id="sIsNewConstruction" runat="server"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap>
                                        <table class="InsetBorder" id="Table13" cellspacing="0" cellpadding="0" width="99%"
                                            border="0">
                                            <tr>
                                                <td class="FieldLabel" nowrap colspan="2">
                                                    <a tabindex="10" runat="server" id="MortgagePILink" disableforlead>This Mortgage (P &amp; I)</a>
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td class="FieldLabel" nowrap align="right">
                                                    <% if (m_isInterestOnly)
                                                       { %>(* Interest Only)<% } %>
                                                </td>
                                                <td nowrap width="90%">
                                                    <ml:MoneyTextBox ID="sProThisMPmt" TabIndex="10" runat="server" Width="90" preset="money"
                                                        ReadOnly="True"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap colspan="2">
                                                    <a tabindex="10" href="javascript:linkMe('LoanInfo.aspx?pg=2');">Other Financing (P
                                                        &amp; I)</a>
                                                </td>
                                                <td class="FieldLabel" nowrap align="right" colspan="2">
                                                    <ml:EncodedLiteral ID="OtherFinancingDescription" runat="server"></ml:EncodedLiteral>
                                                </td>
                                                <td class="FieldLabel" nowrap align="right">
                                                    <ml:EncodedLiteral ID="sIsIOnlyForSubFin" runat="server" Text="(* Interest Only)"></ml:EncodedLiteral>
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sProOFinPmt" TabIndex="10" runat="server" Width="90" preset="money"
                                                        ReadOnly="True"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap>Hazard Ins</td>
                                                <td nowrap>
                                                    <asp:PlaceHolder ID="sProHazInsTHolder" runat="server">
                                                        <asp:DropDownList ID="sProHazInsT" TabIndex="10" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                                                    </asp:PlaceHolder>
                                                </td>
                                                <td nowrap>
                                                    <asp:PlaceHolder ID="sProHazInsBaseAmtHolder" runat="server">
                                                        (<ml:MoneyTextBox ID="sProHazInsBaseAmt" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                    </asp:PlaceHolder>
                                                </td>
                                                <td nowrap>
                                                    <asp:PlaceHolder ID="sProHazInsRHolder" runat="server">
                                                        x<ml:PercentTextBox ID="sProHazInsR" TabIndex="10" runat="server" Width="70" onchange="refreshCalculation();" preset="percent"></ml:PercentTextBox>&nbsp;)
                                                    </asp:PlaceHolder>
                                                </td>
                                                <td nowrap>
                                                    <asp:PlaceHolder ID="sProHazInsMbHolder" runat="server">
                                                        +<ml:MoneyTextBox ID="sProHazInsMb" TabIndex="10" runat="server" Width="90" onchange="refreshCalculation();" preset="money" decimalDigits="4"></ml:MoneyTextBox>&nbsp;=
                                                    </asp:PlaceHolder>
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sProHazIns" TabIndex="2" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap>Property Taxes</td>
                                                <td nowrap>
                                                    <asp:PlaceHolder runat="server" ID="sProRealETxTHolder">
                                                        <asp:DropDownList ID="sProRealETxT" TabIndex="10" runat="server" onchange="refreshCalculation();"></asp:DropDownList>
                                                    </asp:PlaceHolder>
                                                </td>
                                                <td nowrap>
                                                    <asp:PlaceHolder runat="server" ID="sProRealETxBaseAmtHolder">
                                                        (<ml:MoneyTextBox ID="sProRealETxBaseAmt" TabIndex="10" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                    </asp:PlaceHolder>
                                                </td>
                                                <td nowrap>
                                                    <asp:PlaceHolder runat="server" ID="sProRealETxRHolder">
                                                        x<ml:PercentTextBox ID="sProRealETxR" TabIndex="10" runat="server" Width="70" onchange="refreshCalculation();" preset="percent"></ml:PercentTextBox>&nbsp;)
                                                    </asp:PlaceHolder>
                                                </td>
                                                <td nowrap>
                                                    <asp:PlaceHolder runat="server" ID="sProRealETxMbHolder">
                                                        +<ml:MoneyTextBox ID="sProRealETxMb" TabIndex="10" runat="server" Width="90" onchange="refreshCalculation();" preset="money" designtimedragdrop="1583"></ml:MoneyTextBox>&nbsp;=
                                                    </asp:PlaceHolder>
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sProRealETx" TabIndex="2" runat="server" Width="90" onchange="refreshCalculation();" preset="money" ReadOnly="True"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap colspan="5">
                                                    <a tabindex="10" href="javascript:linkMe('LoanInfo.aspx?pg=1');">Mortgage Insurance</a>
                                                </td>
                                                <td nowrap class="FieldLabel">
                                                    <ml:MoneyTextBox ID="sProMIns" TabIndex="10" runat="server" Width="90" preset="money"
                                                        onchange="valueSync(this,'sProMIns_mip');refreshCalculation();" />
                                                    <asp:CheckBox ID="sProMInsLckd" runat="server"
                                                        onclick="checkedSync(this,'sProMInsLckd_mip');refreshCalculation();" TabIndex="10"
                                                        Text="Lock" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    HOA
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sProHoAssocDues" TabIndex="10" runat="server" Width="90" onchange="refreshCalculation();"
                                                        preset="money"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap colspan="2">
                                                    Other Taxes and Expenses
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sProOHExp" TabIndex="10" runat="server" Width="90" onchange="refreshCalculation();"
                                                        preset="money"></ml:MoneyTextBox><asp:CheckBox ID="sProOHExpLckd" runat="server"
                                                            onclick="refreshCalculation();" TabIndex="10" Text="Lock" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel" nowrap>
                                                    PITI
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                </td>
                                                <td nowrap>
                                                    <ml:MoneyTextBox ID="sMonthlyPmt" TabIndex="2" runat="server" Width="90" preset="money"
                                                        ReadOnly="True"></ml:MoneyTextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                        </table>
                        <table>
                            <tr>
                                <td class="FieldLabel" nowrap>
                                    Lead Source
                                </td>
                                <td nowrap>
                                    <asp:DropDownList ID="sLeadSrcDesc" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="MonthlyIncome|CreditScore|SubjPropInfoRow">
                    <td nowrap style="padding-left: 5px; width: 400px;">
                        <table id="MonthlyIncomeTable" cellspacing="0" cellpadding="0" border="0" class="InsetBorder">
                            <tr>
                                <td>
                                    <table id="Table8" cellspacing="0" cellpadding="3" border="0">
                                        <tr>
                                            <td nowrap>
                                            </td>
                                            <td class="FieldLabel" nowrap>
                                                Borrower
                                            </td>
                                            <td class="FieldLabel" nowrap>
                                                Co-borrower
                                            </td>
                                            <td class="FieldLabel" nowrap>
                                                Total
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel income-collection-dependent" nowrap>
                                                <a href="#" id="BaseIncCalcLink" onclick="f_incomeCalc(<%= AspxTools.JsString(aBBaseI.ClientID)%>, <%= AspxTools.JsString(aCBaseI.ClientID)%>, 'Base Income');">
                                                    Base Income</a>
                                                <label ID="BaseIncLabel">Base Income</label>
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aBBaseI" runat="server" preset="money" Width="90" onchange="refreshCalculation();" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aCBaseI" runat="server" preset="money" Width="90" onchange="refreshCalculation();" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aTotBaseI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel income-collection-dependent" nowrap>
                                                <a href="#" id="OvertimeIncCalcLink" onclick="f_incomeCalc(<%= AspxTools.JsString(aBOvertimeI.ClientID)%>, <%=AspxTools.JsString(aCOvertimeI.ClientID)%>, 'Overtime');">
                                                    Overtime</a>
                                                <label ID="OvertimeIncLabel">Overtime</label>
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aBOvertimeI" runat="server" preset="money" Width="90" onchange="refreshCalculation();" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aCOvertimeI" runat="server" preset="money" Width="90" onchange="refreshCalculation();" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aTotOvertimeI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel income-collection-dependent"" nowrap>
                                                <a href="#" id="BonusesIncCalcLink" onclick="f_incomeCalc(<%= AspxTools.JsString(aBBonusesI.ClientID)%>, <%= AspxTools.JsString(aCBonusesI.ClientID)%>, 'Bonuses');">
                                                    Bonuses</a>
                                                <label ID="BonusesIncLabel">Bonuses</label>
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aBBonusesI" runat="server" preset="money" Width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aCBonusesI" runat="server" preset="money" Width="90" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aTotBonusesI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel income-collection-dependent"" nowrap>
                                                <a href="#" id="CommissionIncCalcLink" onclick="f_incomeCalc(<%= AspxTools.JsString(aBCommisionI.ClientID)%>, <%= AspxTools.JsString(aCCommisionI.ClientID)%>, 'Commission');">
                                                    Commission</a>
                                                <label ID="CommissionIncLabel">Commission</label>
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aBCommisionI" runat="server" preset="money" Width="90" onchange="refreshCalculation();" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aCCommisionI" runat="server" preset="money" Width="90" onchange="refreshCalculation();" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aTotCommisionI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel income-collection-dependent"" nowrap>
                                                <a href="#" id="DividendIncCalcLink" onclick="f_incomeCalc(<%= AspxTools.JsString(aBDividendI.ClientID)%>, <%= AspxTools.JsString(aCDividendI.ClientID)%>, 'Dividends/Interest');">
                                                    Dividends/Interest</a>
                                                <label ID="DividendIncLabel">Dividends/Interest</label>
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aBDividendI" runat="server" preset="money" Width="90" onchange="refreshCalculation();" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aCDividendI" runat="server" preset="money" Width="90" onchange="refreshCalculation();" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aTotDividendI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" nowrap>
                                                <a onclick="redirectToUladPage('REO');" title="Go to Real Estate Schedule">
                                                    Net Rent:</a> &nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:CheckBox ID="aNetRentI1003Lckd" onclick="refreshCalculation();" runat="server"
                                                    Text="lock"></asp:CheckBox>
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aBNetRentI1003" runat="server" preset="money" Width="90" onchange="refreshCalculation();" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aCNetRentI1003" runat="server" preset="money" Width="90" onchange="refreshCalculation();" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aTotNetRentI1003" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" nowrap>
                                                Subject Net Cash
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aBSpPosCf" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aCSpPosCf" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aTotSpPosCf" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                        </tr>
                                        <tr title="(see 'Describe Other Income' below)">
                                            <td class="FieldLabel" nowrap>
                                                Other Income:
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aBTotOI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aCTotOI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aTotOI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" nowrap>
                                                Application Total:
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aBTotI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aCTotI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                            <td nowrap>
                                                <ml:MoneyTextBox ID="aTotI" runat="server" preset="money" Width="90" ReadOnly="True" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Total monthly income of all borrowers
                                    <ml:MoneyTextBox ID="sLTotI_mon" runat="server" preset="money" Width="90" ReadOnly="True" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td nowrap style="padding-left: 5px;" valign="top">
                        <div>
                            <table class="InsetBorder" id="CreditScoresTable" cellspacing="1" cellpadding="1"
                                border="0" style="display: inline-table;">
                                <tr>
                                    <td>
                                    </td>
                                    <td class="FieldLabel">
                                        Borrower
                                    </td>
                                    <td class="FieldLabel">
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td class="FieldLabel">
                                        Coborrower
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                        Experian
                                    </td>
                                    <td>
                                        <asp:TextBox ID="aBExperianScore" runat="server" Width="40px" onchange="refreshCalculation();"
                                            MaxLength="4" TabIndex="5"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="aCExperianScore" runat="server" Width="40px" onchange="refreshCalculation();"
                                            MaxLength="4" TabIndex="20"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                        Trans Union
                                    </td>
                                    <td>
                                        <asp:TextBox ID="aBTransUnionScore" runat="server" Width="40px" onchange="refreshCalculation();"
                                            MaxLength="4" TabIndex="10"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="aCTransUnionScore" runat="server" Width="40px" onchange="refreshCalculation();"
                                            MaxLength="4" TabIndex="25"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">
                                        Equifax
                                    </td>
                                    <td>
                                        <asp:TextBox ID="aBEquifaxScore" runat="server" Width="40px" onchange="refreshCalculation();"
                                            MaxLength="4" TabIndex="15"></asp:TextBox>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="aCEquifaxScore" runat="server" Width="40px" onchange="refreshCalculation();"
                                            MaxLength="4" TabIndex="30"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <table style="display: inline-table;">
                                <tr class="FieldLabel">
                                    <td>
                                        Estimated Closing Date
                                    </td>
                                    <td>
                                        <ml:DateTextBox runat="server" ID="sEstCloseD" preset="date"></ml:DateTextBox>
                                        <asp:CheckBox runat="server" ID="sEstCloseDLckd" onchange="lockFields(); refreshCalculation();" /> Lock
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table class="InsetBorder" id="SubjPropInfoTable" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td class="FormTableSubheader" colspan="2">
                                    Subject Property Info
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel" nowrap>
                                    Property Address
                                </td>
                                <td nowrap>
                                    <asp:TextBox onchange="syncMailingAddress();" ID="sSpAddr_spi" Width="359px" MaxLength="60"
                                        runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>
                                </td>
                                <td nowrap>
                                    <asp:TextBox onchange="syncMailingAddress();" ID="sSpCity_spi" MaxLength="36" runat="server"
                                        Width="258px"></asp:TextBox>
                                    <ml:StateDropDownList onchange="valueSync(this, 'sSpState');syncMailingAddress();refreshCalculation();" ID="sSpState_spi" runat="server"
                                        IncludeTerritories="false"></ml:StateDropDownList>
                                    <ml:ZipcodeTextBox onchange="syncMailingAddress();" ID="sSpZip_spi" Width="50" runat="server"
                                        preset="zipcode"></ml:ZipcodeTextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr id="MIP|MiscRow">
                    <td colspan="2" nowrap style="padding-left: 5px;">
                        <asp:HiddenField ID="m_sFfUfMipIsBeingFinancedChanged" runat="server" />
                        <table id="MIPTable">
                            <tr>
                                <td>
                                    <table class="InsetBorder FieldLabel" style="width: 350px">
                                        <tr>
                                            <td colspan="2" class="FormTableSubHeader">
                                                Borrower Paid Upfront MIP/FF
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Total UFMIP / FF
                                                <ml:PercentTextBox ID="sFfUfmipR" Width="70" preset="percent" runat="server" onchange="refreshCalculation();"
                                                    decimalDigits="6" />
                                            </td>
                                            <td align="right">
                                                <asp:CheckBox ID="sFfUfmip1003Lckd" runat="server" onclick="checkedSync(this, 'sFfUfmip1003Lckd_foot');refreshCalculation();">
                                                </asp:CheckBox>
                                                <ml:MoneyTextBox ID="sFfUfmip1003" Width="90" preset="money" runat="server" onchange="valueSync(this, 'sFfUfmip1003_foot');refreshCalculation();"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="LeftPadding">
                                                <input type="radio" id="sMipFrequency_0" value="0" name="sMipFrequency" onclick="refreshCalculation();"
                                                    runat="server" />
                                                Single payment
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="LeftPadding">
                                                <input type="radio" id="sMipFrequency_1" value="1" name="sMipFrequency" onclick="refreshCalculation();"
                                                    runat="server" />
                                                Annual rate for
                                                <asp:TextBox ID="sMipPiaMon" Width="30px" onchange="refreshCalculation();" runat="server"></asp:TextBox>
                                                months
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" nowrap>
                                                Is UFMIP/FF being financed?
                                                <input type="radio" id="sFfUfMipIsBeingFinanced_0" value="True" name="sFfUfMipIsBeingFinanced"
                                                    onclick="sFfUfMipIsBeingFinancedChanged();" runat="server" />Yes
                                                <input type="radio" id="sFfUfMipIsBeingFinanced_1" value="False" name="sFfUfMipIsBeingFinanced"
                                                    onclick="sFfUfMipIsBeingFinancedChanged();" runat="server" />No
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftPadding">
                                                UFMIP/FF financed amount
                                            </td>
                                            <td align="right">
                                                <ml:MoneyTextBox ID="sFfUfmipFinanced_mip" Width="90" preset="money" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftPadding">
                                                Paid in Cash
                                            </td>
                                            <td align="right">
                                                <asp:CheckBox ID="sUfCashPdLckd" runat="server" onclick="refreshCalculation();">
                                                </asp:CheckBox>
                                                <ml:MoneyTextBox ID="sUfCashPd" Width="90" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top">
                                    <table class="InsetBorder FieldLabel" style="width: 350px">
                                        <tr>
                                            <td colspan="2" class="FormTableSubHeader">
                                                MI Policy
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Mortgage insurance type
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="sMiInsuranceT" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                MI coverage %
                                            </td>
                                            <td>
                                                <ml:PercentTextBox ID="sMiLenderPaidCoverage" runat="server" onchange="refreshCalculation();"
                                                    decimalDigits="4"></ml:PercentTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                MI provider
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="sMiCompanyNmT" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                MI commitment requested date
                                            </td>
                                            <td>
                                                <ml:DateTextBox ID="sMiCommitmentRequestedD" runat="server" Width="75" onchange="onDateAddTime(this);" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                MI commitment received date
                                            </td>
                                            <td>
                                                <ml:DateTextBox ID="sMiCommitmentReceivedD" runat="server" Width="75" onchange="onDateAddTime(this);" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                MI commitment expiration date
                                            </td>
                                            <td>
                                                <ml:DateTextBox ID="sMiCommitmentExpirationD" runat="server" Width="75" onchange="onDateAddTime(this);" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                MI certificate ID
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sMiCertId" runat="server" onchange="refreshCalculation();"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                UFMIP is refundable on a pro-rata basis
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="sUfmipIsRefundableOnProRataBasis" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="InsetBorder FieldLabel" style="width: 350px">
                                        <tr>
                                            <td colspan="2" class="FormTableSubHeader">
                                                Borrower Paid Monthly MIP
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Annual premium rate
                                            </td>
                                            <td align="right">
                                                <ml:PercentTextBox ID="sProMInsR" Width="70" preset="percent" runat="server" onchange="refreshCalculation();"
                                                    decimalDigits="6"></ml:PercentTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="LeftPadding">
                                                of
                                                <asp:DropDownList ID="sProMInsT" runat="server" onchange="refreshCalculation();" />
                                            </td>
                                            <td align="right">
                                                <ml:MoneyTextBox ID="sProMInsBaseAmt" Width="90" preset="money" runat="server" onchange="refreshCalculation();"
                                                    ReadOnly="true"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Base monthly premium
                                            </td>
                                            <td align="right">
                                                <ml:MoneyTextBox ID="sProMInsBaseMonthlyPremium" Width="90" preset="money" runat="server"
                                                    onchange="refreshCalculation();" ReadOnly="true"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Monthly premium adjustment
                                            </td>
                                            <td align="right">
                                                <ml:MoneyTextBox ID="sProMInsMb" Width="90" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Monthly premium
                                            </td>
                                            <td align="right">
                                                <asp:CheckBox ID="sProMInsLckd_mip" runat="server" onclick="checkedSync(this,'sProMInsLckd');refreshCalculation();"></asp:CheckBox>
                                                <ml:MoneyTextBox ID="sProMIns_mip" Width="90" preset="money" runat="server" onchange="valueSync(this,'sProMIns');refreshCalculation();"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table id="MiscSection" class="InsetBorder FieldLabel" style="width: 358px">
                                        <tr>
                                            <td>
                                                Agency Case Num
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="sAgencyCaseNum"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Case Assignment Date
                                            </td>
                                            <td>
                                                <ml:DateTextBox runat=server ID="sCaseAssignmentD" preset="date"></ml:DateTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Section of the Act
                                            </td>
                                            <td>
                                                <ml:ComboBox runat="server" ID="sFHAHousingActSection"></ml:ComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Entitlement Code
                                            </td>
                                            <td>
                                                <ml:ComboBox runat="server" ID="aVaEntitleCode"></ml:ComboBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Amount of Entitlement Available
                                            </td>
                                            <td>
                                                <ml:MoneyTextBox runat=server ID="aVaEntitleAmt" preset="money"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Branch of Service
                                            </td>
                                            <td>
                                                <asp:DropDownList runat=server ID="aVaServiceBranchT"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Military Status
                                            </td>
                                            <td>
                                                <asp:DropDownList runat=server ID="aVaMilitaryStatT"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="InsetBorder FieldLabel" style="width: 350px">
                                        <tr>
                                            <td colspan="2" class="FormTableSubHeader">
                                                Borrower Paid MIP Renewal and Cancellation
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Term for initial rate
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sProMInsMon" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Term for renewal annual rate
                                            </td>
                                            <td>
                                                <asp:TextBox ID="sProMIns2Mon" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Renewal annual premium rate
                                            </td>
                                            <td>
                                                <ml:PercentTextBox ID="sProMInsR2" runat="server" onchange="refreshCalculation();"
                                                    decimalDigits="6"></ml:PercentTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Renewal monthly amount
                                            </td>
                                            <td>
                                                <ml:MoneyTextBox ID="sProMIns2" runat="server" preset="money" onchange="refreshCalculation();"
                                                    ReadOnly="true"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Cancel at LTV
                                                <ml:PercentTextBox ID="sProMInsCancelLtv" runat="server" onchange="refreshCalculation();"></ml:PercentTextBox>
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="sProMInsMidptCancel" runat="server" Text="Cancel at midpoint" onchange="refreshCalculation();" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Cancel at Appraisal LTV
                                                <ml:PercentTextBox ID="sProMInsCancelAppraisalLtv" runat="server" onchange="refreshCalculation();"></ml:PercentTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                Minimum number of payments before cancellation
                                                <asp:TextBox ID="sProMInsCancelMinPmts" runat="server" onchange="refreshCalculation();"
                                                    Width="40px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="InsetBorder FieldLabel" style="width: 350px">
                                        <tr>
                                            <td colspan="2" class="FormTableSubHeader">
                                                Lender Paid Single Premium
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Single premium rate
                                                <ml:PercentTextBox ID="sLenderUfmipR" Width="70" preset="percent" runat="server"
                                                    onchange="refreshCalculation();" decimalDigits="6" />
                                            </td>
                                            <td align="right">
                                                <asp:CheckBox ID="sLenderUfmipLckd" runat="server" onclick="refreshCalculation();">
                                                </asp:CheckBox>
                                                <ml:MoneyTextBox ID="sLenderUfmip" Width="90" preset="money" runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <hr>
            <table>
                <tr>
                    <td nowrap style='font-size: 11px'>
                        <%=AspxTools.HtmlString(LendersOffice.Constants.ConstAppDavid.CopyrightMessage)%>
                    </td>
                </tr>
            </table>
        </div>

        <div id="footerWrapper">
            <div id="footer">
                <ul id="Tabs" class="tabnav">
                    <li id="DotLnk"><a href="#DotLnk" onclick="switchTab('Dot');">Details of Transaction</a></li>
                    <li id="MinLnk"><a href="#MinLnk" onclick="switchTab('Min');">Minimize</a></li>
                </ul>
                <div id="closeIcon">
                    <a href="#MinLnk" onclick="switchTab('Min');">&times;</a>
                </div>
                <div id="Dot" class="tabContent">
                    <div>
                        <table>
                            <tr>
                                <td>
                                    <table style="border-color: White" id="Table11" cellspacing="0" cellpadding="0" width="700" border="0">
                                        <tr>
                                            <td nowrap>
                                                <table id="Table4" cellspacing="0" cellpadding="0" border="0">
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td colspan="2" class="align-center">
                                                            Locked
                                                        </td>
                                                        <td width="5">
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td colspan="2">
                                                            Locked
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            a. Purchase price
                                                        </td>
                                                        <td style="width: 11px">
                                                        </td>
                                                        <td style="width: 79px">
                                                            <ml:MoneyTextBox ID="sPurchPrice_foot" TabIndex="3" Width="86" preset="money" runat="server"
                                                                onchange="valueSync(this,'sPurchPrice');refreshCalculation();" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            j. <a tabindex="-1" href="javascript:linkMe(gVirtualRoot + '/newlos/LoanInfo.aspx?pg=2');">
                                                                Subordinate financing</a>
                                                        </td>
                                                        <td style="width: 6px">
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox ID="sONewFinBal" TabIndex="4" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();" ReadOnly="True" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            b. Alterations, improvements, repairs
                                                        </td>
                                                        <td style="width: 11px">
                                                            <input type="checkbox" id="sAltCostLckd" runat="server" onclick="refreshCalculation();"/>
                                                        </td>
                                                        <td style="width: 79px">
                                                            <ml:MoneyTextBox ID="sAltCost" TabIndex="3" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            k. Borrower's closing costs paid by Seller
                                                        </td>
                                                        <td style="width: 6px">
                                                            <asp:CheckBox ID="sTotCcPbsLocked" TabIndex="4" runat="server" onclick="lockFields(); refreshCalculation();">
                                                            </asp:CheckBox>
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox ID="sTotCcPbs" TabIndex="4" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            c. Land (if acquired separately)
                                                        </td>
                                                        <td style="width: 11px">
                                                        </td>
                                                        <td style="width: 79px">
                                                            <ml:MoneyTextBox ID="sLandCost_foot" TabIndex="3" Width="86px" preset="money" runat="server"
                                                                onchange="valueSync(this,'sLandCost');refreshCalculation();" data-field-id="sLandCost"></ml:MoneyTextBox>
                                                        </td>
                                                        <td style="height: 23px">
                                                        </td>
                                                        <td>
                                                            l.
                                                            <ml:ComboBox ID="footer_sOCredit1Desc" TabIndex="4" runat="server" Width="168px"></ml:ComboBox>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="sOCredit1Lckd" runat="server" onclick="lockFields(); refreshCalculation();"
                                                                TabIndex="4"></asp:CheckBox>
                                                        </td>
                                                        <td style="height: 23px">
                                                            <ml:MoneyTextBox ID="sOCredit1Amt" TabIndex="4" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            d. Refi (incl. debts to be paid off)&nbsp;
                                                        </td>
                                                        <td style="width: 11px" class="align-right">
                                                            <asp:CheckBox ID="sRefPdOffAmt1003Lckd" TabIndex="3" runat="server" onclick="lockFields(); refreshCalculation();">
                                                            </asp:CheckBox>
                                                        </td>
                                                        <td style="width: 79px">
                                                            <ml:MoneyTextBox ID="sRefPdOffAmt1003" TabIndex="3" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;<ml:ComboBox ID="footer_sOCredit2Desc" TabIndex="4" runat="server"
                                                                Width="168px"></ml:ComboBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox ID="sOCredit2Amt" TabIndex="4" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            e. Estimated prepaid items&nbsp;
                                                        </td>
                                                        <td style="width: 11px" class="align-right">
                                                            <asp:CheckBox ID="sTotEstPp1003Lckd" TabIndex="3" runat="server" onclick="lockFields(); refreshCalculation();">
                                                            </asp:CheckBox>
                                                        </td>
                                                        <td style="width: 79px">
                                                            <ml:MoneyTextBox ID="sTotEstPp1003" TabIndex="3" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td colspan="2">
                                                            &nbsp;&nbsp;&nbsp;<ml:ComboBox ID="footer_sOCredit3Desc" TabIndex="4" runat="server"
                                                                Width="168px"></ml:ComboBox>
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox ID="sOCredit3Amt" TabIndex="4" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="little-indent">
                                                            <ml:EncodedLiteral runat="server" ID="PrepaidsAndReservesLabel">Prepaids and reserves</ml:EncodedLiteral></td>
                                                        <td>
                                                            <ml:MoneyTextBox runat="server" ID="sTotEstPp" ReadOnly="true"></ml:MoneyTextBox>
                                                        </td>
                                                        <td style="width: 79px">
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td colspan="2">
                                                            &nbsp;&nbsp;&nbsp;<ml:ComboBox ID="footer_sOCredit4Desc" TabIndex="4" runat="server"
                                                                Width="168px"></ml:ComboBox>
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox ID="sOCredit4Amt" TabIndex="4" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="little-indent">
                                                            <ml:EncodedLiteral runat="server" ID="ProrationsPaidByBorrowerLabel">Prorations paid by borrower</ml:EncodedLiteral>
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox runat="server" ID="sTotalBorrowerPaidProrations" ReadOnly="true"></ml:MoneyTextBox>
                                                        </td>
                                                        <td style="width: 79px">
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td colspan="2">
                                                            &nbsp;&nbsp;&nbsp;Lender credit
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox ID="sOCredit5Amt" TabIndex="4" Width="86px" preset="money" runat="server"
                                                                ReadOnly="true" />
                                                        </td>
                                                    </tr>
                                                    <asp:PlaceHolder runat="server" ID="HideIfOtherFinancingCcIncludedInCc">
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td style="width: 11px">
                                                        </td>
                                                        <td style="width: 79px">
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;Other financing closing costs
                                                        </td>
                                                        <td align="right">
                                                            -&nbsp;
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox ID="sONewFinCc" TabIndex="4" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                        </td>
                                                    </tr>
                                                    </asp:PlaceHolder>
                                                    <tr>
                                                        <td>
                                                            f. Estimated closing costs
                                                        </td>
                                                        <td style="width: 11px" class="align-right">
                                                            <asp:CheckBox ID="sTotEstCc1003Lckd" TabIndex="3" runat="server" onclick="lockFields(); refreshCalculation();">
                                                            </asp:CheckBox>
                                                        </td>
                                                        <td style="width: 79px">
                                                            <ml:MoneyTextBox ID="sTotEstCcNoDiscnt1003" TabIndex="3" Width="86px" preset="money"
                                                                runat="server" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                        </td>
                                                        <td colspan="2" runat="server" id="LabelCellM">
                                                            m. Loan amount (exclude PMI, MIP, FF financed)
                                                        </td>
                                                        <td style="width: 6px" runat="server" id="LockedCellM">
                                                            <asp:CheckBox ID="sLAmtLckd_foot" onclick="lockFields();checkedSync(this,'sLAmtLckd');refreshCalculation();"
                                                                runat="server" TabIndex="202"></asp:CheckBox>
                                                        </td>
                                                        <td runat="server" id="AmountCellM">
                                                            <ml:MoneyTextBox ID="sLAmtCalc_foot" TabIndex="4" ReadOnly="True" Width="86px" preset="money"
                                                                runat="server" onchange="valueSync(this,'sLAmtLckd');"></ml:MoneyTextBox>
                                                        </td>
                                                    </tr>
                                                    <asp:PlaceHolder runat="server" ID="DisplayIfOtherFinancingCcIncludedInCc">
                                                    <tr>
                                                      <td class="little-indent">This loan closing costs</td>
                                                      <td style="width: 11px">
                                                          <ml:MoneyTextBox runat="server" ID="sTotEstCcNoDiscnt" ReadOnly="true"></ml:MoneyTextBox>
                                                      </td>
                                                      <td style="width: 79px"></td>
                                                      <td colspan="2"></td>
                                                      <td style="width: 6px"></td>
                                                      <td></td>
                                                    </tr>
                                                    <tr>
                                                      <td class="little-indent">Other financing costs</td>
                                                      <td style="width: 11px">
                                                          <ml:MoneyTextBox runat="server" ID="sONewFinCc2" data-field-id="sONewFinCc" onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                      </td>
                                                      <td style="width: 79px"></td>
                                                      <td colspan="2"></td>
                                                      <td style="width: 6px"></td>
                                                      <td></td>
                                                    </tr>
                                                    </asp:PlaceHolder>
                                                    <tr>
                                                        <td>
                                                            g. PMI,&nbsp;MIP, Funding Fee
                                                        </td>
                                                        <td style="width: 11px" class="align-right">
                                                            <asp:CheckBox ID="sFfUfmip1003Lckd_foot" TabIndex="3" runat="server" onclick="lockFields();checkedSync(this, 'sFfUfmip1003Lckd');refreshCalculation();">
                                                            </asp:CheckBox>
                                                        </td>
                                                        <td style="width: 79px">
                                                            <ml:MoneyTextBox ID="sFfUfmip1003_foot" TabIndex="3" Width="86px" preset="money"
                                                                runat="server" onchange="valueSync(this, 'sFfUfmip1003');refreshCalculation();"></ml:MoneyTextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            n. PMI, MIP, Funding Fee financed
                                                        </td>
                                                        <td style="width: 6px">
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox ID="sFfUfmipFinanced_foot" TabIndex="4" ReadOnly="True" Width="86px"
                                                                preset="money" runat="server"></ml:MoneyTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            h. Discount (if Borrower will pay)
                                                        </td>
                                                        <td style="width: 11px" class="align-right">
                                                            <asp:CheckBox ID="sLDiscnt1003Lckd" TabIndex="3" runat="server" onclick="lockFields(); refreshCalculation();">
                                                            </asp:CheckBox>
                                                        </td>
                                                        <td style="width: 79px">
                                                            <ml:MoneyTextBox ID="sLDiscnt1003" TabIndex="3" Width="86px" preset="money" runat="server"
                                                                onchange="refreshCalculation();"></ml:MoneyTextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            o. Loan amount (add m &amp; n)
                                                        </td>
                                                        <td style="width: 6px">
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox ID="sFinalLAmt_foot" TabIndex="4" ReadOnly="True" Width="86px" preset="money"
                                                                runat="server"></ml:MoneyTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="FieldLabel">
                                                            i. Total costs (add items a to h)
                                                        </td>
                                                        <td style="width: 11px">
                                                        </td>
                                                        <td style="width: 79px">
                                                            <ml:MoneyTextBox ID="sTotTransC" TabIndex="3" ReadOnly="True" Width="86px" preset="money"
                                                                runat="server"></ml:MoneyTextBox>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            p. Cash from / to Borr (subtract j, k, l &amp; o from i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td style="width: 6px">
                                                            <asp:CheckBox ID="sTransNetCashLckd" TabIndex="4" runat="server" onclick="lockFields(); refreshCalculation();">
                                                            </asp:CheckBox>
                                                        </td>
                                                        <td>
                                                            <ml:MoneyTextBox ID="sTransNetCash" TabIndex="4" Width="86px" preset="money" runat="server"></ml:MoneyTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <uc1:QualifyingBorrower runat="server" ID="QualifyingBorrowerUc" />

        <script id="footerInitJS" type="text/javascript">
            var $wrapper = $("#wrapper");
            var $closeIcon = $("#closeIcon");
            var $tabLinks = $("#Tabs li");
            var $contentDivs = $(".tabContent");

            function switchTab(tabId)
            {
                $closeIcon.hide();
                $contentDivs.hide();
                $tabLinks.removeClass();

                $tabLinks.filter("#" + tabId + "Lnk").addClass("selected");

                $div = $contentDivs.filter("#" + tabId);
                if ($div.length != 0)
                {   // need check because minimize has no content
                    $div.show();
                    $closeIcon.show();
                }

                $(window).trigger('footer/switchTab')
            }

            switchTab("Min");


            var $footerWrapper = $("#footerWrapper");
            var lastWidth = 0;

            // Polling function to catch zoom/resize event
            function pollZoomFireEvent()
            {
                // jQuery does not support quirks mode. As such, we have
                // to grab the client width to determine the width of
                // the window.
                var widthNow = document.body.clientWidth;
                if (lastWidth == widthNow) return;
                lastWidth = widthNow;
                // Length changed, user must have zoomed or resized. Reset foorter bottom
                setFooterBottom();
            }

            // Moves footer out of the way of horizontal scrollbar, if it's showing
            // Only needed for rendering in quirks mode
            function setFooterBottom()
            {
                // OPM 220126, ML, 10/19/2015,
                // Checking the scroll width against the inner width is a more reliable
                // way of determining if there is a scrollbar on the page, as it includes
                // instances where (like in case 220126) a page is opened outside of the
                // loan editor.
                if ($wrapper.get(0).scrollWidth > $wrapper.innerWidth())
                    $footerWrapper.css("bottom", "17px");
                else
                    $footerWrapper.css("bottom", "0px");
            }

            function setWrapperPadding()
            {
                $wrapper.css("padding-bottom", $footerWrapper.height() + "px");
            }

            // This should only run in Quirks mode on IE9 or below
            // NOTE: IE10 quirks mode is not the same as in previous versions of IE.
            if (document.documentMode < 8)
            {
                setFooterBottom();
                setInterval(pollZoomFireEvent, 100);
            }

            setWrapperPadding();
            $.event.add(this, "footer/switchTab", setWrapperPadding);

            $(function () {
                var $qbContainer = $('.true-container');
                if (ML.isUlad2019) {
                    $qbContainer.attr('class', $qbContainer.attr('class') + ' wrap');
                    $('#Dot').html('').append($qbContainer);
                    $('#DotLnk a').text('Qualifying Borrower');
                    initializeQualifyingBorrower(false, refreshCalculation, true);
                    window.setTimeout(function(){ 
                        populateQualifyingBorrower(JSON.parse(ML.qualifyingBorrowerModel)); }, 200);
                }
                else {
                    $qbContainer.remove();
                }
            });
        </script>
    </form>
</body>
</html>
