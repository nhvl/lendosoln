﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvestorRolodexInfo.aspx.cs" Inherits="LendersOfficeApp.newlos.InvestorRolodexInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    
        <style type="text/css">
        #form1 table, #form1 fieldset { width: 700px; }
        #form1 fieldset, #form1 table.last  { margin-top: 10px; display: block;}
        #form1 fieldset table { margin-left: 7px; margin-top: 5px;  }
        #form1 table.first { margin-left: 20px; table-layout: fixed; }
        .MainRightHeader { margin-bottom: 10px; }
        body { font-weight: bold; background-color: gainsboro;}
        legend { font-weight: bolder;  }
        input.address, input.attention, input.company { width: 250px; }
        input.zip { width: 70px; }
        input.email { width: 150px; }
        input.contactname {width: 225px; }
        input.investor { width: 200px; }
        input.mers, input.seller { width: 95px; }
        span.usecb input { margin-left: -3px; position: relative;}
    </style>
</head>
<body>
    <script type="text/javascript">
            function f_save() {
                return f_saveMe();
            }
            
            function saveMe() {
                return f_saveMe();
                
            }
            
            function f_saveMe() {
                var saved = false;
                var data = { 
                    sLId : ML.sLId,
                    id : $('#sInvestorRolodexEntries').val(),
                    fileVersion : $('#sFileVersion').val()
                };
                 callWebMethodAsync({
                    type: "POST",
                    async: false,
                    url: "InvestorRolodexInfo.aspx/Save",
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function(msg){
                        saved = true;
                    },
                    error: function(){
                        alert('Error saving.');
                        saved = false;
                    }
                });
                
                return saved;
            }
            $(function() {
                $('#sInvestorRolodexEntries').change(function() {
                    callWebMethodAsync({
                        type: "POST",
                        url: "InvestorRolodexInfo.aspx/GetInvestorInfo",
                        data: '{ id :' + $(this).val() + '}',
                        contentType: 'application/json; charset=utf-8',
                        dataType: "json",
                        success: function(msg) {
                            $('#SellerId').val(msg.d.SellerId);
                            $('#MersOrganizationId').val(msg.d.MERSOrganizationId);
                            $('#CompanyName').val(msg.d.CompanyName);
                            $('#MainContactName').val(msg.d.MainContactName);
                            $('#MainAttention').val(msg.d.MainAttention);
                            $('#MainEmail').val(msg.d.MainEmail);
                            $('#MainAddress').val(msg.d.MainAddress);
                            $('#MainCity').val(msg.d.MainCity);
                            $('#MainState').val(msg.d.MainState);
                            $('#MainZip').val(msg.d.MainZip);
                            $('#MainPhone').val(msg.d.MainPhone);
                            $('#MainFax').val(msg.d.MainFax);
                            $('#NoteShipToAttention').val(msg.d.NoteShipToAttention);
                            $('#NoteShipToContactName').val(msg.d.NoteShipToContactName);
                            $('#NoteShipToAddress').val(msg.d.NoteShipToAddress);
                            $('#NoteShipToCity').val(msg.d.NoteShipToCity);
                            $('#NoteShipToState').val(msg.d.NoteShipToState);
                            $('#NoteShipToZip').val(msg.d.NoteShipToZip);
                            $('#NoteShipToEmail').val(msg.d.NoteShipToEmail);
                            $('#NoteShipToPhone').val(msg.d.NoteShipToPhone);
                            $('#NoteShipToFax').val(msg.d.NoteShipToFax);
                            $('#PaymentToAttention').val(msg.d.PaymentToAttention);
                            $('#PaymentToContactName').val(msg.d.PaymentToContactName);
                            $('#PaymentToAddress').val(msg.d.PaymentToAddress);
                            $('#PaymentToCity').val(msg.d.PaymentToCity);
                            $('#PaymentToState').val(msg.d.PaymentToState);
                            $('#PaymentToZip').val(msg.d.PaymentToZip);
                            $('#PaymentToEmail').val(msg.d.PaymentToEmail);
                            $('#PaymentToPhone').val(msg.d.PaymentToPhone);
                            $('#PaymentToFax').val(msg.d.PaymentToFax);
                            $('#LossPayeeAttention').val(msg.d.LossPayeeAttention);
                            $('#LossPayeeContactName').val(msg.d.LossPayeeContactName);
                            $('#LossPayeeAddress').val(msg.d.LossPayeeAddress);
                            $('#LossPayeeCity').val(msg.d.LossPayeeCity);
                            $('#LossPayeeState').val(msg.d.LossPayeeState);
                            $('#LossPayeeZip').val(msg.d.LossPayeeZip);
                            $('#LossPayeeEmail').val(msg.d.LossPayeeEmail);
                            $('#LossPayeePhone').val(msg.d.LossPayeePhone);
                            $('#LossPayeeFax').val(msg.d.LossPayeeFax);
                            $('#sHmdaPurchaser2015T').val(msg.d.HmdaPurchaser2015T);
                        },
                        error: function() {
                            alert('Could not fetch investor data.');
                        }
                    });
                });
            });
    </script>
    <form id="form1" runat="server">
    <div class="MainRightHeader">
        Investor Info
    </div>
    <table cellpadding="0" cellspacing="0" class="first">
        <tr>
            <td width="130px" >
                <ml:EncodedLabel ID="Label1" runat="server" AssociatedControlID="sInvestorRolodexEntries">Investor</ml:EncodedLabel>
            </td>
            <td width="230px">
                <asp:DropDownList runat="server" ID="sInvestorRolodexEntries"></asp:DropDownList>
            </td>
           
        </tr>
        <tr>
            <td>
                <ml:EncodedLabel ID="Label3" runat="server" AssociatedControlID="SellerId">Seller ID</ml:EncodedLabel>
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="SellerId" CssClass="seller" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <ml:EncodedLabel ID="Label4" runat="server" AssociatedControlID="MersOrganizationId">MERS Organization ID</ml:EncodedLabel>
            </td>
            <td >
                <asp:TextBox runat="server" ID="MersOrganizationId" preset="mersorgid" ReadOnly="true"  CssClass="mers"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="sHmdaPurchaser2015T">HMDA Purchaser Type</ml:EncodedLabel>
            </td>
            <td colspan="3"> 
                <asp:DropDownList ID="sHmdaPurchaser2015T" runat="server" Enabled="false"></asp:DropDownList>
            </td>
        </tr>
    </table>
    <fieldset style="margin-left: 10px">
        <legend>Main Address</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label5" runat="server" AssociatedControlID="CompanyName">Company Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="CompanyName" ReadOnly="true" CssClass="company"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label6" runat="server" AssociatedControlID="MainContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label7" runat="server" AssociatedControlID="MainAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label8" runat="server" AssociatedControlID="MainEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label9" runat="server" AssociatedControlID="MainAddress" >Address</ml:EncodedLabel>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="MainAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="MainCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="MainState" CssClass="state" BackColor="Gainsboro" ReadOnly="true"  disabled="disabled" />
                    <asp:TextBox runat="server" ID="MainZip" preset="longzipcode" CssClass="zip" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label10" runat="server" AssociatedControlID="MainPhone" >Phone</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="MainPhone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label11" runat="server" AssociatedControlID="MainFax">Fax</ml:EncodedLabel>
                </td>
                <td>
                    <ml:PhoneTextBox runat="server" ID="MainFax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Note Ship To</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label12" runat="server" AssociatedControlID="NoteShipToAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="NoteShipToAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label13" runat="server" AssociatedControlID="NoteShipToContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="NoteShipToContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
          
       
                <td>
                    <ml:EncodedLabel ID="Label16" runat="server" AssociatedControlID="NoteShipToAddress">Address</ml:EncodedLabel>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="NoteShipToAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
                    <td>
                    <ml:EncodedLabel ID="Label15" runat="server" AssociatedControlID="NoteShipToEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="NoteShipToEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="NoteShipToCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="NoteShipToState"  CssClass="state" disabled="disabled"/>
                    <asp:TextBox runat="server" ID="NoteShipToZip" preset="longzipcode" CssClass="zip" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label17" runat="server" AssociatedControlID="NoteShipToPhone">Phone</ml:EncodedLabel>
                </td>
                <td>
                    <ml:PhoneTextBox runat="server" ID="NoteShipToPhone" CssClass="phone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label18" runat="server" AssociatedControlID="NoteShipToFax">Fax</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="NoteShipToFax" CssClass="fax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Payment To</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label19" runat="server" AssociatedControlID="PaymentToAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PaymentToAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label20" runat="server" AssociatedControlID="PaymentToContactName" >Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PaymentToContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
      
        
     
                <td>
                    <ml:EncodedLabel ID="Label23" runat="server" AssociatedControlID="PaymentToAddress">Address</ml:EncodedLabel>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="PaymentToAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
             <td>
                    <ml:EncodedLabel ID="Label22" runat="server" AssociatedControlID="PaymentToEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PaymentToEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
                
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="PaymentToCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="PaymentToState" CssClass="state" disabled="disabled" />
                    <asp:TextBox runat="server" ID="PaymentToZip" preset="longzipcode" CssClass="zip" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label24" runat="server" AssociatedControlID="PaymentToPhone" >Phone</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="PaymentToPhone" CssClass="phone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label25" runat="server" AssociatedControlID="PaymentToFax">Fax</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="PaymentToFax" CssClass="fax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Loss Payee</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label26" runat="server" AssociatedControlID="LossPayeeAttention" >Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeAttention" CssClass="attention" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label27" runat="server" AssociatedControlID="LossPayeeContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeContactName" CssClass="contactname" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
            
   
       
                <td>
                    <ml:EncodedLabel ID="Label30" runat="server" AssociatedControlID="LossPayeeAddress">Address</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeAddress" CssClass="address" ReadOnly="true"></asp:TextBox>
                </td>
             <td>
                    <ml:EncodedLabel ID="Label29" runat="server" AssociatedControlID="LossPayeeEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeEmail" CssClass="email" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="LossPayeeCity" CssClass="city" ReadOnly="true"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="LossPayeeState"  CssClass="state" disabled="disabled"/>
                    <asp:TextBox runat="server" ID="LossPayeeZip" preset="longzipcode" CssClass="zip" ReadOnly="true" ></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label31" runat="server" AssociatedControlID="LossPayeePhone">Phone</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="LossPayeePhone" CssClass="phone" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label32" runat="server" AssociatedControlID="LossPayeeFax">Fax</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="LossPayeeFax" CssClass="fax" preset="phone" ReadOnly="true"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    </form>
</body>
</html>
