using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOffice.Common;
using LendingQB.Core.Data;
using LqbGrammar.DataTypes;
using System;
using System.Data;
using System.Linq;

namespace LendersOfficeApp.newlos.Verifications
{
    public partial class VODRecordService : AbstractSingleEditService
    {
        /// <summary>
        /// Gets the DataObjectIdentifier for liabilities by converting the RecordId.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Asset, Guid> RecordIdentifier => this.RecordID.ToIdentifier<DataObjectKind.Asset>();

        protected override void CustomProcess(string methodName)
        {
            switch (methodName)
            {
                case "ApplySig":
                    ApplySig();
                    break;
                case "ClearSig":
                    ClearSig();
                    break;
                default:
                    break;
            }
        }

        private void ApplySig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VODRecordService));
            dataLoan.InitSave(sFileVersion);
            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);

            if (!this.IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                var field = dataApp.aAssetCollection.GetRegRecordOf(RecordID);
                field.ApplySignature(principal.EmployeeId, signInfo.SignatureKey.ToString());
                field.Update();
            }
            else
            {
                var asset = dataLoan.Assets[this.RecordIdentifier];
                var employeeIdentifier = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(principal.EmployeeId);
                var signatureIdentifier = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(signInfo.SignatureKey);
                asset.SetVerifSignature(employeeIdentifier, signatureIdentifier);
            }

            dataLoan.Save();

            SetResult("EmployeeId", principal.EmployeeId);
        }

        private void ClearSig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = new CAssetRecordData(LoanID);
            dataLoan.InitSave(sFileVersion);

            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);

                var field = dataApp.aAssetCollection.GetRegRecordOf(RecordID);
                field.ClearSignature();
                field.Update();
            }
            else
            {
                var asset = dataLoan.Assets[this.RecordIdentifier];
                asset.ClearSignature();
            }

            dataLoan.Save();
        }

        private void SaveData(IAssetRegular field)
        {
            field.VerifExpD_rep = GetString("VerifExpD");
            field.VerifRecvD_rep = GetString("VerifRecvD");
            field.VerifSentD_rep = GetString("VerifSentD");
            field.VerifReorderedD_rep = GetString("VerifReorderedD");
            field.Attention = GetString("Attention");
            field.IsSeeAttachment = GetBool("IsSeeAttachment");
            field.Update();
        }

        protected override void SaveData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(VODRecordService));
            dataLoan.InitSave(sFileVersion);
            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");

            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfDeposit, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("PreparerName");
            f.CompanyName = GetString("PreparerCompanyName");
            f.Title = GetString("PreparerTitle");
            f.StreetAddr = GetString("PreparerStreetAddr");
            f.City = GetString("PreparerCity");
            f.State = GetString("PreparerState");
            f.Zip = GetString("PreparerZip");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");
            f.Phone = GetString("PreparerPhone");
            f.FaxNum = GetString("PreparerFaxNum");
            f.Update();

            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                var field = dataApp.aAssetCollection.GetRegRecordOf(RecordID);
                SaveData(field);
            }

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        protected override void SaveDataAndLoadNext()
        {

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VODRecordService));
            dataLoan.InitSave(sFileVersion);


            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");

            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfDeposit, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("PreparerName");
            f.CompanyName = GetString("PreparerCompanyName");

            f.Title = GetString("PreparerTitle");
            f.StreetAddr = GetString("PreparerStreetAddr");
            f.City = GetString("PreparerCity");
            f.State = GetString("PreparerState");
            f.Zip = GetString("PreparerZip");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");
            f.Phone = GetString("PreparerPhone");
            f.FaxNum = GetString("PreparerFaxNum");
            f.Update();

            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                var field = dataApp.aAssetCollection.GetRegRecordOf(RecordID);
                SaveData(field);
                dataLoan.Save();
                field = dataApp.aAssetCollection.GetRegRecordOf(NextRecordID);
                LoadData(dataLoan, field);
            }

            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        protected override void LoadData()
        {
            CAssetRecordData dataLoan = new CAssetRecordData(LoanID);
            dataLoan.InitLoad();

            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                var field = dataApp.aAssetCollection.GetRegRecordOf(RecordID);
                LoadData(dataLoan, field);
            }
        }

        private void LoadLoanData(CPageData dataLoan)
        {
            SetResult("sLenderNumVerif", dataLoan.sLenderNumVerif);

            IPreparerFields f1 = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfDeposit, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f1.IsValid)
            {
                SetResult("PreparerName", f1.PreparerName);
                SetResult("PreparerCompanyName", f1.CompanyName);

                SetResult("PreparerTitle", f1.Title);
                SetResult("PreparerStreetAddr", f1.StreetAddr);
                SetResult("PreparerCity", f1.City);
                SetResult("PreparerState", f1.State);
                SetResult("PreparerZip", f1.Zip);
                SetResult("PreparerPrepareDate", f1.PrepareDate_rep);
                SetResult("PreparerPhone", f1.Phone);
                SetResult("PreparerFaxNum", f1.FaxNum);
            }
        }

        private void LoadData(CPageData dataLoan, IAssetRegular field)
        {
            LoadLoanData(dataLoan);
            CAppData dataApp = dataLoan.GetAppData(0);

            SetResult("OwnerName", string.Format(@"{1}, {0}", dataApp.aBFirstNm, dataApp.aBLastNm));
            SetResult("RecordID", field.RecordId);
            SetResult("Attention", field.Attention);
            SetResult("OwnerT", field.OwnerT == E_AssetOwnerT.Borrower ? "0" : "1");
            SetResult("DepositoryName", field.ComNm);
            SetResult("DepositoryStAddr", field.StAddr);
            SetResult("DepositoryCity", field.City);
            SetResult("DepositoryState", field.State);
            SetResult("DepositoryZip", field.Zip);
            SetResult("IsSeeAttachment", field.IsSeeAttachment);
            SetResult("VerifExpD", field.VerifExpD_rep);
            SetResult("VerifSentD", field.VerifSentD_rep);
            SetResult("VerifRecvD", field.VerifRecvD_rep);
            SetResult("VerifReorderedD", field.VerifReorderedD_rep);
            SetResult("VerifSigningEmployeeId", field.VerifSigningEmployeeId);
            SetResult("VerifHasSignature", field.VerifHasSignature);

            // Loop through list of asset record.
            var assetSubcoll = dataApp.aAssetCollection.GetSubcollection(false, E_AssetGroupT.DontNeedVOD);
            int currentIndex = 0;

            foreach (var item in assetSubcoll)
            {
                var f = (IAssetRegular)item;
                if (currentIndex >= 4)
                    break;

                if (f.ComNm.ToLower() == field.ComNm.ToLower())
                {
                    SetResult("AccNm" + currentIndex, f.AccNm);
                    SetResult("AccNum" + currentIndex, f.AccNum.Value);
                    SetResult("AssetT" + currentIndex, f.AssetT_rep);
                    SetResult("Val" + currentIndex, f.Val_rep);
                    currentIndex++;
                } // end if 
            } // end for

            // Blank other empty accounts.
            for (int i = currentIndex; i < 4; i++)
            {
                SetResult("AccNm" + i, "");
                SetResult("AccNum" + i, "");
                SetResult("AssetT" + i, "");
                SetResult("Val" + i, "");
            }
        }
    }
}
