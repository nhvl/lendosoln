<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="VOLandTab.ascx.cs" Inherits="LendersOfficeApp.newlos.Verifications.VOLandTab" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOfficeApp.newlos.Verifications"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script language=javascript>
<!--
function _init() {
  <% if (IsReadOnly) { %>
    document.getElementById("btnAdd").disabled = true;
    document.getElementById("btnDelete").disabled = true;
        f_disableAll();    
  <% } %>
}
<% if (IsReadOnly) { %>
      function f_disableAll() {
        var coll = document.getElementsByTagName("input");      
        var length = coll.length;
        if (length > 0) {
          for (i = 0; i < length; i++) {
            var o = coll[i];
            if (o.type=="text") o.readOnly = true;
            else if (o.type =="checkbox") o.disabled = true;
          }
        }   
        
        coll = document.getElementsByTagName("textarea");      
        length = coll.length;
        if (length > 0) {
          for (i = 0; i < length; i++) {
            var o = coll[i];
            o.style.backgroundColor = gReadonlyBackgroundColor;
            o.readOnly = true;
          }
        }              
      }
<% } %>
      function btnDelete_onclick() 
      {
        var collection = document.getElementsByTagName("input");
        var length = collection.length;
        var list = '';
        for (var i = 0; i < length; i++) 
        {
          var o = collection[i];
          if (o.name == 'del' && o.checked) 
          {
            list += o.value + ',';
          }
        }
        if (list == '')
        {
          alert('No record selected.');
        } 
        else 
        {
          if (confirm('Do you want to delete these records?') == false) 
          {
            return;
          }
          var args = {
                        LoanID: ML.sLId,
                        ApplicationID: ML.aAppId,
                        sFileVersion: document.getElementById('sFileVersion').value,
                        _ClientID: ML._ClientID,
                        Ids: list
                     };
          var result = gService.loanedit.call(ML._ClientID + '_DeleteVerification', args);
          if (!result.error) 
          {
            self.location = self.location;
          }
          else
          {
            if (result.ErrorType === 'VersionMismatchException')
            {
              f_displayVersionMismatch();
            }
            else if (result.ErrorType === 'LoanFieldWritePermissionDenied') 
            {
                f_displayFieldWriteDenied(result.UserMessage);
            }
            else 
            {
              var errMsg = result.UserMessage ||'Unable to process data. Please try again.';
              alert(errMsg);

            } 
          }
        }

      }

function showVOLEdit(id) {
    linkMe('VOLandRecord.aspx', 'recordid=' + id);  

    }

    function downloadPdf(event) {
        var $el = $(retrieveEventTarget(event));
        var url = VRoot + '/' + $el.attr('href');
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);

        event.preventDefault();
        return false;
    }

//-->
</script>
    <table>
   <tr><td>
    <input type="button" value="Add new land contract verification" onclick=<%= AspxTools.HtmlAttribute("showVOLEdit(" + AspxTools.JsString(Guid.Empty) + ");") %>  accessKey="A" id="btnAdd">
    <input type="button" value="Delete selected" onclick="btnDelete_onclick();" id="btnDelete" />
   </td></tr>

  <tr><td class=FieldLabel>
<ml:EncodedLiteral id="m_noRecordLabel" runat="server" EnableViewState="False" Visible="False" Text="No land contract to verify"></ml:EncodedLiteral>
</td></tr>

</table>
<ml:DateTextBox ID="HiddenDTBox" Visible="false" runat="server"></ml:DateTextBox>
<ml:CommonDataGrid id=m_dg runat="server">
  <alternatingitemstyle cssclass="GridAlternatingItem"/>
  <itemstyle cssclass="GridItem"/>
  <headerstyle cssclass="GridHeader"/>
  <columns>
    <asp:templatecolumn>
      <itemtemplate>
        <input type="checkbox" name="del" value=<%# AspxTools.HtmlAttribute(((VerificationData)Container.DataItem).RecordID.ToString()) %>/>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn>
      <itemtemplate>
        (<a href='#' onclick=<%# AspxTools.HtmlAttribute("showVOLEdit(" + AspxTools.JsString(((VerificationData)Container.DataItem).RecordID) + ")") %>>edit</a>)
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn>
      <itemtemplate>
        (<a onclick="downloadPdf(event)" href=<%# AspxTools.SafeUrl("pdf/VOLand.aspx?loanid=" + this.LoanID + "&applicationid=" + ((VerificationData)Container.DataItem).ApplicationID + "&recordid=" + ((VerificationData)Container.DataItem).RecordID) %> >preview</a>)
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Name">
      <itemtemplate>
      <%# AspxTools.HtmlString(((VerificationData)Container.DataItem).Description) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Ordered Date">
      <itemtemplate>
<%# AspxTools.HtmlControls(CreateDateTextBox(((VerificationData)Container.DataItem).RecordID, "VerifSentD", ((VerificationData)Container.DataItem).VerifSentD)) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Reordered Date">
      <itemtemplate>
<%# AspxTools.HtmlControls(CreateDateTextBox(((VerificationData)Container.DataItem).RecordID, "VerifReorderedD", ((VerificationData)Container.DataItem).VerifReorderedD)) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Received Date">
      <itemtemplate>
<%# AspxTools.HtmlControls(CreateDateTextBox(((VerificationData)Container.DataItem).RecordID, "VerifRecvD", ((VerificationData)Container.DataItem).VerifRecvD)) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Expected Date">
      <itemtemplate>
<%# AspxTools.HtmlControls(CreateDateTextBox(((VerificationData)Container.DataItem).RecordID, "VerifExpD", ((VerificationData)Container.DataItem).VerifExpD)) %>
      </itemtemplate>
    </asp:templatecolumn>

  </columns>
</ml:CommonDataGrid>
