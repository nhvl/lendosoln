﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderVOD.aspx.cs" Inherits="LendersOfficeApp.newlos.Verifications.OrderVOD" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Order Verification of Deposits</title>
    <style type="text/css">
        body  {
            background-color: gainsboro;
        }
        a[data-sort] {
            color: white;
        }
        #SelectionHeader {
            width: 170px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        var serviceCredentialId = null;
        jQuery(function ($) {
            var authDocsEnabled = false;
            var isAssetPickerBeingUsed;

            $('#AssetsLink').click(function () {
                if (!isLqbPopup(window)) {
                    redirectToUladPage('Assets');
                }
                else {
                    parent.redirectToUladPage('Assets');
                }
            });

            $('#ContactPicker').click(function () {
                LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/los/RolodexList.aspx?isCurrentAgentOnly=true&loanid=" + this.LoanID)) %>, {
                    hideCloseButton: true,
                    width: 800,
                    height: 400,
                    onReturn: function(returnArgs) {
                        if(typeof(returnArgs) != 'undefined' && returnArgs != null) {
                            $('#NotificationEmail').val(returnArgs.AgentEmail);
                            $('#NotificationEmail').siblings('.RequiredImg').toggle(returnArgs.AgentEmail === '');
                            ToggleOrderBtn();
                        }
                    }
                }, null);
            });

            $('#LenderServices').change(function () {
                var accountHistoryDDL = $('#AccountHistoryOptions');
                accountHistoryDDL.find('option').remove();
                var refreshPeriodDDL = $('#RefreshPeriodOptions');
                refreshPeriodDDL.find('option').remove();

                var lenderServiceId = $(this).val();
                if(!lenderServiceId) {
                    ToggleOrderBtn();
                    AuthorizationDocPicker.RemoveDocPicker(null, $('#DocumentTable'));
                    $('#BorrowerAuthDocsSection').hide();
                    authDocsEnabled = false;
                    return;
                }

                var data = {
                    LenderServiceId: lenderServiceId,
                    LoanId: ML.sLId,
                };

                var result = gService.VOA.call("LoadLenderService", data)
                if(!result.error) {
                    if(result.value["Success"].toLowerCase() === 'true') {
                        var showNotification = result.value["AskForNotificationEmail"].toLowerCase() == 'true';
                        var showCreditCard = result.value["AllowPaymentByCreditCard"].toLowerCase() == 'true';
                        var requiresAccountId = result.value["RequiresAccountId"].toLowerCase() == 'true';
                        var usesAccountId = result.value["UsesAccountId"].toLowerCase() == 'true';
                        var accountHistory = JSON.parse(result.value["AccountHistoryOptions"]);
                        var accountHistoryDefault = result.value["AccountHistoryDefault"];
                        var refreshPeriod = JSON.parse(result.value["RefreshPeriodOptions"]);
                        var refreshPeriodDefault = result.value["RefreshPeriodDefault"];
                        var verificationType = result.value["VoaVerificationT"];
                        var voaAllowRequestWithoutAssets = result.value["VoaAllowRequestWithoutAssets"].toLowerCase() == 'true';
                        var voaEnforceAllowRequestWithoutAssets = result.value["VoaEnforceAllowRequestWithoutAssets"].toLowerCase() == 'true';

                        TogglePickerSection(!voaAllowRequestWithoutAssets, !voaEnforceAllowRequestWithoutAssets);

                        $('#CreditCardRow').toggle(showCreditCard);
                        BindNotificationEmail(showNotification);

                        authDocsEnabled = verificationType === '1';
                        if(!authDocsEnabled) {
                            AuthorizationDocPicker.RemoveDocPicker(null, $('#DocumentTable'));
                            $('#BorrowerAuthDocsSection').hide();
                        }
                        else {
                            if(isAssetPickerBeingUsed) {
                                var assetsCBs = $('.AssetsCB:checked');
                                assetsCBs.each(function (index) {
                                    CheckBorrowerAuthDocsForAssets(this, false);
                                });
                            }
                            else {
                                CheckBorrowerAuthDocsForBorrowers(false);
                            }
                        }

                        BindLenderServiceOptions(accountHistory, accountHistoryDefault, refreshPeriod, refreshPeriodDefault);
                        var credentialId = typeof(result.value["ServiceCredentialId"]) === 'undefined' ? null : result.value["ServiceCredentialId"];
                        var serviceCredentialHasAccountId = typeof(result.value["ServiceCredentialHasAccountId"]) === 'undefined' ? false : result.value["ServiceCredentialHasAccountId"].toLowerCase() === 'true';
                        VOXOrderCommon.BindCredentialOptions(usesAccountId, requiresAccountId, credentialId, serviceCredentialHasAccountId);
                    }
                    else {
                        alert(result.value["Errors"]);
                    }
                }
                else {
                    alert(result.UserMessage);
                }

                ToggleOrderBtn();
            });

            $('#CheckAllCB').click(function () {
                if(this.checked) {
                    var assetCBs = $('.AssetsCB:not(:checked)');
                    assetCBs.each(function (index) {
                        $(this).prop('checked', true);
                        CheckBorrowerAuthDocsForAssets(this, false);
                    });
                }
                else {
                    var assetsCBs = $('.AssetsCB:checked');
                    assetsCBs.each(function (index) {
                        $(this).prop('checked', false);
                        CheckBorrowerAuthDocsForAssets(this, false);
                    });
                }

                ToggleOrderBtn();
            });

            $('.AssetsCB').click(function(e) {
                CheckBorrowerAuthDocsForAssets(this, false);
                $('#CheckAllCB').prop('checked', $('.AssetsCB:checked').length === $('.AssetsCB').length);
                ToggleOrderBtn();
            });

            $('.SectionToggle').change(function() {
                TogglePickerSection(!isAssetPickerBeingUsed);
            });

            $('#BorrowerList').change(function() {
                CheckBorrowerAuthDocsForBorrowers(false);
                ToggleOrderBtn();
            });

            function RowComparer(sortTargetClass) {
                return function (a, b) {
                    var sortTargetA = $(a).find('.' + sortTargetClass);
                    var sortTargetB = $(b).find('.' + sortTargetClass);

                    var sortValueA;
                    var sortValueB;
                    if(sortTargetClass === 'Value') {
                        sortValueA = 0;
                        sortValueB = 0;
                        var aString = sortTargetA.text();
                        if(aString !== '' && aString.charAt(0) === '$') {
                            aString = aString.substr(1);
                            sortValueA = parseFloat(aString);
                        }

                        var bString = sortTargetB.text();
                        if(bString !== '' && bString.charAt(0) === '$') {
                            bString = bString.substr(1);
                            sortValueB = parseFloat(bString);
                        }
                    }
                    else {
                        sortValueA = sortTargetA.text();
                        sortValueB = sortTargetB.text();
                    }

                    if (sortValueA < sortValueB) {
                        return -1;
                    }
                    else if (sortValueA > sortValueB) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            }

            function BindNotificationEmail(askForNotificationEmail) {
                var notificationEmailRow = $('#NotificationEmailRow');
                var notificationEmail = $('#NotificationEmail');
                notificationEmailRow.toggle(askForNotificationEmail);
                notificationEmailRow.find('.RequiredImg').toggle(askForNotificationEmail && notificationEmail.val() === '');
                notificationEmail.toggleClass('RequiredInput', askForNotificationEmail);
            }

            function BindLenderServiceOptions(accountHistory, accountHistoryDefault, refreshPeriod, refreshPeriodDefault) {
                var accountHistoryDDL = $('#AccountHistoryOptions');
                var refreshPeriodDDL = $('#RefreshPeriodOptions');

                PopulateOptions(accountHistoryDDL, accountHistory, accountHistoryDefault);
                PopulateOptions(refreshPeriodDDL, refreshPeriod, refreshPeriodDefault);
            }

            function PopulateOptions(ddl, optionList, defaultValue) {
                ddl.prop('disabled', optionList.length == 1 || optionList.length == 0);
                if(optionList.length === 0) {
                    ddl.append($('<option>', {value: ''}).text("None"));
                }
                else {
                    for(var i = 0; i < optionList.length; i++) {
                        var option = optionList[i];
                        var optionString = option + " Days";
                        if(option == ML.MinOptionValue) {
                            optionString = "None";
                        }
                        else if(option == ML.MaxOptionValue) {
                            optionString = "Max Available";
                        }

                        ddl.append($('<option>', {value: option}).text(optionString));
                    }
                }

                if (defaultValue !== undefined)
                {
                    ddl.val(defaultValue);
                }
            }

            function ToggleOrderBtn() {
                var docsOk = authDocsEnabled ? AuthorizationDocPicker.AllPickersHaveDocs() : true;
                var inputsOk = $('.RequiredInput:visible').filter(function() { return $(this).val() === '' }).length === 0;
                var lenderService = $('#LenderServices').val();
                var lenderServiceChosen = lenderService != null && lenderService !== '';
                var accountHistoryOptionSelected = $('#AccountHistoryOptions').val() !== '';
                var refreshPeriodOptionSelected = $('#RefreshPeriodOptions').val() !== '';
                var assetChosen =  !isAssetPickerBeingUsed || $('.AssetsCB:checked').length > 0;
                var borrowerChosen = isAssetPickerBeingUsed || $('#BorrowerList').val() !== '';
                
                var isValidated = ML.CanOrderVoa && docsOk && inputsOk && lenderServiceChosen && accountHistoryOptionSelected && refreshPeriodOptionSelected && assetChosen && borrowerChosen;
                $('#OrderBtn').prop('disabled', !isValidated);
            }

            function CheckBorrowerAuthDocsForBorrowers(shouldCheckOrderBtn) {
                if(!authDocsEnabled) {
                    return;
                }
                var table = $('#DocumentTable');

                var $borrowerDdl = $('#BorrowerList');
                var key = $borrowerDdl.val();

                if(!key) {
                    AuthorizationDocPicker.RemoveDocPicker(null, table);
                }
                else {
                    var containsPicker = AuthorizationDocPicker.ContainsDocPicker(key, table);
                    if(!containsPicker) {
                        AuthorizationDocPicker.RemoveDocPicker(null, table);
                        var name = $borrowerDdl.find('option:selected').text();
                        var split = key.split('_');
                        var appId = split[0];
                        var ownerType = split[1];

                        var picker = AuthorizationDocPicker.CreateDocPicker(name, appId, ownerType, key);
                        table.append(picker);
                    }
                }

                if(shouldCheckOrderBtn) {
                    ToggleOrderBtn();
                }

                $('#BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, table));
            }

            function CheckBorrowerAuthDocsForAssets(assetCb, shouldCheckOrderBtn) {
                if(!authDocsEnabled) {
                    return;
                }

                var assetRow = $(assetCb).closest('tr');
                var appId = assetRow.find('.AppId').val();
                var ownerT = assetRow.find('.OwnerT').val();
                var ownerName = assetRow.find('.Owner').text();
                var key = assetRow.find('.Key').val();
                var table = $('#DocumentTable');

                if(assetCb.checked) {
                    // This got checked so if the picker doesn't already exist, add it
                    var containsPicker = AuthorizationDocPicker.ContainsDocPicker(key, table);
                    if(!containsPicker) {
                        var picker = AuthorizationDocPicker.CreateDocPicker(ownerName, appId, ownerT, key);
                        table.append(picker);
                    }
                }
                else {
                    // Unchecked, need to see if there are any more that are checked.
                    var anyMoreChecked = $('#AssetsTable').find('.Key[value="' + key + '"]').siblings('.AssetsCB:checked').length > 0;
                    if(!anyMoreChecked && AuthorizationDocPicker.ContainsDocPicker(key, table)) {
                        AuthorizationDocPicker.RemoveDocPicker(key, table);
                    }
                }

                if(shouldCheckOrderBtn) {
                    ToggleOrderBtn();
                }

                $('#BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, table));
            }

            function GatherRequestData()
            {
                var assetsInfo = [];
                $('#AssetsTable tbody').find('tr').each(function() {
                    var assetCbChecked = $(this).find('.AssetsCB').is(':checked');
                    if(assetCbChecked) {
                        var assetId = $(this).find('.AssetId').val();
                        var appId = $(this).find('.AppId').val();
                        var borrowerType = $(this).find('.OwnerT').val();

                        var data = {
                            BorrowerType: borrowerType,
                            AppId: appId,
                            AssetId: assetId
                        };

                        assetsInfo.push(data);
                    }
                });

                var data = {
                    LoanId: ML.sLId,
                    LenderService: $('#LenderServices').val(),
                    NotificationEmail: $('#NotificationEmail').is(':visible') ? $('#NotificationEmail').val() : '',
                    AccountHistory: $('#AccountHistoryOptions').val(),
                    RefreshPeriod: $('#RefreshPeriodOptions').val(),
                    PayWithCreditCard: $('#AllowPayWithCreditCard').is(':visible') ? $('#AllowPayWithCreditCard').is(':checked') : '',
                    AccountId: $('#AccountId').is(':visible') ? $('#AccountId').val() : '',
                    UserName: $('#Username').is(':visible') ? $('#Username').val() : '',
                    Password: $('#Password').is(':visible') ? $('#Password').val() : '',
                    ServiceCredentialId: serviceCredentialId,
                    DocPickerInfo: authDocsEnabled ? AuthorizationDocPicker.RetrieveAllDocPickerInfo($('#DocumentTable')).join(',') : '',
                    AssetInfo: JSON.stringify(assetsInfo),
                    BorrowerInfo: $('#BorrowerList').val(),
                    VerifiesBorrower: !isAssetPickerBeingUsed
                };

                return data;
            }

            function TogglePickerSection(useAssetPicker, canSwitch) 
            {
                $('.PickerSection').hide();
                if(typeof(canSwitch) === 'boolean') {
                    $('#SectionToggleSection').prop('disabled', !canSwitch).toggleClass('Hidden', !canSwitch).toggleClass('InlineBlock', canSwitch);

                    if(canSwitch && typeof(isAssetPickerBeingUsed) === 'boolean') {
                        // If switching is allowed with this new lender service, let it use the same selection picker.
                        useAssetPicker = isAssetPickerBeingUsed;
                    }
                }

                $('.AssetPickerSection').toggle(useAssetPicker);
                $('#AssetToggle').prop('checked', useAssetPicker);
                $('.BorrowerPickerSection').toggle(!useAssetPicker);
                $('#BorrowerToggle').prop('checked', !useAssetPicker);
                
                var oldIsAssetPickerBeingUsed = typeof(isAssetPickerBeingUsed) === 'boolean' ? isAssetPickerBeingUsed : useAssetPicker;
                isAssetPickerBeingUsed = useAssetPicker;

                if(oldIsAssetPickerBeingUsed !== isAssetPickerBeingUsed) {
                    $('.AssetsCB').prop('checked', false);
                    $('#BorrowerList').prop('selectedIndex', 0);
                    AuthorizationDocPicker.RemoveDocPicker(null, $('#DocumentTable'));

                    if(!isAssetPickerBeingUsed) {
                        CheckBorrowerAuthDocsForBorrowers(false);
                    }
                }

                $('#BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, $('#DocumentTable')));
                ToggleOrderBtn();
            }

            // On load
            $('.WarningIcon').toggle(!ML.CanOrderVoa).attr('title', ML.OrderVoaDenialReason);
            
            AuthorizationDocPicker.RegisterDocChangeCallback(ToggleOrderBtn);
            var validStatus = {
                0: false, // Pending
                1: true, // Complete
                5: true, // Active
            };

            VOXOrderCommon.InitializeCommonFunctions({
                RowComparer: RowComparer,
                GatherRequestData: GatherRequestData,
                ServiceType: "VOA",
                ToggleOrderBtn: ToggleOrderBtn,
                ValidStatusesAfterInitialRequest: validStatus
            });

            setTimeout(function() {
                $('#LenderServices').change();
            });
        });
    </script>
    <form id="form1" runat="server">
        <div>
            <div class="MainRightHeader">Order Verification of Deposit</div>
        </div>
        <div class="Padding5">
            <div>
                <div>
                    <div>
                        <div class="InlineBlock PaddingTopBottom5 Header3 align-top" id="SelectionHeader">
                            <span class="PickerSection AssetPickerSection">Select assets to verify</span>
                            <span class="PickerSection BorrowerPickerSection Hidden">Select borrower to verify</span>
                        </div>
                        <div class="Hidden" id="SectionToggleSection">
                            <div>
                                <label>
                                    <input type="radio" name="SectionToggle" class="SectionToggle" id="AssetToggle" checked="checked" />Select assets to verify
                                </label>
                            </div>
                            <div>
                                <label>
                                    <input type="radio" name="SectionToggle" class="SectionToggle" id="BorrowerToggle" />Select borrower to verify
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="PickerSection AssetPickerSection">
                        <table id="AssetsTable" class="Table">
                            <thead>
                                <tr class="LoanFormHeader">
                                    <td>
                                        <input type="checkbox" id="CheckAllCB" />
                                    </td>
                                    <td><a data-sort="true" data-sorttarget="Institution" data-asc="false">Institution</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="AccountNumber" data-asc="false">Account Number</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Owner" data-asc="false">Owner</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="AssetType" data-asc="false">Asset Type</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Description" data-asc="false">Description</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Value" data-asc="false">Value</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="OrderStatus" data-asc="false">Order Status</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="AssetsRepeater" runat="server">
                                    <ItemTemplate>
                                        <tr class="ReverseGridAutoItem">
                                            <td>
                                                <input type="checkbox" class="AssetsCB" />
                                                <input type="hidden" class="AssetId" id="AssetId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AssetId").ToString()) %>" />
                                                <input type="hidden" class="AppId" id="AppId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AppId").ToString()) %>" />
                                                <input type="hidden" class="OwnerT" id="OwnerT" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OwnerT").ToString()) %>" />
                                                <input type="hidden" class="Key" id="Key" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Key").ToString()) %>" />
                                            </td>
                                            <td>
                                                <span class="Institution" id="Institution"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Institution")) %></span>
                                            </td>
                                            <td>
                                                <span class="AccountNumber" id="AccountNumber"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AccountNum")) %></span>
                                            </td>
                                            <td>
                                                <span class="Owner" id="Owner"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Owner")) %></span>
                                            </td>
                                            <td>
                                                <span class="AssetType" id="AssetType"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AssetType")) %></span>
                                            </td>
                                            <td>
                                                <span class="Description" id="Description"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description")) %></span>
                                            </td>
                                            <td>
                                                <span class="Value" id="Value"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Value")) %></span>
                                            </td>
                                            <td>
                                                <span class="OrderStatus" id="OrderStatus"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OrderStatus")) %></span>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <div class="PaddingTop align-right">
                            <a id="AssetsLink">Please enter assets on the Assets page</a>
                        </div>
                    </div>
                    <div class="PickerSection BorrowerPickerSection Hidden">
                        <span class="FieldLabel">Borrower</span>
                        <asp:DropDownList runat="server" ID="BorrowerList"></asp:DropDownList>
                    </div>
                </div>
                <hr />
                <div class="CssTable FullWidthHeight">
                    <div class="CssTableCell RightBorder Padding5 Width50Pc">
                        <div class="PaddingTopBottom5 Header3">Order Options</div>
                        <table>
                            <tr class="no-wrap">
                                <td class="FieldLabel">Service Provider</td>
                                <td>
                                    <asp:DropDownList runat="server" ID="LenderServices"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="NotificationEmailRow" runat="server">
                                <td class="FieldLabel">Notification Email</td>
                                <td>
                                    <input type="text" class="InputLarge RequiredInput" id="NotificationEmail" runat="server"/>
                                    <img alt="Picker" class="align-middle" id="ContactPicker" src="../../images/contacts.png" />
                                    <img alt="Required" class="RequiredImg align-middle" src="../../images/require_icon_red.gif" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Account History</td>
                                <td>
                                    <asp:DropDownList runat="server" ID="AccountHistoryOptions"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Refresh Period</td>
                                <td>
                                    <asp:DropDownList runat="server" ID="RefreshPeriodOptions"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="CreditCardRow" runat="server">
                                <td colspan="2">
                                    <label class="FieldLabel">
                                        <input type="checkbox" id="AllowPayWithCreditCard" />Pay with credit card
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="CredentialsRow" class="LeftBorder CredentialItem CssTableCell Padding5 Width50Pc">
                        <div class="PaddingTopBottom5 Header3">Credentials</div>
                        <table>
                            <tr id="AccountIdRow" class="CredentialItem">
                                <td class="FieldLabel">Account ID</td>
                                <td>
                                    <input type="text" class="InputLarge" id="AccountId" runat="server" />
                                    <img alt="Required" class="RequiredImg Hidden" id="AccountIdImage" runat="server" src="../../images/require_icon_red.gif" />
                                </td>
                            </tr>
                            <tr id="UsernameRow" class="CredentialItem">
                                <td class="FieldLabel">Username</td>
                                <td>
                                    <input type="text" class="InputLarge RequiredInput" id="Username" />
                                    <img alt="Required" class="RequiredImg" id="UsernameImg" src="../../images/require_icon_red.gif" />
                                </td>
                            </tr>
                            <tr id="PasswordRow" class="CredentialItem">
                                <td class="FieldLabel">Password</td>
                                <td>
                                    <input type="password" class="InputLarge RequiredInput" id="Password" />
                                    <img alt="Required" class="RequiredImg" id="PasswordImg" src="../../images/require_icon_red.gif" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="BorrowerAuthDocsSection" runat="server">
                    <hr />
                    <div class="PaddingTopBottom5 Header3">Borrower Authorization Documents</div>
                    <div>
                        <table id="DocumentTable">
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="align-center">
            <input type="button" id="OrderBtn" value="Place Order"/>
            <img src=<%= AspxTools.SafeUrl(this.VirtualRoot + "/images/warning25x25.png") %> class="WarningIcon align-middle" alt="Warning" />
            <input type="button" id="CancelBtn" value="Cancel" />
        </div>
    </form>
</body>
</html>
