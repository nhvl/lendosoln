﻿//     Copyright (c) Meridian Link. All rights reserved.
// </copyright>
// <author>Eduardo Michel</author>
// <summary>This file is the verbal verification of employment editor.</summary>
//-----------------------------------------------------------------------

#region Generated Code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// This editor is for adding and editing verbal verifications of employment.
    /// </summary>
    public partial class VerbalVoeEditor : BaseLoanPage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The object that triggered this method.</param>
        /// <param name="e">The arguments sent along by the trigger.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The object that triggered this method.</param>
        /// <param name="e">The arguments sent along by the trigger.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate  = false;
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("mask.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");

            PDFPrintClass = typeof(LendersOffice.Pdf.VerbalVoePdf);

            EmployerZip.SmartZipcode(EmployerCity, EmployerState);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VerbalVoeEditor));
            dataLoan.InitLoad();

            CAppData app = dataLoan.GetAppData(ApplicationID);
            BorrowerDDL.Items.Add(new ListItem(string.Empty, string.Empty));
            BorrowerDDL.Items.Add(new ListItem(app.aBNm, app.aBNm));
            if (!string.IsNullOrEmpty(app.aCNm))
            {
                BorrowerDDL.Items.Add(new ListItem(app.aCNm, app.aCNm));
            }

            EmployerDDL.Items.Add(new ListItem(string.Empty, string.Empty));
            BindEmployerDDL(app.aBEmpCollection);
            BindEmployerDDL(app.aCEmpCollection);           

            LoadRecord(dataLoan);

            RegisterService("main", "/newlos/Verifications/VerbalVoeEditorService.aspx");
        }

        /// <summary>
        /// This method takes a collection of employment records, and adds their data to the Borrower dropdown list.
        /// </summary>
        /// <param name="employmentRecords">The employment records to be added to the dropdown.</param>
        protected void BindEmployerDDL(IEmpCollection employmentRecords)
        {
            foreach (DataRow row in employmentRecords.SortedView.Table.Rows)
            {
                if (row["EmplrNm"] != DBNull.Value)
                {
                    string displayText = (string)row["EmplrNm"];
                    string value = (string)row["EmplrNm"];
                    var item = new ListItem(displayText, value);

                    string address = SerializationHelper.JsonNetAnonymousSerialize(new
                    {
                        EmplrAddr = row["EmplrAddr"] != DBNull.Value ? (string)row["EmplrAddr"] : string.Empty,
                        EmplrCity = row["EmplrCity"] != DBNull.Value ? (string)row["EmplrCity"] : string.Empty,
                        EmplrState = row["EmplrState"] != DBNull.Value ? (string)row["EmplrState"] : string.Empty,
                        EmplrZip = row["EmplrZip"] != DBNull.Value ? (string)row["EmplrZip"] : string.Empty
                    });

                    item.Attributes.Add("address", address);
                    EmployerDDL.Items.Add(item);
                }
            }
        }

        /// <summary>
        /// Loads the record's value into the editor.
        /// </summary>
        /// <param name="dataLoan">The loan to load the record from.</param>
        protected void LoadRecord(CPageData dataLoan)
        {
            Guid id = RequestHelper.GetGuid("recordId", Guid.Empty);
            RecordId.Value = id.ToString();

            if (id != Guid.Empty)
            {
                CAppData app = dataLoan.GetAppData(ApplicationID);
                VerbalVerificationOfEmploymentRecord record = app.aVerbalVerificationsOfEmployment.FirstOrDefault(p => p.Id.Equals(id));

                int index = app.aVerbalVerificationsOfEmployment.IndexOf(record);

                indexOfMessage.Text = "( " + (index + 1) + " of " + app.aVerbalVerificationsOfEmployment.Count + " )";

                if (index > 0)
                {
                    PreviousButton.Attributes.Add("data-id", app.aVerbalVerificationsOfEmployment.ElementAt(index - 1).Id.ToString());
                }

                if (index < app.aVerbalVerificationsOfEmployment.Count - 1)
                {
                    NextButton.Attributes.Add("data-id", app.aVerbalVerificationsOfEmployment.ElementAt(index + 1).Id.ToString());
                }

                if (record == null)
                {
                    Tools.LogError("The Verbal VOE with id " + id.ToString() + " could not be found.");
                }
                else
                {
                    BorrowerDDL.SelectedValue = record.BorrowerName;

                    EmployerDDL.SelectedValue = record.EmployerName;
                    EmployerStreet.Value = record.EmployerStreet;
                    EmployerCity.Value = record.EmployerCity;
                    EmployerState.Value = record.EmployerState;
                    EmployerZip.Text = record.EmployerZip;

                    DateOfHire.Text = record.DateOfHire_rep;
                    DateOfTermination.Text = record.DateOfTermination_rep;
                    EmploymentStatus.Value = record.EmploymentStatus;
                    BorrowerTitle.Value = record.BorrowerTitle;
                    EmployerContactName.Value = record.EmployerContactName;
                    EmployerContactTitle.Value = record.EmployerContactTitle;

                    EmployerPhone.Value = record.EmployerPhone;
                    ThirdPartySource.Value = record.ThirdPartySource;

                    PreparedBy.Value = record.PreparedBy;
                    PreparedByTitle.Value = record.PreparedByTitle;
                    DateOfCall.Text = record.DateOfCall_rep;
                }
            }
            else
            {
                DateOfCall.Text = DateTime.Today.ToShortDateString();
            }
        }
    }
}
