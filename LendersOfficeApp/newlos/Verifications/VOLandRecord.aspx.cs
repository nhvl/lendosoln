namespace LendersOfficeApp.newlos.Verifications
{
    using System;
    using System.Data;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Pdf;

    /// <summary>
    /// Summary description for VOLandRecord.
    /// </summary>
    public partial class VOLandRecord : BaseSingleEditPage<IVerificationOfRent>
	{
        protected string m_openedDate;
        private CPageData m_dataLoan;

        protected override string ListLocation
        {
            get { return this.VirtualRoot + "/newlos/Verifications/Verifications.aspx?loanid=" + this.LoanID + "&pg=5"; }
        }

        protected override DataView GetDataView()
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("recordid", typeof(string)));

            CAppData dataApp = this.m_dataLoan.GetAppData(this.ApplicationID);
            int count = dataApp.GetVorRecordCount();

            for (int i = 0; i < count; i++)
            {
                IVerificationOfRent f = dataApp.GetVorFields(i);
                if (f.VerifT == E_VorT.LandContract)
                {
                    DataRow row = table.NewRow();
                    row["recordid"] = f.RecordId.ToString();
                    table.Rows.Add(row);
                }
            }

            return table.DefaultView;
        }

        protected override IVerificationOfRent RetrieveRecord()
        {
            CAppData dataApp = this.m_dataLoan.GetAppData(this.ApplicationID);

            return dataApp.GetVorFields(this.RecordID);
        }

        protected override void BindSingleRecord(IVerificationOfRent field)
        {
            LandlordCreditorName.Text = field.LandlordCreditorName;
            Attention.Text = field.Attention;
            AddressTo.Text = field.AddressTo;
            CityTo.Text = field.CityTo;
            StateTo.Value = field.StateTo;
            ZipTo.Text = field.ZipTo;
            AccNm.Text = field.AccountName;
            VerifExpD.Text = field.VerifExpD_rep;
            VerifRecvD.Text = field.VerifRecvD_rep;
            VerifReorderedD.Text = field.VerifReorderedD_rep;
            VerifSentD.Text = field.VerifSentD_rep;
            IsSeeAttachment.Checked = field.IsSeeAttachment;


            AddressFor.Text = field.AddressFor;
            CityFor.Text = field.CityFor;
            StateFor.Value = field.StateFor;
            ZipFor.Text = field.ZipFor;

            ClientScript.RegisterHiddenField("VerifSigningEmployeeId", field.VerifSigningEmployeeId.ToString());
            ClientScript.RegisterHiddenField("VerifHasSignature", field.VerifHasSignature.ToString());
        }

        protected override void LoadData()
        {
            this.m_dataLoan = new CBorrowerInfoData(this.LoanID);
            this.m_dataLoan.InitLoad();

            m_openedDate = this.m_dataLoan.sOpenedD_rep;
            sLenderNumVerif.Text = this.m_dataLoan.sLenderNumVerif;

            IPreparerFields f = this.m_dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfLandContract, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f.IsValid)
            {
                PreparerCompanyName.Text = f.CompanyName;
                PreparerStreetAddr.Text = f.StreetAddr;
                PreparerCity.Text = f.City;
                PreparerState.Value = f.State;
                PreparerZip.Text = f.Zip;

                PreparerName.Text = f.PreparerName;
                PreparerTitle.Text = f.Title;
                PreparerPrepareDate.Text = f.PrepareDate_rep;
            }

            base.LoadData();
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            if (RecordID == Guid.Empty || !this.Broker.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                ClientScript.RegisterHiddenField("VerifSigningEmployeeId", Guid.Empty.ToString());
                ClientScript.RegisterHiddenField("VerifHasSignature", bool.FalseString);
                SigRow.Style.Add("display", "none");
            }

            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(BrokerUser.BrokerId, BrokerUser.EmployeeId, BrokerUser.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
            ClientScript.RegisterHiddenField("EmployeeHasSignature", signInfo.HasUploadedSignature.ToString());
            ClientScript.RegisterHiddenField("EmployeeName", BrokerUser.DisplayName);

            this.PageTitle = "VOLand record";
            this.PDFPrintClass = typeof(CVOLandPDF);
            ZipFor.SmartZipcode(CityFor, StateFor);
            ZipTo.SmartZipcode(CityTo, StateTo);
            PreparerZip.SmartZipcode(PreparerCity, PreparerState);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.PageInit);
        }
		#endregion
	}
}
