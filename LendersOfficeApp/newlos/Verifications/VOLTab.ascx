<%@ Import Namespace="LendersOfficeApp.newlos.Verifications"%>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="VOLTab.ascx.cs" Inherits="LendersOfficeApp.newlos.Verifications.VOLTab" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>



<script type="text/javascript">
<!--
<% if (IsReadOnly) { %>
      function f_disableAll() {
        var coll = document.getElementsByTagName("input");      
        var length = coll.length;
        if (length > 0) {
          for (i = 0; i < length; i++) {
            var o = coll[i];
            if (o.type=="text") o.readOnly = true;
            else if (o.type =="checkbox") o.disabled = true;
          }
        }   
        
        coll = document.getElementsByTagName("textarea");      
        length = coll.length;
        if (length > 0) {
          for (i = 0; i < length; i++) {
            var o = coll[i];
            o.style.backgroundColor = gReadonlyBackgroundColor;
            o.readOnly = true;
          }
        }              
      }
      function _init() {
        f_disableAll();
      }
<% } %>
function showVOLEdit(id) {
  linkMe('VOLRecord.aspx', 'recordid=' + id);
    }

    function downloadPDF(event) {
        var $el = $(retrieveEventTarget(event));
        var url = VRoot + '/' + $el.attr('href');
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);

        event.preventDefault();
        return false;
    }
//-->
</script>

<table>
<ml:DateTextBox ID="HiddenDTBox" Visible="false" runat="server"></ml:DateTextBox>
  <tr>
    <td class=FieldLabel><ml:EncodedLiteral id="m_noRecordLabel"
       Text="No loan to verify" Visible="False"
      EnableViewState="False" runat="server"></ml:EncodedLiteral></TD></TR></TABLE>
<ml:CommonDataGrid id=m_dg runat="server">
  <alternatingitemstyle cssclass="GridAlternatingItem"/>
  <itemstyle cssclass="GridItem" />
  <headerstyle cssclass="GridHeader" />
  <columns>
    <asp:templatecolumn>
      <itemtemplate>(<a href='#' onclick=<%# AspxTools.HtmlAttribute("showVOLEdit(" + AspxTools.JsString(((VerificationData) Container.DataItem).RecordID) + ")") %>>edit</a>)</itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn>
      <itemtemplate>(<a onclick="downloadPDF(event);" href=<%# AspxTools.SafeUrl("pdf/VOL.aspx?loanid=" + this.LoanID + "&applicationid=" + ((VerificationData)Container.DataItem).ApplicationID + "&recordid=" + ((VerificationData)Container.DataItem).RecordID) %>>preview</a>)</itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="B/C/J">
      <itemtemplate><%# AspxTools.HtmlString(((VerificationData)Container.DataItem).OtherDescription0) %></itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Name">
      <itemtemplate>
      <%# AspxTools.HtmlString(((VerificationData)Container.DataItem).Description) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Ordered Date">
      <itemtemplate>
<%# AspxTools.HtmlControls(CreateDateTextBox(((VerificationData)Container.DataItem).RecordID, "VerifSentD", ((VerificationData)Container.DataItem).VerifSentD)) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Reordered Date">
      <itemtemplate>
<%# AspxTools.HtmlControls(CreateDateTextBox(((VerificationData) Container.DataItem).RecordID, "VerifReorderedD", ((VerificationData) Container.DataItem).VerifReorderedD)) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Received Date">
      <itemtemplate>
<%# AspxTools.HtmlControls(CreateDateTextBox(((VerificationData) Container.DataItem).RecordID, "VerifRecvD", ((VerificationData) Container.DataItem).VerifRecvD)) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Expected Date">
      <itemtemplate>
<%# AspxTools.HtmlControls(CreateDateTextBox(((VerificationData) Container.DataItem).RecordID, "VerifExpD", ((VerificationData) Container.DataItem).VerifExpD)) %>
      </itemtemplate>
    </asp:templatecolumn>
  </columns>
</ml:CommonDataGrid>
