﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VOXStatusList.aspx.cs" Inherits="LendersOfficeApp.newlos.Verifications.VOXStatusList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Order Status History</title>
    <script type="text/javascript">
        $(function () {
            if (ML.AutoClose) {
                parent.LQBPopup.Return();
                return;
            }
            $.each(JSON.parse(ML.StatusList), function (index, value) {
                var date = new Date(value.StatusDateTime);
                var dateString = value.StatusDateTime ? value.StatusDateTime.substring(0, value.StatusDateTime.lastIndexOf(',')) : 'Unknown';
                var timeString = value.StatusDateTime ? value.StatusDateTime.substring(value.StatusDateTime.lastIndexOf(',') + 1, value.StatusDateTime.length) : 'Unknown';
                var row = $('<tr class="ReverseGridAutoItem"><td>'
                    + dateString +
                    '</td><td>'
                    + timeString +
                    '</td><td>'
                    + value.StatusCode +
                    '</td><td>'
                    + value.StatusDescription +
                    '</td></tr>');
                $('#StatusTable tbody').append(row);
            });
        });
    </script>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }
    </style>
</head>
<body>
    <h4 class="page-header" runat="server" id="PageTitleHeader">Order Status History</h4>
    <form id="form1" runat="server">
        <div>
            <table id="StatusTable" class="Table">
                <thead>
                    <tr class="LoanFormHeader">
                        <th>
                            Date
                        </th>
                        <th>
                            Time
                        </th>
                        <th>
                            Status Code
                        </th>
                        <th>
                            Status Description
                        </th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
