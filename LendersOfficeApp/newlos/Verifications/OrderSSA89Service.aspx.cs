﻿#region Auto generated code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Conversions.Templates;
    using LendersOffice.Integration.Templates;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// Service page for SSA-89 orders.
    /// </summary>
    public partial class OrderSSA89Service : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Runs a service method.
        /// </summary>
        /// <param name="methodName">The method name.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.LoadLenderService):
                    this.LoadLenderService();
                    break;
                case nameof(this.RunAudit):
                    this.RunAudit();
                    break;
                case nameof(this.RunRequest):
                    this.RunRequest();
                    break;
                case "PollOrder":
                    this.SendSubsequentRequest(VOXRequestT.Get);
                    break;
                case "RefreshOrder":
                    this.SendSubsequentRequest(VOXRequestT.Refresh);
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, $"Invalid method {methodName}.");
            }
        }

        /// <summary>
        /// Loads a lender service to update the UI.
        /// </summary>
        private void LoadLenderService()
        {
            Guid loanId = GetGuid("LoanId");
            int lenderServiceId = GetInt("LenderServiceId");
            VOXLenderService lenderService = VOXLenderService.GetLenderServiceById(PrincipalFactory.CurrentPrincipal.BrokerId, lenderServiceId);
            VOXVendor vendor = lenderService.VendorForVendorAndServiceData;
            AbstractVOXVendorService service;
            if (vendor.Services.TryGetValue(VOXServiceT.SSA_89, out service))
            {
                SSA89VendorService voaService = (SSA89VendorService)service;
                this.SetResult("RequiresAccountId", lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId);
                this.SetResult("UsesAccountId", lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId);

                var principal = PrincipalFactory.CurrentPrincipal;
                var loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderSSA89Service));
                loanData.InitLoad();

                var serviceCredential = ServiceCredential.ListAvailableServiceCredentials(principal, loanData.sBranchId, ServiceCredentialService.Verifications)
                    .FirstOrDefault(credential => credential.VoxVendorId.HasValue && credential.VoxVendorId == lenderService.VendorForProtocolAndTransmissionData.VendorId);
                if (serviceCredential != null)
                {
                    this.SetResult("ServiceCredentialId", serviceCredential.Id);
                    this.SetResult("ServiceCredentialHasAccountId", !string.IsNullOrEmpty(serviceCredential.AccountId));
                }

                this.SetResult("Success", true);
            }
            else
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", "Unable to load Service Provider");
            }
        }

        /// <summary>
        /// Runs the audit for SSA-89 orders.
        /// </summary>
        private void RunAudit()
        {
            ServiceCredential usedServiceCredential;
            SSA89RequestData requestData = this.GetRequestData(out usedServiceCredential);
            if (requestData == null)
            {
                return;
            }

            SSA89RequestHandler requestHandler = new SSA89RequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            List<KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>> auditResultsForUi = new List<KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>>();
            foreach (var section in auditResults.SectionNames)
            {
                KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>> errorsForSection = new KeyValuePair<string, IEnumerable<IntegrationAuditErrorItem>>(section, auditResults.GetErrorsForSection(section));
                auditResultsForUi.Add(errorsForSection);
            }

            SetResult("Success", true);
            SetResult("AuditResults", SerializationHelper.JsonNetSerialize(auditResultsForUi));
            SetResult("AuditPassed", !auditResults.HasErrors);
        }

        /// <summary>
        /// Runs an initial SSA 89 request.
        /// </summary>
        private void RunRequest()
        {
            ServiceCredential usedServiceCredential;
            SSA89RequestData requestData = this.GetRequestData(out usedServiceCredential);
            if (requestData == null)
            {
                return;
            }

            SSA89RequestHandler requestHandler = new SSA89RequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            if (auditResults.HasErrors)
            {
                List<string> errorsFromVendor = new List<string>()
                {
                    "Invalid data. Please run the audit again."
                };

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(errorsFromVendor));
                return;
            }

            var results = requestHandler.SubmitRequest(doAudit: false);
            IEnumerable<string> errors = results?.Errors;
            var initialOrders = results?.Orders;
            if (initialOrders == null || !initialOrders.Any() ||
                (errors != null && errors.Any()))
            {
                List<string> requestErrors = new List<string>() { "Unable to fulfill order request." };
                if (errors != null && errors.Any())
                {
                    requestErrors = errors.ToList();
                }

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(requestErrors));
                return;
            }

            List<SSA89OrderViewModel> orderViewModels = new List<SSA89OrderViewModel>();
            List<Guid> edocIdsToStitch = new List<Guid>();
            foreach (SSA89Order order in initialOrders)
            {
                ExtraOrderViewModelInfo extraInfo = new ExtraOrderViewModelInfo()
                {
                    RequiresAccountId = requestData.LenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId,
                    UsesAccountId = requestData.LenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId,
                    AvailableEdocs = EDocumentRepository.GetUserRepository().GetDocumentIdsByLoanId(requestData.LoanId)
                };

                if (usedServiceCredential != null)
                {
                    extraInfo.ServiceCredentialId = usedServiceCredential.Id;
                    extraInfo.ServiceCredentialHasAccountId = !string.IsNullOrEmpty(usedServiceCredential.AccountId);
                }

                SSA89OrderViewModel model = order.CreateViewModel(extraInfo);
                orderViewModels.Add(model);

                if (model.AssociatedEdocs.Any())
                {
                    edocIdsToStitch.AddRange(model.AssociatedEdocs.Where((edoc) => edoc.EDocId != Guid.Empty && edoc.IsAvailable).Select((edoc) => edoc.EDocId));
                }
            }

            this.SetResult("Orders", SerializationHelper.JsonNetSerialize(orderViewModels));
            this.SetResult("Success", true);

            if (edocIdsToStitch.Any())
            {
                var joinedIds = string.Join("|", edocIdsToStitch);
                var key = AutoExpiredTextCache.AddToCache(joinedIds, TimeSpan.FromMinutes(5));
                SetResult("Key", key);
            }
        }

        /// <summary>
        /// Runs a subsequent request (poll/refresh).
        /// </summary>
        /// <param name="type">The type of request to run.</param>
        private void SendSubsequentRequest(VOXRequestT type)
        {
            string accountId = this.GetString("AccountId", string.Empty);
            string username = this.GetString("Username", string.Empty);
            string password = this.GetString("Password", string.Empty);
            Guid loanId = this.GetGuid("LoanId");
            int previousOrderId = this.GetInt("OrderId");
            int orderId = this.GetInt("OrderId");

            SSA89Order previousOrder = SSA89Order.GetOrderForLoan(PrincipalFactory.CurrentPrincipal.BrokerId, loanId, orderId);
            if (previousOrder == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(new List<string>() { "Order not found." }));
                return;
            }

            if (previousOrder.UpdateForLenderService() && previousOrder.CurrentStatus == VOXOrderStatusT.Error)
            {
                SSA89OrderViewModel errorModel = previousOrder.CreateViewModel(null);
                SetResult("UpdatedOrder", SerializationHelper.JsonNetSerialize(errorModel));
                SetResult("Success", true);
                return;
            }

            SSA89RequestData requestData = new SSA89RequestData(PrincipalFactory.CurrentPrincipal, loanId, type, previousOrder)
            {
                AccountId = accountId,
                Username = username,
                Password = password
            };

            var principal = PrincipalFactory.CurrentPrincipal;
            var loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderSSA89Service));
            loanData.InitLoad();

            var serviceCredential = ServiceCredential.ListAvailableServiceCredentials(principal, loanData.sBranchId, ServiceCredentialService.Verifications)
                                            .FirstOrDefault(credential => credential.VoxVendorId.HasValue && credential.VoxVendorId == requestData.LenderService.VendorForProtocolAndTransmissionData.VendorId);
            if (serviceCredential != null)
            {
                requestData.Username = serviceCredential.UserName;
                requestData.Password = serviceCredential.UserPassword.Value;
                requestData.AccountId = string.IsNullOrEmpty(serviceCredential.AccountId) ? accountId : serviceCredential.AccountId;
            }

            string validationError;
            if (!requestData.ValidateRequestData(out validationError))
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", SerializationHelper.JsonNetSerialize(new List<string>() { validationError }));
                return;
            }

            SSA89RequestHandler requestHandler = new SSA89RequestHandler(requestData);
            var auditResults = requestHandler.AuditRequest();
            if (auditResults.HasErrors)
            {
                List<string> errorsFromVendor = new List<string>();
                foreach (var auditErrors in auditResults.GetAllErrors())
                {
                    errorsFromVendor.Add(auditErrors.ErrorMessage);
                }

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(errorsFromVendor));
                return;
            }

            var results = requestHandler.SubmitRequest(doAudit: false);
            IEnumerable<string> errors = results?.Errors;
            SSA89Order updatedOrder = results?.Orders?.FirstOrDefault() as SSA89Order;
            if (updatedOrder == null ||
                (errors != null && errors.Any()))
            {
                List<string> errorsForUi = new List<string>() { "Unable to fulfill order request." };
                if (errors != null && errors.Any())
                {
                    errorsForUi = errors.ToList();
                }

                SetResult("Success", false);
                SetResult("Errors", SerializationHelper.JsonNetSerialize(errorsForUi));
                return;
            }

            var lenderService = requestData.LenderService;
            if (updatedOrder.LenderServiceId != requestData.LenderServiceId)
            {
                lenderService = VOXLenderService.GetLenderServiceById(PrincipalFactory.CurrentPrincipal.BrokerId, updatedOrder.LenderServiceId);
            }

            ExtraOrderViewModelInfo extraInfo = new ExtraOrderViewModelInfo()
            {
                RequiresAccountId = lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId,
                UsesAccountId = lenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId,
                AvailableEdocs = EDocumentRepository.GetUserRepository().GetDocumentIdsByLoanId(requestData.LoanId)
            };

            if (serviceCredential != null)
            {
                extraInfo.ServiceCredentialHasAccountId = !string.IsNullOrEmpty(serviceCredential.AccountId);
                extraInfo.ServiceCredentialId = serviceCredential.Id;
            }

            SSA89OrderViewModel model = updatedOrder.CreateViewModel(extraInfo);

            SetResult("UpdatedOrder", SerializationHelper.JsonNetSerialize(model));
            SetResult("Success", true);

            if (model.AssociatedEdocs.Count > 0)
            {
                var joinedIds = string.Join("|", model.AssociatedEdocs.Where(edoc => edoc.EDocId != Guid.Empty && edoc.IsAvailable).Select(edoc => edoc.EDocId));
                var key = AutoExpiredTextCache.AddToCache(joinedIds, TimeSpan.FromMinutes(5));
                SetResult("Key", key);
            }
        }

        /// <summary>
        /// Gets the request data for an initial SSA-89 order.
        /// </summary>
        /// <param name="usedServiceCredential">The service credential used in the request data, if any.</param>
        /// <returns>The data if successful, null otherwise.</returns>
        private SSA89RequestData GetRequestData(out ServiceCredential usedServiceCredential)
        {
            usedServiceCredential = null;

            Guid loanId = this.GetGuid("LoanId");
            int lenderService = this.GetInt("LenderService");
            string accountId = this.GetString("AccountId");
            string userName = this.GetString("UserName");
            string password = this.GetString("Password");
            int? serviceCredentialId = this.GetInt("ServiceCredentialId", -1);
            Guid appId = this.GetGuid("AppId");
            E_BorrowerModeT borrowerType = this.GetEnum<E_BorrowerModeT>("BorrowerType");

            List<VOXBorrowerAuthDoc> docPickerInfo = new List<VOXBorrowerAuthDoc>();
            string docPickerString = this.GetString("DocPickerInfo");
            if (!string.IsNullOrEmpty(docPickerString))
            {
                foreach (var info in docPickerString.Split(','))
                {
                    var infoSplit = info.Split('|');
                    E_BorrowerModeT borrowerOwner;
                    if (!Enum.TryParse(infoSplit[2], out borrowerOwner) && Enum.IsDefined(typeof(E_BorrowerModeT), borrowerOwner))
                    {
                        SetResult("Success", false);
                        SetResult("Errors", "Invalid asset owner type.");
                        return null;
                    }

                    docPickerInfo.Add(new VOXBorrowerAuthDoc(Guid.Parse(infoSplit[0]), Guid.Parse(infoSplit[1]), borrowerOwner == E_BorrowerModeT.Coborrower));
                }
            }

            SSA89RequestData requestData = new SSA89RequestData(PrincipalFactory.CurrentPrincipal, lenderService, loanId, VOXRequestT.Initial)
            {
                AccountId = accountId,
                Username = userName,
                Password = password,
                BorrowerAuthDocs = docPickerInfo,
                AppIdForChosenBorrower = appId,
                IsChosenBorrowerACoborrower = borrowerType == E_BorrowerModeT.Coborrower
            };

            var principal = PrincipalFactory.CurrentPrincipal;
            var loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderSSA89Service));
            loanData.InitLoad();

            var serviceCredential = ServiceCredential.ListAvailableServiceCredentials(principal, loanData.sBranchId, ServiceCredentialService.Verifications)
                                            .FirstOrDefault(credential => credential.VoxVendorId.HasValue && credential.VoxVendorId == requestData.LenderService.VendorForProtocolAndTransmissionData.VendorId);
            if ((serviceCredential != null && serviceCredentialId != serviceCredential.Id) ||
                (serviceCredential == null && (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))))
            {
                SetResult("Success", false);
                SetResult("Errors", "Invalid credentials.");
                return null;
            }

            if (serviceCredential != null)
            {
                usedServiceCredential = serviceCredential;
                requestData.Username = serviceCredential.UserName;
                requestData.Password = serviceCredential.UserPassword.Value;
                requestData.AccountId = string.IsNullOrEmpty(serviceCredential.AccountId) ? accountId : serviceCredential.AccountId;
                requestData.UsedServiceCredential = serviceCredential;
            }

            string errors;
            if (!requestData.ValidateRequestData(out errors))
            {
                SetResult("Success", false);
                SetResult("Errors", errors);
                return null;
            }

            return requestData;
        }
    }
}