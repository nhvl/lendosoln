﻿//-----------------------------------------------------------------------
// <copyright file="VerbalVoeEditorService.aspx.cs" company="Merdidian Link">
//     Copyright (c) Meridian Link. All rights reserved.
// </copyright>
// <author>Eduardo Michel</author>
// <summary>This file is the verbal verification of employment editor.</summary>
//-----------------------------------------------------------------------
#region Generated Code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// This file is used to save the changes in the Verbal VOE editor.
    /// </summary>
    public partial class VerbalVoeEditorService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets the loans id.
        /// </summary>
        /// <value>The id of the loan.</value>
        protected Guid LoanId
        {
            get
            {
                return GetGuid("loanid", Guid.Empty);
            }
        }

        /// <summary>
        /// Gets the application's id.
        /// </summary>
        /// <value>The id of the appliation.</value>
        protected Guid ApplicationId
        {
            get
            {
                return GetGuid("applicationid", Guid.Empty);
            }
        }

        /// <summary>
        /// Gets the record's id.
        /// </summary>
        /// <value>The id of the record being updated.</value>
        protected Guid RecordId
        {
            get
            {
                return GetGuid("RecordId", Guid.Empty);
            }
        }

        /// <summary>
        /// Gets a list of record ids.
        /// </summary>
        /// <value>The list of ids for records that will be updated.</value>
        protected List<Guid> RecordIds
        {
            get
            {
                string stringIds = GetString("RecordIds", string.Empty);
                if (string.IsNullOrEmpty(stringIds))
                {
                    return null;
                }
                else
                {
                    return SerializationHelper.JsonNetDeserialize<List<Guid>>(stringIds);
                }
            }
        }

        /// <summary>
        /// This method takes in a method's name and calls the corresponding method.
        /// </summary>
        /// <param name="methodName">The name of the method to be called.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Save":
                    this.Save();
                    break;
                case "Delete":
                    this.Delete();
                    break;
                default:
                    throw new CBaseException("Unhandled method", "There is no method by the name " + methodName);
            }
        }

        /// <summary>
        /// This method takes in a list of record ids and deletes their corresponding records.
        /// </summary>
        protected void Delete()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanId, typeof(VerbalVoeEditorService));
            dataLoan.InitSave(GetInt("sFileVersion"));

            CAppData dataApp = dataLoan.GetAppData(this.ApplicationId);
            var records = dataApp.aVerbalVerificationsOfEmployment;

            records.RemoveAll(p => this.RecordIds.Contains(p.Id));

            dataApp.aVerbalVerificationsOfEmployment = records;
            dataLoan.Save();
        }

        /// <summary>
        /// This method saves the changes.
        /// </summary>
        protected void Save()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(this.LoanId, typeof(VerbalVoeEditorService));
            dataLoan.InitSave(GetInt("sFileVersion"));

            CAppData dataApp = dataLoan.GetAppData(this.ApplicationId);

            List<VerbalVerificationOfEmploymentRecord> records = dataApp.aVerbalVerificationsOfEmployment;

            VerbalVerificationOfEmploymentRecord record;

            if (this.RecordId.Equals(Guid.Empty))
            {
                record = new VerbalVerificationOfEmploymentRecord();
                records.Add(record);
            }
            else
            {
                record = records.FirstOrDefault(p => p.Id.Equals(this.RecordId));
            }

            if (record == null)
            {
                Tools.LogError("The Verbal VOE with id " + this.RecordId.ToString() + " could not be found.");
                SetResult("Message", "Error.");
            }
            else
            {
                record.BorrowerName = this.GetString("BorrowerDDL");
                record.EmployerName = this.GetString("EmployerDDL");
                record.EmployerStreet = this.GetString("EmployerStreet");
                record.EmployerCity = this.GetString("EmployerCity");
                record.EmployerState = this.GetString("EmployerState");
                record.EmployerZip = this.GetString("EmployerZip");
                record.EmployerPhone = this.GetString("EmployerPhone");
                record.ThirdPartySource = this.GetString("ThirdPartySource");

                record.EmploymentStatus = this.GetString("EmploymentStatus");
                record.BorrowerTitle = this.GetString("BorrowerTitle");
                record.EmployerContactName = this.GetString("EmployerContactName");
                record.EmployerContactTitle = this.GetString("EmployerContactTitle");

                record.DateOfHire_rep = this.GetString("DateOfHire");
                record.DateOfTermination_rep = this.GetString("DateOfTermination");
                record.DateOfCall_rep = this.GetString("DateOfCall");

                record.PreparedBy = this.GetString("PreparedBy");
                record.PreparedByTitle = this.GetString("PreparedByTitle");

                dataApp.aVerbalVerificationsOfEmployment = records;

                dataLoan.Save();
            }

            if (records.Count > 2)
            {
                this.SetResult("previousId", records.ElementAt(records.Count - 2).Id);
            }

            this.SetResult("RecordId", record.Id);
        }
    }
}