<%@ Page language="c#" Codebehind="VOERecord.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Verifications.VOERecord" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>VOERecord</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
        <style type="text/css">
        .verif_headers { text-decoration: underline; }
        #ApplySig, #ClearSig {   display: block;  margin-right: 10px; float:left;  }
        .clear { clear: both; }
    </style>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	<script type="text/javascript">
    <!--
    var openedDate = <%= AspxTools.JsString(m_openedDate) %>;

    function onDateKeyUp(o, event) {
        if (event.keyCode == 79) {
            o.value = openedDate;
            updateDirtyBit(event);
        }
    }
  
    function printVOE() {
      PolyShouldShowConfirmSave(isDirty(), function(){
        var recordid = document.getElementById('RecordID').value; ;
        var isborrower = document.getElementById('OwnerT').value == "0" ? "True" : "False";
          var url = 'pdf/VOE.aspx?loanid=' + ML.sLId + '&applicationid=' + ML.aAppId + '&recordid=' + recordid + '&isborrower=' + isborrower + '&crack=' + new Date();
          LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
      }, saveMe);

    }

    function _init() {
        var enableSignature = document.getElementById('EnableSignature').value === 'True';
        if (!enableSignature) {
            document.getElementById('SignatureRow').style.display = "none";
            return;
        }

        var hasSignature = document.getElementById('EmployeeHasSignature').value === 'True';
        var missingSignature = document.getElementById('MissingSignature');
        missingSignature.style.display = hasSignature ? 'none' : '';
        var applySig = document.getElementById('ApplySig');
        applySig.style.display = hasSignature ? 'block' : 'none';
        postPopulateForm();
    }
    
    function postPopulateForm() {
        var verifHasSignature = document.getElementById('VerifHasSignature').value === 'True';
        var verifSigningEmployeeId = document.getElementById('VerifSigningEmployeeId').value; 
        var clearSig = document.getElementById('ClearSig');
        clearSig.style.display = verifHasSignature ? 'block' : 'none';
        var img = document.getElementById('SignatureImg');
        
        if( verifHasSignature ) {
            img.src = 'UserSignature.aspx?eid=' + verifSigningEmployeeId;
        }
        else {
            img.src = 'UserSignature.aspx';
        }
    }
    
    function onclearsig() {
        var isborrower = document.getElementById('OwnerT').value == "0" ? "True" : "False";

        if( false == confirm('Remove existing signature from verification?') ) {
            return;
        }   
        var recordId = document.getElementById('RecordID').value;
        var img = document.getElementById('SignatureImg');
        
        var args = { 
            'recordid' : recordId,
             'loanid'  : ML.sLId,
            'applicationid' :  ML.aAppId,
            'isborrower' : isborrower
             
        };
        var results = gService.singleedit.call('ClearSig', args);
        if( results.error ) {
            alert(results.UserMessage);
        }
        else {
            img.src = "UserSignature.aspx";
            document.getElementById('VerifHasSignature').value = 'False';
            document.getElementById('VerifSigningEmployeeId').value = '00000000-0000-0000-0000-000000000000';

        }
        return false;
        
    }
    
    function onapplysig() {
        var isborrower = document.getElementById('OwnerT').value == "0" ? "True" : "False";
        var username = document.getElementById('EmployeeName').value;
        var verifName = document.getElementById('PreparerName');
        
        if( username != verifName.value ) {
            if( false == confirm('Your name will be populated to match your signature. Review other fields for accuracy.') ) {
                return; 
            }
            updateDirtyBit();
            verifName.value = username;
        }
        
        var recordId = document.getElementById('RecordID').value;
        var img = document.getElementById('SignatureImg');
        var clearSig = document.getElementById('ClearSig');
        
        
        
        var args = { 
            'recordid' : recordId,
            'loanid' : ML.sLId,
            'applicationid' :  ML.aAppId,
            'isborrower' : isborrower
        };
        
        
        var results = gService.singleedit.call('ApplySig', args);
        if( results.error ) {
            alert(results.UserMessage);
        }
        else {
            img.src = "UserSignature.aspx?eid="+ results.value.EmployeeId ;
            clearSig.style.display = 'block';
            document.getElementById('VerifHasSignature').value = 'True';
            document.getElementById('VerifSigningEmployeeId').value = results.value.EmployeeId;
        }
        
        return false;
    }
    //-->
    </script>
    <form id="VOERecord" method="post" runat="server">
    <table class=FormTable id=Table1 cellSpacing=0 cellPadding=0 border=0>
      <tr>
        <td class=MainRightHeader noWrap>Verification of Employment <span id="OwnerNameLabel"><ml:EncodedLiteral id="m_nameLabel" runat="server" EnableViewState="False"></ml:EncodedLiteral></span> <span id=IndexLabel></span></td>
      </tr>
      <tr>
        <TD noWrap>
          <TABLE id=Table5 cellSpacing=0 cellPadding=0 border=0>
            <TR>
              <TD vAlign=top noWrap>
                <TABLE class=InsetBorder id=Table6 cellSpacing=0 cellPadding=0 border=0>
                  <TR>
                    <TD noWrap>
                      <TABLE id=Table3 cellSpacing=0 cellPadding=0 border=0>
                        <TR><TD class=FieldLabel noWrap colSpan=2><span class="verif_headers">To:</span></TD></TR>
                        <TR><TD class=FieldLabel noWrap>Attn</TD>
                            <TD noWrap><asp:TextBox id=Attention runat="server"></asp:TextBox></TD>
                        </TR>
                        <TR>
                      <TD class=FieldLabel noWrap> Name</TD>
                      <TD noWrap><asp:TextBox id=EmplrNm runat="server" Width="203px" ReadOnly="True"></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap> Address</TD>
                      <TD noWrap><asp:TextBox id=EmplrAddr runat="server" Width="203px" ReadOnly="True"></asp:TextBox></TD></TR>
                    <TR>
                      <TD noWrap></TD>
                      <TD noWrap><asp:TextBox id=EmplrCity runat="server" Width="109px" ReadOnly="True"></asp:TextBox><asp:TextBox id=EmplrState runat="server" Width="40px" ReadOnly="True"></asp:TextBox><asp:TextBox id=EmplrZip runat="server" Width="53px" ReadOnly="True"></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Phone</TD>
                      <TD noWrap><ml:PhoneTextBox id=EmplrBusPhone runat="server" width="120" preset="phone" ReadOnly="True"></ml:PhoneTextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Fax</TD>
                      <TD noWrap><ml:PhoneTextBox id=EmplrFax runat="server" width="120" preset="phone" ReadOnly="True"></ml:PhoneTextBox></TD></TR></TABLE></TD></TR></TABLE></TD>
          <TD vAlign=top noWrap>
            <TABLE class=InsetBorder id=Table7 cellSpacing=0 cellPadding=0 border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table2 cellSpacing=0 cellPadding=0 border=0>
                    <TR>
                      <TD class=FieldLabel noWrap colSpan=2><span class="verif_headers">From:</span></TD></TR>

                    <TR>
                      <TD class=FieldLabel noWrap>Name</TD>
                      <TD noWrap><asp:TextBox id=PreparerName runat="server" Width="229px"></asp:TextBox></TD></TR>
                                             <TR id="SignatureRow">
                        <TD class=FieldLabel valign="top">Apply Signature</TD>
                        <TD width="207px">
                           <img src="UserSignature.aspx" alt="Signature" id="SignatureImg" width="168" />
                            <a href="#" style="display:none" id="ApplySig" onclick="return onapplysig()">apply my signature</a> <a
                                href="#" style="display:none"  id="ClearSig" onclick="return onclearsig()">clear signature</a>
                            <br class="clear" />
                            <span id="MissingSignature" style="display:none">
                            To use your signature, upload your signature image to your profile.
                            </span>
                            <br />
                        </TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Title</TD>
                      <TD noWrap><asp:TextBox id=PreparerTitle runat="server" Width="229px"></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Lender Number</TD>
                      <TD noWrap><asp:TextBox id=sLenderNumVerif runat="server" Width="229px"></asp:TextBox></TD></TR>
                    <tr>
                      <td class=FieldLabel nowrap>Broker Name</td>
                      <td nowrap><asp:TextBox id=PreparerCompanyName runat="server" Width="229px"></asp:TextBox></td></tr>
                    <TR>
                      <TD class=FieldLabel noWrap>Broker Address</TD>
                      <TD noWrap><asp:TextBox id=PreparerStreetAddr runat="server" Width="229px" ></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap></TD>
                      <TD noWrap><asp:TextBox id=PreparerCity runat="server" Width="122px" ></asp:TextBox><ml:StateDropDownList id=PreparerState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=PreparerZip runat="server" preset="zipcode" width="50"></ml:ZipcodeTextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Phone</TD>
                      <TD noWrap><ml:PhoneTextBox id=PreparerPhone runat="server" width="120" preset="phone"></ml:PhoneTextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Fax</TD>
                      <TD noWrap><ml:PhoneTextBox id=PreparerFaxNum runat="server" width="120" preset="phone"></ml:PhoneTextBox></TD></TR>
                    <tr>
                      <td class=FieldLabel nowrap>Prepared Date</td>
                      <td nowrap><ml:DateTextBox id=PreparerPrepareDate runat="server" width="75" preset="date"></ml:DateTextBox></td></tr>
                    <tr>
                      <td class=FieldLabel nowrap colspan=2><asp:CheckBox id=IsSeeAttachment runat="server" Text="Print 'See Attachment' in borrower signature"></asp:CheckBox></td></tr></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></tr>
  <tr>
    <td noWrap></td></tr>
  <tr>
    <td noWrap></td></tr>
  <TR>
    <TD noWrap>
      <TABLE id=Table4 cellSpacing=0 cellPadding=0 border=0 class=InsetBorder>
        <TR>
          <TD class=FieldLabel noWrap colSpan=8>
            Verification (Shortcut: Enter 't' for today's date. Enter 'o' for opened date.)
          </TD></TR>
        <TR>
          <TD class=FieldLabel noWrap> Ordered</TD>
          <TD noWrap><ml:DateTextBox id=VerifSentD runat="server" width="65px" preset="date" CssClass="mask" onkeyup="onDateKeyUp(this, event);"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap>Re-order</TD>
          <TD class=FieldLabel noWrap><ml:DateTextBox id=VerifReorderedD onkeyup="onDateKeyUp(this, event);" runat="server" CssClass="mask" preset="date" width="65px"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap> Received</TD>
          <TD noWrap><ml:DateTextBox id=VerifRecvD onkeyup="onDateKeyUp(this, event);" runat="server" CssClass="mask" preset="date" width="65px"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap> Expected</TD>
          <TD noWrap><ml:DateTextBox id=VerifExpD onkeyup="onDateKeyUp(this, event);" runat="server" CssClass="mask" preset="date" width="65px"></ml:DateTextBox></TD></TR></TABLE></TD></TR>
  <tr>
    <td noWrap align=center>
<INPUT id=btnPrevious onclick=goPrevious(); type=button value=Previous name=btnPrevious> 
<INPUT id=btnNext onclick=goNext(); type=button value=Next name=btnNext>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<INPUT onclick=goToList(); type=button value="Back to VOE list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<INPUT type=button value="Print &amp; Preview" onclick="printVOE();">
      </td></tr></table>

     </form>

  </body>
</HTML>
