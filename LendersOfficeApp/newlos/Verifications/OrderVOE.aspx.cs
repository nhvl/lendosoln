﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// Page for order VOE.
    /// </summary>
    public partial class OrderVOE : OrderVOX
    {
        /// <summary>
        /// The VOE component loader.
        /// </summary>
        private VOELoader voeLoader;

        /// <summary>
        /// Gets the lender service dropdown list.
        /// </summary>
        protected override DropDownList LenderServiceList
        {
            get
            {
                return this.LenderServices;
            }
        }

        /// <summary>
        /// Gets the reasons the privilege was not allowed during the page's workflow checks.
        /// </summary>
        /// <returns>An array containing <seealso cref="WorkflowOperations.OrderVoa"/>.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderVoe };
        }

        /// <summary>
        /// Gets the extra ops to check during the page's workflow checks.
        /// </summary>
        /// <returns>An array containing <seealso cref="WorkflowOperations.OrderVoa"/>.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderVoe };
        }

        /// <summary>
        /// Gets the VOE component loader.
        /// </summary>
        /// <returns>The VOE component loader.</returns>
        protected override VOXLoader GetLoader()
        {
            if (this.voeLoader == null)
            {
                var loaderFactor = new VOXLoaderFactory(PrincipalFactory.CurrentPrincipal, this.LoanID);
                this.voeLoader = loaderFactor.VoeLoader;
            }

            return this.voeLoader;
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        protected override void LoadData()
        {
            this.RegisterJsGlobalVariables("CanOrderVoe", this.UserHasWorkflowPrivilege(WorkflowOperations.OrderVoe));
            this.RegisterJsGlobalVariables("OrderVoeDenialReason", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.OrderVoe));

            VOELoader loader = this.GetLoader() as VOELoader;

            var records = loader.GetEmploymentRecords();
            this.EmploymentRepeater.DataSource = records;
            this.EmploymentRepeater.DataBind();

            var borrowers = loader.GetBorrowers().ToList();
            if (borrowers.Count > 1)
            {
                borrowers.Insert(0, new KeyValuePair<string, string>(string.Empty, string.Empty));
            }

            this.BorrowerList.DataTextField = nameof(KeyValuePair<string, string>.Value);
            this.BorrowerList.DataValueField = nameof(KeyValuePair<string, string>.Key);
            this.BorrowerList.DataSource = borrowers;
            this.BorrowerList.DataBind();

            var principal = PrincipalFactory.CurrentPrincipal;
            var employee = LendersOffice.Admin.EmployeeDB.RetrieveByIdOrNull(principal.BrokerId, principal.EmployeeId);
            if (employee != null)
            {
                this.NotificationEmail.Value = employee.Email;
            }
        }

        /// <summary>
        /// Order specific OnInit actions.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void PerformOnInit(EventArgs e)
        {
            List<string> states = new List<string>(StateInfo.StatesAndTerritories.Keys);
            states.Insert(0, string.Empty);
            this.RegisterJsObjectWithJsonNetSerializer("States", states);
            this.RegisterJsScript("AuthorizationDocPicker.js");
            this.RegisterJsScript("VOX/SalaryKey.js");
            this.RegisterJsScript("VOX/SelfEmployment.js");
            this.RegisterJsScript("mask.js");
            this.RegisterService("VOE", "/newlos/Verifications/OrderVOEService.aspx");
        }
    }
}