<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<%@ Page language="c#" Codebehind="VOLRecord.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Verifications.VOLRecord" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>VOLRecord</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
       <style type="text/css">
        .verif_headers { text-decoration: underline; }
        #ApplySig, #ClearSig {   display: block;  margin-right: 10px; float:left;  }
        .clear { clear: both; }
    </style>
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
<script type="text/javascript">
  <!--
    var uladToLegacyMap = {
        Attention: 'Attention',

        Bal: 'Bal0',
        CompanyName: 'ComNm',
        CompanyAddress: 'ComAddr',
        CompanyCity: 'ComCity',
        CompanyState: 'ComState',
        CompanyZip: 'ComZip',

        IsSeeAttachment: 'IsSeeAttachment',

        VerifSentDate: 'VerifSentD',
        VerifReorderedDate: 'VerifReorderedD',
        VerifRecvDate: 'VerifRecvD',
        VerifExpiresDate: 'VerifExpD'
    };

    function createListOfEntries(entries, selectedAsset) {
          for (var i = 0; i < entries.length; i++) {
              var entry = entries[i];

              if (entry.RequiresVol) {
                  list.push(entry.Id);

                  if (entry.Id == ML.recordId) {
                      currentIndex = list.length - 1;
                  }
              }
          }

          $('#AccNm0').val(selectedAsset.AccountName);
          $('#AccNum0').val(selectedAsset.AccountNum);
          $('#AssetT0').val(selectedAsset.AssetType);
          $('#Val0').val(selectedAsset.Value);
    }

  var openedDate = <%= AspxTools.JsString(m_openedDate) %>;
      function onDateKeyUp(o, event) {
    if (event.keyCode == 79) {
      o.value = openedDate;
      updateDirtyBit(event);
    }
  }
            function _init() {
                var enableSignature = document.getElementById('EnableSignature').value === 'True';
                if (!enableSignature) {
                    document.getElementById('SignatureRow').style.display = "none";
                    return;
                }

                var hasSignature = document.getElementById('EmployeeHasSignature').value === 'True';
                var missingSignature = document.getElementById('MissingSignature');
                missingSignature.style.display = hasSignature ? 'none' : '';
                var applySig = document.getElementById('ApplySig');
                applySig.style.display = hasSignature ? 'block' : 'none';
                postPopulateForm();
            }
            
            function postPopulateForm() {
                var verifHasSignature = document.getElementById('VerifHasSignature').value === 'True';
                var verifSigningEmployeeId = document.getElementById('VerifSigningEmployeeId').value; 
                var clearSig = document.getElementById('ClearSig');
                clearSig.style.display = verifHasSignature ? 'block' : 'none';
                var img = document.getElementById('SignatureImg');
                
                if( verifHasSignature ) {
                    img.src = 'UserSignature.aspx?eid=' + verifSigningEmployeeId;
                }
                else {
                    img.src = 'UserSignature.aspx';
                }
            }
            
            function onclearsig() {
                if( false == confirm('Remove existing signature from verification?') ) {
                    return;
                }   
                var recordId = document.getElementById('RecordID').value;
                var img = document.getElementById('SignatureImg');
                
                var args = { 
                    'recordid' : recordId,
                     'loanid'  : ML.sLId,
                    'applicationid' :  ML.aAppId
                     
                };
                var results = gService.singleedit.call('ClearSig', args);
                if( results.error ) {
                    alert(results.UserMessage);
                }
                else {
                    img.src = "UserSignature.aspx";
                    document.getElementById('VerifHasSignature').value = 'False';
                    document.getElementById('VerifSigningEmployeeId').value = '00000000-0000-0000-0000-000000000000';
                }
                return false;
                
            }
            
            function onapplysig() {
                var username = document.getElementById('EmployeeName').value;
                var verifName = document.getElementById('PreparerName');
                
                if( username != verifName.value ) {
                    if( false == confirm('Your name will be populated to match your signature. Review other fields for accuracy.') ) {
                        return; 
                    }
                    updateDirtyBit();
                    verifName.value = username;
                }
                
                var recordId = document.getElementById('RecordID').value;
                var img = document.getElementById('SignatureImg');
                var clearSig = document.getElementById('ClearSig');
                
                
                
                var args = { 
                    'recordid' : recordId,
                    'loanid' : ML.sLId,
                    'applicationid' :  ML.aAppId
                };

                var results = gService.singleedit.call('ApplySig', args);
                if( results.error ) {
                    alert(results.UserMessage);
                }
                else {
                    img.src = "UserSignature.aspx?eid="+ results.value.EmployeeId ;
                    clearSig.style.display = 'block';
                    document.getElementById('VerifHasSignature').value = 'True';
                    document.getElementById('VerifSigningEmployeeId').value = results.value.EmployeeId;
                }
                
                return false;
            }
            
                function printVOL() {
                    PolyShouldShowConfirmSave(isDirty(), function(){
                        var recordid = document.getElementById('RecordID').value;
                        var url = 'pdf/VOL.aspx?loanid=' + ML.sLId + '&applicationid=' + ML.aAppId + '&recordid=' + recordid + '&crack=' + new Date();
                        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
                    }, saveMe);
    }

    
      
  //-->
</script>
    <form id="VOLRecord" method="post" runat="server">
        <asp:HiddenField runat="server" ID="VerifSigningEmployeeId" />
        <asp:HiddenField runat="server" ID="VerifHasSignature" />
<TABLE class=FormTable id=Table1 cellSpacing=0 cellPadding=0 border=0>
  <TR>
    <TD class=MainRightHeader noWrap>Verification of
    Loan <span id=IndexLabel></span></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE id=Table6 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD vAlign=top noWrap>
            <TABLE class=InsetBorder id=Table7 cellSpacing=0 cellPadding=0 
            border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table5 cellSpacing=0 cellPadding=0 border=0>
                    <TR>
                      <TD class=FieldLabel noWrap colSpan=2><span class="verif_headers"> To:</span></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Attn</TD>
                      <TD noWrap><asp:TextBox id=Attention runat="server"></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Name</TD>
                      <TD noWrap><asp:textbox id=ComNm runat="server" Width="203px" ReadOnly="True"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Address</TD>
                      <TD noWrap><asp:textbox id=ComAddr runat="server" Width="202px" ReadOnly="True"></asp:textbox></TD></TR>
                    <TR>
                      <TD noWrap></TD>
                      <TD noWrap><asp:textbox id=ComCity runat="server" Width="107px" ReadOnly="True"></asp:textbox><asp:textbox id=ComState runat="server" Width="40px" ReadOnly="True"></asp:textbox><asp:textbox id=ComZip runat="server" Width="55px" ReadOnly="True"></asp:textbox></TD></TR></TABLE></TD></TR></TABLE></TD>
          <TD vAlign=top noWrap>
            <TABLE class=InsetBorder id=Table8 cellSpacing=0 cellPadding=0 
            border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table2 cellSpacing=0 cellPadding=0 border=0>
                    <TR>
                      <TD class=FieldLabel noWrap><span class="verif_headers"> From: </span></TD>
                      <TD noWrap></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Attn</TD>
                      <TD noWrap><asp:textbox id=PreparerName runat="server" Width="203"></asp:textbox></TD></TR>
                         <TR id="SignatureRow">
                        <TD class=FieldLabel valign="top">Apply Signature</TD>
                        <TD width="207px">
                           <img src="UserSignature.aspx" alt="Signature" id="SignatureImg" width="168" />
                            <a href="#" style="display:none" id="ApplySig" onclick="return onapplysig()">apply my signature</a> <a
                                href="#" style="display:none"  id="ClearSig" onclick="return onclearsig()">clear signature</a>
                            <br class="clear" />
                            <span id="MissingSignature" style="display:none">
                            To use your signature, upload your signature image to your profile.
                            </span>
                            <br />
                        </TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Title:</TD>
                      <TD noWrap><asp:textbox id=PreparerTitle runat="server" Width="203px"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel nowrap>Broker Name</td>
                      <td nowrap><asp:textbox id=PreparerCompanyName runat="server" Width="203px"></asp:textbox></td></tr>
                    <TR>
                      <TD class=FieldLabel noWrap>Broker Address</TD>
                      <TD noWrap><asp:textbox id=PreparerStreetAddr runat="server" Width="225px" ></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>City</TD>
                      <TD noWrap><asp:textbox id=PreparerCity runat="server" Width="128px" ></asp:textbox><ml:StateDropDownList id=PreparerState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=PreparerZip runat="server" preset="zipcode" width="50"></ml:ZipcodeTextBox></TD></TR>
                    <tr>
                      <td class=FieldLabel nowrap>Phone</td>
                      <td nowrap><ml:PhoneTextBox id=PreparerPhone runat="server" width="120" preset="phone"></ml:PhoneTextBox></td></tr>
                    <TR>
                      <TD class=FieldLabel noWrap>Lender Number</TD>
                      <TD noWrap><asp:textbox id=sLenderNumVerif runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel nowrap>Prepared Date</td>
                      <td nowrap><ml:DateTextBox id=PreparerPrepareDate runat="server" width="75" preset="date"></ml:DateTextBox></td></tr>
                    <tr>
                      <td class=FieldLabel nowrap colspan=2><asp:CheckBox id=IsSeeAttachment runat="server" Text="Print 'See Attachment' in borrower signature"></asp:CheckBox></td></tr></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE class=InsetBorder id=Table9 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD noWrap>
            <TABLE id=Table3 cellSpacing=0 cellPadding=0 border=0>
              <TR>
                <TD class=FieldLabel noWrap colSpan=4>Information to be 
                  verified</TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>Type Of Account</TD>
                <TD class=FieldLabel noWrap>Name Of Account</TD>
                <TD class=FieldLabel noWrap>Account Number</TD>
                <TD class=FieldLabel noWrap>Balance</TD></TR>
              <TR>
                <TD noWrap><asp:TextBox id=m_accountType0TF runat="server" Width="103" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNm0 runat="server" Width="193" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNum0 runat="server" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><ml:MoneyTextBox id=Bal0 runat="server" ReadOnly="True" width="90" preset="money" CssClass="mask"></ml:MoneyTextBox></TD></TR>
              <TR>
                <TD noWrap><asp:TextBox id=m_accountType1TF runat="server" Width="103px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNm1 runat="server" Width="193px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNum1 runat="server" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><ml:MoneyTextBox id=Bal1 runat="server" ReadOnly="True" width="90" preset="money" CssClass="mask"></ml:MoneyTextBox></TD></TR>
              <TR>
                <TD noWrap><asp:TextBox id=m_accountType2TF runat="server" Width="103px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNm2 runat="server" Width="193px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNum2 runat="server" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><ml:MoneyTextBox id=Bal2 runat="server" ReadOnly="True" width="90" preset="money" CssClass="mask"></ml:MoneyTextBox></TD></TR>
              <TR>
                <TD noWrap><asp:TextBox id=m_accountType3TF runat="server" Width="103px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNm3 runat="server" Width="193px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNum3 runat="server" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><ml:MoneyTextBox id=Bal3 runat="server" ReadOnly="True" width="90" preset="money" CssClass="mask"></ml:MoneyTextBox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD noWrap></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE id=Table4 cellSpacing=0 cellPadding=0 border=0 class=InsetBorder>
        <TR>
          <TD class=FieldLabel noWrap colSpan=8>Verification (Shortcut: Enter 't' for today's 
            date. Enter 'o' for opened date.)</TD></TR>
        <TR>
          <TD class=FieldLabel noWrap> Ordered</TD>
          <TD noWrap><ml:DateTextBox id=VerifSentD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap>Re-order</TD>
          <TD class=FieldLabel noWrap><ml:DateTextBox id=VerifReorderedD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap> Received</TD>
          <TD noWrap><ml:DateTextBox id=VerifRecvD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap> Expected</TD>
          <TD noWrap><ml:DateTextBox id=VerifExpD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:DateTextBox></TD></TR></TABLE></TD></TR>
  <TR>
    <TD noWrap align=middle>
<INPUT id=btnPrevious onclick=goPrevious(); type=button value=Previous name=btnPrevious> 
<INPUT id=btnNext onclick=goNext(); type=button value=Next name=btnNext>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<INPUT onclick=goToList(); type=button value="Back to VOL list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       
<input onclick="printVOL();" type="button" value="Preview &amp; Print" />

    </TD></TR></TABLE>
     </form>
	
  </body>
</html>
