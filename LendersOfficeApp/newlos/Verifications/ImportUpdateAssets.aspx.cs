﻿#region Auto-generated code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Page for presenting options for importing and updating assets returned from a VOA order.
    /// </summary>
    public partial class ImportUpdateAssets : BaseLoanPage
    {
        /// <summary>
        /// Force edge mode for this page in IE.
        /// </summary>
        /// <returns>Edge mode.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Page load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var orderId = RequestHelper.GetInt("order", -1);
            var order = VOAOrder.GetOrderForLoan(this.BrokerID, this.LoanID, orderId);
            if (order == null)
            {
                throw new ServerException(ErrorMessage.SystemError);
            }

            var service = VOXLenderService.GetLenderServiceById(PrincipalFactory.CurrentPrincipal.BrokerId, order.LenderServiceId);
            this.RegisterJsGlobalVariables("VoaAllowImportingAssets", service.VoaAllowImportingAssets);
            if (!service.VoaAllowImportingAssets)
            {
                throw new ServerException(ErrorMessage.SystemError);
            }

            var loan = CPageData.CreateUsingSmartDependencyForLoad(this.LoanID, typeof(ImportUpdateAssets));

            this.RegisterJsScript("angular-1.5.5.min.js");
            this.RegisterJsScript("angularModules/LqbForms.module.js");
            this.RegisterService("vodService", "/newlos/Verifications/OrderVODService.aspx");
            this.RegisterCSS("material-components-web.min.css");
            this.RegisterCSS("stylesheetnew.css");
            this.RegisterCSS("material-icons.css");
            this.RegisterCSS("ImportUpdateAssets.css");
            this.DisplayCopyRight = false;
            this.RegisterJsGlobalVariables("orderId", orderId);

            List<Dictionary<string, object>> accounts = new List<Dictionary<string, object>>();
            if (order != null)
            {
                HashSet<string> accountNumbers = new HashSet<string>();
                var app = loan.GetAppData(order.ApplicationId);
                this.RegisterJsGlobalVariables("appId", app.aAppId);

                accounts = app.aAssetCollection.AssetsForVerification.Where(a => a.Val > 0 && AssetImport.AssetOwnerMatchesOrderOwner(a, order)).Select(assetOnFile => GetAssetViewModel(assetOnFile, order, accountNumbers)).ToList();
                accounts.AddRange(order.VerifiedAssetRecords.Where(returnedAsset => !accountNumbers.Contains(returnedAsset.AccountNumber)).Select(returnedAsset =>
                    new Dictionary<string, object>
                    {
                        { "accountLabel", returnedAsset.FinancialInstitutionName },
                        { "accountNumber", returnedAsset.AccountNumber },
                        { "verifiedValue", Money.Create(returnedAsset.AccountBalance)?.Value },
                        { "valueOnFile", null }
                    }));
                var totals = AssetImport.GetAssetTotals(new HashSet<Guid>(), loan, app, order);
                this.RegisterJsGlobalVariables("loanTotal", totals["loanTotal"]);
                this.RegisterJsGlobalVariables("borrowerTotal", totals["borrowerTotal"]);
                this.RegisterJsGlobalVariables("borrowerName", order.IsForCoborrower ? app.aCNm : app.aBNm);
                this.RegisterJsGlobalVariables("previousLoanTotal", totals["loanOnFileTotal"]);
                this.RegisterJsGlobalVariables("previousBorrowerTotal", totals["borrowerOnFileTotal"]);
            }

            this.RegisterJsObjectWithJsonNetSerializer("accounts", accounts);
        }

        /// <summary>
        /// Creates a dictionary of asset fields to serve as a view model for an asset entry on the page.
        /// </summary>
        /// <param name="assetOnFile">The asset record on the loan file.</param>
        /// <param name="order">The VOA order object which may contain returned asset information.</param>
        /// <param name="accountNumbers">A set of account numbers that have been already encountered.</param>
        /// <returns>A dictionary of asset fields for the view.</returns>
        private static Dictionary<string, object> GetAssetViewModel(IAssetRegular assetOnFile, VOAOrder order, HashSet<string> accountNumbers)
        {
            var matchingVerifiedAsset = order.VerifiedAssetRecords.FirstOrDefault(verifiedAsset => !string.IsNullOrEmpty(assetOnFile.AccNum.Value) && assetOnFile.AccNum == verifiedAsset.AccountNumber);
            accountNumbers.Add(assetOnFile.AccNum.Value);
            var accountLabel = matchingVerifiedAsset?.FinancialInstitutionName;
            if (string.IsNullOrEmpty(accountLabel))
            {
                accountLabel = assetOnFile.ComNm;
            }

            var assetFields = new Dictionary<string, object>
                    {
                        { "accountLabel", accountLabel },
                        { "accountNumber", assetOnFile.AccNum.Value },
                        { "verifiedValue", matchingVerifiedAsset?.AccountBalance },
                        { "valueOnFile", assetOnFile.Val },
                        { "assetId", assetOnFile.RecordId }
                    };
            if (string.IsNullOrEmpty(accountLabel))
            {
                assetFields["accountLabel"] = $"[{assetOnFile.AssetT.GetDescription()}]";
                assetFields["accountLabelIsAccountType"] = true;
            }

            return assetFields;
        }
    }
}