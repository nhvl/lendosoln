using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Pdf;
using LendingQB.Core.Data;
using LqbGrammar.DataTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace LendersOfficeApp.newlos.Verifications
{
    /// <summary>
    /// Summary description for VODRecord.
    /// </summary>
    public partial class VODRecord : BaseSingleEditPage<IAssetRegular>
	{
        protected string m_openedDate;

        private CPageData dataLoan;
        private CAppData dataApp;

        protected override string ListLocation
        {
            get { return VirtualRoot + "/newlos/Verifications/Verifications.aspx?loanid=" + LoanID + "&pg=0"; }
        }

        protected override DataView GetDataView()
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("recordid", typeof(string)));

            var depositoryNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            if (!this.IsUlad)
            {
                var assetSubcoll = dataApp.aAssetCollection.GetSubcollection(false, E_AssetGroupT.DontNeedVOD);
                foreach (var item in assetSubcoll)
                {
                    var field = (IAssetRegular)item;
                    if (depositoryNames.Add(field.ComNm))
                    {
                        DataRow row = table.NewRow();
                        row["recordid"] = field.RecordId.ToString();
                        table.Rows.Add(row);
                    }
                }
            }

            return table.DefaultView;
        }

        protected override IAssetRegular RetrieveRecord()
        {
            try
            {
                return this.dataApp.aAssetCollection.GetRegRecordOf(this.RecordID);
            }
            catch(CBaseException e)
            {
                //OPM 112805: Accessing deleted record causes server error; handle with user message and return to parent
                Tools.LogError(e);
                ErrorMessage.Value = e.UserMessage;
                return null;
            }

        }

        protected override void BindSingleRecord(IAssetRegular field)
        {
            //Error retrieving record; cannot bind
            if (field == null)
            {
                return;
            }

            Attention.Text = field.Attention;

            DepositoryName.Text = field.ComNm;
            DepositoryStAddr.Text = field.StAddr;
            DepositoryCity.Text = field.City;
            DepositoryState.Text = field.State;
            DepositoryZip.Text = field.Zip;
            AssetT0.Text = field.AssetT_rep;
            AccNum0.Text = field.AccNum.Value;
            AccNm0.Text = field.AccNm;
            Val0.Text = field.Val_rep;
            IsSeeAttachment.Checked = field.IsSeeAttachment;
            VerifExpD.Text = field.VerifExpD_rep;
            VerifSentD.Text = field.VerifSentD_rep;
            VerifRecvD.Text = field.VerifRecvD_rep;
            VerifReorderedD.Text = field.VerifReorderedD_rep;

            VerifSigningEmployeeId.Value = field.VerifSigningEmployeeId.ToString();
            VerifHasSignature.Value = field.VerifHasSignature.ToString();
            OwnerT.Value = field.OwnerT == E_AssetOwnerT.Borrower ? "0" : "1";
        }

        protected override void LoadData()
        {
            this.dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(VODRecord));
            this.dataLoan.InitLoad();

            RegisterJsScript("Ulad.Verifications.Vod.js");
            RegisterJsGlobalVariables("recordId", this.RecordID);
            PopulateSiblingAssets(dataLoan);

            sLenderNumVerif.Text = this.dataLoan.sLenderNumVerif;
            m_openedDate = this.dataLoan.sOpenedD_rep;

            IPreparerFields preparer = this.dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfDeposit, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (preparer.IsValid)
            {
                PreparerName.Text = preparer.PreparerName;
                PreparerTitle.Text = preparer.Title;
                PreparerCompanyName.Text = preparer.CompanyName;
                PreparerStreetAddr.Text = preparer.StreetAddr;
                PreparerCity.Text = preparer.City;
                PreparerState.Value = preparer.State;
                PreparerZip.Text = preparer.Zip;
                PreparerPrepareDate.Text = preparer.PrepareDate_rep;
                PreparerPhone.Text = preparer.Phone;
                PreparerFaxNum.Text = preparer.FaxNum;
            }

            base.LoadData();
        }

        /// <summary>
        /// Iterates through the given dataloan, and finds assets that meet the VOD requirement, and that have the same 
        /// company name as the selected asset.
        /// </summary>
        /// <param name="dataLoan">The loan to retrieve the assets from.</param>
        protected void PopulateSiblingAssets(CPageData dataLoan)
        {
            if (this.ApplicationID != Guid.Empty)
            {
                this.dataApp = this.dataLoan.GetAppData(this.ApplicationID);
                m_nameLabel.Text = this.dataApp.aBLastFirstNm;

                // Loop through list of asset record.
                int currentIndex = 1;
                var assetSubcoll = this.dataApp.aAssetCollection.GetSubcollection(false, E_AssetGroupT.DontNeedVOD);
                foreach (var item in assetSubcoll)
                {
                    var f = (IAssetRegular)item;
                    if (f.RecordId != RecordID)
                    {
                        if (f.ComNm.ToLower() == DepositoryName.Text.ToLower())
                        {
                            PopulateDepository(currentIndex, f.AccNm, f.AccNum.Value, f.AssetT_rep, f.Val_rep);
                            currentIndex++;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Simply populates the depository UI controls with the given data.
        /// </summary>
        /// <param name="index">Which row to update.</param>
        /// <param name="accountName">The account name.</param>
        /// <param name="accountNum">The account number.</param>
        /// <param name="assetType">The asset type.</param>
        /// <param name="value">The asset value.</param>
        protected void PopulateDepository(int index, string accountName, string accountNum, string assetType, string value)
        {
            switch (index)
            {
                case 1:
                    AccNm1.Text = accountName;
                    AccNum1.Text = accountNum;
                    AssetT1.Text = assetType;
                    Val1.Text = value;
                    break;
                case 2:
                    AccNm2.Text = accountName;
                    AccNum2.Text = accountNum;
                    AssetT2.Text = assetType;
                    Val2.Text = value;
                    break;
                case 3:
                    AccNm3.Text = accountName;
                    AccNum3.Text = accountNum;
                    AssetT3.Text = assetType;
                    Val3.Text = value;
                    break;
            }
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            var assetTypeDdl = new DropDownList();
            assetTypeDdl.ID = "AssetTypeDdl";
            Tools.Bind_AssetT(assetTypeDdl, true);
            Form.Controls.Add(assetTypeDdl);
            assetTypeDdl.CssClass = "Hidden";
            this.PageTitle = "VOD Record";
            this.PDFPrintClass = typeof(CVODPDF);
            PreparerZip.SmartZipcode(PreparerCity, PreparerState);

            bool enableSignature = this.Broker.EnableLqbNonCompliantDocumentSignatureStamp;
            ClientScript.RegisterHiddenField("EnableSignature", enableSignature.ToString());
            if (enableSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(BrokerUser.BrokerId, BrokerUser.EmployeeId, enableSignature);
                ClientScript.RegisterHiddenField("EmployeeHasSignature", signInfo.HasUploadedSignature.ToString());
                ClientScript.RegisterHiddenField("EmployeeName", BrokerUser.DisplayName);
            }

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            this.RegisterJsGlobalVariables("IsLiability", false);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
