using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Pdf;
using LendingQB.Core.Data;
using LqbGrammar.DataTypes;
using System;
using System.Data;
using System.Linq;

namespace LendersOfficeApp.newlos.Verifications
{
    /// <summary>
    /// Summary description for VOLRecord.
    /// </summary>
    public partial class VOLRecord : BaseSingleEditPage<ILiabilityRegular>
	{

        protected string m_openedDate;

        private CPageData m_dataLoan;

        protected override string ListLocation
        {
            get { return this.VirtualRoot + "/newlos/Verifications/Verifications.aspx?loanid=" + this.LoanID + "&pg=4"; }
        }
        protected override DataView GetDataView()
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("recordid", typeof(string)));

            if (!IsUlad)
            {
                CAppData dataApp = m_dataLoan.GetAppData(this.ApplicationID);

                ILiaCollection liaList = dataApp.aLiaCollection;
                int liaCount = liaList.CountRegular;
                for (int index = 0; index < liaCount; index++)
                {
                    ILiability field = liaList.GetRegularRecordAt(index);
                    if (field.DebtT == E_DebtT.Installment || field.DebtT == E_DebtT.Revolving)
                    {
                        DataRow row = table.NewRow();
                        row["recordid"] = field.RecordId.ToString();
                        table.Rows.Add(row);
                    }
                }
            }

            return table.DefaultView;
        }

        protected override ILiabilityRegular RetrieveRecord()
        {
            CAppData dataApp = m_dataLoan.GetAppData(this.ApplicationID);

            return dataApp.aLiaCollection.GetRegRecordOf(this.RecordID);
        }

        protected override void BindSingleRecord(ILiabilityRegular field)
        {
            Attention.Text = field.Attention;
            ComAddr.Text = field.ComAddr;
            ComCity.Text = field.ComCity;
            ComState.Text = field.ComState;
            ComZip.Text = field.ComZip;
            ComNm.Text = field.ComNm;
            AccNm0.Text = field.AccNm;
            AccNum0.Text = field.AccNum.Value;
            Bal0.Text = field.Bal_rep;
            VerifSentD.Text = field.VerifSentD_rep;
            VerifExpD.Text = field.VerifExpD_rep;
            VerifRecvD.Text = field.VerifRecvD_rep;
            VerifReorderedD.Text = field.VerifReorderedD_rep;
            IsSeeAttachment.Checked = field.IsSeeAttachment;
            VerifSigningEmployeeId.Value = field.VerifSigningEmployeeId.ToString();
            VerifHasSignature.Value = field.VerifHasSignature.ToString();
        }

        protected override void LoadData()
        {
            m_dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOLRecord)); new CVolData(LoanID);
            m_dataLoan.InitLoad();

            sLenderNumVerif.Text = m_dataLoan.sLenderNumVerif;
            m_openedDate = m_dataLoan.sOpenedD_rep;

            IPreparerFields f = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfLoan, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f.IsValid)
            {
                PreparerName.Text = f.PreparerName;
                PreparerTitle.Text = f.Title;
                PreparerCompanyName.Text = f.CompanyName;
                PreparerStreetAddr.Text = f.StreetAddr;
                PreparerCity.Text = f.City;
                PreparerState.Value = f.State;
                PreparerZip.Text = f.Zip;
                PreparerPhone.Text = f.Phone;
                PreparerPrepareDate.Text = f.PrepareDate_rep;
            }

            base.LoadData();
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "VOL record";
            this.PDFPrintClass = typeof(CVOLPDF);
            PreparerZip.SmartZipcode(PreparerCity, PreparerState);

            RegisterJsScript("Ulad.Verifications.Vol.js");
            RegisterJsGlobalVariables("recordId", this.RecordID);

            bool enableSignature = this.Broker.EnableLqbNonCompliantDocumentSignatureStamp;
            ClientScript.RegisterHiddenField("EnableSignature", enableSignature.ToString());
            if (enableSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(BrokerUser.BrokerId, BrokerUser.EmployeeId, enableSignature);
                ClientScript.RegisterHiddenField("EmployeeHasSignature", signInfo.HasUploadedSignature.ToString());
                ClientScript.RegisterHiddenField("EmployeeName", BrokerUser.DisplayName);
            }

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
