﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VerificationsDashboard.aspx.cs" Inherits="LendersOfficeApp.newlos.Verifications.VerificationsDashboard" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Verifications Ordering Dashboard</title>
    <style type="text/css">
        body  { 
            background-color: gainsboro; 
        }
        a[data-sort] {
            color: white;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            $('a[data-sort="true"]').click(function () {
                var link = $(this);
                var table = $(this).closest('table').eq(0);
                var rows = table.find('tr:gt(0)').toArray().sort(RowComparer(link.data('sorttarget')));
                table.find('.SortImg').hide();

                link.data('asc', !link.data('asc'));
                if (!link.data('asc')) {
                    rows = rows.reverse();
                    link.siblings('img').prop('src', '../../images/Tri_Desc.gif').show();
                }
                else {
                    link.siblings('img').prop('src', '../../images/Tri_Asc.gif').show();
                }

                for (var i = 0; i < rows.length; i++) {
                    table.append(rows[i]);
                }
            });

            function RowComparer(sortTargetClass) {
                return function (a, b) {
                    var sortTargetA = $(a).find('.' + sortTargetClass);
                    var sortTargetB = $(b).find('.' + sortTargetClass);
                    
                    var sortValueA = sortTargetA.text();
                    var sortValueB = sortTargetB.text();
                    if (sortValueA < sortValueB) {
                        return -1;
                    }
                    else if (sortValueA > sortValueB) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            }

            $('#OrderVODBtn').click(function () {
                OpenOrderPage(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/Verifications/OrderVOD.aspx?loanid=" + this.LoanID + "&appid=" + this.ApplicationID)) %>, "VOATable", VOAOrderRow.CreateVoaRow);
            });

            $('#OrderVOEBtn').click(function() {
                OpenOrderPage(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/Verifications/OrderVOE.aspx?loanid=" + this.LoanID + "&appid=" + this.ApplicationID)) %>, "VOETable", VOEOrderRow.CreateVOERow)
            });

            $('#OrderSSA89Btn').click(function() {
                OpenOrderPage(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/newlos/Verifications/OrderSSA89.aspx?loanid=" + this.LoanID + "&appid=" + this.ApplicationID)) %>, "SSA89Table", SSA89OrderRow.CreateSsa89Row, 1000, 450)
            });
            
            $('.TabLink').click(function() {
                $('.TabContent').hide();
                $('.Tab').removeClass('selected');
                var contentPrefix = $(this).data('content');
                $('#' + contentPrefix + 'TabContent').show();
                $(this).closest('li').addClass('selected');
            });

            $('body').on('click', '.EDocLink', function() {
                var docId = $(this).data('docid');
                window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?docid=" + docId, '_self');
            });

            $('body').on('click', '.ErrorLink', function() {
                var error = $(this).data('error');
                var suberror = $(this).data('suberror');
                var alertPopup = SimplePopups.CreateAlertPopup(error, suberror, null, "Error");
                LQBPopup.ShowElement($('<div>').append(alertPopup), {
                    width: 350,
                    height: 200,
                    elementClasses: 'FullWidthHeight',
                    hideCloseButton: true
                });
            });

            $('body').on('click', '.PollLink', function () {
                RequestLinkClick(this, true, true);
            });

            $('body').on('click', '.RefreshLink', function() {
                if(confirm("Your are about to refresh this order. Proceed?")) {
                    RequestLinkClick(this, false, false);
                }
            });

            $('body').on('click', '.NoConfirmRefreshLink', function() {
                RequestLinkClick(this, true, false);
            });

            $('body').on('click', '#OkCredentialsBtn', function() {
                var popup = $(this).closest('#Credentials');
                var accountId = popup.find('#AccountId').val();
                var userName = popup.find('#Username').val();
                var password = popup.find('#Password').val();
                if((popup.find('#AccountIdRow').is(':visible') && popup.find('#AccountId').hasClass('RequiredInput') && accountId === '') ||
                    (popup.find('#UsernameRow').is(':visible') && userName === '') ||
                    (popup.find('#PasswordRow').is(':visible') && password === '')) {
                    alert("Please enter values for all required fields.");
                }
                else {
                    LQBPopup.Return({AccountId: accountId, Username: userName, Password: password});
                }
            });

            $('body').on('click', '#CancelCredentialsBtn', function() {
                LQBPopup.Return(null);
            });

            $('body').on('change', '.RequiredInput', function() {
                $(this).siblings('.RequiredImg').toggle(this.value === '');
            });

            $('body').on('click', '.DisplayVendorReferenceIdsLink', function() {
                var isShowMore = $(this).data('isShowMore') ? true : false; 

                var cell = $(this).closest('.VendorReferenceIdsCell');
                cell.find('.ToggleableRefId').toggle(!isShowMore);

                $(this).text(isShowMore ? "show more" : "show less");
                $(this).data('isShowMore', !isShowMore);
            });

            function RequestLinkClick(link, usePollMsg, usePollMethod) {
                var requiresAccountId = $(link).siblings('.RequiresAccountId').val().toLowerCase() === 'true';
                var usesAccountId = $(link).siblings('.UsesAccountId').val().toLowerCase() === 'true';
                var serviceCredentialId = $(link).siblings('.ServiceCredentialId').val();
                var serviceCredentialHasAccountId = $(link).siblings('.ServiceCredentialHasAccountId').val().toLowerCase() === 'true';

                if(serviceCredentialId !== '-1' && (serviceCredentialHasAccountId || !usesAccountId)) {
                    var data = {
                        OrderId: $(link).siblings('.OrderId').val(),
                        LoanId: ML.sLId
                    };

                    SubmitRequest(data, $(link).closest('.OrderRow'), usePollMsg, usePollMethod);
                }
                else {
                    var popup = $('#CredentialsPopup');
                    popup.find('#AccountIdRow').toggle(usesAccountId);
                    popup.find('#AccountIdImage').toggle(requiresAccountId);
                    popup.find('#AccountId').toggleClass('RequiredInput', requiresAccountId);
                    popup.find('#UsernameRow, #PasswordRow').toggle(serviceCredentialId === '-1');
                    popup.find('#Username, #Password').toggleClass('RequiredInput', serviceCredentialId === '-1');
                    popup.find('#UsernameImg, #PasswordImg').toggle(serviceCredentialId === '-1');

                    LQBPopup.ShowElement(popup, {
                        width: 350,
                        height: 200,
                        hideCloseButton: true,
                        elementClasses: 'FullWidthHeight',
                        onReturn: function(returnArgs) {
                            if(typeof(returnArgs) != 'undefined' && returnArgs != null) {
                                var data = {
                                    AccountId: returnArgs.AccountId,
                                    Username: returnArgs.Username,
                                    Password: returnArgs.Password,
                                    OrderId: $(link).siblings('.OrderId').val(),
                                    LoanId: ML.sLId
                                };

                            return function() { SubmitRequest(data, $(link).closest('.OrderRow'), usePollMsg, usePollMethod); };
                            }
                        }
                    });
                }
            }

            function SubmitRequest(data, associatedRow, usePollMsg, usePollMethod) {
                var popupMessage = usePollMsg ? "Getting Results..." : "Refreshing Order...";
                var methodName = usePollMethod ? "PollOrder" : "RefreshOrder";

                var serviceObject;
                var rowCreator;
                var header;
                var serviceType = $(associatedRow).find('.ServiceType').val();
                
                // Add new order types here.
                if(serviceType === "0") { // VOA
                    serviceObject = gService.vod;
                    rowCreator = VOAOrderRow.CreateVoaRow;
                    header = "Verification of Deposit";
                }
                else if(serviceType === '1') { // VOE
                    serviceObject = gService.voe;
                    rowCreator = VOEOrderRow.CreateVOERow;
                    header = "Verification of Employment";
                }
                else if(serviceType === '2') { // SSA89
                    serviceObject = gService.ssa89;
                    rowCreator = SSA89OrderRow.CreateSsa89Row;
                    header = "SSN Verification";
                }
                else {
                    LQBPopup.Return(null);
                    alert("Invalid service type.");
                    return;
                }

                var loadingPopup = SimplePopups.CreateLoadingPopup(popupMessage, header);
                LQBPopup.ShowElement($('<div>').append(loadingPopup), {
                    width: 350,
                    height: 200,
                    hideCloseButton: true,
                    elementClasses: 'FullWidthHeight'
                });

                serviceObject.callAsync(methodName, data, true, false, true, true, 
                    function(results) { 
                        HandleRequestResults(results, associatedRow, usePollMethod, rowCreator, header, serviceObject);
                    }, ML.VOXTimeoutInMilliseconds);
            }

            function HandleRequestResults(results, associatedRow, usePollMethod, rowCreatorFunction, header, serviceObject) {
                if(!results.error) {
                    if (results.value["Success"].toLowerCase() === 'true') {
                        if (results.value.PublicJobId) {
                            var publicJobId = results.value.PublicJobId;
                            var pollingIntervalInMilliseconds = parseInt(results.value.PollingIntervalInSeconds) * 1000;
                            var attemptNumber = 1;
                            window.setTimeout(function () {
                                PollForSubsequentResult(publicJobId, pollingIntervalInMilliseconds, attemptNumber, results.value, serviceObject, associatedRow, usePollMethod, rowCreatorFunction, header);
                            }, pollingIntervalInMilliseconds);
                        }
                        else {
                            LQBPopup.Return(null);
                            HandleSuccessResult(results, associatedRow, usePollMethod, rowCreatorFunction, header);
                        }
                    }
                    else {
                        LQBPopup.Return(null);
                        var requestErrors = JSON.parse(results.value["Errors"]);
                        var errorPopup = SimplePopups.CreateErrorPopup("Something went wrong", requestErrors, header);
                        LQBPopup.ShowElement($('<div>').append(errorPopup), {
                            width: 350,
                            height: 200,
                            hideCloseButton: true,
                            elementClasses: 'FullWidthHeight'
                        });
                    }
                }
                else {
                    LQBPopup.Return(null);
                    alert(results.UserMessage);
                }
            }

            function PollForSubsequentResult(publicJobId, pollingIntervalInMilliseconds, attemptNumber, previousResult, serviceObject, associatedRow, usePollMethod, rowCreatorFunction, header) {
                var args = {};
                args.LoanId = ML.sLId;
                args.PublicJobId = publicJobId;
                args.AttemptNumber = attemptNumber;
                for (var key in previousResult) {
                    if (previousResult.hasOwnProperty(key)) {
                        args[key] = previousResult[key];
                    }
                }

                serviceObject.callAsyncBypassOverlay('PollForSubsequentResults', args,
                    function (result) {
                        if (result.error) {
                            LQBPopup.Return(null);
                            alert(result.UserMessage);
                        }
                        else if (result.value.Success.toLowerCase() !== 'true') {
                            LQBPopup.Return(null);

                            var requestErrors = JSON.parse(result.value["Errors"]);
                            var errorPopup = SimplePopups.CreateErrorPopup(requestErrors);
                            LQBPopup.ShowElement($('<div>').append(errorPopup), {
                                width: 350,
                                height: 200,
                                hideCloseButton: true,
                                elementClasses: 'FullWidthHeight'
                            });
                        }
                        else if (result.value.PublicJobId) {
                            publicJobId = result.value.PublicJobId;
                            window.setTimeout(function () {
                                PollForSubsequentResult(publicJobId, pollingIntervalInMilliseconds, ++attemptNumber, previousResult, serviceObject, associatedRow, usePollMethod, rowCreatorFunction, header);
                            }, pollingIntervalInMilliseconds);
                        }
                        else {
                            LQBPopup.Return(null);
                            HandleSuccessResult(result, associatedRow, usePollMethod, rowCreatorFunction, header);
                        }
                });
            }

            function HandleSuccessResult(results, associatedRow, usePollMethod, rowCreatorFunction, header) {
                var updatedOrder = JSON.parse(results.value["UpdatedOrder"]);
                var newRow = rowCreatorFunction(updatedOrder);

                if(updatedOrder.Status === 0) {
                    var alertPopup = SimplePopups.CreateAlertPopup("Service order is pending.", updatedOrder.StatusDescription || "The vendor has received your order.", null, header);
                    LQBPopup.ShowElement($('<div>').append(alertPopup), {
                        width: 350,
                        height: 200,
                        elementClasses: 'FullWidthHeight',
                        hideCloseButton: true,
                        onReturn: function() {
                            if(!usePollMethod) {
                                $(associatedRow).closest('tbody').append(newRow);
                            }
                            else {
                                $(associatedRow).replaceWith(newRow);
                            }
                            HideEmptyVoeColumns()

                            if (results.value['Key']) {
                                window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=multipdf&sLId=" + ML.sLId + "&ckey=" + results.value['Key'], '_self')
                            }
                        }
                    });
                }
                else if(updatedOrder.Status === 1 || updatedOrder.Status === 5) {
                    var alertPopup = SimplePopups.CreateAlertPopup("Order results have been received.", "", '../../images/success.png', header);
                    LQBPopup.ShowElement($('<div>').append(alertPopup), {
                        width: 350,
                        height: 200,
                        elementClasses: 'FullWidthHeight',
                        hideCloseButton: true,
                        onReturn: function() {
                            if(!usePollMethod && updatedOrder.ServiceType == 1) {
                                // Special behavior for VOE/I.
                                $(associatedRow).closest('tbody').append(newRow);
                            }
                            else {
                                $(associatedRow).replaceWith(newRow);
                            }
                            HideEmptyVoeColumns()

                            var pdfKey = results.value['Key'];
                            if (typeof(results.value['HasReturnedAssets']) === 'string' && results.value['HasReturnedAssets'].toLowerCase() === 'true') {
                                LQBPopup.Show(ML.VirtualRoot + '/newlos/Verifications/ImportUpdateAssets.aspx?loanid=' + ML.sLId + '&order=' + results.value['OrderId'],
                                {
                                    width: 500,
                                    height: 500,
                                    elementClasses: 'FullWidthHeight',
                                    hideCloseButton: true,
                                    onReturn: function(result) {
                                        var verifiedPopup = SimplePopups.CreateAlertPopup(
                                            result.Success ? "Verified Assets Were Imported" : "No Assets Were Imported" , 
                                            result.Success ? (result.Added + " assets were added, " + result.Modified + " assets were modified, and " + result.Removed + " assets were removed.")
                                                : "",
                                            result.Success ? '../../images/success.png' : null, header);
                                        window.setTimeout(function() {
                                            LQBPopup.ShowElement($('<div>').append(verifiedPopup), {
                                                width: 350,
                                                height: 200,
                                                elementClasses: 'FullWidthHeight',
                                                hideCloseButton: true,
                                                onReturn: function() {
                                                    if (pdfKey) {
                                                        window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=multipdf&sLId=" + ML.sLId + "&ckey=" + pdfKey, '_self');
                                                    }
                                                }
                                            });
                                        });
                                    }
                                });
                            }
                            else if (pdfKey) {
                                window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=multipdf&sLId=" + ML.sLId + "&ckey=" + pdfKey, '_self');
                            }
                        }
                    });
                }
                else {
                    var errors = [updatedOrder.Error];
                    var errorPopup = SimplePopups.CreateErrorPopup("Something went wrong", errors, header);
                    LQBPopup.ShowElement($('<div>').append(errorPopup), {
                        width: 350,
                        height: 200,
                        hideCloseButton: true,
                        elementClasses: 'FullWidthHeight',
                        onReturn: function() {
                            // VOE/I behavior only
                            if(updatedOrder.ServiceType == 1) { 
                                $(associatedRow).closest('tbody').append(newRow);
                            }
                            else {
                                $(associatedRow).replaceWith(newRow);
                            }
                            HideEmptyVoeColumns();
                        }
                    });
                }
            }

            function LoadVOATable()
            {
                $.each(VOAOrders, function(index, value) {
                    var row = VOAOrderRow.CreateVoaRow(value);
                    $('#VOATable tbody').append(row);
                });
            }

            function LoadVOETable() {
                $.each(VOEOrders, function(index, value) {
                    var row = VOEOrderRow.CreateVOERow(value);
                    $('#VOETable tbody').append(row);
                });
                HideEmptyVoeColumns();
            }

            function LoadSSA89Table() {
                $.each(SSA89Orders, function(index, value) {
                    var row = SSA89OrderRow.CreateSsa89Row(value);
                    $('#SSA89Table tbody').append(row);
                });
            }

            function HideEmptyVoeColumns() {
                if ($('#VOETable .ParentOrderNumber').toArray().every(function (elem) { return elem.innerHTML === ''; })) {
                    $('#VOETable .ParentOrderNumber,#VOETable a[data-sorttarget=ParentOrderNumber]').closest('td,th').hide();
                }
            }

            function OpenOrderPage(url, tableId, rowCreationMethod, width, height) {
                var w = 1000;
                if(width) {
                    w = width;
                }

                var h = 600;
                if(height) {
                    h = height;
                }

                LQBPopup.Show(url, {
                    hideCloseButton: true,
                    width: w,
                    height: h,
                    onReturn: function(returnArgs) {
                        if(typeof(returnArgs) != 'undefined' && returnArgs != null)
                        {
                            var orders = returnArgs.Orders;
                            $.each(orders, function(index, order) {
                                var row = rowCreationMethod(order);
                                $('#' + tableId + ' tbody').append(row);
                            });
                            HideEmptyVoeColumns();

                            var cacheKey = returnArgs.StitchedCacheKey;
                            if(cacheKey) {
                                window.open(ML.EDocsRoot + "/newlos/ElectronicDocs/ViewEdocPdf.aspx?cmd=multipdf&sLId=" + ML.sLId + "&ckey=" + cacheKey, '_self')
                            }
                        }
                    }
                }, null);
            }

            VOAOrderRow.PostTemplateLoadCallback(LoadVOATable);
            VOEOrderRow.PostTemplateLoadCallback(LoadVOETable);
            SSA89OrderRow.PostTemplateLoadCallback(LoadSSA89Table);
        });
    </script>
    <form id="form1" runat="server">
        <div>
            <div class="MainRightHeader">Verifications Ordering Dashboard</div>
            <div id="container">
                <div class="Tabs">
                    <ul class="tabnav">
                        <li class="selected Tab" id="VODTab">
                            <a id="VODLink" class="TabLink" data-content="VOD">VOA/VOD</a>
                        </li>
                        <li id="VOETab" class="Tab">
                            <a id="VOELink" class="TabLink" data-content="VOE">VOE/VOI</a>
                        </li>
                        <li id="SSA89Tab" class="Tab">
                            <a id="SSA89Link" class="TabLink" data-content="SSA89">SSA-89</a>
                        </li>
                    </ul>
                </div>
                <br />
                <div class="Content PaddingLeftRight5">
                    <div id="VODTabContent" class="TabContent">
                        <table class="Table no-wrap" id="VOATable">
                            <thead>
                                <tr class="LoanFormHeader">
                                    <td><a data-sort="true" data-sorttarget="Institution" data-asc="false">Institution</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="AccountNumber" data-asc="false">Account Number</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="LenderServiceName" data-asc="false">Service Provider</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="OrderNumber" data-asc="false">Order Number</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Status" data-asc="false">Status</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="HasBeenRefreshed" data-asc="false">Refresh Order?</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Results" data-asc="false">Results</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Action" data-asc="false">Action</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="OrderedBy" data-asc="false">Ordered By</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="DateOrdered" data-asc="false">Date Ordered</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="DateCompleted" data-asc="false">Date Completed</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <br />
                        <input type="button" class="MarginLeft" id="OrderVODBtn" value="Order New VOA/VOD" />
                        <span class="FloatRight">
                            <a onclick="redirectToUladPage('Assets');">Please enter assets on the Assets page</a>
                        </span>
                    </div>
                    <div id="VOETabContent" class="Hidden TabContent">
                        <table class="Table NoWrap" id="VOETable">
                            <thead>
                                <tr class="LoanFormHeader">
                                    <td><a data-sort="true" data-sorttarget="Borrower" data-asc="false">Borrower</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Employer" data-asc="false">Employer</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Self" data-asc="false">Self</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="ServiceProvider" data-asc="false">Service Provider</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="OrderNumber" data-asc="false">Order Number</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="ParentOrderNumber" data-asc="false">Parent Order</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="VendorReferenceIds" data-asc="false">Vendor Reference Ids</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Status" data-asc="false">Status</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Results" data-asc="false">Results</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Action" data-asc="false">Action</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="OrderedBy" data-asc="false">Ordered By</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="DateOrdered" data-asc="false">Date Ordered</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="DateCompleted" data-asc="false">Date Completed</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Type" data-asc="false">Type</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <br />
                        <input type="button" class="MarginLeft" id="OrderVOEBtn" value="Order New VOE/VOI" />
                        <span class="FloatRight">
                            <a onclick="linkMe(gVirtualRoot + '/newlos/BorrowerInfo.aspx?pg=1');">Please enter employment on the Employment page</a>
                        </span>
                    </div>
                    <div id="SSA89TabContent" class="Hidden TabContent">
                        <div class="Hidden" id="SubProductStatusPopup">
                            <div class="FullWidthHeight Content CssTable">
                                <div class="CssTableRow">
                                    <div class="CssTableCell align-middle align-center">
                                        <h3>
                                            Order <span class="OrderNumber"></span> Sub-Product Statuses
                                        </h3>
                                        <table class="InlineBlock align-left">
                                            <tr>
                                                <td><span class="FieldLabel PaddingLeftRight5">Death Master</span></td>
                                                <td><span class="DmStatus"></span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="FieldLabel PaddingLeftRight5">SSA</span></td>
                                                <td><span class="SsaStatus"></span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="FieldLabel PaddingLeftRight5">OFAC</span></td>
                                                <td><span class="OfacStatus"></span></td>
                                            </tr>
                                            <tr>
                                                <td><span class="FieldLabel PaddingLeftRight5">Credit Header</span></td>
                                                <td><span class="ChStatus"></span></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="CssTableRow">
                                    <div class="align-bottom">
                                        <input type="button" class="BackSubStatusBtn" value="Back" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="Table NoWrap" id="SSA89Table">
                            <thead>
                                <tr class="LoanFormHeader">
                                    <td><a data-sort="true" data-sorttarget="BorrowerName" data-asc="false">Borrower</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Ssn" data-asc="false">SSN</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="DateOfBirth" data-asc="false">Date of Birth</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="LenderServiceName" data-asc="false">Service Provider</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="OrderNumber" data-asc="false">Order Number</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Status" data-asc="false">Status</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Results" data-asc="false">Results</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Action" data-asc="false">Action</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="OrderedBy" data-asc="false">Ordered By</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="DateOrdered" data-asc="false">Date Ordered</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="DateCompleted" data-asc="false">Date Completed</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <br />
                        <input type="button" class="MarginLeft" id="OrderSSA89Btn" value="Order New SSN Verification" />
                    </div>
                </div>
                <div class="Hidden" id="CredentialsPopup">
                    <div id="Credentials" class="FullWidthHeight Content CssTable">
                        <div class="CssTableRow">
                            <div class="CssTableCell align-middle align-center">
                                <h3>Credentials</h3>
                                <table class="InlineBlock">
                                    <tr class="align-left" id="AccountIdRow">
                                        <td class="FieldLabel">Account ID</td>
                                        <td>
                                            <input SkipMe type="text" class="Input RequiredInput" id="AccountId" />
                                            <img alt="Required" class="RequiredImg" id="AccountIdImage" src="../../images/require_icon_red.gif" />
                                        </td>
                                    </tr>
                                    <tr class="align-left" id="UsernameRow">
                                        <td class="FieldLabel">Username</td>
                                        <td>
                                            <input SkipMe type="text" class="Input RequiredInput" id="Username" />
                                            <img alt="Required" class="RequiredImg" id="UsernameImg" src="../../images/require_icon_red.gif" />
                                        </td>
                                    </tr>
                                    <tr class="align-left" id="PasswordRow">
                                        <td class="FieldLabel">Password</td>
                                        <td>
                                            <input SkipMe type="password" class="Input RequiredInput" id="Password" />
                                            <img alt="Required" class="RequiredImg" id="PasswordImg" src="../../images/require_icon_red.gif" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="CssTableRow">
                            <div class="align-bottom">
                                <input type="button" id="OkCredentialsBtn" value="OK" />
                                <input type="button" id="CancelCredentialsBtn" value="Cancel" />
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </form>
</body>
</html>
