﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Security;

    /// <summary>
    /// Abstract class to use as a base for all order VOX pages.
    /// </summary>
    public abstract class OrderVOX : BaseLoanPage
    {
        /// <summary>
        /// Gets the lender service dropdown list.
        /// </summary>
        protected abstract DropDownList LenderServiceList
        {
            get;
        }

        /// <summary>
        /// Order specific OnInit actions.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected abstract void PerformOnInit(EventArgs e);

        /// <summary>
        /// Gets the VOX loader for the service.
        /// </summary>
        /// <returns>The VOX component loader.</returns>
        protected abstract VOXLoader GetLoader();

        /// <summary>
        /// Binds data for use on load if there is only a signle lender service to use.
        /// </summary>
        protected virtual void BindForSingleLenderService()
        {
            // TODO: We want to get rid of this. Let the page call the 'GetLenderService' service call on load instead.
        }

        /// <summary>
        /// Initialize function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            var loader = this.GetLoader();
            var availableServiceProviders = loader.GetLenderServicesAvailableToUser();
            foreach (var service in availableServiceProviders)
            {
                this.LenderServiceList.Items.Add(new ListItem(service.DisplayName, service.LenderServiceId.ToString()));
            }

            if (this.LenderServiceList.Items.Count > 1)
            {
                this.LenderServiceList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
            }
            else if (this.LenderServiceList.Items.Count == 1)
            {
                this.BindForSingleLenderService();
            }

            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterCSS("font-awesome.css");
            this.RegisterJsScript("VOX/VoxOrderCommon.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("AuditPage.js");
            this.RegisterJsScript("SimplePopups.js");
            this.RegisterJsGlobalVariables("VOXTimeoutInMilliseconds", ConstStage.VOXTimeoutInMilliseconds == 0 ? 0 : ConstStage.VOXTimeoutInMilliseconds + 1000);
            this.PerformOnInit(e);
            base.OnInit(e);
        }
    }
}
