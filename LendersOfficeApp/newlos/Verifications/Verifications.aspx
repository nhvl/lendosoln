<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="VOMTab" Src="VOMTab.ascx" %>
<%@ Register TagPrefix="uc1" TagName="VOLTab" Src="VOLTab.ascx" %>
<%@ Register TagPrefix="uc1" TagName="VOETab" Src="VOETab.ascx" %>
<%@ Register TagPrefix="uc1" TagName="VODTab" Src="VODTab.ascx" %>
<%@ Register TagPrefix="uc1" TagName="VOLandTab" Src="VOLandTab.ascx" %>
<%@ Register TagPrefix="uc1" TagName="VerbalVoeTab" Src="VerbalVoeTab.ascx" %>
<%@ Page language="c#" Codebehind="Verifications.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Verifications.Verifications" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>Verifications</title>


      <style>
          .VerbalVoeRecords
          {
              margin: 10px 0;
              width: 90%;
          }
          .DataGrid td { 
            padding-top: 4px; 
            padding-bottom: 4px; 
          }
          .DataGrid td input[type=text] {
            vertical-align: bottom;
          }
      </style>

  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="RightBackground" scroll="yes">
    <form id="Verifications" method="post" runat="server">
    <%--This datetextbox makes the dynamic ones added by the tabs work; don't remove it! --%>
    <ml:DateTextBox ID="always_hidden" Visible="false" runat="server"></ml:DateTextBox>
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
      <tr>
        <td nowrap class="Tabs">
            <uc1:Tabs runat="server" id="Tabs" />
        </td>
      </tr>
      <tr>
        <td nowrap>
          <uc1:VODTab ID="VODTab" runat="server" />
          <uc1:VOETab ID="VOETab" runat="server" />
          <uc1:VerbalVoeTab ID="VerbalVoeTab" runat="server" />
          <uc1:VOLTab ID="VOLTab" runat="server" />
          <uc1:VOMTab ID="VOMTab" runat="server" />
          <uc1:VOLandTab ID="VOLandTab" runat="server" />
        </td>
      </tr>
    </table>
    </form>
  </body>
</HTML>
