﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VerbalVoeTab.ascx.cs" Inherits="LendersOfficeApp.newlos.Verifications.VerbalVoeTab" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script>

    var editorUrl;

    $(document).ready(function () {
        $(".editLink").click(editRecord);
        $(".addButton").click(addRecord);
        $(".previewLink").click(viewPdf);
        $(".deleteButton").click(deleteRecord);

        editorUrl = ML.VirtualRoot + "/newlos/Verifications/VerbalVoeEditor.aspx?loanid=" + ML.sLId + "&appid=" + ML.aAppId;
    });

    function editRecord() {
        var url = editorUrl + "&recordId=" + $(this).data("id");
        self.location = (url);
    }

    function addRecord() {
        self.location = (editorUrl);
    }

    function deleteRecord() {
        var args = getAllFormValues();

        var records = [];

        $(".deleteCheckbox:checked").each(function () {
            records.push($(this).data("id"));
        });

        args["RecordIds"] = JSON.stringify(records);
        gService.main.call("Delete", args);
        location.href = location.href;
    }

    function viewPdf() {
        var id = $(this).data("id");
        var url = ML.VirtualRoot + "/pdf/VerbalVoePdf.aspx?loanid=" + ML.sLId + "&applicationid=" + ML.aAppId + "&recordid=" + id;
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
    }
</script>

<asp:DataGrid ID="VerbalVoeRecords" CssClass="VerbalVoeRecords" runat="server" AutoGenerateColumns="false">
    <HeaderStyle CssClass="GridHeader" />
    <ItemStyle CssClass="GridItem" />
    <AlternatingItemStyle CssClass="GridAlternatingItem" />
    <Columns>
        <asp:TemplateColumn>
            <ItemTemplate>
                <input doesntDirty type="checkbox" class="deleteCheckbox" data-id="<%# (AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id"))) %>" />
            </ItemTemplate>
        </asp:TemplateColumn>

        <asp:TemplateColumn HeaderText="Borrower">
            <ItemTemplate>
                <%# (AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BorrowerName")))  %>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Employer">
            <ItemTemplate>
                <%# (AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EmployerName")))  %>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Date of Call">
            <ItemTemplate>
                <%# (AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DateOfCall")))  %>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn>
            <ItemTemplate>
                <a class="editLink" href="#" data-id="<%# (AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id"))) %>">edit</a>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn>
            <ItemTemplate>
                <a href="#" class="previewLink" data-id="<%# (AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id"))) %>">preview</a>
            </ItemTemplate>
        </asp:TemplateColumn>

    </Columns>
</asp:DataGrid>

<div>
    <input type="button" class="addButton" value="Add Verbal VOE" />
    <input type="button" class="deleteButton" value="Delete Selected" />
</div>
