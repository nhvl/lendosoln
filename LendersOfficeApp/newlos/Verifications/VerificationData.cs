using System;

namespace LendersOfficeApp.newlos.Verifications
{
    /// <summary>
    /// Summary description for VerificationData.
    /// </summary>
    public class VerificationData
    {
        private Guid m_recordID;
        private Guid m_applicationID;
        private string m_description;
        private string m_otherDescription0;
        private bool m_isBorrower;
        private int m_index;
        private string m_verifSentD;
        private string m_verifReorderedD;
        private string m_verifRecvD;
        private string m_verifExpD;


        public Guid RecordID
        {
            get { return m_recordID; }
            set { m_recordID = value; }
        }


        public Guid ApplicationID
        {
            get { return m_applicationID; }
            set { m_applicationID = value; }
        }

        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }


        public bool IsBorrower
        {
            get { return m_isBorrower; }
            set { m_isBorrower = value; }
        }

        public int Index
        {
            get { return m_index; }
            set { m_index = value; }
        }

        public string VerifSentD
        {
            get { return m_verifSentD; }
            set { m_verifSentD = value; }
        }

        public string VerifReorderedD
        {
            get { return m_verifReorderedD; }
            set { m_verifReorderedD = value; }
        }

        
        public string VerifRecvD
        {
            get { return m_verifRecvD; }
            set { m_verifRecvD = value; }
        }

        public string VerifExpD
        {
            get { return m_verifExpD; }
            set { m_verifExpD = value; }
        }

        public string OtherDescription0 
        {
            get { return m_otherDescription0; }
            set { m_otherDescription0 = value; }
        }



        public VerificationData(string description, Guid applicationID, Guid recordID) :
            this(description, applicationID, recordID, true)
        {
        }
        public VerificationData(string description, Guid applicationID, Guid recordID, bool isBorrower) :
            this (description, applicationID, recordID, isBorrower, "", "", "", "")
        {

        }
        public VerificationData(string description, Guid applicationID, Guid recordID, bool isBorrower, string verifSentD, string verifReorderedD, string verifRecvD, string verifExpD) :
            this (description, applicationID, recordID, isBorrower, verifSentD, verifReorderedD, verifRecvD, verifExpD, "")
        {
        }
        public VerificationData(string description, Guid applicationID, Guid recordID, bool isBorrower, string verifSentD, string verifReorderedD, string verifRecvD, string verifExpD, string otherDescription0) 
        {
            m_description = description;
            m_applicationID = applicationID;
            m_recordID = recordID;
            m_isBorrower = isBorrower;
            m_verifSentD = verifSentD;
            m_verifReorderedD = verifReorderedD;
            m_verifRecvD = verifRecvD;
            m_verifExpD = verifExpD;
            m_otherDescription0 = otherDescription0;


        }
        public VerificationData(string description, Guid applicationID, int index) :
            this (description, applicationID, index, true) 
        {
        }
        public VerificationData(string description, Guid applicationID, int index, bool isBorrower) :
            this (description, applicationID, index, isBorrower, "", "", "", "")
        {
        }
        public VerificationData(string description, Guid applicationID, int index, bool isBorrower, string verifSentD, string verifReorderedD, string verifRecvD, string verifExpD) :
            this (description, applicationID, index, isBorrower, verifSentD, verifReorderedD, verifRecvD, verifExpD, "")

        {
        }
        public VerificationData(string description, Guid applicationID, int index, bool isBorrower, string verifSentD, string verifReorderedD, string verifRecvD, string verifExpD, string otherDescription0) 
        {
            m_description = description;
            m_applicationID = applicationID;
            m_index = index;
            m_isBorrower = isBorrower;
            m_verifSentD = verifSentD;
            m_verifReorderedD = verifReorderedD;
            m_verifRecvD = verifRecvD;
            m_verifExpD = verifExpD;
            m_otherDescription0 = otherDescription0;

        }
    }
}
