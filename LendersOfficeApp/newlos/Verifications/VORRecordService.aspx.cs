using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using LendersOffice.Common;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Verifications
{

	public partial class VORRecordService : AbstractSingleEditService
	{

        protected override void CustomProcess(string methodName)
        {
            switch (methodName)
            {
                case "ApplySig":
                    ApplySig();
                    break;
                case "ClearSig":
                    ClearSig();
                    break;
                default:
                    break;
            }
        }

        private void ApplySig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
            IVerificationOfRent field = dataApp.GetVorFields(RecordID);
            field.ApplySignature(principal.EmployeeId, signInfo.SignatureKey.ToString());
            field.Update();
            dataLoan.Save();

            SetResult("EmployeeId", principal.EmployeeId);
        }

        private void ClearSig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IVerificationOfRent field = dataApp.GetVorFields(RecordID);
            field.ClearSignature();
            field.Update();
            dataLoan.Save();
        }


	    protected override void SaveData()
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IVerificationOfRent field = dataApp.GetVorFields(RecordID);
            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");
            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfRent, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("PreparerName");
            f.Title = GetString("PreparerTitle");
            f.CompanyName = GetString("PreparerCompanyName");
            f.StreetAddr = GetString("PreparerStreetAddr");
            f.City = GetString("PreparerCity");
            f.State = GetString("PreparerState");
            f.Zip = GetString("PreparerZip");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");
            f.Phone = GetString("PreparerPhone");
            f.FaxNum = GetString("PreparerFaxNum");
            f.Update();

            SaveData(field);

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            SetResult("PrevRecordID", field.RecordId.ToString());
        }

        protected override void SaveDataAndLoadNext()
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IVerificationOfRent field = dataApp.GetVorFields(RecordID);
            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");
            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfRent, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName    = GetString("PreparerName");
            f.Title           = GetString("PreparerTitle");
            f.CompanyName     = GetString("PreparerCompanyName");
            f.StreetAddr      = GetString("PreparerStreetAddr");
            f.City            = GetString("PreparerCity");
            f.State           = GetString("PreparerState");
            f.Zip             = GetString("PreparerZip");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");
            f.Phone           = GetString("PreparerPhone");
            f.FaxNum          = GetString("PreparerFaxNum");

            f.Update();

            SaveData(field);

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            field = dataApp.GetVorFields(NextRecordID);
            LoadData(dataLoan, dataApp, field);
        }

        protected override void LoadData()
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            IVerificationOfRent field = dataApp.GetVorFields(RecordID);

            LoadData(dataLoan, dataApp, field);

        }

        private void SaveData(IVerificationOfRent field) 
        {
            field.LandlordCreditorName = GetString("LandlordCreditorName") ;
            field.Attention            = GetString("Attention");
            field.AddressTo            = GetString("AddressTo") ;
            field.CityTo               = GetString("CityTo") ;
            field.StateTo              = GetString("StateTo") ;
            field.ZipTo                = GetString("ZipTo") ;
            field.PhoneTo              = GetString("PhoneTo");
            field.FaxTo                = GetString("FaxTo");
            field.AccountName          = GetString("AccNm") ;
            field.IsSeeAttachment      = GetBool("IsSeeAttachment");

            field.VerifExpD_rep			= GetString("VerifExpD");
            field.VerifRecvD_rep		= GetString("VerifRecvD");
            field.VerifSentD_rep		= GetString("VerifSentD");
            field.VerifReorderedD_rep	= GetString("VerifReorderedD");

            field.Update();
        }

        private void LoadData(CPageData dataLoan, CAppData dataApp, IVerificationOfRent field) 
        {
            SetResult("sLenderNumVerif", dataLoan.sLenderNumVerif);

            IPreparerFields f1 = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfRent, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f1.IsValid) 
            {
                SetResult("PreparerName", f1.PreparerName);
                SetResult("PreparerTitle", f1.Title);
                SetResult("PreparerCompanyName", f1.CompanyName );
                SetResult("PreparerStreetAddr", f1.StreetAddr );
                SetResult("PreparerCity", f1.City );
                SetResult("PreparerState", f1.State );
                SetResult("PreparerZip", f1.Zip );
                SetResult("PreparerPrepareDate", f1.PrepareDate_rep );
                SetResult("PreparerPhone", f1.Phone);
                SetResult("PreparerFaxNum", f1.FaxNum);

            }
            SetResult("Attention", field.Attention);
            SetResult("RecordID", field.RecordId);
            SetResult("LandlordCreditorName", field.LandlordCreditorName);
            SetResult("AddressTo", field.AddressTo);
            SetResult("CityTo", field.CityTo);
            SetResult("StateTo", field.StateTo);
            SetResult("ZipTo", field.ZipTo);
            SetResult("PhoneTo", field.PhoneTo);
            SetResult("FaxTo", field.FaxTo);
            SetResult("AccNm", field.AccountName);

            SetResult("VerifSentD", field.VerifSentD_rep);
            SetResult("VerifRecvD", field.VerifRecvD_rep);
            SetResult("VerifExpD", field.VerifExpD_rep);
            SetResult("VerifReorderedD", field.VerifReorderedD_rep);
            SetResult("IsSeeAttachment", field.IsSeeAttachment);

            SetResult("VerifSigningEmployeeId", field.VerifSigningEmployeeId);
            SetResult("VerifHasSignature", field.VerifHasSignature);

            // For property address to verify use address enter in borrower information (aBAddr, aBPrev1Addr ....)
            if (field.RecordId == new Guid("11111111-1111-1111-1111-111111111111")) 
            {
                SetResult("PropertyAddress", dataApp.aBAddr);
                SetResult("PropertyCity", dataApp.aBCity);
                SetResult("PropertyState", dataApp.aBState);
                SetResult("PropertyZipcode", dataApp.aBZip);
            } 
            else if (field.RecordId == new Guid("22222222-2222-2222-2222-222222222222")) 
            {
                SetResult("PropertyAddress", dataApp.aBPrev1Addr);
                SetResult("PropertyCity", dataApp.aBPrev1City);
                SetResult("PropertyState", dataApp.aBPrev1State);
                SetResult("PropertyZipcode", dataApp.aBPrev1Zip);

            }
            else if (field.RecordId == new Guid("33333333-3333-3333-3333-333333333333")) 
            {
                SetResult("PropertyAddress", dataApp.aBPrev2Addr);
                SetResult("PropertyCity", dataApp.aBPrev2City);
                SetResult("PropertyState", dataApp.aBPrev2State);
                SetResult("PropertyZipcode", dataApp.aBPrev2Zip);
            }
            else if (field.RecordId == new Guid("44444444-4444-4444-4444-444444444444")) 
            {
                SetResult("PropertyAddress", dataApp.aCAddr);
                SetResult("PropertyCity", dataApp.aCCity);
                SetResult("PropertyState", dataApp.aCState);
                SetResult("PropertyZipcode", dataApp.aCZip);

            }
            else if (field.RecordId == new Guid("55555555-5555-5555-5555-555555555555")) 
            {
                SetResult("PropertyAddress", dataApp.aCPrev1Addr);
                SetResult("PropertyCity", dataApp.aCPrev1City);
                SetResult("PropertyState", dataApp.aCPrev1State);
                SetResult("PropertyZipcode", dataApp.aCPrev1Zip);
            }
            else if (field.RecordId == new Guid("66666666-6666-6666-6666-666666666666")) 
            {
                SetResult("PropertyAddress", dataApp.aCPrev2Addr);
                SetResult("PropertyCity", dataApp.aCPrev2City);
                SetResult("PropertyState", dataApp.aCPrev2State);
                SetResult("PropertyZipcode", dataApp.aCPrev2Zip);
            }


        }
    }
}
