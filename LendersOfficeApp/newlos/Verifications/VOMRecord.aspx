<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="VOMRecord.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Verifications.VOMRecord" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>VOM Record</title>
        <style type="text/css">
        .verif_headers { text-decoration: underline; }
        #ApplySig, #ClearSig {   display: block;  margin-right: 10px; float:left;  }
        .clear { clear: both; }
    </style>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
	<script language=javascript>
  <!--
        var uladToLegacyMap = {
            Attention: 'Attention',

            CompanyName: 'ComNm',
            CompanyAddress: 'ComAddr',
            CompanyCity: 'ComCity',
            CompanyState: 'ComState',
            CompanyZip: 'ComZip',

            IsSeeAttachment: 'IsSeeAttachment',

            VerifSentDate: 'VerifSentD',
            VerifReorderedDate: 'VerifReorderedD',
            VerifRecvDate: 'VerifRecvD',
            VerifExpiresDate: 'VerifExpD',
            AccountName: 'AccNm',
            AccountNum: 'AccNum'
        };

        function retrieveSelectedReoId() {
            return $('#MatchedReRecordId').val();
        }

        function pageSpecificPopulation(recordList, otherData) {
          // Populate the REO dropdown
            var realProperties = otherData.RealProperties;
            var realPropertyLiabilities = otherData.RealPropertyLiabilities;

            var associatedRealPropertyId = window.getLinkedReoId(ML.recordId, Object.values(realPropertyLiabilities));

            var matchedRecordId = $('#MatchedReRecordId');

            matchedRecordId[0].selectedIndex = 0;
            $('#PropertyAddress').val('');
            $('#PropertyCity').val('');
            $('#PropertyState').val('');
            $('#PropertyZipcode').val('');

            var shouldPopulateMatchedRecordId = matchedRecordId.children().length <= 1;
            for (var i = 0; i < realProperties.length; i++) {
                var realProperty = realProperties[i];

                if (shouldPopulateMatchedRecordId) {
                    var text = realProperty.StreetAddress + ', ' + realProperty.City + ', ' + realProperty.State + ', ' + realProperty.Zip;
                    var option = $('<option></option>').attr("value", realProperty.Id).text(text);
                    matchedRecordId.append(option);
                }   

                if (realProperty.Id == associatedRealPropertyId) {
                    $('#PropertyAddress').val(realProperty.StreetAddress);
                    $('#PropertyCity').val(realProperty.City);
                    $('#PropertyState').val(realProperty.State);
                    $('#PropertyZipcode').val(realProperty.Zip);
                    matchedRecordId.val(realProperty.Id);
                }
            }
        }

      function createListOfEntries(entries, selectedAsset) {
          list = [];
          for (var i = 0; i < entries.length; i++) {
              var entry = entries[i];

              if (entry.RequiresVom) {
                  list.push(entry.Id);

                  if (entry.Id == ML.recordId) {
                      currentIndex = list.length - 1;
                  }
              }
          }
      }

    var openedDate = <%=AspxTools.JsString(m_openedDate) %>;
    function onDateKeyUp(o, event) {
      if (event.keyCode == 79) {
        o.value = openedDate;
        updateDirtyBit(event);
      }
    }
    function f_getREOAddress() {
      var args = new Object();
      args["LoanID"] = <%=AspxTools.JsString(LoanID) %>;
      args["ApplicationID"] = <%= AspxTools.JsString(ApplicationID) %>;
      args["MatchedReRecordId"] = <%= AspxTools.JsGetElementById(MatchedReRecordId) %>.value;
      
      var result = gService.singleedit.call("GetREOAddress", args);
      if (!result.error) {
        <%= AspxTools.JsGetElementById(PropertyAddress) %>.value = result.value["PropertyAddress"];
        <%= AspxTools.JsGetElementById(PropertyCity) %>.value = result.value["PropertyCity"];
        <%= AspxTools.JsGetElementById(PropertyState) %>.value = result.value["PropertyState"];
        <%= AspxTools.JsGetElementById(PropertyZipcode) %>.value = result.value["PropertyZipcode"];
      }
    }
            function _init() {
                var enableSignature = document.getElementById('EnableSignature').value === 'True';
                if (!enableSignature) {
                    document.getElementById('SignatureRow').style.display = "none";
                    return;
                }

                var hasSignature = document.getElementById('EmployeeHasSignature').value === 'True';
                var missingSignature = document.getElementById('MissingSignature');
                missingSignature.style.display = hasSignature ? 'none' : '';
                var applySig = document.getElementById('ApplySig');
                applySig.style.display = hasSignature ? 'block' : 'none';
                postPopulateForm();
            }
            
            function postPopulateForm() {
                var verifHasSignature = document.getElementById('VerifHasSignature').value === 'True';
                var verifSigningEmployeeId = document.getElementById('VerifSigningEmployeeId').value; 
                var clearSig = document.getElementById('ClearSig');
                clearSig.style.display = verifHasSignature ? 'block' : 'none';
                var img = document.getElementById('SignatureImg');
                
                if( verifHasSignature ) {
                    img.src = 'UserSignature.aspx?eid=' + verifSigningEmployeeId;
                }
                else {
                    img.src = 'UserSignature.aspx';
                }
            }
            
            function onclearsig() {
                if( false == confirm('Remove existing signature from verification?') ) {
                    return;
                }   
                var recordId = document.getElementById('RecordID').value;
                var img = document.getElementById('SignatureImg');
                
                var args = { 
                    'recordid' : recordId,
                     'loanid'  : ML.sLId,
                    'applicationid' :  ML.aAppId
                     
                };
                var results = gService.singleedit.call('ClearSig', args);
                if( results.error ) {
                    alert(results.UserMessage);
                }
                else {
                    img.src = "UserSignature.aspx";
                    document.getElementById('VerifHasSignature').value = 'False';
                    document.getElementById('VerifSigningEmployeeId').value = '00000000-0000-0000-0000-000000000000';                   
                }
                return false;
                
            }
            
            function onapplysig() {
                var username = document.getElementById('EmployeeName').value;
                var verifName = document.getElementById('PreparerName');
                
                if( username != verifName.value ) {
                    if( false == confirm('Your name will be populated to match your signature. Review other fields for accuracy.') ) {
                        return; 
                    }
                    updateDirtyBit();
                    verifName.value = username;
                }
                
                var recordId = document.getElementById('RecordID').value;
                var img = document.getElementById('SignatureImg');
                var clearSig = document.getElementById('ClearSig');
                
                
                
                var args = { 
                    'recordid' : recordId,
                    'loanid' : ML.sLId,
                    'applicationid' :  ML.aAppId
                };
                
                
                var results = gService.singleedit.call('ApplySig', args);
                if( results.error ) {
                    alert(results.UserMessage);
                }
                else {
                    img.src = "UserSignature.aspx?eid="+ results.value.EmployeeId ;
                    clearSig.style.display = 'block';
                    document.getElementById('VerifHasSignature').value = 'True';
                    document.getElementById('VerifSigningEmployeeId').value = results.value.EmployeeId;
                }
                
                return false;
            }

        function printVOM() {
            PolyShouldShowConfirmSave(isDirty(), function () {
                var applicationid = document.getElementById("applicationid").value;
                var recordid = document.getElementById("RecordID").value;
                var url = 'pdf/VOM.aspx?loanid=' + <%=AspxTools.JsString(LoanID) %> +'&applicationid=' + applicationid + '&recordid=' + recordid +  '&crack=' + new Date();

                    LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
            }, saveMe);


        }
  //-->
    </script>	
    <form id="VOMRecord" method="post" runat="server">
    <asp:HiddenField runat="server" ID="VerifSigningEmployeeId" />
    <asp:HiddenField runat="server" ID="VerifHasSignature" />
<TABLE class=FormTable id=Table1 cellSpacing=0 cellPadding=0 border=0>
  <TR>
    <TD class=MainRightHeader noWrap>Verification of Mortgage <span id=IndexLabel></span></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE id=Table5 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD vAlign=top noWrap>
            <TABLE class=InsetBorder id=Table6 cellSpacing=0 cellPadding=0 
            border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table3 cellSpacing=0 cellPadding=0 border=0 
                  DESIGNTIMEDRAGDROP="43">
                    <TR>
                      <TD class=FieldLabel noWrap colSpan=2><span class="verif_headers"> To: </span></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Attn</TD>
                      <TD noWrap><asp:TextBox id=Attention runat="server" Width="203px"></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Name</TD>
                      <TD noWrap><asp:TextBox id=ComNm runat="server" Width="203px" ReadOnly="True"></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Address</TD>
                      <TD noWrap><asp:TextBox id=ComAddr runat="server" Width="203px" ReadOnly="True"></asp:TextBox></TD></TR>
                    <TR>
                      <TD noWrap></TD>
                      <TD noWrap><asp:TextBox id=ComCity runat="server" Width="110px" ReadOnly="True"></asp:TextBox><asp:TextBox id=ComState runat="server" Width="40px" ReadOnly="True"></asp:TextBox><asp:TextBox id=ComZip runat="server" Width="50px" ReadOnly="True"></asp:TextBox></TD></TR>
                    <TR>
                      <TD noWrap class=FieldLabel>Phone</TD>
                      <TD noWrap><ml:PhoneTextBox id=ComPhone runat="server" width="120" preset="phone" ReadOnly="True"></ml:PhoneTextBox></TD></TR>
                    <TR>
                      <TD noWrap class=FieldLabel>Fax</TD>
                      <TD noWrap><ml:PhoneTextBox id=ComFax runat="server" width="120" preset="phone" ReadOnly="True"></ml:PhoneTextBox></TD></TR></TABLE></TD></TR></TABLE></TD>
          <TD vAlign=top noWrap>
            <TABLE class=InsetBorder id=Table7 cellSpacing=0 cellPadding=0 
            border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table2 cellSpacing=0 cellPadding=0 border=0>
                    <TR>
                      <TD class=FieldLabel noWrap><span class="verif_headers"> From:</span></TD>
                      <TD noWrap></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Name</TD>
                      <TD noWrap><asp:TextBox id=PreparerName runat="server" Width="203"></asp:TextBox></TD></TR>
                        <TR id="SignatureRow">
                        <TD class=FieldLabel valign="top">Apply Signature</TD>
                        <TD width="207px">
                           <img src="UserSignature.aspx" alt="Signature" id="SignatureImg" width="168" />
                            <a href="#" style="display:none" id="ApplySig" onclick="return onapplysig()">apply my signature</a> <a
                                href="#" style="display:none"  id="ClearSig" onclick="return onclearsig()">clear signature</a>
                            <br class="clear" />
                            <span id="MissingSignature" style="display:none">
                            To use your signature, upload your signature image to your profile.
                            </span>
                            <br />
                        </TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Title:</TD>
                      <TD noWrap><asp:TextBox id=PreparerTitle runat="server" Width="203px"></asp:TextBox></TD></TR>
                    <tr>
                      <td class=FieldLabel nowrap>Broker Name</td>
                      <td nowrap><asp:TextBox id=PreparerCompanyName runat="server" Width="203px"></asp:TextBox></td></tr>
                    <TR>
                      <TD class=FieldLabel noWrap>Broker Address</TD>
                      <TD noWrap><asp:TextBox id=PreparerStreetAddr runat="server" Width="203px" ></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap></TD>
                      <TD noWrap><asp:TextBox id=PreparerCity runat="server" Width="115px" ></asp:TextBox><ml:StateDropDownList id=PreparerState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=PreparerZip runat="server" preset="zipcode" width="50"></ml:ZipcodeTextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Phone</TD>
                      <TD noWrap><ml:PhoneTextBox id=PreparerPhone runat="server" width="120" preset="phone"></ml:PhoneTextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Fax</TD>
                      <TD noWrap><ml:PhoneTextBox id=PreparerFaxNum runat="server" width="120" preset="phone"></ml:PhoneTextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Lender Number</TD>
                      <TD noWrap><asp:TextBox id=sLenderNumVerif runat="server"></asp:TextBox></TD></TR>
                    <tr>
                      <td class=FieldLabel nowrap>Prepared Date</td>
                      <td nowrap><ml:DateTextBox id=PreparerPrepareDate runat="server" width="75" preset="date"></ml:DateTextBox></td></tr>
                    <tr>
                      <td class=FieldLabel nowrap colspan=2><asp:CheckBox id=IsSeeAttachment runat="server" Text="Print 'See Attachment' in borrower signature"></asp:CheckBox></td></tr></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE class=InsetBorder id=Table8 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD noWrap>
            <TABLE id=Table4 cellSpacing=0 cellPadding=0 border=0>
              <TR>
                <TD class=FieldLabel noWrap>Information to be verified</TD>
                <TD noWrap></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap></TD>
                <TD noWrap><asp:DropDownList id=MatchedReRecordId runat="server" onchange="f_getREOAddress();"></asp:DropDownList></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>Property Address</TD>
                <TD noWrap><asp:TextBox id=PropertyAddress runat="server" Width="249px" ReadOnly="True"></asp:TextBox></TD></TR>
              <TR>
                <TD noWrap></TD>
                <TD noWrap><asp:TextBox id=PropertyCity runat="server" Width="142px" ReadOnly="True"></asp:TextBox><asp:TextBox id=PropertyState runat="server" Width="40px" ReadOnly="True"></asp:TextBox><asp:TextBox id=PropertyZipcode runat="server" Width="65px" ReadOnly="True"></asp:TextBox></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>Account Name</TD>
                <TD noWrap><asp:TextBox id=AccNm runat="server" ReadOnly="True"></asp:TextBox></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>Account Number</TD>
                <TD noWrap><asp:TextBox id=AccNum runat="server" ReadOnly="True"></asp:TextBox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE id=Table4 cellSpacing=0 cellPadding=0 border=0 class=InsetBorder>
        <TR>
          <TD class=FieldLabel noWrap colSpan=8>Verification (Shortcut: Enter 't' for today's 
            date. Enter 'o' for opened date.)</TD></TR>
        <TR>
          <TD class=FieldLabel noWrap> Ordered</TD>
          <TD noWrap><ml:DateTextBox id=VerifSentD runat="server" width="65px" preset="date" CssClass="mask" onkeyup="onDateKeyUp(this, event);"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap>Re-order</TD>
          <TD class=FieldLabel noWrap><ml:DateTextBox id=VerifReorderedD onkeyup="onDateKeyUp(this, event);" runat="server" CssClass="mask" preset="date" width="65px"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap> Received</TD>
          <TD noWrap><ml:DateTextBox id=VerifRecvD onkeyup="onDateKeyUp(this, event);" runat="server" CssClass="mask" preset="date" width="65px"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap> Expected</TD>
          <TD noWrap><ml:DateTextBox id=VerifExpD onkeyup="onDateKeyUp(this, event);" runat="server" CssClass="mask" preset="date" width="65px"></ml:DateTextBox></TD></TR></TABLE>    
    </TD></TR>
  <TR>
    <TD noWrap align=center>
<INPUT id=btnPrevious onclick=goPrevious(); type=button value=Previous name=btnPrevious> 
<INPUT id=btnNext onclick=goNext(); type=button value=Next name=btnNext>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<INPUT onclick=goToList(); type=button value="Back to VOM list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
<input onclick="printVOM();" type="button" value="Preview &amp; Print" />
   
    </TD></TR></TABLE>
     </form>
	
  </body>
</HTML>
