﻿#region Auto generated code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System;
    using System.Linq;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Security;

    /// <summary>
    /// Page for the verification dashboard.
    /// </summary>
    public partial class VerificationsDashboard : BaseLoanPage
    {
        /// <summary>
        /// The loader for all VOX components.
        /// </summary>
        private VOXLoaderFactory loaderFactory;

        /// <summary>
        /// Gets the reasons the privilege was not allowed during the page's workflow checks.
        /// </summary>
        /// <returns>An array containing the VOX operations to check.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderVoa, WorkflowOperations.OrderVoe, WorkflowOperations.OrderSSA89 };
        }

        /// <summary>
        /// Gets the extra ops to check during the page's workflow checks.
        /// </summary>
        /// <returns>An array containing the VOX operations to check.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderVoa, WorkflowOperations.OrderVoe, WorkflowOperations.OrderSSA89 };
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        protected override void LoadData()
        {
            if (!PrincipalFactory.CurrentPrincipal.BrokerDB.IsEnableVOXConfiguration)
            {
                return;
            }

            this.RegisterJsGlobalVariables("CanOrderVoa", this.UserHasWorkflowPrivilege(WorkflowOperations.OrderVoa));
            this.RegisterJsGlobalVariables("OrderVoaDenialReason", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.OrderVoa));
            this.RegisterJsGlobalVariables("CanOrderVoe", this.UserHasWorkflowPrivilege(WorkflowOperations.OrderVoe));
            this.RegisterJsGlobalVariables("OrderVoeDenialReason", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.OrderVoe));
            this.RegisterJsGlobalVariables("CanOrderSSA89", this.UserHasWorkflowPrivilege(WorkflowOperations.OrderSSA89));
            this.RegisterJsGlobalVariables("OrderSSA89DenialReason", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.OrderSSA89));

            this.loaderFactory = new VOXLoaderFactory(PrincipalFactory.CurrentPrincipal, this.LoanID);

            this.LoadVOAOrders();
            this.LoadVOEOrders();
            this.LoadSSA89Orders();
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void PageInit(object sender, System.EventArgs e)
        {
            if (!PrincipalFactory.CurrentPrincipal.BrokerDB.IsEnableVOXConfiguration)
            {
                return;
            }

            this.PageID = "VerificationDashboard";
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("VOX/VOAOrderRow.js");
            this.RegisterJsScript("VOX/VOEOrderRow.js");
            this.RegisterJsScript("VOX/SSA89OrderRow.js");
            this.RegisterJsScript("SimplePopups.js");
            this.RegisterService("vod", "/newlos/Verifications/OrderVODService.aspx");
            this.RegisterService("voe", "/newlos/Verifications/OrderVOEService.aspx");
            this.RegisterService("ssa89", "/newlos/Verifications/OrderSSA89Service.aspx");
            this.RegisterJsGlobalVariables("VOXTimeoutInMilliseconds", ConstStage.VOXTimeoutInMilliseconds == 0 ? 0 : ConstStage.VOXTimeoutInMilliseconds + 1000);
        }

        /// <summary>
        /// Initialize function.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            this.UseNewFramework = true;
            this.EnableJqueryMigrate = false;
            InitializeComponent();
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }

        /// <summary>
        /// Loads the VOA order data.
        /// </summary>
        private void LoadVOAOrders()
        {
            var voaLoader = this.loaderFactory.VoaLoader;
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("VOAOrders", voaLoader.LoadOrderViewModels());
        }

        /// <summary>
        /// Loads VOE orders.
        /// </summary>
        private void LoadVOEOrders()
        {
            var voeLoader = this.loaderFactory.VoeLoader;
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("VOEOrders", voeLoader.LoadOrderViewModels());
        }

        /// <summary>
        /// Loads SSA89 Orders.
        /// </summary>
        private void LoadSSA89Orders()
        {
            var ssa89Loader = this.loaderFactory.Ssa89Loader;
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("SSA89Orders", ssa89Loader.LoadOrderViewModels());
        }
    }
}
