using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Pdf;
using LendingQB.Core.Data;
using LqbGrammar.DataTypes;
using System;
using System.Data;

namespace LendersOfficeApp.newlos.Verifications
{

    public partial class VORRecord : BaseSingleEditPage<IVerificationOfRent>
	{
        protected System.Web.UI.WebControls.TextBox AccNum;

        protected string m_openedDate;
        private CPageData m_dataLoan;
        private CAppData m_dataApp;

        protected override string ListLocation 
        {
            get { return VirtualRoot + "/newlos/Verifications/Verifications.aspx?loanid=" + LoanID + "&pg=3"; }
        }

        protected override DataView GetDataView() 
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("recordid", typeof(string)));

            if (!this.IsUlad)
            {
                CAppData dataApp = m_dataLoan.GetAppData(ApplicationID);
                if (dataApp.aBAddrT == E_aBAddrT.Rent)
                {
                    DataRow row = table.NewRow();
                    row["recordid"] = "11111111-1111-1111-1111-111111111111";
                    table.Rows.Add(row);
                }
                if (dataApp.aBPrev1AddrT == E_aBPrev1AddrT.Rent)
                {
                    DataRow row = table.NewRow();
                    row["recordid"] = "22222222-2222-2222-2222-222222222222";
                    table.Rows.Add(row);
                }
                if (dataApp.aBPrev2AddrT == E_aBPrev2AddrT.Rent)
                {
                    DataRow row = table.NewRow();
                    row["recordid"] = "33333333-3333-3333-3333-333333333333";
                    table.Rows.Add(row);
                }
                if (dataApp.aCAddrT == E_aCAddrT.Rent)
                {
                    DataRow row = table.NewRow();
                    row["recordid"] = "44444444-4444-4444-4444-444444444444";
                    table.Rows.Add(row);
                }
                if (dataApp.aCPrev1AddrT == E_aCPrev1AddrT.Rent)
                {
                    DataRow row = table.NewRow();
                    row["recordid"] = "55555555-5555-5555-5555-555555555555";
                    table.Rows.Add(row);
                }
                if (dataApp.aCPrev2AddrT == E_aCPrev2AddrT.Rent)
                {
                    DataRow row = table.NewRow();
                    row["recordid"] = "66666666-6666-6666-6666-666666666666";
                    table.Rows.Add(row);
                }
            }
            else
            {
                foreach (var kvp in m_dataLoan.VorRecords)
                {
                    DataRow row = table.NewRow();
                    row["recordid"] = kvp.Key.ToString();
                    table.Rows.Add(row);
                }
            }

            return table.DefaultView;
        }

        protected override void LoadData()
        {
            this.m_dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(VORRecord));
            this.m_dataLoan.InitLoad();

            if(!IsUlad)
            {
                m_dataApp = m_dataLoan.GetAppData(ApplicationID);
            }

            base.LoadData();
        }

        protected override IVerificationOfRent RetrieveRecord() 
        {
            return m_dataApp.GetVorFields(RecordID);
        }

        protected VorRecord RetrieveUladRecord()
        {
            VorRecord record;
            m_dataLoan.VorRecords.TryGetValue(DataObjectIdentifier<DataObjectKind.VorRecord, Guid>.Create(this.RecordID), out record);
            return record;
        }

        protected void BindLoanData()
        {
            m_openedDate = m_dataLoan.sOpenedD_rep;
            sLenderNumVerif.Text = m_dataLoan.sLenderNumVerif;

            IPreparerFields f = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfRent, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f.IsValid)
            {
                PreparerName.Text = f.PreparerName;
                PreparerTitle.Text = f.Title;
                PreparerCompanyName.Text = f.CompanyName;
                PreparerStreetAddr.Text = f.StreetAddr;
                PreparerCity.Text = f.City;
                PreparerState.Value = f.State;
                PreparerZip.Text = f.Zip;
                PreparerPrepareDate.Text = f.PrepareDate_rep;
                PreparerPhone.Text = f.Phone;
                PreparerFaxNum.Text = f.FaxNum;
            }
        }

        protected override void BindSingleRecord(IVerificationOfRent field)
        {
            BindLoanData();

            LandlordCreditorName.Text = field.LandlordCreditorName;
            AddressTo.Text = field.AddressTo;
            CityTo.Text = field.CityTo;
            StateTo.Value = field.StateTo;
            ZipTo.Text = field.ZipTo;
            PhoneTo.Text = field.PhoneTo;
            FaxTo.Text = field.FaxTo;
            AccNm.Text = field.AccountName;
            VerifExpD.Text = field.VerifExpD_rep;
            VerifRecvD.Text = field.VerifRecvD_rep;
            VerifReorderedD.Text = field.VerifReorderedD_rep;
            VerifSentD.Text = field.VerifSentD_rep;
            Attention.Text = field.Attention;

            IsSeeAttachment.Checked = field.IsSeeAttachment;

            ClientScript.RegisterHiddenField("VerifSigningEmployeeId", field.VerifSigningEmployeeId.ToString());
            ClientScript.RegisterHiddenField("VerifHasSignature", field.VerifHasSignature.ToString());
      

            // For property address to verify use address enter in borrower information (aBAddr, aBPrev1Addr ....)
            if (field.RecordId == new Guid("11111111-1111-1111-1111-111111111111")) 
            {
                PropertyAddress.Text = m_dataApp.aBAddr;
                PropertyCity.Text = m_dataApp.aBCity;
                PropertyState.Text = m_dataApp.aBState;
                PropertyZipcode.Text = m_dataApp.aBZip;
            } 
            else if (field.RecordId == new Guid("22222222-2222-2222-2222-222222222222")) 
            {
                PropertyAddress.Text = m_dataApp.aBPrev1Addr;
                PropertyCity.Text = m_dataApp.aBPrev1City;
                PropertyState.Text = m_dataApp.aBPrev1State;
                PropertyZipcode.Text = m_dataApp.aBPrev1Zip;

            }
            else if (field.RecordId == new Guid("33333333-3333-3333-3333-333333333333")) 
            {
                PropertyAddress.Text = m_dataApp.aBPrev2Addr;
                PropertyCity.Text = m_dataApp.aBPrev2City;
                PropertyState.Text = m_dataApp.aBPrev2State;
                PropertyZipcode.Text = m_dataApp.aBPrev2Zip;
            }
            else if (field.RecordId == new Guid("44444444-4444-4444-4444-444444444444")) 
            {
                PropertyAddress.Text = m_dataApp.aCAddr;
                PropertyCity.Text = m_dataApp.aCCity;
                PropertyState.Text = m_dataApp.aCState;
                PropertyZipcode.Text = m_dataApp.aCZip;

            }
            else if (field.RecordId == new Guid("55555555-5555-5555-5555-555555555555")) 
            {
                PropertyAddress.Text = m_dataApp.aCPrev1Addr;
                PropertyCity.Text = m_dataApp.aCPrev1City;
                PropertyState.Text = m_dataApp.aCPrev1State;
                PropertyZipcode.Text = m_dataApp.aCPrev1Zip;
            }
            else if (field.RecordId == new Guid("66666666-6666-6666-6666-666666666666")) 
            {
                PropertyAddress.Text = m_dataApp.aCPrev2Addr;
                PropertyCity.Text = m_dataApp.aCPrev2City;
                PropertyState.Text = m_dataApp.aCPrev2State;
                PropertyZipcode.Text = m_dataApp.aCPrev2Zip;
            }
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            this.PageTitle = "VOR record";
            this.PDFPrintClass = typeof(CVORPDF);
            ZipTo.SmartZipcode(CityTo, StateTo);
            PreparerZip.SmartZipcode(PreparerCity, PreparerState);

            bool enableSignature = this.Broker.EnableLqbNonCompliantDocumentSignatureStamp;
            ClientScript.RegisterHiddenField("EnableSignature", enableSignature.ToString());
            if (enableSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(BrokerUser.BrokerId, BrokerUser.EmployeeId, enableSignature);
                ClientScript.RegisterHiddenField("EmployeeHasSignature", signInfo.HasUploadedSignature.ToString());
                ClientScript.RegisterHiddenField("EmployeeName", BrokerUser.DisplayName);
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
