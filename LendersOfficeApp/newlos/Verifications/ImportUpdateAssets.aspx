﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportUpdateAssets.aspx.cs" Inherits="LendersOfficeApp.newlos.Verifications.ImportUpdateAssets" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Asset Verification Review</title>
</head>
<body ng-controller="VoaImportAssets" ng-app="AssetImportUpdate">
    <div class="MainRightHeader" class="ng-cloak">
        Asset Verification Review - {{borrowerName}}
    </div>
    <form id="aspform" runat="server"></form>
    <div class="Accounts" class="ng-cloak" ng-show="accounts">
        <div class="AccountInfo" ng-repeat="account in accounts">
            <div class="AccountBox" ng-class="accountRowClass(account)">
                <div class="AccountDescription">
                    <div ng-class="accountLabelClass(account)">{{account.accountLabel}}</div>
                    <div style="font-family: monospace">{{account.accountNumber}}</div>
                </div>
                <div class="AccountValueContainer">
                    Value on file:<br />
                    <div ng-show="account.valueOnFile" ng-class="getValueOnFileClass(account)">${{account.valueOnFile | number : 2}}</div>
                    <div ng-show="!account.valueOnFile" title="No value on file is available.">---</div>
                </div>
                <div class="AccountValueContainer">
                    Verified value:
                    <div ng-show="account.verifiedValue" ng-class="getUpdatedValueClass(account.valueOnFile, account.verifiedValue)">${{account.verifiedValue | number : 2}}</div>
                    <div ng-show="!account.verifiedValue" title="No verified value is available.">---</div>
                </div>
                <div class="AccountStatusMark">
                    <span ng-show="account.verifiedValue" class="material-icons UpdatedAccountIcon" title="This account will be updated.">check_circle</span>
                    <span ng-show="account.removeAccount" class="material-icons RemoveAccountIcon" title="This account will be removed. Click here to keep it." ng-click="toggleRemoveAccount(account)">remove_circle</span>
                    <span ng-show="!account.verifiedValue && !account.removeAccount" class="material-icons KeepAccountIcon" title="This account will be kept as it is. Click here to mark it for removal." ng-click="toggleRemoveAccount(account)">remove_circle_outline</span>
                </div>
            </div>
        </div>
    </div>
    <div class="StaticBottomSection ng-cloak">
        <hr />
        <div class="Summary">
            <div class="SummaryContainer" style="border-right: 1px solid black;">
                <div>
                    <div style="font-weight: bold;">{{borrowerName}}</div>
                    <div class="indented BorrowerValueSummary">
                        <div>Assets on File: </div>
                        <div>${{previousBorrowerTotal | number : 2}}</div>
                    </div>
                    <div class="indented BorrowerValueSummary">
                        <div>Updated value: </div>
                        <div ng-class="getUpdatedValueClass(previousBorrowerTotal, borrowerTotal)">${{borrowerTotal | number : 2}}</div>
                    </div>
                </div>
            </div>
            <div class="SummaryContainer">
                <div>
                    <div style="font-weight: bold;">Loan File</div>
                    <div class="indented BorrowerValueSummary">
                        <div>Assets on File: </div>
                        <div>${{previousLoanTotal | number : 2}}</div>
                    </div>
                    <div class="indented BorrowerValueSummary">
                        <div>Updated value: </div>
                        <div ng-class="getUpdatedValueClass(previousLoanTotal, loanTotal)">${{loanTotal | number : 2}}</div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="Confirmation">
            <div class="ConfirmationText">
                Update asset records on file with the above changes?
            </div>
            <div class="ConfirmationButtons">
                <input type="button" class="mdc-button" value="Do Not Update" ng-click="exit()" NoHighlight="true" />
                <input type="button" class="mdc-button" value="Update Assets" ng-click="updateAssets()" NoHighlight="true" />
            </div>
        </div>
        </div>
    <script>
        if (!ML.VoaAllowImportingAssets) {
            try {
                parent.LQBPopup.Hide();
            }
            catch(e) {}
            window.close();
        }
        var assetImportModule = angular.module('AssetImportUpdate', ['LqbForms']);
        assetImportModule.controller('VoaImportAssets', function($scope, $window, LqbPopupService, gService, $location) {
            $scope.previousLoanTotal = $window.ML.previousLoanTotal;
            $scope.previousBorrowerTotal = $window.ML.previousBorrowerTotal;
            $scope.loanTotal = $window.ML.loanTotal;
            $scope.borrowerTotal = $window.ML.borrowerTotal;
            $scope.borrowerName = $window.ML.borrowerName;
            $scope.accounts = $window.accounts;

            $scope.exit = function() {
                LqbPopupService.popup.Return({});
            };

            $scope.getUpdatedValueClass = function (valueOnFile, verifiedValue) {
                var classes = [];
                if ((typeof verifiedValue) === 'number') {
                    classes.push('AccountValue-Used');
                }

                if (!valueOnFile || valueOnFile < verifiedValue) {
                    classes.push('AccountValue-Positive');
                }
                else if (valueOnFile > verifiedValue) {
                    classes.push('AccountValue-Negative');
                }
                else {
                    classes.push('AccountValue-Neutral');
                }

                return classes;
            };

            // Bolds the value on file if it's going to be used
            $scope.getValueOnFileClass = function (account) {
                if (((typeof account.verifiedValue) !== 'number' || !account.verifiedValue) && !account.removeAccount) {
                    return 'AccountValue-Used';
                }
                else return '';
            };

            $scope.toggleRemoveAccount = function(account) {
                account.removeAccount = !account.removeAccount;
                $scope.getLoanTotals();
            };

            $scope.accountRowClass = function(account) {
                if (account.removeAccount) {
                    return 'AccountBox-ToBeRemoved';
                }
                else {
                    return 'AccountBox-Normal';
                }
            };

            $scope.accountLabelClass = function(account) {
                if (account.accountLabelIsAccountType) {
                    return 'AccountLabel-AccountType';
                }
                else return '';
            }

            $scope.getLoanTotals = function () {
                gService.vodService.callAsyncSimple("GetLoanTotals", { accountUpdates: angular.toJson($scope.accounts), loanId: $window.ML.sLId, appId: $window.ML.appId, orderId: $window.ML.orderId },
                    function successCallback(result) {
                        if (result.value.totals && typeof(result.value.totals) === 'string') {
                            $scope.$apply(function () {
                                var totals = angular.fromJson(result.value.totals);
                                $scope.loanTotal = totals.loanTotal || $scope.loanTotal;
                                $scope.borrowerTotal = totals.borrowerTotal || $scope.borrowerTotal;
                                });
                        }
                    },
                    function errorCallback(result) {
                        alert (result.UserMessage);
                    });
            };

            $scope.updateAssets = function() {
                gService.vodService.callAsyncSimple('UpdateAssets', { accountUpdates: angular.toJson($scope.accounts), loanId: $window.ML.sLId, appId: $window.ML.appId, orderId: $window.ML.orderId }, 
                    function successCallback(result) {
                        if (result.value["Success"] !== 'True') {
                            alert(result.value["Error"]);
                            return;
                        }
                        LqbPopupService.popup.Return({ Success: true, Added: result.value["Added"], Modified: result.value["Modified"], Removed: result.value["Removed"] });
                    },
                    function errorCallback(result) {
                        alert(result.UserMessage);
                    });
            };
        });
    </script>
</body>
</html>
