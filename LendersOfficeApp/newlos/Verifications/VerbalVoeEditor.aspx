﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VerbalVoeEditor.aspx.cs" Inherits="LendersOfficeApp.newlos.Verifications.VerbalVoeEditor" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .table-layout {
            display: table;
            border-collapse: collapse;
            margin-bottom: 15px;
            width: 650px;
        }

            .table-layout > div {
                display: table-row;
            }

                .table-layout > div > div {
                    padding: 5px;
                    display: table-cell;
                }

        form {
            padding-left: 10px;
        }

        .section-break {
            border-bottom: 1px solid black;
        }

            .section-break > div {
                padding-top: 10px;
            }

        .button-container {
        }

        input[type='text'] {
            width: 100%;
        }

        .city-state-container input[type='text'] {
            width: 140px;
        }

        input[preset='phone'] {
            width: 120px;
        }

        input[preset='date'] {
            width: 75px;
        }

        .dateOfTerminationContainer {
            float: right;
        }

        #SavePrompt {
            display: none;
            text-align: center;
        }

        

          .ui-dialog-titlebar.ui-helper-clearfix {
            display: none;
          }
    </style>
</head>
<body class="EditBackground">

    <script>

        var urlArguments;
        var verbalVoeUrl;
        $(document).ready(function () {
            _initInput();

            urlArguments = "loanid=" + ML.sLId + "&pg=2&appid=" + ML.aAppId;

            verbalVoeUrl = "Verifications.aspx?" + urlArguments;


            $("#CloseBtn").click(function () {
                promptSave(backToList);
            });

            $("#pickerLink").click(pickPreparedByContact);

            $("#EmployerDDL").change(OnEmployerChange);

            $("#SaveBtn").click(f_saveMe);
            $("#OkBtn").click(function () { save(); $("#SavePrompt").dialog("close"); });
            $("#NoBtn").click(function () { $("#SavePrompt").dialog("close"); });

            $("#PreviewButton").click(viewPDF);

            $("#NextButton, #PreviousButton").each(function () {
                var el = $(this);
                if (el.data("id") == undefined) {
                    el.prop("disabled", true);
                }
            });
            $("#PreviousButton, #NextButton").click(switchRecord);
        });

        // Shows the picker and populates the Agent Name and Title fields.
        function pickPreparedByContact() {
            showModal('/los/RolodexList.aspx?&loanid=' + ML.sLId + '&IsFeeContactPicker=false', null, null, null, pickPreparedByContactCallback,{hideCloseButton:true});
        }

        function pickPreparedByContactCallback(args)
        {
            if (args.OK) {
                $("#PreparedBy").val(args["AgentName"]);
                $("#PreparedByTitle").val(args["AgentTitle"]);
            }
        }

        // Populates the Employer Address fields when changed.
        function OnEmployerChange() {
            var address = $(this).find("option:selected").attr("address");
            var addressComponents = JSON.parse(address);

            $("#EmployerStreet").val(addressComponents['EmplrAddr']);
            $("#EmployerCity").val(addressComponents['EmplrCity']);
            $("#EmployerState").val(addressComponents['EmplrState']);
            $("#EmployerZip").val(addressComponents['EmplrZip']);
        }

        // Navigates back to the Verbal VOE tab.
        function backToList() {
            self.location = verbalVoeUrl;
        }

        function viewPdfCallback() {
            // Only show the PDF is the Verbal VOE exists in the DB.  If it does not exist, alert them that they need to save.
            var recordId = $("#RecordId").val();
            if (recordId != <%= AspxTools.JsString(Guid.Empty) %>) {
                var url = ML.VirtualRoot + "/pdf/VerbalVoePdf.aspx?loanid=" + ML.sLId + "&applicationid=" + ML.aAppId + "&recordid=" + recordId;
                LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
            }
            else
            {
                alert("You must save the Verbal VOE before you can view the PDF.");
            }
        }

        // Downloads the pdf file.
        function viewPDF() {
            promptSave(viewPdfCallback);
        }

        // Saves the current record.
        function save() {

            var args = getAllFormValues();
            args["RecordId"] = $("#RecordId").val();
            var result = gService.main.call("Save", args);

            if (result.error) {
                alert(result.UserMessage);
            }
            else {
                clearDirty();
            }

            return result;
        }

        // Saves the current record and updates the UI 
        function f_saveMe() {

            var result = save();
            if (!result.error) {

                $("#RecordId").val(result.value["RecordId"]);
                if (result.value["previousId"]) {
                    var previousButton = $("#PreviousButton");
                    previousButton.data("id", result.value["previousId"]);
                    previousButton.prop("disabled", false);
                }

                alert("VOE Saved.");
            }
        }

        // If the page is dirty, prompt whether the use wants to save or not.  Call the callback function after the prompt is closed, or if the page is not dirty.
        function promptSave(callback) {
            if (isDirty()) {
                $("#SavePrompt").dialog({
                    width: 250,
                    height: 100,
                    modal: true,
                    resizable: false,
                    close: callback,
                    closeOnEscape: false
                });
            }
            else {
                callback();
            }
        }

        // Navigate to the next record.
        var nextRecord;
        function switchRecordCallback() {
            window.location = "VerbalVoeEditor.aspx?loanid=" + ML.sLId + "&appid=" + ML.aAppId + "&recordid=" + nextRecord;
        }

        // Before navigating, check if they should save.
        function switchRecord() {
            nextRecord = $(this).data("id");
            promptSave(switchRecordCallback);
            return false;
        }

    </script>

    <div class="MainRightHeader">
        Verbal VOE
        <ml:EncodedLiteral ID="indexOfMessage" runat="server"></ml:EncodedLiteral>
    </div>
    <form id="form1" runat="server">
        <asp:HiddenField runat="server" ID="RecordId" />
        <div class="table-layout">

            <div>
                <div>
                    <label>Borrower</label>
                </div>
                <div>
                    <asp:DropDownList runat="server" ID="BorrowerDDL"></asp:DropDownList>
                </div>
            </div>

            <div>
                <div>
                    <label>Employer</label>
                </div>
                <div>
                    <asp:DropDownList runat="server" ID="EmployerDDL"></asp:DropDownList>
                </div>
            </div>

            <div>
                <div>
                    <label>Employer Address</label>
                </div>
                <div>
                    <input type="text" runat="server" id="EmployerStreet" />
                </div>
            </div>

            <div class="city-state-container">
                <div>
                    <label></label>
                </div>
                <div>
                    <input type="text" runat="server" id="EmployerCity" />
                    <ml:StateDropDownList runat="server" ID="EmployerState" />
                    <ml:ZipcodeTextBox runat="server" ID="EmployerZip"></ml:ZipcodeTextBox>
                </div>
            </div>

            <div>
                <div>
                    <label>Employer Phone Number</label>
                </div>
                <div>
                    <input type="text" runat="server" id="EmployerPhone" preset="phone" />
                </div>
            </div>

            <div class="section-break">
                <div>
                    <label>3rd Party Source</label>
                </div>
                <div>
                    <input type="text" runat="server" id="ThirdPartySource" />
                </div>
            </div>

            <div>
                <div>
                    <label>Date of Hire</label>
                </div>
                <div>
                    <ml:DateTextBox ID="DateOfHire" runat="server"></ml:DateTextBox>
                    <span class="dateOfTerminationContainer">
                        <label>Date of Termination</label>
                        <ml:DateTextBox ID="DateOfTermination" runat="server"></ml:DateTextBox>
                    </span>

                </div>
                <div></div>
            </div>

            <div>
                <div>
                    <label>Employment Status</label>
                </div>
                <div>
                    <input type="text" runat="server" id="EmploymentStatus" />
                </div>
            </div>

            <div>
                <div>
                    <label>Borrower's Title/Position</label>
                </div>
                <div>
                    <input type="text" runat="server" id="BorrowerTitle" />
                </div>
            </div>

            <div>
                <div>
                    <label>Employer Contact Name</label>
                </div>
                <div>
                    <input type="text" runat="server" id="EmployerContactName" />
                </div>
            </div>

            <div class="section-break">
                <div>
                    <label>Contact's Title</label>
                </div>
                <div>
                    <input type="text" runat="server" id="EmployerContactTitle" />
                </div>
            </div>

            <div>
                <div>
                    <label>Prepared By</label>
                </div>
                <div>
                    <input type="text" runat="server" id="PreparedBy" />
                </div>
                <div><a href="#" id="pickerLink">pick from contacts</a></div>
            </div>

            <div>
                <div>
                    <label>Title</label>
                </div>
                <div>
                    <input type="text" runat="server" id="PreparedByTitle" />
                </div>
            </div>

            <div class="section-break">
                <div>
                    <label>Date of Call</label>
                </div>
                <div>
                    <ml:DateTextBox runat="server" ID="DateOfCall"></ml:DateTextBox>
                </div>
                <div></div>
            </div>
        </div>
        <div class="button-container">
            <input type="button" value="Prev" runat="server" id="PreviousButton" />
            <input type="button" value="Next" runat="server" id="NextButton" />
            <input type="button" id="CloseBtn" value="Back to Verbal VOE List" />
            <input type="button" id="SaveBtn" value="Save" />
            <input type="button" id="PreviewButton" runat="server" value="Preview & Print" />
        </div>
    </form>

    <div id="SavePrompt">
        <p>Do you want to save the changes?</p>
        <div id="SavePromptButtons">
            <input type="button" id="OkBtn" value="Yes" />
            <input type="button" id="NoBtn" value="No" />
        </div>
    </div>
</body>
</html>
