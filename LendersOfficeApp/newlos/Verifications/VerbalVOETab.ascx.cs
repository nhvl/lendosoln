﻿/// -----------------------------------------------------------------------
/// <copyright file="VerbalVoeTab.ascx.cs" company="MeridianLink">
///     Copyright (c) MeridianLink. All rights reserved.
/// </copyright>
/// <author>Eduardo Michel</author>
/// -----------------------------------------------------------------------
#region Generated Code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.AntiXss;

    /// <summary>
    /// This control renders a table containing verbal verifications of employement.
    /// </summary>
    public partial class VerbalVoeTab : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
    {
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VerbalVoeTab));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            List<VerbalVerificationOfEmploymentRecord> records = dataApp.aVerbalVerificationsOfEmployment;
            VerbalVoeRecords.DataSource = records;
            VerbalVoeRecords.DataBind();
        }

        /// <summary>
        /// Saves the data.
        /// </summary>
        public void SaveData()
        {
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The object that triggered the load.</param>
        /// <param name="e">The event arguments sent by the object.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ((BaseLoanPage)Page).RegisterService("main", "/newlos/Verifications/VerbalVoeEditorService.aspx");
        }
    }
}