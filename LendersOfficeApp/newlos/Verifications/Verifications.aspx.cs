using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.Verifications
{
	public partial class Verifications : BaseLoanPage
	{
        protected VODTab VODTab;
        protected VOETab VOETab;
        protected VOMTab VOMTab;
        protected VOLTab VOLTab;
        protected VOLandTab VOLandTab;
        protected VerbalVoeTab VerbalVoeTab;

        override protected void OnInit(EventArgs e)
        {
            EnableJqueryMigrate  = false;

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            Tabs.RegisterControl("Verif of Deposit", VODTab);
            Tabs.RegisterControl("Verif of Employment", VOETab);
            Tabs.RegisterControl("Verbal Verif of Employment", VerbalVoeTab);
            Tabs.RegisterControl("Verif of Mortgage/Rent", VOMTab);
            Tabs.RegisterControl("Verif of Loan", VOLTab);
            Tabs.RegisterControl("Verif of Land Contract", VOLandTab);
            Tabs.AddToQueryString("loanid", LoanID.ToString());
            
            UseNewFramework = true;
            IsAppSpecific = true;

            base.OnInit(e);
        }
        protected override void OnPreRender(EventArgs e) 
        {
            int index = Tabs.TabIndex;
            if (index == 0) 
            {
                this.PageTitle = "Verification of Deposit";
                this.PageID = "VOD";
				this.PDFPrintClass = typeof(LendersOffice.Pdf.CVODPDF);
            }
            else if (index == 1) 
            {
                this.PageTitle = "Verification of Employment";
                this.PageID = "VOE";
				this.PDFPrintClass = typeof(LendersOffice.Pdf.CVOEPDF);
            }
            else if (index == 2) 
            {
                this.PageTitle = "Verbal Verification of Employment";
                this.PageID = "VerbalVoe";
				this.PDFPrintClass = typeof(LendersOffice.Pdf.VerbalVoePdf);
            }

            else if (index == 3) 
            {
                this.PageTitle = "Verification of Mortgage/Rent";
                this.PageID = "VOM";
				this.PDFPrintClass = typeof(LendersOffice.Pdf.CVOMPDF);
            }
            else if (index == 4) 
            {
                this.PageTitle = "Verification of Loan";
                this.PageID = "VOL";
				this.PDFPrintClass = typeof(LendersOffice.Pdf.CVOLPDF);
            }
            else if (index  == 5) 
            {
                this.PageTitle = "Verification of Land Contract";
                this.PageID = "VOLand";
				this.PDFPrintClass = typeof(LendersOffice.Pdf.CVOLandPDF);
            }
            base.OnPreRender(e);

        }

	}
}
