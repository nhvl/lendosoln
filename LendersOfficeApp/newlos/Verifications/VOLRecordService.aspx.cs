using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using System.Linq;
using LendingQB.Core.Data;
using LqbGrammar.DataTypes;

namespace LendersOfficeApp.newlos.Verifications
{
	public partial class VOLRecordService : AbstractSingleEditService
	{
        /// <summary>
        /// Gets the DataObjectIdentifier for liabilities by converting the RecordId.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Liability, Guid> RecordIdentifier => this.RecordID.ToIdentifier<DataObjectKind.Liability>();

        protected override void CustomProcess(string methodName)
        {
            switch (methodName)
            {
                case "ApplySig":
                    ApplySig();
                    break;
                case "ClearSig":
                    ClearSig();
                    break;
                default:
                    break;
            }
        }

        private void ApplySig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOLRecordService));
            dataLoan.InitSave(sFileVersion);

            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                ILiabilityRegular field = dataApp.aLiaCollection.GetRegRecordOf(RecordID);
                field.ApplySignature(principal.EmployeeId, signInfo.SignatureKey.ToString());
                field.Update();
            }
            else
            {
                Liability liability = dataLoan.Liabilities[this.RecordIdentifier];
                var employeeIdentifier = DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(principal.EmployeeId);
                var signatureIdentifier = DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(signInfo.SignatureKey);
                liability.SetVerifSignature(employeeIdentifier, signatureIdentifier);

            }

            dataLoan.Save();

            SetResult("EmployeeId", principal.EmployeeId);
        }

        private void ClearSig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOLRecordService));
            dataLoan.InitSave(sFileVersion);

            if (!this.IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);

                ILiabilityRegular field = dataApp.aLiaCollection.GetRegRecordOf(RecordID);
                field.ClearSignature();
                field.Update();
            }
            else
            {
                Liability liability = dataLoan.Liabilities[this.RecordIdentifier];
                liability.ClearVerifSignature();
            }

            dataLoan.Save();
        }

        private void SaveData(ILiabilityRegular field) 
        {
            field.VerifExpD_rep = GetString("VerifExpD");
            field.VerifRecvD_rep = GetString("VerifRecvD");
            field.VerifSentD_rep = GetString("VerifSentD");
            field.VerifReorderedD_rep = GetString("VerifReorderedD");
            field.Attention = GetString("Attention");
            field.IsSeeAttachment = GetBool("IsSeeAttachment");

            field.Update();
        }

        protected override void SaveData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOLRecordService));
            dataLoan.InitSave(sFileVersion);

            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");

            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfLoan, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("PreparerName") ;
            f.Title = GetString("PreparerTitle") ;
            f.CompanyName = GetString("PreparerCompanyName");
            f.StreetAddr = GetString("PreparerStreetAddr");
            f.City = GetString("PreparerCity");
            f.State = GetString("PreparerState");
            f.Zip = GetString("PreparerZip");
            f.Phone = GetString("PreparerPhone");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");
            f.Update();

            if(!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                ILiaCollection liaList = dataApp.aLiaCollection;
                ILiabilityRegular field = liaList.GetRegRecordOf(RecordID);
                SaveData(field);
            }

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        protected override void SaveDataAndLoadNext() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOLRecordService));
            dataLoan.InitSave(sFileVersion);

            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");

            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfLoan, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("PreparerName") ;
            f.Title = GetString("PreparerTitle") ;
            f.CompanyName = GetString("PreparerCompanyName");
            f.StreetAddr = GetString("PreparerStreetAddr");
            f.City = GetString("PreparerCity");
            f.State = GetString("PreparerState");
            f.Zip = GetString("PreparerZip");
            f.Phone = GetString("PreparerPhone");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");

            f.Update();

            if (!this.IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                ILiaCollection liaList = dataApp.aLiaCollection;
                ILiabilityRegular field = liaList.GetRegRecordOf(RecordID);
                SaveData(field);
                field = liaList.GetRegRecordOf(NextRecordID);
                LoadData(dataLoan, dataApp, field);
            }

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOLRecordService));
            dataLoan.InitLoad();
            
            if(!this.IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                ILiaCollection liaList = dataApp.aLiaCollection;
                ILiabilityRegular field = liaList.GetRegRecordOf(RecordID);
                LoadData(dataLoan, dataApp, field);
            }
        }

        private void LoadLoanData(CPageData dataLoan)
        {
            SetResult("sLenderNumVerif", dataLoan.sLenderNumVerif);

            IPreparerFields f1 = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfLoan, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f1.IsValid) 
            {
                SetResult("PreparerName", f1.PreparerName );
                SetResult("PreparerTitle", f1.Title );
                SetResult("PreparerCompanyName", f1.CompanyName );
                SetResult("PreparerStreetAddr", f1.StreetAddr );
                SetResult("PreparerCity", f1.City );
                SetResult("PreparerState", f1.State );
                SetResult("PreparerZip", f1.Zip );
                SetResult("PreparerPhone", f1.Phone );
                SetResult("PreparerPrepareDate", f1.PrepareDate_rep );
            }
        }

        private void LoadData(CPageData dataLoan, CAppData dataApp, ILiabilityRegular field) 
        {
            SetResult("Attention", field.Attention);
            SetResult("RecordID", field.RecordId);
            SetResult("ComAddr", field.ComAddr);
            SetResult("ComCity", field.ComCity);
            SetResult("ComState", field.ComState);
            SetResult("ComZip", field.ComZip);
            SetResult("ComNm", field.ComNm);
            SetResult("AccNm0", field.AccNm);
            SetResult("AccNum0", field.AccNum.Value);
            SetResult("Bal0", field.Bal_rep);

            SetResult("VerifSentD", field.VerifSentD_rep);
            SetResult("VerifRecvD", field.VerifRecvD_rep);
            SetResult("VerifExpD", field.VerifExpD_rep);
            SetResult("VerifReorderedD", field.VerifReorderedD_rep);
            SetResult("IsSeeAttachment", field.IsSeeAttachment);

            SetResult("VerifSigningEmployeeId", field.VerifSigningEmployeeId);
            SetResult("VerifHasSignature", field.VerifHasSignature);
        }
	}
}
