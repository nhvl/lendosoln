using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Pdf;
using LendingQB.Core.Data;
using LqbGrammar.DataTypes;
using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Verifications
{

    public partial class VOMRecord : BaseSingleEditPage<ILiabilityRegular>
    {

        private CPageData m_dataLoan;
        private CAppData m_dataApp;

        protected string m_openedDate;

        protected override string ListLocation
        {
            get { return VirtualRoot + "/newlos/Verifications/Verifications.aspx?loanid=" + LoanID + "&pg=3"; }
        }
        protected override void LoadData()
        {
            this.m_dataLoan = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(VOMRecord));
            this.m_dataLoan.InitLoad();

            m_openedDate = m_dataLoan.sOpenedD_rep;
            sLenderNumVerif.Text = m_dataLoan.sLenderNumVerif;

            IPreparerFields f = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfMortgage, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f.IsValid)
            {
                PreparerName.Text = f.PreparerName;
                PreparerTitle.Text = f.Title;
                PreparerCompanyName.Text = f.CompanyName;
                PreparerStreetAddr.Text = f.StreetAddr;
                PreparerCity.Text = f.City;
                PreparerState.Value = f.State;
                PreparerZip.Text = f.Zip;
                PreparerPrepareDate.Text = f.PrepareDate_rep;
                PreparerPhone.Text = f.Phone;
                PreparerFaxNum.Text = f.FaxNum;
            }

            if (!IsUlad)
            {
                m_dataApp = m_dataLoan.GetAppData(ApplicationID);
            }

            base.LoadData();
        }
        protected override DataView GetDataView()
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("recordid", typeof(string)));

            if (!IsUlad)
            {
                ILiaCollection liaList = m_dataApp.aLiaCollection;
                int liaCnt = liaList.CountRegular;
                for (int index = 0; index < liaCnt; index++)
                {
                    ILiability field = liaList.GetRegularRecordAt(index);
                    if (field.DebtT.EqualsOneOf(E_DebtT.Mortgage, E_DebtT.Heloc)) 
                    {
                        DataRow row = table.NewRow();
                        row["recordid"] = field.RecordId.ToString();
                        table.Rows.Add(row);
                    }
                }
            }

            return table.DefaultView;
        }

        protected override ILiabilityRegular RetrieveRecord()
        {
            ILiaCollection liaList = m_dataApp.aLiaCollection;
            return liaList.GetRegRecordOf(RecordID);

        }

        protected override void BindSingleRecord(ILiabilityRegular field)
        {
            Attention.Text = field.Attention;
            ComAddr.Text = field.ComAddr;
            ComCity.Text = field.ComCity;
            ComState.Text = field.ComState;
            ComZip.Text = field.ComZip;
            ComPhone.Text = field.ComPhone;
            ComFax.Text = field.ComFax;
            ComNm.Text = field.ComNm;
            AccNm.Text = field.AccNm;
            AccNum.Text = field.AccNum.Value;
            VerifSentD.Text = field.VerifSentD_rep;
            VerifExpD.Text = field.VerifExpD_rep;
            VerifRecvD.Text = field.VerifRecvD_rep;
            VerifReorderedD.Text = field.VerifReorderedD_rep;
            IsSeeAttachment.Checked = field.IsSeeAttachment;

            Guid reRecordID = field.MatchedReRecordId;
            Tools.SetDropDownListValue(MatchedReRecordId, field.MatchedReRecordId.ToString());

            var reColl = m_dataApp.aReCollection;
            var reSubcoll = reColl.GetSubcollection(true, E_ReoGroupT.All);

            foreach (var item in reSubcoll)
            {
                var reField = (IRealEstateOwned)item;
                if (reField.RecordId == reRecordID)
                {
                    PropertyAddress.Text = reField.Addr;
                    PropertyCity.Text = reField.City;
                    PropertyState.Text = reField.State;
                    PropertyZipcode.Text = reField.Zip;

                    break;
                }
            }

            ClientScript.RegisterHiddenField("VerifSigningEmployeeId", field.VerifSigningEmployeeId.ToString());
            ClientScript.RegisterHiddenField("VerifHasSignature", field.VerifHasSignature.ToString());
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            RegisterJsScript("Ulad.Verifications.Vom.js");
            RegisterJsGlobalVariables("recordId", this.RecordID);

            this.PageTitle = "VOM record";
            this.PDFPrintClass = typeof(CVOMPDF);
            PreparerZip.SmartZipcode(PreparerCity, PreparerState);

            bool enableSignature = this.Broker.EnableLqbNonCompliantDocumentSignatureStamp;
            ClientScript.RegisterHiddenField("EnableSignature", enableSignature.ToString());
            if (enableSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(BrokerUser.BrokerId, BrokerUser.EmployeeId, enableSignature);
                ClientScript.RegisterHiddenField("EmployeeHasSignature", signInfo.HasUploadedSignature.ToString());
                ClientScript.RegisterHiddenField("EmployeeName", BrokerUser.DisplayName);
            }

            m_dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOMRecord));
            m_dataLoan.InitLoad();

            MatchedReRecordId.Items.Add(new ListItem("<-- Select a matched REO -->", Guid.Empty.ToString()));

            if (!IsUlad)
            {
                m_dataApp = m_dataLoan.GetAppData(ApplicationID);
                var recoll = m_dataApp.aReCollection;
                var subcoll = recoll.GetSubcollection(true, E_ReoGroupT.All);

                foreach (var item in subcoll)
                {
                    var reField = (IRealEstateOwned)item;
                    string addr = string.Format(@"{0}, {1}, {2} {3}", reField.Addr, reField.City, reField.State, reField.Zip);
                    MatchedReRecordId.Items.Add(new ListItem(addr, reField.RecordId.ToString()));
                }
            }
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
