﻿#region Auto generated code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Integration.VOXFramework;

    /// <summary>
    /// Page for the list of VOX statuses.
    /// </summary>
    public partial class VOXStatusList : BaseLoanPage
    {
        /// <summary>
        /// Page data load event.
        /// </summary>
        protected override void LoadData()
        {
            base.LoadData();
            this.DisplayCopyRight = false;
            var orderId = RequestHelper.GetInt("OrderId");
            var orderNumber = RequestHelper.GetSafeQueryString("OrderNumber");
            if (!string.IsNullOrEmpty(orderNumber))
            {
                this.PageTitleHeader.InnerText = $"Order {orderNumber} Status History";
            }

            var statusList = VOXStatus.GetStatusesForOrder(this.BrokerID, this.LoanID, orderId).OrderBy(status => status.StatusDateTime).ToList();

            this.RegisterJsGlobalVariables("AutoClose", statusList.Count == 0);
            this.RegisterJsGlobalVariables("StatusList", SerializationHelper.JsonNetSafeSerializeBeautifully(statusList, "ddd MMM dd, yyyy, h:mm:ss tt PST"));
            this.RegisterCSS("stylesheetnew.css");
        }
    }
}