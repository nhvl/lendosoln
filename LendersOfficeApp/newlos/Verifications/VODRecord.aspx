<%@ Page language="c#" Codebehind="VODRecord.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Verifications.VODRecord" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>VODRecord</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
    <style type="text/css">
        .verif_headers { text-decoration: underline; }
        #ApplySig, #ClearSig {   display: block;  margin-right: 10px; float:left;  }
        .clear { clear: both; }
    </style>
  </head>
<body ms_positioning="FlowLayout" class="RightBackground">


    <form id="VODRecord" method="post" runat="server">
        <asp:HiddenField runat="server" ID="VerifSigningEmployeeId" />
        <asp:HiddenField runat="server" ID="VerifHasSignature" />
        <asp:HiddenField runat="server" ID="OwnerT" />
        <input id="ErrorMessage" type="hidden" runat="server" value="" />
        <table class="FormTable" id="Table1" cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td class="MainRightHeader" nowrap>Verification
      of&nbsp;Deposit
                    <ml:EncodedLiteral ID="m_nameLabel" runat="server" EnableViewState="False"></ml:EncodedLiteral>
                    <span id="IndexLabel"></span></td>
            </tr>
            <tr>
                <td nowrap>
                    <table id="Table6" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td valign="top" nowrap>
                                <table class="InsetBorder" id="Table9" cellspacing="0" cellpadding="0"
                                    border="0">
                                    <tr>
                                        <td nowrap>
                                            <table id="Table3" cellspacing="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td class="FieldLabel" nowrap colspan="2"><span class="verif_headers">To:</span></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel" nowrap>Attn</td>
                                                    <td nowrap>
                                                        <asp:TextBox ID="Attention" runat="server" Width="215px"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <TD class=FieldLabel noWrap>Name</TD>
                      <TD noWrap><asp:TextBox id=DepositoryName runat="server" Width="215px" ReadOnly="True"></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Address</TD>
                      <TD noWrap><asp:TextBox id=DepositoryStAddr runat="server" Width="214px" ReadOnly="True"></asp:TextBox></TD></TR>
                    <TR>
                      <TD noWrap></TD>
                      <TD noWrap><asp:TextBox id=DepositoryCity runat="server" Width="127px" ReadOnly="True"></asp:TextBox><asp:TextBox id=DepositoryState runat="server" Width="40px" ReadOnly="True"></asp:TextBox><asp:TextBox id=DepositoryZip runat="server" Width="46px" ReadOnly="True"></asp:TextBox></TD></TR></TABLE></TD></TR></TABLE></TD>
          <TD vAlign=top noWrap>
            <TABLE class=InsetBorder id=Table8 cellSpacing=0 cellPadding=0 
            border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table2 cellSpacing=0 cellPadding=0 border=0>
                    <TR>
                      <TD class=FieldLabel noWrap colSpan=2><span class="verif_headers">From:</span></TD></TR>

                    <TR>
                      <TD class=FieldLabel noWrap>Name</TD>
                      <TD noWrap><asp:TextBox id=PreparerName runat="server" Width="207px"></asp:TextBox></TD></TR>
                    <TR id="SignatureRow">
                        <TD class=FieldLabel valign="top">Apply Signature</TD>
                        <TD width="207px">
                           <img src="#" alt="Signature" id="SignatureImg" width="168" />
                            <a href="#" style="display:none" id="ApplySig" onclick="return onapplysig()">apply my signature</a> <a
                                href="#" style="display:none"  id="ClearSig" onclick="return onclearsig()">clear signature</a>
                            <br class="clear" />
                            <span id="MissingSignature" style="display:none">
                            To use your signature, upload your signature image to your profile.
                            </span>
                            <br />
                        </TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Title</TD>
                      <TD noWrap><asp:TextBox id=PreparerTitle runat="server" Width="207px"></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Lender Number</TD>
                      <TD noWrap><asp:TextBox id=sLenderNumVerif runat="server" Width="207px"></asp:TextBox></TD></TR>
                    <tr>
                      <td class=FieldLabel nowrap>Broker Name</td>
                      <td nowrap><asp:TextBox id=PreparerCompanyName runat="server" Width="207px"></asp:TextBox></td></tr>
                    <TR>
                      <TD class=FieldLabel noWrap>Broker Address</TD>
                      <TD noWrap><asp:TextBox id=PreparerStreetAddr runat="server" Width="207px" ></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>City</TD>
                      <TD noWrap><asp:TextBox id=PreparerCity runat="server" Width="114px" ></asp:TextBox><ml:StateDropDownList id=PreparerState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=PreparerZip runat="server" preset="zipcode" width="50"></ml:ZipcodeTextBox></TD></TR>
                    <tr>
                      <td class='FieldLabel' noWrap>Phone</td>
                      <td><ml:PhoneTextBox id=PreparerPhone runat="server" width="120" preset="phone"></ml:PhoneTextBox></td>
                    </tr>
                    <tr>
                      <td class='FieldLabel' noWrap>Fax</td>
                      <td><ml:PhoneTextBox id=PreparerFaxNum runat="server" width="120" preset="phone"></ml:PhoneTextBox></td>
                    </tr>
                    <tr>
                      <td class=FieldLabel nowrap>Prepared Date</td>
                      <td nowrap><ml:DateTextBox id=PreparerPrepareDate runat="server" width="75" preset="date"></ml:DateTextBox></td></tr>
                    <tr>
                      <td class=FieldLabel nowrap colspan=2><asp:CheckBox id=IsSeeAttachment runat="server" Text="Print 'See Attachment' in borrower signature"></asp:CheckBox></td></tr></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <tr>
    <td noWrap>
      <TABLE class=InsetBorder id=Table7 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD noWrap>
            <TABLE id=Table4 cellSpacing=0 cellPadding=0 border=0>
              <TR>
                <TD class=FieldLabel noWrap colSpan=4>Information to be 
                  verified:</TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>Type of Account</TD>
                <TD class=FieldLabel noWrap>Name of Account Holder</TD>
                <TD class=FieldLabel noWrap>Account Number</TD>
                <TD class=FieldLabel noWrap>Balance</TD></TR>
              <TR>
                <TD noWrap><asp:TextBox id=AssetT0 runat="server" Width="103" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNm0 runat="server" Width="193" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNum0 runat="server" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><ml:MoneyTextBox id=Val0 runat="server" ReadOnly="True" width="90" preset="money" CssClass="mask"></ml:MoneyTextBox></TD></TR>
              <TR>
                <TD noWrap><asp:TextBox id=AssetT1 runat="server" Width="103px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNm1 runat="server" Width="193px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNum1 runat="server" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><ml:MoneyTextBox id=Val1 runat="server" ReadOnly="True" width="90" preset="money" CssClass="mask"></ml:MoneyTextBox></TD></TR>
              <TR>
                <TD noWrap><asp:TextBox id=AssetT2 runat="server" Width="103px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNm2 runat="server" Width="193px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNum2 runat="server" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><ml:MoneyTextBox id=Val2 runat="server" ReadOnly="True" width="90" preset="money" CssClass="mask"></ml:MoneyTextBox></TD></TR>
              <TR>
                <TD noWrap><asp:TextBox id=AssetT3 runat="server" Width="103px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNm3 runat="server" Width="193px" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><asp:TextBox id=AccNum3 runat="server" ReadOnly="True"></asp:TextBox></TD>
                <TD noWrap><ml:MoneyTextBox id=Val3 runat="server" ReadOnly="True" width="90" preset="money" CssClass="mask"></ml:MoneyTextBox></TD></TR></TABLE></TD></TR></TABLE></td></tr>
  <TR>
    <TD noWrap>
      <TABLE id=Table5 cellSpacing=0 cellPadding=0 border=0 class=InsetBorder>
        <TR>
          <TD class=FieldLabel noWrap colSpan=8>Verification&nbsp; (Shortcut:&nbsp; Enter 't' for 
            today's date.&nbsp; Enter 'o' for opened date.)</TD></TR>
        <TR>
          <TD class=FieldLabel noWrap> Ordered</TD>
          <TD noWrap><ml:DateTextBox id=VerifSentD runat="server"  onkeyup="onDateKeyUp(this, event);" CssClass="mask" preset="date" width="65px"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap>Re-order</TD>
          <TD class=FieldLabel noWrap><ml:DateTextBox id=VerifReorderedD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap> Received</TD>
          <TD noWrap><ml:DateTextBox id=VerifRecvD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:DateTextBox></TD>
          <TD class=FieldLabel noWrap>Expected</TD>
          <TD noWrap><ml:DateTextBox id=VerifExpD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:DateTextBox></TD></TR></TABLE></TD></TR>
  <tr>
    <td noWrap align=middle>&nbsp;<INPUT id=btnPrevious onclick=goPrevious(); type=button value=Previous name=btnPrevious> 
<INPUT id=btnNext onclick=goNext(); type=button value=Next name=btnNext>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<INPUT onclick=goToList(); type=button value="Back to VOD list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input onclick="printVOD();" type="button" value="Preview &amp; Print" />
</td></tr></table>

     </form>
  </body>
  <script type="text/javascript">

      var uladToLegacyMap = {
          Attention: 'Attention',

          CompanyName: 'DepositoryName',

          StreetAddress: 'DepositoryStAddr',
          City: 'DepositoryCity',
          State: 'DepositoryState',
          Zip: 'DepositoryZip',

          IsSeeAttachment: 'IsSeeAttachment',

          VerifSentDate: 'VerifSentD',
          VerifReorderedDate: 'VerifReorderedD',
          VerifRecvDate: 'VerifRecvD',
          VerifExpiresDate: 'VerifExpD'
      }

      var openedDate = <%= AspxTools.JsString(m_openedDate) %>;
      var list;
      var currentIndex;

      function createListOfEntries(entries, selectedAsset) {
          list = [];
          var companies = [];
          var depositoryIndex = 1;
          for (var i = 0; i < entries.length; i++) {
              var entry = entries[i];
              if (entry.RequiresVod) {
                  var companyName = entry.CompanyName.toLowerCase();
                  if (companies.indexOf(companyName) == -1) {
                      companies.push(companyName);
                      list.push(entry.Id);

                      if (entry.Id == ML.recordId) {
                          currentIndex = i;
                          selectedAsset = entry;
                      }
                  }

                  if (selectedAsset.CompanyName == entry.CompanyName && depositoryIndex <= 4) {
                      // This is a fellow depository
                      var index = entry == selectedAsset ? 0 : depositoryIndex;
                      $('#AccNm' + index).val(entry.AccountName);
                      $('#AccNum' + index).val(entry.AccountNum);
                      $('#AssetT' + index).val(entry.AssetTypeDesc);
                      $('#Val' + index).val(entry.Value);

                      if (!entry == selectedAsset) {
                          depositoryIndex++;
                      }
                  }
              }
          }
      }

            function onDateKeyUp(o, event) {
            if (event.keyCode == 79) {
              o.value = openedDate;
              updateDirtyBit(event);
            }
            
     
          }
          function _init() {
              var errMsg = document.getElementById('ErrorMessage').value;
                if (errMsg != '') {
                    alert(errMsg);
                    if (parent.info != undefined && typeof (parent.info.f_goBack) == "function") {
                        parent.info.f_goBack();
                    }
                }

                var enableSignature = document.getElementById('EnableSignature').value === 'True';
                if (!enableSignature) {
                    document.getElementById('SignatureRow').style.display = "none";
                    return;
                }
                var hasSignature = document.getElementById('EmployeeHasSignature').value === 'True';
                var missingSignature = document.getElementById('MissingSignature');
                missingSignature.style.display = hasSignature ? 'none' : '';
                var applySig = document.getElementById('ApplySig');
                applySig.style.display = hasSignature ? 'block' : 'none';
                postPopulateForm();
            }
            
            function postPopulateForm() {
                var verifHasSignature = document.getElementById('VerifHasSignature').value === 'True';
                var verifSigningEmployeeId = document.getElementById('VerifSigningEmployeeId').value; 
                var clearSig = document.getElementById('ClearSig');
                clearSig.style.display = verifHasSignature ? 'block' : 'none';
                var img = document.getElementById('SignatureImg');
                
                if( verifHasSignature ) {
                    img.src = 'UserSignature.aspx?eid=' + verifSigningEmployeeId;
                }
                else {
                    img.src = 'UserSignature.aspx';
                }
            }
            
            function onclearsig() {
                if( false == confirm('Remove existing signature from verification?') ) {
                    return;
                }   
                var recordId = document.getElementById('RecordID').value;
                var img = document.getElementById('SignatureImg');
                
                var args = { 
                    'recordid' : recordId,
                     'loanid'  : ML.sLId,
                    'applicationid' :  ML.aAppId
                     
                };
                var results = gService.singleedit.call('ClearSig', args);
                if( results.error ) {
                    alert(results.UserMessage);
                }
                else {
                    img.src = "UserSignature.aspx";
                    document.getElementById('VerifHasSignature').value = 'False';
                    document.getElementById('VerifSigningEmployeeId').value = '00000000-0000-0000-0000-000000000000';
                }
                return false;
                
            }
            
            function onapplysig() {
                var username = document.getElementById('EmployeeName').value;
                var verifName = document.getElementById('PreparerName');
                
                if( username != verifName.value ) {
                    if( false == confirm('Your name will be populated to match your signature. Review other fields for accuracy.') ) {
                        return; 
                    }
                    updateDirtyBit();
                    verifName.value = username;
                }
                
                var recordId = document.getElementById('RecordID').value;
                var img = document.getElementById('SignatureImg');
                var clearSig = document.getElementById('ClearSig');
                
                
                
                var args = { 
                    'recordid' : recordId,
                    'loanid' : ML.sLId,
                    'applicationid' :  ML.aAppId
                };
                
                
                var results = gService.singleedit.call('ApplySig', args);
                if( results.error ) {
                    alert(results.UserMessage);
                }
                else {
                    img.src = "UserSignature.aspx?eid="+ results.value.EmployeeId ;
                    clearSig.style.display = 'block';
                    document.getElementById('VerifHasSignature').value = 'True';
                    document.getElementById('VerifSigningEmployeeId').value = results.value.EmployeeId;
                }
                
                return false;
            }

    function printVOD() {
      PolyShouldShowConfirmSave(isDirty(), function(){
        var recordid = document.getElementById('RecordID').value;
        var isborrower = document.getElementById('OwnerT').value == "0" ? "True" : "False";

        var url = 'pdf/VOD.aspx?loanid=' + ML.sLId + '&applicationid=' + ML.aAppId + '&recordid=' + recordid + '&isborrower=' + isborrower + '&crack=' + new Date();
        LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
      }, saveMe);
    }
  </script>
</html>
