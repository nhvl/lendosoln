﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderSSA89.aspx.cs" Inherits="LendersOfficeApp.newlos.Verifications.OrderSSA89" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Order SSN Verification</title>
    <style type="text/css">
        body  { 
            background-color: gainsboro; 
        }
        a[data-sort] {
            color: white;
        }
        #BorrowerTable {
            width: 400px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        var serviceCredentialId = null;
        jQuery(function ($) {
            $('#LenderServices').change(function () {
                if ($(this).val() == '') {
                    ToggleOrderBtn();
                    AuthorizationDocPicker.RemoveDocPicker(null, $('#DocumentTable'));
                    return;
                }

                var data = {
                    LenderServiceId: $(this).val(),
                    LoanId: ML.sLId
                };

                var result = gService.SSA89.call("LoadLenderService", data)
                if (!result.error) {
                    if (result.value["Success"].toLowerCase() === 'true') {
                        var requiresAccountId = result.value["RequiresAccountId"].toLowerCase() == 'true';
                        var usesAccountId = result.value["UsesAccountId"].toLowerCase() === 'true';
                        var credentialId = typeof (result.value["ServiceCredentialId"]) === 'undefined' ? null : result.value["ServiceCredentialId"];
                        var serviceCredentialHasAccountId = typeof (result.value["ServiceCredentialHasAccountId"]) === 'undefined' ? false : result.value["ServiceCredentialHasAccountId"].toLowerCase() === 'true';
                        VOXOrderCommon.BindCredentialOptions(usesAccountId, requiresAccountId, credentialId, serviceCredentialHasAccountId);
                    }
                    else {
                        alert(result.value["Errors"]);
                    }
                }
                else {
                    alert(result.UserMessage);
                }

                ToggleOrderBtn();
            });

            $('.BorrowerPicker').click(function () {
                CheckSignedForms(this, true);
            });

            function RowComparer(sortTargetClass) {
                return function (a, b) {
                    var sortTargetA = $(a).find('.' + sortTargetClass);
                    var sortTargetB = $(b).find('.' + sortTargetClass);

                    var sortValueA = sortTargetA.text();
                    var sortValueB = sortTargetB.text();

                    if (sortValueA < sortValueB) {
                        return -1;
                    }
                    else if (sortValueA > sortValueB) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            }

            function ToggleOrderBtn() {
                var docsOk = AuthorizationDocPicker.AllPickersHaveDocs();
                var inputsOk = $('.RequiredInput:visible').filter(function () { return $(this).val() === '' }).length === 0;
                var lenderService = $('#LenderServices').val();
                var lenderServiceChosen = lenderService != null && lenderService !== '';
                $('#OrderBtn').prop('disabled', !(ML.CanOrderSSA89 && docsOk && inputsOk && lenderServiceChosen));
            }

            function CheckSignedForms(borrowerRadio, shouldCheckOrderBtn) {
                AuthorizationDocPicker.RemoveDocPicker(null, $('#DocumentTable'));

                var borrowerRow = $(borrowerRadio).closest('tr');
                var appId = borrowerRow.find('.AppId').val();
                var borrowerType = borrowerRow.find('.BorrowerType').val();
                var borrowerName = borrowerRow.find('.BorrowerName').text();

                var picker = AuthorizationDocPicker.CreateDocPicker(borrowerName, appId, borrowerType, appId + borrowerType);
                $('#DocumentTable').append(picker);
                if (typeof (shouldCheckOrderBtn) !== 'undefined' && shouldCheckOrderBtn !== null && shouldCheckOrderBtn) {
                    ToggleOrderBtn();
                }
            }

            function GatherRequestData() {
                var checkedBorrower = $('#BorrowerTable input[class="BorrowerPicker"]:checked');
                if (checkedBorrower === null || checkedBorrower.length == 0) {
                    alert("Please choose a borrower.");
                    return;
                }

                var checkedBorrowerRow = checkedBorrower.closest('tr');
                var data = {
                    LoanId: ML.sLId,
                    LenderService: $('#LenderServices').val(),
                    AccountId: $('#AccountId').is(':visible') ? $('#AccountId').val() : '',
                    UserName: $('#Username').val(),
                    Password: $('#Password').val(),
                    DocPickerInfo: AuthorizationDocPicker.RetrieveAllDocPickerInfo($('#DocumentTable')).join(','),
                    AppId: checkedBorrowerRow.find('.AppId').val(),
                    BorrowerType: checkedBorrowerRow.find('.BorrowerType').val(),
                    ServiceCredentialId: serviceCredentialId
                };

                return data;
            }

            if (typeof (InitialLenderService) !== 'undefined') {
                VOXOrderCommon.BindCredentialOptions(InitialLenderService.UsesAccountId, InitialLenderService.RequiresAccountId, InitialLenderService.ServiceCredentialId, InitialLenderService.ServiceCredentialHasAccountId);
            }

            $('.WarningIcon').toggle(!ML.CanOrderSSA89).attr('title', ML.OrderSSA89DenialReason);
            ToggleOrderBtn();
            AuthorizationDocPicker.RegisterDocChangeCallback(ToggleOrderBtn);
            var validStatus = {
                0: false, // Pending
                1: true, // Complete
            };

            VOXOrderCommon.InitializeCommonFunctions({
                RowComparer: RowComparer,
                GatherRequestData: GatherRequestData,
                ServiceType: "SSA89",
                ToggleOrderBtn: ToggleOrderBtn,
                ValidStatusesAfterInitialRequest: validStatus
            });
        });
    </script>
    <form id="form1" runat="server">
        <div>
            <div class="MainRightHeader">Order SSN Verification</div>
        </div>
        <div class="Padding5">
            <div>
                <h3>Select a Borrower</h3>
            </div>
            <div>
                <table id="BorrowerTable" class="Table">
                    <thead>
                        <tr class="LoanFormHeader">
                            <td></td>
                            <td><a data-sort="true" data-sorttarget="BorrowerName" data-asc="false">Borrower</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                            <td><a data-sort="true" data-sorttarget="Ssn" data-asc="false">SSN</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                            <td><a data-sort="true" data-sorttarget="DateOfBirth" data-asc="false">DOB</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                            <td><a data-sort="true" data-sorttarget="OrderStatus" data-asc="false">Order Status</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater runat="server" ID="BorrowerRepeater">
                            <ItemTemplate>
                                <tr class="ReverseGridAutoItem">
                                    <td>
                                        <input type="radio" name="BorrowerPicker" class="BorrowerPicker" />
                                        <input type="hidden" class="AppId" id="AppId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AppId").ToString()) %>" />
                                        <input type="hidden" class="BorrowerType" id="BorrowerType" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BorrowerType").ToString()) %>" />
                                    </td>
                                    <td>
                                        <span class="BorrowerName" id="BorrowerName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BorrowerName")) %></span>
                                    </td>
                                    <td>
                                        <span class="Ssn" id="Ssn"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Ssn")) %></span>
                                    </td>
                                    <td>
                                        <span class="DateOfBirth" id="DateOfBirth"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DateOfBirth")) %></span>
                                    </td>
                                    <td>
                                        <span class="OrderStatus" id="OrderStatus"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OrderStatus")) %></span>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>

            <hr />
            <div class="CssTable FullWidthHeight">
                <div class="CssTableCell Padding5 Width50Pc">
                    <h3>Order Options</h3>
                    <table>
                            <tr class="no-wrap">
                                <td class="FieldLabel">Service Provider</td>
                                <td>
                                    <asp:DropDownList runat="server" ID="LenderServices"></asp:DropDownList>
                                </td>
                            </tr>
                    </table>
                </div>
                <div id="CredentialsRow" class="LeftBorder CredentialItem CssTableCell Padding5 Width50Pc">
                    <h3>Credentials</h3>
                    <table>
                        <tr id="AccountIdRow" class="CredentialItem">
                            <td class="FieldLabel">Account ID</td>
                            <td>
                                <input type="text" class="InputLarge" id="AccountId" runat="server" />
                                <img alt="Required" class="RequiredImg Hidden" id="AccountIdImage" runat="server" src="../../images/require_icon_red.gif" />
                            </td>
                        </tr>
                        <tr id="UsernameRow" class="CredentialItem">
                            <td class="FieldLabel">Username</td>
                            <td>
                                <input type="text" class="InputLarge RequiredInput" id="Username" />
                                <img alt="Required" class="RequiredImg" id="UsernameImg" src="../../images/require_icon_red.gif" />
                            </td>
                        </tr>
                        <tr id="PasswordRow" class="CredentialItem">
                            <td class="FieldLabel">Password</td>
                            <td>
                                <input type="password" class="InputLarge RequiredInput" id="Password" />
                                <img alt="Required" class="RequiredImg" id="PasswordImg" src="../../images/require_icon_red.gif" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="SignedSSA89FormsSection" runat="server">
                <hr />
                <h3>Signed SSA-89 Form</h3>
                <div>
                    <table id="DocumentTable">
                    </table>
                </div>
            </div>
        </div>
        <hr />
        <div class="align-center">
            <input type="button" id="OrderBtn" value="Place Order"/>
            <img src=<%= AspxTools.SafeUrl(this.VirtualRoot + "/images/warning25x25.png") %> class="WarningIcon align-middle" alt="Warning" />
            <input type="button" id="CancelBtn" value="Cancel" />
        </div>
    </form>
</body>
</html>
