<%@ Import Namespace="LendersOfficeApp.newlos.Verifications"%>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="VOETab.ascx.cs" Inherits="LendersOfficeApp.newlos.Verifications.VOETab" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<script type="text/javascript">
  <!--
<% if (IsReadOnly) { %>
      function f_disableAll() {
        var coll = document.getElementsByTagName("input");      
        var length = coll.length;
        if (length > 0) {
          for (i = 0; i < length; i++) {
            var o = coll[i];
            if (o.type=="text") o.readOnly = true;
            else if (o.type =="checkbox") o.disabled = true;
          }
        }   
        
        coll = document.getElementsByTagName("textarea");      
        length = coll.length;
        if (length > 0) {
          for (i = 0; i < length; i++) {
            var o = coll[i];
            o.style.backgroundColor = gReadonlyBackgroundColor;
            o.readOnly = true;
          }
        }              
      }
      function _init() {
        f_disableAll();
      }
<% } %>
  function showVOEEdit(id, isborrower) {
    linkMe('VOERecord.aspx', 'recordid=' + id + '&isborrower=' + isborrower);
  }
  //-->
</script>
<ml:DateTextBox ID="HiddenDTBox" Visible="false" runat="server"></ml:DateTextBox>
<ml:commondatagrid id=m_dg runat="server">
  <alternatingitemstyle cssclass="GridAlternatingItem" />
  <itemstyle cssclass="GridItem" />
  <headerstyle cssclass="GridHeader" />
  <columns>
    <asp:templatecolumn>
      <itemtemplate>
      (<a href='#' onclick=<%# AspxTools.HtmlAttribute("showVOEEdit(" + AspxTools.JsString(((VerificationData)Container.DataItem).RecordID) + "," + AspxTools.JsBool(((VerificationData)Container.DataItem).IsBorrower) + ");") %>>edit</a>)
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn>
      <itemtemplate>
        (<a onclick='LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(<%# AspxTools.SafeUrl("pdf/VOE.aspx?loanid=" + this.LoanID + "&applicationid=" + ((VerificationData)Container.DataItem).ApplicationID + "&recordid=" + ((VerificationData)Container.DataItem).RecordID + "&isborrower=" + ((VerificationData)Container.DataItem).IsBorrower) %>);' target=_parent>preview</a>)
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="B/C">
      <itemtemplate>
        <%# AspxTools.HtmlString(((VerificationData) Container.DataItem).IsBorrower ? "B" : "C") %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Employer Name">
      <itemtemplate>
          <%# AspxTools.HtmlString(((VerificationData) Container.DataItem).Description) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Ordered Date">
      <itemtemplate>
<%# AspxTools.HtmlControls(CreateDateTextBox(((VerificationData)Container.DataItem).RecordID, "VerifSentD", ((VerificationData)Container.DataItem).VerifSentD)) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Reordered Date">
      <itemtemplate>
<%# AspxTools.HtmlControls(CreateDateTextBox(((VerificationData)Container.DataItem).RecordID, "VerifReorderedD", ((VerificationData)Container.DataItem).VerifReorderedD)) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Received Date">
      <itemtemplate>
<%#AspxTools.HtmlControls(CreateDateTextBox(((VerificationData)Container.DataItem).RecordID, "VerifRecvD", ((VerificationData)Container.DataItem).VerifRecvD)) %>
      </itemtemplate>
    </asp:templatecolumn>
    <asp:templatecolumn headertext="Expected Date">
      <itemtemplate>
<%#AspxTools.HtmlControls(CreateDateTextBox(((VerificationData)Container.DataItem).RecordID, "VerifExpD", ((VerificationData)Container.DataItem).VerifExpD)) %>
      </itemtemplate>
    </asp:templatecolumn>
  </columns>
</ml:commondatagrid>
<table>
  <tr>
    <td class=FieldLabel><ml:EncodedLiteral id="m_noRecordLabel" runat="server" text="No employment to verify" visible="False" enableviewstate="False"></ml:EncodedLiteral></td></tr></table>
