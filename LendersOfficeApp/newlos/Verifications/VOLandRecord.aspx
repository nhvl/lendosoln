<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="VOLandRecord.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.Verifications.VOLandRecord" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>VOLandRecord</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
    <style type="text/css">
        .verif_headers { text-decoration: underline; }
        #ApplySig, #ClearSig {   display: block;  margin-right: 10px; float:left;  }
        .clear { clear: both; }
    </style>
<script type="text/javascript">
<!--
function _init() {
  <% if (IsReadOnly) { %>
    document.getElementById("btnAdd").disabled = true;
  <% } %>
}
    var openedDate = <%= AspxTools.JsString(m_openedDate) %>;
    function onDateKeyUp(o, event) {
      if (event.keyCode == 79) {
        o.value = openedDate;
        updateDirtyBit(event);
      }
    }
    function resetForm() {
      <%= AspxTools.JsGetElementById(LandlordCreditorName) %>.value = '';
      <%= AspxTools.JsGetElementById(AddressTo) %>.value = '';
      <%= AspxTools.JsGetElementById(CityTo) %>.value='';
      <%= AspxTools.JsGetElementById(StateTo) %>.value = '';
      <%= AspxTools.JsGetElementById(ZipTo) %>.value = '';
      <%= AspxTools.JsGetElementById(AddressFor) %>.value = '';
      <%= AspxTools.JsGetElementById(CityFor) %>.value='';
      <%= AspxTools.JsGetElementById(StateFor) %>.value = '';
      <%= AspxTools.JsGetElementById(ZipFor) %>.value = '';
      <%= AspxTools.JsGetElementById(AccNm) %>.value = '';
      <%= AspxTools.JsGetElementById(VerifSentD) %>.value = '';
      <%= AspxTools.JsGetElementById(VerifReorderedD) %>.value = '';
      <%= AspxTools.JsGetElementById(VerifRecvD) %>.value = '';
      <%= AspxTools.JsGetElementById(VerifExpD) %>.value = '';
      <%= AspxTools.JsGetElementById(Attention) %>.value = '';
    }
          function _init() {    
                var hasSignature = document.getElementById('EmployeeHasSignature').value === 'True';
                var missingSignature = document.getElementById('MissingSignature');
                missingSignature.style.display = hasSignature ? 'none' : '';
                var applySig = document.getElementById('ApplySig');
                applySig.style.display = hasSignature ? 'block' : 'none';
                postPopulateForm();
            }
            
            function postPopulateForm() {
                var verifHasSignature = document.getElementById('VerifHasSignature').value === 'True';
                var verifSigningEmployeeId = document.getElementById('VerifSigningEmployeeId').value; 
                var clearSig = document.getElementById('ClearSig');
                clearSig.style.display = verifHasSignature ? 'block' : 'none';
                var img = document.getElementById('SignatureImg');
                
                if( verifHasSignature ) {
                    img.src = 'UserSignature.aspx?eid=' + verifSigningEmployeeId;
                }
                else {
                    img.src = 'UserSignature.aspx';
                }
            }
            
            function onclearsig() {
                if( false == confirm('Remove existing signature from verification?') ) {
                    return;
                }   
                var recordId = document.getElementById('RecordID').value;
                var img = document.getElementById('SignatureImg');
                
                var args = { 
                    'recordid' : recordId,
                     'loanid'  : ML.sLId,
                    'applicationid' :  ML.aAppId
                     
                };
                var results = gService.singleedit.call('ClearSig', args);
                if( results.error ) {
                    alert(results.UserMessage);
                }
                else {
                    document.getElementById('VerifHasSignature').value = 'False';
                    document.getElementById('VerifSigningEmployeeId').value = '00000000-0000-0000-0000-000000000000';
                    img.src = "UserSignature.aspx";
                }
                return false;
                
            }
            
            function onapplysig() {
                var username = document.getElementById('EmployeeName').value;
                var verifName = document.getElementById('PreparerName');
                
                if( username != verifName.value ) {
                    if( false == confirm('Your name will be populated to match your signature. Review other fields for accuracy.') ) {
                        return; 
                    }
                    updateDirtyBit();
                    verifName.value = username;
                }
                
                var recordId = document.getElementById('RecordID').value;
                var img = document.getElementById('SignatureImg');
                var clearSig = document.getElementById('ClearSig');
                
                
                
                var args = { 
                    'recordid' : recordId,
                    'loanid' : ML.sLId,
                    'applicationid' :  ML.aAppId
                };
                
                
                var results = gService.singleedit.call('ApplySig', args);
                if( results.error ) {
                    alert(results.UserMessage);
                }
                else {
                    img.src = "UserSignature.aspx?eid="+ results.value.EmployeeId ;
                    clearSig.style.display = 'block';
                    document.getElementById('VerifHasSignature').value = 'True';
                    document.getElementById('VerifSigningEmployeeId').value = results.value.EmployeeId;
                }
                
                return false;
            }
            
            function printVOLand() {
                PolyShouldShowConfirmSave(isDirty(), function(){
                    var recordid = document.getElementById('RecordID').value;
                    var url = VRoot + '/pdf/VOLand.aspx?loanid=' + ML.sLId + '&applicationid=' + ML.aAppId + '&recordid=' + recordid + '&crack=' + new Date();
                    LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
                }, saveMe);
            }
//-->
</script>
</head>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<form id=VOLandRecord method=post runat="server">
<TABLE class=FormTable id=Table1 cellSpacing=0 cellPadding=0 border=0>
  <TR>
    <TD class=MainRightHeader noWrap>Verification 
      of Land Contract <span id="IndexLabel"></span></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE id=Table5 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD vAlign=top noWrap>
            <TABLE class=InsetBorder id=Table6 cellSpacing=0 cellPadding=0  border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table3 cellSpacing=0 cellPadding=0 border=0 >
                    <TR>
                      <TD class=FieldLabel noWrap colSpan=2 ><span class="verif_headers">To:</span></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Attn</TD>
                      <TD noWrap><asp:TextBox id=Attention runat="server"></asp:TextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Name</TD>
                      <TD noWrap><asp:textbox id=LandlordCreditorName runat="server" Width="203px"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap >Address</TD>
                      <TD noWrap><asp:textbox id=AddressTo runat="server" Width="203px"></asp:textbox></TD></TR>
                    <TR>
                      <TD noWrap></TD>
                      <TD noWrap><asp:textbox id=CityTo runat="server" Width="110px"></asp:textbox><ml:statedropdownlist id=StateTo runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=ZipTo runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></TD></TR></TABLE></TD></TR></TABLE></TD>
          <TD vAlign=top noWrap>
            <TABLE class=InsetBorder id=Table7 cellSpacing=0 cellPadding=0 
            border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table2 cellSpacing=0 cellPadding=0 border=0 
                  >
                    <TR>
                      <TD class=FieldLabel noWrap><span class="verif_headers">From:</span></TD>
                      <TD noWrap></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Name</TD>
                      <TD noWrap><asp:textbox id=PreparerName runat="server" Width="203"></asp:textbox></TD></TR>
                       <TR runat="server" id="SigRow">
                        <TD class=FieldLabel valign="top">Apply Signature</TD>
                        <TD width="207px">
                           <img  width="168" src="UserSignature.aspx" alt="Signature" id="SignatureImg" />
                            <a href="#" style="display:none" id="ApplySig" onclick="return onapplysig()">apply my signature</a> <a
                                href="#" style="display:none"  id="ClearSig" onclick="return onclearsig()">clear signature</a>
                            <br class="clear" />
                            <span id="MissingSignature" style="display:none">
                            To use your signature, upload your signature image to your profile.
                            </span>
                            <br />
                        </TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap >Title:</TD>
                      <TD noWrap><asp:textbox id=PreparerTitle runat="server" Width="203px"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel nowrap>Broker Name</td>
                      <td nowrap><asp:TextBox id=PreparerCompanyName runat="server" Width="203px"></asp:TextBox></td></tr>
                    <TR>
                      <TD class=FieldLabel noWrap>Broker 
                        Address</TD>
                      <TD noWrap><asp:textbox id=PreparerStreetAddr runat="server" Width="203px" ></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap></TD>
                      <TD noWrap><asp:textbox id=PreparerCity runat="server" Width="115px" ></asp:textbox><ml:StateDropDownList id=PreparerState runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id=PreparerZip runat="server" preset="zipcode" width="50"></ml:ZipcodeTextBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Lender 
                        Number</TD>
                      <TD noWrap><asp:textbox id=sLenderNumVerif runat="server"></asp:textbox></TD></TR>
                    <tr>
                      <td class=FieldLabel nowrap>Prepared Date</td>
                      <td nowrap><ml:DateTextBox id=PreparerPrepareDate runat="server" preset="date" width="75"></ml:DateTextBox></td></tr>
                    <tr>
                      <td class=FieldLabel nowrap colspan=2><asp:CheckBox id=IsSeeAttachment runat="server" Text="Print 'See Attachment' in borrower signature"></asp:CheckBox></td></tr></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE class=InsetBorder id=Table8 cellSpacing=0 cellPadding=0 border=0 
      >
        <TR>
          <TD noWrap>
            <TABLE id=Table4 cellSpacing=0 cellPadding=0 border=0 
            >
              <TR>
                <TD class=FieldLabel noWrap>Information 
                  to be verified</TD>
                <TD noWrap></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>Property 
                  Address</TD>
                <TD noWrap><asp:textbox id=AddressFor runat="server" Width="249px"></asp:textbox></TD></TR>
              <TR>
                <TD noWrap></TD>
                <TD noWrap><asp:textbox id=CityFor runat="server" Width="142px"></asp:textbox><ml:statedropdownlist id=StateFor runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=ZipFor runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>Account 
                Name</TD>
                <TD noWrap><asp:textbox id=AccNm runat="server"></asp:textbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
  <TR>
    <TD noWrap>
      <TABLE class=InsetBorder id=Table4 cellSpacing=0 cellPadding=0 border=0 
      >
        <TR>
          <TD class=FieldLabel noWrap colSpan=8 
            >Verification (Shortcut: Enter 't' for today's 
            date. Enter 'o' for opened date.)</TD></TR>
        <TR>
          <TD class=FieldLabel noWrap>Ordered</TD>
          <TD noWrap><ml:datetextbox id=VerifSentD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:datetextbox></TD>
          <TD class=FieldLabel noWrap>Re-order</TD>
          <TD class=FieldLabel noWrap><ml:datetextbox id=VerifReorderedD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:datetextbox></TD>
          <TD class=FieldLabel noWrap>Received</TD>
          <TD noWrap><ml:datetextbox id=VerifRecvD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:datetextbox></TD>
          <TD class=FieldLabel noWrap>Expected</TD>
          <TD noWrap><ml:datetextbox id=VerifExpD onkeyup="onDateKeyUp(this, event);" runat="server" width="65px" preset="date" CssClass="mask"></ml:datetextbox></TD></TR></TABLE></TD></TR>
  <TR>
    <TD noWrap align=middle><INPUT id=btnPrevious onclick=goPrevious(); type=button value=Previous name=btnPrevious> 
<INPUT id=btnNext onclick=goNext(); type=button value=Next name=btnNext>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<INPUT onclick=goToList(); type=button value="Back to Verif Land Contract list">&nbsp;
<INPUT onclick="createNew();postPopulateForm();" type="button" value="Add new record" accessKey=A id="btnAdd"> 
<input onclick="printVOLand();" type="button" value="Preview &amp; Print" />

    </TD></TR></TABLE></form>
  </body>
</html>
