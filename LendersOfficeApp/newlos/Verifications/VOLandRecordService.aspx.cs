using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Verifications
{
	/// <summary>
	/// Summary description for VOLandRecordSerivce.
	/// </summary>
	public partial class VOLandRecordService : AbstractSingleEditService
	{
        protected override void CustomProcess(string methodName)
        {
            switch (methodName)
            {
                case "ApplySig":
                    ApplySig();
                    break;
                case "ClearSig":
                    ClearSig();
                    break;
                default:
                    break;
            }
        }

        private void ApplySig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
            IVerificationOfRent field = dataApp.GetVorFields(RecordID);
            field.ApplySignature(principal.EmployeeId, signInfo.SignatureKey.ToString());
            field.Update();
            dataLoan.Save();

            SetResult("EmployeeId", principal.EmployeeId);
        }

        private void ClearSig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IVerificationOfRent field = dataApp.GetVorFields(RecordID);
            field.ClearSignature();
            field.Update();
            dataLoan.Save();
        }

        private void SaveData(IVerificationOfRent field) 
        {
            field.VerifExpD_rep = GetString("VerifExpD");
            field.VerifRecvD_rep = GetString("VerifRecvD");
            field.VerifSentD_rep = GetString("VerifSentD");
            field.VerifReorderedD_rep = GetString("VerifReorderedD");

            field.LandlordCreditorName = GetString("LandlordCreditorName") ;
            field.Attention = GetString("Attention");
            field.AddressTo = GetString("AddressTo") ;
            field.CityTo = GetString("CityTo") ;
            field.StateTo = GetString("StateTo") ;
            field.ZipTo = GetString("ZipTo") ;
            field.AccountName = GetString("AccNm") ;
            field.AddressFor = GetString("AddressFor") ;
            field.CityFor = GetString("CityFor") ;
            field.StateFor = GetString("StateFor") ;
            field.ZipFor = GetString("ZipFor") ;
            field.IsSeeAttachment = GetBool("IsSeeAttachment");

            field.Update();
        }
        protected override void SaveData() 
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IVerificationOfRent field = dataApp.GetVorFields(RecordID);
            field.VerifT = E_VorT.LandContract;

            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");

            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfLandContract, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("PreparerName") ;
            f.Title = GetString("PreparerTitle") ;
            f.CompanyName = GetString("PreparerCompanyName");
            f.StreetAddr = GetString("PreparerStreetAddr");
            f.City = GetString("PreparerCity");
            f.State = GetString("PreparerState");
            f.Zip = GetString("PreparerZip");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");
            f.Update();

            SaveData(field);

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            SetResult("PrevRecordID", field.RecordId.ToString());
            SetResult("VerifSigningEmployeeId", field.VerifSigningEmployeeId);
            SetResult("VerifHasSignature", field.VerifHasSignature);
        }

        protected override void SaveDataAndLoadNext() 
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IVerificationOfRent field = dataApp.GetVorFields(RecordID);

            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");

            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfLandContract, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("PreparerName") ;
            f.Title = GetString("PreparerTitle") ;
            f.CompanyName = GetString("PreparerCompanyName");
            f.StreetAddr = GetString("PreparerStreetAddr");
            f.City = GetString("PreparerCity");
            f.State = GetString("PreparerState");
            f.Zip = GetString("PreparerZip");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");

            f.Update();

            Guid prevRecordID = field.RecordId;
            SaveData(field);
            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            field = dataApp.GetVorFields(NextRecordID);
            LoadData(dataLoan, dataApp, field, prevRecordID);
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            IVerificationOfRent field = dataApp.GetVorFields(RecordID);

            LoadData(dataLoan, dataApp, field, field.RecordId);
        }
        private void LoadData(CPageData dataLoan, CAppData dataApp, IVerificationOfRent field, Guid prevRecordID) 
        {
            if (prevRecordID != Guid.Empty) 
            {
                SetResult("PrevRecordID", prevRecordID.ToString());
            }

            SetResult("sLenderNumVerif", dataLoan.sLenderNumVerif);

            IPreparerFields f1 = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfLandContract, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f1.IsValid) 
            {
                SetResult("PreparerName", f1.PreparerName );
                SetResult("PreparerTitle", f1.Title );
                SetResult("PreparerCompanyName", f1.CompanyName );
                SetResult("PreparerStreetAddr", f1.StreetAddr );
                SetResult("PreparerCity", f1.City );
                SetResult("PreparerState", f1.State );
                SetResult("PreparerZip", f1.Zip );
                SetResult("PreparerPrepareDate", f1.PrepareDate_rep );

            }
            SetResult("RecordID", field.RecordId);

            SetResult("VerifSentD", field.VerifSentD);
            SetResult("VerifRecvD", field.VerifRecvD);
            SetResult("VerifExpD", field.VerifExpD);
            SetResult("VerifReorderedD", field.VerifReorderedD);

            SetResult("LandlordCreditorName", field.LandlordCreditorName);
            SetResult("Attention", field.Attention);
            SetResult("AddressTo", field.AddressTo);
            SetResult("CityTo", field.CityTo);
            SetResult("StateTo", field.StateTo);
            SetResult("ZipTo", field.ZipTo);
            SetResult("AccNm", field.AccountName);
            SetResult("AddressFor", field.AddressFor);
            SetResult("CityFor", field.CityFor);
            SetResult("StateFor", field.StateFor);
            SetResult("ZipFor", field.ZipFor);
            SetResult("IsSeeAttachment", field.IsSeeAttachment);
            SetResult("VerifSigningEmployeeId", field.VerifSigningEmployeeId);
            SetResult("VerifHasSignature", field.VerifHasSignature);



        }

	}
}
