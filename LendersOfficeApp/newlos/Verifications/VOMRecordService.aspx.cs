using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendingQB.Core.Commands;
using LendingQB.Core.Data;
using LqbGrammar.DataTypes;
using LqbGrammar.DataTypes.PathDispatch;
using System;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Verifications
{

    public partial class VOMRecordService : AbstractSingleEditService
    {
        /// <summary>
        /// Gets the DataObjectIdentifier for liabilities by converting the RecordId.
        /// </summary>
        public DataObjectIdentifier<DataObjectKind.Liability, Guid> RecordIdentifier => this.RecordID.ToIdentifier<DataObjectKind.Liability>();

        private void ApplySig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOMRecordService));
            dataLoan.InitSave(sFileVersion);

            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);

            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);

                ILiabilityRegular field = dataApp.aLiaCollection.GetRegRecordOf(RecordID);
                field.ApplySignature(principal.EmployeeId, signInfo.SignatureKey.ToString());
                field.Update();
            }
            else
            {
                var mortgageRecord = dataLoan.Liabilities[this.RecordIdentifier];
                mortgageRecord.SetVerifSignature
                    (
                    DataObjectIdentifier<DataObjectKind.Employee, Guid>.Create(principal.EmployeeId),
                    DataObjectIdentifier<DataObjectKind.Signature, Guid>.Create(signInfo.SignatureKey)
                    );
            }

            dataLoan.Save();
            SetResult("EmployeeId", principal.EmployeeId);
        }

        private void ClearSig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOMRecordService));
            dataLoan.InitSave(sFileVersion);

            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                ILiabilityRegular field = dataApp.aLiaCollection.GetRegRecordOf(RecordID);
                field.ClearSignature();
                field.Update();
            }
            else
            {
                var mortgageRecord = dataLoan.Liabilities[this.RecordIdentifier];
                mortgageRecord.ClearVerifSignature();
            }

            dataLoan.Save();
        }

        private void SaveData(ILiabilityRegular field)
        {
            field.VerifExpD_rep = GetString("VerifExpD");
            field.VerifRecvD_rep = GetString("VerifRecvD");
            field.VerifSentD_rep = GetString("VerifSentD");
            field.VerifReorderedD_rep = GetString("VerifReorderedD");
            field.Attention = GetString("Attention");
            field.IsSeeAttachment = GetBool("IsSeeAttachment");
            field.MatchedReRecordId = GetGuid("MatchedReRecordId");

            field.Update();
        }

        protected override void CustomProcess(string methodName)
        {
            switch (methodName)
            {
                case "GetREOAddress":
                    GetREOAddress();
                    break;
                case "ApplySig":
                    ApplySig();
                    break;
                case "ClearSig":
                    ClearSig();
                    break;
            }
        }

        private void GetREOAddress()
        {
            Guid matchedReRecordId = GetGuid("MatchedReRecordId");
            if (Guid.Empty == matchedReRecordId)
            {
                SetResult("PropertyAddress", "");
                SetResult("PropertyCity", "");
                SetResult("PropertyState", "");
                SetResult("PropertyZipcode", "");
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOMRecordService));
            dataLoan.InitLoad();

            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                var subcoll = dataApp.aReCollection.GetSubcollection(true, E_ReoGroupT.All);
                foreach (var item in subcoll)
                {
                    var reField = (IRealEstateOwned)item;
                    if (reField.RecordId == matchedReRecordId)
                    {
                        SetResult("PropertyAddress", reField.Addr);
                        SetResult("PropertyCity", reField.City);
                        SetResult("PropertyState", reField.State);
                        SetResult("PropertyZipcode", reField.Zip);

                        break;
                    }
                }
            }
        }

        protected override void SaveData()
        {
            CPageData dataLoan = new CLiaRecordData(LoanID);
            dataLoan.InitSave(sFileVersion);

            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");

            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfMortgage, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("PreparerName");
            f.Title = GetString("PreparerTitle");
            f.CompanyName = GetString("PreparerCompanyName");
            f.StreetAddr = GetString("PreparerStreetAddr");
            f.City = GetString("PreparerCity");
            f.State = GetString("PreparerState");
            f.Zip = GetString("PreparerZip");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");
            f.Phone = GetString("PreparerPhone");
            f.FaxNum = GetString("PreparerFaxNum");
            f.Update();

            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);

                ILiaCollection liaList = dataApp.aLiaCollection;
                ILiabilityRegular field = liaList.GetRegRecordOf(RecordID);
                SaveData(field);
                SetResult("PrevRecordID", field.RecordId.ToString());
            }

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        protected override void SaveDataAndLoadNext()
        {

            CPageData dataLoan = new CLiaRecordData(LoanID);
            dataLoan.InitSave(sFileVersion);


            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");

            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfMortgage, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("PreparerName");
            f.Title = GetString("PreparerTitle");
            f.CompanyName = GetString("PreparerCompanyName");
            f.StreetAddr = GetString("PreparerStreetAddr");
            f.City = GetString("PreparerCity");
            f.State = GetString("PreparerState");
            f.Zip = GetString("PreparerZip");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");
            f.Phone = GetString("PreparerPhone");
            f.FaxNum = GetString("PreparerFaxNum");
            f.Update();

            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                ILiaCollection liaList = dataApp.aLiaCollection;
                ILiabilityRegular field = liaList.GetRegRecordOf(RecordID);
                SaveData(field);
                dataLoan.Save();
                field = liaList.GetRegRecordOf(NextRecordID);
                LoadData(dataLoan, dataApp, field);
            }

            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        protected override void LoadData()
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(VOMRecordService));
            dataLoan.InitLoad();

            if (!IsUlad)
            {
                CAppData dataApp = dataLoan.GetAppData(ApplicationID);
                ILiabilityRegular field = dataApp.aLiaCollection.GetRegRecordOf(RecordID);
                LoadData(dataLoan, dataApp, field);
            }
        }

        private void LoadLoanData(CPageData dataLoan)
        {
            SetResult("sLenderNumVerif", dataLoan.sLenderNumVerif);

            IPreparerFields f1 = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfMortgage, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f1.IsValid)
            {
                SetResult("PreparerName", f1.PreparerName);
                SetResult("PreparerTitle", f1.Title);
                SetResult("PreparerCompanyName", f1.CompanyName);
                SetResult("PreparerStreetAddr", f1.StreetAddr);
                SetResult("PreparerCity", f1.City);
                SetResult("PreparerState", f1.State);
                SetResult("PreparerZip", f1.Zip);
                SetResult("PreparerPrepareDate", f1.PrepareDate_rep);
                SetResult("PreparerPhone", f1.Phone);
                SetResult("PreparerFaxNum", f1.FaxNum);
            }
        }

        private void LoadData(CPageData dataLoan, CAppData dataApp, ILiabilityRegular field)
        {
            LoadLoanData(dataLoan);
            SetResult("RecordID", field.RecordId);
            SetResult("Attention", field.Attention);
            SetResult("ComAddr", field.ComAddr);
            SetResult("ComCity", field.ComCity);
            SetResult("ComState", field.ComState);
            SetResult("ComZip", field.ComZip);
            SetResult("ComNm", field.ComNm);
            SetResult("ComPhone", field.ComPhone);
            SetResult("ComFax", field.ComFax);
            SetResult("AccNm0", field.AccNm);
            SetResult("AccNum0", field.AccNum.Value);
            SetResult("Bal0", field.Bal_rep);

            SetResult("VerifSentD", field.VerifSentD_rep);
            SetResult("VerifRecvD", field.VerifRecvD_rep);
            SetResult("VerifExpD", field.VerifExpD_rep);
            SetResult("VerifReorderedD", field.VerifReorderedD_rep);
            SetResult("IsSeeAttachment", field.IsSeeAttachment);
            SetResult("MatchedReRecordId", field.MatchedReRecordId);

            Guid loanID = dataLoan.sLId;
            Guid applicationID = dataApp.aAppId;
            Guid reRecordID = field.MatchedReRecordId;

            var subcoll = dataApp.aReCollection.GetSubcollection(true, E_ReoGroupT.All);
            foreach (var item in subcoll)
            {
                var reField = (IRealEstateOwned)item;
                if (reField.RecordId == reRecordID)
                {
                    SetResult("PropertyAddress", reField.Addr);
                    SetResult("PropertyCity", reField.City);
                    SetResult("PropertyState", reField.State);
                    SetResult("PropertyZipcode", reField.Zip);

                    break;
                }
            }

            SetResult("VerifSigningEmployeeId", field.VerifSigningEmployeeId);
            SetResult("VerifHasSignature", field.VerifHasSignature);
        }
    }
}
