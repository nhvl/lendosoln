﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using System.Data;

namespace LendersOfficeApp.newlos.Verifications
{
    public class VODTabServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VODTabServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var subCollection = dataApp.aAssetCollection.GetSubcollection(false, E_AssetGroupT.DontNeedVOD);
            Dictionary<string, string> hash = new Dictionary<string, string>();
            foreach (var item in subCollection)
            {
                var record = (IAssetRegular)item;
                string key = record.ComNm.ToLower();
                if (!hash.ContainsKey(key))
                {
                    hash.Add(key, key);
                    string id = record.RecordId.ToString().Replace("-", "");
                    record.VerifSentD_rep = GetString("VerifSentD_" + id);
                    record.VerifReorderedD_rep = GetString("VerifReorderedD_" + id);
                    record.VerifRecvD_rep = GetString("VerifRecvD_" + id);
                    record.VerifExpD_rep = GetString("VerifExpD_" + id);

                }
            }
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
    public class VOETabServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VOETabServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            for (int i = 0; i < 2; i++)
            {
                // i = 0 --> Borrower, i == 1 --> Coborrower
                IEmpCollection recordList = i == 0 ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
                var subCollection = recordList.GetSubcollection(true, E_EmpGroupT.All);
                foreach (IEmploymentRecord record in subCollection)
                {
                    if (record.EmplrNm.TrimWhitespaceAndBOM() != "")
                    {
                        string id = record.RecordId.ToString().Replace("-", "");
                        record.VerifSentD_rep = GetString("VerifSentD_" + id);
                        record.VerifReorderedD_rep = GetString("VerifReorderedD_" + id);
                        record.VerifRecvD_rep = GetString("VerifRecvD_" + id);
                        record.VerifExpD_rep = GetString("VerifExpD_" + id);
                    }
                }
            }
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class VOMTabServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VOMTabServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            string addr = string.Format("{0}, {1}, {2} {3}", dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip);

            if (dataApp.aBAddrT == E_aBAddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,")
            {
                Guid id = new Guid("11111111-1111-1111-1111-111111111111");
                IVerificationOfRent record = dataApp.GetVorFields(id);

                string str = id.ToString().Replace("-", "");
                record.VerifSentD_rep = GetString("VerifSentD_" + str);
                record.VerifReorderedD_rep = GetString("VerifReorderedD_" + str);
                record.VerifRecvD_rep = GetString("VerifRecvD_" + str);
                record.VerifExpD_rep = GetString("VerifExpD_" + str);
                record.Update();
            }

            addr = string.Format("{0}, {1}, {2} {3}", dataApp.aBPrev1Addr, dataApp.aBPrev1City, dataApp.aBPrev1State, dataApp.aBPrev1Zip);
            if (dataApp.aBPrev1AddrT == E_aBPrev1AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,")
            {
                Guid id = new Guid("22222222-2222-2222-2222-222222222222");
                IVerificationOfRent record = dataApp.GetVorFields(id);
                string str = id.ToString().Replace("-", "");
                record.VerifSentD_rep = GetString("VerifSentD_" + str);
                record.VerifReorderedD_rep = GetString("VerifReorderedD_" + str);
                record.VerifRecvD_rep = GetString("VerifRecvD_" + str);
                record.VerifExpD_rep = GetString("VerifExpD_" + str);
                record.Update();
            }
            addr = string.Format("{0}, {1}, {2} {3}", dataApp.aBPrev2Addr, dataApp.aBPrev2City, dataApp.aBPrev2State, dataApp.aBPrev2Zip);
            if (dataApp.aBPrev2AddrT == E_aBPrev2AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,")
            {
                Guid id = new Guid("33333333-3333-3333-3333-333333333333");
                IVerificationOfRent record = dataApp.GetVorFields(id);
                string str = id.ToString().Replace("-", "");
                record.VerifSentD_rep = GetString("VerifSentD_" + str);
                record.VerifReorderedD_rep = GetString("VerifReorderedD_" + str);
                record.VerifRecvD_rep = GetString("VerifRecvD_" + str);
                record.VerifExpD_rep = GetString("VerifExpD_" + str);
                record.Update();
            }

            addr = string.Format("{0}, {1}, {2} {3}", dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip);
            if (dataApp.aCAddrT == E_aCAddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,")
            {
                Guid id = new Guid("44444444-4444-4444-4444-444444444444");
                IVerificationOfRent record = dataApp.GetVorFields(id);
                string str = id.ToString().Replace("-", "");
                record.VerifSentD_rep = GetString("VerifSentD_" + str);
                record.VerifReorderedD_rep = GetString("VerifReorderedD_" + str);
                record.VerifRecvD_rep = GetString("VerifRecvD_" + str);
                record.VerifExpD_rep = GetString("VerifExpD_" + str);
                record.Update();
            }

            addr = string.Format("{0}, {1}, {2} {3}", dataApp.aCPrev1Addr, dataApp.aCPrev1City, dataApp.aCPrev1State, dataApp.aCPrev1Zip);
            if (dataApp.aCPrev1AddrT == E_aCPrev1AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,")
            {
                Guid id = new Guid("55555555-5555-5555-5555-555555555555");
                IVerificationOfRent record = dataApp.GetVorFields(id);
                string str = id.ToString().Replace("-", "");
                record.VerifSentD_rep = GetString("VerifSentD_" + str);
                record.VerifReorderedD_rep = GetString("VerifReorderedD_" + str);
                record.VerifRecvD_rep = GetString("VerifRecvD_" + str);
                record.VerifExpD_rep = GetString("VerifExpD_" + str);
                record.Update();
            }
            addr = string.Format("{0}, {1}, {2} {3}", dataApp.aCPrev2Addr, dataApp.aCPrev2City, dataApp.aCPrev2State, dataApp.aCPrev2Zip);
            if (dataApp.aCPrev2AddrT == E_aCPrev2AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,")
            {
                Guid id = new Guid("66666666-6666-6666-6666-666666666666");
                IVerificationOfRent record = dataApp.GetVorFields(id);
                string str = id.ToString().Replace("-", "");
                record.VerifSentD_rep = GetString("VerifSentD_" + str);
                record.VerifReorderedD_rep = GetString("VerifReorderedD_" + str);
                record.VerifRecvD_rep = GetString("VerifRecvD_" + str);
                record.VerifExpD_rep = GetString("VerifExpD_" + str);
                record.Update();
            }

            //dataApp = dataLoan.GetAppData(ApplicationID);

            ILiaCollection liaList = dataApp.aLiaCollection;
            int liaCnt = liaList.CountRegular;
            for (int index = 0; index < liaCnt; index++)
            {
                ILiabilityRegular record = liaList.GetRegularRecordAt(index);
                if (record.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc))
                {
                    string id = record.RecordId.ToString().Replace("-", "");
                    record.VerifSentD_rep = GetString("VerifSentD_" + id);
                    record.VerifReorderedD_rep = GetString("VerifReorderedD_" + id);
                    record.VerifRecvD_rep = GetString("VerifRecvD_" + id);
                    record.VerifExpD_rep = GetString("VerifExpD_" + id);
                }
            }
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
    public class VOLTabServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VOLTabServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            ILiaCollection liaList = dataApp.aLiaCollection;
            int liaCnt = liaList.CountRegular;
            for (int index = 0; index < liaCnt; index++)
            {
                ILiabilityRegular record = liaList.GetRegularRecordAt(index);
                if (record.DebtT == E_DebtRegularT.Installment || record.DebtT == E_DebtRegularT.Revolving)
                {
                    string id = record.RecordId.ToString().Replace("-", "");
                    record.VerifSentD_rep = GetString("VerifSentD_" + id);
                    record.VerifReorderedD_rep = GetString("VerifReorderedD_" + id);
                    record.VerifRecvD_rep = GetString("VerifRecvD_" + id);
                    record.VerifExpD_rep = GetString("VerifExpD_" + id);
                }
            }
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class VOLandTabServiceItem : AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "DeleteVerification":
                    DeleteVerification();
                    break;
            }
        }
        private void DeleteVerification()
        {
            string value = GetString("Ids", string.Empty);
            if (string.IsNullOrEmpty(value) == false)
            {
                CPageData dataLoan = ConstructPageDataClass(sLId);
                dataLoan.InitSave(sFileVersion);

                CAppData dataApp = dataLoan.GetAppData(aAppId);
                string[] ids = value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                var vorCollection = dataApp.aVorCollection;
                foreach (string id in ids)
                {
                    vorCollection.DeleteByRecordId(new Guid(id));
                }

                dataApp.aVorCollection.Update();
                dataLoan.Save();
            }
        }
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(VOLandTabServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            int count = dataApp.GetVorRecordCount();

            for (int i = 0; i < count; i++)
            {
                IVerificationOfRent record = dataApp.GetVorFields(i);
                if (record.VerifT == E_VorT.LandContract || record.VerifT == E_VorT.Other)
                {
                    string id = record.RecordId.ToString().Replace("-", "");
                    record.VerifSentD_rep = GetString("VerifSentD_" + id);
                    record.VerifReorderedD_rep = GetString("VerifReorderedD_" + id);
                    record.VerifRecvD_rep = GetString("VerifRecvD_" + id);
                    record.VerifExpD_rep = GetString("VerifExpD_" + id);
                    record.Update();
                }
            }            

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public partial class VerificationsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("VODTab", new VODTabServiceItem());
            AddBackgroundItem("VOETab", new VOETabServiceItem());
            AddBackgroundItem("VOLTab", new VOLTabServiceItem());
            AddBackgroundItem("VOMTab", new VOMTabServiceItem());
            AddBackgroundItem("VOLandTab", new VOLandTabServiceItem());
        }
    }
}
