using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Pdf;

namespace LendersOfficeApp.newlos.Verifications
{
	public partial class VOERecord : BaseSingleEditPage<IEmploymentRecord>
	{
        protected string m_openedDate;

        private CPageData m_dataLoan;

        protected bool m_isBorrower;

        private CAppData m_dataApp;

        protected override string ListLocation
        {
            get { return VirtualRoot + "/newlos/Verifications/Verifications.aspx?loanid=" + LoanID + "&pg=1"; }
        }

        protected override DataView GetDataView()
        {
            DataTable table = new DataTable();
            table.Columns.Add(new DataColumn("recordid", typeof(string)));

            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            IEmpCollection recordList = dataApp.aBEmpCollection;
            var subCollection = recordList.GetSubcollection(true, E_EmpGroupT.All);

            foreach (IEmploymentRecord field in subCollection) {
                //OPM 68366: we should only be able to edit employers with non-empty names
                if (!string.IsNullOrEmpty(field.EmplrNm.Trim()))
                {
                    DataRow row = table.NewRow();
                    row["recordid"] = field.RecordId;
                    table.Rows.Add(row);
                }
            }

            recordList = dataApp.aCEmpCollection;
            subCollection = recordList.GetSubcollection(true, E_EmpGroupT.All);

            foreach (IEmploymentRecord field in subCollection)
            {
                //OPM 68366: we should only be able to edit employers with non-empty names
                if (!string.IsNullOrEmpty(field.EmplrNm.Trim()))
                {
                    DataRow row = table.NewRow();
                    row["recordid"] = field.RecordId;
                    table.Rows.Add(row);
                }

            }

            return table.DefaultView;
        }

        private IEmploymentRecord FindEmployment(CAppData dataApp, Guid recordID)
        {
            for (int i = 0; i < 2; i++)
            {
                m_isBorrower = i == 0;
                IEmpCollection empCollection = m_isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
                var subCollection = empCollection.GetSubcollection(true, E_EmpGroupT.All);
                foreach (IEmploymentRecord o in subCollection)
                {
                    if (o.RecordId == recordID)
                        return o; // 5/25/2004 dd - Return immediately
                }
            }
            return null; // 5/25/2004 dd - NOT FOUND return null
        }

        protected override IEmploymentRecord RetrieveRecord()
        {
            m_dataLoan = new CBorrowerInfoData(LoanID);
            m_dataLoan.InitLoad();
            m_dataApp = m_dataLoan.GetAppData(ApplicationID);
            IEmploymentRecord field = FindEmployment(m_dataApp, RecordID);

            Page.ClientScript.RegisterHiddenField("OwnerT", m_isBorrower ? "0" : "1");
            return field;
        }

        protected override void BindSingleRecord(IEmploymentRecord field)
        {
            if (field == null)
            {
                throw new CBaseException("Record no longer exists.", "The IEmploymentRecord f1 was null in VOEREcord.BindSingleRecord().");
            }

            m_openedDate = m_dataLoan.sOpenedD_rep;
            sLenderNumVerif.Text = m_dataLoan.sLenderNumVerif;

            IPreparerFields f = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfEmployment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f.IsValid)
            {
                PreparerName.Text = f.PreparerName;
                PreparerTitle.Text = f.Title;
                PreparerCompanyName.Text = f.CompanyName;
                PreparerStreetAddr.Text = f.StreetAddr;
                PreparerCity.Text = f.City;
                PreparerState.Value = f.State;
                PreparerZip.Text = f.Zip;
                PreparerPrepareDate.Text = f.PrepareDate_rep;
                PreparerPhone.Text = f.Phone;
                PreparerFaxNum.Text = f.FaxNum;
            }

            string name = m_isBorrower ? m_dataApp.aBLastFirstNm : m_dataApp.aCLastFirstNm;
            m_nameLabel.Text = name;
            Page.ClientScript.RegisterHiddenField("OwnerName", name);

            Attention.Text = field.Attention;
            EmplrAddr.Text = field.EmplrAddr;
            EmplrCity.Text = field.EmplrCity;
            EmplrState.Text = field.EmplrState;
            EmplrZip.Text = field.EmplrZip;
            EmplrNm.Text = field.EmplrNm;
            EmplrBusPhone.Text = field.EmplrBusPhone;
            EmplrFax.Text = field.EmplrFax;
            VerifSentD.Text = field.VerifSentD_rep;
            VerifRecvD.Text = field.VerifRecvD_rep;
            VerifExpD.Text = field.VerifExpD_rep;
            VerifReorderedD.Text = field.VerifReorderedD_rep;
            IsSeeAttachment.Checked = field.IsSeeAttachment;

            ClientScript.RegisterHiddenField("VerifSigningEmployeeId", field.VerifSigningEmployeeId.ToString());
            ClientScript.RegisterHiddenField("VerifHasSignature", field.VerifHasSignature.ToString());
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "VOE Record";
            this.PDFPrintClass = typeof(CVOEPDF);
            PreparerZip.SmartZipcode(PreparerCity, PreparerState);

            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
            bool enableSignature = this.Broker.EnableLqbNonCompliantDocumentSignatureStamp;
            ClientScript.RegisterHiddenField("EnableSignature", enableSignature.ToString());
            if (enableSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(BrokerUser.BrokerId, BrokerUser.EmployeeId, enableSignature);
                ClientScript.RegisterHiddenField("EmployeeHasSignature", signInfo.HasUploadedSignature.ToString());
                ClientScript.RegisterHiddenField("EmployeeName", BrokerUser.DisplayName);
            }
        }
	}
}
