﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// Order SSA-89 page.
    /// </summary>
    public partial class OrderSSA89 : OrderVOX
    {
        /// <summary>
        /// The SSA 89 component loader.
        /// </summary>
        private SSA89Loader ssa89Loader;

        /// <summary>
        /// Gets the lender service drop down list.
        /// </summary>
        protected override DropDownList LenderServiceList
        {
            get
            {
                return this.LenderServices;
            }
        }

        /// <summary>
        /// Gets the reasons the privilege was not allowed during the page's workflow checks.
        /// </summary>
        /// <returns>An array containing <seealso cref="WorkflowOperations.OrderSSA89"/>.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderSSA89 };
        }

        /// <summary>
        /// Gets the extra ops to check during the page's workflow checks.
        /// </summary>
        /// <returns>An array containing <seealso cref="WorkflowOperations.OrderSSA89"/>.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderSSA89 };
        }

        /// <summary>
        /// Binds some values if only one lender service is available.
        /// </summary>
        protected override void BindForSingleLenderService()
        {
            if (this.LenderServices.Items.Count == 1)
            {
                var service = VOXLenderService.GetLenderServiceById(this.BrokerID, int.Parse(this.LenderServices.Items[0].Value));
                AbstractVOXVendorService vendorService;
                if (service.VendorForVendorAndServiceData.Services.TryGetValue(VOXServiceT.SSA_89, out vendorService))
                {
                    SSA89VendorService voaService = (SSA89VendorService)vendorService;

                    var initialData = new InitialLenderServiceData();
                    var principal = PrincipalFactory.CurrentPrincipal;
                    var loanData = CPageData.CreateUsingSmartDependency(this.LoanID, typeof(OrderSSA89));
                    loanData.InitLoad();

                    var savedCredential = ServiceCredential.ListAvailableServiceCredentials(principal, loanData.sBranchId, ServiceCredentialService.Verifications)
                                            .FirstOrDefault(credential => credential.VoxVendorId.HasValue && credential.VoxVendorId == service.VendorForProtocolAndTransmissionData.VendorId);
                    if (savedCredential != null)
                    {
                        initialData.ServiceCredentialId = savedCredential.Id;
                        initialData.ServiceCredentialHasAccountId = !string.IsNullOrEmpty(savedCredential.AccountId);
                    }

                    initialData.UsesAccountId = service.VendorForProtocolAndTransmissionData.AssociatedPlatform.UsesAccountId;
                    initialData.RequiresAccountId = service.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId;

                    this.RegisterJsObjectWithJsonNetSerializer("InitialLenderService", initialData);
                }
            }
        }

        /// <summary>
        /// Gets the SSA-89 component loader.
        /// </summary>
        /// <returns>The SSA-89 component loader.</returns>
        protected override VOXLoader GetLoader()
        {
            if (this.ssa89Loader == null)
            {
                var loaderFactory = new VOXLoaderFactory(PrincipalFactory.CurrentPrincipal, this.LoanID);
                this.ssa89Loader = loaderFactory.Ssa89Loader;
            }

            return this.ssa89Loader;
        }

        /// <summary>
        /// Order specific OnInit actions.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void PerformOnInit(EventArgs e)
        {
            this.RegisterService("SSA89", "/newlos/Verifications/OrderSSA89Service.aspx");
            this.RegisterJsScript("AuthorizationDocPicker.js");
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        protected override void LoadData()
        {
            this.RegisterJsGlobalVariables("CanOrderSSA89", this.UserHasWorkflowPrivilege(WorkflowOperations.OrderSSA89));
            this.RegisterJsGlobalVariables("OrderSSA89DenialReason", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.OrderSSA89));

            SSA89Loader loader = this.GetLoader() as SSA89Loader;

            var borrowers = loader.GetBorrowers();
            this.BorrowerRepeater.DataSource = borrowers;
            this.BorrowerRepeater.DataBind();
        }

        /// <summary>
        /// Class to hold data for the initially chosen lender service.
        /// </summary>
        private class InitialLenderServiceData
        {
            /// <summary>
            /// Gets or sets the service credential id.
            /// </summary>
            /// <value>The service credential id.</value>
            public int? ServiceCredentialId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the service credential has account id.
            /// </summary>
            /// <value>Whether the service credential has an account id.</value>
            public bool? ServiceCredentialHasAccountId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether it uses an account id.
            /// </summary>
            /// <value>Whether it uses an account id.</value>
            public bool? UsesAccountId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether it requires an account id.
            /// </summary>
            /// <value>Whether it requires an account id.</value>
            public bool? RequiresAccountId
            {
                get; set;
            }
        }
    }
}