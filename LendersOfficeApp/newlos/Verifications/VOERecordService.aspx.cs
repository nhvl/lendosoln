using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.newlos.Verifications
{

	public partial class VOERecordService : AbstractSingleEditService
	{

        protected override void CustomProcess(string methodName)
        {
            switch (methodName)
            {
                case "ApplySig":
                    ApplySig();
                    break;
                case "ClearSig":
                    ClearSig();
                    break;
                default:
                    break;
            }
        }

        private IEmploymentRecord FindEmployment(CAppData dataApp, Guid recordID, bool isBorrower)
        {
            for (int i = 0; i < 2; i++)
            {
                isBorrower = i == 0;
                IEmpCollection empCollection = isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
                var subCollection = empCollection.GetSubcollection(true, E_EmpGroupT.All);
                foreach (IEmploymentRecord o in subCollection)
                {
                    if (o.RecordId == recordID)
                        return o; // 5/25/2004 dd - Return immediately
                }
            }
            return null; // 5/25/2004 dd - NOT FOUND return null
        }

        private void ApplySig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
            IEmploymentRecord field = FindEmployment(dataApp, RecordID, GetBool("isborrower"));
            field.ApplySignature(principal.EmployeeId, signInfo.SignatureKey.ToString());
            field.Update();
            dataLoan.Save();

            SetResult("EmployeeId", principal.EmployeeId);
        }

        private void ClearSig()
        {
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            if (!principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            IEmploymentRecord field = FindEmployment(dataApp, RecordID, GetBool("isborrower"));
            field.ClearSignature();
            field.Update();
            dataLoan.Save();
        }

        private void BindData(CPageData dataLoan) 
        {
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            dataLoan.sLenderNumVerif = GetString("sLenderNumVerif");

            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfEmployment, E_ReturnOptionIfNotExist.CreateNew);
            f.PreparerName = GetString("PreparerName") ;
            f.CompanyName = GetString("PreparerCompanyName");
            f.Title = GetString("PreparerTitle") ;
            f.StreetAddr = GetString("PreparerStreetAddr");
            f.City = GetString("PreparerCity");
            f.State = GetString("PreparerState");
            f.Zip = GetString("PreparerZip");
            f.PrepareDate_rep = GetString("PreparerPrepareDate");
            f.Phone = GetString("PreparerPhone");
            f.FaxNum = GetString("PreparerFaxNum");
            f.Update();

            bool isBorrower = GetString("OwnerT") == "0";

            IEmpCollection recordList = isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;

            IEmploymentRecord field = recordList.GetPrimaryEmp(false);

            if (null != field) 
            {
                if (field.RecordId != RecordID) field = null;
            }
            if (null == field) 
            {
                field = recordList.GetRegRecordOf(RecordID);
            }

            field.VerifExpD_rep = GetString("VerifExpD");
            field.VerifRecvD_rep = GetString("VerifRecvD");
            field.VerifSentD_rep = GetString("VerifSentD");
            field.VerifReorderedD_rep = GetString("VerifReorderedD") ;
            field.IsSeeAttachment = GetBool("IsSeeAttachment");

            field.Attention = GetString("Attention");


        

        }
        protected override void SaveData() 
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);

            BindData(dataLoan);

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
        }

        protected override void SaveDataAndLoadNext() 
        {

            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            BindData(dataLoan);
            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan, dataApp, NextRecordID);
        }

        protected override void LoadData() 
        {
            CPageData dataLoan = new CBorrowerInfoData(LoanID);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            
            LoadData(dataLoan, dataApp, RecordID);
        }
        private void LoadData(CPageData dataLoan, CAppData dataApp, Guid recordID) 
        {


            SetResult("sLenderNumVerif", dataLoan.sLenderNumVerif);

            IPreparerFields f1 = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfEmployment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (f1.IsValid) 
            {
                SetResult("PreparerName", f1.PreparerName );
                SetResult("PreparerTitle", f1.Title );
                SetResult("PreparerCompanyName", f1.CompanyName );

                SetResult("PreparerStreetAddr", f1.StreetAddr );
                SetResult("PreparerCity", f1.City );
                SetResult("PreparerState", f1.State );
                SetResult("PreparerZip", f1.Zip );
                SetResult("PreparerPrepareDate", f1.PrepareDate_rep );
                SetResult("PreparerPhone", f1.Phone);
                SetResult("PreparerFaxNum", f1.FaxNum);

            }
            // Since there is no API to search employment record to search across borrower and coborrower.
            // I have to handle it here. dd 10/7/03.

            bool isBorrower = true;
            IEmpCollection recordList = dataApp.aBEmpCollection;

            IEmploymentRecord field = recordList.GetPrimaryEmp(false);
            if (null != field) 
            {
                if (field.RecordId != recordID) field = null;
            }

            if (null == field) 
            {
                try 
                {
                    field = recordList.GetRegRecordOf(recordID);
                } 
                catch
                {
                    // Employment record does not belong to borrower.
                }
            }
            if (null == field) 
            {
                // Try to search in coborrower section.
                isBorrower = false;
                
                recordList = dataApp.aCEmpCollection;

                field = recordList.GetPrimaryEmp(false);

                if (null != field) 
                {
                    if (field.RecordId != recordID) field = null;
                }

                if (null == field) 
                {
                    // Employment record must existed here or exception will be throw.
                    field = recordList.GetRegRecordOf(recordID);
                }

            }

            if (isBorrower) 
            {
                SetResult("OwnerName", string.Format(@"{1}, {0}", dataApp.aBFirstNm, dataApp.aBLastNm));
                SetResult("OwnerT", "0");
            } 
            else 
            {
                SetResult("OwnerName", string.Format(@"{1}, {0}", dataApp.aCFirstNm, dataApp.aCLastNm));
                SetResult("OwnerT", "1");
            }

            SetResult("RecordID", field.RecordId);
            SetResult("EmplrAddr", field.EmplrAddr);
            SetResult("EmplrCity", field.EmplrCity);
            SetResult("EmplrState", field.EmplrState);
            SetResult("EmplrZip", field.EmplrZip);
            SetResult("EmplrNm", field.EmplrNm);
            SetResult("EmplrBusPhone", field.EmplrBusPhone);
            SetResult("EmplrFax", field.EmplrFax);
            SetResult("Attention", field.Attention);
            SetResult("VerifSentD", field.VerifSentD);
            SetResult("VerifRecvD", field.VerifRecvD);
            SetResult("VerifExpD", field.VerifExpD);
            SetResult("VerifReorderedD", field.VerifReorderedD);
            SetResult("IsSeeAttachment", field.IsSeeAttachment);

            SetResult("VerifSigningEmployeeId", field.VerifSigningEmployeeId);
            SetResult("VerifHasSignature", field.VerifHasSignature);

        }

	}
}
