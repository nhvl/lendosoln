﻿#region Auto Generated code
namespace LendersOfficeApp.newlos.Verifications
#endregion
{
    using System;
    using System.Web.UI.WebControls;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Security;

    /// <summary>
    /// Page for ordering VOD.
    /// </summary>
    public partial class OrderVOD : OrderVOX
    {
        /// <summary>
        /// The VOA component loader.
        /// </summary>
        private VOALoader voaLoader;

        /// <summary>
        /// Gets the lender service dropdown list.
        /// </summary>
        protected override DropDownList LenderServiceList
        {
            get
            {
                return this.LenderServices;
            }
        }

        /// <summary>
        /// Gets the reasons the privilege was not allowed during the page's workflow checks.
        /// </summary>
        /// <returns>An array containing <seealso cref="WorkflowOperations.OrderVoa"/>.</returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderVoa };
        }

        /// <summary>
        /// Gets the extra ops to check during the page's workflow checks.
        /// </summary>
        /// <returns>An array containing <seealso cref="WorkflowOperations.OrderVoa"/>.</returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new WorkflowOperation[] { WorkflowOperations.OrderVoa };
        }

        /// <summary>
        /// Gets the VOA component loader.
        /// </summary>
        /// <returns>The VOA component loader.</returns>
        protected override VOXLoader GetLoader()
        {
            if (this.voaLoader == null)
            {
                var loaderFactory = new VOXLoaderFactory(PrincipalFactory.CurrentPrincipal, this.LoanID);
                this.voaLoader = loaderFactory.VoaLoader;
            }

            return this.voaLoader;
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        protected override void LoadData()
        {
            this.RegisterJsGlobalVariables("CanOrderVoa", this.UserHasWorkflowPrivilege(WorkflowOperations.OrderVoa));
            this.RegisterJsGlobalVariables("OrderVoaDenialReason", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.OrderVoa));

            VOALoader loader = this.GetLoader() as VOALoader;

            var assets = loader.GetAvailableAssets();
            this.AssetsRepeater.DataSource = assets;
            this.AssetsRepeater.DataBind();

            var borrowers = loader.GetAvailableBorrowers();
            foreach (var borrower in borrowers)
            {
                this.BorrowerList.Items.Add(new ListItem(borrower.Name, borrower.Key));
            }

            if (this.BorrowerList.Items.Count > 1)
            {
                this.BorrowerList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
            }

            var principal = PrincipalFactory.CurrentPrincipal;
            var employee = LendersOffice.Admin.EmployeeDB.RetrieveByIdOrNull(principal.BrokerId, principal.EmployeeId);
            if (employee != null)
            {
                this.NotificationEmail.Value = employee.Email;
            }
        }

        /// <summary>
        /// Order specific OnInit actions.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void PerformOnInit(EventArgs e)
        {
            this.RegisterService("VOA", "/newlos/Verifications/OrderVODService.aspx");
            this.RegisterJsScript("AuthorizationDocPicker.js");
            this.RegisterJsGlobalVariables("MaxOptionValue", VOAVendorService.MaxOption);
            this.RegisterJsGlobalVariables("MinOptionValue", VOAVendorService.MinOption);
        }
    }
}
