﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderVOE.aspx.cs" Inherits="LendersOfficeApp.newlos.Verifications.OrderVOE" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Order Verification of Employment</title>
    <style type="text/css">
        body  {
            background-color: gainsboro;
        }
        a[data-sort] {
            color: white;
        }
        #SelectionHeader {
            width: 200px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        var serviceCredentialId = null;
        $(function ($) {
            var verifyIncomeEnabled = false;
            var borrowerAuthDocsEnabled = false;
            var isEmploymentPickerBeingUsed;
            var salaryKeyEnabled = false;

            $('#EmploymentLink').click(function () {
                if (!isLqbPopup(window)) {
                    linkMe(gVirtualRoot + '/newlos/BorrowerInfo.aspx?pg=1');
                }
                else {
                    parent.linkMe(gVirtualRoot + '/newlos/BorrowerInfo.aspx?pg=1');
                }
            });

            $('#ContactPicker').click(function () {
                LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/los/RolodexList.aspx?isCurrentAgentOnly=true&loanid=" + this.LoanID)) %>, {
                    hideCloseButton: true,
                    width: 800,
                    height: 400,
                    onReturn: function(returnArgs) {
                        if(typeof(returnArgs) != 'undefined' && returnArgs != null) {
                            var email = typeof(returnArgs.AgentEmail) !== 'string' ? '' : returnArgs.AgentEmail;
                            ToggleNotificationEmail(email);
                        }
                    }
                }, null);
            });

            $('#CheckAllCB').click(function () {
                if(this.checked) {
                    var employmentCBs = $('.EmploymentCB:not(:checked)');
                    employmentCBs.each(function (index) {
                        $(this).prop('checked', true);
                        CheckBorrowerAuthDocsForEmployments(this, false);
                        CheckSalaryKey(this);
                        CheckSelfEmployment(this, false);
                    });
                }
                else {
                    var employmentCBs = $('.EmploymentCB:checked');
                    employmentCBs.each(function (index) {
                        $(this).prop('checked', false);
                        CheckBorrowerAuthDocsForEmployments(this, false);
                        CheckSalaryKey(this);
                        CheckSelfEmployment(this, false);
                    });
                }

                ToggleOrderBtn();
            });

            $('.EmploymentCB').click(function() {
                CheckBorrowerAuthDocsForEmployments(this, false);
                CheckSalaryKey(this);
                CheckSelfEmployment(this, false);
                $('#CheckAllCB').prop('checked', $('.EmploymentCB:checked').length === $('.EmploymentCB').length);
                ToggleOrderBtn();
            });

            $('.SectionToggle').change(function() {
                TogglePickerSection(!isEmploymentPickerBeingUsed);
            });

            $('#BorrowerList').change(function() {
                CheckBorrowerAuthDocsForBorrowers(false);
                ToggleOrderBtn();
            });

            function OnClearLenderServicesDDL() {
                ToggleSpecificEmployerRecordSearch({ UseSpecificEmployerRecordSearch: false, EnforceUseSpecificEmployerRecordSearch: true });
                SalaryKey.RemoveSalaryKey(null, $('#SalaryKeyTable'));
                salaryKeyEnabled = false;
                $('#SalaryKeySection').hide();

                AuthorizationDocPicker.RemoveDocPicker(null, $('#DocumentTable'));
                borrowerAuthDocsEnabled = false;
                $('#BorrowerAuthDocsSection').hide();

                $('#VerifyIncome').prop('checked', false);
                verifyIncomeEnabled = false;
                ToggleOrderBtn();
            }

            $('#LenderServices').change(function () {
                var previousValue = $(this).data('previous');
                var lenderServiceId = $(this).val();
                $(this).data('previous', lenderServiceId);

                if(!lenderServiceId) {
                    OnClearLenderServicesDDL();
                    return;
                }

                var data = {
                    LenderServiceId: lenderServiceId,
                    LoanId: ML.sLId,
                };

                var result = gService.VOE.call("LoadLenderService", data)
                if(result.error) {
                    alert(result.UserMessage);
                }
                else if (result.value["Success"].toLowerCase() !== 'true') {
                    alert(result.value["Errors"]);
                } else {
                    var showCreditCard = result.value["AllowPaymentByCreditCard"].toLowerCase() == 'true';
                    var requiresAccountId = result.value["RequiresAccountId"].toLowerCase() == 'true';
                    var usesAccountId = result.value["UsesAccountId"].toLowerCase() === 'true';
                    var verificationType = result.value["VoeVerificationT"];
                    var askForSalaryKey = result.value["AskForSalaryKey"];
                    var useSpecificEmployerRecordSearch = result.value['UseSpecificEmployerRecordSearch'] === 'True';
                    var enforceUseSpecificEmployerRecordSearch = result.value['EnforceUseSpecificEmployerRecordSearch'] === 'True';
                    var voeAllowRequestWithoutEmployment = result.value["VoeAllowRequestWithoutEmployment"].toLowerCase() == 'true';
                    var voeEnforceRequestsWithoutEmployments = result.value["VoeEnforceRequestsWithoutEmployments"].toLowerCase() == 'true';

                    TogglePickerSection(!voeAllowRequestWithoutEmployment, !voeEnforceRequestsWithoutEmployments);

                    $('#CreditCardRow').toggle(showCreditCard);
                    ToggleSpecificEmployerRecordSearch({ UseSpecificEmployerRecordSearch: useSpecificEmployerRecordSearch, EnforceUseSpecificEmployerRecordSearch: enforceUseSpecificEmployerRecordSearch });

                    var credentialId = typeof(result.value["ServiceCredentialId"]) === 'undefined' ? null : result.value["ServiceCredentialId"];
                    var serviceCredentialHasAccountId = typeof(result.value["ServiceCredentialHasAccountId"]) === 'undefined' ? false : result.value["ServiceCredentialHasAccountId"].toLowerCase() === 'true';
                    VOXOrderCommon.BindCredentialOptions(usesAccountId, requiresAccountId, credentialId, serviceCredentialHasAccountId);

                    $('#BorrowerAuthDocsSection').show();
                    borrowerAuthDocsEnabled = true;

                    var salaryKeyPreviouslyEnabled = salaryKeyEnabled;
                    salaryKeyEnabled = askForSalaryKey.toLowerCase() === 'true';
                    if(!salaryKeyEnabled) {
                        SalaryKey.RemoveSalaryKey(null, $('#SalaryKeyTable'));
                        $('#SalaryKeySection').hide();
                    }
                    else if(!salaryKeyPreviouslyEnabled && salaryKeyEnabled) {
                        $('.EmploymentCB:checked').each(function(index, element) {
                            CheckSalaryKey(this);
                        });
                    }

                    var verifyIncomePreviouslyEnabled = verifyIncomeEnabled;
                    verifyIncomeEnabled = verificationType === '1';
                    var verifyIncomePrevious = $('#VerifyIncome').is(':checked');
                    if(!verifyIncomeEnabled) {
                        $('#VerifyIncome').prop('checked', false);
                        $('#VerifyIncomeRow').hide();
                    }
                    else if(!verifyIncomePreviouslyEnabled && verifyIncomeEnabled) {
                        $('#VerifyIncome').prop('checked', true);
                        $('#VerifyIncomeRow').show();
                    }

                    if (isEmploymentPickerBeingUsed) {
                        var employmentCBs = $('.EmploymentCB:checked');
                        employmentCBs.each(function () {
                            CheckBorrowerAuthDocsForEmployments(this, false);
                        });
                    }
                    else {
                        CheckBorrowerAuthDocsForBorrowers(false);
                    }

                    if(verifyIncomePrevious != $('#VerifyIncome').is(':checked')) {
                        $('#VerifyIncome').change();
                    }
                }

                ToggleOrderBtn();
                $('#BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, $('#DocumentTable')));
            });

            $('#VerifyIncome').change(function() {
                var isVerifyIncome = $(this).is(':checked');
                var table = $('#DocumentTable');
                $('.EmploymentCB:checked').each(function () {
                    if (isVerifyIncome && isEmploymentPickerBeingUsed) {
                        CheckBorrowerAuthDocsForEmployments(this, false);
                    }
                    else if (!isVerifyIncome && isEmploymentPickerBeingUsed) {
                        var employmentRow = $(this).closest('tr');
                        var isSelfEmployment = employmentRow.find('.IsSelfEmployment').text().toLowerCase() === 'yes';
                        var key = employmentRow.find('.Key').val();

                        var anyMoreChecked = $('#EmploymentTable').find('.Key[value="' + key + '"]').filter(function() {
                            var isChecked = $(this).siblings('.EmploymentCB').is(':checked');
                            var isSelfEmployment = $(this).closest('tr').find('.IsSelfEmployment').text().toLowerCase() === 'yes';
                            return isChecked && isSelfEmployment;
                        }).length > 0;

                        if(!anyMoreChecked && AuthorizationDocPicker.ContainsDocPicker(key, table)) {
                            AuthorizationDocPicker.RemoveDocPicker(key, table);
                        }
                    }
                });

                if (!isEmploymentPickerBeingUsed) {
                    CheckBorrowerAuthDocsForBorrowers(true);
                }

                ToggleOrderBtn();
                $('#BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, $('#DocumentTable')));
            });

            function RowComparer(sortTargetClass) {
                return function (a, b) {
                    var sortTargetA = $(a).find('.' + sortTargetClass);
                    var sortTargetB = $(b).find('.' + sortTargetClass);

                    var sortValueA;
                    var sortValueB;
                    if(sortTargetClass === 'YrsOnJob') {
                        sortValueA = 0;
                        sortValueB = 0;
                        var aString = sortTargetA.text();
                        if(aString !== '') {
                            sortValueA = parseFloat(aString);
                        }

                        var bString = sortTargetB.text();
                        if(bString !== '') {
                            sortValueB = parseFloat(bString);
                        }
                    }
                    else {
                        sortValueA = sortTargetA.text();
                        sortValueB = sortTargetB.text();
                    }

                    if (sortValueA < sortValueB) {
                        return -1;
                    }
                    else if (sortValueA > sortValueB) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                }
            }

            function ToggleNotificationEmail(email) {
                if(typeof(email) === 'string') {
                    $('#NotificationEmail').val(email);
                }

                $('#NotificationEmail').siblings('.RequiredImg').toggle(email === '');

                ToggleOrderBtn();
            }

            function CheckBorrowerAuthDocsForBorrowers(shouldCheckOrderBtn) {
                if(!borrowerAuthDocsEnabled) {
                    return;
                }
                var table = $('#DocumentTable');

                var $borrowerDdl = $('#BorrowerList');
                var key = $borrowerDdl.val();
                var verifyIncome = $('#VerifyIncome').is(':visible :checked');

                if(!key || !verifyIncome) {
                    AuthorizationDocPicker.RemoveDocPicker(null, table);
                }
                else {
                    var containsPicker = AuthorizationDocPicker.ContainsDocPicker(key, table);
                    if(!containsPicker) {
                        AuthorizationDocPicker.RemoveDocPicker(null, table);
                        var name = $borrowerDdl.find('option:selected').text();
                        var split = key.split('_');
                        var appId = split[0];
                        var ownerType = split[1];

                        var picker = AuthorizationDocPicker.CreateDocPicker(name, appId, ownerType, key);
                        table.append(picker);
                    }
                }

                if(shouldCheckOrderBtn) {
                    ToggleOrderBtn();
                }

                $('#BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, table));
            }

            function CheckBorrowerAuthDocsForEmployments(employmentCB, shouldCheckOrderBtn) {
                if(!borrowerAuthDocsEnabled) {
                    return;
                }

                var employmentRow = $(employmentCB).closest('tr');
                var appId = employmentRow.find('.AppId').val();
                var borrowerType = employmentRow.find('.BorrowerType').val();
                var borrowerName = employmentRow.find('.Borrower').text();
                var isSelfEmployment = employmentRow.find('.IsSelfEmployment').text().toLowerCase() === 'yes';
                var verifyIncome = $('#VerifyIncome').is(':visible :checked');
                var key = employmentRow.find('.Key').val();
                var table = $('#DocumentTable');

                if(employmentCB.checked) {
                    // This got checked so if the picker doesn't already exist, add it
                    var containsPicker = AuthorizationDocPicker.ContainsDocPicker(key, table);
                    if(!containsPicker && (verifyIncome || isSelfEmployment)) {
                        var picker = AuthorizationDocPicker.CreateDocPicker(borrowerName, appId, borrowerType, key);
                        table.append(picker);
                    }
                }
                else {

                    // Unchecked, need to see if there are any more that are checked.
                    var anyMoreChecked;
                    if(verifyIncome) {
                        // If verify income is enabled, all checkboxes are up for grabs.
                        anyMoreChecked = $('#EmploymentTable').find('.Key[value="' + key + '"]').siblings('.EmploymentCB:checked').length > 0;
                    }
                    else {
                        // If not enabled, we only consider the rows that are self employment
                        anyMoreChecked = $('#EmploymentTable').find('.Key[value="' + key + '"]').filter(function() {
                            var isChecked = $(this).siblings('.EmploymentCB').is(':checked');
                            var isSelfEmployment = $(this).closest('tr').find('.IsSelfEmployment').text().toLowerCase() === 'yes';
                            return isChecked && isSelfEmployment;
                        }).length > 0;
                    }

                    if(!anyMoreChecked && AuthorizationDocPicker.ContainsDocPicker(key, table)) {
                        AuthorizationDocPicker.RemoveDocPicker(key, table);
                    }
                }

                if(shouldCheckOrderBtn) {
                    ToggleOrderBtn();
                }

                $('#BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, $('#DocumentTable')));
            }

            function CheckSalaryKey(employmentCb) {
                if(!salaryKeyEnabled) {
                    return;
                }

                var employmentRow = $(employmentCb).closest('tr');
                var appId = employmentRow.find('.AppId').val();
                var borrowerType = employmentRow.find('.BorrowerType').val();
                var borrowerName = employmentRow.find('.Borrower').text();
                var key = employmentRow.find('.Key').val();
                var container = $('#SalaryKeyTable');

                if(employmentCb.checked) {
                    // This was checked. If no salary key, then add it.
                    var containsSalaryKey = SalaryKey.ContainsSalaryKey(key, container);
                    if(!containsSalaryKey) {
                        var picker = SalaryKey.CreateSalaryKeyRow(borrowerName, appId, borrowerType, key);
                        $('#SalaryKeyTable').append(picker);
                    }
                }
                else {
                    var anyMoreChecked = $('#EmploymentTable').find('.Key[value="' + key + '"]').siblings('.EmploymentCB:checked').length > 0;
                    if(!anyMoreChecked && SalaryKey.ContainsSalaryKey(key, container)) {
                        SalaryKey.RemoveSalaryKey(key, container);
                    }
                }

                $('#SalaryKeySection').toggle(SalaryKey.ContainsSalaryKey(null, container));
            }

            function CheckSelfEmployment(employmentCb, shouldCheckOrderBtn) {
                var employmentRow = $(employmentCb).closest('tr');
                var isSelfEmployment = employmentRow.find('.IsSelfEmployment').text().toLowerCase() === 'yes';
                if(!isSelfEmployment) {
                    return;
                }

                var appId = employmentRow.find('.AppId').val();
                var borrowerType = employmentRow.find('.BorrowerType').val();
                var borrowerName = employmentRow.find('.Borrower').text();
                var key = employmentRow.find('.Key').val();
                var container = $('#SelfEmploymentTarget');

                if(employmentCb.checked) {
                    var containsPicker = SelfEmployment.ContainsSelfEmploymentRow(key, container);
                    if(!containsPicker) {
                        var picker = SelfEmployment.CreateSelfEmploymentRow(borrowerName, appId, borrowerType, key);
                        $('#SelfEmploymentTarget').append(picker);
                    }
                }
                else {
                    var anyMoreChecked = $('#EmploymentTable').find('.Key[value="' + key + '"]').filter(function() {
                        var isChecked = $(this).siblings('.EmploymentCB').is(':checked');
                        var isSelfEmployment = $(this).closest('tr').find('.IsSelfEmployment').text().toLowerCase() === 'yes';
                        return isChecked && isSelfEmployment;
                    }).length > 0;

                    if(!anyMoreChecked && SelfEmployment.ContainsSelfEmploymentRow(key, container)) {
                        SelfEmployment.RemoveSelfEmploymentRow(key, container);
                    }
                }

                if(typeof(shouldCheckOrderBtn) !== 'undefined' && shouldCheckOrderBtn !== null && shouldCheckOrderBtn) {
                    ToggleOrderBtn();
                }
            }

            function ToggleSpecificEmployerRecordSearch(lenderService) {
                $('#UseSpecificEmployerRecordSearch').prop('checked', lenderService.UseSpecificEmployerRecordSearch);
                $('.UseSpecificEmployerRecordSearchRow').toggle(!lenderService.EnforceUseSpecificEmployerRecordSearch);
            }

            function ToggleOrderBtn() {
                var docsVisible = $("#BorrowerAuthDocsSection").is(':visible');
                var docsOk = docsVisible ? AuthorizationDocPicker.AllPickersHaveDocs() : true;
                var inputsOk = $('.RequiredInput:visible').filter(function() { return $(this).val() === '' }).length === 0;
                var selfPrepareOk = SelfEmployment.CheckAllSelfEmploymentFilledOut();
                var lenderService = $('#LenderServices').val();
                var lenderServiceChosen = lenderService != null && lenderService !== '';
                var employmentChosen = !isEmploymentPickerBeingUsed || $('.EmploymentCB:checked').length > 0;
                var borrowerChosen = isEmploymentPickerBeingUsed || $('#BorrowerList').val() !== '';

                var isValidated = ML.CanOrderVoe && docsOk && inputsOk && lenderServiceChosen && selfPrepareOk && employmentChosen && borrowerChosen;
                $('#OrderBtn').prop('disabled', !isValidated);
            }

            function GatherRequestData() {
                var employmentInfo = [];
                $('#EmploymentTable tbody').find('tr').each(function() {
                    var employmentCbChecked = $(this).find('.EmploymentCB').is(':checked');
                    if(employmentCbChecked)
                    {
                        var recordId = $(this).find('.EmploymentRecordId').val();
                        var appId = $(this).find('.AppId').val();
                        var borrowerType = $(this).find('.BorrowerType').val();

                        var recordData = {
                            AppId: appId,
                            EmploymentRecordId: recordId,
                            BorrowerType: borrowerType
                        };

                        employmentInfo.push(recordData);
                    }
                });

                var data = {
                    LoanId: ML.sLId,
                    LenderService: $('#LenderServices').val(),
                    NotificationEmail: $('#NotificationEmail').is(':visible') ? $('#NotificationEmail').val() : '',
                    PayWithCreditCard: $('#AllowPayWithCreditCard').is(':visible') && $('#AllowPayWithCreditCard').is(':checked'),
                    UseSpecificEmployerRecordSearch: $('#UseSpecificEmployerRecordSearch').is(':checked'),
                    AccountId: $('#AccountId').is(':visible') ? $('#AccountId').val() : '',
                    UserName: $('#Username').is(':visible') ? $('#Username').val() : '',
                    Password: $('#Password').is(':visible') ? $('#Password').val() : '',
                    ServiceCredentialId: serviceCredentialId,
                    DocPickerInfo: $('#BorrowerAuthDocsSection').is(':visible') ? AuthorizationDocPicker.RetrieveAllDocPickerInfo($('#DocumentTable')).join(',') : '',
                    EmploymentInfo: JSON.stringify(employmentInfo),
                    VerifyIncome: verifyIncomeEnabled && $('#VerifyIncome').is(':checked'),
                    SalaryKeys: JSON.stringify(salaryKeyEnabled ? SalaryKey.RetrieveAllSalaryKeys($('#SalaryKeySection')) : []),
                    SelfEmploymentInfo: JSON.stringify(SelfEmployment.RetrieveAllSelfEmploymentSections($('#SelfEmploymentTarget'))),
                    BorrowerInfo: $('#BorrowerList').val(),
                    VerifiesBorrower: !isEmploymentPickerBeingUsed
                };

                return data;
            }

            function TogglePickerSection(useEmploymentPicker, canSwitch) {
                $('.PickerSection').hide();
                if (typeof (canSwitch) === 'boolean') {
                    $('#SectionToggleSection').prop('disabled', !canSwitch).toggleClass('Hidden', !canSwitch).toggleClass('InlineBlock', canSwitch);
                    if (canSwitch && typeof (isEmploymentPickerBeingUsed) === 'boolean') {
                        useEmploymentPicker = isEmploymentPickerBeingUsed;
                    }
                }

                $('.EmploymentPickerSection').toggle(useEmploymentPicker);
                $('#EmploymentToggle').prop('checked', useEmploymentPicker);
                $('.BorrowerPickerSection').toggle(!useEmploymentPicker);
                $('#BorrowerToggle').prop('checked', !useEmploymentPicker);

                var oldIsEmploymentPickerBeingUsed = typeof (isEmploymentPickerBeingUsed) === 'boolean' ? isEmploymentPickerBeingUsed : useEmploymentPicker;
                isEmploymentPickerBeingUsed = useEmploymentPicker;

                if (oldIsEmploymentPickerBeingUsed !== isEmploymentPickerBeingUsed) {
                    $('.EmploymentCB').prop('checked', false);
                    $('#BorrowerList').prop('selectedIndex', 0);
                    AuthorizationDocPicker.RemoveDocPicker(null, $('#DocumentTable'));

                    if (!isEmploymentPickerBeingUsed) {
                        CheckBorrowerAuthDocsForBorrowers(true);
                    }
                }

                $('#BorrowerAuthDocsSection').toggle(AuthorizationDocPicker.ContainsDocPicker(null, $('#DocumentTable')));
                ToggleOrderBtn();
            }

            OnClearLenderServicesDDL();

            //// On Load
            $('.WarningIcon').toggle(!ML.CanOrderVoe).attr('title', ML.OrderVoeDenialReason);

            ToggleNotificationEmail();
            AuthorizationDocPicker.RegisterDocChangeCallback(ToggleOrderBtn);
            SelfEmployment.RegisterStates(States);
            SelfEmployment.RegisterPostChangeCallback(ToggleOrderBtn);
            var validStatus = {
                0: false, // Pending
                1: true, // Complete
                3: true, // NoHit
            };

            VOXOrderCommon.InitializeCommonFunctions({
                RowComparer: RowComparer,
                GatherRequestData: GatherRequestData,
                ServiceType: "VOE",
                ToggleOrderBtn: ToggleOrderBtn,
                ValidStatusesAfterInitialRequest: validStatus,
                ShouldPushOutErrors: true
            });

            setTimeout(function() {
                $('#LenderServices').change();
            });
        });
    </script>
    <form id="form1" runat="server">
        <div>
            <div class="MainRightHeader">Order Verification of Employment</div>
        </div>
        <div class="Padding5">
            <div>
                <div>
                    <div>
                        <div class="InlineBlock PaddingTopBottom5 Header3 align-top" id="SelectionHeader">
                            <span class="PickerSection EmploymentPickerSection">Select employment to verify</span>
                            <span class="PickerSection BorrowerPickerSection Hidden">Select borrower to verify</span>
                        </div>
                        <div class="Hidden" id="SectionToggleSection">
                            <div>
                                <label>
                                    <input type="radio" name="SectionToggle" class="SectionToggle" id="EmploymentToggle" checked="checked" />Verify employment records
                                </label>
                            </div>
                            <div>
                                <label>
                                    <input type="radio" name="SectionToggle" class="SectionToggle" id="BorrowerToggle" />Verify borrower employment
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="PickerSection EmploymentPickerSection">
                        <table id="EmploymentTable" class="Table">
                            <thead>
                                <tr class="LoanFormHeader">
                                    <td>
                                        <input type="checkbox" id="CheckAllCB" />
                                    </td>
                                    <td><a data-sort="true" data-sorttarget="Borrower" data-asc="false">Borrower</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Employer" data-asc="false">Employer</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="IsSelfEmployment" data-asc="false">Self</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="IsCurrent" data-asc="false">Current</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="EmploymentEndDate" data-asc="false">End Date</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="YrsOnJob" data-asc="false">Yrs on Job</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="PositionTitle" data-asc="false">Position/Title</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="OrderStatus" data-asc="false">Order Status</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                    <td><a data-sort="true" data-sorttarget="Type" data-asc="false">Type</a>&nbsp;<img alt="Sort Image" class="SortImg align-middle Hidden"/></td>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="EmploymentRepeater" runat="server">
                                    <ItemTemplate>
                                        <tr class="ReverseGridAutoItem">
                                            <td>
                                                <input type="checkbox" class="EmploymentCB" />
                                                <input type="hidden" class="EmploymentRecordId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EmploymentRecordId").ToString()) %>" />
                                                <input type="hidden" class="AppId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AppId").ToString()) %>" />
                                                <input type="hidden" class="BorrowerType" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "BorrowerType").ToString()) %>" />
                                                <input type="hidden" class="Key" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Key")) %>" />
                                            </td>
                                            <td>
                                                <span class="Borrower" id="Borrower"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Borrower")) %></span>
                                            </td>
                                            <td>
                                                <span class="Employer" id="Employer"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Employer")) %></span>
                                            </td>
                                            <td>
                                                <span class="IsSelfEmployment" id="IsSelfEmployment"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IsSelfEmployment")) %></span>
                                            </td>
                                            <td>
                                                <span class="IsCurrent" id="IsCurrent"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IsCurrent")) %></span>
                                            </td>
                                            <td>
                                                <span class="EmploymentEndDate" id="EmploymentEndDate"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EmploymentEndDate")) %></span>
                                            </td>
                                            <td>
                                                <span class="YrsOnJob" id="YrsOnJob"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "YrsOnJob")) %></span>
                                            </td>
                                            <td>
                                                <span class="PositionTitle" id="PositionTitle"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PositionTitle")) %></span>
                                            </td>
                                            <td>
                                                <span class="OrderStatus" id="OrderStatus"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "OrderStatus")) %></span>
                                            </td>
                                            <td>
                                                <span class="Type" id="Type"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Type")) %></span>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <div class="PaddingTop align-right">
                            <a id="EmploymentLink">Please enter employment on the Employment page</a>
                        </div>
                    </div>
                    <div class="PickerSection BorrowerPickerSection Hidden">
                        <span class="FieldLabel">Borrower</span>
                        <asp:DropDownList runat="server" ID="BorrowerList"></asp:DropDownList>
                    </div>
                </div>
                <hr />
                <div class="CssTable FullWidthHeight" id="SelfEmploymentTarget">
                    <div class="CssTableRow">
                        <div class="CssTableCell RightBorder BottomBorder Padding5 Width50Pc">
                            <div class="PaddingTopBottom5 Header3">Order Options</div>
                            <table>
                                <tr class="no-wrap">
                                    <td class="FieldLabel">Service Provider</td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="LenderServices"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="NotificationEmailRow" runat="server">
                                    <td class="FieldLabel">Notification Email</td>
                                    <td>
                                        <input type="text" class="InputLarge RequiredInput" id="NotificationEmail" runat="server" />
                                        <img alt="Picker" class="align-middle" id="ContactPicker" src="../../images/contacts.png" />
                                        <img alt="Required" class="RequiredImg align-middle" src="../../images/require_icon_red.gif" />
                                    </td>
                                </tr>
                                <tr id="VerifyIncomeRow" runat="server">
                                    <td colspan="2">
                                        <label class="FieldLabel">
                                            <input type="checkbox" id="VerifyIncome" runat="server" />Verify Income
                                        </label>
                                    </td>
                                </tr>
                                <tr id="CreditCardRow" runat="server">
                                    <td colspan="2">
                                        <label class="FieldLabel">
                                            <input type="checkbox" id="AllowPayWithCreditCard" />Pay with credit card
                                        </label>
                                    </td>
                                </tr>
                                <tr class="UseSpecificEmployerRecordSearchRow">
                                    <td colspan="2">
                                        <input type="checkbox" id="UseSpecificEmployerRecordSearch" />
                                        <label class="FieldLabel" for="UseSpecificEmployerRecordSearch">Use Specific Employer Record Search?</label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="CredentialsRow" class="CssTableCell LeftBorder BottomBorder CredentialItem Padding5 Width50Pc">
                            <div class="PaddingTopBottom5 Header3">Credentials</div>
                            <table>
                                <tr id="AccountIdRow" class="CredentialItem" runat="server">
                                    <td class="FieldLabel">Account ID</td>
                                    <td>
                                        <input type="text" class="InputLarge" id="AccountId" runat="server" />
                                        <img alt="Required" class="RequiredImg Hidden" id="AccountIdImage" runat="server" src="../../images/require_icon_red.gif" />
                                    </td>
                                </tr>
                                <tr class="CredentialItem" id="UsernameRow">
                                    <td class="FieldLabel">Username</td>
                                    <td>
                                        <input type="text" class="InputLarge RequiredInput" id="Username" />
                                        <img alt="Required" id="UsernameImg" class="RequiredImg" src="../../images/require_icon_red.gif" />
                                    </td>
                                </tr>
                                <tr class="CredentialItem" id="PasswordRow">
                                    <td class="FieldLabel">Password</td>
                                    <td>
                                        <input type="password" class="InputLarge RequiredInput" id="Password" />
                                        <img alt="Required" class="RequiredImg" id="PasswordImg" src="../../images/require_icon_red.gif" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="CssTableRow">
                        <div class="CssTableCell TopBorder RightBorder Padding5 Width50Pc" id="BorrowerAuthDocsSection" runat="server">
                            <div class="PaddingTopBottom5 Header3">Borrower Authorization Documents</div>
                            <div>
                                <table id="DocumentTable"></table>
                            </div>
                        </div>
                        <div class="CssTableCell TopBorder Padding5 Width50Pc" id="SalaryKeySection" runat="server">
                            <h3 class="InlineBlock">Salary Key</h3>&nbsp;<a title="A salary key is a single-use, six digit number and is a form of authorization to pull an employment/income verification from The Work Number.">?</a>
                            <div>
                                <table id="SalaryKeyTable"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="align-center">
            <input type="button" id="OrderBtn" value="Place Order"/>
            <img src=<%= AspxTools.SafeUrl(this.VirtualRoot + "/images/warning25x25.png") %> class="WarningIcon align-middle" alt="Warning" />
            <input type="button" id="CancelBtn" value="Cancel" />
        </div>
    </form>
</body>
</html>
