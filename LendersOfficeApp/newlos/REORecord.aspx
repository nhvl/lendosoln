<%@ Page language="c#" Codebehind="REORecord.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.REORecord" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
		<title>Borrower REO</title>
  </HEAD>
	<body class="RightBackground" MS_POSITIONING="FlowLayout">
		<script type="text/javascript">
		    function _init() {
		        parent.parent_initScreen(1);
		        updateCashFlowCalc();
		    }

		function updateCashFlowCalc() {
		    var isSubjectProp = document.getElementById("IsSubjectProp").checked;

		    if (isSubjectProp) {
		        document.getElementById("IsForceCalcNetRentalI").checked = false;
		    }

		    document.getElementById("IsForceCalcNetRentalI").disabled = isSubjectProp;
		}

		function updateCashFlowUIOnly() {
		    if (document.getElementById("IsPrimaryResidence").checked) {
		        document.getElementById("IsForceCalcNetRentalI").checked = false;
		    }
		}
		
		function setSpecialValues(args) {

          parent.updateTotal(args["aReTotVal"], args["aReTotMAmt"], args["aReRentalTotGrossRentI"], args["aReRentalTotMPmt"], args["aReRentalTotHExp"], args["aReRentalTotNetRentI"],
          args["aReRetainedTotGrossRentI"], args["aReRetainedTotMPmt"], args["aReRetainedTotHExp"], args["aReRetainedTotNetRentI"]);  
		}
		
	function getRecordID()
	{
	    return document.getElementById("RecordID").value;
	}
		
	function GoToLiabilities(liabilityID)
	{
	    parent.GoToLiabilities(liabilityID);
	}
	
	function populateLiabilitiesDisplay()
	{
	    var i = 0;
	    var matchedLiabilities = 0;
	    var reachedEnd = false;
	    
	    while(!reachedEnd && matchedLiabilities < 3)
	    {
	       var recordElement = document.getElementById("RecordId" + i);
	       if(recordElement == null){
	            break;}
	            
	       if(document.getElementById("RecordID").value == recordElement.value)
	       {
	           document.getElementById("Link" + matchedLiabilities).href = "javascript:parent.linkMe('BorrowerInfo.aspx?pg=4&RecordID="+document.getElementById("liabilityRecordID" + i).value+"')";
	           document.getElementById("CreditorName" + matchedLiabilities).value =document.getElementById("cNameHidden" + i).value;
	           document.getElementById("Balance" + matchedLiabilities).value = document.getElementById("cBalHidden" + i).value;
	           document.getElementById("Payment" + matchedLiabilities).value = document.getElementById("cPmtHidden" + i).value;
	           
	           document.getElementById("LiabilityRow" + matchedLiabilities).style.display = "table-row";

               matchedLiabilities++;
	       }
	       i++;
	       
	    }
	    
	    for(i = matchedLiabilities; i < 3; i++)
	        document.getElementById("LiabilityRow" + i).style.display = "none";
	    
	    if(matchedLiabilities == 0)
	    {
	        document.getElementById("LiabilityTable").style.display = "none";
	        document.getElementById("LiabilityTableHeader").innerText = "No Linked Liabilities";
	    }
	    else
	    {
	        document.getElementById("LiabilityTable").style.display = "table";
	        document.getElementById("LiabilityTableHeader").innerText = "Linked Liabilities";
	    }
	}
	
	function refreshCalculation() {
	    updateCashFlowCalc();
        invokeMethod("CalculateData", document.getElementById('RecordID').value);
    }
    function updateDirtyBit_callback() { parent.parent_disableSaveBtn(false); }
    
    function updateListIFrame(iIndex) {
     var list_updateRow = retrieveFrameProperty(parent, "ListIFrame", "list_updateRow"); 
      if (list_updateRow) {
        var Addr = <%= AspxTools.JsGetElementById(Addr) %>.value + ", " + <%= AspxTools.JsGetElementById(City) %>.value + ", " + <%= AspxTools.JsGetElementById(State) %>.value + " " + <%= AspxTools.JsGetElementById(Zip) %>.value;
        var IsSubjectProp = <%= AspxTools.JsGetElementById(IsSubjectProp) %>.checked ? "Yes" : "No";
        var Stat = getDropDownValue(<%= AspxTools.JsGetElementById(Stat) %>);
        
        if (Stat == "Sold") Stat = "S";
        else if (Stat == "Pending Sale") Stat = "PS";
        else if (Stat == "Rental") Stat = "R";
        else if (Stat == "Retained") Stat = "";
        
        var NetRentI = <%= AspxTools.JsGetElementById(NetRentI) %>.value;
        list_updateRow(iIndex, document.getElementById("RecordID").value, Addr, IsSubjectProp, Stat, NetRentI);
        
      }
    }
    function getDropDownValue(ddl) {
      return ddl.options[ddl.selectedIndex].innerText;
    }    
    function refreshUI(isFocus) {

        if (document.getElementById("MainEdit").style.display != "none" && isFocus)  
        {
          <%= AspxTools.JsGetElementById(Addr) %>.focus();
        }
        
        var stat = <%= AspxTools.JsGetElementById(Stat) %>.value;
        
        <%= AspxTools.JsGetElementById(NetRentI) %>.readOnly = <%= AspxTools.JsGetElementById(NetRentILckd) %>.checked == false;
        <%= AspxTools.JsGetElementById(OccR) %>.readOnly = stat != "R";
        
        if (stat == "PS" || stat == "") 
        {
            document.getElementById("IsForceCalcNetRentalIPanel").style.display = "";
        } 
        else 
        {
            <%= AspxTools.JsGetElementById(IsForceCalcNetRentalI) %>.checked = false;
            document.getElementById("IsForceCalcNetRentalIPanel").style.display = "none";
        }
        
        populateLiabilitiesDisplay();
        
        

}
function OnCopyFromSubjectProperty()
{
	document.getElementById('Addr').value = document.getElementById('sSpAddr').value;
	document.getElementById('City').value = document.getElementById('sSpCity').value;
	document.getElementById('State').value = document.getElementById('sSpState').value;
	document.getElementById('Zip').value = document.getElementById('sSpZip').value;
	document.getElementById('IsSubjectProp').checked = true;
	updateDirtyBit();
}
function OnCopyFromPresentAddress() {
  document.getElementById('Addr').value = document.getElementById('aBAddr').value;
  document.getElementById('City').value = document.getElementById('aBCity').value;
  document.getElementById('State').value = document.getElementById('aBState').value;
  document.getElementById('Zip').value = document.getElementById('aBZip').value;
  document.getElementById("IsPrimaryResidence").checked = true;
  updateDirtyBit();  
}

function statusDdlChange(){
    var stat = document.getElementById("Stat").value;

    if (stat === "PS" || stat === ""){
        document.getElementById("IsForceCalcNetRentalI").checked = true;
    }
    if (stat === "R"){
        document.getElementById("OccR").value = "75";
    }
}
		</script>
		<form id="REORecord" method="post" runat="server">
		  <div id="MainEdit">
			<table class="FormTable" id="Table3" style="MARGIN-TOP: 0px" cellSpacing="0" cellPadding="0" width="100%" border="0">
  <TR>
    <TD>
      <TABLE id=Table2 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD vAlign=top class=FieldLabel>Ownership held by: <asp:dropdownlist id=ReOwnerT runat="server">
							<asp:ListItem Value="0" Selected="True">Borrower</asp:ListItem>
							<asp:ListItem Value="1">CoBorrower</asp:ListItem>
							<asp:ListItem Value="2">Joint</asp:ListItem>
						</asp:dropdownlist></TD>
          <TD vAlign=top></TD></TR>
        <TR>
          <TD vAlign=top>
            <TABLE id=Table4 cellSpacing=0 cellPadding=0 border=0 class=InsetBorder>
              <tr>
                <td class=FieldLabel colspan=2><input style="WIDTH: 238px; HEIGHT: 21px" type=button value="Copy From Borrower Present Address" onclick="OnCopyFromPresentAddress();"><asp:CheckBox ID=IsPrimaryResidence runat=server Text="Is Primary Residence" onclick="updateCashFlowUIOnly(); refreshCalculation();"/></td></tr>
              <TR>
                <TD class=FieldLabel colSpan=2><INPUT style="WIDTH: 151px; HEIGHT: 21px" onclick=OnCopyFromSubjectProperty(); type=button value="Copy From Subject Property"><asp:checkbox id=IsSubjectProp runat="server" Text="Is Subject Property" onclick="refreshCalculation();"></asp:checkbox></TD></TR>
              <TR>
                <TD class=FieldLabel>Property Address</TD>
                <TD><asp:textbox id=Addr runat="server" Width="292px"></asp:textbox></TD></TR>
              <TR>
                <TD></TD>
                <TD><asp:textbox id=City runat="server" Width="192px"></asp:textbox><ml:statedropdownlist id=State IncludeTerritories="false" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=Zip runat="server" width="50" preset="zipcode" CssClass="mask"></ml:zipcodetextbox></TD></TR></TABLE></TD>
          <TD vAlign=top>
            <TABLE class=InsetBorder id=Table7 cellSpacing=0 cellPadding=0 
            border=0>
              <TR>
                <TD class=FieldLabel noWrap>Type</TD>
                <TD noWrap><asp:dropdownlist id=Type runat="server" Height="15px" /></TD></TR>
              <TR>
                <TD noWrap class=FieldLabel> Status</TD>
                <TD noWrap><asp:dropdownlist id=Stat runat="server" onchange="statusDdlChange(); refreshCalculation();">
										<asp:ListItem Value="S">Sold</asp:ListItem>
										<asp:ListItem Value="PS">Pending Sale</asp:ListItem>
										<asp:ListItem Value="R">Rental</asp:ListItem>
										<asp:ListItem Value="" Selected="True">Retained</asp:ListItem>
									</asp:dropdownlist></TD></TR>
              <TR>
                <TD noWrap class=FieldLabel>Occ. Rate</TD>
                <TD noWrap><asp:TextBox id=OccR runat="server" Width="39px" onchange="refreshCalculation();" MaxLength="3"></asp:TextBox>%</TD></TR>
              <TR id=IsForceCalcNetRentalIPanel>
                <td class=FieldLabel noWrap colSpan=2><asp:CheckBox id=IsForceCalcNetRentalI onclick=refreshCalculation(); runat="server" Text="Calculate Cash Flow"></asp:CheckBox></TD></TR></TABLE></TD>
                <td valign = top>
                <div id="LiabilitiesDisplay" >
                <span ID=LiabilityTableHeader style="font-weight:bold"></span>
                <table class=InsetBorder id = 'LiabilityTable' rules=cols cellspacing = 0 cellPadding = 0 border = 0>
	                <tr><td noWrap class=FieldLabel></td><td noWrap class=FieldLabel>Creditor Name</td><td noWrap class=FieldLabel>Balance</td><td nowrap class=FieldLabel>Payment</td></tr>
                    
                    <tr id="LiabilityRow0"><td noWrap class=FieldLabel><a id="Link0"> view </a></td>
	                <td><asp:TextBox runat="server" ID = "CreditorName0" style='width:200px;'  type='text' readonly='true'/></td>
                    <td><ml:MoneyTextBox runat="server" ID ="Balance0" style='width:100px;' type='text'  readonly='true'/></td> 
                    <td><ml:MoneyTextBox runat="server" ID="Payment0" style='width:100px;' type='text'  readonly='true'/></td></tr> 
                    
                    <tr id="LiabilityRow1"><td noWrap class=FieldLabel><a id="Link1"> view </a></td>
	                <td><asp:TextBox runat="server" ID = "CreditorName1" style='width:200px;'  type='text' readonly='true'/></td>
                    <td><ml:MoneyTextBox runat="server" ID ="Balance1" style='width:100px;' type='text'  readonly='true'/></td> 
                    <td><ml:MoneyTextBox runat="server" ID="Payment1" style='width:100px;' type='text'  readonly='true'/></td></tr> 
                    
                    <tr id="LiabilityRow2"><td noWrap class=FieldLabel><a id="Link2"> view </a></td>
	                <td><asp:TextBox runat="server" ID = "CreditorName2" style='width:200px;'  type='text' readonly='true'/></td>
                    <td><ml:MoneyTextBox runat="server" ID ="Balance2" style='width:100px;' type='text'  readonly='true'/></td> 
                    <td><ml:MoneyTextBox runat="server" ID="Payment2" style='width:100px;' type='text'  readonly='true'/></td></tr> 
                </table>
                </div>
                </td>
                
                </TR></TABLE></TD></TR>
  <TR>
    <TD>
      <TABLE class=InsetBorder id=Table1 cellSpacing=0 cellPadding=0 border=0>
        <tr>
          <td nowrap class="FieldLabel">Market Value</td>
          <td nowrap class="FieldLabel">Mtg Amount</td>
          <td nowrap class="FieldLabel">Gross Rent</td>
          <td nowrap class="FieldLabel">Mtg Payment</td>
          <td nowrap class="FieldLabel">Ins/Maint/Taxes</td>
          <td nowrap class="FieldLabel">Cash Flow</td>
        </tr>
        <tr>
          <td nowrap><ml:MoneyTextBox ID="Val" runat="server" Width="80" preset="money" CssClass="mask" onchange="refreshCalculation();" /></td>
          <td nowrap><ml:MoneyTextBox ID="MAmt" runat="server" Width="80" preset="money" CssClass="mask" onchange="refreshCalculation();" /></td>
          
          <%-- There should be a warning stating that the status should be rental before this can be filled out. Very confusing. --%>
          <%-- Right now it just zeroes out the textbox without saying a word. --%>
          <td nowrap><ml:MoneyTextBox ID="GrossRentI" runat="server" Width="80" preset="money" CssClass="mask" onchange="refreshCalculation();" /></td>
          
          <td nowrap><ml:MoneyTextBox ID="MPmt" runat="server" Width="80" preset="money" CssClass="mask" onchange="refreshCalculation();" /></td>
          <td nowrap><asp:TextBox ID="HExp" onchange="refreshCalculation();" runat="server" Width="80" Style="text-align: right" /></td>
          <td nowrap class="FieldLabel">
            <asp:CheckBox ID="NetRentILckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
            <ml:MoneyTextBox ID="NetRentI" runat="server" Width="80" preset="money" CssClass="mask"  onchange="refreshCalculation();"/>
          </td>
        </tr>
      </table>
    </td>
  </tr>
      </table>
			</div>
		</form>
	</body>
</HTML>
