﻿namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Views;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;
    using LqbGrammar.Exceptions;
    using LendersOffice.Drivers.ConversationLog;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Security.Authorization.ConversationLog;


    /// <summary>
    /// Conversation log service page.
    /// </summary>
    public partial class ConversationLogService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Performs service called from page.
        /// </summary>
        /// <param name="methodName">The method to run.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "PostComment":
                    this.PostComment();
                    break;
                case "ReplyToComment":
                    this.ReplyToComment();
                    break;
                case "HideComment":
                    this.HideComment();
                    break;
                case "ShowComment":
                    this.ShowComment();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Method <" + methodName + "> is not supported.");
            }
        }

        /// <summary>
        /// Posts a comment to the conversation with the specified category. <para/>
        /// Inputs: comment text, category name <para/>
        /// Interpreted inputs: loanid, author, datetime  <para/>
        /// Outputs: comment (id, datetime) <para/>
        /// </summary>
        private void PostComment()
        {
            var resourceString = this.GetString("resourceString");
            var commentText = this.GetString("commentText");
            var categoryId = this.GetLong("categoryId");
            int? permissionLevelId = this.GetInt("permissionLevelId", -1);
            if (permissionLevelId == -1)
            {
                permissionLevelId = null;
            }

            var securityToken = SecurityService.CreateToken();
            var identifier = ResourceIdentifier.Create(resourceString);
            if (identifier == null)
            {
                throw new LqbGrammar.Exceptions.ServerException(ErrorMessage.SystemError);
            }

            var resourceId = new ResourceId()
            {
                Type = ResourceType.LoanFile,
                Identifier = identifier.Value
            };

            var categoryReference = ConversationLogHelper.GetAllCategories(securityToken, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort).Where(c => c.Identity.Id == categoryId).First().Identity;

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            var comment = new Comment()
            {
                Commenter = PersonName.CreateWithReplace(principal.FirstName, principal.LastName).Value,
                CommenterId = UserAccountIdentifier.Create(principal.UserId.ToString()).Value,
                Value = CommentLogEntry.Create(commentText).Value,
                Identity = new CommentReference()
                // the below properties get set on Post:
                // Created = LqbEventDate.Create(DateTime.Now.ToString()).Value
                // Depth
                // Identity's Id.                 
            };

            try
            {
                ConversationLogHelper.Post(securityToken, resourceId, categoryReference, comment, permissionLevelId, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
                this.SetResult("newCommentId", comment.Identity.Id); // so we can scroll to this.
            }
            catch (LqbException e)
            {
                this.SetResult("alertMessage", e.Message);
            }

            SendUpReInitializationModel(securityToken, resourceId);
        }

        private void SendUpReInitializationModel(SecurityToken securityToken, ResourceId resourceId)
        {
            if (securityToken == null)
            {
                throw new ArgumentNullException(nameof(securityToken));
            }

            if (resourceId == null)
            {
                throw new ArgumentNullException(nameof(resourceId));
            }

            if (!SecurityToken.IsValid(securityToken))
            {
                throw new ArgumentException(nameof(securityToken) + " is not valid.");
            }

            if (resourceId.Identifier == ResourceIdentifier.BadIdentifier)
            {
                throw new ArgumentException(nameof(resourceId) + " has bad identifier.");
            }
             
            var excludeConversations = this.GetBool("excludeConversations", false);

            // Sending back everything.  One, it's easier.  Two, include any new comments/replies.     
            if (excludeConversations)
            {
                this.SendUpCategories(securityToken, new List<ViewOfConversationForLoUser>());
            }
            else
            {
                var conversations = ConversationLogHelper.GetAllConversations(securityToken, resourceId, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
                var viewOfConversations = conversations.Select(c => new ViewOfConversationForLoUser(c, Tools.PacificStandardVariantOffset)).ToList();
                this.SetResult("newConversationsJson", SerializationHelper.JsonNetSerialize(viewOfConversations));
                this.SendUpCategories(securityToken, viewOfConversations);
            }
            
            this.SetResult("enableConversationLogPermissions", ConstStage.EnableConversationLogPermissions);

            if (ConstStage.EnableConversationLogPermissions)
            {
                var permissionLevels = ConversationLogPermissionLevel.GetOrderedPermissionLevelsForBrokerIncludingDefault(securityToken.BrokerId);
                var viewOfPermissionLevels = permissionLevels.Select(p => ViewOfPermissionLevelForLoUser.FromPermissionLevel(securityToken, p)).ToList();
                this.SetResult("newPermissionLevels", SerializationHelper.JsonNetSerialize(viewOfPermissionLevels));
            }
        }

        /// <summary>
        /// Sends ths relevant categories back to the UI in json serialized form.
        /// </summary>
        /// <param name="securityToken">The current security token.</param>
        /// <param name="viewOfConversations">The view of conversations the user has.</param>
        private void SendUpCategories(SecurityToken securityToken, List<ViewOfConversationForLoUser> viewOfConversations)
        {
            var allCategories = ConversationLogHelper.GetAllCategories(securityToken, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);

            var usedCategoryIds = new HashSet<long>(viewOfConversations.Select(v => v.CategoryId).Distinct());
            var viewOfCategories = allCategories
                .Where(c => c.IsActive || usedCategoryIds.Contains(c.Identity.Id))
                .Select(c => new ViewOfCategoryForLoUser(c))
                .ToList();

            this.SetResult("updatedCategoriesJson", SerializationHelper.JsonNetSerialize(viewOfCategories));
        }

        /// <summary>
        /// Stores the reply as a reply to the specified comment. <para/>
        /// Inputs: reply text, id of the parent comment <para/>
        /// Interpreted inputs: author, datetime  <para/>
        /// Outputs: comment (id, datetime) <para/>
        /// </summary>
        private void ReplyToComment()
        {
            var replyText = this.GetString("replyText");
            var parentCommentId = this.GetLong("parentCommentId");
            var resourceString = this.GetString("resourceString");

            var securityToken = SecurityService.CreateToken();
            var identifier = ResourceIdentifier.Create(resourceString);
            if (identifier == null)
            {
                throw new LqbGrammar.Exceptions.ServerException(ErrorMessage.SystemError);
            }

            var parentCommentReference = new CommentReference()
            {
                Id = parentCommentId
            };

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            var reply = new Comment()
            {
                Commenter = PersonName.CreateWithReplace(principal.FirstName, principal.LastName).Value,
                CommenterId = UserAccountIdentifier.Create(principal.UserId.ToString()).Value,
                Value = CommentLogEntry.Create(replyText).Value,
                Identity = new CommentReference()
                // the below properties get set on Reply:
                // Created = LqbEventDate.Create(DateTime.Now.ToString()).Value
                // Depth
                // Identity's Id.
            };

            try
            {
                ConversationLogHelper.Reply(securityToken, parentCommentReference, reply, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
                this.SetResult("newReplyId", reply.Identity.Id); // so we can scroll to this.
            }
            catch (LqbException e)
            {
                this.SetResult("alertMessage", e.Message);
            }

            var resourceId = new ResourceId()
            {
                Type = ResourceType.LoanFile,
                Identifier = identifier.Value
            };

            SendUpReInitializationModel(securityToken, resourceId);
        }

        /// <summary>
        /// Hides the specified comment. <para/>
        /// Inputs: id of the comment <para/>
        /// Interpreted inputs: author <para/>
        /// Outputs: success or not. <para/>
        /// </summary>
        private void HideComment()
        {
            HideOrShowComment(hide: true);
        }

        /// <summary>
        /// Unhides the specified comment. <para/>
        /// Inputs: id of the comment <para/>
        /// Interpreted inputs: author <para/>
        /// Outputs: success or not. <para/>
        /// </summary>
        private void ShowComment()
        {
            HideOrShowComment(hide: false);
        }

        /// <summary>
        /// Hides or shows the comment depending on the boolean passed in.
        /// </summary>
        /// <param name="hide">If true, hides the comment.  If false, shows the comment.</param>
        private void HideOrShowComment(bool hide)
        {
            var commentId = this.GetLong("commentId");
            var resourceString = this.GetString("resourceString");

            var securityToken = SecurityService.CreateToken();
            var identifier = ResourceIdentifier.Create(resourceString);
            if (identifier == null)
            {
                throw new LqbGrammar.Exceptions.ServerException(ErrorMessage.SystemError);
            }

            var commentReference = new CommentReference()
            {
                Id = commentId
            };

            try
            {
                bool success = false;
                if (hide)
                {
                    success = ConversationLogHelper.HideComment(securityToken, commentReference, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
                }
                else
                {
                    success = ConversationLogHelper.ShowComment(securityToken, commentReference, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);
                }

                if (success == false)
                {
                    this.SetResult("alertMessage", "Failed to " + (hide ? "hide" : "show") + "  comment.  Please try again.");
                }
            }
            catch (LqbException e)
            {
                this.SetResult("alertMessage", e.Message);
            }


            var resourceId = new ResourceId()
            {
                Type = ResourceType.LoanFile,
                Identifier = identifier.Value
            };

            SendUpReInitializationModel(securityToken, resourceId);
        }
    }
}