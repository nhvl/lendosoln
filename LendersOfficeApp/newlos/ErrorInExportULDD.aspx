﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorInExportULDD.aspx.cs" Inherits="LendersOfficeApp.newlos.ErrorInExportingULDD" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">    
    <form id="FloodCertification" runat="server">
    		<table cellSpacing="0" cellPadding="0">
			<tr>
				<td class="MainRightHeader" noWrap>
				    Errors
				</td>				
		    </tr>	
		    <%for(int i=0; i<errors.Length; i++) { %>
		    <tr>
		        <td>
		            <table class="InsetBorder FieldLabel" cellSpacing="0" cellPadding="0" width="99%" border="0">
                        <tr>
                            <td style='font-size:12px;color:red'>Error: </td>
                            <td>
                                <% Response.Write(showErrorMessage(errors[i])); %>
                            </td>
                        </tr>
                        <tr>                                                
                            <td style='font-size:12px;color:red'>Page: </td>
                            <td>
                                <% Response.Write(showPage(errors[i])); %>
                            </td>
                            
                        </tr>                                 
                    </table>
		        </td>
		    </tr>
		    <%} %>	 
		    <tr>
		        <td align ="center">
		            <input type="button" value="Close" onclick="onClosePopup()" />  
		        </td>
		    </tr>   
		    </table>
		    
    </form>
</body>
</html>
