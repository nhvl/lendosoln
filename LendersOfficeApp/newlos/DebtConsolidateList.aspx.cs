using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.newlos
{
	public partial class DebtConsolidateList : LendersOfficeApp.newlos.BaseLoanPage
	{
        private string[] m_idList = null;
        private int m_currentIndex = 0;

        protected void PageLoad(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.DisplayCopyRight = false;

            this.RegisterService("loanedit", "/newlos/DebtConsolidateListService.aspx");
            this.RegisterJsScript("LQBPrintFix.js");
            this.RegisterJsScript("LQBPopup.js");	
            this.EnableJquery = true;	
        }

        protected override void LoadData() 
        {

            CPageData dataLoan = new CLiaData(LoanID, false);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            ILiaCollection recordList = dataApp.aLiaCollection;
            m_idList = new string[recordList.SortedView.Count];

            m_dg.DataSource = recordList.SortedView;
            m_dg.DataBind();
            
            ClientScript.RegisterArrayDeclaration("g_aIds", string.Join(",", m_idList));


        }

        protected string DisplayOwnerType(string type) 
        {
            try
            {
                return CLiaFields.DisplayStringOfOwnerT( (E_LiaOwnerT) int.Parse( type ) );
            }
            catch
            {	return "B"; }// Make unknown type and default become borrower.
        }

        protected string DisplayLiabilityType(string type) 
        {
            string str = "";
            try 
            {
                E_DebtRegularT debtType = (E_DebtRegularT) int.Parse(type);
                return CLiaRegular.DisplayStringOfDebtT( debtType );
            } 
            catch {}
            return str;
        }
        protected string DisplayMoneyString(string value) 
        {
            if (value == null || value == "") 
            {
                return "";
            }
            try 
            {
                value = value.Replace("*", "");
                return decimal.Parse(value).ToString("C");
            } 
            catch {}
            // Liability value import from credit not always return correct format.
            return value;
        }

        protected HtmlInputCheckBox DisplayPayoffCheckbox(object o) 
        {
            DataRowView row = o as DataRowView;
            if (null == row) 
            {
                Tools.LogBug("Liability record is not DataRowView.");
                return null;
            }

            bool willBePdOff = GetSafeValue(row, "WillBePdOff") == "True";

            var checkbox = new HtmlInputCheckBox();
            checkbox.ID = $"willBePdOff_{m_currentIndex}";
            checkbox.Name = checkbox.ID;
            checkbox.Checked = willBePdOff;
            checkbox.Attributes.Add("onclick", $"f_onPaidOffCheck({m_currentIndex});");

            return checkbox;
        }
        protected HtmlControl DisplayUsedInRatioCheckbox(object o) 
        {
            DataRowView row = o as DataRowView;
            if (null == row) 
            {
                Tools.LogBug("Liability record is not DataRowView.");
                return null;
            }

            E_DebtT debtT = E_DebtT.Other;

            try 
            {
                string t = GetSafeValue(row, "DebtT");
                if (t != "") 
                {
                    debtT = (E_DebtT) int.Parse(t);
                }
            } 
            catch {}

            if (debtT == E_DebtT.Mortgage) 
            {
                var control = new HtmlGenericControl("span");
                control.InnerText = "See REO";
                return control;

            }
            else 
            {
                bool usedInRatio = GetSafeValue(row, "NotUsedInRatio") != "True";
                
                var checkbox = new HtmlInputCheckBox();
                checkbox.ID = $"usedInRatio_{m_currentIndex}";
                checkbox.Name = checkbox.ID;
                checkbox.Checked = usedInRatio;
                checkbox.Attributes.Add("onclick", "f_refreshCalculation();");

                return checkbox;
            }
        }


        protected string GetSafeValue(DataRowView row, string columnName) 
        {
            object v = row[columnName];
            if (null == v)
                return "";
            else
                return v.ToString();
        }
        private void m_dg_ItemDataBound(object sender, DataGridItemEventArgs e) 
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) 
            {
                m_idList[m_currentIndex++] = "'" + DataBinder.Eval(e.Item.DataItem, "RecordId").ToString() + "'";
            }
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
            
            m_dg.ItemDataBound += new DataGridItemEventHandler(this.m_dg_ItemDataBound);

        }
		#endregion
	}
}
