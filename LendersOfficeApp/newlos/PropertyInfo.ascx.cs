using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.los.admin;
using System.Collections;

namespace LendersOfficeApp.newlos
{

	public partial  class PropertyInfoUserControl : LendersOfficeApp.newlos.BaseLoanUserControl, IAutoLoadUserControl
	{
        public void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(PropertyInfoUserControl));
            dataLoan.InitLoad();
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState; 
            sSpZip.Text = dataLoan.sSpZip.TrimWhitespaceAndBOM();
			// 01-14-08 av 18913 
			if ( dataLoan.sSpState.Length != 0 )   
			{
				ArrayList list = StateInfo.Instance.GetCountiesFor(dataLoan.sSpState);  
				list.Insert(0, String.Empty ); 
				sSpCounty.DataSource = list;
				sSpCounty.DataBind();
			}


			Tools.Bind_sSpCounty( dataLoan.sSpState, sSpCounty, true );
			Tools.SetDropDownListCaseInsensitive( sSpCounty, dataLoan.sSpCounty );

            Tools.Bind_sGseSpT(sGseSpT);
            Tools.SetDropDownListValue(this.sHomeIsMhAdvantageTri, dataLoan.sHomeIsMhAdvantageTri);

            sSpLegalDesc.Text = dataLoan.sSpLegalDesc;
            sSpAddr.Text = dataLoan.sSpAddr;
            sUnitsNum.Text = dataLoan.sUnitsNum_rep;
            sYrBuilt.Text = dataLoan.sYrBuilt.TrimWhitespaceAndBOM();

            Tools.SetDropDownListValue(sGseSpT, dataLoan.sGseSpT);
            sIsNewConstruction.Checked = dataLoan.sIsNewConstruction;
            sSpIsMixedUse.Checked = dataLoan.sSpIsMixedUse;
            sSpIsInPud.Checked = dataLoan.sSpIsInPud;

            sSubjPropertyMineralAbbrLegalDesc.Text = dataLoan.sSubjPropertyMineralAbbrLegalDesc;

            sProjNm.Text = dataLoan.sProjNm;
            sNfipFloodZoneId.Text = dataLoan.sNfipFloodZoneId;
            sAssessorsParcelId.Text = dataLoan.sAssessorsParcelId;
            sHasMultipleParcels.Checked = dataLoan.sHasMultipleParcels;

            sFloodCertificationIsInSpecialArea.SelectedValue = dataLoan.sFloodCertificationIsInSpecialArea.ToString();
        }

        public void SaveData() 
        {
        }
		protected void PageInit(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            sSpZip.SmartZipcode(sSpCity, sSpState, sSpCounty);
			// 01-14-08 av 18913
			sSpState.Attributes.Add( "onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event);" );
            Tools.Bind_TriState(this.sHomeIsMhAdvantageTri);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
