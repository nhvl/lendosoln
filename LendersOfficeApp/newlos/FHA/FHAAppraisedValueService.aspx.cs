using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
namespace LendersOfficeApp.newlos.FHA
{
    public class FHAAppraisedValueServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHAAppraisedValueServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sFHALenderIdCode = GetString("sFHALenderIdCode") ;
            dataLoan.sFHASponsorAgentIdCode = GetString("sFHASponsorAgentIdCode") ;
            dataLoan.sFHACondCommUnderNationalHousingAct = GetBool("sFHACondCommUnderNationalHousingAct") ;
            //dataLoan.sFHACondCommNationalHousingActSection = GetString("sFHACondCommNationalHousingActSection") ; // opm 175879
            dataLoan.sFHAHousingActSection = GetString("sFHAHousingActSection"); // opm 175879
            dataLoan.sFHACondCommSeeBelow = GetBool("sFHACondCommSeeBelow") ;
            dataLoan.sFHACondCommInstCaseRef = GetString("sFHACondCommInstCaseRef") ;
            dataLoan.sApprVal_rep = GetString("sApprVal") ;
            dataLoan.sFHAConstructionT = (E_sFHAConstructionT) GetInt("sFHAConstructionT");
            dataLoan.sSpAddr = GetString("sSpAddr") ;
            dataLoan.sSpCity = GetString("sSpCity") ;
            dataLoan.sSpState = GetString("sSpState") ;
            dataLoan.sSpZip = GetString("sSpZip") ;
            dataLoan.sSpCounty = GetString("sSpCounty") ;
            dataLoan.sFHACondCommIssuedD_rep = GetString("sFHACondCommIssuedD") ;
            dataLoan.sFHACondCommExpiredD_rep = GetString("sFHACondCommExpiredD") ;
            dataLoan.sFHACondCommImprovedLivingArea = GetString("sFHACondCommImprovedLivingArea") ;
            dataLoan.sProHoAssocDues_rep = GetString("sProHoAssocDues") ;
            dataLoan.sProOHExp_rep = GetString("sProOHExp") ;
            dataLoan.sProOHExpLckd = GetBool("sProOHExpLckd");
            dataLoan.sFHACondCommRemainEconLife = GetString("sFHACondCommRemainEconLife") ;

            dataLoan.sFHACondCommMaxFinanceEligibleTri = GetTriState("sFHACondCommMaxFinanceEligibleTri");
            dataLoan.sFHACondCommIsManufacturedHousing = GetBool("sFHACondCommIsManufacturedHousing") ;
            dataLoan.sFHACondCommIsAssuranceOfCompletion = GetBool("sFHACondCommIsAssuranceOfCompletion") ;
            dataLoan.sFHACondCommAssuranceOfCompletionAmt_rep = GetString("sFHACondCommAssuranceOfCompletionAmt") ;
            dataLoan.sFHACondCommSeeAttached = GetBool("sFHACondCommSeeAttached") ;
            dataLoan.sFHACondCommSeeAttachedDesc = GetString("sFHACondCommSeeAttachedDesc") ;
            dataLoan.sFHACondCommSeeFollowingCondsOnBack = GetBool("sFHACondCommSeeFollowingCondsOnBack") ;
            dataLoan.sFHACondCommCondsOnBack2 = GetString("sFHACondCommCondsOnBack2") ;
            dataLoan.sFHACondCommCondsOnBack3 = GetString("sFHACondCommCondsOnBack3") ;
            dataLoan.sFHACondCommCondsOnBack4 = GetString("sFHACondCommCondsOnBack4") ;
            dataLoan.sFHACondCommCondsOnBack5 = GetString("sFHACondCommCondsOnBack5") ;
            dataLoan.sFHACondCommCondsOnBack6 = GetString("sFHACondCommCondsOnBack6") ;
            dataLoan.sFHACondCommCondsOnBack7 = GetString("sFHACondCommCondsOnBack7") ;
            dataLoan.sFHACondCommCondsOnBack1 = GetString("sFHACondCommCondsOnBack1") ;
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum") ;

            if (!dataLoan.sHazardExpenseInDisbursementMode)
            {
                dataLoan.sProHazInsT = (E_PercentBaseT)GetInt("sProHazInsT");
                dataLoan.sProHazInsR_rep = GetString("sProHazInsR");
                dataLoan.sProHazInsMb_rep = GetString("sProHazInsMb");
            }
            if (!dataLoan.sRealEstateTaxExpenseInDisbMode)
            {
                dataLoan.sProRealETxT = (E_PercentBaseT)GetInt("sProRealETxT");
                dataLoan.sProRealETxR_rep = GetString("sProRealETxR");
                dataLoan.sProRealETxMb_rep = GetString("sProRealETxMb");
            }

            IPreparerFields field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACondCommitMortgagee, E_ReturnOptionIfNotExist.CreateNew);
            field.CompanyName = GetString("FHACondCommitMortgageeCompanyName") ;
            field.StreetAddr = GetString("FHACondCommitMortgageeStreetAddr") ;
            field.City = GetString("FHACondCommitMortgageeCity") ;
            field.State = GetString("FHACondCommitMortgageeState") ;
            field.Zip = GetString("FHACondCommitMortgageeZip") ;
            field.Update();

            field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAConditionalCommitment, E_ReturnOptionIfNotExist.CreateNew);
            field.PreparerName = GetString("FHAConditionalCommitmentPreparerName") ;
            field.PrepareDate_rep = GetString("FHAConditionalCommitmentNamePrepareDate") ;
            field.Update();

        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sFHALenderIdCode", dataLoan.sFHALenderIdCode);
            SetResult("sFHASponsorAgentIdCode", dataLoan.sFHASponsorAgentIdCode);
            SetResult("sFHACondCommUnderNationalHousingAct", dataLoan.sFHACondCommUnderNationalHousingAct);
            //SetResult("sFHACondCommNationalHousingActSection", dataLoan.sFHACondCommNationalHousingActSection); // opm 175879
            SetResult("sFHAHousingActSection", dataLoan.sFHAHousingActSection); // opm 175879
            SetResult("sFHACondCommSeeBelow", dataLoan.sFHACondCommSeeBelow);
            SetResult("sFHACondCommInstCaseRef", dataLoan.sFHACondCommInstCaseRef);
            SetResult("sApprVal", dataLoan.sApprVal_rep);
            SetResult("sFHAConstructionT", dataLoan.sFHAConstructionT);
            SetResult("sProHazIns", dataLoan.sProHazIns_rep);
            SetResult("sProRealETx", dataLoan.sProRealETx_rep);
            SetResult("sProOHExpLckd", dataLoan.sProOHExpLckd);
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);
            SetResult("sSpCounty", dataLoan.sSpCounty);
            SetResult("sFHACondCommIssuedD", dataLoan.sFHACondCommIssuedD_rep);
            SetResult("sFHACondCommExpiredD", dataLoan.sFHACondCommExpiredD_rep);
            SetResult("sFHACondCommImprovedLivingArea", dataLoan.sFHACondCommImprovedLivingArea);
            SetResult("sProHoAssocDues", dataLoan.sProHoAssocDues_rep);
            SetResult("sProOHExp", dataLoan.sProOHExp_rep);
            SetResult("sFHACondCommMonExpenseEstimate", dataLoan.sFHACondCommMonExpenseEstimate_rep);
            SetResult("sFHACondCommRemainEconLife", dataLoan.sFHACondCommRemainEconLife);
            SetResult("sFHACondCommMaxFinanceEligibleTri", dataLoan.sFHACondCommMaxFinanceEligibleTri);
            SetResult("sFHACondCommIsManufacturedHousing", dataLoan.sFHACondCommIsManufacturedHousing);
            SetResult("sFHACondCommIsAssuranceOfCompletion", dataLoan.sFHACondCommIsAssuranceOfCompletion);
            SetResult("sFHACondCommAssuranceOfCompletionAmt", dataLoan.sFHACondCommAssuranceOfCompletionAmt_rep);
            SetResult("sFHACondCommSeeAttached", dataLoan.sFHACondCommSeeAttached);
            SetResult("sFHACondCommSeeAttachedDesc", dataLoan.sFHACondCommSeeAttachedDesc);
            SetResult("sFHACondCommSeeFollowingCondsOnBack", dataLoan.sFHACondCommSeeFollowingCondsOnBack);
            SetResult("sFHACondCommCondsOnBack2", dataLoan.sFHACondCommCondsOnBack2);
            SetResult("sFHACondCommCondsOnBack3", dataLoan.sFHACondCommCondsOnBack3);
            SetResult("sFHACondCommCondsOnBack4", dataLoan.sFHACondCommCondsOnBack4);
            SetResult("sFHACondCommCondsOnBack5", dataLoan.sFHACondCommCondsOnBack5);
            SetResult("sFHACondCommCondsOnBack6", dataLoan.sFHACondCommCondsOnBack6);
            SetResult("sFHACondCommCondsOnBack7", dataLoan.sFHACondCommCondsOnBack7);
            SetResult("sFHACondCommCondsOnBack1", dataLoan.sFHACondCommCondsOnBack1);
            SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);


            SetResult("sProHazInsT", dataLoan.sProHazInsT);
            SetResult("sProHazInsR", dataLoan.sProHazInsR_rep);
            SetResult("sProHazInsMb", dataLoan.sProHazInsMb_rep);
            SetResult("sProRealETxT", dataLoan.sProRealETxT);
            SetResult("sProRealETxR", dataLoan.sProRealETxR_rep);
            SetResult("sProRealETxMb", dataLoan.sProRealETxMb_rep);


            IPreparerFields field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACondCommitMortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("FHACondCommitMortgageeCompanyName", field.CompanyName);
            SetResult("FHACondCommitMortgageeStreetAddr", field.StreetAddr);
            SetResult("FHACondCommitMortgageeCity", field.City);
            SetResult("FHACondCommitMortgageeState", field.State);
            SetResult("FHACondCommitMortgageeZip", field.Zip);

            field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAConditionalCommitment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("FHAConditionalCommitmentPreparerName", field.PreparerName);
            SetResult("FHAConditionalCommitmentNamePrepareDate", field.PrepareDate_rep);
        }
    }
	public partial class FHAAppraisedValueService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FHAAppraisedValueServiceItem());
        }
	}
}
