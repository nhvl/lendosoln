using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
    public class FHAAnalysisAppraisedValueServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHAAnalysisAppraisedValueServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sFHAApprNotFairExplanation          = GetString("sFHAApprNotFairExplanation");
            dataLoan.sFHAApprQualityComment              = GetString("sFHAApprQualityComment");
            dataLoan.sFHAApprAreComparablesAcceptableTri = GetTriState("sFHAApprAreComparablesAcceptableTri");
            dataLoan.sFHAApprComparablesComment          = GetString("sFHAApprComparablesComment");
            dataLoan.sFHAApprAdjAcceptableTri            = GetTriState("sFHAApprAdjAcceptableTri");
            dataLoan.sFHAApprAdjNotAcceptableExplanation = GetString("sFHAApprAdjNotAcceptableExplanation");
            dataLoan.sFHAApprIsValAcceptableTri          = GetTriState("sFHAApprIsValAcceptableTri");
            dataLoan.sFHAApprValNeedCorrectionTri        = GetTriState("sFHAApprValNeedCorrectionTri");
            dataLoan.sFHAApprCorrectedVal_rep            = GetString("sFHAApprCorrectedVal");
            dataLoan.sFHAApprValCorrectionJustification  = GetString("sFHAApprValCorrectionJustification");
            dataLoan.sFHAApprRepairConditions            = GetString("sFHAApprRepairConditions");
            dataLoan.sFHAApprOtherComments               = GetString("sFHAApprOtherComments");
            dataLoan.sFHAApprIsFairTri                   = GetTriState("sFHAApprIsFairTri");

            IPreparerFields appraisal = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAnalysisAppraisalAppraiser, E_ReturnOptionIfNotExist.CreateNew);
            appraisal.PreparerName = GetString("AnalysisAppraisalAppraiserPreparerName");
            appraisal.Update();

            dataLoan.sFHAApprUnderwriterChumsIdLckd = GetBool("sFHAApprUnderwriterChumsIdLckd");
            dataLoan.sFHAApprUnderwriterNameLckd = GetBool("sFHAApprUnderwriterNameLckd");
            dataLoan.sFHAApprUnderwriterName = GetString("sFHAApprUnderwriterName");
            dataLoan.sFHAApprUnderwriterChumsId = GetString("sFHAApprUnderwriterChumsId");
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
        }
    }
	public partial class FHAAnalysisAppraisedValueService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FHAAnalysisAppraisedValueServiceItem());
        }
	}
}
