<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="FHACreditAnalysisRefinanceCombine.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHACreditAnalysisRefinanceCombine" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>FHACreditAnalysisRefinanceCombine</title>
  </HEAD>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<script type="text/javascript">

    function _init() {
      enableBorrowerInfo();
      enableMonthIncome();
      enableMonthlyDebt();
      lockField(<%= AspxTools.JsGetElementById(sFfUfmip1003Lckd) %>, 'sFfUfmip1003');
      lockField(<%= AspxTools.JsGetElementById(sFHAMBasisRefinMultiplyLckd) %>, 'sFHAMBasisRefinMultiply');
      lockField(<%= AspxTools.JsGetElementById(sFHAApprValMultiplyLckd) %>, 'sFHAApprValMultiply');
      lockField(<%= AspxTools.JsGetElementById(sFHANonrealtyLckd) %>, 'sFHANonrealty');
      lockField(<%= AspxTools.JsGetElementById(sFfUfmip1003Lckd) %>, 'sFfUfmip1003');
      lockField(<%= AspxTools.JsGetElementById(sFHAProMInsLckd) %>, 'sFHAProMIns');
      <% if (IsReadOnly) { %>
        document.getElementById("btnCopyFromLoanApp").disabled = true;
        document.getElementById("btnCopyFromGFE").disabled = true;
      <% } %>

    }

    function enableBorrowerInfo() {
      var b = <%= AspxTools.JsGetElementById(sCombinedBorInfoLckd) %>.checked;
      <%= AspxTools.JsGetElementById(sCombinedBorFirstNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorMidNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorLastNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorSuffix) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorSsn) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborFirstNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborMidNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborLastNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborSuffix) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborSsn) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborUsing2ndAppBorr) %>.disabled = b;
    }
    function enableMonthIncome() {
      var b = <%= AspxTools.JsGetElementById(sFHAIncomeLckd) %>.checked;
      <%= AspxTools.JsGetElementById(sFHABBaseI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHABOI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHACBaseI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHACOI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHANetRentalI) %>.readOnly = !b;
    }
    function enableMonthlyDebt() {
      var b = <%= AspxTools.JsGetElementById(sFHADebtLckd) %>.checked;
      <%= AspxTools.JsGetElementById(sFHADebtInstallPmt) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHADebtInstallBal) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHAChildSupportPmt) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHAOtherDebtPmt) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHAOtherDebtBal) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHANegCfRentalI) %>.readOnly = !b;
    }
    function handlingError(result)
    {
      var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
      if (result.ErrorType === 'VersionMismatchException')
      {
        f_displayVersionMismatch();
      }
      else if (result.ErrorType === 'LoanFieldWritePermissionDenied')
      {
        f_displayFieldWriteDenied(result.UserMessage);
      }
      else
      {
        alert(errMsg);
      }

    }

    function __executeCalculation(methodName)
    {
      PolyShouldShowConfirmSave(isDirty(), function(confirmed){
        var args = getAllFormValues();
        args["loanid"] = ML.sLId;
        args["applicationid"] = ML.aAppId;
        args["issave"] =  confirmed ? 'T' : 'F';
        var result = gService.loanedit.call(methodName, args);
        if (!result.error) {
          populateForm(result.value); // Refresh value;
          _init();
          clearDirty();
        } else {
          handlingError(result);
        }
      });
      
    }
    function copyFromGFE() {
      __executeCalculation('CopyFromGFE');
    }

    function copyFromLoanApp() {
      __executeCalculation('CopyFromLoanApp');
    }
    function validateAR(control, event) {
      updateDirtyBit(event);
      if (event != null) {
        if (event.keyCode == 65) control.value = 'A';
        else if (event.keyCode == 82) control.value = 'R';
        else if (event.keyCode < 127 && event.keyCode > 31) control.value = '';
      } else {
        var value = '';
        if (control.value.length > 0) value = control.value.charAt(0);
        if (value == 'a' || value == 'A') control.value = 'A';
        else if (value == 'r' || value == 'R') control.value = 'A';
        else control.value = '';
      }
    }

		</script>

<form id=FHACreditAnalysisRefinanceCombine method=post runat="server">
<table class=FormTable id=Table8 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class=MainRightHeader>FHA MCAW - Combined
      Refinance (HUD-92900-WS)</td></tr>
  <tr>
    <td><input onclick=copyFromLoanApp(); type=button value="Copy Data From Loan App" id=btnCopyFromLoanApp><input onclick=copyFromGFE(); type=button value="Copy Closing Cost Data to Form" id=btnCopyFromGFE></td></tr>
  <tr>
    <td>
      <table class=InsetBorder cellSpacing=0 cellPadding=0 width="98%" border=0
      >
        <tr>
          <td>
            <table id=Table9 cellSpacing=0 cellPadding=0 border=0
            >
              <tr>
                <td class=FieldLabel>Loan Type</td>
                <td><asp:dropdownlist id=sLT runat="server"></asp:dropdownlist></td>
                <td class=FieldLabel noWrap></td>
                <td></td>
                <td class=FieldLabel></td>
                <td></td></tr>
              <tr>
                <td class=FieldLabel>Case Number</td>
                <td><asp:textbox id=sAgencyCaseNum runat="server" width="108px"></asp:textbox></td>
                <td class=FieldLabel noWrap>Housing
Act</td>
                <td><ml:combobox id=sFHAHousingActSection runat="server" width="70px"></ml:combobox></td>
                <td class=FieldLabel>Construction</td>
                <td><asp:dropdownlist id=sFHAConstructionT runat="server" >
									</asp:dropdownlist></td></tr>
              <tr>
                <td class=FieldLabel noWrap>CHUMS ID
                  Number</td>
                <td><asp:textbox id=FHACreditAnalysisRefinanceLicenseNumOfAgent runat="server" width="108px"></asp:textbox></td>
                <td class=FieldLabel>Underwriter Name</td>
                <td colSpan=3><asp:textbox id=FHACreditAnalysisRefinancePreparerName runat="server"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Refinance
                Type</td>
                <td colSpan=2><ml:combobox id=sFHARefinanceTypeDesc runat="server" width="192px" text="">
									</ml:combobox></td>
                <td colSpan=3
        ></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table6 cellSpacing=0 cellPadding=0 width="98%"
      border=0>
        <tr>
          <td noWrap>
            <table id=Table5 cellSpacing=0 cellPadding=0 border=0
            >
              <tr>
                <td class=FieldLabel noWrap colSpan=2
                  >Borrower Information&nbsp;<asp:CheckBox id=sCombinedBorInfoLckd runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox></td>
                <td noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td class=FieldLabel noWrap colSpan=2
                  >Coborrower Information</td></tr>
              <tr>
                <td class=FieldLabel noWrap colSpan=2></td>
                <td noWrap></td>
                <td class=FieldLabel noWrap colSpan=2
                  ><asp:CheckBox id=sCombinedCoborUsing2ndAppBorr runat="server" Text="Show 2nd Borrower info" onclick="refreshCalculation();"></asp:CheckBox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>First Name</td>
                <td noWrap><asp:textbox id=sCombinedBorFirstNm runat="server" maxlength="21" tabindex=5></asp:textbox></td>
                <td noWrap>&nbsp;&nbsp;</td>
                <td class=FieldLabel noWrap>First Name</td>
                <td noWrap><asp:textbox id=sCombinedCoborFirstNm runat="server" maxlength="21" tabindex=10></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Middle
Name</td>
                <td noWrap><asp:textbox id=sCombinedBorMidNm runat="server" maxlength="21" tabindex=5></asp:textbox></td>
                <td noWrap></td>
                <td class=FieldLabel noWrap>Middle
Name</td>
                <td noWrap><asp:textbox id=sCombinedCoborMidNm runat="server" maxlength="21" tabindex=10></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Last Name</td>
                <td noWrap><asp:textbox id=sCombinedBorLastNm runat="server" maxlength="21" tabindex=5></asp:textbox></td>
                <td noWrap></td>
                <td class=FieldLabel noWrap>Last Name</td>
                <td noWrap><asp:textbox id=sCombinedCoborLastNm runat="server" maxlength="21" tabindex=10></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>Suffix</td>
                <td noWrap><asp:textbox id=sCombinedBorSuffix runat="server" maxlength="21" tabindex=5></asp:textbox></td>
                <td noWrap></td>
                <td class=FieldLabel noWrap>Suffix</td>
                <td noWrap><asp:textbox id=sCombinedCoborSuffix runat="server" maxlength="21" tabindex=10></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>SSN</td>
                <td noWrap><ml:ssntextbox id=sCombinedBorSsn runat="server" width="75px" preset="ssn" tabindex=5></ml:ssntextbox></td>
                <td noWrap></td>
                <td class=FieldLabel noWrap>SSN</td>
                <td noWrap><ml:ssntextbox id=sCombinedCoborSsn runat="server" width="75px" preset="ssn" tabindex=10></ml:ssntextbox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table7 cellSpacing=0 cellPadding=0 width="98%"
      border=0>
        <tr>
          <td noWrap>
            <table id=Table10 cellSpacing=0 cellPadding=0 border=0
            >
              <tr id=row1>
                <td class=FieldLabel noWrap>3a. Mortgage
                  w/o Upfront MIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><ml:moneytextbox id=sLAmt runat="server" width="100" preset="money" cssclass="mask" readonly="True" tabindex=15></ml:moneytextbox></td>
                <td width="2%"></td>
                <td class=FieldLabel noWrap>6. Current
                  Housing Expenses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><ml:moneytextbox id=sPresLTotPersistentHExp runat="server" width="100" preset="money" cssclass="mask" readonly="True" tabindex=18></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel>3b. Total UFMIP <ml:percenttextbox id=sFfUfmipR runat="server" width="70" preset="percent" onchange="refreshCalculation();" tabindex=15  decimalDigits="6" />&nbsp;for
<asp:textbox id=sMipPiaMon runat="server" Width="30px" onchange="refreshCalculation();" tabindex=15></asp:textbox>&nbsp;mos.=<asp:CheckBox id=sFfUfmip1003Lckd tabIndex=15 runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox></td>
                <td><ml:moneytextbox id=sFfUfmip1003 runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel>7. Term of loan
                  (years)</td>
                <td><asp:textbox id=sTermInYr runat="server" width="46px" maxlength="3" readonly="True" tabindex=18></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel
                  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MIP
                  Paid in Cash</td>
                <td><ml:moneytextbox id=sUfCashPd runat="server" width="100px" preset="money" onchange="refreshCalculation();" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel>8. Interest Rate</td>
                <td><ml:percenttextbox id=sNoteIR runat="server" width="100" preset="percent" cssclass="mask" onchange="refreshCalculation();" tabindex=18></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel>&nbsp;&nbsp;&nbsp;
                  PMI, MIP, FF
                  financed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  =</td>
                <td><ml:moneytextbox id=sFfUfmipFinanced runat="server" width="100px" preset="money" ReadOnly="True" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel></td>
                <td></td></tr>
              <tr>
                <td class=FieldLabel>3c. Mortgage with
                  UFMIP</td>
                <td><ml:moneytextbox id=sFinalLAmt runat="server" width="100" preset="money" cssclass="mask" readonly="True" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel>9. Adj Buy-down
                  Interest Rate</td>
                <td><ml:percenttextbox id=sBuydownResultIR runat="server" width="100" preset="percent" cssclass="mask" ReadOnly="True" tabindex=18></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel>4&nbsp;.
                  &nbsp;Appraised Value (w/o CC)</td>
                <td><ml:moneytextbox id=sApprVal runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel noWrap></td>
                <td></td></tr>
              <tr>
                <td class=FieldLabel noWrap>5a. Total
                  Closing Costs (CC)&nbsp;</td>
                <td><ml:moneytextbox id=sFHACcTot runat="server" width="100" preset="money" cssclass="mask" readonly="True" onchange="refreshCalculation();" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel>Energy Efficient
                  Improvements</td>
                <td><ml:moneytextbox id=sFHAEnergyEffImprov runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=18></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel>5b. Less paid by
                  seller</td>
                <td><ml:moneytextbox id=sFHACcPbs runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td></td>
                <td></td></tr>
              <tr>
                <td class=FieldLabel>5c. Equals
                  Borrower's CC</td>
                <td><ml:moneytextbox id=sFHACcPbb runat="server" width="100" preset="money" cssclass="mask" ReadOnly="True" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td></td>
                <td
      ></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table12 cellSpacing=0 cellPadding=0
      width="98%" border=0>
        <tr>
          <td noWrap>
            <table id=Table11 cellSpacing=0 cellPadding=0 border=0
            >
              <tr>
                <td class=FieldLabel noWrap>10. Statutory
                  Investment Requirements&nbsp;</td>
                <td noWrap></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Existing Mortgage Lien</td>
                <td noWrap><ml:moneytextbox id=sFHAExistingMLien runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b.
                  Repairs &amp; Improvements <asp:textbox id=sFHAImprovementsDesc runat="server" width="194px" tabindex=20></asp:textbox></td>
                <td noWrap><ml:moneytextbox id=sFHAImprovements runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Borrower-Paid Closing Costs (from 5c)&nbsp;&nbsp; </td>
                <td noWrap><ml:moneytextbox id=sFHACcPbb2 runat="server" width="90px" preset="money" cssclass="mask" readonly="True" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d.
                  MIP Refund</td>
                <td noWrap><ml:moneytextbox id=sFHASalesConcessions runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e.
                  Mortgage Basis (10a + b + c minus 10d)</td>
                <td noWrap><ml:moneytextbox id=sFHAMBasisRefin runat="server" width="90" preset="money" readonly="True" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f.
                  (1) Appraised Value + CC Multiplied by LTV Factor<asp:checkbox id=sFHAMBasisRefinMultiplyLckd onclick="lockField(this, 'sFHAMBasisRefinMultiply'); refreshCalculation();" runat="server" text="locked" tabindex=20></asp:checkbox></td>
                <td noWrap><ml:moneytextbox id=sFHAMBasisRefinMultiply runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  (2) Appraised Value multiplied by 98.75/97.75%<asp:checkbox id=sFHAApprValMultiplyLckd onclick="lockField(this, 'sFHAApprValMultiply'); refreshCalculation();" runat="server" text="locked" tabindex=20></asp:checkbox></td>
                <td noWrap><ml:moneytextbox id=sFHAApprValMultiply runat="server" width="90" preset="money" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; g.
                  Mortgage (without UFMIP)</td>
                <td noWrap><ml:moneytextbox id=sLAmt2 runat="server" width="90" preset="money" readonly="True" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h.
                  Required Investment (10e minus 10g)</td>
                <td noWrap><ml:moneytextbox id=sFHAReqInvestment runat="server" width="90" preset="money" readonly="True" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i.
                  Discounts</td>
                <td noWrap><ml:moneytextbox id=sFHADiscountPoints runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; j.
                  Prepayable Expenses</td>
                <td noWrap><ml:moneytextbox id=sFHAPrepaidExp runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; k.
                  MIP Paid in Cash</td>
                <td noWrap><ml:moneytextbox id=sUfCashPdzzz2 runat="server" width="90px" preset="money" readonly="True" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; l.
                  Non-Realty and Other Items&nbsp;&nbsp;&nbsp;&nbsp; <asp:checkbox id=sFHANonrealtyLckd onclick="lockField(this, 'sFHANonrealty'); refreshCalculation();" runat="server" text="Lock" tabindex=20></asp:checkbox></td>
                <td noWrap><ml:moneytextbox id=sFHANonrealty runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; m.
                  Total Requirements (sum of lines 10h - 10l)</td>
                <td noWrap><ml:moneytextbox id=sFHAReqTot runat="server" width="90" preset="money" readonly="True" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; n.
                  Amount paid in <asp:checkbox id=sFHAIsAmtPdInCash runat="server" text="cash" designtimedragdrop="53" tabindex=20></asp:checkbox><asp:checkbox id=sFHAIsAmtPdInOther runat="server" text="other" tabindex=20></asp:checkbox></td>
                <td noWrap><ml:moneytextbox id=sFHAAmtPaid runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o.
                  Amount to be paid in&nbsp;<asp:checkbox id=sFHAIsAmtToBePdInCash runat="server" text="cash" tabindex=20></asp:checkbox><asp:checkbox id=sFHAIsAmtToBePdInOther runat="server" text="other" tabindex=20></asp:checkbox></td>
                <td noWrap><ml:moneytextbox id=sFHAAmtToBePd runat="server" width="90" preset="money" readonly="True" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p.
                  Assets Available</td>
                <td noWrap><ml:moneytextbox id=sFHAAssetAvail runat="server" width="90" preset="money" readonly="True" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;q.
                  2nd Mortgage Proceeds (if applicable)</td>
                <td noWrap><ml:moneytextbox id=sFHA2ndMAmt runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>11. Monthly
                  Effective Income</td>
                <td noWrap><asp:checkbox id=sFHAIncomeLckd onclick=refreshCalculation(); runat="server" Text="Lock" tabindex=20></asp:checkbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Borrower's Base Pay</td>
                <td noWrap><ml:moneytextbox id=sFHABBaseI runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b.
                  Borrower's Other Earnings</td>
                <td noWrap><ml:moneytextbox id=sFHABOI runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Co-borrower's Base Pay</td>
                <td noWrap><ml:moneytextbox id=sFHACBaseI runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d.
                  Co-borrower's Other Earnings</td>
                <td noWrap><ml:moneytextbox id=sFHACOI runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e. Net
                  Income From Real Estate</td>
                <td noWrap><ml:moneytextbox id=sFHANetRentalI runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f. Gross
                  Monthly Income</td>
                <td noWrap><ml:moneytextbox id=sFHAGrossMonI runat="server" width="100" preset="money" cssclass="mask" readonly="True" tabindex=20></ml:moneytextbox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table13 cellspacing=0 cellpadding=0
      width="98%" border=0>
        <tr>
          <td nowrap>
      <table id=Table1 cellSpacing=0 cellPadding=0 border=0>
              <tr>
                <td class=FieldLabel nowrap>12. Debts &amp; Obligations <asp:CheckBox id=sFHADebtLckd onclick=refreshCalculation(); runat="server" Text="Lock" tabindex=25></asp:CheckBox></td>
                <td class=FieldLabel nowrap>Monthly
                  Payment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                <td class=FieldLabel nowrap>Unpaid Balance</td></tr>
              <tr>
                <td class=FieldLabel
                  nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Total
                  installment debt&nbsp;&nbsp; </td>
                <td nowrap><ml:moneytextbox id=sFHADebtInstallPmt runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td>
                <td nowrap><ml:moneytextbox id=sFHADebtInstallBal runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel
                  nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Child Support,
                  etc.</td>
                <td nowrap><ml:moneytextbox id=sFHAChildSupportPmt runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td>
                <td nowrap></td></tr>
              <tr>
                <td class=FieldLabel
                  nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Other</td>
                <td nowrap><ml:moneytextbox id=sFHAOtherDebtPmt runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td>
                <td nowrap><ml:moneytextbox id=sFHAOtherDebtBal runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <TR>
                <TD class=FieldLabel
                  noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  Other (Negative Cashflow)</TD>
                <TD noWrap><ml:moneytextbox id=sFHANegCfRentalI tabIndex=25 runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                <TD noWrap></TD></TR>
              <tr>
                <td class=FieldLabel
                  nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Total monthly
                  payments&nbsp;&nbsp;&nbsp;&nbsp; </td>
                <td nowrap><ml:moneytextbox id=sFHADebtPmtTot runat="server" width="100" preset="money" readonly="True" cssclass="mask" tabindex=25></ml:moneytextbox></td>
                <td nowrap></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table14 cellspacing=0 cellpadding=0
      width="98%" border=0>
        <tr>
          <td nowrap>
            <table id=Table2 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td class=FieldLabel nowrap>13. Future Monthly Payments&nbsp;
                </td>
                <td nowrap></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Principal &amp; Interest - 1st Mortgage&nbsp;</td>
                <td nowrap><ml:moneytextbox id=sFHAPro1stMPmt runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b.
                  Monthly MIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="sFHAProMInsLckd" runat="server" TabIndex="30" onclick="refreshCalculation();" Text="Lock"/></td>
                <td nowrap><ml:moneytextbox id=sFHAProMIns runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=30 /></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Homeowners Association Fee</td>
                <td nowrap><ml:moneytextbox id=sFHAProHoAssocDues runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d.
                  Ground Rent</td>
                <td nowrap><ml:moneytextbox id=sFHAProGroundRent runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e.
                  Principal &amp; Interest - 2nd Mortgage</td>
                <td nowrap><ml:moneytextbox id=sFHAPro2ndFinPmt runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f.
                  Hazard Insurance</td>
                <td nowrap><ml:moneytextbox id=sFHAProHazIns runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; g.
                  Taxes &amp; special assessments</td>
                <td nowrap><ml:moneytextbox id=sFHAProRealETx runat="server" width="100" preset="money" cssclass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h.
                  Total mortgage payment</td>
                <td nowrap><ml:moneytextbox id=sFHAMonthlyPmt runat="server" width="100" preset="money" readonly="True" cssclass="mask" tabindex=30></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i.
                  Recurring expenses (from 12d)</td>
                <td nowrap><ml:moneytextbox id=sFHADebtPmtTot2 runat="server" width="100" preset="money" readonly="True" cssclass="mask" tabindex=30></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; j.
                  Total fixed payment</td>
                <td nowrap><ml:moneytextbox id=sFHAPmtFixedTot runat="server" width="100" preset="money" readonly="True" cssclass="mask" tabindex=30></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>14. Ratios</td>
                <td nowrap></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Loan-to-value (10g divided by 4)</td>
                <td nowrap><ml:percenttextbox id=sFHALtv2 runat="server" width="100" preset="percent" readonly="True" cssclass="mask" tabindex=30></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b.
                  Mortgage payment-to-income (13h divided by 11f)&nbsp; </td>
                <td nowrap><ml:percenttextbox id=sFHAMPmtToIRatio runat="server" width="100" preset="percent" readonly="True" cssclass="mask" tabindex=30></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Total fixed payment-to-income (13j divided by 11f)</td>
                <td nowrap><ml:percenttextbox id=sFHAFixedPmtToIRatio runat="server" width="100" preset="percent" readonly="True" cssclass="mask" tabindex=30></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>15. Borrower Rating (enter "A" for
                  acceptable or "R" for reject)</td>
                <td nowrap></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Credit characteristics</td>
                <td nowrap><asp:textbox id=sFHACreditRating onkeyup="validateAR(this, event);" runat="server" width="30px" readonly="True" tabindex=30></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b.
                  Adequacy of effective income</td>
                <td nowrap><asp:textbox id=sFHARatingIAdequacy onkeyup="validateAR(this, event);" runat="server" width="30px" readonly="True" tabindex=30></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Stability of effective income</td>
                <td nowrap><asp:textbox id=sFHARatingIStability onkeyup="validateAR(this, event);" runat="server" width="30px" readonly="True" tabindex=30></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d.
                  Adequacy of available assets</td>
                <td nowrap><asp:textbox id=sFHARatingAssetAdequacy onkeyup="validateAR(this, event);" runat="server" width="30px" readonly="True" tabindex=30></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap></td>
                <td nowrap></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table15 cellspacing=0 cellpadding=0
      width="98%" border=0>
        <tr>
          <td nowrap>
            <table id=Table3 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td class=FieldLabel nowrap>16. CAIVRS -
                  LDP/GSA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                <td class=FieldLabel nowrap>Borrower</td>
                <td class=FieldLabel nowrap>Co-borrower</td></tr>
              <tr>
                <td class=FieldLabel
                  nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CAIVRS Number</td>
                <td nowrap><asp:textbox id=sFHABCaivrsNum runat="server" width="164px" readonly="True" tabindex=35></asp:textbox></td>
                <td nowrap><asp:textbox id=sFHACCaivrsNum runat="server" width="164px" readonly="True" tabindex=35></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel
                  nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LDP/GSA (page no.
                  &amp; date)&nbsp;&nbsp; </td>
                <td nowrap><asp:textbox id=sFHABLpdGsa runat="server" width="164px" readonly="True" tabindex=35></asp:textbox></td>
                <td nowrap><asp:textbox id=sFHACLpdGsa runat="server" width="164px" readonly="True" tabindex=35></asp:textbox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table16 cellspacing=0 cellpadding=0
      width="98%" border=0>
        <tr>
          <td nowrap>
            <table id=Table4 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td class=FieldLabel nowrap>Attachment A Information</td>
                <td nowrap></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp; A1. Contract Sales
                  Price of Property (line 10a above)&nbsp; </td>
                <td nowrap><ml:moneytextbox id=sPurchPrice2 runat="server" width="100" preset="money" readonly="True" cssclass="mask" tabindex=40></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp; A2. 6% of line A1</td>
                <td nowrap><ml:moneytextbox id=sPurchPrice6pc runat="server" width="100" preset="money" readonly="True" cssclass="mask" tabindex=40></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp; A3. Total Seller
                  Contribution</td>
                <td nowrap><ml:moneytextbox id=sFHASellerContribution runat="server" width="100" preset="money" readonly="True" cssclass="mask" onchange="refreshCalculation();" tabindex=40></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp; A4. Excess
                  Contribution</td>
                <td nowrap><ml:moneytextbox id=sFHAExcessContribution runat="server" width="100" preset="money" readonly="True" cssclass="mask" onchange="refreshCalculation();" tabindex=40></ml:moneytextbox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td class=FieldLabel>Remarks</td></tr>
  <tr>
    <td><asp:textbox id=sFHACreditAnalysisRemarks runat="server" width="580px" height="160px" textmode="MultiLine" tabindex=45></asp:textbox></td></tr>
  <tr>
    <td class=FieldLabel>Total Amount of Gift<ml:moneytextbox id=sFHAGiftAmtTot runat="server" width="90" preset="money" tabindex=45></ml:moneytextbox></td></tr>
  <tr>
    <td></td></tr></table></form>

  </body>
</HTML>
