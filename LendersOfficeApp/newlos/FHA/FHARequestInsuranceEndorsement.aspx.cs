using System;
using System.Collections;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
	public partial class FHARequestInsuranceEndorsement : BaseLoanPage
	{
        #region Protected member variables
        #endregion


        protected void PageInit(object sender, System.EventArgs e) 
        {
            sSpZip.SmartZipcode(sSpCity, sSpState);

            Tools.Bind_sFHAHousingActSection(sFHAHousingActSection);
            Tools.Bind_TriState(sFHAInsEndorseInclSolarWindHeater);
            Tools.Bind_TriState(sFHAInsEndorseExemptFromSSNTri);
            Tools.Bind_TriState(sFHAInsEndorseVeteranPrefTri);
            Tools.Bind_TriState(sFHAInsEndorseEnergyEffMTri);
            Tools.Bind_TriState(sFHAInsEndorseIssueMicInSponsorNmTri);
            Tools.Bind_TriState(sFHAInsEndorseMailToSponsorTri);
            Tools.Bind_TriState(sFHAInsEndorseCurrentPmtTri);
            Tools.Bind_TriState(sFHAInsEndorseUfmipFinancedTri);
            Tools.Bind_TriState(sFHAInsEndorseRepairEscrowTri);
            Tools.Bind_aRaceT(aBRaceT);
            Tools.Bind_aRaceT(aCRaceT);
            Tools.Bind_GenderT(aBGenderT, shouldIncludeBothOption: true);
            Tools.Bind_GenderT(aCGenderT, shouldIncludeBothOption: true);
            sFHAInsEndorseExemptFromSSNTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAInsEndorseInclSolarWindHeater.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAInsEndorseVeteranPrefTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAInsEndorseEnergyEffMTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAInsEndorseIssueMicInSponsorNmTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAInsEndorseMailToSponsorTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAInsEndorseCurrentPmtTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAInsEndorseUfmipFinancedTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAInsEndorseRepairEscrowTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            this.PageTitle = "Request for Insurance Endorsement";
            this.PageID = "FHA_54111";

        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHARequestInsuranceEndorsement));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            aCMidNm.Text = dataApp.aCMidNm;
            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aCFirstNm.Text = dataApp.aCFirstNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aBSsn.Text = dataApp.aBSsn;
            aCSsn.Text = dataApp.aCSsn;
            Tools.SetDropDownListValue(aBGenderT, dataApp.aBGender);
            Tools.SetDropDownListValue(aCGenderT, dataApp.aCGender);
            Tools.SetDropDownListValue(aBRaceT, dataApp.aBRaceT);
            Tools.SetDropDownListValue(aCRaceT, dataApp.aCRaceT);
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;
            sNoteIR.Text = dataLoan.sNoteIR_rep;

            sFHAInsEndorseProgId.Text = dataLoan.sFHAInsEndorseProgId;
            Tools.Set_TriState(sFHAInsEndorseInclSolarWindHeater, dataLoan.sFHAInsEndorseInclSolarWindHeater);
            sFHAInsEndorseWillBorrBeOccupant.Checked = dataLoan.sFHAInsEndorseWillBorrBeOccupant;
            sFHAInsEndorseWillBorrBeLandlord.Checked = dataLoan.sFHAInsEndorseWillBorrBeLandlord;
            sFHAIsEscrowCommitment.Checked = dataLoan.sFHAIsEscrowCommitment;
            sFHAInsEndorseWillBorrBeCorp.Checked = dataLoan.sFHAInsEndorseWillBorrBeCorp;
            sFHAIsNonProfit.Checked = dataLoan.sFHAIsNonProfit;
            sFHAIsGovAgency.Checked = dataLoan.sFHAIsGovAgency;
            sFHAInsEndorseRepairCompleteD.Text = dataLoan.sFHAInsEndorseRepairCompleteD;
            sFHAInsEndorseRepairEscrowAmt.Text = dataLoan.sFHAInsEndorseRepairEscrowAmt_rep;
            Tools.SetDropDownListValue(sFHAInsEndorseCounselingT, dataLoan.sFHAInsEndorseCounselingT);
            sFHAInsEndorseOrigMortgageeId.Text = dataLoan.sFHAInsEndorseOrigMortgageeId;
            sFHAInsEndorseAgentNum.Text = dataLoan.sFHAInsEndorseAgentNum;
            sFHAInsEndorseMMaturityDate.Text = dataLoan.sFHAInsEndorseMMaturityDate;
            sFHAInsEndorseWarrantyEnrollNum.Text = dataLoan.sFHAInsEndorseWarrantyEnrollNum;
            sFHAInsEndorseAmortPlanCode.Text = dataLoan.sFHAInsEndorseAmortPlanCode;
            sFHAInsEndorseConstructionCode.Text = dataLoan.sFHAInsEndorseConstructionCode;
            sFHAInsEndorseLivingUnits.Text = dataLoan.sFHAInsEndorseLivingUnits;
            Tools.Set_TriState(sFHAInsEndorseExemptFromSSNTri, dataLoan.sFHAInsEndorseExemptFromSSNTri);
            Tools.Set_TriState(sFHAInsEndorseVeteranPrefTri, dataLoan.sFHAInsEndorseVeteranPrefTri);
            Tools.Set_TriState(sFHAInsEndorseEnergyEffMTri, dataLoan.sFHAInsEndorseEnergyEffMTri);
            Tools.Set_TriState(sFHAInsEndorseIssueMicInSponsorNmTri, dataLoan.sFHAInsEndorseIssueMicInSponsorNmTri);
            Tools.Set_TriState(sFHAInsEndorseMailToSponsorTri, dataLoan.sFHAInsEndorseMailToSponsorTri);
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
            Tools.Set_TriState(sFHAInsEndorseCurrentPmtTri, dataLoan.sFHAInsEndorseCurrentPmtTri);
            Tools.Set_TriState(sFHAInsEndorseUfmipFinancedTri, dataLoan.sFHAInsEndorseUfmipFinancedTri);
            sFHALtv.Text = dataLoan.sFHALtvInsEndorsement_rep;
            sFHAInsEndorseLPurposeCode.Text = dataLoan.sFHAInsEndorseLPurposeCode;
            sHas1stTimeBuyer.Checked = dataLoan.sHas1stTimeBuyer;
            sFHASponsorAgentIdCode.Text = dataLoan.sFHASponsorAgentIdCode;
            sTerm.Text = dataLoan.sTerm_rep;

            sSchedDueD1.Text = dataLoan.sSchedDueD1_rep;
            sClosedD.Text = dataLoan.sClosedD_rep;
            Tools.Set_TriState(sFHAInsEndorseRepairEscrowTri, dataLoan.sFHAInsEndorseRepairEscrowTri);
            sFHAHousingActSection.Text = dataLoan.sFHAHousingActSection;

            IPreparerFields appraiser = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAInsEndorsementAppraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHAInsEndorsementAppraiserPreparerName.Text = appraiser.PreparerName;
            FHAInsEndorsementAppraiserLicenseNumOfAgent.Text = appraiser.LicenseNumOfAgent;

            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAInsEndorsementUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHAInsEndorsementUnderwriterPreparerName.Text = underwriter.PreparerName;
            FHAInsEndorsementUnderwriterLicenseNumOfAgent.Text = underwriter.LicenseNumOfAgent;

        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        
        }

        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));


        }

	}
}
