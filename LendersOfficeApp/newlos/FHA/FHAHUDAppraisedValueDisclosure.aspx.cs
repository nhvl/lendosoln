using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
	public partial class FHAHUDAppraisedValueDisclosure : BaseLoanPage
	{

        protected void PageInit(object sender, System.EventArgs e)
        {
            FHAAppraisedValueDisclosureZip.SmartZipcode(FHAAppraisedValueDisclosureCity, FHAAppraisedValueDisclosureState);
            this.PageTitle = "FHA Appraised Value Disclosure";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CFHAHUDAppraisedValueDisclosurePDF);
            this.PageID = "FHAAppraisedValueDisclosure";
            RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAHUDAppraisedValueDisclosure));

            dataLoan.InitLoad();
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAppraisedValueDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHAAppraisedValueDisclosureCompanyName.Text = preparer.CompanyName;
            FHAAppraisedValueDisclosureStreetAddr.Text = preparer.StreetAddr;
            FHAAppraisedValueDisclosureCity.Text = preparer.City;
            FHAAppraisedValueDisclosureState.Value = preparer.State;
            FHAAppraisedValueDisclosureZip.Text = preparer.Zip;
            FHAAppraisedValueDisclosurePhoneOfCompany.Text = preparer.PhoneOfCompany;
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
