<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="FHARequestInsuranceEndorsement.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHARequestInsuranceEndorsement" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>FHARequestInsuranceEndorsement</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<script language=javascript>
<!--
function updateBorrowerName() {
  var name = <%= AspxTools.JsGetElementById(aBLastNm) %>.value + ", " + <%= AspxTools.JsGetElementById(aBFirstNm) %>.value;
  parent.info.f_updateApplicantDDL(name, ML.aAppId);
}    
//-->
</script>

<form id=FHARequestInsuranceEndorsement method=post runat="server">
<table id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class=MainRightHeader>Request for Insurance 
      Endorsement (HUD-54111)</td></tr>
  <tr>
    <td>
      <table id=Table2 cellSpacing=0 cellPadding=2 border=0>
        <tr>
          <td class=FieldLabel noWrap>1) Case Number</td>
          <td noWrap><asp:textbox id=sAgencyCaseNum runat="server" width="80px" tabindex=1></asp:textbox></td>
          <td class=FieldLabel noWrap>2) Section of the 
            Act</td>
          <td class=FieldLabel noWrap><ml:combobox id=sFHAHousingActSection runat="server" width="70px" tabindex=1></ml:combobox>3) 
            Program ID</td>
          <td noWrap><asp:textbox id=sFHAInsEndorseProgId runat="server" width="80px" tabindex=1></asp:textbox></td></tr></table></td></tr>
  <tr>
    <td class=FieldLabel>4) LTV Ratio&nbsp; <ml:percenttextbox id=sFHALtv runat="server" width="70" DESIGNTIMEDRAGDROP="181" preset="percent" readonly="True" tabindex=1></ml:percenttextbox></td></tr>
  <tr>
    <td class=FieldLabel>5) Include amounts for solar or 
      wind driven heating systems? <asp:CheckBoxList id=sFHAInsEndorseInclSolarWindHeater runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></td></tr>
  <tr>
    <td class=FieldLabel>6) Purpose of Loan Code From 
      Block 20 of Form HUD-92900-A&nbsp; <asp:TextBox id=sFHAInsEndorseLPurposeCode runat="server" Width="50px" ReadOnly="True"></asp:TextBox></td></tr>
  <tr>
    <td class=FieldLabel>7) Borrower Will Be <asp:checkbox id=sFHAInsEndorseWillBorrBeOccupant runat="server" Text="Occupant" tabindex=1></asp:checkbox><asp:checkbox id=sFHAInsEndorseWillBorrBeLandlord runat="server" Text="Landlord" tabindex=1></asp:checkbox><asp:checkbox id=sFHAIsEscrowCommitment runat="server" Text="Escrow Commmitment" tabindex=1></asp:checkbox><asp:checkbox id=sFHAInsEndorseWillBorrBeCorp runat="server" Text="Corporation Or Partnership" tabindex=1></asp:checkbox><asp:checkbox id=sFHAIsNonProfit runat="server" Text="Non-profit" tabindex=1></asp:checkbox><asp:checkbox id=sFHAIsGovAgency runat="server" Text="Government Agency" tabindex=1></asp:checkbox></td></tr>
  <tr>
    <td class=FieldLabel>8) Repair Escrow?&nbsp; <asp:CheckBoxList id=sFHAInsEndorseRepairEscrowTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList>&nbsp;&nbsp;&nbsp;&nbsp; 
      If "Yes" Date complete (mm/yyyy) <asp:textbox id=sFHAInsEndorseRepairCompleteD runat="server" Width="50px" tabindex=1></asp:textbox>&nbsp;Escrow 
      Amt<ml:moneytextbox id=sFHAInsEndorseRepairEscrowAmt runat="server" width="70px" preset="money" tabindex=1></ml:moneytextbox></td></tr>
  <tr>
    <td>
      <table id=Table3 cellSpacing=0 cellPadding=0 border=0>
        <tr>
          <td class=FieldLabel noWrap>Borrower</td>
          <td noWrap></td>
          <td class=FieldLabel noWrap>Coborrower</td>
          <td noWrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap>10) First Name</td>
          <td noWrap><asp:textbox id=aBFirstNm tabIndex=5 runat="server" onchange="updateBorrowerName();"></asp:textbox></td>
          <td class=FieldLabel noWrap>14) First Name</td>
          <td noWrap><asp:textbox id=aCFirstNm tabIndex=10 runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Middle 
          Name</td>
          <td noWrap><asp:textbox id=aBMidNm tabIndex=5 runat="server"></asp:textbox></td>
          <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Middle Name</td>
          <td noWrap><asp:textbox id=aCMidNm tabIndex=10 runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Last Name</td>
          <td noWrap><asp:textbox id=aBLastNm tabIndex=5 runat="server" onchange="updateBorrowerName();"></asp:textbox></td>
          <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Last Name</td>
          <td noWrap><asp:textbox id=aCLastNm tabIndex=10 runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Suffix</td>
          <td noWrap><ml:ComboBox id=aBSuffix tabIndex=5 runat="server" Width="97px"></ml:ComboBox></td>
          <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp; Suffix</td>
          <td noWrap><ml:ComboBox id=aCSuffix tabIndex=10 runat="server" Width="88px"></ml:ComboBox></td></tr>
        <tr>
          <td class=FieldLabel noWrap>11) SSN</td>
          <td noWrap><ml:ssntextbox id=aBSsn tabIndex=5 runat="server" width="75px" preset="ssn"></ml:ssntextbox></td>
          <td class=FieldLabel noWrap>15) SSN</td>
          <td noWrap><ml:ssntextbox id=aCSsn tabIndex=10 runat="server" width="75px" preset="ssn"></ml:ssntextbox></td></tr>
        <tr>
          <td class=FieldLabel style="HEIGHT: 13px" noWrap >12) Race</td>
          <td style="HEIGHT: 13px" noWrap><asp:dropdownlist id=aBRaceT tabIndex=5 runat="server"></asp:dropdownlist></td>
          <td class=FieldLabel style="HEIGHT: 13px" noWrap >16) Race</td>
          <td style="HEIGHT: 13px" noWrap><asp:dropdownlist id=aCRaceT tabIndex=10 runat="server"></asp:dropdownlist></td></tr>
        <tr>
          <td class=FieldLabel noWrap>13) Sex</td>
          <td noWrap><asp:dropdownlist id=aBGenderT tabIndex=5 runat="server"></asp:dropdownlist></td>
          <td class=FieldLabel noWrap>17) Sex</td>
          <td noWrap><asp:dropdownlist id=aCGenderT tabIndex=10 runat="server"></asp:dropdownlist></td></tr></table></td></tr>
  <tr>
    <td>
      <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td class=FieldLabel>20) Exempt from SSN</td>
        <td><asp:CheckBoxList id=sFHAInsEndorseExemptFromSSNTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></td>
      </tr>
      <tr>
        <td colspan=2 class=FieldLabel>21)<asp:checkbox id=sHas1stTimeBuyer tabIndex=15 runat="server" Text="Is any borrower a first time home buyer?"></asp:checkbox></td>
      </tr>
      <tr>
        <td class=FieldLabel>22) Counseling</td>
        <td><asp:dropdownlist id=sFHAInsEndorseCounselingT tabIndex=15 runat="server">
            <asp:ListItem Value="0">Leave Blank</asp:ListItem>
            <asp:ListItem Value="2">A) None</asp:ListItem>
            <asp:ListItem Value="1">B) Lender</asp:ListItem>
            <asp:ListItem Value="3">C) Third Party</asp:ListItem>
          </asp:dropdownlist>
        </td>
      </tr>
    <tr>
      <td class=FieldLabel>23) Have Veteran Preference</td>
      <td><asp:CheckBoxList id=sFHAInsEndorseVeteranPrefTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></td>
    </tr>
    <tr>
      <td class=FieldLabel>24) Energy Efficient Mortgage</td>
      <td><asp:CheckBoxList id=sFHAInsEndorseEnergyEffMTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></td>
    </tr>
    <tr>
      <td class=FieldLabel>25) Property Address</td>
      <td><asp:textbox id=sSpAddr tabIndex=15 runat="server" Width="256px"></asp:textbox></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class=FieldLabel><asp:textbox id=sSpCity tabIndex=15 runat="server"></asp:textbox><ml:statedropdownlist id=sSpState tabIndex=15 runat="server" IncludeTerritories="false"></ml:statedropdownlist><ml:zipcodetextbox id=sSpZip tabIndex=15 runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>26) Originating Mortgagee ID</td>
      <td><asp:textbox id=sFHAInsEndorseOrigMortgageeId tabIndex=15 runat="server" Width="90px"></asp:textbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>27) Sponsor ID</td>
      <td><asp:textbox id=sFHASponsorAgentIdCode tabIndex=15 runat="server" Width="90px"></asp:textbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>28) Authorized Agent ID</td>
      <td><asp:textbox id=sFHAInsEndorseAgentNum tabIndex=15 runat="server" Width="90px"></asp:textbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>29) Issue MIC In Sponsor Name</td>
      <td><asp:CheckBoxList id=sFHAInsEndorseIssueMicInSponsorNmTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></td>
    </tr>
    <tr>
      <td class=FieldLabel>30) Mail to Sponsor</td>
      <td><asp:CheckBoxList id=sFHAInsEndorseMailToSponsorTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></td>
    </tr>
    <tr>
      <td class=FieldLabel>31) Mortgage Amount</td>
      <td><ml:moneytextbox id=sFinalLAmt tabIndex=15 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>32) Interest Rate</td>
      <td><ml:percenttextbox id=sNoteIR tabIndex=15 runat="server" width="70" preset="percent" ReadOnly="True"></ml:percenttextbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>33) (P &amp; I)</td>
      <td><ml:moneytextbox id=sProThisMPmt tabIndex=15 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>34) Maturity Date (MMYY)</td>
      <td><asp:textbox id=sFHAInsEndorseMMaturityDate tabIndex=15 runat="server" Width="71px" MaxLength="4"></asp:textbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>35) Term</td>
      <td><asp:textbox id=sTerm tabIndex=15 runat="server" Width="54px" ReadOnly="True" MaxLength="4"></asp:textbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>36) Warranty (10 yr)</td>
      <td><asp:textbox id=sFHAInsEndorseWarrantyEnrollNum tabIndex=15 runat="server" Width="90px"></asp:textbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>37) Amortization Plan Code</td>
      <td><asp:textbox id=sFHAInsEndorseAmortPlanCode tabIndex=15 runat="server" Width="90px"></asp:textbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>38) Date 1st Payment</td>
      <td><ml:DateTextBox id=sSchedDueD1 tabIndex=15 runat="server" width="75" preset="date"></ml:DateTextBox></td>
    </tr>
    <tr>
      <td class=FieldLabel>39) Current Pmt?</td>
      <td><asp:CheckBoxList id=sFHAInsEndorseCurrentPmtTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></td>
    </tr>
    <tr>
      <td class=FieldLabel>40) Construction Code</td>
      <td><asp:textbox id=sFHAInsEndorseConstructionCode tabIndex=15 runat="server" Width="54px" MaxLength="4"></asp:textbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>41) Living Units</td>
      <td><asp:textbox id=sFHAInsEndorseLivingUnits tabIndex=15 runat="server" Width="54px" MaxLength="4"></asp:textbox></td>
    </tr>
    <tr>
      <td class=FieldLabel>42) UFMIP Financed?</td>
      <td><asp:CheckBoxList id=sFHAInsEndorseUfmipFinancedTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></td>
    </tr>
    <tr>
      <td class=FieldLabel>43) Closing Date </td>
      <td><ml:datetextbox id=sClosedD tabIndex=15 runat="server" width="75" preset="date"></ml:datetextbox></td>
    </tr>
  </table>
    </td>
  </tr>      
  <tr>
    <td class=FieldLabel>45) Appraiser ID <asp:textbox id=FHAInsEndorsementAppraiserLicenseNumOfAgent tabIndex=15 runat="server" Width="90px"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp; 
      46) Appraiser Name<asp:textbox id=FHAInsEndorsementAppraiserPreparerName tabIndex=15 runat="server"></asp:textbox></td></tr>
  <tr>
    <td class=FieldLabel>47) Underwriter's CHUMS ID <asp:textbox id=FHAInsEndorsementUnderwriterLicenseNumOfAgent tabIndex=15 runat="server" Width="90px"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp; 
      48) Underwriter Name <asp:textbox id=FHAInsEndorsementUnderwriterPreparerName tabIndex=15 runat="server"></asp:textbox></td></tr></table></form>
	</body>
</html>
