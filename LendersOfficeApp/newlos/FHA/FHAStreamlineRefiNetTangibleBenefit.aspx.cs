﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
    public partial class FHAStreamlineRefiNetTangibleBenefit : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            this.PageID = "FHAStreamlineRefiNetTangibleBenefit";
            base.OnInit(e);
        }

        protected override void LoadData()
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAStreamlineRefiNetTangibleBenefit));
            dataLoan.InitLoad();

            #region Streamline Refi Without Appraisal
            // Section A
            sOriginalAppraisedValue.Text = dataLoan.sOriginalAppraisedValue_rep;
            sVaLoanAmtCurrentLoan.Text = dataLoan.sVaLoanAmtCurrentLoan_rep;
            sIsTermLTE15Yr.SelectedValue = dataLoan.sIsTermLTE15Yrs ? "Y" : "N";
            sFfUfMipIsBeingFinanced.SelectedValue = dataLoan.sFfUfMipIsBeingFinanced ? "Y" : "N";
            sProdIsLoanEndorsedBeforeJune09.SelectedValue = dataLoan.sProdIsLoanEndorsedBeforeJune09 ? "Y" : "N";

            // Section B
            sSpLien.Text = dataLoan.sSpLien_rep;
            s30DayInterestOldLoan.Text = dataLoan.s30DayInterestOldLoan_rep;
            sFHATotalPayoffBalance.Text = dataLoan.sFHATotalPayoffBalance_rep;
            sFHACcTot.Text = dataLoan.sFHACcTot_rep;
            sFHADiscountPoints.Text = dataLoan.sFHADiscountPoints_rep;
            sFHAPrepaidExp.Text = dataLoan.sFHAPrepaidExp_rep;
            sFHATotalNewLoanCosts.Text = dataLoan.sFHATotalNewLoanCosts_rep;

            // Section C
            sFHASalesConcessions.Text = dataLoan.sFHASalesConcessions_rep;
            sFHANewMaximumBaseLAmt.Text = dataLoan.sFHANewMaximumBaseLAmt_rep;

            // Section D
            sFHAAmountNeededToClose.Text = dataLoan.sFHAAmountNeededToClose_rep;

            // Section E
            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            sFfUfmipR.Text = dataLoan.sFfUfmipR_rep;
            sFfuFmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;

            // Section F
            sFHALtvR.Text = dataLoan.sFHALtvR_rep;
            sProMInsR.Text = dataLoan.sProMInsR_rep;
            #endregion

            #region Net Tangible Benefit
            sLAmtCalc_b.Text = dataLoan.sLAmtCalc_rep;
            sFinalLAmt_b.Text = dataLoan.sFinalLAmt_rep;
            sTerm.Text = dataLoan.sTerm_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sProThisMPmt.Text = dataLoan.sProThisMPmt_rep;
            sProMInsR_b.Text = dataLoan.sProMInsR_rep;
            sProThisMPmtAndMIns.Text = dataLoan.sProThisMPmtAndMIns_rep;
            sProMIns.Text = dataLoan.sProMIns_rep;
            sTotalCurrentPIAndMonMIP.Text = dataLoan.sTotalCurrentPIAndMonMIP_rep;
            sProThisMPmtAndMIns_b.Text = dataLoan.sProThisMPmtAndMIns_rep;
            sFHAMPmtDiffPc.Text = dataLoan.sFHAMPmtDiffPc_rep;
            #endregion

            base.LoadData();
        }
    }
}
