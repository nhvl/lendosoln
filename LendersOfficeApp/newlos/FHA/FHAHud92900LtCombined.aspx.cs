namespace LendersOfficeApp.newlos.FHA
{
    using System;
    using System.Collections;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOfficeApp.newlos.Forms;

    public partial class FHAHud92900LtCombined : BaseLoanPage
    {
        internal static void BindDataFromControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.BindData(loan, item);
        }

        internal static void LoadDataForControls(CPageData loan, CAppData application, AbstractBackgroundServiceItem item)
        {
            QualRateCalculationPopup.LoadData(loan, item);
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.PageTitle = "FHA Loan Underwriting and Transmittal Summary Combined";
            this.PageID = "FHATransmittalCombined";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CHUD_92900_LTCombinedPDF);

            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");

            sSpZip.SmartZipcode(sSpCity, sSpState);

            Tools.Bind_sFHAHousingActSection(sFHAHousingActSection);
            
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sGseSpT(sGseSpT);
            Tools.Bind_sFHAConstructionT(sFHAConstructionT);
            Tools.Bind_sEstateHeldT(sEstateHeldT);
            Tools.Bind_TriState(sFHAScoreByTotalTri);
            Tools.Bind_TriState(sFHABLdpGsaTri);
            Tools.Bind_TriState(sFHACLdpGsaTri);
            Tools.Bind_sLPurposeT(sLPurposeT);

            sFHAScoreByTotalTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHABLdpGsaTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHACLdpGsaTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);


        }
        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));


        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAHud92900LtCombined));
            dataLoan.InitLoad();

            this.QualRatePopup.LoadData(dataLoan);

            sFHA92900LtUnderwriterChumsId.Text = dataLoan.sFHA92900LtUnderwriterChumsId;

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            sFHAHousingActSection.Text = dataLoan.sFHAHousingActSection;
            sFHANegCfRentalI.Text = dataLoan.sFHANegCfRentalI_rep;



            sCombinedBorInfoLckd.Checked = dataLoan.sCombinedBorInfoLckd;
            sCombinedCoborUsing2ndAppBorr.Checked = dataLoan.sCombinedCoborUsing2ndAppBorr;
            sCombinedBorFirstNm.Text = dataLoan.sCombinedBorFirstNm;
            sCombinedBorMidNm.Text = dataLoan.sCombinedBorMidNm;
            sCombinedBorLastNm.Text = dataLoan.sCombinedBorLastNm;
            sCombinedBorSuffix.Text = dataLoan.sCombinedBorSuffix;
            sCombinedBorSsn.Text = dataLoan.sCombinedBorSsn;
            sCombinedCoborFirstNm.Text = dataLoan.sCombinedCoborFirstNm;
            sCombinedCoborMidNm.Text = dataLoan.sCombinedCoborMidNm;
            sCombinedCoborLastNm.Text = dataLoan.sCombinedCoborLastNm;
            sCombinedCoborSuffix.Text = dataLoan.sCombinedCoborSuffix;
            sCombinedCoborSsn.Text = dataLoan.sCombinedCoborSsn;

            
            
            
            
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;

            Tools.SetDropDownListValue(sGseSpT, dataLoan.sGseSpT);
            sPurchPrice.Text = dataLoan.sPurchPrice_rep;
            sApprVal.Text = dataLoan.sApprVal_rep;
            Tools.SetDropDownListValue(sEstateHeldT, dataLoan.sEstateHeldT);
            Tools.SetDropDownListValue(sFHAConstructionT, dataLoan.sFHAConstructionT);
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            sRAdjMarginR.Text = dataLoan.sRAdjMarginR_rep;
            sBuydown.Checked = dataLoan.sBuydown;
            sLAmtCalc.Text = dataLoan.sLAmtCalc_rep;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sQualIR.Text = dataLoan.sQualIR_rep;
            this.sQualIRLckd.Checked = dataLoan.sQualIRLckd;
            sTerm.Text = dataLoan.sTerm_rep;
            sUnitsNum.Text = dataLoan.sUnitsNum_rep;
            sRAdj1stCapMon.Text = dataLoan.sRAdj1stCapMon_rep;


            sFHAIncomeLckd.Checked = dataLoan.sFHAIncomeLckd;
            sFHATotBaseI.Text = dataLoan.sFHATotBaseI_rep;
            sFHABBaseI.Text = dataLoan.sFHABBaseI_rep;
            sFHACBaseI.Text = dataLoan.sFHACBaseI_rep;
            sFHABOI.Text = dataLoan.sFHABOI_rep;
            sFHACOI.Text = dataLoan.sFHACOI_rep;
            sFHATotOI.Text = dataLoan.sFHATotOI_rep;
            sFHANetRentalI.Text = dataLoan.sFHANetRentalI_rep;
            sFHANetRentalI2.Text = dataLoan.sFHANetRentalI_rep;
            sFHABTotI.Text = dataLoan.sFHABTotI_rep;
            sFHACTotI.Text = dataLoan.sFHACTotI_rep;
            sFHAGrossMonI.Text = dataLoan.sFHAGrossMonI_rep;

            sFHADebtLckd.Checked = dataLoan.sFHADebtLckd;
            sFHADebtInstallPmt.Text = dataLoan.sFHADebtInstallPmt_rep;
            sFHAChildSupportPmt.Text = dataLoan.sFHAChildSupportPmt_rep;
            sFHADebtInstallBal.Text = dataLoan.sFHADebtInstallBal_rep;
            sFHAOtherDebtPmt.Text = dataLoan.sFHAOtherDebtPmt_rep;
            sFHAOtherDebtBal.Text = dataLoan.sFHAOtherDebtBal_rep;
            sFHAPmtFixedTot.Text = dataLoan.sFHAPmtFixedTot_rep;




            sTransmFntc.Text = dataLoan.sTransmFntc_rep;
            sVerifAssetAmt.Text = dataLoan.sVerifAssetAmt_rep;
            sFHACcTot.Text = dataLoan.sFHACcTot_rep;
            sFntcSrc.Text = dataLoan.sFntcSrc;
            sRsrvMonNumDesc.Text = dataLoan.sRsrvMonNumDesc;
            sFHASellerContributionPc.Text = dataLoan.sFHASellerContributionPc_rep;
            sTransmFntcLckd.Checked = dataLoan.sTransmFntcLckd;

            sFHAPro1stMPmt.Text = dataLoan.sFHAPro1stMPmt_rep;
            sFHAProMIns.Text = dataLoan.sFHAProMIns_rep;
            sFHAProMInsLckd.Checked = dataLoan.sFHAProMInsLckd;
            sFHAProHoAssocDues.Text = dataLoan.sFHAProHoAssocDues_rep;
            sFHAProGroundRent.Text = dataLoan.sFHAProGroundRent_rep;
            sFHAProGroundRent.ReadOnly = (dataLoan.sIsHousingExpenseMigrated && dataLoan.sLT == E_sLT.FHA
                                            && dataLoan.sGroundRentExpense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements);
            sFHAPro2ndFinPmt.Text = dataLoan.sFHAPro2ndFinPmt_rep;
            sFHAProHazIns.Text = dataLoan.sFHAProHazIns_rep;
            sFHAProRealETx.Text = dataLoan.sFHAProRealETx_rep;
            sFHAMonthlyPmt.Text = dataLoan.sFHAMonthlyPmt_rep;

            sLtvR.Text = dataLoan.sLtvR_rep;
            sCltvR.Text = dataLoan.sCltvR_rep;
            sHcltvR.Text = dataLoan.sHcltvR_rep;
            sFHAMPmtToIRatio.Text = dataLoan.sFHAMPmtToIRatio_rep;
            sFHAFixedPmtToIRatio.Text = dataLoan.sFHAFixedPmtToIRatio_rep;


            sFHABCaivrsNum.Text = dataLoan.sFHABCaivrsNum;
            sFHACCaivrsNum.Text = dataLoan.sFHACCaivrsNum;



            sTransmUwerComments.Text = dataLoan.sTransmUwerComments;
            sFHASellerContribution.Text = dataLoan.sFHASellerContribution_rep;

            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            sFHAPurposeIsPurchase.Checked = dataLoan.sFHAPurposeIsPurchase; 
            sFHAPurposeIsNoCashoutRefi.Checked = dataLoan.sFHAPurposeIsNoCashoutRefi; 
            sFHAPurposeIsCashoutRefi.Checked = dataLoan.sFHAPurposeIsCashoutRefi; 
            sFHAPurposeIsStreamlineRefi.Checked = dataLoan.sFHAPurposeIsStreamlineRefi; 
            sFHAPurposeIsStreamlineRefiWithAppr.Checked = dataLoan.sFHAPurposeIsStreamlineRefiWithAppr; 
            sFHAPurposeIsStreamlineRefiWithoutAppr.Checked = dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr; 
            sFHAPurposeIsConstToPerm.Checked = dataLoan.sFHAPurposeIsConstToPerm; 
            sFHAPurposeIsEnergyEfficientMortgage.Checked = dataLoan.sFHAPurposeIsEnergyEfficientMortgage; 
            sFHAPurposeIsBuildOnOwnLand.Checked = dataLoan.sFHAPurposeIsBuildOnOwnLand; 
            sFHAPurposeIsHudReo.Checked = dataLoan.sFHAPurposeIsHudReo; 
            sFHAPurposeIs203k.Checked = dataLoan.sFHAPurposeIs203k; 
            sFHAPurposeIsOther.Checked = dataLoan.sFHAPurposeIsOther; 

            sFHASecondaryFinancingIsGov.Checked = dataLoan.sFHASecondaryFinancingIsGov; 
            sFHASecondaryFinancingIsNP.Checked = dataLoan.sFHASecondaryFinancingIsNP; 
            sFHASecondaryFinancingIsFamily.Checked = dataLoan.sFHASecondaryFinancingIsFamily; 
            sFHASecondaryFinancingIsOther.Checked = dataLoan.sFHASecondaryFinancingIsOther; 

            sFHAIsSellerFundDAP.Checked = dataLoan.sFHAIsSellerFundDAP; 
            sFHAGift1IsGov.Checked = dataLoan.sFHAGift1IsGov; 
            sFHAGift1IsNP.Checked = dataLoan.sFHAGift1IsNP; 
            sFHAGift1IsFamily.Checked = dataLoan.sFHAGift1IsFamily; 
            sFHAGift1IsOther.Checked = dataLoan.sFHAGift1IsOther; 
            sFHAGift2IsGov.Checked = dataLoan.sFHAGift2IsGov; 
            sFHAGift2IsNP.Checked = dataLoan.sFHAGift2IsNP; 
            sFHAGift2IsFamily.Checked = dataLoan.sFHAGift2IsFamily; 
            sFHAGift2IsOther.Checked = dataLoan.sFHAGift2IsOther; 

            sFHARiskClassAA.Checked = dataLoan.sFHARiskClassAA; 
            sFHARiskClassRefer.Checked = dataLoan.sFHARiskClassRefer;

            sFHASecondaryFinancingSource.Text = dataLoan.sFHASecondaryFinancingSource;
            sFHASecondaryFinancingOtherDesc.Text = dataLoan.sFHASecondaryFinancingOtherDesc; 
            sFHASecondaryFinancingAmt.Text = dataLoan.sFHASecondaryFinancingAmt_rep; 


            sFHAGift1Source.Text = dataLoan.sFHAGift1Source;

            sFHAGift1OtherDesc.Text = dataLoan.sFHAGift1OtherDesc;
            sFHAGift1gAmt.Text = dataLoan.sFHAGift1gAmt_rep; 

            sFHAGift2Source.Text = dataLoan.sFHAGift2Source;
            sFHAGift2OtherDesc.Text = dataLoan.sFHAGift2OtherDesc;
            sFHAGift2gAmt.Text = dataLoan.sFHAGift2gAmt_rep; 

            sFHAArmIndex.Text = dataLoan.sFHAArmIndex;
            sChumsIdReviewerAppraisal.Text = dataLoan.sChumsIdReviewerAppraisal;

            Tools.Set_TriState(sFHAScoreByTotalTri, dataLoan.sFHAScoreByTotalTri);
            Tools.Set_TriState(sFHABLdpGsaTri, dataLoan.sFHABLdpGsaTri);
            Tools.Set_TriState(sFHACLdpGsaTri, dataLoan.sFHACLdpGsaTri);

        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
    }
}
