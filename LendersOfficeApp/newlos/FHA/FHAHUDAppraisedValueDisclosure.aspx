<%@ Page language="c#" Codebehind="FHAHUDAppraisedValueDisclosure.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHAHUDAppraisedValueDisclosure" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>FHAHUDAppraisedValueDisclosure</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" class="RightBackground">
    <script type="text/javascript">
<!--
  var oRolodex = null;  
  function _init() {
    if (null == oRolodex)
      oRolodex = new cRolodex();
  }
//-->
</script>

    <form id="FHAHUDAppraisedValueDisclosure" method="post" runat="server">
<table id=Table1 cellspacing=0 cellpadding=0 border=0>
  <tr>
    <td nowrap class=MainRightHeader>FHA HUD Appraised Value Disclosure</td></tr>
  <tr>
    <td nowrap>
      <table id=Table2 cellspacing=0 cellpadding=0 width="98%" border=0 class=InsetBorder>
        <tr>
          <td nowrap>
            <table id=Table3 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td nowrap class=FieldLabel>Lender</td>
                <td nowrap>
                    <uc:CFM id="CFM" runat="server" Type="21" CompanyNameField="FHAAppraisedValueDisclosureCompanyName" StreetAddressField="FHAAppraisedValueDisclosureStreetAddr" CityField="FHAAppraisedValueDisclosureCity" StateField="FHAAppraisedValueDisclosureState" ZipField="FHAAppraisedValueDisclosureZip" CompanyPhoneField="FHAAppraisedValueDisclosurePhoneOfCompany"></uc:CFM>
                </td></tr>
              <tr>
                <td nowrap class=FieldLabel>Name</td>
                <td nowrap><asp:textbox id=FHAAppraisedValueDisclosureCompanyName runat="server" width="285px"></asp:textbox></td></tr>
              <tr>
                <td nowrap class=FieldLabel>Address</td>
                <td nowrap><asp:textbox id=FHAAppraisedValueDisclosureStreetAddr runat="server" width="285px"></asp:textbox></td></tr>
              <tr>
                <td nowrap></td>
                <td nowrap><asp:textbox id=FHAAppraisedValueDisclosureCity runat="server" width="185px"></asp:textbox><ml:statedropdownlist id=FHAAppraisedValueDisclosureState runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=FHAAppraisedValueDisclosureZip runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></td></tr>
              <tr>
                <td nowrap class=FieldLabel>Phone</td>
                <td nowrap><ml:phonetextbox id=FHAAppraisedValueDisclosurePhoneOfCompany runat="server" width="120" preset="phone"></ml:phonetextbox></td></tr></table></td></tr></table></td></tr></table><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>
     </form>
  </body>
</html>
