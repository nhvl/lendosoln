using System;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
	public partial class FHACreditAnalysisRefinanceCombine : BaseLoanPage
	{
        #region Protected member variables
        protected MeridianLink.CommonControls.MoneyTextBox aFHAAssetAvail;
        #endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHACreditAnalysisRefinanceCombine));
            dataLoan.InitLoad();

            sFHADebtLckd.Checked = dataLoan.sFHADebtLckd;
            sFHANegCfRentalI.Text = dataLoan.sFHANegCfRentalI_rep;
            sCombinedBorInfoLckd.Checked = dataLoan.sCombinedBorInfoLckd;
            sCombinedBorFirstNm.Text = dataLoan.sCombinedBorFirstNm;
            sCombinedBorLastNm.Text = dataLoan.sCombinedBorLastNm;
            sCombinedBorMidNm.Text = dataLoan.sCombinedBorMidNm;
            sCombinedBorSuffix.Text = dataLoan.sCombinedBorSuffix;
            sCombinedBorSsn.Text = dataLoan.sCombinedBorSsn;
            sCombinedCoborFirstNm.Text = dataLoan.sCombinedCoborFirstNm;
            sCombinedCoborLastNm.Text = dataLoan.sCombinedCoborLastNm;
            sCombinedCoborMidNm.Text = dataLoan.sCombinedCoborMidNm;
            sCombinedCoborSuffix.Text = dataLoan.sCombinedCoborSuffix;
            sCombinedCoborSsn.Text = dataLoan.sCombinedCoborSsn;
            sCombinedCoborUsing2ndAppBorr.Checked = dataLoan.sCombinedCoborUsing2ndAppBorr;

            Tools.SetDropDownListValue(sLT, dataLoan.sLT);

            if (Tools.ShouldBeReadonly_sLT(dataLoan.sLPurposeT))
            {
                Tools.ReadonlifyDropDown(sLT);
            }

            sFHARefinanceTypeDesc.Text = dataLoan.sFHARefinanceTypeDesc;
            sFHACreditAnalysisRemarks.Text = dataLoan.sFHACreditAnalysisRemarks;
            
            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisRefinanceUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHACreditAnalysisRefinancePreparerName.Text = underwriter.PreparerName;
            FHACreditAnalysisRefinanceLicenseNumOfAgent.Text = underwriter.LicenseNumOfAgent;

            Tools.SetDropDownListValue(sFHAConstructionT, dataLoan.sFHAConstructionT);
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            sFHAHousingActSection.Text = dataLoan.sFHAHousingActSection;
            sLAmt.Text = dataLoan.sLAmtCalc_rep;
            sPresLTotPersistentHExp.Text = dataLoan.sPresLTotHExp_rep;
            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sBuydownResultIR.Text = dataLoan.sBuydownResultIR_rep;
            sApprVal.Text = dataLoan.sApprVal_rep;
            sFHACcTot.Text = dataLoan.sFHACcTot_rep;
            sFHACcPbs.Text = dataLoan.sFHACcPbs_rep;
            sFHACcPbb.Text = dataLoan.sFHACcPbb_rep;
            sFHACcPbb2.Text = dataLoan.sFHACcPbb_rep;
            sPurchPrice2.Text = dataLoan.sFHAExistingMLien_rep;
            sPurchPrice6pc.Text = dataLoan.sPurchPrice6pc_rep;
            sFHASellerContribution.Text = dataLoan.sFHASellerContribution_rep;
            sFHAExcessContribution.Text = dataLoan.sFHAExcessContribution_rep;
            sFHAExistingMLien.Text = dataLoan.sFHAExistingMLien_rep;
            sFHAMBasisRefin.Text = dataLoan.sFHAMBasisRefin_rep;
            sFHAMBasisRefinMultiply.Text = dataLoan.sFHAMBasisRefinMultiply_rep;
            sFHAApprValMultiply.Text = dataLoan.sFHAApprValMultiply_rep;
            sFHAMBasisRefinMultiplyLckd.Checked = dataLoan.sFHAMBasisRefinMultiplyLckd;
            sFHAApprValMultiplyLckd.Checked = dataLoan.sFHAApprValMultiplyLckd;
            sFHAReqInvestment.Text = dataLoan.sFHAReqInvestment_rep;
            sFHAReqTot.Text = dataLoan.sFHAReqTot_rep;
            sFHAAmtPaid.Text = dataLoan.sFHAAmtPaid_rep;
            sFHAIsAmtPdInOther.Checked = dataLoan.sFHAIsAmtPdInOther;
            sFHAIsAmtPdInCash.Checked = dataLoan.sFHAIsAmtPdInCash;
            sFHAIsAmtToBePdInOther.Checked = dataLoan.sFHAIsAmtToBePdInOther;
            sFHAIsAmtToBePdInCash.Checked = dataLoan.sFHAIsAmtToBePdInCash;
            sTermInYr.Text = dataLoan.sTermInYr_rep;
            sFHAImprovementsDesc.Text = dataLoan.sFHAImprovementsDesc;
            sFHAImprovements.Text = dataLoan.sFHAImprovements_rep;
            sFHASalesConcessions.Text = dataLoan.sFHASalesConcessions_rep;
            sLAmt2.Text = dataLoan.sLAmtCalc_rep;
            sFHADiscountPoints.Text = dataLoan.sFHADiscountPoints_rep;
            sFHAPrepaidExp.Text = dataLoan.sFHAPrepaidExp_rep;
            sUfCashPd.Text = dataLoan.sUfCashPd_rep;
            sFHANonrealty.Text = dataLoan.sFHANonrealty_rep;
            sFHANonrealtyLckd.Checked = dataLoan.sFHANonrealtyLckd;
            sFHAAssetAvail.Text = dataLoan.sFHAAssetAvail_rep;
            sFHA2ndMAmt.Text = dataLoan.sFHA2ndMAmt_rep;
            sFHABBaseI.Text = dataLoan.sFHABBaseI_rep;
            sFHABOI.Text = dataLoan.sFHABOI_rep;
            sFHACBaseI.Text = dataLoan.sFHACBaseI_rep;
            sFHACOI.Text = dataLoan.sFHACOI_rep;
            sFHANetRentalI.Text = dataLoan.sFHANetRentalI_rep;
            sFHAGrossMonI.Text = dataLoan.sFHAGrossMonI_rep;
            sFHADebtInstallPmt.Text = dataLoan.sFHADebtInstallPmt_rep;
            sFHADebtInstallBal.Text = dataLoan.sFHADebtInstallBal_rep;
            sFHAChildSupportPmt.Text = dataLoan.sFHAChildSupportPmt_rep;
            sFHAOtherDebtPmt.Text = dataLoan.sFHAOtherDebtPmt_rep;
            sFHAOtherDebtBal.Text = dataLoan.sFHAOtherDebtBal_rep;
            sFHADebtPmtTot.Text = dataLoan.sFHADebtPmtTot_rep;
            sFHAPro1stMPmt.Text = dataLoan.sFHAPro1stMPmt_rep;
            sFHAProMIns.Text = dataLoan.sFHAProMIns_rep;
            sFHAProMInsLckd.Checked = dataLoan.sFHAProMInsLckd;
            sFHAProHoAssocDues.Text = dataLoan.sFHAProHoAssocDues_rep;
            sFHAProGroundRent.Text = dataLoan.sFHAProGroundRent_rep;
            sFHAPro2ndFinPmt.Text = dataLoan.sFHAPro2ndFinPmt_rep;
            sFHAProHazIns.Text = dataLoan.sFHAProHazIns_rep;
            sFHAProRealETx.Text = dataLoan.sFHAProRealETx_rep;
            sFHAMonthlyPmt.Text = dataLoan.sFHAMonthlyPmt_rep;
            sFHADebtPmtTot2.Text = dataLoan.sFHADebtPmtTot_rep;
            sFHAPmtFixedTot.Text = dataLoan.sFHAPmtFixedTot_rep;
            sFHALtv2.Text = dataLoan.sFHALtvRefi_rep;
            sFHAMPmtToIRatio.Text = dataLoan.sFHAMPmtToIRatio_rep;
            sFHAFixedPmtToIRatio.Text = dataLoan.sFHAFixedPmtToIRatio_rep;
			sFHACreditRating.Text = dataLoan.sFHACreditRating;
            sFHARatingIAdequacy.Text = dataLoan.sFHARatingIAdequacy; 
            sFHARatingIStability.Text = dataLoan.sFHARatingIStability; 
            sFHARatingAssetAdequacy.Text = dataLoan.sFHARatingAssetAdequacy; 
            sFHABCaivrsNum.Text = dataLoan.sFHABCaivrsNum;
            sFHACCaivrsNum.Text = dataLoan.sFHACCaivrsNum;
            sFHABLpdGsa.Text = dataLoan.sFHABLpdGsa;
            sFHACLpdGsa.Text = dataLoan.sFHACLpdGsa;
            sFHAGiftAmtTot.Text = dataLoan.sFHAGiftAmtTot_rep;
            sFHAAmtToBePd.Text = dataLoan.sFHAAmtToBePd_rep;
            sUfCashPdzzz2.Text = dataLoan.sUfCashPd_rep;
            sFfUfmipR.Text = dataLoan.sFfUfmipR_rep;
            sFHAEnergyEffImprov.Text = dataLoan.sFHAEnergyEffImprov_rep;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
            sMipPiaMon.Text = dataLoan.sMipPiaMon_rep;
            sFfUfmip1003Lckd.Checked = dataLoan.sFfUfmip1003Lckd;
            sFHAIncomeLckd.Checked = dataLoan.sFHAIncomeLckd;


        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            Tools.Bind_sFHAHousingActSection(sFHAHousingActSection);
            Tools.Bind_sFHARefinanceTypeDesc(sFHARefinanceTypeDesc);

            this.PDFPrintClass = typeof(LendersOffice.Pdf.CHUD_92900_WS_CombinedPDF);
            this.PageTitle = "FHA MCAW Refi Combined";
            this.PageID = "FHA_92900_WS_Combined";

            Tools.Bind_sLT(sLT);
            Tools.Bind_sFHAConstructionT(sFHAConstructionT);


        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}
}
