<%@ Page language="c#" Codebehind="FHAHud92900Lt.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHAHud92900Lt" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess"%>
<%@ Register TagPrefix="QRP" TagName="QualRatePopup" Src="~/newlos/Forms/QualRateCalculationPopup.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>FHAHud92900Lt</title>
  </HEAD>
<body bgColor=gainsboro scroll=yes MS_POSITIONING="FlowLayout" margin="0">
<script type="text/javascript">
var lPurposeWasOther = false;
function _init() {
    lockField(<%= AspxTools.JsGetElementById(sTransmFntcLckd) %>, 'sTransmFntc');
    lockField(<%= AspxTools.JsGetElementById(sFHAProMInsLckd) %>, 'sFHAProMIns');
    lockField(<%=AspxTools.JsGetElementById(this.sQualIRLckd)%>, 'sQualIR');
      <% if (IsReadOnly) { %>
        document.getElementById("btnCopyFromLoanApp").disabled = true;
        document.getElementById("btnCopyFromGFE").disabled = true;
      <% } %>

    updateLoanPurposeCheckboxes(true);
    enableDisableOtherDesc('sFHASecondaryFinancing');
    enableDisableOtherDesc('sFHAGift1');
    enableDisableOtherDesc('sFHAGift2');
    f_onChangeFinMethT();

    document.getElementById("sFHARiskClassAA").onclick = function(){setOpposite("AA");};
    document.getElementById("sFHARiskClassRefer").onclick = function(){setOpposite("Refer");};

    SetQualTermReadonly();
}

    function SetQualTermReadonly() {
        var isManual = $(".sQualTermCalculationType").val() == '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Manual)%>';
        $(".sQualTerm").prop("readonly", !isManual);
    }

    function setOpposite(name)
    {
        var referEl = document.getElementById("sFHARiskClassRefer");
        var aaEl = document.getElementById("sFHARiskClassAA");
        if(name=="Refer" && referEl.checked)
        {
            aaEl.checked = false;
        }else if(aaEl.checked){
            referEl.checked = false;
        }

        refreshCalculation();
    }

    function updateLoanPurposeCheckboxes(bIsInit) {
      var sFHAPurposeIsPurchase = document.getElementById('sFHAPurposeIsPurchase');
      var sFHAPurposeIsNoCashoutRefi = document.getElementById('sFHAPurposeIsNoCashoutRefi');
      var sFHAPurposeIsCashoutRefi = document.getElementById('sFHAPurposeIsCashoutRefi');
      var sFHAPurposeIsStreamlineRefi = document.getElementById('sFHAPurposeIsStreamlineRefi');
      var sFHAPurposeIsStreamlineRefiWithAppr = document.getElementById('sFHAPurposeIsStreamlineRefiWithAppr');
      var sFHAPurposeIsStreamlineRefiWithoutAppr = document.getElementById('sFHAPurposeIsStreamlineRefiWithoutAppr');
      var sFHAPurposeIsConstToPerm = document.getElementById('sFHAPurposeIsConstToPerm');
      var sFHAPurposeIsOther = document.getElementById('sFHAPurposeIsOther');

      if(lPurposeWasOther)
        sFHAPurposeIsOther.checked = false;
      lPurposeWasOther = false;
      sFHAPurposeIsOther.disabled = false;
      sFHAPurposeIsPurchase.checked = false;
      sFHAPurposeIsNoCashoutRefi.checked = false;
      sFHAPurposeIsCashoutRefi.checked = false;
      sFHAPurposeIsStreamlineRefi.checked = false;
      sFHAPurposeIsConstToPerm.checked = false;

      var lPurposeDDL = <%= AspxTools.JsGetElementById(sLPurposeT) %>;
      var lPurpose = lPurposeDDL.options[lPurposeDDL.selectedIndex].value;

      if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Purchase)) %>)
        sFHAPurposeIsPurchase.checked = true;
      else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Refin)) %>)
        sFHAPurposeIsNoCashoutRefi.checked = true;
      else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.RefinCashout)) %>)
        sFHAPurposeIsCashoutRefi.checked = true;
      else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.ConstructPerm)) %>)
        sFHAPurposeIsConstToPerm.checked = true;
      else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.FhaStreamlinedRefinance)) %> ||
            lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.VaIrrrl)) %>)
      {
        sFHAPurposeIsStreamlineRefi.checked = true;

        sFHAPurposeIsStreamlineRefiWithAppr.disabled = false;
        sFHAPurposeIsStreamlineRefiWithoutAppr.disabled = false;
        if (! bIsInit )
        {
            var appraisedVal = <%= AspxTools.JsGetElementById(sApprVal) %>.value;
            var bDefaultNoAppr = (appraisedVal == '' || appraisedVal == '$0.00');

            sFHAPurposeIsStreamlineRefiWithAppr.checked = !bDefaultNoAppr;
            sFHAPurposeIsStreamlineRefiWithoutAppr.checked = bDefaultNoAppr;
        }
      }
      else if ((lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Other)) %>)
            || (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Construct)) %>))
      {
        sFHAPurposeIsOther.disabled = true;
        sFHAPurposeIsOther.checked = true;
        lPurposeWasOther = true;
      }

      if (lPurpose != <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.FhaStreamlinedRefinance)) %> &&
        lPurpose != <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.VaIrrrl)) %>)
      {
        sFHAPurposeIsStreamlineRefiWithAppr.checked = false;
        sFHAPurposeIsStreamlineRefiWithoutAppr.checked = false;
        sFHAPurposeIsStreamlineRefiWithAppr.disabled = true;
        sFHAPurposeIsStreamlineRefiWithoutAppr.disabled = true;
      }
    }

    function toggleStreamlineRefinanceOption(checkbox)
    {
      var oWithAppr = document.getElementById('sFHAPurposeIsStreamlineRefiWithAppr');
      var oWithoutAppr = document.getElementById('sFHAPurposeIsStreamlineRefiWithoutAppr');

      if(checkbox.checked) {
        oWithAppr.checked = false;
        oWithoutAppr.checked = false;
        checkbox.checked = true;
      }
      else // unchecking situation
      {
        <%-- // Act like radio button group. If this unchecking will make it so nothing is checked, do not allow. --%>
        if ( oWithAppr.checked == false && oWithoutAppr.checked == false )
         checkbox.checked = true;
      }
    }

    function verifySecondaryFinancingSourceCheckboxes(checkbox) {
      var id = checkbox.id;
      var group = id.substring(0, id.indexOf('Is'));
      if(checkbox.checked) {
        document.getElementById(group + 'IsGov').checked = false;
        document.getElementById(group + 'IsNP').checked = false;
        document.getElementById(group + 'IsFamily').checked = false;
        document.getElementById(group + 'IsOther').checked = false;
        checkbox.checked = true;
      }

      enableDisableOtherDesc(group);
    }

    function enableDisableOtherDesc(group) {
      var otherDesc = document.getElementById(group + 'OtherDesc');
      if(document.getElementById(group + 'IsOther').checked){
        otherDesc.readOnly = "";
        otherDesc.style.backgroundColor = "white";
        otherDesc.disabled = false;
      }
      else {
        otherDesc.readOnly = "readonly";
        otherDesc.disabled = true;
        otherDesc.innerText = "";
      }
    }

function handlingError(result)
{
  var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
  if (result.ErrorType === 'VersionMismatchException')
  {
      f_displayVersionMismatch();
  }
  else if (result.ErrorType === 'LoanFieldWritePermissionDenied')
  {
    f_displayFieldWriteDenied(result.UserMessage);
  }
  else
  {
    alert(errMsg);
  }

}

    function copyFromGFE() {
      PolyShouldShowConfirmSave(isDirty(), function(confirmed){
        var args = getAllFormValues();

        if (typeof _postGetAllFormValues === 'function')
        {
            _postGetAllFormValues(args);
        }

        args["loanid"] = ML.sLId;
        args["applicationid"] = ML.aAppId;
        args["issave"] =  confirmed ? 'T' : 'F';
        var result = gService.loanedit.call("CopyFromGFE", args);
        if (!result.error) {
          populateForm(result.value); // Refresh value;
          _init();
          clearDirty();
        }
        else
        {
          handlingError(result);
        }
      });
    }
    function copyFromLoanApp() {
      PolyShouldShowConfirmSave(isDirty(), function(confirmed){
        var args = getAllFormValues();

        if (typeof _postGetAllFormValues === 'function')
        {
            _postGetAllFormValues(args);
        }

        args["loanid"] = ML.sLId;
        args["applicationid"] = ML.aAppId;
        args["issave"] = confirmed ? 'T' : 'F';
        var result = gService.loanedit.call("CopyFromLoanApp", args);
        if (!result.error) {
          populateForm(result.value); // Refresh value;
          _init();
          clearDirty();
        } else {
          handlingError(result);
        } 
      });
    }
    function f_populateFromAsset()
    {
     var args = new Object();
     args["LoanID"] = ML.sLId;

     var result = gService.loanedit.call("CalculateAssetTotal", args);
     if (!result.error) {
     <%= AspxTools.JsGetElementById(sVerifAssetAmt) %>.value = result.value["VerifiedAssetTotal"];
     updateDirtyBit();
     } else {
      handlingError(result);
     }
    }
    function f_onChangeFinMethT() {
	    var ddl = <%= AspxTools.JsGetElementById(sFinMethT) %>;
	    var bIsArm = ddl[ddl.selectedIndex].text == "ARM";
	    <%= AspxTools.JsGetElementById(sRAdj1stCapMon) %>.readOnly = !bIsArm;
	    <%= AspxTools.JsGetElementById(sFHAArmIndex) %>.readOnly = !bIsArm;
	    <%= AspxTools.JsGetElementById(sRAdjMarginR) %>.readOnly = !bIsArm;
	}

	function manualEdit()
    {
	    var elem = document.getElementById("sFHA92900LtUnderwriterChumsIdLckd");
        elem.checked = true;
    }
</script>

<form id=FHAHud92900Lt method=post runat="server">
<QRP:QualRatePopup ID="QualRatePopup" runat="server" />
<asp:CheckBox runat="server" style="display:none;" ID="sFHA92900LtUnderwriterChumsIdLckd" />
<table class=FormTable cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class=MainRightHeader noWrap>FHA Loan
      Underwriting and Transmittal Summary (HUD-92900-LT)</td></tr>
  <tr>
    <td style="PADDING-LEFT: 10px" noWrap>
      <table cellSpacing=0 cellPadding=0 width=770 border=0>
        <TR>
          <TD style="PADDING-LEFT: 5px"><INPUT id="btnCopyFromLoanApp" type=button value="Copy Data From Loan App" onclick="copyFromLoanApp();" style="background-color:Yellow">
          <INPUT id="btnCopyFromGFE" type=button value="Copy Closing Cost Data to Form" onclick="copyFromGFE();" style="background-color:Yellow"></TD></TR>
        <tr>
          <td class=LoanFormHeader style="PADDING-LEFT: 5px"
          >Borrower and Property Information</td></tr>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <table class=InsetBorder cellSpacing=0 cellPadding=0 width="98%"
            >
              <tr>
                <td>
                  <TABLE id=Table1 cellSpacing=0 cellPadding=0 width="98%"
                  border=0>
                    <TR>
                      <TD class=FieldLabel noWrap>Agency
                        Case Number</TD>
                      <TD noWrap><asp:textbox id=sAgencyCaseNum runat="server"></asp:textbox></TD>
                      <td style="WIDTH: 20px"></td>
                      <TD class=FieldLabel noWrap>Section
                        of the Act</TD>
                      <TD noWrap><ml:combobox id=sFHAHousingActSection runat="server"></ml:combobox></TD></TR></TABLE></td></tr></table></TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <table class=InsetBorder cellSpacing=0 cellPadding=0 width="98%"
            border=0>
              <tr>
                <td>
                  <TABLE cellSpacing=0 cellPadding=0 border=0
                  >
                    <TR>
                      <TD class=FieldLabel noWrap >Borrower</TD>
                      <TD noWrap></TD>
                      <TD class=FieldLabel noWrap></TD>
                      <TD class=FieldLabel noWrap >Co-borrower</TD>
                      <TD noWrap></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>First
                        Name</TD>
                      <TD noWrap><asp:textbox id=aBFirstNm runat="server"></asp:textbox></TD>
                      <TD class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp; </TD>
                      <TD class=FieldLabel noWrap>First
                        Name</TD>
                      <TD noWrap><asp:textbox id=aCFirstNm runat="server" tabIndex=5></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Middle
                        Name</TD>
                      <TD noWrap><asp:textbox id=aBMidNm runat="server"></asp:textbox></TD>
                      <TD class=FieldLabel noWrap></TD>
                      <TD class=FieldLabel noWrap>Middle
                        Name</TD>
                      <TD noWrap><asp:textbox id=aCMidNm runat="server" tabIndex=5></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Last
                        Name</TD>
                      <TD noWrap><asp:textbox id=aBLastNm runat="server"></asp:textbox></TD>
                      <TD class=FieldLabel noWrap></TD>
                      <TD class=FieldLabel noWrap>Last
                        Name</TD>
                      <TD noWrap><asp:textbox id=aCLastNm runat="server" tabIndex=5></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap >Suffix</TD>
                      <TD noWrap><ml:ComboBox id=aBSuffix runat="server"></ml:ComboBox></TD>
                      <TD class=FieldLabel noWrap></TD>
                      <TD class=FieldLabel noWrap >Suffix</TD>
                      <TD noWrap><ml:ComboBox id=aCSuffix runat="server" tabIndex=5></ml:ComboBox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>SSN</TD>
                      <TD noWrap><ml:ssntextbox id=aBSsn runat="server" width="75px" preset="ssn"></ml:ssntextbox></TD>
                      <TD class=FieldLabel noWrap></TD>
                      <TD class=FieldLabel noWrap>SSN</TD>
                      <TD noWrap><ml:ssntextbox id=aCSsn runat="server" width="75px" preset="ssn" tabIndex=5></ml:ssntextbox></TD></TR></TABLE></td></tr></table></TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <table class=InsetBorder cellSpacing=0 cellPadding=0 width="98%"
            border=0>
              <tr>
                <td>
                  <table cellSpacing=0 cellPadding=0>
                    <tr>
                      <td class=FieldLabel>Property
                        Address</td>
                      <td><asp:textbox id=sSpAddr tabIndex=10 runat="server" Width="265px"></asp:textbox></td></tr>
                    <tr>
                      <td></td>
                      <td><asp:textbox id=sSpCity tabIndex=10 runat="server" Width="166px"></asp:textbox><ml:statedropdownlist id=sSpState tabIndex=10 runat="server" IncludeTerritories="false"></ml:statedropdownlist><ml:zipcodetextbox id=sSpZip tabIndex=10 runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></td></tr></table></td></tr></table></TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <TABLE class=InsetBorder id=Table9 cellSpacing=0 cellPadding=0
            width="98%" border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table10 cellSpacing=0 cellPadding=0 border=0
                  >
                    <TR>
                      <TD class=FieldLabel noWrap >Property Type</TD>
                      <TD noWrap><asp:dropdownlist id=sGseSpT runat="server" tabIndex=10></asp:dropdownlist></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Number
                        Of Units</TD>
                      <TD noWrap><asp:textbox id=sUnitsNum runat="server" Width="49px" tabIndex=10></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Sales
                        Price</TD>
                      <TD noWrap><ml:moneytextbox id=sPurchPrice runat="server" width="90" preset="money" tabIndex=10></ml:moneytextbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap >Appraised Price</TD>
                      <TD noWrap><ml:moneytextbox id=sApprVal runat="server" width="90" preset="money" tabIndex=10></ml:moneytextbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap >Property Right</TD>
                      <TD noWrap><asp:dropdownlist id=sEstateHeldT runat="server" tabIndex=10></asp:dropdownlist></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap >Construction</TD>
                      <TD noWrap><asp:dropdownlist id=sFHAConstructionT runat="server" tabIndex=10></asp:dropdownlist></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
        <TR>
          <TD class=LoanFormHeader style="PADDING-LEFT: 5px"
          >Mortgage Information</TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <TABLE id=Table2 cellSpacing=0 cellPadding=0 width="98%" border=0
            >
              <TR>
                <TD style="PADDING-RIGHT: 5px; WIDTH: 33%" vAlign=top noWrap >
                  <TABLE class=InsetBorder id=Table3 cellSpacing=0 cellPadding=0
                  width="99%" border=0>
                    <TR>
                      <TD noWrap>
                        <TABLE id=Table8 cellSpacing=0 cellPadding=0 border=0
                        >
                          <TR>
                            <TD class=FieldLabel noWrap >Amortization Type</TD>
                            <TD noWrap><asp:dropdownlist id=sFinMethT runat="server" onchange="f_onChangeFinMethT();" tabIndex=15></asp:dropdownlist></TD></TR>
                            <TR>
                            <TD class=FieldLabel noWrap >1st Rate Change</TD>
                            <TD noWrap><asp:textbox id="sRAdj1stCapMon" Width="50px" runat="server" tabIndex=15></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >ARM Index</TD>
                            <TD noWrap><asp:textbox id=sFHAArmIndex runat="server" tabIndex=15></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >ARM Margin</TD>
                            <TD noWrap><ml:percenttextbox id=sRAdjMarginR runat="server" width="70" preset="percent" tabIndex=15></ml:percenttextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sBuydown runat="server" Text="Is Buydown" tabIndex=15></asp:checkbox></TD>
                            <TD noWrap></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Loan Information</TD>
                            <TD noWrap></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Mortgage w/o UFMIP</TD>
                            <TD noWrap><ml:moneytextbox id=sLAmtCalc runat="server" width="90" preset="money" ReadOnly="True" tabIndex=15></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Total UFMIP</TD>
                            <TD noWrap><ml:moneytextbox id=sFfUfmipFinanced runat="server" width="90" preset="money" ReadOnly="True" tabIndex=15></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Mortgage w/ UFMIP</TD>
                            <TD noWrap><ml:moneytextbox id=sFinalLAmt runat="server" width="90" preset="money" ReadOnly="True" tabIndex=15></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Interest Rate</TD>
                            <TD noWrap><ml:percenttextbox id=sNoteIR runat="server" width="70" preset="percent" tabIndex=15  onchange="refreshCalculation();" ></ml:percenttextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap ><a id="QualRateCalcLink">Qualifying Rate</a></TD>
                            <TD noWrap>
                                <ml:percenttextbox id=sQualIR runat="server" width="70" preset="percent" tabIndex=15></ml:percenttextbox>
                                <asp:CheckBox ID="sQualIRLckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
                            </TD>
                          </TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Term</TD>
                            <TD noWrap><asp:textbox id=sTerm runat="server" Width="49px" tabIndex=15></asp:textbox></TD>
                          </TR>

                            <tr>
                                <td class="FieldLabel" noWrap >Qualifying Term</td>
                                <td>
                                    <asp:DropDownList id="sQualTermCalculationType" runat="server" onchange="refreshCalculation();" CssClass="sQualTermCalculationType" />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="FieldLabel">
                                    <input type="text" id="sQualTerm" runat="server" preset="numeric" onchange="refreshCalculation();" class="sQualTerm" />
                                </td>
                            </tr>
                        </TABLE>
                      </TD>
                    </TR>
                  </TABLE>
                </TD>
                <TD style="PADDING-RIGHT: 5px; WIDTH: 33%" vAlign=top noWrap >
                  <TABLE class=InsetBorder id=Table4 cellSpacing=0 cellPadding=0
                  width="99%" border=0>
                    <TR>
                      <TD noWrap>
                        <TABLE id=Table7 cellSpacing=0 cellPadding=0 border=0
                        >
                          <TR>
                            <TD class=FieldLabel noWrap >Loan Purpose <asp:DropDownList ID="sLPurposeT" runat="server" onchange="updateLoanPurposeCheckboxes(false);"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsPurchase runat="server" Enabled="false" tabIndex=20></asp:checkbox>Purchase</TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsNoCashoutRefi runat="server" Enabled="false" tabIndex=20></asp:checkbox>No Cash-Out Refinance</TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsCashoutRefi runat="server" Enabled="false" tabIndex=20></asp:checkbox>Cash-Out Refinance</TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsStreamlineRefi runat="server" Enabled="false" tabIndex=20></asp:checkbox>Streamline Refinance</TD></TR>
                          <TR>
                            <TD class=FieldLabel style="PADDING-LEFT: 20px"
                            noWrap><asp:checkbox id=sFHAPurposeIsStreamlineRefiWithAppr runat="server" Text="w/ appraisal" onclick="toggleStreamlineRefinanceOption(this);" tabIndex=20></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel style="PADDING-LEFT: 20px"
                            noWrap><asp:checkbox id=sFHAPurposeIsStreamlineRefiWithoutAppr runat="server" Text="w/o appraisal" onclick="toggleStreamlineRefinanceOption(this);" tabIndex=20></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsConstToPerm runat="server" Enabled="false" tabIndex=20></asp:checkbox>Construction-to-Perm</TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsEnergyEfficientMortgage runat="server" Text="Energy Efficient Mortgage" tabIndex=20></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsBuildOnOwnLand runat="server" Text="Building On Own Land" tabIndex=20></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsHudReo runat="server" Text="HUD REO" tabIndex=20></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIs203k runat="server" Text="203(k)" tabIndex=20></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsOther runat="server" Text="Other" tabIndex=20></asp:checkbox></TD></TR></TABLE></TD></TR></TABLE></TD>
                <TD style="WIDTH: 33%" vAlign=top noWrap>
                  <TABLE class=InsetBorder id=Table5 cellSpacing=0 cellPadding=0
                  width="99%" border=0>
                    <TR>
                      <TD noWrap>
                        <TABLE id=Table6 cellSpacing=0 cellPadding=0 border=0
                        >
                          <TR>
                            <TD class=FieldLabel noWrap >Secondary Financing</TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Source/EIN <asp:textbox id=sFHASecondaryFinancingSource runat="server" tabIndex=25></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHASecondaryFinancingIsGov runat="server" Text="Gov't" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:checkbox id=sFHASecondaryFinancingIsNP onclick="verifySecondaryFinancingSourceCheckboxes(this);" runat="server" Text="NP" tabIndex=25></asp:checkbox><asp:checkbox id=sFHASecondaryFinancingIsFamily runat="server" Text="Family" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHASecondaryFinancingIsOther runat="server" Text="Other" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:textbox id=sFHASecondaryFinancingOtherDesc runat="server" tabIndex=25></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Amount <ml:moneytextbox id=sFHASecondaryFinancingAmt runat="server" width="90" preset="money" tabIndex=25></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Gifts&nbsp;&nbsp;&nbsp;
                              Seller Funded DAP<asp:checkbox id=sFHAIsSellerFundDAP runat="server" Text="Yes" tabIndex=25></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >1) Source/EIN <asp:textbox id=sFHAGift1Source runat="server" tabIndex=25></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAGift1IsGov runat="server" Text="Gov't" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:checkbox id=sFHAGift1IsNP runat="server" Text="NP" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:checkbox id=sFHAGift1IsFamily runat="server" Text="Family" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAGift1IsOther runat="server" Text="Other" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:textbox id=sFHAGift1OtherDesc runat="server" tabIndex=25></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Amount <ml:moneytextbox id=sFHAGift1gAmt runat="server" width="90" preset="money" tabIndex=25></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD noWrap></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >2) Source/EIN <asp:textbox id=sFHAGift2Source runat="server" tabIndex=25></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAGift2IsGov runat="server" Text="Gov't" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:checkbox id=sFHAGift2IsNP runat="server" Text="NP" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:checkbox id=sFHAGift2IsFamily runat="server" Text="Family" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAGift2IsOther runat="server" Text="Other" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:textbox id=sFHAGift2OtherDesc runat="server" tabIndex=25></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Amount <ml:moneytextbox id=sFHAGift2gAmt runat="server" width="90" preset="money" tabIndex=25></ml:moneytextbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px" class=LoanFormHeader>Underwriting
            Information</TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <TABLE id=Table11 cellSpacing=0 cellPadding=0 width="98%"
              border=0>
              <TR>
                <TD width="50%" style="PADDING-RIGHT: 5px" vAlign=top>
                  <TABLE id=Table12 cellSpacing=0 cellPadding=0 width="100%"
                  border=0>
                    <TR>
                      <TD noWrap>
                        <TABLE id=Table13 cellSpacing=0 cellPadding=0
                        width="98%" border=0 class=InsetBorder>
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table14 cellSpacing=0 cellPadding=0
                              border=0>
                                <TR>
                                <TD class=FieldLabel noWrap>Monthly Income</TD>
                                <TD class=FieldLabel noWrap></TD>
                                <TD class=FieldLabel noWrap></TD>
                                <TD class=FieldLabel noWrap></TD></TR>
                                <TR>
                                <TD noWrap></TD>
                                <TD noWrap class=FieldLabel>Borrower</TD>
                                <TD noWrap class=FieldLabel>Co-Borrower</TD>
                                <TD noWrap class=FieldLabel>Total</TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Base Income</TD>
                                <TD noWrap><ml:moneytextbox id=aFHABBaseI runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=30></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=aFHACBaseI runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=30></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=aFHATotBaseI runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Other Income</TD>
                                <TD noWrap><ml:moneytextbox id=aFHABOI runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=30></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=aFHACOI runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=30></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=aFHATotOI runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Net Rental
                                Income</TD>
                                <TD noWrap><ml:moneytextbox id=aFHANetRentalI runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=30></ml:moneytextbox></TD>
                                <TD noWrap></TD>
                                <TD noWrap><ml:moneytextbox id=aFHANetRentalI2 runat="server" preset="money" width="90" ReadOnly="True" tabIndex=30></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Total Income</TD>
                                <TD noWrap><ml:moneytextbox id=aFHABTotI runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=aFHACTotI runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=aFHAGrossMonI runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD noWrap>
                        <TABLE class=InsetBorder id=Table15 cellSpacing=0 cellPadding=0 width="98%" border=0>
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table16 cellSpacing=0 cellPadding=0
                              border=0>
                                <TR>
                                <TD class=FieldLabel noWrap>Debts &amp;
                                Obligation</TD>
                                <TD class=FieldLabel noWrap></TD>
                                <TD class=FieldLabel noWrap></TD></TR>
                                <TR>
                                <TD noWrap></TD>
                                <TD class=FieldLabel noWrap>Monthly</TD>
                                <TD class=FieldLabel
noWrap>Unpaid Balance</TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Total
                                Installment</TD>
                                <TD noWrap><ml:moneytextbox id=aFHADebtInstallPmt runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=40></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=aFHADebtInstallBal runat="server" preset="money" width="90" tabIndex=40></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Child Support</TD>
                                <TD noWrap><ml:moneytextbox id=aFHAChildSupportPmt runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=40></ml:moneytextbox></TD>
                                <TD noWrap></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Negative Rental
                                Cashflow</TD>
                                <TD noWrap><ml:moneytextbox id=aFHANegCfRentalI runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=40></ml:moneytextbox></TD>
                                <TD noWrap></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Other Payments</TD>
                                <TD noWrap><ml:moneytextbox id=aFHAOtherDebtPmt runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=40></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=aFHAOtherDebtBal runat="server" preset="money" width="90" tabIndex=40></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Total Fixed
                                Payment</TD>
                                <TD noWrap><ml:moneytextbox id="aFHAPmtFixedTot" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
                                <TD
                        noWrap></TD></TR></TABLE></TD></TR></TABLE>
                        <TABLE id=Table17 cellSpacing=0 cellPadding=0
                        width="98%" border=0 class=InsetBorder>
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table18 cellSpacing=0 cellPadding=0
                              border=0>
                                <TR>
                                <TD class=FieldLabel noWrap>Borrower Funds to
                                Close</TD>
                                <TD noWrap></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Required <asp:CheckBox id=sTransmFntcLckd runat="server" Text="Locked" onclick="refreshCalculation();" tabIndex=45></asp:CheckBox></TD>
                                <TD noWrap><ml:MoneyTextBox id=sTransmFntc runat="server" preset="money" width="90" tabIndex=45></ml:MoneyTextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Verified Assets&nbsp;&nbsp;<INPUT style="WIDTH: 80px" onclick="f_populateFromAsset();" type="button" value="From Assets" NoHighlight>&nbsp;</TD>
                                <TD noWrap><ml:MoneyTextBox id=sVerifAssetAmt runat="server" preset="money" width="90" tabIndex=45></ml:MoneyTextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Closing Costs</TD>
                                <TD noWrap><ml:MoneyTextBox id=sFHACcTot runat="server" preset="money" width="90" tabIndex=45></ml:MoneyTextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Source Of Funds</TD>
                                <TD noWrap><asp:TextBox id=sFntcSrc runat="server" tabIndex=45></asp:TextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>No. Of Months In
                                Reserves</TD>
                                <TD noWrap><asp:TextBox id=sRsrvMonNumDesc runat="server" Width="69px" tabIndex=45></asp:TextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Seller Contribution
                                ($)</TD>
                                <TD
                          noWrap><ml:MoneyTextBox id=sFHASellerContribution runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=45></ml:MoneyTextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Seller Contribution
                                (%)</TD>
                                <TD noWrap><ml:PercentTextBox id=sFHASellerContributionPc runat="server" preset="percent" width="70" ReadOnly="True"></ml:PercentTextBox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD noWrap></TD></TR></TABLE></TD>
                <TD width="50%" vAlign=top>
                  <TABLE id=Table19 cellSpacing=0 cellPadding=0 width="100%"
                  border=0>
                    <TR>
                      <TD>
                        <TABLE class=InsetBorder id=Table20 cellSpacing=0
                        cellPadding=0 width="98%" border=0>
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table21 cellSpacing=0 cellPadding=0
                              border=0>
                                <TR>
                                <TD noWrap class=FieldLabel colspan="2">Proposed Monthly Payments</TD>
                                </TR>
                                <TR>
                                <TD noWrap class=FieldLabel colspan="2">Borrowers Primary Residence</TD>
                                </TR>
                                <TR>
                                <TD noWrap class=FieldLabel>First Mortgage P&amp;I</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAPro1stMPmt runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=35></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD noWrap class=FieldLabel>Monthly MIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="sFHAProMInsLckd" TabIndex="35" runat="server" onclick="refreshCalculation();" Text="Lock"/></TD>
                                <TD noWrap><ml:moneytextbox id=sFHAProMIns runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=35 /></TD></TR>
                                <TR>
                                <TD noWrap class=FieldLabel>HOA Fees</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAProHoAssocDues runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=35></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD noWrap class=FieldLabel>Lease/Ground Rent</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAProGroundRent runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=35></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD noWrap class=FieldLabel>Second Mortgage P&amp;I</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAPro2ndFinPmt runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=35></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD noWrap class=FieldLabel>Hazard Insurance</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAProHazIns runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=35></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD noWrap class=FieldLabel>Taxes &amp; Special Assessments</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAProRealETx runat="server" preset="money" width="90" onchange="refreshCalculation();" tabIndex=35></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD noWrap class=FieldLabel>Total Mortgage Payment</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAMonthlyPmt runat="server" preset="money" width="90" ReadOnly="True" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD>
                        <TABLE class=InsetBorder id=Table22 cellSpacing=0
                        cellPadding=0 width="98%" border=0>
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table23 cellSpacing=0 cellPadding=0
                              border=0>
                                <TR>
                                <TD class=FieldLabel noWrap>Qualifying
                                Ratios</TD>
                                <TD noWrap></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>LTV</TD>
                                <TD noWrap><ml:PercentTextBox id=sLtvR runat="server" preset="percent" width="70" ReadOnly="True"></ml:PercentTextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>CLTV</TD>
                                <TD noWrap><ml:PercentTextBox id=sCltvR runat="server" preset="percent" width="70" ReadOnly="True"></ml:PercentTextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>HCLTV</TD>
                                <TD noWrap><ml:PercentTextBox id=sHcltvR runat="server" preset="percent" width="70" ReadOnly="True"></ml:PercentTextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Mortgage
                                Payment-to-income</TD>
                                <TD noWrap><ml:PercentTextBox id=aFHAMPmtToIRatio runat="server" preset="percent" width="70" ReadOnly="True"></ml:PercentTextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>Total Fixed
                                Payment-to-income</TD>
                                <TD noWrap><ml:PercentTextBox id=aFHAFixedPmtToIRatio runat="server" preset="percent" width="70" ReadOnly="True"></ml:PercentTextBox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD>
                        <TABLE id=Table24 cellSpacing=0 cellPadding=0
                        width="98%" border=0 class=InsetBorder>
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table25 cellSpacing=0 cellPadding=0
                              border=0>
                                <TR>
                                <TD noWrap></TD>
                                <TD class=FieldLabel noWrap>Borrower</TD>
                                <TD class=FieldLabel
noWrap>Co-Borrower</TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>CAIVRS #</TD>
                                <TD noWrap><asp:TextBox id=aFHABCaivrsNum runat="server" tabIndex=50></asp:TextBox></TD>
                                <TD noWrap><asp:TextBox id=aFHACCaivrsNum runat="server" tabIndex=50></asp:TextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>LDP/GSA</TD>
                                <TD noWrap class=FieldLabel><asp:CheckBoxList id=aFHABLdpGsaTri runat="server" RepeatDirection="Horizontal" CssClass="FieldLabel" tabIndex=50></asp:CheckBoxList></TD>
                                <TD
                        noWrap class=FieldLabel><asp:CheckBoxList id=aFHACLdpGsaTri runat="server" RepeatDirection="Horizontal" CssClass="FieldLabel" tabIndex=50></asp:CheckBoxList></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px"
      class=FieldLabel>Underwriter Comment</TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px"><asp:TextBox id=sTransmUwerComments runat="server" Width="520px" TextMode="MultiLine" tabIndex=55 Height="150px"></asp:TextBox></TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <TABLE id=Table26 cellSpacing=0 cellPadding=0 border=0>
              <TR>
                <TD noWrap class=FieldLabel>CHUMS ID# </TD>
                <TD noWrap><asp:TextBox onchange="manualEdit();" id=sFHA92900LtUnderwriterChumsId runat="server" tabIndex=55></asp:TextBox></TD></TR>
              <TR>
                <TD noWrap class=FieldLabel>Scored By TOTAL</TD>
                <TD noWrap class=FieldLabel><asp:CheckBoxList id=sFHAScoreByTotalTri runat="server" RepeatDirection="Horizontal" CssClass="FieldLabel" tabIndex=55></asp:CheckBoxList></TD></TR>
              <TR>
                <TD noWrap class=FieldLabel>Risk Class</TD>
                <TD noWrap class=FieldLabel><asp:CheckBox id=sFHARiskClassAA runat="server" Text="A/A" tabIndex=55></asp:CheckBox><asp:CheckBox id=sFHARiskClassRefer runat="server" Text="Refer" tabIndex=55></asp:CheckBox></TD></TR>
              <TR>
                <TD noWrap class="FieldLabel">CHUMS ID # for Reviewer of appraisal</TD>
                <TD noWrap><asp:TextBox id="sChumsIdReviewerAppraisal" runat="server" tabIndex="55"></asp:TextBox></TD></TR></TABLE></TD></TR>
        <TR>
          <TD
style="PADDING-LEFT: 5px"></TD></TR></table></td></tr></table></form>

  </body>
</HTML>
