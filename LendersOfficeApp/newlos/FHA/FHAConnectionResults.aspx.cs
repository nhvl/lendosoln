﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.FHA
{
    public partial class FHAConnectionResults : BaseLoanPage
    {
#if LQB_NET45
        protected global::System.Web.UI.HtmlControls.HtmlIframe CAIVRSAuthorizationIFrame;
        protected global::System.Web.UI.HtmlControls.HtmlIframe CaseQueryIFrame;
        protected global::System.Web.UI.HtmlControls.HtmlIframe CaseNumberAssignmentIFrame;

#else
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl CAIVRSAuthorizationIFrame;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl CaseQueryIFrame;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl CaseNumberAssignmentIFrame;
#endif

        private string ContentMissingText = "There are no {0} results on this loan file.";

        protected string Destination;

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "FHA Connection Results";
            this.PageID = "FHAConnectionResults";
            this.RegisterJsScript("LQBPopup.js");
        }

        protected Guid BrokerId
        {
            get { return Broker.BrokerID; }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            IncludeStyleSheet("~/css/Tabs.css");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAConnectionResults));
            dataLoan.InitLoad();

            var isVaLoan = dataLoan.sLT == E_sLT.VA;

            if (isVaLoan)
            {
                Destination = "caivrs";
            }
            else
            {
                Destination = RequestHelper.GetSafeQueryString("dest");
            }

            string url;

            if (isVaLoan)
            {
                this.CaseNumberAssignment.Visible = false;
                this.CaseNumberAssignmentTab.Visible = false;
            }
            else if (dataLoan.sFHACaseNumberResultXmlContent != "")
            {
                url = Page.ResolveUrl("FHAConnectionResultsCaseNumber.aspx") + "?LoanId=" + LoanID;
                CaseNumberAssignmentIFrame.Attributes.Add("src", url);
            }
            else
            {
                CaseNumberAssignmentIFrame.Visible = false;
                CaseNumberAssignmentMissingText.Text = string.Format(ContentMissingText, "Case Number Assignment");
                PlaceHolderCaseNumber.Visible = false;
            }

            if (isVaLoan)
            {
                this.CaseQuery.Visible = false;
                this.CaseQueryTab.Visible = false;
            }
            else if (dataLoan.sFHACaseQueryResultXmlContent != "")
            {
                url = Page.ResolveUrl("FHAConnectionResultsCaseQuery.aspx") + "?LoanId=" + LoanID;
                CaseQueryIFrame.Attributes.Add("src", url);
            }
            else
            {
                CaseQueryIFrame.Visible = false;
                CaseQueryMissingText.Text = string.Format(ContentMissingText, "Case Query");
                PlaceHolderCaseQuery.Visible = false;
            }

            if (dataLoan.sFHACAVIRSResultXmlContent != "")
            {
                url = Page.ResolveUrl("FHAConnectionResultsCAIVRS.aspx") + "?LoanId=" + LoanID;
                CAIVRSAuthorizationIFrame.Attributes.Add("src", url);
            }
            else
            {
                CAIVRSAuthorizationIFrame.Visible = false;
                CAIVRSAuthorizationMissingText.Text = string.Format(ContentMissingText, "CAIVRS Authorization");
                PlaceHolderCAIVRS.Visible = false;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            EnableJqueryMigrate = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
