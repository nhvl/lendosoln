using System;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
    public class FHARequestInsuranceEndorsementServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHARequestInsuranceEndorsementServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum");
            dataApp.aCMidNm = GetString("aCMidNm");
            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aCFirstNm = GetString("aCFirstNm");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aCLastNm = GetString("aCLastNm");
            dataApp.aCSuffix = GetString("aCSuffix");
            dataApp.aBSsn = GetString("aBSsn");
            dataApp.aCSsn = GetString("aCSsn");
            dataApp.aBGender = (E_GenderT)GetInt("aBGenderT");
            dataApp.aCGender = (E_GenderT)GetInt("aCGenderT");
            dataApp.aBRaceT = (E_aBRaceT)GetInt("aBRaceT");
            dataApp.aCRaceT = (E_aCRaceT)GetInt("aCRaceT");
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sFHAInsEndorseProgId = GetString("sFHAInsEndorseProgId");
            dataLoan.sFHAInsEndorseInclSolarWindHeater = GetTriState("sFHAInsEndorseInclSolarWindHeater");
            dataLoan.sFHAInsEndorseWillBorrBeOccupant = GetBool("sFHAInsEndorseWillBorrBeOccupant");
            dataLoan.sFHAInsEndorseWillBorrBeLandlord = GetBool("sFHAInsEndorseWillBorrBeLandlord");
            dataLoan.sFHAIsEscrowCommitment = GetBool("sFHAIsEscrowCommitment");
            dataLoan.sFHAInsEndorseWillBorrBeCorp = GetBool("sFHAInsEndorseWillBorrBeCorp");
            dataLoan.sFHAIsNonProfit = GetBool("sFHAIsNonProfit");
            dataLoan.sFHAIsGovAgency = GetBool("sFHAIsGovAgency");
            dataLoan.sFHAInsEndorseRepairCompleteD = GetString("sFHAInsEndorseRepairCompleteD");
            dataLoan.sFHAInsEndorseRepairEscrowAmt_rep = GetString("sFHAInsEndorseRepairEscrowAmt");
            dataLoan.sFHAInsEndorseCounselingT = (E_sFHAInsEndorseCounselingT)GetInt("sFHAInsEndorseCounselingT");
            dataLoan.sFHAInsEndorseOrigMortgageeId = GetString("sFHAInsEndorseOrigMortgageeId");
            dataLoan.sFHAInsEndorseAgentNum = GetString("sFHAInsEndorseAgentNum");
            dataLoan.sFHAInsEndorseMMaturityDate = GetString("sFHAInsEndorseMMaturityDate");
            dataLoan.sFHAInsEndorseWarrantyEnrollNum = GetString("sFHAInsEndorseWarrantyEnrollNum");
            dataLoan.sFHAInsEndorseAmortPlanCode = GetString("sFHAInsEndorseAmortPlanCode");
            dataLoan.sFHAInsEndorseConstructionCode = GetString("sFHAInsEndorseConstructionCode");
            dataLoan.sFHAInsEndorseLivingUnits = GetString("sFHAInsEndorseLivingUnits");
            dataLoan.sFHAInsEndorseExemptFromSSNTri = GetTriState("sFHAInsEndorseExemptFromSSNTri");
            dataLoan.sFHAInsEndorseVeteranPrefTri = GetTriState("sFHAInsEndorseVeteranPrefTri");
            dataLoan.sFHAInsEndorseEnergyEffMTri = GetTriState("sFHAInsEndorseEnergyEffMTri");
            dataLoan.sFHAInsEndorseIssueMicInSponsorNmTri = GetTriState("sFHAInsEndorseIssueMicInSponsorNmTri");
            dataLoan.sFHAInsEndorseMailToSponsorTri = GetTriState("sFHAInsEndorseMailToSponsorTri");
            dataLoan.sFHAInsEndorseCurrentPmtTri = GetTriState("sFHAInsEndorseCurrentPmtTri");
            dataLoan.sFHAInsEndorseUfmipFinancedTri = GetTriState("sFHAInsEndorseUfmipFinancedTri");
            dataLoan.sFHASponsorAgentIdCode = GetString("sFHASponsorAgentIdCode");
            dataLoan.sTerm_rep = GetString("sTerm");

            dataLoan.sSchedDueD1_rep = GetString("sSchedDueD1");
            dataLoan.sClosedD_rep = GetString("sClosedD");
            dataLoan.sFHAInsEndorseRepairEscrowTri = GetTriState("sFHAInsEndorseRepairEscrowTri");
            dataLoan.sFHAHousingActSection = GetString("sFHAHousingActSection");

            IPreparerFields appraiser = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAInsEndorsementAppraiser, E_ReturnOptionIfNotExist.CreateNew);
            appraiser.PreparerName = GetString("FHAInsEndorsementAppraiserPreparerName");
            appraiser.LicenseNumOfAgent = GetString("FHAInsEndorsementAppraiserLicenseNumOfAgent");
            appraiser.Update();

            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAInsEndorsementUnderwriter, E_ReturnOptionIfNotExist.CreateNew);
            underwriter.PreparerName = GetString("FHAInsEndorsementUnderwriterPreparerName");
            underwriter.LicenseNumOfAgent = GetString("FHAInsEndorsementUnderwriterLicenseNumOfAgent");
            underwriter.Update();

        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

	public partial class FHARequestInsuranceEndorsementService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize()
        {
            AddBackgroundItem("", new FHARequestInsuranceEndorsementServiceItem());
        }
	}
}
