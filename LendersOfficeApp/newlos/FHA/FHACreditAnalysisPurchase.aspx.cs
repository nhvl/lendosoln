namespace LendersOfficeApp.newlos.FHA
{
    using System;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Migration;

    public partial class FHACreditAnalysisPurchase : BaseLoanPage
	{
        #region "Member variables"

		protected System.Web.UI.WebControls.Button fromGFE_btn;
		protected System.Web.UI.WebControls.Button from1003_btn;
#endregion 

		protected override void LoadData() 
		{
			CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHACreditAnalysisPurchase));
			dataLoan.InitLoad();
			CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBSSN.Text = dataApp.aBSsn;
            
            aFHANegCfRentalI.Text = dataApp.aFHANegCfRentalI_rep;
            aCFirstNm.Text				= dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text				= dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCSSN.Text					= dataApp.aCSsn;
            aFHAChildSupportPmt.Text    = dataApp.aFHAChildSupportPmt_rep;
            aFHAChildSupportPmt.ReadOnly = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V25_ConsolidateMonthlyChildSupportPayments);
            aFHAGiftFundSrc.Text		= dataApp.aFHAGiftFundSrc;
            aFHAGiftFundAmt.Text		= dataApp.aFHAGiftFundAmt_rep;
            aFHAAssetAvail.Text			= dataApp.aFHAAssetAvail_rep;
            aFHADebtInstallPmt.Text		= dataApp.aFHADebtInstallPmt_rep;
            aFHAOtherDebtPmt.Text		= dataApp.aFHAOtherDebtPmt_rep;
            aFHADebtInstallBal.Text		= dataApp.aFHADebtInstallBal_rep;
            aFHAOtherDebtBal.Text		= dataApp.aFHAOtherDebtBal_rep;
            aFHABBaseI.Text				= dataApp.aFHABBaseI_rep;
            aFHABOI.Text				= dataApp.aFHABOI_rep;
            aFHACBaseI.Text				= dataApp.aFHACBaseI_rep;
            aFHACOI.Text				= dataApp.aFHACOI_rep;
            aFHANetRentalI.Text			= dataApp.aFHANetRentalI_rep;
            aFHAGrossMonI.Text = dataApp.aFHAGrossMonI_rep;

            aFHADebtPmtTot.Text			= dataApp.aFHADebtPmtTot_rep;
            sFHAPro1stMPmt.Text			= dataLoan.sFHAPro1stMPmt_rep;
            sFHAProMIns.Text			= dataLoan.sFHAProMIns_rep;
            sFHAProMInsLckd.Checked = dataLoan.sFHAProMInsLckd;
            sFHAProHoAssocDues.Text		= dataLoan.sFHAProHoAssocDues_rep;
            sFHAProGroundRent.Text		= dataLoan.sFHAProGroundRent_rep;
            sFHAPro2ndFinPmt.Text		= dataLoan.sFHAPro2ndFinPmt_rep;
            sFHAProHazIns.Text			= dataLoan.sFHAProHazIns_rep;
            sFHAProRealETx.Text			= dataLoan.sFHAProRealETx_rep;
            sFHAMonthlyPmt.Text			= dataLoan.sFHAMonthlyPmt_rep;
            aFHADebtPmtTot2.Text		= dataApp.aFHADebtPmtTot_rep;
            aFHAPmtFixedTot.Text		= dataApp.aFHAPmtFixedTot_rep;
            aFHAMPmtToIRatio.Text		= dataApp.aFHAMPmtToIRatio_rep;
            aFHAFixedPmtToIRatio.Text	= dataApp.aFHAFixedPmtToIRatio_rep;
            aFHACreditRating.Text		= dataApp.aFHACreditRating;
            aFHARatingIAdequacy.Text	= dataApp.aFHARatingIAdequacy;
            aFHARatingIStability.Text	= dataApp.aFHARatingIStability;
            aFHARatingAssetAdequacy.Text = dataApp.aFHARatingAssetAdequacy;
            aFHABCaivrsNum.Text			= dataApp.aFHABCaivrsNum;
            aFHACCaivrsNum.Text			= dataApp.aFHACCaivrsNum;
            aFHABLpdGsa.Text			= dataApp.aFHABLpdGsa;
            aFHACLpdGsa.Text			= dataApp.aFHACLpdGsa;

            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            
            if (Tools.ShouldBeReadonly_sLT(dataLoan.sLPurposeT))
            {
                Tools.ReadonlifyDropDown(sLT);
            }

            sFHANonrealtyLckd.Checked = dataLoan.sFHANonrealtyLckd;
            
            
            sPurchPrice6pc.Text = dataLoan.sPurchPrice6pc_rep;
            sFHAHousingActSection.Text = dataLoan.sFHAHousingActSection;
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            Tools.SetDropDownListValue(sFHAConstructionT, dataLoan.sFHAConstructionT);

            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisPurchaseUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHACreditAnalysisPurchaseUnderwriterLicenseNumOfAgent.Text = underwriter.LicenseNumOfAgent;
            FHACreditAnalysisPurchaseUnderwriterPreparerName.Text = underwriter.PreparerName;
            sLAmt.Text = dataLoan.sLAmtCalc_rep;
            sPresLTotHExp.Text = dataApp.aPresTotHExp_rep;
            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sTermInYr.Text = dataLoan.sTermInYr_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sApprVal.Text = dataLoan.sApprVal_rep;
            sBuydownResultIR.Text = dataLoan.sBuydownResultIR_rep;
            sFHACcTot.Text = dataLoan.sFHACcTot_rep;
            sFHACcPbs.Text = dataLoan.sFHACcPbs_rep;
            sFHAPurchPrice.Text  = dataLoan.sFHAPurchPrice_rep;
            string shareVal = dataLoan.sFHACcPbb_rep;
            sFHACcPbb2.Text = shareVal;
            sFHACcPbb.Text = shareVal;
            sFHAUnadjAcquisition.Text = dataLoan.sFHAUnadjAcquisition_rep;
            sFHAStatutoryInvestReq.Text = dataLoan.sFHAStatutoryInvestReq_rep;
            sFHAHouseVal.Text = dataLoan.sFHAHouseVal_rep;
            sFHAReqAdj.Text = dataLoan.sFHAReqAdj_rep;
            sFHAMBasis.Text = dataLoan.sFHAMBasis_rep;
            sFHALtvLckd.Checked = dataLoan.sFHALtvLckd;
            sFHALtv.Text = dataLoan.sFHALtv_rep;
            sFHAMAmt.Text = dataLoan.sFHAMAmt_rep;
            sFHAMAmt2.Text = dataLoan.sFHAMAmt_rep;
            sFHAMinDownPmt.Text = dataLoan.sFHAMinDownPmt_rep;
            sFHAPrepaidExp.Text = dataLoan.sFHAPrepaidExp_rep;
            sFHADiscountPoints.Text = dataLoan.sFHADiscountPoints_rep;
            sFHAImprovements.Text = dataLoan.sFHAImprovements_rep;
            sUfCashPd.Text = dataLoan.sUfCashPd_rep;
            sFHANonrealty.Text = dataLoan.sFHANonrealty_rep;
            sFHATotCashToClose.Text = dataLoan.sFHATotCashToClose_rep;
            sFHAAmtPaid.Text = dataLoan.sFHAAmtPaid_rep;           
            sFHA2ndMSrc.Text = dataLoan.sFHA2ndMSrc;
            sFHA2ndMAmt.Text = dataLoan.sFHA2ndMAmt_rep;
            sFHACashReserves.Text = dataLoan.sFHACashReserves_rep;
           
            
            sFHALtv2.Text = dataLoan.sFHALtvPurch_rep;
            sFHAPurchPrice2.Text = dataLoan.sFHAPurchPrice_rep;
            sFHASellerContribution.Text = dataLoan.sFHASellerContribution_rep;
            sFHAExcessContribution.Text = dataLoan.sFHAExcessContribution_rep;
            sFHACreditAnalysisRemarks.Text = dataLoan.sFHACreditAnalysisRemarks;

            sFfUfmipR.Text					= dataLoan.sFfUfmipR_rep;
            sMipPiaMon.Text = dataLoan.sMipPiaMon_rep;
            sFfUfmip1003Lckd.Checked		= dataLoan.sFfUfmip1003Lckd;
            sFfUfmip1003.Text				= dataLoan.sFfUfmip1003_rep;
            sUfCashPdzzz2.Text				= dataLoan.sUfCashPd_rep;
            sFHAAmtPaidByCash.Checked		= dataLoan.sFHAAmtPaidByCash;
            sFHAAmtPaidByOtherT.Checked		= dataLoan.sFHAAmtPaidByOtherT;
            sFHAEnergyEffImprov.Text		= dataLoan.sFHAEnergyEffImprov_rep;
            sFfUfmipFinanced.Text           = dataLoan.sFfUfmipFinanced_rep;
            Tools.SetDropDownListValue(sFHAPurchPriceSrcT, dataLoan.sFHAPurchPriceSrcT);
            sFHA203kInvestmentRequiredPc.Text = dataLoan.sFHA203kInvestmentRequiredPc_rep;
		}

        private void HighlightRow(WebControl ctl) 
        {
            ctl.Attributes.Add("onfocus", "highlightRow(this);");
            ctl.Attributes.Add("onblur", "unhighlightRow(this);");
        }
        protected void PageInit(object sender, System.EventArgs e) 
        {
            HighlightRow(sFHALtv);
            HighlightRow(sFHAReqAdj);
            HighlightRow(sFHAPrepaidExp);
            HighlightRow(sFHADiscountPoints);
            HighlightRow(sFHAImprovements);
            HighlightRow(sFHANonrealty);
            HighlightRow(sFHANonrealtyLckd);
            HighlightRow(sFHAAmtPaid);
            HighlightRow(aFHAGiftFundSrc);
            HighlightRow(aFHAGiftFundAmt);
            HighlightRow(aFHAAssetAvail);
            HighlightRow(sFHA2ndMSrc);
            HighlightRow(sFHA2ndMAmt);
            HighlightRow(aFHABBaseI);
            HighlightRow(aFHABOI);
            HighlightRow(aFHACBaseI);
            HighlightRow(aFHACOI);
            HighlightRow(aFHANetRentalI);
            HighlightRow(aFHADebtInstallPmt);
            HighlightRow(aFHADebtInstallBal);
            HighlightRow(aFHAChildSupportPmt);
            HighlightRow(aFHAOtherDebtPmt);
            HighlightRow(aFHAOtherDebtBal);
            HighlightRow(sFHAPro1stMPmt);
            HighlightRow(sFHAProMIns);
            HighlightRow(sFHAProHoAssocDues);
            HighlightRow(sFHAProGroundRent);
            HighlightRow(sFHAPro2ndFinPmt);
            HighlightRow(sFHAProHazIns);
            HighlightRow(sFHAProRealETx);
            HighlightRow(aFHACreditRating);
            HighlightRow(aFHARatingIAdequacy);
            HighlightRow(aFHARatingIStability);
            HighlightRow(aFHARatingAssetAdequacy);
            HighlightRow(aFHABCaivrsNum);
            HighlightRow(aFHACCaivrsNum);
            HighlightRow(aFHABLpdGsa);
            HighlightRow(aFHACLpdGsa);
            HighlightRow(sFHASellerContribution);
            HighlightRow(sFHAExcessContribution);

            Tools.Bind_sFHAHousingActSection(sFHAHousingActSection);
            Tools.Bind_sLT(sLT);
            Tools.Bind_sFHAConstructionT(sFHAConstructionT);

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            this.PageID = "FHA_92900_PUR";
            this.PageTitle = "FHA MCAW Purchase";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CHUD_92900_PURPDF);


        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion



	}
}
