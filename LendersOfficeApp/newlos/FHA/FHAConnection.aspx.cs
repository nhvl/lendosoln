﻿namespace LendersOfficeApp.newlos.FHA
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;

    /// <summary>
    /// Notes:
    ///   Design failure - I really should have made a separate page for each
    ///     case processing request. It would be much easier to maintain.
    ///     We wouldn't have to deal with all those section lists in the js,
    ///     validation would be much simpler, loading/saving would be simpler.
    ///     This page should be a target for refactoring.
    /// </summary>
    public partial class FHAConnection : BaseLoanPage
    {

        protected class ExtraBorrowerInfo
        {
            public int SequenceNumber;
            public string BorrName;
            public string BorrDOB;
            public string BorrSSN;

            public string CoborrName;
            public string CoborrDOB;
            public string CoborrSSN;
        }

        protected E_sLT sLT;
        protected E_CaseProcessingRequestType InitialCaseProcessingRequestType;

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageTitle = "FHA Connection";
            this.PageID = "FHAConnection";

            CaseProcessingRequestType.Items.Add(new ListItem("New Case Number Assignment", E_CaseProcessingRequestType.NewCaseNumberAssignment.ToString("D")));
            CaseProcessingRequestType.Items.Add(new ListItem("Update an Existing Case", E_CaseProcessingRequestType.UpdateAnExistingCase.ToString("D")));
            CaseProcessingRequestType.Items.Add(new ListItem("Holds Tracking", E_CaseProcessingRequestType.HoldsTracking.ToString("D")));
            CaseProcessingRequestType.Items.Add(new ListItem("Case Query", E_CaseProcessingRequestType.CaseQuery.ToString("D")));
            CaseProcessingRequestType.Items.Add(new ListItem("CAIVRS Authorization", E_CaseProcessingRequestType.CAIVRSAuthorization.ToString("D")));

            _initNewCaseNumberAssignment();
        }

        private void _initNewCaseNumberAssignment()
        {
            sSpZip.SmartZipcode(sSpCity, sSpState, sSpCounty);

            // Subject property
            Tools.Bind_sSpMonthBuiltT(sSpMonthBuiltT);
            Tools.Bind_CompassDirections(sSpStDir);
            Tools.Bind_CompassDirections(sSpStPostDir);
            Tools.Bind_UspsStreetTypes(sSpStType);

            // Borrower/Co-Borrower Information
            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            // General information
            Tools.Bind_sFHACaseT(sFHACaseT);
            Tools.Bind_sFHAConstCodeT(sFHAConstCodeT);
            Tools.Bind_sFHAProcessingT(sFHAProcessingT);
            Tools.Bind_sFHAProgramId(sFHAProgramId);
            Tools.Bind_sFHAFinT(sFHAFinT);

            // ADP Code Criteria
            Tools.Bind_sFinMethT(sFinMethT);
            Tools.Bind_sFHAADPHousingProgramT(sFHAADPHousingProgramT);
            Tools.Bind_sGseSpT(sGseSpT);
            Tools.Bind_sFHAADPSpecialProgramT(sFHAADPSpecialProgramT);
            Tools.Bind_sFHAADPPrincipalWriteDownT(sFHAADPPrincipalWriteDownT);

            // General information
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sTotalScoreRefiT_FHAConnection(sTotalScoreRefiT);

            // Compliance inspection
            Tools.Bind_sFHAComplianceInspectionAssignmentT(sFHAComplianceInspectionAssignmentT);

            // FHA Agency
            Tools.Bind_sFHAAgencyT(sFHAAgencyT);
        }

        private void _initUpdateAnExistingCase()
        {
            _initNewCaseNumberAssignment();
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAConnection));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            sLT = dataLoan.sLT; // Required in order to determine visibility of certain fields

            this.caivrs_sVALenderIdCode.Visible = false;

            // If the request is for a VA loan, show only the CAIVRS option and disable the dropdown
            // Else if the request has the "requestType" query string parameter, use that
            // Otherwise, if the sAgencyCaseNumber is blank, select the "new case number assignment" option
            //            if it's not blank, select the "update existing case" option
            if (dataLoan.sLT == E_sLT.VA)
            {
                CaseProcessingRequestType.SelectedValue = E_CaseProcessingRequestType.CAIVRSAuthorization.ToString("D");
                CaseProcessingRequestType.Enabled = false;

                this.caivrs_sFHALenderIdCode.Visible = false;

                this.caivrs_sVALenderIdCode.Visible = true;
                this.caivrs_sVALenderIdCode.Text = dataLoan.sVALenderIdCode;
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["requestType"]))
            {
                CaseProcessingRequestType.SelectedValue = Request.QueryString["requestType"];
            }
            else
            {
                if (string.IsNullOrEmpty(dataLoan.sAgencyCaseNum))
                {
                    CaseProcessingRequestType.SelectedValue = E_CaseProcessingRequestType.NewCaseNumberAssignment.ToString("D");
                }
                else
                {
                    CaseProcessingRequestType.SelectedValue = E_CaseProcessingRequestType.UpdateAnExistingCase.ToString("D");
                }
            }

            CurrentCaseProcessingRequestType.Value = CaseProcessingRequestType.SelectedValue;
            E_CaseProcessingRequestType requestType = (E_CaseProcessingRequestType)int.Parse(CaseProcessingRequestType.SelectedValue);
            InitialCaseProcessingRequestType = requestType;
            switch (requestType)
            {
                case E_CaseProcessingRequestType.NewCaseNumberAssignment:
                    PageLoad_NewCaseNumberAssignment(dataLoan, dataApp);
                    break;
                case E_CaseProcessingRequestType.UpdateAnExistingCase:
                    PageLoad_UpdateAnExistingCase(dataLoan, dataApp);
                    break;
                case E_CaseProcessingRequestType.HoldsTracking:
                    PageLoad_HoldsTracking(dataLoan, dataApp);
                    break;
                case E_CaseProcessingRequestType.CaseQuery:
                    PageLoad_CaseQuery(dataLoan, dataApp);
                    break;
                case E_CaseProcessingRequestType.CAIVRSAuthorization:
                    PageLoad_CAIVRSAuthorization(dataLoan, dataApp);
                    break;
                default:
                    throw new UnhandledEnumException(requestType);
            }
            
        }

        protected void PageLoad_NewCaseNumberAssignment(CPageData dataLoan, CAppData dataApp)
        {
            // Subject Property Address
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;
            sSpCounty.Text = dataLoan.sSpCounty;
            sUnitsNum.Text = dataLoan.sUnitsNum_rep;
            Tools.SetDropDownListValue(sSpMonthBuiltT, dataLoan.sSpMonthBuiltT);
            sYrBuilt.Text = dataLoan.sYrBuilt;
            sSpLotNum.Text = dataLoan.sSpLotNum;
            sSpBlockPlatNum.Text = dataLoan.sSpBlockPlatNum;

            this.sUseParsedAddressFieldsForIntegrations.Checked = dataLoan.sUseParsedAddressFieldsForIntegrations;
            this.sSpStNum.Text = dataLoan.sSpStNum;
            this.sSpStUnit.Text = dataLoan.sSpStUnit;
            this.sSpStDir.Text = dataLoan.sSpStDir;
            this.sSpStName.Text = dataLoan.sSpStName;
            this.sSpStType.Text = dataLoan.sSpStType;
            this.sSpStPostDir.Text = dataLoan.sSpStPostDir;

            // Borrower information
            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBSsn.Text = dataApp.aBSsn;
            aBDob.Text = dataApp.aBDob_rep;

            aCFirstNm.Text = dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCSsn.Text = dataApp.aCSsn;
            aCDob.Text = dataApp.aCDob_rep;

            // Extra borrowers
            int nApps = dataLoan.nApps;
            var extraBorrowers = new List<ExtraBorrowerInfo>();
            int appNum = 2;
            for (int i = 0; i < nApps; i++) // Skip the first app. That's already accounted for.
            {
                CAppData app = dataLoan.GetAppData(i);

                if (app.aBSsn == dataApp.aBSsn)
                {
                    continue; // This app is taken care of above
                }

                extraBorrowers.Add(
                    new ExtraBorrowerInfo()
                    {
                        SequenceNumber = appNum,
                        BorrName = app.aBNm,
                        BorrDOB = app.aBDob_rep,
                        BorrSSN = app.aBSsn,
                        CoborrName = app.aCNm,
                        CoborrDOB = app.aCDob_rep,
                        CoborrSSN = app.aCSsn,
                    });

                appNum++;
            }
            ExtraBorrowerTable.DataSource = extraBorrowers;
            ExtraBorrowerTable.DataBind();

            // Active Loan App
            sFHALenderHasThisApplication.Checked = dataLoan.sFHALenderHasThisApplication;

            // General information
            Tools.Bind_sFHAFieldOfficeCode(sFHAFieldOfficeCode, dataLoan.sSpState); // Have to bind here since it needs the state
            
            Tools.SetDropDownListValue(sFHAFieldOfficeCode, dataLoan.sFHAFieldOfficeCode);
            sLenderCaseNum.Text = dataLoan.sLenderCaseNum;

            IPreparerFields interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            LoanOriginatorName.Text = interviewer.PreparerName;
            LoanOriginatorNMLS_ID.Text = interviewer.LoanOriginatorIdentifier;

            if (dataLoan.sFhaLenderIdT == E_sFhaLenderIdT.RegularFhaLender)
                sFhaLenderIdT_RegularFhaLender.Checked = true;
            else if (dataLoan.sFhaLenderIdT == E_sFhaLenderIdT.SponsoredOriginatorEIN)
                sFhaLenderIdT_SponsoredOriginatorEIN.Checked = true;

            sFHALenderIdCode.Text = dataLoan.sFHALenderIdCode;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
            {
                // This will hook the field into mask.js for migrated files.
                sFHASponsoredOriginatorEIN.Attributes.Add("preset", "employerIdentificationNumber");
            }

            sFHASponsoredOriginatorEIN.Text = dataLoan.sFHASponsoredOriginatorEIN;
            sFhaSponsoredOriginatorEinLckd.Checked = dataLoan.sFhaSponsoredOriginatorEinLckd;
            sFHASponsorAgentIdCode.Text = dataLoan.sFHASponsorAgentIdCode;

            Tools.SetDropDownListValue(sFHACaseT, dataLoan.sFHACaseT);
            Tools.SetDropDownListValue(sFHAConstCodeT, dataLoan.sFHAConstCodeT);
            Tools.SetDropDownListValue(sFHAProcessingT, dataLoan.sFHAProcessingT);

            Tools.SetDropDownListValue(sFHAProgramId, dataLoan.sFHAProgramId);

            sTerm.Text = dataLoan.sTerm_rep;
            Tools.SetDropDownListValue(sFHAFinT, dataLoan.sFHAFinT);

            // ADP Code Criteria
            Tools.SetDropDownListValue(sFinMethT, dataLoan.sFinMethT);
            Tools.SetDropDownListValue(sFHAADPHousingProgramT, dataLoan.sFHAADPHousingProgramT);
            Tools.SetDropDownListValue(sGseSpT, dataLoan.sGseSpT);
            Tools.SetDropDownListValue(sFHAADPSpecialProgramT, dataLoan.sFHAADPSpecialProgramT);
            sBuydown.SelectedIndex = dataLoan.sBuydown ? 0 : 1;
            Tools.SetDropDownListValue(sFHAADPPrincipalWriteDownT, dataLoan.sFHAADPPrincipalWriteDownT);
            //

            sFHAADPCode.Text = dataLoan.sFHAADPCode;
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            sLPurposeT_TypeOfCase.Text = dataLoan.sLPurposeT == E_sLPurposeT.Purchase ? "Forward Purchase" : "Forward Refinance";
            sFHACasePreviouslySoldByHUDAsREO.SelectedIndex = dataLoan.sFHACasePreviouslySoldByHUDAsREO ? 0 : 1;
            Tools.SetDropDownListValue(sTotalScoreRefiT, dataLoan.sTotalScoreRefiT);

            // As Required
            sFHAPurposeIsStreamlineRefiApprChoice.SelectedIndex = dataLoan.sFHAPurposeIsStreamlineRefiApprChoice ? 0 : 1;

            // Update Existing Case: Refinance Authorization Number

            // Prior FHA/REO
            sFHAPreviousCaseNum.Text = dataLoan.sFHAPreviousCaseNum;
            sEstCloseDLckd.Checked = dataLoan.sEstCloseDLckd;
            sEstCloseD.Text = dataLoan.sEstCloseD_rep;
            sFHANewCaseContactNm.Text = dataLoan.sFHANewCaseContactNm;
            sFHANewCaseContactPhone.Text = dataLoan.sFHANewCaseContactPhone;

            sFHA203kConsultantId.Text = dataLoan.sFHA203kConsultantId;
            Tools.Bind_sFHA203kType(sFHA203kType);
            Tools.SetDropDownListValue(sFHA203kType, dataLoan.sFHA203kType);

            // PUD/Condo
            Tools.Bind_sFHAPUDCondoT(sFHAPUDCondoT, includeUnspecified: dataLoan.sFHAPUDCondoT == E_sFHAPUDCondoT.Unspecified);
            Tools.SetDropDownListValue(sFHAPUDCondoT, dataLoan.sFHAPUDCondoT);
            sProjNm.Text = dataLoan.sProjNm;
            sCpmProjectId.Text = dataLoan.sCpmProjectId;
            sFHAPUDSubmissionPhase.Text = dataLoan.sFHAPUDSubmissionPhase;
            Tools.Bind_sFHAPUDSiteCondoT(sFHAPUDSiteCondoT, includeSpotLot: dataLoan.sFHAPUDSiteCondoT == E_sFHAPUDSiteCondoT.SpotLot);
            Tools.SetDropDownListValue(sFHAPUDSiteCondoT, dataLoan.sFHAPUDSiteCondoT);
            

            sVACRVNum.Text = dataLoan.sVACRVNum;
            aVALAnalysisExpirationD.Text = dataApp.aVALAnalysisExpirationD_rep;

            // Compliance inspection fields
            Tools.SetDropDownListValue(sFHAComplianceInspectionAssignmentT, dataLoan.sFHAComplianceInspectionAssignmentT);
            sFHAComplianceInspectionAssignmentID.Text = dataLoan.sFHAComplianceInspectionAssignmentID;

            // FHAC Lender Notes
            sFHALenderNotes.Text = dataLoan.sFHALenderNotes;

            // Authentication
            // DO NOT LOAD/STORE
            
        }

        protected void PageLoad_UpdateAnExistingCase(CPageData dataLoan, CAppData dataApp)
        {
            PageLoad_NewCaseNumberAssignment(dataLoan, dataApp);

            // FHA Case Number
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            
            // Update existing case: Refinance authorization
            sFHARefiAuthNum.Text = dataLoan.sFHARefiAuthNum;
        }

        protected void PageLoad_HoldsTracking(CPageData dataLoan, CAppData dataApp)
        {
            // Holds Tracking: Borrower Information
            holdsTracking_aBFirstNm.Text = dataApp.aBFirstNm;
            holdsTracking_aBMidNm.Text = dataApp.aBMidNm;
            holdsTracking_aBLastNm.Text = dataApp.aBLastNm;

            // Holds Tracking: General Information
            holdsTracking_sFHALenderIdCode.Text = dataLoan.sFHALenderIdCode;
        }

        protected void PageLoad_CaseQuery(CPageData dataLoan, CAppData dataApp)
        {
            // FHA Case Number
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;

            // Case Query: Field office
            Tools.Bind_sFHAFieldOfficeCode(caseQuery_sFHAFieldOfficeCode, dataLoan.sSpState);
            Tools.SetDropDownListValue(caseQuery_sFHAFieldOfficeCode, dataLoan.sFHAFieldOfficeCode);

        }

        protected void PageLoad_CAIVRSAuthorization(CPageData dataLoan, CAppData dataApp)
        {
            // CAIVRS:

            int borrowerCount = 0;
            // Borrower
            if (!string.IsNullOrEmpty(dataApp.aBFirstNm) && !string.IsNullOrEmpty(dataApp.aBLastNm) && !string.IsNullOrEmpty(dataApp.aBSsn))
            {
                borrowerCount++;
            }
            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBSsn.Text = dataApp.aBSsn;
            aBDob.Text = dataApp.aBDob_rep;

            if (!string.IsNullOrEmpty(dataApp.aCFirstNm) && !string.IsNullOrEmpty(dataApp.aCLastNm) && !string.IsNullOrEmpty(dataApp.aCSsn))
            {
                borrowerCount++;
            }
            aCFirstNm.Text = dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCSsn.Text = dataApp.aCSsn;
            aCDob.Text = dataApp.aCDob_rep;
            
            // Extra borrowers
            int nApps = dataLoan.nApps;
            var extraBorrowers = new List<ExtraBorrowerInfo>();
            int appNum = 2;
            for (int i = 0; i < nApps; i++)
            {
                CAppData app = dataLoan.GetAppData(i);

                if (app.aBSsn == dataApp.aBSsn)
                {
                    continue; // This app is taken care of above
                }

                // Display only 5 borrowers at most
                if (borrowerCount >= 5) break;

                bool shouldAddBorrower = false;

                var extraBorrower = new ExtraBorrowerInfo();
                extraBorrower.SequenceNumber = appNum;
                appNum++;
                if (!string.IsNullOrEmpty(app.aBFirstNm) && !string.IsNullOrEmpty(app.aBLastNm) && !string.IsNullOrEmpty(app.aBSsn))
                {
                    borrowerCount++;
                    extraBorrower.BorrName = app.aBNm;
                    //extraBorrower.BorrDOB = app.aBDob_rep;
                    extraBorrower.BorrSSN = app.aBSsn;
                    shouldAddBorrower = true;
                }

                if (!string.IsNullOrEmpty(app.aCFirstNm) && !string.IsNullOrEmpty(app.aCLastNm) && !string.IsNullOrEmpty(app.aCSsn))
                {
                    borrowerCount++;
                    extraBorrower.CoborrName = app.aCNm;
                    //extraBorrower.CoborrDOB = app.aCDob_rep;
                    extraBorrower.CoborrSSN = app.aCSsn;
                    shouldAddBorrower = true;
                }

                if (shouldAddBorrower)
                {
                    extraBorrowers.Add(extraBorrower);
                }
            }
            ExtraBorrowerTable.DataSource = extraBorrowers;
            ExtraBorrowerTable.DataBind();

            // Lender ID
            caivrs_sFHALenderIdCode.Text = dataLoan.sFHALenderIdCode;
            
            // Agency
            Tools.SetDropDownListValue(sFHAAgencyT, dataLoan.sFHAAgencyT);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            EnableJqueryMigrate = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion
    }
}
