using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
	public partial class FHAAnalysisAppraisedValue : BaseLoanPage
	{
        #region Protected member variables
        #endregion

        protected void PageInit(object sender, System.EventArgs e)
        {
            Tools.Bind_TriState(sFHAApprAreComparablesAcceptableTri);
            Tools.Bind_TriState(sFHAApprAdjAcceptableTri);
            Tools.Bind_TriState(sFHAApprIsValAcceptableTri);
            Tools.Bind_TriState(sFHAApprValNeedCorrectionTri);
            Tools.Bind_TriState(sFHAApprIsFairTri);

            sFHAApprAreComparablesAcceptableTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAApprAdjAcceptableTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAApprIsValAcceptableTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAApprValNeedCorrectionTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAApprIsFairTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);

            this.PageID = "FHA_54114";
            this.PageTitle = "FHA DE Analysis of Appraisal";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CHUD_54114PDF);


        }
        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAAnalysisAppraisedValue));
            dataLoan.InitLoad();

            sFHAApprNotFairExplanation.Text = dataLoan.sFHAApprNotFairExplanation;
            sFHAApprQualityComment.Text = dataLoan.sFHAApprQualityComment;
            Tools.Set_TriState(sFHAApprAreComparablesAcceptableTri, dataLoan.sFHAApprAreComparablesAcceptableTri);
            sFHAApprComparablesComment.Text = dataLoan.sFHAApprComparablesComment;
            Tools.Set_TriState(sFHAApprAdjAcceptableTri, dataLoan.sFHAApprAdjAcceptableTri);
            sFHAApprAdjNotAcceptableExplanation.Text = dataLoan.sFHAApprAdjNotAcceptableExplanation;
            Tools.Set_TriState(sFHAApprIsValAcceptableTri, dataLoan.sFHAApprIsValAcceptableTri);
            Tools.Set_TriState(sFHAApprValNeedCorrectionTri, dataLoan.sFHAApprValNeedCorrectionTri);
            sFHAApprCorrectedVal.Text = dataLoan.sFHAApprCorrectedVal_rep;
            sFHAApprValCorrectionJustification.Text = dataLoan.sFHAApprValCorrectionJustification;
            sFHAApprRepairConditions.Text = dataLoan.sFHAApprRepairConditions;
            sFHAApprOtherComments.Text = dataLoan.sFHAApprOtherComments;
            Tools.Set_TriState(sFHAApprIsFairTri, dataLoan.sFHAApprIsFairTri);

            IPreparerFields appraisal = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAnalysisAppraisalAppraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AnalysisAppraisalAppraiserPreparerName.Text = appraisal.PreparerName;

            sFHAApprUnderwriterName.Text = dataLoan.sFHAApprUnderwriterName;
            sFHAApprUnderwriterChumsId.Text = dataLoan.sFHAApprUnderwriterChumsId;
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        
        }
        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));


        }
	}
}
