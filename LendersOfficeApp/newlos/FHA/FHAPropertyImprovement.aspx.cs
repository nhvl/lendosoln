using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
	public partial class FHAPropertyImprovement : BaseLoanPage
	{
        #region Protected member variables
        #endregion

        protected void PageInit(object sender, System.EventArgs e)
        {
            Tools.Bind_TriState(sFHAPropImprovRefinTitle1LoanTri);
            Tools.Bind_TriState(sFHAPropImprovHasFHAPendingAppTri);
            Tools.Bind_TriState(sFHAPropImprovHasFedPastDueTri);

            sFHAPropImprovBorrRelativeZip.SmartZipcode(sFHAPropImprovBorrRelativeCity, sFHAPropImprovBorrRelativeState);
            sFHAPropImprovCoborRelativeZip.SmartZipcode(sFHAPropImprovCoborRelativeCity, sFHAPropImprovCoborRelativeState);

            sFHAPropImprovRefinTitle1LoanTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAPropImprovHasFHAPendingAppTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAPropImprovHasFedPastDueTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);

            this.PageTitle = "Property Improvement";
            this.PageID = "FHAPropertyImprovement";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CHUD_56001PDF);


        }
        protected override void LoadData() 
        {
            CPageData dataLoan =CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAPropertyImprovement));
            dataLoan.InitLoad();

            sFHAPropImprovHasFHAPendingAppWithWhom.Text = dataLoan.sFHAPropImprovHasFHAPendingAppWithWhom;
            sFHAPropImprovRefinTitle1LoanNum.Text = dataLoan.sFHAPropImprovRefinTitle1LoanNum;
            sFHAPropImprovBorrRelativeNm.Text = dataLoan.sFHAPropImprovBorrRelativeNm;
            sFHAPropImprovCoborRelativeNm.Text = dataLoan.sFHAPropImprovCoborRelativeNm;
            sFHAPropImprovBorrRelativeRelationship.Text = dataLoan.sFHAPropImprovBorrRelativeRelationship;
            sFHAPropImprovCoborRelativeRelationship.Text = dataLoan.sFHAPropImprovCoborRelativeRelationship;
            sFHAPropImprovBorrRelativePhone.Text = dataLoan.sFHAPropImprovBorrRelativePhone;
            sFHAPropImprovCoborRelativePhone.Text = dataLoan.sFHAPropImprovCoborRelativePhone;
            sFHAPropImprovIsPropSingleFamily.Checked = dataLoan.sFHAPropImprovIsPropSingleFamily;
            sFHAPropImprovIsPropOwnedByBorr.Checked = dataLoan.sFHAPropImprovIsPropOwnedByBorr;
            sFHAPropImprovIsPropMultifamily.Checked = dataLoan.sFHAPropImprovIsPropMultifamily;
            sUnitsNum.Text = dataLoan.sUnitsNum_rep;
            sFHAPropImprovIsPropLeasedFromSomeone.Checked = dataLoan.sFHAPropImprovIsPropLeasedFromSomeone;
            sFHAPropImprovIsPropNonresidential.Checked = dataLoan.sFHAPropImprovIsPropNonresidential;
            sFHAPropImprovNonresidentialUsageDesc.Text = dataLoan.sFHAPropImprovNonresidentialUsageDesc;
            sFHAPropImprovIsPropBeingPurchasedOnContract.Checked = dataLoan.sFHAPropImprovIsPropBeingPurchasedOnContract;
            sFHAPropImprovIsPropManufacturedHome.Checked = dataLoan.sFHAPropImprovIsPropManufacturedHome;
            sFHAPropImprovIsThereMortOnProp.Checked = dataLoan.sFHAPropImprovIsThereMortOnProp;
            sFHAPropImprovIsPropHistoricResidential.Checked= dataLoan.sFHAPropImprovIsPropHistoricResidential;
            sFHAPropImprovIsPropHistoricResidentialUnitsNum.Text = dataLoan.sFHAPropImprovIsPropHistoricResidentialUnitsNum;
            sFHAPropImprovIsPropHealthCareFacility.Checked= dataLoan.sFHAPropImprovIsPropHealthCareFacility;
            sFHAPropImprovLeaseMonPmt.Text = dataLoan.sFHAPropImprovLeaseMonPmt_rep;
            sFHAPropImprovLeaseExpireD.Text = dataLoan.sFHAPropImprovLeaseExpireD_rep;
            sFHAPropImprovIsPropNewOccMoreThan90Days.Checked = dataLoan.sFHAPropImprovIsPropNewOccMoreThan90Days;
            sFHAPropImprovDealerContractorContactInfo.Text = dataLoan.sFHAPropImprovDealerContractorContactInfo;
            //sFHAPropImprovBorrRelativeAddr.Text = dataLoan.sFHAPropImprovBorrRelativeAddr;
            sFHAPropImprovBorrRelativeStreetAddress.Text = dataLoan.sFHAPropImprovBorrRelativeStreetAddress;
            sFHAPropImprovBorrRelativeCity.Text = dataLoan.sFHAPropImprovBorrRelativeCity;
            sFHAPropImprovBorrRelativeState.Value = dataLoan.sFHAPropImprovBorrRelativeState;
            sFHAPropImprovBorrRelativeZip.Text = dataLoan.sFHAPropImprovBorrRelativeZip;
            //sFHAPropImprovCoborRelativeAddr.Text = dataLoan.sFHAPropImprovCoborRelativeAddr;
            sFHAPropImprovCoborRelativeStreetAddress.Text = dataLoan.sFHAPropImprovCoborRelativeStreetAddress;
            sFHAPropImprovCoborRelativeCity.Text = dataLoan.sFHAPropImprovCoborRelativeCity;
            sFHAPropImprovCoborRelativeState.Value = dataLoan.sFHAPropImprovCoborRelativeState;
            sFHAPropImprovCoborRelativeZip.Text = dataLoan.sFHAPropImprovCoborRelativeZip;
            sFHAPropImprovBorrBankInfo.Text = dataLoan.sFHAPropImprovBorrBankInfo.Value;
            sFHAPropImprovCoborBankInfo.Text = dataLoan.sFHAPropImprovCoborBankInfo.Value;
            sFHAPropImprovLeaseOwnerInfo.Text = dataLoan.sFHAPropImprovLeaseOwnerInfo;

            Tools.Set_TriState(sFHAPropImprovRefinTitle1LoanTri, dataLoan.sFHAPropImprovRefinTitle1LoanTri);
            Tools.Set_TriState(sFHAPropImprovHasFHAPendingAppTri, dataLoan.sFHAPropImprovHasFHAPendingAppTri);

            Tools.Set_TriState(sFHAPropImprovHasFedPastDueTri, dataLoan.sFHAPropImprovHasFedPastDueTri);

            sFHAPropImprovRefinTitle1LoanBal.Text = dataLoan.sFHAPropImprovRefinTitle1LoanBal_rep;
			
			sFHAPropImprovBorrHasChecking.Checked		= dataLoan.sFHAPropImprovBorrHasChecking;
			sFHAPropImprovBorrHasSaving.Checked			= dataLoan.sFHAPropImprovBorrHasSaving;
			sFHAPropImprovBorrHasNoBankAcount.Checked	= dataLoan.sFHAPropImprovBorrHasNoBankAcount;
			sFHAPropImprovCoborHasChecking.Checked		= dataLoan.sFHAPropImprovCoborHasChecking;
			sFHAPropImprovCoborHasSaving.Checked		= dataLoan.sFHAPropImprovCoborHasSaving;
			sFHAPropImprovCoborHasNoBankAcount.Checked	= dataLoan.sFHAPropImprovCoborHasNoBankAcount;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));
            AddInitScriptFunctionWithArgs("_register_onchange", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));



        }
	}
}
