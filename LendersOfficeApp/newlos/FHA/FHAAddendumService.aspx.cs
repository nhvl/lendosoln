

namespace LendersOfficeApp.newlos.FHA
{
    using DataAccess;
    using LendersOffice.Common;
    using System;

    public class FHAAddendumServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHAAddendumServiceItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            if (dataLoan.sIsRequire2016FhaAddendum)
            {
                SetResult("sFhaDirectEndorsementAllConditionsOfApprovalMet", dataLoan.sFhaDirectEndorsementAllConditionsOfApprovalMet);
                SetResult("sFhaDirectEndorsementWasRatedAcceptAndUnderwritten", dataLoan.sFhaDirectEndorsementWasRatedAcceptAndUnderwritten);
                SetResult("aFhaBorrCertOtherProptoBeSoldNA", dataApp.aFhaBorrCertOtherProptoBeSoldNA);

                // ToString is called on the TriState enum to avoid special TriState logic when setting the result.
                SetResult("aFhaBorrCertWillOccupyFor1YearTri", dataApp.aFhaBorrCertWillOccupyFor1YearTri.ToString());

                SetResult("sFHACondOwnerOccNotRequired2", dataLoan.sFHACondOwnerOccNotRequired);
            }
            else
            {
                SetResult("sFHACondIfProposedConstBuilderCertified", dataLoan.sFHACondIfProposedConstBuilderCertified);
                SetResult("sFHACondIfNewConstLenderCertified", dataLoan.sFHACondIfNewConstLenderCertified);
                SetResult("sFHACondBuilderWarrantyRequired", dataLoan.sFHACondBuilderWarrantyRequired);
                SetResult("sFHACond10YrsWarranty", dataLoan.sFHACond10YrsWarranty);
                SetResult("sFHACondMortgageHighLtvForNonOccupant", dataLoan.sFHACondMortgageHighLtvForNonOccupant);
                SetResult("sFHACondOther", dataLoan.sFHACondOther);
                SetResult("sFHACondOtherDesc", dataLoan.sFHACondOtherDesc);
            }

            SetResult("aFHABorrCertReceivedLeadPaintPoisonInfoTri", dataApp.aFHABorrCertReceivedLeadPaintPoisonInfoTri);
            SetResult("sLT", dataLoan.sLT);
            SetResult("aFHABorrCertOwnOrSoldOtherFHAPropTri", dataApp.aFHABorrCertOwnOrSoldOtherFHAPropTri);
            SetResult("aFHABorrCertOtherPropToBeSoldTri", dataApp.aFHABorrCertOtherPropToBeSoldTri);
            SetResult("aFHABorrCertOtherPropCoveredByThisLoanTri", dataApp.aFHABorrCertOtherPropCoveredByThisLoanTri);
            SetResult("aFHABorrCertOwnMoreThan4DwellingsTri", dataApp.aFHABorrCertOwnMoreThan4DwellingsTri);
            SetResult("aHas1stTimeBuyerTri", dataApp.aHas1stTimeBuyerTri);

            SetResult("aVaVestTitleT", dataApp.aVaVestTitleT);

            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aBAddr", dataApp.aBAddr);
            SetResult("aBCity", dataApp.aBCity);
            SetResult("aBState", dataApp.aBState);
            SetResult("aBZip", dataApp.aBZip);
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);
            SetResult("sSpLegalDesc", dataLoan.sSpLegalDesc );
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            SetResult("sCaseAssignmentD", dataLoan.sCaseAssignmentD_rep);
            SetResult("sLenderCaseNum", dataLoan.sLenderCaseNum);
            SetResult("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sTermInYrRemainingMonths", dataLoan.sTermInYrRemainingMonths_rep);
            SetResult("sTermInYr", dataLoan.sTermInYr_rep);
            SetResult("sFHALDiscnt", dataLoan.sFHALDiscnt_rep);
            SetResult("sFHALDiscntLckd", dataLoan.sFHALDiscntLckd);
            SetResult("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            SetResult("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            SetResult("sFHAProMInsLckd", dataLoan.sFHAProMInsLckd);
            SetResult("sFHAProMInsMonLckd", dataLoan.sFHAProMInsMonLckd);
            SetResult("sFHAProMInsMon", dataLoan.sFHAProMInsMon_rep);
            SetResult("aFHABorrCertOtherPropStAddr", dataApp.aFHABorrCertOtherPropStAddr);
            SetResult("aFHABorrCertOtherPropCity", dataApp.aFHABorrCertOtherPropCity);
            SetResult("aFHABorrCertOtherPropState", dataApp.aFHABorrCertOtherPropState);
            SetResult("aFHABorrCertOtherPropZip", dataApp.aFHABorrCertOtherPropZip);
            SetResult("aFHABorrCertOtherPropSalesPrice", dataApp.aFHABorrCertOtherPropSalesPrice_rep);
            SetResult("aFHABorrCertOtherPropOrigMAmt", dataApp.aFHABorrCertOtherPropOrigMAmt_rep);
            SetResult("aFHABorrCertInformedPropVal", dataApp.aFHABorrCertInformedPropVal_rep);
            SetResult("aFHABorrCertInformedPropValDeterminedByVA", dataApp.aFHABorrCertInformedPropValDeterminedByVA);
            SetResult("aFHABorrCertInformedPropValDeterminedByHUD", dataApp.aFHABorrCertInformedPropValDeterminedByHUD);
            SetResult("aFHABorrCertInformedPropValAwareAtContractSigning", dataApp.aFHABorrCertInformedPropValAwareAtContractSigning);
            SetResult("aFHABorrCertInformedPropValNotAwareAtContractSigning", dataApp.aFHABorrCertInformedPropValNotAwareAtContractSigning);
            SetResult("sFHALenderIdCode", dataLoan.sFHALenderIdCode);
            SetResult("sVALenderIdCode", dataLoan.sVALenderIdCode);
            SetResult("sFHASponsorAgentIdCode", dataLoan.sFHASponsorAgentIdCode);
            SetResult("sVASponsorAgentIdCode", dataLoan.sVASponsorAgentIdCode);
            SetResult("sLPurposeT", dataLoan.sLPurposeT);
            SetResult("sFHAConstructionT", dataLoan.sFHAConstructionT);
            SetResult("sGseSpT", dataLoan.sGseSpT);
            SetResult("sLenderCaseNumLckd", dataLoan.sLenderCaseNumLckd);

            if (dataLoan.sFHAPurposeIsPurchaseExistHome)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsPurchaseExistHome");
            else if (dataLoan.sFHAPurposeIsConstructHome)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsConstructHome");
            else if (dataLoan.sFHAPurposeIsFinanceImprovement)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsFinanceImprovement");
            else if (dataLoan.sFHAPurposeIsFinanceCoopPurchase)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsFinanceCoopPurchase");
            else if (dataLoan.sFHAPurposeIsPurchaseManufacturedHome)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsPurchaseManufacturedHome");
            else if (dataLoan.sFHAPurposeIsPurchaseNewCondo)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsPurchaseNewCondo");
            else if (dataLoan.sFHAPurposeIsManufacturedHomeAndLot)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsManufacturedHomeAndLot");
            else if (dataLoan.sFHAPurposeIsPurchaseExistCondo)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsPurchaseExistCondo");
            else if (dataLoan.sFHAPurposeIsRefiManufacturedHomeOrLotLoan)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsRefiManufacturedHomeOrLotLoan");
            else if (dataLoan.sFHAPurposeIsPurchaseNewHome)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsPurchaseNewHome");
            else if (dataLoan.sFHAPurposeIsRefiManufacturedHomeToBuyLot)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsRefiManufacturedHomeToBuyLot");
            else if (dataLoan.sFHAPurposeIsRefinance)
                SetResult("sFHAPurposeOfLoanOption", "sFHAPurposeIsRefinance");
            else
                SetResult("sFHAPurposeOfLoanOption", "");

            SetResult("aVaBorrCertHadVaLoanTri", dataApp.aVaBorrCertHadVaLoanTri);

            if (dataLoan.sLT == E_sLT.VA)
            {
                if (dataApp.aFHABorrCertOccIsAsHome)
                    SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsAsHome");
                else if (dataApp.aFHABorrCertOccIsAsHomeForActiveSpouse)
                    SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsAsHomeForActiveSpouse");
                else if (dataApp.aFHABorrCertOccIsChildAsHome)
                   SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsChildAsHome");
                else if (dataApp.aFHABorrCertOccIsAsHomePrev)
                    SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsAsHomePrev");
                else if (dataApp.aFHABorrCertOccIsAsHomePrevForActiveSpouse)
                    SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsAsHomePrevForActiveSpouse");
                else if (dataApp.aFHABorrCertOccIsChildAsHomePrev)
                    SetResult("aFHABorrCertOcc", "aFHABorrCertOccIsChildAsHomePrev");
            }
            
            SetResult("aVaVestTitleODesc", dataApp.aVaVestTitleODesc);

            this.SetResult(nameof(dataLoan.sFhaLoanOrigCompanyNm), dataLoan.sFhaLoanOrigCompanyNm);
            this.SetResult(nameof(dataLoan.sFhaLoanOrigCompanyNmLckd), dataLoan.sFhaLoanOrigCompanyNmLckd);
            this.SetResult(nameof(dataLoan.sFHASponsoredOriginatorEIN), dataLoan.sFHASponsoredOriginatorEIN);
            this.SetResult(nameof(dataLoan.sFhaSponsoredOriginatorEinLckd), dataLoan.sFhaSponsoredOriginatorEinLckd);
            this.SetResult(nameof(dataLoan.sFhaLoanOrigCompanyNmlsId), dataLoan.sFhaLoanOrigCompanyNmlsId);
            this.SetResult(nameof(dataLoan.sFhaLoanOrigCompanyNmlsIdLckd), dataLoan.sFhaLoanOrigCompanyNmlsIdLckd);

            IPreparerFields field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("FHAAddendumLenderCompanyName", field.CompanyName);
            SetResult("FHAAddendumLenderStreetAddr", field.StreetAddr);
            SetResult("FHAAddendumLenderCity", field.City);
            SetResult("FHAAddendumLenderState", field.State);
            SetResult("FHAAddendumLenderZip", field.Zip);
            SetResult("FHAAddendumLenderPhoneOfCompany", field.PhoneOfCompany);
            SetResult("FHAAddendumLenderPreparerName", field.PreparerName);
            //SetResult("FHAAddendumLenderPhone", field.Phone);
            SetResult("FHAAddendumLenderTitle", field.Title);

            field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumSponsor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("FHAAddendumSponsorCompanyName", field.CompanyName);
            SetResult("FHAAddendumSponsorStreetAddr", field.StreetAddr);
            SetResult("FHAAddendumSponsorCity", field.City);
            SetResult("FHAAddendumSponsorState", field.State);
            SetResult("FHAAddendumSponsorZip", field.Zip);

            field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumDulyAgent, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("FHAAddendumDulyAgentPreparerName", field.PreparerName);
            SetResult("FHAAddendumDulyAgentStreetAddr", field.StreetAddr);
            SetResult("FHAAddendumDulyAgentCity", field.City);
            SetResult("FHAAddendumDulyAgentState", field.State);
            SetResult("FHAAddendumDulyAgentZip", field.Zip);
            SetResult("FHAAddendumDulyAgentTitle", field.Title);

            SetResult("sFHAAddendumUnderwriterNameLckd", dataLoan.sFHAAddendumUnderwriterNameLckd);
            SetResult("sFHAAddendumUnderwriterChumsIdLckd", dataLoan.sFHAAddendumUnderwriterChumsIdLckd);

            SetResult("sFHAAddendumUnderwriterName", dataLoan.sFHAAddendumUnderwriterName);
            SetResult("sFHAAddendumUnderwriterChumsId", dataLoan.sFHAAddendumUnderwriterChumsId);
            if (dataLoan.sIsRequire2016FhaAddendum)
            {
                SetResult("sFHAAddendumUnderwriterChumsId2", dataLoan.sFHAAddendumUnderwriterChumsId);
            }

            SetResult("sFHAAddendumMortgageeNameLckd", dataLoan.sFHAAddendumMortgageeNameLckd);
            SetResult("sFHAAddendumMortgageeName", dataLoan.sFHAAddendumMortgageeName);
            if (dataLoan.sIsRequire2016FhaAddendum)
            {
                SetResult("sFHAAddendumMortgageeTitle", dataLoan.sFHAAddendumMortgageeTitle);
            }


            SetResult("sFHAApprovedSubj", dataLoan.sFHAApprovedSubj);
            SetResult("sFHAApprovedD", dataLoan.sFHAApprovedD_rep);
            SetResult("sFHAApprovedExpiresD", dataLoan.sFHAApprovedExpiresD_rep);
            SetResult("sFHAApprovedModified", dataLoan.sFHAApprovedModified);
            SetResult("sFHAModifiedFinalLAmt", dataLoan.sFHAModifiedFinalLAmt_rep);
            SetResult("sFHAModifiedNoteIR", dataLoan.sFHAModifiedNoteIR_rep);
            SetResult("sFHAModifiedTermInYr", dataLoan.sFHAModifiedTermInYr_rep);
            SetResult("sFHAModifiedTermInMonths", dataLoan.sFHAModifiedTermInMonths_rep);
            SetResult("sFHAModifiedFfUfmip1003", dataLoan.sFHAModifiedFfUfmip1003_rep);
            SetResult("sFHAModifiedProMInsMon", dataLoan.sFHAModifiedProMInsMon_rep);
            SetResult("sFHAModifiedProMIns", dataLoan.sFHAModifiedProMIns_rep);
            SetResult("sFHAModifiedMonthlyPmt", dataLoan.sFHAModifiedMonthlyPmt_rep);

            SetResult("sFHACondOwnerOccNotRequired", dataLoan.sFHACondOwnerOccNotRequired);
            SetResult("sFHARatedAcceptedByTotalScorecard", dataLoan.sFHARatedAcceptedByTotalScorecard);
            SetResult("sFHARatedReferByTotalScorecard", dataLoan.sFHARatedReferByTotalScorecard);
            SetResult("sFHAMortgageHaveFinanceInterestTri", dataLoan.sFHAMortgageHaveFinanceInterestTri);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            if (dataLoan.sIsRequire2016FhaAddendum)
            {
                dataLoan.sFhaDirectEndorsementAllConditionsOfApprovalMet = GetBool("sFhaDirectEndorsementAllConditionsOfApprovalMet");
                dataLoan.sFhaDirectEndorsementWasRatedAcceptAndUnderwritten = GetBool("sFhaDirectEndorsementWasRatedAcceptAndUnderwritten");
            }
            else
            {
                dataLoan.sFHACondIfProposedConstBuilderCertified = GetBool("sFHACondIfProposedConstBuilderCertified");
                dataLoan.sFHACondIfNewConstLenderCertified = GetBool("sFHACondIfNewConstLenderCertified");
                dataLoan.sFHACondBuilderWarrantyRequired = GetBool("sFHACondBuilderWarrantyRequired");
                dataLoan.sFHACond10YrsWarranty = GetBool("sFHACond10YrsWarranty");
                dataLoan.sFHACondMortgageHighLtvForNonOccupant = GetBool("sFHACondMortgageHighLtvForNonOccupant");
                dataLoan.sFHACondOther = GetBool("sFHACondOther");
                dataLoan.sFHACondOtherDesc = GetString("sFHACondOtherDesc");
            }

            dataLoan.sLT                                                  = (E_sLT) GetInt("sLT");
            dataApp.aBLastNm                                              = GetString("aBLastNm");
            dataApp.aBMidNm                                               = GetString("aBMidNm");
            dataApp.aBFirstNm                                             = GetString("aBFirstNm");
            dataApp.aBSuffix                                              = GetString("aBSuffix");
            dataApp.aCLastNm                                              = GetString("aCLastNm");
            dataApp.aCMidNm                                               = GetString("aCMidNm");
            dataApp.aCFirstNm                                             = GetString("aCFirstNm");
            dataApp.aCSuffix                                              = GetString("aCSuffix");
            dataApp.aBAddr                                                = GetString("aBAddr");
            dataApp.aBCity                                                = GetString("aBCity");
            dataApp.aBState                                               = GetString("aBState");
            dataApp.aBZip                                                 = GetString("aBZip");
            dataLoan.sSpAddr                                              = GetString("sSpAddr");
            dataLoan.sSpCity                                              = GetString("sSpCity");
            dataLoan.sSpState                                             = GetString("sSpState");
            dataLoan.sSpZip                                               = GetString("sSpZip");
            dataLoan.sSpLegalDesc                                         = GetString("sSpLegalDesc");
            dataLoan.sNoteIR_rep                                          = GetString("sNoteIR");
            dataLoan.sAgencyCaseNum                                       = GetString("sAgencyCaseNum");
            dataLoan.sCaseAssignmentD_rep                                 = GetString("sCaseAssignmentD");
            dataLoan.sLenderCaseNum                                       = GetString("sLenderCaseNum");
            dataLoan.sFHAHousingActSection                                = GetString("sFHAHousingActSection");
            dataLoan.sFHALDiscnt_rep                                      = GetString("sFHALDiscnt");
            dataLoan.sFHALDiscntLckd                                      = GetBool("sFHALDiscntLckd");
            dataLoan.sFfUfmip1003_rep                                     = GetString("sFfUfmip1003");
            dataLoan.sFHAProMIns_rep                                      = GetString("sFHAProMIns");
            dataLoan.sFHAProMInsLckd                                      = GetBool("sFHAProMInsLckd");
            dataLoan.sFHAProMInsMonLckd                                   = GetBool("sFHAProMInsMonLckd");
            dataLoan.sFHAProMInsMon_rep                                   = GetString("sFHAProMInsMon");
            dataApp.aHas1stTimeBuyerTri                                   = GetTriState("aHas1stTimeBuyerTri");
            dataApp.aFHABorrCertOtherPropStAddr                          = GetString("aFHABorrCertOtherPropStAddr");
            dataApp.aFHABorrCertOtherPropCity                            = GetString("aFHABorrCertOtherPropCity");
            dataApp.aFHABorrCertOtherPropState                           = GetString("aFHABorrCertOtherPropState");
            dataApp.aFHABorrCertOtherPropZip                             = GetString("aFHABorrCertOtherPropZip");
            dataApp.aFHABorrCertOtherPropSalesPrice_rep                  = GetString("aFHABorrCertOtherPropSalesPrice");
            dataApp.aFHABorrCertOtherPropOrigMAmt_rep                    = GetString("aFHABorrCertOtherPropOrigMAmt");
            dataApp.aFHABorrCertInformedPropVal_rep                      = GetString("aFHABorrCertInformedPropVal");
            dataApp.aFHABorrCertInformedPropValDeterminedByVA            = GetBool("aFHABorrCertInformedPropValDeterminedByVA");
            dataApp.aFHABorrCertInformedPropValDeterminedByHUD           = GetBool("aFHABorrCertInformedPropValDeterminedByHUD");
            dataApp.aFHABorrCertInformedPropValAwareAtContractSigning    = GetBool("aFHABorrCertInformedPropValAwareAtContractSigning");
            dataApp.aFHABorrCertInformedPropValNotAwareAtContractSigning = GetBool("aFHABorrCertInformedPropValNotAwareAtContractSigning");
            dataApp.aFHABorrCertReceivedLeadPaintPoisonInfoTri           = GetTriState("aFHABorrCertReceivedLeadPaintPoisonInfoTri");
            dataLoan.sFHALenderIdCode                                    = GetString("sFHALenderIdCode");
            dataLoan.sVALenderIdCode                                     = GetString("sVALenderIdCode");
            dataLoan.sFHASponsorAgentIdCode                              = GetString("sFHASponsorAgentIdCode");
            dataLoan.sVASponsorAgentIdCode                               = GetString("sVASponsorAgentIdCode");
            dataApp.aFHABorrCertOwnOrSoldOtherFHAPropTri                 = GetTriState("aFHABorrCertOwnOrSoldOtherFHAPropTri");
            dataApp.aFHABorrCertOtherPropToBeSoldTri                     = GetTriState("aFHABorrCertOtherPropToBeSoldTri");
            dataApp.aFHABorrCertOtherPropCoveredByThisLoanTri            = GetTriState("aFHABorrCertOtherPropCoveredByThisLoanTri");
            dataApp.aFHABorrCertOwnMoreThan4DwellingsTri                 = GetTriState("aFHABorrCertOwnMoreThan4DwellingsTri");

            dataLoan.sLPurposeT = (E_sLPurposeT)GetInt("sLPurposeT");
            dataLoan.sFHAConstructionT = (E_sFHAConstructionT)GetInt("sFHAConstructionT");
            dataLoan.sGseSpT = (E_sGseSpT)GetInt("sGseSpT");

            string sFHAPurposeOfLoanOption = GetString("sFHAPurposeOfLoanOption", "");
            if (!String.IsNullOrEmpty(sFHAPurposeOfLoanOption))
            {
                dataLoan.sFHAPurposeIsPurchaseExistHome                   = (sFHAPurposeOfLoanOption == "sFHAPurposeIsPurchaseExistHome");
                dataLoan.sFHAPurposeIsConstructHome                       = (sFHAPurposeOfLoanOption == "sFHAPurposeIsConstructHome");
                dataLoan.sFHAPurposeIsFinanceImprovement                  = (sFHAPurposeOfLoanOption == "sFHAPurposeIsFinanceImprovement");
                dataLoan.sFHAPurposeIsFinanceCoopPurchase                 = (sFHAPurposeOfLoanOption == "sFHAPurposeIsFinanceCoopPurchase");
                dataLoan.sFHAPurposeIsPurchaseManufacturedHome            = (sFHAPurposeOfLoanOption == "sFHAPurposeIsPurchaseManufacturedHome");
                dataLoan.sFHAPurposeIsPurchaseNewCondo                    = (sFHAPurposeOfLoanOption == "sFHAPurposeIsPurchaseNewCondo");
                dataLoan.sFHAPurposeIsManufacturedHomeAndLot              = (sFHAPurposeOfLoanOption == "sFHAPurposeIsManufacturedHomeAndLot");
                dataLoan.sFHAPurposeIsPurchaseExistCondo                  = (sFHAPurposeOfLoanOption == "sFHAPurposeIsPurchaseExistCondo");
                dataLoan.sFHAPurposeIsRefiManufacturedHomeOrLotLoan       = (sFHAPurposeOfLoanOption == "sFHAPurposeIsRefiManufacturedHomeOrLotLoan");
                dataLoan.sFHAPurposeIsPurchaseNewHome                     = (sFHAPurposeOfLoanOption == "sFHAPurposeIsPurchaseNewHome");
                dataLoan.sFHAPurposeIsRefiManufacturedHomeToBuyLot        = (sFHAPurposeOfLoanOption == "sFHAPurposeIsRefiManufacturedHomeToBuyLot");
                dataLoan.sFHAPurposeIsRefinance                           = (sFHAPurposeOfLoanOption == "sFHAPurposeIsRefinance");
            }
            dataApp.aVaVestTitleT                                         = (E_aVaVestTitleT) GetInt("aVaVestTitleT");
            dataApp.aVaVestTitleODesc                                     = GetString("aVaVestTitleODesc");

            dataApp.aVaBorrCertHadVaLoanTri = GetTriState("aVaBorrCertHadVaLoanTri");

            string aFHABorrCertOcc = GetString("aFHABorrCertOcc", "");
            if (!string.IsNullOrEmpty(aFHABorrCertOcc)) // pretend this set of bools is actually a single enum
            {
                dataApp.aFHABorrCertOccIsAsHome                           = (aFHABorrCertOcc == "aFHABorrCertOccIsAsHome");
                dataApp.aFHABorrCertOccIsAsHomeForActiveSpouse            = (aFHABorrCertOcc == "aFHABorrCertOccIsAsHomeForActiveSpouse");
                dataApp.aFHABorrCertOccIsChildAsHome                      = (aFHABorrCertOcc == "aFHABorrCertOccIsChildAsHome");
                dataApp.aFHABorrCertOccIsAsHomePrev                       = (aFHABorrCertOcc == "aFHABorrCertOccIsAsHomePrev");
                dataApp.aFHABorrCertOccIsAsHomePrevForActiveSpouse        = (aFHABorrCertOcc == "aFHABorrCertOccIsAsHomePrevForActiveSpouse");
                dataApp.aFHABorrCertOccIsChildAsHomePrev                  = (aFHABorrCertOcc == "aFHABorrCertOccIsChildAsHomePrev");
            }


            IPreparerFields field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumLender, E_ReturnOptionIfNotExist.CreateNew)  ;
            field.CompanyName    = GetString("FHAAddendumLenderCompanyName");
            field.StreetAddr     = GetString("FHAAddendumLenderStreetAddr");
            field.City           = GetString("FHAAddendumLenderCity");
            field.State          = GetString("FHAAddendumLenderState");
            field.Zip            = GetString("FHAAddendumLenderZip");
            field.PhoneOfCompany = GetString("FHAAddendumLenderPhoneOfCompany");
            field.PreparerName   = GetString("FHAAddendumLenderPreparerName");
            //field.Phone          = GetString("FHAAddendumLenderPhone");
            field.Title          = GetString("FHAAddendumLenderTitle");
            field.Update();

            field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumSponsor, E_ReturnOptionIfNotExist.CreateNew) ;
            field.CompanyName = GetString("FHAAddendumSponsorCompanyName");
            field.StreetAddr  = GetString("FHAAddendumSponsorStreetAddr");
            field.City        = GetString("FHAAddendumSponsorCity");
            field.State       = GetString("FHAAddendumSponsorState");
            field.Zip         = GetString("FHAAddendumSponsorZip");
            field.Update();

            field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumDulyAgent, E_ReturnOptionIfNotExist.CreateNew);
            field.PreparerName = GetString("FHAAddendumDulyAgentPreparerName");
            field.StreetAddr = GetString("FHAAddendumDulyAgentStreetAddr");
            field.City = GetString("FHAAddendumDulyAgentCity");
            field.State = GetString("FHAAddendumDulyAgentState");
            field.Zip = GetString("FHAAddendumDulyAgentZip");
            field.Title = GetString("FHAAddendumDulyAgentTitle");
            field.Update();

            dataLoan.sFHAAddendumUnderwriterChumsIdLckd = GetBool("sFHAAddendumUnderwriterChumsIdLckd");
            dataLoan.sFHAAddendumUnderwriterNameLckd = GetBool("sFHAAddendumUnderwriterNameLckd");

            dataLoan.sFHAAddendumUnderwriterName = GetString("sFHAAddendumUnderwriterName");
            if (dataLoan.sIsRequire2016FhaAddendum)
            {
                dataLoan.sFHAAddendumUnderwriterChumsId = GetString("sFHAAddendumUnderwriterChumsId2");
            }
            else
            {
                dataLoan.sFHAAddendumUnderwriterChumsId = GetString("sFHAAddendumUnderwriterChumsId");
            }

            dataLoan.sFHAAddendumMortgageeNameLckd = GetBool("sFHAAddendumMortgageeNameLckd");
            dataLoan.sFHAAddendumMortgageeName = GetString("sFHAAddendumMortgageeName");
            if (dataLoan.sIsRequire2016FhaAddendum)
            {
                dataLoan.sFHAAddendumMortgageeTitle = GetString("sFHAAddendumMortgageeTitle");
            }

            dataLoan.sFHAApprovedSubj = GetBool("sFHAApprovedSubj");
            dataLoan.sFHAApprovedD_rep = GetString("sFHAApprovedD");
            dataLoan.sFHAApprovedExpiresD_rep = GetString("sFHAApprovedExpiresD") ;
            dataLoan.sFHAApprovedModified = GetBool("sFHAApprovedModified");
            dataLoan.sFHAModifiedFinalLAmt_rep = GetString("sFHAModifiedFinalLAmt");
            dataLoan.sFHAModifiedNoteIR_rep = GetString("sFHAModifiedNoteIR");
            dataLoan.sFHAModifiedTermInYr_rep = GetString("sFHAModifiedTermInYr");
            dataLoan.sFHAModifiedTermInMonths_rep = GetString("sFHAModifiedTermInMonths");
            dataLoan.sFHAModifiedFfUfmip1003_rep = GetString("sFHAModifiedFfUfmip1003") ;
            dataLoan.sFHAModifiedProMInsMon_rep = GetString("sFHAModifiedProMInsMon");
            dataLoan.sFHAModifiedProMIns_rep = GetString("sFHAModifiedProMIns");
            dataLoan.sFHAModifiedMonthlyPmt_rep = GetString("sFHAModifiedMonthlyPmt");

            if(dataLoan.sIsRequire2016FhaAddendum)
            {
                dataLoan.sFHACondOwnerOccNotRequired = GetBool("sFHACondOwnerOccNotRequired2");
            }
            else
            {
                dataLoan.sFHACondOwnerOccNotRequired = GetBool("sFHACondOwnerOccNotRequired");
            }

            dataLoan.sFHARatedAcceptedByTotalScorecard = GetBool("sFHARatedAcceptedByTotalScorecard");
            dataLoan.sFHARatedReferByTotalScorecard = GetBool("sFHARatedReferByTotalScorecard");
            dataLoan.sFHAMortgageHaveFinanceInterestTri = GetTriState("sFHAMortgageHaveFinanceInterestTri");
            dataLoan.sLenderCaseNumLckd = GetBool("sLenderCaseNumLckd");

            dataLoan.sFhaLoanOrigCompanyNm = this.GetString(nameof(dataLoan.sFhaLoanOrigCompanyNm));
            dataLoan.sFhaLoanOrigCompanyNmLckd = this.GetBool(nameof(dataLoan.sFhaLoanOrigCompanyNmLckd));
            dataLoan.sFHASponsoredOriginatorEIN = this.GetString(nameof(dataLoan.sFHASponsoredOriginatorEIN));
            dataLoan.sFhaSponsoredOriginatorEinLckd = this.GetBool(nameof(dataLoan.sFhaSponsoredOriginatorEinLckd));
            dataLoan.sFhaLoanOrigCompanyNmlsId = this.GetString(nameof(dataLoan.sFhaLoanOrigCompanyNmlsId));
            dataLoan.sFhaLoanOrigCompanyNmlsIdLckd = this.GetBool(nameof(dataLoan.sFhaLoanOrigCompanyNmlsIdLckd));
            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }
    }

	public partial class FHAAddendumService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FHAAddendumServiceItem());
        }
	}
}
