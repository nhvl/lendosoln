using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
    public class FHAPropertyImprovementServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHAPropertyImprovementServiceItem));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {

        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataLoan.sFHAPropImprovHasFHAPendingAppWithWhom          = GetString("sFHAPropImprovHasFHAPendingAppWithWhom");
            dataLoan.sFHAPropImprovRefinTitle1LoanNum                = GetString("sFHAPropImprovRefinTitle1LoanNum");
            dataLoan.sFHAPropImprovBorrRelativeNm                    = GetString("sFHAPropImprovBorrRelativeNm");
            dataLoan.sFHAPropImprovCoborRelativeNm                   = GetString("sFHAPropImprovCoborRelativeNm");
            dataLoan.sFHAPropImprovBorrRelativeRelationship          = GetString("sFHAPropImprovBorrRelativeRelationship");
            dataLoan.sFHAPropImprovCoborRelativeRelationship         = GetString("sFHAPropImprovCoborRelativeRelationship");
            dataLoan.sFHAPropImprovBorrRelativePhone                 = GetString("sFHAPropImprovBorrRelativePhone");
            dataLoan.sFHAPropImprovCoborRelativePhone                = GetString("sFHAPropImprovCoborRelativePhone");
            dataLoan.sFHAPropImprovIsPropSingleFamily                = GetBool("sFHAPropImprovIsPropSingleFamily");
            dataLoan.sFHAPropImprovIsPropOwnedByBorr                 = GetBool("sFHAPropImprovIsPropOwnedByBorr");
            dataLoan.sFHAPropImprovIsPropMultifamily                 = GetBool("sFHAPropImprovIsPropMultifamily");
            dataLoan.sUnitsNum_rep                                   = GetString("sUnitsNum");
            dataLoan.sFHAPropImprovIsPropLeasedFromSomeone           = GetBool("sFHAPropImprovIsPropLeasedFromSomeone");
            dataLoan.sFHAPropImprovIsPropNonresidential              = GetBool("sFHAPropImprovIsPropNonresidential");
            dataLoan.sFHAPropImprovNonresidentialUsageDesc           = GetString("sFHAPropImprovNonresidentialUsageDesc");
            dataLoan.sFHAPropImprovIsPropBeingPurchasedOnContract    = GetBool("sFHAPropImprovIsPropBeingPurchasedOnContract");
            dataLoan.sFHAPropImprovIsPropManufacturedHome            = GetBool("sFHAPropImprovIsPropManufacturedHome");
            dataLoan.sFHAPropImprovIsThereMortOnProp                 = GetBool("sFHAPropImprovIsThereMortOnProp");
            dataLoan.sFHAPropImprovIsPropHistoricResidential         = GetBool("sFHAPropImprovIsPropHistoricResidential");
            dataLoan.sFHAPropImprovIsPropHistoricResidentialUnitsNum = GetString("sFHAPropImprovIsPropHistoricResidentialUnitsNum");
            dataLoan.sFHAPropImprovIsPropHealthCareFacility          = GetBool("sFHAPropImprovIsPropHealthCareFacility");
            dataLoan.sFHAPropImprovLeaseMonPmt_rep                   = GetString("sFHAPropImprovLeaseMonPmt");
            dataLoan.sFHAPropImprovLeaseExpireD_rep                  = GetString("sFHAPropImprovLeaseExpireD");
            dataLoan.sFHAPropImprovIsPropNewOccMoreThan90Days        = GetBool("sFHAPropImprovIsPropNewOccMoreThan90Days");
            dataLoan.sFHAPropImprovDealerContractorContactInfo       = GetString("sFHAPropImprovDealerContractorContactInfo");
            //dataLoan.sFHAPropImprovBorrRelativeAddr                  = GetString("sFHAPropImprovBorrRelativeAddr");
            dataLoan.sFHAPropImprovBorrRelativeStreetAddress         = GetString("sFHAPropImprovBorrRelativeStreetAddress");
            dataLoan.sFHAPropImprovBorrRelativeCity                  = GetString("sFHAPropImprovBorrRelativeCity");
            dataLoan.sFHAPropImprovBorrRelativeState                 = GetString("sFHAPropImprovBorrRelativeState");
            dataLoan.sFHAPropImprovBorrRelativeZip                   = GetString("sFHAPropImprovBorrRelativeZip");
            //dataLoan.sFHAPropImprovCoborRelativeAddr                 = GetString("sFHAPropImprovCoborRelativeAddr");
            dataLoan.sFHAPropImprovCoborRelativeStreetAddress        = GetString("sFHAPropImprovCoborRelativeStreetAddress");
            dataLoan.sFHAPropImprovCoborRelativeCity                 = GetString("sFHAPropImprovCoborRelativeCity");
            dataLoan.sFHAPropImprovCoborRelativeState                = GetString("sFHAPropImprovCoborRelativeState");
            dataLoan.sFHAPropImprovCoborRelativeZip                  = GetString("sFHAPropImprovCoborRelativeZip");
            dataLoan.sFHAPropImprovBorrBankInfo                      = GetString("sFHAPropImprovBorrBankInfo");
            dataLoan.sFHAPropImprovCoborBankInfo                     = GetString("sFHAPropImprovCoborBankInfo");
            dataLoan.sFHAPropImprovLeaseOwnerInfo                    = GetString("sFHAPropImprovLeaseOwnerInfo");
            dataLoan.sFHAPropImprovRefinTitle1LoanTri                = GetTriState("sFHAPropImprovRefinTitle1LoanTri");
            dataLoan.sFHAPropImprovHasFHAPendingAppTri               = GetTriState("sFHAPropImprovHasFHAPendingAppTri");
            dataLoan.sFHAPropImprovHasFedPastDueTri                  = GetTriState("sFHAPropImprovHasFedPastDueTri");
            dataLoan.sFHAPropImprovRefinTitle1LoanBal_rep            = GetString("sFHAPropImprovRefinTitle1LoanBal");
            dataLoan.sFHAPropImprovBorrHasChecking                   = GetBool("sFHAPropImprovBorrHasChecking");
            dataLoan.sFHAPropImprovBorrHasSaving                     = GetBool("sFHAPropImprovBorrHasSaving");
            dataLoan.sFHAPropImprovBorrHasNoBankAcount               = GetBool("sFHAPropImprovBorrHasNoBankAcount");
            dataLoan.sFHAPropImprovCoborHasChecking                  = GetBool("sFHAPropImprovCoborHasChecking");
            dataLoan.sFHAPropImprovCoborHasSaving                    = GetBool("sFHAPropImprovCoborHasSaving");
            dataLoan.sFHAPropImprovCoborHasNoBankAcount              = GetBool("sFHAPropImprovCoborHasNoBankAcount");

        }


    }
	public partial class FHAPropertyImprovementService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FHAPropertyImprovementServiceItem());
        }
	}
}
