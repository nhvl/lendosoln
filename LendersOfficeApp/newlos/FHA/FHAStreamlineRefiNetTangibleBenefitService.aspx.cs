﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
    public partial class FHAStreamlineRefiNetTangibleBenefitService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new FHAStreamlineRefiNetTangibleBenefitServiceItem());
        }
    }
    
    public class FHAStreamlineRefiNetTangibleBenefitServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHAStreamlineRefiNetTangibleBenefitServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            #region Streamline Refi Without Appraisal
            // Section A
            dataLoan.sVaLoanAmtCurrentLoan_rep = GetString("sVaLoanAmtCurrentLoan");

            // Section B
            dataLoan.s30DayInterestOldLoan_rep = GetString("s30DayInterestOldLoan");
            dataLoan.sFHACcTot_rep = GetString("sFHACcTot");
            dataLoan.sFHADiscountPoints_rep = GetString("sFHADiscountPoints");
            dataLoan.sFHAPrepaidExp_rep = GetString("sFHAPrepaidExp");

            // Section C
            dataLoan.sFHASalesConcessions_rep = GetString("sFHASalesConcessions");
            #endregion

            #region Net Tangible Benefit
            dataLoan.sTotalCurrentPIAndMonMIP_rep = GetString("sTotalCurrentPIAndMonMIP");
            #endregion
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            #region Streamline Refi Without Appraisal
            // Section A
            SetResult("sOriginalAppraisedValue", dataLoan.sOriginalAppraisedValue_rep);
            SetResult("sVaLoanAmtCurrentLoan", dataLoan.sVaLoanAmtCurrentLoan_rep);
            SetResult("sIsTermLTE15Yr", dataLoan.sIsTermLTE15Yrs ? "Y" : "N");
            SetResult("sFfUfMipIsBeingFinanced", dataLoan.sFfUfMipIsBeingFinanced ? "Y" : "N");
            SetResult("sProdIsLoanEndorsedBeforeJune09", dataLoan.sProdIsLoanEndorsedBeforeJune09 ? "Y" : "N");

            // Section B
            SetResult("sSpLien", dataLoan.sSpLien_rep);
            SetResult("s30DayInterestOldLoan", dataLoan.s30DayInterestOldLoan_rep);
            SetResult("sFHATotalPayoffBalance", dataLoan.sFHATotalPayoffBalance_rep);
            SetResult("sFHACcTot", dataLoan.sFHACcTot_rep);
            SetResult("sFHADiscountPoints", dataLoan.sFHADiscountPoints_rep);
            SetResult("sFHAPrepaidExp", dataLoan.sFHAPrepaidExp_rep);
            SetResult("sFHATotalNewLoanCosts", dataLoan.sFHATotalNewLoanCosts_rep);

            // Section C
            SetResult("sFHASalesConcessions", dataLoan.sFHASalesConcessions_rep);
            SetResult("sFHANewMaximumBaseLAmt", dataLoan.sFHANewMaximumBaseLAmt_rep);

            // Section D
            SetResult("sFHAAmountNeededToClose", dataLoan.sFHAAmountNeededToClose_rep);

            // Section E
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sFfUfmipR", dataLoan.sFfUfmipR_rep);
            SetResult("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);

            // Section F
            SetResult("sFHALtvR", dataLoan.sFHALtvR_rep);
            SetResult("sProMInsR", dataLoan.sProMInsR_rep);
            #endregion

            #region Net Tangible Benefit
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            SetResult("sProMInsR", dataLoan.sProMInsR_rep);
            SetResult("sProThisMPmtAndMIns", dataLoan.sProThisMPmtAndMIns_rep);
            SetResult("sProMIns", dataLoan.sProMIns_rep);
            SetResult("sTotalCurrentPIAndMonMIP", dataLoan.sTotalCurrentPIAndMonMIP_rep);
            SetResult("sProThisMPmtAndMIns", dataLoan.sProThisMPmtAndMIns_rep);
            SetResult("sFHAMPmtDiffPc", dataLoan.sFHAMPmtDiffPc_rep);
            #endregion
        }
    }
}
