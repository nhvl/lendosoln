namespace LendersOfficeApp.newlos.FHA
{
    using DataAccess;
    using LendersOffice.Common;
    using System;

    public class FHA203kWorksheetServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHA203kWorksheetServiceItem));
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            dataApp.aCFirstNm                            = GetString("aCFirstNm") ;
            dataApp.aBFirstNm                            = GetString("aBFirstNm") ;
            dataApp.aBLastNm                             = GetString("aBLastNm") ;
            dataLoan.sSpAddr                             = GetString("sSpAddr") ;
            dataLoan.sSpCity                             = GetString("sSpCity") ;
            dataLoan.sSpState                            = GetString("sSpState") ;
            dataLoan.sSpZip                              = GetString("sSpZip") ;
            dataLoan.sAgencyCaseNum                      = GetString("sAgencyCaseNum") ;
            dataLoan.sFHA203kSpHudOwnedTri               = (E_TriState) GetInt("sFHA203kSpHudOwnedTri");
            
            dataApp.aOccT                                = (E_aOccT) GetInt("aOccT") ;
            dataLoan.sFHAIsGovAgency                     = GetBool("sFHAIsGovAgency") ;
            dataLoan.sFHAIsNonProfit                     = GetBool("sFHAIsNonProfit") ;
            dataLoan.sLPurposeT                          = (E_sLPurposeT) GetInt("sLPurposeT") ;
            dataLoan.sFHAExistingMLien_rep               = GetString("sFHAExistingMLien") ;
            dataLoan.sFHAIsExistingDebt                  = GetBool("sFHAIsExistingDebt") ;
            dataLoan.sFHASpAsIsVal_rep                   = GetString("sFHASpAsIsVal") ;
            dataLoan.sFHASpAfterImprovedVal_rep          = GetString("sFHASpAfterImprovedVal") ;
            dataLoan.sFHA203kBorPdCc_rep                 = GetString("sFHA203kBorPdCc") ;
            dataLoan.sFHA203kAllowEnergyImprovement_rep  = GetString("sFHA203kAllowEnergyImprovement") ;
            dataLoan.sFHA203kRepairCost_rep              = GetString("sFHA203kRepairCost") ;
            dataLoan.sFHA203kRepairCostReserveR_rep      = GetString("sFHA203kRepairCostReserveR") ;
            dataLoan.sFHA203kInspectCount_rep            = GetString("sFHA203kInspectCount") ;
            dataLoan.sFHA203kCostPerInspect_rep          = GetString("sFHA203kCostPerInspect") ;
            dataLoan.sFHA203kTitleDrawCount_rep          = GetString("sFHA203kTitleDrawCount") ;
            dataLoan.sFHA203kCostPerTitleDraw_rep        = GetString("sFHA203kCostPerTitleDraw") ;
            dataLoan.sFHA203kMonhtlyPmtEscrowed_rep      = GetString("sFHA203kMonhtlyPmtEscrowed") ;
            dataLoan.sFHA203kPmtEscrowedMonths_rep       = GetString("sFHA203kPmtEscrowedMonths") ;
            dataLoan.sFHA203kEngineeringFee_rep          = GetString("sFHA203kEngineeringFee") ;
            dataLoan.sFHA203kConsultantFee_rep           = GetString("sFHA203kConsultantFee") ;
            dataLoan.sFHA203kOtherFee_rep                = GetString("sFHA203kOtherFee") ;
            dataLoan.sFHA203kPlanReviewerMiles_rep       = GetString("sFHA203kPlanReviewerMiles") ;
            dataLoan.sFHA203kPlanReviewerCostPerMile_rep = GetString("sFHA203kPlanReviewerCostPerMile") ;
            dataLoan.sFHA203kRepairDiscountFeePt_rep     = GetString("sFHA203kRepairDiscountFeePt") ;
            dataLoan.sFHA203kRequiredAdjustment_rep      = GetString("sFHA203kRequiredAdjustment");
            dataLoan.sFHA203kMaxMAmtC5Lckd               = GetBool("sFHA203kMaxMAmtC5Lckd") ;
            dataLoan.sFHA203kMaxMAmtC5_rep               = GetString("sFHA203kMaxMAmtC5") ;
            dataLoan.sFHA203kD1Lckd                      = GetBool("sFHA203kD1Lckd") ;
            dataLoan.sFHA203kD1_rep                      = GetString("sFHA203kD1") ;
            dataLoan.sFHA203kMaxMAmtD5Lckd               = GetBool("sFHA203kMaxMAmtD5Lckd") ;
            dataLoan.sFHA203kMaxMAmtD5_rep               = GetString("sFHA203kMaxMAmtD5") ;
            dataLoan.sFHA203kEnergyEfficientMAmt_rep     = GetString("sFHA203kEnergyEfficientMAmt") ;
            dataLoan.sFHA203kRemarks                     = GetString("sFHA203kRemarks") ;
            dataLoan.sFHA203kEscrowedFundsTot_rep        = GetString("sFHA203kEscrowedFundsTot") ;
            dataLoan.sNoteIR_rep                         = GetString("sNoteIR") ;
            dataLoan.sLDiscntPc_rep                      = GetString("sLDiscntPc") ;
            dataApp.aBMidNm                              = GetString("aBMidNm") ;
            dataApp.aCMidNm                              = GetString("aCMidNm") ;
            dataApp.aCSuffix                             = GetString("aCSuffix") ;
            dataApp.aBSuffix                             = GetString("aBSuffix") ;
            dataLoan.sUnitsNum_rep                       = GetString("sUnitsNum") ;
            dataApp.aCLastNm                             = GetString("aCLastNm") ;
            dataLoan.sFHA203kFHAMipRefundLckd = GetBool("sFHA203kFHAMipRefundLckd");
            dataLoan.sFHA203kFHAMipRefund_rep = GetString("sFHA203kFHAMipRefund");
            dataLoan.sFHA203kD3LtvFactorR_rep = GetString("sFHA203kD3LtvFactorR");

            dataLoan.sFHA203kUnderwriterChumsIdLckd = GetBool("sFHA203kUnderwriterChumsIdLckd");
            dataLoan.sFHA203kUnderwriterNameLckd = GetBool("sFHA203kUnderwriterNameLckd");

            dataLoan.sFHA203kUnderwriterChumsId = GetString("sFHA203kUnderwriterChumsId");
            dataLoan.sFHA203kUnderwriterName = GetString("sFHA203kUnderwriterName");

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);
            SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            SetResult("sFHA203kSpHudOwnedTri", dataLoan.sFHA203kSpHudOwnedTri);
            
            SetResult("aOccT", dataApp.aOccT);
            SetResult("sFHAIsGovAgency", dataLoan.sFHAIsGovAgency);
            SetResult("sFHAIsNonProfit", dataLoan.sFHAIsNonProfit);
            SetResult("sLPurposeT", dataLoan.sLPurposeT);
            SetResult("sFHAExistingMLien", dataLoan.sFHAExistingMLien_rep);
            SetResult("sFHAIsExistingDebt", dataLoan.sFHAIsExistingDebt);
            SetResult("sFHASpAfterImprovedVal110pc", dataLoan.sFHASpAfterImprovedVal110pc_rep);
            SetResult("sFHASpAsIsVal", dataLoan.sFHASpAsIsVal_rep);
            SetResult("sFHASpAfterImprovedVal", dataLoan.sFHASpAfterImprovedVal_rep);
            SetResult("sFHA203kBorPdCc", dataLoan.sFHA203kBorPdCc_rep);
            SetResult("sFHA203kAllowEnergyImprovement", dataLoan.sFHA203kAllowEnergyImprovement_rep);
            SetResult("sFHA203kRepairCost", dataLoan.sFHA203kRepairCost_rep);
            SetResult("sFHA203kRepairCostReserveR", dataLoan.sFHA203kRepairCostReserveR_rep);
            SetResult("sFHA203kRepairCostReserve", dataLoan.sFHA203kRepairCostReserve_rep);
            SetResult("sFHA203kInspectCount", dataLoan.sFHA203kInspectCount_rep);
            SetResult("sFHA203kCostPerInspect", dataLoan.sFHA203kCostPerInspect_rep);
            SetResult("sFHA203kTitleDrawCount", dataLoan.sFHA203kTitleDrawCount_rep);
            SetResult("sFHA203kCostPerTitleDraw", dataLoan.sFHA203kCostPerTitleDraw_rep);
            SetResult("sFHA203kInspectAndTitleFee", dataLoan.sFHA203kInspectAndTitleFee_rep);
            SetResult("sFHA203kMonhtlyPmtEscrowed", dataLoan.sFHA203kMonhtlyPmtEscrowed_rep);
            SetResult("sFHA203kPmtEscrowedMonths", dataLoan.sFHA203kPmtEscrowedMonths_rep);
            SetResult("sFHA203kPmtEscrowed", dataLoan.sFHA203kPmtEscrowed_rep);
            SetResult("sFHA203kRehabEscrowAccountSubtot", dataLoan.sFHA203kRehabEscrowAccountSubtot_rep);
            SetResult("sFHA203kEngineeringFee", dataLoan.sFHA203kEngineeringFee_rep);
            SetResult("sFHA203kConsultantFee", dataLoan.sFHA203kConsultantFee_rep);
            SetResult("sFHA203kOtherFee", dataLoan.sFHA203kOtherFee_rep);
            SetResult("sFHA203kPlanReviewerMiles", dataLoan.sFHA203kPlanReviewerMiles_rep);
            SetResult("sFHA203kPlanReviewerCostPerMile", dataLoan.sFHA203kPlanReviewerCostPerMile_rep);
            SetResult("sFHA203kPlanReviewerFee", dataLoan.sFHA203kPlanReviewerFee_rep);
            SetResult("sFHA203kB5toB9Subtot", dataLoan.sFHA203kB5toB9Subtot_rep);
            SetResult("sFHA203kSupplementOrigFee", dataLoan.sFHA203kSupplementOrigFee_rep);
            SetResult("sFHA203kRepairDiscountFeePt", dataLoan.sFHA203kRepairDiscountFeePt_rep);
            SetResult("sFHA203kRepairDiscount", dataLoan.sFHA203kRepairDiscount_rep);
            SetResult("sFHA203kReleaseAtClosingSubtot", dataLoan.sFHA203kReleaseAtClosingSubtot_rep);
            SetResult("sFHA203kRehabCostTot", dataLoan.sFHA203kRehabCostTot_rep);
            SetResult("sFHA203kSalesPriceOrAsIsVal", dataLoan.sFHA203kSalesPriceOrAsIsVal_rep);
            SetResult("sFHA203kRehabCostTot2", dataLoan.sFHA203kRehabCostTot_rep);
            SetResult("sFHA203kC1PlusC2", dataLoan.sFHA203kC1PlusC2_rep);
            SetResult("sFHA203kC1PlusC2OrA4", dataLoan.sFHA203kC1PlusC2OrA4_rep);
            SetResult("sFHA203kRequiredAdjustment", dataLoan.sFHA203kRequiredAdjustment_rep);
            SetResult("sFHA203kInvestmentRequired", dataLoan.sFHA203kInvestmentRequired_rep);
            SetResult("sFHA203kC5Part2", dataLoan.sFHA203kC5Part2_rep);
            SetResult("sFHA203kMaxMAmtC5Lckd", dataLoan.sFHA203kMaxMAmtC5Lckd);
            SetResult("sFHA203kMaxMAmtC5", dataLoan.sFHA203kMaxMAmtC5_rep);
            SetResult("sFHA203kFHAMipRefundLckd", dataLoan.sFHA203kFHAMipRefundLckd);
            SetResult("sFHA203kFHAMipRefund", dataLoan.sFHA203kFHAMipRefund_rep);
            SetResult("sFHA203kD1Lckd", dataLoan.sFHA203kD1Lckd);
            SetResult("sFHA203kD1", dataLoan.sFHA203kD1_rep);
            SetResult("sFHA203kA2PlusB14", dataLoan.sFHA203kA2PlusB14_rep);
            SetResult("sFHA203kD2", dataLoan.sFHA203kD2_rep);
            SetResult("sFHA203kD22", dataLoan.sFHA203kD2_rep);
            SetResult("sFHA203kD4", dataLoan.sFHA203kD4_rep);
            SetResult("sFHA203kMaxMAmtD5Lckd", dataLoan.sFHA203kMaxMAmtD5Lckd);
            SetResult("sFHA203kMaxMAmtD5", dataLoan.sFHA203kMaxMAmtD5_rep);
            SetResult("sFHA203kEnergyEfficientMAmt", dataLoan.sFHA203kEnergyEfficientMAmt_rep);
            SetResult("sFHA203kRemarks", dataLoan.sFHA203kRemarks);
            SetResult("sFHA203kEscrowedFundsTot", dataLoan.sFHA203kEscrowedFundsTot_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("sUnitsNum", dataLoan.sUnitsNum_rep);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("sFHA203kD3LtvFactorR", dataLoan.sFHA203kD3LtvFactorR_rep);

            SetResult("sFHA203kUnderwriterChumsIdLckd", dataLoan.sFHA203kUnderwriterChumsIdLckd);
            SetResult("sFHA203kUnderwriterNameLckd", dataLoan.sFHA203kUnderwriterNameLckd);
            SetResult("sFHA203kUnderwriterChumsId", dataLoan.sFHA203kUnderwriterChumsId);
            SetResult("sFHA203kUnderwriterName", dataLoan.sFHA203kUnderwriterName);
        }

    }


	public partial class FHA203kWorksheetService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FHA203kWorksheetServiceItem());
        }
	}
}
