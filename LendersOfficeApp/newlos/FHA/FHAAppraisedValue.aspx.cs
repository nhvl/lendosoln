using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{

	public partial class FHAAppraisedValue : BaseLoanPage
	{
        #region Protected member variables
        #endregion
    
		protected void PageInit(object sender, System.EventArgs e)
		{
            Tools.Bind_sFHAConstructionT(sFHAConstructionT);
            FHACondCommitMortgageeZip.SmartZipcode(FHACondCommitMortgageeCity, FHACondCommitMortgageeState);
            sSpZip.SmartZipcode(sSpCity, sSpState, sSpCounty);
            sFHACondCommMaxFinanceEligibleTri.Load += new System.EventHandler(this.ZeroOneCheckBoxList_Load);
            Tools.Bind_sProRealETxT(sProRealETxT);
            Tools.Bind_PercentBaseT(sProHazInsT);
            Tools.Bind_sFHAHousingActSection(sFHAHousingActSection); 

            this.PageTitle = "Statement of Appraised Value";
            this.PageID = "FHA_92800_5B";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CHUD_92800_5B_1PDF);


			this.sSpState.Attributes.Add( "onchange", "javascript:UpdateCounties(this,document.getElementById('" + sSpCounty.ClientID + "'), event)");
		}

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAAppraisedValue));
            dataLoan.InitLoad();

            sFHALenderIdCode.Text = dataLoan.sFHALenderIdCode;
            sFHASponsorAgentIdCode.Text = dataLoan.sFHASponsorAgentIdCode;
            sFHACondCommUnderNationalHousingAct.Checked = dataLoan.sFHACondCommUnderNationalHousingAct;
            sFHAHousingActSection.Text = dataLoan.sFHAHousingActSection; // opm 175879 from sFHACondCommNationalHousingActSection
            sFHACondCommSeeBelow.Checked = dataLoan.sFHACondCommSeeBelow;
            sFHACondCommInstCaseRef.Text = dataLoan.sFHACondCommInstCaseRef;
            sApprVal.Text = dataLoan.sApprVal_rep;
            Tools.SetDropDownListValue(sFHAConstructionT, dataLoan.sFHAConstructionT);
            sProHazIns.Text = dataLoan.sProHazIns_rep;
            sProRealETx.Text = dataLoan.sProRealETx_rep;
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;

			// 01-14-08 av 18913 

			Tools.Bind_sSpCounty(dataLoan.sSpState,sSpCounty, true);
			Tools.SetDropDownListCaseInsensitive(sSpCounty,dataLoan.sSpCounty);

			sFHACondCommIssuedD.Text = dataLoan.sFHACondCommIssuedD_rep;
            sFHACondCommExpiredD.Text = dataLoan.sFHACondCommExpiredD_rep;
            sFHACondCommImprovedLivingArea.Text = dataLoan.sFHACondCommImprovedLivingArea;
            sProHoAssocDues.Text = dataLoan.sProHoAssocDues_rep;
            sProHoAssocDues.ReadOnly = dataLoan.sHoaDuesExpenseInDisbMode;
            sProOHExp.Text = dataLoan.sProOHExp_rep;
            sProOHExpLckd.Checked = dataLoan.sProOHExpLckd;
            sFHACondCommMonExpenseEstimate.Text = dataLoan.sFHACondCommMonExpenseEstimate_rep;
            sFHACondCommRemainEconLife.Text = dataLoan.sFHACondCommRemainEconLife;
            Tools.Set_TriState(sFHACondCommMaxFinanceEligibleTri, dataLoan.sFHACondCommMaxFinanceEligibleTri);

            sFHACondCommIsManufacturedHousing.Checked = dataLoan.sFHACondCommIsManufacturedHousing;
            sFHACondCommIsAssuranceOfCompletion.Checked = dataLoan.sFHACondCommIsAssuranceOfCompletion;
            sFHACondCommAssuranceOfCompletionAmt.Text = dataLoan.sFHACondCommAssuranceOfCompletionAmt_rep;
            sFHACondCommSeeAttached.Checked = dataLoan.sFHACondCommSeeAttached;
            sFHACondCommSeeAttachedDesc.Text = dataLoan.sFHACondCommSeeAttachedDesc;
            sFHACondCommSeeFollowingCondsOnBack.Checked = dataLoan.sFHACondCommSeeFollowingCondsOnBack;
            sFHACondCommCondsOnBack2.Text = dataLoan.sFHACondCommCondsOnBack2;
            sFHACondCommCondsOnBack3.Text = dataLoan.sFHACondCommCondsOnBack3;
            sFHACondCommCondsOnBack4.Text = dataLoan.sFHACondCommCondsOnBack4;
            sFHACondCommCondsOnBack5.Text = dataLoan.sFHACondCommCondsOnBack5;
            sFHACondCommCondsOnBack6.Text = dataLoan.sFHACondCommCondsOnBack6;
            sFHACondCommCondsOnBack7.Text = dataLoan.sFHACondCommCondsOnBack7;
            sFHACondCommCondsOnBack1.Text = dataLoan.sFHACondCommCondsOnBack1;

            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;


            Tools.SetDropDownListValue(sProHazInsT, dataLoan.sProHazInsT);
            sProHazInsR.Text = dataLoan.sProHazInsR_rep;
            sProHazInsMb.Text = dataLoan.sProHazInsMb_rep;
            sProHazInsHolder.Visible = !dataLoan.sHazardExpenseInDisbursementMode;
            Tools.SetDropDownListValue(sProRealETxT, dataLoan.sProRealETxT);
            sProRealETxR.Text = dataLoan.sProRealETxR_rep;
            sProRealETxMb.Text = dataLoan.sProRealETxMb_rep;
            sProRealETxHolder.Visible = !dataLoan.sRealEstateTaxExpenseInDisbMode;

            IPreparerFields field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACondCommitMortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHACondCommitMortgageeCompanyName.Text = field.CompanyName;
            FHACondCommitMortgageeStreetAddr.Text = field.StreetAddr;
            FHACondCommitMortgageeCity.Text = field.City;
            FHACondCommitMortgageeState.Value = field.State;
            FHACondCommitMortgageeZip.Text = field.Zip;

            field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAConditionalCommitment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHAConditionalCommitmentPreparerName.Text = field.PreparerName;
            FHAConditionalCommitmentNamePrepareDate.Text = field.PrepareDate_rep;


        }

        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        

            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));

        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion




	}
}
