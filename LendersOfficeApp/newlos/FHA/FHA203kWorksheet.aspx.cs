using System;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
	public partial class FHA203kWorksheet : BaseLoanPage
	{

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHA203kWorksheet));
			
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData( ApplicationID );

            aCFirstNm.Text = dataApp.aCFirstNm;
            aBFirstNm.Text = dataApp.aBFirstNm;
            aBLastNm.Text = dataApp.aBLastNm;
            sSpAddr.Text = dataLoan.sSpAddr;
            sSpCity.Text = dataLoan.sSpCity;
            sSpState.Value = dataLoan.sSpState;
            sSpZip.Text = dataLoan.sSpZip;
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            Tools.SetDropDownListValue(sFHA203kSpHudOwnedTri, dataLoan.sFHA203kSpHudOwnedTri);

            Tools.SetDropDownListValue(aOccT, dataApp.aOccT);
            sFHAIsGovAgency.Checked = dataLoan.sFHAIsGovAgency;
            sFHAIsNonProfit.Checked = dataLoan.sFHAIsNonProfit;
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            sFHAExistingMLien.Text = dataLoan.sFHAExistingMLien_rep;
            sFHAIsExistingDebt.Checked = dataLoan.sFHAIsExistingDebt;
            sFHASpAfterImprovedVal110pc.Text = dataLoan.sFHASpAfterImprovedVal110pc_rep;
            sFHASpAsIsVal.Text = dataLoan.sFHASpAsIsVal_rep;
            sFHASpAfterImprovedVal.Text = dataLoan.sFHASpAfterImprovedVal_rep;
            sFHA203kBorPdCc.Text = dataLoan.sFHA203kBorPdCc_rep;
            sFHA203kAllowEnergyImprovement.Text = dataLoan.sFHA203kAllowEnergyImprovement_rep;
            sFHA203kRepairCost.Text = dataLoan.sFHA203kRepairCost_rep;
            sFHA203kRepairCostReserveR.Text = dataLoan.sFHA203kRepairCostReserveR_rep;
            sFHA203kRepairCostReserve.Text = dataLoan.sFHA203kRepairCostReserve_rep;
            sFHA203kInspectCount.Text = dataLoan.sFHA203kInspectCount_rep;
            sFHA203kCostPerInspect.Text = dataLoan.sFHA203kCostPerInspect_rep;
            sFHA203kTitleDrawCount.Text = dataLoan.sFHA203kTitleDrawCount_rep;
            sFHA203kCostPerTitleDraw.Text = dataLoan.sFHA203kCostPerTitleDraw_rep;
            sFHA203kInspectAndTitleFee.Text = dataLoan.sFHA203kInspectAndTitleFee_rep;
            sFHA203kMonhtlyPmtEscrowed.Text = dataLoan.sFHA203kMonhtlyPmtEscrowed_rep;
            sFHA203kPmtEscrowedMonths.Text = dataLoan.sFHA203kPmtEscrowedMonths_rep;
            sFHA203kPmtEscrowed.Text = dataLoan.sFHA203kPmtEscrowed_rep;
            sFHA203kRehabEscrowAccountSubtot.Text = dataLoan.sFHA203kRehabEscrowAccountSubtot_rep;
            sFHA203kEngineeringFee.Text = dataLoan.sFHA203kEngineeringFee_rep;
            sFHA203kConsultantFee.Text = dataLoan.sFHA203kConsultantFee_rep;
            sFHA203kOtherFee.Text = dataLoan.sFHA203kOtherFee_rep;
            sFHA203kPlanReviewerMiles.Text = dataLoan.sFHA203kPlanReviewerMiles_rep;
            sFHA203kPlanReviewerCostPerMile.Text = dataLoan.sFHA203kPlanReviewerCostPerMile_rep;
            sFHA203kPlanReviewerFee.Text = dataLoan.sFHA203kPlanReviewerFee_rep;
            sFHA203kB5toB9Subtot.Text = dataLoan.sFHA203kB5toB9Subtot_rep;
            sFHA203kSupplementOrigFee.Text = dataLoan.sFHA203kSupplementOrigFee_rep;
            sFHA203kRepairDiscountFeePt.Text = dataLoan.sFHA203kRepairDiscountFeePt_rep;
            sFHA203kRepairDiscount.Text = dataLoan.sFHA203kRepairDiscount_rep;
            sFHA203kReleaseAtClosingSubtot.Text = dataLoan.sFHA203kReleaseAtClosingSubtot_rep;
            sFHA203kRehabCostTot.Text = dataLoan.sFHA203kRehabCostTot_rep;
            sFHA203kSalesPriceOrAsIsVal.Text = dataLoan.sFHA203kSalesPriceOrAsIsVal_rep;
            sFHA203kRehabCostTot2.Text = dataLoan.sFHA203kRehabCostTot_rep;
            sFHA203kC1PlusC2.Text = dataLoan.sFHA203kC1PlusC2_rep;
            sFHA203kC1PlusC2OrA4.Text = dataLoan.sFHA203kC1PlusC2OrA4_rep;
            sFHA203kRequiredAdjustment.Text = dataLoan.sFHA203kRequiredAdjustment_rep;
            sFHA203kC5Part2.Text = dataLoan.sFHA203kC5Part2_rep;
            sFHA203kMaxMAmtC5Lckd.Checked = dataLoan.sFHA203kMaxMAmtC5Lckd;
            sFHA203kMaxMAmtC5.Text = dataLoan.sFHA203kMaxMAmtC5_rep;
            sFHA203kFHAMipRefundLckd.Checked = dataLoan.sFHA203kFHAMipRefundLckd;
            sFHA203kFHAMipRefund.Text = dataLoan.sFHA203kFHAMipRefund_rep;
            sFHA203kD1Lckd.Checked = dataLoan.sFHA203kD1Lckd;
            sFHA203kD1.Text = dataLoan.sFHA203kD1_rep;
            sFHA203kA2PlusB14.Text = dataLoan.sFHA203kA2PlusB14_rep;
            sFHA203kD2.Text = dataLoan.sFHA203kD2_rep;
            sFHA203kD22.Text = dataLoan.sFHA203kD2_rep;
            sFHA203kD4.Text = dataLoan.sFHA203kD4_rep;
            sFHA203kMaxMAmtD5Lckd.Checked = dataLoan.sFHA203kMaxMAmtD5Lckd;
            sFHA203kMaxMAmtD5.Text = dataLoan.sFHA203kMaxMAmtD5_rep;
            sFHA203kEnergyEfficientMAmt.Text = dataLoan.sFHA203kEnergyEfficientMAmt_rep;
            sFHA203kRemarks.Text = dataLoan.sFHA203kRemarks;
            sFHA203kEscrowedFundsTot.Text = dataLoan.sFHA203kEscrowedFundsTot_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sLDiscntPc.Text = dataLoan.sLDiscntPc_rep;
            aBMidNm.Text = dataApp.aBMidNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aBSuffix.Text = dataApp.aBSuffix;
            sUnitsNum.Text = dataLoan.sUnitsNum_rep;
            aCLastNm.Text = dataApp.aCLastNm;
            sFHA203kD3LtvFactorR.Text = dataLoan.sFHA203kD3LtvFactorR_rep;

            sFHA203kUnderwriterChumsId.Text = dataLoan.sFHA203kUnderwriterChumsId;
            sFHA203kUnderwriterName.Text = dataLoan.sFHA203kUnderwriterName;
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            Tools.Bind_aOccT(aOccT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_TriState(sFHA203kSpHudOwnedTri);
            sSpZip.SmartZipcode(sSpCity, sSpState);

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            this.PageTitle = "FHA 203(k) Worksheet";
            this.PageID = "FHA_92700";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CHUD_92700PDF);
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}
}
