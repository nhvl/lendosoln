<%@ Page language="c#" Codebehind="FHA203kWorksheet.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHA203kWorksheet" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
		<title>FHA203kWorksheet</title>
  </head>
<body bgcolor="gainsboro" MS_POSITIONING="FlowLayout">
<form id=FHA203kWorksheet method=post runat="server">
<script language=javascript>
    <!--
    function updateBorrowerName() {
      var name = <%= AspxTools.JsGetElementById(aBLastNm) %>.value + ", " + <%= AspxTools.JsGetElementById(aBFirstNm)%>.value;
      parent.info.f_updateApplicantDDL(name, <%= AspxTools.JsString(ApplicationID) %>);
    }    
    function _init() {
      lockField(<%= AspxTools.JsGetElementById(sFHA203kMaxMAmtC5Lckd) %>, 'sFHA203kMaxMAmtC5');
      lockField(<%= AspxTools.JsGetElementById(sFHA203kD1Lckd) %>, 'sFHA203kD1');
      lockField(<%= AspxTools.JsGetElementById(sFHA203kMaxMAmtD5Lckd) %>, 'sFHA203kMaxMAmtD5');
      lockField(<%= AspxTools.JsGetElementById(sFHA203kFHAMipRefundLckd) %>, 'sFHA203kFHAMipRefund');
      
      var sLPurposeT = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value;
      var bRefi = sLPurposeT == '1' || sLPurposeT == '2';
      
    }
    
      function manualEdit(type) 
    {
        var elem;
        if(type=="name")
            elem = document.getElementById("sFHA203kUnderwriterNameLckd");
        else if (type == "chumsId")
            elem = document.getElementById("sFHA203kUnderwriterChumsIdLckd");

        elem.checked = true;
    } 
//-->
</script>


		<asp:CheckBox runat="server" style="display:none;" ID="sFHA203kUnderwriterChumsIdLckd" />
		<asp:CheckBox runat="server" style="display:none;" ID="sFHA203kUnderwriterNameLckd" />
<table class="FormTable" id="Table1" cellspacing="0" cellpadding="0" border="0">
  <tr>
    <td class="MainRightHeader" nowrap>FHA 203(k) Worksheet (HUD-92700)</td>
  </tr>
  <tr>
    <td nowrap>
      <table class="InsetBorder" id="Table2" cellspacing="0" cellpadding="0" width="98%" border="0">
        <tr>
          <td class="FieldLabel" nowrap>Borrower First Name</td>
          <td nowrap>
            <asp:TextBox ID="aBFirstNm" TabIndex="1" runat="server" onchange="updateBorrowerName();" /></td>
          <td class="FieldLabel" nowrap>Coborrower First Name</td>
          <td nowrap>
            <asp:TextBox ID="aCFirstNm" TabIndex="5" runat="server" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>Borrower Middle Name</td>
          <td nowrap>
            <asp:TextBox ID="aBMidNm" TabIndex="1" runat="server" /></td>
          <td class=FieldLabel noWrap>Coborrower Middle Name</td>
          <td noWrap><asp:textbox id=aCMidNm tabIndex=5 runat="server" /></td></tr>
        <TR>
          <TD class=FieldLabel noWrap>Borrower Last Name</TD>
          <TD noWrap><asp:textbox id=aBLastNm tabIndex=1 runat="server" maxlength="21" onchange="updateBorrowerName();"></asp:textbox></TD>
          <TD class=FieldLabel noWrap>Coborrower Last Name</TD>
          <TD noWrap><asp:textbox id=aCLastNm tabIndex=5 runat="server" maxlength="21"></asp:textbox></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>Borrower Suffix</td>
          <td noWrap><ml:ComboBox id=aBSuffix tabIndex=1 runat="server" maxlength="21"></ml:ComboBox></td>
          <td class=FieldLabel noWrap>Coborrower Suffix</td>
          <td noWrap><ml:ComboBox id=aCSuffix tabIndex=5 runat="server" maxlength="21"></ml:ComboBox></td></tr>
        <TR>
          <TD class=FieldLabel noWrap>FHA Case Number</TD>
          <TD noWrap><asp:textbox id=sAgencyCaseNum tabIndex=10 runat="server"></asp:textbox></TD>
          <TD class=FieldLabel noWrap colSpan=2># of 
            Units <asp:textbox id=sUnitsNum tabIndex=10 runat="server" maxlength="2" Width="52px"></asp:textbox>&nbsp;Prop 
            Owned By HUD&nbsp; <asp:dropdownlist id=sFHA203kSpHudOwnedTri tabIndex=10 runat="server"></asp:dropdownlist></TD></TR>
        <tr>
          <td class=FieldLabel noWrap>CHUMS ID</td>
          <td noWrap><asp:textbox onchange="manualEdit('chumsId');" id=sFHA203kUnderwriterChumsId tabIndex=10 runat="server"></asp:textbox></td>
          <td class=FieldLabel noWrap>Underwriter Name</td>
          <td noWrap><asp:textbox onchange="manualEdit('name');" id=sFHA203kUnderwriterName tabIndex=10 runat="server"></asp:textbox></td></tr>
        <tr>
          <TD class=FieldLabel noWrap>Property Address</TD>
          <TD noWrap colSpan=3><asp:textbox id=sSpAddr tabIndex=10 runat="server" Width="319px" MaxLength="60"></asp:textbox></TD></tr>
        <TR>
          <TD class=FieldLabel noWrap>City</TD>
          <TD noWrap colSpan=3><asp:textbox id=sSpCity tabIndex=10 runat="server" Width="218px"></asp:textbox><ml:statedropdownlist id=sSpState tabIndex=10 runat="server" IncludeTerritories="false"></ml:statedropdownlist><ml:zipcodetextbox id=sSpZip tabIndex=10 runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></TD></TR></TABLE></TD></TR>
  <tr>
    <td nowrap>
      <table id="Table3" cellspacing="0" cellpadding="5" border="0">
        <tr>
          <td valign="top" nowrap></td>
          <td valign="top" nowrap>
            <table class="InsetBorder" id="Table20" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td class="FieldLabel" nowrap>Occupancy Type</td>
              </tr>
              <tr>
                <td nowrap>
                  <asp:DropDownList ID="aOccT" TabIndex="10" runat="server" onchange="refreshCalculation();">
                  </asp:DropDownList>
                </td>
              </tr>
              <tr>
                <td nowrap>
                  <asp:CheckBox ID="sFHAIsGovAgency" TabIndex="10" runat="server" Text="Govt Agency" /></td>
              </tr>
              <tr>
                <td nowrap>
                  <asp:CheckBox ID="sFHAIsNonProfit" TabIndex="10" runat="server" Text="Nonprofit" /></td>
              </tr>
            </table>
          </td>
          <td valign="top" nowrap>
            <table class="InsetBorder" id="Table21" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td class="FieldLabel" nowrap>Loan Purpose</td>
              </tr>
              <tr>
                <td nowrap>
                  <asp:DropDownList ID="sLPurposeT" TabIndex="10" runat="server" onchange="refreshCalculation();" />
                </td>
              </tr>
            </table>
          </td>
          <td valign="top" nowrap></td>
          <td class="FieldLabel" valign="top" nowrap></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td nowrap>
      <table class="InsetBorder" id="Table5" cellspacing="0" cellpadding="0" width="98%" border="0">
        <tr>
          <td class="LoanFormHeader" nowrap colspan="4">A. Property Information</td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>1. Contract Sales Price</td>
          <td nowrap>
            <ml:MoneyTextBox ID="sFHAExistingMLien" TabIndex="15" runat="server" Width="90" preset="money" onchange="refreshCalculation();" /></td>
          <td nowrap></td>
          <td nowrap></td>
        </tr>
        <tr>
          <td nowrap></td>
          <td nowrap>
            <asp:CheckBox ID="sFHAIsExistingDebt" TabIndex="15" runat="server" Text="Or Existing Debt" /></td>
          <td class="FieldLabel" nowrap>4. 110% After-Improved Value</td>
          <td nowrap>
            <ml:MoneyTextBox ID="sFHASpAfterImprovedVal110pc" TabIndex="20" runat="server" Width="90" preset="money" ReadOnly="True" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>2. "As-Is" Value</td>
          <td nowrap>
            <ml:MoneyTextBox ID="sFHASpAsIsVal" TabIndex="15" runat="server" Width="90" preset="money" onchange="refreshCalculation();" /></td>
          <td class="FieldLabel" nowrap>5. Borrower Paid Closing Costs</td>
          <td nowrap>
            <ml:MoneyTextBox ID="sFHA203kBorPdCc" TabIndex="20" runat="server" Width="90" preset="money" onchange="refreshCalculation();" /></td>
        </tr>
        <tr>
          <td class="FieldLabel" nowrap>3. After-Improved Value</td>
          <td nowrap>
            <ml:MoneyTextBox ID="sFHASpAfterImprovedVal" TabIndex="15" runat="server" Width="90" preset="money" onchange="refreshCalculation();" /></td>
          <td class="FieldLabel" nowrap>6. Allowable Energy Improvements</td>
          <td nowrap>
            <ml:MoneyTextBox ID="sFHA203kAllowEnergyImprovement" TabIndex="20" runat="server" Width="90" preset="money" onchange="refreshCalculation();" /></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td nowrap>
      <table class="InsetBorder" id="Table6" cellspacing="0" cellpadding="0" width="98%" border="0">
        <tr>
          <td nowrap>
            <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
              <tr>
                <td class="LoanFormHeader" nowrap>B. Rehabilitation and Other Allowable Cost</td>
                <td class="LoanFormHeader" nowrap></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>1. Total Cost of Repairs includes the improvement in A6</td>
                <td nowrap width="100%">
                  <ml:MoneyTextBox ID="sFHA203kRepairCost" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>2. Contingency Reserved on Repair Costs&nbsp;&nbsp;&nbsp;&nbsp;
                  <ml:PercentTextBox ID="sFHA203kRepairCostReserveR" TabIndex="25" runat="server" Width="70" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox>&nbsp;(10 - 20% of B1)</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kRepairCostReserve" TabIndex="25" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>3. Inspection Fees&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <asp:TextBox ID="sFHA203kInspectCount" TabIndex="25" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>&nbsp;x
                  <ml:MoneyTextBox ID="sFHA203kCostPerInspect" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>&nbsp;per inspection</td>
                <td nowrap></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>&nbsp;&nbsp;&nbsp; + Title Update Fee&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <asp:TextBox ID="sFHA203kTitleDrawCount" TabIndex="25" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>&nbsp;x
                  <ml:MoneyTextBox ID="sFHA203kCostPerTitleDraw" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>&nbsp;per draw =</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kInspectAndTitleFee" TabIndex="25" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>4. Mortgage Payments Escrowed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <asp:TextBox ID="sFHA203kPmtEscrowedMonths" TabIndex="25" runat="server" Width="40px" onchange="refreshCalculation();"></asp:TextBox>&nbsp;x
                  <ml:MoneyTextBox ID="sFHA203kMonhtlyPmtEscrowed" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>&nbsp;per month</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kPmtEscrowed" TabIndex="25" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>5. Sub-total for Rehabilitation Escrow Account (Total of B1 thru B4)</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kRehabEscrowAccountSubtot" TabIndex="25" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>6. Architectural and Engineering Fees</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kEngineeringFee" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>7. Independent Consultant Fees</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kConsultantFee" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>8. Permits and Other Fees (Explained in remarks)</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kOtherFee" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>9. Plan Reviewer Fees&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <asp:TextBox ID="sFHA203kPlanReviewerMiles" TabIndex="25" runat="server" Width="65px" onchange="refreshCalculation();"></asp:TextBox>&nbsp;miles @
                  <ml:MoneyTextBox ID="sFHA203kPlanReviewerCostPerMile" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:MoneyTextBox>&nbsp;per mile</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kPlanReviewerFee" TabIndex="25" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>10. Sub-total&nbsp; (Total of B5 thru B9) </td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kB5toB9Subtot" TabIndex="25" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>11. Supplemental Origination Fee (greater of $350 or 1.5% of B10)</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kSupplementOrigFee" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>12. Discount Points on Repair Costs and Fees&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <ml:PercentTextBox ID="sFHA203kRepairDiscountFeePt" TabIndex="25" runat="server" Width="70" preset="percent" onchange="refreshCalculation();"></ml:PercentTextBox>&nbsp;of B10</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kRepairDiscount" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>13. Sub-total for Release at Closing (Total of B6 thru B9 + B11 and B12)</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kReleaseAtClosingSubtot" runat="server" Width="90" preset="money" ReadOnly="True" /></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>14. Total Rehabilitation Cost (Total of B5 and B13 minus A6)</td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kRehabCostTot" runat="server" Width="90" preset="money" ReadOnly="True" /></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td nowrap>
      <table class=InsetBorder id=Table8 cellspacing=0 cellpadding=0 width="98%" border=0>
        <tr>
          <td class=LoanFormHeader nowrap>C. Mortgage Calculation for Purchase Transactions</td>
          <td class=LoanFormHeader nowrap></td></tr>
        <tr>
          <td class=FieldLabel nowrap>1. Lesser of Sales Price (A1) or As-Is Value (A2)</td>
          <td nowrap><ml:moneytextbox id=sFHA203kSalesPriceOrAsIsVal runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>2. Total Rehabilitation Cost (B14) </td>
          <td nowrap><ml:moneytextbox id=sFHA203kRehabCostTot2 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel nowrap>3. Lesser of Sum of C1 + C2 <ml:moneytextbox id=sFHA203kC1PlusC2 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox>or 
            110% of After-Improved Value (A4)</td>
          <td nowrap><ml:moneytextbox id=sFHA203kC1PlusC2OrA4 runat="server" width="90" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
        <tr>
          <td class=FieldLabel nowrap colspan=2>4. Mortgage Amt: Sum of C3 
            + (-) Required Adjustment
            <ml:moneytextbox ID="sFHA203kRequiredAdjustment" runat="server" Width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox>
            * LTV Factor (96.5%)(Own)</td></tr>
        <tr>
          <td class=FieldLabel nowrap>&nbsp;&nbsp;or Less Allowable Downpayment / 
            HUD-Owned&nbsp; Prop <ml:moneytextbox id=sFHA203kC5Part2 tabIndex=25 runat="server" width="90" preset="money" onchange="refreshCalculation();" readonly="True"></ml:moneytextbox>&nbsp;&nbsp;&nbsp; 
<asp:checkbox id=sFHA203kMaxMAmtC5Lckd onclick="lockField(this, 'sFHA203kMaxMAmtC5'); refreshCalculation();" tabIndex=25 runat="server" Text="Lock"></asp:checkbox></td>
          <td nowrap><ml:moneytextbox id=sFHA203kMaxMAmtC5 tabIndex=25 runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td></tr>
        </table></td></tr>
  <TR>
    <TD noWrap>
      <table id="Table10" cellspacing="0" cellpadding="0" width="98%" border="0">
        <tr>
          <td nowrap>
            <table class="InsetBorder" id="Table11" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td class="LoanFormHeader" nowrap>D. Mortgage Calculation for Refinance Transactions</td>
                <td class="LoanFormHeader" nowrap></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>1. Sum of Existing Debt (A1) + Rehabilitation Cost(B14) + Borrower Paid CC (A5)</td>
                <td nowrap></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>+ Prepaids + Discount on Total Loan Amt - Discount on Repair Cost (B12)</td>
                <td nowrap></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>minus <A href="javascript:linkMe('FHACreditAnalysisRefinance.aspx');" >FHA MIP Refund</A>
                    <asp:CheckBox ID="sFHA203kFHAMipRefundLckd" onclick="refreshCalculation();" TabIndex="25" runat="server" Text="Lock"></asp:CheckBox>
                    <ml:MoneyTextBox ID="sFHA203kFHAMipRefund" runat="server" Width="90" preset="money" ReadOnly="true" onchange="refreshCalculation();"></ml:MoneyTextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="sFHA203kD1Lckd" onclick="lockField(this, 'sFHA203kD1');" TabIndex="25" runat="server" Text="Lock"></asp:CheckBox>
                </td>
                <td nowrap width="100%">
                  <ml:MoneyTextBox ID="sFHA203kD1" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>2. Lesser of Sum of A2 + B14
                  <ml:MoneyTextBox ID="sFHA203kA2PlusB14" TabIndex="25" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>&nbsp;or A4 </td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kD2" TabIndex="25" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>3. D2&nbsp;&nbsp;&nbsp;
                  <ml:MoneyTextBox ID="sFHA203kD22" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox>&nbsp;x LTV Factor <ml:PercentTextBox ID="sFHA203kD3LtvFactorR" runat="server" preset="percent" onchange="refreshCalculation();" TabIndex="25" /> (Owner-Occupant) </td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kD4" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>4. Maximum Mortgage Amount: Lesser of D1 or D3 (NEW NEW)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <asp:CheckBox ID="sFHA203kMaxMAmtD5Lckd" onclick="lockField(this, 'sFHA203kMaxMAmtD5');" TabIndex="25" runat="server" Text="Lock"></asp:CheckBox></td>
                <td nowrap>
                  <ml:MoneyTextBox ID="sFHA203kMaxMAmtD5" TabIndex="25" runat="server" Width="90" preset="money" onchange="refreshCalculation();" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td nowrap>
      <table class="InsetBorder" id="Table14" cellspacing="0" cellpadding="0" width="98%" border="0">
        <tr>
          <td nowrap>
            <table id="Table15" cellspacing="0" cellpadding="0" width="100%" border="0">
              <tr>
                <td class="LoanFormHeader" nowrap>E. Calculation for Energy Efficient Mortgage</td>
                <td class="LoanFormHeader" nowrap></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>1. Energy Efficient Mortgage Amount (C4 or C7, or D4) + A6</td>
                <td nowrap width="100%">
                  <ml:MoneyTextBox ID="sFHA203kEnergyEfficientMAmt" runat="server" Width="90" preset="money" /></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td nowrap>
      <table class=InsetBorder id=Table16 cellspacing=0 cellpadding=0 
      width="98%" border=0>
        <tr>
          <td nowrap>
            <table id=Table17 cellspacing=0 cellpadding=0 width="100%" 
              border=0>
              <tr>
                <td class=LoanFormHeader nowrap>Remarks</td></tr>
              <tr>
                <td nowrap><asp:textbox id=sFHA203kRemarks tabIndex=25 runat="server" Width="561px" TextMode="MultiLine" Height="97px"></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>Total Escrowed Funds <ml:moneytextbox id=sFHA203kEscrowedFundsTot tabIndex=25 runat="server" width="90" preset="money"></ml:moneytextbox>&nbsp;Interest 
                  Rate <ml:percenttextbox id=sNoteIR tabIndex=25 runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox>&nbsp;Discount 
                  Points <ml:percenttextbox id=sLDiscntPc tabIndex=25 runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td></tr></table></td></tr></table></td></tr>
</table>
  </form>
</body>
</html>
