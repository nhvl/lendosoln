using System;
using System.Collections;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Migration;

namespace LendersOfficeApp.newlos.FHA
{
	public partial class FHAAddendum : BaseLoanPage
	{
        #region Protected member variables
		protected System.Web.UI.WebControls.CheckBox sHas1stTimeBuyer;
        #endregion

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            EnableJqueryMigrate = false;
            aBZip.SmartZipcode(aBCity, aBState);
            sSpZip.SmartZipcode(sSpCity, sSpState);
            aFHABorrCertOtherPropZip.SmartZipcode(aFHABorrCertOtherPropCity, aFHABorrCertOtherPropState);
            FHAAddendumLenderZip.SmartZipcode(FHAAddendumLenderCity, FHAAddendumLenderState);
            FHAAddendumSponsorZip.SmartZipcode(FHAAddendumSponsorCity, FHAAddendumSponsorState);
            FHAAddendumDulyAgentZip.SmartZipcode(FHAAddendumDulyAgentCity, FHAAddendumDulyAgentState);

            Tools.Bind_sLT(sLT);
            Tools.Bind_aVaVestTitleT(aVaVestTitleT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sFHAConstructionT(sFHAConstructionT);
            Tools.Bind_sGseSpT(sGseSpT);

            Tools.Bind_TriState(aFHABorrCertOwnOrSoldOtherFHAPropTri);
            Tools.Bind_TriState(aFHABorrCertOtherPropToBeSoldTri);
            Tools.Bind_TriState(aFHABorrCertOtherPropCoveredByThisLoanTri);
            Tools.Bind_TriState(aFHABorrCertOwnMoreThan4DwellingsTri);
            Tools.Bind_TriState(aVaBorrCertHadVaLoanTri);
            Tools.Bind_TriState(aHas1stTimeBuyerTri);

            Tools.Bind_sFHAHousingActSection(sFHAHousingActSection);

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            aVaBorrCertHadVaLoanTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            aFHABorrCertReceivedLeadPaintPoisonInfoTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            aFHABorrCertOwnOrSoldOtherFHAPropTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            aFHABorrCertOtherPropToBeSoldTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            aFHABorrCertOtherPropCoveredByThisLoanTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            aFHABorrCertOwnMoreThan4DwellingsTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            aHas1stTimeBuyerTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);
            sFHAMortgageHaveFinanceInterestTri.Load += new EventHandler(this.ZeroOneCheckBoxList_Load);


            this.PageTitle = "FHA Addendum";
            this.PageID = "FHAAddendum";
                this.PDFPrintClass = typeof(LendersOffice.Pdf.CHUD_92900_APDF);
            if (!string.IsNullOrEmpty(RequestHelper.GetSafeQueryString("pageid")))
                this.PageID = RequestHelper.GetSafeQueryString("pageid");


        }

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAAddendum));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);

            FHAAddendumForm.Attributes.Add("class", dataLoan.sIsRequire2016FhaAddendum ? "Addendum2016" : "Addendum2010");
            sFHACondOwnerOccNotRequired2.InputAttributes.Add("data-field-id", "sFHACondOwnerOccNotRequired");

            if (dataLoan.sIsRequire2016FhaAddendum)
            {
                sFhaDirectEndorsementAllConditionsOfApprovalMet.Checked = dataLoan.sFhaDirectEndorsementAllConditionsOfApprovalMet;
                sFhaDirectEndorsementWasRatedAcceptAndUnderwritten.Checked = dataLoan.sFhaDirectEndorsementWasRatedAcceptAndUnderwritten;
                aFhaBorrCertOtherPropToBeSoldNA.Checked = dataApp.aFhaBorrCertOtherProptoBeSoldNA;
                aFhaBorrCertWillOccupyFor1YearTri_No.Checked = dataApp.aFhaBorrCertWillOccupyFor1YearTri == E_TriState.No;
                aFhaBorrCertWillOccupyFor1YearTri_Yes.Checked = dataApp.aFhaBorrCertWillOccupyFor1YearTri == E_TriState.Yes;
                aFHABorrCertOccIsChildAsHome.Checked = dataApp.aFHABorrCertOccIsChildAsHome;
                aFHABorrCertOccIsChildAsHomePrev.Checked = dataApp.aFHABorrCertOccIsChildAsHomePrev;

                sFHACondOwnerOccNotRequired2.Checked = dataLoan.sFHACondOwnerOccNotRequired;
            }
            else
            {
                sFHACondIfProposedConstBuilderCertified.Checked = dataLoan.sFHACondIfProposedConstBuilderCertified;
                sFHACondIfNewConstLenderCertified.Checked = dataLoan.sFHACondIfNewConstLenderCertified;
                sFHACondBuilderWarrantyRequired.Checked = dataLoan.sFHACondBuilderWarrantyRequired;
                sFHACond10YrsWarranty.Checked = dataLoan.sFHACond10YrsWarranty;
                sFHACondMortgageHighLtvForNonOccupant.Checked = dataLoan.sFHACondMortgageHighLtvForNonOccupant;
                sFHACondOther.Checked = dataLoan.sFHACondOther;
                sFHACondOtherDesc.Text = dataLoan.sFHACondOtherDesc;
                sFHACondOwnerOccNotRequired.Checked = dataLoan.sFHACondOwnerOccNotRequired;
            }

            Tools.Set_TriState(aFHABorrCertReceivedLeadPaintPoisonInfoTri, dataApp.aFHABorrCertReceivedLeadPaintPoisonInfoTri);
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            Tools.Set_TriState(aFHABorrCertOwnOrSoldOtherFHAPropTri, dataApp.aFHABorrCertOwnOrSoldOtherFHAPropTri);
            Tools.Set_TriState(aFHABorrCertOtherPropToBeSoldTri, dataApp.aFHABorrCertOtherPropToBeSoldTri);
            Tools.Set_TriState(aFHABorrCertOtherPropCoveredByThisLoanTri, dataApp.aFHABorrCertOtherPropCoveredByThisLoanTri);
            Tools.Set_TriState(aFHABorrCertOwnMoreThan4DwellingsTri, dataApp.aFHABorrCertOwnMoreThan4DwellingsTri);
            Tools.Set_TriState(aHas1stTimeBuyerTri, dataApp.aHas1stTimeBuyerTri);
            Tools.Set_TriState(aVaBorrCertHadVaLoanTri, dataApp.aVaBorrCertHadVaLoanTri);

            Tools.SetDropDownListValue(aVaVestTitleT, dataApp.aVaVestTitleT);

            aBLastNm.Text                                                = dataApp.aBLastNm;
            aBMidNm.Text                                                 = dataApp.aBMidNm;
            aBFirstNm.Text                                               = dataApp.aBFirstNm;
            aBSuffix.Text                                                = dataApp.aBSuffix;
            aCLastNm.Text                                                = dataApp.aCLastNm;
            aCMidNm.Text                                                 = dataApp.aCMidNm;
            aCFirstNm.Text                                               = dataApp.aCFirstNm;
            aCSuffix.Text                                                = dataApp.aCSuffix;
            aBAddr.Text                                                  = dataApp.aBAddr;
            aBCity.Text                                                  = dataApp.aBCity;
            aBState.Value                                                = dataApp.aBState;
            aBZip.Text                                                   = dataApp.aBZip;
            sSpAddr.Text                                                 = dataLoan.sSpAddr;
            sSpCity.Text                                                 = dataLoan.sSpCity;
            sSpState.Value                                               = dataLoan.sSpState;
            sSpZip.Text                                                  = dataLoan.sSpZip;
            sSpLegalDesc.Text                                            = dataLoan.sSpLegalDesc ;
            sNoteIR.Text                                                 = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sAgencyCaseNum.Text                                          = dataLoan.sAgencyCaseNum;
            sCaseAssignmentD.Text                                        = dataLoan.sCaseAssignmentD_rep;
            sLenderCaseNum.Text                                          = dataLoan.sLenderCaseNum;
            sFHAHousingActSection.Text                                   = dataLoan.sFHAHousingActSection;
            sFinalLAmt.Text                                              = dataLoan.sFinalLAmt_rep;
            sTermInYrRemainingMonths.Text                                = dataLoan.sTermInYrRemainingMonths_rep;
            sTermInYr.Text                                               = dataLoan.sTermInYr_rep;
            sFHALDiscnt.Text                                             = dataLoan.sFHALDiscnt_rep;
            sFHALDiscntLckd.Checked                                      = dataLoan.sFHALDiscntLckd;
            sFfUfmip1003.Text                                            = dataLoan.sFfUfmip1003_rep;
            sFHAProMIns.Text                                             = dataLoan.sFHAProMIns_rep;
            sFHAProMInsLckd.Checked                                      = dataLoan.sFHAProMInsLckd;
            sFHAProMInsMon.Text                                          = dataLoan.sFHAProMInsMon_rep;
            sFHAProMInsMonLckd.Checked                                   = dataLoan.sFHAProMInsMonLckd;
            
            aFHABorrCertOtherPropStAddr.Text                             = dataApp.aFHABorrCertOtherPropStAddr;
            aFHABorrCertOtherPropCity.Text                               = dataApp.aFHABorrCertOtherPropCity;
            aFHABorrCertOtherPropState.Value                             = dataApp.aFHABorrCertOtherPropState;
            aFHABorrCertOtherPropZip.Text                                = dataApp.aFHABorrCertOtherPropZip;
            aFHABorrCertOtherPropSalesPrice.Text                         = dataApp.aFHABorrCertOtherPropSalesPrice_rep;
            aFHABorrCertOtherPropOrigMAmt.Text                           = dataApp.aFHABorrCertOtherPropOrigMAmt_rep;
            aFHABorrCertInformedPropVal.Text                             = dataApp.aFHABorrCertInformedPropVal_rep;
            aFHABorrCertInformedPropValDeterminedByVA.Checked            = dataApp.aFHABorrCertInformedPropValDeterminedByVA;
            aFHABorrCertInformedPropValDeterminedByHUD.Checked           = dataApp.aFHABorrCertInformedPropValDeterminedByHUD;
            aFHABorrCertInformedPropValAwareAtContractSigning.Checked    = dataApp.aFHABorrCertInformedPropValAwareAtContractSigning;
            aFHABorrCertInformedPropValNotAwareAtContractSigning.Checked = dataApp.aFHABorrCertInformedPropValNotAwareAtContractSigning;
            sFHALenderIdCode.Text                                        = dataLoan.sFHALenderIdCode;
            sVALenderIdCode.Text                                         = dataLoan.sVALenderIdCode;
            sFHASponsorAgentIdCode.Text                                  = dataLoan.sFHASponsorAgentIdCode;
            sVASponsorAgentIdCode.Text                                   = dataLoan.sVASponsorAgentIdCode;

            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sFHAConstructionT, dataLoan.sFHAConstructionT);
            Tools.SetDropDownListValue(sGseSpT, dataLoan.sGseSpT);
            sFHAPurposeIsPurchaseExistHome.Checked                       = dataLoan.sFHAPurposeIsPurchaseExistHome;
            sFHAPurposeIsConstructHome.Checked                           = dataLoan.sFHAPurposeIsConstructHome;
            sFHAPurposeIsFinanceImprovement.Checked                      = dataLoan.sFHAPurposeIsFinanceImprovement;
            sFHAPurposeIsFinanceCoopPurchase.Checked                     = dataLoan.sFHAPurposeIsFinanceCoopPurchase;
            sFHAPurposeIsPurchaseManufacturedHome.Checked                = dataLoan.sFHAPurposeIsPurchaseManufacturedHome;
            sFHAPurposeIsPurchaseNewCondo.Checked                        = dataLoan.sFHAPurposeIsPurchaseNewCondo;
            sFHAPurposeIsManufacturedHomeAndLot.Checked                  = dataLoan.sFHAPurposeIsManufacturedHomeAndLot;
            sFHAPurposeIsPurchaseExistCondo.Checked                      = dataLoan.sFHAPurposeIsPurchaseExistCondo;
            sFHAPurposeIsRefiManufacturedHomeOrLotLoan.Checked           = dataLoan.sFHAPurposeIsRefiManufacturedHomeOrLotLoan;
            sFHAPurposeIsPurchaseNewHome.Checked                         = dataLoan.sFHAPurposeIsPurchaseNewHome;
            sFHAPurposeIsRefiManufacturedHomeToBuyLot.Checked            = dataLoan.sFHAPurposeIsRefiManufacturedHomeToBuyLot;
            sFHAPurposeIsRefinance.Checked                               = dataLoan.sFHAPurposeIsRefinance;

            aVaVestTitleODesc.Text                                       = dataApp.aVaVestTitleODesc;

            if (dataLoan.sLT == E_sLT.VA) //OPM 147768 - form specifies "for VA only"
            {
                aFHABorrCertOccIsAsHome.Checked = dataApp.aFHABorrCertOccIsAsHome;
                aFHABorrCertOccIsAsHomeForActiveSpouse.Checked = dataApp.aFHABorrCertOccIsAsHomeForActiveSpouse;
                aFHABorrCertOccIsAsHomePrev.Checked = dataApp.aFHABorrCertOccIsAsHomePrev;
                aFHABorrCertOccIsAsHomePrevForActiveSpouse.Checked = dataApp.aFHABorrCertOccIsAsHomePrevForActiveSpouse;
                sFHALenderIdCode.Attributes.Add("style", "display:none");
                sFHASponsorAgentIdCode.Attributes.Add("style", "display:none");
            }
            else
            {
                sVALenderIdCode.Attributes.Add("style", "display:none");
                sVASponsorAgentIdCode.Attributes.Add("style", "display:none");
            }

            sFHAApprovedSubj.Checked = dataLoan.sFHAApprovedSubj;
            sFHAApprovedD.Text = dataLoan.sFHAApprovedD_rep;
            sFHAApprovedExpiresD.Text = dataLoan.sFHAApprovedExpiresD_rep;
            sFHAApprovedModified.Checked = dataLoan.sFHAApprovedModified;
            sFHAModifiedFinalLAmt.Text = dataLoan.sFHAModifiedFinalLAmt_rep;
            sFHAModifiedNoteIR.Text = dataLoan.sFHAModifiedNoteIR_rep;
            sFHAModifiedTermInYr.Text = dataLoan.sFHAModifiedTermInYr_rep;
            sFHAModifiedTermInMonths.Text = dataLoan.sFHAModifiedTermInMonths_rep;
            sFHAModifiedFfUfmip1003.Text = dataLoan.sFHAModifiedFfUfmip1003_rep;
            sFHAModifiedProMInsMon.Text = dataLoan.sFHAModifiedProMInsMon_rep;
            sFHAModifiedProMIns.Text = dataLoan.sFHAModifiedProMIns_rep;
            sFHAModifiedMonthlyPmt.Text = dataLoan.sFHAModifiedMonthlyPmt_rep;
            
            sFHARatedAcceptedByTotalScorecard.Checked = dataLoan.sFHARatedAcceptedByTotalScorecard;
            sFHARatedReferByTotalScorecard.Checked = dataLoan.sFHARatedReferByTotalScorecard;
            Tools.Set_TriState(sFHAMortgageHaveFinanceInterestTri, dataLoan.sFHAMortgageHaveFinanceInterestTri);
            sLenderCaseNumLckd.Checked = dataLoan.sLenderCaseNumLckd;

            sFhaLoanOrigCompanyNm.Text = dataLoan.sFhaLoanOrigCompanyNm;
            sFhaLoanOrigCompanyNmLckd.Checked = dataLoan.sFhaLoanOrigCompanyNmLckd;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V18_ConsolidateTaxIdFields))
            {
                // This will hook the field into mask.js for migrated files.
                sFHASponsoredOriginatorEIN.Attributes.Add("preset", "employerIdentificationNumber");
            }

            sFHASponsoredOriginatorEIN.Text = dataLoan.sFHASponsoredOriginatorEIN;
            sFhaSponsoredOriginatorEinLckd.Checked = dataLoan.sFhaSponsoredOriginatorEinLckd;
            sFhaLoanOrigCompanyNmlsId.Text = dataLoan.sFhaLoanOrigCompanyNmlsId;
            sFhaLoanOrigCompanyNmlsIdLckd.Checked = dataLoan.sFhaLoanOrigCompanyNmlsIdLckd;

            IPreparerFields field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHAAddendumLenderCompanyName.Text = field.CompanyName;
            FHAAddendumLenderStreetAddr.Text = field.StreetAddr;
            FHAAddendumLenderCity.Text = field.City;
            FHAAddendumLenderState.Value = field.State;
            FHAAddendumLenderZip.Text = field.Zip;
            FHAAddendumLenderPhoneOfCompany.Text = field.PhoneOfCompany;
            FHAAddendumLenderPreparerName.Text = field.PreparerName;
            //FHAAddendumLenderPhone.Text = field.Phone;
            FHAAddendumLenderTitle.Text = field.Title;

            field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumSponsor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHAAddendumSponsorCompanyName.Text = field.CompanyName;
            FHAAddendumSponsorStreetAddr.Text = field.StreetAddr;
            FHAAddendumSponsorCity.Text = field.City;
            FHAAddendumSponsorState.Value = field.State;
            FHAAddendumSponsorZip.Text = field.Zip;

            field = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumDulyAgent, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHAAddendumDulyAgentPreparerName.Text = field.PreparerName;
            FHAAddendumDulyAgentStreetAddr.Text = field.StreetAddr;
            FHAAddendumDulyAgentCity.Text = field.City;
            FHAAddendumDulyAgentState.Value = field.State;
            FHAAddendumDulyAgentZip.Text = field.Zip;
            FHAAddendumDulyAgentTitle.Text = field.Title;

            sFHAAddendumUnderwriterName.Text = dataLoan.sFHAAddendumUnderwriterName;
            sFHAAddendumUnderwriterChumsId.Text = dataLoan.sFHAAddendumUnderwriterChumsId;
            if (dataLoan.sIsRequire2016FhaAddendum)
            {
                sFHAAddendumUnderwriterChumsId2.Text = dataLoan.sFHAAddendumUnderwriterChumsId;
            }

            sFHAAddendumMortgageeName.Text = dataLoan.sFHAAddendumMortgageeName;

            if (dataLoan.sIsRequire2016FhaAddendum)
            {
                sFHAAddendumMortgageeTitle.Text = dataLoan.sFHAAddendumMortgageeTitle;
            }
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion


        /// <summary>
        /// Is there to make this method global? So I would not need to copy-n-paste this methods on every page use Z
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZeroOneCheckBoxList_Load(object sender, System.EventArgs e) 
        {
            CheckBoxList cbl = (CheckBoxList) sender;
            ArrayList list = new ArrayList();

            foreach (ListItem o in cbl.Items) 
            {
                list.Add("'" + o.Value + "'");
            }

            ClientScript.RegisterArrayDeclaration(cbl.UniqueID + "_items", string.Join(",", (string[])list.ToArray(typeof(string))));        
            ClientScript.RegisterHiddenField("cbl_" + cbl.ClientID, "");
            AddInitScriptFunctionWithArgs("_registerCBL", string.Format("'{0}', {1}", cbl.ClientID, cbl.Items.Count));
        }
	}
}
