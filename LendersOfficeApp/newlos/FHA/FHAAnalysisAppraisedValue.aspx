<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="FHAAnalysisAppraisedValue.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHAAnalysisAppraisedValue" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>FHAAnalysisAppraisedValue</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
<script>
    function manualEdit(type) {
        var elem;
        if (type == "name")
            elem = document.getElementById("sFHAApprUnderwriterNameLckd");
        else if (type == "chumsId")
            elem = document.getElementById("sFHAApprUnderwriterChumsIdLckd");

        elem.checked = true;
    }
</script>
  </head>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<form id=FHAAnalysisAppraisedValue method=post 
runat="server">
<asp:CheckBox runat="server" style="display:none;" ID="sFHAApprUnderwriterChumsIdLckd" />
<asp:CheckBox runat="server" style="display:none;" ID="sFHAApprUnderwriterNameLckd" />
<TABLE class=FormTable id=Table1 cellSpacing=0 cellPadding=0 width="100%" 
border=0>
  <TR>
    <TD class=MainRightHeader noWrap>FHA DE Analysis of 
      Appraisal (HUD-54114)</TD></TR>
  <tr>
    <td class=FieldLabel noWrap>Appraiser Name <asp:textbox id=AnalysisAppraisalAppraiserPreparerName runat="server" Width="234px"></asp:textbox></td></tr>
  <TR>
    <TD class=FieldLabel noWrap>1. Does the report present 
      a consistent and fair analysis of the property? <asp:CheckBoxList id=sFHAApprIsFairTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>If Not, Explain.</TD></TR>
  <TR>
    <TD noWrap><asp:textbox id=sFHAApprNotFairExplanation runat="server" Height="68px" Width="602px" TextMode="MultiLine"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>2. Comment on the 
      report's quality, completeness, consistency, and accuracy.</TD></TR>
  <TR>
    <TD noWrap><asp:textbox id=sFHAApprQualityComment runat="server" Height="68px" Width="602px" TextMode="MultiLine"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>3. Are the comparables 
      acceptable? <asp:CheckBoxList id=sFHAApprAreComparablesAcceptableTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>Comment</TD></TR>
  <TR>
    <TD noWrap><asp:textbox id=sFHAApprComparablesComment runat="server" Height="68px" Width="602px" TextMode="MultiLine"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>4. Are the adjustments 
      acceptable? <asp:CheckBoxList id=sFHAApprAdjAcceptableTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>If Not, Explain </TD></TR>
  <TR>
    <TD noWrap><asp:textbox id=sFHAApprAdjNotAcceptableExplanation runat="server" Height="68px" Width="602px" TextMode="MultiLine"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>5. Is the value acceptable 
      for HUD/VA loan purposes?&nbsp;&nbsp;&nbsp;<asp:CheckBoxList id=sFHAApprIsValAcceptableTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
      If 
      not, corrected? <asp:CheckBoxList id=sFHAApprValNeedCorrectionTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:CheckBoxList></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>Justification for 
      correction&nbsp;&nbsp;&nbsp;&nbsp; Value for HUD/FHA loan purposes <ml:moneytextbox id=sFHAApprCorrectedVal runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
  <TR>
    <TD noWrap><asp:textbox id=sFHAApprValCorrectionJustification runat="server" Height="68px" Width="602px" TextMode="MultiLine"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>6. Repair 
conditions</TD></TR>
  <TR>
    <TD noWrap><asp:textbox id=sFHAApprRepairConditions runat="server" Height="68px" Width="602px" TextMode="MultiLine"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap>7. Other comments</TD></TR>
  <TR>
    <TD noWrap><asp:textbox id=sFHAApprOtherComments runat="server" Height="68px" Width="602px" TextMode="MultiLine"></asp:textbox></TD></TR>
  <tr>
    <td class=FieldLabel noWrap>DE Underwriter Name <asp:textbox id="sFHAApprUnderwriterName"  onchange="manualEdit('name');"  runat="server" Width="283px"></asp:textbox></td></tr>
  <tr>
    <td class=FieldLabel noWrap>CHUMS Number <asp:textbox id="sFHAApprUnderwriterChumsId" onchange="manualEdit('chumsId');" runat="server" Width="283px"></asp:textbox></td></tr></TABLE></form>
	</body>
</html>
