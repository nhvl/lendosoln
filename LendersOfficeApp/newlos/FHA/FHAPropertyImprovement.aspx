<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page Language="c#" CodeBehind="FHAPropertyImprovement.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHAPropertyImprovement" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>FHAPropertyImprovement</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href=<%=AspxTools.SafeUrl(StyleSheet)%> type="text/css" rel="stylesheet">
</head>
<body class="RightBackground" ms_positioning="FlowLayout">

    <script language="javascript">
  <!--
  function _init() {
    enableLeaseInfo(document.getElementById("<%= AspxTools.ClientId(sFHAPropImprovIsPropLeasedFromSomeone) %>"));
    onchange_TriState();
  }

  function enableLeaseInfo(cb) {
    <%= AspxTools.JsGetElementById(sFHAPropImprovLeaseOwnerInfo) %>.readOnly = !cb.checked;
    <%= AspxTools.JsGetElementById(sFHAPropImprovLeaseOwnerInfo) %>.style.backgroundColor = !cb.checked ? gReadonlyBackgroundColor : "";
    <%= AspxTools.JsGetElementById(sFHAPropImprovLeaseMonPmt) %>.readOnly = !cb.checked;
    <%= AspxTools.JsGetElementById(sFHAPropImprovLeaseExpireD) %>.readOnly = !cb.checked;
  
  }
  function enableTextBox(tb, obj) {
    tb.readOnly = !obj.checked;
  }
  function onchange_TriState() {
    enableTextBox(document.getElementById("<%= AspxTools.ClientId(sFHAPropImprovHasFHAPendingAppWithWhom) %>"), document.getElementById("<%= AspxTools.ClientId(sFHAPropImprovHasFHAPendingAppTri) %>_0"));
    enableTextBox(document.getElementById("<%= AspxTools.ClientId(sFHAPropImprovRefinTitle1LoanNum) %>"), document.getElementById("<%= AspxTools.ClientId(sFHAPropImprovRefinTitle1LoanTri) %>_0"));
    enableTextBox(document.getElementById("<%= AspxTools.ClientId(sFHAPropImprovRefinTitle1LoanBal) %>"), document.getElementById("<%= AspxTools.ClientId(sFHAPropImprovRefinTitle1LoanTri) %>_0"));  
  }
  function _register_onchange(name, c) {
    for (var i = 0; i < c; i++) {
      var o = document.getElementById(name + "_" + i);
      
            addEventHandler(o, "click", onchange_TriState, false);
    }
  }  
  //-->
    </script>

    <form id="FHAPropertyImprovement" method="post" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td class="MainRightHeader" nowrap>
                Credit Application for Property Improvement Loan
            </td>
        </tr>
        <tr>
            <td nowrap>
                Have any past due obligations owed to or insured by any agency of the Federal Government?<asp:CheckBoxList ID="sFHAPropImprovHasFedPastDueTri" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td nowrap>
                Have other application for an FHA Title I loan pending?<asp:CheckBoxList ID="sFHAPropImprovHasFHAPendingAppTri" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                </asp:CheckBoxList>
                &nbsp; With whom <asp:TextBox ID="sFHAPropImprovHasFHAPendingAppWithWhom" runat="server" Width="134px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td nowrap>
                Refinance a Title I Loan&nbsp;<asp:CheckBoxList ID="sFHAPropImprovRefinTitle1LoanTri" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                </asp:CheckBoxList>
                &nbsp;&nbsp;&nbsp; Loan Number
                <asp:TextBox ID="sFHAPropImprovRefinTitle1LoanNum" runat="server" Width="121px"></asp:TextBox>
                &nbsp;Balance
                <ml:MoneyTextBox ID="sFHAPropImprovRefinTitle1LoanBal" runat="server" preset="money" Width="80px"></ml:MoneyTextBox>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table2" cellspacing="0" cellpadding="0" width="98%" border="0" class="InsetBorder">
                    <tr>
                        <td nowrap class="LoanFormHeader">
                            Applicant
                        </td>
                        <td nowrap class="LoanFormHeader">
                        </td>
                        <td nowrap width="10" class="LoanFormHeader">
                        </td>
                        <td nowrap class="LoanFormHeader">
                            Co-Applicant
                        </td>
                        <td nowrap class="LoanFormHeader">
                        </td>
                    </tr>
                    <tr>
                        <td nowrap colspan="2" class="FieldLabel">
                            Nearest Relative not living with applicant
                        </td>
                        <td nowrap>
                        </td>
                        <td nowrap colspan="2" class="FieldLabel">
                            Nearest Relative not living with co-applicant
                        </td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel">
                            Name
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sFHAPropImprovBorrRelativeNm" runat="server" Width="200px" TabIndex="5"></asp:TextBox>
                        </td>
                        <td nowrap>
                        </td>
                        <td nowrap class="FieldLabel">
                            Name
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sFHAPropImprovCoborRelativeNm" runat="server" Width="200px" TabIndex="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel" valign="top">
                            Address
                        </td>
                        <td nowrap>
                            <%-- 
                            <asp:TextBox ID="sFHAPropImprovBorrRelativeAddr" runat="server" Width="200px" TabIndex="5" TextMode="MultiLine"></asp:TextBox>
                            <br />
                            --%>
                            <asp:TextBox ID="sFHAPropImprovBorrRelativeStreetAddress" runat="server" Width="200px" TabIndex="5"></asp:TextBox>
                            <br />
                            <asp:TextBox ID="sFHAPropImprovBorrRelativeCity" runat="server" Width="100px" TabIndex="5"></asp:TextBox>
                            <ml:StateDropDownList ID="sFHAPropImprovBorrRelativeState" runat="server" TabIndex="5"></ml:StateDropDownList>
                            <ml:ZipcodeTextBox ID="sFHAPropImprovBorrRelativeZip" runat="server" preset="zipcode" Width="49" TabIndex="5"></ml:ZipcodeTextBox>
                        </td>
                        <td nowrap>
                        </td>
                        <td nowrap class="FieldLabel" valign="top">
                            Address
                        </td>
                        <td nowrap>
                            <%--
                            <asp:TextBox ID="sFHAPropImprovCoborRelativeAddr" runat="server" Width="200px" TabIndex="10" TextMode="MultiLine"></asp:TextBox>
                            <br />
                            --%>
                            <asp:TextBox ID="sFHAPropImprovCoborRelativeStreetAddress" runat="server" Width="200px" TabIndex="10"></asp:TextBox>
                            <br />
                            <asp:TextBox ID="sFHAPropImprovCoborRelativeCity" runat="server" Width="100px" TabIndex="10"></asp:TextBox>
                            <ml:StateDropDownList ID="sFHAPropImprovCoborRelativeState" runat="server" TabIndex="10"></ml:StateDropDownList>
                            <ml:ZipcodeTextBox ID="sFHAPropImprovCoborRelativeZip" runat="server" preset="zipcode" Width="49" TabIndex="10"></ml:ZipcodeTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel">
                            Relationship
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sFHAPropImprovBorrRelativeRelationship" runat="server" Width="120" TabIndex="5"></asp:TextBox>
                        </td>
                        <td nowrap>
                        </td>
                        <td nowrap class="FieldLabel">
                            Relationship
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sFHAPropImprovCoborRelativeRelationship" runat="server" Width="120" TabIndex="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel">
                            Phone
                        </td>
                        <td nowrap>
                            <ml:PhoneTextBox ID="sFHAPropImprovBorrRelativePhone" runat="server" preset="phone" Width="120" TabIndex="5"></ml:PhoneTextBox>
                        </td>
                        <td nowrap>
                        </td>
                        <td nowrap class="FieldLabel">
                            Phone
                        </td>
                        <td nowrap>
                            <ml:PhoneTextBox ID="sFHAPropImprovCoborRelativePhone" runat="server" preset="phone" Width="120" TabIndex="10"></ml:PhoneTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table7" cellspacing="0" cellpadding="0" width="98%" border="0" class="InsetBorder">
                    <tr>
                        <td nowrap class="LoanFormHeader">
                            Applicant
                        </td>
                        <td nowrap class="LoanFormHeader">
                        </td>
                        <td nowrap width="10" class="LoanFormHeader">
                        </td>
                        <td nowrap class="LoanFormHeader">
                            Co-Applicant
                        </td>
                    </tr>
                    <tr>
                        <td nowrap class="FieldLabel">
                            Bank Accounts
                            <asp:CheckBox ID="sFHAPropImprovBorrHasChecking" runat="server" Text="Checking" TabIndex="15"></asp:CheckBox>
                            <asp:CheckBox ID="sFHAPropImprovBorrHasSaving" runat="server" Text="Saving" TabIndex="15"></asp:CheckBox>
                            <asp:CheckBox ID="sFHAPropImprovBorrHasNoBankAcount" runat="server" Text="None" TabIndex="15"></asp:CheckBox>
                        </td>
                        <td nowrap>
                        </td>
                        <td nowrap>
                        </td>
                        <td nowrap class="FieldLabel">
                            Bank Accounts
                            <asp:CheckBox ID="sFHAPropImprovCoborHasChecking" runat="server" Text="Checking" TabIndex="20"></asp:CheckBox>
                            <asp:CheckBox ID="sFHAPropImprovCoborHasSaving" runat="server" Text="Saving" TabIndex="20"></asp:CheckBox>
                            <asp:CheckBox ID="sFHAPropImprovCoborHasNoBankAcount" runat="server" Text="None" TabIndex="20"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap colspan="2" class="FieldLabel">
                            Bank Name &amp; Address
                        </td>
                        <td nowrap>
                        </td>
                        <td nowrap class="FieldLabel">
                            Bank Name &amp; Address
                        </td>
                    </tr>
                    <tr>
                        <td nowrap colspan="2">
                            <asp:TextBox ID="sFHAPropImprovBorrBankInfo" runat="server" Width="284px" TextMode="MultiLine" Height="97px" TabIndex="15"></asp:TextBox>
                        </td>
                        <td nowrap>
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sFHAPropImprovCoborBankInfo" runat="server" Width="281px" TextMode="MultiLine" Height="96px" TabIndex="20"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="98%" class="InsetBorder" cellspacing="0" cellpadding="0">
                    <tr>
                        <td nowrap class="LoanFormHeader">
                            Debt
                        </td>
                    </tr>
                    <tr>
                        <td nowrap align="middle">
                            <a tabindex="-1" onclick="redirectToUladPage('Liabilities');">See Liabilities</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table class="InsetBorder" id="Table3" cellspacing="0" cellpadding="0" width="98%" border="0">
                    <tr>
                        <td class="LoanFormHeader" nowrap colspan="2">
                            Property to be Improved
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                            Type of Property
                        </td>
                        <td nowrap>
                            This property is:
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsPropSingleFamily" TabIndex="25" runat="server" Text="Single Family"></asp:CheckBox>
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsPropOwnedByBorr" TabIndex="30" runat="server" Text="Owned by borrower"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsPropMultifamily" TabIndex="25" runat="server" Text="Multifamily"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No. of Units
                            <asp:TextBox ID="sUnitsNum" TabIndex="25" runat="server" Width="39px"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsPropLeasedFromSomeone" TabIndex="30" runat="server" Text="Leased from someone else" onclick="enableLeaseInfo(this);"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsPropNonresidential" TabIndex="25" runat="server" Text="Nonresidential"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp; Type of use
                            <asp:TextBox ID="sFHAPropImprovNonresidentialUsageDesc" TabIndex="25" runat="server" Width="120px"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsPropBeingPurchasedOnContract" TabIndex="30" runat="server" Text="Purchased on a land installment contract"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsPropManufacturedHome" TabIndex="25" runat="server" Text="Manufactured home"></asp:CheckBox>
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsThereMortOnProp" TabIndex="30" runat="server" Text="Mortgage or deed of trust on this property"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsPropHistoricResidential" TabIndex="25" runat="server" Text="Historic residential structure"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp; No. of units
                            <asp:TextBox ID="sFHAPropImprovIsPropHistoricResidentialUnitsNum" TabIndex="25" runat="server" Width="39px"></asp:TextBox>
                        </td>
                        <td nowrap>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsPropHealthCareFacility" TabIndex="25" runat="server" Text="Health care facility"></asp:CheckBox>
                        </td>
                        <td nowrap>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table5" cellspacing="0" cellpadding="0" border="0" class="InsetBorder" width="98%">
                    <tr>
                        <td nowrap colspan="2" class="FieldLabel">
                            Name &amp; Address of Property Owner (if different from the applicant)
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="sFHAPropImprovLeaseOwnerInfo" runat="server" Width="383px" TextMode="MultiLine" Height="99px" TabIndex="35"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                        </td>
                        <td nowrap class="FieldLabel">
                            Monthly Lease Payment
                            <ml:MoneyTextBox ID="sFHAPropImprovLeaseMonPmt" runat="server" preset="money" Width="90" TabIndex="35"></ml:MoneyTextBox>&nbsp;&nbsp; Lease Expiration Date<ml:DateTextBox ID="sFHAPropImprovLeaseExpireD" runat="server" preset="date" Width="75" TabIndex="35"></ml:DateTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td nowrap>
                        </td>
                        <td nowrap>
                            <asp:CheckBox ID="sFHAPropImprovIsPropNewOccMoreThan90Days" runat="server" Text="If this is a new residential structure, it been completed and occupied for 90 days or longer." TabIndex="40"></asp:CheckBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <table id="Table4" style="width: 361px; height: 58px" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td class="FieldLabel" nowrap>
                            Name &amp; Address of Dealer / Contractor
                        </td>
                    </tr>
                    <tr>
                        <td nowrap width="50">
                            <asp:TextBox ID="sFHAPropImprovDealerContractorContactInfo" TabIndex="40" runat="server" Width="20pc" Height="105px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td nowrap>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
