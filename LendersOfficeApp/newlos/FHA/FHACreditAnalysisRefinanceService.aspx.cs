namespace LendersOfficeApp.newlos.FHA
{
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using System;

    public class FHACreditAnalysisRefinanceServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHACreditAnalysisRefinanceServiceItem));
        }
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "CopyFromLoanApp":
                    CopyFromLoanApp();
                    break;
                case "CopyFromGFE":
                    CopyFromGFE();
                    break;
            }
        }
        private void CopyFromLoanApp() 
        {
            bool isSave = GetString("issave", "") == "T";

            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(aAppId);
            if (isSave)
                BindData(dataLoan, dataApp);

            dataApp.FhaMcawApplyAppFieldsFrom1003();

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan, dataApp);
        }
        private void CopyFromGFE() 
        {
            bool isSave = GetString("issave", "") == "T";

            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(aAppId);
            
            if (isSave)
                BindData(dataLoan, dataApp);

            dataLoan.ApplyGfeItemsToMcawRefi();

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan, dataApp);
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            dataApp.aFHANegCfRentalI_rep = GetString("aFHANegCfRentalI");
            dataApp.aBFirstNm                  = GetString("aBFirstNm") ;
            dataApp.aBMidNm                    = GetString("aBMidNm") ;
            dataApp.aBLastNm                   = GetString("aBLastNm") ;
            dataApp.aBSuffix                   = GetString("aBSuffix") ;
            dataApp.aBSsn                      = GetString("aBSSN") ;
            dataApp.aCFirstNm                  = GetString("aCFirstNm") ;
            dataApp.aCMidNm                    = GetString("aCMidNm") ;
            dataApp.aCLastNm                   = GetString("aCLastNm") ;
            dataApp.aCSuffix                   = GetString("aCSuffix") ;
            dataApp.aCSsn                      = GetString("aCSSN") ;
            dataLoan.sLT                       = (E_sLT) GetInt("sLT") ;
            dataLoan.sFHACreditAnalysisRemarks = GetString("sFHACreditAnalysisRemarks") ;
            IPreparerFields underwriter        = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisRefinanceUnderwriter, E_ReturnOptionIfNotExist.CreateNew);
            underwriter.PreparerName = GetString("FHACreditAnalysisRefiancePreparerName") ;
            underwriter.LicenseNumOfAgent = GetString("FHACreditAnalysisRefinanceLicenseNumOfAgent") ;
            underwriter.Update();

            dataLoan.sFHAConstructionT           = (E_sFHAConstructionT) GetInt("sFHAConstructionT") ;
            dataLoan.sFHARefinanceTypeDesc       = GetString("sFHARefinanceTypeDesc");
            dataLoan.sAgencyCaseNum              = GetString("sAgencyCaseNum") ;
            dataLoan.sFHAHousingActSection       = GetString("sFHAHousingActSection") ;
            dataLoan.sLAmtCalc_rep               = GetString("sLAmt") ;
            dataLoan.sFfUfmip1003_rep            = GetString("sFfUfmip1003") ;
            dataLoan.sNoteIR_rep                 = GetString("sNoteIR") ;
            dataLoan.sApprVal_rep                = GetString("sApprVal") ;
            dataLoan.sFHACcTot_rep               = GetString("sFHACcTot") ;
            dataLoan.sFHACcPbs_rep               = GetString("sFHACcPbs") ;
            dataLoan.sFHASellerContribution_rep  = GetString("sFHASellerContribution") ;
            dataLoan.sFHAExcessContribution_rep  = GetString("sFHAExcessContribution") ;
            dataLoan.sFHAExistingMLien_rep       = GetString("sFHAExistingMLien") ;
            dataLoan.sFHAMBasisRefinMultiply_rep = GetString("sFHAMBasisRefinMultiply") ;
            dataLoan.sFHAApprValMultiply_rep     = GetString("sFHAApprValMultiply") ;
            dataLoan.sFHAMBasisRefinMultiplyLckd = GetBool("sFHAMBasisRefinMultiplyLckd") ;
            dataLoan.sFHAApprValMultiplyLckd     = GetBool("sFHAApprValMultiplyLckd") ;
            dataLoan.sFHAAmtPaid_rep             = GetString("sFHAAmtPaid") ;
            dataLoan.sFHAIsAmtPdInOther          = GetBool("sFHAIsAmtPdInOther") ;
            dataLoan.sFHAIsAmtPdInCash           = GetBool("sFHAIsAmtPdInCash") ;
            dataLoan.sFHAIsAmtToBePdInOther      = GetBool("sFHAIsAmtToBePdInOther") ;
            dataLoan.sFHAIsAmtToBePdInCash       = GetBool("sFHAIsAmtToBePdInCash") ;
            dataLoan.sFHAImprovementsDesc        = GetString("sFHAImprovementsDesc") ;
            dataLoan.sFHAImprovements_rep        = GetString("sFHAImprovements") ;
            dataLoan.sFHASalesConcessions_rep    = GetString("sFHASalesConcessions") ;
            dataLoan.sLAmtCalc_rep               = GetString("sLAmt2") ;
            dataLoan.sFHADiscountPoints_rep      = GetString("sFHADiscountPoints") ;
            dataLoan.sFHAPrepaidExp_rep          = GetString("sFHAPrepaidExp") ;
            dataLoan.SetsUfCashPdViaImport(GetString("sUfCashPd")); // OPM 32441
            dataLoan.sFHANonrealty_rep           = GetString("sFHANonrealty") ;
            dataLoan.sFHANonrealtyLckd           = GetBool("sFHANonrealtyLckd") ;
            dataApp.aFHAAssetAvail_rep           = GetString("aFHAAssetAvail") ;
            dataLoan.sFHA2ndMAmt_rep             = GetString("sFHA2ndMAmt") ;
            dataApp.aFHABBaseI_rep               = GetString("aFHABBaseI") ;
            dataApp.aFHABOI_rep                  = GetString("aFHABOI") ;
            dataApp.aFHACBaseI_rep               = GetString("aFHACBaseI") ;
            dataApp.aFHACOI_rep                  = GetString("aFHACOI") ;
            dataApp.aFHANetRentalI_rep           = GetString("aFHANetRentalI") ;
            dataApp.aFHADebtInstallPmt_rep       = GetString("aFHADebtInstallPmt") ;
            dataApp.aFHADebtInstallBal_rep       = GetString("aFHADebtInstallBal") ;

            if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V25_ConsolidateMonthlyChildSupportPayments))
            {
                dataApp.aFHAChildSupportPmt_rep = GetString("aFHAChildSupportPmt");
            }
            
            dataApp.aFHAOtherDebtPmt_rep         = GetString("aFHAOtherDebtPmt") ;
            dataApp.aFHAOtherDebtBal_rep         = GetString("aFHAOtherDebtBal") ;
            dataLoan.sFHAPro1stMPmt_rep          = GetString("sFHAPro1stMPmt") ;
            dataLoan.sFHAProMIns_rep             = GetString("sFHAProMIns") ;
            dataLoan.sFHAProMInsLckd = GetBool("sFHAProMInsLckd");
            dataLoan.sFHAProHoAssocDues_rep      = GetString("sFHAProHoAssocDues") ;
            dataLoan.sFHAProGroundRent_rep       = GetString("sFHAProGroundRent") ;
            dataLoan.sFHAPro2ndFinPmt_rep        = GetString("sFHAPro2ndFinPmt") ;
            dataLoan.sFHAProHazIns_rep           = GetString("sFHAProHazIns") ;
            dataLoan.sFHAProRealETx_rep          = GetString("sFHAProRealETx") ;
            dataApp.aFHACreditRating             = GetString("aFHACreditRating") ;
            dataApp.aFHARatingIAdequacy          = GetString("aFHARatingIAdequacy") ;
            dataApp.aFHARatingIStability         = GetString("aFHARatingIStability") ;
            dataApp.aFHARatingAssetAdequacy      = GetString("aFHARatingAssetAdequacy") ;
            dataApp.aFHABCaivrsNum               = GetString("aFHABCaivrsNum") ;
            dataApp.aFHACCaivrsNum               = GetString("aFHACCaivrsNum") ;
            dataApp.aFHABLpdGsa                  = GetString("aFHABLpdGsa") ;
            dataApp.aFHACLpdGsa                  = GetString("aFHACLpdGsa") ;
            dataLoan.sFHAGiftAmtTot_rep          = GetString("sFHAGiftAmtTot") ;
            dataLoan.SetsUfCashPdViaImport(GetString("sUfCashPdzzz2")); // OPM 32441
            dataLoan.sFfUfmipR_rep               = GetString("sFfUfmipR") ;
            dataLoan.sFHAEnergyEffImprov_rep     = GetString("sFHAEnergyEffImprov") ;
            dataLoan.sFfUfmip1003Lckd            = GetBool("sFfUfmip1003Lckd");
            dataLoan.sMipPiaMon_rep              = GetString("sMipPiaMon") ;

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("aFHANegCfRentalI", dataApp.aFHANegCfRentalI_rep);

            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aBSSN", dataApp.aBSsn);

            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aCSSN", dataApp.aCSsn);

            SetResult("sLT", dataLoan.sLT);
            SetResult("sFHACreditAnalysisRemarks", dataLoan.sFHACreditAnalysisRemarks);
            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisRefinanceUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("FHACreditAnalysisRefiancePreparerName", underwriter.PreparerName);
            SetResult("FHACreditAnalysisRefinanceLicenseNumOfAgent", underwriter.LicenseNumOfAgent);

            SetResult("sFHARefinanceTypeDesc", dataLoan.sFHARefinanceTypeDesc );

            SetResult("sFHAConstructionT", dataLoan.sFHAConstructionT);
            SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            SetResult("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            SetResult("sLAmt", dataLoan.sLAmtCalc_rep);
            SetResult("sPresLTotPersistentHExp", dataApp.aPresTotHExp_rep);
            SetResult("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sBuydownResultIR", dataLoan.sBuydownResultIR_rep);
            SetResult("sApprVal", dataLoan.sApprVal_rep);
            SetResult("sFHACcTot", dataLoan.sFHACcTot_rep);
            SetResult("sFHACcPbs", dataLoan.sFHACcPbs_rep);
            SetResult("sFHACcPbb", dataLoan.sFHACcPbb_rep);
            SetResult("sFHACcPbb2", dataLoan.sFHACcPbb_rep);
            SetResult("sPurchPrice2", dataLoan.sFHAExistingMLien_rep);
            SetResult("sPurchPrice6pc", dataLoan.sPurchPrice6pc_rep);
            SetResult("sFHASellerContribution", dataLoan.sFHASellerContribution_rep);
            SetResult("sFHAExcessContribution", dataLoan.sFHAExcessContribution_rep);
            SetResult("sFHAExistingMLien", dataLoan.sFHAExistingMLien_rep);
            SetResult("sFHAMBasisRefin", dataLoan.sFHAMBasisRefin_rep);
            SetResult("sFHAMBasisRefinMultiply", dataLoan.sFHAMBasisRefinMultiply_rep);
            SetResult("sFHAApprValMultiply", dataLoan.sFHAApprValMultiply_rep);
            SetResult("sFHAMBasisRefinMultiplyLckd", dataLoan.sFHAMBasisRefinMultiplyLckd);
            SetResult("sFHAApprValMultiplyLckd", dataLoan.sFHAApprValMultiplyLckd);
            SetResult("sFHAReqInvestment", dataLoan.sFHAReqInvestment_rep);
            SetResult("sFHAReqTot", dataLoan.sFHAReqTot_rep);
            SetResult("sFHAAmtPaid", dataLoan.sFHAAmtPaid_rep);
            SetResult("sFHAIsAmtPdInOther", dataLoan.sFHAIsAmtPdInOther);
            SetResult("sFHAIsAmtPdInCash", dataLoan.sFHAIsAmtPdInCash);
            SetResult("sFHAIsAmtToBePdInOther", dataLoan.sFHAIsAmtToBePdInOther);
            SetResult("sFHAIsAmtToBePdInCash", dataLoan.sFHAIsAmtToBePdInCash);
            SetResult("sTermInYr", dataLoan.sTermInYr_rep);
            SetResult("sFHAImprovementsDesc", dataLoan.sFHAImprovementsDesc);
            SetResult("sFHAImprovements", dataLoan.sFHAImprovements_rep);
            SetResult("sFHASalesConcessions", dataLoan.sFHASalesConcessions_rep);
            SetResult("sLAmt2", dataLoan.sLAmtCalc_rep);
            SetResult("sFHADiscountPoints", dataLoan.sFHADiscountPoints_rep);
            SetResult("sFHAPrepaidExp", dataLoan.sFHAPrepaidExp_rep);
            SetResult("sUfCashPd", dataLoan.sUfCashPd_rep);
            SetResult("sFHANonrealty", dataLoan.sFHANonrealty_rep);
            SetResult("sFHANonrealtyLckd", dataLoan.sFHANonrealtyLckd);
            SetResult("aFHAAssetAvail", dataApp.aFHAAssetAvail_rep);
            SetResult("sFHA2ndMAmt", dataLoan.sFHA2ndMAmt_rep);
            SetResult("aFHABBaseI", dataApp.aFHABBaseI_rep);
            SetResult("aFHABOI", dataApp.aFHABOI_rep);
            SetResult("aFHACBaseI", dataApp.aFHACBaseI_rep);
            SetResult("aFHACOI", dataApp.aFHACOI_rep);
            SetResult("aFHANetRentalI", dataApp.aFHANetRentalI_rep);
            SetResult("aFHAGrossMonI", dataApp.aFHAGrossMonI_rep);
            SetResult("aFHADebtInstallPmt", dataApp.aFHADebtInstallPmt_rep);
            SetResult("aFHADebtInstallBal", dataApp.aFHADebtInstallBal_rep);
            SetResult("aFHAChildSupportPmt", dataApp.aFHAChildSupportPmt_rep);
            SetResult("aFHAOtherDebtPmt", dataApp.aFHAOtherDebtPmt_rep);
            SetResult("aFHAOtherDebtBal", dataApp.aFHAOtherDebtBal_rep);
            SetResult("aFHADebtPmtTot", dataApp.aFHADebtPmtTot_rep);
            SetResult("sFHAPro1stMPmt", dataLoan.sFHAPro1stMPmt_rep);
            SetResult("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            SetResult("sFHAProMInsLckd", dataLoan.sFHAProMInsLckd);
            SetResult("sFHAProHoAssocDues", dataLoan.sFHAProHoAssocDues_rep);
            SetResult("sFHAProGroundRent", dataLoan.sFHAProGroundRent_rep);
            SetResult("sFHAPro2ndFinPmt", dataLoan.sFHAPro2ndFinPmt_rep);
            SetResult("sFHAProHazIns", dataLoan.sFHAProHazIns_rep);
            SetResult("sFHAProRealETx", dataLoan.sFHAProRealETx_rep);
            SetResult("sFHAMonthlyPmt", dataLoan.sFHAMonthlyPmt_rep);
            SetResult("aFHADebtPmtTot2", dataApp.aFHADebtPmtTot_rep);
            SetResult("aFHAPmtFixedTot", dataApp.aFHAPmtFixedTot_rep);
            SetResult("sFHALtv2", dataLoan.sFHALtvRefi_rep);
            SetResult("aFHAMPmtToIRatio", dataApp.aFHAMPmtToIRatio_rep);
            SetResult("aFHAFixedPmtToIRatio", dataApp.aFHAFixedPmtToIRatio_rep);
            SetResult("aFHACreditRating", dataApp.aFHACreditRating);
            SetResult("aFHARatingIAdequacy", dataApp.aFHARatingIAdequacy);
            SetResult("aFHARatingIStability", dataApp.aFHARatingIStability);
            SetResult("aFHARatingAssetAdequacy", dataApp.aFHARatingAssetAdequacy);
            SetResult("aFHABCaivrsNum", dataApp.aFHABCaivrsNum);
            SetResult("aFHACCaivrsNum", dataApp.aFHACCaivrsNum);
            SetResult("aFHABLpdGsa", dataApp.aFHABLpdGsa);
            SetResult("aFHACLpdGsa", dataApp.aFHACLpdGsa);
            SetResult("sFHAGiftAmtTot", dataLoan.sFHAGiftAmtTot_rep);
            SetResult("sFHAAmtToBePd", dataLoan.sFHAAmtToBePd_rep);
            SetResult("sUfCashPdzzz2", dataLoan.sUfCashPd_rep);
            SetResult("sFfUfmipR", dataLoan.sFfUfmipR_rep);
            SetResult("sFHAEnergyEffImprov", dataLoan.sFHAEnergyEffImprov_rep);
            SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep );
            SetResult("sMipPiaMon", dataLoan.sMipPiaMon_rep);
            SetResult("sFfUfmip1003Lckd", dataLoan.sFfUfmip1003Lckd);

        }
    }

	public partial class FHACreditAnalysisRefinanceService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FHACreditAnalysisRefinanceServiceItem());
        }

	}
}
