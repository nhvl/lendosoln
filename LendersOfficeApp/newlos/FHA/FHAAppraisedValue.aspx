<%@ Page language="c#" Codebehind="FHAAppraisedValue.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHAAppraisedValue" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>FHAAppraisedValue</title>

	</HEAD>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<form id=FHAAppraisedValue method=post runat="server">

<script type="text/javascript">
  function _init() {
	  <%= AspxTools.JsGetElementById(sProOHExp) %>.readOnly = !<%= AspxTools.JsGetElementById(sProOHExpLckd) %>.checked;
	}
</script>
<table id=Table1 cellSpacing=0 cellPadding=0 border=0>
  <tr>
    <td class=MainRightHeader noWrap>Statement of 
      Appraised Value (HUD-92800.5B)</td></tr>
  <tr>
    <td class=FieldLabel noWrap>
      <table id=Table2 cellSpacing=0 cellPadding=0 width="100%" border=0>
        <tr>
          <td class=FieldLabel noWrap colSpan=5><asp:checkbox id=sFHACondCommUnderNationalHousingAct runat="server" ToolTip="Conditional Commitment for mortgage insurance under the National Housing Act. Sec." Text="Section of the Act"></asp:checkbox><ml:combobox id=sFHAHousingActSection runat="server"></ml:combobox></td>
          <td class=FieldLabel nowrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap><asp:checkbox id=sFHACondCommSeeBelow runat="server" Text="See below"></asp:checkbox></td>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap width=10></td>
          <td class=FieldLabel noWrap>Est. Value</td>
          <td class=FieldLabel noWrap><ml:moneytextbox id=sApprVal tabIndex=10 runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
          <td class=FieldLabel nowrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap>By: </td>
          <td class=FieldLabel noWrap><asp:textbox id=FHAConditionalCommitmentPreparerName tabIndex=5 runat="server" width="131px"></asp:textbox></td>
          <td class=FieldLabel noWrap width=10></td>
          <td class=FieldLabel noWrap>Property Address</td>
          <td class=FieldLabel noWrap><asp:textbox id=sSpAddr tabIndex=10 runat="server" Width="229px"></asp:textbox></td>
          <td class=FieldLabel nowrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap>Action Date </td>
          <td class=FieldLabel noWrap><ml:datetextbox id=FHAConditionalCommitmentNamePrepareDate tabIndex=5 runat="server" width="75" preset="date"></ml:datetextbox></td>
          <td class=FieldLabel noWrap width=10></td>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap><asp:textbox id=sSpCity tabIndex=10 runat="server" Width="125px"></asp:textbox><ml:statedropdownlist id=sSpState tabIndex=10 runat="server" IncludeTerritories="false"></ml:statedropdownlist><ml:zipcodetextbox id=sSpZip tabIndex=10 runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></td>
          <td class=FieldLabel nowrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap>Lender ID</td>
          <td class=FieldLabel noWrap><asp:textbox id=sFHALenderIdCode tabIndex=5 runat="server" width="131px"></asp:textbox></td>
          <td class=FieldLabel noWrap width=10></td>
          <td class=FieldLabel noWrap>County</td>
          <td class=FieldLabel noWrap><asp:DropDownList id=sSpCounty tabIndex=10 runat="server"></asp:DropDownList></td>
          <td class=FieldLabel nowrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap>FHA Case No.</td>
          <td class=FieldLabel noWrap><asp:textbox id=sAgencyCaseNum tabIndex=5 runat="server" width="131px"></asp:textbox></td>
          <td class=FieldLabel noWrap width=10></td>
          <td class=FieldLabel noWrap>Construction</td>
          <td class=FieldLabel noWrap>
            <asp:dropdownlist id=sFHAConstructionT tabIndex=10 runat="server">
            </asp:dropdownlist></td>
          <td class=FieldLabel nowrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap>Sponsor/Agent</td>
          <td class=FieldLabel noWrap><asp:textbox id=sFHASponsorAgentIdCode tabIndex=5 runat="server" width="131px"></asp:textbox></td>
          <td class=FieldLabel noWrap width=10></td>
          <td class=FieldLabel noWrap>Commitment 
Issued</td>
          <td class=FieldLabel noWrap><ml:datetextbox id=sFHACondCommIssuedD tabIndex=10 runat="server" width="75" preset="date"></ml:datetextbox></td>
          <td class=FieldLabel nowrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap>INST Case Ref. 
          No.</td>
          <td class=FieldLabel noWrap><asp:textbox id=sFHACondCommInstCaseRef tabIndex=5 runat="server" width="131px"></asp:textbox></td>
          <td class=FieldLabel noWrap width=10></td>
          <td class=FieldLabel noWrap>Commitment 
          Expires</td>
          <td class=FieldLabel noWrap><ml:datetextbox id=sFHACondCommExpiredD tabIndex=10 runat="server" width="75" preset="date"></ml:datetextbox></td>
          <td class=FieldLabel nowrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap width=10></td>
          <td class=FieldLabel noWrap>Improved Living 
          Area</td>
          <td class=FieldLabel noWrap colSpan=1><asp:textbox id=sFHACondCommImprovedLivingArea tabIndex=10 runat="server" Width="68px"></asp:textbox>Sq. 
            Feet</td>
          <td class=FieldLabel nowrap></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2>Mortgagee</td>
          <td class=FieldLabel noWrap width=10></td>
          <td class=FieldLabel noWrap colSpan=3>Monthly 
            Expense Estimate</td></tr>
        <tr>
          <td class=FieldLabel valign=top nowrap colspan=2>
            <table id=Table4 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td nowrap><asp:textbox id=FHACondCommitMortgageeCompanyName tabIndex=15 runat="server" Width="229px"></asp:textbox></td></tr>
              <tr>
                <td nowrap><asp:textbox id=FHACondCommitMortgageeStreetAddr tabIndex=15 runat="server" Width="229px"></asp:textbox></td></tr>
              <tr>
                <td nowrap><asp:textbox id=FHACondCommitMortgageeCity tabIndex=15 runat="server" Width="129px"></asp:textbox><ml:statedropdownlist id=FHACondCommitMortgageeState tabIndex=15 runat="server"></ml:statedropdownlist><ml:zipcodetextbox id=FHACondCommitMortgageeZip tabIndex=15 runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></td></tr></table></td>
          <td class=FieldLabel nowrap width=10></td>
          <td class=FieldLabel nowrap colspan=3 rowspan=7>
            <table id=Table3 cellspacing=0 cellpadding=0 border=0>
              <tr>
                <td class=FieldLabel nowrap>Fire Ins.</td>
                <td nowrap>
                    <asp:PlaceHolder runat="server" ID="sProHazInsHolder">
                        <asp:DropDownList id=sProHazInsT runat="server" onchange="refreshCalculation();" tabindex=20></asp:DropDownList>
                        <ml:PercentTextBox id=sProHazInsR runat="server" width="50px" preset="percent" onchange="refreshCalculation();" tabindex=20></ml:PercentTextBox>
                        +<ml:MoneyTextBox id=sProHazInsMb runat="server" width="60px" preset="money" onchange="refreshCalculation();" tabindex=20 decimalDigits="4"></ml:MoneyTextBox>=
                    </asp:PlaceHolder>
                </td>
                <td nowrap>
                    <ml:moneytextbox id=sProHazIns tabIndex=20 runat="server" width="70px" preset="money" ReadOnly="True"></ml:moneytextbox>
                </td>
              </tr>
              <tr>
                <td class=FieldLabel nowrap>Taxes</td>
                <td nowrap>
                    <asp:PlaceHolder runat="server" ID="sProRealETxHolder">
                        <asp:DropDownList id=sProRealETxT runat="server" onchange="refreshCalculation();" tabindex=20></asp:DropDownList>
                        <ml:PercentTextBox id=sProRealETxR runat="server" width="50px" preset="percent" onchange="refreshCalculation();" tabindex=20></ml:PercentTextBox>
                        +<ml:MoneyTextBox id=sProRealETxMb runat="server" width="60px" preset="money" onchange="refreshCalculation();" tabindex=20></ml:MoneyTextBox>=
                    </asp:PlaceHolder>
                </td>
                <td nowrap><ml:moneytextbox id=sProRealETx tabIndex=20 runat="server" width="70px" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap colspan=2>HOA Dues</td>
                <td nowrap><ml:moneytextbox id=sProHoAssocDues tabIndex=20 runat="server" width="70px" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>Other</td>
                <td nowrap></td>
                <td nowrap><ml:moneytextbox id=sProOHExp tabIndex=20 runat="server" width="70px" preset="money" onchange="refreshCalculation();"></ml:moneytextbox><asp:CheckBox ID="sProOHExpLckd" runat="server" TabIndex="20" Text="Lock" onclick="refreshCalculation();" /></td></tr>
              <tr>
                <td class=FieldLabel nowrap>Total</td>
                <td nowrap></td>
                <td nowrap><ml:moneytextbox id=sFHACondCommMonExpenseEstimate tabIndex=20 runat="server" width="70px" preset="money" ReadOnly="True"></ml:moneytextbox></td></tr></table></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2></td>
          <td class=FieldLabel nowrap width=10></td></tr>
        <tr>
          <td class=FieldLabel noWrap colSpan=2></td>
          <td class=FieldLabel nowrap width=10></td></tr>
        <tr>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap width=10></td></tr>
        <tr>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap width=10></td></tr>
        <tr>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap width=10></td></tr>
        <tr>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap width=10></td></tr>
        <tr>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap></td>
          <td class=FieldLabel noWrap width=10></td></tr></table></td></tr>
  <tr>
    <td class=LoanFormHeader noWrap align=center>Specific 
      Commitment Conditions</td></tr>
  <tr>
    <td class=FieldLabel noWrap>Estimated Remaining 
      Economic Life of this property is <asp:textbox id=sFHACondCommRemainEconLife tabIndex=25 runat="server" Width="53px"></asp:textbox>&nbsp;years.</td></tr>
  <tr>
    <td class=FieldLabel noWrap>This property <asp:CheckBoxList id=sFHACondCommMaxFinanceEligibleTri runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" tabindex=25>
<asp:ListItem Value="1">is</asp:ListItem>
<asp:ListItem Value="2">is not</asp:ListItem>
</asp:CheckBoxList>&nbsp;eligible 
      for maximum financing</td></tr>
  <tr>
    <td class=FieldLabel noWrap><asp:checkbox id=sFHACondCommIsManufacturedHousing tabIndex=25 runat="server" Text="Manufactured Housing"></asp:checkbox></td></tr>
  <tr>
    <td class=FieldLabel noWrap><asp:checkbox id=sFHACondCommIsAssuranceOfCompletion tabIndex=25 runat="server" Text="Assurance of Completion: Amount to ensure completion"></asp:checkbox><ml:moneytextbox id=sFHACondCommAssuranceOfCompletionAmt tabIndex=25 runat="server" width="90" preset="money"></ml:moneytextbox></td></tr>
  <tr>
    <td class=FieldLabel noWrap><asp:checkbox id=sFHACondCommSeeAttached tabIndex=25 runat="server" Text="See indicated additional items on attached:"></asp:checkbox><asp:textbox id=sFHACondCommSeeAttachedDesc tabIndex=25 runat="server" Width="235px"></asp:textbox></td></tr>
  <tr>
    <td class=FieldLabel noWrap><asp:checkbox id=sFHACondCommSeeFollowingCondsOnBack tabIndex=25 runat="server" Text="See the following additional conditions on the back:"></asp:checkbox></td></tr>
  <tr>
    <td class=FieldLabel noWrap>Additional 
  Conditions</td></tr>
  <tr>
    <td class=FieldLabel noWrap><asp:textbox id=sFHACondCommCondsOnBack1 tabIndex=25 runat="server" Width="590px"></asp:textbox></td></tr>
  <TR>
    <TD class=FieldLabel noWrap><asp:textbox id=sFHACondCommCondsOnBack2 tabIndex=25 runat="server" Width="590px"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap><asp:textbox id=sFHACondCommCondsOnBack3 tabIndex=25 runat="server" Width="590px"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap><asp:textbox id=sFHACondCommCondsOnBack4 tabIndex=25 runat="server" Width="590px"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap><asp:textbox id=sFHACondCommCondsOnBack5 tabIndex=25 runat="server" Width="590px"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap><asp:textbox id=sFHACondCommCondsOnBack6 tabIndex=25 runat="server" Width="590px"></asp:textbox></TD></TR>
  <TR>
    <TD class=FieldLabel noWrap><asp:textbox id=sFHACondCommCondsOnBack7 tabIndex=25 runat="server" Width="590px"></asp:textbox></TD></TR>
  <tr>
    <td class=FieldLabel noWrap></td></tr></table></form>
	</body>
</HTML>
