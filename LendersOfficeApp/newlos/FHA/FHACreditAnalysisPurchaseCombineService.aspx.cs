
namespace LendersOfficeApp.newlos.FHA
{
    using DataAccess;
    using LendersOffice.Common;
    using System;

    public class FHACreditAnalysisPurchaseCombineServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHACreditAnalysisPurchaseCombineServiceItem));
        }
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "CopyFromLoanApp":
                    CopyFromLoanApp();
                    break;
                case "CopyFromGFE":
                    CopyFromGFE();
                    break;
            }

        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisPurchaseUnderwriter, E_ReturnOptionIfNotExist.CreateNew );
            underwriter.LicenseNumOfAgent = GetString("FHACreditAnalysisPurchaseUnderwriterLicenseNumOfAgent");
            underwriter.PreparerName = GetString("FHACreditAnalysisPurchaseUnderwriterPreparerName");
            underwriter.Update();

            dataLoan.sFHANegCfRentalI_rep = GetString("sFHANegCfRentalI");
            dataLoan.sAgencyCaseNum                = GetString("sAgencyCaseNum");
            dataLoan.sApprVal_rep                  = GetString("sApprVal");
            dataLoan.sCombinedBorFirstNm           = GetString("sCombinedBorFirstNm") ;
            dataLoan.sCombinedBorInfoLckd          = GetBool("sCombinedBorInfoLckd") ;
            dataLoan.sCombinedBorLastNm            = GetString("sCombinedBorLastNm") ;
            dataLoan.sCombinedBorMidNm             = GetString("sCombinedBorMidNm") ;
            dataLoan.sCombinedBorSsn               = GetString("sCombinedBorSsn") ;
            dataLoan.sCombinedBorSuffix            = GetString("sCombinedBorSuffix") ;
            dataLoan.sCombinedCoborFirstNm         = GetString("sCombinedCoborFirstNm") ;
            dataLoan.sCombinedCoborLastNm          = GetString("sCombinedCoborLastNm") ;
            dataLoan.sCombinedCoborMidNm           = GetString("sCombinedCoborMidNm") ;
            dataLoan.sCombinedCoborSsn             = GetString("sCombinedCoborSsn") ;
            dataLoan.sCombinedCoborSuffix          = GetString("sCombinedCoborSuffix") ;
            dataLoan.sCombinedCoborUsing2ndAppBorr = GetBool("sCombinedCoborUsing2ndAppBorr");
            dataLoan.sFHA2ndMAmt_rep               = GetString("sFHA2ndMAmt");
            dataLoan.sFHA2ndMSrc                   = GetString("sFHA2ndMSrc");
            dataLoan.sFHAAmtPaidByCash             = GetBool("sFHAAmtPaidByCash");
            dataLoan.sFHAAmtPaidByOtherT           = GetBool("sFHAAmtPaidByOtherT");
            dataLoan.sFHAAmtPaid_rep               = GetString("sFHAAmtPaid");
            dataLoan.sFHABBaseI_rep                = GetString("sFHABBaseI");
            dataLoan.sFHABOI_rep                   = GetString("sFHABOI");
            dataLoan.sFHACBaseI_rep                = GetString("sFHACBaseI");
            dataLoan.sFHACOI_rep                   = GetString("sFHACOI");
            dataLoan.sFHACcPbs_rep                 = GetString("sFHACcPbs");
            dataLoan.sFHACcTot_rep                 = GetString("sFHACcTot");
            dataLoan.sFHAChildSupportPmt_rep       = GetString("sFHAChildSupportPmt");
            dataLoan.sFHAConstructionT             = (E_sFHAConstructionT) GetInt("sFHAConstructionT");
            dataLoan.sFHACreditAnalysisRemarks     = GetString("sFHACreditAnalysisRemarks");
            dataLoan.sFHADebtInstallBal_rep        = GetString("sFHADebtInstallBal");
            dataLoan.sFHADebtInstallPmt_rep        = GetString("sFHADebtInstallPmt");
            dataLoan.sFHADebtLckd                  = GetBool("sFHADebtLckd");
            dataLoan.sFHADiscountPoints_rep        = GetString("sFHADiscountPoints");
            dataLoan.sFHAEnergyEffImprov_rep       = GetString("sFHAEnergyEffImprov");
            dataLoan.sFHAExcessContribution_rep    = GetString("sFHAExcessContribution");
            dataLoan.sFHAHousingActSection         = GetString("sFHAHousingActSection");
            dataLoan.sFHAImprovements_rep          = GetString("sFHAImprovements");
            dataLoan.sFHAIncomeLckd                = GetBool("sFHAIncomeLckd");
            dataLoan.sFHALtvLckd                   = GetBool("sFHALtvLckd");
            dataLoan.sFHALtv_rep                   = GetString("sFHALtv");
            dataLoan.sFHANetRentalI_rep            = GetString("sFHANetRentalI");
            dataLoan.sFHANonrealtyLckd             = GetBool("sFHANonrealtyLckd");
            dataLoan.sFHANonrealty_rep             = GetString("sFHANonrealty");
            dataLoan.sFHAOtherDebtBal_rep          = GetString("sFHAOtherDebtBal");
            dataLoan.sFHAOtherDebtPmt_rep          = GetString("sFHAOtherDebtPmt");
            dataLoan.sFHAPrepaidExp_rep            = GetString("sFHAPrepaidExp");
            dataLoan.sFHAPro1stMPmt_rep            = GetString("sFHAPro1stMPmt");
            dataLoan.sFHAPro2ndFinPmt_rep          = GetString("sFHAPro2ndFinPmt");
            dataLoan.sFHAProGroundRent_rep         = GetString("sFHAProGroundRent");
            dataLoan.sFHAProHazIns_rep             = GetString("sFHAProHazIns");
            dataLoan.sFHAProHoAssocDues_rep        = GetString("sFHAProHoAssocDues");
            dataLoan.sFHAProMIns_rep               = GetString("sFHAProMIns");
            dataLoan.sFHAProMInsLckd = GetBool("sFHAProMInsLckd");
            dataLoan.sFHAProRealETx_rep            = GetString("sFHAProRealETx");
            dataLoan.sFHAPurchPriceSrcT            = (E_sFHAPurchPriceSrcT) GetInt("sFHAPurchPriceSrcT");
            dataLoan.sFHAPurchPrice_rep            = GetString("sFHAPurchPrice");
            dataLoan.sFHAReqAdj_rep                = GetString("sFHAReqAdj");
            dataLoan.sFHASellerContribution_rep    = GetString("sFHASellerContribution");
            dataLoan.sFfUfmip1003Lckd              = GetBool("sFfUfmip1003Lckd");
            dataLoan.sFfUfmip1003_rep              = GetString("sFfUfmip1003");
            dataLoan.sFfUfmipR_rep                 = GetString("sFfUfmipR");
            dataLoan.sLT                           = (E_sLT) GetInt("sLT");
            dataLoan.sMipPiaMon_rep                = GetString("sMipPiaMon");
            dataLoan.sNoteIR_rep                   = GetString("sNoteIR");
            dataLoan.SetsUfCashPdViaImport(GetString("sUfCashPd")); // OPM 32441
            dataLoan.sFHA203kInvestmentRequiredPc_rep = GetString("sFHA203kInvestmentRequiredPc");

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        private void CopyFromLoanApp() 
        {
            bool isSave = GetString("issave", "") == "T";
            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(sFileVersion);

            if (isSave)
                BindData(dataLoan, null);
            dataLoan.FhaMcawApplyFromAll1003s();
            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan, null);
        }
        private void CopyFromGFE() 
        {
            bool isSave = GetString("issave", "") == "T";

            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(sFileVersion);

            if (isSave)
                BindData(dataLoan, null);

            dataLoan.ApplyGfeItemsToMcawPurch();
            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan, null);
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisPurchaseUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("FHACreditAnalysisPurchaseUnderwriterLicenseNumOfAgent", underwriter.LicenseNumOfAgent);
            SetResult("FHACreditAnalysisPurchaseUnderwriterPreparerName", underwriter.PreparerName);

            SetResult("sFHANegCfRentalI", dataLoan.sFHANegCfRentalI_rep);
            SetResult("sAgencyCaseNum",            dataLoan.sAgencyCaseNum);
            SetResult("sApprVal",                  dataLoan.sApprVal_rep);
            SetResult("sBuydownResultIR",          dataLoan.sBuydownResultIR_rep);
            SetResult("sCombinedBorFirstNm",       dataLoan.sCombinedBorFirstNm );
            SetResult("sCombinedBorInfoLckd",      dataLoan.sCombinedBorInfoLckd );
            SetResult("sCombinedBorLastNm",        dataLoan.sCombinedBorLastNm );
            SetResult("sCombinedBorMidNm",         dataLoan.sCombinedBorMidNm );
            SetResult("sCombinedBorSsn",           dataLoan.sCombinedBorSsn );
            SetResult("sCombinedBorSuffix",        dataLoan.sCombinedBorSuffix );
            SetResult("sCombinedCoborFirstNm",     dataLoan.sCombinedCoborFirstNm );
            SetResult("sCombinedCoborLastNm",      dataLoan.sCombinedCoborLastNm );
            SetResult("sCombinedCoborMidNm",       dataLoan.sCombinedCoborMidNm );
            SetResult("sCombinedCoborSsn",         dataLoan.sCombinedCoborSsn );
            SetResult("sCombinedCoborSuffix",      dataLoan.sCombinedCoborSuffix );
            SetResult("sFHA2ndMAmt",               dataLoan.sFHA2ndMAmt_rep);
            SetResult("sFHA2ndMSrc",               dataLoan.sFHA2ndMSrc);
            SetResult("sFHAAmtPaid",               dataLoan.sFHAAmtPaid_rep);           
            SetResult("sFHAAmtPaidByCash",         dataLoan.sFHAAmtPaidByCash);
            SetResult("sFHAAmtPaidByOtherT",       dataLoan.sFHAAmtPaidByOtherT);
            SetResult("sFHAAssetAvail",            dataLoan.sFHAAssetAvail_rep);
            SetResult("sFHABBaseI",                dataLoan.sFHABBaseI_rep);
            SetResult("sFHABCaivrsNum",            dataLoan.sFHABCaivrsNum);
            SetResult("sFHABLpdGsa",               dataLoan.sFHABLpdGsa);
            SetResult("sFHABOI",                   dataLoan.sFHABOI_rep);
            SetResult("sFHACBaseI",                dataLoan.sFHACBaseI_rep);
            SetResult("sFHACCaivrsNum",            dataLoan.sFHACCaivrsNum);
            SetResult("sFHACLpdGsa",               dataLoan.sFHACLpdGsa);
            SetResult("sFHACOI",                   dataLoan.sFHACOI_rep);
            SetResult("sFHACashReserves",          dataLoan.sFHACashReserves_rep);
            SetResult("sFHACcPbb",                 dataLoan.sFHACcPbb_rep);
            SetResult("sFHACcPbb2",                dataLoan.sFHACcPbb_rep);
            SetResult("sFHACcPbs",                 dataLoan.sFHACcPbs_rep);
            SetResult("sFHACcTot",                 dataLoan.sFHACcTot_rep);
            SetResult("sFHAChildSupportPmt",       dataLoan.sFHAChildSupportPmt_rep);
            SetResult("sFHAConstructionT",         dataLoan.sFHAConstructionT);
            SetResult("sFHACreditAnalysisRemarks", dataLoan.sFHACreditAnalysisRemarks);
            SetResult("sFHACreditRating",          dataLoan.sFHACreditRating); 
            SetResult("sFHADebtInstallBal",        dataLoan.sFHADebtInstallBal_rep);
            SetResult("sFHADebtInstallPmt",        dataLoan.sFHADebtInstallPmt_rep);
            SetResult("sFHADebtLckd",              dataLoan.sFHADebtLckd);
            SetResult("sFHADebtPmtTot",            dataLoan.sFHADebtPmtTot_rep);
            SetResult("sFHADebtPmtTot2",           dataLoan.sFHADebtPmtTot_rep);
            SetResult("sFHADiscountPoints",        dataLoan.sFHADiscountPoints_rep);
            SetResult("sFHAEnergyEffImprov",       dataLoan.sFHAEnergyEffImprov_rep);
            SetResult("sFHAExcessContribution",    dataLoan.sFHAExcessContribution_rep);
            SetResult("sFHAFixedPmtToIRatio",      dataLoan.sFHAFixedPmtToIRatio_rep);
            SetResult("sFHAGiftFundAmt",           dataLoan.sFHAGiftFundAmt_rep);
            SetResult("sFHAGiftFundSrc",           dataLoan.sFHAGiftFundSrc);
            SetResult("sFHAGrossMonI",             dataLoan.sFHAGrossMonI_rep);
            SetResult("sFHAHouseVal",              dataLoan.sFHAHouseVal_rep);
            SetResult("sFHAHousingActSection",     dataLoan.sFHAHousingActSection);
            SetResult("sFHAImprovements",          dataLoan.sFHAImprovements_rep);
            SetResult("sFHAIncomeLckd",            dataLoan.sFHAIncomeLckd);
            SetResult("sFHALtv",                   dataLoan.sFHALtv_rep);
            SetResult("sFHALtv2",                  dataLoan.sFHALtvPurch_rep);
            SetResult("sFHALtvLckd",               dataLoan.sFHALtvLckd);
            SetResult("sFHAMAmt",                  dataLoan.sFHAMAmt_rep);
            SetResult("sFHAMBasis",                dataLoan.sFHAMBasis_rep);
            SetResult("sFHAMPmtToIRatio",          dataLoan.sFHAMPmtToIRatio_rep);
            SetResult("sFHAMinDownPmt",            dataLoan.sFHAMinDownPmt_rep);
            SetResult("sFHAMonthlyPmt",            dataLoan.sFHAMonthlyPmt_rep);
            SetResult("sFHANetRentalI",            dataLoan.sFHANetRentalI_rep);
            SetResult("sFHANonrealty",             dataLoan.sFHANonrealty_rep);
            SetResult("sFHANonrealtyLckd",         dataLoan.sFHANonrealtyLckd);
            SetResult("sFHAOtherDebtBal",          dataLoan.sFHAOtherDebtBal_rep);
            SetResult("sFHAOtherDebtPmt",          dataLoan.sFHAOtherDebtPmt_rep);
            SetResult("sFHAPmtFixedTot",           dataLoan.sFHAPmtFixedTot_rep);
            SetResult("sFHAPrepaidExp",            dataLoan.sFHAPrepaidExp_rep);
            SetResult("sFHAPro1stMPmt",            dataLoan.sFHAPro1stMPmt_rep);
            SetResult("sFHAPro2ndFinPmt",          dataLoan.sFHAPro2ndFinPmt_rep);
            SetResult("sFHAProGroundRent",         dataLoan.sFHAProGroundRent_rep);
            SetResult("sFHAProHazIns",             dataLoan.sFHAProHazIns_rep);
            SetResult("sFHAProHoAssocDues",        dataLoan.sFHAProHoAssocDues_rep);
            SetResult("sFHAProMIns",               dataLoan.sFHAProMIns_rep);
            SetResult("sFHAProMInsLckd", dataLoan.sFHAProMInsLckd);
            SetResult("sFHAProRealETx",            dataLoan.sFHAProRealETx_rep);
            SetResult("sFHAPurchPrice",            dataLoan.sFHAPurchPrice_rep);
            SetResult("sFHAPurchPrice2",           dataLoan.sFHAPurchPrice_rep);
            SetResult("sFHAPurchPriceSrcT",        dataLoan.sFHAPurchPriceSrcT);
            SetResult("sFHARatingAssetAdequacy",   dataLoan.sFHARatingAssetAdequacy); 
            SetResult("sFHARatingIAdequacy",       dataLoan.sFHARatingIAdequacy);
            SetResult("sFHARatingIStability",      dataLoan.sFHARatingIStability);
            SetResult("sFHAReqAdj",                dataLoan.sFHAReqAdj_rep);
            SetResult("sFHASellerContribution",    dataLoan.sFHASellerContribution_rep);
            SetResult("sFHAStatutoryInvestReq",    dataLoan.sFHAStatutoryInvestReq_rep);
            SetResult("sFHATotCashToClose",        dataLoan.sFHATotCashToClose_rep);
            SetResult("sFHAUnadjAcquisition",      dataLoan.sFHAUnadjAcquisition_rep);
            SetResult("sFfUfmip1003",              dataLoan.sFfUfmip1003_rep);
            SetResult("sFfUfmip1003Lckd",          dataLoan.sFfUfmip1003Lckd);
            SetResult("sFfUfmipFinanced",          dataLoan.sFfUfmipFinanced_rep);
            SetResult("sFfUfmipR",                 dataLoan.sFfUfmipR_rep);
            SetResult("sFinalLAmt",                dataLoan.sFinalLAmt_rep);
            SetResult("sLAmt",                     dataLoan.sLAmtCalc_rep);
            SetResult("sLT",                       dataLoan.sLT);
            SetResult("sMipPiaMon",                dataLoan.sMipPiaMon_rep);
            SetResult("sNoteIR",                   dataLoan.sNoteIR_rep);
            SetResult("sPresLTotHExp",             dataLoan.sPresLTotHExp_rep);
            SetResult("sPurchPrice6pc",            dataLoan.sPurchPrice6pc_rep);
            SetResult("sTermInYr",                 dataLoan.sTermInYr_rep);
            SetResult("sUfCashPd",                 dataLoan.sUfCashPd_rep);
            SetResult("sUfCashPdzzz2",             dataLoan.sUfCashPd_rep);
            SetResult("sFHA203kInvestmentRequiredPc", dataLoan.sFHA203kInvestmentRequiredPc_rep);
        }

    }

	public partial class FHACreditAnalysisPurchaseCombineService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FHACreditAnalysisPurchaseCombineServiceItem());
        }

	}
}
