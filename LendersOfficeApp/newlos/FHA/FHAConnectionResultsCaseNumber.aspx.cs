﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using System.Xml;
using System.Xml.Linq;

namespace LendersOfficeApp.newlos.FHA
{
    public partial class FHAConnectionResultsCaseNumber : BaseXsltPage
    {
        private string m_xsltFileLocation;

        protected override string XsltFileLocation
        {
            get { return m_xsltFileLocation; }
        }
        protected override bool IsXsltFromEmbeddedResource
        {
            get { return true; }
        }
        private Guid LoanID
        {
            get { return RequestHelper.LoanID; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void GenerateXmlData(XmlWriter writer)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAConnectionResultsCaseNumber));
            dataLoan.InitLoad();
            string xml = dataLoan.sFHACaseNumberResultXmlContent;
            if (xml != "")
            {
                XDocument xmlDoc = XDocument.Parse(xml);
                XElement rootElement;
                // Check if it's a case number assignment or a holds tracking request
                if (xmlDoc.Root.Name.LocalName == "MORTGAGEDATA")
                {
                    m_xsltFileLocation = "LendersOffice.ObjLib.FHAConnection.xslt.caseNumberAssignment.xslt";
                    rootElement = xmlDoc.Element("MORTGAGEDATA");
                }
                else if (xmlDoc.Root.Name.LocalName == "MULTICASEDATA")
                {
                    m_xsltFileLocation = "LendersOffice.ObjLib.FHAConnection.xslt.holdsTracking.xslt";
                    rootElement = xmlDoc.Element("MULTICASEDATA");
                }
                else
                {
                    Tools.LogError("XML root name not recognized!");
                    return;
                }

                var lqbElement = new XElement("LQB_DATA");
                lqbElement.Add(new XElement("ImageFileName", Tools.VRoot + "/images/logo_fha.gif"));
                lqbElement.Add(new XElement("PropertyType", dataLoan.sGseSpTFriendlyDisplay));
                rootElement.Add(lqbElement);

                string xmlString = xmlDoc.ToString();
                writer.WriteRaw(xmlString);

            }
            else
            {
                HasContent = false;
            }
        }
    }
}
