﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHAConnection.aspx.cs" Inherits="LendersOfficeApp.newlos.FHA.FHAConnection" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title>FHA Connection B2G</title>
<style type="text/css">

fieldset
{
    border: solid 1px black;
    padding-bottom: 0.5em;
    margin-bottom: 0.5em;
}
fieldset legend
{
    margin-left: 1em;
    margin-bottom: 0.5em;
    font-weight: bold;
}

h1, h3
{
    margin: 0em; /* Otherwise it'll look funny in IE7 */
}

input
{
    padding: 0.1em;
    margin: 0.15em;
}
input[type="text"],
input[type="password"]
{
    width: 8em;
}
input[type="checkbox"],
input[type="radio"]
{
    width: 1em;
}
select
{
    padding: 0em;
    margin: 0.25em;    
}

.alignright
{
    text-align: right;
}
.placeholder
{
    visibility: hidden;
}
.textboxmimic
{
    display: inline-block;
    text-align: left;
    width: 8.5em;
    padding-left: 0.25em;
}
.fillwidth
{
    width: 100%;
    margin: 0;
    padding: 0;
    border-spacing: 0;
    border-collapse: collapse;
    table-layout: fixed;
}

.contentSection
{
    border-collapse: collapse;
    width: 100%;
    padding: 0;
    margin: 0;
}

.grid_50
{
    width: 50%;    
}
.grid_33
{
    width: 33%;
}
.grid_25
{
    width: 25%;
}

.Validator
{
	display: none;
    position: relative;
    left: -0.5em;
}

.FieldLabel
{
    padding: 0.25em;
    margin: 0em;
}

label.FieldLabel
{
    display: inline-block;
}
label.TopLabel
{
    width:inherit;
    display: block;
    padding-bottom: 0em;
}

#subjectPropertyAddressSection label.InlineLabel,
#borrowerInfoSection label.InlineLabel,
#authenticationSection label.InlineLabel,
#holdsTracking_BorrowerInfoSection label.InlineLabel,
#holdsTracking_GeneralInfoSection label.InlineLabel,
#caivrs_GeneralInfoSection label.InlineLabel,
#caseQuery_inputFieldOfficeSection label.InlineLabel
{
    width: 6.5em;
}

#generalInfoSection label.InlineLabel,
#asRequiredSection label.InlineLabel,
#complianceInspectionFieldsSection label.InlineLabel
{
    width: 11em;
}

#WaitMsg
{
    
    font-weight:normal;
    padding: 5px;
    border: solid 3px black;
    
    width: 190px;
    height: 40px;
    
    position:absolute;
    z-index: 900;
    top: 45%;
    left: 30%;
    display: none;
    
    background-color: #F5F5F5;
}

#sFHALenderHasThisApplicationLabel
{
    background-color: Yellow;
    vertical-align: bottom;
    position: relative;
    top: -1px;
}

#sSpStNum
{
    width: 60px;
}

#sSpStUnit
{
    width: 25px;
}

#sSpStDir
{
    width: 50px;
}

#sSpStName
{
    width: 100px;
}

#sSpStType
{
    width: 50px;
}

#sSpStPostDir
{
    width: 50px;
}

input.has-input-error {
    background: url("../../images/error_icon.gif") no-repeat right;
}

</style>

</head>

<body class="RightBackground">
<script type="text/javascript">
<!--

// To add a new FHAC method:
// HTML stuff. Try to follow the naming convention.
// Modify showSelectedSection
// Add a change_yourNewMethod function
// Modify do_change to use it
// Add a validate_yourNewMethod function
// Modify fhac_validate to use it

// Override f_saveMe to prevent refreshing
//function f_saveMe() {
//    return saveMe(false);
//}

    function _init() {
        lockField(document.getElementById('sEstCloseDLckd'), 'sEstCloseD');
        lockField(document.getElementById('sFhaSponsoredOriginatorEinLckd'), 'sFHASponsoredOriginatorEIN');
        toggleShowParsedAddressSection();
    }

$(function () {
    // These lists determine what elements are shown according to appropriate the case processing request type
    var allSections = [
        // "#processingRequestSection"
        "#subjectPropertyAddressSection",
        "#borrowerInfoSection",
        "#holdsTracking_BorrowerInfoSection",
        "#activeLoanAppSection",
        "#generalInfoSection",
        "#caivrs_GeneralInfoSection",
        "#holdsTracking_GeneralInfoSection",
        "#asRequiredSection",
        "#complianceInspectionFieldsSection",
        "#lenderNotesSection",
        // "#authenticationSection"
        "#sAgencyCaseNumSection",
        ".updateExistingCase_refinanceAuthorizationNumSection",
        "#caseQuery_inputFieldOfficeSection"
    ];
    var defaultSections = [
        "#subjectPropertyAddressSection",
        "#borrowerInfoSection",
        "#activeLoanAppSection",
        "#generalInfoSection",
        "#asRequiredSection",
        "#complianceInspectionFieldsSection",
        "#lenderNotesSection"
    ];
    
    var updateExistingCaseSections = [
        "#sAgencyCaseNumSection",
        ".updateExistingCase_refinanceAuthorizationNumSection"
    ];
    
    var holdsTrackingSections = [
        "#holdsTracking_BorrowerInfoSection",
        "#holdsTracking_GeneralInfoSection"
    ];
    
    var caseQuerySections = [
        "#sAgencyCaseNumSection",
        "#caseQuery_inputFieldOfficeSection"
    ];
    
    var caivrsSections = [
        "#borrowerInfoSection",
        "#caivrs_GeneralInfoSection"
    ];
    
    var showSelectedSection = function(sectionNumber) {
        $.each(allSections, function(i, item) {
            $(item).hide();
        });
        if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.NewCaseNumberAssignment) %>) {
            // Spec 4.1
            // Show default
            $.each(defaultSections, function(i, item) {
                $(item).show();
            });
        } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.UpdateAnExistingCase) %>) {
            // Spec 5.1:
            // Show default
            // Show FHA Case Number and Refinance Authorization Number
            $.each(defaultSections.concat(updateExistingCaseSections), function(i, item) {
                $(item).show();
            });
        } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.HoldsTracking) %>) {
            // Spec 6.1:
            // Show Case Processing Request Type, FHA Approval Lists
            // Show Borrower first name, middle name, last name
            // General information
                // Show Lender ID
            // Show Authentication
            // Hide everything else
            $.each(holdsTrackingSections, function(i, item) {
                $(item).show();
            });
        } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.CaseQuery) %>) {
            // Spec 7.1
            // Show Case Processing Request Type, FHA Approval Lists
            // Show FHA Case Number
            // Show Field Office
            // Show Authentication
            // Hide everything else
            $.each(caseQuerySections, function(i, item) {
                $(item).show();
            });
        } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.CAIVRSAuthorization) %>) {
            // Spec 8.1
            // Show Case Processing Request Type, FHA Approval Lists
            // Show Borrower Information section
            // General information
                // Show LenderID
                // Show Agency
            $.each(caivrsSections, function(i, item) {
                $(item).show();
            });
            $('#aBDobCell').hide();
            $('#aCDobCell').hide();
            $('.extraBorrowerDOB').hide();
        }
    };
    
    $('#SubmitToFHACBtn').click(click_submit);
    
    $('input').keyup(on_input_change);
    $('input[type=radio]').change(on_input_change);
    $('select').change(on_input_change);
    
    // Only allow digits in sFHAADPCode
    $('#sFHAADPCode').keypress(function (e) {
        // Prevent users from entering invalid characters (we should still validate)
        var digitRegex = /^[0-9]$/i;
        if (!digitRegex.test(String.fromCharCode(e.which))) {
            return false;
        }
        return true;
    });
    
    // Field office list may change due to a change in state or zip
    $('#sSpState').change(retrieve_FieldOffices);

    $("input[name='sFhaLenderIdT']").change(change_sFhaLenderIdT);
    change_sFhaLenderIdT();

    $('#sFhaSponsoredOriginatorEinLckd').change(function() {
        refreshCalculation();

        if ($("input[name='sFhaLenderIdT']:checked").prop('id') === "sFhaLenderIdT_SponsoredOriginatorEIN") {
            validateSponsoredOriginatorEin(function(fieldVal) { return fieldVal != ''});
        }

        if (this.checked) {
            // Remove the readonly background color but keep the validation icon
            // if this value hasn't been specified.
            $('#sFHASponsoredOriginatorEIN').css('background-color', 'white');
        }
    });
    
    $('#CaseProcessingRequestType').change(change_CaseProcessingRequestType);
    
    // Show the default sections on page load
    var selectedSection = $('#CaseProcessingRequestType option:selected').val();
    showSelectedSection(selectedSection);
    
    // Show fields based on loan purpose, loan type, etc
    var bValid = fhac_validate(selectedSection);
    do_change(selectedSection, bValid);
});


    function manuallyEditAddressClicked() {
        var manuallyEdit = document.getElementById('sUseParsedAddressFieldsForIntegrations');

        if (manuallyEdit.checked)
        {
            var args = { UnparsedAddress: $('#sSpAddr').val() };
            var result = gService.loanedit.call('ParseAddress', args);

            if (result && result.value)
            {
                if (result.value["Error"])
                {
                    alert(result.value["Error"]);
                    $('#sSpStNum').val('');
                    $('#sSpStUnit').val('');
                    $('#sSpStDir').val('');
                    $('#sSpStName').val('');
                    $('#sSpStType').val('');
                    $('#sSpStPostDir').val('');
                }
                else
                {
                    $('#sSpStNum').val(result.value["HouseNumber"]);
                    $('#sSpStUnit').val(result.value["Unit"]);
                    $('#sSpStDir').val(result.value["PreDirectional"]);
                    $('#sSpStName').val(result.value["StreetName"]);
                    $('#sSpStType').val(result.value["Type"]);
                    $('#sSpStPostDir').val(result.value["PostDirectional"]);
                }
            }
        }

        toggleShowParsedAddressSection();
    }

    function toggleShowParsedAddressSection() {
        var manuallyEdit = document.getElementById('sUseParsedAddressFieldsForIntegrations');

        if (manuallyEdit.checked)
        {
            $('.ParsedAddressSection').show();
        }
        else
        {
            $('.ParsedAddressSection').hide();
        }
    }

// **********
// SUBMISSION
// **********
var click_submit = function (e) {
    PolyShouldShowConfirmSave(isDirty(), submitToFHAC, saveMe);
};

function submitToFHAC(){
    var requestType = $('#CaseProcessingRequestType').val();
        if (requestType === '0' /*New Case Number Assignment*/ || requestType === '1' /*Update an Existing Case*/) {
            if (!$('#sFhaLenderIdT_RegularFhaLender').is(':checked') && !$('#sFhaLenderIdT_SponsoredOriginatorEIN').is(':checked')) {
                alert('Please enter a valid Lender ID or Sponsored Originator EIN.');
                return false;
            }
        }

        // We don't have to check for validity, since the button should only be enabled
        // if the required fields are present

        // Disable all inputs, so that the user is not tempted to change anything while
        // they wait.
        $('input,textarea,select').prop('disabled', true);

        // Present a waiting message, to show that work is being done
        var $WaitMsg = $('#WaitMsg');
        $WaitMsg.css('display', 'block');
        $WaitMsg.css("top", (($(window).height() - $WaitMsg.outerHeight()) / 2) + 
                                                $(window).scrollTop() + "px");
        $WaitMsg.css("left", (($(window).width() - $WaitMsg.outerWidth())  / 2) + 
                                                $(window).scrollLeft() + "px");
        $WaitMsg.show();

        // Then, do this thing
        var clientID = null;
        if (document.getElementById("_ClientID") != null)
            clientID = document.getElementById("_ClientID").value;

        // Then, get all the args
        var args = getAllFormValues(clientID);
        args["sLId"] = ML.sLId;

        // Submit it.
        // ajax request
        callWebMethodAsync({
            type: "POST",
            url: "FHAConnectionService.aspx?method=SubmitToFHAC",
            data: JSON.stringify(args),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            async : true,
            success: function(msg, status){
                // msg.d
                if (msg.d.status_messages)
                {
                    var messages = JSON.parse(msg.d.status_messages);
                    alert( messages.join('\n') );
                }
                else
                {
                    var dest;
                    if (msg.d.destination) {
                        dest = "&dest=" + encodeURIComponent(msg.d.destination);
                    }
                    window.location = "FHAConnectionResults.aspx?loanid="+ <%= AspxTools.JsString(LoanID) %> + dest;
                }
            },
            error: function(msg, status, errorThrown){
                var d = msg.responseText;
                if (d) {
                    d = JSON.parse(d).Message;
                }
                alert('Submission failed. Error: Failed to import response. Please contact your LendingQB system administrator.'); 
            },
            complete: function(msg, status) {
            $('input,textarea,select').prop('disabled', false);
                $WaitMsg.hide();
            }
        });
}

var on_input_change = function(e) {
    var selectedSection = $('#CaseProcessingRequestType option:selected').val();
    var bValid = fhac_validate(selectedSection);
    do_change(selectedSection, bValid);
};

// Should happen on every change. Will validate the page, then determine if any fields need to
// be shown/hidden. Enables/disables the submit button
var do_change = function(sectionNumber, bValid) {
    if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.NewCaseNumberAssignment) %>) {
        change_NewCaseNumberAssignment(bValid);
    } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.UpdateAnExistingCase) %>) {
        change_UpdateAnExistingCase(bValid);
    } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.HoldsTracking) %>) {
        change_HoldsTracking(bValid);
    } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.CaseQuery) %>) {
        change_CaseQuery(bValid);
    } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.CAIVRSAuthorization) %>) {
        change_CAIVRSAuthorization(bValid);
    }
    
    // Submission button
    $('#SubmitToFHACBtn').prop('disabled', !bValid);
};

// *************************************************************************
// Perform some action whenever something changes in a specific request type
// *************************************************************************
var change_NewCaseNumberAssignment = function(bValid) {
    // Determine visibility
    var sLPurposeT = $('#sLPurposeT').val();
    
    // HUD REO
    var hudREOVal = $('input:radio[name=sFHACasePreviouslySoldByHUDAsREO]:checked').val();
    if (sLPurposeT == <%= AspxTools.JsString(E_sLPurposeT.Purchase) %>) {
        // alert('Show HUD REO');
        $('#HUD_REO').show();
        $('#sLPurposeT_TypeOfCase').text('Forward Purchase');
    } else {
        $('#HUD_REO').hide();
    }
    
    // Refi Type
    if (sLPurposeT != <%= AspxTools.JsString(E_sLPurposeT.Purchase) %>) {
        // alert('Show Refi Type');
        $('#REFINANCE_TYPE').show();
        $('#sLPurposeT_TypeOfCase').text('Forward Refinance');
    } else {
        $('#REFINANCE_TYPE').hide();
    }
    
    // As Required section: only display if at least one of the following fields should be shown:
    // Streamline Refi
    // Prior FHA/REO
    // New Case subsection
    // 203k Consultant ID,
    // PUD/Condo
    // VA/RCV info
    
    var showAsRequired = false;
    
    // Streamline Refi
    if (sLPurposeT == <%= AspxTools.JsString(E_sLPurposeT.FhaStreamlinedRefinance) %>) {
        // alert('Show streamline refi');
        $('#STREAMLINE_REFINANCE').show();
        showAsRequired = true;
    } else {
        $('#STREAMLINE_REFINANCE').hide();
    }
    
    // Prior FHA/REO
    var isHUDREO = hudREOVal == '1';
    var sTotalScoreRefiT = $('#sTotalScoreRefiT').val(); // Refinance type
    var isPriorFHA = sTotalScoreRefiT == <%= AspxTools.JsString(E_sTotalScoreRefiT.FHAToFHANonStreamline) %>;
    if ((sLPurposeT == <%= AspxTools.JsString(E_sLPurposeT.Purchase) %> && isHUDREO) ||
        (sLPurposeT != <%= AspxTools.JsString(E_sLPurposeT.Purchase) %> && isPriorFHA)) {
        $('#PRIOR_FHA_REO').show();
        showAsRequired = true;
    } else {
        $('#PRIOR_FHA_REO').hide();
    }
    
    // New Case subsection
    if (sLPurposeT != <%= AspxTools.JsString(E_sLPurposeT.Purchase) %> && isPriorFHA) {
        // alert('Show new case subsection');
        $('#NEW_CASE').show();
        showAsRequired = true;
    } else {
        $('#NEW_CASE').hide();
    }
    
    // New Case Contact Name and Phone
    if (isPriorFHA) {
        // alert('Show contact name and phone');
        $('#NEW_CASE_CONTACT_NAME').show()
        $('#NEW_CASE_PHONE').show()
    } else {
        $('#NEW_CASE_CONTACT_NAME').hide()
        $('#NEW_CASE_PHONE').hide()
    }
    
    // 203k Consultant ID
    showAsRequired = true; // seems to be always true for now
    
    // PUD/Condo
    var sGseSpT = $('#sGseSpT').val();
    if (sGseSpT == <%= AspxTools.JsString(E_sGseSpT.Condominium) %> ||
        sGseSpT == <%= AspxTools.JsString(E_sGseSpT.DetachedCondominium) %> ||
        sGseSpT == <%= AspxTools.JsString(E_sGseSpT.HighRiseCondominium) %> ||
        sGseSpT == <%= AspxTools.JsString(E_sGseSpT.ManufacturedHomeCondominium) %> ||
        sGseSpT == <%= AspxTools.JsString(E_sGseSpT.PUD) %>) {
        showAsRequired = true;
        $('#PUD_CONDO').show();
    } else {
        $('#PUD_CONDO').hide();
    }
        
    
    // VA CRV info
    var sLT = <%= AspxTools.JsString(sLT) %>;
    if (sLT == <%= AspxTools.JsString(E_sLT.VA) %>) {
        showAsRequired = true;
        $('#VA_CRV_INFO').show()
    } else {
        $('#VA_CRV_INFO').hide()
    }
    
    if (showAsRequired) {
        $('#asRequiredSection').show();
    } else {
        $('#asRequiredSection').hide();
    }
    
};

var change_UpdateAnExistingCase = function(bValid) {
    change_NewCaseNumberAssignment(bValid);
};

var change_HoldsTracking = function(bValid) {

};

var change_CaseQuery = function(bValid) {

};

var change_CAIVRSAuthorization = function(bValid) {

};

// **********************************
// Validate FHAC request types
// **********************************
// Validate all the fields needed for an FHAC submission
// We can't use the ASP validators because we still want to allow people to save
var fhac_validate = function(sectionNumber) {
    var args = { IsValid: true };

    if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.NewCaseNumberAssignment) %>) {
        validate_NewCaseNumberAssignment(args);
    } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.UpdateAnExistingCase) %>) {
        validate_UpdateAnExistingCase(args);
    } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.HoldsTracking) %>) {
        validate_HoldsTracking(args);
    } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.CaseQuery) %>) {
        validate_CaseQuery(args);
    } else if (sectionNumber == <%= AspxTools.JsString(E_CaseProcessingRequestType.CAIVRSAuthorization) %>) {
        validate_CAIVRSAuthorization(args);
    }
    
    if (!validateTextField('FHAUserName', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateTextField('FHAPassword', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    
    return args.IsValid;
};

// *******************************
// Validate specific request types
// *******************************
// These will take in an args object that contains an IsValid field.
// They will set IsValid to false if any validation fails. (This is
// separate from the asp.net validators!)
var validate_NewCaseNumberAssignment = function(args) {
    $('.Validator').css('display', 'none');
    
    if (!validateAddressTextFieldWithConstruction(function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateTextField('sSpCity', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateDropDownList('sSpState', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateTextField('sSpZip', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateTextField('sSpCounty', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    
    if (!validateTextField('aBFirstNm', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateTextField('aBLastNm', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateTextField('aBSsn', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;

    if (!validateDropDownList('sFHA203kType', function(fieldVal) { return fieldVal != '0' /* N/A */ || $('#sFHAADPHousingProgramT').val() != '3' /* Improvements203k */ })) args.IsValid = false;
    
    if ($('#sFhaLenderIdT_RegularFhaLender').is(':checked')) {
        if (!validateTextField('sFHALenderIdCode', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    } else { validateTextField('sFHALenderIdCode', function() {return true;}); }//hide validation icon
    
    if ($('#sFhaLenderIdT_SponsoredOriginatorEIN').is(':checked') && !validateSponsoredOriginatorEin(function (fieldVal) { return fieldVal != ''})) {
        args.IsValid = false;
    }
};

var validate_UpdateAnExistingCase = function(args) {
    validate_NewCaseNumberAssignment(args);
    
    if (!validateTextField('sAgencyCaseNum', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
};

var validate_HoldsTracking = function(args) {
    if (!validateTextField('holdsTracking_aBFirstNm', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateTextField('holdsTracking_aBLastNm', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    
    if (!validateTextField('holdsTracking_sFHALenderIdCode', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    
};

var validate_CaseQuery = function(args) {
    if (!validateTextField('sAgencyCaseNum', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateDropDownList('caseQuery_sFHAFieldOfficeCode', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
};

var validate_CAIVRSAuthorization = function(args) {
    if (!validateTextField('aBFirstNm', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateTextField('aBLastNm', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    if (!validateTextField('aBSsn', function (fieldVal) { return fieldVal != ''})) args.IsValid = false;
    
};

// **************************
// Validate specific fields
// **************************
// valFxn is a predicate that returns true if the field successfully validated
var validateTextField = function(fieldId, valFxn) {
    var fieldVal = $('#' + fieldId).val();
    
    if (valFxn(fieldVal)) {
        $('#' + fieldId).css('background', 'white');
        //$('#' + fieldId + 'ValidatorIcon').css('display', 'none');
        return true;
    } else {
        $('#' + fieldId).css('background', 'white url("../../images/error_icon.gif") no-repeat right');
        //$('#' + fieldId + 'ValidatorIcon').css('display', 'inline');
        return false;
    }
}

var validateDropDownList = function(fieldId, valFxn) {
    var fieldVal = $('#' + fieldId + ' option:selected').val();
    
    if (valFxn(fieldVal)) {
        $('#' + fieldId + 'ValidatorIcon').css('display', 'none');
        return true;
    } else {
        $('#' + fieldId + 'ValidatorIcon').css('display', 'inline');
        return false;
    }
}
// 4/8/2014 tj - OPM 177711 - Add support for houses under construction
var validateAddressTextFieldWithConstruction = function (valFxn) {
    var sLPurposeT = $('#sLPurposeT').val();
    var sFHAConstCodeT = $('#sFHAConstCodeT').val();
    var isConstruction = (sLPurposeT === <%=AspxTools.JsString(E_sLPurposeT.Construct)%> || sLPurposeT === <%=AspxTools.JsString(E_sLPurposeT.ConstructPerm)%>)
            || (sFHAConstCodeT === <%=AspxTools.JsString(E_sFHAConstCodeT.Proposed)%> || sFHAConstCodeT === <%=AspxTools.JsString(E_sFHAConstCodeT.UnderConstruction)%>);
    
    if (isConstruction) {
        if (validateTextField('sSpAddr', valFxn)) {
            validateTextField('sSpLotNum', alwaysValid); validateTextField('sSpBlockPlatNum', alwaysValid);//Hide validation symbols
            return true;
        }
        var IsValid_sSpBlockPlatNum = validateTextField('sSpBlockPlatNum', valFxn);//Ensure this executes, even if sSpLotNum is invalid
        if (validateTextField('sSpLotNum', valFxn) && IsValid_sSpBlockPlatNum) {
            validateTextField('sSpAddr', alwaysValid);
            return true;
        }
        return false; //Show any validation symbols
    }
    else {
        validateTextField('sSpLotNum', alwaysValid); validateTextField('sSpBlockPlatNum', alwaysValid);//hide sSpLotNum,sSpBlockPlatNum validation symbols
        return validateTextField('sSpAddr', valFxn); 
    }
}

var validateSponsoredOriginatorEin = function (valFxn) {
    var sFHASponsoredOriginatorEIN = $('#sFHASponsoredOriginatorEIN');

    if (valFxn(sFHASponsoredOriginatorEIN.val())) {
        sFHASponsoredOriginatorEIN.removeClass('has-input-error');
        return true;
    } 

    sFHASponsoredOriginatorEIN.addClass('has-input-error');
    return false;
}

var alwaysValid = function(fieldVal) { return true; };

// Changing the request type should redirect the user. This way,
//   it'll prompt to save, refresh, etc. We pretty much just want
//   changing the dropdown to be akin to navigating to another page.
var change_CaseProcessingRequestType = function(e) {
    // Redirect to the selected request
    linkMe('FHAConnection.aspx', 'requestType=' + retrieveEventTarget(e).value );
    
    // Switch the dropdown back to the original selection in case something goes wrong. For example,
    // if the user hits cancel on the save prompt.
    $('#CaseProcessingRequestType').val($('#CurrentCaseProcessingRequestType').val());
};

var change_sFhaLenderIdT = function() {
    var selectedId = $("input[name='sFhaLenderIdT']:checked").prop('id');

    // Remove the required icon from the sponsored originator EIN input
    // if we're no longer using that ID.
    if (selectedId == 'sFhaLenderIdT_RegularFhaLender') {
        $('#sFHASponsoredOriginatorEIN').removeClass('has-input-error');
    }
}

// Getting the field office
var retrieve_FieldOffices = function() {
    var args = {
        sSpState : $('#sSpState').val()
    };
    var result = gService.loanedit.call('RetrieveFieldOffices', args);
    
    if (!result.error) {
        var regularDDL = $('#sFHAFieldOfficeCode');
        var caseQueryDDL = $('#caseQuery_sFHAFieldOfficeCode');
        
        // Clear out the field office dropdown
        regularDDL.empty();
        caseQueryDDL.empty();
        
        // Populate it with our retrieved offices
        var field_offices = JSON.parse(result.value.field_offices);
        
        for (var i = 0; i < field_offices.length; i++) {
            regularDDL.append($('<option />')
                      .text(field_offices[i].Key)
                      .val(field_offices[i].Value));
            caseQueryDDL.append($('<option />')
                        .text(field_offices[i].Key)
                        .val(field_offices[i].Value));
        }
    }
}
// -->
</script>
<form id="form1" runat="server">

<asp:HiddenField ID="CurrentCaseProcessingRequestType" runat="server"/>

<div style="padding: 0px; margin: 0px; width: 80.5em;">
    <h1 class="MainRightHeader">FHA Connection</h1>

    <%-- Time until I gave up and used tables: 2 hours. Func Spec for FHA Connection B2G Integration 05/10/12 --%>
    <table class="contentSection" id="processingRequestSection" runat="server"><tr><td>
        <table class="fillwidth">
            <tr>
                <td>
                    <label class="FieldLabel InlineLabel" for="CaseProcessingRequestType">
                        Case Processing Request Type
                    </label>
                    <asp:DropDownList ID="CaseProcessingRequestType" runat="server" NotForEdit></asp:DropDownList>
                </td>
                <td class="grid_50">
                    <label class="FieldLabel InlineLabel">FHA Approval Lists:</label>
                    <label class="FieldLabel InlineLabel">
                        <a href='https://www5.hud.gov/ecpcis/main/ECPCIS_List.jsp' target='_blank'>LDP</a>
                        /
                        <a href='https://www.sam.gov' target='_blank'>GSA</a>
                    </label>
                </td>
            </tr>
            <tr id="sAgencyCaseNumSection">
                <td>
                    <label class="FieldLabel InlineLabel" for="sAgencyCaseNum">
                        FHA Case Number
                    </label>
                    <asp:TextBox runat="server" ID="sAgencyCaseNum" Width="15em" />
                    <img id="sAgencyCaseNumValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                </td>
            </tr>
            <tr id="caseQuery_inputFieldOfficeSection">
                <td>
                    <label class="FieldLabel InlineLabel" for="caseQuery_sFHAFieldOfficeCode">Field Office</label>
                    <asp:DropDownList runat="server" id="caseQuery_sFHAFieldOfficeCode">
                        <asp:ListItem Value="" Text=""></asp:ListItem>
                    </asp:DropDownList>
                    <img id="caseQuery_sFHAFieldOfficeCodeValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                </td>
            </tr>
        </table>
    </td></tr></table>

    <table class="contentSection" id="subjectPropertyAddressSection" runat="server"><tr><td>
        <h3 class="FormTableSubheader">Subject Property Address</h3>
        <table class="fillwidth"><tr><td class="grid_50">
            <table class="fillwidth">
                <tr>
                    <td>
                        <label class="FieldLabel InlineLabel" for="sSpAddr">Street</label>
                        <asp:TextBox runat="server" ID="sSpAddr" width="27em"/>
                        <img id="sSpAddrValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel InlineLabel" for="sSpCity">City/State/Zip</label>
                        <asp:TextBox runat="server" id="sSpCity" style="width: 17.5em" />
                        <img id="sSpCityValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                        <ml:StateDropDownList runat="server" ID="sSpState" style="width: 4em"/>
                        <img id="sSpStateValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                        <ml:ZipcodeTextBox runat="server" id="sSpZip" onchange="on_input_change(this); retrieve_FieldOffices();" style="width: 2.75em" />
                        <img id="sSpZipValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel InlineLabel" for="sSpCounty">County</label>
                        <asp:TextBox runat="server" id="sSpCounty" style="width: 15em"/>
                        <img id="sSpCountyValidatorIcon" class="Validator" src="../../images/error_icon.gif" />

                        <label class="FieldLabel InlineLabel" for="sUnitsNum">No. of Units</label>
                        <asp:TextBox runat="server" id="sUnitsNum" style="width: 3.5em"/>
                    </td>
                </tr>
            </table>
        </td>
        <td class="grid_50">
            <table class="fillwidth">
                <tr>
                    <td>
                        <label class="FieldLabel InlineLabel" for="sSpMonthBuiltT">Mnth/Yr Built</label>
                        <asp:DropDownList runat="server" id="sSpMonthBuiltT" style="width: 4.5em"></asp:DropDownList>
                        /
                        <asp:TextBox runat="server" id="sYrBuilt" style="width:2.25em" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel InlineLabel" for="inputLot">Lot</label>
                        <asp:TextBox runat="server" id="sSpLotNum" />                      
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel InlineLabel" for="inputBlkPlat">Blk/Plat</label>
                        <asp:TextBox runat="server" id="sSpBlockPlatNum" />
                    </td>
                </tr>
            </table>
        </td></tr>
        </table>
    </td></tr>
        <tr>
            <td colspan="4">
                <asp:CheckBox type="checkbox" id="sUseParsedAddressFieldsForIntegrations" name="UseParsedAddressFieldsForIntegrations" runat="server" onchange="manuallyEditAddressClicked();"/>
                <label class="FieldLabel InlineLabel no-wrap" for="UseParsedAddressFieldsForIntegration">
                    Manually edit street address for submission.
                </label>
            </td>
        </tr>
        <tr class="ParsedAddressSection">
            <td colspan="2">
                <span class="FieldLabel">House Number</span>
                <asp:TextBox runat="server" ID="sSpStNum" />
                &nbsp;
                <span class="FieldLabel">Unit</span>
                <asp:TextBox runat="server" ID="sSpStUnit" />
                &nbsp;
                <span class="FieldLabel">Pre-Directional</span>
                <ml:ComboBox runat="server" ID="sSpStDir" />
            </td>
        </tr>
        <tr class="ParsedAddressSection">
            <td colspan="2">
                <span class="FieldLabel">Street Name</span>
                <asp:TextBox runat="server" ID="sSpStName" />
                &nbsp;
                <span class="FieldLabel">Type</span>
                <ml:ComboBox runat="server" ID="sSpStType" />
                &nbsp;
                <span class="FieldLabel">Post-Directional</span>
                <ml:ComboBox runat="server" ID="sSpStPostDir" />
            </td>
        </tr>
        <tr><td>
        <input type="checkbox" id="OverrideAddressValidation" name="OverrideAddressValidation" NotForEdit/>
        <label class="FieldLabel InlineLabel" for="OverrideAddressValidation" style="width: auto; vertical-align: bottom;">
            Override address validation.
        </label>
    </td></tr></table>
    
    <table class="contentSection" id="borrowerInfoSection" runat="server"><tr><td>
        <table class="fillwidth"><tr><td class="grid_50">
            <h3 class="FormTableSubheader">Borrower Information</h3>
            <table class="fillwidth">
                <tr>
                    <td colspan="2">
                        <label class="FieldLabel InlineLabel" for="aBFirstNm">First Name</label>
                        <asp:TextBox runat="server" id="aBFirstNm" Width="15em" />
                        <img id="aBFirstNmValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                    
                        <label class="FieldLabel InlineLabel" for="aBMidNm">Middle Name</label>
                        <asp:TextBox runat="server" id="aBMidNm" Width="7em" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label class="FieldLabel InlineLabel" for="aBLastNm">Last Name</label>
                        <asp:TextBox runat="server" id="aBLastNm" Width="15em" />
                        <img id="aBLastNmValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                        
                        <label class="FieldLabel InlineLabel" for="aBSuffix">Suffix</label>
                        <ml:ComboBox id="aBSuffix" runat="server" Width="5em"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel InlineLabel" for="aBSsn">SSN</label>
                        <ml:SSNTextBox runat="server" id="aBSsn" preset="ssn" />
                        <img id="aBSsnValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                    </td>
                    <td class="alignright" id="aBDobCell">
                        <label class="FieldLabel InlineLabel" for="aBDob">DOB</label>
                        <ml:DateTextBox runat="server" id="aBDob" width="6em" />
                    </td>
                </tr>
            </table>
        </td>
        <td class="grid_50">
            <h3 class="FormTableSubheader">Co-Borrower Information</h3>
            <table class="fillwidth">
                <tr>
                    <td colspan="2">
                        <label class="FieldLabel InlineLabel" for="aCFirstNm">First Name</label>
                        <asp:TextBox runat="server" id="aCFirstNm" Width="15em" />
                    
                        <label class="FieldLabel InlineLabel" for="aCMidNm">Middle Name</label>
                        <asp:TextBox runat="server" id="aCMidNm" Width="7em" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label class="FieldLabel InlineLabel" for="aCLastNm">Last Name</label>
                        <asp:TextBox runat="server" id="aCLastNm" Width="15em" />
                        
                        <label class="FieldLabel InlineLabel" for="aCSuffix">Suffix</label>
                        <ml:ComboBox id="aCSuffix" runat="server" Width="5em"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="FieldLabel InlineLabel" for="aCSsn">SSN</label>
                        <ml:SSNTextBox runat="server" id="aCSsn" preset="ssn" />
                    </td>
                    <td class="alignright" id="aCDobCell">
                        <label class="FieldLabel InlineLabel" for="aCDob">DOB</label>
                        <ml:DateTextBox runat="server" id="aCDob" width="6em" />
                    </td>
                </tr>
            </table>
        </td></tr></table>
    
        <asp:Repeater id="ExtraBorrowerTable" runat="server">
            <ItemTemplate>
                <table class="fillwidth"><tr><td class="grid_50">
                    <h3 class="FormTableSubheader">
                        App <%# AspxTools.HtmlString( ((ExtraBorrowerInfo)Container.DataItem).SequenceNumber.ToString() ) %> Borrower Information
                    </h3>
                    <table class="fillwidth">
                        <tr>
                            <td>
                                <label class="FieldLabel InlineLabel">Name</label>
                                <%# AspxTools.HtmlString( ((ExtraBorrowerInfo)Container.DataItem).BorrName ) %>
                            </td>
                            <td class="extraBorrowerDOB alignright">
                                <label class="FieldLabel InlineLabel">DOB</label>
                                <span class="textboxmimic">
                                    <%# AspxTools.HtmlString( ((ExtraBorrowerInfo)Container.DataItem).BorrDOB ) %>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="FieldLabel InlineLabel">SSN</label>
                                <%# AspxTools.HtmlString( ((ExtraBorrowerInfo)Container.DataItem).BorrSSN ) %>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="grid_50">
                    <h3 class="FormTableSubheader">
                        App <%# AspxTools.HtmlString( ((ExtraBorrowerInfo)Container.DataItem).SequenceNumber.ToString() ) %> Co-borrower Information
                    </h3>
                    <table class="fillwidth">
                        <tr>
                            <td>
                                <label class="FieldLabel InlineLabel">Name</label>
                                <%# AspxTools.HtmlString( ((ExtraBorrowerInfo)Container.DataItem).CoborrName ) %>
                            </td>
                            <td class="extraBorrowerDOB alignright">
                                <label class="FieldLabel InlineLabel">DOB</label>
                                <span class="textboxmimic">
                                    <%# AspxTools.HtmlString( ((ExtraBorrowerInfo)Container.DataItem).CoborrDOB ) %>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="FieldLabel InlineLabel">SSN</label>
                                <%# AspxTools.HtmlString( ((ExtraBorrowerInfo)Container.DataItem).CoborrSSN ) %>
                            </td>
                        </tr>
                    </table>
                </td></tr></table>
            </ItemTemplate>
        </asp:Repeater>
    
    </td></tr></table>
    
    <table class="contentSection" id="holdsTracking_BorrowerInfoSection" runat="server"><tr><td>
        <h3 class="FormTableSubheader">Borrower Information</h3>
        <table class="fillwidth"><tr><td class="grid_50">
            <table class="fillwidth">
                <tr>
                    <td colspan="2">
                        <label class="FieldLabel InlineLabel" for="holdsTracking_aBFirstNm">First Name</label>
                        <asp:TextBox runat="server" id="holdsTracking_aBFirstNm" Width="15em" />
                        <img id="holdsTracking_aBFirstNmValidatorIcon" class="Validator" src="../../images/error_icon.gif" />

                        <label class="FieldLabel InlineLabel" for="holdsTracking_aBMidNm">Middle Name</label>
                        <asp:TextBox runat="server" id="holdsTracking_aBMidNm" Width="7em" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label class="FieldLabel InlineLabel" for="holdsTracking_aBLastNm">Last Name</label>
                        <asp:TextBox runat="server" id="holdsTracking_aBLastNm" Width="15em" />
                        <img id="holdsTracking_aBLastNmValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                    </td>
                </tr>
            </table>
        </td>
        <td></td>
        </tr></table>
    </td></tr></table>
    
    <div id="activeLoanAppSection" runat="server">
        <asp:CheckBox runat="server" id="sFHALenderHasThisApplication" />
        <label class="FieldLabel InlineLabel" for="sFHALenderHasThisApplication" id="sFHALenderHasThisApplicationLabel">
            I certify that the lender associated with this case number request has an active loan application for this property address and listed borrower(s).
        </label>
    </div>

    <table class="contentSection" id="generalInfoSection" runat="server"><tr><td>
        <h3 class="FormTableSubheader">General Information</h3>
        <table class="fillwidth">
            <colgroup>
                <col class="grid_33" />
                <col class="grid_33" />
                <col class="grid_33" />
            </colgroup>
            <tr>
                <td>
                    <label class="FieldLabel InlineLabel" for="sFHAFieldOfficeCode">Field Office</label>
                    <asp:DropDownList runat="server" id="sFHAFieldOfficeCode" ></asp:DropDownList>
                </td>
                <td>
                    <label class="FieldLabel InlineLabel" for="sLenderCaseNum">Lender Case Ref</label>
                    <asp:TextBox runat="server" id="sLenderCaseNum" Width="12.5em"/>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <label class="FieldLabel InlineLabel">
                        <a href="../Forms/Loan1003.aspx?pg=2&loanid=<%= AspxTools.HtmlString(LoanID.ToString()) %>&appid=<%= AspxTools.HtmlString(ApplicationID.ToString()) %>#ToBeCompletedByLoanOriginator">
                            Loan Officer
                        </a>
                    </label>
                    <ml:EncodedLabel runat="server" ID="LoanOriginatorName"></ml:EncodedLabel>
                    <br />
                    <label class="FieldLabel InlineLabel">
                        <a href="../Forms/Loan1003.aspx?pg=2&loanid=<%= AspxTools.HtmlString(LoanID.ToString()) %>&appid=<%= AspxTools.HtmlString(ApplicationID.ToString()) %>#ToBeCompletedByLoanOriginator">
                            LO NMLS ID
                        </a>
                    </label>
                    <ml:EncodedLabel runat="server" ID="LoanOriginatorNMLS_ID"></ml:EncodedLabel>
                </td>

                <td>
                    <span class="FieldLabel InlineLabel" style="display:inline-block; width: 11em">
                        <input type="radio" id="sFhaLenderIdT_RegularFhaLender" name="sFhaLenderIdT" value="0" runat="server" />
                        Lender ID
                    </span>
                    <asp:TextBox runat="server" id="sFHALenderIdCode" />
                    <br />
                    <span class="FieldLabel InlineLabel" style="display:inline-block; width: 14.5em">
                        <input type="radio" id="sFhaLenderIdT_SponsoredOriginatorEIN" name="sFhaLenderIdT" value="1" runat="server" />
                        Sponsored Originator EIN
                    </span>
                    <asp:TextBox runat="server" ID="sFHASponsoredOriginatorEIN" />
                    <asp:CheckBox runat="server" ID="sFhaSponsoredOriginatorEinLckd" />
                </td>
                <td>
                    <input type="text" class="placeholder" />
                    <br />
                    <label class="FieldLabel InlineLabel" for="sFHASponsorAgentIdCode" style="width:10.5em;">Sponsor/Agent ID</label>
                    <asp:TextBox runat="server" id="sFHASponsorAgentIdCode"  />
                </td>
            </tr>

            <tr>
                <td>
                    <label class="FieldLabel InlineLabel" for="sFHACaseT">Case Type</label>
                    <asp:DropDownList runat="server" id="sFHACaseT"></asp:DropDownList>
                </td>
                <td>
                    <label class="FieldLabel InlineLabel" for="sFHAConstCodeT">Construction Code</label>
                    <asp:DropDownList runat="server" id="sFHAConstCodeT" ></asp:DropDownList>
                </td>
                <td>
                    <label class="FieldLabel InlineLabel" for="sFHAProcessingT" style="width:10.5em;">Processing Type</label>
                    <asp:DropDownList runat="server" id="sFHAProcessingT" Width="14.5em"></asp:DropDownList>
                </td>
            </tr>
    
            <tr>
                <td>
                    <label class="FieldLabel InlineLabel" for="sTerm">Loan Term (mnths)</label>
                    <asp:TextBox runat="server" id="sTerm" Width="2.5em" />
                </td>
                <td colspan="2">
                    <label class="FieldLabel InlineLabel" for="sFHAFinT">Financing Type</label>
                    <asp:DropDownList runat="server" id="sFHAFinT" ></asp:DropDownList>
                </td>
            </tr>
            
            <tr>
                <td colspan="3">
                    <label class="FieldLabel InlineLabel" for="sFHAProgramId">Program ID</label>
                    <asp:DropDownList runat="server" id="sFHAProgramId" ></asp:DropDownList>
                </td>
            </tr>
    
            <tr>
                <td colspan="3">
                    <fieldset>
                        <legend>ADP Code Criteria</legend>
            
                        <table class="fillwidth">
                            <colgroup>
                                <col class="grid_33" />
                                <col class="grid_33" />
                                <col class="grid_33" />
                            </colgroup>
                            <tr>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sFinMethT">Amort. Type</label>
                                    <asp:DropDownList runat="server" id="sFinMethT" ></asp:DropDownList>
                                </td>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sFHAADPHousingProgramT">Housing Program</label>
                                    <asp:DropDownList runat="server" id="sFHAADPHousingProgramT" ></asp:DropDownList>
                                </td>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sGseSpT">Property Type</label>
                                    <asp:DropDownList runat="server" id="sGseSpT" ></asp:DropDownList>
                                </td>
                            </tr>

                            </tr>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sFHAADPSpecialProgramT">Special Program</label>
                                    <asp:DropDownList runat="server" id="sFHAADPSpecialProgramT" ></asp:DropDownList>
                                </td>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sBuydown">Buydown</label>
                                    <asp:RadioButtonList ID="sBuydown" Runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="0">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sFHAADPPrincipalWriteDownT">Principal Write-Down</label>
                                    <asp:DropDownList runat="server" id="sFHAADPPrincipalWriteDownT" ></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
    
            <tr>
                <td colspan="3">
                    <label class="FieldLabel InlineLabel" for="sFHAADPCode">ADP Code</label>
                    <asp:TextBox runat="server" id="sFHAADPCode" Width="4em" />
                    <a href='https://entp.hud.gov/sfnw/public/catinfo.cfm' target='_blank'>lookup</a>
                </td>
            </tr>
    
            <tr>
                <td>
                    <label class="FieldLabel InlineLabel" for="sLPurposeT">Loan Purpose</label>
                    <asp:DropDownList runat="server" id="sLPurposeT" Width="13em" ></asp:DropDownList>
                </td>
                <td>
                    Type of Case:
                    <ml:EncodedLabel runat="server" id="sLPurposeT_TypeOfCase"></ml:EncodedLabel>
                </td>
                <td></td>
            </tr>
    
            <tr id="HUD_REO">
                <td colspan="3">
                    <span class="FieldLabel InlineLabel" for="sFHACasePreviouslySoldByHUDAsREO">Was this case previously sold by HUD as Real Estate Owned?</span>
                    <asp:RadioButtonList ID="sFHACasePreviouslySoldByHUDAsREO" Runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Value="1">Yes</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
    
            <tr id="REFINANCE_TYPE">
                <td colspan="3">
                    <label class="FieldLabel InlineLabel" for="sTotalScoreRefiT">Refinance Type</label>
                    <asp:DropDownList runat="server" id="sTotalScoreRefiT" Width="13em"></asp:DropDownList>
                </td>
            </tr>
        </table>
    </td></tr></table>
    
    <table class="contentSection" id="caivrs_GeneralInfoSection" runat="server"><tr><td>
        <h3 class="FormTableSubheader">General Information</h3>
        <table class="fillwidth">
            <tr>
                <td>
                    <label class="FieldLabel InlineLabel">Lender ID</label>
                    <asp:TextBox runat="server" id="caivrs_sFHALenderIdCode" />
                    <asp:TextBox runat="server" id="caivrs_sVALenderIdCode" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="FieldLabel InlineLabel" for="sFHAAgencyT">Agency</label>
                    <asp:DropDownList runat="server" id="sFHAAgencyT" />
                </td>
            </tr>
        </table>
    </td></tr></table>
    
    <table class="contentSection" id="holdsTracking_GeneralInfoSection" runat="server"><tr><td>
        <h3 class="FormTableSubheader">General Information</h3>
        <table class="fillwidth">
            <tr>
                <td>
                    <label class="FieldLabel InlineLabel" for="holdsTracking_sFHALenderIdCode">Lender ID</label>
                    <asp:TextBox runat="server" id="holdsTracking_sFHALenderIdCode" />
                    <img id="holdsTracking_sFHALenderIdCodeValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                </td>
            </tr>
        </table>
    </td></tr></table>
    
    <table class="contentSection" id="asRequiredSection" runat="server"><tr><td>
        <h3 class="FormTableSubheader">As Required</h3>
        
        <table class="fillwidth">
            <colgroup>
                <col class="grid_25" />
                <col class="grid_25" />
                <col class="grid_25" />
                <col class="grid_25" />
            </colgroup>
            <tr> <%-- IE7 colspan workaround --%>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr id="STREAMLINE_REFINANCE">
                <%-- Display this iff sLPurposeT == FHA Streamline Refi --%>
                <td colspan="2">
                    <label class="FieldLabel InlineLabel" for="sFHAPurposeIsStreamlineRefiApprChoice">Streamline Refinance</label>
                    <asp:RadioButtonList ID="sFHAPurposeIsStreamlineRefiApprChoice" Runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Value="1">w/ appraisal</asp:ListItem>
                        <asp:ListItem Value="0">w/o appraisal</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="updateExistingCase_refinanceAuthorizationNumSection">
                    <span class="FieldLabel InlineLabel" for="sFHARefiAuthNum">Refinance Authorization Number</span>
                </td>
                <td class="updateExistingCase_refinanceAuthorizationNumSection">
                    <asp:TextBox runat="server" ID="sFHARefiAuthNum" />
                </td>
            </tr>

            <tr>
                <td id="PRIOR_FHA_REO">
                    <fieldset>
                        <legend>Prior FHA/REO</legend>
                        
                        <table class="fillwidth">
                            <tr>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sFHAPreviousCaseNum">Case number of previous case</label>
                                    <asp:TextBox runat="server" id="sFHAPreviousCaseNum" width="17.5em" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td colspan="3" id="NEW_CASE">
                    <fieldset>
                        <legend>New Case</legend>
        
                        <table class="fillwidth">
                            <colgroup>
                                <col class="grid_33" />
                                <col class="grid_33" />
                                <col class="grid_33" />
                            </colgroup>
                            <tr>
                                <td class="FieldLabel">
                                    <label class="FieldLabel TopLabel" for="sEstCloseD">Estimated Closing Date</label>
                                    <ml:DateTextBox runat="server" ID="sEstCloseD"></ml:DateTextBox>
                                    <input type="checkbox" runat="server" id="sEstCloseDLckd" onchange="lockField(this, 'sEstCloseD'); refreshCalculation();" />
                                    Lock
                                </td>
                                <td id="NEW_CASE_CONTACT_NAME">
                                    <label class="FieldLabel TopLabel" for="sFHANewCaseContactNm">Contact Name</label>
                                    <asp:TextBox runat="server" id="sFHANewCaseContactNm" width="12em" />
                                </td>
                                <td id="NEW_CASE_PHONE">
                                    <label class="FieldLabel TopLabel" for="sFHANewCaseContactPhone">Phone</label>
                                    <ml:PhoneTextBox runat="server" preset="phone" id="sFHANewCaseContactPhone"  />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>

            <tr>
                <td>
                    <label class="FieldLabel InlineLabel" for="sFHA203kConsultantId">203k Consultant ID</label>
                    <asp:TextBox runat="server" Width="6em" id="sFHA203kConsultantId"  />
                </td>
                <td colspan="3">
                    &nbsp;&nbsp;<label class="FieldLabel InlineLabel" for="sFHA203kType">203k Type</label>
                    <asp:DropDownList runat="server" ID="sFHA203kType"></asp:DropDownList>
                    <img id="sFHA203kTypeValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                </td>
            </tr>

            <tr>
                <td colspan="4" id="PUD_CONDO">
                    <fieldset>
                        <legend>PUD/Condo</legend>
                        
                        <table class="fillwidth">
                            <colgroup>
                                <col class="grid_25" />
                                <col class="grid_25" />
                                <col class="grid_25" />
                            </colgroup>
                            <tr>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sFHAPUDCondoT">PUD/Condo Type</label>
                                    <asp:DropDownList runat="server" id="sFHAPUDCondoT" ></asp:DropDownList>
                                </td>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sProjNm">PUD/Condo Name</label>
                                    <asp:TextBox runat="server" width="12em" id="sProjNm"  />
                                </td>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sCpmProjectId">PUD/Condo ID</label>
                                    <asp:TextBox runat="server" Width="12em" id="sCpmProjectId"  />
                                </td>
                                <td>
                                    <label class="FieldLabel TopLabel" for="sFHAPUDSubmissionPhase">Submission/Phase</label>
                                    <asp:TextBox runat="server" Width="12em" id="sFHAPUDSubmissionPhase"  />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <label class="FieldLabel TopLabel" for="sFHAPUDSiteCondoT">Site Condo</label>
                                    <asp:DropDownList runat="server" id="sFHAPUDSiteCondoT" ></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>

            <tr id="VA_CRV_INFO">
                <td>
                    <label class="FieldLabel InlineLabel" for="sVACRVNum">VA CRV Number</label>
                    <asp:TextBox runat="server" Width="6em" id="sVACRVNum"  />
                </td>
                <td colspan="3">
                    <label class="FieldLabel InlineLabel" for="aVALAnalysisExpirationD">VA CRV Exp Date</label>
                    <ml:DateTextBox runat="server" id="aVALAnalysisExpirationD"  />
                </td>
            </tr>
        </table>
        
    </td></tr></table>

    <table class="contentSection" id="complianceInspectionFieldsSection" runat="server"><tr><td>
        <h3 class="FormTableSubheader">Compliance Inspection Fields</h3>

        <table class="fillwidth">
            <colgroup>
                <col class="grid_25" />
                <col class="grid_25" />
                <col class="grid_25" />
                <col class="grid_25" />
            </colgroup>
            <tr> <%-- IE7 colspan workaround --%>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4">
                    <label class="FieldLabel InlineLabel" for="sFHAComplianceInspectionAssignmentT">Assignment Choice</label>
                    <asp:DropDownList runat="server" id="sFHAComplianceInspectionAssignmentT" Width="8.5em" ></asp:DropDownList>

                    <label class="FieldLabel InlineLabel" for="sFHAComplianceInspectionAssignmentID">Assignment ID</label>
                    <asp:TextBox runat="server" id="sFHAComplianceInspectionAssignmentID" />
                </td>
            </tr>
        </table>
        
    </td></tr></table>

    <table class="contentSection" id="lenderNotesSection" runat="server"><tr><td>
        <h3 class="FormTableSubheader">FHAC Lender Notes</h3>

        <asp:TextBox runat="server" ID="sFHALenderNotes" TextMode="Multiline" Rows="5" Width="65.5em"></asp:TextBox>
    </td></tr></table>

    <table class="contentSection" id="authenticationSection"><tr><td>
        <h3 class="FormTableSubheader">Authentication</h3>

        <table class="fillwidth">
            <tr>
                <td>
                    <label class="FieldLabel InlineLabel" for="FHAUserName" >User Name</label>
                    <asp:TextBox runat="server" ID="FHAUserName" NotForEdit />
                    <img id="FHAUserNameValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="FieldLabel InlineLabel" for="FHAPassword" >Password</label>
                    <asp:TextBox runat="server" TextMode="Password" ID="FHAPassword" NotForEdit />
                    <img id="FHAPasswordValidatorIcon" class="Validator" src="../../images/error_icon.gif" />
                </td>
            </tr>
        </table>
    </td></tr></table>

    <input type="button" id="SubmitToFHACBtn" value="Submit to FHAC" />
</div>

<div id="WaitMsg"> 
    <b>Please wait...</b>
    <br />
    <img src="../../images/status.gif" alt="loading" />
</div>

</form>

</body>

</html>
