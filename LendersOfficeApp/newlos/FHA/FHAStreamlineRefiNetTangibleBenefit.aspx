﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHAStreamlineRefiNetTangibleBenefit.aspx.cs" Inherits="LendersOfficeApp.newlos.FHA.FHAStreamlineRefiNetTangibleBenefit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>FHA Streamline Refinance / Net Tangible Benefit Worksheet</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        label { display: inline-block; font-weight: bold; }
        .FormTableSubheader { padding: 5px; }
        .StreamlineRefi { width: 550px; }
        .StreamlineRefi label { width: 300px; padding-left: 5px; }
        .StreamlineRefi table { display: inline; overflow: hidden; height: 20px; }
        .RadioButtonList label { width: 30px; }
        .NetTangibleBenefit { width: 550px; }
        .NetTangibleBenefit .FormTableSubheader { margin-bottom: 5px; }
        .NetTangibleBenefit .LeftColumn label { width: 120px; }
        .NetTangibleBenefit .RightColumn label { width: 200px; }
        .LeftColumn { float: left; padding-left: 5px;} 
        .RightColumn { float: left; padding-left: 20px; }
        .Container { width: 420px; border: solid 1px black; margin: 5px; padding-top: 5px; padding-bottom: 5px; }
        .PercentOverride { width: 90px !important; text-align: right; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="MainRightHeader">FHA Streamline Refinance / Net Tangible Benefit Worksheet</div>
    <div class="StreamlineRefi">
        <div class="FormTableSubheader">Streamline Refinance Without Appraisal</div>
        <div class="Container">
            <div>
                <label>a. Original Appraised Value (FHA Authorization Line 4I)</label>
                <ml:MoneyTextBox runat="server" ID="sOriginalAppraisedValue" ReadOnly="true"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>Original Loan Amount (FHA Authorization Line 4E)</label>
                <ml:MoneyTextBox runat="server" ID="sVaLoanAmtCurrentLoan" onchange="backgroundCalculation();"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>15 Year?</label>
                <asp:RadioButtonList runat="server" ID="sIsTermLTE15Yr" RepeatDirection="Horizontal" Enabled="false" CssClass="RadioButtonList" RepeatLayout="Flow">
                    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            
            <div>
                <label>UFMIP Financed?</label>
                <asp:RadioButtonList runat="server" ID="sFfUfMipIsBeingFinanced" RepeatDirection="Horizontal" Enabled="false" CssClass="RadioButtonList" RepeatLayout="Flow">
                    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            
            <div>
                <label>Previous Loan Endorsed on or before 5/31/2009?</label>
                <asp:RadioButtonList runat="server" ID="sProdIsLoanEndorsedBeforeJune09" RepeatDirection="Horizontal" Enabled="false" CssClass="RadioButtonList" RepeatLayout="Flow">
                    <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                    <asp:ListItem Text="No" Value="N"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="Container">
            <div>
                <label>Unpaid Principal Balance (Payoff Statement)</label>
                <ml:MoneyTextBox runat="server" ID="sSpLien" ReadOnly="true"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>30-day Interest on Old Loan (Payoff Statement)</label>
                <ml:MoneyTextBox runat="server" ID="s30DayInterestOldLoan" onchange="backgroundCalculation();"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>Total Payoff Balance</label>
                <ml:MoneyTextBox runat="server" ID="sFHATotalPayoffBalance" ReadOnly="true"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>FHA Allowable Closing Costs</label>
                <ml:MoneyTextBox runat="server" ID="sFHACcTot" onchange="backgroundCalculation();"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>Discount Points</label>
                <ml:MoneyTextBox runat="server" ID="sFHADiscountPoints" onchange="backgroundCalculation();"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>Pre-paids</label>
                <ml:MoneyTextBox runat="server" ID="sFHAPrepaidExp" onchange="backgroundCalculation();"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>b. Total</label>
                <ml:MoneyTextBox runat="server" ID="sFHATotalNewLoanCosts" ReadOnly="true"></ml:MoneyTextBox>
            </div>
        </div>
        <div class="Container">
            <div>
                <label>UFMIP Refund (FHA Authorization Line 4H)</label>
                <ml:MoneyTextBox runat="server" ID="sFHASalesConcessions" onchange="backgroundCalculation();"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>c. New Maximum Base Loan Amount</label>
                <ml:MoneyTextBox runat="server" ID="sFHANewMaximumBaseLAmt" ReadOnly="true"></ml:MoneyTextBox>
            </div>
        </div>
        <div class="Container">
            <div>
                <label>d. Amount Needed to Close</label>
                <ml:MoneyTextBox runat="server" ID="sFHAAmountNeededToClose" ReadOnly="true"></ml:MoneyTextBox>
            </div>
        </div>
        <div class="Container">
            <div>
                <label>Base Loan Amount</label>
                <ml:MoneyTextBox runat="server" ID="sLAmtCalc" ReadOnly="true"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>UFMIP</label>
                <span style="margin-left: -99px;">
                    <ml:PercentTextBox runat="server" ID="sFfUfmipR" ReadOnly="true" decimalDigits="6" class="PercentOverride"></ml:PercentTextBox>
                    <ml:MoneyTextBox runat="server" ID="sFfuFmip1003" ReadOnly="true"></ml:MoneyTextBox>
                </span>
            </div>
            
            <div>
                <label>e. Total Loan Amount</label>
                <ml:MoneyTextBox runat="server" ID="sFinalLAmt" ReadOnly="true"></ml:MoneyTextBox>
            </div>
        </div>
        <div class="Container">
            <div>
                <label>f. Loan to Value</label>
                <ml:PercentTextBox runat="server" ID="sFHALtvR" ReadOnly="true" class="PercentOverride"></ml:PercentTextBox>
            </div>
            
            <div>
                <label>Monthly MIP Factor</label>
                <ml:PercentTextBox runat="server" ID="sProMInsR" ReadOnly="true" decimalDigits="6" class="PercentOverride"></ml:PercentTextBox>
            </div>
        </div>
    </div>
    <div class="NetTangibleBenefit">
        <div class="FormTableSubheader">Net Tangible Benefit (Fixed Rate to Fixed Rate)</div>
        <div class="LeftColumn">
            <div>
                <label>Base Loan</label> 
                <ml:MoneyTextBox runat="server" ID="sLAmtCalc_b" ReadOnly="true"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>Total Loan Amount</label>
                <ml:MoneyTextBox runat="server" ID="sFinalLAmt_b" ReadOnly="true"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>Loan Term</label>
                <asp:TextBox runat="server" ID="sTerm" ReadOnly="true" class="PercentOverride"></asp:TextBox>
            </div>
            
            <div>
                <label>Interest Rate</label>
                <ml:PercentTextBox runat="server" ID="sNoteIR" ReadOnly="true" class="PercentOverride"></ml:PercentTextBox>
            </div>
            
            <div>
                <label>P&amp;I</label>
                <ml:MoneyTextBox runat="server" ID="sProThisMPmt" ReadOnly="true"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>MIP Factor</label>
                <ml:PercentTextBox runat="server" ID="sProMInsR_b" ReadOnly="true" decimalDigits="6" class="PercentOverride"></ml:PercentTextBox>
            </div>
            
            <div>
                <label>P&amp;I + MIP</label>
                <ml:MoneyTextBox runat="server" ID="sProThisMPmtAndMIns" ReadOnly="true"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>Monthly MIP</label>
                <ml:MoneyTextBox runat="server" ID="sProMIns" ReadOnly="true"></ml:MoneyTextBox>
            </div>
        </div>
        <div class="RightColumn">
            <div>
                <label>Total Current P&amp;I + Monthly MIP</label>
                <ml:MoneyTextBox runat="server" ID="sTotalCurrentPIAndMonMIP" onchange="backgroundCalculation();"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>Total Proposed P&amp;I + Monthly MIP</label>
                <ml:MoneyTextBox runat="server" ID="sProThisMPmtAndMIns_b" ReadOnly="true"></ml:MoneyTextBox>
            </div>
            
            <div>
                <label>Difference in Monthly Payment</label>
                <ml:PercentTextBox runat="server" ID="sFHAMPmtDiffPc" ReadOnly="true" class="PercentOverride"></ml:PercentTextBox>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
    </form>
</body>
</html>
