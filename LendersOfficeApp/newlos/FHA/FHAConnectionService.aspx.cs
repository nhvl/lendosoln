﻿namespace LendersOfficeApp.newlos.FHA
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.ObjLib.FHAConnection;
    using LendersOffice.Common;

    public partial class FHAConnectionServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHAConnectionServiceItem));
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "ParseAddress":
                    ParseAddress();
                    break;
                case "SubmitToFHAC":
                    SubmitToFHAC();
                    break;
                case "RetrieveFieldOffices":
                    RetrieveFieldOffices();
                    break;
            }
        }

        /// <summary>
        /// Parses an address string and sends the parts back to the page.
        /// </summary>
        private void ParseAddress()
        {
            var address = GetString("UnparsedAddress");

            if (string.IsNullOrEmpty(address))
            {
                return;
            }

            var parser = new CommonLib.Address();

            try
            {
                parser.ParseStreetAddress(address);
            }
            catch (ApplicationException)
            {
                SetResult("Error", "We could not parse the street address. Please manually fill out the individual address fields.");
                return;
            }

            SetResult("HouseNumber", parser.StreetNumber);
            SetResult("Unit", parser.AptNum);
            SetResult("PreDirectional", parser.sStreetDir);
            SetResult("StreetName", parser.StreetName);
            SetResult("Type", parser.StreetType);
            SetResult("PostDirectional", parser.sStPostDir);
        }

        private void SubmitToFHAC()
        {
            string username = GetString("FHAUserName");
            string password = GetString("FHAPassword");

            E_CaseProcessingRequestType requestType = (E_CaseProcessingRequestType)GetInt("CaseProcessingRequestType");
            switch (requestType)
            {
                case E_CaseProcessingRequestType.NewCaseNumberAssignment:
                    SubmitToFHAC_New(username, password);
                    break;
                case E_CaseProcessingRequestType.UpdateAnExistingCase:
                    SubmitToFHAC_Update(username, password);
                    break;
                case E_CaseProcessingRequestType.HoldsTracking:
                    SubmitToFHAC_HoldsTracking(username, password);
                    break;
                case E_CaseProcessingRequestType.CaseQuery:
                    SubmitToFHAC_CaseQuery(username, password);
                    break;
                case E_CaseProcessingRequestType.CAIVRSAuthorization:
                    SubmitToFHAC_CAIVRSAuthorization(username, password);
                    break;
                default:
                    break;
            }

            // The redirect to the results page will happen in the javascript.
        }

        private void SubmitToFHAC_New(string username, string password)
        {
            // See FHA Connection B2G Integration Spec 4.2, Submission Process, for details

            // 1. Save the FHA Connection page data. Done on the page.
            // 2. Send the data to FHAC B2G.
            // 3. Parse the B2G response and save the data to the loan file.
            // 4. Add an entry to the loan file's audit history.
            // 5. Take the user to the View Results page. Done on the page.

            bool IsOverrideAddressValidation = GetBool("OverrideAddressValidation");
            // Send the data to FHAC B2G
            var request = new CaseNumberAssignmentRequest(sLId, false, IsOverrideAddressValidation);
            var response = request.Submit(username, password);

            if (response.IsError)
            {
                var dict = ObsoleteSerializationHelper.JavascriptJsonSerialize(response.StatusMessages);
                SetResult("status_messages", dict);
            }
            else
            {
                SetResult("destination", "casenum");
                response.ApplyResultToLoan();
            }

            // Afterwards, take the user to the view results page.

        }

        private void SubmitToFHAC_Update(string username, string password)
        {
            // See FHA Connection B2G Integration Spec 5.2, Submission Process, for details

            // Do the same thing as the submission process for New Case Number Assignment,
            // but use "Update an Existing Case" in place of "new Case Number Assignment" in the
            // audit history's Audit Detail title and Event areas.

            Guid sLId = GetGuid("sLId");
            bool IsOverrideAddressValidation = GetBool("OverrideAddressValidation");
            // Send the data to FHAC B2G
            var request = new CaseNumberAssignmentRequest(sLId, true, IsOverrideAddressValidation);
            var response = request.Submit(username, password);

            if (response.IsError)
            {
                var dict = ObsoleteSerializationHelper.JavascriptJsonSerialize(response.StatusMessages);
                SetResult("status_messages", dict);
            }
            else
            {
                SetResult("destination", "casenum");
                response.ApplyResultToLoan();
            }

            // Afterwards, take the user to the view results page.
        }

        private void SubmitToFHAC_HoldsTracking(string username, string password)
        {
            // See FHA Connection B2G Integration Spec 6.2, Submission Process, for details

            // Send the data to FHAC B2G
            var request = new HoldsTrackingRequest(sLId);
            var response = request.Submit(username, password);

            if (response.IsError)
            {
                var dict = ObsoleteSerializationHelper.JavascriptJsonSerialize(response.StatusMessages);
                SetResult("status_messages", dict);
            }
            else
            {
                SetResult("destination", "casenum");
                response.ApplyResultToLoan();
            }

            // Afterwards, take the user to the view results page.
        }

        private void SubmitToFHAC_CaseQuery(string username, string password)
        {
            // See FHA Connection B2G Integration Spec 7.2, Submission Process, for details

            // Send the data to FHAC B2G
            var request = new CaseQueryRequest(sLId);
            var response = request.Submit(username, password);

            if (response.IsError)
            {
                var dict = ObsoleteSerializationHelper.JavascriptJsonSerialize(response.StatusMessages);
                SetResult("status_messages", dict);
            }
            else
            {
                SetResult("destination", "casequery");
                response.ApplyResultToLoan();
            }

            // Afterwards, take the user to the view results page.
        }

        private void SubmitToFHAC_CAIVRSAuthorization(string username, string password)
        {
            // See FHA Connection B2G Integration Spec 8.2, Submission Process, for details

            // Send the data to FHAC B2G
            var request = new CAIVRSAuthorizationRequest(sLId);
            var response = request.Submit(username, password);

            if (response.IsError)
            {
                var dict = ObsoleteSerializationHelper.JavascriptJsonSerialize(response.StatusMessages);
                SetResult("status_messages", dict);
            }
            else
            {
                SetResult("destination", "caivrs");
                response.ApplyResultToLoan();
            }

            // Afterwards, take the user to the view results page.
        }

        private void RetrieveFieldOffices()
        {
            string state = GetString("sSpState"); // get the state

            // Dummy ddl so we can reuse the Bind_sFHAFieldOfficeCode method
            DropDownList ddl = new DropDownList();
            Tools.Bind_sFHAFieldOfficeCode(ddl, state);

            // But we really only want the text + value
            var offices = new List<KeyValuePair<string, string>>();
            foreach (ListItem item in ddl.Items)
            {
                offices.Add(new KeyValuePair<string, string>(item.Text, item.Value));
            }

            var json = ObsoleteSerializationHelper.JavascriptJsonSerialize(offices);
            SetResult("field_offices", json);
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            // Selection
            E_CaseProcessingRequestType requestType = (E_CaseProcessingRequestType)GetInt("CurrentCaseProcessingRequestType");

            switch (requestType)
            {
                case E_CaseProcessingRequestType.NewCaseNumberAssignment:
                    Bind_NewCaseNumberAssignment(dataLoan, dataApp);
                    break;
                case E_CaseProcessingRequestType.UpdateAnExistingCase:
                    Bind_UpdateAnExistingCase(dataLoan, dataApp);
                    break;
                case E_CaseProcessingRequestType.HoldsTracking:
                    Bind_HoldsTracking(dataLoan, dataApp);
                    break;
                case E_CaseProcessingRequestType.CaseQuery:
                    Bind_CaseQuery(dataLoan, dataApp);
                    break;
                case E_CaseProcessingRequestType.CAIVRSAuthorization:
                    Bind_CAIVRSAuthorization(dataLoan, dataApp);
                    break;
                default:
                    throw new UnhandledEnumException(requestType);
            }

            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }

        private void Bind_NewCaseNumberAssignment(CPageData dataLoan, CAppData dataApp)
        {
            // FHA Case Number
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum");

            // Subject Property Address
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");
            dataLoan.sSpCounty = GetString("sSpCounty");
            dataLoan.sUnitsNum_rep = GetString("sUnitsNum");
            dataLoan.sSpMonthBuiltT = (E_sSpMonthBuiltT)GetInt("sSpMonthBuiltT");
            dataLoan.sYrBuilt = GetString("sYrBuilt");
            dataLoan.sSpLotNum = GetString("sSpLotNum");
            dataLoan.sSpBlockPlatNum = GetString("sSpBlockPlatNum");

            dataLoan.sUseParsedAddressFieldsForIntegrations = GetBool("sUseParsedAddressFieldsForIntegrations");
            dataLoan.sSpStNum = GetString("sSpStNum");
            dataLoan.sSpStUnit = GetString("sSpStUnit");
            dataLoan.sSpStDir = GetString("sSpStDir");
            dataLoan.sSpStName = GetString("sSpStName");
            dataLoan.sSpStType = GetString("sSpStType");
            dataLoan.sSpStPostDir = GetString("sSpStPostDir");

            // Borrower information
            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aBSsn = GetString("aBSsn");
            dataApp.aBDob_rep = GetString("aBDob");

            dataApp.aCFirstNm = GetString("aCFirstNm");
            dataApp.aCMidNm = GetString("aCMidNm");
            dataApp.aCLastNm = GetString("aCLastNm");
            dataApp.aCSuffix = GetString("aCSuffix");
            dataApp.aCSsn = GetString("aCSsn");
            dataApp.aCDob_rep = GetString("aCDob");

            // Active Loan App
            dataLoan.sFHALenderHasThisApplication = GetBool("sFHALenderHasThisApplication"); // MPTODO: TEST THIS! SAVING AND LOADING. IT LOOKS FUNNY

            // General information
            dataLoan.sFHAFieldOfficeCode = GetString("sFHAFieldOfficeCode");
            dataLoan.sLenderCaseNum = GetString("sLenderCaseNum");

            // LoanOriginatorName.Text // Loan Originator's Name
            // LoanOriginatorNMLS_ID.Text = // Loan Originator's NMLS ID

            int sFhaLenderIdT = GetInt("sFhaLenderIdT", -1);
            if (sFhaLenderIdT > -1)
            {
                dataLoan.sFhaLenderIdT = (E_sFhaLenderIdT)sFhaLenderIdT;
            }
            dataLoan.sFHALenderIdCode = GetString("sFHALenderIdCode");
            dataLoan.sFHASponsoredOriginatorEIN = GetString("sFHASponsoredOriginatorEIN");
            dataLoan.sFhaSponsoredOriginatorEinLckd = this.GetBool("sFhaSponsoredOriginatorEinLckd");
            dataLoan.sFHASponsorAgentIdCode = GetString("sFHASponsorAgentIdCode");

            dataLoan.sFHACaseT = (E_sFHACaseT)GetInt("sFHACaseT");
            dataLoan.sFHAConstCodeT = (E_sFHAConstCodeT)GetInt("sFHAConstCodeT");
            dataLoan.sFHAProcessingT = (E_sFHAProcessingT)GetInt("sFHAProcessingT");

            dataLoan.sFHAProgramId = GetString("sFHAProgramId");

            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sFHAFinT = (E_sFHAFinT)GetInt("sFHAFinT");

            // ADP Code Criteria
            dataLoan.sFinMethT = (E_sFinMethT)GetInt("sFinMethT");
            dataLoan.sFHAADPHousingProgramT = (E_sFHAADPHousingProgramT)GetInt("sFHAADPHousingProgramT");
            dataLoan.sGseSpT = (E_sGseSpT)GetInt("sGseSpT");
            dataLoan.sFHAADPSpecialProgramT = (E_sFHAADPSpecialProgramT)GetInt("sFHAADPSpecialProgramT");
            dataLoan.sBuydown = Convert.ToBoolean(GetInt("sBuydown"));
            dataLoan.sFHAADPPrincipalWriteDownT = (E_sFHAADPPrincipalWriteDownT)GetInt("sFHAADPPrincipalWriteDownT");
            //

            dataLoan.sFHAADPCode = GetString("sFHAADPCode");
            dataLoan.sLPurposeT = (E_sLPurposeT)GetInt("sLPurposeT");
            //sLPurposeT_TypeOfCase.Text = blahblah // MPTODO
            dataLoan.sFHACasePreviouslySoldByHUDAsREO = Convert.ToBoolean(GetInt("sFHACasePreviouslySoldByHUDAsREO"));
            dataLoan.sTotalScoreRefiT = (E_sTotalScoreRefiT)GetInt("sTotalScoreRefiT");

            // As Required
            dataLoan.sFHAPurposeIsStreamlineRefiApprChoice = Convert.ToBoolean(GetInt("sFHAPurposeIsStreamlineRefiApprChoice"));

            // Prior FHA/REO
            dataLoan.sFHAPreviousCaseNum = GetString("sFHAPreviousCaseNum");
            dataLoan.sEstCloseDLckd = GetBool("sEstCloseDLckd");
            dataLoan.sEstCloseD_rep = GetString("sEstCloseD");
            dataLoan.sFHANewCaseContactNm = GetString("sFHANewCaseContactNm");
            dataLoan.sFHANewCaseContactPhone = GetString("sFHANewCaseContactPhone");
            //

            dataLoan.sFHA203kConsultantId = GetString("sFHA203kConsultantId");
            dataLoan.sFHA203kType = (E_sFHA203kType)GetInt("sFHA203kType");

            // PUD/Condo
            dataLoan.sFHAPUDCondoT = (E_sFHAPUDCondoT)GetInt("sFHAPUDCondoT");
            dataLoan.sProjNm = GetString("sProjNm");
            dataLoan.sCpmProjectId = GetString("sCpmProjectId");
            dataLoan.sFHAPUDSubmissionPhase = GetString("sFHAPUDSubmissionPhase");
            dataLoan.sFHAPUDSiteCondoT = (E_sFHAPUDSiteCondoT)GetInt("sFHAPUDSiteCondoT");
            //

            dataLoan.sVACRVNum = GetString("sVACRVNum");
            dataApp.aVALAnalysisExpirationD_rep = GetString("aVALAnalysisExpirationD");

            // Compliance inspection fields
            dataLoan.sFHAComplianceInspectionAssignmentT = (E_sFHAComplianceInspectionAssignmentT)GetInt("sFHAComplianceInspectionAssignmentT");
            dataLoan.sFHAComplianceInspectionAssignmentID = GetString("sFHAComplianceInspectionAssignmentID");

            // FHAC Lender Notes
            dataLoan.sFHALenderNotes = GetString("sFHALenderNotes");
        }

        private void Bind_UpdateAnExistingCase(CPageData dataLoan, CAppData dataApp)
        {
            Bind_NewCaseNumberAssignment(dataLoan, dataApp);

            // Update existing case: Refinance authorization
            dataLoan.sFHARefiAuthNum = GetString("sFHARefiAuthNum");
        }

        private void Bind_HoldsTracking(CPageData dataLoan, CAppData dataApp)
        {
            // Holds Tracking: Borrower Information
            dataApp.aBFirstNm = GetString("holdsTracking_aBFirstNm");
            dataApp.aBMidNm = GetString("holdsTracking_aBMidNm");
            dataApp.aBLastNm = GetString("holdsTracking_aBLastNm");

            // Holds Tracking: General Information
            dataLoan.sFHALenderIdCode = GetString("holdsTracking_sFHALenderIdCode");
        }

        private void Bind_CaseQuery(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum");
            
            // Case Query: Field office
            dataLoan.sFHAFieldOfficeCode = GetString("caseQuery_sFHAFieldOfficeCode");
        }

        private void Bind_CAIVRSAuthorization(CPageData dataLoan, CAppData dataApp)
        {
            // CAIVRS:
            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBSuffix = GetString("aBSuffix");
            dataApp.aBSsn = GetString("aBSsn");
            dataApp.aBDob_rep = GetString("aBDob");

            dataApp.aCFirstNm = GetString("aCFirstNm");
            dataApp.aCMidNm = GetString("aCMidNm");
            dataApp.aCLastNm = GetString("aCLastNm");
            dataApp.aCSuffix = GetString("aCSuffix");
            dataApp.aCSsn = GetString("aCSsn");
            dataApp.aCDob_rep = GetString("aCDob");

            // Lender ID
            if (dataLoan.sLT == E_sLT.VA)
            {
                dataLoan.sVALenderIdCode = this.GetString("caivrs_sVALenderIdCode");
            }
            else
            {
                dataLoan.sFHALenderIdCode = GetString("caivrs_sFHALenderIdCode");
            }

            // Agency
            dataLoan.sFHAAgencyT = (E_sFHAAgencyT)GetInt("sFHAAgencyT");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            E_CaseProcessingRequestType requestType = (E_CaseProcessingRequestType)GetInt("CurrentCaseProcessingRequestType");

            switch (requestType)
            {
                case E_CaseProcessingRequestType.NewCaseNumberAssignment:
                    this.Load_NewCaseNumberAssignment(dataLoan, dataApp);
                    break;
                case E_CaseProcessingRequestType.UpdateAnExistingCase:
                    this.Load_UpdateAnExistingCase(dataLoan, dataApp);
                    break;
                case E_CaseProcessingRequestType.HoldsTracking:
                case E_CaseProcessingRequestType.CaseQuery:
                case E_CaseProcessingRequestType.CAIVRSAuthorization:
                    break;
                default:
                    throw new UnhandledEnumException(requestType);
            }
        }

        private void Load_NewCaseNumberAssignment(CPageData dataLoan, CAppData dataApp)
        {
            this.SetResult("sEstCloseD", dataLoan.sEstCloseD);
            this.LoadFhaSponsoredOriginatorEinFields(dataLoan);
        }

        private void Load_UpdateAnExistingCase(CPageData dataLoan, CAppData dataApp)
        {
            this.LoadFhaSponsoredOriginatorEinFields(dataLoan);
        }

        private void LoadFhaSponsoredOriginatorEinFields(CPageData dataLoan)
        {
            this.SetResult(nameof(dataLoan.sFHASponsoredOriginatorEIN), dataLoan.sFHASponsoredOriginatorEIN);
            this.SetResult(nameof(dataLoan.sFhaSponsoredOriginatorEinLckd), dataLoan.sFhaSponsoredOriginatorEinLckd);
        }
    }

    public partial class FHAConnectionService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override bool PerformResultOptimization
        {
            get { return true; }
        }
        protected override void Initialize()
        {
            AddBackgroundItem("", new FHAConnectionServiceItem());
        }
    }
}
