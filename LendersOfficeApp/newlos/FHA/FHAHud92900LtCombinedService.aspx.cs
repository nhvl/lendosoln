using System;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.FHA
{
    public class FHAHud92900LtCombinedServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHAHud92900LtCombinedServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            var initialValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum");
            dataLoan.sFHAHousingActSection = GetString("sFHAHousingActSection");


            dataLoan.sCombinedBorInfoLckd = GetBool("sCombinedBorInfoLckd");
            dataLoan.sCombinedBorFirstNm = GetString("sCombinedBorFirstNm");
            dataLoan.sCombinedBorMidNm = GetString("sCombinedBorMidNm");
            dataLoan.sCombinedBorLastNm = GetString("sCombinedBorLastNm");
            dataLoan.sCombinedBorSuffix = GetString("sCombinedBorSuffix");
            dataLoan.sCombinedBorSsn = GetString("sCombinedBorSsn");
            dataLoan.sCombinedCoborFirstNm = GetString("sCombinedCoborFirstNm");
            dataLoan.sCombinedCoborMidNm = GetString("sCombinedCoborMidNm");
            dataLoan.sCombinedCoborLastNm = GetString("sCombinedCoborLastNm");
            dataLoan.sCombinedCoborSuffix = GetString("sCombinedCoborSuffix");
            dataLoan.sCombinedCoborSsn = GetString("sCombinedCoborSsn");
            dataLoan.sCombinedCoborUsing2ndAppBorr = GetBool("sCombinedCoborUsing2ndAppBorr");            
            
            dataLoan.sFHANegCfRentalI_rep = GetString("sFHANegCfRentalI");
            
            dataLoan.sSpAddr = GetString("sSpAddr");
            dataLoan.sSpCity = GetString("sSpCity");
            dataLoan.sSpState = GetString("sSpState");
            dataLoan.sSpZip = GetString("sSpZip");

            dataLoan.sGseSpT = (E_sGseSpT) GetInt("sGseSpT");
            dataLoan.sPurchPrice_rep = GetString("sPurchPrice");
            dataLoan.sApprVal_rep = GetString("sApprVal");
            dataLoan.sEstateHeldT = (E_sEstateHeldT) GetInt("sEstateHeldT");
            dataLoan.sFHAConstructionT = (E_sFHAConstructionT) GetInt("sFHAConstructionT");
            dataLoan.sFinMethT = (E_sFinMethT) GetInt("sFinMethT");
            dataLoan.sRAdjMarginR_rep = GetString("sRAdjMarginR");
            dataLoan.sBuydown = GetBool("sBuydown");
            dataLoan.sNoteIR_rep = GetString("sNoteIR");
            dataLoan.sQualIR_rep = GetString("sQualIR");
            dataLoan.sQualIRLckd = this.GetBool(nameof(dataLoan.sQualIRLckd));
            dataLoan.sTerm_rep = GetString("sTerm");
            dataLoan.sUnitsNum_rep = GetString("sUnitsNum");
            dataLoan.sRAdj1stCapMon_rep = GetString("sRAdj1stCapMon");



            dataLoan.sFHAIncomeLckd = GetBool("sFHAIncomeLckd");
            dataLoan.sFHABBaseI_rep = GetString("sFHABBaseI");
            dataLoan.sFHACBaseI_rep = GetString("sFHACBaseI");
            dataLoan.sFHABOI_rep = GetString("sFHABOI");
            dataLoan.sFHACOI_rep = GetString("sFHACOI");
            dataLoan.sFHANetRentalI_rep = GetString("sFHANetRentalI");
            
            dataLoan.sFHADebtLckd = GetBool("sFHADebtLckd");
            dataLoan.sFHADebtInstallPmt_rep = GetString("sFHADebtInstallPmt");
            dataLoan.sFHAChildSupportPmt_rep = GetString("sFHAChildSupportPmt");
            dataLoan.sFHADebtInstallBal_rep = GetString("sFHADebtInstallBal");
            dataLoan.sFHAOtherDebtPmt_rep = GetString("sFHAOtherDebtPmt");
            dataLoan.sFHAOtherDebtBal_rep = GetString("sFHAOtherDebtBal");

            
            
            
            dataLoan.sTransmFntc_rep = GetString("sTransmFntc");
            dataLoan.sVerifAssetAmt_rep = GetString("sVerifAssetAmt");
            dataLoan.sFHACcTot_rep = GetString("sFHACcTot");
            dataLoan.sFntcSrc = GetString("sFntcSrc");
            dataLoan.sRsrvMonNumDesc = GetString("sRsrvMonNumDesc");
            dataLoan.sTransmFntcLckd = GetBool("sTransmFntcLckd");
            dataLoan.sFHAPro1stMPmt_rep = GetString("sFHAPro1stMPmt");
            dataLoan.sFHAProMIns_rep = GetString("sFHAProMIns");
            dataLoan.sFHAProMInsLckd = GetBool("sFHAProMInsLckd");
            dataLoan.sFHAProHoAssocDues_rep = GetString("sFHAProHoAssocDues");
            dataLoan.sFHAProGroundRent_rep = GetString("sFHAProGroundRent");
            dataLoan.sFHAPro2ndFinPmt_rep = GetString("sFHAPro2ndFinPmt");
            dataLoan.sFHAProHazIns_rep = GetString("sFHAProHazIns");
            dataLoan.sFHAProRealETx_rep = GetString("sFHAProRealETx");


            dataLoan.sTransmUwerComments = GetString("sTransmUwerComments");
            dataLoan.sFHASellerContribution_rep = GetString("sFHASellerContribution");

            dataLoan.sLPurposeT = (E_sLPurposeT)GetInt("sLPurposeT");
            dataLoan.sFHAPurposeIsPurchase = GetBool("sFHAPurposeIsPurchase"); 
            dataLoan.sFHAPurposeIsNoCashoutRefi = GetBool("sFHAPurposeIsNoCashoutRefi"); 
            dataLoan.sFHAPurposeIsCashoutRefi = GetBool("sFHAPurposeIsCashoutRefi"); 
            dataLoan.sFHAPurposeIsStreamlineRefi = GetBool("sFHAPurposeIsStreamlineRefi"); 
            dataLoan.sFHAPurposeIsStreamlineRefiWithAppr = GetBool("sFHAPurposeIsStreamlineRefiWithAppr"); 
            dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr = GetBool("sFHAPurposeIsStreamlineRefiWithoutAppr"); 
            dataLoan.sFHAPurposeIsConstToPerm = GetBool("sFHAPurposeIsConstToPerm"); 
            dataLoan.sFHAPurposeIsEnergyEfficientMortgage = GetBool("sFHAPurposeIsEnergyEfficientMortgage"); 
            dataLoan.sFHAPurposeIsBuildOnOwnLand = GetBool("sFHAPurposeIsBuildOnOwnLand"); 
            dataLoan.sFHAPurposeIsHudReo = GetBool("sFHAPurposeIsHudReo"); 
            dataLoan.sFHAPurposeIs203k = GetBool("sFHAPurposeIs203k"); 
            dataLoan.sFHAPurposeIsOther = GetBool("sFHAPurposeIsOther"); 

            dataLoan.sFHASecondaryFinancingIsGov = GetBool("sFHASecondaryFinancingIsGov"); 
            dataLoan.sFHASecondaryFinancingIsNP = GetBool("sFHASecondaryFinancingIsNP"); 
            dataLoan.sFHASecondaryFinancingIsFamily = GetBool("sFHASecondaryFinancingIsFamily"); 
            dataLoan.sFHASecondaryFinancingIsOther = GetBool("sFHASecondaryFinancingIsOther"); 

            dataLoan.sFHAIsSellerFundDAP = GetBool("sFHAIsSellerFundDAP"); 
            dataLoan.sFHAGift1IsGov = GetBool("sFHAGift1IsGov"); 
            dataLoan.sFHAGift1IsNP = GetBool("sFHAGift1IsNP"); 
            dataLoan.sFHAGift1IsFamily = GetBool("sFHAGift1IsFamily"); 
            dataLoan.sFHAGift1IsOther = GetBool("sFHAGift1IsOther"); 
            dataLoan.sFHAGift2IsGov = GetBool("sFHAGift2IsGov"); 
            dataLoan.sFHAGift2IsNP = GetBool("sFHAGift2IsNP"); 
            dataLoan.sFHAGift2IsFamily = GetBool("sFHAGift2IsFamily"); 
            dataLoan.sFHAGift2IsOther = GetBool("sFHAGift2IsOther"); 

            dataLoan.sFHARiskClassAA = GetBool("sFHARiskClassAA"); 
            dataLoan.sFHARiskClassRefer = GetBool("sFHARiskClassRefer");
            dataLoan.sFHASecondaryFinancingSource = GetString("sFHASecondaryFinancingSource");
            dataLoan.sFHASecondaryFinancingOtherDesc = GetString("sFHASecondaryFinancingOtherDesc"); 
            dataLoan.sFHASecondaryFinancingAmt_rep = GetString("sFHASecondaryFinancingAmt"); 


            dataLoan.sFHAGift1Source = GetString("sFHAGift1Source");

            dataLoan.sFHAGift1OtherDesc = GetString("sFHAGift1OtherDesc");
            dataLoan.sFHAGift1gAmt_rep = GetString("sFHAGift1gAmt"); 

            dataLoan.sFHAGift2Source = GetString("sFHAGift2Source");
            dataLoan.sFHAGift2OtherDesc = GetString("sFHAGift2OtherDesc");
            dataLoan.sFHAGift2gAmt_rep = GetString("sFHAGift2gAmt"); 

            dataLoan.sFHAArmIndex = GetString("sFHAArmIndex");
            dataLoan.sChumsIdReviewerAppraisal = GetString("sChumsIdReviewerAppraisal");
            dataLoan.sFHAScoreByTotalTri = GetTriState("sFHAScoreByTotalTri");

            dataLoan.sFHA92900LtUnderwriterChumsIdLckd = GetBool("sFHA92900LtUnderwriterChumsIdLckd");
            dataLoan.sFHA92900LtUnderwriterChumsId = GetString("sFHA92900LtUnderwriterChumsId");

            FHAHud92900LtCombined.BindDataFromControls(dataLoan, dataApp, this);
            var finalValues = BaseSimpleServiceXmlPage.GetLeftTreeFrameWatchedValues(dataLoan);
            SetResult("ShouldResetLeftNav", BaseSimpleServiceXmlPage.ShouldRefreshLeftNav(initialValues, finalValues));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            SetResult("sFHAHousingActSection", dataLoan.sFHAHousingActSection);

            SetResult("sFHANegCfRentalI", dataLoan.sFHANegCfRentalI_rep);
            SetResult("sCombinedBorInfoLckd", dataLoan.sCombinedBorInfoLckd);
            SetResult("sCombinedBorFirstNm", dataLoan.sCombinedBorFirstNm);
            SetResult("sCombinedBorMidNm", dataLoan.sCombinedBorMidNm);
            SetResult("sCombinedBorLastNm", dataLoan.sCombinedBorLastNm);
            SetResult("sCombinedBorSuffix", dataLoan.sCombinedBorSuffix);
            SetResult("sCombinedBorSsn", dataLoan.sCombinedBorSsn);
            SetResult("sCombinedCoborFirstNm", dataLoan.sCombinedCoborFirstNm);
            SetResult("sCombinedCoborMidNm", dataLoan.sCombinedCoborMidNm);
            SetResult("sCombinedCoborLastNm", dataLoan.sCombinedCoborLastNm);
            SetResult("sCombinedCoborSuffix", dataLoan.sCombinedCoborSuffix);
            SetResult("sCombinedCoborSsn", dataLoan.sCombinedCoborSsn);
            SetResult("sCombinedCoborUsing2ndAppBorr", dataLoan.sCombinedCoborUsing2ndAppBorr);            
            
            
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);

            SetResult("sGseSpT", dataLoan.sGseSpT);
            SetResult("sPurchPrice", dataLoan.sPurchPrice_rep);
            SetResult("sApprVal", dataLoan.sApprVal_rep);
            SetResult("sEstateHeldT", dataLoan.sEstateHeldT);
            SetResult("sFHAConstructionT", dataLoan.sFHAConstructionT);
            SetResult("sFinMethT", dataLoan.sFinMethT);
            SetResult("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);
            SetResult("sBuydown", dataLoan.sBuydown);
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sQualIR", dataLoan.sQualIR_rep);
            this.SetResult(nameof(dataLoan.sQualIRLckd), dataLoan.sQualIRLckd);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sUnitsNum", dataLoan.sUnitsNum_rep);
            SetResult("sRAdj1stCapMon", dataLoan.sRAdj1stCapMon_rep);

            SetResult("sFHAIncomeLckd", dataLoan.sFHAIncomeLckd);
            SetResult("sFHATotBaseI", dataLoan.sFHATotBaseI_rep);
            SetResult("sFHABBaseI", dataLoan.sFHABBaseI_rep);
            SetResult("sFHACBaseI", dataLoan.sFHACBaseI_rep);
            SetResult("sFHABOI", dataLoan.sFHABOI_rep);
            SetResult("sFHACOI", dataLoan.sFHACOI_rep);
            SetResult("sFHATotOI", dataLoan.sFHATotOI_rep);
            SetResult("sFHANetRentalI", dataLoan.sFHANetRentalI_rep);
            SetResult("sFHANetRentalI2", dataLoan.sFHANetRentalI_rep);
            SetResult("sFHABTotI", dataLoan.sFHABTotI_rep);
            SetResult("sFHACTotI", dataLoan.sFHACTotI_rep);
            SetResult("sFHAGrossMonI", dataLoan.sFHAGrossMonI_rep);

            SetResult("sFHADebtLckd", dataLoan.sFHADebtLckd);
            SetResult("sFHADebtInstallPmt", dataLoan.sFHADebtInstallPmt_rep);
            SetResult("sFHAChildSupportPmt", dataLoan.sFHAChildSupportPmt_rep);
            SetResult("sFHADebtInstallBal", dataLoan.sFHADebtInstallBal_rep);
            SetResult("sFHAOtherDebtPmt", dataLoan.sFHAOtherDebtPmt_rep);
            SetResult("sFHAOtherDebtBal", dataLoan.sFHAOtherDebtBal_rep);
            SetResult("sFHAPmtFixedTot", dataLoan.sFHAPmtFixedTot_rep);




            SetResult("sTransmFntc", dataLoan.sTransmFntc_rep);
            SetResult("sVerifAssetAmt", dataLoan.sVerifAssetAmt_rep);
            SetResult("sFHACcTot", dataLoan.sFHACcTot_rep);
            SetResult("sFntcSrc", dataLoan.sFntcSrc);
            SetResult("sRsrvMonNumDesc", dataLoan.sRsrvMonNumDesc);
            SetResult("sFHASellerContributionPc", dataLoan.sFHASellerContributionPc_rep);
            SetResult("sTransmFntcLckd", dataLoan.sTransmFntcLckd);

            SetResult("sFHAPro1stMPmt", dataLoan.sFHAPro1stMPmt_rep);
            SetResult("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            SetResult("sFHAProMInsLckd", dataLoan.sFHAProMInsLckd);
            SetResult("sFHAProHoAssocDues", dataLoan.sFHAProHoAssocDues_rep);
            SetResult("sFHAProGroundRent", dataLoan.sFHAProGroundRent_rep);
            SetResult("sFHAPro2ndFinPmt", dataLoan.sFHAPro2ndFinPmt_rep);
            SetResult("sFHAProHazIns", dataLoan.sFHAProHazIns_rep);
            SetResult("sFHAProRealETx", dataLoan.sFHAProRealETx_rep);
            SetResult("sFHAMonthlyPmt", dataLoan.sFHAMonthlyPmt_rep);

            SetResult("sLtvR", dataLoan.sLtvR_rep);
            SetResult("sCltvR", dataLoan.sCltvR_rep);
            SetResult("sFHAMPmtToIRatio", dataLoan.sFHAMPmtToIRatio_rep);
            SetResult("sFHAFixedPmtToIRatio", dataLoan.sFHAFixedPmtToIRatio_rep);
            SetResult("sFHABCaivrsNum", dataLoan.sFHABCaivrsNum);
            SetResult("sFHACCaivrsNum", dataLoan.sFHACCaivrsNum);
            SetResult("sTransmUwerComments", dataLoan.sTransmUwerComments);
            SetResult("sFHASellerContribution", dataLoan.sFHASellerContribution_rep);

            SetResult("sLPurposeT", dataLoan.sLPurposeT);
            SetResult("sFHAPurposeIsPurchase", dataLoan.sFHAPurposeIsPurchase); 
            SetResult("sFHAPurposeIsNoCashoutRefi", dataLoan.sFHAPurposeIsNoCashoutRefi); 
            SetResult("sFHAPurposeIsCashoutRefi", dataLoan.sFHAPurposeIsCashoutRefi); 
            SetResult("sFHAPurposeIsStreamlineRefi", dataLoan.sFHAPurposeIsStreamlineRefi); 
            SetResult("sFHAPurposeIsStreamlineRefiWithAppr", dataLoan.sFHAPurposeIsStreamlineRefiWithAppr); 
            SetResult("sFHAPurposeIsStreamlineRefiWithoutAppr", dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr); 
            SetResult("sFHAPurposeIsConstToPerm", dataLoan.sFHAPurposeIsConstToPerm); 
            SetResult("sFHAPurposeIsEnergyEfficientMortgage", dataLoan.sFHAPurposeIsEnergyEfficientMortgage); 
            SetResult("sFHAPurposeIsBuildOnOwnLand", dataLoan.sFHAPurposeIsBuildOnOwnLand); 
            SetResult("sFHAPurposeIsHudReo", dataLoan.sFHAPurposeIsHudReo); 
            SetResult("sFHAPurposeIs203k", dataLoan.sFHAPurposeIs203k); 
            SetResult("sFHAPurposeIsOther", dataLoan.sFHAPurposeIsOther); 

            SetResult("sFHASecondaryFinancingIsGov", dataLoan.sFHASecondaryFinancingIsGov); 
            SetResult("sFHASecondaryFinancingIsNP", dataLoan.sFHASecondaryFinancingIsNP); 
            SetResult("sFHASecondaryFinancingIsFamily", dataLoan.sFHASecondaryFinancingIsFamily); 
            SetResult("sFHASecondaryFinancingIsOther", dataLoan.sFHASecondaryFinancingIsOther); 

            SetResult("sFHAIsSellerFundDAP", dataLoan.sFHAIsSellerFundDAP); 
            SetResult("sFHAGift1IsGov", dataLoan.sFHAGift1IsGov); 
            SetResult("sFHAGift1IsNP", dataLoan.sFHAGift1IsNP); 
            SetResult("sFHAGift1IsFamily", dataLoan.sFHAGift1IsFamily); 
            SetResult("sFHAGift1IsOther", dataLoan.sFHAGift1IsOther); 
            SetResult("sFHAGift2IsGov", dataLoan.sFHAGift2IsGov); 
            SetResult("sFHAGift2IsNP", dataLoan.sFHAGift2IsNP); 
            SetResult("sFHAGift2IsFamily", dataLoan.sFHAGift2IsFamily); 
            SetResult("sFHAGift2IsOther", dataLoan.sFHAGift2IsOther); 

            SetResult("sFHARiskClassAA", dataLoan.sFHARiskClassAA); 
            SetResult("sFHARiskClassRefer", dataLoan.sFHARiskClassRefer);
            SetResult("sFHASecondaryFinancingSource", dataLoan.sFHASecondaryFinancingSource);
            SetResult("sFHASecondaryFinancingOtherDesc", dataLoan.sFHASecondaryFinancingOtherDesc); 
            SetResult("sFHASecondaryFinancingAmt", dataLoan.sFHASecondaryFinancingAmt_rep); 


            SetResult("sFHAGift1Source", dataLoan.sFHAGift1Source);

            SetResult("sFHAGift1OtherDesc", dataLoan.sFHAGift1OtherDesc);
            SetResult("sFHAGift1gAmt", dataLoan.sFHAGift1gAmt_rep); 

            SetResult("sFHAGift2Source", dataLoan.sFHAGift2Source);
            SetResult("sFHAGift2OtherDesc", dataLoan.sFHAGift2OtherDesc);
            SetResult("sFHAGift2gAmt", dataLoan.sFHAGift2gAmt_rep); 

            SetResult("sFHAArmIndex", dataLoan.sFHAArmIndex);
            SetResult("sChumsIdReviewerAppraisal", dataLoan.sChumsIdReviewerAppraisal);

            SetResult("sFHAScoreByTotalTri", dataLoan.sFHAScoreByTotalTri);  
            SetResult("sFHABLdpGsaTri", dataLoan.sFHABLdpGsaTri);
            SetResult("sFHACLdpGsaTri", dataLoan.sFHACLdpGsaTri);

            SetResult("sFHA92900LtUnderwriterChumsId", dataLoan.sFHA92900LtUnderwriterChumsId);

            FHAHud92900LtCombined.LoadDataForControls(dataLoan, dataApp, this);
        }
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "":
                    break;
				case "CalculateAssetTotal":
					CalculateAssetTotal();
					break;
            }
        }

		//opm 25105 fs 09/30/08
		private void CalculateAssetTotal() 
		{
			Guid sLId = GetGuid("LoanId");

			CPageData dataLoan = ConstructPageDataClass(sLId);
			dataLoan.InitLoad();

            // 5/10/2012 vm - OPM 25561 - Copy the value from aAsstLiqTot
			int nApps = dataLoan.nApps;

			decimal total = 0.0M;
			for (int i = 0; i < nApps; i++) 
			{
				CAppData dataApp = dataLoan.GetAppData(i);
                total += dataApp.aAsstLiqTot;
			}

			SetResult("VerifiedAssetTotal", dataLoan.m_convertLos.ToMoneyString(total, FormatDirection.ToRep));

		}
    }

    public partial class FHAHud92900LtCombinedService : BaseSimpleServiceXmlPage 
    {
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FHAHud92900LtCombinedServiceItem());
        }
    }
}
