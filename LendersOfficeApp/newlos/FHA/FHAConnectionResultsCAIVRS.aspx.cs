﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using System.Xml;
using DataAccess;
using System.Xml.Linq;

namespace LendersOfficeApp.newlos.FHA
{
    public partial class FHAConnectionResultsCAIVRS : BaseXsltPage
    {
        protected override string XsltFileLocation
        {
            get { return "LendersOffice.ObjLib.FHAConnection.xslt.caivrsAuthorization.xslt"; }
        }
        protected override bool IsXsltFromEmbeddedResource
        {
            get { return true; }
        }
        private Guid LoanID
        {
            get { return RequestHelper.LoanID; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void GenerateXmlData(XmlWriter writer)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHAConnectionResultsCAIVRS));
            dataLoan.InitLoad();
            string xml = dataLoan.sFHACAVIRSResultXmlContent;
            if (xml != "")
            {
                XDocument xmlDoc = XDocument.Parse(xml);
                var rootElement = xmlDoc.Element("MORTGAGEDATA");
                
                var lqbElement = new XElement("LQB_DATA");
                lqbElement.Add(new XElement("ImageFileName", Tools.VRoot + "/images/logo_fha.gif"));
                rootElement.Add(lqbElement);

                string xmlString = xmlDoc.ToString();
                writer.WriteRaw(xmlString);
                // then include any extra stuff that's needed
            }
            else
            {
                HasContent = false;
            }
        }
    }
}
