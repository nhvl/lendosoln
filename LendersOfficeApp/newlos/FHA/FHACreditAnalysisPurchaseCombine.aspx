<%@ Page language="c#" Codebehind="FHACreditAnalysisPurchaseCombine.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHACreditAnalysisPurchaseCombine" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
		<title>FHACreditAnalysisPurchaseCombine</title>
  </HEAD>
<body class=RightBackground MS_POSITIONING="FlowLayout">
<script type="text/javascript">
    function _init() {
      enableBorrowerInfo();
      enableMonthIncome();
      enableMonthlyDebt();
      lockField(<%= AspxTools.JsGetElementById(sFHALtvLckd) %>, 'sFHALtv');
      lockField(<%= AspxTools.JsGetElementById(sFHANonrealtyLckd) %>, 'sFHANonrealty');
      lockField(<%= AspxTools.JsGetElementById(sFfUfmip1003Lckd) %>, 'sFfUfmip1003');
      lockField(<%= AspxTools.JsGetElementById(sFHAProMInsLckd) %>, 'sFHAProMIns');
      var v = <%= AspxTools.JsGetElementById(sFHAPurchPriceSrcT) %>.value;
      document.getElementById("sFHAPurchPrice").readOnly = v != 2;

      <% if (IsReadOnly) { %>
        document.getElementById("btnCopyFromLoanApp").disabled = true;
        document.getElementById("btnCopyFromGFE").disabled = true;
      <% } %>

    }

    function enableBorrowerInfo() {

      var b = <%= AspxTools.JsGetElementById(sCombinedBorInfoLckd) %>.checked;
      <%= AspxTools.JsGetElementById(sCombinedBorFirstNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorMidNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorLastNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorSuffix) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorSsn) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborFirstNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborMidNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborLastNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborSuffix) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborSsn) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborUsing2ndAppBorr) %>.disabled = b;
    }

    function enableMonthIncome() {
      var b = <%= AspxTools.JsGetElementById(sFHAIncomeLckd) %>.checked;
      <%= AspxTools.JsGetElementById(sFHABBaseI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHABOI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHACBaseI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHACOI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHANetRentalI) %>.readOnly = !b;
    }
    function enableMonthlyDebt() {
      var b = <%= AspxTools.JsGetElementById(sFHADebtLckd) %>.checked;
      <%= AspxTools.JsGetElementById(sFHADebtInstallPmt) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHADebtInstallBal) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHAChildSupportPmt) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHAOtherDebtPmt) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHAOtherDebtBal) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHANegCfRentalI) %>.readOnly = !b;
    }
    function handlingError(result)
    {
      var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
      if (result.ErrorType === 'VersionMismatchException')
      {
        f_displayVersionMismatch();
      }
      else if (result.ErrorType === 'LoanFieldWritePermissionDenied')
      {
        f_displayFieldWriteDenied(result.UserMessage);
      }
      else
      {
        alert(errMsg);
      }

    }
    function __executeCalculation(methodName)
    {
      PolyShouldShowConfirmSave(isDirty(), function(confirmed){
        var args = getAllFormValues();
        args["loanid"] = ML.sLId;
        args["issave"] =  confirmed ? 'T' : 'F';
        var result = gService.loanedit.call(methodName, args);
        if (!result.error) {
          populateForm(result.value); // Refresh value;
          _init();
          clearDirty();
        } else {
          handlingError(result);
        }
      });
    }

    function copyFromGFE() {
      __executeCalculation("CopyFromGFE");

    }
    function copyFromLoanApp() {
      __executeCalculation("CopyFromLoanApp");
    }
    </script>

<form id=FHACreditAnalysisPurchaseCombine method=post runat="server">
<table class=FormTable id=Table8 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class=MainRightHeader>FHA MCAW - Combined
      Purchase (HUD-92900-PUR)</td></tr>
  <tr>
    <td><input onclick=copyFromLoanApp(); type=button value="Copy Data From All Loan Apps" id=btnCopyFromLoanApp><input onclick=copyFromGFE(); type=button value="Copy Closing Cost Data to Form" id=btnCopyFromGFE></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table6 cellSpacing=0 cellPadding=0 width="98%" border=0>
        <tr>
          <td noWrap>
            <table id=Table9 cellSpacing=0 cellPadding=0 border=0>
              <tr>
                <td class="FieldLabel">Loan Type</td>
                <td>
                  <asp:DropDownList ID="sLT" runat="server" />
                </td>
                <td class="FieldLabel" nowrap></td>
                <td></td>
                <td class="FieldLabel"></td>
                <td></td>
              </tr>
              <tr>
                <td class="FieldLabel">Case Number</td>
                <td>
                  <asp:TextBox ID="sAgencyCaseNum" runat="server" Width="108px" /></td>
                <td class="FieldLabel" nowrap>Housing Act</td>
                <td>
                  <ml:ComboBox ID="sFHAHousingActSection" runat="server" Width="70px" /></td>
                <td class="FieldLabel">Construction</td>
                <td>
                  <asp:DropDownList ID="sFHAConstructionT" runat="server" />
                </td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>CHUMS ID Number</td>
                <td>
                  <asp:TextBox ID="FHACreditAnalysisPurchaseUnderwriterLicenseNumOfAgent" runat="server" Width="108px" /></td>
                <td class="FieldLabel">Underwriter Name</td>
                <td>
                  <asp:TextBox ID="FHACreditAnalysisPurchaseUnderwriterPreparerName" runat="server" /></td>
                <td class="FieldLabel"></td>
                <td></td>
              </tr>
            </table></td></tr></table></td></tr>
  <TR>
    <TD>
      <table class="InsetBorder" id="Table7" cellspacing="0" cellpadding="0" width="98%" border="0">
        <tr>
          <td noWrap>
            <table id="Table5" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td class="FieldLabel" nowrap colspan="2">Borrower Information&nbsp;&nbsp;<asp:CheckBox ID="sCombinedBorInfoLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox></td>
                <td nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td class="FieldLabel" nowrap colspan="2">Coborrower Information</td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap colspan="2"></td>
                <td nowrap></td>
                <td class="FieldLabel" nowrap colspan="2"><asp:CheckBox ID="sCombinedCoborUsing2ndAppBorr" runat="server" Text="Use 2nd Borrower Info" onclick="refreshCalculation();" TabIndex="10" /></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>First Name</td>
                <td nowrap>
                  <asp:TextBox ID="sCombinedBorFirstNm" runat="server" TabIndex="5" /></td>
                <td nowrap>&nbsp;&nbsp;</td>
                <td class="FieldLabel" nowrap>First Name</td>
                <td nowrap>
                  <asp:TextBox ID="sCombinedCoborFirstNm" runat="server" TabIndex="10" /></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>Middle Name</td>
                <td nowrap>
                  <asp:TextBox ID="sCombinedBorMidNm" runat="server" TabIndex="5" /></td>
                <td nowrap></td>
                <td class="FieldLabel" nowrap>Middle Name</td>
                <td nowrap>
                  <asp:TextBox ID="sCombinedCoborMidNm" runat="server" TabIndex="10" /></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>Last Name</td>
                <td nowrap>
                  <asp:TextBox ID="sCombinedBorLastNm" runat="server" TabIndex="5" /></td>
                <td nowrap></td>
                <td class="FieldLabel" nowrap>Last Name</td>
                <td nowrap>
                  <asp:TextBox ID="sCombinedCoborLastNm" runat="server" TabIndex="10" /></td>
              </tr>
              <tr>
                <td class=FieldLabel noWrap>Suffix</td>
                <td noWrap><asp:textbox id=sCombinedBorSuffix runat="server" tabindex=5 /></td>
                <td noWrap></td>
                <td class=FieldLabel noWrap>Suffix</td>
                <td noWrap><asp:textbox id=sCombinedCoborSuffix runat="server" maxlength="21" tabindex=10></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>SSN</td>
                <td class=FieldLabelNoPadding noWrap><ml:ssntextbox id=sCombinedBorSsn runat="server" width="75px" preset="ssn" tabindex=5></ml:ssntextbox></td>
                <td noWrap></td>
                <td class=FieldLabel noWrap>SSN</td>
                <td class=FieldLabelNoPadding noWrap><ml:ssntextbox id=sCombinedCoborSsn runat="server" width="75px" preset="ssn" tabindex=10></ml:ssntextbox></td></tr></table></td></tr></table></TD></TR>
  <tr>
    <td>
      <table class=InsetBorder id=Table12 cellSpacing=0 cellPadding=0
      width="98%" border=0>
        <tr>
          <td noWrap>
            <table id=Table10 cellSpacing=0 cellPadding=0 border=0
            >
              <tr id=row1>
                <td class=FieldLabel noWrap>3a. Mortgage
                  w/o Upfront MIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><ml:moneytextbox id=sLAmt runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=15></ml:moneytextbox></td>
                <td width="2%"></td>
                <td class=FieldLabel noWrap>6. Current
                  Housing Expenses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><ml:moneytextbox id=sPresLTotHExp runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel>3b. Total UFMIP <ml:percenttextbox id=sFfUfmipR runat="server" width="70" preset="percent" onchange="refreshCalculation();" tabindex=15  decimalDigits="6"/>&nbsp;for&nbsp;&nbsp;<asp:textbox id=sMipPiaMon tabIndex=15 runat="server" Width="30px" onchange="refreshCalculation();"></asp:textbox>&nbsp;mos.=<asp:CheckBox id=sFfUfmip1003Lckd runat="server" Text="Lock" onclick="refreshCalculation();" tabindex=15></asp:CheckBox></td>
                <td><ml:moneytextbox id=sFfUfmip1003 runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel>7. Term of loan
                  (years)</td>
                <td><asp:textbox id=sTermInYr runat="server" ReadOnly="True" Width="46px" MaxLength="3" tabindex=20></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel
                  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MIP
                  Paid in Cash</td>
                <td><ml:moneytextbox id=sUfCashPd runat="server" preset="money" width="100" CssClass="mask" onchange="refreshCalculation();" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel>8. Interest Rate</td>
                <td><ml:percenttextbox id=sNoteIR runat="server" width="100" preset="percent" CssClass="mask" onchange="refreshCalculation();" tabindex=20></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel>&nbsp;&nbsp;&nbsp;
                  PMI, MIP, FF
                  financed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  =</td>
                <td><ml:moneytextbox id=sFfUfmipFinanced runat="server" ReadOnly="True" width="100px" preset="money" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel></td>
                <td></td></tr>
              <tr>
                <td class=FieldLabel>3c. Mortgage with
                  UFMIP</td>
                <td><ml:moneytextbox id=sFinalLAmt runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel>9. Adj Buy-down
                  Interest Rate</td>
                <td><ml:percenttextbox id=sBuydownResultIR runat="server" ReadOnly="True" width="100" preset="percent" CssClass="mask" tabindex=20></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel>4. Appraised Value (w/o CC)</td>
                <td><ml:moneytextbox id=sApprVal runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel noWrap></td>
                <td></td></tr>
              <tr>
                <td class=FieldLabel noWrap>5a. Total
                  Closing Costs (CC)&nbsp;</td>
                <td><ml:moneytextbox id=sFHACcTot runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td class=FieldLabel>Energy Efficient
                  Improvements</td>
                <td><ml:moneytextbox id=sFHAEnergyEffImprov runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex=20></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel>5b. Less paid by
                  seller</td>
                <td><ml:moneytextbox id=sFHACcPbs runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td></td>
                <td></td></tr>
              <tr>
                <td class=FieldLabel>5c. Equals
                  Borrower's CC</td>
                <td><ml:moneytextbox id=sFHACcPbb runat="server" width="100" preset="money" CssClass="mask" readonly="True" tabindex=15></ml:moneytextbox></td>
                <td></td>
                <td></td>
                <td
      ></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table13 cellSpacing=0 cellPadding=0 width="98%" border=0>
        <tr>
          <td noWrap>
            <table id=Table11 cellSpacing=0 cellPadding=0 border=0 >
              <tr>
                <td class=FieldLabel noWrap>10. Statutory Investment Requirements&nbsp; </td>
                <td noWrap></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Contract Sales
                  Price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:DropDownList id=sFHAPurchPriceSrcT tabIndex=25 runat="server" onchange="refreshCalculation();">
<asp:ListItem Value="0" Selected="True">Purchase Price</asp:ListItem>
<asp:ListItem Value="1">203k WS line C3</asp:ListItem>
<asp:ListItem Value="2">Locked</asp:ListItem>
									</asp:DropDownList></td>
                <td noWrap><ml:moneytextbox id=sFHAPurchPrice runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b.
                  Borrower-Paid Closing Costs (from 5c)&nbsp;&nbsp; </td>
                <td noWrap><ml:moneytextbox id=sFHACcPbb2 runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Unadjusted Acquisition (10a + 10b)</td>
                <td noWrap><ml:moneytextbox id=sFHAUnadjAcquisition runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d.
                  Statutory Investment Requirement (10a&nbsp;x <ml:PercentTextBox ID="sFHA203kInvestmentRequiredPc" runat="server" tabindex="25" onchange="refreshCalculation();"/>)&nbsp;&nbsp;
                </td>
                <td noWrap><ml:moneytextbox id=sFHAStatutoryInvestReq runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>11. Maximum
                  Mortgage Calculation</td>
                <td noWrap></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Lesser of Sales Price (10a) or Value (from 4)</td>
                <td noWrap><ml:moneytextbox id=sFHAHouseVal runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.
                  Required Adjustments (+/-)</td>
                <td noWrap><ml:moneytextbox id=sFHAReqAdj runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Mortgage Basis (11a + 11b)</td>
                <td noWrap><ml:moneytextbox id=sFHAMBasis runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d.
                  Mortgage Amount (11c x LTV Factor<asp:checkbox id=sFHALtvLckd onclick="refreshCalculation();" runat="server" Text="Lock" tabindex=25></asp:checkbox><ml:percenttextbox id=sFHALtv runat="server" width="100" preset="percent" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:percenttextbox>)</td>
                <td noWrap><ml:moneytextbox id=sFHAMAmt runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>12. Cash
                  Investment Requirements</td>
                <td noWrap></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Min
                  Down Payment (10c - 11d) (Must equal or exceed 10d)</td>
                <td noWrap><ml:moneytextbox id=sFHAMinDownPmt runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Prepaid
                  Expenses</td>
                <td noWrap><ml:moneytextbox id=sFHAPrepaidExp runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Discount Points</td>
                <td noWrap><ml:moneytextbox id=sFHADiscountPoints runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d.
                  Repairs/Improvements (Non-Financeable)</td>
                <td noWrap><ml:moneytextbox id=sFHAImprovements runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e. Upfront
                  MIP Paid in Cash</td>
                <td noWrap><ml:moneytextbox id=sUfCashPdzzz2 runat="server" preset="money" width="100px" ReadOnly="True" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;f.
                  Non-Realty and Other Items<asp:checkbox id=sFHANonrealtyLckd onclick="refreshCalculation();" runat="server" Text="Lock" tabindex=25></asp:checkbox></td>
                <td noWrap><ml:moneytextbox id=sFHANonrealty runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; g. Total
                  Cash to Close (Sum of 12a thru 12f)</td>
                <td noWrap><ml:moneytextbox id=sFHATotCashToClose runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h. Amount
                  Paid (Earnest Money, etc.)<asp:checkbox id=sFHAAmtPaidByCash runat="server" Text="Cash" tabindex=25></asp:checkbox><asp:checkbox id=sFHAAmtPaidByOtherT runat="server" Text="Other" tabindex=25></asp:checkbox></td>
                <td noWrap><ml:moneytextbox id=sFHAAmtPaid runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i. Amount
                  of Gift Funds&nbsp; Src:<asp:textbox id=sFHAGiftFundSrc runat="server" Width="218px" readonly="True" tabindex=25></asp:textbox></td>
                <td noWrap><ml:moneytextbox id=sFHAGiftFundAmt runat="server" width="100" preset="money" CssClass="mask" readonly="True" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; j. Asset
                  Available</td>
                <td noWrap><ml:moneytextbox id=sFHAAssetAvail runat="server" width="100" preset="money" CssClass="mask" readonly="True" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; k. 2nd
                  Mortgage (if applicable) Src:<asp:textbox id=sFHA2ndMSrc runat="server" Width="197px" tabindex=25></asp:textbox></td>
                <td noWrap><ml:moneytextbox id=sFHA2ndMAmt runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; l. Cash
                  Reserves (Sum 12h thru 12k, minus 12g)</td>
                <td noWrap><ml:moneytextbox id=sFHACashReserves runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>13. Monthly
                  Effective Income</td>
                <td noWrap><asp:checkbox id=sFHAIncomeLckd onclick="refreshCalculation();" runat="server" Text="Lock" tabindex=25></asp:checkbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Borrower's Base Pay</td>
                <td noWrap><ml:moneytextbox id=sFHABBaseI runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b.
                  Borrower's Other Earnings</td>
                <td noWrap><ml:moneytextbox id=sFHABOI runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Co-borrower's Base Pay</td>
                <td noWrap><ml:moneytextbox id=sFHACBaseI runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d.
                  Co-borrower's Other Earnings</td>
                <td noWrap><ml:moneytextbox id=sFHACOI runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e. Net
                  Income From Real Estate</td>
                <td noWrap><ml:moneytextbox id=sFHANetRentalI runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=25></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f. Gross
                  Monthly Income</td>
                <td noWrap><ml:moneytextbox id=sFHAGrossMonI runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=25></ml:moneytextbox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table14 cellSpacing=0 cellPadding=0
      width="98%" border=0>
        <tr>
          <td noWrap>
            <table id=Table1 cellSpacing=0 cellPadding=0 border=0
            >
              <tr>
                <td class=FieldLabel noWrap>14. Debts
                  &amp; Obligations <asp:CheckBox id=sFHADebtLckd onclick=refreshCalculation(); runat="server" Text="Lock" tabindex=30></asp:CheckBox> </td>
                <td class=FieldLabel noWrap>Monthly
                  Payment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                <td class=FieldLabel noWrap>Unpaid
                Balance</td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Total installment debt&nbsp;&nbsp; </td>
                <td noWrap><ml:moneytextbox id=sFHADebtInstallPmt runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td>
                <td noWrap><ml:moneytextbox id=sFHADebtInstallBal runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b.
                  Child Support, etc.</td>
                <td noWrap><ml:moneytextbox id=sFHAChildSupportPmt runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td>
                <td noWrap></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Other</td>
                <td noWrap><ml:moneytextbox id=sFHAOtherDebtPmt runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td>
                <td noWrap><ml:moneytextbox id=sFHAOtherDebtBal runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=30></ml:moneytextbox></td></tr>
              <TR>
                <TD class=FieldLabel
                  noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  Other (Negative Cashflow)</TD>
                <TD noWrap><ml:moneytextbox id=sFHANegCfRentalI tabIndex=30 runat="server" preset="money" width="100" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                <TD noWrap></TD></TR>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d.
                  Total monthly payments&nbsp;&nbsp;&nbsp;&nbsp; </td>
                <td noWrap><ml:moneytextbox id=sFHADebtPmtTot runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=30></ml:moneytextbox></td>
                <td noWrap ></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table15 cellSpacing=0 cellPadding=0
      width="98%" border=0>
        <tr>
          <td noWrap>
            <table id=Table2 cellSpacing=0 cellPadding=0 border=0
            >
              <tr>
                <td class=FieldLabel noWrap>15. Future
                  Monthly Payments&nbsp; </td>
                <td noWrap></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Principal &amp; Interest - 1st Mortgage&nbsp; </td>
                <td noWrap><ml:moneytextbox id=sFHAPro1stMPmt runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=35></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Monthly
                  MIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="sFHAProMInsLckd" runat="server" TabIndex="35" onclick="refreshCalculation();" Text="Lock"/></td>
                <td noWrap><ml:moneytextbox id=sFHAProMIns runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=35 /></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Homeowners Association Fee</td>
                <td noWrap><ml:moneytextbox id=sFHAProHoAssocDues runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=35></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Ground
                  Rent</td>
                <td noWrap><ml:moneytextbox id=sFHAProGroundRent runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=35></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e.
                  Principal &amp; Interest - 2nd Mortgage</td>
                <td noWrap><ml:moneytextbox id=sFHAPro2ndFinPmt runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=35></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f. Hazard
                  Insurance</td>
                <td noWrap><ml:moneytextbox id=sFHAProHazIns runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=35></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; g. Taxes
                  &amp; special assessments</td>
                <td noWrap><ml:moneytextbox id=sFHAProRealETx runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=35></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h. Total
                  mortgage payment</td>
                <td noWrap><ml:moneytextbox id=sFHAMonthlyPmt runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=35></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i.
                  Recurring expenses (from 14d)</td>
                <td noWrap><ml:moneytextbox id=sFHADebtPmtTot2 runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=35></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; j. Total
                  fixed payment</td>
                <td noWrap><ml:moneytextbox id=sFHAPmtFixedTot runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=35></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>16. Ratios</td>
                <td noWrap></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a.
                  Loan-to-value (11d divided by 11a)</td>
                <td noWrap><ml:percenttextbox id=sFHALtv2 runat="server" ReadOnly="True" width="100" preset="percent" CssClass="mask" tabindex=35></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b.
                  Mortgage payment-to-income (15h divided by 13f)&nbsp; </td>
                <td noWrap><ml:percenttextbox id=sFHAMPmtToIRatio runat="server" ReadOnly="True" width="100" preset="percent" CssClass="mask" tabindex=35></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Total
                  fixed payment-to-income (15j divided by 13f)</td>
                <td noWrap><ml:percenttextbox id=sFHAFixedPmtToIRatio runat="server" ReadOnly="True" width="100" preset="percent" CssClass="mask" tabindex=35></ml:percenttextbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap>17. Borrower
                  Rating (enter "A" for acceptable or "R" for reject)</td>
                <td noWrap></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Credit
                  characteristics</td>
                <td noWrap><asp:textbox id=sFHACreditRating onkeyup="validateAR(this, event);" runat="server" ReadOnly="True" Width="30px" tabindex=35></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b.
                  Adequacy of effective income</td>
                <td noWrap><asp:textbox id=sFHARatingIAdequacy onkeyup="validateAR(this, event);" runat="server" ReadOnly="True" Width="30px" tabindex=35></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c.
                  Stability of effective income</td>
                <td noWrap><asp:textbox id=sFHARatingIStability onkeyup="validateAR(this, event);" runat="server" ReadOnly="True" Width="30px" tabindex=35></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d.
                  Adequacy of available assets</td>
                <td noWrap><asp:textbox id=sFHARatingAssetAdequacy onkeyup="validateAR(this, event);" runat="server" ReadOnly="True" Width="30px" tabindex=35></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap></td>
                <td noWrap ></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table16 cellSpacing=0 cellPadding=0
      width="98%" border=0>
        <tr>
          <td noWrap>
            <table id=Table3 cellSpacing=0 cellPadding=0 border=0
            >
              <tr>
                <td class=FieldLabel noWrap>18. CAIVRS -
                  LDP/GSA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                <td class=FieldLabel noWrap>Borrower</td>
                <td class=FieldLabel noWrap >Co-borrower</td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  CAIVRS Number</td>
                <td noWrap><asp:textbox id=sFHABCaivrsNum runat="server" ReadOnly="True" Width="164px" tabindex=40></asp:textbox></td>
                <td noWrap><asp:textbox id=sFHACCaivrsNum runat="server" ReadOnly="True" Width="164px" tabindex=40></asp:textbox></td></tr>
              <tr>
                <td class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  LDP/GSA (page no. &amp; date)&nbsp;&nbsp; </td>
                <td noWrap><asp:textbox id=sFHABLpdGsa runat="server" ReadOnly="True" Width="164px" tabindex=40></asp:textbox></td>
                <td noWrap><asp:textbox id=sFHACLpdGsa runat="server" ReadOnly="True" Width="164px" tabindex=40></asp:textbox></td></tr></table></td></tr></table></td></tr>
  <tr>
    <td>
      <table class=InsetBorder id=Table17 cellSpacing=0 cellPadding=0
      width="98%" border=0>
        <tr>
          <td noWrap>
            <table id=Table4 cellSpacing=0 cellPadding=0 border=0
            >
              <tr>
                <td class=FieldLabel noWrap>Attachment A
                  Information</td>
                <td nowrap></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp; A1. Contract Sales
                  Price of Property (line 10a above)&nbsp; </td>
                <td nowrap><ml:moneytextbox id=sFHAPurchPrice2 runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=45></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp; A2. 6% of line A1</td>
                <td nowrap><ml:moneytextbox id=sPurchPrice6pc runat="server" ReadOnly="True" width="100" preset="money" CssClass="mask" tabindex=45></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp; A3. Total Seller
                  Contribution</td>
                <td nowrap><ml:moneytextbox id=sFHASellerContribution runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=45></ml:moneytextbox></td></tr>
              <tr>
                <td class=FieldLabel nowrap>&nbsp;&nbsp; A4. Excess
                  Contribution</td>
                <td nowrap><ml:moneytextbox id=sFHAExcessContribution runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex=45></ml:moneytextbox></td></tr></table></td></tr></table></td></tr>
				<tr>
					<td class="FieldLabel">Remarks</td>
				</tr>
				<tr>
					<td><asp:textbox id="sFHACreditAnalysisRemarks" runat="server" Width="580px" Height="160px" TextMode="MultiLine" tabindex=50></asp:textbox></td>
				</tr>
				<tr>
					<td></td>
				</tr></table>&nbsp; </form>
	</body>
</HTML>
