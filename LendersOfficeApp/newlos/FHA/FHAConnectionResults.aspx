﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FHAConnectionResults.aspx.cs" Inherits="LendersOfficeApp.newlos.FHA.FHAConnectionResults" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>FHA Connection Results Viewer</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <style type="text/css">
        body { background-color: gainsboro; }
        .Tab { display: none; padding-left: 10px;  background-color:gainsboro; }
        .visible { display: block; }
        #Tabs { background-color : black; margin-top: -1em; margin-bottom: 1em; }
        ul.TabNav { background-color: black; overflow:visible; }
        .centeringDiv { text-align: center; margin-top: 10px;}
        .centeringDiv input { margin: 0 auto; width: 50px;  }
        iframe { width: 98%; margin: 0 auto; background-color: gainsboro; height: 800px; }
    </style>
    
    <script type="text/javascript" src="../../inc/json.js"></script>
    <link type="text/css" rel="Stylesheet" href="../../css/pdfeditor.css" />
    <script type="text/javascript" src="../../inc/PdfEditor.js"></script>
</head>
<body>
    <form id="form1" runat="server">
            <div class="Tabs" id="Tabs">
                <ul class="tabnav">
                    <li id="CaseNumberAssignment" runat="server" class="selected"><a id="CaseNumberAssignmentBtn" href="#CaseNumberAssignmentTab">Case Number Assignment</a></li>
                    <li id="CaseQuery" runat="server"><a id="CaseQueryBtn" href="#CaseQueryTab" >Case Query</a></li>
                    <li id="CAIVRSAuthorization" runat="server"><a id="CAIVRSAuthorizationBtn" href="#CAIVRSAuthorizationTab" >CAIVRS Authorization</a></li>
                </ul>
            </div>
            
            <div id="CaseNumberAssignmentTab" class="Tab visible" runat="server">
                <asp:PlaceHolder runat="server" ID="PlaceHolderCaseNumber">
                    <input  type="hidden" value="-1" id="DocType1" />
                    <span id="DocTypeText1">None Selected</span>
                    [ <a href="#" onclick="selectDocType(1); return false;">select Doctype</a> ]
                    <input NoHighlight="NoHighlight" type="button" id="CreateBtn1" onclick="CreateEDoc(1);" value="Create EDoc" />
                    
                    <br />                    
                    
                </asp:PlaceHolder>
                <iframe id="CaseNumberAssignmentIFrame" runat="server"></iframe>
                <ml:EncodedLabel ID="CaseNumberAssignmentMissingText" runat="server"></ml:EncodedLabel>
            </div>
            
            <div id="CaseQueryTab" class="Tab" runat="server">
                <asp:PlaceHolder runat="server" ID="PlaceHolderCaseQuery">
                    <input type="hidden" value="-1" id="DocType2" />
                    <span id="DocTypeText2">None Selected</span>
                    [ <a href="#" onclick="selectDocType(2); return false;">select Doctype</a> ]
                    <input NoHighlight="NoHighlight" type="button" id="CreateBtn2" onclick="CreateEDoc(2);" value="Create EDoc" />
                    <br />                    
                </asp:PlaceHolder>
            
                <iframe id="CaseQueryIFrame" runat="server"></iframe>
                <ml:EncodedLabel ID="CaseQueryMissingText" runat="server"></ml:EncodedLabel>
            </div>
            
            <div id="CAIVRSAuthorizationTab" class="Tab" runat="server">
                <asp:PlaceHolder runat="server" ID="PlaceHolderCAIVRS">
                    <input type="hidden" value="-1" id="DocType3" />
                    <span id="DocTypeText3">None Selected</span>
                    [ <a href="#" onclick="selectDocType(3); return false;">select Doctype</a> ]
                    <input NoHighlight="NoHighlight" type="button" onclick="CreateEDoc(3);" id="CreateBtn3" value="Create EDoc" />
                <br />                    
                </asp:PlaceHolder>
            
                <iframe id="CAIVRSAuthorizationIFrame" runat="server"></iframe>
                <ml:EncodedLabel ID="CAIVRSAuthorizationMissingText" runat="server"></ml:EncodedLabel>
            </div>
    </form>
    
    <script type="text/javascript">
    
        function selectDocType(num) {
		    var docTypePickerLink = "/newlos/ElectronicDocs/DocTypePicker.aspx";
            var queryString = "?brokerid=" + <%= AspxTools.JsString(BrokerId) %>;
            showModal(docTypePickerLink + queryString, null, null, null, function(result){ 
                if (result && result.docTypeId && result.docTypeName) { // New doc type selected
                    document.getElementById('DocType' + num).value = result.docTypeId;
                    document.getElementById('DocTypeText' + num).textContent = result.docTypeName;     
                }
            },{ hideCloseButton: true });
        }
        
        function CreateEDoc(num){
            var doctype = document.getElementById('DocType' + num ).value;
            if( doctype == "-1") 
            {
                alert("Please pick a doctype.");
                return;
            }
            var data = {
               sLId : <%= AspxTools.JsString(LoanID) %>,
               DocTypeId : doctype,
               Num : num
            };
            
            var results = gService.utils.call("CreateFhaConEdoc", data);
            
            if( results.error ) {
                alert(results.UserMessage );
            }
            else {
                document.getElementById('CreateBtn' + num).disabled = true;
            }
            
            
        }

        
        
        $(function() {
            var iframeBuffer = 40;
            
            $('.Tabs a').click(function(e) {
                $('.tabnav li').removeClass('selected');
                $('div.Tab').hide();
                $(this).parent().addClass('selected');

                var iframe = $($(this).attr('href')).show().find('iframe');
                if (iframe.length) myResizeIframe(iframe[0].id, iframeBuffer);
                return false;
            });

            $('iframe').load(function() {
                myResizeIframe(this.id, iframeBuffer);
            });
            
            $(window).resize(function() {
                $('iframe').each(function() {
                    myResizeIframe(this.id, iframeBuffer);
                });
            });

            <%-- This is duplicated from f_resizeIframe in common.js, because half the time it doesn't show up when I load the page --%>
            function myResizeIframe(id, buffer) {
                if (!buffer) { buffer = 10; }
                var iframe = document.getElementById(id);
                var height = self.innerHeight || (document.documentElement.clientHeight || document.body.clientHeight);
                height -= pageY(iframe) + buffer;
                height = (height < 0) ? 0 : height;
                iframe.style.height = height + 'px';
            }
            
            var dest = <%= AspxTools.JsString(Destination) %>;
            if (dest == "casenum") {
                $('#CaseNumberAssignmentBtn').click();
            } else if (dest == "casequery") {
                $('#CaseQueryBtn').click();
            } else if (dest == "caivrs") {
                $('#CAIVRSAuthorizationBtn').click();
            } // else do nothing
            
        });
    </script>
</body>
</html>

