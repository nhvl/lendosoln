<%@ Register TagPrefix="UC" TagName="CFM" Src="../Status/ContactFieldMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="FHAAddendum.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHAAddendum" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
		<title>FHA Addendum</title>
    <style type="text/css">
        .Addendum2016 .indent td:first-child, .BorrowerCertificationTable .indent td:first-child{
            padding-left: 1.9em;
        }

        .Addendum2016 .indent2 td:first-child{
            padding-left: 3.8em;
        }
        
        .Addendum2010 .hud-92900-2016{
            display: none;
        }
        .Addendum2016 .hud-92900-2010{
            display: none;
        }
        #DirectEndorsementSection td:first-child{
            width: 350px;
            box-sizing:border-box;
        }

        .Addendum2010 .extended-section input{
            float:right;
        }

        .extended-section{
            white-space: nowrap;
        }

        .extended-section label {
            margin-right:.75em;
        }  
        .extended-section input:not(:last-child){
            margin-right:1.5em;
        }

        .align-label{
            width: 160px;
            display:inline-block;
        }

        .wide-table{
            width: 99%;
        }

        input#sFhaLoanOrigCompanyNm {
	        width: 331px;
        }
    </style>
  </HEAD>

<body class="RightBackground" MS_POSITIONING="FlowLayout">
<script type="text/javascript">
    function _init() {
      on_aVaVestTitleT_change();
      lockField(<%= AspxTools.JsGetElementById(sFHAProMInsLckd) %>, 'sFHAProMIns');
      lockField(<%= AspxTools.JsGetElementById(sFHAProMInsMonLckd) %>, 'sFHAProMInsMon');
      lockField(<%= AspxTools.JsGetElementById(sFHALDiscntLckd) %>, 'sFHALDiscnt');
      lockField(<%= AspxTools.JsGetElementById(sLenderCaseNumLckd) %>, 'sLenderCaseNum');
      lockField(<%= AspxTools.JsGetElementById(sFhaLoanOrigCompanyNmLckd) %>, 'sFhaLoanOrigCompanyNm');
      lockField(<%= AspxTools.JsGetElementById(sFhaSponsoredOriginatorEinLckd) %>, 'sFHASponsoredOriginatorEIN');
      lockField(<%= AspxTools.JsGetElementById(sFhaLoanOrigCompanyNmlsIdLckd) %>, 'sFhaLoanOrigCompanyNmlsId');
      
      document.getElementById('purposeOfLoanErrorMsg').innerText = "";
      var sFHAPurposeIsPurchaseExistHome = document.getElementById('sFHAPurposeIsPurchaseExistHome');
      var sFHAPurposeIsFinanceImprovement = document.getElementById('sFHAPurposeIsFinanceImprovement');
      var sFHAPurposeIsRefinance = document.getElementById('sFHAPurposeIsRefinance');
      var sFHAPurposeIsPurchaseNewCondo = document.getElementById('sFHAPurposeIsPurchaseNewCondo');
      var sFHAPurposeIsPurchaseExistCondo = document.getElementById('sFHAPurposeIsPurchaseExistCondo');
      var sFHAPurposeIsPurchaseNewHome = document.getElementById('sFHAPurposeIsPurchaseNewHome');
      var sFHAPurposeIsConstructHome = document.getElementById('sFHAPurposeIsConstructHome');
      var sFHAPurposeIsFinanceCoopPurchase = document.getElementById('sFHAPurposeIsFinanceCoopPurchase');
      var sFHAPurposeIsPurchaseManufacturedHome = document.getElementById('sFHAPurposeIsPurchaseManufacturedHome');
      var sFHAPurposeIsManufacturedHomeAndLot = document.getElementById('sFHAPurposeIsManufacturedHomeAndLot');
      var sFHAPurposeIsRefiManufacturedHomeToBuyLot = document.getElementById('sFHAPurposeIsRefiManufacturedHomeToBuyLot');
      var sFHAPurposeIsRefiManufacturedHomeOrLotLoan = document.getElementById('sFHAPurposeIsRefiManufacturedHomeOrLotLoan');
      
      var lPurposeDDL = <%= AspxTools.JsGetElementById(sLPurposeT) %>;
      var lPurpose = lPurposeDDL.options[lPurposeDDL.selectedIndex].value;
      var gseSpDDL = <%= AspxTools.JsGetElementById(sGseSpT) %>;
      var gseSp = gseSpDDL.options[gseSpDDL.selectedIndex].value;
      var fhaConstructionDDL = <%= AspxTools.JsGetElementById(sFHAConstructionT) %>;
      var fhaConstruction = fhaConstructionDDL.options[fhaConstructionDDL.selectedIndex].value;
      var ltDDL = <%= AspxTools.JsGetElementById(sLT) %>;
      var lt = ltDDL.options[ltDDL.selectedIndex].value;
      
      var ConstructionError = 'Construction type is required for purchase loans';
      var PropertyError = 'Property type is required for purchase loans';
      var OtherError = 'Other is not an acceptable purpose type for this form';
        
      sFHAPurposeIsPurchaseExistHome.disabled = true;
      sFHAPurposeIsFinanceImprovement.disabled = true;
      sFHAPurposeIsRefinance.disabled = true;
      sFHAPurposeIsPurchaseNewCondo.disabled = true;
      sFHAPurposeIsPurchaseExistCondo.disabled = true;
      sFHAPurposeIsPurchaseNewHome.disabled = true;
      sFHAPurposeIsConstructHome.disabled = true;
      sFHAPurposeIsFinanceCoopPurchase.disabled = true;
      sFHAPurposeIsPurchaseManufacturedHome.disabled = true;
      sFHAPurposeIsManufacturedHomeAndLot.disabled = true;
      sFHAPurposeIsRefiManufacturedHomeToBuyLot.disabled = true;
      sFHAPurposeIsRefiManufacturedHomeOrLotLoan.disabled = true;
      
        // Note:  The init function is run every time refreshCalculation is run.  Do not use Jquery event handlers
        // here unless you take extra care to ensure that handler isn't added multiple times.
        // Alternatively, you can add the jQuery handlers in the jQuery initialization function below, which will only run once
        // at the beginning.

      document.getElementById("sFHARatedAcceptedByTotalScorecard").onclick = function(){setOpposite("Accepted");};
      document.getElementById("sFhaDirectEndorsementWasRatedAcceptAndUnderwritten").onclick = function(){setOpposite("Accepted");};
      document.getElementById("sFHARatedReferByTotalScorecard").onclick = function(){setOpposite("Refer");};

      <%-- // Removed in OPM 468575, will be re-added in OPM 468765
      AFhaBorrCertOtherPropToBeSoldTriChange(); --%>

      if(lt == '<%= AspxTools.JsNumeric(Convert.ToInt32(E_sLT.VA)) %>') 
      {
        // For VA Loan types
        if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Purchase)) %>)
        {
          
          if (gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.Cooperative)) %>)
          {
            sFHAPurposeIsFinanceCoopPurchase.disabled = false;
          }
          else if (gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHomeMultiwide)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHousing)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHousingSingleWide)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHomeCondominium)) %>)
          {
            sFHAPurposeIsPurchaseManufacturedHome.disabled = false;
            sFHAPurposeIsManufacturedHomeAndLot.disabled = false;
          }
          else if (gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.Attached)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.Detached)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.Modular)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.PUD)) %>)
          {
            if (fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.Existing)) %>)
            {
              sFHAPurposeIsPurchaseExistHome.disabled = false;
            }
            else if (fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.New)) %>
                  || fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.Proposed)) %>)
            {
              sFHAPurposeIsPurchaseNewHome.disabled = false;
            }
            else
            {
              displayPurposeOfLoanError(ConstructionError);
            }
          }
          else if (gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.Condominium)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.DetachedCondominium)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.HighRiseCondominium)) %>)
          {
            if (fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.Existing)) %>)
            {
              sFHAPurposeIsPurchaseExistCondo.disabled = false;
            }
            else if (fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.New)) %>
                  || fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.Proposed)) %>)
            {
              sFHAPurposeIsPurchaseNewCondo.disabled = false;
            }
            else
            {
              displayPurposeOfLoanError(ConstructionError);
            }
          }
          else
          {
            displayPurposeOfLoanError(PropertyError);
          }
        }
        else if ((lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Refin)) %>) 
              || (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.RefinCashout)) %>))
        {
          sFHAPurposeIsRefinance.disabled = false;
          sFHAPurposeIsFinanceImprovement.disabled = false;
          if (gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHomeMultiwide)) %>
           || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHousing)) %>
           || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHousingSingleWide)) %>
           || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHomeCondominium)) %>)
          {
            sFHAPurposeIsRefiManufacturedHomeToBuyLot.disabled = false;
            sFHAPurposeIsRefiManufacturedHomeOrLotLoan.disabled = false;
          }
        }
        else if ((lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Construct)) %>) 
              || (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.ConstructPerm)) %>))
        {
          sFHAPurposeIsConstructHome.disabled = false;
        }
        else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.FhaStreamlinedRefinance)) %>)
        {
          sFHAPurposeIsRefinance.disabled = false;
        }
        else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.VaIrrrl)) %>)
        {
          sFHAPurposeIsRefinance.disabled = false;
          sFHAPurposeIsFinanceImprovement.disabled = false;
        }
        else
        {
          displayPurposeOfLoanError(OtherError);
        }
      } 
      else 
      {
        // For non VA Loan types
        if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Purchase)) %>)
        {
          if (gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.Cooperative)) %>)
          {
            sFHAPurposeIsFinanceCoopPurchase.disabled = false;
          }
          else if (gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.Attached)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.Detached)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHomeMultiwide)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHousing)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHousingSingleWide)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.ManufacturedHomeCondominium)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.Modular)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.PUD)) %>)
          {
            if (fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.Existing)) %>)
            {
              sFHAPurposeIsPurchaseExistHome.disabled = false;
            }
            else if (fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.New)) %>
                  || fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.Proposed)) %>)
            {
              sFHAPurposeIsPurchaseNewHome.disabled = false;
            }
            else
            {
              displayPurposeOfLoanError(ConstructionError);
            }
          }
          else if (gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.Condominium)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.DetachedCondominium)) %>
                || gseSp == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sGseSpT.HighRiseCondominium)) %>)
          {
            if (fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.Existing)) %>)
            {
              sFHAPurposeIsPurchaseExistCondo.disabled = false;
            }
            else if (fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.New)) %>
                  || fhaConstruction == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sFHAConstructionT.Proposed)) %>)
            {
              sFHAPurposeIsPurchaseNewCondo.disabled = false;
            }
            else
            {
              displayPurposeOfLoanError(ConstructionError);
            }
          }
          else
          {
            displayPurposeOfLoanError(PropertyError);
          }
        }
        else if ((lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Refin)) %>) 
              || (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.RefinCashout)) %>))
        {
          sFHAPurposeIsRefinance.disabled = false;
          sFHAPurposeIsFinanceImprovement.disabled = false;
        }
        else if ((lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Construct)) %>) 
              || (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.ConstructPerm)) %>))
        {
          sFHAPurposeIsConstructHome.disabled = false;
        }
        else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.FhaStreamlinedRefinance)) %>)
        {
          sFHAPurposeIsRefinance.disabled = false;
        }
        else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.VaIrrrl)) %>)
        {
          sFHAPurposeIsRefinance.disabled = false;
          sFHAPurposeIsFinanceImprovement.disabled = false;
        }
        else
        {
          displayPurposeOfLoanError(OtherError);
        }
      }
    }
    
    function setOpposite(name)
    {
        var acceptedEl = document.getElementById("sFHARatedAcceptedByTotalScorecard");
        var deAcceptEl = document.getElementById("sFhaDirectEndorsementWasRatedAcceptAndUnderwritten");
        var referEl = document.getElementById("sFHARatedReferByTotalScorecard");
        if(name=="Accepted" && (acceptedEl.checked) )
        {
            referEl.checked = false;
            deAcceptEl.disabled = false;
        }
        else if(name == "Accepted" && (!acceptedEl.checked)){
            deAcceptEl.checked = false;
            deAcceptEl.disabled = true;
        }
        else if(referEl.checked){
            acceptedEl.checked = false;
            deAcceptEl.checked = false;
            deAcceptEl.disabled = true;
        }
        
        refreshCalculation();
    }
    
    function displayPurposeOfLoanError(error) {
      document.getElementById('purposeOfLoanErrorMsg').innerText = error;
    }
    
    function updateBorrowerName() {
      var name = <%= AspxTools.JsGetElementById(aBLastNm) %>.value + ", " + <%= AspxTools.JsGetElementById(aBFirstNm) %>.value;
      parent.info.f_updateApplicantDDL(name, <%= AspxTools.JsString(ApplicationID) %> );
    }    
    function on_aVaVestTitleT_change() {
      var b = <%= AspxTools.JsGetElementById(aVaVestTitleT) %>.value == <%= AspxTools.JsString(E_aVaVestTitleT.Other.ToString("D")) %>;
      <%= AspxTools.JsGetElementById(aVaVestTitleODesc) %>.readOnly = !b;
    }
  function f_displayAmortization() {
    PolyShouldShowConfirmSave(isDirty(), function(){
        showModal('/newlos/Forms/AmortizationTable.aspx?loanid=' + <%=AspxTools.JsString(LoanID) %>, null, null, null, null, { hideCloseButton: true });
    },saveMe);
  }   
  
      function manualEdit(type) 
    {
        var elem;
        if(type=="name")
            elem = document.getElementById("sFHAAddendumUnderwriterNameLckd");
        else if (type == "license")
            elem = document.getElementById("sFHAAddendumUnderwriterChumsIdLckd");
        else if(type == "mortgagee")
            elem = document.getElementById("sFHAAddendumMortgageeNameLckd");

        elem.checked = true;
    }  
    $(function(){
        var loanTypeDDL = document.getElementById('sLT');
        $(loanTypeDDL).change(f_on_sLT_change);
        f_on_sLT_change();
        
        function f_on_sLT_change()
        {
            var isVA = ($(loanTypeDDL).val() == '<%= AspxTools.JsNumeric(Convert.ToInt32(E_sLT.VA)) %>')
            var isFha = ($(loanTypeDDL).val() == '<%= AspxTools.JsNumeric(Convert.ToInt32(E_sLT.FHA)) %>') 

            $('#sFHALenderIdCode').toggle(false == isVA);
            $('#sFHASponsorAgentIdCode').toggle(false == isVA);
            $('#sVALenderIdCode').toggle(isVA);
            $('#sVASponsorAgentIdCode').toggle(isVA);
            $("#label_aFhaBorrCertWillOccupyFor1YearTri_Yes").prop("disabled", !isFha);
            $("#label_aFhaBorrCertWillOccupyFor1YearTri_No").prop("disabled", !isFha);

            <%-- // Removed in OPM 468575, will be re-added in OPM 468765
            $("input[id^='aFHABorrCertOwnOrSoldOtherFHAPropTri']").change(AFhaBorrCertOtherPropToBeSoldTriChange); --%>
            $("input[name='aFHABorrCertOcc'").prop("disabled", !isVA);
        }

        $("#sCaseAssignmentD").blur(CheckAddendumDate);
    });
    <%-- // Removed in OPM 468575 (Kevin said to let it be for now), will be re-added in OPM 468765
    function AFhaBorrCertOtherPropToBeSoldTriChange() {
        var yesCheckbox = $('#aFHABorrCertOwnOrSoldOtherFHAPropTri_0');
        var noCheckbox = $('#aFHABorrCertOwnOrSoldOtherFHAPropTri_1');

        if (yesCheckbox.is(this) && yesCheckbox.prop('checked')) {
            noCheckbox.prop('checked', false);
        }
        else if (noCheckbox.is(this) && noCheckbox.prop('checked')) {
            yesCheckbox.prop('checked', false);
        }

        var naCheckbox =  $('#aFhaBorrCertOtherPropToBeSoldNA');
        if (yesCheckbox.prop('checked')){
            naCheckbox.prop('disabled', true);
            naCheckbox.prop('checked', false);
            $("[id^='aFHABorrCertOtherPropToBeSoldTri']").prop('disabled', false);
        }
        else if (noCheckbox.prop("checked"))
        {
            naCheckbox.prop('disabled', false);
            $("[id^='aFHABorrCertOtherPropToBeSoldTri']").prop('checked', false);
            $("[id^='aFHABorrCertOtherPropToBeSoldTri']").prop('disabled', true);
            naCheckbox.prop('checked', true);
        }
        else
        {
            $("[id^='aFHABorrCertOtherPropToBeSoldTri']").prop('disabled', false);
            naCheckbox.prop("checked", false);
        }
    }
    --%>
    function CheckAddendumDate(e){
        window.setTimeout(function(){
            var el = $(e.currentTarget);
            var addendumD = new Date("8/1/2016");

            var assignmentD;
            var assignmentDVal = el.val();

            if(assignmentDVal == ""){
                assignmentD = new Date();
            }
            else{
                assignmentD = new Date(assignmentDVal);
            }

            var form = $("form");
            form.removeClass("Addendum2016");
            form.removeClass("Addendum2010");

            if(assignmentD.getTime() >= addendumD.getTime()){
                form.addClass("Addendum2016");
            }
            else
            {
                form.addClass("Addendum2010");
            }

            refreshCalculation();
        }, 0);
    }
</script>
		<form id="FHAAddendumForm" method="post" runat="server">
		<asp:CheckBox runat="server" style="display:none;" ID="sFHAAddendumUnderwriterChumsIdLckd" />
		<asp:CheckBox runat="server" style="display:none;" ID="sFHAAddendumUnderwriterNameLckd" />
		<asp:CheckBox runat="server" style="display:none;" id="sFHAAddendumMortgageeNameLckd" />
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<TD class="MainRightHeader" noWrap>FHA / VA Addendum</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder wide-table" id="Table2" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap colSpan="2">1. Loan Type
									<asp:dropdownlist id="sLT" tabIndex="1" runat="server" onchange="refreshCalculation();" Width="135px"></asp:dropdownlist></TD>
								<TD noWrap></TD>
								<TD class="FieldLabel" noWrap>3. Lender's Case Number</TD>
								<TD noWrap><asp:CheckBox ID="sLenderCaseNumLckd" TabIndex="3" runat="server" onclick="refreshCalculation();"/><asp:textbox id="sLenderCaseNum" tabIndex="3" runat="server"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>2. Agency Case Number</TD>
								<TD noWrap><asp:textbox id="sAgencyCaseNum" tabIndex="2" runat="server"></asp:textbox></TD>
								<TD noWrap></TD>
								<TD class="FieldLabel" noWrap>4. Section of the Act</TD>
								<TD noWrap><ml:combobox id="sFHAHousingActSection" tabIndex="4" runat="server" Width="70px" DESIGNTIMEDRAGDROP="28"></ml:combobox></TD>
							</TR>
							<tr>
							    <TD class="FieldLabel" noWrap><span style="visibility:hidden">X. </span>Case Assignment Date</TD>
								<TD noWrap><ml:DateTextBox ID="sCaseAssignmentD" runat="server" preset="date" Width="75" CssClass="mask"></ml:DateTextBox></TD>
								<td colspan=2></td>
							</tr>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder wide-table" id="Table3" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap>5. General Information</TD>
								<TD noWrap></TD>
								<TD noWrap></TD>
								<TD noWrap></TD>
								<TD noWrap></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>Borrower First Name</TD>
								<TD noWrap><asp:textbox id="aBFirstNm" tabIndex="5" runat="server" maxlength="21" onchange="updateBorrowerName();"></asp:textbox></TD>
								<TD noWrap>
								</TD>
								<TD class="FieldLabel" noWrap>Coborrower&nbsp;First Name</TD>
								<TD noWrap><asp:textbox id="aCFirstNm" tabIndex="9" runat="server" maxlength="21"></asp:textbox></TD>
							</TR>
							<tr>
								<td class="FieldLabel" noWrap>Borrower Middle Name</td>
								<td noWrap><asp:textbox id="aBMidNm" tabIndex="6" runat="server" maxlength="21"></asp:textbox></td>
								<td noWrap></td>
								<td class="FieldLabel" noWrap>Coborrower Middle Name</td>
								<td noWrap><asp:textbox id="aCMidNm" tabIndex="10" runat="server" maxlength="21"></asp:textbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>Borrower Last Name</td>
								<td noWrap><asp:textbox id="aBLastNm" tabIndex="7" runat="server" maxlength="21" onchange="updateBorrowerName();"></asp:textbox></td>
								<td noWrap></td>
								<td class="FieldLabel" noWrap>Coborrower Last Name</td>
								<td noWrap><asp:textbox id="aCLastNm" tabIndex="11" runat="server" maxlength="21"></asp:textbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>Borrower Suffix</td>
								<td noWrap><ml:ComboBox id="aBSuffix" tabIndex="8" runat="server" maxlength="21"></ml:ComboBox></td>
								<td noWrap></td>
								<td class="FieldLabel" noWrap>Coborrower Suffix</td>
								<td noWrap><ml:ComboBox id="aCSuffix" tabIndex="12" runat="server" maxlength="21"></ml:ComboBox></td>
							</tr>
							<TR>
								<TD class="FieldLabel" noWrap>Present Address</TD>
								<TD noWrap colSpan="4"><asp:textbox id="aBAddr" tabIndex="13" runat="server" Width="331px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD noWrap></TD>
								<TD noWrap colSpan="4"><asp:textbox id="aBCity" tabIndex="14" runat="server" Width="231px"></asp:textbox><ml:statedropdownlist id="aBState" tabIndex="15" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="aBZip" tabIndex="16" runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
            <table class="InsetBorder wide-table" id="Table4" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td class="FieldLabel" nowrap>6. Property Address</td>
                <td nowrap></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>Street</td>
                <td nowrap>
                  <asp:TextBox ID="sSpAddr" TabIndex="17" runat="server" Width="331px" MaxLength="60"></asp:TextBox></td>
              </tr>
              <tr>
                <td nowrap></td>
                <td nowrap>
                  <asp:TextBox ID="sSpCity" TabIndex="18" runat="server" Width="231px"></asp:TextBox><ml:StateDropDownList ID="sSpState" TabIndex="19" runat="server"></ml:StateDropDownList>
                  <ml:ZipcodeTextBox ID="sSpZip" TabIndex="20" runat="server" Width="50" preset="zipcode"></ml:ZipcodeTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap colspan="2">Legal Description</td>
                <td nowrap></td>
              </tr>
              <tr>
                <td nowrap colspan="2">
                  <asp:TextBox ID="sSpLegalDesc" TabIndex="21" runat="server" Width="455px" TextMode="MultiLine" Height="70px"></asp:TextBox></td>
              </tr>
            </table>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
            <table class="InsetBorder wide-table" id="Table5" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td class="FieldLabel" nowrap>7. Loan Amount</td>
                <td nowrap><ml:MoneyTextBox ID="sFinalLAmt" TabIndex="22" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
                <td nowrap> </td>
                <td class="FieldLabel" nowrap>10. Discount Amount</td>
                <td nowrap><asp:CheckBox ID="sFHALDiscntLckd" TabIndex="26" runat="server" onclick="refreshCalculation();"/><ml:MoneyTextBox ID="sFHALDiscnt" TabIndex="26" runat="server" Width="90" preset="money"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>8. Interest Rate</td>
                <td nowrap>
                  <ml:PercentTextBox ID="sNoteIR" TabIndex="23" runat="server" Width="70" preset="percent"></ml:PercentTextBox></td>
                <td nowrap></td>
                <td class="FieldLabel" nowrap>11. Amount of UFP</td>
                <td nowrap><ml:MoneyTextBox ID="sFfUfmip1003" TabIndex="27" runat="server" Width="90" preset="money" ReadOnly="True"></ml:MoneyTextBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>9. Maturity</td>
                <td nowrap>
                  <asp:TextBox ID="sTermInYr" TabIndex="24" runat="server" Width="34px" ReadOnly="True"></asp:TextBox>yrs
                  <asp:TextBox ID="sTermInYrRemainingMonths" TabIndex="25" runat="server" Width="34px" ReadOnly="True"></asp:TextBox>mos</td>
                <td nowrap></td>
                <td class="FieldLabel" nowrap>12. Monthly Premium</td>
                <td nowrap><asp:CheckBox ID="sFHAProMInsLckd" TabIndex="28" runat="server" onclick="refreshCalculation();"/><ml:MoneyTextBox ID="sFHAProMIns" TabIndex="29" runat="server" Width="90" preset="money" /></td>
              </tr>
              <tr>
                <td nowrap></td>
                <td nowrap></td>
                <td nowrap></td>
                <td class="FieldLabel" nowrap> Term of monthly Prem.</td>
                <td nowrap>
                  <asp:CheckBox ID="sFHAProMInsMonLckd" TabIndex="28" runat="server" onclick="refreshCalculation();"/>
                  <asp:TextBox ID="sFHAProMInsMon" TabIndex="30" runat="server" Width="34px"></asp:TextBox>mos<input type="button" nohighlight value="Amortization Table" onclick="f_displayAmortization();"></td>
              </tr>
            </table>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder wide-table" id="Table6" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap>13. Lender's ID Code
									<asp:TextBox ID="sFHALenderIdCode" tabIndex="32" runat="server" Width="100px"></asp:TextBox>
									<asp:TextBox ID="sVALenderIdCode"  tabIndex="32" runat="server" Width="100px"></asp:TextBox>
								</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>14. Sponsor/Agent ID Code
									<asp:textbox id="sFHASponsorAgentIdCode" tabIndex="33" runat="server" Width="70px"></asp:textbox>
									<asp:TextBox id="sVASponsorAgentIdCode" TabIndex="33" runat="server" Width="70px"></asp:TextBox>
									<uc:CFM name=CFM1 runat="server" Type="10" AgentLicenseField="sFHASponsorAgentIdCode" CompanyNameField="FHAAddendumSponsorCompanyName" StreetAddressField="FHAAddendumSponsorStreetAddr" CityField="FHAAddendumSponsorCity" StateField="FHAAddendumSponsorState" ZipField="FHAAddendumSponsorZip" id=CFM1></uc:CFM>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<td nowrap>
						<table class="InsetBorder wide-table" id="Table7" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td class="FieldLabel" nowrap colspan="2">15. Lender</td>
							</tr>
							<tr>
								<td class="FieldLabel" nowrap>Name</td>
								<td nowrap><asp:textbox id="FHAAddendumLenderCompanyName" tabIndex="34" runat="server"></asp:textbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" nowrap>Address</td>
								<td nowrap><asp:textbox id="FHAAddendumLenderStreetAddr" tabIndex="35" runat="server" Width="331px"></asp:textbox></td>
							</tr>
							<tr>
								<td nowrap></td>
								<td nowrap><asp:textbox id="FHAAddendumLenderCity" tabIndex="36" runat="server" Width="231px"></asp:textbox><ml:statedropdownlist id="FHAAddendumLenderState" tabIndex="37" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="FHAAddendumLenderZip" tabIndex="38" runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td nowrap>
						<table class="InsetBorder wide-table" id="Table8" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td class="FieldLabel" nowrap colspan="2">16. Sponsor/Agent 
									<uc:CFM name=CFM2 runat="server" Type="10" AgentLicenseField="sFHASponsorAgentIdCode" CompanyNameField="FHAAddendumSponsorCompanyName" StreetAddressField="FHAAddendumSponsorStreetAddr" CityField="FHAAddendumSponsorCity" StateField="FHAAddendumSponsorState" ZipField="FHAAddendumSponsorZip" id=CFM2></uc:CFM>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel" nowrap>Name</td>
								<td nowrap><asp:textbox id="FHAAddendumSponsorCompanyName" tabIndex="40" runat="server"></asp:textbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" nowrap>Address</td>
								<td nowrap><asp:textbox id="FHAAddendumSponsorStreetAddr" tabIndex="41" runat="server" Width="331px"></asp:textbox></td>
							</tr>
							<tr>
								<td nowrap></td>
								<td nowrap><asp:textbox id="FHAAddendumSponsorCity" tabIndex="42" runat="server" Width="231px"></asp:textbox><ml:statedropdownlist id="FHAAddendumSponsorState" tabIndex="43" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="FHAAddendumSponsorZip" tabIndex="44" runat="server" width="50" preset="zipcode"></ml:zipcodetextbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder wide-table" id="Table9" cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td class="FieldLabel" nowrap>17. Lender's Telephone Number
									<ml:phonetextbox id="FHAAddendumLenderPhoneOfCompany" tabIndex="45" runat="server" width="120" preset="phone"></ml:phonetextbox></td>
							</tr>
                        </table>
                        <table class="InsetBorder wide-table" cellSpacing="0" cellPadding="0" border="0">
                            <tr>
                                <td class="FieldLabel">FHA Sponsored Originations</td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Name of Loan Origination Company
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="sFhaLoanOrigCompanyNm" MaxLength="50"></asp:TextBox>
                                    <asp:CheckBox runat="server" ID="sFhaLoanOrigCompanyNmLckd" onclick="refreshCalculation();" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    Tax ID of Loan Origination Company
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="sFHASponsoredOriginatorEIN"></asp:TextBox>
                                    <asp:CheckBox runat="server" ID="sFhaSponsoredOriginatorEinLckd" onclick="refreshCalculation();" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    NMLS ID of Loan Origination Company
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="sFhaLoanOrigCompanyNmlsId" MaxLength="12"></asp:TextBox>
                                    <asp:CheckBox runat="server" ID="sFhaLoanOrigCompanyNmlsIdLckd" onclick="refreshCalculation();" />
                                </td>
                            </tr>
                        </table>
                        <table class="InsetBorder wide-table" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap>18. First time homebuyer?
									<asp:CheckBoxList id="aHas1stTimeBuyerTri" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" tabindex="46"></asp:CheckBoxList></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>19. VA Only: Title will be vested as
									<asp:DropDownList id="aVaVestTitleT" runat="server" onchange="on_aVaVestTitleT_change();" tabindex="47"></asp:DropDownList><asp:TextBox id="aVaVestTitleODesc" runat="server" tabindex="48"></asp:TextBox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>20. Purpose of loan</TD>
							</TR>
							<TR>
								<TD noWrap>
									<table id="Table13" style="margin-left:10px">
									    <tr><td colspan="4"><table style="margin-left:3px">
									        <tr>
									            <td class="FieldLabel" noWrap>Loan Purpose <asp:DropDownList ID="sLPurposeT" onchange="refreshCalculation();" runat="server"></asp:DropDownList></td>
									            <td class="FieldLabel" noWrap>Construction <asp:DropDownList ID="sFHAConstructionT" onchange="refreshCalculation();" runat="server"></asp:DropDownList></td>
									        </tr>									    
									        <tr>
									            <td class="FieldLabel" noWrap>Property Type <asp:DropDownList ID="sGseSpT" onchange="refreshCalculation();" runat="server"></asp:DropDownList></td>
									        </tr>
									    </table></td></tr>
										<tr>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsPurchaseExistHome" tabIndex="49" runat="server" Text="Purchase Home (Prev Occupied)" /></td>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsConstructHome" tabIndex="55" runat="server" Text="Construct Home" /></td>
										</tr>
										<tr>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsFinanceImprovement" tabIndex="50" runat="server" Text="Finance Improvement" /></td>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsFinanceCoopPurchase" tabIndex="56" runat="server" Text="Finance Co-op Purchase" /></td>
										</tr>
										<tr>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsRefinance" tabIndex="51" runat="server" Text="Refinance" /></td>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsPurchaseManufacturedHome" tabIndex="57" runat="server" Text="Purchase Manufactured Home" /></td>
										</tr>
										<tr>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsPurchaseNewCondo" tabIndex="52" runat="server" Text="Purchase New Condo Unit" /></td>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsManufacturedHomeAndLot" tabIndex="58" runat="server" Text="Purchase Manufactured Home &amp; Lot" /></td>
										</tr>
										<tr>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsPurchaseExistCondo" tabIndex="53" runat="server" Text="Purchase Existing Condo Unit" /></td>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsRefiManufacturedHomeToBuyLot" tabIndex="59" runat="server" Text="Refi Manufactured Home to buy lot" /></td>
										</tr>
										<tr>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsPurchaseNewHome" tabIndex="54" runat="server" Text="Purchase Home (Not Prev Occupied)" /></td>
											<td noWrap><asp:radiobutton GroupName="sFHAPurposeOfLoanOption" id="sFHAPurposeIsRefiManufacturedHomeOrLotLoan" tabIndex="60" runat="server" Text="Refi Manufactured Home/Lot Loan" /></td>
										</tr>
										<tr>
										    <td noWrap colspan="2"><div style="color:Red" id="purposeOfLoanErrorMsg"></div></td>
										</tr>
									</table>
								</TD>
							</TR>
						</table>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE id="Table10" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap>21h. Duly Authorized Agents Info:</TD>
								<TD noWrap></TD>
							</TR>
							<tr>
								<td class="FieldLabel" nowrap> Name</td>
								<td nowrap><asp:TextBox id="FHAAddendumDulyAgentPreparerName" runat="server" Width="252px" TabIndex="61"></asp:TextBox></td>
							</tr>
							<tr>
								<td class="FieldLabel" nowrap> Address</td>
								<td nowrap><asp:TextBox id="FHAAddendumDulyAgentStreetAddr" runat="server" Width="252px" TabIndex="62"></asp:TextBox></td>
							</tr>
							<tr>
								<td class="FieldLabel" nowrap></td>
								<td nowrap><asp:TextBox id="FHAAddendumDulyAgentCity" runat="server" TabIndex="63"></asp:TextBox><ml:StateDropDownList id="FHAAddendumDulyAgentState" runat="server" TabIndex="64"></ml:StateDropDownList><ml:ZipcodeTextBox id="FHAAddendumDulyAgentZip" runat="server" preset="zipcode" width="50" TabIndex="65"></ml:ZipcodeTextBox></td>
							</tr>
							<tr>
								<td class="FieldLabel" nowrap> Function
								</td>
								<td nowrap><asp:TextBox id="FHAAddendumDulyAgentTitle" runat="server" Width="252px" tabindex="66"></asp:TextBox></td>
							</tr>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD noWrap>
						<TABLE class="InsetBorder wide-table" id="BorrowerCertificationTable" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="LoanFormHeader" noWrap>Borrower Certification</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>22a. Own/sold other real estate...?
									<asp:checkboxlist id="aFHABorrCertOwnOrSoldOtherFHAPropTri" tabIndex="67" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:checkboxlist>
								</TD>
							</TR>
							<TR class="indent">
								<TD class="FieldLabel" noWrap> to be sold?
									<asp:checkboxlist id="aFHABorrCertOtherPropToBeSoldTri" tabIndex="68" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:checkboxlist>
                                    <span class="hud-92900-2016">
                                        <asp:CheckBox Text="NA" ID="aFhaBorrCertOtherPropToBeSoldNA" disabled="true" TabIndex ="67" runat="server" />
                                    </span>
								</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>22b. Sales Price
									<ml:moneytextbox id="aFHABorrCertOtherPropSalesPrice" tabIndex="69" runat="server" DESIGNTIMEDRAGDROP="1472" width="110px" preset="money"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>22c. Original Mtg Amount
									<ml:moneytextbox id="aFHABorrCertOtherPropOrigMAmt" tabIndex="70" runat="server" width="110px" preset="money"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>22d. Address
									<asp:textbox id="aFHABorrCertOtherPropStAddr" tabIndex="71" runat="server" Width="331px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD noWrap><asp:textbox id="aFHABorrCertOtherPropCity" tabIndex="72" runat="server" Width="231px"></asp:textbox><ml:statedropdownlist id="aFHABorrCertOtherPropState" tabIndex="73" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="aFHABorrCertOtherPropZip" tabIndex="74" runat="server" width="56px" preset="zipcode"></ml:zipcodetextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>22e. If the dwelling to be covered by...?
									<asp:checkboxlist id="aFHABorrCertOtherPropCoveredByThisLoanTri" tabIndex="75" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:checkboxlist></TD>
							</TR>
							<TR class="hud-92900-2010">
								<TD class="FieldLabel" noWrap>22f. Own more than 4 dwellings?
									<asp:checkboxlist id="aFHABorrCertOwnMoreThan4DwellingsTri" tabIndex="78" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:checkboxlist></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>23. Ever had V.A. home loan?
									<asp:CheckBoxList id="aVaBorrCertHadVaLoanTri" tabIndex="79" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"></asp:CheckBoxList></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>25. Undersigned Borrower(s) Certify that:</TD>
							</TR>
                            
							<TR class="indent">
								<TD noWrap class="indent">
									 (2) Occupancy: (for HUD only)</TD>
							</TR>
                            <tr class="hud-92900-2016 indent">
                                <td>
									<asp:RadioButton Enabled="false" id="aFhaBorrCertWillOccupyFor1YearTri_Yes" GroupName="aFhaBorrCertWillOccupyFor1YearTri" value="Yes" tabIndex="81" runat="server"></asp:RadioButton>
                                    <label id="label_aFhaBorrCertWillOccupyFor1YearTri_Yes">I, the Borrower or Co-Borrower, will occupy the property...</label>
                                </td>
                            </tr>
                            <tr class="hud-92900-2016 indent">
                                <td>
									<asp:RadioButton Enabled="false" id="aFhaBorrCertWillOccupyFor1YearTri_No" GroupName="aFhaBorrCertWillOccupyFor1YearTri" value="No" tabIndex="81" runat="server"></asp:RadioButton>
                                    <label id="label_aFhaBorrCertWillOccupyFor1YearTri_No">I do not intend to occupy the property as my primary residence</label>
                                </td>
                            </tr>
							<TR class="indent">
								<TD noWrap class="indent">
									 Occupancy: (for VA only)</TD>
							</TR>
							<tr class="indent section-25-2">
								<td nowrap>
    								<asp:RadioButton id="aFHABorrCertOccIsAsHome" GroupName="aFHABorrCertOcc" tabIndex="80" runat="server" Text="(a) I now actually occupy the above property as my home..."></asp:RadioButton>
                                </td>
							</tr>
							<tr class="indent">
								<td nowrap>
									<asp:RadioButton id="aFHABorrCertOccIsAsHomeForActiveSpouse" GroupName="aFHABorrCertOcc" tabIndex="81" runat="server" Text="(b) My spouse is on active military duty and ... I occupy ..."></asp:RadioButton></td>
							</tr>
							<tr class="indent">
								<td nowrap>
									<asp:RadioButton id="aFHABorrCertOccIsAsHomePrev" GroupName="aFHABorrCertOcc" tabIndex="82" runat="server" Text="(c) I previously occupied the property ... as my home"></asp:RadioButton></td>
							</tr>
							<tr class="indent">
								<td nowrap>
									<asp:RadioButton id="aFHABorrCertOccIsAsHomePrevForActiveSpouse" GroupName="aFHABorrCertOcc" tabIndex="83" runat="server" Text="(d) While my spouse was on active military duty ... I previously ..."></asp:RadioButton></td>
							</tr>
                            <tr class="hud-92900-2016 indent">
								<td nowrap>
									<asp:RadioButton id="aFHABorrCertOccIsChildAsHome" GroupName="aFHABorrCertOcc" tabIndex="83" runat="server" Text="(e) The veteran is on active duty... dependent child... occupies"></asp:RadioButton></td>
							</tr>
                            <tr class="hud-92900-2016 indent">
								<td nowrap>
									<asp:RadioButton id="aFHABorrCertOccIsChildAsHomePrev" GroupName="aFHABorrCertOcc" tabIndex="83" runat="server" Text="(f) The veteran is on active duty... was occupied... dependent child"></asp:RadioButton></td>
							</tr>
							<TR class="indent">
								<TD noWrap>(3) Mark the applicable box, I have been 
									informed that
									<ml:moneytextbox id="aFHABorrCertInformedPropVal" tabIndex="84" runat="server" width="90" preset="money"></ml:moneytextbox>is</TD>
							</TR>
							<TR class="indent">
								<TD noWrap>
									<asp:checkbox id="aFHABorrCertInformedPropValDeterminedByVA" tabIndex="85" runat="server" Text="the reasonable value of the property as the determined by V.A. or:"></asp:checkbox></TD>
							</TR>
							<TR class="indent">
								<TD noWrap>
									<asp:checkbox id="aFHABorrCertInformedPropValDeterminedByHUD" tabIndex="86" runat="server" Text="the statement of appraised value as determined by HUD/FHA"></asp:checkbox></TD>
							</TR>
							<TR class="indent">
								<TD noWrap><asp:checkbox id="aFHABorrCertInformedPropValAwareAtContractSigning" tabIndex="87" runat="server" Text="(a) I was aware of this valuation when I signed my contract..."></asp:checkbox></TD>
							</TR>
							<TR class="indent">
								<TD noWrap>
									<asp:checkbox id="aFHABorrCertInformedPropValNotAwareAtContractSigning" tabIndex="88" runat="server" Text="(b) I was not aware of this valuation when I signed my contract ..."></asp:checkbox></TD>
							</TR>
							<TR class="indent">
								<TD noWrap>(6) For HUD only - I have 
									received info on lead paint poisoning.
									<asp:checkboxlist id="aFHABorrCertReceivedLeadPaintPoisonInfoTri" tabIndex="89" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
										<asp:ListItem Value="1">Yes</asp:ListItem>
										<asp:ListItem Value="2">N/A</asp:ListItem>
									</asp:checkboxlist></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr>
				    <td noWrap>
				        <table  class="InsetBorder wide-table" cellspacing="0" cellpadding="0" border="0">
				            <tr>
				                <td>
				                    <table cellspacing="0" cellpadding="0" border="0">
				                        <colgroup width="200px"></colgroup>
				                        <colgroup width="132px"></colgroup>
				                        <colgroup width="185px"></colgroup>
				                        <colgroup width="100px"></colgroup>
				                        <tr>
				                            <td class="FieldLabel" colspan="4" noWrap>
				                                <asp:CheckBox id="sFHAApprovedSubj" tabIndex="90" runat="server" Text="Approved: Approved subject to additional conditions stated below, if any."></asp:CheckBox>
				                             </td>
				                        </tr>
				                        <tr>
				                            <td noWrap>Date Mortgage Approved:</td>
				                            <td noWrap><ml:datetextbox id="sFHAApprovedD" runat="server" width="75" preset="date" CssClass="mask" EnableViewState="False" TabIndex="91"></ml:datetextbox></td>
				                            <td noWrap>Date Approval Expires:</td>
				                            <td noWrap><ml:datetextbox id="sFHAApprovedExpiresD" runat="server" width="75" preset="date" CssClass="mask" EnableViewState="False" TabIndex="92"></ml:datetextbox></td>
				                        </tr>
				                        <tr>
				                            <td class="FieldLabel" colspan="4">
				                                <asp:CheckBox id="sFHAApprovedModified" tabIndex="93" runat="server" Text="Modified & Approved as follows:" />
				                            </td>
				                        </tr>
				                        <tr>
				                            <td noWrap>Total Loan Amount</td>
				                            <td noWrap><ml:moneytextbox ID="sFHAModifiedFinalLAmt" TabIndex="94" runat="server" Width="90px" preset="money" />
				                            <td noWrap>Interest Rate</td>
				                            <td noWrap><ml:percenttextbox ID="sFHAModifiedNoteIR" TabIndex="99" runat="server" Width="70px" preset="percent" /></td>
				                        </tr>
				                        <tr>
				                            <td noWrap>Proposed Maturity</td>
				                            <td noWrap>
				                                <asp:textbox ID="sFHAModifiedTermInYr" TabIndex="95" runat="server" Width="25px"></asp:textbox> Yrs.
				                                <asp:textbox ID="sFHAModifiedTermInMonths" TabIndex="96" runat="server" Width="25px"></asp:textbox> Mos
				                            </td>
            				                
				                            <td noWrap>Monthly Payment</td>
				                            <td noWrap><ml:moneytextbox ID="sFHAModifiedMonthlyPmt" TabIndex="100" runat="server" Width="90px" preset="money" /></td>
				                        </tr>
				                        <tr>
				                            <td noWrap>Amount of Upfront MIP</td>
				                            <td noWrap><ml:moneytextbox ID="sFHAModifiedFfUfmip1003" TabIndex="97" runat="server" Width="90px" preset="money" /></td>
				                            <td noWrap>Amount of Monthly Premium</td>
				                            <td noWrap><ml:moneytextbox ID="sFHAModifiedProMIns" TabIndex="101" runat="server" Width="90px" preset="money" /></td>
				                        </tr>
				                        <tr>
				                            <td noWrap>Term of Monthly Premium</td>
				                            <td noWrap><asp:textbox ID="sFHAModifiedProMInsMon" TabIndex="98" runat="server" Width="25px" /> Mos</td>
				                        </tr>
				                    </table>
				                </td>
				            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="AdditionalConditionsSection" runat="server" class="InsetBorder hud-92900-2010 wide-table" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td class="FieldLabel" colspan="4">Additional Conditions:</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:CheckBox id="sFHACondIfProposedConstBuilderCertified" tabIndex="102" runat="server" Text="If this is proposed construction, the builder has certified compliance with HUD requirements on form HUD-92541"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:CheckBox id="sFHACondIfNewConstLenderCertified" tabIndex="103" runat="server" Text="If this is new construction, the lender certifies that the property is 100% complete and the property meets HUD's minimum property standards and local building codes."></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:CheckBox id="sFHACondBuilderWarrantyRequired" tabIndex="104" runat="server" Text="Form HUD-92544, Builder's Warranty is required."></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:CheckBox id="sFHACond10YrsWarranty" tabIndex="105" runat="server" Text="The property has a 10-year warranty."></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:CheckBox id="sFHACondOwnerOccNotRequired" tabIndex="106" runat="server" Text="Owner-Occupancy Not required (item (b) of the Borrower's Certificate does not apply)."></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:CheckBox id="sFHACondMortgageHighLtvForNonOccupant" tabIndex="107" runat="server" Text="The mortgage is a high loan-to-value ratio for non-occupant mortgagor in military."></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox id="sFHACondOther" tabIndex="108" runat="server" Text="Other: (specify)"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:TextBox ID="sFHACondOtherDesc" TabIndex="109" runat="server" Width="455px" TextMode="MultiLine" Height="70px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                </tr>
            </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="InsetBorder" id="DirectEndorsementSection" cellspacing="0" cellpadding="0" width="99%" border="0">
                <tbody class="hud-92900-2016">
                    <tr>
                        <td><asp:CheckBox runat="server" ID="sFHACondOwnerOccNotRequired2" Text="Owner-Occupancy Not required" /></td>
                        <td class="extended-section"> 
                            <label class="align-label">DE's CHUMS ID Number</label>
                            <asp:textbox id="sFHAAddendumUnderwriterChumsId2" data-field-id="sFHAAddendumUnderwriterChumsId" onchange="manualEdit('license');" tabIndex="110" runat="server"></asp:textbox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="sFhaDirectEndorsementAllConditionsOfApprovalMet" Text="All conditions of Approval have been satisfied" />
                        </td>
                    </tr>
                </tbody>
                <colgroup width="200px"></colgroup>
                <tr class="indent">
                    <td colspan="4" class="FieldLabel">
                        <asp:CheckBox id="sFHARatedAcceptedByTotalScorecard" tabIndex="110" runat="server" Text="This mortgage was rated as an &quot;accept&quot; or &quot;approve&quot; by FHA's Total Mortgage Scorecard."></asp:CheckBox>
                    </td>
                </tr>
                <tr class="indent2">
                    <td class="extended-section" nowrap><label class="hud-92900-2016 align-l">Mortgagee Representative Name</label>
                        <label class="hud-92900-2010">Mortgagee Representative</label>
                        <asp:textbox onchange="manualEdit('mortgagee');" id="sFHAAddendumMortgageeName" tabIndex="111" runat="server"></asp:textbox>
                        </td>
                    <td class="extended-section">
                        <span class="hud-92900-2016">
                            <label class="align-label">Mortgagee Representative Title</label>
                            <asp:textbox runat="server" ID="sFHAAddendumMortgageeTitle"></asp:textbox>
                        </span>
                    </td>
                </tr>
                <tr class="hud-92900-2016 indent2">
                    <td colspan="2"><asp:CheckBox runat="server" ID="sFhaDirectEndorsementWasRatedAcceptAndUnderwritten" text="This mortgage was rated as an &quot;accept&quot;... I have personally reviewed and underwritten the appraisal..."/></td>
                </tr>
                <tr class="indent">
                    <td colspan="4" class="FieldLabel">
                        <asp:CheckBox id="sFHARatedReferByTotalScorecard" tabIndex="112" runat="server" Text="This mortgage was rated as a &quot;refer&quot; by a FHA's Total Mortgage Scorecard, and/or was manually underwritten by a Direct Endorsement underwriter."></asp:CheckBox>
                    </td>
                </tr>
                <tr class="hud-92900-2010">
                    <td nowrap class="extended-section">
                       <label>Direct Endorsement Underwriter</label> 
                        <asp:textbox id="sFHAAddendumUnderwriterName" onchange="manualEdit('name');" tabIndex="113" runat="server"></asp:textbox>
                    </td>
                    <td nowrap>
                       
                        <span class="hud-92900-2010">
                        DE's CHUMS ID Number
                        
                        <asp:textbox id="sFHAAddendumUnderwriterChumsId" onchange="manualEdit('license');" tabIndex="114" runat="server"></asp:textbox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        The Mortgage, its owners, officers, employees or directors 
                        <asp:checkboxlist id="sFHAMortgageHaveFinanceInterestTri" tabIndex="115" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        	<asp:ListItem Value="1">Do</asp:ListItem>
							<asp:ListItem Value="2">Do Not</asp:ListItem>
                        </asp:checkboxlist>
                        have a financial interest in or a relationship, by affiliation or ownership, with the builder or seller involved in this transaction.
                    </td>
                </tr>
	        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <TABLE class="InsetBorder  wide-table" id="Table12" cellSpacing="0" cellPadding="0" border="0">
                <colgroup width="60px"></colgroup>
				<TR>
					<TD class="FieldLabel" noWrap colSpan="2">Lender's Representative</TD>
				</TR>
				<TR>
                    <TD noWrap>Name</TD>
					<TD noWrap><asp:textbox id="FHAAddendumLenderPreparerName" tabIndex="116" runat="server"></asp:textbox></TD>
					<TD noWrap>Title</TD>
					<TD noWrap><asp:textbox id="FHAAddendumLenderTitle" tabIndex="117" runat="server"></asp:textbox></TD>
				</TR>
				<!--
				<TR>
					<TD noWrap>Phone</TD>
					<TD noWrap><ml:phonetextbox id="FHAAddendumLenderPhone" tabIndex="25" runat="server" width="120" preset="phone"></ml:phonetextbox></TD>
				</TR>
				-->
			</TABLE>
                    </td>
                </tr>
            </table>
            <div>
            </div>
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
		</form>
	</body>
</HTML>
