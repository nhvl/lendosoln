using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
    public class FHAHUDAppraisedValueDisclosureServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHAHUDAppraisedValueDisclosureServiceItem));
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAppraisedValueDisclosure, E_ReturnOptionIfNotExist.CreateNew);
            preparer.CompanyName = GetString("FHAAppraisedValueDisclosureCompanyName");
            preparer.StreetAddr = GetString("FHAAppraisedValueDisclosureStreetAddr");
            preparer.City = GetString("FHAAppraisedValueDisclosureCity");
            preparer.State = GetString("FHAAppraisedValueDisclosureState");
            preparer.Zip = GetString("FHAAppraisedValueDisclosureZip");
            preparer.PhoneOfCompany = GetString("FHAAppraisedValueDisclosurePhoneOfCompany");

            preparer.Update();

        }
    }
	public partial class FHAHUDAppraisedValueDisclosureService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FHAHUDAppraisedValueDisclosureServiceItem());
        }
	}
}
