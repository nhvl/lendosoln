namespace LendersOfficeApp.newlos.FHA
{
    using System;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Migration;

    public partial class FHACreditAnalysisRefinance : BaseLoanPage
    {

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHACreditAnalysisRefinance));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            aFHANegCfRentalI.Text = dataApp.aFHANegCfRentalI_rep;

            aBFirstNm.Text = dataApp.aBFirstNm;
            aBMidNm.Text = dataApp.aBMidNm;
            aBLastNm.Text = dataApp.aBLastNm;
            aBSuffix.Text = dataApp.aBSuffix;
            aBSSN.Text = dataApp.aBSsn;


            aCFirstNm.Text = dataApp.aCFirstNm;
            aCMidNm.Text = dataApp.aCMidNm;
            aCLastNm.Text = dataApp.aCLastNm;
            aCSuffix.Text = dataApp.aCSuffix;
            aCSSN.Text = dataApp.aCSsn;

            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            
            if (Tools.ShouldBeReadonly_sLT(dataLoan.sLPurposeT))
            {
                Tools.ReadonlifyDropDown(sLT);
            }

            sFHARefinanceTypeDesc.Text = dataLoan.sFHARefinanceTypeDesc;
            sFHACreditAnalysisRemarks.Text = dataLoan.sFHACreditAnalysisRemarks;
            
            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisRefinanceUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHACreditAnalysisRefiancePreparerName.Text = underwriter.PreparerName;
            FHACreditAnalysisRefinanceLicenseNumOfAgent.Text = underwriter.LicenseNumOfAgent;

            Tools.SetDropDownListValue(sFHAConstructionT, dataLoan.sFHAConstructionT);
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            sFHAHousingActSection.Text = dataLoan.sFHAHousingActSection;
            sLAmt.Text = dataLoan.sLAmtCalc_rep;
            sPresLTotPersistentHExp.Text = dataApp.aPresTotHExp_rep;
            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sMipPiaMon.Text = dataLoan.sMipPiaMon_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sBuydownResultIR.Text = dataLoan.sBuydownResultIR_rep;
            sApprVal.Text = dataLoan.sApprVal_rep;
            sFHACcTot.Text = dataLoan.sFHACcTot_rep;
            sFHACcPbs.Text = dataLoan.sFHACcPbs_rep;
            sFHACcPbb.Text = dataLoan.sFHACcPbb_rep;
            sFHACcPbb2.Text = dataLoan.sFHACcPbb_rep;
            sPurchPrice2.Text = dataLoan.sFHAExistingMLien_rep;
            sPurchPrice6pc.Text = dataLoan.sPurchPrice6pc_rep;
            sFHASellerContribution.Text = dataLoan.sFHASellerContribution_rep;
            sFHAExcessContribution.Text = dataLoan.sFHAExcessContribution_rep;
            sFHAExistingMLien.Text = dataLoan.sFHAExistingMLien_rep;
            sFHAMBasisRefin.Text = dataLoan.sFHAMBasisRefin_rep;
            sFHAMBasisRefinMultiply.Text = dataLoan.sFHAMBasisRefinMultiply_rep;
            sFHAApprValMultiply.Text = dataLoan.sFHAApprValMultiply_rep;
            sFHAMBasisRefinMultiplyLckd.Checked = dataLoan.sFHAMBasisRefinMultiplyLckd;
            sFHAApprValMultiplyLckd.Checked = dataLoan.sFHAApprValMultiplyLckd;
            sFHAReqInvestment.Text = dataLoan.sFHAReqInvestment_rep;
            sFHAReqTot.Text = dataLoan.sFHAReqTot_rep;
            sFHAAmtPaid.Text = dataLoan.sFHAAmtPaid_rep;
            sFHAIsAmtPdInOther.Checked = dataLoan.sFHAIsAmtPdInOther;
            sFHAIsAmtPdInCash.Checked = dataLoan.sFHAIsAmtPdInCash;
            sFHAIsAmtToBePdInOther.Checked = dataLoan.sFHAIsAmtToBePdInOther;
            sFHAIsAmtToBePdInCash.Checked = dataLoan.sFHAIsAmtToBePdInCash;
            sTermInYr.Text = dataLoan.sTermInYr_rep;
            sFHAImprovementsDesc.Text = dataLoan.sFHAImprovementsDesc;
            sFHAImprovements.Text = dataLoan.sFHAImprovements_rep;
            sFHASalesConcessions.Text = dataLoan.sFHASalesConcessions_rep;
            sLAmt2.Text = dataLoan.sLAmtCalc_rep;
            sFHADiscountPoints.Text = dataLoan.sFHADiscountPoints_rep;
            sFHAPrepaidExp.Text = dataLoan.sFHAPrepaidExp_rep;
            sUfCashPd.Text = dataLoan.sUfCashPd_rep;
            sFHANonrealty.Text = dataLoan.sFHANonrealty_rep;
            sFHANonrealtyLckd.Checked = dataLoan.sFHANonrealtyLckd;
            aFHAAssetAvail.Text = dataApp.aFHAAssetAvail_rep;
            sFHA2ndMAmt.Text = dataLoan.sFHA2ndMAmt_rep;
            aFHABBaseI.Text = dataApp.aFHABBaseI_rep;
            aFHABOI.Text = dataApp.aFHABOI_rep;
            aFHACBaseI.Text = dataApp.aFHACBaseI_rep;
            aFHACOI.Text = dataApp.aFHACOI_rep;
            aFHANetRentalI.Text = dataApp.aFHANetRentalI_rep;
            aFHAGrossMonI.Text = dataApp.aFHAGrossMonI_rep;
            aFHADebtInstallPmt.Text = dataApp.aFHADebtInstallPmt_rep;
            aFHADebtInstallBal.Text = dataApp.aFHADebtInstallBal_rep;
            aFHAChildSupportPmt.Text = dataApp.aFHAChildSupportPmt_rep;
            aFHAChildSupportPmt.ReadOnly = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V25_ConsolidateMonthlyChildSupportPayments);
            aFHAOtherDebtPmt.Text = dataApp.aFHAOtherDebtPmt_rep;
            aFHAOtherDebtBal.Text = dataApp.aFHAOtherDebtBal_rep;
            aFHADebtPmtTot.Text = dataApp.aFHADebtPmtTot_rep;
            sFHAPro1stMPmt.Text = dataLoan.sFHAPro1stMPmt_rep;
            sFHAProMIns.Text = dataLoan.sFHAProMIns_rep;
            sFHAProMInsLckd.Checked = dataLoan.sFHAProMInsLckd;
            sFHAProHoAssocDues.Text = dataLoan.sFHAProHoAssocDues_rep;
            sFHAProGroundRent.Text = dataLoan.sFHAProGroundRent_rep;
            sFHAPro2ndFinPmt.Text = dataLoan.sFHAPro2ndFinPmt_rep;
            sFHAProHazIns.Text = dataLoan.sFHAProHazIns_rep;
            sFHAProRealETx.Text = dataLoan.sFHAProRealETx_rep;
            sFHAMonthlyPmt.Text = dataLoan.sFHAMonthlyPmt_rep;
            aFHADebtPmtTot2.Text = dataApp.aFHADebtPmtTot_rep;
            aFHAPmtFixedTot.Text = dataApp.aFHAPmtFixedTot_rep;
            sFHALtv2.Text = dataLoan.sFHALtvRefi_rep;
            aFHAMPmtToIRatio.Text = dataApp.aFHAMPmtToIRatio_rep;
            aFHAFixedPmtToIRatio.Text = dataApp.aFHAFixedPmtToIRatio_rep;
            aFHACreditRating.Text = dataApp.aFHACreditRating;
            aFHARatingIAdequacy.Text = dataApp.aFHARatingIAdequacy;
            aFHARatingIStability.Text = dataApp.aFHARatingIStability;
            aFHARatingAssetAdequacy.Text = dataApp.aFHARatingAssetAdequacy;
            aFHABCaivrsNum.Text = dataApp.aFHABCaivrsNum;
            aFHACCaivrsNum.Text = dataApp.aFHACCaivrsNum;
            aFHABLpdGsa.Text = dataApp.aFHABLpdGsa;
            aFHACLpdGsa.Text = dataApp.aFHACLpdGsa;
            sFHAGiftAmtTot.Text = dataLoan.sFHAGiftAmtTot_rep;
            sFHAAmtToBePd.Text = dataLoan.sFHAAmtToBePd_rep;
            sUfCashPdzzz2.Text = dataLoan.sUfCashPd_rep;
            sFfUfmipR.Text = dataLoan.sFfUfmipR_rep;
            sFHAEnergyEffImprov.Text = dataLoan.sFHAEnergyEffImprov_rep;
            sFfUfmip1003Lckd.Checked = dataLoan.sFfUfmip1003Lckd;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
        }
        private void HighlightRow(WebControl ctl) 
        {
            ctl.Attributes.Add("onfocus", "highlightRow(this);");
            ctl.Attributes.Add("onblur", "unhighlightRow(this);");
        }
        protected void PageInit(object sender, System.EventArgs e)
        {
            HighlightRow(sFHAExistingMLien);
            HighlightRow(sFHAImprovementsDesc);
            HighlightRow(sFHAImprovements);
            HighlightRow(sFHAMBasisRefinMultiply);
            HighlightRow(sFHAApprValMultiply);
            HighlightRow(sFHASalesConcessions);

            HighlightRow(sFHAPrepaidExp);
            HighlightRow(sFHADiscountPoints);
            HighlightRow(sFHAImprovements);
            HighlightRow(sFHANonrealty);
            HighlightRow(sFHANonrealtyLckd);
            HighlightRow(sFHAAmtPaid);
            HighlightRow(aFHAAssetAvail);
            HighlightRow(sFHA2ndMAmt);
            HighlightRow(aFHABBaseI);
            HighlightRow(aFHABOI);
            HighlightRow(aFHACBaseI);
            HighlightRow(aFHACOI);
            HighlightRow(aFHANetRentalI);
            HighlightRow(aFHADebtInstallPmt);
            HighlightRow(aFHADebtInstallBal);
            HighlightRow(aFHAChildSupportPmt);
            HighlightRow(aFHAOtherDebtPmt);
            HighlightRow(aFHAOtherDebtBal);
            HighlightRow(sFHAPro1stMPmt);
            HighlightRow(sFHAProMIns);
            HighlightRow(sFHAProHoAssocDues);
            HighlightRow(sFHAProGroundRent);
            HighlightRow(sFHAPro2ndFinPmt);
            HighlightRow(sFHAProHazIns);
            HighlightRow(sFHAProRealETx);
            HighlightRow(aFHACreditRating);
            HighlightRow(aFHARatingIAdequacy);
            HighlightRow(aFHARatingIStability);
            HighlightRow(aFHARatingAssetAdequacy);
            HighlightRow(aFHABCaivrsNum);
            HighlightRow(aFHACCaivrsNum);
            HighlightRow(aFHABLpdGsa);
            HighlightRow(aFHACLpdGsa);
            HighlightRow(sFHASellerContribution);
            HighlightRow(sFHAExcessContribution);

            Tools.Bind_sFHAHousingActSection(sFHAHousingActSection);
            Tools.Bind_sFHARefinanceTypeDesc(sFHARefinanceTypeDesc);
            Tools.Bind_sFHAConstructionT(sFHAConstructionT);
            Tools.Bind_sLT(sLT);

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

            this.PageTitle = "FHA MCAW Refi";
            this.PageID = "FHA_92900_WS";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CHUD_92900_WSPDF);

        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        
        }
    }
}
