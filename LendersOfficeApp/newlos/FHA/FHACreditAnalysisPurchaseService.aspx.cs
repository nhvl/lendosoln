namespace LendersOfficeApp.newlos.FHA
{
    using System;
    using DataAccess;
    using LendersOffice.Migration;

    public class FHACreditAnalysisPurchaseServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem 
    {
        protected override CPageData ConstructPageDataClass(Guid sLId) 
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHACreditAnalysisPurchaseServiceItem));
        }
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "CopyFromLoanApp":
                    CopyFromLoanApp();
                    break;
                case "CopyFromGFE":
                    CopyFromGFE();
                    break;
            }
        }
        private void CopyFromLoanApp() 
        {
            bool isSave = GetString("issave", "") == "T";

            Guid applicationID = GetGuid("applicationid");

            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(applicationID);
            if (isSave)
                BindData(dataLoan, dataApp);

            dataApp.FhaMcawApplyAppFieldsFrom1003();

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan, dataApp);
        }
        private void CopyFromGFE() 
        {
            bool isSave = GetString("issave", "") == "T";
            Guid loanID = GetGuid("loanid");
            Guid applicationID = GetGuid("applicationid");

            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(applicationID);

            if (isSave)
                BindData(dataLoan, dataApp);

            dataLoan.ApplyGfeItemsToMcawPurch();

            dataLoan.Save();
            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan, dataApp);
        }
        protected override void BindData(CPageData dataLoan, CAppData dataApp) 
        {
            dataApp.aFHANegCfRentalI_rep = GetString("aFHANegCfRentalI");

            dataApp.aBFirstNm		= GetString("aBFirstNm") ;
            dataApp.aBMidNm                 = GetString("aBMidNm");
            dataApp.aBLastNm		= GetString("aBLastNm") ;
            dataApp.aBSuffix                = GetString("aBSuffix");

            dataApp.aBSsn			= GetString("aBSSN") ;
            dataApp.aCFirstNm		= GetString("aCFirstNm") ;
            dataApp.aCMidNm                 = GetString("aCMidNm");
            dataApp.aCLastNm		= GetString("aCLastNm") ;
            dataApp.aCSuffix                = GetString("aCSuffix");
            dataApp.aCSsn			= GetString("aCSSN") ;
            dataApp.aFHAGiftFundSrc			= GetString("aFHAGiftFundSrc");
            dataApp.aFHAGiftFundAmt_rep		= GetString("aFHAGiftFundAmt") ;
            dataApp.aFHAAssetAvail_rep      = GetString("aFHAAssetAvail") ;
            dataApp.aFHADebtInstallPmt_rep  = GetString("aFHADebtInstallPmt") ;

            if (!LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V25_ConsolidateMonthlyChildSupportPayments))
            {
                dataApp.aFHAChildSupportPmt_rep = GetString("aFHAChildSupportPmt");
            }
            
            dataApp.aFHAOtherDebtPmt_rep    = GetString("aFHAOtherDebtPmt") ;
            dataApp.aFHADebtInstallBal_rep  = GetString("aFHADebtInstallBal") ;     
            dataApp.aFHAOtherDebtBal_rep    = GetString("aFHAOtherDebtBal") ;
            dataApp.aFHABBaseI_rep          = GetString("aFHABBaseI") ;
            dataApp.aFHABOI_rep             = GetString("aFHABOI") ;
            dataApp.aFHACBaseI_rep          = GetString("aFHACBaseI") ;
            dataApp.aFHACOI_rep             = GetString("aFHACOI") ;
            dataApp.aFHANetRentalI_rep      = GetString("aFHANetRentalI") ;    
 
            dataLoan.sFHAPro1stMPmt_rep	= GetString("sFHAPro1stMPmt") ;
            dataLoan.sFHAProMIns_rep		= GetString("sFHAProMIns") ;
            dataLoan.sFHAProMInsLckd = GetBool("sFHAProMInsLckd");
            dataLoan.sFHAProHoAssocDues_rep = GetString("sFHAProHoAssocDues") ;
            dataLoan.sFHAProGroundRent_rep  = GetString("sFHAProGroundRent") ;
            dataLoan.sFHAPro2ndFinPmt_rep   = GetString("sFHAPro2ndFinPmt") ;
            dataLoan.sFHAProHazIns_rep	= GetString("sFHAProHazIns") ;
            dataLoan.sFHAProRealETx_rep	= GetString("sFHAProRealETx") ;
            dataApp.aFHACreditRating	= GetString("aFHACreditRating") ;
            dataApp.aFHARatingIAdequacy     = GetString("aFHARatingIAdequacy") ;
            dataApp.aFHARatingIStability    = GetString("aFHARatingIStability") ;
            dataApp.aFHARatingAssetAdequacy = GetString("aFHARatingAssetAdequacy") ;
            dataApp.aFHABCaivrsNum	= GetString("aFHABCaivrsNum") ;
            dataApp.aFHACCaivrsNum	= GetString("aFHACCaivrsNum") ;
            dataApp.aFHABLpdGsa		= GetString("aFHABLpdGsa") ;
            dataApp.aFHACLpdGsa		= GetString("aFHACLpdGsa") ;

            dataLoan.sFHANonrealtyLckd      = GetBool("sFHANonrealtyLckd") ;
            dataLoan.sLT                    = (E_sLT) GetInt("sLT");

            
            dataLoan.sFHAHousingActSection = GetString("sFHAHousingActSection") ;
            dataLoan.sAgencyCaseNum = GetString("sAgencyCaseNum") ;
            dataLoan.sFHAConstructionT = (E_sFHAConstructionT) GetInt("sFHAConstructionT");

            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisPurchaseUnderwriter, E_ReturnOptionIfNotExist.CreateNew);
            underwriter.LicenseNumOfAgent = GetString("FHACreditAnalysisPurchaseUnderwriterLicenseNumOfAgent") ;
            underwriter.PreparerName = GetString("FHACreditAnalysisPurchaseUnderwriterPreparerName") ;
            underwriter.Update();
            
            dataLoan.sLAmtCalc_rep              = GetString("sLAmt") ;
            dataLoan.sFfUfmip1003_rep           = GetString("sFfUfmip1003") ;
            dataLoan.sMipPiaMon_rep             = GetString("sMipPiaMon") ;

            dataLoan.sNoteIR_rep                = GetString("sNoteIR") ;
            dataLoan.sApprVal_rep               = GetString("sApprVal") ;
            dataLoan.sFHACcTot_rep              = GetString("sFHACcTot") ;
            dataLoan.sFHACcPbs_rep              = GetString("sFHACcPbs") ;
            dataLoan.sFHAPurchPrice_rep         = GetString("sFHAPurchPrice")  ;
            dataLoan.sFHAReqAdj_rep             = GetString("sFHAReqAdj") ;
            dataLoan.sFHALtvLckd                = GetBool("sFHALtvLckd") ;
            dataLoan.sFHALtv_rep                = GetString("sFHALtv") ;
            dataLoan.sFHAPrepaidExp_rep         = GetString("sFHAPrepaidExp") ;
            dataLoan.sFHADiscountPoints_rep     = GetString("sFHADiscountPoints") ;
            dataLoan.sFHAImprovements_rep       = GetString("sFHAImprovements") ;
            dataLoan.SetsUfCashPdViaImport(GetString("sUfCashPdzzz2")); // OPM 32441
            dataLoan.sFHANonrealty_rep          = GetString("sFHANonrealty") ;
            dataLoan.sFHAAmtPaid_rep            = GetString("sFHAAmtPaid") ;
            
            dataLoan.sFHA2ndMSrc                = GetString("sFHA2ndMSrc") ;
            dataLoan.sFHA2ndMAmt_rep            = GetString("sFHA2ndMAmt") ; 
            dataLoan.sFHASellerContribution_rep = GetString("sFHASellerContribution") ;
            dataLoan.sFHAExcessContribution_rep = GetString("sFHAExcessContribution") ;
            dataLoan.sFHACreditAnalysisRemarks  = GetString("sFHACreditAnalysisRemarks") ;
            dataLoan.sFfUfmipR_rep			    = GetString("sFfUfmipR");
            dataLoan.sFfUfmip1003Lckd		    = GetBool("sFfUfmip1003Lckd");
            dataLoan.sFHAAmtPaidByCash		    = GetBool("sFHAAmtPaidByCash");
            dataLoan.sFHAAmtPaidByOtherT	    = GetBool("sFHAAmtPaidByOtherT");
            dataLoan.sFHAEnergyEffImprov_rep    = GetString("sFHAEnergyEffImprov");
            dataLoan.sFHAPurchPriceSrcT         = (E_sFHAPurchPriceSrcT) GetInt("sFHAPurchPriceSrcT") ;
            dataLoan.sFHA203kInvestmentRequiredPc_rep = GetString("sFHA203kInvestmentRequiredPc");

        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp) 
        {
            SetResult("aFHANegCfRentalI", dataApp.aFHANegCfRentalI_rep);
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aBSSN", dataApp.aBSsn);

            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aCSSN", dataApp.aCSsn);
            SetResult("aFHAChildSupportPmt", dataApp.aFHAChildSupportPmt_rep);
            SetResult("aFHAGiftFundSrc", dataApp.aFHAGiftFundSrc);
            SetResult("aFHAGiftFundAmt", dataApp.aFHAGiftFundAmt_rep);
            SetResult("aFHAAssetAvail", dataApp.aFHAAssetAvail_rep);
            SetResult("aFHADebtInstallPmt", dataApp.aFHADebtInstallPmt_rep);
            SetResult("aFHAOtherDebtPmt", dataApp.aFHAOtherDebtPmt_rep);
            SetResult("aFHADebtInstallBal", dataApp.aFHADebtInstallBal_rep);
            SetResult("aFHAOtherDebtBal", dataApp.aFHAOtherDebtBal_rep);
            SetResult("aFHABBaseI", dataApp.aFHABBaseI_rep);
            SetResult("aFHABOI", dataApp.aFHABOI_rep);
            SetResult("aFHACBaseI", dataApp.aFHACBaseI_rep);
            SetResult("aFHACOI", dataApp.aFHACOI_rep);
            SetResult("aFHANetRentalI", dataApp.aFHANetRentalI_rep);
            SetResult("aFHADebtPmtTot", dataApp.aFHADebtPmtTot_rep);
            SetResult("sFHAPro1stMPmt", dataLoan.sFHAPro1stMPmt_rep);
            SetResult("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            SetResult("sFHAProMInsLckd", dataLoan.sFHAProMInsLckd);
            SetResult("sFHAProHoAssocDues", dataLoan.sFHAProHoAssocDues_rep);
            SetResult("sFHAProGroundRent", dataLoan.sFHAProGroundRent_rep);
            SetResult("sFHAPro2ndFinPmt", dataLoan.sFHAPro2ndFinPmt_rep);
            SetResult("sFHAProHazIns", dataLoan.sFHAProHazIns_rep);
            SetResult("sFHAProRealETx", dataLoan.sFHAProRealETx_rep);
            SetResult("sFHAMonthlyPmt", dataLoan.sFHAMonthlyPmt_rep);
            SetResult("aFHADebtPmtTot2", dataApp.aFHADebtPmtTot_rep);
            SetResult("aFHAPmtFixedTot", dataApp.aFHAPmtFixedTot_rep);
            SetResult("aFHAMPmtToIRatio", dataApp.aFHAMPmtToIRatio_rep);
            SetResult("aFHAFixedPmtToIRatio", dataApp.aFHAFixedPmtToIRatio_rep);
            SetResult("aFHACreditRating", dataApp.aFHACreditRating);
            SetResult("aFHARatingIAdequacy", dataApp.aFHARatingIAdequacy);
            SetResult("aFHARatingIStability", dataApp.aFHARatingIStability);
            SetResult("aFHARatingAssetAdequacy", dataApp.aFHARatingAssetAdequacy);
            SetResult("aFHABCaivrsNum", dataApp.aFHABCaivrsNum);
            SetResult("aFHACCaivrsNum", dataApp.aFHACCaivrsNum);
            SetResult("aFHABLpdGsa", dataApp.aFHABLpdGsa);
            SetResult("aFHACLpdGsa", dataApp.aFHACLpdGsa);

            SetResult("sLT", dataLoan.sLT);

            SetResult("sFHANonrealtyLckd", dataLoan.sFHANonrealtyLckd);
            
            
            SetResult("sPurchPrice6pc", dataLoan.sPurchPrice6pc_rep);
            SetResult("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            SetResult("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            SetResult("sFHAConstructionT", dataLoan.sFHAConstructionT);

            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisPurchaseUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            SetResult("FHACreditAnalysisPurchaseUnderwriterLicenseNumOfAgent", underwriter.LicenseNumOfAgent);
            SetResult("FHACreditAnalysisPurchaseUnderwriterPreparerName", underwriter.PreparerName);

            SetResult("sLAmt", dataLoan.sLAmtCalc_rep);
            SetResult("sPresLTotHExp", dataApp.aPresTotHExp_rep);
            SetResult("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            SetResult("sMipPiaMon", dataLoan.sMipPiaMon_rep);

            SetResult("sTermInYr", dataLoan.sTermInYr_rep);
            SetResult("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sApprVal", dataLoan.sApprVal_rep);
            SetResult("sBuydownResultIR", dataLoan.sBuydownResultIR_rep);
            SetResult("sFHACcTot", dataLoan.sFHACcTot_rep);
            SetResult("sFHACcPbs", dataLoan.sFHACcPbs_rep);
            SetResult("sFHAPurchPrice", dataLoan.sFHAPurchPrice_rep);
            SetResult("sFHAPurchPrice2", dataLoan.sFHAPurchPrice_rep);

            string shareVal = dataLoan.sFHACcPbb_rep;
            SetResult("sFHACcPbb2", shareVal);
            SetResult("sFHACcPbb", shareVal);
            SetResult("sFHAUnadjAcquisition", dataLoan.sFHAUnadjAcquisition_rep);
            SetResult("sFHAStatutoryInvestReq", dataLoan.sFHAStatutoryInvestReq_rep);
            SetResult("sFHAHouseVal", dataLoan.sFHAHouseVal_rep);
            SetResult("sFHAReqAdj", dataLoan.sFHAReqAdj_rep);
            SetResult("sFHAMBasis", dataLoan.sFHAMBasis_rep);
            SetResult("sFHALtvLckd", dataLoan.sFHALtvLckd);
            SetResult("sFHALtv", dataLoan.sFHALtv_rep);
            SetResult("sFHAMAmt", dataLoan.sFHAMAmt_rep);
            SetResult("sFHAMAmt2", dataLoan.sFHAMAmt_rep);
            SetResult("sFHAMinDownPmt", dataLoan.sFHAMinDownPmt_rep);
            SetResult("sFHAPrepaidExp", dataLoan.sFHAPrepaidExp_rep);
            SetResult("sFHADiscountPoints", dataLoan.sFHADiscountPoints_rep);
            SetResult("sFHAImprovements", dataLoan.sFHAImprovements_rep);
            SetResult("sUfCashPd", dataLoan.sUfCashPd_rep);
            SetResult("sFHANonrealty", dataLoan.sFHANonrealty_rep);
            SetResult("sFHATotCashToClose", dataLoan.sFHATotCashToClose_rep);
            SetResult("sFHAAmtPaid", dataLoan.sFHAAmtPaid_rep);           
            SetResult("sFHA2ndMSrc", dataLoan.sFHA2ndMSrc);
            SetResult("sFHA2ndMAmt", dataLoan.sFHA2ndMAmt_rep);
            SetResult("sFHACashReserves", dataLoan.sFHACashReserves_rep);
           
            
            SetResult("sFHALtv2", dataLoan.sFHALtvPurch_rep);
            SetResult("sFHASellerContribution", dataLoan.sFHASellerContribution_rep);
            SetResult("sFHAExcessContribution", dataLoan.sFHAExcessContribution_rep);
            SetResult("sFHACreditAnalysisRemarks", dataLoan.sFHACreditAnalysisRemarks);
            SetResult("aFHAGrossMonI", dataApp.aFHAGrossMonI_rep);

            SetResult("sFfUfmipR", dataLoan.sFfUfmipR_rep);
            SetResult("sFfUfmip1003Lckd", dataLoan.sFfUfmip1003Lckd);
            SetResult("sUfCashPdzzz2", dataLoan.sUfCashPd_rep);
            SetResult("sFHAAmtPaidByCash", dataLoan.sFHAAmtPaidByCash);
            SetResult("sFHAAmtPaidByOtherT", dataLoan.sFHAAmtPaidByOtherT);
            SetResult("sFHAEnergyEffImprov", dataLoan.sFHAEnergyEffImprov_rep);
            SetResult("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep );
            SetResult("sFHAPurchPriceSrcT", dataLoan.sFHAPurchPriceSrcT);
            SetResult("sFHA203kInvestmentRequiredPc", dataLoan.sFHA203kInvestmentRequiredPc_rep);
        }
    }
	/// <summary>
	/// Summary description for FHACreditAnalysisPurchaseService.
	/// </summary>
	public partial class FHACreditAnalysisPurchaseService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Initialize() 
        {
            AddBackgroundItem("", new FHACreditAnalysisPurchaseServiceItem());
        }
	}
}
