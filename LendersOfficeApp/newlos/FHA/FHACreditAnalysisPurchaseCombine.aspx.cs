using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
	public partial class FHACreditAnalysisPurchaseCombine : BaseLoanPage
	{

        protected override void LoadData() 
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(FHACreditAnalysisPurchaseCombine));
            dataLoan.InitLoad();

            sCombinedBorInfoLckd.Checked = dataLoan.sCombinedBorInfoLckd;
            sCombinedBorFirstNm.Text = dataLoan.sCombinedBorFirstNm;
            sCombinedBorLastNm.Text = dataLoan.sCombinedBorLastNm;
            sCombinedBorMidNm.Text = dataLoan.sCombinedBorMidNm;
            sCombinedBorSuffix.Text = dataLoan.sCombinedBorSuffix;
            sCombinedBorSsn.Text = dataLoan.sCombinedBorSsn;
            sCombinedCoborFirstNm.Text = dataLoan.sCombinedCoborFirstNm;
            sCombinedCoborLastNm.Text = dataLoan.sCombinedCoborLastNm;
            sCombinedCoborMidNm.Text = dataLoan.sCombinedCoborMidNm;
            sCombinedCoborSuffix.Text = dataLoan.sCombinedCoborSuffix;
            sCombinedCoborSsn.Text = dataLoan.sCombinedCoborSsn;

            sFHAChildSupportPmt.Text    = dataLoan.sFHAChildSupportPmt_rep;
            sFHAGiftFundSrc.Text		= dataLoan.sFHAGiftFundSrc;
            sFHAGiftFundAmt.Text		= dataLoan.sFHAGiftFundAmt_rep;
            sFHAAssetAvail.Text			= dataLoan.sFHAAssetAvail_rep;
            sFHADebtInstallPmt.Text		= dataLoan.sFHADebtInstallPmt_rep;
            sFHAOtherDebtPmt.Text		= dataLoan.sFHAOtherDebtPmt_rep;
            sFHADebtInstallBal.Text		= dataLoan.sFHADebtInstallBal_rep;
            sFHAOtherDebtBal.Text		= dataLoan.sFHAOtherDebtBal_rep;
            sFHABBaseI.Text				= dataLoan.sFHABBaseI_rep;
            sFHABOI.Text				= dataLoan.sFHABOI_rep;
            sFHACBaseI.Text				= dataLoan.sFHACBaseI_rep;
            sFHACOI.Text				= dataLoan.sFHACOI_rep;
            sFHANetRentalI.Text			= dataLoan.sFHANetRentalI_rep;
            sFHADebtPmtTot.Text			= dataLoan.sFHADebtPmtTot_rep;
            sFHAPro1stMPmt.Text			= dataLoan.sFHAPro1stMPmt_rep;
            sFHAProMIns.Text			= dataLoan.sFHAProMIns_rep;
            sFHAProMInsLckd.Checked = dataLoan.sFHAProMInsLckd;
            sFHAProHoAssocDues.Text		= dataLoan.sFHAProHoAssocDues_rep;
            sFHAProGroundRent.Text		= dataLoan.sFHAProGroundRent_rep;
            sFHAPro2ndFinPmt.Text		= dataLoan.sFHAPro2ndFinPmt_rep;
            sFHAProHazIns.Text			= dataLoan.sFHAProHazIns_rep;
            sFHAProRealETx.Text			= dataLoan.sFHAProRealETx_rep;
            sFHAMonthlyPmt.Text			= dataLoan.sFHAMonthlyPmt_rep;
            sFHADebtPmtTot2.Text		= dataLoan.sFHADebtPmtTot_rep;
            sFHAPmtFixedTot.Text		= dataLoan.sFHAPmtFixedTot_rep;
            sFHAMPmtToIRatio.Text		= dataLoan.sFHAMPmtToIRatio_rep;
            sFHAFixedPmtToIRatio.Text	= dataLoan.sFHAFixedPmtToIRatio_rep;
			sFHACreditRating.Text		= dataLoan.sFHACreditRating; 
			sFHARatingIAdequacy.Text	= dataLoan.sFHARatingIAdequacy; 
			sFHARatingIStability.Text	= dataLoan.sFHARatingIStability; 
			sFHARatingAssetAdequacy.Text = dataLoan.sFHARatingAssetAdequacy;
            sFHABCaivrsNum.Text			= dataLoan.sFHABCaivrsNum;
            sFHACCaivrsNum.Text			= dataLoan.sFHACCaivrsNum;
            sFHABLpdGsa.Text			= dataLoan.sFHABLpdGsa;
            sFHACLpdGsa.Text			= dataLoan.sFHACLpdGsa;

            Tools.SetDropDownListValue(sLT, dataLoan.sLT);

            if (Tools.ShouldBeReadonly_sLT(dataLoan.sLPurposeT))
            {
                Tools.ReadonlifyDropDown(sLT);
            }

            sFHANonrealtyLckd.Checked = dataLoan.sFHANonrealtyLckd;
            
            
            sPurchPrice6pc.Text = dataLoan.sPurchPrice6pc_rep;
            sFHAHousingActSection.Text = dataLoan.sFHAHousingActSection;
            sAgencyCaseNum.Text = dataLoan.sAgencyCaseNum;
            Tools.SetDropDownListValue(sFHAConstructionT, dataLoan.sFHAConstructionT);

            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisPurchaseUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            FHACreditAnalysisPurchaseUnderwriterLicenseNumOfAgent.Text = underwriter.LicenseNumOfAgent;
            FHACreditAnalysisPurchaseUnderwriterPreparerName.Text = underwriter.PreparerName;

            sLAmt.Text = dataLoan.sLAmtCalc_rep;
            sPresLTotHExp.Text = dataLoan.sPresLTotHExp_rep;
            sFfUfmip1003.Text = dataLoan.sFfUfmip1003_rep;
            sTermInYr.Text = dataLoan.sTermInYr_rep;
            sFinalLAmt.Text = dataLoan.sFinalLAmt_rep;
            sNoteIR.Text = dataLoan.sNoteIR_rep;
            sNoteIR.ReadOnly = dataLoan.sIsRateLocked || IsReadOnly;
            sApprVal.Text = dataLoan.sApprVal_rep;
            sBuydownResultIR.Text = dataLoan.sBuydownResultIR_rep;
            sFHACcTot.Text = dataLoan.sFHACcTot_rep;
            sFHACcPbs.Text = dataLoan.sFHACcPbs_rep;
            sFHAPurchPrice.Text  = dataLoan.sFHAPurchPrice_rep;
            string shareVal = dataLoan.sFHACcPbb_rep;
            sFHACcPbb2.Text = shareVal;
            sFHACcPbb.Text = shareVal;
            sFHAUnadjAcquisition.Text = dataLoan.sFHAUnadjAcquisition_rep;
            sFHAStatutoryInvestReq.Text = dataLoan.sFHAStatutoryInvestReq_rep;
            sFHAHouseVal.Text = dataLoan.sFHAHouseVal_rep;
            sFHAReqAdj.Text = dataLoan.sFHAReqAdj_rep;
            sFHAMBasis.Text = dataLoan.sFHAMBasis_rep;
            sFHALtvLckd.Checked = dataLoan.sFHALtvLckd;
            sFHALtv.Text = dataLoan.sFHALtv_rep;
            sFHAMAmt.Text = dataLoan.sFHAMAmt_rep;
            sFHAMinDownPmt.Text = dataLoan.sFHAMinDownPmt_rep;
            sFHAPrepaidExp.Text = dataLoan.sFHAPrepaidExp_rep;
            sFHADiscountPoints.Text = dataLoan.sFHADiscountPoints_rep;
            sFHAImprovements.Text = dataLoan.sFHAImprovements_rep;
            sUfCashPd.Text = dataLoan.sUfCashPd_rep;
            sFHANonrealty.Text = dataLoan.sFHANonrealty_rep;
            sFHATotCashToClose.Text = dataLoan.sFHATotCashToClose_rep;
            sFHAAmtPaid.Text = dataLoan.sFHAAmtPaid_rep;           
            sFHA2ndMSrc.Text = dataLoan.sFHA2ndMSrc;
            sFHA2ndMAmt.Text = dataLoan.sFHA2ndMAmt_rep;
            sFHACashReserves.Text = dataLoan.sFHACashReserves_rep;
           
            
            sFHALtv2.Text = dataLoan.sFHALtvPurch_rep;
            sFHAPurchPrice2.Text = dataLoan.sFHAPurchPrice_rep;
            sFHASellerContribution.Text = dataLoan.sFHASellerContribution_rep;
            sFHAExcessContribution.Text = dataLoan.sFHAExcessContribution_rep;
            sFHACreditAnalysisRemarks.Text = dataLoan.sFHACreditAnalysisRemarks;
            sFHAGrossMonI.Text = dataLoan.sFHAGrossMonI_rep;

            sFfUfmipR.Text					= dataLoan.sFfUfmipR_rep;
            sMipPiaMon.Text = dataLoan.sMipPiaMon_rep;
            sFfUfmip1003Lckd.Checked		= dataLoan.sFfUfmip1003Lckd;
            sFfUfmip1003.Text				= dataLoan.sFfUfmip1003_rep;
            sUfCashPdzzz2.Text				= dataLoan.sUfCashPd_rep;
            sFHAAmtPaidByCash.Checked		= dataLoan.sFHAAmtPaidByCash;
            sFHAAmtPaidByOtherT.Checked		= dataLoan.sFHAAmtPaidByOtherT;
            sFHAEnergyEffImprov.Text		= dataLoan.sFHAEnergyEffImprov_rep;
            sFfUfmipFinanced.Text = dataLoan.sFfUfmipFinanced_rep;
            sCombinedCoborUsing2ndAppBorr.Checked = dataLoan.sCombinedCoborUsing2ndAppBorr;

            Tools.SetDropDownListValue(sFHAPurchPriceSrcT, dataLoan.sFHAPurchPriceSrcT);

            sFHAIncomeLckd.Checked = dataLoan.sFHAIncomeLckd;
            sFHADebtLckd.Checked = dataLoan.sFHADebtLckd;
            sFHANegCfRentalI.Text = dataLoan.sFHANegCfRentalI_rep;
            sFHA203kInvestmentRequiredPc.Text = dataLoan.sFHA203kInvestmentRequiredPc_rep;
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            Tools.Bind_sLT(sLT);
            Tools.Bind_sFHAHousingActSection(sFHAHousingActSection);

            Tools.Bind_sFHAConstructionT(sFHAConstructionT);
            this.PageID = "FHA_92900_PUR_Combined";
            this.PageTitle = "FHA MCAW Purchase Combined";
            this.PDFPrintClass = typeof(LendersOffice.Pdf.CHUD_92900_PUR_CombinedPDF);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
            IsAppSpecific = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        protected void PageLoad(object sender, System.EventArgs e)
        {
        
        }

	}
}
