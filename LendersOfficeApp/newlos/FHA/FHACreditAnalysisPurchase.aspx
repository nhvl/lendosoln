<%@ Page language="c#" Codebehind="FHACreditAnalysisPurchase.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHACreditAnalysisPurchase" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
		<title>FHACreditAnalysisPurchase</title>
  </HEAD>
	<body class="RightBackground" MS_POSITIONING="FlowLayout">
	<script type="text/javascript">
    function _init() {

      lockField(<%= AspxTools.JsGetElementById(sFHALtvLckd) %>, 'sFHALtv');
      lockField(<%= AspxTools.JsGetElementById(sFHANonrealtyLckd) %>, 'sFHANonrealty');
      lockField(<%= AspxTools.JsGetElementById(sFfUfmip1003Lckd) %>, 'sFfUfmip1003');
      lockField(<%= AspxTools.JsGetElementById(sFHAProMInsLckd) %>, 'sFHAProMIns');
      var v = <%= AspxTools.JsGetElementById(sFHAPurchPriceSrcT) %>.value;
      document.getElementById("sFHAPurchPrice").readOnly = v != 2;
      <% if (IsReadOnly) { %>
        document.getElementById("btnCopyFromLoanApp").disabled = true;
        document.getElementById("btnCopyFromGFE").disabled = true;
      <% } %>
    }

    function updateBorrowerName() {
      var name = <%= AspxTools.JsGetElementById(aBLastNm) %>.value + ", " + <%= AspxTools.JsGetElementById(aBFirstNm) %>.value;
      parent.info.f_updateApplicantDDL(name, <%= AspxTools.JsString(ApplicationID) %>);
    }

    function handlingError(result)
    {
      var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
      if (result.ErrorType === 'VersionMismatchException')
      {
        f_displayVersionMismatch();
      }
      else if (result.ErrorType === 'LoanFieldWritePermissionDenied')
      {
        f_displayFieldWriteDenied(result.UserMessage);
      }
      else
      {
        alert(errMsg);
      }

    }

    function __executeCalculation(methodName)
    {
			PolyShouldShowConfirmSave(isDirty(), function(confirmed){
				var args = getAllFormValues();
				args["loanid"] = ML.sLId;
				args["applicationid"] = ML.aAppId;
				args["issave"] = confirmed ? 'T' : 'F';
				var result = gService.loanedit.call(methodName, args);
				if (!result.error) {
					populateForm(result.value); // Refresh value;
					_init();
					clearDirty();
				} else {
					handlingError(result);
				}
			});
    }

		function copyFromGFE() {
      __executeCalculation("CopyFromGFE");
    }
    function copyFromLoanApp() {
      __executeCalculation("CopyFromLoanApp");
    }

    function validateAR(control, event) {
      updateDirtyBit(event);
      if (event != null) {
        if (event.keyCode == 65) control.value = 'A';
        else if (event.keyCode == 82) control.value = 'R';
        else if (event.keyCode < 127 && event.keyCode > 31) control.value = '';
      } else {
        var value = '';
        if (control.value.length > 0) value = control.value.charAt(0);
        if (value == 'a' || value == 'A') control.value = 'A';
        else if (value == 'r' || value == 'R') control.value = 'A';
        else control.value = '';
      }

    }
	</script>
		<form id="FHACreditAnalysisPurchase" method="post" runat="server">
			<table class="FormTable" id="Table8" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td class="MainRightHeader">FHA MCAW - Purchase (HUD-92900-PUR)</td>
				</tr>
				<tr>
					<td>
						<input type="button" value="Copy Data From Loan App" onclick="copyFromLoanApp();" id="btnCopyFromLoanApp"><input type="button" value="Copy Closing Cost Data to Form" onclick="copyFromGFE();" id="btnCopyFromGFE"></td>
				</tr>
				<tr>
					<td>
						<table id="Table9" cellSpacing="0" cellPadding="0" border="0">
              <tr>
                <td class="FieldLabel">Loan Type</td>
                <td>
                  <asp:DropDownList ID="sLT" runat="server" />
                </td>
                <td class="FieldLabel" nowrap></td>
                <td></td>
                <td class="FieldLabel"></td>
                <td></td>
              </tr>
              <tr>
                <td class="FieldLabel">Case Number</td>
                <td>
                  <asp:TextBox ID="sAgencyCaseNum" runat="server" Width="108px" /></td>
                <td class="FieldLabel" nowrap>Housing Act</td>
                <td>
                  <ml:ComboBox ID="sFHAHousingActSection" runat="server" Width="70px" /></td>
                <td class="FieldLabel">Construction</td>
                <td>
                  <asp:DropDownList ID="sFHAConstructionT" runat="server" />
                </td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>CHUMS ID Number</td>
                <td>
                  <asp:TextBox ID="FHACreditAnalysisPurchaseUnderwriterLicenseNumOfAgent" runat="server" Width="108px" /></td>
                <td class="FieldLabel">Underwriter Name</td>
                <td>
                  <asp:TextBox ID="FHACreditAnalysisPurchaseUnderwriterPreparerName" runat="server" /></td>
                <td class="FieldLabel"></td>
                <td></td>
              </tr>
            </table>
					</td>
				</tr>
        <tr>
          <td>
            <table id="Table5" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td class="FieldLabel" nowrap colspan="2">Borrower Information</td>
                <td nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td class="FieldLabel" nowrap colspan="2">Coborrower Information</td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>First Name</td>
                <td nowrap>
                  <asp:TextBox ID="aBFirstNm" runat="server" onchange="updateBorrowerName();" /></td>
                <td nowrap>&nbsp;&nbsp;</td>
                <td class="FieldLabel" nowrap>First Name</td>
                <td nowrap>
                  <asp:TextBox ID="aCFirstNm" runat="server" TabIndex="5" /></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>Middle Name</td>
                <td nowrap>
                  <asp:TextBox ID="aBMidNm" runat="server" /></td>
                <td nowrap></td>
                <td class="FieldLabel" nowrap>Middle Name</td>
                <td nowrap>
                  <asp:TextBox ID="aCMidNm" runat="server" TabIndex="5" /></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>Last Name</td>
                <td nowrap>
                  <asp:TextBox ID="aBLastNm" runat="server" onchange="updateBorrowerName();" /></td>
                <td nowrap></td>
                <td class="FieldLabel" nowrap>Last Name</td>
                <td nowrap>
                  <asp:TextBox ID="aCLastNm" runat="server" TabIndex="5" /></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>Suffix</td>
                <td nowrap>
                  <ml:ComboBox ID="aBSuffix" runat="server" Width="102px"></ml:ComboBox></td>
                <td nowrap></td>
                <td class="FieldLabel" nowrap>Suffix</td>
                <td nowrap>
                  <ml:ComboBox ID="aCSuffix" runat="server" Width="99px" TabIndex="5"></ml:ComboBox></td>
              </tr>
              <tr>
                <td class="FieldLabel" nowrap>SSN</td>
                <td nowrap>
                  <ml:SSNTextBox ID="aBSSN" runat="server" Width="75px" preset="ssn" /></td>
                <td nowrap></td>
                <td class="FieldLabel" nowrap>SSN</td>
                <td nowrap>
                  <ml:SSNTextBox ID="aCSSN" runat="server" Width="75px" preset="ssn" TabIndex="5" /></td>
              </tr>
            </table>
          </td>
        </tr>
				<tr>
					<td>
						<table id="Table10" cellSpacing="0" cellPadding="0" border="0">
							<tr id="row1">
								<td class="FieldLabel" noWrap>3a. Mortgage w/o Upfront
									MIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td><ml:moneytextbox id="sLAmt" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
								<td width="2%"></td>
								<td class="FieldLabel" noWrap>6. Current Housing
									Expenses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td><ml:moneytextbox id="sPresLTotHExp" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask" tabindex="15"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel">3b. Total UFMIP
									<ml:percenttextbox id="sFfUfmipR" runat="server" width="70" preset="percent" onchange="refreshCalculation();" tabindex="10"  decimalDigits="6" />&nbsp;for
<asp:TextBox id=sMipPiaMon tabIndex=10 runat="server" Width="30px" onchange="refreshCalculation();"></asp:TextBox>&nbsp;mos.
            =<asp:CheckBox id=sFfUfmip1003Lckd runat="server" onclick="refreshCalculation();" tabindex=10></asp:CheckBox></td>
								<td><ml:moneytextbox id="sFfUfmip1003" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="10"></ml:moneytextbox></td>
								<td></td>
								<td class="FieldLabel">7. Term of loan (years)</td>
								<td><asp:textbox id="sTermInYr" runat="server" Width="46px" MaxLength="3" ReadOnly="True"></asp:textbox></td>
							</tr>
							<TR>
								<TD class="FieldLabel">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MIP Paid in
            Cash&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (-)</TD>
								<TD><ml:moneytextbox id="sUfCashPdzzz2" runat="server" width="100px" preset="money" onchange="refreshCalculation();" tabindex="10"></ml:moneytextbox></TD>
								<TD></TD>
								<TD class="FieldLabel">8. Interest Rate</TD>
								<TD><ml:percenttextbox id="sNoteIR" runat="server" width="100" preset="percent" ReadOnly="True" CssClass="mask" tabindex=15></ml:percenttextbox></TD>
							</TR>
        <tr>
          <td class=FieldLabel>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PMI, MIP, FF
            financed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; =</td>
          <td><ml:MoneyTextBox id=sFfUfmipFinanced runat="server" preset="money" width="100px" ReadOnly="True"></ml:MoneyTextBox></td>
          <td></td>
          <td class=FieldLabel></td>
          <td></td></tr>
							<tr>
								<td class="FieldLabel">3c. Mortgage with UFMIP</td>
								<td><ml:moneytextbox id="sFinalLAmt" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
								<td></td>
								<td class="FieldLabel">9. Adj Buy-down Interest Rate</td>
								<td><ml:percenttextbox id="sBuydownResultIR" runat="server" width="100" preset="percent" ReadOnly="True" CssClass="mask"></ml:percenttextbox></td>
							</tr>
							<TR>
								<td class="FieldLabel">4. Appraised Value (w/o CC)</td>
								<td><ml:moneytextbox id="sApprVal" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="10"></ml:moneytextbox></td>
								<td></td>
								<td class="FieldLabel" noWrap>FHA Max Loan Amount</td>
								<td><ml:MoneyTextBox id=sFHAMAmt2 runat="server" preset="money" width="100px" ReadOnly="True"></ml:MoneyTextBox></td>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>5a. Total Closing Costs (CC)&nbsp;</TD>
								<td><ml:moneytextbox id="sFHACcTot" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="10"></ml:moneytextbox></td>
								<td></td>
								<td class="FieldLabel">Energy Efficient Improvements</td>
								<td><ml:moneytextbox id="sFHAEnergyEffImprov" runat="server" width="90" preset="money" onchange="refreshCalculation();" tabindex="15"></ml:moneytextbox></td>
							</TR>
							<tr>
								<td class="FieldLabel">5b. Less paid by seller</td>
								<td><ml:moneytextbox id="sFHACcPbs" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="10"></ml:moneytextbox></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="FieldLabel">5c. Equals Borrower's CC</td>
								<td><ml:moneytextbox id="sFHACcPbb" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask" tabindex="10"></ml:moneytextbox></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table id="Table11" cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td class="FieldLabel" noWrap>10. Statutory Investment Requirements&nbsp;
								</td>
								<td noWrap></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Contract
									Sales
									Price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:DropDownList id="sFHAPurchPriceSrcT" runat="server" tabindex=20 onchange="refreshCalculation();">
<asp:ListItem Value="0" Selected="True">Purchase Price</asp:ListItem>
<asp:ListItem Value="1">203k WS line C3</asp:ListItem>
<asp:ListItem Value="2">Locked</asp:ListItem>
									</asp:DropDownList></td>
								<td noWrap><ml:moneytextbox id="sFHAPurchPrice" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask" tabindex=20></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Borrower-Paid
									Closing Costs (from 5c)&nbsp;&nbsp;
								</td>
								<td noWrap><ml:moneytextbox id="sFHACcPbb2" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Unadjusted
									Acquisition (10a + 10b)</td>
								<td noWrap><ml:moneytextbox id="sFHAUnadjAcquisition" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Statutory
									Investment Requirement (10a&nbsp;x <ml:PercentTextBox ID="sFHA203kInvestmentRequiredPc" runat="server" tabindex="20" onchange="refreshCalculation();"/>)&nbsp;&nbsp;
								</td>
								<td noWrap><ml:moneytextbox id="sFHAStatutoryInvestReq" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>11. Maximum Mortgage Calculation</td>
								<td noWrap></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Lesser of Sales
									Price (10a) or Value (from 4)</td>
								<td noWrap><ml:moneytextbox id="sFHAHouseVal" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Required
									Adjustments (+/-)</td>
								<td noWrap><ml:moneytextbox id="sFHAReqAdj" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Mortgage
									Basis (11a + 11b)</td>
								<td noWrap><ml:moneytextbox id="sFHAMBasis" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Mortgage
									Amount (11c x LTV Factor<asp:checkbox id="sFHALtvLckd" onclick="refreshCalculation();" runat="server" Text="Lock" tabindex="20"></asp:checkbox><ml:percenttextbox id="sFHALtv" runat="server" width="100" preset="percent" CssClass="mask" tabindex="20" onchange="refreshCalculation();"></ml:percenttextbox>)</td>
								<td noWrap><ml:moneytextbox id="sFHAMAmt" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>12. Cash Investment Requirements</td>
								<td noWrap></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Min Down Payment
									(10c - 11d) (Must equal or exceed 10d)</td>
								<td noWrap><ml:moneytextbox id="sFHAMinDownPmt" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Prepaid Expenses</td>
								<td noWrap><ml:moneytextbox id="sFHAPrepaidExp" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Discount Points</td>
								<td noWrap><ml:moneytextbox id="sFHADiscountPoints" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d.
									Repairs/Improvements (Non-Financeable)</td>
								<td noWrap><ml:moneytextbox id="sFHAImprovements" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e. Upfront MIP Paid in
									Cash</td>
								<td noWrap><ml:moneytextbox id="sUfCashPd" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;f. Non-Realty and
									Other Items&nbsp;&nbsp;&nbsp;<asp:checkbox id="sFHANonrealtyLckd" onclick="refreshCalculation();" runat="server" Text="Lock" tabindex="20"></asp:checkbox></td>
								<td noWrap><ml:moneytextbox id="sFHANonrealty" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; g. Total Cash to Close
									(Sum of 12a thru 12f)</td>
								<td noWrap><ml:moneytextbox id="sFHATotCashToClose" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h. Amount Paid
									(Earnest Money, etc.)<asp:checkbox id="sFHAAmtPaidByCash" runat="server" Text="Cash" tabindex="20"></asp:checkbox><asp:checkbox id="sFHAAmtPaidByOtherT" runat="server" Text="Other" tabindex="20"></asp:checkbox></td>
								<td noWrap><ml:moneytextbox id="sFHAAmtPaid" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i. Amount of Gift
									Funds&nbsp; Src:<asp:textbox id="aFHAGiftFundSrc" runat="server" Width="218px" tabindex="20"></asp:textbox></td>
								<td noWrap><ml:moneytextbox id="aFHAGiftFundAmt" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; j. Asset Available</td>
								<td noWrap><ml:moneytextbox id="aFHAAssetAvail" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; k. 2nd Mortgage (if
									applicable) Src:<asp:textbox id="sFHA2ndMSrc" runat="server" Width="197px" tabindex="20"></asp:textbox></td>
								<td noWrap><ml:moneytextbox id="sFHA2ndMAmt" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; l. Cash Reserves (Sum
									12h thru 12k, minus 12g)</td>
								<td noWrap><ml:moneytextbox id="sFHACashReserves" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>13. Monthly Effective Income</td>
								<td noWrap></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Borrower's Base Pay</td>
								<td noWrap><ml:moneytextbox id="aFHABBaseI" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Borrower's Other
									Earnings</td>
								<td noWrap><ml:moneytextbox id="aFHABOI" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Co-borrower's Base
									Pay</td>
								<td noWrap><ml:moneytextbox id="aFHACBaseI" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Co-borrower's Other
									Earnings</td>
								<td noWrap><ml:moneytextbox id="aFHACOI" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e. Net Income From
									Real Estate</td>
								<td noWrap><ml:moneytextbox id="aFHANetRentalI" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f. Gross Monthly
									Income</td>
								<td noWrap><ml:moneytextbox id="aFHAGrossMonI" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask" tabindex="20"></ml:moneytextbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table id="Table1" cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td class="FieldLabel" noWrap>14. Debts &amp; Obligations
								</td>
								<td class="FieldLabel" noWrap>Monthly Payment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
								<td class="FieldLabel" noWrap>Unpaid Balance</td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Total
									installment debt&nbsp;&nbsp;
								</td>
								<td noWrap><ml:moneytextbox id="aFHADebtInstallPmt" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
								<td noWrap><ml:moneytextbox id="aFHADebtInstallBal" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Child
									Support, etc.</td>
								<td noWrap><ml:moneytextbox id="aFHAChildSupportPmt" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
								<td noWrap></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Other</td>
								<td noWrap><ml:moneytextbox id="aFHAOtherDebtPmt" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
								<td noWrap><ml:moneytextbox id="aFHAOtherDebtBal" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
        <TR>
          <TD class=FieldLabel
            noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Other (Negative Cashflow)</TD>
          <TD noWrap><ml:moneytextbox id=aFHANegCfRentalI tabIndex=20 runat="server" onchange="refreshCalculation();" preset="money" width="100" CssClass="mask"></ml:moneytextbox></TD>
          <TD noWrap></TD></TR>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Total monthly
									payments&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
								<td noWrap><ml:moneytextbox id="aFHADebtPmtTot" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
								<td noWrap></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table id="Table2" cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td class="FieldLabel" noWrap>15. Future Monthly Payments&nbsp;
								</td>
								<td noWrap></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Principal &amp;
									Interest - 1st Mortgage&nbsp;
								</td>
								<td noWrap><ml:moneytextbox id="sFHAPro1stMPmt" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Monthly MIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="sFHAProMInsLckd" runat="server" TabIndex="20" onclick="refreshCalculation();" Text="Lock"/></td>
								<td noWrap><ml:moneytextbox id="sFHAProMIns" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20" /></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Homeowners
									Association Fee</td>
								<td noWrap><ml:moneytextbox id="sFHAProHoAssocDues" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Ground Rent</td>
								<td noWrap><ml:moneytextbox id="sFHAProGroundRent" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e. Principal &amp;
									Interest - 2nd Mortgage</td>
								<td noWrap><ml:moneytextbox id="sFHAPro2ndFinPmt" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f. Hazard Insurance</td>
								<td noWrap><ml:moneytextbox id="sFHAProHazIns" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; g. Taxes &amp; special
									assessments</td>
								<td noWrap><ml:moneytextbox id="sFHAProRealETx" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h. Total mortgage
									payment</td>
								<td noWrap><ml:moneytextbox id="sFHAMonthlyPmt" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i. Recurring expenses
									(from 14d)</td>
								<td noWrap><ml:moneytextbox id="aFHADebtPmtTot2" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; j. Total fixed payment</td>
								<td noWrap><ml:moneytextbox id="aFHAPmtFixedTot" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>16. Ratios</td>
								<td noWrap></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Loan-to-value (11d
									divided by 11a)</td>
								<td noWrap><ml:percenttextbox id="sFHALtv2" runat="server" width="100" preset="percent" ReadOnly="True" CssClass="mask"></ml:percenttextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Mortgage
									payment-to-income (15h divided by 13f)&nbsp;
								</td>
								<td noWrap><ml:percenttextbox id="aFHAMPmtToIRatio" runat="server" width="100" preset="percent" ReadOnly="True" CssClass="mask"></ml:percenttextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Total fixed
									payment-to-income (15j divided by 13f)</td>
								<td noWrap><ml:percenttextbox id="aFHAFixedPmtToIRatio" runat="server" width="100" preset="percent" ReadOnly="True" CssClass="mask"></ml:percenttextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>17. Borrower Rating (enter "A" for acceptable or "R"
									for reject)</td>
								<td noWrap></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Credit
									characteristics</td>
								<td noWrap><asp:textbox id="aFHACreditRating" onkeyup="validateAR(this, event);" runat="server" Width="30px" tabindex="20"></asp:textbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Adequacy of
									effective income</td>
								<td noWrap><asp:textbox id="aFHARatingIAdequacy" onkeyup="validateAR(this, event);" runat="server" Width="30px" tabindex="20"></asp:textbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Stability of
									effective income</td>
								<td noWrap><asp:textbox id="aFHARatingIStability" onkeyup="validateAR(this, event);" runat="server" Width="30px" tabindex="20"></asp:textbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Adequacy of
									available assets</td>
								<td noWrap><asp:textbox id="aFHARatingAssetAdequacy" onkeyup="validateAR(this, event);" runat="server" Width="30px" tabindex="20"></asp:textbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap></td>
								<td noWrap></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table id="Table3" cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td class="FieldLabel" noWrap>18. CAIVRS -
									LDP/GSA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</td>
								<td class="FieldLabel" noWrap>Borrower</td>
								<td class="FieldLabel" noWrap>Co-borrower</td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CAIVRS Number</td>
								<td noWrap><asp:textbox id="aFHABCaivrsNum" runat="server" Width="164px" tabindex="20"></asp:textbox></td>
								<td noWrap><asp:textbox id="aFHACCaivrsNum" runat="server" Width="164px" tabindex="20"></asp:textbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LDP/GSA (page
									no. &amp; date)&nbsp;&nbsp;
								</td>
								<td noWrap><asp:textbox id="aFHABLpdGsa" runat="server" Width="164px" tabindex="20"></asp:textbox></td>
								<td noWrap><asp:textbox id="aFHACLpdGsa" runat="server" Width="164px" tabindex="20"></asp:textbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table id="Table4" cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td class="FieldLabel" noWrap>Attachment A Information</td>
								<td noWrap></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp; A1. Contract Sales Price of Property
									(line 10a above)&nbsp;
								</td>
								<td noWrap><ml:moneytextbox id="sFHAPurchPrice2" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp; A2. 6% of line A1</td>
								<td noWrap><ml:moneytextbox id="sPurchPrice6pc" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp; A3. Total Seller Contribution</td>
								<td noWrap><ml:moneytextbox id="sFHASellerContribution" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
							<tr>
								<td class="FieldLabel" noWrap>&nbsp;&nbsp; A4. Excess Contribution</td>
								<td noWrap><ml:moneytextbox id="sFHAExcessContribution" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" tabindex="20"></ml:moneytextbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="FieldLabel">Remarks</td>
				</tr>
				<tr>
					<td><asp:textbox id="sFHACreditAnalysisRemarks" runat="server" Width="580px" TextMode="MultiLine" Height="160px" tabindex="20"></asp:textbox></td>
				</tr>
				<tr>
					<td></td>
				</tr>
			</table>
			&nbsp;
		</form>
	</body>
</HTML>
