﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LendersOfficeApp.newlos.FHA {
    
    
    public partial class FHAPropertyImprovement {
        
        /// <summary>
        /// sFHAPropImprovHasFedPastDueTri control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList sFHAPropImprovHasFedPastDueTri;
        
        /// <summary>
        /// sFHAPropImprovHasFHAPendingAppTri control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList sFHAPropImprovHasFHAPendingAppTri;
        
        /// <summary>
        /// sFHAPropImprovHasFHAPendingAppWithWhom control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovHasFHAPendingAppWithWhom;
        
        /// <summary>
        /// sFHAPropImprovRefinTitle1LoanTri control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList sFHAPropImprovRefinTitle1LoanTri;
        
        /// <summary>
        /// sFHAPropImprovRefinTitle1LoanNum control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovRefinTitle1LoanNum;
        
        /// <summary>
        /// sFHAPropImprovRefinTitle1LoanBal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sFHAPropImprovRefinTitle1LoanBal;
        
        /// <summary>
        /// sFHAPropImprovBorrRelativeNm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovBorrRelativeNm;
        
        /// <summary>
        /// sFHAPropImprovCoborRelativeNm control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovCoborRelativeNm;
        
        /// <summary>
        /// sFHAPropImprovBorrRelativeStreetAddress control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovBorrRelativeStreetAddress;
        
        /// <summary>
        /// sFHAPropImprovBorrRelativeCity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovBorrRelativeCity;
        
        /// <summary>
        /// sFHAPropImprovBorrRelativeState control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.StateDropDownList sFHAPropImprovBorrRelativeState;
        
        /// <summary>
        /// sFHAPropImprovBorrRelativeZip control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.ZipcodeTextBox sFHAPropImprovBorrRelativeZip;
        
        /// <summary>
        /// sFHAPropImprovCoborRelativeStreetAddress control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovCoborRelativeStreetAddress;
        
        /// <summary>
        /// sFHAPropImprovCoborRelativeCity control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovCoborRelativeCity;
        
        /// <summary>
        /// sFHAPropImprovCoborRelativeState control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.StateDropDownList sFHAPropImprovCoborRelativeState;
        
        /// <summary>
        /// sFHAPropImprovCoborRelativeZip control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.ZipcodeTextBox sFHAPropImprovCoborRelativeZip;
        
        /// <summary>
        /// sFHAPropImprovBorrRelativeRelationship control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovBorrRelativeRelationship;
        
        /// <summary>
        /// sFHAPropImprovCoborRelativeRelationship control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovCoborRelativeRelationship;
        
        /// <summary>
        /// sFHAPropImprovBorrRelativePhone control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PhoneTextBox sFHAPropImprovBorrRelativePhone;
        
        /// <summary>
        /// sFHAPropImprovCoborRelativePhone control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.PhoneTextBox sFHAPropImprovCoborRelativePhone;
        
        /// <summary>
        /// sFHAPropImprovBorrHasChecking control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovBorrHasChecking;
        
        /// <summary>
        /// sFHAPropImprovBorrHasSaving control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovBorrHasSaving;
        
        /// <summary>
        /// sFHAPropImprovBorrHasNoBankAcount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovBorrHasNoBankAcount;
        
        /// <summary>
        /// sFHAPropImprovCoborHasChecking control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovCoborHasChecking;
        
        /// <summary>
        /// sFHAPropImprovCoborHasSaving control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovCoborHasSaving;
        
        /// <summary>
        /// sFHAPropImprovCoborHasNoBankAcount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovCoborHasNoBankAcount;
        
        /// <summary>
        /// sFHAPropImprovBorrBankInfo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovBorrBankInfo;
        
        /// <summary>
        /// sFHAPropImprovCoborBankInfo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovCoborBankInfo;
        
        /// <summary>
        /// sFHAPropImprovIsPropSingleFamily control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsPropSingleFamily;
        
        /// <summary>
        /// sFHAPropImprovIsPropOwnedByBorr control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsPropOwnedByBorr;
        
        /// <summary>
        /// sFHAPropImprovIsPropMultifamily control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsPropMultifamily;
        
        /// <summary>
        /// sUnitsNum control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sUnitsNum;
        
        /// <summary>
        /// sFHAPropImprovIsPropLeasedFromSomeone control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsPropLeasedFromSomeone;
        
        /// <summary>
        /// sFHAPropImprovIsPropNonresidential control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsPropNonresidential;
        
        /// <summary>
        /// sFHAPropImprovNonresidentialUsageDesc control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovNonresidentialUsageDesc;
        
        /// <summary>
        /// sFHAPropImprovIsPropBeingPurchasedOnContract control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsPropBeingPurchasedOnContract;
        
        /// <summary>
        /// sFHAPropImprovIsPropManufacturedHome control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsPropManufacturedHome;
        
        /// <summary>
        /// sFHAPropImprovIsThereMortOnProp control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsThereMortOnProp;
        
        /// <summary>
        /// sFHAPropImprovIsPropHistoricResidential control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsPropHistoricResidential;
        
        /// <summary>
        /// sFHAPropImprovIsPropHistoricResidentialUnitsNum control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovIsPropHistoricResidentialUnitsNum;
        
        /// <summary>
        /// sFHAPropImprovIsPropHealthCareFacility control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsPropHealthCareFacility;
        
        /// <summary>
        /// sFHAPropImprovLeaseOwnerInfo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovLeaseOwnerInfo;
        
        /// <summary>
        /// sFHAPropImprovLeaseMonPmt control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.MoneyTextBox sFHAPropImprovLeaseMonPmt;
        
        /// <summary>
        /// sFHAPropImprovLeaseExpireD control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MeridianLink.CommonControls.DateTextBox sFHAPropImprovLeaseExpireD;
        
        /// <summary>
        /// sFHAPropImprovIsPropNewOccMoreThan90Days control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox sFHAPropImprovIsPropNewOccMoreThan90Days;
        
        /// <summary>
        /// sFHAPropImprovDealerContractorContactInfo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox sFHAPropImprovDealerContractorContactInfo;
    }
}
