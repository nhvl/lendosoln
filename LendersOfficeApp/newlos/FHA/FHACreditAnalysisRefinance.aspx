<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="FHACreditAnalysisRefinance.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHACreditAnalysisRefinance" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
		<title>FHACreditAnalysisRefinance</title>
  </HEAD>
	<body class="RightBackground" MS_POSITIONING="FlowLayout">
		<script type="text/javascript">

    function _init() {
      lockField(<%= AspxTools.JsGetElementById(sFHAMBasisRefinMultiplyLckd) %>, 'sFHAMBasisRefinMultiply');
      lockField(<%= AspxTools.JsGetElementById(sFHAApprValMultiplyLckd) %>, 'sFHAApprValMultiply');
      lockField(<%= AspxTools.JsGetElementById(sFHANonrealtyLckd) %>, 'sFHANonrealty');
      lockField(<%= AspxTools.JsGetElementById(sFfUfmip1003Lckd) %>, 'sFfUfmip1003');
      lockField(<%= AspxTools.JsGetElementById(sFHAProMInsLckd) %>, 'sFHAProMIns');
      <% if (IsReadOnly) { %>
        document.getElementById("btnCopyFromLoanApp").disabled = true;
        document.getElementById("btnCopyFromGFE").disabled = true;
      <% } %>
    }
    function updateBorrowerName() {
      var name = <%= AspxTools.JsGetElementById(aBLastNm) %>.value + ", " + <%= AspxTools.JsGetElementById(aBFirstNm) %>.value;
      parent.info.f_updateApplicantDDL(name, <%= AspxTools.JsString(ApplicationID) %>);
    }
    function handlingError(result)
    {
      var errMsg = result.UserMessage || 'Unable to save data. Please try again.';
      if (result.ErrorType === 'VersionMismatchException')
      {
        f_displayVersionMismatch();
      }
      else if (result.ErrorType === 'LoanFieldWritePermissionDenied')
      {
        f_displayFieldWriteDenied(result.UserMessage);
      }
      else
      {
        alert(errMsg);
      }

    }

    function __executeCalculation(methodName)
    {
		PolyShouldShowConfirmSave(isDirty(), function(confirmed){
			var args = getAllFormValues();
			args["loanid"] = ML.sLId;
			args["applicationid"] = ML.aAppId;
			args["issave"] =  confirmed ? 'T' : 'F';
			var result = gService.loanedit.call(methodName, args);
			if (!result.error) {
				populateForm(result.value); // Refresh value;
				_init();
				clearDirty();
			} else {
				handlingError(result);
			}
      	});
    }
    function copyFromGFE() {
      __executeCalculation("CopyFromGFE");
    }
    function copyFromLoanApp() {
      __executeCalculation("CopyFromLoanApp");
    }
    function validateAR(control, event) {
      updateDirtyBit(event);
      if (event != null) {
        if (event.keyCode == 65) control.value = 'A';
        else if (event.keyCode == 82) control.value = 'R';
        else if (event.keyCode < 127 && event.keyCode > 31) control.value = '';
      } else {
        var value = '';
        if (control.value.length > 0) value = control.value.charAt(0);
        if (value == 'a' || value == 'A') control.value = 'A';
        else if (value == 'r' || value == 'R') control.value = 'A';
        else control.value = '';
      }
    }

		</script>
		<form id="FHACreditAnalysisRefinance" method="post" runat="server">
			<TABLE class="FormTable" id="Table8" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="MainRightHeader">FHA MCAW - Refinance (HUD-92900-WS)</TD>
				</TR>
				<tr>
					<td><input onclick="copyFromLoanApp();" type="button" value="Copy Data From Loan App" id=btnCopyFromLoanApp><input onclick="copyFromGFE();" type="button" value="Copy Closing Cost Data to Form" id=btnCopyFromGFE></td>
				</tr>
				<TR>
					<TD>
						<TABLE id="Table9" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel">Loan Type</TD>
								<TD><asp:dropdownlist id="sLT" runat="server"/></TD>
								<TD class="FieldLabel" noWrap></TD>
								<TD></TD>
								<TD class="FieldLabel"></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Case Number</TD>
								<TD><asp:textbox id="sAgencyCaseNum" runat="server" Width="108px"/></TD>
								<TD class="FieldLabel" noWrap>Housing Act</TD>
								<TD><ml:combobox id="sFHAHousingActSection" runat="server" Width="70px"/></TD>
								<TD class="FieldLabel">Construction</TD>
								<TD><asp:dropdownlist id="sFHAConstructionT" runat="server" Width="121px"/>
									</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>CHUMS ID Number</TD>
								<TD><asp:textbox id="FHACreditAnalysisRefinanceLicenseNumOfAgent" runat="server" Width="108px"/></TD>
								<TD class="FieldLabel">Underwriter Name</TD>
								<TD colSpan="3"><asp:TextBox id=FHACreditAnalysisRefiancePreparerName runat="server"/></TD>
							</TR>
        <tr>
          <td class=FieldLabel nowrap>Refinance Type </td>
          <td colspan=2><ml:combobox id="sFHARefinanceTypeDesc" runat="server" Width="192px" Text=""/></td>
          <td colspan=3></td></tr>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table5" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap colSpan="2">Borrower Information</TD>
								<TD noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
								<TD class="FieldLabel" noWrap colSpan="2">Coborrower Information</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>First Name</TD>
								<TD noWrap><asp:textbox id="aBFirstNm" tabIndex="3" runat="server" maxlength="21" onchange="updateBorrowerName();"></asp:textbox></TD>
								<TD noWrap>&nbsp;&nbsp;</TD>
								<TD class="FieldLabel" noWrap>First Name</TD>
								<TD noWrap><asp:textbox id="aCFirstNm" tabIndex="5" runat="server" maxlength="21"/></TD>
							</TR>
							<tr>
								<td class="FieldLabel" noWrap>Middle Name</td>
								<td noWrap><asp:textbox id="aBMidNm" runat="server" tabIndex="3" maxlength="21"/></td>
								<td noWrap></td>
								<td class="FieldLabel" noWrap>Middle Name</td>
								<td noWrap><asp:textbox id="aCMidNm" tabIndex="5" runat="server" maxlength="21"/></td>
							</tr>
							<TR>
								<TD class="FieldLabel" noWrap>Last Name</TD>
								<TD noWrap><asp:textbox id="aBLastNm" runat="server" tabIndex="3" maxlength="21" onchange="updateBorrowerName();"></asp:textbox></TD>
								<TD noWrap></TD>
								<TD class="FieldLabel" noWrap>Last Name</TD>
								<TD noWrap><asp:textbox id="aCLastNm" tabIndex="5" runat="server" maxlength="21"/></TD>
							</TR>
							<tr>
								<td class="FieldLabel" noWrap>Suffix</td>
								<td noWrap><ml:ComboBox id="aBSuffix" runat="server" maxlength="21" tabIndex="3"></ml:ComboBox></td>
								<td noWrap></td>
								<td class="FieldLabel" noWrap>Suffix</td>
								<td noWrap><ml:ComboBox id="aCSuffix" tabIndex="5" runat="server" maxlength="21"></ml:ComboBox></td>
							</tr>
							<TR>
								<TD class="FieldLabel" noWrap>SSN</TD>
								<TD noWrap><ml:ssntextbox id="aBSSN" runat="server" width="75px" preset="ssn" tabIndex="3"/></TD>
								<TD noWrap></TD>
								<TD class="FieldLabel" noWrap>SSN</TD>
								<TD noWrap><ml:ssntextbox id="aCSSN" tabIndex="5" runat="server" width="75px" preset="ssn"/></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table10" cellSpacing="0" cellPadding="0" border="0">
							<TR id="row1">
								<TD class="FieldLabel" noWrap>3a. Mortgage w/o Upfront
									MIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
								<TD><ml:moneytextbox id="sLAmt" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
								<TD width="2%"></TD>
								<TD class="FieldLabel" noWrap>6. Current Housing
									Expenses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
								<TD><ml:moneytextbox id="sPresLTotPersistentHExp" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">3b. Total UFMIP
									<ml:percenttextbox id="sFfUfmipR" tabIndex="10" runat="server" width="70" preset="percent" onchange="refreshCalculation();"  decimalDigits="6" />&nbsp;for
<asp:TextBox id=sMipPiaMon tabIndex=10 runat="server" Width="30px" onchange="refreshCalculation();"></asp:TextBox>&nbsp;mos=

									<INPUT id="sFfUfmip1003Lckd" onclick="lockField(this, 'sFfUfmip1003'); refreshCalculation();" tabIndex="10" type="checkbox" name="sFfUfmip1003Lckd" runat="server">lock</TD>
								<TD><ml:moneytextbox id="sFfUfmip1003" tabIndex="10" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD></TD>
								<TD class="FieldLabel">7. Term of loan (years)</TD>
								<TD><asp:textbox id="sTermInYr" runat="server" Width="46px" ReadOnly="True" MaxLength="3"/></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MIP Paid in Cash</TD>
								<TD><ml:moneytextbox id="sUfCashPdzzz2" tabIndex="10" runat="server" width="100px" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD></TD>
								<TD class="FieldLabel">8. Interest Rate</TD>
								<TD><ml:percenttextbox id="sNoteIR" runat="server" tabIndex="15" width="100" preset="percent" ReadOnly="True" CssClass="mask"></ml:percenttextbox></TD>
							</TR>
							<tr>
								<td class="FieldLabel">&nbsp;&nbsp;&nbsp; PMI, MIP, FF
									financed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; =</td>
								<td><ml:moneytextbox id="sFfUfmipFinanced" runat="server" width="100px" preset="money" ReadOnly="True"></ml:moneytextbox></td>
								<td></td>
								<td class="FieldLabel"></td>
								<td></td>
							</tr>
							<TR>
								<TD class="FieldLabel">3c. Mortgage with UFMIP</TD>
								<TD><ml:moneytextbox id="sFinalLAmt" tabIndex="10" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
								<TD></TD>
								<TD class="FieldLabel">9. Adj Buy-down Interest Rate</TD>
								<TD><ml:percenttextbox id="sBuydownResultIR" runat="server" width="100" preset="percent" ReadOnly="True" CssClass="mask"></ml:percenttextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">4&nbsp;. &nbsp;Appraised Value (w/o CC)</TD>
								<TD><ml:moneytextbox id="sApprVal" tabIndex="10" runat="server" width="100" preset="money" CssClass="mask"></ml:moneytextbox></TD>
								<TD></TD>
								<TD class="FieldLabel" noWrap></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>5a. Total Closing Costs (CC)&nbsp;</TD>
								<TD><ml:moneytextbox id="sFHACcTot" tabIndex="10" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD></TD>
								<TD class="FieldLabel">Energy Efficient Improvements</TD>
								<TD><ml:moneytextbox id="sFHAEnergyEffImprov" tabIndex="15" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">5b. Less paid by seller</TD>
								<TD><ml:moneytextbox id="sFHACcPbs" tabIndex="10" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">5c. Equals Borrower's CC</TD>
								<TD><ml:moneytextbox id="sFHACcPbb" tabIndex="10" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table11" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap>10. Statutory Investment Requirements&nbsp;</TD>
								<TD noWrap></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Existing
									Mortgage Lien</TD>
								<TD noWrap><ml:moneytextbox id="sFHAExistingMLien" tabIndex="20" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Repairs &amp;
									Improvements
									<asp:textbox id="sFHAImprovementsDesc" tabIndex="20" runat="server" Width="194px"></asp:textbox></TD>
								<TD noWrap><ml:moneytextbox id="sFHAImprovements" runat="server" width="90" preset="money" tabindex=20 onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Borrower-Paid
									Closing Costs (from 5c)&nbsp;&nbsp;
								</TD>
								<TD noWrap><ml:moneytextbox id="sFHACcPbb2" runat="server" width="90px" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d. MIP
									Refund</TD>
								<TD noWrap><ml:moneytextbox id="sFHASalesConcessions" tabIndex="20" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e. Mortgage
									Basis (10a + b + c minus 10d)</TD>
								<TD noWrap><ml:moneytextbox id="sFHAMBasisRefin" runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f. (1) Appraised
									Value + CC Multiplied by LTV Factor<asp:checkbox id="sFHAMBasisRefinMultiplyLckd" onclick="lockField(this, 'sFHAMBasisRefinMultiply'); refreshCalculation();" tabIndex="20" runat="server" Text="locked"></asp:checkbox></TD>
								<TD noWrap><ml:moneytextbox id="sFHAMBasisRefinMultiply" tabIndex="20" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									(2) Appraised Value multiplied by 98.75/97.75%<asp:checkbox id="sFHAApprValMultiplyLckd" onclick="lockField(this, 'sFHAApprValMultiply'); refreshCalculation();" tabIndex="20" runat="server" Text="locked"></asp:checkbox></TD>
								<TD noWrap><ml:moneytextbox id="sFHAApprValMultiply" tabIndex="20" runat="server" width="90" preset="money"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; g. Mortgage
									(without UFMIP)</TD>
								<TD noWrap><ml:moneytextbox id="sLAmt2" runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h. Required
									Investment (10e minus 10g)</TD>
								<TD noWrap><ml:moneytextbox id="sFHAReqInvestment" runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i. Discounts</TD>
								<TD noWrap><ml:moneytextbox id="sFHADiscountPoints" tabIndex="20" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; j. Prepayable
									Expenses</TD>
								<TD noWrap><ml:moneytextbox id="sFHAPrepaidExp" tabIndex="20" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; k. MIP Paid in
									Cash</TD>
								<TD noWrap><ml:moneytextbox id="sUfCashPd" runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; l. Non-Realty
									and Other Items&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:checkbox id="sFHANonrealtyLckd" onclick="lockField(this, 'sFHANonrealty'); refreshCalculation();" tabIndex="20" runat="server" Text="Lock"></asp:checkbox></TD>
								<TD noWrap><ml:moneytextbox id="sFHANonrealty" tabIndex="20" runat="server" width="90" preset="money"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; m. Total
									Requirements (sum of lines 10h - 10l)</TD>
								<TD noWrap><ml:moneytextbox id="sFHAReqTot" runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; n. Amount paid
									in
									<asp:checkbox id="sFHAIsAmtPdInCash" tabIndex="20" runat="server" Text="cash" DESIGNTIMEDRAGDROP="53"></asp:checkbox><asp:checkbox id="sFHAIsAmtPdInOther" tabIndex="20" runat="server" Text="other" DESIGNTIMEDRAGDROP="54"></asp:checkbox></TD>
								<TD noWrap><ml:moneytextbox id="sFHAAmtPaid" tabIndex="20" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;o. Amount
									to be paid in&nbsp;<asp:checkbox id="sFHAIsAmtToBePdInCash" tabIndex="20" runat="server" Text="cash"></asp:checkbox><asp:checkbox id="sFHAIsAmtToBePdInOther" tabIndex="20" runat="server" Text="other"></asp:checkbox></TD>
								<TD noWrap><ml:moneytextbox id="sFHAAmtToBePd" tabIndex="20" runat="server" width="90" preset="money" readonly="True"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;p. Assets
									Available</TD>
								<TD noWrap><ml:moneytextbox id="aFHAAssetAvail" tabIndex="20" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;q. 2nd
									Mortgage Proceeds (if applicable)</TD>
								<TD noWrap><ml:moneytextbox id="sFHA2ndMAmt" tabIndex="20" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>11. Monthly Effective Income</TD>
								<TD noWrap></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Borrower's Base Pay</TD>
								<TD noWrap><ml:moneytextbox id="aFHABBaseI" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Borrower's Other
									Earnings</TD>
								<TD noWrap><ml:moneytextbox id="aFHABOI" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Co-borrower's Base
									Pay</TD>
								<TD noWrap><ml:moneytextbox id="aFHACBaseI" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Co-borrower's Other
									Earnings</TD>
								<TD noWrap><ml:moneytextbox id="aFHACOI" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e. Net Income From
									Real Estate</TD>
								<TD noWrap><ml:moneytextbox id="aFHANetRentalI" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f. Gross Monthly
									Income</TD>
								<TD noWrap><ml:moneytextbox id="aFHAGrossMonI" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap>12. Debts &amp; Obligations
								</TD>
								<TD class="FieldLabel" noWrap>Monthly Payment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</TD>
								<TD class="FieldLabel" noWrap>Unpaid Balance</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Total
									installment debt&nbsp;&nbsp;
								</TD>
								<TD noWrap><ml:moneytextbox id="aFHADebtInstallPmt" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap><ml:moneytextbox id="aFHADebtInstallBal" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Child
									Support, etc.</TD>
								<TD noWrap><ml:moneytextbox id="aFHAChildSupportPmt" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Other</TD>
								<TD noWrap><ml:moneytextbox id="aFHAOtherDebtPmt" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
								<TD noWrap><ml:moneytextbox id="aFHAOtherDebtBal" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
        <TR>
          <TD class=FieldLabel
            noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Other (Negative Cashflow)</TD>
          <TD noWrap><ml:moneytextbox id=aFHANegCfRentalI tabIndex=20 runat="server" onchange="refreshCalculation();" preset="money" width="100" CssClass="mask"></ml:moneytextbox></TD>
          <TD noWrap></TD></TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Total monthly
									payments&nbsp;&nbsp;&nbsp;&nbsp;
								</TD>
								<TD noWrap><ml:moneytextbox id="aFHADebtPmtTot" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
								<TD noWrap></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap>13. Future Monthly Payments&nbsp;
								</TD>
								<TD noWrap></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Principal &amp;
									Interest - 1st Mortgage&nbsp;</TD>
								<TD noWrap><ml:moneytextbox id="sFHAPro1stMPmt" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Monthly MIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="sFHAProMInsLckd" runat="server" TabIndex="20" onclick="refreshCalculation();" Text="Lock"/></TD>
								<TD noWrap><ml:moneytextbox id="sFHAProMIns" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();" /></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Homeowners
									Association Fee</TD>
								<TD noWrap><ml:moneytextbox id="sFHAProHoAssocDues" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Ground Rent</TD>
								<TD noWrap><ml:moneytextbox id="sFHAProGroundRent" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e. Principal &amp;
									Interest - 2nd Mortgage</TD>
								<TD noWrap><ml:moneytextbox id="sFHAPro2ndFinPmt" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; f. Hazard Insurance</TD>
								<TD noWrap><ml:moneytextbox id="sFHAProHazIns" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; g. Taxes &amp; special
									assessments</TD>
								<TD noWrap><ml:moneytextbox id="sFHAProRealETx" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; h. Total mortgage
									payment</TD>
								<TD noWrap><ml:moneytextbox id="sFHAMonthlyPmt" tabIndex="20" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; i. Recurring expenses
									(from 12d)</TD>
								<TD noWrap><ml:moneytextbox id="aFHADebtPmtTot2" tabIndex="20" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; j. Total fixed payment</TD>
								<TD noWrap><ml:moneytextbox id="aFHAPmtFixedTot" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>14. Ratios</TD>
								<TD noWrap></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Loan-to-value (10g
									divided by 4)</TD>
								<TD noWrap><ml:percenttextbox id="sFHALtv2" runat="server" width="100" preset="percent" ReadOnly="True" CssClass="mask"></ml:percenttextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Mortgage
									payment-to-income (13h divided by 11f)&nbsp;
								</TD>
								<TD noWrap><ml:percenttextbox id="aFHAMPmtToIRatio" runat="server" width="100" preset="percent" ReadOnly="True" CssClass="mask"></ml:percenttextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Total fixed
									payment-to-income (13j divided by 11f)</TD>
								<TD noWrap><ml:percenttextbox id="aFHAFixedPmtToIRatio" runat="server" width="100" preset="percent" ReadOnly="True" CssClass="mask"></ml:percenttextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>15. Borrower Rating (enter "A" for acceptable or "R"
									for reject)</TD>
								<TD noWrap></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; a. Credit
									characteristics</TD>
								<TD noWrap><asp:textbox id="aFHACreditRating" onkeyup="validateAR(this, event);" tabIndex="20" runat="server" Width="30px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; b. Adequacy of
									effective income</TD>
								<TD noWrap><asp:textbox id="aFHARatingIAdequacy" onkeyup="validateAR(this, event);" tabIndex="20" runat="server" Width="30px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c. Stability of
									effective income</TD>
								<TD noWrap><asp:textbox id="aFHARatingIStability" onkeyup="validateAR(this, event);" tabIndex="20" runat="server" Width="30px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d. Adequacy of
									available assets</TD>
								<TD noWrap><asp:textbox id="aFHARatingAssetAdequacy" onkeyup="validateAR(this, event);" tabIndex="20" runat="server" Width="30px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap></TD>
								<TD noWrap></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table3" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap>16. CAIVRS -
									LDP/GSA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</TD>
								<TD class="FieldLabel" noWrap>Borrower</TD>
								<TD class="FieldLabel" noWrap>Co-borrower</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CAIVRS Number</TD>
								<TD noWrap><asp:textbox id="aFHABCaivrsNum" tabIndex="20" runat="server" Width="164px"></asp:textbox></TD>
								<TD noWrap><asp:textbox id="aFHACCaivrsNum" tabIndex="20" runat="server" Width="164px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LDP/GSA (page
									no. &amp; date)&nbsp;&nbsp;
								</TD>
								<TD noWrap><asp:textbox id="aFHABLpdGsa" tabIndex="20" runat="server" Width="164px"></asp:textbox></TD>
								<TD noWrap><asp:textbox id="aFHACLpdGsa" tabIndex="20" runat="server" Width="164px"></asp:textbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table4" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD class="FieldLabel" noWrap>Attachment A Information</TD>
								<TD noWrap></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp; A1. Contract Sales Price of Property
									(line 10a above)&nbsp;
								</TD>
								<TD noWrap><ml:moneytextbox id="sPurchPrice2" tabIndex="20" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp; A2. 6% of line A1</TD>
								<TD noWrap><ml:moneytextbox id="sPurchPrice6pc" runat="server" width="100" preset="money" ReadOnly="True" CssClass="mask"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp; A3. Total Seller Contribution</TD>
								<TD noWrap><ml:moneytextbox id="sFHASellerContribution" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>&nbsp;&nbsp; A4. Excess Contribution</TD>
								<TD noWrap><ml:moneytextbox id="sFHAExcessContribution" tabIndex="20" runat="server" width="100" preset="money" CssClass="mask" onchange="refreshCalculation();"></ml:moneytextbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD class="FieldLabel">Remarks</TD>
				</TR>
				<TR>
					<TD><asp:textbox id="sFHACreditAnalysisRemarks" tabIndex="20" runat="server" Width="580px" TextMode="MultiLine" Height="160px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="FieldLabel">Total Amount of Gift<ml:moneytextbox id="sFHAGiftAmtTot" tabIndex="20" runat="server" width="90" preset="money"></ml:moneytextbox></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
