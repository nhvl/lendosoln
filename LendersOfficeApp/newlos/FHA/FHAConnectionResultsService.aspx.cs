﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace LendersOfficeApp.newlos.FHA
{
    public partial class FHAConnectionResultsServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(FHAConnectionResultsServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            
        }
        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            
        }
    }
    public partial class FHAConnectionResultsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new FHAConnectionResultsServiceItem());
        }
    }
}
