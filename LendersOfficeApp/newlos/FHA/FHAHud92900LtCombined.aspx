<%@ Page language="c#" Codebehind="FHAHud92900LtCombined.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.FHA.FHAHud92900LtCombined" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess"%>
<%@ Register TagPrefix="QRP" TagName="QualRatePopup" Src="~/newlos/Forms/QualRateCalculationPopup.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>FHAHud92900Lt</title>
  </HEAD>
<body bgColor=gainsboro scroll=yes margin="0" MS_POSITIONING="FlowLayout">
<script language=javascript>
<!--
var lPurposeWasOther = false;
function _init() {
      enableBorrowerInfo();
      enableMonthIncome();
      enableMonthlyDebt();

    lockField(<%= AspxTools.JsGetElementById(sTransmFntcLckd) %>, 'sTransmFntc');
    lockField(<%= AspxTools.JsGetElementById(sFHAProMInsLckd) %>, 'sFHAProMIns');
    lockField(<%=AspxTools.JsGetElementById(this.sQualIRLckd)%>, 'sQualIR');
      <% if (IsReadOnly) { %>
        document.getElementById("btnCopyFromLoanApp").disabled = true;
        document.getElementById("btnCopyFromGFE").disabled = true;
      <% } %>      
            
    updateLoanPurposeCheckboxes(true);
    enableDisableOtherDesc('sFHASecondaryFinancing');
    enableDisableOtherDesc('sFHAGift1');
    enableDisableOtherDesc('sFHAGift2');
    f_onChangeFinMethT();
    
    document.getElementById("sFHARiskClassAA").onclick = function(){setOpposite("AA");};
    document.getElementById("sFHARiskClassRefer").onclick = function(){setOpposite("Refer");};
    
}

    function setOpposite(name)
    {
        var referEl = document.getElementById("sFHARiskClassRefer");
        var aaEl = document.getElementById("sFHARiskClassAA");
        if(name=="Refer" && referEl.checked)
        {
            aaEl.checked = false;
        }else if(aaEl.checked){
            referEl.checked = false;
        }
        
        refreshCalculation();
    }

    function updateLoanPurposeCheckboxes(bIsInit) {
      var sFHAPurposeIsPurchase = document.getElementById('sFHAPurposeIsPurchase');
      var sFHAPurposeIsNoCashoutRefi = document.getElementById('sFHAPurposeIsNoCashoutRefi');
      var sFHAPurposeIsCashoutRefi = document.getElementById('sFHAPurposeIsCashoutRefi');
      var sFHAPurposeIsStreamlineRefi = document.getElementById('sFHAPurposeIsStreamlineRefi');
      var sFHAPurposeIsStreamlineRefiWithAppr = document.getElementById('sFHAPurposeIsStreamlineRefiWithAppr');
      var sFHAPurposeIsStreamlineRefiWithoutAppr = document.getElementById('sFHAPurposeIsStreamlineRefiWithoutAppr');
      var sFHAPurposeIsConstToPerm = document.getElementById('sFHAPurposeIsConstToPerm');
      var sFHAPurposeIsOther = document.getElementById('sFHAPurposeIsOther');
      
      if(lPurposeWasOther)
        sFHAPurposeIsOther.checked = false;
      lPurposeWasOther = false;
      sFHAPurposeIsOther.disabled = false;
      sFHAPurposeIsPurchase.checked = false;
      sFHAPurposeIsNoCashoutRefi.checked = false;
      sFHAPurposeIsCashoutRefi.checked = false;
      sFHAPurposeIsStreamlineRefi.checked = false;
      sFHAPurposeIsConstToPerm.checked = false;
      
      var lPurposeDDL = <%= AspxTools.JsGetElementById(sLPurposeT) %>;
      var lPurpose = lPurposeDDL.options[lPurposeDDL.selectedIndex].value;
      
      if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Purchase)) %>)
        sFHAPurposeIsPurchase.checked = true;
      else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Refin)) %>) 
        sFHAPurposeIsNoCashoutRefi.checked = true;
      else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.RefinCashout)) %>)
        sFHAPurposeIsCashoutRefi.checked = true;
      else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.ConstructPerm)) %>)
        sFHAPurposeIsConstToPerm.checked = true;
      else if (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.FhaStreamlinedRefinance)) %> ||
        lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.VaIrrrl)) %>)
      {
        sFHAPurposeIsStreamlineRefi.checked = true;
        sFHAPurposeIsStreamlineRefiWithAppr.disabled = false;
        sFHAPurposeIsStreamlineRefiWithoutAppr.disabled = false;
        if (! bIsInit )
        {
            var appraisedVal = <%= AspxTools.JsGetElementById(sApprVal) %>.value;
            var bDefaultNoAppr = (appraisedVal == '' || appraisedVal == '$0.00');

            sFHAPurposeIsStreamlineRefiWithAppr.checked = !bDefaultNoAppr;
            sFHAPurposeIsStreamlineRefiWithoutAppr.checked = bDefaultNoAppr;
        }        
      }
      else if ((lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Other)) %>)
            || (lPurpose == <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.Construct)) %>))
      {
        sFHAPurposeIsOther.disabled = true;
        sFHAPurposeIsOther.checked = true;
        lPurposeWasOther = true;
      }
      
      if (lPurpose != <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.FhaStreamlinedRefinance)) %> &&
        lPurpose != <%= AspxTools.JsNumeric(Convert.ToInt32(E_sLPurposeT.VaIrrrl)) %>)
      {
        sFHAPurposeIsStreamlineRefiWithAppr.checked = false;
        sFHAPurposeIsStreamlineRefiWithoutAppr.checked = false;
        sFHAPurposeIsStreamlineRefiWithAppr.disabled = true;
        sFHAPurposeIsStreamlineRefiWithoutAppr.disabled = true;
      }
    }
    
    function toggleStreamlineRefinanceOption(checkbox)
    {
      var oWithAppr = document.getElementById('sFHAPurposeIsStreamlineRefiWithAppr');
      var oWithoutAppr = document.getElementById('sFHAPurposeIsStreamlineRefiWithoutAppr');

      if(checkbox.checked) {
        oWithAppr.checked = false;
        oWithoutAppr.checked = false;
        checkbox.checked = true;
      }
      else // unchecking situation
      {
        <%-- // Act like radio button group. If this unchecking will make it so nothing is checked, do not allow. --%>
        if ( oWithAppr.checked == false && oWithoutAppr.checked == false )
         checkbox.checked = true;
      }
    }
    
    function verifySecondaryFinancingSourceCheckboxes(checkbox) {
      var id = checkbox.id;
      var group = id.substring(0, id.indexOf('Is'));
      if(checkbox.checked) {
        document.getElementById(group + 'IsGov').checked = false;
        document.getElementById(group + 'IsNP').checked = false;
        document.getElementById(group + 'IsFamily').checked = false;
        document.getElementById(group + 'IsOther').checked = false;
        checkbox.checked = true;
      }
      
      enableDisableOtherDesc(group);
    }
    
    function enableDisableOtherDesc(group) {
      var otherDesc = document.getElementById(group + 'OtherDesc');
      if(document.getElementById(group + 'IsOther').checked){
        otherDesc.readOnly = "";
        otherDesc.style.backgroundColor = "white";
        otherDesc.disabled = false;
      }
      else {
        otherDesc.readOnly = "readonly";
        otherDesc.disabled = true;
        otherDesc.innerText = "";
      }
    }

    function enableBorrowerInfo() {
  
      var b = <%= AspxTools.JsGetElementById(sCombinedBorInfoLckd) %>.checked;
      <%= AspxTools.JsGetElementById(sCombinedBorFirstNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorMidNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorLastNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorSuffix) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedBorSsn) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborFirstNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborMidNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborLastNm) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborSuffix) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborSsn) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sCombinedCoborUsing2ndAppBorr) %>.disabled = b;  
    }   
    function enableMonthIncome() {
      var b = <%= AspxTools.JsGetElementById(sFHAIncomeLckd) %>.checked;
      <%= AspxTools.JsGetElementById(sFHABBaseI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHABOI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHACBaseI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHACOI) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHANetRentalI) %>.readOnly = !b;
    }
    function enableMonthlyDebt() {
      var b = <%= AspxTools.JsGetElementById(sFHADebtLckd) %>.checked;
      <%= AspxTools.JsGetElementById(sFHADebtInstallPmt) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHADebtInstallBal) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHAChildSupportPmt) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHAOtherDebtPmt) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHAOtherDebtBal) %>.readOnly = !b;
      <%= AspxTools.JsGetElementById(sFHANegCfRentalI) %>.readOnly = !b;
    }
    function f_populateFromAsset() {
     var args = new Object();
     args["LoanID"] = <%= AspxTools.JsString(LoanID) %>;
 
     var result = gService.loanedit.call("CalculateAssetTotal", args);
     if (!result.error) {
      <%= AspxTools.JsGetElementById(sVerifAssetAmt) %>.value = result.value["VerifiedAssetTotal"];
      updateDirtyBit();
     }
	}
	function f_onChangeFinMethT() {
	    var ddl = <%= AspxTools.JsGetElementById(sFinMethT) %>;
	    var bIsArm = ddl[ddl.selectedIndex].text == "ARM";
	    <%= AspxTools.JsGetElementById(sRAdj1stCapMon) %>.readOnly = !bIsArm;
	    <%= AspxTools.JsGetElementById(sFHAArmIndex) %>.readOnly = !bIsArm;
	    <%= AspxTools.JsGetElementById(sRAdjMarginR) %>.readOnly = !bIsArm;
	}
	
	function manualEdit() 
    {
	    var elem = document.getElementById("sFHA92900LtUnderwriterChumsIdLckd");
        elem.checked = true;
    }  
//-->
</script>

<form id=FHAHud92900LtCombined method=post runat="server">
<QRP:QualRatePopup ID="QualRatePopup" runat="server" />
<asp:CheckBox runat="server" style="display:none;" ID="sFHA92900LtUnderwriterChumsIdLckd" />
<table class=FormTable cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class=MainRightHeader noWrap>FHA Loan Combined 
      Underwriting and Transmittal Summary (HUD-92900-LT)</td></tr>
  <tr>
    <td style="PADDING-LEFT: 10px" noWrap>
      <table cellSpacing=0 cellPadding=0 width=770 border=0>
        <tr>
          <td class=LoanFormHeader style="PADDING-LEFT: 5px" 
          >Borrower and Property Information</td></tr>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <table class=InsetBorder cellSpacing=0 cellPadding=0 width="98%" 
            >
              <tr>
                <td>
                  <TABLE id=Table1 cellSpacing=0 cellPadding=0 width="98%" 
                  border=0>
                    <TR>
                      <TD class=FieldLabel noWrap>Agency 
                        Case Number</TD>
                      <TD noWrap><asp:textbox id=sAgencyCaseNum runat="server"></asp:textbox></TD>
                      <td style="WIDTH: 20px"></td>
                      <TD class=FieldLabel noWrap>Section 
                        of the Act</TD>
                      <TD noWrap><ml:combobox id=sFHAHousingActSection runat="server"></ml:combobox></TD></TR></TABLE></td></tr></table></TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <table class=InsetBorder cellSpacing=0 cellPadding=0 width="98%" 
            border=0>
              <tr>
                <td>
                  <TABLE cellSpacing=0 cellPadding=0 border=0 
                  >
                    <TR>
                      <TD class=FieldLabel noWrap >Borrower</TD>
                      <TD noWrap><asp:checkbox id=sCombinedBorInfoLckd onclick=refreshCalculation(); runat="server" Text="Lock"></asp:checkbox></TD>
                      <TD class=FieldLabel noWrap></TD>
                      <TD class=FieldLabel noWrap >Co-borrower</TD>
                      <TD noWrap><asp:checkbox id=sCombinedCoborUsing2ndAppBorr onclick=refreshCalculation(); tabIndex=5 runat="server" Text="Use 2nd Borrower Info"></asp:checkbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>First 
                        Name</TD>
                      <TD noWrap><asp:textbox id=sCombinedBorFirstNm runat="server"></asp:textbox></TD>
                      <TD class=FieldLabel noWrap >&nbsp;&nbsp;&nbsp;&nbsp; </TD>
                      <TD class=FieldLabel noWrap>First 
                        Name</TD>
                      <TD noWrap><asp:textbox id=sCombinedCoborFirstNm tabIndex=5 runat="server"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Middle 
                        Name</TD>
                      <TD noWrap><asp:textbox id=sCombinedBorMidNm runat="server"></asp:textbox></TD>
                      <TD class=FieldLabel noWrap></TD>
                      <TD class=FieldLabel noWrap>Middle 
                        Name</TD>
                      <TD noWrap><asp:textbox id=sCombinedCoborMidNm tabIndex=5 runat="server"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Last 
                        Name</TD>
                      <TD noWrap><asp:textbox id=sCombinedBorLastNm runat="server"></asp:textbox></TD>
                      <TD class=FieldLabel noWrap></TD>
                      <TD class=FieldLabel noWrap>Last 
                        Name</TD>
                      <TD noWrap><asp:textbox id=sCombinedCoborLastNm tabIndex=5 runat="server"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap >Suffix</TD>
                      <TD noWrap><asp:textbox id=sCombinedBorSuffix runat="server"></asp:textbox></TD>
                      <TD class=FieldLabel noWrap></TD>
                      <TD class=FieldLabel noWrap >Suffix</TD>
                      <TD noWrap><asp:textbox id=sCombinedCoborSuffix tabIndex=5 runat="server"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>SSN</TD>
                      <TD noWrap><ml:ssntextbox id=sCombinedBorSsn runat="server" preset="ssn" width="75px"></ml:ssntextbox></TD>
                      <TD class=FieldLabel noWrap></TD>
                      <TD class=FieldLabel noWrap>SSN</TD>
                      <TD noWrap><ml:ssntextbox id=sCombinedCoborSsn tabIndex=5 runat="server" preset="ssn" width="75px"></ml:ssntextbox></TD></TR></TABLE></td></tr></table></TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <table class=InsetBorder cellSpacing=0 cellPadding=0 width="98%" 
            border=0>
              <tr>
                <td>
                  <table cellSpacing=0 cellPadding=0>
                    <tr>
                      <td class=FieldLabel>Property 
                        Address</td>
                      <td><asp:textbox id=sSpAddr tabIndex=10 runat="server" Width="265px"></asp:textbox></td></tr>
                    <tr>
                      <td></td>
                      <td><asp:textbox id=sSpCity tabIndex=10 runat="server" Width="166px"></asp:textbox><ml:statedropdownlist id=sSpState tabIndex=10 runat="server" IncludeTerritories="false"></ml:statedropdownlist><ml:zipcodetextbox id=sSpZip tabIndex=10 runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td></tr></table></td></tr></table></TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <TABLE class=InsetBorder id=Table9 cellSpacing=0 cellPadding=0 
            width="98%" border=0>
              <TR>
                <TD noWrap>
                  <TABLE id=Table10 cellSpacing=0 cellPadding=0 border=0 
                  >
                    <TR>
                      <TD class=FieldLabel noWrap >Property Type</TD>
                      <TD noWrap><asp:dropdownlist id=sGseSpT tabIndex=10 runat="server"></asp:dropdownlist></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Number 
                        Of Units</TD>
                      <TD noWrap><asp:textbox id=sUnitsNum tabIndex=10 runat="server" Width="49px"></asp:textbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap>Sales 
                        Price</TD>
                      <TD noWrap><ml:moneytextbox id=sPurchPrice tabIndex=10 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap >Appraised Price</TD>
                      <TD noWrap><ml:moneytextbox id=sApprVal tabIndex=10 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap >Property Right</TD>
                      <TD noWrap><asp:dropdownlist id=sEstateHeldT tabIndex=10 runat="server"></asp:dropdownlist></TD></TR>
                    <TR>
                      <TD class=FieldLabel noWrap >Construction</TD>
                      <TD noWrap><asp:dropdownlist id=sFHAConstructionT tabIndex=10 runat="server"></asp:dropdownlist></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
        <TR>
          <TD class=LoanFormHeader style="PADDING-LEFT: 5px" 
          >Mortgage Information</TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <TABLE id=Table2 cellSpacing=0 cellPadding=0 width="98%" border=0 
            >
              <TR>
                <TD style="PADDING-RIGHT: 5px; WIDTH: 33%" vAlign=top noWrap >
                  <TABLE class=InsetBorder id=Table3 cellSpacing=0 cellPadding=0 
                  width="99%" border=0>
                    <TR>
                      <TD noWrap>
                        <TABLE id=Table8 cellSpacing=0 cellPadding=0 border=0 
                        >
                          <TR>
                            <TD class=FieldLabel noWrap >Amortization Type</TD>
                            <TD noWrap><asp:dropdownlist id=sFinMethT tabIndex=15 runat="server" onchange="f_onChangeFinMethT();"></asp:dropdownlist></TD></TR>
                            <TR>
                            <TD class=FieldLabel noWrap >1st Rate Change</TD>
                            <TD noWrap><asp:textbox id="sRAdj1stCapMon" Width="50px" runat="server" tabIndex=15></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >ARM Index</TD>
                            <TD noWrap><asp:textbox id=sFHAArmIndex tabIndex=15 runat="server"></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >ARM Margin</TD>
                            <TD noWrap><ml:percenttextbox id=sRAdjMarginR tabIndex=15 runat="server" preset="percent" width="70"></ml:percenttextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sBuydown tabIndex=15 runat="server" Text="Is Buydown"></asp:checkbox></TD>
                            <TD noWrap></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Loan Information</TD>
                            <TD noWrap></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Mortgage w/o UFMIP</TD>
                            <TD noWrap><ml:moneytextbox id=sLAmtCalc tabIndex=15 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Total UFMIP</TD>
                            <TD noWrap><ml:moneytextbox id=sFfUfmipFinanced tabIndex=15 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Mortgage w/ UFMIP</TD>
                            <TD noWrap><ml:moneytextbox id=sFinalLAmt tabIndex=15 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Interest Rate</TD>
                            <TD noWrap><ml:percenttextbox id=sNoteIR tabIndex=15 runat="server" preset="percent" width="70" onchange="refreshCalculation();"></ml:percenttextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap ><a id="QualRateCalcLink">Qualifying Rate</a></TD>
                            <TD noWrap>
                                <ml:percenttextbox id=sQualIR tabIndex=15 runat="server" preset="percent" width="70"></ml:percenttextbox>
                                <asp:CheckBox ID="sQualIRLckd" runat="server" Text="Lock" onclick="refreshCalculation();" />
                            </TD>
                          </TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Term</TD>
                            <TD noWrap><asp:textbox id=sTerm tabIndex=15 runat="server" Width="49px"></asp:textbox></TD></TR></TABLE></TD></TR></TABLE></TD>
                <TD style="PADDING-RIGHT: 5px; WIDTH: 33%" vAlign=top noWrap >
                  <TABLE class=InsetBorder id=Table4 cellSpacing=0 cellPadding=0 
                  width="99%" border=0>
                    <TR>
                      <TD noWrap>
                        <TABLE id=Table7 cellSpacing=0 cellPadding=0 border=0 
                        >
                          <TR>
                            <TD class=FieldLabel noWrap >Loan Purpose <asp:DropDownList ID="sLPurposeT" runat="server" onchange="updateLoanPurposeCheckboxes(false);"></asp:DropDownList></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsPurchase runat="server" Enabled="false" tabIndex=20></asp:checkbox>Purchase</TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsNoCashoutRefi runat="server" Enabled="false" tabIndex=20></asp:checkbox>No Cash-Out Refinance</TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsCashoutRefi runat="server" Enabled="false" tabIndex=20></asp:checkbox>Cash-Out Refinance</TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsStreamlineRefi runat="server" Enabled="false" tabIndex=20></asp:checkbox>Streamline Refinance</TD></TR>
                          <TR>
                            <TD class=FieldLabel style="PADDING-LEFT: 20px" 
                            noWrap><asp:checkbox id=sFHAPurposeIsStreamlineRefiWithAppr runat="server" Text="w/ appraisal" onclick="toggleStreamlineRefinanceOption(this);" tabIndex=20></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel style="PADDING-LEFT: 20px" 
                            noWrap><asp:checkbox id=sFHAPurposeIsStreamlineRefiWithoutAppr runat="server" Text="w/o appraisal" onclick="toggleStreamlineRefinanceOption(this);" tabIndex=20></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsConstToPerm runat="server" Enabled="false" tabIndex=20></asp:checkbox>Construction-to-Perm</TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsEnergyEfficientMortgage tabIndex=20 runat="server" Text="Energy Efficient Mortgage"></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsBuildOnOwnLand tabIndex=20 runat="server" Text="Building On Own Land"></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsHudReo tabIndex=20 runat="server" Text="HUD REO"></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIs203k tabIndex=20 runat="server" Text="203(k)"></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAPurposeIsOther tabIndex=20 runat="server" Text="Other"></asp:checkbox></TD></TR></TABLE></TD></TR></TABLE></TD>
                <TD style="WIDTH: 33%" vAlign=top noWrap>
                  <TABLE class=InsetBorder id=Table5 cellSpacing=0 cellPadding=0 
                  width="99%" border=0>
                    <TR>
                      <TD noWrap>
                        <TABLE id=Table6 cellSpacing=0 cellPadding=0 border=0 
                        >
                          <TR>
                            <TD class=FieldLabel noWrap >Secondary Financing</TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Source/EIN <asp:textbox id=sFHASecondaryFinancingSource tabIndex=25 runat="server"></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHASecondaryFinancingIsGov runat="server" Text="Gov't" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:checkbox id=sFHASecondaryFinancingIsNP onclick="verifySecondaryFinancingSourceCheckboxes(this);" runat="server" Text="NP" tabIndex=25></asp:checkbox><asp:checkbox id=sFHASecondaryFinancingIsFamily runat="server" Text="Family" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHASecondaryFinancingIsOther runat="server" Text="Other" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:textbox id=sFHASecondaryFinancingOtherDesc runat="server" tabIndex=25></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Amount <ml:moneytextbox id=sFHASecondaryFinancingAmt tabIndex=25 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Gifts&nbsp;&nbsp;&nbsp; 
                              Seller Funded DAP<asp:checkbox id=sFHAIsSellerFundDAP tabIndex=25 runat="server" Text="Yes"></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >1) Source/EIN <asp:textbox id=sFHAGift1Source tabIndex=25 runat="server"></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAGift1IsGov runat="server" Text="Gov't" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:checkbox id=sFHAGift1IsNP runat="server" Text="NP" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:checkbox id=sFHAGift1IsFamily runat="server" Text="Family" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAGift1IsOther runat="server" Text="Other" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:textbox id=sFHAGift1OtherDesc runat="server" tabIndex=25></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Amount <ml:moneytextbox id=sFHAGift1gAmt tabIndex=25 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
                          <TR>
                            <TD noWrap></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >2) Source/EIN <asp:textbox id=sFHAGift2Source tabIndex=25 runat="server"></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAGift2IsGov runat="server" Text="Gov't" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:checkbox id=sFHAGift2IsNP runat="server" Text="NP" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:checkbox id=sFHAGift2IsFamily runat="server" Text="Family" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap><asp:checkbox id=sFHAGift2IsOther runat="server" Text="Other" onclick="verifySecondaryFinancingSourceCheckboxes(this);" tabIndex=25></asp:checkbox><asp:textbox id=sFHAGift2OtherDesc runat="server" tabIndex=25></asp:textbox></TD></TR>
                          <TR>
                            <TD class=FieldLabel noWrap >Amount <ml:moneytextbox id=sFHAGift2gAmt tabIndex=25 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
        <TR>
          <TD class=LoanFormHeader style="PADDING-LEFT: 5px" 
          >Underwriting Information</TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <TABLE id=Table11 cellSpacing=0 cellPadding=0 width="98%" border=0 
            >
              <TR>
                <TD style="PADDING-RIGHT: 5px" vAlign=top width="50%" 
                >
                  <TABLE id=Table12 cellSpacing=0 cellPadding=0 width="100%" 
                  border=0>
                    <TR>
                      <TD noWrap>
                        <TABLE class=InsetBorder id=Table13 cellSpacing=0 
                        cellPadding=0 width="98%" border=0 
                          >
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table14 cellSpacing=0 cellPadding=0 
                              border=0>
                                <TR>
                                <TD class=FieldLabel noWrap >Monthly Income</TD>
                                <TD class=FieldLabel noWrap><asp:checkbox id=sFHAIncomeLckd onclick=refreshCalculation(); tabIndex=30 runat="server" Text="Lock"></asp:checkbox></TD>
                                <TD class=FieldLabel noWrap ></TD>
                                <TD class=FieldLabel noWrap ></TD></TR>
                                <TR>
                                <TD noWrap></TD>
                                <TD class=FieldLabel noWrap >Borrower</TD>
                                <TD class=FieldLabel noWrap >Co-Borrower</TD>
                                <TD class=FieldLabel noWrap >Total</TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Base Income</TD>
                                <TD noWrap><ml:moneytextbox id=sFHABBaseI tabIndex=30 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=sFHACBaseI tabIndex=30 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=sFHATotBaseI runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Other Income</TD>
                                <TD noWrap><ml:moneytextbox id=sFHABOI tabIndex=30 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=sFHACOI tabIndex=30 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=sFHATotOI runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Net Rental Income</TD>
                                <TD noWrap><ml:moneytextbox id=sFHANetRentalI tabIndex=30 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                                <TD noWrap></TD>
                                <TD noWrap><ml:moneytextbox id=sFHANetRentalI2 tabIndex=30 runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Total Income</TD>
                                <TD noWrap><ml:moneytextbox id=sFHABTotI runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=sFHACTotI runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=sFHAGrossMonI runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD noWrap>
                        <TABLE class=InsetBorder id=Table15 cellSpacing=0 
                        cellPadding=0 width="98%" border=0 
                          >
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table16 cellSpacing=0 cellPadding=0 
                              border=0>
                                <TR>
                                <TD class=FieldLabel noWrap >Debts &amp; Obligation</TD>
                                <TD class=FieldLabel noWrap><asp:checkbox id=sFHADebtLckd onclick=refreshCalculation(); tabIndex=40 runat="server" Text="Lock"></asp:checkbox></TD>
                                <TD class=FieldLabel noWrap ></TD></TR>
                                <TR>
                                <TD noWrap></TD>
                                <TD class=FieldLabel noWrap >Monthly</TD>
                                <TD class=FieldLabel noWrap >Unpaid Balance</TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Total Installment</TD>
                                <TD noWrap><ml:moneytextbox id=sFHADebtInstallPmt tabIndex=40 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=sFHADebtInstallBal tabIndex=40 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Child Support</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAChildSupportPmt tabIndex=40 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                                <TD noWrap></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Negative Rental 
                                Cashflow</TD>
                                <TD noWrap><ml:moneytextbox id=sFHANegCfRentalI tabIndex=40 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                                <TD noWrap></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Other Payments</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAOtherDebtPmt tabIndex=40 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD>
                                <TD noWrap><ml:moneytextbox id=sFHAOtherDebtBal tabIndex=40 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Total Fixed Payment</TD>
                                <TD noWrap><ml:moneytextbox id="sFHAPmtFixedTot" runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
                                <TD noWrap ></TD></TR></TABLE></TD></TR></TABLE>
                        <TABLE class=InsetBorder id=Table17 cellSpacing=0 
                        cellPadding=0 width="98%" border=0 
                          >
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table18 cellSpacing=0 cellPadding=0 
                              border=0>
                                <TR>
                                <TD class=FieldLabel noWrap >Borrower Funds to 
Close</TD>
                                <TD noWrap></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Required <asp:checkbox id=sTransmFntcLckd onclick=refreshCalculation(); tabIndex=45 runat="server" Text="Lock"></asp:checkbox></TD>
                                <TD noWrap><ml:moneytextbox id=sTransmFntc tabIndex=45 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Verified Assets&nbsp;&nbsp;<INPUT style="WIDTH: 80px" onclick="f_populateFromAsset();" type="button" value="From Assets" NoHighlight>&nbsp;</TD>
                                <TD noWrap><ml:moneytextbox id=sVerifAssetAmt tabIndex=45 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Closing Costs</TD>
                                <TD noWrap><ml:moneytextbox id=sFHACcTot tabIndex=45 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Source Of Funds</TD>
                                <TD noWrap><asp:textbox id=sFntcSrc tabIndex=45 runat="server"></asp:textbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >No. Of Months In 
                                Reserves</TD>
                                <TD noWrap><asp:textbox id=sRsrvMonNumDesc tabIndex=45 runat="server" Width="69px"></asp:textbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Seller Contribution 
($)</TD>
                                <TD noWrap><ml:moneytextbox id=sFHASellerContribution tabIndex=45 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Seller Contribution 
(%)</TD>
                                <TD noWrap><ml:percenttextbox id=sFHASellerContributionPc runat="server" preset="percent" width="70" ReadOnly="True"></ml:percenttextbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD noWrap></TD></TR></TABLE></TD>
                <TD vAlign=top width="50%">
                  <TABLE id=Table19 cellSpacing=0 cellPadding=0 width="100%" 
                  border=0>
                    <TR>
                      <TD>
                        <TABLE class=InsetBorder id=Table20 cellSpacing=0 
                        cellPadding=0 width="98%" border=0 
                          >
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table21 cellSpacing=0 cellPadding=0 
                              border=0>
                                <TR>
                                <TD class=FieldLabel noWrap >Proposed Monthly 
                                Payments</TD>
                                <TD noWrap></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Borrowers Primary 
                                Residence</TD>
                                <TD noWrap></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >First Mortgage P&amp;I</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAPro1stMPmt tabIndex=35 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Monthly MIP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="sFHAProMInsLckd" runat="server" TabIndex="35" onclick="refreshCalculation();" Text="Lock"/></TD>
                                <TD noWrap><ml:moneytextbox id=sFHAProMIns tabIndex=35 runat="server" preset="money" width="90" onchange="refreshCalculation();" /></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >HOA Fees</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAProHoAssocDues tabIndex=35 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Lease/Ground Rent</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAProGroundRent tabIndex=35 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Second Mortgage 
P&amp;I</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAPro2ndFinPmt tabIndex=35 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Hazard Insurance</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAProHazIns tabIndex=35 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Taxes &amp; Special 
                                Assessments</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAProRealETx tabIndex=35 runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Total Mortgage Payment</TD>
                                <TD noWrap><ml:moneytextbox id=sFHAMonthlyPmt runat="server" preset="money" width="90" ReadOnly="True" onchange="refreshCalculation();"></ml:moneytextbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD>
                        <TABLE class=InsetBorder id=Table22 cellSpacing=0 
                        cellPadding=0 width="98%" border=0 
                          >
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table23 cellSpacing=0 cellPadding=0 
                              border=0>
                                <TR>
                                <TD class=FieldLabel noWrap >Qualifying Ratios</TD>
                                <TD noWrap></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >LTV</TD>
                                <TD noWrap><ml:percenttextbox id=sLtvR runat="server" preset="percent" width="70" ReadOnly="True"></ml:percenttextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >CLTV</TD>
                                <TD noWrap><ml:percenttextbox id=sCltvR runat="server" preset="percent" width="70" ReadOnly="True"></ml:percenttextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap>HCLTV</TD>
                                <TD noWrap><ml:PercentTextBox id=sHcltvR runat="server" preset="percent" width="70" ReadOnly="True"></ml:PercentTextBox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Mortgage 
                                Payment-to-income</TD>
                                <TD noWrap><ml:percenttextbox id=sFHAMPmtToIRatio runat="server" preset="percent" width="70" ReadOnly="True"></ml:percenttextbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >Total Fixed 
                                Payment-to-income</TD>
                                <TD noWrap><ml:percenttextbox id=sFHAFixedPmtToIRatio runat="server" preset="percent" width="70" ReadOnly="True"></ml:percenttextbox></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD>
                        <TABLE class=InsetBorder id=Table24 cellSpacing=0 
                        cellPadding=0 width="98%" border=0 
                          >
                          <TR>
                            <TD noWrap>
                              <TABLE id=Table25 cellSpacing=0 cellPadding=0 
                              border=0>
                                <TR>
                                <TD noWrap></TD>
                                <TD class=FieldLabel noWrap >Borrower</TD>
                                <TD class=FieldLabel noWrap >Co-Borrower</TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >CAIVRS #</TD>
                                <TD noWrap><asp:textbox id=sFHABCaivrsNum tabIndex=50 runat="server" ReadOnly="True"></asp:textbox></TD>
                                <TD noWrap><asp:textbox id=sFHACCaivrsNum tabIndex=50 runat="server" ReadOnly="True"></asp:textbox></TD></TR>
                                <TR>
                                <TD class=FieldLabel noWrap >LDP/GSA</TD>
                                <TD class=FieldLabel noWrap><asp:checkboxlist id=sFHABLdpGsaTri tabIndex=50 runat="server" Enabled="False" CssClass="FieldLabel" RepeatDirection="Horizontal"></asp:checkboxlist></TD>
                                <TD class=FieldLabel noWrap><asp:checkboxlist id=sFHACLdpGsaTri tabIndex=50 runat="server" Enabled="False" CssClass="FieldLabel" RepeatDirection="Horizontal"></asp:checkboxlist></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></TD></TR>
        <TR>
          <TD class=FieldLabel style="PADDING-LEFT: 5px" 
            >Underwriter Comment</TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px"><asp:textbox id=sTransmUwerComments tabIndex=55 runat="server" Width="520px" Height="150px" TextMode="MultiLine"></asp:textbox></TD></TR>
        <TR>
          <TD style="PADDING-LEFT: 5px">
            <TABLE id=Table26 cellSpacing=0 cellPadding=0 border=0>
              <TR>
                <TD class=FieldLabel noWrap>CHUMS ID# </TD>
                <TD noWrap><asp:textbox onchange="manualEdit();" id=sFHA92900LtUnderwriterChumsId tabIndex=55 runat="server"></asp:textbox></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>Scored By 
                  TOTAL</TD>
                <TD class=FieldLabel noWrap><asp:checkboxlist id=sFHAScoreByTotalTri tabIndex=55 runat="server" CssClass="FieldLabel" RepeatDirection="Horizontal"></asp:checkboxlist></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap>Risk Class</TD>
                <TD class=FieldLabel noWrap><asp:checkbox id=sFHARiskClassAA tabIndex=55 runat="server" Text="A/A"></asp:checkbox><asp:checkbox id=sFHARiskClassRefer tabIndex=55 runat="server" Text="Refer"></asp:checkbox></TD></TR>
              <TR>
                <TD noWrap class="FieldLabel">CHUMS ID # for Reviewer of appraisal</TD>
                <TD noWrap><asp:TextBox id="sChumsIdReviewerAppraisal" runat="server" tabIndex="55"></asp:TextBox></TD></TR></TABLE></TD></TR>
        <TR>
          <TD 
style="PADDING-LEFT: 5px"></TD></TR></table></td></tr></table></form>
	
  </body>
</HTML>
