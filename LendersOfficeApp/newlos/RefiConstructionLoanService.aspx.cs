﻿namespace LendersOfficeApp.newlos
{
    using System;
    using DataAccess;
    using LendersOffice.Migration;

    public class RefiConstrucctionLoanServiceItem : LendersOffice.Common.AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(RefiConstrucctionLoanServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges))
            {
                dataLoan.sLotAcquiredD_rep = GetString("sLotAcquiredD");
            }
            else
            {
                dataLoan.sLotAcqYr = GetString("sLotAcqYr");
                dataLoan.sLotVal_rep = GetString("sLotVal");
            }

            dataLoan.sLotOrigC_rep = GetString("sLotOrigC");
            dataLoan.sLotLien_rep = GetString("sLotLien");
            dataLoan.sLotImprovC_rep = GetString("sLotImprovC");

            dataLoan.sSpAcqYr = GetString("sSpAcqYr");
            dataLoan.sSpOrigC_rep = GetString("sSpOrigC");
            dataLoan.sSpLien_rep = GetString("sSpLien");
            dataLoan.sRefPurpose = GetString("sRefPurpose");
            dataLoan.sSpImprovDesc = GetString("sSpImprovDesc");

            dataLoan.sSpImprovC_rep = GetString("sSpImprovC");
            dataLoan.sSpImprovTimeFrameT = (E_sSpImprovTimeFrameT)GetInt("sSpImprovTimeFrameT");
           
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("sLotWImprovTot", dataLoan.sLotWImprovTot_rep);
        }
    }

    public partial class RefiConstructionLoanService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("", new RefiConstrucctionLoanServiceItem());
        }
    }
}
