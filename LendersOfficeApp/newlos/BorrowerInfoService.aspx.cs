namespace LendersOfficeApp.newlos
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LqbGrammar.DataTypes;

    public class BorrowerInfoServiceItem : AbstractBackgroundServiceItem
    {
        protected override void Process(string methodName)
        {
            switch (methodName) 
            {
                case "CalculateAge":
                    CalculateAge();
                    break;
            }
        }
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(BorrowerInfoServiceItem));
        }

        private static string GetLanguagePrefTypeRadioSuffix(LanguagePrefType LanguagePrefType)
        {
            switch (LanguagePrefType)
            {
                case LanguagePrefType.English:
                    return "Eng";
                case LanguagePrefType.Chinese:
                    return "Chi";
                case LanguagePrefType.Korean:
                    return "Kor";
                case LanguagePrefType.Spanish:
                    return "Spa";
                case LanguagePrefType.Tagalog:
                    return "Tag";
                case LanguagePrefType.Vietnamese:
                    return "Vie";
                case LanguagePrefType.Other:
                    return "Oth";
                case LanguagePrefType.Blank:
                    return "Bla";
                default:
                    throw new UnhandledEnumException(LanguagePrefType);
            }
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aBAliases = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("aBAlias"));
            dataApp.aCAliases = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("aCAlias"));
            dataApp.aBPowerOfAttorneyNm = GetString("aBPowerOfAttorneyNm");
            dataApp.aCPowerOfAttorneyNm = GetString("aCPowerOfAttorneyNm");

            dataApp.aBFirstNm = GetString("aBFirstNm");
            dataApp.aBPreferredNm = GetString("aBPreferredNm");
            dataApp.aBPreferredNmLckd = GetBool("aBPreferredNmLckd");
            dataApp.aBMidNm = GetString("aBMidNm");
            dataApp.aBLastNm = GetString("aBLastNm");
            dataApp.aBSuffix = GetString("aBSuffix");

            if (dataLoan.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy)
            {
                if (GetBool("aHasCoborrowerData"))
                {
                    dataApp.LoanData.AddCoborrowerToLegacyApplication(dataApp.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                }
                else
                {
                    dataApp.LoanData.RemoveCoborrowerFromLegacyApplication(dataApp.aAppId.ToIdentifier<DataObjectKind.LegacyApplication>());
                }
            }

            dataApp.aCFirstNm = GetString("aCFirstNm");
            dataApp.aCPreferredNm = GetString("aCPreferredNm");
            dataApp.aCPreferredNmLckd = GetBool("aCPreferredNmLckd");
            dataApp.aCMidNm = GetString("aCMidNm");
            dataApp.aCLastNm = GetString("aCLastNm");
            dataApp.aCSuffix = GetString("aCSuffix");
            dataApp.aBSsn = GetString("aBSsn");
            dataApp.aCSsn = GetString("aCSsn");
            dataApp.aBDob_rep = GetString("aBDob");
            dataApp.aCDob_rep = GetString("aCDob");
            dataApp.aProdBCitizenT = (E_aProdCitizenT)GetInt("aProdBCitizenT");
            dataApp.aProdCCitizenT = (E_aProdCitizenT)GetInt("aProdCCitizenT");
            dataApp.aBAge_rep = GetString("aBAge");
            dataApp.aBSchoolYrs_rep = GetString("aBSchoolYrs");
            dataApp.aCAge_rep = GetString("aCAge");
            dataApp.aCSchoolYrs_rep = GetString("aCSchoolYrs");
            dataApp.aBDependNum_rep = GetString("aBDependNum");
            dataApp.aCDependNum_rep = GetString("aCDependNum");
            dataApp.aBDependAges = GetString("aBDependAges");
            dataApp.aCDependAges = GetString("aCDependAges");
            dataApp.aBHPhone = GetString("aBHPhone");
            dataApp.aCHPhone = GetString("aCHPhone");
            dataApp.aBCellPhone = GetString("aBCellphone");
            dataApp.aCCellPhone = GetString("aCCellphone");
            dataApp.aBBusPhone = GetString("aBBusPhone");
            dataApp.aCBusPhone = GetString("aCBusPhone");
            dataApp.aBEmail = GetString("aBEmail");
            dataApp.aCEmail = GetString("aCEmail");
            dataApp.aBTypeT = (E_aTypeT) GetInt("aBTypeT");
            dataApp.aCTypeT = (E_aTypeT) GetInt("aCTypeT");
            dataApp.aBAddr = GetString("aBAddr");
            dataApp.aCAddr = GetString("aCAddr");
            dataApp.aBCity = GetString("aBCity");
            dataApp.aBState = GetString("aBState");
            dataApp.aBZip = GetString("aBZip");
            dataApp.aBAddrYrs = GetString("aBAddrYrs");
            dataApp.aCAddrYrs = GetString("aCAddrYrs");
            dataApp.aCZip = GetString("aCZip");
            dataApp.aCState = GetString("aCState");
            dataApp.aCCity = GetString("aCCity");
            dataApp.aBAddrMail = GetString("aBAddrMail");
            dataApp.aCAddrMail = GetString("aCAddrMail");
            dataApp.aBCityMail = GetString("aBCityMail");
            dataApp.aCCityMail = GetString("aCCityMail");
            dataApp.aBStateMail = GetString("aBStateMail");
            dataApp.aCStateMail = GetString("aCStateMail");
            dataApp.aBZipMail = GetString("aBZipMail");
            dataApp.aCZipMail = GetString("aCZipMail");
            dataApp.aBAddrMailSourceT = (E_aAddrMailSourceT)GetInt("aBAddrMailSourceT");
            dataApp.aCAddrMailSourceT = (E_aAddrMailSourceT)GetInt("aCAddrMailSourceT");
            dataApp.aBPrev1Addr = GetString("aBPrev1Addr");
            dataApp.aBPrev1AddrYrs = GetString("aBPrev1AddrYrs");
            dataApp.aBPrev1State = GetString("aBPrev1State");
            dataApp.aBPrev1Zip = GetString("aBPrev1Zip");
            dataApp.aBPrev1City = GetString("aBPrev1City");
            dataApp.aCPrev1Addr = GetString("aCPrev1Addr");
            dataApp.aCPrev1City = GetString("aCPrev1City");
            dataApp.aCPrev1State = GetString("aCPrev1State");
            dataApp.aCPrev1Zip = GetString("aCPrev1Zip");
            dataApp.aCPrev1AddrYrs = GetString("aCPrev1AddrYrs");
            dataApp.aBPrev2Addr = GetString("aBPrev2Addr");
            dataApp.aBPrev2AddrYrs = GetString("aBPrev2AddrYrs");
            dataApp.aBPrev2State = GetString("aBPrev2State");
            dataApp.aBPrev2Zip = GetString("aBPrev2Zip");
            dataApp.aBPrev2City = GetString("aBPrev2City");
            dataApp.aCPrev2Addr = GetString("aCPrev2Addr");
            dataApp.aCPrev2State = GetString("aCPrev2State");
            dataApp.aCPrev2Zip = GetString("aCPrev2Zip");
            dataApp.aCPrev2City = GetString("aCPrev2City");
            dataApp.aCPrev2AddrYrs = GetString("aCPrev2AddrYrs");
            dataApp.aBAddrPostSourceTLckd = GetBool("aBAddrPostSourceTLckd");
            dataApp.aBAddrPostSourceT = (E_aAddrPostSourceT)GetInt("aBAddrPostSourceT");
            dataApp.aBAddrPost = GetString("aBAddrPost");
            dataApp.aBCityPost = GetString("aBCityPost");
            dataApp.aBStatePost = GetString("aBStatePost");
            dataApp.aBZipPost = GetString("aBZipPost");
            dataApp.aCAddrPostSourceTLckd = GetBool("aCAddrPostSourceTLckd");
            dataApp.aCAddrPostSourceT = (E_aAddrPostSourceT)GetInt("aCAddrPostSourceT");
            dataApp.aCAddrPost = GetString("aCAddrPost");
            dataApp.aCCityPost = GetString("aCCityPost");
            dataApp.aCStatePost = GetString("aCStatePost");
            dataApp.aCZipPost = GetString("aCZipPost");
            dataApp.aBFax = GetString("aBFax");
            dataApp.aCFax = GetString("aCFax");
            dataApp.aBMaritalStatT = (E_aBMaritalStatT) GetInt("aBMaritalStatT");
            dataApp.aCMaritalStatT = (E_aCMaritalStatT) GetInt("aCMaritalStatT");
            dataApp.aBAddrT = (E_aBAddrT)GetInt("aBAddrT");
            dataApp.aBPrev1AddrT = (E_aBPrev1AddrT)GetInt("aBPrev1AddrT");
            dataApp.aBPrev2AddrT = (E_aBPrev2AddrT)GetInt("aBPrev2AddrT");
            dataApp.aCAddrT = (E_aCAddrT)GetInt("aCAddrT");
            dataApp.aCPrev1AddrT = (E_aCPrev1AddrT)GetInt("aCPrev1AddrT");
            dataApp.aCPrev2AddrT = (E_aCPrev2AddrT)GetInt("aCPrev2AddrT");
            dataApp.aOccT = (E_aOccT)GetInt("aOccT");

            dataApp.aBCoreSystemId = GetString("aBCoreSystemId", "");
            dataApp.aCCoreSystemId = GetString("aCCoreSystemId", "");

            dataApp.aBIsVeteran = GetBool("aBIsVeteran");
            dataApp.aCIsVeteran = GetBool("aCIsVeteran");
            dataApp.aBIsSurvivingSpouseOfVeteran = GetBool("aBIsSurvivingSpouseOfVeteran");
            dataApp.aCIsSurvivingSpouseOfVeteran = GetBool("aCIsSurvivingSpouseOfVeteran");

            dataLoan.sMultiApps = GetBool("sMultiApps");
            dataApp.aSpouseIExcl = GetBool("aSpouseIExcl");

            if (dataLoan.sLPurposeT == E_sLPurposeT.Purchase)
            {
                dataApp.aBTotalScoreIsFthb = GetBool("aBTotalScoreIsFthb");
                dataApp.aCTotalScoreIsFthb = GetBool("aCTotalScoreIsFthb");
                dataApp.aBHasHousingHist = GetBool("aBHasHousingHist");
                dataApp.aCHasHousingHist = GetBool("aCHasHousingHist");
            }

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V1_CalcVaLoanElig_Opm204251))
            {
                dataApp.aBVServiceT = (E_aVServiceT)GetInt("aBVServiceT");
                dataApp.aCVServiceT = (E_aVServiceT)GetInt("aCVServiceT");
                dataApp.aBVEnt = (E_aVEnt)GetInt("aBVEnt");
                dataApp.aCVEnt = (E_aVEnt)GetInt("aCVEnt");
                dataApp.aIsBVAFFEx = GetBool("aIsBVAFFEx");
                dataApp.aIsCVAFFEx = GetBool("aIsCVAFFEx");
            }
            
            dataApp.aBLanguagePrefType = (LanguagePrefType)GetInt("aBLanguagePrefType");
            dataApp.aCLanguagePrefType = (LanguagePrefType)GetInt("aCLanguagePrefType");
            dataApp.aBLanguagePrefTypeOtherDesc = GetString("aBLanguagePrefTypeOtherDesc");
            dataApp.aCLanguagePrefTypeOtherDesc = GetString("aCLanguagePrefTypeOtherDesc");
            dataApp.aBLanguageRefusal = GetBool("aBLanguageRefusal");
            dataApp.aCLanguageRefusal = GetBool("aCLanguageRefusal");

            var aBDomesticRelationshipTri = GetString("aBDomesticRelationshipTri", string.Empty);

            if (aBDomesticRelationshipTri.Equals("true", StringComparison.OrdinalIgnoreCase))
            {
                dataApp.aBDomesticRelationshipTri = E_TriState.Yes;
            }
            else if (aBDomesticRelationshipTri.Equals("false", StringComparison.OrdinalIgnoreCase))
            {
                dataApp.aBDomesticRelationshipTri = E_TriState.No;
            }
            else
            {
                dataApp.aBDomesticRelationshipTri = E_TriState.Blank;
            }

            var aCDomesticRelationshipTri = GetString("aCDomesticRelationshipTri", string.Empty);

            if (aCDomesticRelationshipTri.Equals("true", StringComparison.OrdinalIgnoreCase))
            {
                dataApp.aCDomesticRelationshipTri = E_TriState.Yes;
            }
            else if (aCDomesticRelationshipTri.Equals("false", StringComparison.OrdinalIgnoreCase))
            {
                dataApp.aCDomesticRelationshipTri = E_TriState.No;
            }
            else
            {
                dataApp.aCDomesticRelationshipTri = E_TriState.Blank;
            }

            dataApp.aBDomesticRelationshipType = (DomesticRelationshipType)GetInt("aBDomesticRelationshipType");
            dataApp.aCDomesticRelationshipType = (DomesticRelationshipType)GetInt("aCDomesticRelationshipType");
            dataApp.aBDomesticRelationshipTypeOtherDescription = GetString("aBDomesticRelationshipTypeOtherDescription");
            dataApp.aCDomesticRelationshipTypeOtherDescription = GetString("aCDomesticRelationshipTypeOtherDescription");
            dataApp.aBDomesticRelationshipStateCode = GetString("aBDomesticRelationshipStateCode");
            dataApp.aCDomesticRelationshipStateCode = GetString("aCDomesticRelationshipStateCode");

            dataApp.aBActiveDuty = GetBool("aBActiveDuty", false);
            dataApp.aCActiveDuty = GetBool("aCActiveDuty", false);
            dataApp.aBActiveDutyExpirationDate_rep = GetString("aBActiveDutyExpirationDate");
            dataApp.aCActiveDutyExpirationDate_rep = GetString("aCActiveDutyExpirationDate");
            dataApp.aBRetiredDischargedOrSeparated = GetBool("aBRetiredDischargedOrSeparated", false);
            dataApp.aCRetiredDischargedOrSeparated = GetBool("aCRetiredDischargedOrSeparated", false);
            dataApp.aBReserveNationalGuardNeverActivated = GetBool("aBReserveNationalGuardNeverActivated", false);
            dataApp.aCReserveNationalGuardNeverActivated = GetBool("aCReserveNationalGuardNeverActivated", false);
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBPreferredNm", dataApp.aBPreferredNm);
            SetResult("aBPreferredNmLckd", dataApp.aBPreferredNmLckd);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aHasCoborrowerData", dataApp.aHasCoborrowerData);
            SetResult("aHasCoborrowerCalc", dataApp.aHasCoborrowerCalc);
            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCPreferredNm", dataApp.aCPreferredNm);
            SetResult("aCPreferredNmLckd", dataApp.aCPreferredNmLckd);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aBSsn", dataApp.aBSsn);
            SetResult("aCSsn", dataApp.aCSsn);
            SetResult("aBDob", dataApp.aBDob_rep);
            SetResult("aCDob", dataApp.aCDob_rep);
            SetResult("aProdBCitizenT", dataApp.aProdBCitizenT);
            SetResult("aProdCCitizenT", dataApp.aProdCCitizenT);
            SetResult("aBAge", dataApp.aBAge_rep);
            SetResult("aBSchoolYrs", dataApp.aBSchoolYrs_rep);
            SetResult("aCAge", dataApp.aCAge_rep);
            SetResult("aCSchoolYrs", dataApp.aCSchoolYrs_rep);
            SetResult("aBDependNum", dataApp.aBDependNum_rep);
            SetResult("aCDependNum", dataApp.aCDependNum_rep);
            SetResult("aBDependAges", dataApp.aBDependAges);
            SetResult("aCDependAges", dataApp.aCDependAges);
            SetResult("aBHPhone", dataApp.aBHPhone);
            SetResult("aCHPhone", dataApp.aCHPhone);
            SetResult("aBCellphone", dataApp.aBCellPhone);
            SetResult("aCCellphone", dataApp.aCCellPhone);
            SetResult("aBBusPhone", dataApp.aBBusPhone);
            SetResult("aCBusPhone", dataApp.aCBusPhone);
            SetResult("aBEmail", dataApp.aBEmail);
            SetResult("aCEmail", dataApp.aCEmail);
            SetResult("aBTypeT", dataApp.aBTypeT);
            SetResult("aCTypeT", dataApp.aCTypeT);
            SetResult("aBAddr", dataApp.aBAddr);
            SetResult("aCAddr", dataApp.aCAddr);
            SetResult("aBCity", dataApp.aBCity);
            SetResult("aBState", dataApp.aBState);
            SetResult("aBZip", dataApp.aBZip);
            SetResult("aBAddrYrs", dataApp.aBAddrYrs);
            SetResult("aCAddrYrs", dataApp.aCAddrYrs);
            SetResult("aCZip", dataApp.aCZip);
            SetResult("aCState", dataApp.aCState);
            SetResult("aCCity", dataApp.aCCity);
            SetResult("aBAddrMail", dataApp.aBAddrMail);
            SetResult("aCAddrMail", dataApp.aCAddrMail);
            SetResult("aBCityMail", dataApp.aBCityMail);
            SetResult("aCCityMail", dataApp.aCCityMail);
            SetResult("aBStateMail", dataApp.aBStateMail);
            SetResult("aCStateMail", dataApp.aCStateMail);
            SetResult("aBZipMail", dataApp.aBZipMail);
            SetResult("aCZipMail", dataApp.aCZipMail);
            SetResult("aBAddrMailSourceT", dataApp.aBAddrMailSourceT);
            SetResult("aCAddrMailSourceT", dataApp.aCAddrMailSourceT);
            SetResult("aBPrev1Addr", dataApp.aBPrev1Addr);
            SetResult("aBPrev1AddrYrs", dataApp.aBPrev1AddrYrs);
            SetResult("aBPrev1State", dataApp.aBPrev1State);
            SetResult("aBPrev1Zip", dataApp.aBPrev1Zip);
            SetResult("aBPrev1City", dataApp.aBPrev1City);
            SetResult("aCPrev1Addr", dataApp.aCPrev1Addr);
            SetResult("aCPrev1City", dataApp.aCPrev1City);
            SetResult("aCPrev1State", dataApp.aCPrev1State);
            SetResult("aCPrev1Zip", dataApp.aCPrev1Zip);
            SetResult("aCPrev1AddrYrs", dataApp.aCPrev1AddrYrs);
            SetResult("aBPrev2Addr", dataApp.aBPrev2Addr);
            SetResult("aBPrev2AddrYrs", dataApp.aBPrev2AddrYrs);
            SetResult("aBPrev2State", dataApp.aBPrev2State);
            SetResult("aBPrev2Zip", dataApp.aBPrev2Zip);
            SetResult("aBPrev2City", dataApp.aBPrev2City);
            SetResult("aCPrev2Addr", dataApp.aCPrev2Addr);
            SetResult("aCPrev2State", dataApp.aCPrev2State);
            SetResult("aCPrev2Zip", dataApp.aCPrev2Zip);
            SetResult("aCPrev2City", dataApp.aCPrev2City);
            SetResult("aCPrev2AddrYrs", dataApp.aCPrev2AddrYrs);
            SetResult("aBAddrPostSourceTLckd", dataApp.aBAddrPostSourceTLckd);
            SetResult("aBAddrPostSourceT", dataApp.aBAddrPostSourceT);
            SetResult("aBAddrPost", dataApp.aBAddrPost);
            SetResult("aBCityPost", dataApp.aBCityPost);
            SetResult("aBStatePost", dataApp.aBStatePost);
            SetResult("aBZipPost", dataApp.aBZipPost);
            SetResult("aCAddrPostSourceTLckd", dataApp.aCAddrPostSourceTLckd);
            SetResult("aCAddrPostSourceT", dataApp.aCAddrPostSourceT);
            SetResult("aCAddrPost", dataApp.aCAddrPost);
            SetResult("aCCityPost", dataApp.aCCityPost);
            SetResult("aCStatePost", dataApp.aCStatePost);
            SetResult("aCZipPost", dataApp.aCZipPost);
            SetResult("aBFax", dataApp.aBFax);
            SetResult("aCFax", dataApp.aCFax);
            SetResult("aBMaritalStatT", dataApp.aBMaritalStatT);
            SetResult("aCMaritalStatT", dataApp.aCMaritalStatT);
            SetResult("aBAddrT", dataApp.aBAddrT);
            SetResult("aBPrev1AddrT", dataApp.aBPrev1AddrT);
            SetResult("aBPrev2AddrT", dataApp.aBPrev2AddrT);
            SetResult("aCAddrT", dataApp.aCAddrT);
            SetResult("aCPrev1AddrT", dataApp.aCPrev1AddrT);
            SetResult("aCPrev2AddrT", dataApp.aCPrev2AddrT);
            SetResult("aOccT", dataApp.aOccT);

            SetResult("aBIsVeteran", dataApp.aBIsVeteran);
            SetResult("aCIsVeteran", dataApp.aCIsVeteran);

            SetResult("sMultiApps", dataLoan.sMultiApps);
            SetResult("aSpouseIExcl", dataApp.aSpouseIExcl);

            SetResult("aBTotalScoreIsFthb", dataApp.aBTotalScoreIsFthb);
            SetResult("aCTotalScoreIsFthb", dataApp.aCTotalScoreIsFthb);
            SetResult("aBHasHousingHist", dataApp.aBHasHousingHist);
            SetResult("aCHasHousingHist", dataApp.aCHasHousingHist);

            var isBeyondV1_CalcVaLoanElig = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V1_CalcVaLoanElig_Opm204251);
            if (isBeyondV1_CalcVaLoanElig)
            {
                SetResult("aBVServiceT", dataApp.aBVServiceT);
                SetResult("aBVEnt", dataApp.aBVEnt);
                SetResult("aIsBVAFFEx", dataApp.aIsBVAFFEx);
                SetResult("aCVServiceT", dataApp.aCVServiceT);
                SetResult("aCVEnt", dataApp.aCVEnt);
                SetResult("aIsCVAFFEx", dataApp.aIsCVAFFEx);
            }

            SetResult("IsBeyondV1_CalcVaLoanElig", isBeyondV1_CalcVaLoanElig.ToString());
            SetResult("aIsBVAElig", dataApp.aIsBVAElig.ToString());
            SetResult("aIsCVAElig", dataApp.aIsCVAElig.ToString());

            SetResult("aBLanguagePrefType" + GetLanguagePrefTypeRadioSuffix(dataApp.aBLanguagePrefType), true);
            SetResult("aCLanguagePrefType" + GetLanguagePrefTypeRadioSuffix(dataApp.aCLanguagePrefType), true);

            SetResult("aBLanguagePrefTypeOtherDesc", dataApp.aBLanguagePrefTypeOtherDesc);
            SetResult("aCLanguagePrefTypeOtherDesc", dataApp.aCLanguagePrefTypeOtherDesc);
            SetResult("aBLanguageRefusal", dataApp.aBLanguageRefusal);
            SetResult("aCLanguageRefusal", dataApp.aCLanguageRefusal);            
            SetResult("aBDomesticRelationshipTri", dataApp.aBDomesticRelationshipTri);
            SetResult("aCDomesticRelationshipTri", dataApp.aCDomesticRelationshipTri);
            SetResult("aBDomesticRelationshipType", dataApp.aBDomesticRelationshipType);
            SetResult("aCDomesticRelationshipType", dataApp.aCDomesticRelationshipType);
            SetResult("aBDomesticRelationshipTypeOtherDescription", dataApp.aBDomesticRelationshipTypeOtherDescription);
            SetResult("aCDomesticRelationshipTypeOtherDescription", dataApp.aCDomesticRelationshipTypeOtherDescription);
            SetResult("aBDomesticRelationshipStateCode", dataApp.aBDomesticRelationshipStateCode);
            SetResult("aCDomesticRelationshipStateCode", dataApp.aCDomesticRelationshipStateCode);

            SetResult("aBActiveDuty", dataApp.aBActiveDuty);
            SetResult("aCActiveDuty", dataApp.aCActiveDuty);
            SetResult("aBActiveDutyExpirationDate", dataApp.aBActiveDutyExpirationDate_rep);
            SetResult("aCActiveDutyExpirationDate", dataApp.aCActiveDutyExpirationDate_rep);
            SetResult("aBRetiredDischargedOrSeparated", dataApp.aBRetiredDischargedOrSeparated);
            SetResult("aCRetiredDischargedOrSeparated", dataApp.aCRetiredDischargedOrSeparated);
            SetResult("aBReserveNationalGuardNeverActivated", dataApp.aBReserveNationalGuardNeverActivated);
            SetResult("aCReserveNationalGuardNeverActivated", dataApp.aCReserveNationalGuardNeverActivated);
        }

        private void CalculateAge()
        {
            string dob = GetString("dob");
            string currentAge = GetString("age");

            DateTime dt = DateTime.MinValue;
            if (DateTime.TryParse(dob, out dt) == true)
            {
                try
                {
                    SetResult("age", Tools.CalcAgeForToday(dt).ToString());
                }
                catch (CBaseException)
                {
                    SetResult("age", currentAge);
                }
            }
            else
            {
                SetResult("age", currentAge);
            }
        }
    }

    public class BorrowerEmploymentServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(BorrowerEmploymentServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class MonthlyIncomeServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(MonthlyIncomeServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aBBaseI_rep = GetString("aBBaseI");
            dataApp.aBOvertimeI_rep = GetString("aBOvertimeI");
            dataApp.aBBonusesI_rep = GetString("aBBonusesI");
            dataApp.aBCommisionI_rep = GetString("aBCommisionI");
            dataApp.aBDividendI_rep = GetString("aBDividendI");
            dataApp.aCBaseI_rep = GetString("aCBaseI");
            dataApp.aCOvertimeI_rep = GetString("aCOvertimeI");
            dataApp.aCBonusesI_rep = GetString("aCBonusesI");
            dataApp.aCCommisionI_rep = GetString("aCCommisionI");
            dataApp.aCDividendI_rep = GetString("aCDividendI");
            dataApp.aBNetRentI1003_rep = GetString("aBNetRentI1003");
            dataApp.aCNetRentI1003_rep = GetString("aCNetRentI1003");
            dataApp.aNetRentI1003Lckd = GetBool("aNetRentI1003Lckd");

            dataApp.aOtherIncomeList = ObsoleteSerializationHelper.JsonDeserialize<List<OtherIncome>>(GetString("OtherIncomeJson"));
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aBBaseI", dataApp.aBBaseI_rep);
            SetResult("aBOvertimeI", dataApp.aBOvertimeI_rep);
            SetResult("aBBonusesI", dataApp.aBBonusesI_rep);
            SetResult("aBCommisionI", dataApp.aBCommisionI_rep);
            SetResult("aBDividendI", dataApp.aBDividendI_rep);
            SetResult("aBNetRentI1003", dataApp.aBNetRentI1003_rep);
            SetResult("aCBaseI", dataApp.aCBaseI_rep);
            SetResult("aCOvertimeI", dataApp.aCOvertimeI_rep);
            SetResult("aCBonusesI", dataApp.aCBonusesI_rep);
            SetResult("aCCommisionI", dataApp.aCCommisionI_rep);
            SetResult("aCDividendI", dataApp.aCDividendI_rep);
            SetResult("aCNetRentI1003", dataApp.aCNetRentI1003_rep);
            SetResult("aNetRentI1003Lckd", dataApp.aNetRentI1003Lckd);
            SetResult("aBSpPosCf", dataApp.aBSpPosCf_rep);
            SetResult("aCSpPosCf", dataApp.aCSpPosCf_rep);
            SetResult("aTotSpPosCf", dataApp.aTotSpPosCf_rep);

            SetResult("aBTotI", dataApp.aBTotI_rep);
            SetResult("aCTotI", dataApp.aCTotI_rep);
            
            SetResult("OtherIncomeJson", ObsoleteSerializationHelper.JsonSerialize(dataApp.aOtherIncomeList));

            SetResult("aTotI", dataApp.aTotI_rep);
            SetResult("aBTotOI", dataApp.aBTotOI_rep);
            SetResult("aCTotOI", dataApp.aCTotOI_rep);
            SetResult("aTotOI", dataApp.aTotOI_rep);

            SetResult("aTotNetRentI1003", dataApp.aTotNetRentI1003_rep);
            SetResult("aTotDividendI", dataApp.aTotDividendI_rep);
            SetResult("aTotCommisionI", dataApp.aTotCommisionI_rep);
            SetResult("aTotBonusesI", dataApp.aTotBonusesI_rep);
            SetResult("aTotBaseI", dataApp.aTotBaseI_rep);
            SetResult("aTotOvertimeI", dataApp.aTotOvertimeI_rep);

            SetResult("sLTotI", dataLoan.sLTotI_rep);
        }
    }

    public class LiabilitiesServiceItem : AbstractBackgroundServiceItem
    {

        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LiabilitiesServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
    public class AssetsServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(AssetsServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class ReoServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(ReoServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class HousingExpenseServiceItem : AbstractBackgroundServiceItem
    {
        protected override CPageData ConstructPageDataClass(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(HousingExpenseServiceItem));
        }

        protected override void BindData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.aPres1stM_rep = GetString("aPres1stM");
            dataApp.aPresRent_rep = GetString("aPresRent");
            dataApp.aPresOFin_rep = GetString("aPresOFin");
            dataApp.aPresHazIns_rep = GetString("aPresHazIns");
            dataApp.aPresRealETx_rep = GetString("aPresRealETx");
            dataApp.aPresMIns_rep = GetString("aPresMIns");
            dataApp.aPresHoAssocDues_rep = GetString("aPresHoAssocDues");
            dataApp.aPresOHExp_rep = GetString("aPresOHExp");
            dataApp.aPresOHExpDesc = GetString("aPresOHExpDesc");
            dataApp.aOccT = (E_aOccT) GetInt("aOccT");
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp)
        {
            SetResult("aPres1stM", dataApp.aPres1stM_rep);
            SetResult("aPresRent", dataApp.aPresRent_rep);
            SetResult("aPresOFin", dataApp.aPresOFin_rep);
            SetResult("aPresHazIns", dataApp.aPresHazIns_rep);
            SetResult("aPresRealETx", dataApp.aPresRealETx_rep);
            SetResult("aPresMIns", dataApp.aPresMIns_rep);
            SetResult("aPresHoAssocDues", dataApp.aPresHoAssocDues_rep);
            SetResult("aPresOHExp", dataApp.aPresOHExp_rep);
            SetResult("aPresTotHExp", dataApp.aPresTotHExp_rep);
            SetResult("aPresOHExpDesc", dataApp.aPresOHExpDesc);
            SetResult("aOccT", dataApp.aOccT);
            SetResult("sPresLTotPersistentHExp", dataLoan.sPresLTotPersistentHExp_rep);
        }
    }

    public partial class BorrowerInfoService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Initialize()
        {
            AddBackgroundItem("BorrowerInfoTab", new BorrowerInfoServiceItem());
            AddBackgroundItem("BorrowerEmploymentFrame", new BorrowerEmploymentServiceItem());
            AddBackgroundItem("CoborrowerEmploymentFrame", new BorrowerEmploymentServiceItem());
            AddBackgroundItem("BorrowerMonthlyIncome", new MonthlyIncomeServiceItem());
            AddBackgroundItem("BorrowerLiabilityFrame", new LiabilitiesServiceItem());
            AddBackgroundItem("BorrowerAssetFrame", new AssetsServiceItem());
            AddBackgroundItem("BorrowerREOFrame", new ReoServiceItem());
            AddBackgroundItem("BorrowerHousingExpense", new HousingExpenseServiceItem());
        }
    }
}
