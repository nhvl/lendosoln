﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContinuationData.aspx.cs" Inherits="LendersOfficeApp.newlos.ContinuationData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <div>
    <table id="Table1" cellSpacing="0" cellPadding="0" width="500" border="0">
    <tr>
            <td class="MainRightHeader" nowrap>
                Continuation Data
            </td>
        </tr>
  <tr>
    <td>Use this continuation sheet if you need more space to complete the Residential Loan Application. Mark B for Borrower or C for Co-Borrower.</td>
  </tr>
  <tr>
    <td><asp:TextBox id="a1003ContEditSheet" Height="426px" Width="500px" runat="server" TextMode="MultiLine"></asp:TextBox></td>
  </tr>
</table>

    </div>
    </form>
</body>
</html>
