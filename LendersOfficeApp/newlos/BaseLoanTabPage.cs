using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using LendersOffice.Common;
using DataAccess;
using LendersOfficeApp.common;

namespace LendersOfficeApp.newlos
{
	/// <summary>
	/// Summary description for BaseLoanTabPage.
	/// </summary>
	public class BaseLoanTabPage : BaseLoanPage
	{

        /// <summary>
        /// Set this to false if this page doesn't care about individual application.
        /// </summary>
        protected bool AllowMultiApplications = true;

        protected virtual Microsoft.Web.UI.WebControls.TabStrip MainTab 
        {
            get 
            {
                throw new NotImplementedException("Need to override MainTab property.");
            }
        }

        private int m_pg 
        {
            get { return RequestHelper.GetInt("pg", 0); }
        }
        private int m_previousIndex 
        {
            get { return (int) ViewState["PreviousIndex"]; }
            set { ViewState["PreviousIndex"] = value; }
        }
        private ArrayList m_controlList;
        /// <summary>
        /// Make selected control visible, and load data for the control.
        /// </summary>
        /// <param name="index"></param>
        private void SetVisibleExclusive(int index) 
        {
            // TODO: David - If previous index the same as current index then LoadData() should not be call. Reason is the screen remain
            // the same no need to reload data. SaveData() should do databinding after save. However, I need to check to see if any user control
            // inherit from this class depends on the fact that LoadData() get call every time.
            for (int i = 0; i < m_controlList.Count; i++) 
            {
                bool b = i == index;
                Control c = (Control) m_controlList[i];
                c.Visible = b;
                c.EnableViewState = c.EnableViewState & b;
                if (b) 
                {
                    IAutoLoadUserControl c1 = c as IAutoLoadUserControl;
                    if (c1 != null) 
                    {
                        RegisterJsGlobalVariables("_ClientID", c.ClientID);
                        ClientScript.RegisterHiddenField("_ClientID", c.ClientID);
                        c1.LoadData();
                    }
                }
            }
        }


        protected string m_newLocation 
        {
            get { return string.Format("{0}?loanid={1}&pg={2}&appid=", Request.Path, RequestHelper.LoanID, MainTab.SelectedIndex); }
        }
        protected virtual void InitializeTabControls(ArrayList list) 
        {
            throw new NotImplementedException("Need to override InitializeTabControls");
        }

        private void PageInit(object sender, System.EventArgs e) 
        {
            m_controlList = new ArrayList();
            InitializeTabControls(m_controlList);
        }

        private void PageLoad(object sender, System.EventArgs e) 
        {
            try 
            {
                if (!Page.IsPostBack) 
                {
                    int pg = m_pg < MainTab.Items.Count ? m_pg : 0;
                    SetVisibleExclusive(pg);
                    MainTab.SelectedIndex = pg;
                    m_previousIndex = pg;
                } 
                else 
                {
                    if (this.IsUpdate && !IsReadOnly) 
                    {
                        Control c = (Control) m_controlList[m_previousIndex];
                        IAutoLoadUserControl c1 = c as IAutoLoadUserControl;
                        if (c1 != null)
                            c1.SaveData();
                    }
                    ProcessSpecialCommand();
                    SetVisibleExclusive(MainTab.SelectedIndex);
                    m_previousIndex = MainTab.SelectedIndex;
                }
                AddResponseQueryString("pg", MainTab.SelectedIndex);

                if (UseNewFramework) 
                {
                    ClientScript.RegisterHiddenField("_PerAppExtraArgs", "pg=" + MainTab.SelectedIndex);
                }
            } 
            catch (PageDataAccessDenied) 
            {
                // This access denied will get hanle in BaseLoanPage:Page_Load
            }
        }
        /// <summary>
        /// Process command argument in __EVENTARGUMENT, 
        /// Should only handle command with target (__EVENTTARGET) = "".
        /// Why is this method needed? Because there is no way to intercept this command
        /// in current ASP.NET framework, not as I know. dd 6/6/2003
        /// </summary>
        private void ProcessSpecialCommand() 
        {
            if (Request.Form["__EVENTTARGET"] == "") 
            {
                string cmd = Request.Form["__EVENTARGUMENT"];
                if (cmd != "") 
                {

                    string[] args = cmd.Split(':');
                    if (args[0] == "load") 
                    {

                        // Redirect to load correct application id.
                        Response.Redirect(m_newLocation + args[1]);
                    }

                }
            }
        }
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        override protected void OnPreRender(EventArgs e) 
        {

            if (AllowMultiApplications) 
            {
                string script = @"
<script language=javascript>
<!--

";
                script += string.Format(@"
function switchApplicant() {{
  f_load('{0}?loanid={1}&pg={2}');
}}
", Request.Path, LoanID, MainTab.SelectedIndex);
                script += "//--></script>";

                ClientScript.RegisterClientScriptBlock(this.GetType(),"TabScript", script);
            }
  
            string scriptInit = string.Format(@"
function _initTab() {{
    {0}.onselectedindexchange = f_onTabChange;
}}
function f_onTabChange() {{
    document.all.__{0}_State__.value = {0}.selectedIndex;
    //var href= self.location.href.replace(/(&pg=[0-9]+|$)/, '&pg=' + {0}.selectedIndex);
    //alert(href);
    f_load('{1}?loanid={2}&pg=' + {0}.selectedIndex);
}}
", MainTab.ClientID, Request.Path, LoanID);
            AddInitScriptFunction("_initTab", scriptInit);
            base.OnPreRender(e);
        }
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
	}
}
