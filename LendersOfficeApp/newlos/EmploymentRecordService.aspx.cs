using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos
{
	public partial class EmploymentRecordService : AbstractSingleEditService2
	{
        protected override void CustomProcess(string methodName) 
        {
            switch (methodName) 
            {
                case "GetPrimaryBusinessPhone":
                    GetPrimaryBusinessPhone();
                    break;
                case "IsCurrentClicked":
                    IsCurrentClicked();
                    break;
            }
        }

        private void GetPrimaryBusinessPhone() 
        {
            CPageData dataLoan = this.DataLoan;
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(this.ApplicationID);

            bool isBorrower = GetBool("IsBorrower");
            if (isBorrower) 
            {
                dataApp.aBEmplrBusPhoneLckd = GetBool("aEmplrBusPhoneLckd");

            } 
            else 
            {
                dataApp.aCEmplrBusPhoneLckd = GetBool("aEmplrBusPhoneLckd");
            }
            IEmpCollection recordList = isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;

            IPrimaryEmploymentRecord field = recordList.GetPrimaryEmp(true);
            SetResult("aEmplrBusPhone", field.EmplrBusPhone);

        }

        private void IsCurrentClicked()
        {
            CPageData dataLoan = this.DataLoan;
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(ApplicationID);
            var recordList = LoadRecordList(dataLoan, dataApp);
            var record = RecordID == Guid.Empty ? null : recordList.GetRegRecordOf(RecordID);
            IRegularEmploymentRecord field = (IRegularEmploymentRecord) record;

            field.IsCurrent = GetBool("IsCurrent");
            SetResult("EmplmtEndD", field.EmplmtEndD_rep);
        }

        protected override void SaveData(CPageData dataLoan, CAppData dataApp, IRecordCollection collection, ICollectionItemBase2 record) 
        {
            IEmpCollection recordList = (IEmpCollection)collection;
            IRegularEmploymentRecord field = (IRegularEmploymentRecord) record;

            if (null != field) 
            {
                field.EmplrNm = GetString("EmplrNm");
                field.IsSelfEmplmt = GetBool("IsSelfEmplmt");
                field.IsCurrent = GetBool("IsCurrent");
                field.MonI_rep = GetString("MonI");
                field.EmplmtStartD_rep = GetString("EmplmtStartD");
                field.EmplmtEndD_rep = GetString("EmplmtEndD");
                field.EmplrAddr = GetString("EmplrAddr");
                field.EmplrCity = GetString("EmplrCity");
                field.EmplrState = GetString("EmplrState");
                field.EmplrZip = GetString("EmplrZip");
                field.EmplrBusPhone = GetString("EmplrBusPhone");
                field.EmplrFax = GetString("EmplrFax");
                field.JobTitle = GetString("JobTitle");
                field.VerifExpD_rep = GetString("VerifExpD");
                field.VerifRecvD_rep = GetString("VerifRecvD");
                field.VerifSentD_rep = GetString("VerifSentD");
                field.VerifReorderedD_rep = GetString("VerifReorderedD");
                field.EmployeeIdVoe = GetString("EmployeeIdVoe");
                field.EmployerCodeVoe = GetString("EmployerCodeVoe");
                field.SelfOwnershipShareT = GetEnum<SelfOwnershipShare>("SelfOwnershipShareT");
                field.IsSpecialBorrowerEmployerRelationship = GetBool("IsSpecialBorrowerEmployerRelationship");
            }

            // Save Primary employment
            IPrimaryEmploymentRecord primary = recordList.GetPrimaryEmp(true); 
            
            primary.IsSelfEmplmt         = GetBool("Primary_IsSelfEmplmt")  ;
            primary.EmplrNm              = GetString("Primary_EmplrNm")  ;
            primary.EmplrAddr            = GetString("Primary_EmplrAddr")  ;
            primary.EmplrCity            = GetString("Primary_EmplrCity")  ;
            primary.EmplrState           = GetString("Primary_EmplrState")  ;
            primary.EmplrZip             = GetString("Primary_EmplrZip")  ;
            primary.JobTitle             = GetString("Primary_JobTitle")  ;
            primary.EmplmtLen_rep        = GetString("Primary_EmplmtLen")  ;
            primary.EmpltStartD_rep      = GetString("Primary_EmpltStartD") ;
            primary.ProfLen_rep          = GetString("Primary_ProfLen")  ;
            primary.ProfStartD_rep       = GetString("Primary_ProfStartD") ;
            primary.EmplrBusPhone        = GetString("Primary_EmplrBusPhone")  ;
            primary.EmplrFax             = GetString("Primary_EmplrFax")  ;
            primary.VerifSentD_rep       = GetString("Primary_VerifSentD")  ;
            primary.VerifReorderedD_rep  = GetString("Primary_VerifReorderedD")  ;
            primary.VerifRecvD_rep       = GetString("Primary_VerifRecvD")  ;
            primary.VerifExpD_rep        = GetString("Primary_VerifExpD")  ;
            primary.EmployeeIdVoe = GetString("Primary_EmployeeIdVoe");
            primary.EmployerCodeVoe = GetString("Primary_EmployerCodeVoe");

            primary.SelfOwnershipShareT = GetEnum<SelfOwnershipShare>("Primary_SelfOwnershipShareT");
            primary.IsSpecialBorrowerEmployerRelationship = GetBool("Primary_IsSpecialBorrowerEmployerRelationship");

            if (GetString("OwnerT", "") == "0") 
            {
                dataApp.aBEmplrBusPhoneLckd = GetBool("aEmplrBusPhoneLckd");
            }
            else 
            {
                dataApp.aCEmplrBusPhoneLckd = GetBool("aEmplrBusPhoneLckd");
            }
        }


        protected override CPageData DataLoan 
        {
            get { return new CEmploymentInfoData(LoanID); }
        }

        protected override IRecordCollection LoadRecordList(CPageData dataLoan, CAppData dataApp) 
        {
            return GetString("OwnerT", "") == "0" ? (IRecordCollection) dataApp.aBEmpCollection : (IRecordCollection) dataApp.aCEmpCollection;
        }

        protected override void LoadData(CPageData dataLoan, CAppData dataApp, ICollectionItemBase2 record) 
        {
            NameValueCollection ret = new NameValueCollection();
            IRegularEmploymentRecord field = (IRegularEmploymentRecord) record;

            if (GetString("IsSwap", "") == "T") 
            {
                // Swap primary employment record with new previous employment.
                if (null != field) 
                {
                    SetResult("RecordID", field.RecordId);
                }
                SetResult("IsSelfEmplmt", GetString("Primary_IsSelfEmplmt") );
                SetResult("EmplrNm", GetString("Primary_EmplrNm") );
                SetResult("EmplrAddr", GetString("Primary_EmplrAddr") );
                SetResult("EmplrCity", GetString("Primary_EmplrCity") );
                SetResult("EmplrState", GetString("Primary_EmplrState") );
                SetResult("EmplrZip", GetString("Primary_EmplrZip") );
                SetResult("JobTitle", GetString("Primary_JobTitle") );
                SetResult("EmplrBusPhone", GetString("Primary_EmplrBusPhone") );
                SetResult("EmplrFax", GetString("Primary_EmplrFax") );
                SetResult("VerifSentD", GetString("Primary_VerifSentD") );
                SetResult("VerifReorderedD", GetString("Primary_VerifReorderedD") );
                SetResult("VerifRecvD", GetString("Primary_VerifRecvD") );
                SetResult("VerifExpD", GetString("Primary_VerifExpD") );
                SetResult("MonI", "$0.00" );
                SetResult("EmplmtStartD", "" );
                SetResult("EmplmtEndD", "" );
                SetResult("EmployeeIdVoe", GetString("Primary_EmployeeIdVoe"));
                SetResult("EmployerCodeVoe", GetString("Primary_EmployerCodeVoe"));
                SetResult("IsSpecialBorrowerEmployerRelationship", GetBool("Primary_IsSpecialBorrowerEmployerRelationship"));
                SetResult("SelfOwnershipShareT", GetString("Primary_SelfOwnershipShareT"));

                SetResult("Primary_IsSelfEmplmt", bool.FalseString );
                SetResult("Primary_EmplrNm", "" );
                SetResult("Primary_EmplrAddr", "" );
                SetResult("Primary_EmplrCity", "" );
                SetResult("Primary_EmplrState", "" );
                SetResult("Primary_EmplrZip", "" );
                SetResult("Primary_JobTitle", "" );
                SetResult("Primary_EmplmtLen", "" );
                SetResult("Primary_EmpltStartD", "" );
                SetResult("Primary_ProfLen", "" );
                SetResult("Primary_ProfStartD", "" );
                SetResult("Primary_EmplrBusPhone", "" );
                SetResult("Primary_EmplrFax", "" );
                SetResult("Primary_VerifSentD", "" );
                SetResult("Primary_VerifReorderedD", "" );
                SetResult("Primary_VerifRecvD", "" );
                SetResult("Primary_VerifExpD", "" );
                SetResult("Primary_EmployeeIdVoe", string.Empty);
                SetResult("Primary_EmployerCodeVoe", string.Empty);
                SetResult("Primary_IsSpecialBorrowerEmployerRelationship", false);
                SetResult("Primary_SelfOwnershipShareT", SelfOwnershipShare.Blank);
            } 
            else 
            {
                if (null != field) 
                {
                    SetResult("RecordID", field.RecordId);
                    SetResult("EmplrNm", field.EmplrNm );
                    SetResult("IsCurrent", field.IsCurrent);
                    SetResult("IsSelfEmplmt", field.IsSelfEmplmt );
                    SetResult("MonI", field.MonI_rep );
                    SetResult("EmplmtStartD", field.EmplmtStartD_rep );
                    SetResult("EmplmtEndD", field.EmplmtEndD_rep );
                    SetResult("EmplrAddr", field.EmplrAddr );
                    SetResult("EmplrCity", field.EmplrCity );
                    SetResult("EmplrState", field.EmplrState);
                    SetResult("EmplrZip", field.EmplrZip );
                    SetResult("EmplrBusPhone", field.EmplrBusPhone );
                    SetResult("EmplrFax", field.EmplrFax );
                    SetResult("JobTitle", field.JobTitle );
                    SetResult("VerifExpD", field.VerifExpD);
                    SetResult("VerifRecvD", field.VerifRecvD );
                    SetResult("VerifSentD", field.VerifSentD );
                    SetResult("VerifReorderedD", field.VerifReorderedD);
                    SetResult("EmployeeIdVoe", field.EmployeeIdVoe);
                    SetResult("EmployerCodeVoe", field.EmployerCodeVoe);
                    SetResult("IsSpecialBorrowerEmployerRelationship", field.IsSpecialBorrowerEmployerRelationship);
                    SetResult("SelfOwnershipShareT", field.SelfOwnershipShareT);
                }

                IEmpCollection recordList = GetString("OwnerT", "") == "0" ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
                IPrimaryEmploymentRecord primary = recordList.GetPrimaryEmp(true); 
            
                SetResult("Primary_IsSelfEmplmt", primary.IsSelfEmplmt );
                SetResult("Primary_EmplrNm", primary.EmplrNm );
                SetResult("Primary_EmplrAddr", primary.EmplrAddr );
                SetResult("Primary_EmplrCity", primary.EmplrCity );
                SetResult("Primary_EmplrState", primary.EmplrState );
                SetResult("Primary_EmplrZip", primary.EmplrZip );
                SetResult("Primary_JobTitle", primary.JobTitle );
                SetResult("Primary_EmplmtLen", primary.EmplmtLen_rep );
                SetResult("Primary_EmpltStartD", primary.EmpltStartD_rep );
                SetResult("Primary_ProfLen", primary.ProfLen_rep );
                SetResult("Primary_ProfStartD", primary.ProfStartD_rep );
                SetResult("Primary_EmplrBusPhone", primary.EmplrBusPhone );
                SetResult("Primary_EmplrFax", primary.EmplrFax );
                SetResult("Primary_VerifSentD", primary.VerifSentD_rep );
                SetResult("Primary_VerifReorderedD", primary.VerifReorderedD_rep );
                SetResult("Primary_VerifRecvD", primary.VerifRecvD_rep );
                SetResult("Primary_VerifExpD", primary.VerifExpD_rep );
                SetResult("Primary_EmployeeIdVoe", primary.EmployeeIdVoe);
                SetResult("Primary_EmployerCodeVoe", primary.EmployerCodeVoe);
                SetResult("Primary_IsSpecialBorrowerEmployerRelationship", primary.IsSpecialBorrowerEmployerRelationship);
                SetResult("Primary_SelfOwnershipShareT", primary.SelfOwnershipShareT);

                SetResult("aEmplrBusPhoneLckd", GetString("OwnerT", "") == "0" ? dataApp.aBEmplrBusPhoneLckd : dataApp.aCEmplrBusPhoneLckd);
            }
        }
    }
}
