﻿namespace LendersOfficeApp.newlos
{
    using System;
    using System.Linq;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendersOffice.Security;

    public partial class ManageNewFeatures : BaseLoanPage
    {
        protected override void OnInit(EventArgs e)
        {
            UseNewFramework = true;
            base.OnInit(e);
        }

        protected void Page_Init(object sender, System.EventArgs e)
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowManageNewFeatures))
            {
                return;
            }

            this.PageTitle = "Manage New Features";
            this.PageID = "ManageNewFeatures";
        }

        protected bool isMigratedWithNonLegacyArchive = false;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void LoadData()
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowManageNewFeatures))
            {
                return;
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(ManageNewFeatures));
            dataLoan.InitLoad();

            // OPM 228640. If any archive exists currently that is not in the legacy archive,
            // we cannot let users go back to legacy.
            isMigratedWithNonLegacyArchive = !Broker.AllowMovingFilesBackToLegacyWithMigratedArchives &&
                dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                && !dataLoan.sClosingCostArchive.TrueForAll(newArchive => dataLoan.GFEArchives.Any(oldArchive => oldArchive.DateArchived == newArchive.DateArchived));

            var isTridMode = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

            Tools.Bind_sClosingCostFeeVersionT(CFPBUISettings, isTridMode || isMigratedWithNonLegacyArchive);

            Tools.SetDropDownListValue(CFPBUISettings, dataLoan.sClosingCostFeeVersionT);
            sGfeIsTPOTransactionIsCalculated.Checked = dataLoan.sGfeIsTPOTransactionIsCalculated;

            sExcludeToleranceCureFromFinal1003LenderCredit.Checked = dataLoan.sExcludeToleranceCureFromFinal1003LenderCredit;
            sConfLoanLimitDeterminationD.Text = dataLoan.sConfLoanLimitDeterminationD_rep;
            sConfLoanLimitDeterminationDLckd.Checked = dataLoan.sConfLoanLimitDeterminationDLckd;

            if (dataLoan.BrokerDB.EnableAutoPriceEligibilityProcess)
            {
                AutoPricePh.Visible = true;
                sIsIncludeInAutoPriceEligibilityProcess.Checked = dataLoan.sIsIncludeInAutoPriceEligibilityProcess;
                sIncludeInAutoPriceEligibilityProcessT.SelectedIndex = (int)dataLoan.sIncludeInAutoPriceEligibilityProcessT;
                if (dataLoan.sAutoPriceProcessResultLockedRate != 0)
                {
                    sAutoPriceProcessResultLockedPrice.Text = dataLoan.sAutoPriceProcessResultLockedPrice_rep;
                    sAutoPriceProcessResultLockedRate.Text = dataLoan.sAutoPriceProcessResultLockedRate_rep;

                    if (dataLoan.sAutoPriceProcessResultHistoricalPrice != 0)
                    {
                        sAutoPriceProcessResultHistoricalPrice.Text = dataLoan.sAutoPriceProcessResultHistoricalPrice_rep;
                    }
                }
            }
            else
            {
                AutoPricePh.Visible = false;
            }

            this.sUseInitialLeSignedDateAsIntentToProceedDate.Checked = dataLoan.sUseInitialLeSignedDateAsIntentToProceedDate;

            this.RegisterJsGlobalVariables("ShowUladMigrationControls",
                LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges) &&
                dataLoan.sLoanFileT == E_sLoanFileT.Test);

            this.RegisterJsGlobalVariables("IsMigratedToLatestUladDataLayer", dataLoan.sBorrowerApplicationCollectionT == EnumHelper.MaxValue<E_sLqbCollectionT>());
        }
    }
}
