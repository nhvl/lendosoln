<%@ Register TagPrefix="uc1" TagName="UpfrontMIP" Src="UpfrontMIP.ascx" %>

<%@ Page Language="c#" CodeBehind="LoanInfo.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.newlos.LoanInfo" %>

<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LoanInfo" Src="LoanInfo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="OtherFinancing" Src="OtherFinancing.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>LoanInfo</title>
</head>
<body bgcolor="gainsboro" scroll="yes" ms_positioning="FlowLayout">
    <form id="LoanInfo" method="post" runat="server">
    <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td nowrap class="Tabs">
                <uc1:Tabs runat="server" ID="Tabs" />
            </td>
        </tr>
        <tr>
            <td class="MainRightHeader" nowrap>
                <%= AspxTools.HtmlString(HeaderDisplay)%>
            </td>
        </tr>
        <tr>
            <td nowrap style="padding-left: 5px;">
                <uc1:LoanInfo ID="LoanInfoUC" runat="server"></uc1:LoanInfo>
                <uc1:OtherFinancing ID="OtherFinancing" runat="server"></uc1:OtherFinancing>
                <uc1:UpfrontMIP ID="UpfrontMIP" runat="server"></uc1:UpfrontMIP>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
